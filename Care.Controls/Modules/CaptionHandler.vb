﻿Imports Care.Global
Imports Care.Data

Public Class CaptionHandler

    Friend Shared Sub ChangeCaption(ByRef LabelControl As CareLabel)
        Dim _frm As New frmChangeCaption(LabelControl)
        _frm.ShowDialog()
    End Sub

    Friend Shared Sub ChangeCaption(ByRef FrameControl As CareFrame)
        Dim _frm As New frmChangeCaption(FrameControl)
        _frm.ShowDialog()
    End Sub

    Friend Shared Sub RemoveCaption(ByVal FormName As String, ByVal ControlName As String, ByVal Caption As String)

        Dim _C As Dictionary.ControlCaption = ReturnControlCaption(FormName, ControlName, Caption)
        Session.DictionaryControlCaptions.Remove(_C)

        Dim _SQL As String = ""

        _SQL += "delete from AppControlCaptions"
        _SQL += " where form_name = '" + FormName + "'"
        _SQL += " and control_name = '" + ControlName + "'"

        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

    End Sub

    Friend Shared Sub AddCaption(ByVal FormName As String, ByVal ControlName As String, ByVal Caption As String)

        Dim _C As Dictionary.ControlCaption = ReturnControlCaption(FormName, ControlName, Caption)
        Session.DictionaryControlCaptions.Add(_C)

        Dim _SQL As String = ""
        _SQL += "insert into AppControlCaptions"
        _SQL += " values ("
        _SQL += "'" + Guid.NewGuid.ToString + "'" + ","
        _SQL += "'" + FormName + "'" + ","
        _SQL += "'" + ControlName + "'" + ","
        _SQL += "'" + Caption + "'"
        _SQL += ")"

        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

    End Sub

    Private Shared Function ReturnControlCaption(ByVal FormName As String, ByVal ControlName As String, ByVal Caption As String) As Dictionary.ControlCaption

        Dim _C As New Dictionary.ControlCaption
        _C.FormName = FormName
        _C.ControlName = ControlName
        _C.ControlCaption = Caption

        Return _C

    End Function

    Friend Shared Sub UpdateCaption(ByVal FormName As String, ByVal ControlName As String, ByVal Caption As String)
        RemoveCaption(FormName, ControlName, Caption)
        AddCaption(FormName, ControlName, Caption)
    End Sub

End Class
