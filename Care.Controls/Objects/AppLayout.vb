﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class AppLayout
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_DateCreated As DateTime?
        Dim m_FormName As String
        Dim m_GridName As String
        Dim m_UserId As Guid?
        Dim m_Name As String
        Dim m_Description As String
        Dim m_Private As Boolean
        Dim m_QueryId As Guid?
        Dim m_Layout As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._DateCreated = Nothing
                ._FormName = ""
                ._GridName = ""
                ._UserId = Nothing
                ._Name = ""
                ._Description = ""
                ._Private = False
                ._QueryId = Nothing
                ._Layout = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._DateCreated = Nothing
                ._FormName = ""
                ._GridName = ""
                ._UserId = Nothing
                ._Name = ""
                ._Description = ""
                ._Private = False
                ._QueryId = Nothing
                ._Layout = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@DateCreated")> _
        Public Property _DateCreated() As DateTime?
            Get
                Return m_DateCreated
            End Get
            Set(ByVal value As DateTime?)
                m_DateCreated = value
            End Set
        End Property

        <StoredProcParameter("@FormName")> _
        Public Property _FormName() As String
            Get
                Return m_FormName
            End Get
            Set(ByVal value As String)
                m_FormName = value
            End Set
        End Property

        <StoredProcParameter("@GridName")> _
        Public Property _GridName() As String
            Get
                Return m_GridName
            End Get
            Set(ByVal value As String)
                m_GridName = value
            End Set
        End Property

        <StoredProcParameter("@UserId")> _
        Public Property _UserId() As Guid?
            Get
                Return m_UserId
            End Get
            Set(ByVal value As Guid?)
                m_UserId = value
            End Set
        End Property

        <StoredProcParameter("@Name")> _
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@Description")> _
        Public Property _Description() As String
            Get
                Return m_Description
            End Get
            Set(ByVal value As String)
                m_Description = value
            End Set
        End Property

        <StoredProcParameter("@Private")> _
        Public Property _Private() As Boolean
            Get
                Return m_Private
            End Get
            Set(ByVal value As Boolean)
                m_Private = value
            End Set
        End Property

        <StoredProcParameter("@QueryId")> _
        Public Property _QueryId() As Guid?
            Get
                Return m_QueryId
            End Get
            Set(ByVal value As Guid?)
                m_QueryId = value
            End Set
        End Property

        <StoredProcParameter("@Layout")> _
        Public Property _Layout() As String
            Get
                Return m_Layout
            End Get
            Set(ByVal value As String)
                m_Layout = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As AppLayout

            Dim _AppLayout As AppLayout
            _AppLayout = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getAppLayoutbyID", ID))
            Return _AppLayout

        End Function

        Public Shared Function RetreiveAll() As List(Of AppLayout)

            Dim _AppLayoutList As List(Of AppLayout)
            _AppLayoutList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getAppLayoutTable"))
            Return _AppLayoutList

        End Function

        Public Shared Sub SaveAll(ByVal AppLayoutList As List(Of AppLayout))

            For Each _AppLayout As AppLayout In AppLayoutList
                SaveRecord(_AppLayout)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal AppLayout As AppLayout) As Guid
            DAL.SaveRecord(Session.ConnectionString, AppLayout, "upsertAppLayout")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteAppLayoutbyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertAppLayout")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As AppLayout

            Dim _A As New AppLayout()

            If DR IsNot Nothing Then
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("ID"))
                    _A._DateCreated = GetDateTime(DR("date_created"))
                    _A._FormName = DR("form_name").ToString.Trim
                    _A._GridName = DR("grid_name").ToString.Trim
                    _A._UserId = GetGUID(DR("user_id"))
                    _A._Name = DR("name").ToString.Trim
                    _A._Description = DR("description").ToString.Trim
                    _A._Private = GetBoolean(DR("private"))
                    _A._QueryId = GetGUID(DR("query_id"))
                    _A._Layout = GetXML(DR("layout"))

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of AppLayout)

            Dim _AppLayoutList As New List(Of AppLayout)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _AppLayoutList.Add(PropertiesFromData(_DR))
            Next

            Return _AppLayoutList

        End Function


#End Region

    End Class

End Namespace
