﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Care.Controls")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Care Software Ltd")> 
<Assembly: AssemblyProduct("Care.Controls")> 
<Assembly: AssemblyCopyright("Copyright ©  2012")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("27084ae5-84a6-40f0-b13c-a99ce8dcefd1")>
<Assembly: AssemblyVersion("1.19.1.3")>
<Assembly: AssemblyFileVersion("1.19.1.3")>



' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 
