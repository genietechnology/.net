﻿Imports Care.Global
Imports System.Windows.Forms
Imports DevExpress.XtraBars.Controls

Public Class frmLayoutSave

    Private m_New As Boolean = False
    Private m_LayoutID As Guid? = Nothing
    Private m_FormName As String = ""
    Private m_GridName As String = ""
    Private m_QueryID As Guid? = Nothing
    Private m_LayoutXML As String = ""

    Public Sub New(ByVal LayoutID As Guid?, ByVal LayoutString As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_New = False
        m_LayoutID = LayoutID
        m_FormName = ""
        m_GridName = ""
        m_QueryID = Nothing
        m_LayoutXML = LayoutString
        DisplayLayout()

    End Sub

    Public Sub New(ByVal FormName As String, ByVal GridName As String, ByVal QueryID As Guid?, ByVal LayoutString As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_New = True
        m_LayoutID = Nothing
        m_FormName = FormName
        m_GridName = GridName
        m_QueryID = QueryID
        m_LayoutXML = LayoutString

    End Sub

    Private Sub DisplayLayout()

        Dim _Layout As Business.AppLayout = Business.AppLayout.RetreiveByID(m_LayoutID.Value)
        If _Layout IsNot Nothing Then
            txtName.Text = _Layout._Name
            txtDescription.Text = _Layout._Description
            chkPrivate.Checked = _Layout._Private
            _Layout = Nothing
        End If

    End Sub

    Private Sub frmLayoutSave_Load(sender As Object, e As EventArgs) Handles Me.Load

        txtName.BackColor = Session.ChangeColour
        txtDescription.BackColor = Session.ChangeColour
        chkPrivate.BackColor = Session.ChangeColour

        btnSaveNew.Enabled = Not m_New

    End Sub

    Public ReadOnly Property LayoutID As Guid?
        Get
            Return m_LayoutID
        End Get
    End Property

    Private Function DoChecks() As Boolean

        If txtName.Text = "" Then
            CareMessage("Please enter a name for this layout.", MessageBoxIcon.Warning, "Check Mandatory Fields")
            Return False
        End If

        Return True

    End Function

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        Save(m_New)
    End Sub

    Private Sub Save(ByVal NewRecord As Boolean)

        If Not DoChecks() Then Exit Sub

        Dim _Layout As Business.AppLayout
        If NewRecord Then
            _Layout = New Business.AppLayout()
            _Layout._DateCreated = Now
            _Layout._FormName = m_FormName
            _Layout._GridName = m_GridName
            _Layout._UserId = Session.CurrentUser.ID
            _Layout._QueryId = m_QueryID
        Else
            _Layout = Business.AppLayout.RetreiveByID(m_LayoutID.Value)
        End If

        With _Layout
            ._Name = txtName.Text
            ._Description = txtDescription.Text
            ._Private = chkPrivate.Checked
            ._Layout = m_LayoutXML
            .Store()
        End With

        m_LayoutID = _Layout._ID

        _Layout = Nothing

        Me.DialogResult = DialogResult.OK
        Me.Close()

    End Sub

    Private Sub btnSaveNew_Click(sender As Object, e As EventArgs) Handles btnSaveNew.Click
        Save(True)
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub
End Class