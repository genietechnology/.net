﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLayoutManager
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLayoutManager))
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.chkAllLayouts = New DevExpress.XtraEditors.CheckEdit()
        Me.chkPrivateLayouts = New DevExpress.XtraEditors.CheckEdit()
        Me.chkMyLayouts = New DevExpress.XtraEditors.CheckEdit()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.btnProperties = New DevExpress.XtraEditors.SimpleButton()
        Me.btnLoad = New DevExpress.XtraEditors.SimpleButton()
        Me.btnClose = New DevExpress.XtraEditors.SimpleButton()
        Me.cgLayouts = New Care.Controls.CareGrid()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.chkAllLayouts.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkPrivateLayouts.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkMyLayouts.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.chkAllLayouts)
        Me.GroupControl1.Controls.Add(Me.chkPrivateLayouts)
        Me.GroupControl1.Controls.Add(Me.chkMyLayouts)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(518, 35)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "GroupControl1"
        '
        'chkAllLayouts
        '
        Me.chkAllLayouts.Location = New System.Drawing.Point(299, 8)
        Me.chkAllLayouts.Name = "chkAllLayouts"
        Me.chkAllLayouts.Properties.Caption = "Other Users' Layouts"
        Me.chkAllLayouts.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.chkAllLayouts.Properties.RadioGroupIndex = 0
        Me.chkAllLayouts.Size = New System.Drawing.Size(146, 19)
        Me.chkAllLayouts.TabIndex = 2
        Me.chkAllLayouts.TabStop = False
        '
        'chkPrivateLayouts
        '
        Me.chkPrivateLayouts.Location = New System.Drawing.Point(162, 8)
        Me.chkPrivateLayouts.Name = "chkPrivateLayouts"
        Me.chkPrivateLayouts.Properties.Caption = "My Private Layouts"
        Me.chkPrivateLayouts.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.chkPrivateLayouts.Properties.RadioGroupIndex = 0
        Me.chkPrivateLayouts.Size = New System.Drawing.Size(131, 19)
        Me.chkPrivateLayouts.TabIndex = 1
        Me.chkPrivateLayouts.TabStop = False
        '
        'chkMyLayouts
        '
        Me.chkMyLayouts.EditValue = True
        Me.chkMyLayouts.Location = New System.Drawing.Point(9, 8)
        Me.chkMyLayouts.Name = "chkMyLayouts"
        Me.chkMyLayouts.Properties.Caption = "Layouts I've Created"
        Me.chkMyLayouts.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.chkMyLayouts.Properties.RadioGroupIndex = 0
        Me.chkMyLayouts.Size = New System.Drawing.Size(147, 19)
        Me.chkMyLayouts.TabIndex = 0
        '
        'PanelControl1
        '
        Me.PanelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl1.Controls.Add(Me.btnProperties)
        Me.PanelControl1.Controls.Add(Me.btnLoad)
        Me.PanelControl1.Controls.Add(Me.btnClose)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelControl1.Location = New System.Drawing.Point(0, 335)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(542, 30)
        Me.PanelControl1.TabIndex = 2
        '
        'btnProperties
        '
        Me.btnProperties.Location = New System.Drawing.Point(143, 0)
        Me.btnProperties.Name = "btnProperties"
        Me.btnProperties.Size = New System.Drawing.Size(125, 23)
        Me.btnProperties.TabIndex = 1
        Me.btnProperties.Text = "Layout Properties"
        '
        'btnLoad
        '
        Me.btnLoad.Location = New System.Drawing.Point(12, 0)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(125, 23)
        Me.btnLoad.TabIndex = 0
        Me.btnLoad.Text = "Load Selected Layout"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Location = New System.Drawing.Point(405, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(125, 23)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "Close"
        '
        'cgLayouts
        '
        Me.cgLayouts.AllowBuildColumns = True
        Me.cgLayouts.AllowHorizontalScroll = False
        Me.cgLayouts.AllowMultiSelect = False
        Me.cgLayouts.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgLayouts.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgLayouts.Appearance.Options.UseFont = True
        Me.cgLayouts.AutoSizeByData = True
        Me.cgLayouts.HideFirstColumn = False
        Me.cgLayouts.Location = New System.Drawing.Point(12, 57)
        Me.cgLayouts.Name = "cgLayouts"
        Me.cgLayouts.QueryID = Nothing
        Me.cgLayouts.SearchAsYouType = True
        Me.cgLayouts.ShowFindPanel = False
        Me.cgLayouts.ShowGroupByBox = False
        Me.cgLayouts.ShowNavigator = False
        Me.cgLayouts.Size = New System.Drawing.Size(518, 270)
        Me.cgLayouts.TabIndex = 1
        Me.cgLayouts.TabStop = False
        '
        'frmLayoutManager
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(542, 365)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.cgLayouts)
        Me.Controls.Add(Me.GroupControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(558, 403)
        Me.Name = "frmLayoutManager"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Load Layout"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.chkAllLayouts.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkPrivateLayouts.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkMyLayouts.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents chkAllLayouts As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkPrivateLayouts As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkMyLayouts As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents cgLayouts As Care.Controls.CareGrid
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnLoad As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnProperties As DevExpress.XtraEditors.SimpleButton
End Class
