﻿Imports Care.Global
Imports Care.Data
Imports DevExpress.XtraGrid.Views.Base

Public Class frmTabConfiguration

    Private m_FormName As String = ""
    Private m_TabControl As CareTab = Nothing
    Private m_GridTabs As New List(Of TabPageItem)
    Private m_GridPerms As New List(Of Dictionary.ControlSecurity)
    Private m_Perms As New List(Of Dictionary.ControlSecurity)
    Private m_SelectedTabPageItem As TabPageItem = Nothing

    Public Sub New(ByRef TabControl As CareTab)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_TabControl = TabControl
        m_FormName = TabControl.FindForm.Name

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub frmTabConfiguration_Load(sender As Object, e As EventArgs) Handles Me.Load

        'populate tab pages from control
        For Each _tp As DevExpress.XtraTab.XtraTabPage In m_TabControl.TabPages
            m_GridTabs.Add(New TabPageItem(_tp.Name, _tp.Text))
        Next

        cgTabs.Populate(m_GridTabs)

        'populate any existing security settings for this tab control
        Dim _Q As IEnumerable(Of Dictionary.ControlSecurity) = Nothing

        _Q = From _S As Dictionary.ControlSecurity In Session.DictionaryControlSecurity
             Where _S.FormName = m_FormName And _S.ControlTabName = m_TabControl.Name

        For Each _S In _Q
            m_Perms.Add(_S)
        Next

        DisplaySecurity()

    End Sub

    Private Sub DisplaySecurity()

        If m_SelectedTabPageItem Is Nothing Then Exit Sub

        m_GridPerms.Clear()

        'populate any existing security settings for this tab control
        Dim _Q As IEnumerable(Of Dictionary.ControlSecurity) = Nothing

        _Q = From _S As Dictionary.ControlSecurity In m_Perms
             Where _S.ControlName = m_SelectedTabPageItem.TabName

        For Each _S In _Q
            m_GridPerms.Add(_S)
        Next

        cgPerms.BeginUpdate

        cgPerms.Populate(m_GridPerms)

        cgPerms.Columns("FormName").Visible = False
        cgPerms.Columns("ControlName").Visible = False
        cgPerms.Columns("ControlType").Visible = False
        cgPerms.Columns("ControlTabName").Visible = False
        cgPerms.Columns("ControlSecurityType").Visible = False
        cgPerms.Columns("ControlSecurityID").Visible = False
        cgPerms.Columns("ControlSecurityName").Caption = "Name"

        cgPerms.EndUpdate()

        If m_GridPerms.Count > 0 Then
            btnRemove.Enabled = True
        Else
            btnRemove.Enabled = False
        End If

    End Sub

    Private Sub cgTabs_FocusedRowChanged(sender As Object, e As FocusedRowChangedEventArgs) Handles cgTabs.FocusedRowChanged

        m_SelectedTabPageItem = Nothing

        If e Is Nothing Then Exit Sub
        If e.FocusedRowHandle < 0 Then Exit Sub

        m_SelectedTabPageItem = CType(cgTabs.GetRowObject(e.FocusedRowHandle), TabPageItem)

        DisplaySecurity()

    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click

        Dim _Caption As String = ""
        Dim _SQL As String = ""
        Dim _DR As DataRow = Nothing

        If radUsers.Checked Then
            _SQL = "select ID, username as 'Username', fullname as 'Name' from AppUsers order by fullname"
            _Caption = "Select User"
        Else
            _SQL = "select ID, name as 'Name' from AppGroups order by name"
            _Caption = "Select Group"
        End If

        If SelectGrid(_DR, _Caption, _SQL) Then

            Dim _s As New Dictionary.ControlSecurity
            _s.FormName = m_FormName
            _s.ControlType = [Global].Dictionary.ControlSecurity.EnumControlType.Tab
            _s.ControlTabName = m_TabControl.Name
            _s.ControlName = m_SelectedTabPageItem.TabName

            If radGroups.Checked Then
                _s.ControlSecurityType = [Global].Dictionary.ControlSecurity.EnumSecurityType.Group
            Else
                _s.ControlSecurityType = [Global].Dictionary.ControlSecurity.EnumSecurityType.User
            End If

            _s.ControlSecurityID = New Guid(_DR.Item("ID").ToString)
            _s.ControlSecurityName = _DR.Item("Name").ToString

            m_Perms.Add(_s)
            DisplaySecurity()

        End If

    End Sub

    Private Function SelectGrid(ByRef ReturnedRow As DataRow, FormCaption As String, SQL As String) As Boolean

        Dim _frm As New frmGrid(SQL)
        _frm.Text = FormCaption

        _frm.ShowDialog()
        If _frm.DialogResult = DialogResult.OK Then
            ReturnedRow = _frm.SelectedRow
            Return True
        Else
            Return False
        End If

    End Function

    Private Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click

        If cgPerms.RecordCount = 0 Then Exit Sub
        If cgPerms.CurrentRowObject Is Nothing Then Exit Sub

        Dim _s As Dictionary.ControlSecurity = CType(cgPerms.CurrentRowObject, Dictionary.ControlSecurity)
        m_Perms.Remove(_s)

        DisplaySecurity()

    End Sub

    Private Class TabPageItem

        Public Sub New(ByVal Name As String, ByVal Caption As String)
            TabName = Name
            TabCaption = Caption
        End Sub

        Property TabName As String
        Property TabCaption As String

    End Class

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        UpdateSessionSecurity()
        UpdateDatabase()
        Me.DialogResult = DialogResult.OK
        Me.Close()
    End Sub

    Private Sub UpdateSessionSecurity()

        'remove them all first
        For Each _p In m_Perms
            Session.DictionaryControlSecurity.Remove(_p)
        Next

        'now add them
        Session.DictionaryControlSecurity.AddRange(m_Perms)

    End Sub

    Private Sub UpdateDatabase()

        Dim _SQL As String = ""
        _SQL += "delete from AppControlSecurity"
        _SQL += " where form_name = '" + m_FormName + "'"
        _SQL += " and control_tab_name = '" + m_TabControl.Name + "'"

        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        For Each _p In m_Perms

            _SQL = ""
            _SQL += "insert into AppControlSecurity"
            _SQL += " values ("
            _SQL += "'" + Guid.NewGuid.ToString + "'" + ","
            _SQL += "'" + _p.FormName + "'" + ","
            _SQL += "'" + _p.ControlName + "'" + ","
            _SQL += "'" + _p.ControlType.ToString + "'" + ","
            _SQL += "'" + _p.ControlTabName + "'" + ","
            _SQL += "'" + _p.ControlSecurityType.ToString + "'" + ","
            _SQL += "'" + _p.ControlSecurityID.ToString + "'" + ","
            _SQL += "'" + _p.ControlSecurityName + "'"
            _SQL += ")"

            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        Next

    End Sub

    Private Sub radUsers_CheckedChanged(sender As Object, e As EventArgs) Handles radUsers.CheckedChanged

    End Sub

    Private Sub radGroups_CheckedChanged(sender As Object, e As EventArgs) Handles radGroups.CheckedChanged

    End Sub
End Class