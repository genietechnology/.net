﻿Imports Care.Global

Public Class frmChangeCaption

    Private m_FormName As String = ""
    Private m_ControlName As String = ""
    Private m_ControlType As String = ""
    Private m_DesignTimeCaption As String = ""
    Private m_Label As CareLabel = Nothing
    Private m_Frame As CareFrame = Nothing

    Public Sub New(ByRef ControlIn As CareLabel)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_Label = ControlIn
        m_FormName = ControlIn.FindForm.Name
        m_ControlName = ControlIn.Name
        m_ControlType = ControlIn.GetType.Name
        txtCaption.Text = ControlIn.Text

        m_DesignTimeCaption = ControlIn.AccessibleName
        If ControlIn.AccessibleName = "" Then btnDefault.Enabled = False

    End Sub

    Public Sub New(ByRef ControlIn As CareFrame)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_Frame = ControlIn
        m_FormName = ControlIn.FindForm.Name
        m_ControlName = ControlIn.Name
        m_ControlType = ControlIn.GetType.Name
        txtCaption.Text = ControlIn.Text

        m_DesignTimeCaption = ControlIn.AccessibleName
        If ControlIn.AccessibleName = "" Then btnDefault.Enabled = False

    End Sub

    Private Sub frmChangeCaption_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = "Change Caption for: " + m_ControlName
        txtCaption.BackColor = Session.ChangeColour
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click

        Select Case m_ControlType

            Case "CareLabel"
                m_Label.Text = txtCaption.Text

            Case "CareFrame"
                m_Frame.Text = txtCaption.Text

        End Select

        CaptionHandler.UpdateCaption(m_FormName, m_ControlName, txtCaption.Text)

        Me.Close()

    End Sub

    Private Sub btnDefault_Click(sender As Object, e As EventArgs) Handles btnDefault.Click

        Select Case m_ControlType

            Case "CareLabel"
                m_Label.Text = m_DesignTimeCaption
                m_Label.AccessibleName = ""

            Case "CareFrame"
                m_Frame.Text = m_DesignTimeCaption
                m_Frame.AccessibleName = ""

        End Select

        CaptionHandler.RemoveCaption(m_FormName, m_ControlName, txtCaption.Text)
        Me.Close()

    End Sub

End Class