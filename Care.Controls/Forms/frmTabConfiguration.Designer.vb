﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTabConfiguration
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.cgTabs = New Care.Controls.CareGrid()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.btnRemove = New Care.Controls.CareButton(Me.components)
        Me.btnAdd = New Care.Controls.CareButton(Me.components)
        Me.radUsers = New Care.Controls.CareRadioButton(Me.components)
        Me.radGroups = New Care.Controls.CareRadioButton(Me.components)
        Me.cgPerms = New Care.Controls.CareGrid()
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        Me.btnOK = New Care.Controls.CareButton(Me.components)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.radUsers.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radGroups.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.cgTabs)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(371, 464)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Tabs"
        '
        'cgTabs
        '
        Me.cgTabs.AllowBuildColumns = True
        Me.cgTabs.AllowEdit = False
        Me.cgTabs.AllowHorizontalScroll = False
        Me.cgTabs.AllowMultiSelect = False
        Me.cgTabs.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgTabs.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgTabs.Appearance.Options.UseFont = True
        Me.cgTabs.AutoSizeByData = True
        Me.cgTabs.DisableAutoSize = False
        Me.cgTabs.DisableDataFormatting = False
        Me.cgTabs.FocusedRowHandle = -2147483648
        Me.cgTabs.HideFirstColumn = False
        Me.cgTabs.Location = New System.Drawing.Point(5, 23)
        Me.cgTabs.Name = "cgTabs"
        Me.cgTabs.PreviewColumn = ""
        Me.cgTabs.QueryID = Nothing
        Me.cgTabs.RowAutoHeight = False
        Me.cgTabs.SearchAsYouType = True
        Me.cgTabs.ShowAutoFilterRow = False
        Me.cgTabs.ShowFindPanel = False
        Me.cgTabs.ShowGroupByBox = False
        Me.cgTabs.ShowLoadingPanel = False
        Me.cgTabs.ShowNavigator = False
        Me.cgTabs.Size = New System.Drawing.Size(361, 436)
        Me.cgTabs.TabIndex = 0
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.btnRemove)
        Me.GroupControl2.Controls.Add(Me.btnAdd)
        Me.GroupControl2.Controls.Add(Me.radUsers)
        Me.GroupControl2.Controls.Add(Me.radGroups)
        Me.GroupControl2.Controls.Add(Me.cgPerms)
        Me.GroupControl2.Location = New System.Drawing.Point(389, 12)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(391, 464)
        Me.GroupControl2.TabIndex = 2
        Me.GroupControl2.Text = "Tab Access Permissions"
        '
        'btnRemove
        '
        Me.btnRemove.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemove.Location = New System.Drawing.Point(86, 436)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(75, 23)
        Me.btnRemove.TabIndex = 4
        Me.btnRemove.Text = "Remove"
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.Location = New System.Drawing.Point(5, 436)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(75, 23)
        Me.btnAdd.TabIndex = 3
        Me.btnAdd.Text = "Add"
        '
        'radUsers
        '
        Me.radUsers.Location = New System.Drawing.Point(118, 23)
        Me.radUsers.Name = "radUsers"
        Me.radUsers.Properties.AutoWidth = True
        Me.radUsers.Properties.Caption = "Excluded Users"
        Me.radUsers.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radUsers.Properties.RadioGroupIndex = 0
        Me.radUsers.Size = New System.Drawing.Size(95, 19)
        Me.radUsers.TabIndex = 1
        Me.radUsers.TabStop = False
        '
        'radGroups
        '
        Me.radGroups.EditValue = True
        Me.radGroups.Location = New System.Drawing.Point(10, 23)
        Me.radGroups.Name = "radGroups"
        Me.radGroups.Properties.AutoWidth = True
        Me.radGroups.Properties.Caption = "Excluded Groups"
        Me.radGroups.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radGroups.Properties.RadioGroupIndex = 0
        Me.radGroups.Size = New System.Drawing.Size(102, 19)
        Me.radGroups.TabIndex = 0
        '
        'cgPerms
        '
        Me.cgPerms.AllowBuildColumns = True
        Me.cgPerms.AllowEdit = False
        Me.cgPerms.AllowHorizontalScroll = False
        Me.cgPerms.AllowMultiSelect = False
        Me.cgPerms.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgPerms.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgPerms.Appearance.Options.UseFont = True
        Me.cgPerms.AutoSizeByData = True
        Me.cgPerms.DisableAutoSize = False
        Me.cgPerms.DisableDataFormatting = False
        Me.cgPerms.FocusedRowHandle = -2147483648
        Me.cgPerms.HideFirstColumn = False
        Me.cgPerms.Location = New System.Drawing.Point(5, 48)
        Me.cgPerms.Name = "cgPerms"
        Me.cgPerms.PreviewColumn = ""
        Me.cgPerms.QueryID = Nothing
        Me.cgPerms.RowAutoHeight = False
        Me.cgPerms.SearchAsYouType = True
        Me.cgPerms.ShowAutoFilterRow = False
        Me.cgPerms.ShowFindPanel = False
        Me.cgPerms.ShowGroupByBox = False
        Me.cgPerms.ShowLoadingPanel = False
        Me.cgPerms.ShowNavigator = False
        Me.cgPerms.Size = New System.Drawing.Size(381, 382)
        Me.cgPerms.TabIndex = 2
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(705, 482)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(624, 482)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 3
        Me.btnOK.Text = "OK"
        '
        'frmTabConfiguration
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(792, 511)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTabConfiguration"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Tab Configuration"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.radUsers.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radGroups.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cgTabs As CareGrid
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cgPerms As CareGrid
    Friend WithEvents btnOK As CareButton
    Friend WithEvents btnCancel As CareButton
    Friend WithEvents radUsers As CareRadioButton
    Friend WithEvents radGroups As CareRadioButton
    Friend WithEvents btnAdd As CareButton
    Friend WithEvents btnRemove As CareButton
End Class
