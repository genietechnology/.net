﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmObjectSecurity
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        Me.btnOK = New Care.Controls.CareButton(Me.components)
        Me.btnRemove = New Care.Controls.CareButton(Me.components)
        Me.btnAdd = New Care.Controls.CareButton(Me.components)
        Me.radUsers = New Care.Controls.CareRadioButton(Me.components)
        Me.radGroups = New Care.Controls.CareRadioButton(Me.components)
        Me.cgPerms = New Care.Controls.CareGrid()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.radUsers.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radGroups.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.btnRemove)
        Me.GroupControl2.Controls.Add(Me.btnAdd)
        Me.GroupControl2.Controls.Add(Me.radUsers)
        Me.GroupControl2.Controls.Add(Me.radGroups)
        Me.GroupControl2.Controls.Add(Me.cgPerms)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(391, 399)
        Me.GroupControl2.TabIndex = 5
        Me.GroupControl2.Text = "Tab Access Permissions"
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(328, 417)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 7
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(247, 417)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 6
        Me.btnOK.Text = "OK"
        '
        'btnRemove
        '
        Me.btnRemove.Location = New System.Drawing.Point(86, 371)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(75, 23)
        Me.btnRemove.TabIndex = 4
        Me.btnRemove.Text = "Remove"
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(5, 371)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(75, 23)
        Me.btnAdd.TabIndex = 3
        Me.btnAdd.Text = "Add"
        '
        'radUsers
        '
        Me.radUsers.Location = New System.Drawing.Point(118, 23)
        Me.radUsers.Name = "radUsers"
        Me.radUsers.Properties.AutoWidth = True
        Me.radUsers.Properties.Caption = "Excluded Users"
        Me.radUsers.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radUsers.Properties.RadioGroupIndex = 0
        Me.radUsers.Size = New System.Drawing.Size(95, 19)
        Me.radUsers.TabIndex = 1
        Me.radUsers.TabStop = False
        '
        'radGroups
        '
        Me.radGroups.EditValue = True
        Me.radGroups.Location = New System.Drawing.Point(10, 23)
        Me.radGroups.Name = "radGroups"
        Me.radGroups.Properties.AutoWidth = True
        Me.radGroups.Properties.Caption = "Excluded Groups"
        Me.radGroups.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radGroups.Properties.RadioGroupIndex = 0
        Me.radGroups.Size = New System.Drawing.Size(102, 19)
        Me.radGroups.TabIndex = 0
        '
        'cgPerms
        '
        Me.cgPerms.AllowBuildColumns = True
        Me.cgPerms.AllowEdit = False
        Me.cgPerms.AllowHorizontalScroll = False
        Me.cgPerms.AllowMultiSelect = False
        Me.cgPerms.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgPerms.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgPerms.Appearance.Options.UseFont = True
        Me.cgPerms.AutoSizeByData = True
        Me.cgPerms.DisableAutoSize = False
        Me.cgPerms.DisableDataFormatting = False
        Me.cgPerms.FocusedRowHandle = -2147483648
        Me.cgPerms.HideFirstColumn = False
        Me.cgPerms.Location = New System.Drawing.Point(5, 48)
        Me.cgPerms.Name = "cgPerms"
        Me.cgPerms.PreviewColumn = ""
        Me.cgPerms.QueryID = Nothing
        Me.cgPerms.RowAutoHeight = False
        Me.cgPerms.SearchAsYouType = True
        Me.cgPerms.ShowAutoFilterRow = False
        Me.cgPerms.ShowFindPanel = False
        Me.cgPerms.ShowGroupByBox = False
        Me.cgPerms.ShowLoadingPanel = False
        Me.cgPerms.ShowNavigator = False
        Me.cgPerms.Size = New System.Drawing.Size(381, 317)
        Me.cgPerms.TabIndex = 2
        '
        'frmObjectSecurity
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(413, 448)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.GroupControl2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmObjectSecurity"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Object Security"
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.radUsers.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radGroups.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnCancel As CareButton
    Friend WithEvents btnOK As CareButton
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnRemove As CareButton
    Friend WithEvents btnAdd As CareButton
    Friend WithEvents radUsers As CareRadioButton
    Friend WithEvents radGroups As CareRadioButton
    Friend WithEvents cgPerms As CareGrid
End Class
