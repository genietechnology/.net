﻿Imports Care.Global
Imports System.Windows.Forms

Public Class frmLayoutManager

    Private m_FormName As String = ""
    Private m_GridName As String = ""
    Private m_QueryID As Guid? = Nothing
    Private m_ReturnLayoutID As Guid? = Nothing
    Private m_ReturnLayoutString As String = ""

    Public Sub New(ByVal FormName As String, ByVal GridName As String, ByVal QueryID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_FormName = FormName
        m_GridName = GridName
        m_QueryID = QueryID

    End Sub

    Private Sub frmLayoutManager_Load(sender As Object, e As EventArgs) Handles Me.Load
        RefreshGrid()
    End Sub

#Region "Properties"

    Public ReadOnly Property LayoutID As Guid?
        Get
            Return m_ReturnLayoutID
        End Get
    End Property

    Public ReadOnly Property LayoutString As String
        Get
            Return m_ReturnLayoutString
        End Get
    End Property

#End Region

    Private Sub chkMyLayouts_Click(sender As Object, e As EventArgs) Handles chkMyLayouts.CheckedChanged
        RefreshGrid()
    End Sub

    Private Sub chkPrivateLayouts_Click(sender As Object, e As EventArgs) Handles chkPrivateLayouts.CheckedChanged
        RefreshGrid()
    End Sub

    Private Sub chkAllLayouts_Click(sender As Object, e As EventArgs) Handles chkAllLayouts.CheckedChanged
        RefreshGrid()
    End Sub

    Private Sub RefreshGrid()

        Dim _SQL As String = ""
        Dim _Private As String = ""
        Dim _UserOperator As String = ""
        Dim _Order As String = " order by date_created desc"

        If chkMyLayouts.Checked Then
            _Private = "0"
            _UserOperator = " = "
        Else
            If chkPrivateLayouts.Checked Then
                _Private = "1"
                _UserOperator = " = "
            Else
                _Private = "0"
                _UserOperator = " <> "
            End If
        End If

        _SQL = "select AppLayouts.ID as 'ID', name as 'Name', fullname as 'User'," & _
               " private as 'Private', description as 'Description'" & _
               " from AppLayouts" & _
               " left join AppUsers on AppUsers.ID = AppLayouts.user_id" & _
               " where AppLayouts.form_name = '" + m_FormName + "'" & _
               " and AppLayouts.grid_name = '" + m_GridName + "'" & _
               " and AppLayouts.user_id" + _UserOperator + "'" + Session.CurrentUser.ID.ToString + "'" & _
               " and AppLayouts.private = " + _Private

        If m_QueryID IsNot Nothing Then _SQL += " and AppLayouts.query_id = '" + m_QueryID.Value.ToString + "'"
        _SQL += _Order

        cgLayouts.Populate(Session.ConnectionString, _SQL)
        cgLayouts.Columns("ID").Visible = False

        If cgLayouts.RecordCount > 0 Then
            btnLoad.Enabled = True
            btnProperties.Enabled = True
        Else
            btnLoad.Enabled = False
            btnProperties.Enabled = False
        End If

    End Sub

    Private Sub LoadLayout()

        If cgLayouts.CurrentRow Is Nothing Then Exit Sub
        m_ReturnLayoutID = New Guid(cgLayouts.CurrentRow.Item("ID").ToString)

        Dim _Layout As Business.AppLayout = Business.AppLayout.RetreiveByID(m_ReturnLayoutID.Value)
        If _Layout IsNot Nothing Then

            m_ReturnLayoutString = _Layout._Layout.ToString
            _Layout = Nothing

            Me.DialogResult = DialogResult.OK
            Me.Close()

        End If

    End Sub

    Private Sub cgLayouts_GridDoubleClick(sender As Object, e As EventArgs) Handles cgLayouts.GridDoubleClick
        LoadLayout()
    End Sub

    Private Sub btnLoad_Click(sender As Object, e As EventArgs) Handles btnLoad.Click
        LoadLayout()
    End Sub

    Private Sub btnProperties_Click(sender As Object, e As EventArgs) Handles btnProperties.Click

    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub
End Class