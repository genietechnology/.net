﻿Imports Care.Global
Imports DevExpress.XtraGrid.Views.Base

Public Class frmGrid

    Private m_SelectedRow As DataRow = Nothing

    Public ReadOnly Property SelectedRow As DataRow
        Get
            Return m_SelectedRow
        End Get
    End Property

    Public Sub New(ByVal SQL As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        cgRecords.HideFirstColumn = True
        cgRecords.Populate(Session.ConnectionString, SQL)

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnSelect_Click(sender As Object, e As EventArgs) Handles btnSelect.Click
        Me.DialogResult = DialogResult.OK
        Me.Close()
    End Sub

    Private Sub cgRecords_FocusedRowChanged(sender As Object, e As FocusedRowChangedEventArgs) Handles cgRecords.FocusedRowChanged

        If e Is Nothing Then Exit Sub
        If e.FocusedRowHandle < 0 Then Exit Sub

        m_SelectedRow = cgRecords.GetRow(e.FocusedRowHandle)

    End Sub

    Private Sub cgRecords_GridDoubleClick(sender As Object, e As EventArgs) Handles cgRecords.GridDoubleClick
        Me.DialogResult = DialogResult.OK
        Me.Close()
    End Sub
End Class