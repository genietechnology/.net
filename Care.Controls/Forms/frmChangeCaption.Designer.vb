﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChangeCaption
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.txtCaption = New Care.Controls.CareTextBox()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.btnCancel = New Care.Controls.CareButton()
        Me.btnOK = New Care.Controls.CareButton()
        Me.btnDefault = New Care.Controls.CareButton()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.txtCaption.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.txtCaption)
        Me.GroupControl3.Controls.Add(Me.LabelControl1)
        Me.GroupControl3.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(391, 59)
        Me.GroupControl3.TabIndex = 0
        Me.GroupControl3.Text = "Caption"
        '
        'txtCaption
        '
        Me.txtCaption.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtCaption.EnterMoveNextControl = True
        Me.txtCaption.Location = New System.Drawing.Point(74, 28)
        Me.txtCaption.MaxLength = 0
        Me.txtCaption.Name = "txtCaption"
        Me.txtCaption.NumericAllowNegatives = False
        Me.txtCaption.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtCaption.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCaption.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtCaption.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtCaption.Properties.Appearance.Options.UseFont = True
        Me.txtCaption.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCaption.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtCaption.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtCaption.Size = New System.Drawing.Size(312, 22)
        Me.txtCaption.TabIndex = 1
        Me.txtCaption.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCaption.ToolTipText = ""
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(10, 32)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(58, 13)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Tab Caption"
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(328, 77)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(247, 77)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "OK"
        '
        'btnDefault
        '
        Me.btnDefault.Location = New System.Drawing.Point(12, 77)
        Me.btnDefault.Name = "btnDefault"
        Me.btnDefault.Size = New System.Drawing.Size(114, 23)
        Me.btnDefault.TabIndex = 1
        Me.btnDefault.Text = "Revert to Default"
        '
        'frmChangeCaption
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(413, 109)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnDefault)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.GroupControl3)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmChangeCaption"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Change Caption"
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.txtCaption.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtCaption As CareTextBox
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnCancel As CareButton
    Friend WithEvents btnOK As CareButton
    Friend WithEvents btnDefault As CareButton
End Class
