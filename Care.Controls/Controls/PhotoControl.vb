﻿Imports Care.Global

Public Class PhotoControl

    Public Enum EnumStatus
        NoChange
        Created
        Rotated
        Deleted
    End Enum

    Private m_DocumentID As Guid? = Nothing
    Private m_Status As EnumStatus = EnumStatus.NoChange
    Private m_HasPhoto As Boolean = False
    Private m_FileExtension As String = ""

    <Obsolete("Do not use the Tag Property.", True)>
    Public Shadows Property Tag As Object

    Public ReadOnly Property FileExtension As String
        Get
            Return m_FileExtension
        End Get
    End Property

    Public ReadOnly Property Status As EnumStatus
        Get
            Return m_Status
        End Get
    End Property

    Public Property Image As System.Drawing.Image
        Get
            Return pic.Image
        End Get
        Set(value As System.Drawing.Image)
            pic.Image = value
            If pic.Image Is Nothing Then
                m_HasPhoto = False
            Else
                m_HasPhoto = True
            End If
        End Set
    End Property

    Public Property DocumentID As Guid?
        Get
            Return m_DocumentID
        End Get
        Set(value As Guid?)
            m_DocumentID = value
        End Set
    End Property

    Public Sub Clear()
        m_HasPhoto = False
        pic.Image = Nothing
        pic.ImageLocation = ""
    End Sub

    Private Sub bbiUpload_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiUpload.ItemClick

        'The filter string must contain a description of the filter, 
        'followed by the vertical bar (|) and the filter pattern.
        'The strings for different filtering options must also be separated by the vertical bar.
        ' Example: "Text files (*.txt)|*.txt|All files (*.*)|*.*"

        With OpenFileDialog1
            .Title = "Browse for Picture to Upload"
            .Filter = "Picture Files|*.jpg;*.png;*.bmp|JPEG Files|*.jpg|PNG Files|*.png|Bitmap Files|*.bmp|All Files|*.*"
            .ShowDialog()
        End With

    End Sub

    Private Sub bbiRemove_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiRemove.ItemClick
        If CareMessage("Are you sure you want to remove this photo?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Remove Photo") = MsgBoxResult.Yes Then
            m_HasPhoto = False
            m_Status = EnumStatus.Deleted
            pic.Image = Nothing
        End If
    End Sub

    Private Sub OpenFileDialog1_FileOk(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles OpenFileDialog1.FileOk
        pic.ImageLocation = OpenFileDialog1.FileName
        pic.Load()
        m_HasPhoto = True
        m_Status = EnumStatus.Created
        m_FileExtension = New IO.FileInfo(pic.ImageLocation).Extension
    End Sub

    Private Sub PictureBox1_MouseDown(sender As Object, e As MouseEventArgs) Handles pic.MouseDown
        If e.Button = MouseButtons.Right Then
            If m_HasPhoto Then
                bbiRemove.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
            Else
                bbiRemove.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            End If
            ctxPopup.ShowPopup(MousePosition)
        End If
    End Sub

    Private Sub bbiRotateLeft_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiRotateLeft.ItemClick
        If m_HasPhoto Then
            Dim b As Drawing.Bitmap = CType(pic.Image, Drawing.Bitmap)
            b.RotateFlip(Drawing.RotateFlipType.Rotate90FlipXY)
            pic.Image = b
            m_Status = EnumStatus.Rotated
        End If
    End Sub

    Private Sub bbiRotateRight_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiRotateRight.ItemClick
        If m_HasPhoto Then
            Dim b As Drawing.Bitmap = CType(pic.Image, Drawing.Bitmap)
            b.RotateFlip(Drawing.RotateFlipType.Rotate90FlipNone)
            pic.Image = b
            m_Status = EnumStatus.Rotated
        End If
    End Sub
End Class
