﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CareGridWithButtons
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.panButtons = New DevExpress.XtraEditors.PanelControl()
        Me.cgRecords = New Care.Controls.CareGrid()
        Me.btnRemove = New Care.Controls.CareButton()
        Me.btnEdit = New Care.Controls.CareButton()
        Me.btnAdd = New Care.Controls.CareButton()
        CType(Me.panButtons, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panButtons.SuspendLayout()
        Me.SuspendLayout()
        '
        'panButtons
        '
        Me.panButtons.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.panButtons.Controls.Add(Me.btnRemove)
        Me.panButtons.Controls.Add(Me.btnEdit)
        Me.panButtons.Controls.Add(Me.btnAdd)
        Me.panButtons.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panButtons.Location = New System.Drawing.Point(0, 269)
        Me.panButtons.Name = "panButtons"
        Me.panButtons.Size = New System.Drawing.Size(637, 23)
        Me.panButtons.TabIndex = 1
        '
        'cgRecords
        '
        Me.cgRecords.AllowBuildColumns = True
        Me.cgRecords.AllowHorizontalScroll = False
        Me.cgRecords.AllowMultiSelect = False
        Me.cgRecords.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgRecords.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgRecords.Appearance.Options.UseFont = True
        Me.cgRecords.AutoSizeByData = True
        Me.cgRecords.HideFirstColumn = False
        Me.cgRecords.Location = New System.Drawing.Point(0, 0)
        Me.cgRecords.Name = "cgRecords"
        Me.cgRecords.QueryID = Nothing
        Me.cgRecords.SearchAsYouType = True
        Me.cgRecords.ShowAutoFilterRow = False
        Me.cgRecords.ShowFindPanel = False
        Me.cgRecords.ShowGroupByBox = False
        Me.cgRecords.ShowNavigator = False
        Me.cgRecords.Size = New System.Drawing.Size(637, 263)
        Me.cgRecords.TabIndex = 2
        '
        'btnRemove
        '
        Me.btnRemove.Location = New System.Drawing.Point(182, 0)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(85, 23)
        Me.btnRemove.TabIndex = 2
        Me.btnRemove.Text = "Remove"
        '
        'btnEdit
        '
        Me.btnEdit.Location = New System.Drawing.Point(91, 0)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(85, 23)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Text = "Edit"
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(0, 0)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(85, 23)
        Me.btnAdd.TabIndex = 0
        Me.btnAdd.Text = "Add"
        '
        'CareGridWithButtons
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.cgRecords)
        Me.Controls.Add(Me.panButtons)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "CareGridWithButtons"
        Me.Size = New System.Drawing.Size(637, 292)
        CType(Me.panButtons, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panButtons.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents panButtons As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnRemove As Care.Controls.CareButton
    Friend WithEvents btnEdit As Care.Controls.CareButton
    Friend WithEvents btnAdd As Care.Controls.CareButton
    Friend WithEvents cgRecords As Care.Controls.CareGrid

End Class
