﻿Imports System.ComponentModel

<DefaultEvent("Validating")>
Public Class CareTextBox

    Public Enum EnumNumericMode
        UseTag
        None
        NumbersOnly
        TwoDecimalPlaces
        Time
    End Enum

    Private m_NumericMode As EnumNumericMode = EnumNumericMode.UseTag
    Private m_NumericAllowNegatives As Boolean = False
    Private m_OriginalBackColour As Drawing.Color
    Private m_TextAlign As Integer

#Region "Properties"

    <Category("_CareAttributes")>
    Public Property ToolTipText As String
        Get
            Return Me.ToolTip
        End Get
        Set(value As String)
            Me.ToolTip = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property TextAlign As HorizontalAlignment
        Get
            Return CType(m_TextAlign, HorizontalAlignment)
        End Get
        Set(value As HorizontalAlignment)
            m_TextAlign = value
            If value = HorizontalAlignment.Left Then Me.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            If value = HorizontalAlignment.Center Then Me.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            If value = HorizontalAlignment.Right Then Me.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Overrides Property BackColor As Drawing.Color
        Get
            Return Me.Properties.Appearance.BackColor
        End Get
        Set(value As Drawing.Color)
            If value <> Drawing.Color.LightCoral Then m_OriginalBackColour = value
            Me.Properties.Appearance.BackColor = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Overloads Property [ReadOnly] As Boolean
        Get
            Return Me.Properties.ReadOnly
        End Get
        Set(value As Boolean)
            Me.Properties.ReadOnly = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property PasswordChar As Char
        Get
            Return Me.Properties.PasswordChar
        End Get
        Set(value As Char)
            Me.Properties.PasswordChar = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property MaxLength As Integer
        Get
            Return Me.Properties.MaxLength
        End Get
        Set(value As Integer)
            Me.Properties.MaxLength = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property CharacterCasing As CharacterCasing
        Get
            Return Me.Properties.CharacterCasing
        End Get
        Set(value As CharacterCasing)
            Me.Properties.CharacterCasing = value
        End Set
    End Property

    <Category("_CareNumerics")>
    Public Property NumericMode As EnumNumericMode
        Get
            Return m_NumericMode
        End Get
        Set(value As EnumNumericMode)
            m_NumericMode = value
            SetupMasks()
        End Set
    End Property

    <Category("_CareNumerics")>
    Public Property NumericAllowNegatives As Boolean
        Get
            Return m_NumericAllowNegatives
        End Get
        Set(value As Boolean)
            m_NumericAllowNegatives = value
            SetupMasks()
        End Set
    End Property

#End Region

    Private Sub SetupMasks()

        Me.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Default
        Me.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True
        Me.Properties.Mask.UseMaskAsDisplayFormat = True

        Select Case m_NumericMode

            Case EnumNumericMode.TwoDecimalPlaces
                Me.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
                Me.Properties.Mask.EditMask = "###,###,##0.00"
                Me.MaxLength = 14
                If Not m_NumericAllowNegatives Then Me.Properties.Mask.EditMask += ";"

            Case EnumNumericMode.NumbersOnly
                Me.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
                Me.Properties.Mask.EditMask = "##########"
                Me.MaxLength = 10
                If Not m_NumericAllowNegatives Then Me.Properties.Mask.EditMask += ";"

            Case EnumNumericMode.Time
                Me.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic
                Me.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
                Me.Properties.Mask.EditMask = "(0?\d|1\d|2[0-3])\:[0-5]\d"
                Me.Properties.MaxLength = 5

            Case Else
                Me.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None
                Me.Properties.Mask.EditMask = ""

        End Select

    End Sub

    Private Sub CareTextBox_EditValueChanged(sender As Object, e As System.EventArgs) Handles Me.EditValueChanged

        'THIS LOGIC REMAINS IN HERE FOR OLDER APPLICATIONS PRIOR TO USING THE MASKS

        If Not Me.Visible Then Exit Sub
        If Me.Text = "" Then Exit Sub

        If Not TagValid() Then Exit Sub
        If Not Me.Tag.ToString.ToUpper.Contains("2DP") Then Exit Sub

        Format2DP()

    End Sub

    Private Sub CareTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Me.KeyPress

        If Not TagValid() Then Exit Sub

        'always allow tab and backspace
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Asc(e.KeyChar) = 9 Then Exit Sub
        If Asc(e.KeyChar) = 13 Then Exit Sub

        'do not allow (') character
        If Asc(e.KeyChar) = 39 Then e.Handled = True

        If Me.Tag.ToString.ToUpper.Contains("2DP") Then
            If Asc(e.KeyChar) = Asc(".") Then
                If Me.Text.Contains(".") Then e.Handled = True
            Else
                If Not Char.IsNumber(e.KeyChar) Then e.Handled = True
            End If
        Else
            If Me.Tag.ToString.ToUpper.Contains("N") Then
                If Not Char.IsNumber(e.KeyChar) Then e.Handled = True
            End If
        End If

    End Sub

    Private Sub CareTextBox_LostFocus(sender As Object, e As System.EventArgs) Handles Me.LostFocus

        If Not Me.Visible Then Exit Sub
        If Me.Text = "" Then Exit Sub

        If Not TagValid() Then Exit Sub
        If Not Me.Tag.ToString.ToUpper.Contains("2DP") Then Exit Sub

        Format2DP()

    End Sub

    Private Function TagValid() As Boolean
        If Me.NumericMode <> EnumNumericMode.UseTag Then Return False
        If Me.Tag Is Nothing Then Return False
        Return True
    End Function

    Private Sub Format2DP()
        Dim _D As Decimal = 0
        If Decimal.TryParse(Me.Text, _D) Then
            Me.Text = Format(_D, "0.00")
        Else
            Me.Text = ""
        End If
    End Sub

End Class
