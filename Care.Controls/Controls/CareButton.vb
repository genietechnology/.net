﻿'ISSUE WITH CONTROL.MODIFIER KEYS
'**********************************
Option Strict Off
'**********************************
Imports System.Windows.Forms
Imports System.ComponentModel

<DefaultEvent("Click")>
Public Class CareButton
    Inherits DevExpress.XtraEditors.SimpleButton

    Public Shadows Event Click(sender As Object, e As System.EventArgs)

    Public ReadOnly Property ShiftDown As Boolean
        Get
            If Control.ModifierKeys AndAlso Keys.Shift Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

    Private m_ClickProcessing As Boolean = False
    Private Sub CareButton_Click(sender As Object, e As System.EventArgs) Handles MyBase.Click

        If m_ClickProcessing Then Exit Sub
        m_ClickProcessing = True

        RaiseEvent Click(sender, e)

        m_ClickProcessing = False

    End Sub
End Class
