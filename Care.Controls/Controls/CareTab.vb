﻿Imports System.Windows.Forms
Imports Care.Global
Imports DevExpress.XtraBars
Imports DevExpress.XtraTab
Imports DevExpress.XtraTab.ViewInfo

Public Class CareTab

    Private m_SecurityApplied As Boolean = False
    Private m_TabsEnabled As Boolean = True
    Private m_TabSecurity As New List(Of Dictionary.ControlSecurity)

    Public Sub New()

        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

    End Sub

#Region "Public Methods"

    Public Sub FirstPage()
        Me.SelectedTabPage = Me.TabPages.First()
    End Sub

    Public Sub EnableTabs()

        For Each _Page As XtraTabPage In Me.TabPages
            _Page.PageEnabled = True
        Next

        m_TabsEnabled = True

    End Sub

    Public Sub DisableTabs()

        Dim _i As Integer = 0
        For Each _Page As XtraTabPage In Me.TabPages
            If Me.SelectedTabPageIndex = _i Then
                _Page.PageEnabled = True
            Else
                _Page.PageEnabled = False
            End If
            _i += 1
        Next

        m_TabsEnabled = False

    End Sub

    Public Sub ClearHighlightedPages()
        For Each _p As XtraTabPage In Me.TabPages
            _p.Appearance.Header.Reset()
        Next
    End Sub

    Public Sub HighlightPage(ByVal PageName As String)
        For Each _p As XtraTabPage In Me.TabPages
            If _p.Name = PageName Then
                _p.Appearance.Header.ForeColor = System.Drawing.Color.Red
            End If
        Next
    End Sub

    Public Sub HighlightPage(ByVal TabPageIndex As Integer)
        Me.TabPages(TabPageIndex).Appearance.Header.ForeColor = System.Drawing.Color.Red
    End Sub

#End Region

    Private Sub CareTab_HandleCreated(sender As Object, e As EventArgs) Handles Me.HandleCreated
        If Not Me.DesignMode Then
            Me.TabMiddleClickFiringMode = TabMiddleClickFiringMode.MouseUp
            If Not m_SecurityApplied Then
                SetTabSecurityAndCaptions()
                m_SecurityApplied = True
            End If
        End If
    End Sub

    Private Sub SetTabSecurityAndCaptions()

        Dim _Q As IEnumerable(Of Dictionary.ControlSecurity) = Nothing

        'user security
        _Q = From _S As Dictionary.ControlSecurity In Session.DictionaryControlSecurity
             Where _S.FormName = Me.FindForm.Name And _S.ControlTabName = Me.Name _
             And _S.ControlSecurityType = [Global].Dictionary.ControlSecurity.EnumSecurityType.User _
             And _S.ControlSecurityID = Session.CurrentUser.ID

        If _Q IsNot Nothing Then
            For Each _S In _Q
                m_TabSecurity.Add(_S)
            Next
        End If

        'group security
        If Session.CurrentUser.GroupID <> "" Then
            _Q = From _S As Dictionary.ControlSecurity In Session.DictionaryControlSecurity
                 Where _S.FormName = Me.FindForm.Name And _S.ControlTabName = Me.Name _
                 And _S.ControlSecurityType = [Global].Dictionary.ControlSecurity.EnumSecurityType.Group _
                 And _S.ControlSecurityID = New Guid(Session.CurrentUser.GroupID)

            If _Q IsNot Nothing Then
                For Each _S In _Q
                    m_TabSecurity.Add(_S)
                Next
            End If
        End If

        'if we need to hide any tabs in this control, we do it now
        For Each _tab As XtraTabPage In Me.TabPages
            For Each _t In m_TabSecurity
                If _t.ControlName = _tab.Name Then
                    _tab.PageVisible = False
                End If
            Next
        Next

    End Sub

    Private Sub CareTab_TabMiddleClick(sender As Object, e As PageEventArgs) Handles Me.TabMiddleClick
        If Session.CurrentUser.IsSuperAdministrator = False Then Exit Sub
        Dim _frm As New frmTabConfiguration(Me)
        _frm.ShowDialog()
    End Sub

End Class
