﻿Imports System.ComponentModel

<DefaultEvent("Validating")>
Public Class CareDateTime

    Private m_OriginalBackColour As Drawing.Color

#Region "Properties"

    Public Overrides Property BackColor As Drawing.Color
        Get
            Return Me.Properties.Appearance.BackColor
        End Get
        Set(value As Drawing.Color)
            If value <> Drawing.Color.LightCoral Then m_OriginalBackColour = value
            Me.Properties.Appearance.BackColor = value
        End Set
    End Property

    Public Property Value As Date?
        Get
            If Me.EditValue Is Nothing Then
                Return Nothing
            Else
                If Me.EditValue.ToString = "" Then
                    Return Nothing
                Else
                    Return CType(Me.EditValue, Date?)
                End If
            End If
        End Get
        Set(value As Date?)
            If value Is Nothing Then
                Me.EditValue = Nothing
            Else
                If value = Date.MinValue Then
                    Me.EditValue = Nothing
                Else
                    Me.EditValue = value
                End If
            End If
        End Set
    End Property

    Public Overrides ReadOnly Property Text As String
        Get
            Return MyBase.Text
        End Get
    End Property

    Public Overloads Property [ReadOnly] As Boolean
        Get
            Return Me.Properties.ReadOnly
        End Get
        Set(value As Boolean)
            Me.Properties.ReadOnly = value
        End Set
    End Property

#End Region

    Private Sub CareDateTime_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown

        If Me.ReadOnly Then Exit Sub

        Select Case e.KeyCode

            Case Keys.T
                Me.EditValue = Today

            Case Keys.Y
                Me.EditValue = Today.AddDays(-1)

            Case Keys.M
                Me.EditValue = Today.AddDays(1)

        End Select

    End Sub

    Private Sub CareDateTime_QueryPopUp(sender As Object, e As CancelEventArgs) Handles Me.QueryPopUp
        If Me.ReadOnly Then e.Cancel = True
    End Sub
End Class
