﻿Imports Care.Data
Imports Care.Global

Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.IO
Imports System.Threading
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraEditors
Imports System.Text

Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports System.Drawing

<DefaultEvent("GridDoubleClick")>
Public Class CareGrid

    Private Enum EnumExportTarget
        OpenExcel
        EmailFile
    End Enum

    Private Const Gap As Integer = 6

    Public Event FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs)
    Public Event GridDoubleClick(sender As Object, e As System.EventArgs)
    Public Event GridClick(sender As Object, e As System.EventArgs)
    Public Event GridKeyDown(sender As Object, e As KeyEventArgs)
    Public Event AfterPopulate(sender As Object, e As System.EventArgs)
    Public Event RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs)
    Public Event RowCellStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs)
    Public Event AfterPopulateColumns(sender As Object, e As System.EventArgs)
    Public Event CustomUnboundColumnData(sender As Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs)
    Public Event CalcPreviewText(sender As Object, e As CalcPreviewTextEventArgs)
    Public Event ColumnsPopulated(sender As Object, e As System.EventArgs)
    Public Event RowCellClick(sender As Object, e As RowCellClickEventArgs)
    Public Event CustomColumnDisplayText(sender As Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs)

    Private m_Populated As Boolean = False
    Private m_HideFirstColumn As Boolean
    Private m_ShowFindPanel As Boolean
    Private m_SearchAsYouType As Boolean = True
    Private m_ShowLoadingPanel As Boolean = False

    Private m_ProcessingKeyPreview As Boolean = False
    Private m_CriteriaHasFocus As Boolean = True
    Private m_GridHasFocus As Boolean = False

    Private m_AutoSizeByData As Boolean = True

    Private m_DT As DataTable = Nothing
    Private m_Connection As String
    Private m_SQL As String

    Private m_PersistStream As System.IO.MemoryStream = Nothing
    Private m_TopRowIndex As Integer
    Private m_FocusedRowIndex As Integer

    Private m_ExportStream As MemoryStream
    Private m_ExportPath As String
    Private m_ExportTarget As EnumExportTarget

    Private m_ColumnChooserVisible As Boolean = False
    Private m_LayoutID As Guid? = Nothing
    Private m_QueryID As Guid? = Nothing

    Private m_ShowPreview As Boolean = False

    Private m_Populating As Boolean = False

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call

        GridView1.OptionsBehavior.Editable = False
        GridView1.OptionsView.ShowIndicator = False
        GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        GridView1.OptionsNavigation.EnterMoveNextColumn = False
        GridView1.OptionsNavigation.UseTabKey = False

        ToggleFindPanel()

        pp.Hide()

    End Sub

#Region "Properties"

    <Category("_CareAttributes")>
    Public Property DisableAutoSize As Boolean

    <Category("_CareAttributes")>
    Public Property DisableDataFormatting As Boolean

    <Category("_CareAttributes")>
    Public Property AutoSizeByData As Boolean
        Get
            Return m_AutoSizeByData
        End Get
        Set(value As Boolean)
            m_AutoSizeByData = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property AllowHorizontalScroll As Boolean
        Get
            Return Not GridView1.OptionsView.ColumnAutoWidth
        End Get
        Set(value As Boolean)
            GridView1.OptionsView.ColumnAutoWidth = Not value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property AllowBuildColumns As Boolean
        Get
            Return GridView1.OptionsBehavior.AutoPopulateColumns
        End Get
        Set(value As Boolean)
            GridView1.OptionsBehavior.AutoPopulateColumns = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property AllowMultiSelect As Boolean
        Get
            Return GridView1.OptionsSelection.MultiSelect
        End Get
        Set(value As Boolean)
            GridView1.OptionsSelection.MultiSelect = value
            If value = True Then
                GridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.RowSelect
            End If
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property AllowEdit As Boolean
        Get
            Return GridView1.OptionsBehavior.Editable
        End Get
        Set(value As Boolean)
            GridView1.OptionsBehavior.Editable = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property PreviewColumn As String
        Get
            Return GridView1.PreviewFieldName
        End Get
        Set(value As String)
            GridView1.PreviewFieldName = value
            If GridView1.PreviewFieldName IsNot Nothing AndAlso GridView1.PreviewFieldName <> "" Then
                GridView1.OptionsView.ShowPreview = True
                GridView1.OptionsView.AutoCalcPreviewLineCount = True
            End If
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property ShowGroupByBox As Boolean
        Get
            Return GridView1.OptionsView.ShowGroupPanel
        End Get
        Set(value As Boolean)
            GridView1.OptionsView.ShowGroupPanel = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property ShowFindPanel As Boolean
        Get
            Return m_ShowFindPanel
        End Get
        Set(value As Boolean)
            m_ShowFindPanel = value
            ToggleFindPanel()
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property ShowNavigator As Boolean
        Get
            Return GridControl1.UseEmbeddedNavigator
        End Get
        Set(value As Boolean)
            GridControl1.UseEmbeddedNavigator = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property ShowAutoFilterRow As Boolean
        Get
            Return GridView1.OptionsView.ShowAutoFilterRow
        End Get
        Set(value As Boolean)
            GridView1.OptionsView.ShowAutoFilterRow = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property ShowLoadingPanel As Boolean
        Get
            Return m_ShowLoadingPanel
        End Get
        Set(value As Boolean)
            m_ShowLoadingPanel = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property HideFirstColumn As Boolean
        Get
            Return m_HideFirstColumn
        End Get
        Set(value As Boolean)
            m_HideFirstColumn = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property SearchAsYouType As Boolean
        Get
            Return m_SearchAsYouType
        End Get
        Set(value As Boolean)
            m_SearchAsYouType = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property RowAutoHeight As Boolean
        Get
            Return GridView1.OptionsView.RowAutoHeight
        End Get
        Set(value As Boolean)
            GridView1.OptionsView.RowAutoHeight = value
        End Set
    End Property

    <BrowsableAttribute(False)>
    Public Property FocusedRowHandle As Integer
        Get
            Return GridView1.FocusedRowHandle
        End Get
        Set(value As Integer)
            GridView1.FocusedRowHandle = value
        End Set
    End Property

    <BrowsableAttribute(False)>
    Public ReadOnly Property CurrentRow As System.Data.DataRow
        Get
            Return GridView1.GetDataRow(GridView1.FocusedRowHandle)
        End Get
    End Property

    Public ReadOnly Property CurrentRowObject As Object
        Get
            Return GridView1.GetFocusedRow
        End Get
    End Property

    <BrowsableAttribute(False)>
    Public ReadOnly Property Columns As DevExpress.XtraGrid.Columns.GridColumnCollection
        Get
            Return GridView1.Columns
        End Get
    End Property

    <BrowsableAttribute(False)>
    Public ReadOnly Property RowIndex As Integer
        Get
            Return GridView1.GetFocusedDataSourceRowIndex
        End Get
    End Property

    <BrowsableAttribute(False)>
    Public ReadOnly Property RecordCount As Integer
        Get
            Return GridView1.RowCount
        End Get
    End Property

    <BrowsableAttribute(False)>
    Public ReadOnly Property UnderlyingGridControl As DevExpress.XtraGrid.GridControl
        Get
            Return GridControl1
        End Get
    End Property

    <BrowsableAttribute(False)>
    Public ReadOnly Property UnderlyingGridView As DevExpress.XtraGrid.Views.Grid.GridView
        Get
            Return GridView1
        End Get
    End Property

    <BrowsableAttribute(False)>
    Public Property QueryID As Guid?
        Get
            Return m_QueryID
        End Get
        Set(value As Guid?)
            m_QueryID = value
        End Set
    End Property

    <BrowsableAttribute(False)>
    Public ReadOnly Property ActiveFilterCriteria As String
        Get
            Dim _Filter As String = ""

            Try
                Dim _Criteria As DevExpress.Data.Filtering.CriteriaOperator = GridView1.ActiveFilterCriteria
                _Filter = DevExpress.Data.Filtering.CriteriaToWhereClauseHelper.GetDataSetWhere(_Criteria)

            Catch ex As Exception
                _Filter = ""
            End Try

            Return _Filter

        End Get
    End Property

#End Region

#Region "Public Methods"

    Public Sub BeginUpdate()
        GridControl1.BeginUpdate()
    End Sub

    Public Sub EndUpdate()
        GridControl1.EndUpdate()
    End Sub

    Public Sub Store()
        GridView1.PostEditor()
        GridView1.UpdateCurrentRow()
    End Sub

    Public Sub ExpandAll()
        GridView1.ExpandAllGroups()
    End Sub

    Public Sub PersistRowPosition()

        m_PersistStream = New System.IO.MemoryStream

        GridControl1.FocusedView.SaveLayoutToStream(m_PersistStream)
        m_PersistStream.Seek(0, IO.SeekOrigin.Begin)

        m_TopRowIndex = GridView1.TopRowIndex
        m_FocusedRowIndex = GridView1.FocusedRowHandle

    End Sub

    Public Sub RestoreRowPosition()

        If m_PersistStream Is Nothing Then Exit Sub

        GridControl1.FocusedView.RestoreLayoutFromStream(m_PersistStream)
        m_PersistStream.Seek(0, IO.SeekOrigin.Begin)

        GridView1.TopRowIndex = m_TopRowIndex
        GridView1.FocusedRowHandle = m_FocusedRowIndex

        m_PersistStream.Flush()
        m_PersistStream.Close()
        m_PersistStream.Dispose()
        m_PersistStream = Nothing

    End Sub

    Public Function GetRow() As System.Data.DataRow
        Return GridView1.GetDataRow(GridView1.FocusedRowHandle)
    End Function

    Public Function GetRow(ByVal RowIndex As Integer) As System.Data.DataRow
        Return GridView1.GetDataRow(RowIndex)
    End Function

    Public Function GetRowObject(ByVal RowHandle As Integer) As Object
        Return GridView1.GetRow(GridView1.FocusedRowHandle)
    End Function

    Public Function GetRowCellValue(ByVal RowHandle As Integer, ByVal FieldName As String) As Object
        Return GridView1.GetRowCellValue(RowHandle, FieldName)
    End Function

    Public Sub MoveFirst()
        GridView1.MoveFirst()
        GridView1.FocusedRowHandle = 0
        RaiseEvent FocusedRowChanged(Nothing, Nothing)
        GridControl1.Focus()
        Application.DoEvents()
    End Sub

    Public Sub MoveLast()
        GridView1.MoveLast()
        GridView1.FocusedRowHandle = GridView1.RowCount - 1
        RaiseEvent FocusedRowChanged(Nothing, Nothing)
        GridControl1.Focus()
        Application.DoEvents()
    End Sub

    Public Sub Clear()
        GridView1.Columns.Clear()
        GridControl1.DataSource = Nothing
        m_LayoutID = Nothing
    End Sub

    Public Sub Populate(ByVal DataIn As DataTable)

        Clear()

        m_Connection = ""
        m_SQL = ""

        m_DT = DataIn
        GridControl1.DataSource = m_DT
        HideColumns()
        RaiseEvent AfterPopulateColumns(GridView1, New System.EventArgs)
        GridControl1.RefreshDataSource()
        Population()

    End Sub

    Public Sub Populate(ByVal ConnectionString As String, ByVal SQL As String)

        If m_Populating Then Exit Sub

        If Me.ShowLoadingPanel Then
            If bwWaitPanel.IsBusy Then Exit Sub
        End If

        m_Populating = True

        '*************************************************************************************
        If Me.ShowLoadingPanel Then
            pp.Show()
            Application.DoEvents()
            bwWaitPanel.RunWorkerAsync()
        End If

        '*************************************************************************************

        Clear()

        m_Connection = ConnectionString
        m_SQL = SQL

        m_DT = DAL.GetDataTablefromSQL(m_Connection, m_SQL)

        '*************************************************************************************

        GridView1.PopulateColumns(m_DT)
        HideColumns()
        RaiseEvent ColumnsPopulated(GridView1, New System.EventArgs)

        '*************************************************************************************

        GridControl1.DataSource = m_DT
        RaiseEvent AfterPopulateColumns(GridView1, New System.EventArgs)

        '*************************************************************************************

        GridControl1.RefreshDataSource()
        Population()

        '*************************************************************************************

    End Sub

    Private Sub bwFetchData_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles bwWaitPanel.DoWork
        While m_Populating
            'we are just waiting whilst we load data...
            Thread.Sleep(100)
        End While
    End Sub

    Private Sub bwFetchData_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bwWaitPanel.RunWorkerCompleted
        pp.Hide()
    End Sub

    Public Sub Populate(ByVal GenericList As IEnumerable)
        Clear()
        GridControl1.DataSource = GenericList
        GridView1.PopulateColumns()
        RaiseEvent AfterPopulateColumns(GridView1, New System.EventArgs)
        GridControl1.RefreshDataSource()
        Population()
    End Sub

    Private Sub Population()

        If GridView1.Columns.Count > 0 Then
            FormatData()
            If m_HideFirstColumn Then
                GridView1.Columns(0).Visible = False
            End If
        End If

        If Not DisableAutoSize Then

            If m_AutoSizeByData Then
                GridView1.BestFitMaxRowCount = 50
            Else
                GridView1.BestFitMaxRowCount = 0
            End If

            GridView1.BestFitColumns()

            MoveFirst()

        End If

        m_Populated = True

        If GridView1.Columns.Count > 0 Then
            RaiseEvent AfterPopulate(Nothing, Nothing)
        End If

        m_Populating = False

    End Sub

    <Obsolete("Use .Clear instead", False)>
    Public Sub ClearGrid()
        GridControl1.DataSource = Nothing
        GridView1.Columns.Clear()
    End Sub

    Public Sub AutoSizeColumns()
        Try
            GridView1.BestFitColumns()
        Catch ex As Exception

        End Try
    End Sub

    Public Sub FocusCriteria()
        If m_ShowFindPanel Then
            Application.DoEvents()
            txtCriteria.Focus()
            Application.DoEvents()
        End If
    End Sub

    Public Function FindRowByValue(ByVal ColumnFieldName As String, ByVal value As Object) As Integer

        Dim _Row As Integer = -1

        Dim _i As Integer
        For _i = 0 To GridView1.RowCount - 1
            If GridView1.GetDataRow(_i)(ColumnFieldName).Equals(value) Then
                _Row = _i
                Exit For
            End If
        Next

        Return _Row

    End Function

    Public Function GetFilteredRows() As DataView
        Dim _DV As New DataView(m_DT)
        _DV.RowFilter = DevExpress.Data.Filtering.CriteriaToWhereClauseHelper.GetDataSetWhere(GridView1.ActiveFilterCriteria)
        Return _DV
    End Function

#End Region

#Region "Control Events"

    Private Sub GridView1_CalcPreviewText(sender As Object, e As CalcPreviewTextEventArgs) Handles GridView1.CalcPreviewText
        RaiseEvent CalcPreviewText(sender, e)
    End Sub

    Private Sub GridView1_CustomUnboundColumnData(sender As Object, e As Views.Base.CustomColumnDataEventArgs) Handles GridView1.CustomUnboundColumnData
        RaiseEvent CustomUnboundColumnData(sender, e)
    End Sub

    Private Sub GridView1_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView1.FocusedRowChanged
        RaiseEvent FocusedRowChanged(sender, e)
    End Sub

    Private Sub GridView1_Click(sender As Object, e As System.EventArgs) Handles GridView1.RowClick
        RaiseEvent GridClick(sender, e)
    End Sub

    Private Sub GridView1_DoubleClick(sender As Object, e As System.EventArgs) Handles GridView1.DoubleClick

        If GridView1 Is Nothing Then Exit Sub

        Try
            Dim _pt As Point = GridView1.GridControl.PointToClient(Control.MousePosition)
            Dim _ghi As GridHitInfo = GridView1.CalcHitInfo(_pt)

            If _ghi.InRowCell Then
                RaiseEvent GridDoubleClick(sender, e)
            End If

        Catch ex As Exception
            Msgbox(ex.Message, MessageBoxIcon.Exclamation, "GridView1_DoubleClick")
        End Try

    End Sub

    Private Sub GridView1_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles GridView1.RowStyle
        RaiseEvent RowStyle(sender, e)
    End Sub

    Private Sub GridView1_RowCellStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs) Handles GridView1.RowCellStyle
        RaiseEvent RowCellStyle(sender, e)
    End Sub

    Private Sub txtCriteria_GotFocus(sender As Object, e As System.EventArgs) Handles txtCriteria.GotFocus
        m_CriteriaHasFocus = True
    End Sub

    Private Sub txtCriteria_LostFocus(sender As Object, e As System.EventArgs) Handles txtCriteria.LostFocus
        m_CriteriaHasFocus = False
    End Sub

    Private Sub GridControl1_GotFocus(sender As Object, e As System.EventArgs) Handles GridControl1.GotFocus
        m_GridHasFocus = True
    End Sub

    Private Sub GridControl1_LostFocus(sender As Object, e As System.EventArgs) Handles GridControl1.LostFocus
        m_GridHasFocus = False
    End Sub

    Private Sub txtCriteria_PreviewKeyDown(sender As Object, e As PreviewKeyDownEventArgs) Handles txtCriteria.PreviewKeyDown

        If e.KeyCode = Keys.Enter Then

            If m_SearchAsYouType Then
                MoveFirst()
            Else
                DoFind(True)
            End If

            AutoSelect()

        End If

    End Sub

    Private Sub txtCriteria_TextChanged(sender As Object, e As System.EventArgs) Handles txtCriteria.TextChanged
        If m_SearchAsYouType Then DoFind()
    End Sub

    Private Sub GridView1_ShowCustomizationForm(sender As Object, e As EventArgs) Handles GridView1.ShowCustomizationForm
        m_ColumnChooserVisible = True
    End Sub

    Private Sub GridView1_HideCustomizationForm(sender As Object, e As EventArgs) Handles GridView1.HideCustomizationForm
        m_ColumnChooserVisible = False
    End Sub

    Private Sub GridControl1_ProcessGridKey(sender As Object, e As KeyEventArgs) Handles GridControl1.ProcessGridKey
        If e.KeyCode = Keys.Enter Then
            RaiseEvent GridDoubleClick(Me.GridView1, Nothing)
        End If
    End Sub

    Private Sub GridView1_PopupMenuShowing(sender As Object, e As PopupMenuShowingEventArgs) Handles GridView1.PopupMenuShowing
        If e.MenuType = GridMenuType.Row Then
            mnuPopup.ShowPopup(MousePosition)
        End If
    End Sub

    Private Sub btnFind_Click(sender As System.Object, e As System.EventArgs) Handles btnFind.Click
        DoFind()
    End Sub

    Private Sub GridView1_RowCellClick(sender As Object, e As RowCellClickEventArgs) Handles GridView1.RowCellClick
        RaiseEvent RowCellClick(sender, e)
    End Sub

    Private Sub GridView1_CustomColumnDisplayText(sender As Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles GridView1.CustomColumnDisplayText
        RaiseEvent CustomColumnDisplayText(sender, e)
    End Sub

#End Region

#Region "Popup Menu"



    Private Sub mnuGroupBy_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuGroupBy.ItemClick

        If GridView1.OptionsView.ShowGroupPanel = True Then
            GridView1.OptionsView.ShowGroupPanel = False
            mnuGroupBy.Caption = "Show Group By Box"
            mnuCollapse.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            mnuExpand.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        Else
            GridView1.OptionsView.ShowGroupPanel = True
            mnuGroupBy.Caption = "Hide Group By Box"
            mnuCollapse.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
            mnuExpand.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        End If

    End Sub

    Private Sub mnuCollapse_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuCollapse.ItemClick
        GridView1.CollapseAllGroups()
    End Sub

    Private Sub mnuExpand_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuExpand.ItemClick
        GridView1.ExpandAllGroups()
    End Sub

    Private Sub mnuFieldChooser_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuFieldChooser.ItemClick
        If GridView1.CustomizationForm Is Nothing Then
            GridView1.ColumnsCustomization()
        End If
    End Sub

    Private Sub mnuExport_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuExport.ItemClick

        Dim savepath As New SaveFileDialog

        savepath.AddExtension = True
        savepath.Filter = "Excel Files |.xlsx|All Files |*.*"

        savepath.DefaultExt = "xlsx"

        savepath.ShowDialog()
        If Not savepath.FileName = "" Then
            If savepath.CheckPathExists Then
                DoExport(savepath.FileName, EnumExportTarget.OpenExcel)
            End If
        End If

    End Sub

    Private Sub mnuEmailXLSX_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuEmailXLSX.ItemClick
        Dim _TempPath As String = Session.TempFolder + Me.ParentForm.Name + "_" + Me.Name + "_" + Format(Now, "yyyy-MM-dd_HHmm") + ".xlsx"
        DoExport(_TempPath, EnumExportTarget.EmailFile)
    End Sub

    Private Sub DoExport(ByVal FilePath As String, ByVal OutputType As EnumExportTarget)

        m_ExportStream = New MemoryStream
        m_ExportPath = FilePath
        m_ExportTarget = OutputType

        m_ExportStream.Seek(0, System.IO.SeekOrigin.Begin)
        GridView1.SaveLayoutToStream(m_ExportStream, DevExpress.Utils.OptionsLayoutBase.FullLayout)

        Dim ExportThread = New Thread(New ThreadStart(AddressOf Export))
        ExportThread.Start()

    End Sub


    Private Sub mnuPrint_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuPrint.ItemClick
        GridView1.OptionsPrint.AutoWidth = False
        GridView1.ShowPrintPreview()
        GridView1.OptionsPrint.AutoWidth = True
    End Sub

    Private Sub mnuPopup_BeforePopup(sender As Object, e As CancelEventArgs) Handles mnuPopup.BeforePopup

        If Not IsPopulated() Then e.Cancel = True

        If Session.SMTPEnabled AndAlso Session.CurrentUser.Email.Trim <> "" Then
            mnuEmailXLSX.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            mnuEmailXLSX.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If

        If GridView1.OptionsView.ShowGroupPanel = True Then
            mnuPopup.Manager.Items("mnuGroupBy").Caption = "Hide Group By Box"
            mnuCollapse.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
            mnuExpand.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
            mnuGroupFooter.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            mnuPopup.Manager.Items("mnuGroupBy").Caption = "Show Group By Box"
            mnuCollapse.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            mnuExpand.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            mnuGroupFooter.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If

        If GridView1.OptionsView.ShowAutoFilterRow = True Then
            mnuPopup.Manager.Items("mnuAutoFilter").Caption = "Hide Auto Filter Row"
        Else
            mnuPopup.Manager.Items("mnuAutoFilter").Caption = "Show Auto Filter Row"
        End If

        If m_ColumnChooserVisible Then
            mnuRemoveColumns.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            mnuRemoveColumns.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If

        If GridView1.OptionsView.ShowFooter Then
            mnuPopup.Manager.Items("mnuTotalsFooter").Caption = "Hide Totals Footer"
        Else
            mnuPopup.Manager.Items("mnuTotalsFooter").Caption = "Show Totals Footer"
        End If

        If GridView1.OptionsView.GroupFooterShowMode = GroupFooterShowMode.VisibleAlways Then
            mnuPopup.Manager.Items("mnuGroupFooter").Caption = "Hide Group Footer"
        Else
            mnuPopup.Manager.Items("mnuGroupFooter").Caption = "Show Group Footer"
        End If

    End Sub

    Private Sub mnuRemoveColumns_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRemoveColumns.ItemClick
        If XtraMessageBox.Show("Are you sure you want to remove all the columns?", "Remove Columns", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = DialogResult.Yes Then
            For Each _C As DevExpress.XtraGrid.Columns.GridColumn In GridView1.Columns
                _C.Visible = False
            Next
        End If
    End Sub

    Private Sub mnuLoadLayout_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuLoadLayout.ItemClick

        Dim _frm As New frmLayoutManager(Me.ParentForm.Name, Me.Name, m_QueryID)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            m_LayoutID = _frm.LayoutID
            LoadLayoutFromString(_frm.LayoutString)
        End If

        _frm.Dispose()
        _frm = Nothing

    End Sub

    Private Sub mnuSaveLayout_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuSaveLayout.ItemClick

        Dim _frm As frmLayoutSave

        If m_LayoutID Is Nothing Then
            _frm = New frmLayoutSave(Me.ParentForm.Name, Me.Name, m_QueryID, ConvertLayoutToString)
        Else
            _frm = New frmLayoutSave(m_LayoutID, ConvertLayoutToString)
        End If

        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            m_LayoutID = _frm.LayoutID
        End If

        _frm.Dispose()
        _frm = Nothing

    End Sub

    Private Sub mnuTotalsFooter_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuTotalsFooter.ItemClick
        If GridView1.OptionsView.ShowFooter Then
            GridView1.OptionsView.ShowFooter = False
        Else
            GridView1.OptionsView.ShowFooter = True
        End If
    End Sub

    Private Sub mnuGroupFooter_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuGroupFooter.ItemClick
        If GridView1.OptionsView.GroupFooterShowMode = GroupFooterShowMode.VisibleAlways Then
            GridView1.OptionsView.GroupFooterShowMode = GroupFooterShowMode.Hidden
        Else
            GridView1.OptionsView.GroupFooterShowMode = GroupFooterShowMode.VisibleAlways
        End If
    End Sub

    Private Sub mnuAutoFilter_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuAutoFilter.ItemClick
        If GridView1.OptionsView.ShowAutoFilterRow Then
            GridView1.OptionsView.ShowAutoFilterRow = False
        Else
            GridView1.OptionsView.ShowAutoFilterRow = True
        End If
    End Sub

    Private Sub mnuAdvancedFilter_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuAdvancedFilter.ItemClick

        Dim _Point As Point = GridControl1.PointToClient(Control.MousePosition)
        If _Point.IsEmpty Then Exit Sub

        Dim _Info As GridHitInfo = GridView1.CalcHitInfo(_Point)
        If _Info IsNot Nothing Then
            If _Info.Column IsNot Nothing Then
                GridView1.ShowFilterEditor(_Info.Column)
            End If
        End If

    End Sub

#End Region

    Private Sub FormatData()

        If DisableDataFormatting Then Exit Sub

        For Each _c As DevExpress.XtraGrid.Columns.GridColumn In GridView1.Columns

            Select Case _c.ColumnType.Name

                Case "Decimal"
                    _c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                    _c.DisplayFormat.FormatString = "0.00"

                Case "Date", "DateTime"
                    _c.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart

                Case Else
                    _c.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList

            End Select

        Next

    End Sub

    Private Sub ToggleFindPanel()

        If m_ShowFindPanel Then

            GridView1.OptionsFind.AllowFindPanel = True
            GridView1.OptionsFind.AlwaysVisible = False

            If m_SearchAsYouType Then
                GridView1.OptionsFind.FindMode = DevExpress.XtraEditors.FindMode.Always
            Else
                GridView1.OptionsFind.FindMode = DevExpress.XtraEditors.FindMode.FindClick
            End If

            'GridControl1.Top = gbxFind.Height + Gap
            'GridControl1.Height -= gbxFind.Height - Gap

            'TableLayoutPanel1.GetControlFromPosition(0, 0).Visible = True

            gbxFind.Show()
            'TableLayoutPanel1.RowStyles(0).SizeType = SizeType.Percent
            'TableLayoutPanel1.RowStyles(0).Height = 16

            txtCriteria.Focus()

        Else

            GridView1.OptionsFind.AllowFindPanel = False
            GridView1.OptionsFind.AlwaysVisible = False
            GridView1.OptionsFind.FindMode = DevExpress.XtraEditors.FindMode.FindClick

            'GridControl1.Top = 0
            'GridControl1.Height = Me.Height

            gbxFind.Hide()
            TableLayoutPanel1.RowStyles(0).SizeType = SizeType.AutoSize

            'TableLayoutPanel1.GetControlFromPosition(0, 0).Visible = False

        End If


    End Sub

    Private Sub DoFind(Optional ByVal ProcessMoveFirst As Boolean = False)

        If m_Populated Then
            GridView1.ApplyFindFilter(txtCriteria.Text)
        Else
            RaiseEvent AfterPopulate(GridView1, New System.EventArgs)
        End If

        If ProcessMoveFirst Then
            MoveFirst()
            AutoSelect()
        Else
            txtCriteria.Focus()
            txtCriteria.ScrollToCaret()
        End If

    End Sub

    Private Sub AutoSelect()
        If Me.RecordCount = 1 Then
            RaiseEvent GridDoubleClick(Me.GridView1, New EventArgs)
        End If
    End Sub

    Private Sub Export()

        Dim _Parent As DevExpress.XtraEditors.XtraForm = DirectCast(Me.ParentForm.MdiParent, DevExpress.XtraEditors.XtraForm)

        Dim _ExportForm As New DevExpress.XtraEditors.XtraForm
        Dim _ExportProgress As New DevExpress.XtraWaitForm.ProgressPanel
        Dim _ExportGrid As New GridControl
        Dim _ExportGridView As New GridView(_ExportGrid)

        With _ExportGrid
            .Parent = _ExportForm
            .Visible = True
            .Location = New System.Drawing.Point(0, 0)
            .Size = New System.Drawing.Size(0, 0)
            .DataSource = GridView1.DataSource
            .MainView = _ExportGridView
            .LookAndFeel.Assign(Me.LookAndFeel)
        End With

        With _ExportForm
            _ExportForm.Size = New System.Drawing.Size(0, 0)
            _ExportForm.Appearance.Assign(Me.Appearance)
            _ExportForm.Show()
        End With

        With _ExportProgress
            _ExportProgress.Appearance.Assign(_ExportForm.Appearance)
            _ExportProgress.LookAndFeel.Assign(Me.LookAndFeel)
            _ExportProgress.Parent = _ExportForm
            _ExportProgress.Location = New System.Drawing.Point(0, 0)
            _ExportProgress.Visible = True
            _ExportProgress.Show()
        End With

        m_ExportStream.Seek(0, System.IO.SeekOrigin.Begin)

        With _ExportGridView
            _ExportGridView.Name = "ExportGrid"
            _ExportGridView.RestoreLayoutFromStream(m_ExportStream, DevExpress.Utils.OptionsLayoutBase.FullLayout)
            _ExportGridView.GridControl = _ExportGrid
            _ExportGridView.OptionsPrint.AutoWidth = False
            _ExportGridView.ExportToXlsx(m_ExportPath)
            _ExportGridView.OptionsPrint.AutoWidth = True
        End With

        m_ExportStream.Close()

        If m_ExportTarget = EnumExportTarget.OpenExcel Then
            Dim _OpenExportFileThread = New Thread(New ThreadStart(AddressOf OpenExportFile))
            _OpenExportFileThread.Start()
        Else
            Session.MDIFormObject.EmailExportFile(m_ExportPath)
        End If

        _ExportGrid.Visible = False

        _ExportProgress.Dispose()
        _ExportProgress = Nothing

        _ExportGridView.Dispose()
        _ExportGridView = Nothing

        _ExportGrid.Dispose()
        _ExportGrid = Nothing

        _ExportForm.Dispose()
        _ExportForm = Nothing

    End Sub

    Private Sub OpenExportFile()
        Process.Start(m_ExportPath)
    End Sub

    Private Function IsPopulated() As Boolean
        If GridView1 Is Nothing Then Return False
        If GridView1.RowCount <= 0 Then Return False
        Return True
    End Function

    Private Sub HideColumns()

        If GridView1 Is Nothing Then Exit Sub
        If GridView1.Columns.Count = 0 Then Exit Sub

        'loop through all columns, checking for !
        For Each _C As DevExpress.XtraGrid.Columns.GridColumn In GridView1.Columns
            If _C.FieldName.Contains("!") Then
                _C.Visible = False
            End If
        Next

    End Sub

    Public Function LoadLayoutFromString(ByVal LayoutString As String) As Boolean

        Try
            Dim _Bytes As Byte() = Encoding.ASCII.GetBytes(LayoutString)
            Dim _ms As MemoryStream = New MemoryStream(_Bytes)
            GridView1.RestoreLayoutFromStream(_ms)
            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function ConvertLayoutToString() As String

        Dim _Return As String = ""

        Try
            Dim _MS As New MemoryStream
            GridView1.SaveLayoutToStream(_MS)

            _MS.Seek(0, SeekOrigin.Begin)
            Dim _Reader As New StreamReader(_MS)
            _Return = _Reader.ReadToEnd()

        Catch ex As Exception

        End Try

        Return _Return

    End Function

End Class
