﻿Imports DevExpress.XtraRichEdit.API.Native
Imports Care.Global
Imports System.IO
Imports DevExpress.XtraSpellChecker
Imports System.Windows.Forms
Imports DevExpress.Office.Services
Imports DevExpress.Office.Utils

Public Class CareRichText

    Public Event ControlLostFocus(sender As Object, e As EventArgs)
    Shadows Event TextChanged As EventHandler

    Private m_CaretPosition As DocumentPosition
    Private m_EmbeddedHTML As String = ""

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        If Session IsNot Nothing AndAlso Session.SpellCheck Then

            If Session.SpellCheckDictionaryPath <> "" AndAlso Session.SpellCheckGrammmarPath <> "" Then
                AddDictionary(Session.SpellCheckDictionaryPath, Session.SpellCheckGrammmarPath)
            End If

            SpellChecker1.SpellCheckMode = SpellCheckMode.AsYouType
        Else
            SpellChecker1.SpellCheckMode = SpellCheckMode.OnDemand
        End If

    End Sub

    Private Function AddDictionary(ByVal DictionaryPath As String, ByVal GrammarPath As String) As Boolean


        If Not File.Exists(DictionaryPath) Then Return False
        If Not File.Exists(GrammarPath) Then Return False

        Dim _Return As Boolean = False

        Try

            Dim _ood As New SpellCheckerOpenOfficeDictionary
            _ood.DictionaryPath = DictionaryPath
            _ood.GrammarPath = GrammarPath
            _ood.Load()

            If _ood.Loaded Then
                SharedDictionaryStorage1.Dictionaries.Add(_ood)
                _Return = True
            End If

            _ood = Nothing

        Catch ex As Exception
            CareMessage(ex.Message, MessageBoxIcon.Warning, MessageBoxButtons.OK, "Setup SpellChecker")
        End Try

        Return _Return

    End Function

    Public Sub InsertTextAtCaret(ByVal TextIn As String)
        rePreview.Document.InsertText(m_CaretPosition, TextIn)
    End Sub

    Public Overrides Property Text As String
        Get
            Return rePreview.Text
        End Get
        Set(value As String)
            rePreview.Text = value
        End Set
    End Property

    Public Property RTFText As String
        Get
            Return rePreview.RtfText
        End Get
        Set(value As String)
            rePreview.RtfText = value
        End Set
    End Property

    Public Property HTMLText As String
        Get
            Return ReturnEmbeddedHTML()
        End Get
        Set(value As String)
            rePreview.HtmlText = value
        End Set
    End Property

    'Public ReadOnly Property EmbeddedHTML As String
    '    Get
    '        Return m_EmbeddedHTML
    '    End Get
    'End Property

    Private Sub rePreview_LostFocus(sender As Object, e As EventArgs) Handles rePreview.LostFocus
        m_CaretPosition = rePreview.Document.CaretPosition
        RaiseEvent ControlLostFocus(sender, e)
    End Sub

    Private Sub rePreview_TextChanged(sender As Object, e As EventArgs) Handles rePreview.TextChanged
        RaiseEvent TextChanged(sender, e)
    End Sub

    Private Function ReturnEmbeddedHTML() As String
        Dim exportOptions As New DevExpress.XtraRichEdit.Export.HtmlDocumentExporterOptions()
        exportOptions.EmbedImages = True
        Return rePreview.Document.GetHtmlText(rePreview.Document.Range, New CustomUriProvider(), exportOptions)
    End Function

    Private Sub rePreview_DocumentLoaded(sender As Object, e As EventArgs) Handles rePreview.DocumentLoaded
        Dim service As IUriProviderService = rePreview.GetService(Of IUriProviderService)()
        If service IsNot Nothing Then
            service.RegisterProvider(New CustomUriProvider())
        End If
    End Sub

    Public Class CustomUriProvider
        Implements IUriProvider

        Public Function CreateCssUri(ByVal rootUri As String, ByVal styleText As String, ByVal relativeUri As String) As String Implements IUriProvider.CreateCssUri
            Return String.Empty
        End Function

        Public Function CreateImageUri(ByVal rootUri As String, ByVal image As OfficeImage, ByVal relativeUri As String) As String Implements IUriProvider.CreateImageUri
            Return image.Uri
        End Function
    End Class

End Class
