﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CareGrid
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CareGrid))
        Me.bwWaitPanel = New System.ComponentModel.BackgroundWorker()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gbxFind = New Care.Controls.CareFrame()
        Me.btnFind = New DevExpress.XtraEditors.SimpleButton()
        Me.txtCriteria = New DevExpress.XtraEditors.TextEdit()
        Me.pp = New DevExpress.XtraWaitForm.ProgressPanel()
        Me.mnuPopup = New DevExpress.XtraBars.PopupMenu()
        Me.mnuExport = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuEmailXLSX = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuPrint = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuAutoFilter = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuAdvancedFilter = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuFieldChooser = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuRemoveColumns = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuTotalsFooter = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuGroupBy = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuCollapse = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuExpand = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuGroupFooter = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuLoadLayout = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSaveLayout = New DevExpress.XtraBars.BarButtonItem()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxFind, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxFind.SuspendLayout()
        CType(Me.txtCriteria.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mnuPopup, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bwWaitPanel
        '
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.GridControl1, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.gbxFind, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(380, 273)
        Me.TableLayoutPanel1.TabIndex = 2
        '
        'GridControl1
        '
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.Location = New System.Drawing.Point(0, 38)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Margin = New System.Windows.Forms.Padding(0)
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(380, 235)
        Me.GridControl1.TabIndex = 2
        Me.GridControl1.TabStop = False
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        '
        'gbxFind
        '
        Me.gbxFind.Controls.Add(Me.btnFind)
        Me.gbxFind.Controls.Add(Me.txtCriteria)
        Me.gbxFind.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxFind.Location = New System.Drawing.Point(0, 0)
        Me.gbxFind.Margin = New System.Windows.Forms.Padding(0)
        Me.gbxFind.Name = "gbxFind"
        Me.gbxFind.ShowCaption = False
        Me.gbxFind.Size = New System.Drawing.Size(380, 38)
        Me.gbxFind.TabIndex = 1
        Me.gbxFind.TabStop = True
        '
        'btnFind
        '
        Me.btnFind.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFind.Location = New System.Drawing.Point(295, 9)
        Me.btnFind.Name = "btnFind"
        Me.btnFind.Size = New System.Drawing.Size(75, 20)
        Me.btnFind.TabIndex = 1
        Me.btnFind.TabStop = False
        Me.btnFind.Text = "Find"
        '
        'txtCriteria
        '
        Me.txtCriteria.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCriteria.EditValue = ""
        Me.txtCriteria.EnterMoveNextControl = True
        Me.txtCriteria.Location = New System.Drawing.Point(10, 9)
        Me.txtCriteria.Name = "txtCriteria"
        Me.txtCriteria.Properties.Appearance.BackColor = System.Drawing.Color.PaleGreen
        Me.txtCriteria.Properties.Appearance.Options.UseBackColor = True
        Me.txtCriteria.Properties.NullText = "Enter your search criteria and hit Enter..."
        Me.txtCriteria.Size = New System.Drawing.Size(279, 20)
        Me.txtCriteria.TabIndex = 0
        '
        'pp
        '
        Me.pp.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pp.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.pp.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.pp.Appearance.Options.UseBackColor = True
        Me.pp.Appearance.Options.UseFont = True
        Me.pp.AppearanceCaption.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pp.AppearanceCaption.Options.UseFont = True
        Me.pp.AppearanceDescription.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pp.AppearanceDescription.Options.UseFont = True
        Me.pp.BarAnimationElementThickness = 2
        Me.pp.Location = New System.Drawing.Point(67, 103)
        Me.pp.Name = "pp"
        Me.pp.Size = New System.Drawing.Size(246, 66)
        Me.pp.TabIndex = 2
        '
        'mnuPopup
        '
        Me.mnuPopup.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuExport), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuEmailXLSX), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuPrint), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAutoFilter, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAdvancedFilter), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFieldChooser, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRemoveColumns), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.mnuTotalsFooter, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuGroupBy, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuCollapse), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuExpand), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuGroupFooter), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuLoadLayout, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSaveLayout)})
        Me.mnuPopup.Manager = Me.BarManager1
        Me.mnuPopup.Name = "mnuPopup"
        '
        'mnuExport
        '
        Me.mnuExport.Caption = "Export to XLSX file"
        Me.mnuExport.Id = 0
        Me.mnuExport.ImageOptions.Image = CType(resources.GetObject("mnuExport.ImageOptions.Image"), System.Drawing.Image)
        Me.mnuExport.ImageOptions.LargeImage = CType(resources.GetObject("mnuExport.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.mnuExport.Name = "mnuExport"
        '
        'mnuEmailXLSX
        '
        Me.mnuEmailXLSX.Caption = "Email me XLSX file"
        Me.mnuEmailXLSX.Id = 13
        Me.mnuEmailXLSX.ImageOptions.Image = CType(resources.GetObject("mnuEmailXLSX.ImageOptions.Image"), System.Drawing.Image)
        Me.mnuEmailXLSX.ImageOptions.LargeImage = CType(resources.GetObject("mnuEmailXLSX.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.mnuEmailXLSX.Name = "mnuEmailXLSX"
        '
        'mnuPrint
        '
        Me.mnuPrint.Caption = "Print Preview"
        Me.mnuPrint.Id = 5
        Me.mnuPrint.ImageOptions.Image = CType(resources.GetObject("mnuPrint.ImageOptions.Image"), System.Drawing.Image)
        Me.mnuPrint.ImageOptions.LargeImage = CType(resources.GetObject("mnuPrint.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.mnuPrint.Name = "mnuPrint"
        '
        'mnuAutoFilter
        '
        Me.mnuAutoFilter.Caption = "Show Auto Filter Row"
        Me.mnuAutoFilter.Id = 11
        Me.mnuAutoFilter.ImageOptions.Image = CType(resources.GetObject("mnuAutoFilter.ImageOptions.Image"), System.Drawing.Image)
        Me.mnuAutoFilter.ImageOptions.LargeImage = CType(resources.GetObject("mnuAutoFilter.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.mnuAutoFilter.Name = "mnuAutoFilter"
        '
        'mnuAdvancedFilter
        '
        Me.mnuAdvancedFilter.Caption = "Advanced Filtering"
        Me.mnuAdvancedFilter.Id = 12
        Me.mnuAdvancedFilter.ImageOptions.Image = CType(resources.GetObject("mnuAdvancedFilter.ImageOptions.Image"), System.Drawing.Image)
        Me.mnuAdvancedFilter.ImageOptions.LargeImage = CType(resources.GetObject("mnuAdvancedFilter.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.mnuAdvancedFilter.Name = "mnuAdvancedFilter"
        '
        'mnuFieldChooser
        '
        Me.mnuFieldChooser.Caption = "Field Chooser"
        Me.mnuFieldChooser.Id = 4
        Me.mnuFieldChooser.ImageOptions.Image = CType(resources.GetObject("mnuFieldChooser.ImageOptions.Image"), System.Drawing.Image)
        Me.mnuFieldChooser.ImageOptions.LargeImage = CType(resources.GetObject("mnuFieldChooser.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.mnuFieldChooser.Name = "mnuFieldChooser"
        '
        'mnuRemoveColumns
        '
        Me.mnuRemoveColumns.Caption = "Remove All Columns"
        Me.mnuRemoveColumns.Id = 7
        Me.mnuRemoveColumns.ImageOptions.Image = CType(resources.GetObject("mnuRemoveColumns.ImageOptions.Image"), System.Drawing.Image)
        Me.mnuRemoveColumns.ImageOptions.LargeImage = CType(resources.GetObject("mnuRemoveColumns.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.mnuRemoveColumns.Name = "mnuRemoveColumns"
        '
        'mnuTotalsFooter
        '
        Me.mnuTotalsFooter.Caption = "Show Totals Footer"
        Me.mnuTotalsFooter.Id = 9
        Me.mnuTotalsFooter.ImageOptions.Image = CType(resources.GetObject("mnuTotalsFooter.ImageOptions.Image"), System.Drawing.Image)
        Me.mnuTotalsFooter.ImageOptions.LargeImage = CType(resources.GetObject("mnuTotalsFooter.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.mnuTotalsFooter.Name = "mnuTotalsFooter"
        '
        'mnuGroupBy
        '
        Me.mnuGroupBy.Caption = "Show Group By Box"
        Me.mnuGroupBy.Id = 1
        Me.mnuGroupBy.ImageOptions.Image = CType(resources.GetObject("mnuGroupBy.ImageOptions.Image"), System.Drawing.Image)
        Me.mnuGroupBy.ImageOptions.LargeImage = CType(resources.GetObject("mnuGroupBy.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.mnuGroupBy.Name = "mnuGroupBy"
        '
        'mnuCollapse
        '
        Me.mnuCollapse.Caption = "Collapse All"
        Me.mnuCollapse.Id = 2
        Me.mnuCollapse.ImageOptions.Image = CType(resources.GetObject("mnuCollapse.ImageOptions.Image"), System.Drawing.Image)
        Me.mnuCollapse.ImageOptions.LargeImage = CType(resources.GetObject("mnuCollapse.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.mnuCollapse.Name = "mnuCollapse"
        '
        'mnuExpand
        '
        Me.mnuExpand.Caption = "Expand All"
        Me.mnuExpand.Id = 3
        Me.mnuExpand.ImageOptions.Image = CType(resources.GetObject("mnuExpand.ImageOptions.Image"), System.Drawing.Image)
        Me.mnuExpand.ImageOptions.LargeImage = CType(resources.GetObject("mnuExpand.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.mnuExpand.Name = "mnuExpand"
        '
        'mnuGroupFooter
        '
        Me.mnuGroupFooter.Caption = "Show Group Totals Footer"
        Me.mnuGroupFooter.Id = 10
        Me.mnuGroupFooter.ImageOptions.Image = CType(resources.GetObject("mnuGroupFooter.ImageOptions.Image"), System.Drawing.Image)
        Me.mnuGroupFooter.ImageOptions.LargeImage = CType(resources.GetObject("mnuGroupFooter.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.mnuGroupFooter.Name = "mnuGroupFooter"
        '
        'mnuLoadLayout
        '
        Me.mnuLoadLayout.Caption = "Load Layout"
        Me.mnuLoadLayout.Id = 6
        Me.mnuLoadLayout.ImageOptions.Image = CType(resources.GetObject("mnuLoadLayout.ImageOptions.Image"), System.Drawing.Image)
        Me.mnuLoadLayout.ImageOptions.LargeImage = CType(resources.GetObject("mnuLoadLayout.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.mnuLoadLayout.Name = "mnuLoadLayout"
        '
        'mnuSaveLayout
        '
        Me.mnuSaveLayout.Caption = "Save Layout"
        Me.mnuSaveLayout.Id = 8
        Me.mnuSaveLayout.ImageOptions.Image = CType(resources.GetObject("mnuSaveLayout.ImageOptions.Image"), System.Drawing.Image)
        Me.mnuSaveLayout.ImageOptions.LargeImage = CType(resources.GetObject("mnuSaveLayout.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.mnuSaveLayout.Name = "mnuSaveLayout"
        '
        'BarManager1
        '
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.mnuExport, Me.mnuGroupBy, Me.mnuCollapse, Me.mnuExpand, Me.mnuFieldChooser, Me.mnuPrint, Me.mnuLoadLayout, Me.mnuRemoveColumns, Me.mnuSaveLayout, Me.mnuTotalsFooter, Me.mnuGroupFooter, Me.mnuAutoFilter, Me.mnuAdvancedFilter, Me.mnuEmailXLSX})
        Me.BarManager1.MaxItemId = 14
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Manager = Me.BarManager1
        Me.barDockControlTop.Size = New System.Drawing.Size(380, 0)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 273)
        Me.barDockControlBottom.Manager = Me.BarManager1
        Me.barDockControlBottom.Size = New System.Drawing.Size(380, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlLeft.Manager = Me.BarManager1
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 273)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(380, 0)
        Me.barDockControlRight.Manager = Me.BarManager1
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 273)
        '
        'CareGrid
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.pp)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Name = "CareGrid"
        Me.Size = New System.Drawing.Size(380, 273)
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxFind, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxFind.ResumeLayout(False)
        Me.gbxFind.PerformLayout()
        CType(Me.txtCriteria.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mnuPopup, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bwWaitPanel As System.ComponentModel.BackgroundWorker
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btnFind As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtCriteria As DevExpress.XtraEditors.TextEdit
    Friend WithEvents mnuPopup As DevExpress.XtraBars.PopupMenu
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents mnuExport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuGroupBy As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuCollapse As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuExpand As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuFieldChooser As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRemoveColumns As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuLoadLayout As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSaveLayout As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuTotalsFooter As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuGroupFooter As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents pp As DevExpress.XtraWaitForm.ProgressPanel
    Friend WithEvents mnuAutoFilter As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuAdvancedFilter As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuEmailXLSX As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents gbxFind As CareFrame
End Class
