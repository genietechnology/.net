﻿
Public Class CareCheckedComboBox

    Private mDataSource As IEnumerable
    Private mList As New List(Of ComboItem)
    Private mAllowBlank As Boolean
    Private mDisplayMember As String
    Private mValueMember As String
    Private mListName As String = ""
    Private mConnectionString As String = ""

#Region "Properties"

    Public ReadOnly Property ListName As String
        Get
            Return mListName
        End Get
    End Property

    Public Property DataSource As IEnumerable
        Get
            Return mDataSource
        End Get
        Set(value As IEnumerable)
            mDataSource = value
            PopulateItems()
        End Set
    End Property

    Public Property SelectedItems As Object
        Get
            If IsPopulated() AndAlso Me.Properties.GetCheckedItems IsNot Nothing Then
                Return Me.Items.GetCheckedValues
            Else
                Return Nothing
            End If
        End Get
        Set(value As Object)
            Me.SetEditValue(value)
        End Set

    End Property

    Public Overrides Property Text As String
        Get
            If IsPopulated() Then
                Return Properties.GetCheckedItems.ToString
            Else
                Return Nothing
            End If
        End Get
        Set(value As String)
            SetEditValue(value)
        End Set
    End Property

    Public Overloads Property [ReadOnly] As Boolean
        Get
            Return Me.Properties.ReadOnly
        End Get
        Set(value As Boolean)
            Me.Properties.ReadOnly = value
        End Set
    End Property

    Public ReadOnly Property ItemCount As Integer
        Get
            If IsPopulated() Then
                Return Me.Properties.Items.Count
            Else
                Return 0
            End If
        End Get
    End Property

    Public ReadOnly Property Items As DevExpress.XtraEditors.Controls.CheckedListBoxItemCollection
        Get
            Return Me.Properties.Items
        End Get
    End Property

    Public Property DisplayMember As String
        Get
            Return mDisplayMember
        End Get
        Set(value As String)
            mDisplayMember = value
        End Set
    End Property

    Public Property ValueMember As String
        Get
            Return mValueMember
        End Get
        Set(value As String)
            mValueMember = value
        End Set
    End Property

#End Region

    Public Sub Clear()
        If IsPopulated() Then Me.Properties.Items.Clear()
    End Sub

    Public Sub AddItem(TextValue As String, Optional Value As String = "", Optional Selected As Boolean = False)
        If Value = "" Then Value = Text
        Me.Properties.Items.Add(New ComboItem(Value, TextValue, Selected))
    End Sub

    Public Sub InsertItem(Index As Integer, TextValue As String, Optional Value As String = "", Optional Selected As Boolean = False)
        If Value = "" Then Value = Text
        Me.Properties.Items.Insert(Index, New ComboItem(Value, TextValue, Selected))
    End Sub

    ''' <summary>
    ''' Populate the Combo using an Application List
    ''' Use ListHandler.ReturnListFromCache
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub PopulateFromListMaintenance(ByVal ApplicationList As Care.Global.Dictionary.ApplicationList)
        Care.Global.KernelLog(Me.Name + " - PopulateFromListMaintenance Begin")
        mListName = ApplicationList.Name
        PopulateItemsFromListMaintenance(ApplicationList)
        Care.Global.KernelLog(Me.Name + " - PopulateFromListMaintenance End")
    End Sub

    ''' <summary>
    ''' Populate the Combo using straight SQL.
    ''' </summary>
    ''' <param name="ConnectionString"></param>
    ''' <param name="SQL"></param>
    ''' <remarks></remarks>
    Public Sub PopulateWithSQL(ByVal ConnectionString As String, ByVal SQL As String)

        Me.Properties.Items.Clear()

        If ConnectionString = "" Then Exit Sub
        If SQL = "" Then Exit Sub

        Dim table As DataTable = Care.Data.DAL.GetDataTablefromSQL(ConnectionString, SQL)

        mDataSource = table.Rows
        PopulateItems()

        table.Dispose()
        table = Nothing

    End Sub

    Private Sub PopulateItemsFromListMaintenance(ByVal ApplicationList As Care.Global.Dictionary.ApplicationList)
        If ApplicationList Is Nothing Then
            mDataSource = Nothing
        Else
            mDataSource = ApplicationList.ListItems
            PopulateItems()
        End If
    End Sub

    Public Sub PopulateWithDictionary(ByVal DictionaryIn As Dictionary(Of Integer, String))
        mDataSource = DictionaryIn
        PopulateItems()
    End Sub

    Private Sub PopulateItems()

        Me.Properties.Items.Clear()

        If mDataSource IsNot Nothing Then

            Me.Properties.Items.BeginUpdate()

            If TypeOf mDataSource Is List(Of Care.Global.Dictionary.ApplicationList.ApplicationListItem) Then
                For Each _i As Care.Global.Dictionary.ApplicationList.ApplicationListItem In mDataSource
                    Me.Properties.Items.Add(New ComboItem(_i.ItemID.ToString, _i.Name))
                Next
            Else
                If TypeOf mDataSource Is System.Collections.Generic.Dictionary(Of Integer, String) Then
                    Dim dictionary As Dictionary(Of Integer, String) = DirectCast(mDataSource, Dictionary(Of Integer, String))
                    For Each item In dictionary
                        Me.Properties.Items.Add(New ComboItem(item.Key.ToString, item.Value))
                    Next
                Else
                    If TypeOf mDataSource Is DataRowCollection Then
                        Dim rows As DataRowCollection = DirectCast(mDataSource, DataRowCollection)
                        If rows.Count > 0 Then
                            For Each row As DataRow In rows
                                If row.ItemArray.Count = 1 Then
                                    Me.Properties.Items.Add(New ComboItem(row.Item(0).ToString, row.Item(0).ToString))
                                Else
                                    Me.Properties.Items.Add(New ComboItem(row.Item(0).ToString, row.Item(1).ToString))
                                End If
                            Next
                        End If
                    End If
                End If
            End If

            Me.Properties.Items.EndUpdate()

        End If

    End Sub

    Private Function IsPopulated() As Boolean
        If Me.Properties.Items IsNot Nothing Then
            If Me.Properties.Items.Count > 0 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function

    Private Sub PopulateItemsFromListMaintenance()

        'Dim sql As String = "select AppListItems.ID, AppListItems.name from AppListItems" &
        '             " left join AppLists on AppLists.ID = AppListItems.list_id" &
        '             " where AppLists.name = '" & mListName & "'" &
        '             " order by AppListItems.seq, AppListItems.name"

        'Dim table As DataTable = Care.Data.DAL.GetDataTablefromSQL(mConnectionString, sql)

        'mDataSource = table.Rows
        'PopulateItems()

        'table.Dispose()
        'table = Nothing

    End Sub

    Public Class ComboItem

        Private mValue As String
        Private mText As String
        Private mSelected As Boolean

        Public Sub New(value As String, text As String, Optional selected As Boolean = False)
            mValue = value
            mText = text
            mSelected = selected
        End Sub

        Public ReadOnly Property Text As String
            Get
                Return mText
            End Get
        End Property

        Public ReadOnly Property Value As String
            Get
                Return mValue
            End Get
        End Property

        Public ReadOnly Property Selected As Boolean
            Get
                Return mSelected
            End Get
        End Property

        Public Overrides Function ToString() As String
            Return mText
        End Function

    End Class

End Class
