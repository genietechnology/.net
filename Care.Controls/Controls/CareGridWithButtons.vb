﻿Imports System.ComponentModel

<DefaultEvent("EditClick")>
Public Class CareGridWithButtons

    Private m_ConnectionString As String = ""
    Private m_SQL As String = ""
    Private m_ButtonsEnabled As Boolean = False

#Region "Events"
    Public Event FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs)
    Public Event AddClick(sender As Object, e As EventArgs)
    Public Event EditClick(sender As Object, e As EventArgs)
    Public Event RemoveClick(sender As Object, e As EventArgs)
    Public Event RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs)
    Public Event RowCellStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs)
#End Region

#Region "Overrideables"
    Public Overridable Function BeforeAdd() As Boolean
        Return True
    End Function

    Public Overridable Function BeforeEdit() As Boolean
        Return True
    End Function

    Public Overridable Function BeforeRemove() As Boolean
        Return True
    End Function
#End Region

#Region "Properties"

    Public Property ButtonsEnabled As Boolean
        Get
            Return m_ButtonsEnabled
        End Get
        Set(value As Boolean)
            m_ButtonsEnabled = value
            SetCMDs()
        End Set
    End Property

    Public Property HideFirstColumn As Boolean
        Get
            Return cgRecords.HideFirstColumn
        End Get
        Set(value As Boolean)
            cgRecords.HideFirstColumn = value
        End Set
    End Property

    Public Property PreviewColumn As String
        Get
            Return cgRecords.PreviewColumn
        End Get
        Set(value As String)
            cgRecords.PreviewColumn = value
        End Set
    End Property

    Public ReadOnly Property RecordCount As Integer
        Get
            Return cgRecords.RecordCount
        End Get
    End Property

    Public ReadOnly Property CurrentRow As System.Data.DataRow
        Get
            Return cgRecords.CurrentRow
        End Get
    End Property

    Public ReadOnly Property Columns As DevExpress.XtraGrid.Columns.GridColumnCollection
        Get
            Return cgRecords.Columns
        End Get
    End Property

    Public ReadOnly Property RowIndex As Integer
        Get
            Return cgRecords.RowIndex
        End Get
    End Property

    Public ReadOnly Property UnderlyingGridControl As DevExpress.XtraGrid.GridControl
        Get
            Return cgRecords.GridControl1
        End Get
    End Property

    Public ReadOnly Property UnderlyingGridView As DevExpress.XtraGrid.Views.Grid.GridView
        Get
            Return cgRecords.GridView1
        End Get
    End Property

#End Region

    Public Sub PersistRowPosition()
        cgRecords.PersistRowPosition()
    End Sub

    Public Sub RestoreRowPosition()
        cgRecords.RestoreRowPosition()
    End Sub

    Public Sub Clear()
        cgRecords.Clear()
        SetCMDs()
    End Sub

    Public Sub AutoSizeColumns()
        cgRecords.AutoSizeColumns()
    End Sub

    Public Sub RePopulate()
        Populate(m_ConnectionString, m_SQL)
    End Sub

    Public Sub Populate(ByVal ConnectionString As String, ByVal SQL As String)

        m_ConnectionString = ConnectionString
        m_SQL = SQL

        cgRecords.Populate(ConnectionString, SQL)
        cgRecords.AutoSizeColumns()
        SetCMDs()

    End Sub

    Public Sub MoveFirst()
        cgRecords.MoveFirst()
        cgRecords.FocusedRowHandle = 0
        RaiseEvent FocusedRowChanged(Nothing, Nothing)
        cgRecords.Focus()
        System.Windows.Forms.Application.DoEvents()
    End Sub

    Private Sub SetCMDs()

        If m_ButtonsEnabled Then
            btnAdd.Enabled = True
            If cgRecords.RecordCount > 0 Then
                btnEdit.Enabled = True
                btnRemove.Enabled = True
            Else
                btnEdit.Enabled = False
                btnRemove.Enabled = False
            End If
        Else
            btnAdd.Enabled = False
            btnEdit.Enabled = False
            btnRemove.Enabled = False
        End If

    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        If BeforeAdd() Then RaiseEvent AddClick(sender, e)
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        EditRecord()
    End Sub

    Private Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click
        If BeforeRemove() Then RaiseEvent RemoveClick(sender, e)
    End Sub

    Private Sub EditRecord()
        If Not btnEdit.Enabled Then Exit Sub
        If BeforeEdit() Then RaiseEvent EditClick(Nothing, Nothing)
    End Sub

    Private Sub cgRecords_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles cgRecords.FocusedRowChanged
        RaiseEvent FocusedRowChanged(sender, e)
    End Sub

    Private Sub cgRecords_GridDoubleClick(sender As Object, e As EventArgs) Handles cgRecords.GridDoubleClick
        EditRecord()
    End Sub

    Private Sub cgRecords_RowCellStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs) Handles cgRecords.RowCellStyle
        RaiseEvent RowCellStyle(sender, e)
    End Sub

    Private Sub cgRecords_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles cgRecords.RowStyle
        RaiseEvent RowStyle(sender, e)
    End Sub
End Class
