﻿Option Strict On

Imports System.ComponentModel
Imports DevExpress.XtraBars

<DefaultEvent("ButtonClick")>
Public Class CareDropDownButton

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private m_MenuItems As String = ""
    Private m_ViaListManager As Boolean = False
    Private m_Items As New List(Of ListItem)

#Region "Public Events"
    Public Event ButtonClick(sender As Object, e As ButtonArgs)
#End Region

#Region "Public Methods"

    Public Sub PopulateMenuItems(ByVal DataRows As DataRowCollection)

        m_ViaListManager = True
        Me.MenuItems = ""

        For Each _r As DataRow In DataRows
            m_Items.Add(New ListItem(_r.Item("id").ToString, _r.Item("name").ToString, _r.Item("ref_1").ToString, _r.Item("ref_2").ToString, _r.Item("ref_3").ToString))
        Next

        PopulateFromList()

    End Sub

#End Region

#Region "Control Properties"

    <Category("_CareAttributes")>
    <Editor(GetType(Design.MultilineStringEditor), GetType(System.Drawing.Design.UITypeEditor))>
    Public Property MenuItems As String
        Get
            Return m_MenuItems
        End Get
        Set(value As String)
            m_MenuItems = value
            PopulateFromStringArray()
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property Caption As String
        Get
            Return b.Text
        End Get
        Set(value As String)
            b.Text = value
        End Set
    End Property

#End Region

#Region "Internal Control Events"

    Private Sub MenuClick(sender As Object, e As ItemClickEventArgs)
        Dim _i As ListItem = ReturnListItem(e.Item.Tag.ToString)
        If _i IsNot Nothing Then
            RaiseEvent ButtonClick(sender, New ButtonArgs(_i.Caption, _i.Ref1, _i.Ref2, _i.Ref3, IsShiftDown))
        End If
    End Sub

    Private Sub b_Click(sender As Object, e As EventArgs) Handles b.Click
        If m_ViaListManager Then
            If m_Items.Count > 0 Then
                Dim _i As ListItem = m_Items.First
                RaiseEvent ButtonClick(sender, New ButtonArgs(_i.Caption, _i.Ref1, _i.Ref2, _i.Ref3, IsShiftDown))
            Else
                RaiseEvent ButtonClick(sender, New ButtonArgs(b.Text, "", "", "", IsShiftDown))
            End If
        Else
            RaiseEvent ButtonClick(sender, New ButtonArgs(b.Text, "", "", "", IsShiftDown))
        End If
    End Sub

#End Region

    Private Function ReturnListItem(ByVal Tag As String) As ListItem
        For Each _i In m_Items
            If _i.ID = Tag Then
                Return _i
            End If
        Next
        Return Nothing
    End Function

    Private Sub PopulateFromList()

        b.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Hide

        bm.BeginUpdate()

        For Each _itm In m_Items

            Dim _Item As New BarButtonItem(bm, _itm.Caption)
            _Item.Name = ReturnMenuName(_itm.Caption)
            _Item.Tag = _itm.ID

            bm.Items.Add(_Item)
            pm.ItemLinks.Add(_Item)
            AddHandler _Item.ItemClick, AddressOf MenuClick

        Next

        bm.EndUpdate()

        If m_Items.Count > 1 Then
            b.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Default
        End If

    End Sub

    Private Sub PopulateFromStringArray()

        b.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Hide

        If m_MenuItems Is Nothing Then Exit Sub
        If m_MenuItems = "" Then Exit Sub

        bm.BeginUpdate()

        Dim _i As Integer = 0
        Dim _Items As String() = Me.MenuItems.Split(CType(vbCrLf, Char()))
        For Each _itm In _Items

            If _itm <> "" Then

                _i += 1

                Dim _Item As New BarButtonItem(bm, _itm)
                _Item.Name = ReturnMenuName(_itm)

                bm.Items.Add(_Item)
                pm.ItemLinks.Add(_Item)
                AddHandler _Item.ItemClick, AddressOf MenuClick

            End If

        Next

        bm.EndUpdate()

        If _i > 1 Then
            b.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Default
        End If

    End Sub

    Private Function ReturnMenuName(ByVal MenuCaption As String) As String
        Return "mnu" + MenuCaption.Replace(" ", "")
    End Function

    Private Function IsShiftDown() As Boolean
        Dim _Shift As Boolean = False
        If Control.ModifierKeys = Keys.Shift Then _Shift = True
        Return _Shift
    End Function

#Region "Event Arg Classes"

    Public Class ButtonArgs

        Inherits EventArgs

        Public Sub New(ByVal Caption As String, ByVal Ref1 As String, ByVal Ref2 As String, ByVal Ref3 As String, ByVal ShiftDown As Boolean)
            Me.Caption = Caption
            Me.Ref1 = Ref1
            Me.Ref2 = Ref2
            Me.Ref3 = Ref3
            Me.ShiftDown = ShiftDown
        End Sub

        Property Caption As String
        Property Ref1 As String
        Property Ref2 As String
        Property Ref3 As String
        Property ShiftDown As Boolean

    End Class

#End Region

    Private Class ListItem

        Public Sub New(ID As String, Caption As String, Ref1 As String, Ref2 As String, Ref3 As String)
            Me.ID = ID
            Me.Caption = Caption
            Me.Ref1 = Ref1
            Me.Ref2 = Ref2
            Me.Ref3 = Ref3
        End Sub

        Property ID As String
        Property Caption As String
        Property Ref1 As String
        Property Ref2 As String
        Property Ref3 As String

    End Class

End Class



