﻿
Public Class CareComboBox

    Private m_DataSource As IEnumerable
    Private m_List As New List(Of ComboItem)
    Private m_AllowBlank As Boolean
    Private m_DisplayMember As String
    Private m_ValueMember As String
    Private m_ListName As String = ""
    Private m_ConnectionString As String = ""
    Private m_ApplicationList As Care.Global.Dictionary.ApplicationList = Nothing

#Region "Properties"

    Public ReadOnly Property ListName As String
        Get
            Return m_ListName
        End Get
    End Property

    Public Property DataSource As IEnumerable
        Get
            Return m_DataSource
        End Get
        Set(value As IEnumerable)
            m_DataSource = value
            PopulateItems()
        End Set
    End Property

    Public Property SelectedValue As Object
        Get
            If IsPopulated() And Me.SelectedItem IsNot Nothing Then
                If IsDBNull(Me.SelectedItem) Then
                    Return Nothing
                Else
                    Dim _item As ComboItem = CType(Me.SelectedItem, ComboItem)
                    Return _item.Value
                End If
            Else
                Return Nothing
            End If
        End Get
        Set(value As Object)

            Me.SelectedIndex = -1
            If value Is Nothing Then Exit Property
            If value.ToString = "" Then Exit Property
            If Not IsPopulated() Then Exit Property

            For Each _item As Object In Me.Properties.Items

                If Not IsDBNull(_item) Then

                    Dim _ComboItem As ComboItem = DirectCast(_item, ComboItem)
                    If _ComboItem.Value = value.ToString Then
                        Me.SelectedItem = _item
                    End If

                    _ComboItem = Nothing

                Else
                    Me.SelectedItem = Nothing
                End If

            Next

        End Set

    End Property

    Public Overrides Property Text As String
        Get
            If IsPopulated() Then
                If Me.SelectedItem IsNot Nothing Then
                    If IsDBNull(Me.SelectedItem) = False Then
                        Dim _item As ComboItem = CType(Me.SelectedItem, ComboItem)
                        Return _item.Text
                    Else
                        Return ""
                    End If
                Else
                    Return ""
                End If
            Else
                Return ""
            End If
        End Get
        Set(value As String)
            Me.SelectedIndex = -1
            If value Is Nothing Then Exit Property
            If value = "" Then Exit Property
            If Not IsPopulated() Then Exit Property

            For Each _item As Object In Me.Properties.Items

                If Not IsDBNull(_item) Then

                    Dim _ComboItem As ComboItem = DirectCast(_item, ComboItem)
                    If _ComboItem.Text = value Then
                        Me.SelectedItem = _item
                    End If

                    _ComboItem = Nothing

                Else
                    Me.SelectedItem = Nothing
                End If

            Next

        End Set
    End Property

    Public Overloads Property [ReadOnly] As Boolean
        Get
            Return Me.Properties.ReadOnly
        End Get
        Set(value As Boolean)
            Me.Properties.ReadOnly = value
        End Set
    End Property

    Public ReadOnly Property ItemCount As Integer
        Get
            If IsPopulated() Then
                Return Me.Properties.Items.Count
            Else
                Return 0
            End If
        End Get
    End Property

    Public ReadOnly Property Items As DevExpress.XtraEditors.Controls.ComboBoxItemCollection
        Get
            Return Me.Properties.Items
        End Get
    End Property

    Public Property DisplayMember As String
        Get
            Return m_DisplayMember
        End Get
        Set(value As String)
            m_DisplayMember = value
        End Set
    End Property

    Public Property ValueMember As String
        Get
            Return m_ValueMember
        End Get
        Set(value As String)
            m_ValueMember = value
        End Set
    End Property

    ''' <summary>
    ''' Allow Blank must be set before Populate is called
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property AllowBlank As Boolean
        Get
            Return m_AllowBlank
        End Get
        Set(value As Boolean)
            m_AllowBlank = value
        End Set
    End Property

#End Region

    Public Sub Clear()
        If IsPopulated() Then Me.Properties.Items.Clear()
    End Sub

    Public Sub AddItem(TextValue As String, Optional Value As String = "")
        If Value = "" Then
            Me.Properties.Items.Add(New ComboItem(TextValue, TextValue))
        Else
            Me.Properties.Items.Add(New ComboItem(Value, TextValue))
        End If
    End Sub

    Public Sub PopulateStart()
        Me.Properties.Items.BeginUpdate()
    End Sub

    Public Sub PopulateEnd()
        Me.Properties.Items.EndUpdate()
    End Sub

    Public Sub InsertItem(TextValue As String, Optional Value As String = "")
        If Value = "" Then
            Me.Properties.Items.Insert(0, New ComboItem(TextValue, TextValue))
        Else
            Me.Properties.Items.Insert(0, New ComboItem(Value, TextValue))
        End If
    End Sub

    ''' <summary>
    ''' Populate the Combo using an Application List
    ''' Use ListHandler.ReturnListFromCache
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub PopulateFromListMaintenance(ByVal ApplicationList As Care.Global.Dictionary.ApplicationList)

        Care.Global.KernelLog(Me.Name + " - PopulateFromListMaintenance Begin")

        m_ApplicationList = ApplicationList
        If m_ApplicationList Is Nothing Then
            m_ListName = ""
        Else
            m_ListName = ApplicationList.Name
        End If

        PopulateItemsFromListMaintenance(ApplicationList)

        Care.Global.KernelLog(Me.Name + " - PopulateFromListMaintenance End")

    End Sub

    ''' <summary>
    ''' Populate the Combo using straight SQL.
    ''' </summary>
    ''' <param name="ConnectionString"></param>
    ''' <param name="SQL"></param>
    ''' <remarks></remarks>
    Public Sub PopulateWithSQL(ByVal ConnectionString As String, ByVal SQL As String)

        Me.Properties.Items.Clear()

        If ConnectionString = "" Then Exit Sub
        If SQL = "" Then Exit Sub

        Care.Global.KernelLog(Me.Name + " - PopulateWithSQL Begin", [Global].DebugLogItem.EnumDebugType.BeginProcess)

        Dim _DT As DataTable = Care.Data.DAL.GetDataTablefromSQL(ConnectionString, SQL)

        m_DataSource = _DT.Rows
        PopulateItems()

        _DT.Dispose()
        _DT = Nothing

        Care.Global.KernelLog(Me.Name + " - PopulateWithSQL End", [Global].DebugLogItem.EnumDebugType.EndProcess)

    End Sub

    Public Sub PopulateWithDictionary(ByVal DictionaryIn As Dictionary(Of Integer, String))
        m_DataSource = DictionaryIn
        PopulateItems()
    End Sub

    Private Sub PopulateItemsFromListMaintenance(ByVal ApplicationList As Care.Global.Dictionary.ApplicationList)
        If ApplicationList Is Nothing Then
            m_DataSource = Nothing
        Else
            m_DataSource = ApplicationList.ListItems
            PopulateItems()
        End If
    End Sub

    Private Sub PopulateItems()

        Me.Properties.Items.Clear()

        If m_DataSource IsNot Nothing Then

            Me.Properties.Items.BeginUpdate()

            If m_AllowBlank Then
                Me.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True
                Me.Properties.Items.Add(DBNull.Value)
            End If

            If TypeOf m_DataSource Is List(Of Care.Global.Dictionary.ApplicationList.ApplicationListItem) Then
                For Each _i As Care.Global.Dictionary.ApplicationList.ApplicationListItem In m_DataSource
                    Me.Properties.Items.Add(New ComboItem(_i.ItemID.ToString, _i.Name))
                Next
            Else

                If TypeOf m_DataSource Is System.Collections.Generic.Dictionary(Of Integer, String) Then

                    Dim _Dictionary As Dictionary(Of Integer, String) = DirectCast(m_DataSource, Dictionary(Of Integer, String))

                    For Each _D In _Dictionary
                        Me.Properties.Items.Add(New ComboItem(_D.Key.ToString, _D.Value))
                    Next

                Else

                    If TypeOf m_DataSource Is DataRowCollection Then
                        Dim _Rows As DataRowCollection = DirectCast(m_DataSource, DataRowCollection)
                        If _Rows.Count > 0 Then
                            For Each _r As DataRow In _Rows
                                If _r.ItemArray.Count = 1 Then
                                    Me.Properties.Items.Add(New ComboItem(_r.Item(0).ToString, _r.Item(0).ToString))
                                Else
                                    Me.Properties.Items.Add(New ComboItem(_r.Item(0).ToString, _r.Item(1).ToString))
                                End If
                            Next
                        End If
                    End If
                End If
            End If

            Me.Properties.Items.EndUpdate()

        End If

    End Sub

    Private Function IsPopulated() As Boolean
        If Me.Properties.Items IsNot Nothing Then
            If Me.Properties.Items.Count > 0 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function

    Public Class ComboItem

        Private _Value As String
        Private _Text As String

        Public Sub New(Value As String, Text As String)
            _Value = Value
            _Text = Text
        End Sub

        Public ReadOnly Property Text As String
            Get
                Return _Text
            End Get
        End Property

        Public ReadOnly Property Value As String
            Get
                Return _Value
            End Get
        End Property

        Public Overrides Function ToString() As String
            Return _Text
        End Function

    End Class

End Class
