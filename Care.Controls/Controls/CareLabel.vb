﻿Imports System.Windows.Forms
Imports Care.Controls
Imports Care.Global
Imports DevExpress.XtraBars

Public Class CareLabel

    Private m_DesignTimeCaption As String = ""

    Public ReadOnly Property DesignTimeCaption As String
        Get
            Return m_DesignTimeCaption
        End Get
    End Property

    Public Property TextAlign As System.Drawing.ContentAlignment
        Get
            Select Case Me.Appearance.HAlignment

                Case DevExpress.Utils.HorzAlignment.Near
                    Return System.Drawing.ContentAlignment.MiddleLeft

                Case DevExpress.Utils.HorzAlignment.Center
                    Return System.Drawing.ContentAlignment.MiddleCenter

                Case DevExpress.Utils.HorzAlignment.Far
                    Return System.Drawing.ContentAlignment.MiddleRight

                Case Else
                    Return System.Drawing.ContentAlignment.MiddleLeft

            End Select
        End Get
        Set(value As System.Drawing.ContentAlignment)
            Select Case value

                Case Drawing.ContentAlignment.MiddleLeft
                    Me.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near

                Case Drawing.ContentAlignment.MiddleCenter
                    Me.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

                Case Drawing.ContentAlignment.MiddleRight
                    Me.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far

            End Select
        End Set
    End Property

    Private Sub CareLabel_DoubleClick(sender As Object, e As EventArgs) Handles Me.DoubleClick
        If Session.CurrentUser.IsSuperAdministrator = False Then Exit Sub
        CaptionHandler.ChangeCaption(Me)
    End Sub

    'Private Sub BarManager1_QueryShowPopupMenu(sender As Object, e As QueryShowPopupMenuEventArgs)
    '    If Session.CurrentUser.IsSuperAdministrator = False Then e.Cancel = True
    'End Sub

    'Private Sub CareLabel_MouseDown(sender As Object, e As MouseEventArgs) Handles Me.MouseDown

    '    If Session.CurrentUser.IsSuperAdministrator = False Then Exit Sub

    '    If e.Button = MouseButtons.Right Then
    '        mnuPopup.ShowPopup(MousePosition)
    '    End If

    'End Sub

    'Private Sub mnuRename_ItemClick(sender As Object, e As ItemClickEventArgs)
    '    CaptionHandler.ChangeCaption(Me)
    'End Sub

End Class
