﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PhotoControl
    Inherits UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PhotoControl))
        Me.pic = New PictureBox()
        Me.ctxPopup = New DevExpress.XtraBars.PopupMenu(Me.components)
        Me.bbiUpload = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiRemove = New DevExpress.XtraBars.BarButtonItem()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.OpenFileDialog1 = New OpenFileDialog()
        Me.bbiRotateLeft = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiRotateRight = New DevExpress.XtraBars.BarButtonItem()
        CType(Me.pic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ctxPopup, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pic
        '
        Me.pic.Dock = DockStyle.Fill
        Me.pic.Location = New System.Drawing.Point(1, 1)
        Me.pic.Name = "pic"
        Me.pic.Size = New System.Drawing.Size(101, 78)
        Me.pic.SizeMode = PictureBoxSizeMode.Zoom
        Me.pic.TabIndex = 0
        Me.pic.TabStop = False
        '
        'ctxPopup
        '
        Me.ctxPopup.AllowRibbonQATMenu = False
        Me.ctxPopup.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.bbiUpload), New DevExpress.XtraBars.LinkPersistInfo(Me.bbiRemove), New DevExpress.XtraBars.LinkPersistInfo(Me.bbiRotateLeft, True), New DevExpress.XtraBars.LinkPersistInfo(Me.bbiRotateRight)})
        Me.ctxPopup.Manager = Me.BarManager1
        Me.ctxPopup.Name = "ctxPopup"
        '
        'bbiUpload
        '
        Me.bbiUpload.Caption = "Upload Photo"
        Me.bbiUpload.Glyph = CType(resources.GetObject("bbiUpload.Glyph"), System.Drawing.Image)
        Me.bbiUpload.Id = 0
        Me.bbiUpload.LargeGlyph = CType(resources.GetObject("bbiUpload.LargeGlyph"), System.Drawing.Image)
        Me.bbiUpload.Name = "bbiUpload"
        '
        'bbiRemove
        '
        Me.bbiRemove.Caption = "Remove Photo"
        Me.bbiRemove.Glyph = CType(resources.GetObject("bbiRemove.Glyph"), System.Drawing.Image)
        Me.bbiRemove.Id = 1
        Me.bbiRemove.LargeGlyph = CType(resources.GetObject("bbiRemove.LargeGlyph"), System.Drawing.Image)
        Me.bbiRemove.Name = "bbiRemove"
        '
        'BarManager1
        '
        Me.BarManager1.AllowCustomization = False
        Me.BarManager1.AllowItemAnimatedHighlighting = False
        Me.BarManager1.AllowMoveBarOnToolbar = False
        Me.BarManager1.AllowQuickCustomization = False
        Me.BarManager1.AllowShowToolbarsPopup = False
        Me.BarManager1.CloseButtonAffectAllTabs = False
        Me.BarManager1.DockingEnabled = False
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.bbiUpload, Me.bbiRemove, Me.bbiRotateLeft, Me.bbiRotateRight})
        Me.BarManager1.MaxItemId = 4
        Me.BarManager1.ShowFullMenusAfterDelay = False
        Me.BarManager1.ShowScreenTipsInToolbars = False
        Me.BarManager1.ShowShortcutInScreenTips = False
        Me.BarManager1.UseAltKeyForMenu = False
        Me.BarManager1.UseF10KeyForMenu = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'bbiRotateLeft
        '
        Me.bbiRotateLeft.Caption = "Rotate Left"
        Me.bbiRotateLeft.Glyph = CType(resources.GetObject("bbiRotateLeft.Glyph"), System.Drawing.Image)
        Me.bbiRotateLeft.Id = 2
        Me.bbiRotateLeft.LargeGlyph = CType(resources.GetObject("bbiRotateLeft.LargeGlyph"), System.Drawing.Image)
        Me.bbiRotateLeft.Name = "bbiRotateLeft"
        '
        'bbiRotateRight
        '
        Me.bbiRotateRight.Caption = "Rotate Right"
        Me.bbiRotateRight.Glyph = CType(resources.GetObject("bbiRotateRight.Glyph"), System.Drawing.Image)
        Me.bbiRotateRight.Id = 3
        Me.bbiRotateRight.LargeGlyph = CType(resources.GetObject("bbiRotateRight.LargeGlyph"), System.Drawing.Image)
        Me.bbiRotateRight.Name = "bbiRotateRight"
        '
        'PhotoControl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.Controls.Add(Me.pic)
        Me.Name = "PhotoControl"
        Me.Padding = New Padding(1, 1, 0, 0)
        Me.Size = New System.Drawing.Size(102, 79)
        CType(Me.pic,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.ctxPopup,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.BarManager1,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)

End Sub
    Friend WithEvents pic As PictureBox
    Friend WithEvents ctxPopup As DevExpress.XtraBars.PopupMenu
    Friend WithEvents bbiUpload As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiRemove As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents bbiRotateLeft As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiRotateRight As DevExpress.XtraBars.BarButtonItem

End Class
