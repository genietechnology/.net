﻿Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class frmGenCode

    Dim m_Connection As String = "Data Source=<SERVER>;" & _
                                 "Initial Catalog=<DB>;" & _
                                 "User Id=james;" & _
                                 "Password=$chm31chel"

    Dim m_Ver As String = My.Application.Info.Version.ToString
    Dim m_Line As String = "--------------------------------------------------------------------------------------"

    Dim m_Variables As String
    Dim m_Properties As String
    Dim m_Constructors As String
    Dim m_Methods As String
    Dim m_DataToBusiness As String
    Dim m_BusinessToData As String
    Dim m_ClassShort As String

    Dim m_SQLVariables As String
    Dim m_UpsertSet As String
    Dim m_SQLFields As String
    Dim m_SQLParams As String

    Private Sub frmGenCode_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.Text = "Code Generator Ver. " + m_Ver
        txtServer.Text = "localhost"
    End Sub

    Private Sub btnBL_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBL.Click
        BuildBusiness()
    End Sub

    Private Sub btnSchema_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSchema.Click
        ReadSchema()
    End Sub

    Private Sub btnUpsert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpsert.Click
        BuildUpsert(True)
    End Sub

    Private Sub btnGets_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGets.Click

        If txtInput.Text = "" Then Exit Sub
        txtOutput.BackColor = Color.White

        txtOutput.Text = ReturnGets(True)

    End Sub

    Private Function ReturnGets(ByVal SetStatements As Boolean) As String

        Dim _tmp As String
        _tmp = BuildGetAll(SetStatements)
        _tmp += vbCrLf
        _tmp += BuildGetbyID(False)

        Return _tmp

    End Function

    Private Sub btnCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        Clipboard.Clear()
        Try
            Clipboard.SetText(txtOutput.Text)
            txtOutput.BackColor = Color.Gray
        Catch ex As Exception

        End Try
    End Sub

#Region "Build Business Object"

    Private Sub BuildBusiness()

        If txtInput.Text = "" Then Exit Sub

        txtOutput.BackColor = Color.White

        m_Variables = ""
        m_Properties = ""
        m_Constructors = ""
        m_Methods = ""
        m_DataToBusiness = ""
        m_BusinessToData = ""

        m_ClassShort = "_" & Mid(txtTable.Text, 1, 1)

        ProcessLines("Business")

        Dim sb As New System.Text.StringBuilder

        Dim _DateTime As String = Now.ToString
        Dim _Line As String = "'Generated " + _DateTime + " using Version " + m_Ver
        sb.AppendLine("'" + New String("*", _Line.Length))
        sb.AppendLine(_Line)
        sb.AppendLine("'" + New String("*", _Line.Length))
        sb.AppendLine()

        sb.AppendLine("Imports Care.Global")
        sb.AppendLine("Imports Care.Data")
        sb.AppendLine()

        sb.AppendLine("Namespace Business")
        sb.AppendLine()

        sb.AppendLine("Public Class " & txtClassName.Text)
        sb.AppendLine("Inherits DataObjectBase")
        sb.AppendLine("Implements IBusinessEntity")
        sb.AppendLine()

        'sb.AppendLine("#Region " & Chr(34) & "Variables" & Chr(34))
        'sb.AppendLine()
        'sb.AppendLine(m_Variables)
        'sb.AppendLine("#End Region")
        'sb.AppendLine()

        sb.AppendLine("#Region " & Chr(34) & "Constructors" & Chr(34))
        sb.AppendLine()
        sb.AppendLine("Public Sub New()")
        sb.AppendLine()
        sb.AppendLine("With Me")

        If Not chkReadOnly.Checked Then
            sb.AppendLine(".IsNew = True")
            sb.AppendLine(".IsDeleted = False")
            sb.AppendLine("._ID = Guid.NewGuid")
        End If

        sb.AppendLine(m_Constructors)
        sb.AppendLine("End With")
        sb.AppendLine()
        sb.AppendLine("End Sub")
        sb.AppendLine()
        sb.AppendLine("Public Sub New(ID as Guid)")
        sb.AppendLine()
        sb.AppendLine("With Me")

        If Not chkReadOnly.Checked Then
            sb.AppendLine(".IsNew = False")
            sb.AppendLine(".IsDeleted = False")
            sb.AppendLine("._ID = ID")
        End If

        sb.AppendLine(m_Constructors)
        sb.AppendLine("End With")
        sb.AppendLine()
        sb.AppendLine("End Sub")
        sb.AppendLine()
        sb.AppendLine("#End Region")
        sb.AppendLine()

        sb.AppendLine("#Region " & Chr(34) & "Properties" & Chr(34))
        sb.AppendLine()
        sb.AppendLine(m_Properties)
        sb.AppendLine("#End Region")
        sb.AppendLine()

        sb.AppendLine("#Region " & Chr(34) & "Methods" & Chr(34))
        sb.AppendLine()
        sb.AppendLine(Methods)
        sb.AppendLine("#End Region")
        sb.AppendLine()

        sb.AppendLine("#Region " & Chr(34) & "Helper Methods" & Chr(34))
        sb.AppendLine()
        sb.AppendLine(HelperMethods)
        sb.AppendLine("#End Region")
        sb.AppendLine()

        sb.AppendLine("End Class")
        sb.AppendLine()

        sb.AppendLine("End Namespace")

        txtOutput.Text = sb.ToString

    End Sub

    Private Sub BuildProperties()

        If txtInput.Text = "" Then Exit Sub

        txtOutput.BackColor = Color.White

        m_Variables = ""
        m_Properties = ""

        ProcessLines("Business")

        Dim sb As New System.Text.StringBuilder
        sb.AppendLine()
        sb.AppendLine(m_Variables)
        sb.AppendLine()
        sb.AppendLine(m_Properties)
        sb.AppendLine()

        txtOutput.Text = sb.ToString

    End Sub

    Private Sub ProcessBL(ByVal Mode As String, ByVal Name As String, ByVal Type As String)

        m_Variables += "Dim m_" & PropertyFormat(Name) & " As " & SQLTypetoVBType(Type) & vbCrLf
        m_Properties += BuildProperty(Mode, Name, Type)

        If Name.ToLower <> "id" Then
            m_Constructors += BuildConstructor(Name, Type) & vbCrLf
        End If

        m_DataToBusiness += m_ClassShort & "._" & PropertyFormat(Name) & " = " & DataRowFormat(Name, Type) & vbCrLf

    End Sub

    Private Sub ProcessDataContract(ByVal Mode As String, ByVal Name As String, ByVal Type As String)
        m_Properties += BuildProperty(Mode, Name, Type)
    End Sub

    Private Function HelperMethods() As String

        Dim sb As New System.Text.StringBuilder

        sb.AppendLine("Private Shared Function PropertiesFromData(ByVal DR As DataRow) As " & txtClassName.Text)
        sb.AppendLine()
        sb.AppendLine("Dim " & m_ClassShort & " As " & txtClassName.Text & " = Nothing")
        sb.AppendLine()
        sb.AppendLine("If DR IsNot Nothing Then")
        sb.AppendLine(m_ClassShort & " = New " & txtClassName.Text & "()")
        sb.AppendLine("With DR")

        If Not chkReadOnly.Checked Then
            sb.AppendLine(m_ClassShort & ".IsNew = False")
            sb.AppendLine(m_ClassShort & ".IsDeleted = False")
        End If

        sb.AppendLine(m_DataToBusiness)
        sb.AppendLine("End With")
        sb.AppendLine("End If")
        sb.AppendLine()
        sb.AppendLine("Return " & m_ClassShort)
        sb.AppendLine()
        sb.AppendLine("End Function")
        sb.AppendLine()

        sb.AppendLine("Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of " & txtClassName.Text & ")")
        sb.AppendLine()
        sb.AppendLine("Dim _" & txtClassName.Text & "List As New List(Of " & txtClassName.Text & ")")
        sb.AppendLine("Dim _DR As DataRow")
        sb.AppendLine()
        sb.AppendLine("For Each _DR In Table.Rows")
        sb.AppendLine("_" & txtClassName.Text & "List.Add(PropertiesFromData(_DR))")
        sb.AppendLine("Next")
        sb.AppendLine()
        sb.AppendLine("Return _" & txtClassName.Text & "List")
        sb.AppendLine()
        sb.AppendLine("End Function")
        sb.AppendLine()

        Return sb.ToString

    End Function

    Private Function Methods() As String

        Dim sb As New System.Text.StringBuilder

        sb.AppendLine("Public Shared Function RetreiveByID(ByVal ID As Guid) As " & txtClassName.Text)
        sb.AppendLine("Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, " & Chr(34) & "get" & txtClassName.Text & "byID" & Chr(34) & ", ID))")
        sb.AppendLine("End Function")
        sb.AppendLine()

        sb.AppendLine("Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As " & txtClassName.Text)
        sb.AppendLine("Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, " & Chr(34) & "get" & txtClassName.Text & "byID" & Chr(34) & ", ID))")
        sb.AppendLine("End Function")
        sb.AppendLine()

        sb.AppendLine("Public Shared Function RetreiveAll() As List(Of " & txtClassName.Text & ")")
        sb.AppendLine("Dim _" & txtClassName.Text & "List As List(Of " & txtClassName.Text & ")")
        sb.AppendLine("_" & txtClassName.Text & "List = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, " & Chr(34) & "get" & txtClassName.Text & "Table" & Chr(34) & "))")
        sb.AppendLine("Return _" & txtClassName.Text & "List")
        sb.AppendLine("End Function")
        sb.AppendLine()

        sb.AppendLine("Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of " & txtClassName.Text & ")")
        sb.AppendLine("Dim _" & txtClassName.Text & "List As List(Of " & txtClassName.Text & ")")
        sb.AppendLine("_" & txtClassName.Text & "List = PopulateList(DAL.GetDataTablefromSP(ConnectionString, " & Chr(34) & "get" & txtClassName.Text & "Table" & Chr(34) & "))")
        sb.AppendLine("Return _" & txtClassName.Text & "List")
        sb.AppendLine("End Function")
        sb.AppendLine()

        If chkReadOnly.Checked = False Then

            sb.AppendLine("Public Shared Sub SaveAll(ByVal " & txtClassName.Text & "List As List(Of " & txtClassName.Text & "))")
            sb.AppendLine("For Each _" & txtClassName.Text & " As " & txtClassName.Text & " In " & txtClassName.Text & "List")
            sb.AppendLine("SaveRecord(_" & txtClassName.Text & ")")
            sb.AppendLine("Next")
            sb.AppendLine("End Sub")
            sb.AppendLine()

            sb.AppendLine("Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal " & txtClassName.Text & "List As List(Of " & txtClassName.Text & "))")
            sb.AppendLine("For Each _" & txtClassName.Text & " As " & txtClassName.Text & " In " & txtClassName.Text & "List")
            sb.AppendLine("SaveRecord(ConnectionString, _" & txtClassName.Text & ")")
            sb.AppendLine("Next")
            sb.AppendLine("End Sub")
            sb.AppendLine()

            sb.AppendLine("Public Shared Function SaveRecord(ByVal " & txtClassName.Text & " As " & txtClassName.Text & ") As Guid")
            sb.AppendLine("Dim _Current As " + txtClassName.Text + " = RetreiveByID(" + txtClassName.Text + "._ID.Value)")
            sb.AppendLine(txtClassName.Text & ".SetDefaultValues")
            sb.AppendLine("DAL.SaveRecord(Session.ConnectionString, Session.CurrentUser.ID, " & txtClassName.Text & ", _Current, " & Chr(34) & "upsert" & txtClassName.Text & Chr(34) & ")")
            sb.AppendLine("End Function")
            sb.AppendLine()

            sb.AppendLine("Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal " & txtClassName.Text & " As " & txtClassName.Text & ") As Guid")
            sb.AppendLine("Dim _Current As " + txtClassName.Text + " = RetreiveByID(ConnectionString, " + txtClassName.Text + "._ID.Value)")
            sb.AppendLine(txtClassName.Text & ".SetDefaultValues")
            sb.AppendLine("DAL.SaveRecord(ConnectionString, Session.CurrentUser.ID, " & txtClassName.Text & ", _Current, " & Chr(34) & "upsert" & txtClassName.Text & Chr(34) & ")")
            sb.AppendLine("End Function")
            sb.AppendLine()

            sb.AppendLine("Public Shared Sub DeleteRecord(ByVal ID As Guid)")
            sb.AppendLine("Dim _Current As " + txtClassName.Text + " = RetreiveByID(ID)")
            sb.AppendLine("DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, " & Chr(34) & "delete" & txtClassName.Text & "byID" & Chr(34) & ", ID)")
            sb.AppendLine("End Sub")
            sb.AppendLine()

            sb.AppendLine("Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)")
            sb.AppendLine("Dim _Current As " + txtClassName.Text + " = RetreiveByID(ID)")
            sb.AppendLine("DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, " & Chr(34) & "delete" & txtClassName.Text & "byID" & Chr(34) & ", ID)")
            sb.AppendLine("End Sub")
            sb.AppendLine()

            sb.AppendLine("Public Sub Store()")
            sb.AppendLine("Dim _Current As " + txtClassName.Text + " = RetreiveByID(Session.ConnectionString, _ID.Value)")
            sb.AppendLine("SetDefaultValues")
            sb.AppendLine("DAL.SaveRecord(Session.ConnectionString, Session.CurrentUser.ID, Me, _Current, " & Chr(34) & "upsert" & txtClassName.Text & Chr(34) & ")")
            sb.AppendLine("End Sub")
            sb.AppendLine()

            sb.AppendLine("Public Sub Store(ByVal ConnectionString As String)")
            sb.AppendLine("Dim _Current As " + txtClassName.Text + " = RetreiveByID(ConnectionString, _ID.Value)")
            sb.AppendLine("SetDefaultValues")
            sb.AppendLine("DAL.SaveRecord(ConnectionString, Session.CurrentUser.ID, Me, _Current, " & Chr(34) & "upsert" & txtClassName.Text & Chr(34) & ")")
            sb.AppendLine("End Sub")
            sb.AppendLine()

        End If

        Return sb.ToString

    End Function

    Private Function BuildConstructor(ByVal Name As String, ByVal Type As String) As String

        Dim constructor As String
        Select Case SQLTypetoVBType(Type)

            Case "String"
                constructor = "._" & PropertyFormat(Name) & " = " & Chr(34) & Chr(34)

            Case "Boolean"
                constructor = "._" & PropertyFormat(Name) & " = False"

            Case "Single", "Integer", "Long", "Double", "Decimal"
                constructor = "._" & PropertyFormat(Name) & " = 0"

            Case "Date"
                constructor = "._" & PropertyFormat(Name) & " = Nothing"

            Case Else
                constructor = "._" & PropertyFormat(Name) & " = Nothing"

        End Select

        Return constructor

    End Function

    Private Function BuildProperty(ByVal Mode As String, ByVal Name As String, ByVal Type As String) As String

        Dim sb As New System.Text.StringBuilder

        If Mode = "DataContract" Then
            sb.AppendLine("<DataMember()>")
            sb.AppendLine("Public Property " & PropertyFormat(Name) & " As " & SQLTypetoVBType(Type))
            sb.AppendLine()
        Else

            If radNoAttrib.Checked = False Then
                If radSP.Checked Then
                    sb.AppendLine("<StoredProcParameter(" & Chr(34) & "@" & PropertyFormat(Name) & Chr(34) & ")> _")
                Else
                    sb.AppendLine("<FieldName(" & Chr(34) & Name & Chr(34) & ")> _")
                End If
            End If

            sb.AppendLine("Public Property _" & PropertyFormat(Name) & "() As " & SQLTypetoVBType(Type))
            'sb.AppendLine("Get")
            'sb.AppendLine("Return m_" & PropertyFormat(Name))
            'sb.AppendLine("End Get")
            'sb.AppendLine("Set(ByVal value as " & SQLTypetoVBType(Type) & ")")
            'sb.AppendLine("m_" & PropertyFormat(Name) & " = value")
            'sb.AppendLine("End Set")
            'sb.AppendLine("End Property")
            sb.AppendLine()

        End If

        Return sb.ToString

    End Function

    Private Function DataRowFormat(ByVal Name As String, ByVal Type As String) As String

        Dim _return As String = ""

        Select Case Type

            Case "varchar", "char", "text", "nvarchar", "nchar"
                _return = "DR(" & Chr(34) & Name & Chr(34) & ").ToString.Trim"

            Case "uniqueidentifier"
                _return = "GetGUID(DR(" & Chr(34) & Name & Chr(34) & "))"

            Case "date"
                _return = "GetDate(DR(" & Chr(34) & Name & Chr(34) & "))"

            Case "datetime", "smalldatetime"
                _return = "GetDateTime(DR(" & Chr(34) & Name & Chr(34) & "))"

            Case "time"
                _return = "GetTimeSpan(DR(" & Chr(34) & Name & Chr(34) & "))"

            Case "bit"
                _return = "GetBoolean(DR(" & Chr(34) & Name & Chr(34) & "))"

            Case "bigint"
                _return = "GetLong(DR(" & Chr(34) & Name & Chr(34) & "))"

            Case "int", "smallint"
                _return = "GetInteger(DR(" & Chr(34) & Name & Chr(34) & "))"

            Case "money", "numeric", "float", "decimal"
                _return = "GetDecimal(DR(" & Chr(34) & Name & Chr(34) & "))"

            Case "image"
                _return = "GetImage(DR(" & Chr(34) & Name & Chr(34) & "))"

            Case "xml"
                _return = "GetXML(DR(" & Chr(34) & Name & Chr(34) & "))"

            Case "tinyint"
                _return = "GetByte(DR(" & Chr(34) & Name & Chr(34) & "))"

            Case Else
                MsgBox("DataRowFormat: UnhandledExceptionEventArgs Type = " & Name & ", " & Type)

        End Select

        Return _return

    End Function

#End Region

#Region "Build Delete"

    Private Sub BuildDelete(ByVal SetStatements As Boolean)

        If txtInput.Text = "" Then Exit Sub

        txtOutput.BackColor = Color.White

        txtOutput.Text = ReturnDelete(SetStatements)

    End Sub

    Private Function ReturnDelete(ByVal SetStatements As Boolean)

        Dim sb As New System.Text.StringBuilder

        sb.AppendLine(ProcedureHeader(SetStatements))
        sb.AppendLine(DropProcedure("delete" & txtClassName.Text & "byID"))
        sb.AppendLine()
        sb.AppendLine("CREATE PROCEDURE delete" & txtClassName.Text & "byID")
        sb.AppendLine("@ID uniqueidentifier")
        sb.AppendLine("AS")
        sb.AppendLine(vbTab & "BEGIN")
        sb.AppendLine(vbTab & vbTab & "SET NOCOUNT ON")

        sb.AppendLine(vbTab & vbTab & "DELETE FROM " & txtTable.Text & " WHERE ID = @ID")
        sb.AppendLine(vbTab & "END")
        sb.AppendLine()
        sb.AppendLine(vbTab & "RETURN")
        sb.AppendLine()
        sb.AppendLine("GO")

        Return sb.ToString
        sb = Nothing

    End Function


#End Region

#Region "Build Upsert"

    Private Sub BuildUpsert(ByVal SetStatements As Boolean)

        If txtInput.Text = "" Then Exit Sub

        txtOutput.BackColor = Color.White

        m_SQLVariables = ""
        m_UpsertSet = ""
        m_SQLFields = ""
        m_SQLParams = ""

        Dim _Script As String = ReturnUpsert(SetStatements)

        If SetStatements Then
            _Script += vbNewLine
            _Script += m_Line
            _Script += vbNewLine
            _Script += vbNewLine
        End If

        txtOutput.Text = _Script

    End Sub

    Private Function ReturnUpsert(ByVal SetStatements As Boolean)

        ProcessLines("Upsert")

        Dim sb As New System.Text.StringBuilder

        sb.AppendLine(ProcedureHeader(SetStatements))
        sb.AppendLine(DropProcedure("upsert" & txtClassName.Text))
        sb.AppendLine()
        sb.AppendLine("CREATE PROCEDURE upsert" & txtClassName.Text)

        sb.AppendLine(m_SQLVariables)
        sb.AppendLine("AS")
        sb.AppendLine(vbTab & "BEGIN")
        sb.AppendLine(vbTab & vbTab & "SET NOCOUNT ON")

        sb.AppendLine(vbTab & vbTab & "IF EXISTS (SELECT ID FROM " & txtTable.Text & " WHERE ID = @ID)")
        sb.AppendLine(vbTab & vbTab & vbTab & "UPDATE " & txtTable.Text)
        sb.AppendLine(vbTab & vbTab & vbTab & "SET " & m_UpsertSet)
        sb.AppendLine(vbTab & vbTab & vbTab & "WHERE ID = @ID")
        sb.AppendLine(vbTab & vbTab & "ELSE")
        sb.AppendLine(vbTab & vbTab & vbTab & "INSERT INTO " & txtTable.Text & "(" & m_SQLFields & ")")
        sb.AppendLine(vbTab & vbTab & vbTab & "VALUES (" & m_SQLParams & ")")
        sb.AppendLine(vbTab & "END")
        sb.AppendLine()
        sb.AppendLine(vbTab & "RETURN")
        sb.AppendLine()
        sb.AppendLine("GO")

        Return sb.ToString

        sb = Nothing

    End Function


    Private Sub ProcessUpsert(ByVal Name As String, ByVal Type As String, ByVal Length As String, ByVal FirstLine As Boolean, ByVal LastLine As Boolean)

        m_SQLVariables += vbTab & "@" & PropertyFormat(Name).ToLower & " " & Type

        If Length = "()" Then Length = ""

        If LastLine Then
            m_SQLVariables += Length & " = NULL" & vbCrLf
        Else
            m_SQLVariables += Length & " = NULL," & vbCrLf
        End If

        If FirstLine Then
            m_UpsertSet += Name & " = @" & PropertyFormat(Name).ToLower & "," & vbCrLf
        Else
            If LastLine Then
                m_UpsertSet += vbTab & vbTab & vbTab & Name & " = @" & PropertyFormat(Name).ToLower
            Else
                m_UpsertSet += vbTab & vbTab & vbTab & Name & " = @" & PropertyFormat(Name).ToLower & "," & vbCrLf
            End If
        End If

        If LastLine Then
            m_SQLFields += Name.ToLower
        Else
            m_SQLFields += Name.ToLower & ", "
        End If

        If LastLine Then
            m_SQLParams += "@" & PropertyFormat(Name).ToLower
        Else
            m_SQLParams += "@" & PropertyFormat(Name).ToLower & ", "
        End If

    End Sub

#End Region

#Region "Build Gets"

    Private Function BuildGetAll(ByVal SetStatements As Boolean) As String

        Dim sb As New System.Text.StringBuilder

        sb.AppendLine(ProcedureHeader(SetStatements))
        sb.AppendLine(DropProcedure("get" & txtClassName.Text & "Table"))
        sb.AppendLine()
        sb.AppendLine("CREATE PROCEDURE get" & txtClassName.Text & "Table")
        sb.AppendLine("AS")
        sb.AppendLine(vbTab & "BEGIN")
        sb.AppendLine(vbTab & vbTab & "SET NOCOUNT ON")

        sb.AppendLine(vbTab & vbTab & "SELECT * FROM " & txtTable.Text)
        sb.AppendLine(vbTab & "END")
        sb.AppendLine()
        sb.AppendLine(vbTab & "RETURN")
        sb.AppendLine()
        sb.AppendLine("GO")

        Return sb.ToString

    End Function

    Private Function BuildGetbyID(ByVal SetStatements As Boolean) As String

        Dim sb As New System.Text.StringBuilder

        sb.AppendLine(ProcedureHeader(SetStatements))
        sb.AppendLine(DropProcedure("get" & txtClassName.Text & "byID"))
        sb.AppendLine()
        sb.AppendLine("CREATE PROCEDURE get" & txtClassName.Text & "byID")
        sb.AppendLine("@ID uniqueidentifier")
        sb.AppendLine("AS")
        sb.AppendLine(vbTab & "BEGIN")
        sb.AppendLine(vbTab & vbTab & "SET NOCOUNT ON")

        sb.AppendLine(vbTab & vbTab & "SELECT * FROM " & txtTable.Text & " WHERE ID = @ID")
        sb.AppendLine(vbTab & "END")
        sb.AppendLine()
        sb.AppendLine(vbTab & "RETURN")
        sb.AppendLine()
        sb.AppendLine("GO")

        Return sb.ToString

    End Function

#End Region

#Region "Helpers"

    Private Sub ProcessLines(ByVal Mode As String)

        Dim Lines As String() = txtInput.Text.Split(New Char() {CChar(vbCrLf)})
        Dim _First As Boolean
        Dim _Last As Boolean

        For i = 0 To Lines.Count - 1

            _First = False
            _Last = False

            If i = 0 Then _First = True
            If i = Lines.Count - 1 Then _Last = True

            ProcessLine(Mode, Lines(i), _First, _Last)

        Next

    End Sub

    Private Sub ProcessLine(ByVal Mode As String, ByVal LineText As String, ByVal FirstLine As Boolean, ByVal LastLine As Boolean)

        Dim Line As String() = LineText.Split(Chr(32))
        If Line.Count = 3 Then

            Dim _Name As String = Line(0)
            Dim _Type As String = Line(1)
            Dim _Length As String = Line(2)

            _Name = Replace(_Name, Chr(10), "")

            Select Case Mode

                Case "Business"
                    ProcessBL(Mode, _Name, _Type)

                Case "DataContract"
                    ProcessDataContract(Mode, _Name, _Type)

                Case "Upsert"
                    ProcessUpsert(_Name, _Type, _Length, FirstLine, LastLine)

            End Select


        End If

    End Sub

    Private Sub ReadSchema()

        txtInput.Text = ""

        Dim _SQL As String
        _SQL = "select COLUMN_NAME, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH from INFORMATION_SCHEMA.COLUMNS" & _
               " where TABLE_NAME = '" & txtTable.Text & "'" & _
               " order by ORDINAL_POSITION"

        Dim _dt As DataTable = GetDataTablefromSQL(_SQL)

        If _dt Is Nothing Then Exit Sub

        Dim _sb As New System.Text.StringBuilder

        Dim _dr As DataRow

        Dim _line As String = ""
        Dim _i = 1
        For Each _dr In _dt.Rows

            Select Case _dr("DATA_TYPE").ToString

                Case "image", "xml"

                    _line = _dr("COLUMN_NAME").ToString & " " & _
                            _dr("DATA_TYPE").ToString & _
                            " ()"

                Case Else

                    _line = _dr("COLUMN_NAME").ToString & " " & _
                            _dr("DATA_TYPE").ToString & _
                            " (" & ReturnMaxLength(_dr("CHARACTER_MAXIMUM_LENGTH").ToString) & ")"

            End Select

            If _i = _dt.Rows.Count Then
                _sb.Append(_line)
            Else
                _sb.AppendLine(_line)
            End If

            _i += 1

        Next

        _dt.Dispose()
        _dt = Nothing

        txtInput.Text = _sb.ToString

    End Sub

    Private Function ReturnMaxLength(ByVal MaxLength As String) As String
        If MaxLength = "" Then Return ""
        If MaxLength = "-1" Then Return "MAX"
        Return MaxLength
    End Function

    Private Function SQLTypetoVBType(ByVal TypeIn As String) As String

        Dim _type As String = ""
        Select Case TypeIn

            Case "uniqueidentifier"
                _type = "Guid?"

            Case "char", "varchar", "text", "nvarchar", "nchar", "xml"
                _type = "String"

            Case "date"
                _type = "Date?"

            Case "datetime", "smalldatetime"
                _type = "DateTime?"

            Case "time"
                _type = "TimeSpan?"

            Case "bit"
                _type = "Boolean"

            Case "int", "smallint"
                _type = "Integer"

            Case "bigint"
                _type = "Long"

            Case "money", "numeric", "float", "decimal"
                _type = "Decimal"

            Case "tinyint"
                _type = "Byte"

            Case "image"
                _type = "Byte()"

            Case Else
                MsgBox("Unhandled Datatype Encountered [" & TypeIn & "]")

        End Select

        Return _type

    End Function

    Private Function PropertyFormat(ByVal StringIn As String) As String

        Dim strX As String = StringIn.ToLower
        Dim str1 As String = ""
        Dim blnCapNext As Boolean = True

        If strX = "id" Then Return "ID"

        For i = 1 To strX.Length

            If i < strX.Length Then

                If strX.Chars(i).ToString = "_" Then
                    blnCapNext = True
                    i = i + 1
                Else

                    If blnCapNext Then

                        Dim strBefore As String = ""
                        If i > 2 Then strBefore = Mid(strX, 1, i - 1)

                        Dim strAfter As String = ""
                        If i < strX.Length Then strAfter = Mid(strX, i + 1)

                        Dim strChar As String = strX.Chars(i - 1).ToString.ToUpper

                        strX = strBefore + strChar + strAfter
                        blnCapNext = False

                    End If

                End If

            End If

        Next

        'remove underscores
        strX = strX.Replace("_", "")
        Return strX

    End Function

    Private Function DropProcedure(ByVal ProcedureName As String) As String

        Dim _sb As New System.Text.StringBuilder

        _sb.AppendLine("IF OBJECT_ID('" & ProcedureName & "') IS NOT NULL")
        _sb.AppendLine("DROP PROCEDURE " & ProcedureName)
        _sb.Append("GO")

        Return _sb.ToString

    End Function

    Private Function ProcedureHeader(ByVal SetStatements As Boolean) As String

        If Not SetStatements Then Return ""

        Dim _sb As New System.Text.StringBuilder
        Dim _DateTime As String = Now.ToString
        Dim _Ver As String = My.Application.Info.Version.ToString

        _sb.AppendLine("--- Generated " + _DateTime + " using Version " + _Ver + " ---")
        _sb.AppendLine()

        If chkUseDB.Checked Then
            _sb.AppendLine("USE [" + txtDB.Text + "]")
            _sb.AppendLine()
        End If

        _sb.AppendLine("SET ANSI_NULLS ON")
        _sb.AppendLine("GO")
        _sb.AppendLine("SET QUOTED_IDENTIFIER ON")
        _sb.AppendLine("GO")

        _sb.AppendLine(m_Line)

        Return _sb.ToString

    End Function

#End Region

    Private Sub btnProps_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProps.Click
        BuildProperties()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        BuildDelete(True)
    End Sub

    Private Function GetDataTablefromSQL(ByVal SQLStatement As String) As DataTable

        Dim _cmd As SqlCommand = ReturnTextCommand(SQLStatement)
        Dim _dt As DataTable = ReturnDataTable(_cmd)

        DisposeofCommand(_cmd)
        Return _dt

    End Function

    Private Function ReturnDataTable(ByVal CommandObject As SqlCommand) As DataTable

        Dim _Return As DataTable = Nothing
        Dim _DT As New DataTable

        Dim _DA As New SqlDataAdapter(CommandObject)

        Try

            _DA.Fill(_DT)
            _Return = _DT

            _DT.Dispose()
            _DT = Nothing

        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "DAL.ReturnDataTable")

        Finally

            _DA.Dispose()
            _DA = Nothing

            DisposeofCommand(CommandObject)

        End Try

        Return _Return

    End Function

    Private Function ReturnConnection() As SqlConnection

        Dim _cstring As String = m_Connection
        _cstring = Replace(_cstring, "<DB>", txtDB.Text)
        _cstring = Replace(_cstring, "<SERVER>", txtServer.Text)

        Dim _cn As New SqlConnection
        _cn.ConnectionString = _cstring

        Try
            _cn.Open()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "DAL.ReturnConnection")
            Return Nothing
        End Try

        Return _cn

    End Function

    Private Function ReturnTextCommand(ByVal CommandText As String) As SqlCommand

        Dim _cmd As New SqlCommand
        With _cmd
            .Connection = ReturnConnection()
            .CommandType = CommandType.Text
            .CommandText = CommandText
            .Parameters.Clear()
        End With

        Return _cmd

    End Function

    Private Sub DisposeofCommand(ByRef cmd As SqlCommand)

        With cmd
            .Parameters.Clear()
            .Connection.Close()
            .Connection.Dispose()
            .Dispose()
        End With

        cmd = Nothing

    End Sub

    Private Sub btnGenerateAll_Click(sender As System.Object, e As System.EventArgs) Handles btnGenerateAll.Click

        m_SQLVariables = ""
        m_UpsertSet = ""
        m_SQLFields = ""
        m_SQLParams = ""

        If txtInput.Text = "" Then Exit Sub
        txtOutput.BackColor = Color.White

        Dim _Script As String = ""

        _Script += ProcedureHeader(True)
        _Script += ReturnGets(False)
        _Script += ReturnUpsert(False)
        _Script += ReturnDelete(False)

        _Script += vbNewLine
        _Script += m_Line
        _Script += vbNewLine
        _Script += vbNewLine

        txtOutput.Text = _Script

    End Sub

    Private Sub btnSQLtoVB_Click(sender As System.Object, e As System.EventArgs) Handles btnSQLtoVB.Click

        Dim _Lines As New List(Of String)
        For Each _line As String In txtInput.Lines

            If _Lines.Count = 0 Then
                _Lines.Add("Dim _SQL As String = " + Chr(34) + _line + Chr(34) + " & _")
            Else
                _Lines.Add(Chr(34) + " " + _line + Chr(34) + " & _")
            End If

        Next

        txtOutput.Text = String.Join(vbCrLf, _Lines.ToArray)

    End Sub

    Private Sub txtTable_TextChanged(sender As Object, e As EventArgs) Handles txtTable.TextChanged
        txtClassName.Text = txtTable.Text
    End Sub

    Private Sub txtTable_Validated(sender As Object, e As EventArgs) Handles txtTable.Validated
        If txtTable.Text <> "" Then
            If txtTable.Text.EndsWith("s") Then
                txtClassName.Text = txtTable.Text.Substring(0, txtTable.Text.Length - 1)
            End If
        End If
    End Sub

    Private Sub btnDC_Click(sender As Object, e As EventArgs) Handles btnDC.Click

        If txtInput.Text = "" Then Exit Sub

        txtOutput.BackColor = Color.White

        m_Variables = ""
        m_Properties = ""

        ProcessLines("DataContract")

        Dim sb As New System.Text.StringBuilder

        Dim _DateTime As String = Now.ToString
        Dim _Line As String = "'Generated " + _DateTime + " using Version " + m_Ver

        sb.AppendLine("'" + New String("*", _Line.Length))
        sb.AppendLine(_Line)
        sb.AppendLine("'" + New String("*", _Line.Length))
        sb.AppendLine()

        sb.AppendLine("Namespace Model")
        sb.AppendLine()

        sb.AppendLine("<DataContract()>")
        sb.AppendLine("Public Class " & txtClassName.Text)
        sb.AppendLine()

        sb.AppendLine(m_Properties)

        sb.AppendLine("End Class")
        sb.AppendLine("End Namespace")

        txtOutput.Text = sb.ToString

    End Sub

    Private Sub btnSwap_Click(sender As Object, e As EventArgs) Handles btnSwap.Click

        If txtInput.Text = "" Then Exit Sub

        Dim _Lines As New List(Of String)
        For Each _line As String In txtInput.Lines

            Dim _Equals As Integer = _line.IndexOf("=")
            If _Equals > 0 Then

                Dim _Left As String = _line.Substring(0, _Equals - 1)
                Dim _Right As String = _line.Substring(_Equals + 1)

                _Lines.Add(_Right.Trim + " = " + _Left.Trim)

            End If

        Next

        txtOutput.Text = String.Join(vbCrLf, _Lines.ToArray)

    End Sub
End Class
