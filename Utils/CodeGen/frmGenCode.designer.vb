﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGenCode
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtInput = New System.Windows.Forms.TextBox()
        Me.txtOutput = New System.Windows.Forms.TextBox()
        Me.btnBL = New System.Windows.Forms.Button()
        Me.txtTable = New System.Windows.Forms.TextBox()
        Me.btnCopy = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtClassName = New System.Windows.Forms.TextBox()
        Me.btnSchema = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnProps = New System.Windows.Forms.Button()
        Me.radSP = New System.Windows.Forms.RadioButton()
        Me.radFieldName = New System.Windows.Forms.RadioButton()
        Me.radNoAttrib = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnDC = New System.Windows.Forms.Button()
        Me.chkReadOnly = New System.Windows.Forms.CheckBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtServer = New System.Windows.Forms.TextBox()
        Me.chkUseDB = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtDB = New System.Windows.Forms.TextBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.btnSwap = New System.Windows.Forms.Button()
        Me.btnSQLtoVB = New System.Windows.Forms.Button()
        Me.btnGets = New System.Windows.Forms.Button()
        Me.btnUpsert = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnGenerateAll = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtInput
        '
        Me.txtInput.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtInput.Font = New System.Drawing.Font("Courier New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInput.Location = New System.Drawing.Point(11, 114)
        Me.txtInput.Margin = New System.Windows.Forms.Padding(2)
        Me.txtInput.Multiline = True
        Me.txtInput.Name = "txtInput"
        Me.txtInput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtInput.Size = New System.Drawing.Size(328, 351)
        Me.txtInput.TabIndex = 4
        '
        'txtOutput
        '
        Me.txtOutput.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtOutput.Font = New System.Drawing.Font("Courier New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOutput.Location = New System.Drawing.Point(346, 114)
        Me.txtOutput.Margin = New System.Windows.Forms.Padding(2)
        Me.txtOutput.Multiline = True
        Me.txtOutput.Name = "txtOutput"
        Me.txtOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtOutput.Size = New System.Drawing.Size(608, 351)
        Me.txtOutput.TabIndex = 5
        '
        'btnBL
        '
        Me.btnBL.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBL.Location = New System.Drawing.Point(4, 17)
        Me.btnBL.Margin = New System.Windows.Forms.Padding(2)
        Me.btnBL.Name = "btnBL"
        Me.btnBL.Size = New System.Drawing.Size(139, 23)
        Me.btnBL.TabIndex = 0
        Me.btnBL.Text = "Generate Business Class"
        Me.btnBL.UseVisualStyleBackColor = True
        '
        'txtTable
        '
        Me.txtTable.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTable.Location = New System.Drawing.Point(79, 45)
        Me.txtTable.Margin = New System.Windows.Forms.Padding(2)
        Me.txtTable.Name = "txtTable"
        Me.txtTable.Size = New System.Drawing.Size(160, 22)
        Me.txtTable.TabIndex = 5
        '
        'btnCopy
        '
        Me.btnCopy.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCopy.Location = New System.Drawing.Point(4, 17)
        Me.btnCopy.Margin = New System.Windows.Forms.Padding(2)
        Me.btnCopy.Name = "btnCopy"
        Me.btnCopy.Size = New System.Drawing.Size(112, 23)
        Me.btnCopy.TabIndex = 0
        Me.btnCopy.Text = "Copy to Clip"
        Me.btnCopy.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 49)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "TableName"
        '
        'txtClassName
        '
        Me.txtClassName.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClassName.Location = New System.Drawing.Point(404, 46)
        Me.txtClassName.Margin = New System.Windows.Forms.Padding(2)
        Me.txtClassName.Name = "txtClassName"
        Me.txtClassName.Size = New System.Drawing.Size(124, 22)
        Me.txtClassName.TabIndex = 8
        '
        'btnSchema
        '
        Me.btnSchema.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSchema.Location = New System.Drawing.Point(243, 44)
        Me.btnSchema.Margin = New System.Windows.Forms.Padding(2)
        Me.btnSchema.Name = "btnSchema"
        Me.btnSchema.Size = New System.Drawing.Size(85, 23)
        Me.btnSchema.TabIndex = 6
        Me.btnSchema.Text = "Read Schema"
        Me.btnSchema.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(332, 49)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(62, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "ClassName"
        '
        'btnProps
        '
        Me.btnProps.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProps.Location = New System.Drawing.Point(4, 44)
        Me.btnProps.Margin = New System.Windows.Forms.Padding(2)
        Me.btnProps.Name = "btnProps"
        Me.btnProps.Size = New System.Drawing.Size(139, 23)
        Me.btnProps.TabIndex = 1
        Me.btnProps.Text = "Generate Properties"
        Me.btnProps.UseVisualStyleBackColor = True
        '
        'radSP
        '
        Me.radSP.AutoSize = True
        Me.radSP.Checked = True
        Me.radSP.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radSP.Location = New System.Drawing.Point(11, 76)
        Me.radSP.Margin = New System.Windows.Forms.Padding(2)
        Me.radSP.Name = "radSP"
        Me.radSP.Size = New System.Drawing.Size(133, 17)
        Me.radSP.TabIndex = 10
        Me.radSP.TabStop = True
        Me.radSP.Text = "Stored Proc Attribute"
        Me.radSP.UseVisualStyleBackColor = True
        '
        'radFieldName
        '
        Me.radFieldName.AutoSize = True
        Me.radFieldName.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radFieldName.Location = New System.Drawing.Point(162, 76)
        Me.radFieldName.Margin = New System.Windows.Forms.Padding(2)
        Me.radFieldName.Name = "radFieldName"
        Me.radFieldName.Size = New System.Drawing.Size(127, 17)
        Me.radFieldName.TabIndex = 11
        Me.radFieldName.Text = "Fieldname Attribute"
        Me.radFieldName.UseVisualStyleBackColor = True
        '
        'radNoAttrib
        '
        Me.radNoAttrib.AutoSize = True
        Me.radNoAttrib.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radNoAttrib.Location = New System.Drawing.Point(305, 76)
        Me.radNoAttrib.Margin = New System.Windows.Forms.Padding(2)
        Me.radNoAttrib.Name = "radNoAttrib"
        Me.radNoAttrib.Size = New System.Drawing.Size(89, 17)
        Me.radNoAttrib.TabIndex = 12
        Me.radNoAttrib.Text = "No Attribute"
        Me.radNoAttrib.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnDC)
        Me.GroupBox1.Controls.Add(Me.btnBL)
        Me.GroupBox1.Controls.Add(Me.btnProps)
        Me.GroupBox1.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(559, 2)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Size = New System.Drawing.Size(150, 108)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Business Classes"
        '
        'btnDC
        '
        Me.btnDC.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDC.Location = New System.Drawing.Point(4, 71)
        Me.btnDC.Margin = New System.Windows.Forms.Padding(2)
        Me.btnDC.Name = "btnDC"
        Me.btnDC.Size = New System.Drawing.Size(139, 23)
        Me.btnDC.TabIndex = 2
        Me.btnDC.Text = "Generate DataContract"
        Me.btnDC.UseVisualStyleBackColor = True
        '
        'chkReadOnly
        '
        Me.chkReadOnly.AutoSize = True
        Me.chkReadOnly.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkReadOnly.Location = New System.Drawing.Point(419, 76)
        Me.chkReadOnly.Name = "chkReadOnly"
        Me.chkReadOnly.Size = New System.Drawing.Size(109, 17)
        Me.chkReadOnly.TabIndex = 13
        Me.chkReadOnly.Text = "ReadOnly Mode"
        Me.chkReadOnly.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.chkReadOnly)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.txtServer)
        Me.GroupBox3.Controls.Add(Me.chkUseDB)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.txtDB)
        Me.GroupBox3.Controls.Add(Me.Label1)
        Me.GroupBox3.Controls.Add(Me.txtTable)
        Me.GroupBox3.Controls.Add(Me.txtClassName)
        Me.GroupBox3.Controls.Add(Me.radNoAttrib)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.radFieldName)
        Me.GroupBox3.Controls.Add(Me.btnSchema)
        Me.GroupBox3.Controls.Add(Me.radSP)
        Me.GroupBox3.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(11, 2)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox3.Size = New System.Drawing.Size(544, 108)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Generate from Table"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(217, 22)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(22, 13)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "DB"
        '
        'txtServer
        '
        Me.txtServer.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtServer.Location = New System.Drawing.Point(79, 19)
        Me.txtServer.Margin = New System.Windows.Forms.Padding(2)
        Me.txtServer.Name = "txtServer"
        Me.txtServer.Size = New System.Drawing.Size(134, 22)
        Me.txtServer.TabIndex = 1
        '
        'chkUseDB
        '
        Me.chkUseDB.AutoSize = True
        Me.chkUseDB.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkUseDB.Location = New System.Drawing.Point(355, 21)
        Me.chkUseDB.Name = "chkUseDB"
        Me.chkUseDB.Size = New System.Drawing.Size(173, 17)
        Me.chkUseDB.TabIndex = 9
        Me.chkUseDB.Text = "Script Use <DatabaseName>"
        Me.chkUseDB.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 22)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "SQL Server"
        '
        'txtDB
        '
        Me.txtDB.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDB.Location = New System.Drawing.Point(243, 19)
        Me.txtDB.Margin = New System.Windows.Forms.Padding(2)
        Me.txtDB.Name = "txtDB"
        Me.txtDB.Size = New System.Drawing.Size(85, 22)
        Me.txtDB.TabIndex = 3
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.btnSwap)
        Me.GroupBox4.Controls.Add(Me.btnSQLtoVB)
        Me.GroupBox4.Controls.Add(Me.btnCopy)
        Me.GroupBox4.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(834, 2)
        Me.GroupBox4.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox4.Size = New System.Drawing.Size(120, 108)
        Me.GroupBox4.TabIndex = 3
        Me.GroupBox4.TabStop = False
        '
        'btnSwap
        '
        Me.btnSwap.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSwap.Location = New System.Drawing.Point(4, 73)
        Me.btnSwap.Margin = New System.Windows.Forms.Padding(2)
        Me.btnSwap.Name = "btnSwap"
        Me.btnSwap.Size = New System.Drawing.Size(112, 23)
        Me.btnSwap.TabIndex = 2
        Me.btnSwap.Text = "Swap Sides"
        Me.btnSwap.UseVisualStyleBackColor = True
        '
        'btnSQLtoVB
        '
        Me.btnSQLtoVB.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSQLtoVB.Location = New System.Drawing.Point(4, 45)
        Me.btnSQLtoVB.Margin = New System.Windows.Forms.Padding(2)
        Me.btnSQLtoVB.Name = "btnSQLtoVB"
        Me.btnSQLtoVB.Size = New System.Drawing.Size(112, 23)
        Me.btnSQLtoVB.TabIndex = 1
        Me.btnSQLtoVB.Text = "SQL to VB"
        Me.btnSQLtoVB.UseVisualStyleBackColor = True
        '
        'btnGets
        '
        Me.btnGets.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGets.Location = New System.Drawing.Point(4, 44)
        Me.btnGets.Margin = New System.Windows.Forms.Padding(2)
        Me.btnGets.Name = "btnGets"
        Me.btnGets.Size = New System.Drawing.Size(109, 23)
        Me.btnGets.TabIndex = 2
        Me.btnGets.Text = "Generate Gets"
        Me.btnGets.UseVisualStyleBackColor = True
        '
        'btnUpsert
        '
        Me.btnUpsert.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpsert.Location = New System.Drawing.Point(4, 17)
        Me.btnUpsert.Margin = New System.Windows.Forms.Padding(2)
        Me.btnUpsert.Name = "btnUpsert"
        Me.btnUpsert.Size = New System.Drawing.Size(53, 23)
        Me.btnUpsert.TabIndex = 0
        Me.btnUpsert.Text = "Upsert"
        Me.btnUpsert.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Location = New System.Drawing.Point(60, 17)
        Me.btnDelete.Margin = New System.Windows.Forms.Padding(2)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(53, 23)
        Me.btnDelete.TabIndex = 1
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnGenerateAll)
        Me.GroupBox2.Controls.Add(Me.btnDelete)
        Me.GroupBox2.Controls.Add(Me.btnUpsert)
        Me.GroupBox2.Controls.Add(Me.btnGets)
        Me.GroupBox2.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(713, 2)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox2.Size = New System.Drawing.Size(117, 108)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Stored Procs"
        '
        'btnGenerateAll
        '
        Me.btnGenerateAll.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerateAll.Location = New System.Drawing.Point(4, 71)
        Me.btnGenerateAll.Margin = New System.Windows.Forms.Padding(2)
        Me.btnGenerateAll.Name = "btnGenerateAll"
        Me.btnGenerateAll.Size = New System.Drawing.Size(109, 23)
        Me.btnGenerateAll.TabIndex = 3
        Me.btnGenerateAll.Text = "Generate ALL"
        Me.btnGenerateAll.UseVisualStyleBackColor = True
        '
        'frmGenCode
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(963, 477)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtOutput)
        Me.Controls.Add(Me.txtInput)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "frmGenCode"
        Me.Text = "Code Generator"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtInput As System.Windows.Forms.TextBox
    Friend WithEvents txtOutput As System.Windows.Forms.TextBox
    Friend WithEvents btnBL As System.Windows.Forms.Button
    Friend WithEvents txtTable As System.Windows.Forms.TextBox
    Friend WithEvents btnCopy As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtClassName As System.Windows.Forms.TextBox
    Friend WithEvents btnSchema As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnProps As System.Windows.Forms.Button
    Friend WithEvents radSP As System.Windows.Forms.RadioButton
    Friend WithEvents radFieldName As System.Windows.Forms.RadioButton
    Friend WithEvents radNoAttrib As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDB As System.Windows.Forms.TextBox
    Friend WithEvents chkReadOnly As System.Windows.Forms.CheckBox
    Friend WithEvents btnGets As System.Windows.Forms.Button
    Friend WithEvents btnUpsert As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnGenerateAll As System.Windows.Forms.Button
    Friend WithEvents btnSQLtoVB As System.Windows.Forms.Button
    Friend WithEvents chkUseDB As System.Windows.Forms.CheckBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtServer As System.Windows.Forms.TextBox
    Friend WithEvents btnDC As System.Windows.Forms.Button
    Friend WithEvents btnSwap As System.Windows.Forms.Button
End Class
