﻿Public Class ClearBooksEngine

    Public Shared Sub OpenEntity(ByVal NetworkID As String, ByVal EntityID As String)
        Dim _URL As String = "https://secure.clearbooks.co.uk/" + NetworkID + "/accounting/contacts/overview/" + EntityID + "/"
        Process.Start(_URL)
    End Sub

    Public Shared Sub OpenStatement(ByVal NetworkID As String, ByVal EntityID As String)
        Dim _URL As String = "https://secure.clearbooks.co.uk/" + NetworkID + "/accounting/statement/view/?entity=" + EntityID
        Process.Start(_URL)
    End Sub

    Public Shared Sub OpenInvoice(ByVal NetworkID As String, ByVal InvoiceID As String)
        Dim _URL As String = "https://secure.clearbooks.co.uk/" + NetworkID + "/accounting/sales/view/?invoice_id=" + InvoiceID
        Process.Start(_URL)
    End Sub

    Public Shared Sub OpenEmployee(ByVal NetworkID As String, ByVal EmployeeID As String)
        Dim _URL As String = "https://secure.clearbooks.co.uk/" + NetworkID + "/payroll/employee-management/details/" + EmployeeID + "/"
        Process.Start(_URL)
    End Sub

    Public Shared Sub OpenPayslip(ByVal NetworkID As String, ByVal Year As Integer, ByVal Month As Integer, ByVal EmployeeID As String)
        Dim _URL As String = "https://secure.clearbooks.co.uk/" + NetworkID + "/payroll/employee/payslip/" + Year.ToString + "/" + Month.ToString + "/M/" + EmployeeID + "/"
        Process.Start(_URL)
    End Sub

    Friend Shared Function Connect(ByRef ConnectionDetailsIn As ClearBooksConnection, ByVal APIID As String, ByVal APIKey As String) As Boolean

        If ConnectionDetailsIn Is Nothing Then
            ConnectionDetailsIn = New ClearBooksConnection
        End If

        Try
            ConnectionDetailsIn.ClearBooksAPI = CType(New ClearBooks.AccountingPortClient, ClearBooks.AccountingPort)
            ConnectionDetailsIn.ClearBooksToken = New ClearBooks.authenticate
            ConnectionDetailsIn.ClearBooksToken.apiKey = APIKey

        Catch ex As Exception
            Return False
        End Try

        Return True

    End Function

    Friend Shared Function CreateEntity(ByRef ConnectionDetailsIn As ClearBooksConnection, ByRef EntityIn As Entity) As Boolean

        Dim _Return As Boolean = True
        Dim _Extra As New ClearBooks.EntityExtra

        Dim _E As New ClearBooks.Entity
        With _E
            .company_name = EntityIn.ContactName
            .address1 = EntityIn.Address1
            .address2 = EntityIn.Address2
            .town = EntityIn.Address3
            .county = EntityIn.Address4
            .contact_name = EntityIn.ContactName
            .company_number = EntityIn.Phone
            .email = EntityIn.Email
            .customer = _Extra
        End With

        Try
            Dim _Req As New ClearBooks.createEntityRequest(ConnectionDetailsIn.ClearBooksToken, _E)
            Dim _Res As ClearBooks.createEntityResponse = ConnectionDetailsIn.ClearBooksAPI.createEntity(_Req)

            EntityIn.ExternalID = _Res.createEntityResult.ToString

        Catch ex As Exception
            _Return = False
        End Try

        Return _Return

    End Function

    Friend Shared Function UpdateEntity(ByRef ConnectionDetailsIn As ClearBooksConnection, ByRef EntityIn As Entity) As Boolean

        Dim _Return As Boolean = True
        Dim _Extra As New ClearBooks.EntityExtra

        Dim _E As New ClearBooks.Entity
        With _E
            .company_name = EntityIn.ContactName
            .address1 = EntityIn.Address1
            .address2 = EntityIn.Address2
            .town = EntityIn.Address3
            .county = EntityIn.Address4
            .contact_name = EntityIn.ContactName
            .company_number = EntityIn.Phone
            .email = EntityIn.Email
            .customer = _Extra
        End With

        Try
            Dim _Req As New ClearBooks.updateEntityRequest(ConnectionDetailsIn.ClearBooksToken, CInt(_E.external_id), _E)
            Dim _Res As ClearBooks.updateEntityResponse = ConnectionDetailsIn.ClearBooksAPI.updateEntity(_Req)

            EntityIn.ExternalID = _Res.updateEntityResult.ToString

        Catch ex As Exception
            _Return = False
        End Try

        Return _Return

    End Function

    Private Shared Function GetDocumentType(ByVal DocType As Care.Financials.Document.EnumDocumentType) As String
        If DocType = Document.EnumDocumentType.Purchase Then Return "purchases"
        If DocType = Document.EnumDocumentType.SalesCreditNote Then Return "cn-sales"
        If DocType = Document.EnumDocumentType.PurchaseCreditNote Then Return "cn-purchases"
        Return "sales"
    End Function

    Friend Shared Function CreateInvoice(ByRef ConnectionDetailsIn As ClearBooksConnection, ByRef DocumentIn As Document) As Boolean

        Dim _Return As Boolean = True

        Dim _CBInv As New ClearBooks.Invoice
        With _CBInv
            .dateCreated = DocumentIn.DateCreated
            .dateDue = Today.AddDays(30)
            .description = DocumentIn.Description
            .reference = DocumentIn.Reference
            .type = GetDocumentType(DocumentIn.DocumentType)
            .entityId = CInt(DocumentIn.EntityID)
        End With

        Dim _CBItems As New List(Of ClearBooks.Item)

        For Each _Item As Care.Financials.DocumentItem In DocumentIn.Items

            Dim _CBItem As New ClearBooks.Item

            _CBItem.description = _Item.Description
            _CBItem.unitPrice = _Item.UnitPrice
            _CBItem.quantity = _Item.Quantity
            _CBItem.type = CInt(_Item.NLCode)
            _CBItem.vatRate = _Item.VATAmount.ToString

            _CBItems.Add(_CBItem)

        Next

        _CBInv.items = _CBItems.ToArray

        Try
            Dim _Req As New ClearBooks.createInvoiceRequest(ConnectionDetailsIn.ClearBooksToken, _CBInv)
            Dim _Res As ClearBooks.createInvoiceResponse = ConnectionDetailsIn.ClearBooksAPI.createInvoice(_Req)

            DocumentIn.ExternalID = _Res.createInvoiceResult.invoice_id.ToString

        Catch ex As Exception
            _Return = False
        End Try

        Return _Return

    End Function



End Class
