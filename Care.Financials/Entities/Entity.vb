﻿Imports Care.Financials

Public Class Entity

    Public Property ContactName As String
    Public Property Forename As String
    Public Property Surname As String
    Public Property Address1 As String
    Public Property Address2 As String
    Public Property Address3 As String
    Public Property Address4 As String
    Public Property Address5 As String
    Public Property Email As String
    Public Property Phone As String
    Public Property Mobile As String
    Public Property Fax As String
    Public Property Website As String

    Public Property ExternalID As String

    Public Property Exception As Boolean
    Public Property ExceptionText As String

    Public Property OurAccountNumber As String

    Public Property SiteID As Guid

End Class
