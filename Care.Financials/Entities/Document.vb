﻿Public Class Document

    Public Enum EnumDocumentType
        Purchase
        PurchaseCreditNote
        Sales
        SalesCreditNote
    End Enum

    Public Property EntityID As String
    Public Property DocumentType As EnumDocumentType
    Public Property DateCreated As Date
    Public Property DateDue As Date
    Public Property Reference As String
    Public Property Description As String

    Public Property Items As List(Of DocumentItem)
    Public Property ExternalID As String

    Public Property Exception As Boolean
    Public Property ExceptionText As String

End Class
