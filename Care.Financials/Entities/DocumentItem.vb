﻿Public Class DocumentItem

    Public Property Description As String
    Public Property Quantity As Integer
    Public Property UnitPrice As Decimal
    Public Property VATAmount As Decimal
    Public Property VATRatePercent As Decimal
    Public Property NLCode As String
    Public Property NLTrackingCategory As String
    Public Property NLTracking As String

    Public Property Exception As Boolean
    Public Property ExceptionText As String


End Class
