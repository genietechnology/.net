﻿Public Class Response

    Private m_Error As Boolean = False
    Private m_Ex As Exception = Nothing
    Private m_ErrorText As String = ""

    Public Sub New()
        m_Error = False
        m_Ex = Nothing
        m_ErrorText = ""
    End Sub

    Public Sub New(ByVal ErrorText As String)
        m_Error = True
        m_ErrorText = ErrorText
    End Sub

    Public Sub New(ByVal ErrorText As String, ByVal Ex As Exception)
        m_Error = True
        m_ErrorText = ErrorText
        m_Ex = Ex
    End Sub

    Public ReadOnly Property Success As Boolean
        Get
            Return Not m_Error
        End Get
    End Property

    Public ReadOnly Property HasErrors As Boolean
        Get
            Return m_Error
        End Get
    End Property

    Public ReadOnly Property Exception As Exception
        Get
            Return m_Ex
        End Get
    End Property

    Public ReadOnly Property ErrorText As String
        Get
            Return m_ErrorText
        End Get
    End Property

End Class
