﻿Option Strict On
Imports System.Net
Imports Care.Financials.IntegrationDetail

Public Class Engine

    Private m_Error As New Response
    Private m_IntegrationDetails As IntegrationDetail
    Private m_Connected As Boolean = False
    Private m_ConnectionDetails As ClearBooksConnection

    Private m_APIID As String = ""
    Private m_APIKey As String = ""

    Private m_SageEngine As SageEngine = Nothing

    Public Sub New(ByVal IntegrationDetails As IntegrationDetail)
        m_IntegrationDetails = IntegrationDetails
    End Sub

    Public ReadOnly Property Connected As Boolean
        Get
            Return m_Connected
        End Get
    End Property

    Public ReadOnly Property ErrorResponse As Response
        Get
            Return m_Error
        End Get
    End Property

    Public Sub Disconnect()

        Select Case m_IntegrationDetails.Partner

            Case EnumIntegrationPartner.Sage
                If m_SageEngine IsNot Nothing Then
                    m_SageEngine.Disconnect()
                End If
                m_Connected = False

        End Select

    End Sub

    Public Sub OpenInvoice(ByVal ExternalID As String)

        Select Case m_IntegrationDetails.Partner

            Case EnumIntegrationPartner.ClearBooks
                ClearBooksEngine.OpenInvoice(m_APIID, ExternalID)

            Case EnumIntegrationPartner.Xero
                XeroEngine.OpenInvoice(ExternalID)

            Case EnumIntegrationPartner.Sage
                SageEngine.OpenInvoice(ExternalID)

        End Select

    End Sub

    Public Sub OpenEntity(ByVal ExternalID As String)

        Select Case m_IntegrationDetails.Partner

            Case EnumIntegrationPartner.ClearBooks
                ClearBooksEngine.OpenEntity(m_APIID, ExternalID)

            Case EnumIntegrationPartner.Xero
                XeroEngine.OpenEntity(ExternalID)

            Case EnumIntegrationPartner.Sage
                SageEngine.OpenEntity(ExternalID)

        End Select

    End Sub

    Public Sub OpenStatement(ByVal ExternalID As String)

        Select Case m_IntegrationDetails.Partner

            Case EnumIntegrationPartner.ClearBooks
                ClearBooksEngine.OpenStatement(m_APIID, ExternalID)

            Case EnumIntegrationPartner.Xero
                XeroEngine.OpenStatement(ExternalID)

        End Select

    End Sub

    Public Function GetBalance(ByVal ExternalID As String, ByRef Balance As Decimal) As Response

        Select Case m_IntegrationDetails.Partner

            Case EnumIntegrationPartner.Xero
                Return XeroEngine.GetBalance(m_IntegrationDetails, ExternalID, Balance)

            Case Else
                Return New Response("GetBalance only supported by Xero")

        End Select

    End Function

    Public Function FindExternalIDByName(ByVal Name As String) As String
        Return XeroEngine.FindExternalIDByName(m_IntegrationDetails, Name)
    End Function

    Public Function CreateDocument(ByRef DocumentIn As Document) As Response

        If DocumentIn.EntityID Is Nothing Then Return New Response("DocumentIn.EntityID is nothing")
        If DocumentIn.EntityID = "" Then Return New Response("DocumentIn.EntityID is nothing")

        Dim _ConnectResponse As Response = Connect()

        If Not _ConnectResponse.HasErrors Then

            Select Case m_IntegrationDetails.Partner

                Case EnumIntegrationPartner.ClearBooks
                    If ClearBooksEngine.CreateInvoice(m_ConnectionDetails, DocumentIn) Then
                        Return New Response
                    Else
                        Return New Response("ClearBooksEngine.CreateInvoice returned False")
                    End If

                Case EnumIntegrationPartner.Xero
                    Return XeroEngine.CreateDocument(m_IntegrationDetails, DocumentIn)

                Case EnumIntegrationPartner.Sage
                    Return m_SageEngine.CreateDocument(DocumentIn)

                Case Else
                    Return New Response("Unhandled Partner.")

            End Select

        Else
            Return _ConnectResponse
        End If

    End Function

    Public Function CreateEntity(ByRef EntityIn As Entity) As Response

        Dim _ConnectResponse As Response = Connect()
        If Not _ConnectResponse.HasErrors Then

            Select Case m_IntegrationDetails.Partner

                Case EnumIntegrationPartner.ClearBooks
                    If ClearBooksEngine.CreateEntity(m_ConnectionDetails, EntityIn) Then
                        Return New Response
                    Else
                        Return New Response("ClearBooksEngine.CreateEntity failed.")
                    End If

                Case EnumIntegrationPartner.Xero
                    Return XeroEngine.CreateContact(m_IntegrationDetails, EntityIn)

                Case EnumIntegrationPartner.Sage
                    Return m_SageEngine.CreateContact(EntityIn)

                Case Else
                    Return New Response("Unhandled Partner.")

            End Select

        Else
            Return _ConnectResponse
        End If

    End Function

    Public Function UpdateEntity(ByRef EntityIn As Entity) As Response

        Dim _ConnectResponse As Response = Connect()
        If Not _ConnectResponse.HasErrors Then

            Select Case m_IntegrationDetails.Partner

                Case EnumIntegrationPartner.ClearBooks
                    If ClearBooksEngine.UpdateEntity(m_ConnectionDetails, EntityIn) Then
                        Return New Response
                    Else
                        Return New Response("ClearBooksEngine.UpdateEntity failed.")
                    End If

                Case EnumIntegrationPartner.Xero
                    Return XeroEngine.UpdateContact(m_IntegrationDetails, EntityIn)

                Case EnumIntegrationPartner.Sage
                    Return m_SageEngine.UpdateContact(EntityIn)

                Case Else
                    Return New Response("Unhandled Partner.")

            End Select

        Else
            Return _ConnectResponse
        End If

    End Function

    Public Function ResetAccountNumbers() As Response

        Dim _ConnectResponse As Response = Connect()
        If Not _ConnectResponse.HasErrors Then

            Select Case m_IntegrationDetails.Partner

                Case EnumIntegrationPartner.Xero
                    Return XeroEngine.ResetAccountNumbers(m_IntegrationDetails)

                Case Else
                    Return New Response("Unhandled Partner.")

            End Select

        Else
            Return _ConnectResponse
        End If

    End Function

    Public Function ReturnContact(ByVal FullName As String, ByVal EmailAddress As String, ByRef EntityIn As Entity) As Response

        Dim _ConnectResponse As Response = Connect()
        If Not _ConnectResponse.HasErrors Then

            Select Case m_IntegrationDetails.Partner

                Case EnumIntegrationPartner.Xero

                    'search by email if we have one
                    If EmailAddress <> "" Then
                        If XeroEngine.ReturnContact(m_IntegrationDetails, "EmailAddress", EmailAddress, EntityIn).Success Then
                            Return New Response
                        Else

                            Threading.Thread.Sleep(500)

                            If XeroEngine.ReturnContact(m_IntegrationDetails, "Name", FullName, EntityIn).Success Then
                                Return New Response
                            Else
                                Return New Response("ReturnContact, unable to find by Name or Email")
                            End If

                        End If

                    Else
                        If XeroEngine.ReturnContact(m_IntegrationDetails, "Name", FullName, EntityIn).Success Then
                            Return New Response
                        Else
                            Return New Response("ReturnContact, unable to find by Name")
                        End If
                    End If

                Case Else
                    Return New Response("ReturnContact only supported by Xero")

            End Select

        Else
            Return _ConnectResponse
        End If

    End Function

    Private Function Connect() As Response

        If m_IntegrationDetails Is Nothing Then Return New Response("Missing Integration Details")

        If Not m_Connected Then

            Select Case m_IntegrationDetails.Partner

                Case EnumIntegrationPartner.ClearBooks
                    m_Connected = ClearBooksEngine.Connect(m_ConnectionDetails, m_APIID, m_APIKey)
                    If m_Connected Then
                        Return New Response
                    Else
                        Return New Response("ClearBooksEngine.Connect - Failed")
                    End If

                Case EnumIntegrationPartner.Xero
                    m_Connected = True
                    Return New Response

                Case EnumIntegrationPartner.Sage
                    m_SageEngine = New SageEngine(m_IntegrationDetails)
                    Dim _Response As Response = m_SageEngine.Connect
                    If Not _Response.HasErrors Then m_Connected = True
                    Return _Response

                Case Else
                    m_Connected = False
                    Return New Response("Unhandled Partner.")

            End Select

        Else
            'already connected
            Return New Response
        End If

    End Function

End Class
