﻿Option Strict On

Public Class SageEngine

    Private m_SageEngine As ISageEngine

    Public Sub New(ByVal IntegrationDetails As IntegrationDetail)

        Select Case IntegrationDetails.SageVersion
            Case "23"
                m_SageEngine = New SageEngine23(IntegrationDetails)
            Case Else
                'default to version 22
                m_SageEngine = New SageEngine22(IntegrationDetails)
        End Select

    End Sub

    Public Function Connect() As Response
        Return m_SageEngine.Connect()
    End Function

    Public Sub Disconnect()
        m_SageEngine.Disconnect()
    End Sub

    Public Function CreateContact(ByRef EntityIn As Entity) As Response
        Return m_SageEngine.CreateContact(EntityIn)
    End Function

    Public Function UpdateContact(ByRef EntityIn As Entity) As Response
        Return m_SageEngine.UpdateContact(EntityIn)
    End Function

    Public Function CreateDocument(ByRef DocumentIn As Document) As Response
        Return m_SageEngine.CreateDocument(DocumentIn)
    End Function

    Public Shared Sub OpenInvoice(ByVal InvoiceID As String)

        Try

            Dim _I As New SDOAPPLib.Invoicing10
            Dim _D As SDOAPPLib.Dialog

            _I.ShowApplication(-1)
            _D = _I.Show(_I.Record)
            _D.Controls.Item(_D.Controls.OrderNo).Text = InvoiceID

        Catch ex As Exception

        End Try

    End Sub

    Public Shared Sub OpenEntity(ByVal EntityID As String)

        Try

            Dim _C As New SDOAPPLib.Customer10
            Dim _D As SDOAPPLib.Dialog

            _C.ShowApplication(-1)
            _D = _C.Show(_C.Record)
            _D.Controls.Item(_D.Controls.AccountNo).Text = EntityID

        Catch ex As Exception

        End Try

    End Sub

End Class
