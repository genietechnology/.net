﻿Option Strict On

Imports SageDataObject230

Public Class SageEngine23

    Implements ISageEngine

    Private m_SagePath As String = ""
    Private m_SageUser As String = ""
    Private m_SagePassword As String = ""

    Private m_SDOEngine As SDOEngine
    Private m_SDOWorkspace As WorkSpace

    Private Const m_WorkSpaceName As String = "NurseryGenie"

    Public Sub New(ByVal IntegrationDetails As IntegrationDetail)
        m_SagePath = IntegrationDetails.SageCompanyPath
        m_SageUser = IntegrationDetails.SageUser
        m_SagePassword = IntegrationDetails.SagePassword
    End Sub

    Public Function Connect() As Response Implements ISageEngine.Connect

        m_SDOEngine = New SDOEngine

        Try
            m_SDOWorkspace = CType(m_SDOEngine.Workspaces.Add(m_WorkSpaceName), WorkSpace)
            If m_SDOWorkspace.Connect(m_SagePath, m_SageUser, m_SagePassword, m_WorkSpaceName) Then
                Return New Response
            Else
                Return New Response("SageEngine.Connect - Returned False")
            End If

        Catch ex As Exception
            Return New Response("SageEngine.Connect - Unhandled Exception", ex)
        End Try

    End Function

    Public Sub Disconnect() Implements ISageEngine.Disconnect

        Try

            m_SDOWorkspace.Disconnect()
            m_SDOWorkspace = Nothing

            m_SDOEngine = Nothing

        Catch ex As Exception

        End Try

    End Sub

    Public Function CreateContact(ByRef EntityIn As Entity) As Response Implements ISageEngine.CreateContact

        Dim _sr As SalesRecord
        _sr = CType(m_SDOWorkspace.CreateObject("SalesRecord"), SalesRecord)

        If _sr.AddNew Then

            WriteField(Of String)(_sr, "ACCOUNT_REF", EntityIn.OurAccountNumber, 8)
            WriteField(Of String)(_sr, "NAME", EntityIn.ContactName, 60)

            WriteField(Of String)(_sr, "ADDRESS_1", EntityIn.Address1, 60)
            WriteField(Of String)(_sr, "ADDRESS_2", EntityIn.Address2, 60)
            WriteField(Of String)(_sr, "ADDRESS_3", EntityIn.Address3, 60)
            WriteField(Of String)(_sr, "ADDRESS_4", EntityIn.Address4, 60)
            WriteField(Of String)(_sr, "ADDRESS_5", EntityIn.Address5, 60)

            Dim _ContactName As String = EntityIn.Forename + " " + EntityIn.Surname
            WriteField(Of String)(_sr, "CONTACT_NAME", _ContactName, 30)

            WriteField(Of String)(_sr, "TELEPHONE", EntityIn.Mobile, 30)
            WriteField(Of String)(_sr, "TELEPHONE_2", EntityIn.Phone, 30)
            WriteField(Of String)(_sr, "E_MAIL", EntityIn.Email, 255)
            WriteField(Of String)(_sr, "FAX", EntityIn.Fax, 30)

            Try

                If _sr.Update Then
                    'with sage, we need to create our own account number as sage won't create one for us
                    EntityIn.ExternalID = EntityIn.OurAccountNumber
                    Return New Response
                Else
                    Return New Response("SalesRecord.Update returned False")
                End If

            Catch ex As Exception
                Return New Response("SalesRecord.Update Unhandled Exception", ex)
            End Try

        Else
            'error creating a new record
            Return New Response("SalesRecord.AddNew returned False")
        End If

    End Function

    Public Function UpdateContact(ByRef EntityIn As Entity) As Response Implements ISageEngine.UpdateContact

        If EntityIn.OurAccountNumber = "" Then Return New Response("EntityIn.OurAccountNumber is blank")

        Dim _sr As SalesRecord = CType(m_SDOWorkspace.CreateObject("SalesRecord"), SalesRecord)
        If FindCustomer(EntityIn.ExternalID, _sr) Then

            If _sr.Edit Then

                WriteField(Of String)(_sr, "NAME", EntityIn.ContactName, 60)

                WriteField(Of String)(_sr, "ADDRESS_1", EntityIn.Address1, 60)
                WriteField(Of String)(_sr, "ADDRESS_2", EntityIn.Address2, 60)
                WriteField(Of String)(_sr, "ADDRESS_3", EntityIn.Address3, 60)
                WriteField(Of String)(_sr, "ADDRESS_4", EntityIn.Address4, 60)
                WriteField(Of String)(_sr, "ADDRESS_5", EntityIn.Address5, 60)

                Dim _ContactName As String = EntityIn.Forename + " " + EntityIn.Surname
                WriteField(Of String)(_sr, "CONTACT_NAME", _ContactName, 30)

                WriteField(Of String)(_sr, "TELEPHONE", EntityIn.Mobile, 30)
                WriteField(Of String)(_sr, "TELEPHONE_2", EntityIn.Phone, 30)
                WriteField(Of String)(_sr, "E_MAIL", EntityIn.Email, 255)
                WriteField(Of String)(_sr, "FAX", EntityIn.Fax, 30)

                Try
                    If _sr.Update Then
                        Return New Response
                    Else
                        Return New Response("UpdateContact - SalesRecord.Update returned False")
                    End If

                Catch ex As Exception
                    Return New Response("UpdateContact - SalesRecord.Update unhandled exception", ex)
                End Try

            Else
                'error editting record
                Return New Response("UpdateContact - SalesRecord.Edit returned False")
            End If

        Else
            'could not find record
            Return New Response("UpdateContact - Unable to find customer")
        End If

    End Function

    Private Function FindCustomer(ByVal CustomerRef As String, ByRef SalesRecordObject As SalesRecord) As Boolean
        SalesRecordObject.Fields.Item("ACCOUNT_REF").Value = CStr(CustomerRef)
        Return SalesRecordObject.Find(False)
    End Function

    Public Function CreateDocument(ByRef DocumentIn As Document) As Response Implements ISageEngine.CreateDocument

        Dim _sr As SalesRecord = CType(m_SDOWorkspace.CreateObject("SalesRecord"), SalesRecord)
        If FindCustomer(DocumentIn.EntityID, _sr) Then

            'create invoice header
            Dim _inv As InvoicePost = CType(m_SDOWorkspace.CreateObject("InvoicePost"), InvoicePost)

            Select Case DocumentIn.DocumentType

                Case Document.EnumDocumentType.Sales
                    _inv.Type = CType(LedgerType.sdoLedgerService, InvoiceType)
                    WriteField(Of Byte)(_inv.Header, "INVOICE_TYPE_CODE", CByte(InvoiceType.sdoServiceInvoice))

                Case Document.EnumDocumentType.SalesCreditNote
                    _inv.Type = CType(LedgerType.sdoLedgerServiceCredit, InvoiceType)
                    WriteField(Of Byte)(_inv.Header, "INVOICE_TYPE_CODE", CByte(InvoiceType.sdoServiceCredit))

                Case Else
                    'not supported yet

            End Select

            WriteField(Of String)(_inv.Header, "ACCOUNT_REF", ReadField(Of String)(_sr, "ACCOUNT_REF"), 8)
            WriteField(Of Date)(_inv.Header, "INVOICE_DATE", DocumentIn.DateCreated)
            WriteField(Of String)(_inv.Header, "CUST_ORDER_NUMBER", DocumentIn.Reference, 60)
            WriteField(Of String)(_inv.Header, "NOTES_1", "Via Nursery Genie Ver " + My.Application.Info.Version.ToString)

            WriteField(Of String)(_inv.Header, "GLOBAL_TAX_CODE", ReadField(Of String)(_sr, "DEF_TAX_CODE"))

            WriteField(Of String)(_inv.Header, "NAME", ReadField(Of String)(_sr, "NAME"), 60)
            WriteField(Of String)(_inv.Header, "ADDRESS_1", ReadField(Of String)(_sr, "ADDRESS_1"), 60)
            WriteField(Of String)(_inv.Header, "ADDRESS_2", ReadField(Of String)(_sr, "ADDRESS_2"), 60)
            WriteField(Of String)(_inv.Header, "ADDRESS_3", ReadField(Of String)(_sr, "ADDRESS_3"), 60)
            WriteField(Of String)(_inv.Header, "ADDRESS_4", ReadField(Of String)(_sr, "ADDRESS_4"), 60)
            WriteField(Of String)(_inv.Header, "ADDRESS_5", ReadField(Of String)(_sr, "ADDRESS_5"), 60)

            WriteField(Of String)(_inv.Header, "DEL_ADDRESS_1", ReadField(Of String)(_sr, "DEL_ADDRESS_1"), 60)
            WriteField(Of String)(_inv.Header, "DEL_ADDRESS_2", ReadField(Of String)(_sr, "DEL_ADDRESS_2"), 60)
            WriteField(Of String)(_inv.Header, "DEL_ADDRESS_3", ReadField(Of String)(_sr, "DEL_ADDRESS_3"), 60)
            WriteField(Of String)(_inv.Header, "DEL_ADDRESS_4", ReadField(Of String)(_sr, "DEL_ADDRESS_4"), 60)
            WriteField(Of String)(_inv.Header, "DEL_ADDRESS_5", ReadField(Of String)(_sr, "DEL_ADDRESS_5"), 60)

            WriteField(Of String)(_inv.Header, "CUST_TEL_NUMBER", ReadField(Of String)(_sr, "TELEPHONE"), 30)
            WriteField(Of String)(_inv.Header, "CONTACT_NAME", ReadField(Of String)(_sr, "CONTACT_NAME"), 30)

            'process lines
            For Each _di As Financials.DocumentItem In DocumentIn.Items

                Dim _Item As InvoiceItem = CType(AddItem(_inv.Items), InvoiceItem)

                Dim _Description As String = Mid(_di.Description, 1, 60)

                _Item.Text = _Description
                WriteField(Of String)(_Item, "DESCRIPTION", _Description, 60)
                WriteField(Of Double)(_Item, "UNIT_PRICE", _di.UnitPrice)
                WriteField(Of Double)(_Item, "NET_AMOUNT", _di.UnitPrice)
                WriteField(Of Double)(_Item, "FULL_NET_AMOUNT", _di.UnitPrice)
                WriteField(Of String)(_Item, "NOMINAL_CODE", _di.NLCode, 8)
                WriteField(Of String)(_Item, "TAX_CODE", ReadField(Of String)(_sr, "DEF_TAX_CODE"), 2)
                WriteField(Of Double)(_Item, "TAX_RATE", 0)

            Next

            Try

                If _inv.Update() Then
                    DocumentIn.ExternalID = ReadField(Of String)(_inv.Header, "INVOICE_NUMBER")
                    Return New Response
                Else
                    Return New Response("CreateDocument - InvoicePost.False returned False")
                End If

            Catch ex As Exception
                Return New Response("CreateDocument - InvoicePost.Update unhandled exception", ex)
            End Try

        Else
            'could not find customer
            Return New Response("CreateDocument - Unable to find customer")
        End If

    End Function

    Private Function AddItem(ByVal ItemCollection As Object) As Object
        Return ItemCollection.GetType.InvokeMember("Add", Reflection.BindingFlags.InvokeMethod, Nothing, ItemCollection, Nothing)
    End Function

    Private Sub WriteField(Of T)(ByVal ObjectIn As Object, ByVal FieldName As String, ByVal FieldValue As T, Optional FieldLength As Integer = 0)

        Dim _Fields As Fields = GetFields(ObjectIn)
        Dim _Field As Field = GetField(_Fields, FieldName)

        If TypeOf (FieldValue) Is String Then

            If FieldValue Is Nothing Then
                _Field.Value = ""
            Else

                Dim _FieldValue As String = FieldValue.ToString.Trim
                If _FieldValue <> "" Then

                    Dim _Length As Integer = 0
                    If FieldLength > 0 Then
                        'if we have supplied a length, we truncate to this
                        _Length = FieldLength
                    Else
                        'otherwise we use the fieldlength from the data definition
                        _Length = _Field.Length
                    End If

                    'do we need to truncate?
                    If _FieldValue.Length > _Length Then
                        _Field.Value = _FieldValue.Substring(0, _Length)
                    Else
                        _Field.Value = _FieldValue
                    End If

                Else
                    _Field.Value = ""
                End If

            End If

        Else
            _Field.Value = FieldValue
        End If

    End Sub

    Private Function ReadField(Of T)(ByVal ObjectIn As Object, ByVal FieldName As String) As T
        Dim _Fields As Fields = GetFields(ObjectIn)
        Dim _Field As Field = GetField(_Fields, FieldName)
        Return CType(_Field.Value, T)
    End Function

    Private Function GetField(FieldsCollection As Fields, ByVal FieldName As String) As Field
        Return FieldsCollection.Item(CType(FieldName, Object))
    End Function

    Private Function GetFields(ByVal ObjectIn As Object) As Fields
        Return CType(ObjectIn.GetType().InvokeMember("Fields", Reflection.BindingFlags.GetProperty, Nothing, ObjectIn, Nothing), Fields)
    End Function

End Class
