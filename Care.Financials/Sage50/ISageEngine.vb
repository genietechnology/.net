﻿Public Interface ISageEngine
    Function Connect() As Response
    Sub Disconnect()
    Function CreateContact(ByRef EntityIn As Entity) As Response
    Function UpdateContact(ByRef EntityIn As Entity) As Response
    Function CreateDocument(ByRef DocumentIn As Document) As Response
End Interface
