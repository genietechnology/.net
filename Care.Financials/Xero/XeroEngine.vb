﻿Public Class XeroEngine

    Public Shared Sub OpenInvoice(ByVal InvoiceID As String)
        Dim _URL As String = "https://go.xero.com/AccountsReceivable/View.aspx?InvoiceID=" + InvoiceID
        Process.Start(_URL)
    End Sub

    Public Shared Sub OpenEntity(ByVal EntityID As String)
        Dim _URL As String = "https://go.xero.com/Contacts/View.aspx?contactID=" + EntityID
        Process.Start(_URL)
    End Sub

    Public Shared Sub OpenStatement(ByVal EntityID As String)
        Dim _URL As String = "https://go.xero.com/AccountsReceivable/ViewStatement.aspx?statementContactID=" + EntityID
        Process.Start(_URL)
    End Sub

    Public Shared Function FindExternalIDByName(ByVal IntegrationDetails As IntegrationDetail, ByVal Name As String) As String

        Dim _Return As String = ""
        Dim _C As New Core(IntegrationDetails)

        Try

            _C.Contacts.ClearQueryString()
            _C.Contacts.Where("Name = " + Chr(34) + Name + Chr(34))

            Dim _Contacts As IEnumerable(Of Xero.Api.Core.Model.Contact) = _C.Contacts.Find()
            If _Contacts IsNot Nothing Then
                If _Contacts.Count = 1 Then
                    _Return = _Contacts.First.Id.ToString
                End If
                _Contacts = Nothing
            End If

            _C = Nothing

        Catch ex As Exception
            _C = Nothing
        End Try

        Return _Return

    End Function

    Public Shared Function GetBalance(ByVal IntegrationDetails As IntegrationDetail, ByVal ExternalID As String, ByRef Balance As Decimal) As Response

        Dim _Return As Boolean = False
        Dim _C As New Core(IntegrationDetails)
        Dim _Contact As Xero.Api.Core.Model.Contact = Nothing

        Try

            _Contact = _C.Contacts.Find(ExternalID)
            Balance = _Contact.Balances.AccountsReceivable.Outstanding

            _Return = True

            _Contact = Nothing
            _C = Nothing

        Catch ex As Exception
            _C = Nothing
            Return New Response("GetBalance", ex)
        End Try

        Return New Response

    End Function

    Public Shared Function UpdateContact(ByVal IntegrationDetails As IntegrationDetail, ByRef EntityIn As Entity) As Response

        Dim _C As New Core(IntegrationDetails)
        Dim _Contact As Xero.Api.Core.Model.Contact = Nothing

        Try
            _Contact = _C.Contacts.Find(EntityIn.ExternalID)

        Catch ex As Exception
            _C = Nothing
            HandleException(EntityIn, ex)
            Return New Response("Update Contact: Find Contact", ex)
        End Try

        With _Contact

            .ContactStatus = Xero.Api.Core.Model.Status.ContactStatus.Active

            .AccountNumber = EntityIn.OurAccountNumber
            .Name = EntityIn.ContactName

            If EntityIn.Forename <> "" Then .FirstName = EntityIn.Forename
            If EntityIn.Surname <> "" Then .LastName = EntityIn.Surname

            Dim _Address As New Xero.Api.Core.Model.Address
            With _Address
                .AttentionTo = EntityIn.ContactName
                .AddressLine1 = EntityIn.Address1
                .AddressLine2 = EntityIn.Address2
                .AddressLine3 = EntityIn.Address3
                .AddressLine4 = EntityIn.Address4
            End With

            Dim _Addresses As New List(Of Xero.Api.Core.Model.Address)
            _Addresses.Add(_Address)
            .Addresses = _Addresses

            Dim _Phones As New List(Of Xero.Api.Core.Model.Phone)

            If EntityIn.Phone <> "" Then
                Dim _Phone As New Xero.Api.Core.Model.Phone
                _Phone.PhoneType = Xero.Api.Core.Model.Types.PhoneType.Default
                _Phone.PhoneNumber = EntityIn.Phone
                _Phones.Add(_Phone)
            End If

            If EntityIn.Mobile <> "" Then
                Dim _Phone As New Xero.Api.Core.Model.Phone
                _Phone.PhoneType = Xero.Api.Core.Model.Types.PhoneType.Mobile
                _Phone.PhoneNumber = EntityIn.Mobile
                _Phones.Add(_Phone)
            End If

            If _Phones.Count > 0 Then .Phones = _Phones

            .EmailAddress = EntityIn.Email

        End With

        Try
            _Contact = _C.Contacts.Update(_Contact)

        Catch ex As Exception
            HandleException(EntityIn, ex)
            _C = Nothing
            Return New Response("Update Contact: Update", ex)
        End Try

        _C = Nothing

        Return New Response

    End Function

    Public Shared Function CreateContact(ByVal IntegrationDetails As IntegrationDetail, ByRef EntityIn As Entity) As Response

        Dim _C As New Core(IntegrationDetails)

        Dim _Contact As New Xero.Api.Core.Model.Contact
        With _Contact

            .IsCustomer = True
            .ContactStatus = Xero.Api.Core.Model.Status.ContactStatus.Active

            .AccountNumber = EntityIn.OurAccountNumber
            .Name = EntityIn.ContactName

            If EntityIn.Forename <> "" Then .FirstName = EntityIn.Forename
            If EntityIn.Surname <> "" Then .LastName = EntityIn.Surname

            Dim _Address As New Xero.Api.Core.Model.Address
            With _Address
                .AttentionTo = EntityIn.ContactName
                .AddressLine1 = EntityIn.Address1
                .AddressLine2 = EntityIn.Address2
                .AddressLine3 = EntityIn.Address3
                .AddressLine4 = EntityIn.Address4
            End With

            Dim _Addresses As New List(Of Xero.Api.Core.Model.Address)
            _Addresses.Add(_Address)
            .Addresses = _Addresses

            Dim _Phones As New List(Of Xero.Api.Core.Model.Phone)

            If EntityIn.Phone <> "" Then
                Dim _Phone As New Xero.Api.Core.Model.Phone
                _Phone.PhoneType = Xero.Api.Core.Model.Types.PhoneType.Default
                _Phone.PhoneNumber = EntityIn.Phone
                _Phones.Add(_Phone)
            End If

            If EntityIn.Mobile <> "" Then
                Dim _Phone As New Xero.Api.Core.Model.Phone
                _Phone.PhoneType = Xero.Api.Core.Model.Types.PhoneType.Mobile
                _Phone.PhoneNumber = EntityIn.Mobile
                _Phones.Add(_Phone)
            End If

            If EntityIn.Fax <> "" Then
                Dim _Phone As New Xero.Api.Core.Model.Phone
                _Phone.PhoneType = Xero.Api.Core.Model.Types.PhoneType.Fax
                _Phone.PhoneNumber = EntityIn.Fax
                _Phones.Add(_Phone)
            End If

            If _Phones.Count > 0 Then .Phones = _Phones

            .EmailAddress = EntityIn.Email

        End With

        Try
            _Contact = _C.Contacts.Create(_Contact)
            EntityIn.ExternalID = _Contact.Id.ToString

        Catch ex As Exception

            HandleException(EntityIn, ex)

            If EntityIn.ExceptionText.Contains("The contact name must be unique across all active contacts") Then

                'if we are dealing with a new contact, lets check if the contact already exists...
                Dim _ID As String = FindExternalIDByName(IntegrationDetails, _Contact.Name)
                If _ID <> "" Then

                    EntityIn.ExternalID = _ID

                    If UpdateContact(IntegrationDetails, EntityIn).Success Then
                        EntityIn.Exception = False
                        EntityIn.ExceptionText = ""
                        Return New Response
                    Else
                        EntityIn.ExternalID = ""
                        Return New Response("Create Contact (Update) Failed", ex)
                    End If

                Else
                    EntityIn.ExternalID = ""
                End If

            Else
                EntityIn.ExternalID = ""
            End If

            _C = Nothing

        End Try

        Return New Response
        _C = Nothing

    End Function

    Public Shared Function ResetAccountNumbers(ByVal IntegrationDetails As IntegrationDetail) As Response

        Dim _C As New Core(IntegrationDetails)

        Try

            Dim _Page As Integer = 0
            Dim _Count As Integer = 100

            While _Count = 100

                _Page += 1

                _C.Contacts.Page(_Page)

                Dim _Contacts As IEnumerable(Of Xero.Api.Core.Model.Contact) = _C.Contacts.Find()
                If _Contacts IsNot Nothing Then

                    _Count = _Contacts.Count

                    For Each _Contact In _Contacts
                        _Contact.AccountNumber = ""
                    Next

                    Try
                        _Contacts = _C.Contacts.Update(_Contacts)

                    Catch ex As Exception
                        Return New Response("ResetAccountNumbers: Contacts.Update", ex)
                    End Try

                Else
                    _Count = 0
                End If

            End While

        Catch ex As Exception
            Return New Response("ResetAccountNumbers", ex)
        End Try

        Return New Response

    End Function

    Public Shared Function ReturnContact(ByVal IntegrationDetails As IntegrationDetail, ByVal SearchField As String, ByVal SearchValue As String, ByRef EntityIn As Entity) As Response

        Dim _C As New Core(IntegrationDetails)

        Try

            _C.Contacts.ClearQueryString()
            _C.Contacts.Where(SearchField + " = " + Chr(34) + SearchValue + Chr(34))

            Dim _Contacts As IEnumerable(Of Xero.Api.Core.Model.Contact) = _C.Contacts.Find()
            If _Contacts IsNot Nothing Then

                If _Contacts.Count = 1 Then

                    Dim _Contact As Xero.Api.Core.Model.Contact = _Contacts.First
                    EntityIn = New Entity
                    With EntityIn

                        .ExternalID = _Contact.Id.ToString
                        .OurAccountNumber = _Contact.AccountNumber

                        If _Contact.Addresses IsNot Nothing AndAlso _Contact.Addresses.Count > 0 Then

                            For Each _Add In _Contact.Addresses

                                If _Add.AddressLine1 <> "" Then
                                    .Address1 = _Contact.Addresses.First.AddressLine1
                                    .Address2 = _Contact.Addresses.First.AddressLine2
                                    .Address3 = _Contact.Addresses.First.AddressLine3
                                    .Address4 = _Contact.Addresses.First.AddressLine4
                                    .Address5 = ""
                                    Exit For
                                End If

                            Next

                        End If

                        .ContactName = _Contact.Name
                        .Forename = _Contact.FirstName
                        .Surname = _Contact.LastName

                        .Email = _Contact.EmailAddress

                        If _Contact.Phones IsNot Nothing Then
                            For Each _P In _Contact.Phones
                                If _P.PhoneNumber <> "" Then
                                    If _P.PhoneType = Xero.Api.Core.Model.Types.PhoneType.Mobile Then
                                        .Mobile = _P.PhoneCountryCode + _P.PhoneAreaCode + _P.PhoneNumber
                                    Else
                                        .Phone = _P.PhoneCountryCode + _P.PhoneAreaCode + _P.PhoneNumber
                                    End If
                                End If
                            Next
                        End If

                        .Website = _Contact.Website

                        .Exception = False
                        .ExceptionText = ""

                    End With

                    _C = Nothing

                End If

                _Contacts = Nothing

            End If

            _C = Nothing

        Catch ex As Exception
            _C = Nothing
            Return New Response("ReturnContact", ex)
        End Try

        Return New Response

    End Function

    Private Shared Sub HandleException(ByRef EntityIn As Entity, ByRef ex As Exception)
        If TypeOf ex Is Xero.Api.Infrastructure.Exceptions.ValidationException Then
            EntityIn.Exception = True
            EntityIn.ExceptionText = CType(ex, Xero.Api.Infrastructure.Exceptions.ValidationException).ValidationErrors.First.Message
        Else
            EntityIn.Exception = True
            EntityIn.ExceptionText = ex.Message
        End If
    End Sub

    Private Shared Sub HandleException(ByRef DocumentIn As Document, ByRef ex As Exception)
        If TypeOf ex Is Xero.Api.Infrastructure.Exceptions.ValidationException Then
            DocumentIn.Exception = True
            DocumentIn.ExceptionText = CType(ex, Xero.Api.Infrastructure.Exceptions.ValidationException).ValidationErrors.First.Message
        Else
            DocumentIn.Exception = True
            DocumentIn.ExceptionText = ex.Message
        End If
    End Sub

    Private Shared Sub HandleException(ByRef DocumentItemIn As DocumentItem, ByRef ex As Exception)
        If TypeOf ex Is Xero.Api.Infrastructure.Exceptions.ValidationException Then
            DocumentItemIn.Exception = True
            DocumentItemIn.ExceptionText = CType(ex, Xero.Api.Infrastructure.Exceptions.ValidationException).ValidationErrors.First.Message
        Else
            DocumentItemIn.Exception = True
            DocumentItemIn.ExceptionText = ex.Message
        End If
    End Sub

    Public Shared Function CreateDocument(ByVal IntegrationDetails As IntegrationDetail, ByRef DocumentIn As Document) As Response

        'ensure the external reference is a guid (mixed ClearBooks and Xero etc)
        If DocumentIn.EntityID.Length <> 36 Then Return New Response("CreateDocument: EntityID <> 36 chars")

        Dim _Return As Boolean = True
        Dim _C As New Core(IntegrationDetails)

        'sometimes we get an error here, sometimes we don't so need to cater for both...

        Dim _Contact As Xero.Api.Core.Model.Contact = Nothing

        Try
            _Contact = _C.Contacts.Find(DocumentIn.EntityID)

        Catch ex As Exception
            Return New Response("CreateDocument: Unable to Find ContactID: " + DocumentIn.EntityID + " - Check contact has not been archived in Xero", ex)
        End Try

        If _Contact Is Nothing Then
            Return New Response("CreateDocument: Unable to Find ContactID: " + DocumentIn.EntityID + " - Check contact has not been archived in Xero")
        Else

            Dim _Items As New List(Of Xero.Api.Core.Model.LineItem)

            Try

                For Each _di As Financials.DocumentItem In DocumentIn.Items

                    Dim _i As New Xero.Api.Core.Model.LineItem
                    With _i

                        .Description = DocumentIn.Description + vbNewLine + _di.Description
                        .Quantity = _di.Quantity

                        If DocumentIn.DocumentType = Document.EnumDocumentType.SalesCreditNote Or DocumentIn.DocumentType = Document.EnumDocumentType.PurchaseCreditNote Then
                            .UnitAmount = _di.UnitPrice * -1
                        Else
                            .UnitAmount = _di.UnitPrice
                        End If

                        .AccountCode = _di.NLCode
                        If _di.NLTrackingCategory <> "" AndAlso _di.NLTracking <> "" Then

                            For Each _o As TrackingOption In IntegrationDetails.TrackingOptions

                                If _o.OptionName = _di.NLTracking Then

                                    Dim _itc As New Xero.Api.Core.Model.ItemTrackingCategory
                                    With _itc
                                        .Id = _o.OptionID
                                        .Name = _di.NLTrackingCategory
                                        .Option = _o.OptionName
                                    End With

                                    .Tracking = New Xero.Api.Core.Model.ItemTracking
                                    .Tracking.Add(_itc)

                                    Exit For

                                End If
                            Next

                        End If

                    End With

                    _Items.Add(_i)

                Next

            Catch ex As Exception
                Return New Response("CreateDocument: Processing Document Lines", ex)
            End Try

            Select Case DocumentIn.DocumentType

                Case Document.EnumDocumentType.Sales, Document.EnumDocumentType.Purchase

                    Dim _Document As New Xero.Api.Core.Model.Invoice

                    If DocumentIn.DocumentType = Document.EnumDocumentType.Sales Then
                        _Document.Type = Xero.Api.Core.Model.Types.InvoiceType.AccountsReceivable
                    Else
                        _Document.Type = Xero.Api.Core.Model.Types.InvoiceType.AccountsPayable
                    End If

                    With _Document
                        .Contact = _Contact
                        .Date = DocumentIn.DateCreated
                        .DueDate = DocumentIn.DateDue
                        .Reference = DocumentIn.Reference
                        .Status = Xero.Api.Core.Model.Status.InvoiceStatus.Authorised
                        .LineItems = _Items
                    End With

                    Try
                        _Document = _C.Invoices.Create(_Document)
                        DocumentIn.ExternalID = _Document.Id.ToString

                    Catch ex As Exception
                        Return New Response("CreateDocument: Invoices.Create", ex)
                    End Try


                Case Document.EnumDocumentType.SalesCreditNote, Document.EnumDocumentType.PurchaseCreditNote

                    Dim _Document As New Xero.Api.Core.Model.CreditNote

                    If DocumentIn.DocumentType = Document.EnumDocumentType.SalesCreditNote Then
                        _Document.Type = Xero.Api.Core.Model.Types.CreditNoteType.AccountsReceivable
                    Else
                        _Document.Type = Xero.Api.Core.Model.Types.CreditNoteType.AccountsPayable
                    End If

                    With _Document
                        .Contact = _Contact
                        .Date = DocumentIn.DateCreated
                        .DueDate = DocumentIn.DateDue
                        .Reference = DocumentIn.Reference
                        .Status = Xero.Api.Core.Model.Status.InvoiceStatus.Authorised
                        .LineItems = _Items
                    End With

                    Try
                        _Document = _C.CreditNotes.Create(_Document)
                        DocumentIn.ExternalID = _Document.Id.ToString

                    Catch ex As Exception
                        Return New Response("CreateDocument: CreditNotes.Create", ex)
                    End Try

            End Select

        End If

        Return New Response

    End Function

    Public Shared Function PopulateTracking(ByVal IntegrationDetails As Care.Financials.IntegrationDetail, ByVal TrackingCategory As String) As List(Of TrackingOption)

        'removed error trapping so any errors are thrown to the global error handler

        Dim _Return As New List(Of TrackingOption)
        Dim _C As New Core(IntegrationDetails)

        Dim _tc As Xero.Api.Core.Model.TrackingCategory
        _tc = _C.TrackingCategories.Find(TrackingCategory)

        If _tc IsNot Nothing Then
            For Each _o As Xero.Api.Core.Model.Option In _tc.Options
                Dim _to As New TrackingOption(_o.Id, _o.Name)
                _Return.Add(_to)
            Next
        End If

        'we return a new blank list if there are no records
        Return _Return

    End Function

End Class
