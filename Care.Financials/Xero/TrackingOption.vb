﻿Public Class TrackingOption

    Public Sub New(ByVal ID As Guid, ByVal Name As String)
        OptionID = ID
        OptionName = Name
    End Sub

    Public Property OptionID As Guid
    Public Property OptionName As String

End Class
