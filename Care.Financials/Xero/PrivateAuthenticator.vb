﻿Imports System.Security.Cryptography.X509Certificates
Imports Xero.Api.Infrastructure.Interfaces
Imports Xero.Api.Infrastructure.OAuth
Imports Xero.Api.Infrastructure.OAuth.Signing

Public Class PrivateAuthenticator

    Implements IAuthenticator

    Private ReadOnly _certificate As X509Certificate2
    Private m_User As IUser

#Region "Constructor"

    Public Sub New(ByVal CertificatePath As String, ByVal CertificatePassword As String)
        _certificate = New X509Certificate2()
        _certificate.Import(CertificatePath, CertificatePassword, X509KeyStorageFlags.MachineKeySet)
    End Sub

    Public Sub New(certificate As X509Certificate2)
        _certificate = certificate
    End Sub

#End Region

#Region "Properties"

    Public Property User() As IUser Implements IAuthenticator.User
        Get
            Return m_User
        End Get
        Set(value As IUser)
            m_User = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Function GetSignature(consumer As IConsumer, user As IUser, uri As Uri, verb As String, consumer1 As IConsumer) As String Implements IAuthenticator.GetSignature

        Dim _token As New Token
        With _token
            .ConsumerKey = consumer.ConsumerKey
            .ConsumerSecret = consumer.ConsumerSecret
            .TokenKey = consumer.ConsumerKey
        End With

        Return New RsaSha1Signer().CreateSignature(_certificate, _token, uri, verb)

    End Function

    Public Function GetToken(consumer As IConsumer, user As IUser) As IToken Implements IAuthenticator.GetToken
        Return Nothing
    End Function

#End Region

End Class

