﻿Imports System.Net
Imports Xero.Api.Core
Imports Xero.Api.Infrastructure.OAuth
Imports Xero.Api.Serialization

Public Class Core

    Inherits XeroCoreApi

    Private Const URI As String = "https://api.xero.com/api.xro/2.0/"
    Private Shared ReadOnly Mapper As New DefaultMapper()

    Public Sub New(ByVal IntegrationDetails As IntegrationDetail)

        MyBase.New(URI, CType(New PrivateAuthenticator(IntegrationDetails.PFXPath, IntegrationDetails.PFXPassword), Xero.Api.Infrastructure.Interfaces.IAuthenticator),
           New Consumer(IntegrationDetails.APIKey, IntegrationDetails.APISecret),
           Nothing,
           Mapper,
           Mapper,
           Nothing)

    End Sub

End Class

