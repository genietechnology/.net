﻿Public Class IntegrationDetail

    Public Enum EnumIntegrationPartner
        ClearBooks
        Xero
        Sage
        Quickbooks
    End Enum

    Public Property Partner As EnumIntegrationPartner

    Public Property APIKey As String = ""
    Public Property APISecret As String = ""

    Public Property PFXPath As String = "" 'Xero Only
    Public Property PFXPassword As String = "" 'Xero Only

    Public Property TrackingOptions As New List(Of TrackingOption)

    Public Property SageCompanyPath As String = ""
    Public Property SageUser As String = ""
    Public Property SagePassword As String = ""
    Public Property SageVersion As String = ""

End Class
