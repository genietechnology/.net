﻿Imports System.Data
Imports System.Data.SqlServerCe
Imports System.Reflection

Public Class DAL

    Public Shared gServerIP As String = ""

    Public Shared gCnLocal As String = "Data Source = " + IO.Path.GetDirectoryName(Application.ExecutablePath) + "\DB.sdf;"
    Public Shared gDataDebugging As Boolean = True

    Public Shared gUpdateURL As String = ""
    Public Shared gIISURL As String = ""
    Public Shared gCnServer As String = ""

    Public Shared Sub SetupGlobals()

        gCnServer = "Data Source=" & gServerIP & ";" & _
                    "Initial Catalog=Nursery;" & _
                    "User ID=nurserytablet;" & _
                    "Password=c@r3s0ftw@r3" & ";"

        gIISURL = "http://" + gServerIP + "/rda/sqlcesa35.dll"
        gUpdateURL = "http://" + gServerIP + "/update"

    End Sub

    Public Shared Function ReturnField(ByVal ConnectionString As String, ByVal TableName As String, _
                                      ByVal FilterField As String, ByVal FilterValue As String, _
                                      ByVal TargetField As String) As String

        Dim strSQL As String
        Dim strReturn As String = ""
        Dim dr As DataRow = Nothing

        strSQL = "select " & TargetField & " from " & TableName & _
                 " where " & FilterField & " = '" & FilterValue & "'"

        dr = ReturnDataRow(ConnectionString, strSQL)

        If Not dr Is Nothing Then
            strReturn = dr.Item(0).ToString
        End If

        dr = Nothing

        Return strReturn

    End Function

    Public Shared Function ReturnParam(ByVal ParameterField As String) As String

        Dim strSQL As String
        Dim strReturn As String = ""
        Dim dr As DataRow = Nothing

        strSQL = "select " & ParameterField & " from mbprmfle"
        dr = ReturnDataRow(DAL.gCnLocal, strSQL)

        If Not dr Is Nothing Then
            strReturn = dr.Item(0).ToString
        End If

        dr = Nothing

        Return strReturn

    End Function

#Region "STANDARD CE DATA FUNCTIONS"

    Public Shared Sub subDisplayDataError(ByVal ErrorTitle As String, ByVal ex As SqlCeException, ByVal ConnectionString As String, ByVal strSQL As String)

        Dim _Mess As String

        _Mess = "Exception: " & ex.Message & vbCrLf & vbCrLf & _
                "Connection String: " & ConnectionString & vbCrLf & vbCrLf & _
                "SQL: " & strSQL

        MsgBox(_Mess, MsgBoxStyle.Critical, ErrorTitle)

        'Dim frm As New frmELLDataError
        'frm.subPassArguments(ErrorTitle, ex, ConnectionString, strSQL)
        'frm.ShowDialog()
        'frm.BringToFront()

        'frm.Dispose()

    End Sub

    Public Shared Function ReturnDataReader(ByVal ConnectionString As String, ByVal strSQL As String) As SqlCeDataReader

        Cursor.Current = Cursors.WaitCursor

        Dim cmd As SqlCeCommand = Nothing
        Dim dr As SqlCeDataReader = Nothing

        cmd = ReturnCommand(ConnectionString, strSQL)

        Try
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)

        Catch ex As SqlCeException
            Call subDisplayDataError("ReturnDataReader", ex, ConnectionString, strSQL)
            dr = Nothing
        End Try

        cmd.Dispose()

        Cursor.Current = Cursors.Default
        Return dr

    End Function

    Public Shared Function ReturnDataAdapter(ByVal ConnectionString As String, ByVal strSQL As String) As SqlCeDataAdapter

        Cursor.Current = Cursors.WaitCursor

        Dim da As SqlCeDataAdapter = Nothing

        Try
            da = New SqlCeDataAdapter(strSQL, ConnectionString)
            With da
                .MissingSchemaAction = MissingSchemaAction.AddWithKey
                .AcceptChangesDuringFill = True
                .AcceptChangesDuringUpdate = True
                .ContinueUpdateOnError = False
            End With

        Catch ex As SqlCeException
            Call subDisplayDataError("ReturnDataAdapter", ex, ConnectionString, strSQL)
            da = Nothing
        End Try

        Cursor.Current = Cursors.Default
        Return da

    End Function

    Public Shared Function ReturnDataTable(ByVal ConnectionString As String, ByVal strSQL As String) As DataTable

        Cursor.Current = Cursors.WaitCursor

        Dim da As SqlCeDataAdapter = Nothing
        Dim dt As New DataTable

        da = ReturnDataAdapter(ConnectionString, strSQL)
        If Not da Is Nothing Then

            Try
                da.Fill(dt)

            Catch ex As SqlCeException
                Call subDisplayDataError("ReturnDataTable", ex, ConnectionString, strSQL)
                dt = Nothing
            End Try

            da.Dispose()
            da = Nothing

        End If

        Cursor.Current = Cursors.Default
        Return dt

    End Function

    Public Shared Function ReturnDataRow(ByVal ConnectionString As String, ByVal strSQL As String) As DataRow

        Cursor.Current = Cursors.WaitCursor

        Dim dr As DataRow = Nothing

        Dim dt As DataTable = ReturnDataTable(ConnectionString, strSQL)
        If Not dt Is Nothing Then

            If dt.Rows.Count = 1 Then dr = dt.Rows(0)

            dt.Dispose()
            dt = Nothing

        End If

        Cursor.Current = Cursors.Default
        Return dr

    End Function

    Public Shared Function ReturnConnection(ByVal ConnectionString As String) As SqlCeConnection

        Cursor.Current = Cursors.WaitCursor

        Dim cn As SqlCeConnection = Nothing

        Try
            cn = New SqlCeConnection
            cn.ConnectionString = ConnectionString
            cn.Open()

        Catch ex As SqlCeException
            Call subDisplayDataError("ReturnConnection", ex, ConnectionString, "")
            cn = Nothing
        End Try

        Cursor.Current = Cursors.Default
        Return cn

    End Function

    Public Shared Function ReturnCommand(ByVal ConnectionString As String, ByVal strSQL As String) As SqlCeCommand

        Cursor.Current = Cursors.WaitCursor

        Dim cn As New SqlCeConnection
        Dim cmd As New SqlCeCommand

        cn = ReturnConnection(ConnectionString)
        If Not cn Is Nothing Then
            cmd.CommandType = CommandType.Text
            cmd.CommandText = strSQL
            cmd.Connection = cn
        End If

        Cursor.Current = Cursors.Default
        Return cmd

    End Function

    Public Shared Function ExecuteCommand(ByVal ConnectionString As String, ByVal strSQL As String) As Boolean

        Dim blnOK As Boolean = True
        Dim cmd As New SqlCeCommand
        cmd = ReturnCommand(ConnectionString, strSQL)

        If Not cmd Is Nothing Then

            Try
                cmd.ExecuteNonQuery()

            Catch ex As SqlCeException
                blnOK = False
                Call subDisplayDataError("ExecuteCommand", ex, ConnectionString, strSQL)

            End Try

        End If

        cmd.Dispose()
        cmd = Nothing

        Return blnOK

    End Function

    Public Shared Function UpdateDB(ByVal UpdateAdapter As SqlCeDataAdapter, ByVal UpdateTable As DataTable) As Boolean

        Dim blnOK As Boolean = True
        Cursor.Current = Cursors.WaitCursor

        Try

            Dim cmdBld As New SqlCeCommandBuilder(UpdateAdapter)
            UpdateAdapter = cmdBld.DataAdapter
            UpdateAdapter.Update(UpdateTable)
            cmdBld.Dispose()

        Catch ex As SqlCeException

            Cursor.Current = Cursors.Default
            blnOK = False
            If gDataDebugging Then
                Call subDisplayDataError("UpdateDB", ex, "", "")
            End If

        End Try

        Cursor.Current = Cursors.Default
        Return blnOK

    End Function

    Public Shared Function GetRowbyID(ByVal TableName As String, ByVal ID As Guid) As DataRow


        Return Nothing

    End Function

    Public Shared Function SaveRecord(ByVal BusinessObject As Object, ByVal TableName As String) As Boolean

        Dim _Fields As String = ""
        Dim _Values As String = ""
        Dim _Value As String = ""

        Dim inputType As Type = BusinessObject.GetType
        Dim typeProperties() As PropertyInfo = inputType.GetProperties
        Dim FieldNameAttribute As FieldNameAttribute
        Dim attributes() As Attribute

        Dim _i As Integer = 1
        Dim _Comma As String = ","
        For Each propInfo As PropertyInfo In typeProperties

            If propInfo.Name = "IsNew" Then Continue For
            If propInfo.Name = "IsDeleted" Then Continue For

            If _i = typeProperties.Length - 2 Then _Comma = ""

            'GET THE FIELD NAME FROM THE FIELDNAME ATTRIBUTE
            attributes = CType(propInfo.GetCustomAttributes(GetType(FieldNameAttribute), True), Attribute())
            If Not IsNothing(attributes) And attributes.Length = 1 Then
                FieldNameAttribute = CType(attributes(0), FieldNameAttribute)
                If Not IsNothing(FieldNameAttribute) And FieldNameAttribute.FieldName.Length > 0 Then
                    _Fields += FieldNameAttribute.FieldName & _Comma
                End If
            End If

            If propInfo.PropertyType.FullName.Contains("Guid") Then
                Dim _GUID As Guid? = CType(propInfo.GetValue(BusinessObject, Nothing), Guid?)
                If _GUID.HasValue Then
                    If _GUID.Value <> Guid.Empty Then
                        _Value = _GUID.ToString
                    Else
                        _Value = "NULL"
                    End If
                Else
                    _Value = "NULL"
                End If
            Else

                If propInfo.PropertyType.FullName.Contains("DateTime") Then
                    If propInfo.GetValue(BusinessObject, Nothing) = Nothing Then
                        _Value = "NULL"
                    Else
                        If Right(Format(propInfo.GetValue(BusinessObject, Nothing), "yyyy-MM-dd hh:mm:ss"), 8) = "12:00:00" Then
                            _Value = Format(propInfo.GetValue(BusinessObject, Nothing), "yyyy-MM-dd")
                        Else
                            _Value = Format(propInfo.GetValue(BusinessObject, Nothing), "yyyy-MM-dd hh:mm:ss")
                        End If
                    End If
                Else

                    If propInfo.GetValue(BusinessObject, Nothing) = Nothing Then
                        _Value = ""
                    Else
                        _Value = propInfo.GetValue(BusinessObject, Nothing).ToString
                    End If

                End If

            End If

            _Values += SortQuotes(_Value) & _Comma
            _i += 1

        Next propInfo

        Dim _SQL = "INSERT INTO " & TableName & _
                   " (" & _Fields & ")" & _
                   " VALUES(" & _Values & ")"

        ExecuteCommand(gCnLocal, _SQL)

    End Function

    Private Shared Function SortQuotes(ByVal StringIn As String) As String
        If StringIn = "NULL" Then
            Return StringIn
        Else
            StringIn = Replace(StringIn, "'", "''")
            Return "'" + StringIn + "'"
        End If
    End Function

#End Region

End Class
