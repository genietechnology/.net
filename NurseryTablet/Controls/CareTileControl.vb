﻿Imports System.ComponentModel

Public Class CareTileControl

    Public Property ItemTileHeight As Integer
    Public Property ItemTileWidth As Integer

    Protected Overrides Function CreateViewInfo() As DevExpress.XtraEditors.TileControlViewInfo
        Return New CustomTileViewInfo(Me)
    End Function

    Public Class CustomTileViewInfo

        Inherits DevExpress.XtraEditors.TileControlViewInfo

        Private Const m_TileHeight As Integer = 100
        Private Const m_TileWidth As Integer = 200

        Public Sub New(TileControl As DevExpress.XtraEditors.TileControl)
            MyBase.New(TileControl)
        End Sub

        Protected Overrides Function GetItemHeight() As Integer
            Return m_TileHeight
        End Function

        Public Overrides Function GetItemSize(itemInfo As DevExpress.XtraEditors.TileItemViewInfo) As Drawing.Size
            Return New Size(m_TileWidth, m_TileHeight)
        End Function

    End Class

End Class
