﻿Imports NurseryTablet.SharedModule
Imports System.ComponentModel

<DefaultEvent("RecordChanged")>
Public Class Scroller

    Public Enum EnumScrollerMode
        Children
        Groups
        Staff
    End Enum

    Public Event RecordChanged(sender As Object, e As RecordChangedArgs)

#Region "Properties"

    <Category("_Setup")>
    Public Property ScrollerMode As EnumScrollerMode

    Public Property ValueID As String

    Public ReadOnly Property Count As Integer
        Get
            Return m_Records.Count
        End Get
    End Property

    Public Property ValueText As String
        Get
            Return be.Text
        End Get
        Set(value As String)
            be.Text = value
        End Set
    End Property

    Public Property SelectedIndex As Integer
        Get
            Return m_SelectedIndex
        End Get
        Set(value As Integer)
            m_SelectedIndex = value
            SetValues()
            SetButtons()
        End Set
    End Property


#End Region

#Region "Variables"

    Private m_Records As New List(Of Pair)
    Private m_Record As Pair
    Private m_SelectedIndex As Integer

#End Region

#Region "Methods"

    Public Sub Clear()
        m_SelectedIndex = -1
        m_Record = Nothing
        ValueID = ""
        ValueText = ""
    End Sub

    Public Sub Populate()

        m_Records.Clear()
        Clear()

        If ScrollerMode = EnumScrollerMode.Groups Then PopulateGroups()
        If ScrollerMode = EnumScrollerMode.Staff Then PopulateStaff()

        If m_Records.Count > 0 Then m_SelectedIndex = 1

        SetButtons()
        SetValues()

    End Sub

    Public Sub SelectRecord(ByVal ID As String)
        If NavigateToRecordByID(ID) Then
            SetButtons()
            SetValues()
        Else
            Clear()
        End If
    End Sub

    Public Sub SelectRecordByName(ByVal Name As String)
        If NavigateToRecordByName(Name) Then
            SetButtons()
            SetValues()
        Else
            Clear()
        End If
    End Sub

    Public Sub PopulateChildren(ByVal Group As String)

        m_Records.Clear()
        Clear()

        If Group Is Nothing Then Exit Sub
        If Group = "" Then Exit Sub

        Dim _Children As List(Of Pair) = Business.Child.GetCheckedInPairs(Group)
        If _Children IsNot Nothing Then
            m_Records = _Children
        End If

        If m_Records.Count > 0 Then m_SelectedIndex = 1

        SetButtons()
        SetValues()

    End Sub

#End Region

    Private Sub PopulateGroups()
        m_Records = Business.Rooms.GetRoomsAsPair
    End Sub

    Private Sub PopulateStaff()
        For Each _S In Business.Staff.GetCheckedIn
            m_Records.Add(New Pair(_S.ID.ToString, _S.Name))
        Next
    End Sub

    Private Sub SetButtons()

        be.Properties.Buttons(2).Enabled = False

        If m_Records.Count = 0 Then
            be.Properties.Buttons(0).Enabled = False
            be.Properties.Buttons(1).Enabled = False
            be.Properties.Buttons(3).Enabled = False
        Else

            If m_Records.Count = 1 Then
                be.Properties.Buttons(3).Enabled = False
            Else
                be.Properties.Buttons(3).Enabled = True
            End If

            If m_SelectedIndex = 1 Then
                be.Properties.Buttons(0).Enabled = False
            Else
                be.Properties.Buttons(0).Enabled = True
            End If

            If m_Records.Count > m_SelectedIndex Then
                be.Properties.Buttons(1).Enabled = True
            Else
                be.Properties.Buttons(1).Enabled = False
            End If

        End If

    End Sub

    Private Sub SetValues()

        If m_Records.Count > 0 Then
            m_Record = m_Records(m_SelectedIndex - 1)
            ValueID = m_Record.Code
            ValueText = m_Record.Text
        End If

        RaiseEvent RecordChanged(Me, New RecordChangedArgs(ValueID, ValueText))

    End Sub

    Private Sub be_ButtonClick(sender As Object, e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles be.ButtonClick

        Select Case e.Button.Caption

            Case "Left"
                m_SelectedIndex -= 1

            Case "Right"
                m_SelectedIndex += 1

            Case "Search"
                DoSearch()

            Case Else
                Exit Sub

        End Select

        SetButtons()
        SetValues()

    End Sub

    Private Sub DoSearch()

        Dim _Selected As Pair = ReturnButtons(m_Records, "Select Item")
        If Not _Selected Is Nothing AndAlso _Selected.Code <> "" Then
            NavigateToRecordByID(_Selected.Code)
        End If

    End Sub

    Private Function NavigateToRecordByID(ByVal ID As String) As Boolean

        'set the current list index depending on what was selected
        Dim _Index As Integer = 1
        For Each _p As Pair In m_Records
            If _p.Code = ID Then
                m_SelectedIndex = _Index
                Return True
            End If
            _Index += 1
        Next

        m_SelectedIndex = -1
        Return False

    End Function

    Private Function NavigateToRecordByName(ByVal Name As String) As Boolean

        'set the current list index depending on what was selected
        Dim _Index As Integer = 1
        For Each _p As Pair In m_Records
            If _p.Text = Name Then
                m_SelectedIndex = _Index
                Return True
            End If
            _Index += 1
        Next

        m_SelectedIndex = -1
        Return False

    End Function

    Private Sub be_EditValueChanged(sender As Object, e As EventArgs) Handles be.EditValueChanged

    End Sub
End Class
