﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ItemControl
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblItem = New DevExpress.XtraEditors.LabelControl()
        Me.btnDelete = New DevExpress.XtraEditors.SimpleButton()
        Me.btnTime = New DevExpress.XtraEditors.SimpleButton()
        Me.SuspendLayout()
        '
        'lblItem
        '
        Me.lblItem.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblItem.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblItem.Location = New System.Drawing.Point(95, 3)
        Me.lblItem.Name = "lblItem"
        Me.lblItem.Size = New System.Drawing.Size(138, 40)
        Me.lblItem.TabIndex = 0
        Me.lblItem.Text = "Item x"
        '
        'btnDelete
        '
        Me.btnDelete.Image = Global.NurseryTablet.My.Resources.Resources.delete_32
        Me.btnDelete.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnDelete.Location = New System.Drawing.Point(3, 3)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(40, 40)
        Me.btnDelete.TabIndex = 9
        '
        'btnTime
        '
        Me.btnTime.Image = Global.NurseryTablet.My.Resources.Resources.alarm_32
        Me.btnTime.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnTime.Location = New System.Drawing.Point(49, 3)
        Me.btnTime.Name = "btnTime"
        Me.btnTime.Size = New System.Drawing.Size(40, 40)
        Me.btnTime.TabIndex = 8
        '
        'ItemControl
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnTime)
        Me.Controls.Add(Me.lblItem)
        Me.Name = "ItemControl"
        Me.Size = New System.Drawing.Size(236, 46)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblItem As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnDelete As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnTime As DevExpress.XtraEditors.SimpleButton

End Class
