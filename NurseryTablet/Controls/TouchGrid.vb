﻿Imports System.ComponentModel

<DefaultEvent("GridDoubleClick")>
Public Class TouchGrid

#Region "Variables"
    Private m_HideFirstColumn As Boolean = False
    Private m_Populated As Boolean = False
#End Region

#Region "Public Events"
    Public Event GridDoubleClick(sender As Object, e As System.EventArgs)
    Public Event AfterPopulate(sender As Object, e As System.EventArgs)
    Public Event FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs)
    Public Event RowCellStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs)
    Public Event RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs)
#End Region

#Region "Properties"

    <Category("_CareAttributes")>
    Public Property HideFirstColumn As Boolean
        Get
            Return m_HideFirstColumn
        End Get
        Set(value As Boolean)
            m_HideFirstColumn = value
        End Set
    End Property

    <BrowsableAttribute(False)> _
    Public ReadOnly Property Row(ByVal RowHandle As Integer) As Object
        Get
            Return GridView1.GetRow(RowHandle)
        End Get
    End Property

    <BrowsableAttribute(False)> _
    Public ReadOnly Property CurrentRow As Object
        Get
            Return GridView1.GetRow(GridView1.FocusedRowHandle)
        End Get
    End Property

    <BrowsableAttribute(False)> _
    Public ReadOnly Property Columns As DevExpress.XtraGrid.Columns.GridColumnCollection
        Get
            Return GridView1.Columns
        End Get
    End Property

    <BrowsableAttribute(False)> _
    Public ReadOnly Property RowIndex As Integer
        Get
            Return GridView1.GetFocusedDataSourceRowIndex
        End Get
    End Property

    <BrowsableAttribute(False)> _
    Public ReadOnly Property RecordCount As Integer
        Get
            Return GridView1.RowCount
        End Get
    End Property

#End Region

#Region "Public Methods"

    Public Sub Clear()
        grd.DataSource = Nothing
        GridView1.Columns.Clear()
    End Sub

    Public Sub MoveFirst()
        GridView1.MoveFirst()
        GridView1.FocusedRowHandle = 0
        GridView1.Focus()
        System.Windows.Forms.Application.DoEvents()
    End Sub

    Public Sub SetHeadings(ByVal ColumnsToDisplay As String, ByVal Headings As String)

        If Headings = "" Then Exit Sub
        Dim _Headings As String() = Headings.Split("|")

        Dim _i As Integer = 0
        For Each _h In _Headings
            GridView1.Columns(_i).Caption = _h
            _i += 1
        Next

    End Sub

    Public Sub Populate(ByVal DataIn As IEnumerable)
        grd.DataSource = DataIn
        Population()
    End Sub

    Public Function GetRowObject(ByVal RowHandle As Integer) As Object
        Return GridView1.GetRow(GridView1.FocusedRowHandle)
    End Function

    Public Function GetRowCellValue(ByVal RowHandle As Integer, ByVal FieldName As String) As Object
        Return GridView1.GetRowCellValue(RowHandle, FieldName)
    End Function

    Public Function GetRowCellValue(ByVal RowHandle As Integer, ByVal ColumnIndex As Integer) As Object
        Return GridView1.GetRowCellValue(RowHandle, GridView1.Columns(ColumnIndex))
    End Function

    Private Sub GridView1_DoubleClick(sender As Object, e As EventArgs) Handles GridView1.DoubleClick
        RaiseEvent GridDoubleClick(sender, e)
    End Sub

    Private Sub GridView1_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridView1.FocusedRowChanged
        RaiseEvent FocusedRowChanged(sender, e)
    End Sub

#End Region

    Private Sub Population()

        If GridView1.Columns.Count > 0 Then
            If m_HideFirstColumn Then
                GridView1.Columns(0).Visible = False
            End If
        End If

        For Each _c As DevExpress.XtraGrid.Columns.GridColumn In GridView1.Columns
            If _c.ColumnType.FullName = "System.DateTime" Then
                _c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
                _c.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm"
            End If
        Next

        GridView1.BestFitColumns()

        m_Populated = True
        MoveFirst()
        RaiseEvent AfterPopulate(Nothing, Nothing)

    End Sub

    Private Sub GridView1_RowCellStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs) Handles GridView1.RowCellStyle
        RaiseEvent RowCellStyle(sender, e)
    End Sub

    Private Sub GridView1_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles GridView1.RowStyle
        RaiseEvent RowStyle(sender, e)
    End Sub
End Class
