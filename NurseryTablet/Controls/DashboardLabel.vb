﻿Option Strict On
Imports System.ComponentModel

<DefaultEvent("DoubleClick")>
Public Class DashboardLabel

    Public Enum EnumColourCode
        Green
        Amber
        Red
        Neutral
    End Enum

    Public Enum EnumMiddleFormat
        WholeNumber
        TwoDecimalPlaces
        Percentage
    End Enum

    Private m_ColourCode As EnumColourCode = EnumColourCode.Neutral
    Private m_Value As Decimal = 0

    Private m_Red As Drawing.Color = Drawing.Color.Black
    Private m_Amber As Drawing.Color = Drawing.Color.Black
    Private m_Green As Drawing.Color = Drawing.Color.Black

    Public Shadows Event Click(sender As Object, e As DashboardEventArgs)
    Public Shadows Event DoubleClick(sender As Object, e As DashboardEventArgs)

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        'save the colour set in the designer
        m_Red = lblTop.BackColor
        m_Amber = lblMiddle.BackColor
        m_Green = lblBottom.BackColor

        lblTop.ResetBackColor()
        lblMiddle.ResetBackColor()
        lblBottom.ResetBackColor()

    End Sub

#Region "Properties"

    Public ReadOnly Property Value As Decimal
        Get
            Return m_Value
        End Get
    End Property

    <Category("_CareAttributes")>
    Public Property LabelTop As String
        Get
            Return lblTop.Text
        End Get
        Set(value As String)
            lblTop.Text = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property LabelMiddle As String
        Get
            Return lblMiddle.Text
        End Get
        Set(value As String)
            lblMiddle.Text = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property LabelBottom As String
        Get
            Return lblBottom.Text
        End Get
        Set(value As String)
            lblBottom.Text = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property Colour As EnumColourCode
        Get
            Return m_ColourCode
        End Get
        Set(value As EnumColourCode)
            m_ColourCode = value
            SetColour()
        End Set
    End Property

#End Region

#Region "Methods"

    Public Sub Reset()
        lblTop.Text = ""
        lblMiddle.Text = ""
        lblBottom.Text = ""
        m_ColourCode = EnumColourCode.Neutral
        SetColour()
    End Sub

    Public Sub SetValue(ByVal DecimalIn As Decimal, FormatRequired As EnumMiddleFormat, ByVal ColourCode As EnumColourCode)

        Select Case FormatRequired

            Case EnumMiddleFormat.WholeNumber
                lblMiddle.Text = Format(DecimalIn, "0").ToString

            Case EnumMiddleFormat.Percentage
                lblMiddle.Text = Format(DecimalIn, "0").ToString + "%"

            Case EnumMiddleFormat.TwoDecimalPlaces
                lblMiddle.Text = Format(DecimalIn, "0.00").ToString

        End Select

        m_ColourCode = ColourCode
        m_Value = DecimalIn
        SetColour()

    End Sub

#End Region

    Private Sub SetColour()

        Select Case m_ColourCode

            Case EnumColourCode.Neutral
                Me.ResetBackColor()
                Me.ResetForeColor()

            Case EnumColourCode.Green
                Me.BackColor = m_Green

            Case EnumColourCode.Amber
                Me.BackColor = m_Amber

            Case EnumColourCode.Red
                Me.BackColor = m_Red

        End Select

    End Sub

    Private Sub lblTop_DoubleClick(sender As Object, e As EventArgs) Handles lblTop.DoubleClick
        RaiseEvent DoubleClick(sender, New DashboardEventArgs(m_Value, lblTop.Text, lblMiddle.Text, lblBottom.Text))
    End Sub

    Private Sub lblMiddle_DoubleClick(sender As Object, e As EventArgs) Handles lblMiddle.DoubleClick
        RaiseEvent DoubleClick(sender, New DashboardEventArgs(m_Value, lblTop.Text, lblMiddle.Text, lblBottom.Text))
    End Sub

    Private Sub lblBottom_DoubleClick(sender As Object, e As EventArgs) Handles lblBottom.DoubleClick
        RaiseEvent DoubleClick(sender, New DashboardEventArgs(m_Value, lblTop.Text, lblMiddle.Text, lblBottom.Text))
    End Sub

    Private Sub lblTop_Click(sender As Object, e As EventArgs) Handles lblTop.Click
        RaiseEvent Click(sender, New DashboardEventArgs(m_Value, lblTop.Text, lblMiddle.Text, lblBottom.Text))
    End Sub

    Private Sub lblMiddle_Click(sender As Object, e As EventArgs) Handles lblMiddle.Click
        RaiseEvent Click(sender, New DashboardEventArgs(m_Value, lblTop.Text, lblMiddle.Text, lblBottom.Text))
    End Sub

    Private Sub lblBottom_Click(sender As Object, e As EventArgs) Handles lblBottom.Click
        RaiseEvent Click(sender, New DashboardEventArgs(m_Value, lblTop.Text, lblMiddle.Text, lblBottom.Text))
    End Sub

    Public Class DashboardEventArgs

        Inherits EventArgs

        Public Property Value As Decimal
        Public TopLabel As String
        Public MiddleLabel As String
        Public BottomLabel As String

        Public Sub New(ByVal ValueIn As Decimal, ByVal TopIn As String, ByVal MiddleIn As String, ByVal BottomIn As String)
            Value = ValueIn
            TopLabel = TopIn
            MiddleLabel = MiddleIn
            BottomLabel = BottomIn
        End Sub

    End Class

End Class
