﻿Public Class ToiletControl

    Private m_FontStandard As Font
    Private m_FontStrikeout As Font

    Public Sub New(ByVal ChildID As String, ByVal ChildName As String, ByVal Nappies As Boolean, _
                   ByVal AllergyRating As String, ByVal MedAllergies As String, ByVal MedMedicine As String, ByVal MedNotes As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_ActivityID = ActivityID
        m_ChildID = ChildID
        m_ChildName = ChildName
        m_Nappies = Nappies
        m_AllergyRating = AllergyRating
        m_MedAllergies = MedAllergies
        m_MedMedicine = MedMedicine
        m_MedNotes = MedNotes

        lblName.Text = ChildName

        If Parameters.DisablePotty Then btnPotty.Hide()

        If m_Nappies Then
            Nappy()
        Else
            Toilet()
        End If

        m_FontStandard = btnDry.Font
        m_FontStrikeout = New Font(m_FontStandard, FontStyle.Strikeout)

    End Sub

#Region "Variables"

    Private m_ActivityID As String = ""
    Private m_ChildID As String = ""
    Private m_ChildName As String = ""
    Private m_Nappies As Boolean = False
    Private m_AllergyRating As String = ""
    Private m_MedAllergies As String = ""
    Private m_MedMedicine As String = ""
    Private m_MedNotes As String = ""

    Private m_ToiletValue As String = ""
    Private m_ToiletType As String = ""

    Private m_CreamApplied As Boolean = False
    Private m_Deleted As Boolean = False

    Private m_LightYellow As Color = Drawing.Color.FromArgb(255, 255, 192)
    Private m_LightOrange As Color = Drawing.Color.FromArgb(255, 224, 192)
    Private m_LightRed As Color = Drawing.Color.FromArgb(255, 192, 192)

    Private m_Yellow As Color = Drawing.Color.FromArgb(255, 255, 128)
    Private m_Orange As Color = Drawing.Color.FromArgb(255, 192, 128)
    Private m_Red As Color = Drawing.Color.FromArgb(255, 128, 128)

#End Region

#Region "Properties"

    Public ReadOnly Property ActivityID As String
        Get
            Return m_ActivityID
        End Get
    End Property

    Public ReadOnly Property ChildID As String
        Get
            Return m_ChildID
        End Get
    End Property

    Public ReadOnly Property ChildName As String
        Get
            Return m_ChildName
        End Get
    End Property

    Public ReadOnly Property ToiletValue As String
        Get
            Return m_ToiletValue
        End Get
    End Property

    Public ReadOnly Property ToiletType As String
        Get
            Return m_ToiletType
        End Get
    End Property

    Public ReadOnly Property CreamApplied As Boolean
        Get
            Return m_CreamApplied
        End Get
    End Property

    Public ReadOnly Property IsDeleted As Boolean
        Get
            Return m_Deleted
        End Get
    End Property

#End Region

#Region "Button Click Implementation"

    Private Sub Nappy()
        m_ToiletType = "Nappy"
        btnNappy.Appearance.BackColor = m_Yellow
        btnToilet.Appearance.BackColor = m_LightYellow
        btnPotty.Appearance.BackColor = m_LightYellow
    End Sub

    Private Sub Toilet()
        m_ToiletType = "Toilet"
        btnNappy.Appearance.BackColor = m_LightYellow
        btnToilet.Appearance.BackColor = m_Yellow
        btnPotty.Appearance.BackColor = m_LightYellow
    End Sub

    Private Sub Potty()
        m_ToiletType = "Potty"
        btnNappy.Appearance.BackColor = m_LightYellow
        btnToilet.Appearance.BackColor = m_LightYellow
        btnPotty.Appearance.BackColor = m_Yellow
    End Sub

    Private Sub Dry()
        m_ToiletValue = "Dry"
        btnDry.Appearance.BackColor = m_Orange
        btnWet.Appearance.BackColor = m_LightOrange
        btnSoiled.Appearance.BackColor = m_LightOrange
        btnLoose.Appearance.BackColor = m_LightOrange
    End Sub

    Private Sub Wet()
        m_ToiletValue = "Wet"
        btnDry.Appearance.BackColor = m_LightOrange
        btnWet.Appearance.BackColor = m_Orange
        btnSoiled.Appearance.BackColor = m_LightOrange
        btnLoose.Appearance.BackColor = m_LightOrange
    End Sub

    Private Sub Soiled()
        m_ToiletValue = "Soiled"
        btnDry.Appearance.BackColor = m_LightOrange
        btnWet.Appearance.BackColor = m_LightOrange
        btnSoiled.Appearance.BackColor = m_Orange
        btnLoose.Appearance.BackColor = m_LightOrange
    End Sub

    Private Sub Loose()
        m_ToiletValue = "Loose"
        btnDry.Appearance.BackColor = m_LightOrange
        btnWet.Appearance.BackColor = m_LightOrange
        btnSoiled.Appearance.BackColor = m_LightOrange
        btnLoose.Appearance.BackColor = m_Orange
    End Sub

    Private Sub Cream()
        If m_CreamApplied Then
            m_CreamApplied = False
            btnCream.Appearance.BackColor = m_LightRed
        Else
            m_CreamApplied = True
            btnCream.Appearance.BackColor = m_Red
        End If
    End Sub

#End Region

#Region "Button Clicks"

    Private Sub btnNappy_Click(sender As Object, e As EventArgs) Handles btnNappy.Click
        Nappy()
    End Sub

    Private Sub btnToilet_Click(sender As Object, e As EventArgs) Handles btnToilet.Click
        Toilet()
    End Sub

    Private Sub btnPotty_Click(sender As Object, e As EventArgs) Handles btnPotty.Click
        Potty()
    End Sub

    Private Sub btnDry_Click(sender As Object, e As EventArgs) Handles btnDry.Click
        Dry()
    End Sub

    Private Sub btnWet_Click(sender As Object, e As EventArgs) Handles btnWet.Click
        Wet()
    End Sub

    Private Sub btnSoiled_Click(sender As Object, e As EventArgs) Handles btnSoiled.Click
        Soiled()
    End Sub

    Private Sub btnLoose_Click(sender As Object, e As EventArgs) Handles btnLoose.Click
        Loose()
    End Sub

    Private Sub btnCream_Click(sender As Object, e As EventArgs) Handles btnCream.Click
        Cream()
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If Msgbox("Are you sure you want to remove this Child from the Toilet Trip?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Remove Child?") = DialogResult.Yes Then
            m_Deleted = True
            lblName.Font = m_FontStrikeout
        End If
    End Sub

#End Region

End Class
