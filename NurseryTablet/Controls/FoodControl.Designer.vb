﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FoodControl
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblItem = New DevExpress.XtraEditors.LabelControl()
        Me.btnNone = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSome = New DevExpress.XtraEditors.SimpleButton()
        Me.btnHalf = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMost = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAll = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSwap = New DevExpress.XtraEditors.SimpleButton()
        Me.btnDelete = New DevExpress.XtraEditors.SimpleButton()
        Me.SuspendLayout()
        '
        'lblItem
        '
        Me.lblItem.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblItem.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblItem.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblItem.Location = New System.Drawing.Point(3, 3)
        Me.lblItem.Name = "lblItem"
        Me.lblItem.Size = New System.Drawing.Size(130, 36)
        Me.lblItem.TabIndex = 0
        Me.lblItem.Text = "Food Item x"
        '
        'btnNone
        '
        Me.btnNone.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNone.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btnNone.Appearance.Options.UseBackColor = True
        Me.btnNone.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnNone.Location = New System.Drawing.Point(223, 3)
        Me.btnNone.Name = "btnNone"
        Me.btnNone.Size = New System.Drawing.Size(60, 36)
        Me.btnNone.TabIndex = 1
        Me.btnNone.Text = "None"
        '
        'btnSome
        '
        Me.btnSome.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSome.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btnSome.Appearance.Options.UseBackColor = True
        Me.btnSome.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnSome.Location = New System.Drawing.Point(289, 3)
        Me.btnSome.Name = "btnSome"
        Me.btnSome.Size = New System.Drawing.Size(60, 36)
        Me.btnSome.TabIndex = 2
        Me.btnSome.Text = "Some"
        '
        'btnHalf
        '
        Me.btnHalf.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnHalf.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btnHalf.Appearance.Options.UseBackColor = True
        Me.btnHalf.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnHalf.Location = New System.Drawing.Point(355, 3)
        Me.btnHalf.Name = "btnHalf"
        Me.btnHalf.Size = New System.Drawing.Size(60, 36)
        Me.btnHalf.TabIndex = 3
        Me.btnHalf.Text = "Half"
        '
        'btnMost
        '
        Me.btnMost.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMost.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnMost.Appearance.Options.UseBackColor = True
        Me.btnMost.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnMost.Location = New System.Drawing.Point(421, 3)
        Me.btnMost.Name = "btnMost"
        Me.btnMost.Size = New System.Drawing.Size(60, 36)
        Me.btnMost.TabIndex = 4
        Me.btnMost.Text = "Most"
        '
        'btnAll
        '
        Me.btnAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAll.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btnAll.Appearance.Options.UseBackColor = True
        Me.btnAll.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnAll.Location = New System.Drawing.Point(487, 3)
        Me.btnAll.Name = "btnAll"
        Me.btnAll.Size = New System.Drawing.Size(60, 36)
        Me.btnAll.TabIndex = 5
        Me.btnAll.Text = "All"
        '
        'btnSwap
        '
        Me.btnSwap.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSwap.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnSwap.Image = Global.NurseryTablet.My.Resources.Resources.swap_32
        Me.btnSwap.Location = New System.Drawing.Point(181, 3)
        Me.btnSwap.Name = "btnSwap"
        Me.btnSwap.Size = New System.Drawing.Size(36, 36)
        Me.btnSwap.TabIndex = 6
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnDelete.Image = Global.NurseryTablet.My.Resources.Resources.delete_32
        Me.btnDelete.Location = New System.Drawing.Point(139, 3)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(36, 36)
        Me.btnDelete.TabIndex = 7
        '
        'FoodControl
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnSwap)
        Me.Controls.Add(Me.btnAll)
        Me.Controls.Add(Me.btnMost)
        Me.Controls.Add(Me.btnHalf)
        Me.Controls.Add(Me.btnSome)
        Me.Controls.Add(Me.btnNone)
        Me.Controls.Add(Me.lblItem)
        Me.Name = "FoodControl"
        Me.Size = New System.Drawing.Size(550, 42)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblItem As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnNone As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSome As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnHalf As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMost As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAll As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSwap As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnDelete As DevExpress.XtraEditors.SimpleButton

End Class
