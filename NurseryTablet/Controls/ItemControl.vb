﻿Public Class ItemControl

    Private m_StrikeOut As Font
    Private m_Normal As Font

    Private m_ActivityID As String = ""
    Private m_Time As String = ""
    Private m_IsDeleted As Boolean = False

    Public Event DeleteClick(sender As Object, e As EventArgs)
    Public Event TimeClick(sender As Object, e As EventArgs)

    Public Sub New(ByVal ActivityID As String, ByVal ItemText As String, ByVal ChangeTime As Boolean, ByVal Delete As Boolean)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_ActivityID = ActivityID

        lblItem.Text = ItemText

        btnTime.Enabled = ChangeTime
        btnDelete.Enabled = Delete

        m_Normal = lblItem.Font
        m_StrikeOut = New Font(lblItem.Font.Name, lblItem.Font.Size, FontStyle.Strikeout)

    End Sub

    Public ReadOnly Property ActivtyID As String
        Get
            Return m_ActivityID
        End Get
    End Property

    Public ReadOnly Property IsDeleted As Boolean
        Get
            Return m_IsDeleted
        End Get
    End Property

    Public ReadOnly Property ItemText As String
        Get
            Return lblItem.Text
        End Get
    End Property

    Public ReadOnly Property Time As String
        Get
            Return m_Time
        End Get
    End Property

    Public Property ChildID As String
    Public Property Value1 As String
    Public Property Value2 As String
    Public Property Value3 As String
    Public Property Stamp As Date?
    Public Property Notes As String

    Private Sub btnTime_Click(sender As Object, e As EventArgs) Handles btnTime.Click
        m_Time = SharedModule.TimeEntry("Please enter a new time.")
        If m_Time <> "" Then RaiseEvent TimeClick(Me, New EventArgs)
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        RaiseEvent DeleteClick(Me, New EventArgs)
    End Sub
End Class
