﻿Partial Class SharedModule

    Public Class Pair

        Private _Code As String
        Private _Text As String

        Public Sub New()
            _Code = ""
            _Text = ""
        End Sub

        Public Sub New(ByVal CodeValue As String, ByVal TextValue As String)
            _Code = CodeValue
            _Text = TextValue
        End Sub

        Public Property Code() As String
            Get
                Return _Code
            End Get
            Set(ByVal value As String)
                _Code = value
            End Set
        End Property

        Public Property Text() As String
            Get
                Return _Text
            End Get
            Set(ByVal value As String)
                _Text = value
            End Set
        End Property

    End Class

End Class

