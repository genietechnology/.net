﻿Public Class MediaContent

    Public Property KeyType As MediaHandler.EnumCaptureCategory
    Public Property KeyID As String
    Public Property DeviceName As String
    Public Property FilePath As String
    Public Property FileSize As Long
    Public Property FileExt As String

    Private m_Data As Byte() = Nothing
    Private m_Image As Drawing.Image = Nothing

    Public Property Data As Byte()
        Get
            Return m_Data
        End Get
        Set(value As Byte())
            m_Data = value
            SetImage()
        End Set
    End Property

    Public ReadOnly Property Image As Drawing.Image
        Get
            Return m_Image
        End Get
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal MediaKeyType As MediaHandler.EnumCaptureCategory, ByVal MediaKeyID As String, ByVal File As FileDetail)

        KeyType = MediaKeyType
        KeyID = MediaKeyID

        PopulateFromFileDetail(File)

    End Sub

    Private Sub PopulateFromFileDetail(ByVal File As FileDetail)

        Data = MediaHandler.ReturnByteArray(File.FullPath)
        SetImage()

        DeviceName = My.Computer.Name
        FilePath = File.FullPath
        FileExt = File.Extension
        FileSize = File.Size

    End Sub

    Private Sub SetImage()
        If Data Is Nothing Then
            m_Image = Nothing
        Else
            m_Image = MediaHandler.ReturnImage(Data)
        End If
    End Sub

End Class
