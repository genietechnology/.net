﻿Public Class RecordChangedArgs

    Inherits System.EventArgs

    Public Property ValueID As String
    Public Property ValueText As String

    Public Sub New(ByVal ValueIDIn As String, ByVal ValueTextIn As String)
        Me.ValueID = ValueIDIn
        Me.ValueText = ValueTextIn
    End Sub

End Class
