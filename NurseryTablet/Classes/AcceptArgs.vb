﻿Public Class AcceptArgs

    Inherits System.EventArgs

    Public Property Enabled As Boolean

    Public Sub New(ByVal EnabledIn As Boolean)
        Me.Enabled = EnabledIn
    End Sub

End Class
