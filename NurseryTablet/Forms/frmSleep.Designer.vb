﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSleep
    Inherits NurseryTablet.frmBaseChild

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSleep))
        Me.btnSleep4 = New System.Windows.Forms.Button()
        Me.btnSleep3 = New System.Windows.Forms.Button()
        Me.btnSleep2 = New System.Windows.Forms.Button()
        Me.btnSleep1 = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnStartH19 = New System.Windows.Forms.Button()
        Me.btnStartH18 = New System.Windows.Forms.Button()
        Me.btnStartH17 = New System.Windows.Forms.Button()
        Me.btnStartH8 = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnStartM55 = New System.Windows.Forms.Button()
        Me.btnStartM50 = New System.Windows.Forms.Button()
        Me.btnStartM45 = New System.Windows.Forms.Button()
        Me.btnStartM40 = New System.Windows.Forms.Button()
        Me.btnStartM35 = New System.Windows.Forms.Button()
        Me.btnStartM30 = New System.Windows.Forms.Button()
        Me.btnStartM25 = New System.Windows.Forms.Button()
        Me.btnStartM20 = New System.Windows.Forms.Button()
        Me.btnStartM15 = New System.Windows.Forms.Button()
        Me.btnStartH9 = New System.Windows.Forms.Button()
        Me.btnStartH16 = New System.Windows.Forms.Button()
        Me.btnStartH15 = New System.Windows.Forms.Button()
        Me.btnStartH14 = New System.Windows.Forms.Button()
        Me.btnStartH13 = New System.Windows.Forms.Button()
        Me.btnStartH12 = New System.Windows.Forms.Button()
        Me.btnStartM10 = New System.Windows.Forms.Button()
        Me.btnStartM5 = New System.Windows.Forms.Button()
        Me.btnStartM0 = New System.Windows.Forms.Button()
        Me.btnStartH11 = New System.Windows.Forms.Button()
        Me.btnStartH10 = New System.Windows.Forms.Button()
        Me.btnRemoveSleep = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnEndH19 = New System.Windows.Forms.Button()
        Me.btnEndH8 = New System.Windows.Forms.Button()
        Me.btnEndH18 = New System.Windows.Forms.Button()
        Me.btnEndH17 = New System.Windows.Forms.Button()
        Me.btnEndH9 = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnEndH16 = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnEndH15 = New System.Windows.Forms.Button()
        Me.btnEndM55 = New System.Windows.Forms.Button()
        Me.btnEndH14 = New System.Windows.Forms.Button()
        Me.btnEndM50 = New System.Windows.Forms.Button()
        Me.btnEndH13 = New System.Windows.Forms.Button()
        Me.btnEndM45 = New System.Windows.Forms.Button()
        Me.btnEndH12 = New System.Windows.Forms.Button()
        Me.btnEndM40 = New System.Windows.Forms.Button()
        Me.btnEndH11 = New System.Windows.Forms.Button()
        Me.btnEndM35 = New System.Windows.Forms.Button()
        Me.btnEndH10 = New System.Windows.Forms.Button()
        Me.btnEndM30 = New System.Windows.Forms.Button()
        Me.btnEndM25 = New System.Windows.Forms.Button()
        Me.btnEndM20 = New System.Windows.Forms.Button()
        Me.btnEndM15 = New System.Windows.Forms.Button()
        Me.btnEndM10 = New System.Windows.Forms.Button()
        Me.btnEndM5 = New System.Windows.Forms.Button()
        Me.btnEndM0 = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.btnSleep5 = New System.Windows.Forms.Button()
        Me.btnSleep6 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnSleep4
        '
        Me.btnSleep4.Location = New System.Drawing.Point(515, 102)
        Me.btnSleep4.Name = "btnSleep4"
        Me.btnSleep4.Size = New System.Drawing.Size(160, 33)
        Me.btnSleep4.TabIndex = 3
        Me.btnSleep4.Text = "Sleep 4"
        Me.btnSleep4.UseVisualStyleBackColor = True
        '
        'btnSleep3
        '
        Me.btnSleep3.Location = New System.Drawing.Point(349, 102)
        Me.btnSleep3.Name = "btnSleep3"
        Me.btnSleep3.Size = New System.Drawing.Size(160, 33)
        Me.btnSleep3.TabIndex = 2
        Me.btnSleep3.Text = "Sleep 3"
        Me.btnSleep3.UseVisualStyleBackColor = True
        '
        'btnSleep2
        '
        Me.btnSleep2.Location = New System.Drawing.Point(183, 102)
        Me.btnSleep2.Name = "btnSleep2"
        Me.btnSleep2.Size = New System.Drawing.Size(160, 33)
        Me.btnSleep2.TabIndex = 1
        Me.btnSleep2.Text = "Sleep 2"
        Me.btnSleep2.UseVisualStyleBackColor = True
        '
        'btnSleep1
        '
        Me.btnSleep1.Location = New System.Drawing.Point(17, 102)
        Me.btnSleep1.Name = "btnSleep1"
        Me.btnSleep1.Size = New System.Drawing.Size(160, 33)
        Me.btnSleep1.TabIndex = 0
        Me.btnSleep1.Text = "Sleep 1"
        Me.btnSleep1.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnStartH19)
        Me.GroupBox1.Controls.Add(Me.btnStartH18)
        Me.GroupBox1.Controls.Add(Me.btnStartH17)
        Me.GroupBox1.Controls.Add(Me.btnStartH8)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.btnStartM55)
        Me.GroupBox1.Controls.Add(Me.btnStartM50)
        Me.GroupBox1.Controls.Add(Me.btnStartM45)
        Me.GroupBox1.Controls.Add(Me.btnStartM40)
        Me.GroupBox1.Controls.Add(Me.btnStartM35)
        Me.GroupBox1.Controls.Add(Me.btnStartM30)
        Me.GroupBox1.Controls.Add(Me.btnStartM25)
        Me.GroupBox1.Controls.Add(Me.btnStartM20)
        Me.GroupBox1.Controls.Add(Me.btnStartM15)
        Me.GroupBox1.Controls.Add(Me.btnStartH9)
        Me.GroupBox1.Controls.Add(Me.btnStartH16)
        Me.GroupBox1.Controls.Add(Me.btnStartH15)
        Me.GroupBox1.Controls.Add(Me.btnStartH14)
        Me.GroupBox1.Controls.Add(Me.btnStartH13)
        Me.GroupBox1.Controls.Add(Me.btnStartH12)
        Me.GroupBox1.Controls.Add(Me.btnStartM10)
        Me.GroupBox1.Controls.Add(Me.btnStartM5)
        Me.GroupBox1.Controls.Add(Me.btnStartM0)
        Me.GroupBox1.Controls.Add(Me.btnStartH11)
        Me.GroupBox1.Controls.Add(Me.btnStartH10)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 141)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(497, 334)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Start Time"
        '
        'btnStartH19
        '
        Me.btnStartH19.Location = New System.Drawing.Point(167, 257)
        Me.btnStartH19.Name = "btnStartH19"
        Me.btnStartH19.Size = New System.Drawing.Size(72, 60)
        Me.btnStartH19.TabIndex = 12
        Me.btnStartH19.Text = "7 pm"
        Me.btnStartH19.UseVisualStyleBackColor = True
        '
        'btnStartH18
        '
        Me.btnStartH18.Location = New System.Drawing.Point(89, 257)
        Me.btnStartH18.Name = "btnStartH18"
        Me.btnStartH18.Size = New System.Drawing.Size(72, 60)
        Me.btnStartH18.TabIndex = 11
        Me.btnStartH18.Text = "6 pm"
        Me.btnStartH18.UseVisualStyleBackColor = True
        '
        'btnStartH17
        '
        Me.btnStartH17.Location = New System.Drawing.Point(11, 257)
        Me.btnStartH17.Name = "btnStartH17"
        Me.btnStartH17.Size = New System.Drawing.Size(72, 60)
        Me.btnStartH17.TabIndex = 10
        Me.btnStartH17.Text = "5 pm"
        Me.btnStartH17.UseVisualStyleBackColor = True
        '
        'btnStartH8
        '
        Me.btnStartH8.Location = New System.Drawing.Point(11, 59)
        Me.btnStartH8.Name = "btnStartH8"
        Me.btnStartH8.Size = New System.Drawing.Size(72, 60)
        Me.btnStartH8.TabIndex = 1
        Me.btnStartH8.Text = "8 am"
        Me.btnStartH8.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(255, 27)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(233, 24)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Minutes"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(6, 27)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(233, 24)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Hours"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnStartM55
        '
        Me.btnStartM55.Location = New System.Drawing.Point(416, 257)
        Me.btnStartM55.Name = "btnStartM55"
        Me.btnStartM55.Size = New System.Drawing.Size(72, 60)
        Me.btnStartM55.TabIndex = 25
        Me.btnStartM55.Text = ":55"
        Me.btnStartM55.UseVisualStyleBackColor = True
        '
        'btnStartM50
        '
        Me.btnStartM50.Location = New System.Drawing.Point(338, 257)
        Me.btnStartM50.Name = "btnStartM50"
        Me.btnStartM50.Size = New System.Drawing.Size(72, 60)
        Me.btnStartM50.TabIndex = 24
        Me.btnStartM50.Text = ":50"
        Me.btnStartM50.UseVisualStyleBackColor = True
        '
        'btnStartM45
        '
        Me.btnStartM45.Location = New System.Drawing.Point(260, 257)
        Me.btnStartM45.Name = "btnStartM45"
        Me.btnStartM45.Size = New System.Drawing.Size(72, 60)
        Me.btnStartM45.TabIndex = 23
        Me.btnStartM45.Text = ":45"
        Me.btnStartM45.UseVisualStyleBackColor = True
        '
        'btnStartM40
        '
        Me.btnStartM40.Location = New System.Drawing.Point(416, 191)
        Me.btnStartM40.Name = "btnStartM40"
        Me.btnStartM40.Size = New System.Drawing.Size(72, 60)
        Me.btnStartM40.TabIndex = 22
        Me.btnStartM40.Text = ":40"
        Me.btnStartM40.UseVisualStyleBackColor = True
        '
        'btnStartM35
        '
        Me.btnStartM35.Location = New System.Drawing.Point(338, 191)
        Me.btnStartM35.Name = "btnStartM35"
        Me.btnStartM35.Size = New System.Drawing.Size(72, 60)
        Me.btnStartM35.TabIndex = 21
        Me.btnStartM35.Text = ":35"
        Me.btnStartM35.UseVisualStyleBackColor = True
        '
        'btnStartM30
        '
        Me.btnStartM30.Location = New System.Drawing.Point(260, 191)
        Me.btnStartM30.Name = "btnStartM30"
        Me.btnStartM30.Size = New System.Drawing.Size(72, 60)
        Me.btnStartM30.TabIndex = 20
        Me.btnStartM30.Text = ":30"
        Me.btnStartM30.UseVisualStyleBackColor = True
        '
        'btnStartM25
        '
        Me.btnStartM25.Location = New System.Drawing.Point(416, 125)
        Me.btnStartM25.Name = "btnStartM25"
        Me.btnStartM25.Size = New System.Drawing.Size(72, 60)
        Me.btnStartM25.TabIndex = 19
        Me.btnStartM25.Text = ":25"
        Me.btnStartM25.UseVisualStyleBackColor = True
        '
        'btnStartM20
        '
        Me.btnStartM20.Location = New System.Drawing.Point(338, 125)
        Me.btnStartM20.Name = "btnStartM20"
        Me.btnStartM20.Size = New System.Drawing.Size(72, 60)
        Me.btnStartM20.TabIndex = 18
        Me.btnStartM20.Text = ":20"
        Me.btnStartM20.UseVisualStyleBackColor = True
        '
        'btnStartM15
        '
        Me.btnStartM15.Location = New System.Drawing.Point(260, 125)
        Me.btnStartM15.Name = "btnStartM15"
        Me.btnStartM15.Size = New System.Drawing.Size(72, 60)
        Me.btnStartM15.TabIndex = 17
        Me.btnStartM15.Text = ":15"
        Me.btnStartM15.UseVisualStyleBackColor = True
        '
        'btnStartH9
        '
        Me.btnStartH9.Location = New System.Drawing.Point(89, 59)
        Me.btnStartH9.Name = "btnStartH9"
        Me.btnStartH9.Size = New System.Drawing.Size(72, 60)
        Me.btnStartH9.TabIndex = 2
        Me.btnStartH9.Text = "9 am"
        Me.btnStartH9.UseVisualStyleBackColor = True
        '
        'btnStartH16
        '
        Me.btnStartH16.Location = New System.Drawing.Point(167, 191)
        Me.btnStartH16.Name = "btnStartH16"
        Me.btnStartH16.Size = New System.Drawing.Size(72, 60)
        Me.btnStartH16.TabIndex = 9
        Me.btnStartH16.Text = "4 pm"
        Me.btnStartH16.UseVisualStyleBackColor = True
        '
        'btnStartH15
        '
        Me.btnStartH15.Location = New System.Drawing.Point(89, 191)
        Me.btnStartH15.Name = "btnStartH15"
        Me.btnStartH15.Size = New System.Drawing.Size(72, 60)
        Me.btnStartH15.TabIndex = 8
        Me.btnStartH15.Text = "3 pm"
        Me.btnStartH15.UseVisualStyleBackColor = True
        '
        'btnStartH14
        '
        Me.btnStartH14.Location = New System.Drawing.Point(11, 191)
        Me.btnStartH14.Name = "btnStartH14"
        Me.btnStartH14.Size = New System.Drawing.Size(72, 60)
        Me.btnStartH14.TabIndex = 7
        Me.btnStartH14.Text = "2 pm"
        Me.btnStartH14.UseVisualStyleBackColor = True
        '
        'btnStartH13
        '
        Me.btnStartH13.Location = New System.Drawing.Point(167, 125)
        Me.btnStartH13.Name = "btnStartH13"
        Me.btnStartH13.Size = New System.Drawing.Size(72, 60)
        Me.btnStartH13.TabIndex = 6
        Me.btnStartH13.Text = "1 pm"
        Me.btnStartH13.UseVisualStyleBackColor = True
        '
        'btnStartH12
        '
        Me.btnStartH12.Location = New System.Drawing.Point(89, 125)
        Me.btnStartH12.Name = "btnStartH12"
        Me.btnStartH12.Size = New System.Drawing.Size(72, 60)
        Me.btnStartH12.TabIndex = 5
        Me.btnStartH12.Text = "12"
        Me.btnStartH12.UseVisualStyleBackColor = True
        '
        'btnStartM10
        '
        Me.btnStartM10.Location = New System.Drawing.Point(416, 59)
        Me.btnStartM10.Name = "btnStartM10"
        Me.btnStartM10.Size = New System.Drawing.Size(72, 60)
        Me.btnStartM10.TabIndex = 16
        Me.btnStartM10.Text = ":10"
        Me.btnStartM10.UseVisualStyleBackColor = True
        '
        'btnStartM5
        '
        Me.btnStartM5.Location = New System.Drawing.Point(338, 59)
        Me.btnStartM5.Name = "btnStartM5"
        Me.btnStartM5.Size = New System.Drawing.Size(72, 60)
        Me.btnStartM5.TabIndex = 15
        Me.btnStartM5.Text = ":05"
        Me.btnStartM5.UseVisualStyleBackColor = True
        '
        'btnStartM0
        '
        Me.btnStartM0.Location = New System.Drawing.Point(260, 59)
        Me.btnStartM0.Name = "btnStartM0"
        Me.btnStartM0.Size = New System.Drawing.Size(72, 60)
        Me.btnStartM0.TabIndex = 14
        Me.btnStartM0.Text = ":00"
        Me.btnStartM0.UseVisualStyleBackColor = True
        '
        'btnStartH11
        '
        Me.btnStartH11.Location = New System.Drawing.Point(11, 125)
        Me.btnStartH11.Name = "btnStartH11"
        Me.btnStartH11.Size = New System.Drawing.Size(72, 60)
        Me.btnStartH11.TabIndex = 4
        Me.btnStartH11.Text = "11 am"
        Me.btnStartH11.UseVisualStyleBackColor = True
        '
        'btnStartH10
        '
        Me.btnStartH10.Location = New System.Drawing.Point(167, 59)
        Me.btnStartH10.Name = "btnStartH10"
        Me.btnStartH10.Size = New System.Drawing.Size(72, 60)
        Me.btnStartH10.TabIndex = 3
        Me.btnStartH10.Text = "10 am"
        Me.btnStartH10.UseVisualStyleBackColor = True
        '
        'btnRemoveSleep
        '
        Me.btnRemoveSleep.Image = CType(resources.GetObject("btnRemoveSleep.Image"), System.Drawing.Image)
        Me.btnRemoveSleep.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRemoveSleep.Location = New System.Drawing.Point(263, 481)
        Me.btnRemoveSleep.Name = "btnRemoveSleep"
        Me.btnRemoveSleep.Size = New System.Drawing.Size(245, 45)
        Me.btnRemoveSleep.TabIndex = 4
        Me.btnRemoveSleep.Text = "Remove Sleep Record"
        Me.btnRemoveSleep.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRemoveSleep.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnEndH19)
        Me.GroupBox2.Controls.Add(Me.btnEndH8)
        Me.GroupBox2.Controls.Add(Me.btnEndH18)
        Me.GroupBox2.Controls.Add(Me.btnEndH17)
        Me.GroupBox2.Controls.Add(Me.btnEndH9)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.btnEndH16)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.btnEndH15)
        Me.GroupBox2.Controls.Add(Me.btnEndM55)
        Me.GroupBox2.Controls.Add(Me.btnEndH14)
        Me.GroupBox2.Controls.Add(Me.btnEndM50)
        Me.GroupBox2.Controls.Add(Me.btnEndH13)
        Me.GroupBox2.Controls.Add(Me.btnEndM45)
        Me.GroupBox2.Controls.Add(Me.btnEndH12)
        Me.GroupBox2.Controls.Add(Me.btnEndM40)
        Me.GroupBox2.Controls.Add(Me.btnEndH11)
        Me.GroupBox2.Controls.Add(Me.btnEndM35)
        Me.GroupBox2.Controls.Add(Me.btnEndH10)
        Me.GroupBox2.Controls.Add(Me.btnEndM30)
        Me.GroupBox2.Controls.Add(Me.btnEndM25)
        Me.GroupBox2.Controls.Add(Me.btnEndM20)
        Me.GroupBox2.Controls.Add(Me.btnEndM15)
        Me.GroupBox2.Controls.Add(Me.btnEndM10)
        Me.GroupBox2.Controls.Add(Me.btnEndM5)
        Me.GroupBox2.Controls.Add(Me.btnEndM0)
        Me.GroupBox2.Location = New System.Drawing.Point(516, 141)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(497, 334)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "End Time"
        '
        'btnEndH19
        '
        Me.btnEndH19.Location = New System.Drawing.Point(165, 257)
        Me.btnEndH19.Name = "btnEndH19"
        Me.btnEndH19.Size = New System.Drawing.Size(72, 60)
        Me.btnEndH19.TabIndex = 34
        Me.btnEndH19.Text = "7 pm"
        Me.btnEndH19.UseVisualStyleBackColor = True
        '
        'btnEndH8
        '
        Me.btnEndH8.Location = New System.Drawing.Point(9, 59)
        Me.btnEndH8.Name = "btnEndH8"
        Me.btnEndH8.Size = New System.Drawing.Size(72, 60)
        Me.btnEndH8.TabIndex = 1
        Me.btnEndH8.Text = "8 am"
        Me.btnEndH8.UseVisualStyleBackColor = True
        '
        'btnEndH18
        '
        Me.btnEndH18.Location = New System.Drawing.Point(87, 257)
        Me.btnEndH18.Name = "btnEndH18"
        Me.btnEndH18.Size = New System.Drawing.Size(72, 60)
        Me.btnEndH18.TabIndex = 33
        Me.btnEndH18.Text = "6 pm"
        Me.btnEndH18.UseVisualStyleBackColor = True
        '
        'btnEndH17
        '
        Me.btnEndH17.Location = New System.Drawing.Point(9, 257)
        Me.btnEndH17.Name = "btnEndH17"
        Me.btnEndH17.Size = New System.Drawing.Size(72, 60)
        Me.btnEndH17.TabIndex = 32
        Me.btnEndH17.Text = "5 pm"
        Me.btnEndH17.UseVisualStyleBackColor = True
        '
        'btnEndH9
        '
        Me.btnEndH9.Location = New System.Drawing.Point(87, 59)
        Me.btnEndH9.Name = "btnEndH9"
        Me.btnEndH9.Size = New System.Drawing.Size(72, 60)
        Me.btnEndH9.TabIndex = 2
        Me.btnEndH9.Text = "9 am"
        Me.btnEndH9.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(258, 27)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(233, 24)
        Me.Label5.TabIndex = 27
        Me.Label5.Text = "Minutes"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnEndH16
        '
        Me.btnEndH16.Location = New System.Drawing.Point(165, 191)
        Me.btnEndH16.Name = "btnEndH16"
        Me.btnEndH16.Size = New System.Drawing.Size(72, 60)
        Me.btnEndH16.TabIndex = 9
        Me.btnEndH16.Text = "4 pm"
        Me.btnEndH16.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(4, 27)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(233, 24)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Hours"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnEndH15
        '
        Me.btnEndH15.Location = New System.Drawing.Point(87, 191)
        Me.btnEndH15.Name = "btnEndH15"
        Me.btnEndH15.Size = New System.Drawing.Size(72, 60)
        Me.btnEndH15.TabIndex = 8
        Me.btnEndH15.Text = "3 pm"
        Me.btnEndH15.UseVisualStyleBackColor = True
        '
        'btnEndM55
        '
        Me.btnEndM55.Location = New System.Drawing.Point(419, 257)
        Me.btnEndM55.Name = "btnEndM55"
        Me.btnEndM55.Size = New System.Drawing.Size(72, 60)
        Me.btnEndM55.TabIndex = 25
        Me.btnEndM55.Text = ":55"
        Me.btnEndM55.UseVisualStyleBackColor = True
        '
        'btnEndH14
        '
        Me.btnEndH14.Location = New System.Drawing.Point(9, 191)
        Me.btnEndH14.Name = "btnEndH14"
        Me.btnEndH14.Size = New System.Drawing.Size(72, 60)
        Me.btnEndH14.TabIndex = 7
        Me.btnEndH14.Text = "2 pm"
        Me.btnEndH14.UseVisualStyleBackColor = True
        '
        'btnEndM50
        '
        Me.btnEndM50.Location = New System.Drawing.Point(341, 257)
        Me.btnEndM50.Name = "btnEndM50"
        Me.btnEndM50.Size = New System.Drawing.Size(72, 60)
        Me.btnEndM50.TabIndex = 24
        Me.btnEndM50.Text = ":50"
        Me.btnEndM50.UseVisualStyleBackColor = True
        '
        'btnEndH13
        '
        Me.btnEndH13.Location = New System.Drawing.Point(165, 125)
        Me.btnEndH13.Name = "btnEndH13"
        Me.btnEndH13.Size = New System.Drawing.Size(72, 60)
        Me.btnEndH13.TabIndex = 6
        Me.btnEndH13.Text = "1 pm"
        Me.btnEndH13.UseVisualStyleBackColor = True
        '
        'btnEndM45
        '
        Me.btnEndM45.Location = New System.Drawing.Point(263, 257)
        Me.btnEndM45.Name = "btnEndM45"
        Me.btnEndM45.Size = New System.Drawing.Size(72, 60)
        Me.btnEndM45.TabIndex = 23
        Me.btnEndM45.Text = ":45"
        Me.btnEndM45.UseVisualStyleBackColor = True
        '
        'btnEndH12
        '
        Me.btnEndH12.Location = New System.Drawing.Point(87, 125)
        Me.btnEndH12.Name = "btnEndH12"
        Me.btnEndH12.Size = New System.Drawing.Size(72, 60)
        Me.btnEndH12.TabIndex = 5
        Me.btnEndH12.Text = "12"
        Me.btnEndH12.UseVisualStyleBackColor = True
        '
        'btnEndM40
        '
        Me.btnEndM40.Location = New System.Drawing.Point(419, 191)
        Me.btnEndM40.Name = "btnEndM40"
        Me.btnEndM40.Size = New System.Drawing.Size(72, 60)
        Me.btnEndM40.TabIndex = 22
        Me.btnEndM40.Text = ":40"
        Me.btnEndM40.UseVisualStyleBackColor = True
        '
        'btnEndH11
        '
        Me.btnEndH11.Location = New System.Drawing.Point(9, 125)
        Me.btnEndH11.Name = "btnEndH11"
        Me.btnEndH11.Size = New System.Drawing.Size(72, 60)
        Me.btnEndH11.TabIndex = 4
        Me.btnEndH11.Text = "11 am"
        Me.btnEndH11.UseVisualStyleBackColor = True
        '
        'btnEndM35
        '
        Me.btnEndM35.Location = New System.Drawing.Point(341, 191)
        Me.btnEndM35.Name = "btnEndM35"
        Me.btnEndM35.Size = New System.Drawing.Size(72, 60)
        Me.btnEndM35.TabIndex = 21
        Me.btnEndM35.Text = ":35"
        Me.btnEndM35.UseVisualStyleBackColor = True
        '
        'btnEndH10
        '
        Me.btnEndH10.Location = New System.Drawing.Point(165, 59)
        Me.btnEndH10.Name = "btnEndH10"
        Me.btnEndH10.Size = New System.Drawing.Size(72, 60)
        Me.btnEndH10.TabIndex = 3
        Me.btnEndH10.Text = "10 am"
        Me.btnEndH10.UseVisualStyleBackColor = True
        '
        'btnEndM30
        '
        Me.btnEndM30.Location = New System.Drawing.Point(263, 191)
        Me.btnEndM30.Name = "btnEndM30"
        Me.btnEndM30.Size = New System.Drawing.Size(72, 60)
        Me.btnEndM30.TabIndex = 20
        Me.btnEndM30.Text = ":30"
        Me.btnEndM30.UseVisualStyleBackColor = True
        '
        'btnEndM25
        '
        Me.btnEndM25.Location = New System.Drawing.Point(419, 125)
        Me.btnEndM25.Name = "btnEndM25"
        Me.btnEndM25.Size = New System.Drawing.Size(72, 60)
        Me.btnEndM25.TabIndex = 19
        Me.btnEndM25.Text = ":25"
        Me.btnEndM25.UseVisualStyleBackColor = True
        '
        'btnEndM20
        '
        Me.btnEndM20.Location = New System.Drawing.Point(341, 125)
        Me.btnEndM20.Name = "btnEndM20"
        Me.btnEndM20.Size = New System.Drawing.Size(72, 60)
        Me.btnEndM20.TabIndex = 18
        Me.btnEndM20.Text = ":20"
        Me.btnEndM20.UseVisualStyleBackColor = True
        '
        'btnEndM15
        '
        Me.btnEndM15.Location = New System.Drawing.Point(263, 125)
        Me.btnEndM15.Name = "btnEndM15"
        Me.btnEndM15.Size = New System.Drawing.Size(72, 60)
        Me.btnEndM15.TabIndex = 17
        Me.btnEndM15.Text = ":15"
        Me.btnEndM15.UseVisualStyleBackColor = True
        '
        'btnEndM10
        '
        Me.btnEndM10.Location = New System.Drawing.Point(419, 59)
        Me.btnEndM10.Name = "btnEndM10"
        Me.btnEndM10.Size = New System.Drawing.Size(72, 60)
        Me.btnEndM10.TabIndex = 10
        Me.btnEndM10.Text = ":10"
        Me.btnEndM10.UseVisualStyleBackColor = True
        '
        'btnEndM5
        '
        Me.btnEndM5.Location = New System.Drawing.Point(341, 59)
        Me.btnEndM5.Name = "btnEndM5"
        Me.btnEndM5.Size = New System.Drawing.Size(72, 60)
        Me.btnEndM5.TabIndex = 9
        Me.btnEndM5.Text = ":05"
        Me.btnEndM5.UseVisualStyleBackColor = True
        '
        'btnEndM0
        '
        Me.btnEndM0.Location = New System.Drawing.Point(263, 59)
        Me.btnEndM0.Name = "btnEndM0"
        Me.btnEndM0.Size = New System.Drawing.Size(72, 60)
        Me.btnEndM0.TabIndex = 8
        Me.btnEndM0.Text = ":00"
        Me.btnEndM0.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Image = CType(resources.GetObject("btnAdd.Image"), System.Drawing.Image)
        Me.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAdd.Location = New System.Drawing.Point(12, 481)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(245, 45)
        Me.btnAdd.TabIndex = 3
        Me.btnAdd.Text = "Add Sleep Record"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnSleep5
        '
        Me.btnSleep5.Location = New System.Drawing.Point(681, 102)
        Me.btnSleep5.Name = "btnSleep5"
        Me.btnSleep5.Size = New System.Drawing.Size(160, 33)
        Me.btnSleep5.TabIndex = 4
        Me.btnSleep5.Text = "Sleep 5"
        Me.btnSleep5.UseVisualStyleBackColor = True
        '
        'btnSleep6
        '
        Me.btnSleep6.Location = New System.Drawing.Point(847, 102)
        Me.btnSleep6.Name = "btnSleep6"
        Me.btnSleep6.Size = New System.Drawing.Size(160, 33)
        Me.btnSleep6.TabIndex = 5
        Me.btnSleep6.Text = "Sleep 6"
        Me.btnSleep6.UseVisualStyleBackColor = True
        '
        'frmSleep
        '
        Me.ClientSize = New System.Drawing.Size(1024, 538)
        Me.Controls.Add(Me.btnSleep6)
        Me.Controls.Add(Me.btnSleep5)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnRemoveSleep)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnSleep4)
        Me.Controls.Add(Me.btnSleep3)
        Me.Controls.Add(Me.btnSleep2)
        Me.Controls.Add(Me.btnSleep1)
        Me.Name = "frmSleep"
        Me.Text = "Sleep Records"
        Me.Controls.SetChildIndex(Me.btnSleep1, 0)
        Me.Controls.SetChildIndex(Me.btnSleep2, 0)
        Me.Controls.SetChildIndex(Me.btnSleep3, 0)
        Me.Controls.SetChildIndex(Me.btnSleep4, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.btnRemoveSleep, 0)
        Me.Controls.SetChildIndex(Me.GroupBox2, 0)
        Me.Controls.SetChildIndex(Me.btnAdd, 0)
        Me.Controls.SetChildIndex(Me.btnSleep5, 0)
        Me.Controls.SetChildIndex(Me.btnSleep6, 0)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSleep4 As System.Windows.Forms.Button
    Friend WithEvents btnSleep3 As System.Windows.Forms.Button
    Friend WithEvents btnSleep2 As System.Windows.Forms.Button
    Friend WithEvents btnSleep1 As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnRemoveSleep As System.Windows.Forms.Button
    Friend WithEvents btnStartH10 As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnStartM55 As System.Windows.Forms.Button
    Friend WithEvents btnStartM50 As System.Windows.Forms.Button
    Friend WithEvents btnStartM45 As System.Windows.Forms.Button
    Friend WithEvents btnStartM40 As System.Windows.Forms.Button
    Friend WithEvents btnStartM35 As System.Windows.Forms.Button
    Friend WithEvents btnStartM30 As System.Windows.Forms.Button
    Friend WithEvents btnStartM25 As System.Windows.Forms.Button
    Friend WithEvents btnStartM20 As System.Windows.Forms.Button
    Friend WithEvents btnStartM15 As System.Windows.Forms.Button
    Friend WithEvents btnStartH9 As System.Windows.Forms.Button
    Friend WithEvents btnStartH16 As System.Windows.Forms.Button
    Friend WithEvents btnStartH15 As System.Windows.Forms.Button
    Friend WithEvents btnStartH14 As System.Windows.Forms.Button
    Friend WithEvents btnStartH13 As System.Windows.Forms.Button
    Friend WithEvents btnStartH12 As System.Windows.Forms.Button
    Friend WithEvents btnStartM10 As System.Windows.Forms.Button
    Friend WithEvents btnStartM5 As System.Windows.Forms.Button
    Friend WithEvents btnStartM0 As System.Windows.Forms.Button
    Friend WithEvents btnStartH11 As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btnEndM55 As System.Windows.Forms.Button
    Friend WithEvents btnEndM50 As System.Windows.Forms.Button
    Friend WithEvents btnEndM45 As System.Windows.Forms.Button
    Friend WithEvents btnEndM40 As System.Windows.Forms.Button
    Friend WithEvents btnEndM35 As System.Windows.Forms.Button
    Friend WithEvents btnEndM30 As System.Windows.Forms.Button
    Friend WithEvents btnEndM25 As System.Windows.Forms.Button
    Friend WithEvents btnEndM20 As System.Windows.Forms.Button
    Friend WithEvents btnEndM15 As System.Windows.Forms.Button
    Friend WithEvents btnEndM10 As System.Windows.Forms.Button
    Friend WithEvents btnEndM5 As System.Windows.Forms.Button
    Friend WithEvents btnEndM0 As System.Windows.Forms.Button
    Friend WithEvents btnEndH9 As System.Windows.Forms.Button
    Friend WithEvents btnEndH16 As System.Windows.Forms.Button
    Friend WithEvents btnEndH15 As System.Windows.Forms.Button
    Friend WithEvents btnEndH14 As System.Windows.Forms.Button
    Friend WithEvents btnEndH13 As System.Windows.Forms.Button
    Friend WithEvents btnEndH12 As System.Windows.Forms.Button
    Friend WithEvents btnEndH11 As System.Windows.Forms.Button
    Friend WithEvents btnEndH10 As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents btnStartH19 As System.Windows.Forms.Button
    Friend WithEvents btnStartH18 As System.Windows.Forms.Button
    Friend WithEvents btnStartH17 As System.Windows.Forms.Button
    Friend WithEvents btnStartH8 As System.Windows.Forms.Button
    Friend WithEvents btnEndH19 As System.Windows.Forms.Button
    Friend WithEvents btnEndH8 As System.Windows.Forms.Button
    Friend WithEvents btnEndH18 As System.Windows.Forms.Button
    Friend WithEvents btnEndH17 As System.Windows.Forms.Button
    Friend WithEvents btnSleep5 As System.Windows.Forms.Button
    Friend WithEvents btnSleep6 As System.Windows.Forms.Button

End Class
