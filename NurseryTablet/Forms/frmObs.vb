﻿Imports NurseryTablet.SharedModule
Imports System.Drawing

Public Class frmObs

    Private m_ObsID As Guid = Nothing
    Private m_TaggedChildren As New List(Of TaggedChild)
    Private m_Media As New List(Of MediaContent)
    Private m_MediaIndex As Integer = -1
    Private m_ExistingRecord As Boolean = False

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_ObsID = Guid.NewGuid

    End Sub

    Public Sub New(ByVal ObsID As Guid)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_ObsID = ObsID
        m_ExistingRecord = True

    End Sub

    Private Sub frmObs_Load(sender As Object, e As EventArgs) Handles Me.Load

        Me.Cursor = Cursors.WaitCursor
        Application.DoEvents()

        If m_ExistingRecord Then
            DisplayRecord()
        End If

        SetPictureButtons()
        SetChildrenButtons()

        Me.Cursor = Cursors.Default

    End Sub

#Region "Display"

    Private Sub DisplayRecord()

        Dim _SQL As String

        _SQL = "select * from Obs" & _
               " where id = '" & m_ObsID.ToString & "'"

        Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
        If Not _DR Is Nothing Then

            fldTitle.ValueText = _DR("title").ToString
            fldStaff.ValueID = _DR("staff_id").ToString
            fldStaff.ValueText = Business.Staff.ReturnName(fldStaff.ValueID)
            fldTime.ValueText = Format(CDate(_DR("stamp")), "HH:mm")

            fldComments.ValueText = _DR("comments").ToString
            txtComments.Text = _DR("comments").ToString

            PopulateTaggedChildrenFromDB()
            PopulateMediaFromDB()

            _DR = Nothing

        End If

    End Sub

    Private Sub DisplayChildren()

        tgChildren.Populate(m_TaggedChildren)

        tgChildren.Columns("ChildID").Visible = False
        tgChildren.Columns("ChildName").Caption = "Child"
        tgChildren.Columns("Assessed").Caption = "Assessed"
        tgChildren.Columns("AssessmentID").Visible = False

        SetChildrenButtons()

    End Sub

    Private Sub PopulateTaggedChildrenFromDB()

        m_TaggedChildren.Clear()

        Dim _SQL As String = ""

        _SQL += "select c.ID as 'child_id', c.fullname as 'Name' from ObsChildren oc"
        _SQL += " left join Children c on c.ID = oc.child_id"
        _SQL += " where oc.obs_id = '" + m_ObsID.ToString + "'"

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If Not _DT Is Nothing Then

            For Each _DR As DataRow In _DT.Rows
                m_TaggedChildren.Add(New TaggedChild(_DR.Item("child_id").ToString, _DR.Item("Name").ToString))
            Next

            _DT.Dispose()
            _DT = Nothing

        End If

        DisplayChildren()

    End Sub

    Private Sub PopulateMediaFromDB()

        picMedia.Image = Nothing
        m_Media.Clear()

        GC.Collect()

        m_MediaIndex = -1

        Dim _SQL As String = ""

        _SQL += "select * from Media"
        _SQL += " where key_id = '" + m_ObsID.ToString + "'"

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If Not _DT Is Nothing Then

            If _DT.Rows.Count > 0 Then

                For Each _DR As DataRow In _DT.Rows

                    Dim _m As New MediaContent
                    With _m
                        .KeyID = _DR.Item("key_id").ToString
                        .KeyType = ReturnKeyType(_DR.Item("data").ToString)
                        .Data = _DR.Item("data")
                        .DeviceName = _DR.Item("device_name").ToString
                        .FilePath = _DR.Item("file_path").ToString
                        .FileExt = _DR.Item("file_ext").ToString
                        .FileSize = _DR.Item("file_size")
                    End With

                    m_Media.Add(_m)

                Next

                m_MediaIndex = 0
                DisplayPicture()

            End If

            _DT.Dispose()
            _DT = Nothing

        End If

        SetPictureButtons()

    End Sub

    Private Function ReturnKeyType(ByVal StringIn As String) As MediaHandler.EnumCaptureCategory
        If StringIn = "Observation" Then Return MediaHandler.EnumCaptureCategory.Observation
        If StringIn = "Accident" Then Return MediaHandler.EnumCaptureCategory.Accident
        Return MediaHandler.EnumCaptureCategory.Media
    End Function

    Private Sub PopulateMediaFromFiles(ByVal CapturedFiles As List(Of FileDetail))

        picMedia.Image = Nothing
        m_Media.Clear()

        If CapturedFiles IsNot Nothing Then

            GC.Collect()
            m_MediaIndex = -1

            If CapturedFiles.Count > 0 Then

                For Each _f In CapturedFiles
                    m_Media.Add(New MediaContent(MediaHandler.EnumCaptureCategory.Observation, m_ObsID.ToString, _f))
                Next

                m_MediaIndex = 0
                DisplayPicture()

            End If

        End If

        SetPictureButtons()

    End Sub

#End Region

#Region "Save"

    Private Function CheckAnswers() As Boolean

        If fldTitle.ValueText = "" Then
            Msgbox("Please enter a Title.", MessageBoxIcon.Exclamation, "Save Observation")
            Return False
        End If

        If fldStaff.ValueText = "" Then
            Msgbox("Please select a member of Staff.", MessageBoxIcon.Exclamation, "Save Observation")
            Return False
        End If

        If fldTime.ValueText = "" Then
            Msgbox("Please enter the time the observation was made.", MessageBoxIcon.Exclamation, "Save Observation")
            Return False
        End If

        If fldComments.ValueText = "" Then
            Msgbox("Please enter comments.", MessageBoxIcon.Exclamation, "Save Observation")
            Return False
        End If

        If m_TaggedChildren.Count = 0 Then
            Msgbox("You have not Tagged any Children.", MessageBoxIcon.Exclamation, "Save Observation")
            Return False
        End If

        If m_Media.Count = 0 Then
            If Msgbox("You have not attached any media - Do you want to Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Save Observation") = Windows.Forms.DialogResult.No Then
                Return False
            End If
        End If

        Return True

    End Function

    Private Function Save()

        If Not CheckAnswers() Then Return False

        Dim _SQL As String

        _SQL = "select * from Obs" & _
               " where ID = '" & m_ObsID.ToString & "'"

        Dim _DA As IDataAdapter = DAL.ReturnDataAdapter(_SQL)
        If Not _DA Is Nothing Then

            Dim _DR As DataRow
            Dim _DS As New DataSet
            _DA.Fill(_DS)

            If _DS.Tables(0).Rows.Count = 0 Then
                _DR = _DS.Tables(0).NewRow
                _DR("id") = m_ObsID
                _DR("day_id") = New Guid(TodayID)
            Else
                _DR = _DS.Tables(0).Rows(0)
            End If

            _DR("date") = Format(DateAndTime.Today, "yyyy-MM-dd") + " " + fldTime.ValueText
            _DR("title") = fldTitle.ValueText
            _DR("comments") = fldComments.ValueText
            _DR("staff_id") = fldStaff.ValueID
            _DR("stamp") = Now

            If _DS.Tables(0).Rows.Count = 0 Then _DS.Tables(0).Rows.Add(_DR)

            DAL.UpdateDB(_DA, _DS)

            SaveObsChildren()
            SaveMedia()

        End If

        Return True

    End Function

    Private Sub SaveMedia()
        DeleteMediaFromDB()
        MediaHandler.SaveMedia(m_Media)
    End Sub

    Private Sub DeleteMediaFromDB()

        Dim _SQL As String = ""
        _SQL += "delete from Media"
        _SQL += " where key_id = '" + m_ObsID.ToString + "'"
        _SQL += " and device_name = '" + My.Computer.Name + "'"

        DAL.ExecuteCommand(_SQL)

    End Sub

    Private Sub SaveObsChildren()

        ClearTaggedChildren()

        For Each _c In m_TaggedChildren
            AddChildToObs(_c.ChildID)
        Next

    End Sub

    Private Sub AddChildToObs(ByVal ChildID As String)

        Dim _ID As Guid = Guid.NewGuid
        Dim _SQL As String

        _SQL = "select * from ObsChildren" & _
               " where ID = '" & _ID.ToString & "'"

        Dim _DA As IDataAdapter = DAL.ReturnDataAdapter(_SQL)
        If Not _DA Is Nothing Then

            Dim _DR As DataRow
            Dim _DS As New DataSet
            _DA.Fill(_DS)

            _DR = _DS.Tables(0).NewRow

            _DR("id") = _ID
            _DR("obs_id") = m_ObsID
            _DR("child_id") = ChildID

            _DS.Tables(0).Rows.Add(_DR)

            Try
                DAL.UpdateDB(_DA, _DS)

            Catch ex As Exception
                _ID = Nothing
            End Try

        End If

    End Sub

#End Region

#Region "Other Controls Events"

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        btnSave.Enabled = False
        Me.Cursor = Cursors.WaitCursor

        If Save() Then
            Me.Cursor = Cursors.Default
            Me.Close()
        End If

        btnSave.Enabled = True
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub fldStaff_ButtonClick(sender As Object, e As EventArgs) Handles fldStaff.ButtonClick

        Dim _Return As Pair = Business.Staff.FindStaff(Enums.PersonMode.OnlyCheckedIn)
        If Not _Return Is Nothing AndAlso _Return.Code <> "" Then
            fldStaff.ValueID = _Return.Code
            fldStaff.ValueText = _Return.Text
        End If

    End Sub

    Private Sub fldComments_AfterButtonClick(sender As Object, e As EventArgs) Handles fldComments.AfterButtonClick
        txtComments.Text = fldComments.ValueText
    End Sub

#End Region

#Region "Child Buttons"

    Private Sub btnChildAdd_Click(sender As Object, e As EventArgs) Handles btnChildAdd.Click
        Dim _Return As Pair = Business.ChildHandler.FindChildByGroup(Enums.PersonMode.OnlyCheckedIn, True)
        If Not _Return Is Nothing AndAlso _Return.Code <> "" Then
            m_TaggedChildren.Add(New TaggedChild(_Return.Code, _Return.Text))
            DisplayChildren()
        End If
    End Sub

    Private Sub btnChildDelete_Click(sender As Object, e As EventArgs) Handles btnChildDelete.Click
        If Msgbox("Are you sure you want to remove this Child from the Observation?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Confirm Remove") Then
            m_TaggedChildren.Remove(m_TaggedChildren.Item(tgChildren.RowIndex))
            DisplayChildren()
        End If
    End Sub

#End Region

#Region "Media Buttons"

    Private Sub btnMediaLeft_Click(sender As Object, e As EventArgs) Handles btnMediaLeft.Click

        btnMediaLeft.Enabled = False
        Me.Cursor = Cursors.AppStarting

        m_MediaIndex -= 1
        DisplayPicture()
        SetPictureButtons()

        Me.Cursor = Cursors.Default

    End Sub

    Private Sub btnMediaRight_Click(sender As Object, e As EventArgs) Handles btnMediaRight.Click

        btnMediaRight.Enabled = False
        Me.Cursor = Cursors.AppStarting

        m_MediaIndex += 1
        DisplayPicture()
        SetPictureButtons()

        Me.Cursor = Cursors.Default

    End Sub

    Private Sub btnMediaCapture_Click(sender As Object, e As EventArgs) Handles btnMediaCapture.Click
        PopulateMediaFromFiles(MediaHandler.CaptureMultiple)
    End Sub

    Private Sub btnMediaDelete_Click(sender As Object, e As EventArgs) Handles btnMediaDelete.Click

        If Msgbox("Are you sure you want to delete this media?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Confirm Delete") Then

            btnMediaDelete.Enabled = False
            Me.Cursor = Cursors.AppStarting

            m_Media.RemoveAt(m_MediaIndex)

            If m_Media.Count > 0 Then
                m_MediaIndex = 0
            Else
                m_MediaIndex = -1
            End If

            DisplayPicture()
            SetPictureButtons()

            Me.Cursor = Cursors.Default

        End If

    End Sub

#End Region

    Private Sub ClearTaggedChildren()
        Dim _SQL As String = "delete from ObsChildren where obs_id = '" + m_ObsID.ToString + "'"
        DAL.ExecuteCommand(_SQL)
    End Sub

    Private Sub SetPictureButtons()

        Select Case m_Media.Count

            Case 0
                btnMediaLeft.Enabled = False
                btnMediaRight.Enabled = False
                btnMediaDelete.Enabled = False

            Case 1
                btnMediaLeft.Enabled = False
                btnMediaRight.Enabled = False
                btnMediaDelete.Enabled = True

            Case Else

                btnMediaDelete.Enabled = True

                If m_MediaIndex = 0 Then
                    btnMediaLeft.Enabled = False
                    btnMediaRight.Enabled = True
                Else
                    If m_MediaIndex = (m_Media.Count - 1) Then
                        btnMediaLeft.Enabled = True
                        btnMediaRight.Enabled = False
                    Else
                        btnMediaLeft.Enabled = True
                        btnMediaRight.Enabled = True
                    End If
                End If

        End Select

    End Sub

    Private Sub SetChildrenButtons()
        If m_TaggedChildren.Count > 0 Then
            btnChildDelete.Enabled = True
        Else
            btnChildDelete.Enabled = False
        End If
    End Sub

    Private Sub DisplayPicture()

        picMedia.Image = Nothing
        GC.Collect()

        If m_MediaIndex >= 0 Then
            picMedia.Image = m_Media(m_MediaIndex).Image
        End If

    End Sub

    Private Class TaggedChild

        Public Sub New(ByVal ChildID As String, ByVal ChildName As String)
            Me.ChildID = ChildID
            Me.ChildName = ChildName
            Me.Assessed = False
            Me.AssessmentID = ""
        End Sub

        Property ChildID As String
        Property ChildName As String
        Property Assessed As Boolean
        Property AssessmentID As String

    End Class

End Class
