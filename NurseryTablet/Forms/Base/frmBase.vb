﻿Public Class frmBase

    Private Sub frmBase_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If Parameters.MaximiseForms Then
            Me.WindowState = FormWindowState.Maximized
        End If

        If Debugger.IsAttached Then
            Me.TopMost = False
        End If

    End Sub
End Class