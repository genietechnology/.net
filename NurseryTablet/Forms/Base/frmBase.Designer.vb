﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBase
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling2 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling3 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling4 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling5 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling6 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling7 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling8 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling9 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling10 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling11 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling12 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling13 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling14 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling15 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling16 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.SpellChecker1 = New DevExpress.XtraSpellChecker.SpellChecker(Me.components)
        Me.txtLightPink = New DevExpress.XtraEditors.TextEdit()
        Me.txtPink = New DevExpress.XtraEditors.TextEdit()
        Me.txtLightBlue = New DevExpress.XtraEditors.TextEdit()
        Me.txtBlue = New DevExpress.XtraEditors.TextEdit()
        Me.txtLightGrey = New DevExpress.XtraEditors.TextEdit()
        Me.txtGrey = New DevExpress.XtraEditors.TextEdit()
        Me.txtLightGreen = New DevExpress.XtraEditors.TextEdit()
        Me.txtLightOrange = New DevExpress.XtraEditors.TextEdit()
        Me.txtLightRed = New DevExpress.XtraEditors.TextEdit()
        Me.txtLightYellow = New DevExpress.XtraEditors.TextEdit()
        Me.txtLightCyan = New DevExpress.XtraEditors.TextEdit()
        Me.txtGreen = New DevExpress.XtraEditors.TextEdit()
        Me.txtOrange = New DevExpress.XtraEditors.TextEdit()
        Me.txtRed = New DevExpress.XtraEditors.TextEdit()
        Me.txtYellow = New DevExpress.XtraEditors.TextEdit()
        Me.txtCyan = New DevExpress.XtraEditors.TextEdit()
        CType(Me.txtLightPink.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPink.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLightBlue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBlue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLightGrey.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGrey.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLightGreen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLightOrange.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLightRed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLightYellow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLightCyan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOrange.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCyan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.Culture = New System.Globalization.CultureInfo("en-GB")
        Me.SpellChecker1.ParentContainer = Me
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.AsYouType
        '
        'txtLightPink
        '
        Me.txtLightPink.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtLightPink.Location = New System.Drawing.Point(215, 510)
        Me.txtLightPink.Name = "txtLightPink"
        Me.txtLightPink.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtLightPink.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtLightPink, True)
        Me.txtLightPink.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtLightPink, OptionsSpelling1)
        Me.txtLightPink.TabIndex = 93
        Me.txtLightPink.Visible = False
        '
        'txtPink
        '
        Me.txtPink.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtPink.Location = New System.Drawing.Point(215, 484)
        Me.txtPink.Name = "txtPink"
        Me.txtPink.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtPink.Properties.Appearance.ForeColor = System.Drawing.Color.White
        Me.txtPink.Properties.Appearance.Options.UseBackColor = True
        Me.txtPink.Properties.Appearance.Options.UseForeColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtPink, True)
        Me.txtPink.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtPink, OptionsSpelling2)
        Me.txtPink.TabIndex = 92
        Me.txtPink.Visible = False
        '
        'txtLightBlue
        '
        Me.txtLightBlue.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtLightBlue.Location = New System.Drawing.Point(186, 510)
        Me.txtLightBlue.Name = "txtLightBlue"
        Me.txtLightBlue.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtLightBlue.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtLightBlue, True)
        Me.txtLightBlue.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtLightBlue, OptionsSpelling3)
        Me.txtLightBlue.TabIndex = 91
        Me.txtLightBlue.Visible = False
        '
        'txtBlue
        '
        Me.txtBlue.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtBlue.Location = New System.Drawing.Point(186, 484)
        Me.txtBlue.Name = "txtBlue"
        Me.txtBlue.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBlue.Properties.Appearance.ForeColor = System.Drawing.Color.White
        Me.txtBlue.Properties.Appearance.Options.UseBackColor = True
        Me.txtBlue.Properties.Appearance.Options.UseForeColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtBlue, True)
        Me.txtBlue.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtBlue, OptionsSpelling4)
        Me.txtBlue.TabIndex = 90
        Me.txtBlue.Visible = False
        '
        'txtLightGrey
        '
        Me.txtLightGrey.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtLightGrey.Location = New System.Drawing.Point(157, 510)
        Me.txtLightGrey.Name = "txtLightGrey"
        Me.txtLightGrey.Properties.Appearance.BackColor = System.Drawing.Color.Silver
        Me.txtLightGrey.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtLightGrey, True)
        Me.txtLightGrey.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtLightGrey, OptionsSpelling5)
        Me.txtLightGrey.TabIndex = 89
        Me.txtLightGrey.Visible = False
        '
        'txtGrey
        '
        Me.txtGrey.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtGrey.Location = New System.Drawing.Point(157, 484)
        Me.txtGrey.Name = "txtGrey"
        Me.txtGrey.Properties.Appearance.BackColor = System.Drawing.Color.Gray
        Me.txtGrey.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtGrey, True)
        Me.txtGrey.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtGrey, OptionsSpelling6)
        Me.txtGrey.TabIndex = 88
        Me.txtGrey.Visible = False
        '
        'txtLightGreen
        '
        Me.txtLightGreen.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtLightGreen.Location = New System.Drawing.Point(70, 510)
        Me.txtLightGreen.Name = "txtLightGreen"
        Me.txtLightGreen.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtLightGreen.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtLightGreen, True)
        Me.txtLightGreen.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtLightGreen, OptionsSpelling7)
        Me.txtLightGreen.TabIndex = 86
        Me.txtLightGreen.Visible = False
        '
        'txtLightOrange
        '
        Me.txtLightOrange.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtLightOrange.Location = New System.Drawing.Point(12, 510)
        Me.txtLightOrange.Name = "txtLightOrange"
        Me.txtLightOrange.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtLightOrange.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtLightOrange, True)
        Me.txtLightOrange.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtLightOrange, OptionsSpelling8)
        Me.txtLightOrange.TabIndex = 84
        Me.txtLightOrange.Visible = False
        '
        'txtLightRed
        '
        Me.txtLightRed.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtLightRed.Location = New System.Drawing.Point(41, 510)
        Me.txtLightRed.Name = "txtLightRed"
        Me.txtLightRed.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtLightRed.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtLightRed, True)
        Me.txtLightRed.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtLightRed, OptionsSpelling9)
        Me.txtLightRed.TabIndex = 83
        Me.txtLightRed.Visible = False
        '
        'txtLightYellow
        '
        Me.txtLightYellow.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtLightYellow.Location = New System.Drawing.Point(128, 510)
        Me.txtLightYellow.Name = "txtLightYellow"
        Me.txtLightYellow.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtLightYellow.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtLightYellow, True)
        Me.txtLightYellow.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtLightYellow, OptionsSpelling10)
        Me.txtLightYellow.TabIndex = 85
        Me.txtLightYellow.Visible = False
        '
        'txtLightCyan
        '
        Me.txtLightCyan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtLightCyan.Location = New System.Drawing.Point(99, 510)
        Me.txtLightCyan.Name = "txtLightCyan"
        Me.txtLightCyan.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtLightCyan.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtLightCyan, True)
        Me.txtLightCyan.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtLightCyan, OptionsSpelling11)
        Me.txtLightCyan.TabIndex = 87
        Me.txtLightCyan.Visible = False
        '
        'txtGreen
        '
        Me.txtGreen.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtGreen.Location = New System.Drawing.Point(70, 484)
        Me.txtGreen.Name = "txtGreen"
        Me.txtGreen.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtGreen.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtGreen, True)
        Me.txtGreen.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtGreen, OptionsSpelling12)
        Me.txtGreen.TabIndex = 81
        Me.txtGreen.Visible = False
        '
        'txtOrange
        '
        Me.txtOrange.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtOrange.Location = New System.Drawing.Point(12, 484)
        Me.txtOrange.Name = "txtOrange"
        Me.txtOrange.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtOrange.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtOrange, True)
        Me.txtOrange.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtOrange, OptionsSpelling13)
        Me.txtOrange.TabIndex = 79
        Me.txtOrange.Visible = False
        '
        'txtRed
        '
        Me.txtRed.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtRed.Location = New System.Drawing.Point(41, 484)
        Me.txtRed.Name = "txtRed"
        Me.txtRed.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtRed.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtRed, True)
        Me.txtRed.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtRed, OptionsSpelling14)
        Me.txtRed.TabIndex = 78
        Me.txtRed.Visible = False
        '
        'txtYellow
        '
        Me.txtYellow.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtYellow.Location = New System.Drawing.Point(128, 484)
        Me.txtYellow.Name = "txtYellow"
        Me.txtYellow.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtYellow.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtYellow, True)
        Me.txtYellow.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtYellow, OptionsSpelling15)
        Me.txtYellow.TabIndex = 80
        Me.txtYellow.Visible = False
        '
        'txtCyan
        '
        Me.txtCyan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtCyan.Location = New System.Drawing.Point(99, 484)
        Me.txtCyan.Name = "txtCyan"
        Me.txtCyan.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtCyan.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtCyan, True)
        Me.txtCyan.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtCyan, OptionsSpelling16)
        Me.txtCyan.TabIndex = 82
        Me.txtCyan.Visible = False
        '
        'frmBase
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1024, 538)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtLightPink)
        Me.Controls.Add(Me.txtPink)
        Me.Controls.Add(Me.txtLightBlue)
        Me.Controls.Add(Me.txtBlue)
        Me.Controls.Add(Me.txtLightGrey)
        Me.Controls.Add(Me.txtGrey)
        Me.Controls.Add(Me.txtLightGreen)
        Me.Controls.Add(Me.txtLightOrange)
        Me.Controls.Add(Me.txtLightRed)
        Me.Controls.Add(Me.txtLightYellow)
        Me.Controls.Add(Me.txtLightCyan)
        Me.Controls.Add(Me.txtGreen)
        Me.Controls.Add(Me.txtOrange)
        Me.Controls.Add(Me.txtRed)
        Me.Controls.Add(Me.txtYellow)
        Me.Controls.Add(Me.txtCyan)
        Me.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(6)
        Me.Name = "frmBase"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmBase"
        CType(Me.txtLightPink.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPink.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLightBlue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBlue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLightGrey.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGrey.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLightGreen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLightOrange.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLightRed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLightYellow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLightCyan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOrange.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCyan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Protected Friend WithEvents SpellChecker1 As DevExpress.XtraSpellChecker.SpellChecker
    Friend WithEvents txtLightPink As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtPink As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLightBlue As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtBlue As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLightGrey As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtGrey As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLightGreen As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLightOrange As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLightRed As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLightYellow As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLightCyan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtGreen As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtOrange As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRed As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtYellow As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCyan As DevExpress.XtraEditors.TextEdit
End Class
