﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBaseChildAddRemove
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.panTop = New DevExpress.XtraEditors.PanelControl()
        Me.txtChild = New System.Windows.Forms.TextBox()
        Me.btnNotes = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMedication = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.btnAllergies = New DevExpress.XtraEditors.SimpleButton()
        Me.panBottom = New DevExpress.XtraEditors.PanelControl()
        Me.btnAccept = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.scrStaff = New NurseryTablet.Scroller()
        CType(Me.panTop, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panTop.SuspendLayout()
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panBottom.SuspendLayout()
        Me.SuspendLayout()
        '
        'panTop
        '
        Me.panTop.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panTop.Appearance.Options.UseFont = True
        Me.panTop.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.panTop.Controls.Add(Me.scrStaff)
        Me.panTop.Controls.Add(Me.LabelControl2)
        Me.panTop.Controls.Add(Me.txtChild)
        Me.panTop.Controls.Add(Me.btnNotes)
        Me.panTop.Controls.Add(Me.btnMedication)
        Me.panTop.Controls.Add(Me.LabelControl3)
        Me.panTop.Controls.Add(Me.btnAllergies)
        Me.panTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.panTop.Location = New System.Drawing.Point(0, 0)
        Me.panTop.Name = "panTop"
        Me.panTop.Size = New System.Drawing.Size(1024, 88)
        Me.panTop.TabIndex = 0
        '
        'txtChild
        '
        Me.txtChild.Location = New System.Drawing.Point(79, 10)
        Me.txtChild.Name = "txtChild"
        Me.txtChild.Size = New System.Drawing.Size(503, 33)
        Me.txtChild.TabIndex = 9
        '
        'btnNotes
        '
        Me.btnNotes.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNotes.Appearance.Options.UseFont = True
        Me.btnNotes.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnNotes.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnNotes.Location = New System.Drawing.Point(878, 25)
        Me.btnNotes.Name = "btnNotes"
        Me.btnNotes.Size = New System.Drawing.Size(134, 38)
        Me.btnNotes.TabIndex = 8
        Me.btnNotes.Text = "Notes"
        '
        'btnMedication
        '
        Me.btnMedication.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMedication.Appearance.Options.UseFont = True
        Me.btnMedication.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnMedication.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnMedication.Location = New System.Drawing.Point(738, 25)
        Me.btnMedication.Name = "btnMedication"
        Me.btnMedication.Size = New System.Drawing.Size(134, 38)
        Me.btnMedication.TabIndex = 7
        Me.btnMedication.Text = "Medication"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.Location = New System.Drawing.Point(12, 13)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(44, 25)
        Me.LabelControl3.TabIndex = 2
        Me.LabelControl3.Text = "Child"
        '
        'btnAllergies
        '
        Me.btnAllergies.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAllergies.Appearance.Options.UseFont = True
        Me.btnAllergies.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnAllergies.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnAllergies.Location = New System.Drawing.Point(598, 25)
        Me.btnAllergies.Name = "btnAllergies"
        Me.btnAllergies.Size = New System.Drawing.Size(134, 38)
        Me.btnAllergies.TabIndex = 6
        Me.btnAllergies.Text = "Allergies"
        '
        'panBottom
        '
        Me.panBottom.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panBottom.Appearance.Options.UseFont = True
        Me.panBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.panBottom.Controls.Add(Me.btnAccept)
        Me.panBottom.Controls.Add(Me.btnCancel)
        Me.panBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panBottom.Location = New System.Drawing.Point(0, 482)
        Me.panBottom.Name = "panBottom"
        Me.panBottom.Size = New System.Drawing.Size(1024, 56)
        Me.panBottom.TabIndex = 1
        '
        'btnAccept
        '
        Me.btnAccept.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAccept.Appearance.Options.UseFont = True
        Me.btnAccept.Image = Global.NurseryTablet.My.Resources.Resources.success_32
        Me.btnAccept.Location = New System.Drawing.Point(706, 3)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(150, 45)
        Me.btnAccept.TabIndex = 0
        Me.btnAccept.Text = "Accept"
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Image = Global.NurseryTablet.My.Resources.Resources.cancel_32
        Me.btnCancel.Location = New System.Drawing.Point(862, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(150, 45)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "Cancel"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(12, 50)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(39, 25)
        Me.LabelControl2.TabIndex = 10
        Me.LabelControl2.Text = "Staff"
        '
        'scrStaff
        '
        Me.scrStaff.Location = New System.Drawing.Point(79, 47)
        Me.scrStaff.Margin = New System.Windows.Forms.Padding(6)
        Me.scrStaff.Name = "scrStaff"
        Me.scrStaff.ScrollerMode = NurseryTablet.Scroller.EnumScrollerMode.Staff
        Me.scrStaff.SelectedIndex = 0
        Me.scrStaff.Size = New System.Drawing.Size(503, 35)
        Me.scrStaff.TabIndex = 11
        Me.scrStaff.ValueID = Nothing
        Me.scrStaff.ValueText = ""
        '
        'frmBaseChildAddRemove
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1024, 538)
        Me.ControlBox = False
        Me.Controls.Add(Me.panTop)
        Me.Controls.Add(Me.panBottom)
        Me.Font = New System.Drawing.Font("Segoe UI", 14.25!)
        Me.Margin = New System.Windows.Forms.Padding(6)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBaseChildAddRemove"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmBaseChildNew"
        CType(Me.panTop, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panTop.ResumeLayout(False)
        Me.panTop.PerformLayout()
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panBottom.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents panTop As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnAccept As DevExpress.XtraEditors.SimpleButton
    Protected Friend WithEvents panBottom As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAllergies As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnNotes As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMedication As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtChild As System.Windows.Forms.TextBox
    Friend WithEvents scrStaff As NurseryTablet.Scroller
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
End Class
