﻿Imports NurseryTablet.SharedModule
Public Class frmLDAssessment

    Private m_AssessmentID As Guid? = Nothing

    Private Sub frmLDAssessment_Load(sender As Object, e As EventArgs) Handles Me.Load

        m_AssessmentID = Guid.NewGuid
        Clear()

        btnPhoto.Enabled = Parameters.Capture.CaptureEnabled

    End Sub

    Private Sub Clear()
        lblArea.Tag = ""
        lblArea.Text = ""
        lblStep.Tag = ""
        lblStep.Text = ""
        txtComments.Text = ""
        lblPhotoNumber.Text = ""
    End Sub

    Private Sub CheckExisting()

        'todo
        'called when the area and step has been selected - we will warn the user if a record already exists

    End Sub

    Protected Overrides Sub DisplayRecord()
        'stub
    End Sub

    Protected Overrides Function BeforeCommitUpdate() As Boolean
        If HasActivity Then
            Return CheckAnswers()
        Else
            Return True
        End If
    End Function

    Private Function HasActivity() As Boolean
        Return False
    End Function

    Private Function CheckAnswers() As Boolean

        If lblArea.Text = "" Then
            Msgbox("Please select the Developmental Area.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If lblStep.Text = "" Then
            Msgbox("Please select the Step and Letter.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If txtComments.Text = "" Then
            Msgbox("Please enter some comments.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        Return True

    End Function

    Protected Overrides Sub CommitUpdate()

        If lblStep.Tag.ToString = "" Then Exit Sub

        Dim _SQL As String

        _SQL = "select * from ChildCDAP" & _
               " where day_id = '" & TodayID & "'" & _
               " and child_id = '" & ChildID & "'" & _
               " and cdap_id = '" & lblStep.Tag.ToString & "'"

        Dim _DA As IDataAdapter = DAL.ReturnDataAdapter(_SQL)
        If Not _DA Is Nothing Then

            Dim _DR As DataRow
            Dim _DS As New DataSet
            _DA.Fill(_DS)

            If _DS.Tables(0).Rows.Count = 0 Then
                _DR = _DS.Tables(0).NewRow
            Else
                _DR = _DS.Tables(0).Rows(0)
            End If

            _DR("id") = m_AssessmentID
            _DR("child_id") = New Guid(ChildID)
            _DR("cdap_id") = New Guid(lblStep.Tag.ToString)
            _DR("day_id") = New Guid(TodayID)

            _DR("date") = Today

            If lblPhotoNumber.Text = "" Then
                _DR("photo_ref") = DBNull.Value
            Else
                _DR("photo_ref") = lblPhotoNumber.Text
            End If

            _DR("photo_imported") = False
            _DR("comments") = txtComments.Text

            _DR("staff_id") = txtStaff.Tag
            _DR("staff_name") = txtStaff.Text
            _DR("stamp") = Now

            If _DS.Tables(0).Rows.Count = 0 Then _DS.Tables(0).Rows.Add(_DR)

            DAL.UpdateDB(_DA, _DS)

        End If

    End Sub

    Private Sub btnPhotoNumber_Click(sender As Object, e As EventArgs) Handles btnPhotoNumber.Click
        lblPhotoNumber.Text = OSK("Enter Photo Number", False, True, "")
    End Sub

    Private Sub btnArea_Click(sender As Object, e As EventArgs) Handles btnArea.Click

        Dim _SQL As String = "select id, name from CDAPAreas" & _
                             " order by seq"

        Dim _P As Pair = SharedModule.ReturnButtonSQL(_SQL, "Select Developmental Area")
        If _P IsNot Nothing Then
            lblArea.Tag = _P.Code
            lblArea.Text = _P.Text
        End If

    End Sub

    Private Sub btnStep_Click(sender As Object, e As EventArgs) Handles btnStep.Click

        Dim _SQL As String = "select id, stepletter + '. ' + name as 'name' from CDAPDefinitions" & _
                             " where area = '" + lblArea.Text + "'" & _
                             " order by step, letter"

        Dim _P As Pair = SharedModule.ReturnButtonSQL(_SQL, "Select Developmental Area")
        If _P IsNot Nothing Then
            lblStep.Tag = _P.Code
            lblStep.Text = _P.Text
            CheckExisting()
        End If

    End Sub

    Private Sub btnComments_Click(sender As Object, e As EventArgs) Handles btnComments.Click
        txtComments.Text = OSK("Enter Comments", True, True, "")
    End Sub

End Class
