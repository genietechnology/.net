﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChild
    Inherits NurseryTablet.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmChild))
        Me.btnSuncream = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMedAuth = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCrossCheck = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMoveRoom = New DevExpress.XtraEditors.SimpleButton()
        Me.btnFeedback = New DevExpress.XtraEditors.SimpleButton()
        Me.btnObs = New DevExpress.XtraEditors.SimpleButton()
        Me.btnIncidents = New DevExpress.XtraEditors.SimpleButton()
        Me.btnPhoto = New DevExpress.XtraEditors.SimpleButton()
        Me.btnContacts = New DevExpress.XtraEditors.SimpleButton()
        Me.btnReport = New DevExpress.XtraEditors.SimpleButton()
        Me.btnLogMedicine = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMilk = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSleep = New DevExpress.XtraEditors.SimpleButton()
        Me.btnToilet = New DevExpress.XtraEditors.SimpleButton()
        Me.btnFood = New DevExpress.XtraEditors.SimpleButton()
        Me.btnRequests = New DevExpress.XtraEditors.SimpleButton()
        Me.btnExit = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCheckOutChild = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCheckInChild = New DevExpress.XtraEditors.SimpleButton()
        Me.picPhoto = New System.Windows.Forms.PictureBox()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.tabPersonal = New DevExpress.XtraTab.XtraTabPage()
        Me.vgBasic = New DevExpress.XtraVerticalGrid.VGridControl()
        Me.tabMedical = New DevExpress.XtraTab.XtraTabPage()
        Me.vgMedical = New DevExpress.XtraVerticalGrid.VGridControl()
        Me.tabConsent = New DevExpress.XtraTab.XtraTabPage()
        Me.tgConsent = New NurseryTablet.TouchGrid()
        Me.tabAllergies = New DevExpress.XtraTab.XtraTabPage()
        Me.tgAllergies = New NurseryTablet.TouchGrid()
        CType(Me.picPhoto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.tabPersonal.SuspendLayout()
        CType(Me.vgBasic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabMedical.SuspendLayout()
        CType(Me.vgMedical, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabConsent.SuspendLayout()
        Me.tabAllergies.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        '
        'btnSuncream
        '
        Me.btnSuncream.Enabled = False
        Me.btnSuncream.Image = Global.NurseryTablet.My.Resources.Resources.Sunny_64
        Me.btnSuncream.Location = New System.Drawing.Point(746, 589)
        Me.btnSuncream.Name = "btnSuncream"
        Me.btnSuncream.Size = New System.Drawing.Size(250, 90)
        Me.btnSuncream.TabIndex = 19
        Me.btnSuncream.Text = "Suncream"
        '
        'btnMedAuth
        '
        Me.btnMedAuth.Image = Global.NurseryTablet.My.Resources.Resources.add_64
        Me.btnMedAuth.Location = New System.Drawing.Point(746, 492)
        Me.btnMedAuth.Name = "btnMedAuth"
        Me.btnMedAuth.Size = New System.Drawing.Size(250, 90)
        Me.btnMedAuth.TabIndex = 23
        Me.btnMedAuth.Text = "Medicine" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Authorisation"
        '
        'btnCrossCheck
        '
        Me.btnCrossCheck.Enabled = False
        Me.btnCrossCheck.Image = CType(resources.GetObject("btnCrossCheck.Image"), System.Drawing.Image)
        Me.btnCrossCheck.Location = New System.Drawing.Point(1002, 299)
        Me.btnCrossCheck.Name = "btnCrossCheck"
        Me.btnCrossCheck.Size = New System.Drawing.Size(250, 90)
        Me.btnCrossCheck.TabIndex = 18
        Me.btnCrossCheck.Text = "Cross Check"
        '
        'btnMoveRoom
        '
        Me.btnMoveRoom.Enabled = False
        Me.btnMoveRoom.Image = Global.NurseryTablet.My.Resources.Resources.order_64
        Me.btnMoveRoom.Location = New System.Drawing.Point(490, 108)
        Me.btnMoveRoom.Name = "btnMoveRoom"
        Me.btnMoveRoom.Size = New System.Drawing.Size(250, 90)
        Me.btnMoveRoom.TabIndex = 8
        Me.btnMoveRoom.Text = "Move Room"
        '
        'btnFeedback
        '
        Me.btnFeedback.Image = CType(resources.GetObject("btnFeedback.Image"), System.Drawing.Image)
        Me.btnFeedback.Location = New System.Drawing.Point(746, 204)
        Me.btnFeedback.Name = "btnFeedback"
        Me.btnFeedback.Size = New System.Drawing.Size(250, 90)
        Me.btnFeedback.TabIndex = 5
        Me.btnFeedback.Text = "Personal" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Feedback"
        '
        'btnObs
        '
        Me.btnObs.Image = CType(resources.GetObject("btnObs.Image"), System.Drawing.Image)
        Me.btnObs.Location = New System.Drawing.Point(746, 300)
        Me.btnObs.Name = "btnObs"
        Me.btnObs.Size = New System.Drawing.Size(250, 90)
        Me.btnObs.TabIndex = 9
        Me.btnObs.Text = "Observations"
        '
        'btnIncidents
        '
        Me.btnIncidents.Image = CType(resources.GetObject("btnIncidents.Image"), System.Drawing.Image)
        Me.btnIncidents.Location = New System.Drawing.Point(746, 395)
        Me.btnIncidents.Name = "btnIncidents"
        Me.btnIncidents.Size = New System.Drawing.Size(250, 90)
        Me.btnIncidents.TabIndex = 13
        Me.btnIncidents.Text = "Incidents"
        '
        'btnPhoto
        '
        Me.btnPhoto.Image = CType(resources.GetObject("btnPhoto.Image"), System.Drawing.Image)
        Me.btnPhoto.Location = New System.Drawing.Point(746, 108)
        Me.btnPhoto.Margin = New System.Windows.Forms.Padding(6)
        Me.btnPhoto.Name = "btnPhoto"
        Me.btnPhoto.Size = New System.Drawing.Size(250, 90)
        Me.btnPhoto.TabIndex = 7
        Me.btnPhoto.Text = "Profile Picture"
        '
        'btnContacts
        '
        Me.btnContacts.Image = CType(resources.GetObject("btnContacts.Image"), System.Drawing.Image)
        Me.btnContacts.Location = New System.Drawing.Point(1002, 395)
        Me.btnContacts.Name = "btnContacts"
        Me.btnContacts.Size = New System.Drawing.Size(250, 90)
        Me.btnContacts.TabIndex = 10
        Me.btnContacts.Text = "View Contacts"
        '
        'btnReport
        '
        Me.btnReport.Image = CType(resources.GetObject("btnReport.Image"), System.Drawing.Image)
        Me.btnReport.Location = New System.Drawing.Point(1002, 204)
        Me.btnReport.Name = "btnReport"
        Me.btnReport.Size = New System.Drawing.Size(250, 90)
        Me.btnReport.TabIndex = 6
        Me.btnReport.Text = "View Report"
        '
        'btnLogMedicine
        '
        Me.btnLogMedicine.Image = CType(resources.GetObject("btnLogMedicine.Image"), System.Drawing.Image)
        Me.btnLogMedicine.Location = New System.Drawing.Point(1002, 492)
        Me.btnLogMedicine.Name = "btnLogMedicine"
        Me.btnLogMedicine.Size = New System.Drawing.Size(250, 90)
        Me.btnLogMedicine.TabIndex = 14
        Me.btnLogMedicine.Text = "Give Medicine"
        '
        'btnMilk
        '
        Me.btnMilk.Image = CType(resources.GetObject("btnMilk.Image"), System.Drawing.Image)
        Me.btnMilk.Location = New System.Drawing.Point(490, 301)
        Me.btnMilk.Name = "btnMilk"
        Me.btnMilk.Size = New System.Drawing.Size(250, 90)
        Me.btnMilk.TabIndex = 15
        Me.btnMilk.Text = "Milk Records"
        '
        'btnSleep
        '
        Me.btnSleep.Image = CType(resources.GetObject("btnSleep.Image"), System.Drawing.Image)
        Me.btnSleep.Location = New System.Drawing.Point(490, 493)
        Me.btnSleep.Name = "btnSleep"
        Me.btnSleep.Size = New System.Drawing.Size(250, 90)
        Me.btnSleep.TabIndex = 16
        Me.btnSleep.Text = "Sleep Records"
        '
        'btnToilet
        '
        Me.btnToilet.Image = CType(resources.GetObject("btnToilet.Image"), System.Drawing.Image)
        Me.btnToilet.Location = New System.Drawing.Point(490, 204)
        Me.btnToilet.Name = "btnToilet"
        Me.btnToilet.Size = New System.Drawing.Size(250, 90)
        Me.btnToilet.TabIndex = 11
        Me.btnToilet.Text = "Nappies && Toilet"
        '
        'btnFood
        '
        Me.btnFood.Image = CType(resources.GetObject("btnFood.Image"), System.Drawing.Image)
        Me.btnFood.Location = New System.Drawing.Point(490, 397)
        Me.btnFood.Name = "btnFood"
        Me.btnFood.Size = New System.Drawing.Size(250, 90)
        Me.btnFood.TabIndex = 12
        Me.btnFood.Text = "Food"
        '
        'btnRequests
        '
        Me.btnRequests.Image = CType(resources.GetObject("btnRequests.Image"), System.Drawing.Image)
        Me.btnRequests.Location = New System.Drawing.Point(490, 589)
        Me.btnRequests.Name = "btnRequests"
        Me.btnRequests.Size = New System.Drawing.Size(250, 90)
        Me.btnRequests.TabIndex = 21
        Me.btnRequests.Text = "Requests"
        '
        'btnExit
        '
        Me.btnExit.Image = CType(resources.GetObject("btnExit.Image"), System.Drawing.Image)
        Me.btnExit.Location = New System.Drawing.Point(1002, 588)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(250, 90)
        Me.btnExit.TabIndex = 22
        Me.btnExit.Text = "Close"
        '
        'btnCheckOutChild
        '
        Me.btnCheckOutChild.Image = CType(resources.GetObject("btnCheckOutChild.Image"), System.Drawing.Image)
        Me.btnCheckOutChild.Location = New System.Drawing.Point(746, 12)
        Me.btnCheckOutChild.Name = "btnCheckOutChild"
        Me.btnCheckOutChild.Size = New System.Drawing.Size(250, 90)
        Me.btnCheckOutChild.TabIndex = 4
        Me.btnCheckOutChild.Text = "Check Out"
        '
        'btnCheckInChild
        '
        Me.btnCheckInChild.Image = CType(resources.GetObject("btnCheckInChild.Image"), System.Drawing.Image)
        Me.btnCheckInChild.Location = New System.Drawing.Point(490, 12)
        Me.btnCheckInChild.Name = "btnCheckInChild"
        Me.btnCheckInChild.Size = New System.Drawing.Size(250, 90)
        Me.btnCheckInChild.TabIndex = 3
        Me.btnCheckInChild.Text = "Check In"
        '
        'picPhoto
        '
        Me.picPhoto.BackColor = System.Drawing.Color.White
        Me.picPhoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picPhoto.Location = New System.Drawing.Point(1002, 12)
        Me.picPhoto.Name = "picPhoto"
        Me.picPhoto.Size = New System.Drawing.Size(250, 186)
        Me.picPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picPhoto.TabIndex = 78
        Me.picPhoto.TabStop = False
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.AppearancePage.Header.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XtraTabControl1.AppearancePage.Header.Options.UseFont = True
        Me.XtraTabControl1.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom
        Me.XtraTabControl1.Location = New System.Drawing.Point(12, 12)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.tabPersonal
        Me.XtraTabControl1.Size = New System.Drawing.Size(472, 667)
        Me.XtraTabControl1.TabIndex = 79
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabPersonal, Me.tabMedical, Me.tabConsent, Me.tabAllergies})
        '
        'tabPersonal
        '
        Me.tabPersonal.Controls.Add(Me.vgBasic)
        Me.tabPersonal.Name = "tabPersonal"
        Me.tabPersonal.Size = New System.Drawing.Size(466, 634)
        Me.tabPersonal.Text = "Personal"
        '
        'vgBasic
        '
        Me.vgBasic.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.vgBasic.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView
        Me.vgBasic.Location = New System.Drawing.Point(3, 3)
        Me.vgBasic.Name = "vgBasic"
        Me.vgBasic.OptionsBehavior.Editable = False
        Me.vgBasic.Size = New System.Drawing.Size(460, 628)
        Me.vgBasic.TabIndex = 4
        Me.vgBasic.TabStop = False
        '
        'tabMedical
        '
        Me.tabMedical.Controls.Add(Me.vgMedical)
        Me.tabMedical.Name = "tabMedical"
        Me.tabMedical.Size = New System.Drawing.Size(466, 634)
        Me.tabMedical.Text = "Medical"
        '
        'vgMedical
        '
        Me.vgMedical.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.vgMedical.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView
        Me.vgMedical.Location = New System.Drawing.Point(3, 3)
        Me.vgMedical.Name = "vgMedical"
        Me.vgMedical.OptionsBehavior.Editable = False
        Me.vgMedical.Size = New System.Drawing.Size(460, 628)
        Me.vgMedical.TabIndex = 5
        Me.vgMedical.TabStop = False
        '
        'tabConsent
        '
        Me.tabConsent.Controls.Add(Me.tgConsent)
        Me.tabConsent.Name = "tabConsent"
        Me.tabConsent.Size = New System.Drawing.Size(466, 634)
        Me.tabConsent.Text = "Consent"
        '
        'tgConsent
        '
        Me.tgConsent.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tgConsent.HideFirstColumn = False
        Me.tgConsent.Location = New System.Drawing.Point(3, 3)
        Me.tgConsent.Name = "tgConsent"
        Me.tgConsent.Size = New System.Drawing.Size(460, 628)
        Me.tgConsent.TabIndex = 10
        '
        'tabAllergies
        '
        Me.tabAllergies.Controls.Add(Me.tgAllergies)
        Me.tabAllergies.Name = "tabAllergies"
        Me.tabAllergies.Size = New System.Drawing.Size(466, 634)
        Me.tabAllergies.Text = "Allergies"
        '
        'tgAllergies
        '
        Me.tgAllergies.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tgAllergies.HideFirstColumn = False
        Me.tgAllergies.Location = New System.Drawing.Point(3, 3)
        Me.tgAllergies.Name = "tgAllergies"
        Me.tgAllergies.Size = New System.Drawing.Size(460, 628)
        Me.tgAllergies.TabIndex = 11
        '
        'frmChild
        '
        Me.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.Appearance.Options.UseBackColor = True
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(1264, 691)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Controls.Add(Me.picPhoto)
        Me.Controls.Add(Me.btnMedAuth)
        Me.Controls.Add(Me.btnCrossCheck)
        Me.Controls.Add(Me.btnMoveRoom)
        Me.Controls.Add(Me.btnFeedback)
        Me.Controls.Add(Me.btnSuncream)
        Me.Controls.Add(Me.btnObs)
        Me.Controls.Add(Me.btnIncidents)
        Me.Controls.Add(Me.btnPhoto)
        Me.Controls.Add(Me.btnContacts)
        Me.Controls.Add(Me.btnReport)
        Me.Controls.Add(Me.btnLogMedicine)
        Me.Controls.Add(Me.btnMilk)
        Me.Controls.Add(Me.btnSleep)
        Me.Controls.Add(Me.btnToilet)
        Me.Controls.Add(Me.btnFood)
        Me.Controls.Add(Me.btnRequests)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnCheckOutChild)
        Me.Controls.Add(Me.btnCheckInChild)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(1280, 730)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(1280, 730)
        Me.Name = "frmChild"
        Me.Text = "Child Record"
        Me.Controls.SetChildIndex(Me.btnCheckInChild, 0)
        Me.Controls.SetChildIndex(Me.btnCheckOutChild, 0)
        Me.Controls.SetChildIndex(Me.btnExit, 0)
        Me.Controls.SetChildIndex(Me.btnRequests, 0)
        Me.Controls.SetChildIndex(Me.btnFood, 0)
        Me.Controls.SetChildIndex(Me.btnToilet, 0)
        Me.Controls.SetChildIndex(Me.btnSleep, 0)
        Me.Controls.SetChildIndex(Me.btnMilk, 0)
        Me.Controls.SetChildIndex(Me.btnLogMedicine, 0)
        Me.Controls.SetChildIndex(Me.btnReport, 0)
        Me.Controls.SetChildIndex(Me.btnContacts, 0)
        Me.Controls.SetChildIndex(Me.btnPhoto, 0)
        Me.Controls.SetChildIndex(Me.btnIncidents, 0)
        Me.Controls.SetChildIndex(Me.btnObs, 0)
        Me.Controls.SetChildIndex(Me.btnSuncream, 0)
        Me.Controls.SetChildIndex(Me.btnFeedback, 0)
        Me.Controls.SetChildIndex(Me.btnMoveRoom, 0)
        Me.Controls.SetChildIndex(Me.btnCrossCheck, 0)
        Me.Controls.SetChildIndex(Me.btnMedAuth, 0)
        Me.Controls.SetChildIndex(Me.picPhoto, 0)
        Me.Controls.SetChildIndex(Me.XtraTabControl1, 0)
        CType(Me.picPhoto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.tabPersonal.ResumeLayout(False)
        CType(Me.vgBasic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabMedical.ResumeLayout(False)
        CType(Me.vgMedical, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabConsent.ResumeLayout(False)
        Me.tabAllergies.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnCheckOutChild As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCheckInChild As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnExit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMilk As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSleep As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnToilet As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnFood As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRequests As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnLogMedicine As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnReport As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnContacts As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnPhoto As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnIncidents As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnObs As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMoveRoom As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnFeedback As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSuncream As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCrossCheck As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMedAuth As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents picPhoto As System.Windows.Forms.PictureBox
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents tabPersonal As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabMedical As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabConsent As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabAllergies As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents vgBasic As DevExpress.XtraVerticalGrid.VGridControl
    Friend WithEvents vgMedical As DevExpress.XtraVerticalGrid.VGridControl
    Friend WithEvents tgConsent As NurseryTablet.TouchGrid
    Friend WithEvents tgAllergies As NurseryTablet.TouchGrid

End Class
