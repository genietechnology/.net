﻿Imports NurseryTablet.Business
Imports NurseryTablet.SharedModule

Public Class frmRequests

    Private m_RequestCount As Integer

    Private Sub frmRequest_AcceptChanged(sender As Object, e As AcceptArgs) Handles Me.AcceptChanged
        btnAdd.Enabled = e.Enabled
    End Sub

    Private Sub frmRequests_ChildChanged(sender As Object, e As EventArgs) Handles Me.ChildChanged
        DisplayRecord()
    End Sub

    Protected Overrides Sub CommitUpdate()

        SharedModule.DeleteRequests(Day.TodayIDString, txtChild.Tag.ToString)

        Dim _i As Integer
        For _i = 1 To 12
            Dim _ctrl As Control() = Me.Controls.Find("lblRequest" + _i.ToString, True)
            If _ctrl IsNot Nothing AndAlso _ctrl.Length = 1 Then
                If _ctrl(0).Visible AndAlso _ctrl(0).Tag.ToString <> "" Then

                    SharedModule.LogRequest(Day.TodayIDString, txtChild.Tag.ToString, txtChild.Text, _
                                            _ctrl(0).Tag.ToString, _ctrl(0).Text, txtStaff.Tag.ToString, txtStaff.Text)

                End If
            End If

        Next

    End Sub

    Private Sub btnAdd_Click(sender As System.Object, e As System.EventArgs) Handles btnAdd.Click

        Dim _SQL = "select id, description from Requests order by description"
        Dim _Return As Pair = ReturnButtonSQL(_SQL, "Select Request")
        If Not _Return Is Nothing AndAlso _Return.Code <> "" Then
            m_RequestCount += 1
            SetControl("lblRequest", m_RequestCount, _Return.Code, _Return.Text, True)
            SetControl("btnRequest", m_RequestCount, "", "Remove", True)
        End If

    End Sub

    Protected Overrides Sub DisplayRecord()

        Clear()

        If Not Me.ChildrenPopulated Then Exit Sub
        If Me.ChildID = "" Then Exit Sub

        Dim _Activity As List(Of NurseryGenieData.Activity) = Business.Activity.ReturnActivity(txtChild.Tag.ToString, Activity.EnumActivityType.Request, Enums.EnumActivityOrder.Stamp)
        If _Activity IsNot Nothing Then
            m_RequestCount = 0
            For Each _A In _Activity
                m_RequestCount += 1
                SetControl("lblRequest", m_RequestCount, _A.Value1, _A.Description, True)
                SetControl("btnRequest", m_RequestCount, "", "Remove", True)
            Next
        End If

    End Sub

    Private Sub Clear()

        Dim i As Integer = 1
        For i = 1 To 12
            SetControl("lblRequest", i, "", "", False)
            SetControl("btnRequest", i, "", "", False)
        Next

    End Sub

    Private Sub SetControl(ByVal ControlName As String, ByVal ControlIndex As Integer, ByVal Tag As String, ByVal Text As String, ByVal Visible As Boolean)

        Dim _name As String = ControlName + ControlIndex.ToString
        Dim _ctrl As Control() = Me.Controls.Find(_name, True)
        If _ctrl.Length = 1 Then
            _ctrl(0).Visible = Visible
            _ctrl(0).Text = Text
            _ctrl(0).Tag = Tag
        End If
    End Sub

    Private Sub btnRequest12_Click(sender As System.Object, e As System.EventArgs) Handles btnRequest12.Click
        SetControl("lblRequest", 12, "", "", False)
        SetControl("btnRequest", 12, "", "", False)
        SaveAndReDisplay()
    End Sub

    Private Sub btnRequest11_Click(sender As System.Object, e As System.EventArgs) Handles btnRequest11.Click
        SetControl("lblRequest", 11, "", "", False)
        SetControl("btnRequest", 11, "", "", False)
        SaveAndReDisplay()
    End Sub

    Private Sub btnRequest10_Click(sender As System.Object, e As System.EventArgs) Handles btnRequest10.Click
        SetControl("lblRequest", 10, "", "", False)
        SetControl("btnRequest", 10, "", "", False)
        SaveAndReDisplay()
    End Sub

    Private Sub btnRequest9_Click(sender As System.Object, e As System.EventArgs) Handles btnRequest9.Click
        SetControl("lblRequest", 9, "", "", False)
        SetControl("btnRequest", 9, "", "", False)
        SaveAndReDisplay()
    End Sub

    Private Sub btnRequest8_Click(sender As System.Object, e As System.EventArgs) Handles btnRequest8.Click
        SetControl("lblRequest", 8, "", "", False)
        SetControl("btnRequest", 8, "", "", False)
        SaveAndReDisplay()
    End Sub

    Private Sub btnRequest7_Click(sender As System.Object, e As System.EventArgs) Handles btnRequest7.Click
        SetControl("lblRequest", 7, "", "", False)
        SetControl("btnRequest", 7, "", "", False)
        SaveAndReDisplay()
    End Sub

    Private Sub btnRequest6_Click(sender As System.Object, e As System.EventArgs) Handles btnRequest6.Click
        SetControl("lblRequest", 6, "", "", False)
        SetControl("btnRequest", 6, "", "", False)
        SaveAndReDisplay()
    End Sub

    Private Sub btnRequest5_Click(sender As System.Object, e As System.EventArgs) Handles btnRequest5.Click
        SetControl("lblRequest", 5, "", "", False)
        SetControl("btnRequest", 5, "", "", False)
        SaveAndReDisplay()
    End Sub

    Private Sub btnRequest4_Click(sender As System.Object, e As System.EventArgs) Handles btnRequest4.Click
        SetControl("lblRequest", 4, "", "", False)
        SetControl("btnRequest", 4, "", "", False)
        SaveAndReDisplay()
    End Sub

    Private Sub btnRequest3_Click(sender As System.Object, e As System.EventArgs) Handles btnRequest3.Click
        SetControl("lblRequest", 3, "", "", False)
        SetControl("btnRequest", 3, "", "", False)
        SaveAndReDisplay()
    End Sub

    Private Sub btnRequest2_Click(sender As System.Object, e As System.EventArgs) Handles btnRequest2.Click
        SetControl("lblRequest", 2, "", "", False)
        SetControl("btnRequest", 2, "", "", False)
        SaveAndReDisplay()
    End Sub

    Private Sub btnRequest1_Click(sender As System.Object, e As System.EventArgs) Handles btnRequest1.Click
        SetControl("lblRequest", 1, "", "", False)
        SetControl("btnRequest", 1, "", "", False)
        SaveAndReDisplay()
    End Sub

    Private Sub SaveAndReDisplay()
        CommitUpdate()
        DisplayRecord()
    End Sub

End Class
