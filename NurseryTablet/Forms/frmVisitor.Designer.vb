﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVisitor
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmVisitor))
        Me.panBottom = New DevExpress.XtraEditors.PanelControl()
        Me.Splitter2 = New System.Windows.Forms.Splitter()
        Me.btnAccept = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.fldCarReg = New NurseryTablet.Field()
        Me.fldName = New NurseryTablet.Field()
        Me.fldVisiting = New NurseryTablet.Field()
        Me.fldCompany = New NurseryTablet.Field()
        Me.btnSig = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panBottom.SuspendLayout()
        Me.SuspendLayout()
        '
        'panBottom
        '
        Me.panBottom.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panBottom.Appearance.Options.UseFont = True
        Me.panBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.panBottom.Controls.Add(Me.btnSig)
        Me.panBottom.Controls.Add(Me.Splitter2)
        Me.panBottom.Controls.Add(Me.btnAccept)
        Me.panBottom.Controls.Add(Me.btnCancel)
        Me.panBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panBottom.Location = New System.Drawing.Point(0, 238)
        Me.panBottom.Name = "panBottom"
        Me.panBottom.Size = New System.Drawing.Size(1019, 56)
        Me.panBottom.TabIndex = 0
        '
        'Splitter2
        '
        Me.Splitter2.Location = New System.Drawing.Point(0, 0)
        Me.Splitter2.Name = "Splitter2"
        Me.Splitter2.Size = New System.Drawing.Size(3, 56)
        Me.Splitter2.TabIndex = 0
        Me.Splitter2.TabStop = False
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAccept.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAccept.Appearance.Options.UseFont = True
        Me.btnAccept.Image = CType(resources.GetObject("btnAccept.Image"), System.Drawing.Image)
        Me.btnAccept.Location = New System.Drawing.Point(701, 3)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(150, 45)
        Me.btnAccept.TabIndex = 0
        Me.btnAccept.Text = "Accept"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Image = CType(resources.GetObject("btnCancel.Image"), System.Drawing.Image)
        Me.btnCancel.Location = New System.Drawing.Point(857, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(150, 45)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "Cancel"
        '
        'fldCarReg
        '
        Me.fldCarReg.ButtonCustom = False
        Me.fldCarReg.ButtonSQL = Nothing
        Me.fldCarReg.ButtonText = ""
        Me.fldCarReg.ButtonType = NurseryTablet.Field.EnumButtonType.Enter
        Me.fldCarReg.ButtonWidth = 50.0!
        Me.fldCarReg.FamilyID = Nothing
        Me.fldCarReg.HideText = False
        Me.fldCarReg.LabelText = "Car Registration"
        Me.fldCarReg.LabelWidth = 150.0!
        Me.fldCarReg.Location = New System.Drawing.Point(12, 183)
        Me.fldCarReg.MaximumSize = New System.Drawing.Size(1000, 51)
        Me.fldCarReg.MinimumSize = New System.Drawing.Size(210, 51)
        Me.fldCarReg.Name = "fldCarReg"
        Me.fldCarReg.QuickTextList = Nothing
        Me.fldCarReg.Size = New System.Drawing.Size(995, 51)
        Me.fldCarReg.TabIndex = 5
        Me.fldCarReg.ValueID = Nothing
        Me.fldCarReg.ValueText = ""
        '
        'fldName
        '
        Me.fldName.ButtonCustom = False
        Me.fldName.ButtonSQL = Nothing
        Me.fldName.ButtonText = ""
        Me.fldName.ButtonType = NurseryTablet.Field.EnumButtonType.Enter
        Me.fldName.ButtonWidth = 50.0!
        Me.fldName.FamilyID = Nothing
        Me.fldName.HideText = False
        Me.fldName.LabelText = "Visitor Name"
        Me.fldName.LabelWidth = 150.0!
        Me.fldName.Location = New System.Drawing.Point(12, 12)
        Me.fldName.MaximumSize = New System.Drawing.Size(1000, 51)
        Me.fldName.MinimumSize = New System.Drawing.Size(210, 51)
        Me.fldName.Name = "fldName"
        Me.fldName.QuickTextList = Nothing
        Me.fldName.Size = New System.Drawing.Size(995, 51)
        Me.fldName.TabIndex = 4
        Me.fldName.ValueID = Nothing
        Me.fldName.ValueText = ""
        '
        'fldVisiting
        '
        Me.fldVisiting.ButtonCustom = False
        Me.fldVisiting.ButtonSQL = Nothing
        Me.fldVisiting.ButtonText = ""
        Me.fldVisiting.ButtonType = NurseryTablet.Field.EnumButtonType.Enter
        Me.fldVisiting.ButtonWidth = 50.0!
        Me.fldVisiting.FamilyID = Nothing
        Me.fldVisiting.HideText = False
        Me.fldVisiting.LabelText = "Visiting"
        Me.fldVisiting.LabelWidth = 150.0!
        Me.fldVisiting.Location = New System.Drawing.Point(12, 126)
        Me.fldVisiting.MaximumSize = New System.Drawing.Size(1000, 51)
        Me.fldVisiting.MinimumSize = New System.Drawing.Size(210, 51)
        Me.fldVisiting.Name = "fldVisiting"
        Me.fldVisiting.QuickTextList = Nothing
        Me.fldVisiting.Size = New System.Drawing.Size(995, 51)
        Me.fldVisiting.TabIndex = 3
        Me.fldVisiting.ValueID = Nothing
        Me.fldVisiting.ValueText = ""
        '
        'fldCompany
        '
        Me.fldCompany.ButtonCustom = False
        Me.fldCompany.ButtonSQL = Nothing
        Me.fldCompany.ButtonText = ""
        Me.fldCompany.ButtonType = NurseryTablet.Field.EnumButtonType.Enter
        Me.fldCompany.ButtonWidth = 50.0!
        Me.fldCompany.FamilyID = Nothing
        Me.fldCompany.HideText = False
        Me.fldCompany.LabelText = "Company"
        Me.fldCompany.LabelWidth = 150.0!
        Me.fldCompany.Location = New System.Drawing.Point(12, 69)
        Me.fldCompany.MaximumSize = New System.Drawing.Size(1000, 51)
        Me.fldCompany.MinimumSize = New System.Drawing.Size(210, 51)
        Me.fldCompany.Name = "fldCompany"
        Me.fldCompany.QuickTextList = Nothing
        Me.fldCompany.Size = New System.Drawing.Size(995, 51)
        Me.fldCompany.TabIndex = 2
        Me.fldCompany.ValueID = Nothing
        Me.fldCompany.ValueText = ""
        '
        'btnSig
        '
        Me.btnSig.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSig.Appearance.Options.UseFont = True
        Me.btnSig.Image = Global.NurseryTablet.My.Resources.Resources.pen_32
        Me.btnSig.Location = New System.Drawing.Point(9, 3)
        Me.btnSig.Name = "btnSig"
        Me.btnSig.Size = New System.Drawing.Size(190, 45)
        Me.btnSig.TabIndex = 3
        Me.btnSig.Text = "Signature"
        '
        'frmVisitor
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1019, 294)
        Me.ControlBox = False
        Me.Controls.Add(Me.fldCarReg)
        Me.Controls.Add(Me.fldName)
        Me.Controls.Add(Me.fldVisiting)
        Me.Controls.Add(Me.fldCompany)
        Me.Controls.Add(Me.panBottom)
        Me.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmVisitor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panBottom.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Protected Friend WithEvents panBottom As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnAccept As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Splitter2 As System.Windows.Forms.Splitter
    Friend WithEvents fldCompany As NurseryTablet.Field
    Friend WithEvents fldVisiting As NurseryTablet.Field
    Friend WithEvents fldName As NurseryTablet.Field
    Friend WithEvents fldCarReg As NurseryTablet.Field
    Friend WithEvents btnSig As DevExpress.XtraEditors.SimpleButton
End Class
