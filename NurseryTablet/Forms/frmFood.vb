﻿Option Strict On

Imports NurseryTablet.Business
Imports NurseryTablet.Business.Food
Imports NurseryTablet.SharedModule
Imports NurseryTablet.SharedModule.Enums

Public Class frmFood

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        SetColours()

    End Sub

    Private m_None As Color
    Private m_Some As Color
    Private m_Most As Color
    Private m_All As Color

    Private Sub SetColours()
        m_None = btnNone1.BackColor
        m_Some = btnSome1.BackColor
        m_Most = btnMost1.BackColor
        m_All = btnAll1.BackColor
    End Sub

    Private Sub frmFood_ChildChanged(sender As Object, e As EventArgs) Handles Me.ChildChanged
        ClearButtons()
    End Sub

    Private Sub frmFood_AcceptChanged(sender As Object, e As AcceptArgs) Handles Me.AcceptChanged

        btnAdd.Enabled = e.Enabled
        btnRemoveAll.Enabled = e.Enabled

        'capture the meal button, before we disable it
        Dim _btn As Button = Nothing
        If e.Enabled = False Then
            _btn = ReturnMealButton()
        End If

        ToggleMealButtons(e.Enabled)

        If e.Enabled = False Then
            If _btn IsNot Nothing Then
                _btn.Enabled = True
                _btn.BackColor = Color.Yellow
                Application.DoEvents()
            End If
        End If

    End Sub

    Protected Overrides Function BeforeCommitUpdate() As Boolean
        If HasActivity() Then
            Return CheckAnswers()
        Else
            Return True
        End If
    End Function

    Private Function HasActivity() As Boolean

        Dim _btn As Button = ReturnMealButton()
        If _btn Is Nothing Then
            Return False
        Else
            For i = 1 To 8
                Dim _name As String = "btnFood" + i.ToString
                Dim _ctrl As Control() = Me.Controls.Find(_name, True)
                If _ctrl.Length = 1 Then
                    If _ctrl(0).Enabled Then
                        If _ctrl(0).AccessibleDescription <> "-1" Then
                            Return True
                        End If
                    End If
                End If
            Next
            Return False
        End If

    End Function

    Protected Overrides Sub CommitUpdate()
        Save()
    End Sub

    Protected Overrides Sub DisplayRecord()

        If BabyFood Then
            btnAdd.Text = "Add Baby Food"
            ToggleMealButtons(False)
            DisplayMealDetails(EnumMealType.BabyFood, g_BabyFoodMealID)
        Else
            btnAdd.Text = "Add Food Item"
            SetDay()
            ToggleMealButtons(True)
        End If

    End Sub

    Private Sub EnableMealButton(ByRef ButtonIn As Button, ByVal Enable As Boolean)
        If Enable Then
            If ButtonIn.AccessibleDescription = "" Then
                ButtonIn.Enabled = False
            Else
                ButtonIn.Enabled = True
            End If
        Else
            ButtonIn.Enabled = False
            ButtonIn.BackColor = SystemColors.Control
        End If
    End Sub

    Private Sub ToggleMealButtons(ByVal Enable As Boolean)
        EnableMealButton(btnBreakfast, Enable)
        EnableMealButton(btnSnack, Enable)
        EnableMealButton(btnLunch, Enable)
        EnableMealButton(btnLunchDessert, Enable)
        EnableMealButton(btnTea, Enable)
        EnableMealButton(btnTeaDessert, Enable)
    End Sub

    Private Sub SetDay()

        Dim _D As NurseryGenieData.Day = Business.Day.Record
        If _D IsNot Nothing Then

            btnBreakfast.Tag = _D.BreakfastId.ToString
            btnBreakfast.AccessibleDescription = _D.BreakfastName

            btnSnack.Tag = _D.SnackId.ToString
            btnSnack.AccessibleDescription = _D.SnackName

            btnLunch.Tag = _D.LunchId.ToString
            btnLunch.AccessibleDescription = _D.LunchName

            btnLunchDessert.Tag = _D.LunchDesId.ToString
            btnLunchDessert.AccessibleDescription = _D.LunchDesName

            btnTea.Tag = _D.TeaId.ToString
            btnTea.AccessibleDescription = _D.TeaName

            btnTeaDessert.Tag = _D.TeaDesId.ToString
            btnTeaDessert.AccessibleDescription = _D.TeaDesName

            SelectFirstMeal()

        End If

    End Sub

    Private Sub SelectFirstMeal()

        If btnBreakfast.AccessibleDescription <> "" Then
            Breakfast()
            Exit Sub
        End If

        If btnSnack.AccessibleDescription <> "" Then
            Snack()
            Exit Sub
        End If

        If btnLunch.AccessibleDescription <> "" Then
            Lunch()
            Exit Sub
        End If

        If btnLunchDessert.AccessibleDescription <> "" Then
            LunchDessert()
            Exit Sub
        End If

        If btnTea.AccessibleDescription <> "" Then
            Tea()
            Exit Sub
        End If

        If btnTeaDessert.AccessibleDescription <> "" Then
            TeaDessert()
            Exit Sub
        End If

    End Sub

    Private Sub DisplayMealDetails(ByVal MealType As EnumMealType, ByVal MealID As String)

        ClearButtons()

        If MealID = "" Then Exit Sub

        Dim _OffMenuMode As Boolean = OffMenu
        Dim _MealCode As String = ReturnMealCode(MealType)
        Dim _DetailRow As Integer = 1

        If Parameters.UseBasicMeals Then
            _OffMenuMode = True
            If Not OffMenu Then
                DisplayMeal(MealType, MealID)
                _DetailRow += 1
            End If
        End If

        Dim _Items As List(Of Business.Food.FoodItemStatus) = Business.Food.GetFoodItems(MyBase.ChildID, MealID, _MealCode, EnumMode.Normal)
        If _Items IsNot Nothing AndAlso _Items.Count > 0 Then
            For Each _FIS In _Items
                SetButton("btnFood", _DetailRow, True, _FIS.FoodName, _FIS.FoodID.ToString, SystemColors.Control, _FIS.Status)
                SetAnswers(_DetailRow, MealType, _FIS.FoodID.ToString)
                _DetailRow += 1
            Next
        End If

        btnAdd.Enabled = True

    End Sub

    Private Sub DisplayMeal(ByVal MealType As EnumMealType, ByVal MealID As String)

        Dim _FoodID As String = ""
        Dim _FoodName As String = ""
        Dim _Status As Integer = -1

        '***********************************************************************************************************************

        Dim _btn As Button = ReturnMealButton()

        _FoodID = _btn.Tag.ToString
        _FoodName = _btn.AccessibleDescription

        _Status = ReturnStatus(MyBase.ChildID, MealType, _FoodID)
        SetButton("btnFood", 1, True, _FoodName, _FoodID, SystemColors.Control, _Status)
        SetAnswers(1, MealType, _FoodID)

        '***********************************************************************************************************************

    End Sub

    Private Sub SetButton(ByVal Name As String, ByVal ControlIndex As Integer, _
                          ByVal Enabled As Boolean, ByVal FoodName As String, ByVal FoodID As String, _
                          ByVal BackColour As Color, _
                          ByVal Status As Integer)

        Dim _Name As String = Name + ControlIndex.ToString
        Dim _ctrl As Control() = Me.Controls.Find(_Name, True)
        If _ctrl.Length = 1 Then
            _ctrl(0).Enabled = Enabled
            _ctrl(0).Tag = FoodID
            _ctrl(0).Text = FoodName
            _ctrl(0).BackColor = BackColour
            _ctrl(0).AccessibleDescription = Status.ToString
        End If

    End Sub

    Private Sub SetAnswers(ByVal ControlIndex As Integer, ByVal MealType As EnumMealType, ByVal FoodID As String)

        Dim _None As Color = SystemColors.Control
        Dim _Some As Color = SystemColors.Control
        Dim _Most As Color = SystemColors.Control
        Dim _All As Color = SystemColors.Control

        Dim _Status As Integer = ReturnStatus(MyBase.ChildID, MealType, FoodID)

        Select Case _Status

            Case 0
                _None = m_None
                _Some = SystemColors.Control
                _Most = SystemColors.Control
                _All = SystemColors.Control

            Case 1
                _None = SystemColors.Control
                _Some = m_Some
                _Most = SystemColors.Control
                _All = SystemColors.Control

            Case 2
                _None = SystemColors.Control
                _Some = SystemColors.Control
                _Most = m_Most
                _All = SystemColors.Control

            Case 3
                _None = SystemColors.Control
                _Some = SystemColors.Control
                _Most = SystemColors.Control
                _All = m_All

        End Select

        SetButton("btnNone", ControlIndex, True, "None", "", _None, _Status)
        SetButton("btnSome", ControlIndex, True, "Some", "", _Some, _Status)
        SetButton("btnMost", ControlIndex, True, "Most", "", _Most, _Status)
        SetButton("btnAll", ControlIndex, True, "All", "", _All, _Status)

    End Sub

    Private Sub ClearButtons()

        Dim i As Integer = 1
        For i = 1 To 8
            ClearButton("btnFood", i)
            ClearButton("btnNone", i)
            ClearButton("btnSome", i)
            ClearButton("btnMost", i)
            ClearButton("btnAll", i)
        Next

        btnAdd.Enabled = False
        btnRemoveAll.Enabled = False

    End Sub

    Private Sub ClearButton(ByVal ButtonName As String, ByVal ControlIndex As Integer)

        Dim _name As String = ButtonName + ControlIndex.ToString
        Dim _ctrl As Control() = Me.Controls.Find(_name, True)
        If _ctrl.Length = 1 Then
            _ctrl(0).Enabled = False
            _ctrl(0).Tag = ""
            _ctrl(0).Text = ""
            _ctrl(0).BackColor = SystemColors.Control
        End If
    End Sub

    Private Sub Breakfast()
        btnBreakfast.BackColor = Color.Yellow
        btnSnack.BackColor = SystemColors.Control
        btnLunch.BackColor = SystemColors.Control
        btnLunchDessert.BackColor = SystemColors.Control
        btnTea.BackColor = SystemColors.Control
        btnTeaDessert.BackColor = SystemColors.Control
        DisplayMealDetails(EnumMealType.Breakfast, btnBreakfast.Tag.ToString)
    End Sub

    Private Function ReturnMealButton() As Button
        If btnBreakfast.BackColor = Color.Yellow Then Return btnBreakfast
        If btnSnack.BackColor = Color.Yellow Then Return btnSnack
        If btnLunch.BackColor = Color.Yellow Then Return btnLunch
        If btnLunchDessert.BackColor = Color.Yellow Then Return btnLunchDessert
        If btnTea.BackColor = Color.Yellow Then Return btnTea
        If btnTeaDessert.BackColor = Color.Yellow Then Return btnTeaDessert
        Return Nothing
    End Function

    Private Sub Save()

        Dim _MealID As String = ""
        Dim _MealName As String = ""
        Dim _MealType As String = ""

        If BabyFood Then
            _MealID = g_BabyFoodMealID
            _MealName = "Baby Food"
            _MealType = "J"
        Else
            Dim _btnMeal As Button = ReturnMealButton()
            If _btnMeal IsNot Nothing Then
                _MealID = _btnMeal.Tag.ToString
                _MealType = _btnMeal.Text
                _MealName = _btnMeal.AccessibleDescription
            End If
        End If

        If _MealID <> "" Then

            _MealType = ReturnMealCode(_MealType)

            For i = 1 To 8

                Dim _name As String = "btnFood" + i.ToString
                Dim _ctrl As Control() = Me.Controls.Find(_name, True)
                If _ctrl.Length = 1 Then
                    If _ctrl(0).Enabled Then
                        Dim _FoodID As String = _ctrl(0).Tag.ToString
                        Dim _FoodName As String = _ctrl(0).Text
                        Dim _FoodStatus As Integer = CInt(_ctrl(0).AccessibleDescription)
                        LogFood(TodayID, txtChild.Tag.ToString, txtChild.Text, _MealID, _MealType, _MealName, _FoodID, _FoodName, _FoodStatus)
                    End If
                End If

            Next

        Else
            Msgbox("No Meal has been selected! Save Cancelled.", MessageBoxIcon.Error, "Save Food")
        End If

    End Sub

    Private Function CheckAnswers() As Boolean

        For i = 1 To 8

            Dim _name As String = "btnFood" + i.ToString
            Dim _ctrl As Control() = Me.Controls.Find(_name, True)
            If _ctrl.Length = 1 Then
                If _ctrl(0).Enabled Then
                    If _ctrl(0).AccessibleDescription = "-1" Then
                        Msgbox("Please select amount of food eaten for " & _ctrl(0).Text, MessageBoxIcon.Exclamation, "Missing Status")
                        Return False
                    End If
                End If
            End If

        Next

        Return True

    End Function

#Region "Food Status Buttons"

    Private Sub btnNone1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNone1.Click
        btnFood1.AccessibleDescription = "0"
        btnNone1.BackColor = Color.Red
        btnSome1.BackColor = SystemColors.Control
        btnMost1.BackColor = SystemColors.Control
        btnAll1.BackColor = SystemColors.Control
    End Sub

    Private Sub btnSome1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSome1.Click
        btnFood1.AccessibleDescription = "1"
        btnNone1.BackColor = SystemColors.Control
        btnSome1.BackColor = Color.Orange
        btnMost1.BackColor = SystemColors.Control
        btnAll1.BackColor = SystemColors.Control

    End Sub

    Private Sub btnMost1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMost1.Click
        btnFood1.AccessibleDescription = "2"
        btnNone1.BackColor = SystemColors.Control
        btnSome1.BackColor = SystemColors.Control
        btnMost1.BackColor = Color.Yellow
        btnAll1.BackColor = SystemColors.Control

    End Sub

    Private Sub btnAll1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAll1.Click
        btnFood1.AccessibleDescription = "3"
        btnNone1.BackColor = SystemColors.Control
        btnSome1.BackColor = SystemColors.Control
        btnMost1.BackColor = SystemColors.Control
        btnAll1.BackColor = Color.YellowGreen
    End Sub

    Private Sub btnNone2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNone2.Click
        btnFood2.AccessibleDescription = "0"
        btnNone2.BackColor = Color.Red
        btnSome2.BackColor = SystemColors.Control
        btnMost2.BackColor = SystemColors.Control
        btnAll2.BackColor = SystemColors.Control
    End Sub

    Private Sub btnSome2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSome2.Click
        btnFood2.AccessibleDescription = "1"
        btnNone2.BackColor = SystemColors.Control
        btnSome2.BackColor = Color.Orange
        btnMost2.BackColor = SystemColors.Control
        btnAll2.BackColor = SystemColors.Control

    End Sub

    Private Sub btnMost2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMost2.Click
        btnFood2.AccessibleDescription = "2"
        btnNone2.BackColor = SystemColors.Control
        btnSome2.BackColor = SystemColors.Control
        btnMost2.BackColor = Color.Yellow
        btnAll2.BackColor = SystemColors.Control

    End Sub

    Private Sub btnAll2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAll2.Click
        btnFood2.AccessibleDescription = "3"
        btnNone2.BackColor = SystemColors.Control
        btnSome2.BackColor = SystemColors.Control
        btnMost2.BackColor = SystemColors.Control
        btnAll2.BackColor = Color.YellowGreen
    End Sub

    Private Sub btnNone3_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNone3.Click
        btnFood3.AccessibleDescription = "0"
        btnNone3.BackColor = Color.Red
        btnSome3.BackColor = SystemColors.Control
        btnMost3.BackColor = SystemColors.Control
        btnAll3.BackColor = SystemColors.Control
    End Sub

    Private Sub btnSome3_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSome3.Click
        btnFood3.AccessibleDescription = "1"
        btnNone3.BackColor = SystemColors.Control
        btnSome3.BackColor = Color.Orange
        btnMost3.BackColor = SystemColors.Control
        btnAll3.BackColor = SystemColors.Control

    End Sub

    Private Sub btnMost3_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMost3.Click
        btnFood3.AccessibleDescription = "2"
        btnNone3.BackColor = SystemColors.Control
        btnSome3.BackColor = SystemColors.Control
        btnMost3.BackColor = Color.Yellow
        btnAll3.BackColor = SystemColors.Control

    End Sub

    Private Sub btnAll3_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAll3.Click
        btnFood3.AccessibleDescription = "3"
        btnNone3.BackColor = SystemColors.Control
        btnSome3.BackColor = SystemColors.Control
        btnMost3.BackColor = SystemColors.Control
        btnAll3.BackColor = Color.YellowGreen
    End Sub

    Private Sub btnNone4_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNone4.Click
        btnFood4.AccessibleDescription = "0"
        btnNone4.BackColor = Color.Red
        btnSome4.BackColor = SystemColors.Control
        btnMost4.BackColor = SystemColors.Control
        btnAll4.BackColor = SystemColors.Control
    End Sub

    Private Sub btnSome4_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSome4.Click
        btnFood4.AccessibleDescription = "1"
        btnNone4.BackColor = SystemColors.Control
        btnSome4.BackColor = Color.Orange
        btnMost4.BackColor = SystemColors.Control
        btnAll4.BackColor = SystemColors.Control

    End Sub

    Private Sub btnMost4_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMost4.Click
        btnFood4.AccessibleDescription = "2"
        btnNone4.BackColor = SystemColors.Control
        btnSome4.BackColor = SystemColors.Control
        btnMost4.BackColor = Color.Yellow
        btnAll4.BackColor = SystemColors.Control

    End Sub

    Private Sub btnAll4_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAll4.Click
        btnFood4.AccessibleDescription = "3"
        btnNone4.BackColor = SystemColors.Control
        btnSome4.BackColor = SystemColors.Control
        btnMost4.BackColor = SystemColors.Control
        btnAll4.BackColor = Color.YellowGreen
    End Sub

    Private Sub btnNone5_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNone5.Click
        btnFood5.AccessibleDescription = "0"
        btnNone5.BackColor = Color.Red
        btnSome5.BackColor = SystemColors.Control
        btnMost5.BackColor = SystemColors.Control
        btnAll5.BackColor = SystemColors.Control
    End Sub

    Private Sub btnSome5_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSome5.Click
        btnFood5.AccessibleDescription = "1"
        btnNone5.BackColor = SystemColors.Control
        btnSome5.BackColor = Color.Orange
        btnMost5.BackColor = SystemColors.Control
        btnAll5.BackColor = SystemColors.Control

    End Sub

    Private Sub btnMost5_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMost5.Click
        btnFood5.AccessibleDescription = "2"
        btnNone5.BackColor = SystemColors.Control
        btnSome5.BackColor = SystemColors.Control
        btnMost5.BackColor = Color.Yellow
        btnAll5.BackColor = SystemColors.Control

    End Sub

    Private Sub btnAll5_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAll5.Click
        btnFood5.AccessibleDescription = "3"
        btnNone5.BackColor = SystemColors.Control
        btnSome5.BackColor = SystemColors.Control
        btnMost5.BackColor = SystemColors.Control
        btnAll5.BackColor = Color.YellowGreen
    End Sub

    Private Sub btnNone6_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNone6.Click
        btnFood6.AccessibleDescription = "0"
        btnNone6.BackColor = Color.Red
        btnSome6.BackColor = SystemColors.Control
        btnMost6.BackColor = SystemColors.Control
        btnAll6.BackColor = SystemColors.Control
    End Sub

    Private Sub btnSome6_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSome6.Click
        btnFood6.AccessibleDescription = "1"
        btnNone6.BackColor = SystemColors.Control
        btnSome6.BackColor = Color.Orange
        btnMost6.BackColor = SystemColors.Control
        btnAll6.BackColor = SystemColors.Control

    End Sub

    Private Sub btnMost6_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMost6.Click
        btnFood6.AccessibleDescription = "2"
        btnNone6.BackColor = SystemColors.Control
        btnSome6.BackColor = SystemColors.Control
        btnMost6.BackColor = Color.Yellow
        btnAll6.BackColor = SystemColors.Control

    End Sub

    Private Sub btnAll6_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAll6.Click
        btnFood6.AccessibleDescription = "3"
        btnNone6.BackColor = SystemColors.Control
        btnSome6.BackColor = SystemColors.Control
        btnMost6.BackColor = SystemColors.Control
        btnAll6.BackColor = Color.YellowGreen
    End Sub

    Private Sub btnNone7_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNone7.Click
        btnFood7.AccessibleDescription = "0"
        btnNone7.BackColor = Color.Red
        btnSome7.BackColor = SystemColors.Control
        btnMost7.BackColor = SystemColors.Control
        btnAll7.BackColor = SystemColors.Control
    End Sub

    Private Sub btnSome7_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSome7.Click
        btnFood7.AccessibleDescription = "1"
        btnNone7.BackColor = SystemColors.Control
        btnSome7.BackColor = Color.Orange
        btnMost7.BackColor = SystemColors.Control
        btnAll7.BackColor = SystemColors.Control

    End Sub

    Private Sub btnMost7_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMost7.Click
        btnFood7.AccessibleDescription = "2"
        btnNone7.BackColor = SystemColors.Control
        btnSome7.BackColor = SystemColors.Control
        btnMost7.BackColor = Color.Yellow
        btnAll7.BackColor = SystemColors.Control

    End Sub

    Private Sub btnAll7_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAll7.Click
        btnFood7.AccessibleDescription = "3"
        btnNone7.BackColor = SystemColors.Control
        btnSome7.BackColor = SystemColors.Control
        btnMost7.BackColor = SystemColors.Control
        btnAll7.BackColor = Color.YellowGreen
    End Sub

    Private Sub btnNone8_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNone8.Click
        btnFood8.AccessibleDescription = "0"
        btnNone8.BackColor = Color.Red
        btnSome8.BackColor = SystemColors.Control
        btnMost8.BackColor = SystemColors.Control
        btnAll8.BackColor = SystemColors.Control
    End Sub

    Private Sub btnSome8_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSome8.Click
        btnFood8.AccessibleDescription = "1"
        btnNone8.BackColor = SystemColors.Control
        btnSome8.BackColor = Color.Orange
        btnMost8.BackColor = SystemColors.Control
        btnAll8.BackColor = SystemColors.Control

    End Sub

    Private Sub btnMost8_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMost8.Click
        btnFood8.AccessibleDescription = "2"
        btnNone8.BackColor = SystemColors.Control
        btnSome8.BackColor = SystemColors.Control
        btnMost8.BackColor = Color.Yellow
        btnAll8.BackColor = SystemColors.Control

    End Sub

    Private Sub btnAll8_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAll8.Click
        btnFood8.AccessibleDescription = "3"
        btnNone8.BackColor = SystemColors.Control
        btnSome8.BackColor = SystemColors.Control
        btnMost8.BackColor = SystemColors.Control
        btnAll8.BackColor = Color.YellowGreen
    End Sub

#End Region

#Region "Other Buttons"

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click

        'Dim _MealID As String
        'Dim _MealName As String
        'Dim _MealType As String

        'If BabyFood Then
        '    _MealID = g_BabyFoodMealID
        '    _MealType = "J"
        '    _MealName = "Baby Food"
        'Else

        '    Dim _btn As Button = ReturnMealButton()

        '    If _btn Is Nothing Then
        '        Msgbox("No meal has been selected. Ensure meals has been defined in the Today screen.", MessageBoxIcon.Exclamation, "Add Meal")
        '        Exit Sub
        '    End If

        '    _MealID = _btn.Tag.ToString
        '    _MealType = ReturnMealCode(_btn.Text)
        '    _MealName = _btn.AccessibleDescription

        'End If

        'Dim _SQL As String
        'Dim _Caption As String
        'Dim _Pair As Pair = Nothing

        'Dim _GroupID As String = ""
        'Dim _GroupName As String = ""

        ''get food group
        '_SQL = "select distinct group_id, group_name from Food order by group_name"
        '_Caption = "Select Food Group"

        '_Pair = ReturnButtonSQL(_SQL, _Caption)
        'If _Pair Is Nothing Then
        '    Exit Sub
        'Else
        '    _GroupID = _Pair.Code
        '    _GroupName = _Pair.Text
        '    _Pair = Nothing
        'End If

        '_SQL = "select id, name from Food where group_id = '" + _GroupID + "'"
        '_Caption = "Select Food"

        'If BabyFood Then
        '    _SQL += " and baby_food = 1"
        '    _Caption = "Select Baby Food"
        'End If

        '_SQL += " order by name"

        '_Pair = ReturnButtonSQL(_SQL, _Caption)
        'If _Pair IsNot Nothing Then

        '    Dim _FoodID As String = _Pair.Code
        '    Dim _FoodName As String = _Pair.Text
        '    Dim _FoodStatus As Integer = 3

        '    LogFood(TodayID, txtChild.Tag.ToString, txtChild.Text, _MealID, _MealType, _MealName, _FoodID, _FoodName, _FoodStatus)
        '    DisplayMealDetails(ReturnMealTypeEnum(_MealType), _MealID)

        'End If

    End Sub

    Private Sub btnBreakfast_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBreakfast.Click
        Save()
        Breakfast()
    End Sub

    Private Sub btnSnack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSnack.Click
        Save()
        Snack()
    End Sub

    Private Sub btnLunch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLunch.Click
        Save()
        Lunch()
    End Sub

    Private Sub btnTea_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnTea.Click
        Save()
        Tea()
    End Sub

    Private Sub btnLunchDessert_Click(sender As Object, e As EventArgs) Handles btnLunchDessert.Click
        Save()
        LunchDessert()
    End Sub

    Private Sub btnTeaDessert_Click(sender As Object, e As EventArgs) Handles btnTeaDessert.Click
        Save()
        TeaDessert()
    End Sub

    Private Sub Snack()
        btnBreakfast.BackColor = SystemColors.Control
        btnSnack.BackColor = Color.Yellow
        btnLunch.BackColor = SystemColors.Control
        btnLunchDessert.BackColor = SystemColors.Control
        btnTea.BackColor = SystemColors.Control
        btnTeaDessert.BackColor = SystemColors.Control
        DisplayMealDetails(EnumMealType.Snack, btnSnack.Tag.ToString)
    End Sub

    Private Sub Lunch()
        btnBreakfast.BackColor = SystemColors.Control
        btnSnack.BackColor = SystemColors.Control
        btnLunch.BackColor = Color.Yellow
        btnLunchDessert.BackColor = SystemColors.Control
        btnTea.BackColor = SystemColors.Control
        btnTeaDessert.BackColor = SystemColors.Control
        DisplayMealDetails(EnumMealType.Lunch, btnLunch.Tag.ToString)
    End Sub

    Private Sub Tea()
        btnBreakfast.BackColor = SystemColors.Control
        btnSnack.BackColor = SystemColors.Control
        btnLunch.BackColor = SystemColors.Control
        btnLunchDessert.BackColor = SystemColors.Control
        btnTea.BackColor = Color.Yellow
        btnTeaDessert.BackColor = SystemColors.Control
        DisplayMealDetails(EnumMealType.Tea, btnTea.Tag.ToString)
    End Sub

    Private Sub LunchDessert()
        btnBreakfast.BackColor = SystemColors.Control
        btnSnack.BackColor = SystemColors.Control
        btnLunch.BackColor = SystemColors.Control
        btnLunchDessert.BackColor = Color.Yellow
        btnTea.BackColor = SystemColors.Control
        DisplayMealDetails(EnumMealType.LunchDessert, btnLunchDessert.Tag.ToString)
    End Sub

    Private Sub TeaDessert()
        btnBreakfast.BackColor = SystemColors.Control
        btnSnack.BackColor = SystemColors.Control
        btnLunch.BackColor = SystemColors.Control
        btnTea.BackColor = SystemColors.Control
        btnTeaDessert.BackColor = Color.Yellow
        DisplayMealDetails(EnumMealType.TeaDessert, btnTeaDessert.Tag.ToString)
    End Sub

    Private Sub btnRemoveAll_Click(sender As Object, e As EventArgs) Handles btnRemoveAll.Click
        'RemoveAll()
        'ClearButtons()
    End Sub

    Private Sub RemoveAll()

        Dim _BTN As Button = ReturnMealButton()
        If _BTN Is Nothing Then Exit Sub

        If _BTN.Tag.ToString <> "" Then
            ClearFoodItems(txtChild.Tag.ToString, _BTN.Tag.ToString())
        End If

    End Sub

#End Region

End Class
