﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChildInfo
    Inherits NurseryTablet.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.vgBasic = New DevExpress.XtraVerticalGrid.VGridControl()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.vgMedical = New DevExpress.XtraVerticalGrid.VGridControl()
        Me.tgAllergiesConsent = New NurseryTablet.TouchGrid()
        Me.btnAllergyMatrix = New DevExpress.XtraEditors.SimpleButton()
        Me.btnConsentMatrix = New DevExpress.XtraEditors.SimpleButton()
        Me.panBottom = New DevExpress.XtraEditors.PanelControl()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.vgBasic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.vgMedical, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panBottom.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        '
        'vgBasic
        '
        Me.vgBasic.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.vgBasic.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView
        Me.vgBasic.Location = New System.Drawing.Point(5, 35)
        Me.vgBasic.Name = "vgBasic"
        Me.vgBasic.OptionsBehavior.Editable = False
        Me.vgBasic.Size = New System.Drawing.Size(326, 424)
        Me.vgBasic.TabIndex = 3
        Me.vgBasic.TabStop = False
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.vgBasic)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(336, 464)
        Me.GroupControl1.TabIndex = 4
        Me.GroupControl1.Text = "Basic Details"
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.vgMedical)
        Me.GroupControl2.Location = New System.Drawing.Point(354, 12)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(336, 464)
        Me.GroupControl2.TabIndex = 5
        Me.GroupControl2.Text = "Medical Details"
        '
        'vgMedical
        '
        Me.vgMedical.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.vgMedical.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView
        Me.vgMedical.Location = New System.Drawing.Point(5, 35)
        Me.vgMedical.Name = "vgMedical"
        Me.vgMedical.OptionsBehavior.Editable = False
        Me.vgMedical.Size = New System.Drawing.Size(326, 424)
        Me.vgMedical.TabIndex = 4
        Me.vgMedical.TabStop = False
        '
        'tgAllergiesConsent
        '
        Me.tgAllergiesConsent.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tgAllergiesConsent.HideFirstColumn = False
        Me.tgAllergiesConsent.Location = New System.Drawing.Point(696, 47)
        Me.tgAllergiesConsent.Name = "tgAllergiesConsent"
        Me.tgAllergiesConsent.Size = New System.Drawing.Size(337, 429)
        Me.tgAllergiesConsent.TabIndex = 9
        '
        'btnAllergyMatrix
        '
        Me.btnAllergyMatrix.Location = New System.Drawing.Point(696, 12)
        Me.btnAllergyMatrix.Name = "btnAllergyMatrix"
        Me.btnAllergyMatrix.Size = New System.Drawing.Size(161, 29)
        Me.btnAllergyMatrix.TabIndex = 10
        Me.btnAllergyMatrix.Text = "Allergy Matrix"
        '
        'btnConsentMatrix
        '
        Me.btnConsentMatrix.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnConsentMatrix.Location = New System.Drawing.Point(872, 12)
        Me.btnConsentMatrix.Name = "btnConsentMatrix"
        Me.btnConsentMatrix.Size = New System.Drawing.Size(161, 29)
        Me.btnConsentMatrix.TabIndex = 11
        Me.btnConsentMatrix.Text = "Consent Matrix"
        '
        'panBottom
        '
        Me.panBottom.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panBottom.Appearance.Options.UseFont = True
        Me.panBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.panBottom.Controls.Add(Me.btnCancel)
        Me.panBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panBottom.Location = New System.Drawing.Point(0, 482)
        Me.panBottom.Name = "panBottom"
        Me.panBottom.Size = New System.Drawing.Size(1045, 56)
        Me.panBottom.TabIndex = 12
        '
        'btnCancel
        '
        Me.btnCancel.Image = Global.NurseryTablet.My.Resources.Resources.cancel_32
        Me.btnCancel.Location = New System.Drawing.Point(883, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(150, 45)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "Close"
        '
        'frmChildInfo
        '
        Me.ClientSize = New System.Drawing.Size(1045, 538)
        Me.Controls.Add(Me.panBottom)
        Me.Controls.Add(Me.btnConsentMatrix)
        Me.Controls.Add(Me.btnAllergyMatrix)
        Me.Controls.Add(Me.tgAllergiesConsent)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmChildInfo"
        Me.Text = "Child Record"
        CType(Me.vgBasic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.vgMedical, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panBottom.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents vgBasic As DevExpress.XtraVerticalGrid.VGridControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents tgAllergiesConsent As NurseryTablet.TouchGrid
    Friend WithEvents btnAllergyMatrix As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnConsentMatrix As DevExpress.XtraEditors.SimpleButton
    Protected Friend WithEvents panBottom As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents vgMedical As DevExpress.XtraVerticalGrid.VGridControl

End Class
