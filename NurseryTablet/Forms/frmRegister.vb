﻿Imports NurseryTablet.Business
Imports NurseryTablet.SharedModule
Imports NurseryTablet.NurseryGenieData

Public Class frmRegister

    Private m_Register As New List(Of LocalRegister)

    Private Sub frmRegister_Load(sender As Object, e As EventArgs) Handles Me.Load
        RefreshGrid()
    End Sub

    Private Sub RefreshGrid()

        Me.Cursor = Cursors.WaitCursor
        Application.DoEvents()

        BuildRegister()

        tgRegister.HideFirstColumn = True
        tgRegister.Populate(m_Register)

        Dim _HasRecords As Boolean = False
        If tgRegister.RecordCount > 0 Then _HasRecords = True

        If radDueIn.Checked Then
            btnCheckOut.Text = "Absent"
        Else
            btnCheckOut.Text = "Check Out"
        End If

        If radAbsent.Checked Then
            btnCheckIn.Enabled = False
            btnCheckOut.Enabled = False
        Else

            If radCheckedIn.Checked Then
                btnCheckIn.Enabled = False
                btnCheckOut.Enabled = _HasRecords
            Else
                If radCheckedOut.Checked Then
                    btnCheckIn.Enabled = _HasRecords
                    btnCheckOut.Enabled = False
                Else
                    If radDueIn.Checked Then
                        btnCheckIn.Enabled = _HasRecords
                        btnCheckOut.Enabled = _HasRecords
                    End If
                End If
            End If

        End If

        Me.Cursor = Cursors.Default
        Application.DoEvents()

    End Sub

    Private Sub BuildRegister()

        m_Register.Clear()
        Dim _Records As Object = Nothing

        If radCheckedIn.Checked Or radCheckedOut.Checked Then

            Dim _Type As String = "C"
            If radStaff.Checked Then _Type = "S"

            If radCheckedIn.Checked Then
                _Records = From _R In DataBase.Tables.Register Where _R.RegisterDate = Today And _R.RegisterType = _Type And _R.InOut = "I" Order By _R.StampIn
            Else
                _Records = From _R In DataBase.Tables.Register Where _R.RegisterDate = Today And _R.RegisterType = _Type And _R.InOut = "O" Order By _R.StampOut Descending
            End If

            For Each _R As NurseryGenieData.Register In _Records

                Dim _Add As Boolean = False
                Dim _Status As Enums.AttendanceStatus = SharedModule.PersonStatus(_R.PersonID.ToString)

                If radCheckedIn.Checked AndAlso _Status = Enums.AttendanceStatus.CheckedIn Then
                    _Add = True
                End If

                If radCheckedOut.Checked AndAlso _Status = Enums.AttendanceStatus.CheckedOut Then
                    _Add = True
                End If

                If _Add Then

                    Dim _LR As New LocalRegister
                    With _LR
                        .PersonID = _R.PersonID
                        .PersonName = _R.PersonName
                        .TariffName = ""
                        .BookedFrom = Nothing
                        .BookedTo = Nothing
                        .ActualIn = ConvertToTimeSpan(_R.StampIn)
                        .ActualOut = ConvertToTimeSpan(_R.StampOut)
                    End With

                    m_Register.Add(_LR)

                End If

            Next

        Else

            _Records = From _B In DataBase.Tables.Bookings Where _B.BookingDate = Today Order By _B.BookingFrom, _B.ChildName

            For Each _B As Booking In _Records

                Dim _Add As Boolean = False
                Dim _Status As Enums.AttendanceStatus = SharedModule.PersonStatus(_B.ChildId.ToString)

                If radDueIn.Checked Then
                    If _Status = Enums.AttendanceStatus.NotArrived Then
                        _Add = True
                    End If
                Else
                    If _Status = Enums.AttendanceStatus.Absent Then
                        _Add = True
                    End If
                End If

                If _Add Then

                    Dim _R As New LocalRegister
                    With _R
                        .PersonID = _B.ChildId.Value
                        .PersonName = _B.ChildName
                        .TariffName = _B.TariffName
                        .BookedFrom = _B.BookingFrom
                        .BookedTo = _B.BookingTo
                        .ActualIn = Nothing
                        .ActualOut = Nothing
                    End With

                    m_Register.Add(_R)

                End If

            Next

        End If

    End Sub

    Private Function ConvertToTimeSpan(ByVal DateIn As Date?) As TimeSpan
        If DateIn.HasValue Then
            Return DateIn.Value.TimeOfDay
        Else
            Return Nothing
        End If
    End Function

    Private Sub SetOptions()

        If radCheckedIn.Checked Then
            RefreshGrid()
        Else
            radCheckedIn.Checked = True
        End If

        radDueIn.Visible = radChildren.Checked
        radAbsent.Visible = radChildren.Checked

    End Sub

    Private Sub CheckIn()

        If Not SharedModule.CheckinsPermitted Then Exit Sub
        If tgRegister.CurrentRow Is Nothing Then Exit Sub

        Dim _R As LocalRegister = tgRegister.CurrentRow

        Dim _Type As Enums.PersonType = Enums.PersonType.Child
        If radStaff.Checked Then _Type = Enums.PersonType.Staff

        Business.Register.RegisterTransaction(_R.PersonID.ToString, _Type, _R.PersonName, Enums.InOut.CheckIn, Enums.RegisterVia.Register)

        RefreshGrid()

    End Sub

    Private Sub CheckOut()

        If Not SharedModule.CheckinsPermitted Then Exit Sub
        If tgRegister.CurrentRow Is Nothing Then Exit Sub

        If radDueIn.Checked Then

            Dim _R As LocalRegister = tgRegister.CurrentRow

            Dim _frm As New frmAbsent(_R.PersonID.ToString, _R.PersonName)
            _frm.ShowDialog()

            If _frm.DialogResult = DialogResult.OK Then
                RefreshGrid()
            End If

            _frm.Dispose()
            _frm = Nothing

        Else

            Dim _R As LocalRegister = tgRegister.CurrentRow

            If radChildren.Checked Then

                Dim _frm As New frmCrossCheck(_R.PersonID.ToString, _R.PersonName, frmCrossCheck.EnumMode.CheckOut)
                _frm.ShowDialog()

                If _frm.DialogResult = DialogResult.OK Then
                    RefreshGrid()
                End If

                _frm.Dispose()
                _frm = Nothing

            End If

            If radStaff.Checked Then
                If Msgbox("Do you want to check " + _R.PersonName + " out?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Check Out") = MsgBoxResult.Yes Then
                    Business.Register.RegisterTransaction(_R.PersonID.ToString, Enums.PersonType.Staff, _R.PersonName, Enums.InOut.CheckOut, Enums.RegisterVia.Register)
                End If
            End If

        End If

        RefreshGrid()

    End Sub

#Region "Controls"

    Private Sub tgRegister_GridDoubleClick(sender As Object, e As EventArgs) Handles tgRegister.GridDoubleClick

        If Not radChildren.Checked Then Exit Sub
        If tgRegister.CurrentRow Is Nothing Then Exit Sub

        Dim _R As LocalRegister = tgRegister.CurrentRow

        Business.Child.DrillDown(_R.PersonID.ToString, _R.PersonName)

    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnCheckIn_Click(sender As Object, e As EventArgs) Handles btnCheckIn.Click
        CheckIn()
    End Sub

    Private Sub btnCheckOut_Click(sender As Object, e As EventArgs) Handles btnCheckOut.Click
        CheckOut()
    End Sub

#End Region

#Region "Radio Buttons"

    Private Sub radCheckedIn_CheckedChanged(sender As Object, e As EventArgs) Handles radCheckedIn.CheckedChanged
        If radCheckedIn.Checked Then RefreshGrid()
    End Sub

    Private Sub radCheckedOut_CheckedChanged(sender As Object, e As EventArgs) Handles radCheckedOut.CheckedChanged
        If radCheckedOut.Checked Then RefreshGrid()
    End Sub

    Private Sub radChildren_CheckedChanged(sender As Object, e As EventArgs) Handles radChildren.CheckedChanged
        SetOptions()
    End Sub

    Private Sub radStaff_CheckedChanged(sender As Object, e As EventArgs) Handles radStaff.CheckedChanged
        SetOptions()
    End Sub

    Private Sub radDueIn_CheckedChanged(sender As Object, e As EventArgs) Handles radDueIn.CheckedChanged
        If radDueIn.Checked Then RefreshGrid()
    End Sub

    Private Sub radAbsent_CheckedChanged(sender As Object, e As EventArgs) Handles radAbsent.CheckedChanged
        If radAbsent.Checked Then RefreshGrid()
    End Sub

#End Region

    Private Class LocalRegister
        Property PersonID As Guid
        Property PersonName As String
        Property BookedFrom As TimeSpan
        Property BookedTo As TimeSpan
        Property TariffName As String
        Property ActualIn As TimeSpan
        Property ActualOut As TimeSpan
    End Class

End Class
