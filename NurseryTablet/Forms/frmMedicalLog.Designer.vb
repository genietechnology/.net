﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMedicalLog
    Inherits frmBaseChild

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMedicalLog))
        Me.gbxMilk = New System.Windows.Forms.GroupBox()
        Me.btnDosage = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMedicine = New DevExpress.XtraEditors.SimpleButton()
        Me.btnNature = New DevExpress.XtraEditors.SimpleButton()
        Me.btnTimeNow = New DevExpress.XtraEditors.SimpleButton()
        Me.btnTimeSelect = New DevExpress.XtraEditors.SimpleButton()
        Me.lblTime = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblDosage = New System.Windows.Forms.Label()
        Me.lblMedicine = New System.Windows.Forms.Label()
        Me.lblNature = New System.Windows.Forms.Label()
        Me.lblTimeDosage = New System.Windows.Forms.Label()
        Me.lblMilk3 = New System.Windows.Forms.Label()
        Me.lblMilk2 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblStaffSig = New System.Windows.Forms.Label()
        Me.btnSigWitness = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSigStaff = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSelectWitness = New DevExpress.XtraEditors.SimpleButton()
        Me.lblWitness = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lblStaff2 = New System.Windows.Forms.Label()
        Me.gbxMilk.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        '
        'gbxMilk
        '
        Me.gbxMilk.Controls.Add(Me.btnDosage)
        Me.gbxMilk.Controls.Add(Me.btnMedicine)
        Me.gbxMilk.Controls.Add(Me.btnNature)
        Me.gbxMilk.Controls.Add(Me.btnTimeNow)
        Me.gbxMilk.Controls.Add(Me.btnTimeSelect)
        Me.gbxMilk.Controls.Add(Me.lblTime)
        Me.gbxMilk.Controls.Add(Me.Label4)
        Me.gbxMilk.Controls.Add(Me.lblDosage)
        Me.gbxMilk.Controls.Add(Me.lblMedicine)
        Me.gbxMilk.Controls.Add(Me.lblNature)
        Me.gbxMilk.Controls.Add(Me.lblTimeDosage)
        Me.gbxMilk.Controls.Add(Me.lblMilk3)
        Me.gbxMilk.Controls.Add(Me.lblMilk2)
        Me.gbxMilk.Location = New System.Drawing.Point(17, 90)
        Me.gbxMilk.Name = "gbxMilk"
        Me.gbxMilk.Size = New System.Drawing.Size(995, 233)
        Me.gbxMilk.TabIndex = 11
        Me.gbxMilk.TabStop = False
        '
        'btnDosage
        '
        Me.btnDosage.Image = CType(resources.GetObject("btnDosage.Image"), System.Drawing.Image)
        Me.btnDosage.Location = New System.Drawing.Point(859, 126)
        Me.btnDosage.Name = "btnDosage"
        Me.btnDosage.Size = New System.Drawing.Size(130, 45)
        Me.btnDosage.TabIndex = 66
        Me.btnDosage.Text = "Enter"
        '
        'btnMedicine
        '
        Me.btnMedicine.Image = CType(resources.GetObject("btnMedicine.Image"), System.Drawing.Image)
        Me.btnMedicine.Location = New System.Drawing.Point(859, 75)
        Me.btnMedicine.Name = "btnMedicine"
        Me.btnMedicine.Size = New System.Drawing.Size(130, 45)
        Me.btnMedicine.TabIndex = 65
        Me.btnMedicine.Text = "Enter"
        '
        'btnNature
        '
        Me.btnNature.Image = CType(resources.GetObject("btnNature.Image"), System.Drawing.Image)
        Me.btnNature.Location = New System.Drawing.Point(859, 24)
        Me.btnNature.Name = "btnNature"
        Me.btnNature.Size = New System.Drawing.Size(130, 45)
        Me.btnNature.TabIndex = 64
        Me.btnNature.Text = "Enter"
        '
        'btnTimeNow
        '
        Me.btnTimeNow.Image = CType(resources.GetObject("btnTimeNow.Image"), System.Drawing.Image)
        Me.btnTimeNow.Location = New System.Drawing.Point(723, 177)
        Me.btnTimeNow.Name = "btnTimeNow"
        Me.btnTimeNow.Size = New System.Drawing.Size(130, 45)
        Me.btnTimeNow.TabIndex = 63
        Me.btnTimeNow.Text = "Now"
        '
        'btnTimeSelect
        '
        Me.btnTimeSelect.Image = CType(resources.GetObject("btnTimeSelect.Image"), System.Drawing.Image)
        Me.btnTimeSelect.Location = New System.Drawing.Point(859, 177)
        Me.btnTimeSelect.Name = "btnTimeSelect"
        Me.btnTimeSelect.Size = New System.Drawing.Size(130, 45)
        Me.btnTimeSelect.TabIndex = 62
        Me.btnTimeSelect.Text = "Select"
        '
        'lblTime
        '
        Me.lblTime.AutoSize = True
        Me.lblTime.Location = New System.Drawing.Point(270, 187)
        Me.lblTime.Name = "lblTime"
        Me.lblTime.Size = New System.Drawing.Size(289, 13)
        Me.lblTime.TabIndex = 59
        Me.lblTime.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 187)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(59, 13)
        Me.Label4.TabIndex = 58
        Me.Label4.Text = "Time Given"
        '
        'lblDosage
        '
        Me.lblDosage.AutoSize = True
        Me.lblDosage.Location = New System.Drawing.Point(270, 136)
        Me.lblDosage.Name = "lblDosage"
        Me.lblDosage.Size = New System.Drawing.Size(385, 13)
        Me.lblDosage.TabIndex = 35
        Me.lblDosage.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblMedicine
        '
        Me.lblMedicine.AutoSize = True
        Me.lblMedicine.Location = New System.Drawing.Point(270, 85)
        Me.lblMedicine.Name = "lblMedicine"
        Me.lblMedicine.Size = New System.Drawing.Size(385, 13)
        Me.lblMedicine.TabIndex = 33
        Me.lblMedicine.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblNature
        '
        Me.lblNature.AutoSize = True
        Me.lblNature.Location = New System.Drawing.Point(270, 34)
        Me.lblNature.Name = "lblNature"
        Me.lblNature.Size = New System.Drawing.Size(385, 13)
        Me.lblNature.TabIndex = 32
        Me.lblNature.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblTimeDosage
        '
        Me.lblTimeDosage.AutoSize = True
        Me.lblTimeDosage.Location = New System.Drawing.Point(6, 136)
        Me.lblTimeDosage.Name = "lblTimeDosage"
        Me.lblTimeDosage.Size = New System.Drawing.Size(79, 13)
        Me.lblTimeDosage.TabIndex = 11
        Me.lblTimeDosage.Text = "Scheduled time"
        '
        'lblMilk3
        '
        Me.lblMilk3.AutoSize = True
        Me.lblMilk3.Location = New System.Drawing.Point(6, 85)
        Me.lblMilk3.Name = "lblMilk3"
        Me.lblMilk3.Size = New System.Drawing.Size(105, 13)
        Me.lblMilk3.TabIndex = 9
        Me.lblMilk3.Text = "Medicine to be given"
        '
        'lblMilk2
        '
        Me.lblMilk2.AutoSize = True
        Me.lblMilk2.Location = New System.Drawing.Point(6, 34)
        Me.lblMilk2.Name = "lblMilk2"
        Me.lblMilk2.Size = New System.Drawing.Size(86, 13)
        Me.lblMilk2.TabIndex = 8
        Me.lblMilk2.Text = "Nature of Illness"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblStaffSig)
        Me.GroupBox1.Controls.Add(Me.btnSigWitness)
        Me.GroupBox1.Controls.Add(Me.btnSigStaff)
        Me.GroupBox1.Controls.Add(Me.btnSelectWitness)
        Me.GroupBox1.Controls.Add(Me.lblWitness)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Location = New System.Drawing.Point(17, 329)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(995, 135)
        Me.GroupBox1.TabIndex = 44
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Signatures"
        '
        'lblStaffSig
        '
        Me.lblStaffSig.AutoSize = True
        Me.lblStaffSig.Location = New System.Drawing.Point(270, 36)
        Me.lblStaffSig.Name = "lblStaffSig"
        Me.lblStaffSig.Size = New System.Drawing.Size(289, 13)
        Me.lblStaffSig.TabIndex = 58
        Me.lblStaffSig.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'btnSigWitness
        '
        Me.btnSigWitness.Image = CType(resources.GetObject("btnSigWitness.Image"), System.Drawing.Image)
        Me.btnSigWitness.Location = New System.Drawing.Point(723, 77)
        Me.btnSigWitness.Name = "btnSigWitness"
        Me.btnSigWitness.Size = New System.Drawing.Size(130, 45)
        Me.btnSigWitness.TabIndex = 57
        Me.btnSigWitness.Text = "Sign"
        '
        'btnSigStaff
        '
        Me.btnSigStaff.Image = CType(resources.GetObject("btnSigStaff.Image"), System.Drawing.Image)
        Me.btnSigStaff.Location = New System.Drawing.Point(859, 26)
        Me.btnSigStaff.Name = "btnSigStaff"
        Me.btnSigStaff.Size = New System.Drawing.Size(130, 45)
        Me.btnSigStaff.TabIndex = 45
        Me.btnSigStaff.Text = "Sign"
        '
        'btnSelectWitness
        '
        Me.btnSelectWitness.Image = CType(resources.GetObject("btnSelectWitness.Image"), System.Drawing.Image)
        Me.btnSelectWitness.Location = New System.Drawing.Point(859, 77)
        Me.btnSelectWitness.Name = "btnSelectWitness"
        Me.btnSelectWitness.Size = New System.Drawing.Size(130, 45)
        Me.btnSelectWitness.TabIndex = 56
        Me.btnSelectWitness.Text = "Select"
        '
        'lblWitness
        '
        Me.lblWitness.AutoSize = True
        Me.lblWitness.Location = New System.Drawing.Point(270, 87)
        Me.lblWitness.Name = "lblWitness"
        Me.lblWitness.Size = New System.Drawing.Size(289, 13)
        Me.lblWitness.TabIndex = 55
        Me.lblWitness.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(10, 87)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(45, 13)
        Me.Label8.TabIndex = 54
        Me.Label8.Text = "Witness"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(10, 36)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(31, 13)
        Me.Label7.TabIndex = 51
        Me.Label7.Text = "Staff"
        '
        'lblStaff2
        '
        Me.lblStaff2.AutoSize = True
        Me.lblStaff2.Location = New System.Drawing.Point(270, 36)
        Me.lblStaff2.Name = "lblStaff2"
        Me.lblStaff2.Size = New System.Drawing.Size(435, 25)
        Me.lblStaff2.TabIndex = 52
        Me.lblStaff2.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'frmMedicalLog
        '
        Me.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.Appearance.Options.UseBackColor = True
        Me.ClientSize = New System.Drawing.Size(1024, 538)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.gbxMilk)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMedicalLog"
        Me.Text = "Medicine Log"
        Me.Controls.SetChildIndex(Me.gbxMilk, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.gbxMilk.ResumeLayout(False)
        Me.gbxMilk.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbxMilk As System.Windows.Forms.GroupBox
    Friend WithEvents lblDosage As System.Windows.Forms.Label
    Friend WithEvents lblMedicine As System.Windows.Forms.Label
    Friend WithEvents lblNature As System.Windows.Forms.Label
    Friend WithEvents lblTimeDosage As System.Windows.Forms.Label
    Friend WithEvents lblMilk3 As System.Windows.Forms.Label
    Friend WithEvents lblMilk2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnSigWitness As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSigStaff As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSelectWitness As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblWitness As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lblTime As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnTimeNow As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnTimeSelect As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnDosage As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMedicine As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnNature As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblStaffSig As System.Windows.Forms.Label
    Friend WithEvents lblStaff2 As System.Windows.Forms.Label

End Class
