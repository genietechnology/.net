﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.timCountdown = New System.Windows.Forms.Timer(Me.components)
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.SplashScreenManager1 = New DevExpress.XtraSplashScreen.SplashScreenManager(Me, GetType(Global.NurseryTablet.frmWait), True, True)
        Me.tlp = New System.Windows.Forms.TableLayoutPanel()
        Me.btnSync = New DevExpress.XtraEditors.SimpleButton()
        Me.btnOptions = New DevExpress.XtraEditors.SimpleButton()
        Me.btnExit = New DevExpress.XtraEditors.SimpleButton()
        Me.btnHelp = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMedicine = New DevExpress.XtraEditors.SimpleButton()
        Me.btnLookupChild = New DevExpress.XtraEditors.SimpleButton()
        Me.btnRequests = New DevExpress.XtraEditors.SimpleButton()
        Me.btnRegister = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCheckOutStaff = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCheckOutChild = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCheckInStaff = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCheckInChild = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMilk = New DevExpress.XtraEditors.SimpleButton()
        Me.btnFood = New DevExpress.XtraEditors.SimpleButton()
        Me.btnToilet = New DevExpress.XtraEditors.SimpleButton()
        Me.btnToiletTrip = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSleep = New DevExpress.XtraEditors.SimpleButton()
        Me.dshMedication = New NurseryTablet.DashboardLabel()
        Me.tlp.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        '
        'timCountdown
        '
        Me.timCountdown.Interval = 60000
        '
        'tlp
        '
        Me.tlp.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tlp.ColumnCount = 4
        Me.tlp.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.tlp.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.tlp.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.tlp.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.tlp.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlp.Controls.Add(Me.btnSync, 0, 4)
        Me.tlp.Controls.Add(Me.btnOptions, 1, 4)
        Me.tlp.Controls.Add(Me.btnExit, 3, 4)
        Me.tlp.Controls.Add(Me.btnHelp, 2, 4)
        Me.tlp.Controls.Add(Me.btnMedicine, 2, 3)
        Me.tlp.Controls.Add(Me.btnLookupChild, 3, 3)
        Me.tlp.Controls.Add(Me.btnRequests, 2, 1)
        Me.tlp.Controls.Add(Me.btnRegister, 2, 0)
        Me.tlp.Controls.Add(Me.btnCheckOutStaff, 0, 1)
        Me.tlp.Controls.Add(Me.btnCheckOutChild, 1, 0)
        Me.tlp.Controls.Add(Me.btnCheckInStaff, 0, 1)
        Me.tlp.Controls.Add(Me.btnCheckInChild, 0, 0)
        Me.tlp.Controls.Add(Me.btnMilk, 2, 2)
        Me.tlp.Controls.Add(Me.btnFood, 1, 2)
        Me.tlp.Controls.Add(Me.btnToilet, 0, 3)
        Me.tlp.Controls.Add(Me.btnToiletTrip, 1, 3)
        Me.tlp.Controls.Add(Me.btnSleep, 0, 2)
        Me.tlp.Controls.Add(Me.dshMedication, 3, 0)
        Me.tlp.Location = New System.Drawing.Point(12, 12)
        Me.tlp.Name = "tlp"
        Me.tlp.RowCount = 5
        Me.tlp.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.tlp.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.tlp.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.tlp.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.tlp.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.tlp.Size = New System.Drawing.Size(1160, 518)
        Me.tlp.TabIndex = 25
        '
        'btnSync
        '
        Me.btnSync.Appearance.Options.UseTextOptions = True
        Me.btnSync.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnSync.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSync.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnSync.Image = Global.NurseryTablet.My.Resources.Resources.order_64
        Me.btnSync.Location = New System.Drawing.Point(3, 415)
        Me.btnSync.Name = "btnSync"
        Me.btnSync.Size = New System.Drawing.Size(284, 100)
        Me.btnSync.TabIndex = 49
        Me.btnSync.Tag = ""
        Me.btnSync.Text = "Synchronise Data"
        '
        'btnOptions
        '
        Me.btnOptions.Appearance.Options.UseTextOptions = True
        Me.btnOptions.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnOptions.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnOptions.Image = CType(resources.GetObject("btnOptions.Image"), System.Drawing.Image)
        Me.btnOptions.Location = New System.Drawing.Point(293, 415)
        Me.btnOptions.Name = "btnOptions"
        Me.btnOptions.Size = New System.Drawing.Size(284, 100)
        Me.btnOptions.TabIndex = 48
        Me.btnOptions.Tag = "SC"
        Me.btnOptions.Text = "Settings"
        '
        'btnExit
        '
        Me.btnExit.Appearance.Options.UseTextOptions = True
        Me.btnExit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnExit.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnExit.Location = New System.Drawing.Point(873, 415)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(284, 100)
        Me.btnExit.TabIndex = 47
        Me.btnExit.Tag = ""
        Me.btnExit.Text = "Exit" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Application"
        '
        'btnHelp
        '
        Me.btnHelp.Appearance.Options.UseTextOptions = True
        Me.btnHelp.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnHelp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnHelp.Image = CType(resources.GetObject("btnHelp.Image"), System.Drawing.Image)
        Me.btnHelp.Location = New System.Drawing.Point(583, 415)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(284, 100)
        Me.btnHelp.TabIndex = 46
        Me.btnHelp.Tag = ""
        Me.btnHelp.Text = "Help!"
        '
        'btnMedicine
        '
        Me.btnMedicine.Appearance.Options.UseTextOptions = True
        Me.btnMedicine.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnMedicine.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnMedicine.Image = CType(resources.GetObject("btnMedicine.Image"), System.Drawing.Image)
        Me.btnMedicine.Location = New System.Drawing.Point(583, 312)
        Me.btnMedicine.Name = "btnMedicine"
        Me.btnMedicine.Size = New System.Drawing.Size(284, 97)
        Me.btnMedicine.TabIndex = 45
        Me.btnMedicine.Tag = "SCD"
        Me.btnMedicine.Text = "Medicine &&" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Incidents"
        '
        'btnLookupChild
        '
        Me.btnLookupChild.Appearance.Options.UseTextOptions = True
        Me.btnLookupChild.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnLookupChild.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnLookupChild.Image = CType(resources.GetObject("btnLookupChild.Image"), System.Drawing.Image)
        Me.btnLookupChild.Location = New System.Drawing.Point(873, 312)
        Me.btnLookupChild.Name = "btnLookupChild"
        Me.btnLookupChild.Size = New System.Drawing.Size(284, 97)
        Me.btnLookupChild.TabIndex = 44
        Me.btnLookupChild.Tag = "SC"
        Me.btnLookupChild.Text = "Lookup Child"
        '
        'btnRequests
        '
        Me.btnRequests.Appearance.Options.UseTextOptions = True
        Me.btnRequests.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnRequests.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnRequests.Image = CType(resources.GetObject("btnRequests.Image"), System.Drawing.Image)
        Me.btnRequests.Location = New System.Drawing.Point(583, 106)
        Me.btnRequests.Name = "btnRequests"
        Me.btnRequests.Size = New System.Drawing.Size(284, 97)
        Me.btnRequests.TabIndex = 42
        Me.btnRequests.Tag = "SC"
        Me.btnRequests.Text = "Request Centre"
        '
        'btnRegister
        '
        Me.btnRegister.Appearance.Options.UseTextOptions = True
        Me.btnRegister.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnRegister.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnRegister.Image = CType(resources.GetObject("btnRegister.Image"), System.Drawing.Image)
        Me.btnRegister.Location = New System.Drawing.Point(583, 3)
        Me.btnRegister.Name = "btnRegister"
        Me.btnRegister.Size = New System.Drawing.Size(284, 97)
        Me.btnRegister.TabIndex = 41
        Me.btnRegister.Tag = "SCD"
        Me.btnRegister.Text = "Register"
        '
        'btnCheckOutStaff
        '
        Me.btnCheckOutStaff.Appearance.Options.UseTextOptions = True
        Me.btnCheckOutStaff.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnCheckOutStaff.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnCheckOutStaff.Image = CType(resources.GetObject("btnCheckOutStaff.Image"), System.Drawing.Image)
        Me.btnCheckOutStaff.Location = New System.Drawing.Point(293, 106)
        Me.btnCheckOutStaff.Name = "btnCheckOutStaff"
        Me.btnCheckOutStaff.Size = New System.Drawing.Size(284, 97)
        Me.btnCheckOutStaff.TabIndex = 40
        Me.btnCheckOutStaff.Tag = "SC"
        Me.btnCheckOutStaff.Text = "Check Out" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Staff"
        '
        'btnCheckOutChild
        '
        Me.btnCheckOutChild.Appearance.Options.UseTextOptions = True
        Me.btnCheckOutChild.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnCheckOutChild.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnCheckOutChild.Image = CType(resources.GetObject("btnCheckOutChild.Image"), System.Drawing.Image)
        Me.btnCheckOutChild.Location = New System.Drawing.Point(293, 3)
        Me.btnCheckOutChild.Name = "btnCheckOutChild"
        Me.btnCheckOutChild.Size = New System.Drawing.Size(284, 97)
        Me.btnCheckOutChild.TabIndex = 39
        Me.btnCheckOutChild.Tag = "SC"
        Me.btnCheckOutChild.Text = "Check Out" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Child"
        '
        'btnCheckInStaff
        '
        Me.btnCheckInStaff.Appearance.Options.UseTextOptions = True
        Me.btnCheckInStaff.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnCheckInStaff.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnCheckInStaff.Image = CType(resources.GetObject("btnCheckInStaff.Image"), System.Drawing.Image)
        Me.btnCheckInStaff.Location = New System.Drawing.Point(3, 106)
        Me.btnCheckInStaff.Name = "btnCheckInStaff"
        Me.btnCheckInStaff.Size = New System.Drawing.Size(284, 97)
        Me.btnCheckInStaff.TabIndex = 38
        Me.btnCheckInStaff.Tag = ""
        Me.btnCheckInStaff.Text = "Check In" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Staff"
        '
        'btnCheckInChild
        '
        Me.btnCheckInChild.Appearance.Options.UseTextOptions = True
        Me.btnCheckInChild.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnCheckInChild.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnCheckInChild.Image = CType(resources.GetObject("btnCheckInChild.Image"), System.Drawing.Image)
        Me.btnCheckInChild.Location = New System.Drawing.Point(3, 3)
        Me.btnCheckInChild.Name = "btnCheckInChild"
        Me.btnCheckInChild.Size = New System.Drawing.Size(284, 97)
        Me.btnCheckInChild.TabIndex = 37
        Me.btnCheckInChild.Tag = "SC"
        Me.btnCheckInChild.Text = "Check In" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Child"
        '
        'btnMilk
        '
        Me.btnMilk.Appearance.Options.UseTextOptions = True
        Me.btnMilk.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnMilk.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnMilk.Image = CType(resources.GetObject("btnMilk.Image"), System.Drawing.Image)
        Me.btnMilk.Location = New System.Drawing.Point(583, 209)
        Me.btnMilk.Name = "btnMilk"
        Me.btnMilk.Size = New System.Drawing.Size(284, 97)
        Me.btnMilk.TabIndex = 36
        Me.btnMilk.Tag = "S"
        Me.btnMilk.Text = "Milk Records"
        '
        'btnFood
        '
        Me.btnFood.Appearance.Options.UseTextOptions = True
        Me.btnFood.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnFood.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnFood.Image = CType(resources.GetObject("btnFood.Image"), System.Drawing.Image)
        Me.btnFood.Location = New System.Drawing.Point(293, 209)
        Me.btnFood.Name = "btnFood"
        Me.btnFood.Size = New System.Drawing.Size(284, 97)
        Me.btnFood.TabIndex = 35
        Me.btnFood.Tag = "S"
        Me.btnFood.Text = "Food"
        '
        'btnToilet
        '
        Me.btnToilet.Appearance.Options.UseTextOptions = True
        Me.btnToilet.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnToilet.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnToilet.Image = CType(resources.GetObject("btnToilet.Image"), System.Drawing.Image)
        Me.btnToilet.Location = New System.Drawing.Point(3, 312)
        Me.btnToilet.Name = "btnToilet"
        Me.btnToilet.Size = New System.Drawing.Size(284, 97)
        Me.btnToilet.TabIndex = 34
        Me.btnToilet.Tag = "S"
        Me.btnToilet.Text = "Toilet Records"
        '
        'btnToiletTrip
        '
        Me.btnToiletTrip.Appearance.Options.UseTextOptions = True
        Me.btnToiletTrip.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnToiletTrip.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnToiletTrip.Image = CType(resources.GetObject("btnToiletTrip.Image"), System.Drawing.Image)
        Me.btnToiletTrip.Location = New System.Drawing.Point(293, 312)
        Me.btnToiletTrip.Name = "btnToiletTrip"
        Me.btnToiletTrip.Size = New System.Drawing.Size(284, 97)
        Me.btnToiletTrip.TabIndex = 33
        Me.btnToiletTrip.Tag = "S"
        Me.btnToiletTrip.Text = "Toilet Trip"
        '
        'btnSleep
        '
        Me.btnSleep.Appearance.Options.UseTextOptions = True
        Me.btnSleep.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnSleep.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnSleep.Image = CType(resources.GetObject("btnSleep.Image"), System.Drawing.Image)
        Me.btnSleep.Location = New System.Drawing.Point(3, 209)
        Me.btnSleep.Name = "btnSleep"
        Me.btnSleep.Size = New System.Drawing.Size(284, 97)
        Me.btnSleep.TabIndex = 28
        Me.btnSleep.Tag = "S"
        Me.btnSleep.Text = "Sleep" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Management"
        '
        'dshMedication
        '
        Me.dshMedication.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.dshMedication.Appearance.Options.UseBackColor = True
        Me.dshMedication.Colour = NurseryTablet.DashboardLabel.EnumColourCode.Green
        Me.dshMedication.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dshMedication.LabelBottom = "Minutes"
        Me.dshMedication.LabelMiddle = "OK"
        Me.dshMedication.LabelTop = "Next Medication"
        Me.dshMedication.Location = New System.Drawing.Point(873, 3)
        Me.dshMedication.Name = "dshMedication"
        Me.tlp.SetRowSpan(Me.dshMedication, 3)
        Me.dshMedication.Size = New System.Drawing.Size(284, 303)
        Me.dshMedication.TabIndex = 50
        '
        'frmMain
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1184, 542)
        Me.Controls.Add(Me.tlp)
        Me.Name = "frmMain"
        Me.Text = "Nursery Genie"
        Me.Controls.SetChildIndex(Me.tlp, 0)
        Me.tlp.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents timCountdown As System.Windows.Forms.Timer
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents SplashScreenManager1 As DevExpress.XtraSplashScreen.SplashScreenManager
    Friend WithEvents tlp As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnSleep As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSync As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnOptions As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnExit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnHelp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMedicine As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnLookupChild As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRequests As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRegister As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCheckOutStaff As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCheckOutChild As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCheckInStaff As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCheckInChild As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMilk As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnFood As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnToilet As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnToiletTrip As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dshMedication As NurseryTablet.DashboardLabel
End Class
