﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTextView
    Inherits NurseryTablet.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.rich = New DevExpress.XtraRichEdit.RichEditControl()
        Me.SuspendLayout()
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Location = New System.Drawing.Point(850, 495)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(162, 57)
        Me.btnClose.TabIndex = 37
        Me.btnClose.Text = "Close"
        '
        'rich
        '
        Me.rich.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple
        Me.rich.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rich.Appearance.Text.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rich.Appearance.Text.Options.UseFont = True
        Me.rich.EnableToolTips = True
        Me.rich.Location = New System.Drawing.Point(12, 12)
        Me.rich.Name = "rich"
        Me.rich.ReadOnly = True
        Me.rich.ShowCaretInReadOnly = False
        Me.rich.Size = New System.Drawing.Size(1000, 477)
        Me.rich.TabIndex = 0
        '
        'frmTextView
        '
        Me.ClientSize = New System.Drawing.Size(1024, 560)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.rich)
        Me.Name = "frmTextView"
        Me.Text = ""
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents rich As DevExpress.XtraRichEdit.RichEditControl
    Friend WithEvents btnClose As System.Windows.Forms.Button

End Class
