﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSync
    Inherits frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.d5 = New NurseryTablet.DashboardLabel()
        Me.d6 = New NurseryTablet.DashboardLabel()
        Me.d8 = New NurseryTablet.DashboardLabel()
        Me.d7 = New NurseryTablet.DashboardLabel()
        Me.d10 = New NurseryTablet.DashboardLabel()
        Me.d9 = New NurseryTablet.DashboardLabel()
        Me.d16 = New NurseryTablet.DashboardLabel()
        Me.d15 = New NurseryTablet.DashboardLabel()
        Me.d14 = New NurseryTablet.DashboardLabel()
        Me.d13 = New NurseryTablet.DashboardLabel()
        Me.d12 = New NurseryTablet.DashboardLabel()
        Me.d11 = New NurseryTablet.DashboardLabel()
        Me.d4 = New NurseryTablet.DashboardLabel()
        Me.d3 = New NurseryTablet.DashboardLabel()
        Me.d2 = New NurseryTablet.DashboardLabel()
        Me.d1 = New NurseryTablet.DashboardLabel()
        Me.dshServer = New NurseryTablet.DashboardLabel()
        Me.dshGoogle = New NurseryTablet.DashboardLabel()
        Me.bgwPingGoogle = New System.ComponentModel.BackgroundWorker()
        Me.bgwPingService = New System.ComponentModel.BackgroundWorker()
        Me.timSecond = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        '
        'd5
        '
        Me.d5.Colour = NurseryTablet.DashboardLabel.EnumColourCode.Neutral
        Me.d5.LabelBottom = ""
        Me.d5.LabelMiddle = ""
        Me.d5.LabelTop = "Top"
        Me.d5.Location = New System.Drawing.Point(12, 168)
        Me.d5.Name = "d5"
        Me.d5.Size = New System.Drawing.Size(150, 150)
        Me.d5.TabIndex = 95
        '
        'd6
        '
        Me.d6.Colour = NurseryTablet.DashboardLabel.EnumColourCode.Neutral
        Me.d6.LabelBottom = ""
        Me.d6.LabelMiddle = ""
        Me.d6.LabelTop = "Top"
        Me.d6.Location = New System.Drawing.Point(168, 168)
        Me.d6.Name = "d6"
        Me.d6.Size = New System.Drawing.Size(150, 150)
        Me.d6.TabIndex = 96
        '
        'd8
        '
        Me.d8.Colour = NurseryTablet.DashboardLabel.EnumColourCode.Neutral
        Me.d8.LabelBottom = ""
        Me.d8.LabelMiddle = ""
        Me.d8.LabelTop = "Top"
        Me.d8.Location = New System.Drawing.Point(480, 168)
        Me.d8.Name = "d8"
        Me.d8.Size = New System.Drawing.Size(150, 150)
        Me.d8.TabIndex = 98
        '
        'd7
        '
        Me.d7.Colour = NurseryTablet.DashboardLabel.EnumColourCode.Neutral
        Me.d7.LabelBottom = ""
        Me.d7.LabelMiddle = ""
        Me.d7.LabelTop = "Top"
        Me.d7.Location = New System.Drawing.Point(324, 168)
        Me.d7.Name = "d7"
        Me.d7.Size = New System.Drawing.Size(150, 150)
        Me.d7.TabIndex = 97
        '
        'd10
        '
        Me.d10.Colour = NurseryTablet.DashboardLabel.EnumColourCode.Neutral
        Me.d10.LabelBottom = ""
        Me.d10.LabelMiddle = ""
        Me.d10.LabelTop = "Top"
        Me.d10.Location = New System.Drawing.Point(792, 168)
        Me.d10.Name = "d10"
        Me.d10.Size = New System.Drawing.Size(150, 150)
        Me.d10.TabIndex = 100
        '
        'd9
        '
        Me.d9.Colour = NurseryTablet.DashboardLabel.EnumColourCode.Neutral
        Me.d9.LabelBottom = ""
        Me.d9.LabelMiddle = ""
        Me.d9.LabelTop = "Top"
        Me.d9.Location = New System.Drawing.Point(636, 168)
        Me.d9.Name = "d9"
        Me.d9.Size = New System.Drawing.Size(150, 150)
        Me.d9.TabIndex = 99
        '
        'd16
        '
        Me.d16.Colour = NurseryTablet.DashboardLabel.EnumColourCode.Neutral
        Me.d16.LabelBottom = ""
        Me.d16.LabelMiddle = ""
        Me.d16.LabelTop = "Top"
        Me.d16.Location = New System.Drawing.Point(792, 324)
        Me.d16.Name = "d16"
        Me.d16.Size = New System.Drawing.Size(150, 150)
        Me.d16.TabIndex = 106
        '
        'd15
        '
        Me.d15.Colour = NurseryTablet.DashboardLabel.EnumColourCode.Neutral
        Me.d15.LabelBottom = ""
        Me.d15.LabelMiddle = ""
        Me.d15.LabelTop = "Top"
        Me.d15.Location = New System.Drawing.Point(636, 324)
        Me.d15.Name = "d15"
        Me.d15.Size = New System.Drawing.Size(150, 150)
        Me.d15.TabIndex = 105
        '
        'd14
        '
        Me.d14.Colour = NurseryTablet.DashboardLabel.EnumColourCode.Neutral
        Me.d14.LabelBottom = ""
        Me.d14.LabelMiddle = ""
        Me.d14.LabelTop = "Top"
        Me.d14.Location = New System.Drawing.Point(480, 324)
        Me.d14.Name = "d14"
        Me.d14.Size = New System.Drawing.Size(150, 150)
        Me.d14.TabIndex = 104
        '
        'd13
        '
        Me.d13.Colour = NurseryTablet.DashboardLabel.EnumColourCode.Neutral
        Me.d13.LabelBottom = ""
        Me.d13.LabelMiddle = ""
        Me.d13.LabelTop = "Top"
        Me.d13.Location = New System.Drawing.Point(324, 324)
        Me.d13.Name = "d13"
        Me.d13.Size = New System.Drawing.Size(150, 150)
        Me.d13.TabIndex = 103
        '
        'd12
        '
        Me.d12.Colour = NurseryTablet.DashboardLabel.EnumColourCode.Neutral
        Me.d12.LabelBottom = ""
        Me.d12.LabelMiddle = ""
        Me.d12.LabelTop = "Top"
        Me.d12.Location = New System.Drawing.Point(168, 324)
        Me.d12.Name = "d12"
        Me.d12.Size = New System.Drawing.Size(150, 150)
        Me.d12.TabIndex = 102
        '
        'd11
        '
        Me.d11.Colour = NurseryTablet.DashboardLabel.EnumColourCode.Neutral
        Me.d11.LabelBottom = ""
        Me.d11.LabelMiddle = ""
        Me.d11.LabelTop = "Top"
        Me.d11.Location = New System.Drawing.Point(12, 324)
        Me.d11.Name = "d11"
        Me.d11.Size = New System.Drawing.Size(150, 150)
        Me.d11.TabIndex = 101
        '
        'd4
        '
        Me.d4.Colour = NurseryTablet.DashboardLabel.EnumColourCode.Neutral
        Me.d4.LabelBottom = ""
        Me.d4.LabelMiddle = ""
        Me.d4.LabelTop = "Top"
        Me.d4.Location = New System.Drawing.Point(792, 12)
        Me.d4.Name = "d4"
        Me.d4.Size = New System.Drawing.Size(150, 150)
        Me.d4.TabIndex = 112
        '
        'd3
        '
        Me.d3.Colour = NurseryTablet.DashboardLabel.EnumColourCode.Neutral
        Me.d3.LabelBottom = ""
        Me.d3.LabelMiddle = ""
        Me.d3.LabelTop = "Top"
        Me.d3.Location = New System.Drawing.Point(636, 12)
        Me.d3.Name = "d3"
        Me.d3.Size = New System.Drawing.Size(150, 150)
        Me.d3.TabIndex = 111
        '
        'd2
        '
        Me.d2.Colour = NurseryTablet.DashboardLabel.EnumColourCode.Neutral
        Me.d2.LabelBottom = ""
        Me.d2.LabelMiddle = ""
        Me.d2.LabelTop = "Top"
        Me.d2.Location = New System.Drawing.Point(480, 12)
        Me.d2.Name = "d2"
        Me.d2.Size = New System.Drawing.Size(150, 150)
        Me.d2.TabIndex = 110
        '
        'd1
        '
        Me.d1.Colour = NurseryTablet.DashboardLabel.EnumColourCode.Neutral
        Me.d1.LabelBottom = ""
        Me.d1.LabelMiddle = ""
        Me.d1.LabelTop = "Top"
        Me.d1.Location = New System.Drawing.Point(324, 12)
        Me.d1.Name = "d1"
        Me.d1.Size = New System.Drawing.Size(150, 150)
        Me.d1.TabIndex = 109
        '
        'dshServer
        '
        Me.dshServer.Colour = NurseryTablet.DashboardLabel.EnumColourCode.Neutral
        Me.dshServer.LabelBottom = "Response ms"
        Me.dshServer.LabelMiddle = "55"
        Me.dshServer.LabelTop = "Service"
        Me.dshServer.Location = New System.Drawing.Point(168, 12)
        Me.dshServer.Name = "dshServer"
        Me.dshServer.Size = New System.Drawing.Size(150, 150)
        Me.dshServer.TabIndex = 108
        '
        'dshGoogle
        '
        Me.dshGoogle.Colour = NurseryTablet.DashboardLabel.EnumColourCode.Neutral
        Me.dshGoogle.LabelBottom = "Response ms"
        Me.dshGoogle.LabelMiddle = "55"
        Me.dshGoogle.LabelTop = "Google.co.uk"
        Me.dshGoogle.Location = New System.Drawing.Point(12, 12)
        Me.dshGoogle.Name = "dshGoogle"
        Me.dshGoogle.Size = New System.Drawing.Size(150, 150)
        Me.dshGoogle.TabIndex = 107
        '
        'bgwPingGoogle
        '
        Me.bgwPingGoogle.WorkerSupportsCancellation = True
        '
        'bgwPingService
        '
        Me.bgwPingService.WorkerSupportsCancellation = True
        '
        'timSecond
        '
        Me.timSecond.Interval = 1000
        '
        'frmSync
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(11.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(955, 480)
        Me.Controls.Add(Me.d4)
        Me.Controls.Add(Me.d3)
        Me.Controls.Add(Me.d2)
        Me.Controls.Add(Me.d1)
        Me.Controls.Add(Me.dshServer)
        Me.Controls.Add(Me.dshGoogle)
        Me.Controls.Add(Me.d16)
        Me.Controls.Add(Me.d15)
        Me.Controls.Add(Me.d14)
        Me.Controls.Add(Me.d10)
        Me.Controls.Add(Me.d9)
        Me.Controls.Add(Me.d8)
        Me.Controls.Add(Me.d7)
        Me.Controls.Add(Me.d6)
        Me.Controls.Add(Me.d5)
        Me.Controls.Add(Me.d13)
        Me.Controls.Add(Me.d12)
        Me.Controls.Add(Me.d11)
        Me.Name = "frmSync"
        Me.Text = "frmSync"
        Me.Controls.SetChildIndex(Me.d11, 0)
        Me.Controls.SetChildIndex(Me.d12, 0)
        Me.Controls.SetChildIndex(Me.d13, 0)
        Me.Controls.SetChildIndex(Me.d5, 0)
        Me.Controls.SetChildIndex(Me.d6, 0)
        Me.Controls.SetChildIndex(Me.d7, 0)
        Me.Controls.SetChildIndex(Me.d8, 0)
        Me.Controls.SetChildIndex(Me.d9, 0)
        Me.Controls.SetChildIndex(Me.d10, 0)
        Me.Controls.SetChildIndex(Me.d14, 0)
        Me.Controls.SetChildIndex(Me.d15, 0)
        Me.Controls.SetChildIndex(Me.d16, 0)
        Me.Controls.SetChildIndex(Me.dshGoogle, 0)
        Me.Controls.SetChildIndex(Me.dshServer, 0)
        Me.Controls.SetChildIndex(Me.d1, 0)
        Me.Controls.SetChildIndex(Me.d2, 0)
        Me.Controls.SetChildIndex(Me.d3, 0)
        Me.Controls.SetChildIndex(Me.d4, 0)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents d5 As NurseryTablet.DashboardLabel
    Friend WithEvents d6 As NurseryTablet.DashboardLabel
    Friend WithEvents d8 As NurseryTablet.DashboardLabel
    Friend WithEvents d7 As NurseryTablet.DashboardLabel
    Friend WithEvents d10 As NurseryTablet.DashboardLabel
    Friend WithEvents d9 As NurseryTablet.DashboardLabel
    Friend WithEvents d16 As NurseryTablet.DashboardLabel
    Friend WithEvents d15 As NurseryTablet.DashboardLabel
    Friend WithEvents d14 As NurseryTablet.DashboardLabel
    Friend WithEvents d13 As NurseryTablet.DashboardLabel
    Friend WithEvents d12 As NurseryTablet.DashboardLabel
    Friend WithEvents d11 As NurseryTablet.DashboardLabel
    Friend WithEvents d4 As NurseryTablet.DashboardLabel
    Friend WithEvents d3 As NurseryTablet.DashboardLabel
    Friend WithEvents d2 As NurseryTablet.DashboardLabel
    Friend WithEvents d1 As NurseryTablet.DashboardLabel
    Friend WithEvents dshServer As NurseryTablet.DashboardLabel
    Friend WithEvents dshGoogle As NurseryTablet.DashboardLabel
    Friend WithEvents bgwPingGoogle As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgwPingService As System.ComponentModel.BackgroundWorker
    Friend WithEvents timSecond As System.Windows.Forms.Timer
End Class
