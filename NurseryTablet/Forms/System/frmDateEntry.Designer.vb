﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDateEntry
    Inherits NurseryTablet.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDateEntry))
        Me.btnToday = New System.Windows.Forms.Button()
        Me.txtText = New System.Windows.Forms.TextBox()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnAccept = New System.Windows.Forms.Button()
        Me.dn = New DevExpress.XtraScheduler.DateNavigator()
        CType(Me.dn, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        '
        'btnToday
        '
        Me.btnToday.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnToday.Image = Global.NurseryTablet.My.Resources.Resources.alarm_32
        Me.btnToday.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnToday.Location = New System.Drawing.Point(616, 12)
        Me.btnToday.Name = "btnToday"
        Me.btnToday.Size = New System.Drawing.Size(145, 61)
        Me.btnToday.TabIndex = 1
        Me.btnToday.Text = "Today"
        Me.btnToday.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnToday.UseVisualStyleBackColor = True
        '
        'txtText
        '
        Me.txtText.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtText.Location = New System.Drawing.Point(14, 12)
        Me.txtText.Multiline = True
        Me.txtText.Name = "txtText"
        Me.txtText.Size = New System.Drawing.Size(594, 61)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtText, OptionsSpelling1)
        Me.txtText.TabIndex = 0
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Image = CType(resources.GetObject("btnCancel.Image"), System.Drawing.Image)
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(616, 433)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(145, 57)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnAccept
        '
        Me.btnAccept.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAccept.Image = CType(resources.GetObject("btnAccept.Image"), System.Drawing.Image)
        Me.btnAccept.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAccept.Location = New System.Drawing.Point(616, 370)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(145, 57)
        Me.btnAccept.TabIndex = 3
        Me.btnAccept.Text = "Accept"
        Me.btnAccept.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAccept.UseVisualStyleBackColor = True
        '
        'dn
        '
        Me.dn.AppearanceCalendar.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dn.AppearanceCalendar.Options.UseFont = True
        Me.dn.AppearanceDisabledCalendarDate.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dn.AppearanceDisabledCalendarDate.Options.UseFont = True
        Me.dn.AppearanceHeader.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dn.AppearanceHeader.Options.UseFont = True
        Me.dn.AppearanceWeekNumber.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dn.AppearanceWeekNumber.Options.UseFont = True
        Me.dn.DateTime = New Date(2015, 8, 16, 0, 0, 0, 0)
        Me.dn.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dn.HighlightTodayCell = DevExpress.Utils.DefaultBoolean.[Default]
        Me.dn.HotDate = Nothing
        Me.dn.Location = New System.Drawing.Point(14, 82)
        Me.dn.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.dn.Multiselect = False
        Me.dn.Name = "dn"
        Me.dn.CellPadding = New System.Windows.Forms.Padding(2)
        Me.dn.ShowTodayButton = False
        Me.dn.ShowWeekNumbers = False
        Me.dn.Size = New System.Drawing.Size(594, 408)
        Me.dn.TabIndex = 2
        '
        'frmDateEntry
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(773, 505)
        Me.Controls.Add(Me.btnToday)
        Me.Controls.Add(Me.txtText)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnAccept)
        Me.Controls.Add(Me.dn)
        Me.Font = New System.Drawing.Font("Segoe UI", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(11, 12, 11, 12)
        Me.Name = "frmDateEntry"
        Me.Text = "Select Date"
        CType(Me.dn, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dn As DevExpress.XtraScheduler.DateNavigator
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnAccept As System.Windows.Forms.Button
    Friend WithEvents txtText As System.Windows.Forms.TextBox
    Friend WithEvents btnToday As System.Windows.Forms.Button
End Class
