﻿Public Class frmGrid

    Private m_Data As IEnumerable = Nothing

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub frmGrid_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Cursor = Cursors.WaitCursor
        TouchGrid1.Populate(m_Data)
        Me.Cursor = Cursors.Default
    End Sub

    Public Sub New(ByVal DataIn As IEnumerable)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_Data = DataIn

    End Sub
End Class
