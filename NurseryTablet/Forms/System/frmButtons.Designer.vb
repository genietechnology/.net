﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmButtons
    Inherits NurseryTablet.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmButtons))
        Me.btn1 = New System.Windows.Forms.Button()
        Me.btn4 = New System.Windows.Forms.Button()
        Me.btn7 = New System.Windows.Forms.Button()
        Me.btn16 = New System.Windows.Forms.Button()
        Me.btn13 = New System.Windows.Forms.Button()
        Me.btn10 = New System.Windows.Forms.Button()
        Me.btnPrev = New System.Windows.Forms.Button()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.btn17 = New System.Windows.Forms.Button()
        Me.btn14 = New System.Windows.Forms.Button()
        Me.btn11 = New System.Windows.Forms.Button()
        Me.btn8 = New System.Windows.Forms.Button()
        Me.btn5 = New System.Windows.Forms.Button()
        Me.btn2 = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btn18 = New System.Windows.Forms.Button()
        Me.btn15 = New System.Windows.Forms.Button()
        Me.btn12 = New System.Windows.Forms.Button()
        Me.btn9 = New System.Windows.Forms.Button()
        Me.btn6 = New System.Windows.Forms.Button()
        Me.btn3 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btn1
        '
        Me.btn1.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn1.Location = New System.Drawing.Point(12, 12)
        Me.btn1.Name = "btn1"
        Me.btn1.Size = New System.Drawing.Size(320, 57)
        Me.btn1.TabIndex = 13
        '
        'btn4
        '
        Me.btn4.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn4.Location = New System.Drawing.Point(12, 88)
        Me.btn4.Name = "btn4"
        Me.btn4.Size = New System.Drawing.Size(320, 57)
        Me.btn4.TabIndex = 14
        '
        'btn7
        '
        Me.btn7.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn7.Location = New System.Drawing.Point(12, 164)
        Me.btn7.Name = "btn7"
        Me.btn7.Size = New System.Drawing.Size(320, 57)
        Me.btn7.TabIndex = 15
        '
        'btn16
        '
        Me.btn16.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn16.Location = New System.Drawing.Point(12, 391)
        Me.btn16.Name = "btn16"
        Me.btn16.Size = New System.Drawing.Size(320, 57)
        Me.btn16.TabIndex = 18
        '
        'btn13
        '
        Me.btn13.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn13.Location = New System.Drawing.Point(12, 315)
        Me.btn13.Name = "btn13"
        Me.btn13.Size = New System.Drawing.Size(320, 57)
        Me.btn13.TabIndex = 17
        '
        'btn10
        '
        Me.btn10.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn10.Location = New System.Drawing.Point(12, 239)
        Me.btn10.Name = "btn10"
        Me.btn10.Size = New System.Drawing.Size(320, 57)
        Me.btn10.TabIndex = 16
        '
        'btnPrev
        '
        Me.btnPrev.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrev.Image = CType(resources.GetObject("btnPrev.Image"), System.Drawing.Image)
        Me.btnPrev.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPrev.Location = New System.Drawing.Point(12, 465)
        Me.btnPrev.Name = "btnPrev"
        Me.btnPrev.Size = New System.Drawing.Size(320, 57)
        Me.btnPrev.TabIndex = 19
        Me.btnPrev.Text = "Previous"
        '
        'btnNext
        '
        Me.btnNext.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNext.Image = CType(resources.GetObject("btnNext.Image"), System.Drawing.Image)
        Me.btnNext.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNext.Location = New System.Drawing.Point(352, 465)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(320, 57)
        Me.btnNext.TabIndex = 26
        Me.btnNext.Text = "Next"
        '
        'btn17
        '
        Me.btn17.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn17.Location = New System.Drawing.Point(352, 391)
        Me.btn17.Name = "btn17"
        Me.btn17.Size = New System.Drawing.Size(320, 57)
        Me.btn17.TabIndex = 25
        '
        'btn14
        '
        Me.btn14.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn14.Location = New System.Drawing.Point(352, 315)
        Me.btn14.Name = "btn14"
        Me.btn14.Size = New System.Drawing.Size(320, 57)
        Me.btn14.TabIndex = 24
        '
        'btn11
        '
        Me.btn11.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn11.Location = New System.Drawing.Point(352, 239)
        Me.btn11.Name = "btn11"
        Me.btn11.Size = New System.Drawing.Size(320, 57)
        Me.btn11.TabIndex = 23
        '
        'btn8
        '
        Me.btn8.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn8.Location = New System.Drawing.Point(352, 164)
        Me.btn8.Name = "btn8"
        Me.btn8.Size = New System.Drawing.Size(320, 57)
        Me.btn8.TabIndex = 22
        '
        'btn5
        '
        Me.btn5.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn5.Location = New System.Drawing.Point(352, 88)
        Me.btn5.Name = "btn5"
        Me.btn5.Size = New System.Drawing.Size(320, 57)
        Me.btn5.TabIndex = 21
        '
        'btn2
        '
        Me.btn2.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn2.Location = New System.Drawing.Point(352, 12)
        Me.btn2.Name = "btn2"
        Me.btn2.Size = New System.Drawing.Size(320, 57)
        Me.btn2.TabIndex = 20
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(690, 465)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(320, 57)
        Me.btnCancel.TabIndex = 33
        Me.btnCancel.Text = "Cancel"
        '
        'btn18
        '
        Me.btn18.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn18.Location = New System.Drawing.Point(690, 391)
        Me.btn18.Name = "btn18"
        Me.btn18.Size = New System.Drawing.Size(320, 57)
        Me.btn18.TabIndex = 32
        '
        'btn15
        '
        Me.btn15.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn15.Location = New System.Drawing.Point(690, 315)
        Me.btn15.Name = "btn15"
        Me.btn15.Size = New System.Drawing.Size(320, 57)
        Me.btn15.TabIndex = 31
        '
        'btn12
        '
        Me.btn12.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn12.Location = New System.Drawing.Point(690, 239)
        Me.btn12.Name = "btn12"
        Me.btn12.Size = New System.Drawing.Size(320, 57)
        Me.btn12.TabIndex = 30
        '
        'btn9
        '
        Me.btn9.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn9.Location = New System.Drawing.Point(690, 164)
        Me.btn9.Name = "btn9"
        Me.btn9.Size = New System.Drawing.Size(320, 57)
        Me.btn9.TabIndex = 29
        '
        'btn6
        '
        Me.btn6.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn6.Location = New System.Drawing.Point(690, 88)
        Me.btn6.Name = "btn6"
        Me.btn6.Size = New System.Drawing.Size(320, 57)
        Me.btn6.TabIndex = 28
        '
        'btn3
        '
        Me.btn3.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn3.Location = New System.Drawing.Point(690, 12)
        Me.btn3.Name = "btn3"
        Me.btn3.Size = New System.Drawing.Size(320, 57)
        Me.btn3.TabIndex = 27
        '
        'frmButtons
        '
        Me.ClientSize = New System.Drawing.Size(1024, 538)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btn18)
        Me.Controls.Add(Me.btn15)
        Me.Controls.Add(Me.btn12)
        Me.Controls.Add(Me.btn9)
        Me.Controls.Add(Me.btn6)
        Me.Controls.Add(Me.btn3)
        Me.Controls.Add(Me.btnNext)
        Me.Controls.Add(Me.btn17)
        Me.Controls.Add(Me.btn14)
        Me.Controls.Add(Me.btn11)
        Me.Controls.Add(Me.btn8)
        Me.Controls.Add(Me.btn5)
        Me.Controls.Add(Me.btn2)
        Me.Controls.Add(Me.btnPrev)
        Me.Controls.Add(Me.btn16)
        Me.Controls.Add(Me.btn13)
        Me.Controls.Add(Me.btn10)
        Me.Controls.Add(Me.btn7)
        Me.Controls.Add(Me.btn4)
        Me.Controls.Add(Me.btn1)
        Me.Name = "frmButtons"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btn1 As System.Windows.Forms.Button
    Friend WithEvents btn4 As System.Windows.Forms.Button
    Friend WithEvents btn7 As System.Windows.Forms.Button
    Friend WithEvents btn16 As System.Windows.Forms.Button
    Friend WithEvents btn13 As System.Windows.Forms.Button
    Friend WithEvents btn10 As System.Windows.Forms.Button
    Friend WithEvents btnPrev As System.Windows.Forms.Button
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents btn17 As System.Windows.Forms.Button
    Friend WithEvents btn14 As System.Windows.Forms.Button
    Friend WithEvents btn11 As System.Windows.Forms.Button
    Friend WithEvents btn8 As System.Windows.Forms.Button
    Friend WithEvents btn5 As System.Windows.Forms.Button
    Friend WithEvents btn2 As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btn18 As System.Windows.Forms.Button
    Friend WithEvents btn15 As System.Windows.Forms.Button
    Friend WithEvents btn12 As System.Windows.Forms.Button
    Friend WithEvents btn9 As System.Windows.Forms.Button
    Friend WithEvents btn6 As System.Windows.Forms.Button
    Friend WithEvents btn3 As System.Windows.Forms.Button

End Class
