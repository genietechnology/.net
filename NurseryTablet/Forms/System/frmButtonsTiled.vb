﻿Option Strict On

Imports NurseryTablet.SharedModule
Imports DevExpress.XtraBars.Navigation
Imports DevExpress.XtraEditors
Imports System.IO

Public Class frmButtonsTiled

    Private m_Buttons As List(Of Pair) = Nothing
    Private m_ReturnPair As Pair = Nothing
    Private m_ReturnPairs As List(Of Pair) = Nothing
    Private m_CurrentFolder As String = ""

    Public Enum EnumPopulateMode
        Custom
        FileSystem
    End Enum

#Region "Properties"

    Public Property PopulateMode As EnumPopulateMode
    Public Property RoomFilter As String
    Public Property MultiSelect As Boolean

    Public Property SQL As String
    Public Property FieldID As String
    Public Property FieldName As String
    Public Property FieldBottomLeft As String
    Public Property FieldBottomRight As String
    Public Property FieldPhoto As String

    Public Property RootPath As String

    Public WriteOnly Property ButtonCollection As List(Of Pair)
        Set(value As List(Of Pair))
            m_Buttons = value
        End Set
    End Property

    Public ReadOnly Property SelectedPair As Pair
        Get
            Return m_ReturnPair
        End Get
    End Property

    Public ReadOnly Property SelectedPairs As List(Of Pair)
        Get
            Return m_ReturnPairs
        End Get
    End Property

#End Region

    Private Sub frmButtonsTiled_Load(sender As Object, e As EventArgs) Handles Me.Load

        PopulateButtons()
        btnAccept.Visible = MultiSelect

    End Sub

    Private Sub PopulateButtons()

        tg.Items.Clear()

        Select Case PopulateMode

            Case EnumPopulateMode.Custom
                If SQL IsNot Nothing AndAlso SQL.Contains("|") Then
                    PopulateHardCoded()
                Else
                    PopulateCustom()
                End If

            Case EnumPopulateMode.FileSystem
                PopulateFilesandFolders()

        End Select

    End Sub

    Private Sub PopulateFilesandFolders()
        If RootPath = "" Then Exit Sub
        m_CurrentFolder = RootPath
        PopulateFolder()
    End Sub

    Private Sub PopulateFolder()

        tg.Items.Clear()

        If m_CurrentFolder <> RootPath Then
            Dim _i As New TileBarItem
            _i.Name = "btnHome"
            _i.Text = "Back to Home Folder"
            _i.AppearanceItem.Normal.BackColor = Color.DarkKhaki
            AddHandler _i.ItemClick, AddressOf TileClick
            tg.Items.Add(_i)
        End If

        For Each _Folder As String In IO.Directory.EnumerateDirectories(m_CurrentFolder)

            Dim _di As New IO.DirectoryInfo(_Folder)
            Dim _i As New TileBarItem

            _i.Name = "dir_" + _Folder
            _i.Text = _di.Name
            _i.AppearanceItem.Normal.BackColor = Color.DarkKhaki
            AddHandler _i.ItemClick, AddressOf TileClick
            tg.Items.Add(_i)

        Next

        For Each _File As String In IO.Directory.EnumerateFiles(m_CurrentFolder)

            Dim _fi As New IO.FileInfo(_File)
            Dim _i As New TileBarItem

            _i.Name = "file_" + _File
            _i.Text = _fi.Name

            AddHandler _i.ItemClick, AddressOf TileClick
            tg.Items.Add(_i)

        Next

    End Sub

    Private Sub PopulateHardCoded()

        Dim _Items As String() = Split(SQL, "|")
        If _Items IsNot Nothing Then

            For Each _Item As String In _Items

                Dim _Values As String() = Split(_Item, "=")
                If _Values IsNot Nothing Then

                    Dim _i As New TileBarItem

                    If _Values.Count = 1 Then
                        _i.Name = "t_" + _Values(0)
                        _i.Text = _Values(0)
                    Else
                        _i.Name = "t_" + _Values(0)
                        _i.Text = _Values(1)
                    End If

                    AddHandler _i.ItemClick, AddressOf TileClick
                    tg.Items.Add(_i)

                End If
            Next

        End If

    End Sub

    Private Sub PopulateCustom()

        If IsNothing(m_Buttons) Then Exit Sub
        For Each _P As Pair In m_Buttons
            Dim _i As New TileBarItem
            With _i
                .Name = "t_" + _P.Code
                .Text = _P.Text
                If .Text = "" Then .Text = _P.Code
                AddHandler _i.ItemClick, AddressOf TileClick
            End With
            tg.Items.Add(_i)
        Next

    End Sub

    Private Sub TileClick(sender As Object, e As TileItemEventArgs)

        If PopulateMode = EnumPopulateMode.FileSystem Then

            If e.Item.Name = "btnHome" Then
                PopulateFilesandFolders()
            Else
                If e.Item.Name.StartsWith("dir_") Then
                    m_CurrentFolder = e.Item.Name.Substring(4)
                    PopulateFolder()
                Else
                    If e.Item.Name.StartsWith("file_") Then
                        m_ReturnPair = New Pair(e.Item.Name.Substring(5), e.Item.Text)
                        Me.DialogResult = Windows.Forms.DialogResult.OK
                        Me.Close()
                    End If
                End If
            End If

        Else

            If MultiSelect Then
                If e.Item.Checked Then
                    e.Item.Checked = False
                Else
                    e.Item.Checked = True
                End If
            Else
                m_ReturnPair = New Pair(e.Item.Name.Substring(2), e.Item.Text)
                Me.DialogResult = Windows.Forms.DialogResult.OK
                Me.Close()
            End If

        End If

    End Sub

    Private Sub Accept()

        Dim _SelectedCount As Integer = 0
        m_ReturnPairs = New List(Of Pair)

        For Each _i As TileBarItem In tg.Items
            If _i.Checked Then
                Dim _ID As String = _i.Name.Substring(2)
                Dim _Name As String = _i.Elements(0).Text
                Dim _P As New Pair(_ID, _Name)
                m_ReturnPairs.Add(_P)
                _SelectedCount += 1
            End If
        Next

        If _SelectedCount > 0 Then
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        Else
            m_ReturnPairs = Nothing
        End If

    End Sub

    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click
        Accept()
    End Sub
End Class
