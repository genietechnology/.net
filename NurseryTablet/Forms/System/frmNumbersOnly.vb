﻿Public Class frmNumbersOnly

    Private m_ReturnValue As String
    Private m_DefaultValue As String

    Public Property AllowDecimals As Boolean
    Public Property ShowCharacters As Boolean

    Public ReadOnly Property ReturnValue As String
        Get
            Return m_ReturnValue
        End Get
    End Property

    Public Property DefaultValue As String
        Get
            Return m_DefaultValue
        End Get
        Set(value As String)
            m_DefaultValue = value
            txtText.Text = value
        End Set
    End Property

    Private Sub frmNumbersOnly_Load(sender As Object, e As EventArgs) Handles Me.Load
        btnDOT.Enabled = AllowDecimals
        If Not ShowCharacters Then txtText.PasswordChar = "*"
    End Sub

    Private Sub btnClear_Click(sender As System.Object, e As System.EventArgs) Handles btnClear.Click
        txtText.Text = ""
    End Sub

    Private Sub btn0_Click(sender As System.Object, e As System.EventArgs) Handles btn0.Click
        txtText.Text += "0"
    End Sub

    Private Sub btn1_Click(sender As System.Object, e As System.EventArgs) Handles btn1.Click
        txtText.Text += "1"
    End Sub

    Private Sub btn2_Click(sender As System.Object, e As System.EventArgs) Handles btn2.Click
        txtText.Text += "2"
    End Sub

    Private Sub btn3_Click(sender As System.Object, e As System.EventArgs) Handles btn3.Click
        txtText.Text += "3"
    End Sub

    Private Sub btn4_Click(sender As System.Object, e As System.EventArgs) Handles btn4.Click
        txtText.Text += "4"
    End Sub

    Private Sub btn5_Click(sender As System.Object, e As System.EventArgs) Handles btn5.Click
        txtText.Text += "5"
    End Sub

    Private Sub btn6_Click(sender As System.Object, e As System.EventArgs) Handles btn6.Click
        txtText.Text += "6"
    End Sub

    Private Sub btn7_Click(sender As System.Object, e As System.EventArgs) Handles btn7.Click
        txtText.Text += "7"
    End Sub

    Private Sub btn8_Click(sender As System.Object, e As System.EventArgs) Handles btn8.Click
        txtText.Text += "8"
    End Sub

    Private Sub btn9_Click(sender As System.Object, e As System.EventArgs) Handles btn9.Click
        txtText.Text += "9"
    End Sub

    Private Sub btnAccept_Click(sender As System.Object, e As System.EventArgs) Handles btnAccept.Click
        m_ReturnValue = txtText.Text
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        m_ReturnValue = ""
        Me.Close()
    End Sub

    Private Sub btnDOT_Click(sender As Object, e As EventArgs) Handles btnDOT.Click
        If txtText.Text = "" Then Exit Sub
        If txtText.Text.Contains(".") Then Exit Sub
        txtText.Text += "."
    End Sub

End Class
