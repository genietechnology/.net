﻿Option Strict On

Imports System.Windows.Forms
Imports System.Net.Sockets
Imports System.Threading
Imports NurseryTablet.DataBase
Imports NurseryTablet.NurseryGenieData

Public Class frmSync

    Private m_Operation As EnumSyncOperation
    Private m_LastService As Double = 0
    Private m_LastGoogle As Double = 0
    Private m_AvgGoogle As Double = 0
    Private m_AvgService As Double = 0

    Public m_ErrorMessage As String = ""

    Public Sub New(ByVal Operation As EnumSyncOperation)


        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_Operation = Operation

    End Sub

    Private Sub frmSync_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.Text = "Synchronise Database"
        ClearAllDashBoards()
        dshServer.LabelBottom = Parameters.ServiceIP
    End Sub

    Private Sub frmSync_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        Me.Cursor = Cursors.WaitCursor
        Application.DoEvents()

        SetDashboard(dshGoogle, -1)
        TestSocket(m_LastGoogle, m_AvgGoogle, "8.8.8.8", 53)
        SetDashboard(dshGoogle, m_AvgGoogle)

        If dshGoogle.Colour = DashboardLabel.EnumColourCode.Green Or dshGoogle.Colour = DashboardLabel.EnumColourCode.Amber Then

            Thread.Sleep(1000)

            SetDashboard(dshServer, -1)
            TestSocket(m_LastService, m_AvgService, Parameters.ServiceIP, CInt(Parameters.ServicePort))
            SetDashboard(dshServer, m_AvgService)

            If dshServer.Colour = DashboardLabel.EnumColourCode.Green Or dshServer.Colour = DashboardLabel.EnumColourCode.Amber Then
                ExitResult(Sync)
                Exit Sub
            Else
                'service ping is bad
            End If

        Else
            'google ping is bad
        End If

        ExitResult(False)

    End Sub

    Private Function Sync() As Boolean

        Thread.Sleep(1000)

        Dim _Return As Boolean = True
        Dim _Service As NurseryGenieDataClient = ServiceHandler.ReturnService()

        If ServiceHandler.Ping(_Service) < 0 Then
            Msgbox("Unable to connect to Service.", MessageBoxIcon.Exclamation, "Connect to Service")
            Return False
        End If

        m_ErrorMessage = ""

        Select Case m_Operation

            Case EnumSyncOperation.UploadOnly
                _Return = Upload(_Service)
                If Not _Return Then m_ErrorMessage &= vbCrLf & vbCrLf & "Part of your data may have been uploaded to the server. Your local data has not been changed."

            Case EnumSyncOperation.SyncAll
                _Return = False
                If Upload(_Service) Then
                    Thread.Sleep(1000)
                    ClearAllDashBoards()
                    If Download(_Service) Then
                        _Return = PersistDatabaseLocally()
                    Else
                        m_ErrorMessage &= vbCrLf & vbCrLf & "Your data was uploaded to the server but the operation" &
                                        " failed when trying to download data from the server. Your local data has not been changed."
                    End If
                Else
                    m_ErrorMessage &= vbCrLf & vbCrLf & "Part of your data may have been uploaded to the server. Your local data has not been changed."
                End If

            Case EnumSyncOperation.DownloadAll
                If Download(_Service) Then
                    _Return = PersistDatabaseLocally()
                Else
                    m_ErrorMessage &= vbCrLf & vbCrLf & "The operation failed when trying to download data from the server. Your local data has not been changed."
                End If

            Case EnumSyncOperation.DownloadDay
                DashboardWait(d1, "Day", "Download")
                DownloadDay(_Service)
                DashboardResult(d1, True)

                _Return = PersistDatabaseLocally()

        End Select

        If _Service.State = ServiceModel.CommunicationState.Opened OrElse
                _Service.State = ServiceModel.CommunicationState.Created Then
            _Service.Close()
        End If
        _Service = Nothing

        Return _Return

        Thread.Sleep(1000)

    End Function

    Private Function Upload(ByRef ServiceClient As NurseryGenieData.NurseryGenieDataClient) As Boolean

        Me.Text = "Uploading Data..."

        Try

            DashboardWait(d1, "Register", "Upload")
            If ServiceClient.UploadRegister(Parameters.APIKey, Parameters.APIPassword, Parameters.Computer, Tables.Register.ToArray()) = 0 Then

                DashboardResult(d1, True)

                DashboardWait(d2, "Activity", "Upload")
                If ServiceClient.UploadActivity(Parameters.APIKey, Parameters.APIPassword, Parameters.Computer, Tables.Activity.ToArray()) = 0 Then

                    DashboardResult(d2, True)

                    DashboardWait(d3, "FoodReg", "Upload")
                    If ServiceClient.UploadFoodRegister(Parameters.APIKey, Parameters.APIPassword, Parameters.Computer, Tables.FoodRegister.ToArray()) = 0 Then

                        DashboardResult(d3, True)

                        DashboardWait(d4, "Media", "Upload")
                        If ServiceClient.UploadMedia(Parameters.APIKey, Parameters.APIPassword, Parameters.Computer, Tables.Media.ToArray()) = 0 Then

                            DashboardResult(d4, True)

                            DashboardWait(d5, "Signatures", "Upload")
                            If ServiceClient.UploadSignatures(Parameters.APIKey, Parameters.APIPassword, Parameters.Computer, Tables.Signatures.ToArray()) = 0 Then

                                DashboardResult(d5, True)

                                DashboardWait(d6, "Incidents", "Upload")
                                If ServiceClient.UploadIncidents(Parameters.APIKey, Parameters.APIPassword, Parameters.Computer, Tables.Incidents.ToArray()) = 0 Then
                                    DashboardResult(d6, True)
                                    Return True
                                Else
                                    DashboardResult(d6, False)
                                End If

                            Else
                                DashboardResult(d5, False)
                            End If
                        Else
                            DashboardResult(d4, False)
                        End If
                    Else
                        DashboardResult(d3, False)
                    End If
                Else
                    DashboardResult(d2, False)
                End If
            Else
                DashboardResult(d1, False)
            End If

        Catch ex As Exception

            If ServiceClient.State = ServiceModel.CommunicationState.Faulted Then
                m_ErrorMessage = "There was a fault with the service:" & vbCrLf &
                                    vbCrLf & """" & ex.Message & """"
            End If

            If ServiceClient.State = ServiceModel.CommunicationState.Closed OrElse
                ServiceClient.State = ServiceModel.CommunicationState.Closing Then
                m_ErrorMessage = "The connection to the service was not open."
            End If

        End Try

        Return False

    End Function

    Private Function Download(ByRef ServiceClient As NurseryGenieData.NurseryGenieDataClient) As Boolean

        Me.Text = "Downloading Data..."

        Try
            DashboardWait(d1, "Parameters", "Download")
            Tables.Parameters = AtoL(ServiceClient.GetParameters(Parameters.APIKey, Parameters.APIPassword, Parameters.Computer))
            DashboardResult(d1, True)

            DashboardWait(d2, "Sites", "Download")
            Tables.Sites = AtoL(ServiceClient.GetSites(Parameters.APIKey, Parameters.APIPassword, Parameters.Computer))
            DashboardResult(d2, True)

            DashboardWait(d3, "Rooms", "Download")
            Tables.SiteRooms = AtoL(ServiceClient.GetRooms(Parameters.APIKey, Parameters.APIPassword, Parameters.Computer))
            DashboardResult(d3, True)

            DashboardWait(d4, "Lists", "Download")
            Tables.Lists = AtoL(ServiceClient.GetElementLists(Parameters.APIKey, Parameters.APIPassword, Parameters.Computer))
            DashboardResult(d4, True)

            DashboardWait(d5, "Children", "Download")
            Tables.Children = AtoL(ServiceClient.GetChildren(Parameters.APIKey, Parameters.APIPassword, Parameters.Computer))
            DashboardResult(d5, True)

            DashboardWait(d6, "Contacts", "Download")
            Tables.Contacts = AtoL(ServiceClient.GetContacts(Parameters.APIKey, Parameters.APIPassword, Parameters.Computer))
            DashboardResult(d6, True)

            DashboardWait(d7, "Staff", "Download")
            Tables.Staff = AtoL(ServiceClient.GetStaff(Parameters.APIKey, Parameters.APIPassword, Parameters.Computer))
            DashboardResult(d7, True)

            DashboardWait(d8, "Food", "Download")
            Tables.Food = AtoL(ServiceClient.GetFood(Parameters.APIKey, Parameters.APIPassword, Parameters.Computer))
            DashboardResult(d8, True)

            DashboardWait(d9, "Requests", "Download")
            Tables.Requests = AtoL(ServiceClient.GetRequests(Parameters.APIKey, Parameters.APIPassword, Parameters.Computer))
            DashboardResult(d9, True)

            DashboardWait(d10, "Register", "Download")
            Tables.Register = AtoL(ServiceClient.GetRegister(Parameters.APIKey, Parameters.APIPassword, Parameters.Computer, Today))
            DashboardResult(d10, True)

            DashboardWait(d11, "Activity", "Download")
            Tables.Activity = AtoL(ServiceClient.GetActivity(Parameters.APIKey, Parameters.APIPassword, Parameters.Computer, Today))
            DashboardResult(d11, True)

            DashboardWait(d12, "FoodReg", "Download")
            Tables.FoodRegister = AtoL(ServiceClient.GetFoodRegister(Parameters.APIKey, Parameters.APIPassword, Parameters.Computer, Today))
            DashboardResult(d12, True)

            DashboardWait(d13, "Bookings", "Download")
            Tables.Bookings = AtoL(ServiceClient.GetBookings(Parameters.APIKey, Parameters.APIPassword, Parameters.Computer, Today))
            DashboardResult(d13, True)

            DashboardWait(d14, "Day", "Download")
            DownloadDay(ServiceClient)
            DashboardResult(d14, True)

            Return True

        Catch ex As Exception

            If ServiceClient.State = ServiceModel.CommunicationState.Faulted Then
                m_ErrorMessage = "There was a fault with the service:" & vbCrLf &
                                    vbCrLf & """" & ex.Message & """"
            End If

            If ServiceClient.State = ServiceModel.CommunicationState.Closed OrElse
                ServiceClient.State = ServiceModel.CommunicationState.Closing Then
                m_ErrorMessage = "The connection to the service was not open."
            End If

        End Try

        Return False


    End Function

    Private Sub DownloadDay(ByRef ServiceClient As NurseryGenieData.NurseryGenieDataClient)
        If Not IsNothing(ServiceClient) Then
            DayRecord = ServiceClient.GetDay(Parameters.APIKey, Parameters.APIPassword, Parameters.Computer, Today)
        End If
    End Sub

    Private Function AtoL(Of T)(ByVal ArrayIn As T()) As List(Of T)
        If ArrayIn Is Nothing Then
            Return New List(Of T)
        Else
            Return ArrayIn.ToList()
        End If
    End Function

    Private Sub ExitResult(ByVal OK As Boolean)

        Me.Cursor = Cursors.Default
        Application.DoEvents()

        If OK Then
            Me.DialogResult = Windows.Forms.DialogResult.OK
        Else
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
        End If

        Me.Close()

    End Sub

    Private Sub ClearAllDashBoards()
        ClearDashboard(d1)
        ClearDashboard(d2)
        ClearDashboard(d3)
        ClearDashboard(d4)
        ClearDashboard(d5)
        ClearDashboard(d6)
        ClearDashboard(d7)
        ClearDashboard(d8)
        ClearDashboard(d9)
        ClearDashboard(d10)
        ClearDashboard(d11)
        ClearDashboard(d12)
        ClearDashboard(d13)
        ClearDashboard(d14)
        ClearDashboard(d15)
        ClearDashboard(d16)
        Application.DoEvents()
    End Sub

    Private Sub ClearDashboard(ByRef Dashboard As DashboardLabel)
        d1.reset()
        d2.Reset()
        d3.Reset()
        d4.Reset()
        d5.Reset()
        d6.Reset()
        d7.Reset()
        d8.Reset()
        d9.Reset()
        d10.Reset()
        d11.Reset()
        d12.Reset()
        d13.Reset()
        d14.Reset()
        d15.Reset()
        d16.Reset()
    End Sub

    Private Sub DashboardWait(ByRef Dashboard As DashboardLabel, ByVal TopLabel As String, ByVal BottomLabel As String)
        Dashboard.LabelTop = TopLabel
        Dashboard.LabelBottom = BottomLabel
        Dashboard.Colour = DashboardLabel.EnumColourCode.Amber
        Dashboard.LabelMiddle = ".."
        Application.DoEvents()
    End Sub

    Private Sub DashboardResult(ByRef Dashboard As DashboardLabel, ByVal OK As Boolean)
        If OK Then
            Dashboard.Colour = DashboardLabel.EnumColourCode.Green
            Dashboard.LabelMiddle = "OK"
        Else
            Dashboard.Colour = DashboardLabel.EnumColourCode.Red
            Dashboard.LabelMiddle = "Err"
        End If
        Thread.Sleep(250)
        Application.DoEvents()
    End Sub

#Region "Network Tests"

    Private Sub SetDashboard(ByRef Dash As DashboardLabel, ByVal TimeIn As Double)

        If TimeIn < 0 Then
            Dash.LabelMiddle = "0"
            Dash.Colour = DashboardLabel.EnumColourCode.Amber
        Else

            If TimeIn > 0 Then

                If TimeIn < 60 Then
                    Dash.Colour = DashboardLabel.EnumColourCode.Green
                Else
                    If TimeIn > 150 Then
                        Dash.Colour = DashboardLabel.EnumColourCode.Red
                    Else
                        Dash.Colour = DashboardLabel.EnumColourCode.Amber
                    End If
                End If

                Dash.LabelMiddle = Format(TimeIn, "0.0").ToString

            Else
                Dash.LabelMiddle = "Err"
                Dash.Colour = DashboardLabel.EnumColourCode.Red
            End If

        End If

        Application.DoEvents()

    End Sub

    Private Sub TestSocket(ByRef LastTime As Double, ByRef AvgTime As Double, ByVal Address As String, ByVal Port As Integer)

        Dim _Times As New List(Of Double)

        For _i = 1 To 20

            Dim _Sock As New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)

            Dim _StopWatch As New Stopwatch
            _StopWatch.Start()

            _Sock.Blocking = True

            Try
                _Sock.Connect(Address, Port)

                LastTime = _StopWatch.Elapsed.TotalMilliseconds
                _Times.Add(LastTime)

            Catch ex As Exception
                'unknown host etc
                'Stop
            Finally
                _StopWatch.Stop()
                _Sock.Close()
            End Try

        Next

        If _Times.Count > 0 Then
            AvgTime = _Times.Average
        End If

    End Sub

#End Region

End Class