﻿Imports NurseryTablet.SharedModule
Public Class frmButtons

    Dim m_Buttons As List(Of Pair)
    Dim m_Caption As String
    Dim m_ReturnValue As String
    Dim m_ReturnText As String

    Public WriteOnly Property ButtonCollection As List(Of Pair)
        Set(value As List(Of Pair))
            m_Buttons = value
        End Set
    End Property

    Public ReadOnly Property ReturnValue() As String
        Get
            Return m_ReturnValue
        End Get
    End Property

    Public ReadOnly Property ReturnText() As String
        Get
            Return m_ReturnText
        End Get
    End Property

    Dim intPage As Int16
    Dim intPageTotal As Int16
    Dim intRecords As Int16

    Private Sub frmButtons_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call subGetRecords()
    End Sub

    Private Sub subGetRecords()

        If m_Buttons IsNot Nothing Then

            intRecords = m_Buttons.Count
            intPage = 1

            intPageTotal = intRecords \ 18
            intPageTotal = intPageTotal + 1

            Call subDisplayPage()

        End If

    End Sub

    Private Sub subDisplayPage()

        Dim i As Int16 = 1
        Dim x As Int16 = 0
        Dim intRecord As Int16

        If intPage = 1 Then
            intRecord = 1
            btnPrev.Enabled = False
        Else
            '18 buttons per page
            intRecord = 18 * (intPage - 1) + 1
            btnPrev.Enabled = True
        End If

        If intPage = intPageTotal Then
            btnNext.Enabled = False
        Else
            btnNext.Enabled = True
        End If

        'datarow will begin at zero
        intRecord = intRecord - 1

        Dim _Pair As SharedModule.Pair
        Dim _name As String = ""

        For i = 1 To 18

            _name = "btn" + i.ToString
            Dim _btn As Button = Me.Controls(_name)

            AddHandler _btn.Click, AddressOf ButtonClick

            If intRecord < intRecords Then

                _Pair = m_Buttons(intRecord)

                _btn.Enabled = True
                _btn.Tag = _Pair.Code

                If _Pair.Text = "" Then
                    _btn.Text = _Pair.Code
                Else
                    _btn.Text = _Pair.Text
                End If

            Else

                _btn.Enabled = False
                _btn.Tag = ""
                _btn.Text = ""

            End If

            intRecord = intRecord + 1

        Next

    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        If intPage < intPageTotal Then
            intPage = intPage + 1
            Call subDisplayPage()
        End If
    End Sub

    Private Sub btnPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrev.Click
        If intPage > 1 Then
            intPage = intPage - 1
            Call subDisplayPage()
        End If
    End Sub

    Private Sub ButtonClick(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim _btn As Button = CType(sender, Button)

        _btn.Enabled = False
        Application.DoEvents()
        m_ReturnValue = _btn.Tag
        m_ReturnText = _btn.Text
        Me.Close()

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub
End Class
