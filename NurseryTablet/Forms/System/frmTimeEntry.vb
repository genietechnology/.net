﻿Public Class frmTimeEntry

    Private m_ReturnValue As String
    Private m_DefaultValue As String

    Private m_Hours As String
    Private m_Minutes As String

    Public ReadOnly Property ReturnValue As String
        Get
            Return m_ReturnValue
        End Get
    End Property

    Public Property DefaultValue As String
        Get
            Return m_DefaultValue
        End Get
        Set(value As String)
            m_DefaultValue = value
            txtText.Text = value
        End Set
    End Property

    Public Property NA As Boolean
        Get
            Return btnNA.Enabled
        End Get
        Set(value As Boolean)
            btnNA.Enabled = value
        End Set
    End Property

    Private Sub btnClear_Click(sender As System.Object, e As System.EventArgs) Handles btnClear.Click
        txtText.Text = ""
        ClearButtons()
    End Sub

#Region "Button Clicks"

    Private Sub btnH0_Click(sender As Object, e As EventArgs) Handles btnH0.Click
        ClearHours()
        btnH0.Appearance.BackColor = Color.Yellow
        m_Hours = "00"
        BuildTime()
    End Sub

    Private Sub btnH1_Click(sender As Object, e As EventArgs) Handles btnH1.Click
        ClearHours()
        btnH1.Appearance.BackColor = Color.Yellow
        m_Hours = "01"
        BuildTime()
    End Sub

    Private Sub btnH2_Click(sender As Object, e As EventArgs) Handles btnH2.Click
        ClearHours()
        btnH2.Appearance.BackColor = Color.Yellow
        m_Hours = "02"
        BuildTime()
    End Sub

    Private Sub btnH3_Click(sender As Object, e As EventArgs) Handles btnH3.Click
        ClearHours()
        btnH3.Appearance.BackColor = Color.Yellow
        m_Hours = "03"
        BuildTime()
    End Sub

    Private Sub btnH4_Click(sender As Object, e As EventArgs) Handles btnH4.Click
        ClearHours()
        btnH4.Appearance.BackColor = Color.Yellow
        m_Hours = "04"
        BuildTime()
    End Sub

    Private Sub btnH5_Click(sender As Object, e As EventArgs) Handles btnH5.Click
        ClearHours()
        btnH5.Appearance.BackColor = Color.Yellow
        m_Hours = "05"
        BuildTime()
    End Sub

    Private Sub btnH6_Click(sender As Object, e As EventArgs) Handles btnH6.Click
        ClearHours()
        btnH6.Appearance.BackColor = Color.Yellow
        m_Hours = "06"
        BuildTime()
    End Sub

    Private Sub btnH7_Click(sender As Object, e As EventArgs) Handles btnH7.Click
        ClearHours()
        btnH7.Appearance.BackColor = Color.Yellow
        m_Hours = "07"
        BuildTime()
    End Sub

    Private Sub btnH8_Click(sender As Object, e As EventArgs) Handles btnH8.Click
        ClearHours()
        btnH8.Appearance.BackColor = Color.Yellow
        m_Hours = "08"
        BuildTime()
    End Sub

    Private Sub btnH9_Click(sender As System.Object, e As System.EventArgs) Handles btnH9.Click
        ClearHours()
        btnH9.Appearance.BackColor = Color.Yellow
        m_Hours = "09"
        BuildTime()
    End Sub

    Private Sub btnH10_Click(sender As System.Object, e As System.EventArgs) Handles btnH10.Click
        ClearHours()
        btnH10.Appearance.BackColor = Color.Yellow
        m_Hours = "10"
        BuildTime()
    End Sub

    Private Sub btnH11_Click(sender As System.Object, e As System.EventArgs) Handles btnH11.Click
        ClearHours()
        btnH11.Appearance.BackColor = Color.Yellow
        m_Hours = "11"
        BuildTime()
    End Sub

    Private Sub btnH12_Click(sender As System.Object, e As System.EventArgs) Handles btnH12.Click
        ClearHours()
        btnH12.Appearance.BackColor = Color.Yellow
        m_Hours = "12"
        BuildTime()
    End Sub

    Private Sub btnH13_Click(sender As System.Object, e As System.EventArgs) Handles btnH13.Click
        ClearHours()
        btnH13.Appearance.BackColor = Color.Yellow
        m_Hours = "13"
        BuildTime()
    End Sub

    Private Sub btnH14_Click(sender As System.Object, e As System.EventArgs) Handles btnH14.Click
        ClearHours()
        btnH14.Appearance.BackColor = Color.Yellow
        m_Hours = "14"
        BuildTime()
    End Sub

    Private Sub btnH15_Click(sender As System.Object, e As System.EventArgs) Handles btnH15.Click
        ClearHours()
        btnH15.Appearance.BackColor = Color.Yellow
        m_Hours = "15"
        BuildTime()
    End Sub

    Private Sub btnH16_Click(sender As System.Object, e As System.EventArgs) Handles btnH16.Click
        ClearHours()
        btnH16.Appearance.BackColor = Color.Yellow
        m_Hours = "16"
        BuildTime()
    End Sub

    Private Sub btnH17_Click(sender As Object, e As EventArgs) Handles btnH17.Click
        ClearHours()
        btnH17.Appearance.BackColor = Color.Yellow
        m_Hours = "17"
        BuildTime()
    End Sub

    Private Sub btnH18_Click(sender As Object, e As EventArgs) Handles btnH18.Click
        ClearHours()
        btnH18.Appearance.BackColor = Color.Yellow
        m_Hours = "18"
        BuildTime()
    End Sub

    Private Sub btnH19_Click(sender As Object, e As EventArgs) Handles btnH19.Click
        ClearHours()
        btnH19.Appearance.BackColor = Color.Yellow
        m_Hours = "19"
        BuildTime()
    End Sub

    Private Sub btnH20_Click(sender As Object, e As EventArgs) Handles btnH20.Click
        ClearHours()
        btnH20.Appearance.BackColor = Color.Yellow
        m_Hours = "20"
        BuildTime()
    End Sub

    Private Sub btnH21_Click(sender As Object, e As EventArgs) Handles btnH21.Click
        ClearHours()
        btnH21.Appearance.BackColor = Color.Yellow
        m_Hours = "21"
        BuildTime()
    End Sub

    Private Sub btnH22_Click(sender As Object, e As EventArgs) Handles btnH22.Click
        ClearHours()
        btnH22.Appearance.BackColor = Color.Yellow
        m_Hours = "22"
        BuildTime()
    End Sub

    Private Sub btnH23_Click(sender As Object, e As EventArgs) Handles btnH23.Click
        ClearHours()
        btnH23.Appearance.BackColor = Color.Yellow
        m_Hours = "23"
        BuildTime()
    End Sub

    Private Sub btnM0_Click(sender As System.Object, e As System.EventArgs) Handles btnM0.Click
        ClearMinutes()
        btnM0.Appearance.BackColor = Color.Yellow
        m_Minutes = "00"
        BuildTime()
    End Sub

    Private Sub btnM5_Click(sender As System.Object, e As System.EventArgs) Handles btnM5.Click
        ClearMinutes()
        btnM5.Appearance.BackColor = Color.Yellow
        m_Minutes = "05"
        BuildTime()
    End Sub

    Private Sub btnM10_Click(sender As System.Object, e As System.EventArgs) Handles btnM10.Click
        ClearMinutes()
        btnM10.Appearance.BackColor = Color.Yellow
        m_Minutes = "10"
        BuildTime()
    End Sub

    Private Sub btnM15_Click(sender As System.Object, e As System.EventArgs) Handles btnM15.Click
        ClearMinutes()
        btnM15.Appearance.BackColor = Color.Yellow
        m_Minutes = "15"
        BuildTime()
    End Sub

    Private Sub btnM20_Click(sender As System.Object, e As System.EventArgs) Handles btnM20.Click
        ClearMinutes()
        btnM20.Appearance.BackColor = Color.Yellow
        m_Minutes = "20"
        BuildTime()
    End Sub

    Private Sub btnM25_Click(sender As System.Object, e As System.EventArgs) Handles btnM25.Click
        ClearMinutes()
        btnM25.Appearance.BackColor = Color.Yellow
        m_Minutes = "25"
        BuildTime()
    End Sub

    Private Sub btnM30_Click(sender As System.Object, e As System.EventArgs) Handles btnM30.Click
        ClearMinutes()
        btnM30.Appearance.BackColor = Color.Yellow
        m_Minutes = "30"
        BuildTime()
    End Sub

    Private Sub btnM35_Click(sender As System.Object, e As System.EventArgs) Handles btnM35.Click
        ClearMinutes()
        btnM35.Appearance.BackColor = Color.Yellow
        m_Minutes = "35"
        BuildTime()
    End Sub

    Private Sub btnM40_Click(sender As System.Object, e As System.EventArgs) Handles btnM40.Click
        ClearMinutes()
        btnM40.Appearance.BackColor = Color.Yellow
        m_Minutes = "40"
        BuildTime()
    End Sub

    Private Sub btnM45_Click(sender As System.Object, e As System.EventArgs) Handles btnM45.Click
        ClearMinutes()
        btnM45.Appearance.BackColor = Color.Yellow
        m_Minutes = "45"
        BuildTime()
    End Sub

    Private Sub btnM50_Click(sender As System.Object, e As System.EventArgs) Handles btnM50.Click
        ClearMinutes()
        btnM50.Appearance.BackColor = Color.Yellow
        m_Minutes = "50"
        BuildTime()
    End Sub

    Private Sub btnM55_Click(sender As System.Object, e As System.EventArgs) Handles btnM55.Click
        ClearMinutes()
        btnM55.Appearance.BackColor = Color.Yellow
        m_Minutes = "55"
        BuildTime()
    End Sub

#End Region

    Private Sub SetTime(Time As String)

        Dim _Hours As String = Mid(Time, 1, 2)
        Dim _Mins As String = Mid(Time, 4, 2)

        SetHours(_Hours)
        SetMinutes(_Mins)

        m_Hours = _Hours
        m_Minutes = _Mins

    End Sub

    Private Sub BuildTime()

        'default to o'clock
        If m_Minutes = "" Then

            m_Minutes = "00"

            ClearMinutes()
            btnM0.Appearance.BackColor = Color.Yellow

        End If

        Dim _Time As String = m_Hours + ":" + m_Minutes
        txtText.Text = _Time

    End Sub

    Private Sub SetButtons(ByVal Enabled As Boolean)
        btnH9.Enabled = Enabled
        btnH10.Enabled = Enabled
        btnH11.Enabled = Enabled
        btnH12.Enabled = Enabled
        btnH13.Enabled = Enabled
        btnH14.Enabled = Enabled
        btnH15.Enabled = Enabled
        btnH16.Enabled = Enabled
        btnM0.Enabled = Enabled
        btnM5.Enabled = Enabled
        btnM10.Enabled = Enabled
        btnM15.Enabled = Enabled
        btnM20.Enabled = Enabled
        btnM25.Enabled = Enabled
        btnM30.Enabled = Enabled
        btnM35.Enabled = Enabled
        btnM40.Enabled = Enabled
        btnM45.Enabled = Enabled
        btnM50.Enabled = Enabled
        btnM55.Enabled = Enabled
    End Sub

    Private Sub ClearHours()
        btnH0.Appearance.Reset()
        btnH1.Appearance.Reset()
        btnH2.Appearance.Reset()
        btnH3.Appearance.Reset()
        btnH4.Appearance.Reset()
        btnH5.Appearance.Reset()
        btnH6.Appearance.Reset()
        btnH7.Appearance.Reset()
        btnH8.Appearance.Reset()
        btnH9.Appearance.Reset()
        btnH10.Appearance.Reset()
        btnH11.Appearance.Reset()
        btnH12.Appearance.Reset()
        btnH13.Appearance.Reset()
        btnH14.Appearance.Reset()
        btnH15.Appearance.Reset()
        btnH16.Appearance.Reset()
        btnH17.Appearance.Reset()
        btnH18.Appearance.Reset()
        btnH19.Appearance.Reset()
        btnH20.Appearance.Reset()
        btnH21.Appearance.Reset()
        btnH22.Appearance.Reset()
        btnH23.Appearance.Reset()
    End Sub

    Private Sub ClearMinutes()
        btnM0.Appearance.Reset()
        btnM5.Appearance.Reset()
        btnM10.Appearance.Reset()
        btnM15.Appearance.Reset()
        btnM20.Appearance.Reset()
        btnM25.Appearance.Reset()
        btnM30.Appearance.Reset()
        btnM35.Appearance.Reset()
        btnM40.Appearance.Reset()
        btnM45.Appearance.Reset()
        btnM50.Appearance.Reset()
        btnM55.Appearance.Reset()
    End Sub

    Private Sub ClearButtons()
        ClearHours()
        ClearMinutes()
    End Sub

    Private Sub SetHours(Hours As String)

        If Hours = "09" Then
            btnH9.Appearance.BackColor = Color.Yellow
        End If

        If Hours = "10" Then
            btnH10.Appearance.BackColor = Color.Yellow
        End If

        If Hours = "11" Then
            btnH11.Appearance.BackColor = Color.Yellow
        End If

        If Hours = "12" Then
            btnH12.Appearance.BackColor = Color.Yellow
        End If

        If Hours = "13" Then
            btnH13.Appearance.BackColor = Color.Yellow
        End If

        If Hours = "14" Then
            btnH14.Appearance.BackColor = Color.Yellow
        End If

        If Hours = "15" Then
            btnH15.Appearance.BackColor = Color.Yellow
        End If

        If Hours = "16" Then
            btnH16.Appearance.BackColor = Color.Yellow
        End If

    End Sub

    Private Sub SetMinutes(Minutes As String)

        If Minutes = "00" Then
            btnM0.Appearance.BackColor = Color.Yellow
        End If

        If Minutes = "05" Then
            btnM5.Appearance.BackColor = Color.Yellow
        End If

        If Minutes = "10" Then
            btnM10.Appearance.BackColor = Color.Yellow
        End If

        If Minutes = "15" Then
            btnM15.Appearance.BackColor = Color.Yellow
        End If

        If Minutes = "20" Then
            btnM20.Appearance.BackColor = Color.Yellow
        End If

        If Minutes = "25" Then
            btnM25.Appearance.BackColor = Color.Yellow
        End If

        If Minutes = "30" Then
            btnM30.Appearance.BackColor = Color.Yellow
        End If

        If Minutes = "35" Then
            btnM35.Appearance.BackColor = Color.Yellow
        End If

        If Minutes = "40" Then
            btnM40.Appearance.BackColor = Color.Yellow
        End If

        If Minutes = "45" Then
            btnM45.Appearance.BackColor = Color.Yellow
        End If

        If Minutes = "50" Then
            btnM50.Appearance.BackColor = Color.Yellow
        End If

        If Minutes = "55" Then
            btnM55.Appearance.BackColor = Color.Yellow
        End If

    End Sub

    Private Sub btnAccept_Click(sender As System.Object, e As System.EventArgs) Handles btnAccept.Click
        m_ReturnValue = txtText.Text
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        m_ReturnValue = ""
        Me.Close()
    End Sub

    Private Sub btnAsRequired_Click(sender As Object, e As EventArgs) Handles btnNA.Click
        m_ReturnValue = "Not Applicable"
        Me.Close()
    End Sub

    Private Sub btnNow_Click(sender As Object, e As EventArgs) Handles btnNow.Click
        txtText.Text = Format(Now, "HH:mm")
    End Sub
End Class
