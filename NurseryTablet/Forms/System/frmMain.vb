﻿Imports NurseryTablet.SharedModule
Imports System.Diagnostics

Public Class frmMain

    Private m_SyncOnStartUp As Boolean = False

    Public Sub New(ByVal SyncRequired As Boolean)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_SyncOnStartUp = SyncRequired

    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.CenterToScreen()
    End Sub

    Private Sub frmMain_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        Dim _OK As Boolean = False

        Me.Cursor = Cursors.AppStarting
        Application.DoEvents()

        If m_SyncOnStartUp Then
            _OK = SyncData()
        Else
            _OK = True
        End If

        If _OK Then

            Me.Text = "Nursery Genie Version " + Application.ProductVersion
            Me.Text += " [" + My.Computer.Name + "]"
            Me.Text += WebServiceStatus()

            btnExit.Text = "Exit Application"

        End If

        SetCMDs()

        Me.Cursor = Cursors.Default
        Application.DoEvents()

    End Sub

    Private Sub SetCMDs()

        Dim _StaffCheckedIn As Boolean = StaffCheckedIn()
        Dim _ChildrenCheckedIn As Boolean = ChildrenCheckedIn()

        btnCheckInChild.Enabled = _StaffCheckedIn
        btnCheckInStaff.Enabled = True
        btnSleep.Enabled = _ChildrenCheckedIn
        btnToilet.Enabled = _ChildrenCheckedIn
        btnToiletTrip.Enabled = _ChildrenCheckedIn
        btnSync.Enabled = True

        btnCheckOutChild.Enabled = _ChildrenCheckedIn
        btnCheckOutStaff.Enabled = _StaffCheckedIn
        btnMilk.Enabled = _ChildrenCheckedIn
        btnFood.Enabled = _ChildrenCheckedIn

        btnRegister.Enabled = _StaffCheckedIn
        btnRequests.Enabled = _ChildrenCheckedIn

        btnRegister.Enabled = True
        btnLookupChild.Enabled = True
        btnMedicine.Enabled = _ChildrenCheckedIn
        btnOptions.Enabled = True
        btnExit.Enabled = True
        
    End Sub

    Private Function WebServiceStatus() As String
        Return " Using Service on " + Parameters.ServiceIP
    End Function

    Private Function SyncData() As Boolean

    End Function

    Private Function StaffCheckedIn() As Boolean
        Dim _Count As Integer = Business.Register.ReturnCheckedInCount(Enums.PersonType.Staff)
        If _Count > 0 Then Return True
        Return False
    End Function

    Private Function ChildrenCheckedIn() As Boolean
        Dim _Count As Integer = Business.Register.ReturnCheckedInCount(Enums.PersonType.Child)
        If _Count > 0 Then Return True
        Return False
    End Function

#Region "Buttons"

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        DataBase.PersistDatabaseLocally()
        End
    End Sub

    Private Sub btnLookupChild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLookupChild.Click

        btnLookupChild.Enabled = False

        'show all current children (even if they are checked in or are not due in today)
        Dim _Return As Pair = Business.Child.FindChildByGroup(Enums.PersonMode.Everyone, True)
        If Not _Return Is Nothing AndAlso _Return.Code <> "" Then
            Business.Child.DrillDown(_Return.Code, _Return.Text)
            SetCMDs()
        End If

        btnLookupChild.Enabled = True

    End Sub

    Private Sub btnCheckInChild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheckInChild.Click

        If Not CheckinsPermitted() Then Exit Sub

        btnCheckInChild.Enabled = False

        Dim _Return As List(Of Pair) = Business.Child.FindMultipleChildrenByGroup(Enums.PersonMode.OnlyCheckedOut)
        If _Return IsNot Nothing Then

            Dim _Location As String = GetLocation(Enums.PersonType.Child, Enums.InOut.CheckIn)

            For Each _P In _Return
                Business.Register.RegisterTransaction(_P.Code, Enums.PersonType.Child, _P.Text, Enums.InOut.CheckIn, Enums.RegisterVia.Menu, _Location, "", "", "")
            Next

            SetCMDs()

        End If

        btnCheckInChild.Enabled = True

    End Sub

    Private Sub btnCheckOutChild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheckOutChild.Click

        If Not CheckinsPermitted() Then Exit Sub

        btnCheckOutChild.Enabled = False

        Dim _Return As Pair = Business.Child.FindChildByGroup(Enums.PersonMode.OnlyCheckedIn, True)
        If Not _Return Is Nothing AndAlso _Return.Code <> "" Then
            SharedModule.DoCheckOut(_Return.Code, _Return.Text)
            SetCMDs()
        End If

        btnCheckOutChild.Enabled = True

    End Sub

    Private Sub btnFood_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFood.Click
        btnFood.Enabled = False
        ChildFood()
        btnFood.Enabled = True
    End Sub

    Private Sub btnSleep_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSleep.Click

        btnSleep.Enabled = False

        Dim _frm As New frmSleepManager
        _frm.ShowDialog()

        btnSleep.Enabled = True

    End Sub

    Private Sub btnToilet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnToilet.Click
        btnToilet.Enabled = False
        ChildToilet()
        btnToilet.Enabled = True
    End Sub

    Private Sub btnCheckInStaff_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheckInStaff.Click

        If Not CheckinsPermitted() Then Exit Sub

        btnCheckInStaff.Enabled = False

        Dim _Return As List(Of Pair) = Business.Staff.FindMultipleStaff(Enums.PersonMode.OnlyCheckedOut)
        If _Return IsNot Nothing Then

            Dim _Location As String = GetLocation(Enums.PersonType.Staff, Enums.InOut.CheckIn)

            For Each _P In _Return
                Business.Register.RegisterTransaction(_P.Code, Enums.PersonType.Staff, _P.Text, Enums.InOut.CheckIn, Enums.RegisterVia.Menu, _Location, "", "", "")
            Next

            SetCMDs()

        End If

        btnCheckInStaff.Enabled = True

    End Sub

    Private Function GetLocation(ByVal PersonType As Enums.PersonType, ByVal InOut As Enums.InOut) As String

        Dim _Return As String = ""

        Dim _Type As String = "these children?"
        If PersonType = Enums.PersonType.Staff Then _Type = "these staff members?"

        Dim _InOut As String = "Check In"
        If InOut = Enums.InOut.CheckOut Then _InOut = "Check Out"

        If Parameters.DisableLocation Then
            'check-in without the prompt
        Else
            If Parameters.DefaultRoom = "" Then
                If Msgbox("Do you wish to specify the location for " + _Type, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, _InOut) = MsgBoxResult.Yes Then
                    Dim _p As Pair = ReturnRoom(Parameters.SiteID, PersonType)
                    If _p IsNot Nothing Then
                        _Return = _p.Text
                    End If
                End If
            Else
                'use the default room
                _Return = Parameters.DefaultRoom
            End If
        End If

        Return _Return

    End Function

    Private Sub btnSync_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSync.Click

        btnSync.Enabled = False

        Dim _OK As Boolean = DataBase.Sync(DataBase.EnumSyncOperation.SyncAll, True)

        If _OK Then
            Msgbox("Sync Completed.", MessageBoxIcon.Information, "Sync")
        Else
            Msgbox("Sync could not be completed. Please try again or contact your system administrator.", MessageBoxIcon.Information, "Sync")
        End If

        btnSync.Enabled = True

    End Sub

    Private Sub btnMilk_Click(sender As System.Object, e As System.EventArgs) Handles btnMilk.Click
        btnMilk.Enabled = False
        ChildMilk()
        btnMilk.Enabled = True
    End Sub

    Private Sub btnCheckOutStaff_Click(sender As System.Object, e As System.EventArgs) Handles btnCheckOutStaff.Click

        If Not CheckinsPermitted() Then Exit Sub

        btnCheckOutStaff.Enabled = False

        Dim _Return As List(Of Pair) = Business.Staff.FindMultipleStaff(Enums.PersonMode.OnlyCheckedOut)
        If _Return IsNot Nothing Then

            For Each _P In _Return
                Business.Register.RegisterTransaction(_P.Code, Enums.PersonType.Staff, _P.Text, Enums.InOut.CheckOut, Enums.RegisterVia.Menu, "", "", "", "")
            Next

            SetCMDs()

        End If

        btnCheckOutStaff.Enabled = True

    End Sub

    Private Sub btnRequests_Click(sender As System.Object, e As System.EventArgs) Handles btnRequests.Click
        btnRequests.Enabled = False
        ChildRequests()
        btnRequests.Enabled = True
    End Sub

    Private Sub btnMedicine_Click(sender As System.Object, e As System.EventArgs) Handles btnMedicine.Click

        Dim _frm As New frmMedicalMain
        _frm.Show()
        _frm.BringToFront()

    End Sub

    Private Sub btnRegister_Click(sender As Object, e As EventArgs) Handles btnRegister.Click
        Dim _frm As New frmRegister
        _frm.Show()
        _frm.BringToFront()
    End Sub

#End Region

    Private Sub btnToiletTrip_Click(sender As Object, e As EventArgs) Handles btnToiletTrip.Click
        Dim _frm As New frmFastToilet
        _frm.ShowDialog()
    End Sub

    Private Sub btnOptions_Click(sender As Object, e As EventArgs) Handles btnOptions.Click
        SharedModule.ChildBehavior()
    End Sub

    Private Sub btnHelp_Click(sender As Object, e As EventArgs) Handles btnHelp.Click
        Dim _frm As New frmHelp
        _frm.ShowDialog()
        _frm.Dispose()
        _frm = Nothing
    End Sub
End Class