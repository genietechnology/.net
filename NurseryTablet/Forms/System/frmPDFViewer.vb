﻿Public Class frmPDFViewer

    Private m_PDFFile As String = ""

    Private Sub btnCancel_Click(sender As Object, e As EventArgs)
        Me.Close()
    End Sub

    Public Sub New(ByVal PDFFile As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_PDFFile = PDFFile

    End Sub

    Private Sub frmPDFViewer_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.Text = m_PDFFile
        PdfViewer1.LoadDocument(m_PDFFile)
    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click
        Me.Close()
    End Sub

    Private Sub frmPDFViewer_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Cursor.Current = Cursors.Default
        Application.DoEvents()
    End Sub
End Class