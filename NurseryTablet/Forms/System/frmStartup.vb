﻿Imports DevExpress.XtraSpellChecker
Imports System.IO
Imports NurseryTablet.SharedModule

Public Class frmStartup

    Dim _Loading As Boolean

    Private Sub frmWWStartup_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ErrorHandler.Activate()

        txtStatus.Text = ""
        _Loading = True
        txtStatus.Text = ""

        Cursor.Current = Cursors.WaitCursor
        Application.DoEvents()

        Parameters.ServiceIP = My.Settings.ServiceIP
        Parameters.ServicePort = My.Settings.ServicePort

        DevExpress.Utils.AppearanceObject.DefaultFont = New Font("Segoe UI", 14)
        DevExpress.XtraEditors.WindowsFormsSettings.TouchUIMode = DevExpress.LookAndFeel.TouchUIMode.True
        DevExpress.XtraEditors.WindowsFormsSettings.ScrollUIMode = DevExpress.XtraEditors.ScrollUIMode.Touch

        timShutdown.Interval = 60000 * 15
        timShutdown.Start()

    End Sub

    Private Sub frmStartup_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        Dim _SyncRequired As Boolean = False

        If _Loading Then

            _Loading = False

            UpdateText("Starting System Build " & SharedModule.Version)
            UpdateText("Service IP " & Parameters.ServiceIP)
            UpdateText("Service Port " & Parameters.ServicePort)
            UpdateText("Loading Application Settings")
            LoadSettings()

            UpdateText("Checking for Local Data")

            If Not DataBase.CheckForLocalData() Then
                UpdateText("Downloading Core Tables")
                If Not DataBase.Sync(DataBase.EnumSyncOperation.DownloadAll, True) Then
                    End
                End If
            End If

            '*****************************************************************************************************************************************************

            UpdateText("Checking for local day record")

            Dim _DownloadDayFromService As Boolean = False
            If DataBase.DayRecord Is Nothing Then
                UpdateText("Local day record does not exist")
                _DownloadDayFromService = True
            Else
                If DataBase.DayRecord.DayDate.Value <> Today Then
                    UpdateText("Local day record does not match todays date")
                    _DownloadDayFromService = True
                Else
                    UpdateText("Local day record is OK")
                End If
            End If

            If _DownloadDayFromService Then

                UpdateText("Downloading day record from service")
                If Not DataBase.Sync(DataBase.EnumSyncOperation.DownloadDay, False) Then
                    Msgbox("There was an error downloading the day record.", MessageBoxIcon.Exclamation, "Error Downloading Day")
                    End
                Else
                    If DataBase.DayRecord Is Nothing Then
                        Msgbox("Today's menus have not been setup." + vbCrLf + _
                               "Please setup the menu using the Today Screen on your workstation.", MessageBoxIcon.Exclamation, "Menu not setup")
                        End
                    End If
                End If

                UpdateText("Local day record downloaded OK")

            End If

            Business.Day.Setup()

        End If

        '*****************************************************************************************************************************************************

        Threading.Thread.Sleep(1500)

        UpdateText("Loading Local Non-Critical Data")
        DataBase.CheckOtherLocalData()

        UpdateText("Setting Parameters")

        If SetParameters() Then
            SetupSpellChecker()
            SetTempFolder()
            UpdateText("Loading Main Form")
            LoadMainForm(_SyncRequired)
        Else
            Msgbox("There was an error loading parameters!", MessageBoxIcon.Exclamation, "SetParameters")
            End
        End If

    End Sub

    Private Function SetParameters() As Boolean

        If SetParameters("MASTER") Then
            Return SetParameters(My.Computer.Name)
        Else
            Return False
        End If

    End Function

    Private Function SetParameters(ByVal DeviceName As String) As Boolean

        Dim _Return As Boolean = False
        Dim _Params As List(Of NurseryGenieData.TabletParam) = Business.Parameters.GetParameters(DeviceName)

        If _Params IsNot Nothing Then

            For Each _P In _Params
                _Return = SetParameter(_P)
                If Not _Return Then Exit For
            Next

        End If

        Return _Return

    End Function

    Private Function SetParameter(ByRef Parameter As NurseryGenieData.TabletParam) As Boolean

        Dim _Name As String = Parameter.ParamId.ToUpper
        Dim _Type As String = Parameter.ParamType.ToUpper
        Dim _StringValue As String = Parameter.ParamValue

        Select Case _Type

            Case "BOOLEAN"

                Dim _BooleanValue As Boolean = ReturnBooleanValue(_StringValue)
                Return SetBooleanParameter(_Name, _BooleanValue)

            Case "STRING"
                Return SetParameter(_Name, _StringValue)

            Case Else
                Return False

        End Select

    End Function

    Private Function SetBooleanParameter(ByVal ParameterName As String, ByVal ParameterValue As Boolean) As Boolean

        If ParameterName = "DISABLECAPTURE" Then Parameters.Capture.CaptureEnabled = Not ParameterValue : Return True

        If ParameterName = "DISABLECAPTURE" Then Parameters.Capture.CaptureEnabled = Not ParameterValue : Return True
        If ParameterName = "DISABLECHECKIN" Then Parameters.DisableCheckIns = ParameterValue : Return True
        If ParameterName = "DISABLEPOTTY" Then Parameters.DisablePotty = ParameterValue : Return True
        If ParameterName = "DISABLELOCATION" Then Parameters.DisableLocation = ParameterValue : Return True

        If ParameterName = "BASICMEALS" Then Parameters.UseBasicMeals = ParameterValue : Return True
        If ParameterName = "CROSSCHECK" Then Parameters.CrossCheckMandatory = ParameterValue : Return True
        If ParameterName = "DEBUG" Then Parameters.DebugMode = ParameterValue : Return True
        If ParameterName = "MAXIMISE" Then Parameters.MaximiseForms = ParameterValue : Return True
        If ParameterName = "SPELLCHECK" Then Parameters.SpellChecking = ParameterValue : Return True

        If ParameterName = "SORTSURNAME" Then Parameters.SortBySurname = ParameterValue : Return True

        'the parameter we have found, we don't need anyway
        Return True

    End Function

    Private Function SetParameter(ByVal ParameterName As String, ByVal ParameterValue As String) As Boolean

        If ParameterName = "ROOM" Then Parameters.DefaultRoom = ParameterValue : Return True

        If ParameterName = "CAMERACOMMAND" Then Parameters.Capture.CameraCommand = ParameterValue : Return True
        If ParameterName = "CAMERADEVICE" Then Parameters.Capture.CameraDevice = ParameterValue : Return True

        If ParameterName = "PATHSHARED" Then Parameters.Paths.SharedFolderPath = ParameterValue : Return True

        If ParameterName = "UNLOCKMODE" Then
            Select Case ParameterValue.ToUpper
                Case "DISABLED"
                    Parameters.UnlockMode = EnumUnlockMode.Disabled
                    Return True
                Case "CODE"
                    Parameters.UnlockMode = EnumUnlockMode.Code
                    Return True
                Case "PIN"
                    Parameters.UnlockMode = EnumUnlockMode.PIN
                    Return True
                Case Else
                    Return False
            End Select
        End If

        If ParameterName = "UNLOCKCODE" Then Parameters.UnlockCode = ParameterValue : Return True

        'the parameter we have found, we don't need anyway
        Return True

    End Function

    Private Function ReturnBooleanValue(ByVal StringIn As String)
        Select Case StringIn.ToUpper
            Case "TRUE", "YES", "Y", "T"
                Return True
            Case Else
                Return False
        End Select
    End Function

    Private Sub LoadMainForm(ByVal SyncRequired As Boolean)

        Me.Hide()

        Dim _frmMain As New frmMain(SyncRequired)
        _frmMain.Show()

    End Sub

    Private Sub SetTempFolder()

        UpdateText("Checking Temp Folder")
        If Not IO.Directory.Exists(SharedModule.TempFolderPath) Then
            UpdateText("Creating Temp Folder")
            IO.Directory.CreateDirectory(SharedModule.TempFolderPath)
        End If

    End Sub

    Private Sub SetupSpellChecker()

        If Parameters.SpellChecking Then

            UpdateText("Loading Spell Check Dictionary")

            Dim _Dictionary As String = IO.Path.GetDirectoryName(Application.ExecutablePath) + "\dict\en_GB.dic"
            Dim _Grammar As String = IO.Path.GetDirectoryName(Application.ExecutablePath) + "\dict\en_GB.aff"

            If _Dictionary <> "" AndAlso _Grammar <> "" Then
                Parameters.SpellChecking = AddDictionary(_Dictionary, _Grammar)
            Else
                Parameters.SpellChecking = False
            End If

            If Parameters.SpellChecking Then
                UpdateText("Spell Checking Enabled")
            Else
                UpdateText("Spell Checking Disabled, Load Failed")
            End If

        Else
            UpdateText("Spell Checking Disabled")
            Parameters.SpellChecking = False
        End If

    End Sub

    Private Function AddDictionary(ByVal DictionaryPath As String, ByVal GrammarPath As String) As Boolean

        If Not File.Exists(DictionaryPath) Then Return False
        If Not File.Exists(GrammarPath) Then Return False

        Dim _Return As Boolean = False

        Try

            Dim _ood As New SpellCheckerOpenOfficeDictionary
            _ood.DictionaryPath = DictionaryPath
            _ood.GrammarPath = GrammarPath
            _ood.Load()

            If _ood.Loaded Then
                SharedDictionaryStorage1.Dictionaries.Add(_ood)
                _Return = True
            End If

            _ood = Nothing

        Catch ex As Exception

        End Try

        Return _Return

    End Function

    Private Sub UpdateText(ByVal TextIn As String)

        If txtStatus.Text = "" Then
            txtStatus.Text = TextIn & "..."
        Else
            txtStatus.Text = txtStatus.Text & vbCrLf & TextIn & "..."
        End If

        txtStatus.SelectionStart = txtStatus.Text.Length
        txtStatus.ScrollToCaret()

        Application.DoEvents()

    End Sub

    Private Sub LoadSettings()

    End Sub

End Class