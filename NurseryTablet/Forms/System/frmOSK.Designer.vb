﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOSK
    Inherits NurseryTablet.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim VirtualKeyboardColorTable1 As DevComponents.DotNetBar.Keyboard.VirtualKeyboardColorTable = New DevComponents.DotNetBar.Keyboard.VirtualKeyboardColorTable()
        Dim FlatStyleRenderer1 As DevComponents.DotNetBar.Keyboard.FlatStyleRenderer = New DevComponents.DotNetBar.Keyboard.FlatStyleRenderer()
        Dim VirtualKeyboardColorTable2 As DevComponents.DotNetBar.Keyboard.VirtualKeyboardColorTable = New DevComponents.DotNetBar.Keyboard.VirtualKeyboardColorTable()
        Dim FlatStyleRenderer2 As DevComponents.DotNetBar.Keyboard.FlatStyleRenderer = New DevComponents.DotNetBar.Keyboard.FlatStyleRenderer()
        Me.tlp = New System.Windows.Forms.TableLayoutPanel()
        Me.kc = New DevComponents.DotNetBar.Keyboard.KeyboardControl()
        Me.kcQuickText = New DevComponents.DotNetBar.Keyboard.KeyboardControl()
        Me.txtText = New DevExpress.XtraEditors.MemoEdit()
        Me.tlp.SuspendLayout()
        CType(Me.txtText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tlp
        '
        Me.tlp.ColumnCount = 2
        Me.tlp.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75.0!))
        Me.tlp.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.tlp.Controls.Add(Me.kc, 0, 1)
        Me.tlp.Controls.Add(Me.kcQuickText, 1, 0)
        Me.tlp.Controls.Add(Me.txtText, 0, 0)
        Me.tlp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp.Location = New System.Drawing.Point(0, 0)
        Me.tlp.Name = "tlp"
        Me.tlp.RowCount = 2
        Me.tlp.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 45.45454!))
        Me.tlp.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 54.54546!))
        Me.tlp.Size = New System.Drawing.Size(1030, 520)
        Me.tlp.TabIndex = 65
        '
        'kc
        '
        VirtualKeyboardColorTable1.BackgroundColor = System.Drawing.Color.Black
        VirtualKeyboardColorTable1.DarkKeysColor = System.Drawing.Color.FromArgb(CType(CType(29, Byte), Integer), CType(CType(28, Byte), Integer), CType(CType(33, Byte), Integer))
        VirtualKeyboardColorTable1.DownKeysColor = System.Drawing.Color.White
        VirtualKeyboardColorTable1.DownTextColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer))
        VirtualKeyboardColorTable1.KeysColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(55, Byte), Integer))
        VirtualKeyboardColorTable1.LightKeysColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(76, Byte), Integer))
        VirtualKeyboardColorTable1.PressedKeysColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(161, Byte), Integer), CType(CType(81, Byte), Integer))
        VirtualKeyboardColorTable1.TextColor = System.Drawing.Color.White
        VirtualKeyboardColorTable1.ToggleTextColor = System.Drawing.Color.Green
        VirtualKeyboardColorTable1.TopBarTextColor = System.Drawing.Color.White
        Me.kc.ColorTable = VirtualKeyboardColorTable1
        Me.kc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.kc.IsTopBarVisible = False
        Me.kc.Location = New System.Drawing.Point(3, 239)
        Me.kc.Name = "kc"
        FlatStyleRenderer1.ColorTable = VirtualKeyboardColorTable1
        FlatStyleRenderer1.ForceAntiAlias = False
        Me.kc.Renderer = FlatStyleRenderer1
        Me.kc.Size = New System.Drawing.Size(766, 278)
        Me.kc.TabIndex = 67
        Me.kc.Text = "KeyboardControl1"
        '
        'kcQuickText
        '
        VirtualKeyboardColorTable2.BackgroundColor = System.Drawing.Color.Black
        VirtualKeyboardColorTable2.DarkKeysColor = System.Drawing.Color.FromArgb(CType(CType(29, Byte), Integer), CType(CType(28, Byte), Integer), CType(CType(33, Byte), Integer))
        VirtualKeyboardColorTable2.DownKeysColor = System.Drawing.Color.White
        VirtualKeyboardColorTable2.DownTextColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer))
        VirtualKeyboardColorTable2.KeysColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(55, Byte), Integer))
        VirtualKeyboardColorTable2.LightKeysColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(76, Byte), Integer))
        VirtualKeyboardColorTable2.PressedKeysColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(161, Byte), Integer), CType(CType(81, Byte), Integer))
        VirtualKeyboardColorTable2.TextColor = System.Drawing.Color.White
        VirtualKeyboardColorTable2.ToggleTextColor = System.Drawing.Color.Green
        VirtualKeyboardColorTable2.TopBarTextColor = System.Drawing.Color.White
        Me.kcQuickText.ColorTable = VirtualKeyboardColorTable2
        Me.kcQuickText.Dock = System.Windows.Forms.DockStyle.Fill
        Me.kcQuickText.IsTopBarVisible = False
        Me.kcQuickText.Location = New System.Drawing.Point(775, 3)
        Me.kcQuickText.Name = "kcQuickText"
        FlatStyleRenderer2.ColorTable = VirtualKeyboardColorTable2
        FlatStyleRenderer2.ForceAntiAlias = False
        Me.kcQuickText.Renderer = FlatStyleRenderer2
        Me.tlp.SetRowSpan(Me.kcQuickText, 2)
        Me.kcQuickText.Size = New System.Drawing.Size(252, 514)
        Me.kcQuickText.TabIndex = 65
        Me.kcQuickText.Text = "KeyboardControl1"
        '
        'txtText
        '
        Me.txtText.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtText.Location = New System.Drawing.Point(3, 3)
        Me.txtText.Name = "txtText"
        Me.txtText.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtText.Properties.Appearance.Options.UseFont = True
        Me.txtText.Size = New System.Drawing.Size(766, 230)
        Me.txtText.TabIndex = 68
        '
        'frmOSK
        '
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1030, 520)
        Me.Controls.Add(Me.tlp)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmOSK"
        Me.tlp.ResumeLayout(False)
        CType(Me.txtText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tlp As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents kcQuickText As DevComponents.DotNetBar.Keyboard.KeyboardControl
    Friend WithEvents kc As DevComponents.DotNetBar.Keyboard.KeyboardControl
    Friend WithEvents txtText As DevExpress.XtraEditors.MemoEdit

End Class
