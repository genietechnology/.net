﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SignatureControl
    Inherits System.Windows.Forms.UserControl

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Initializes a new instance of the InkSecureSignature class.
    ''' </summary>
    Public Sub New()
        MyBase.New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        ' Determine if we are in design time or runtime (based on the owning form).
        SetStyle(ControlStyles.ResizeRedraw, True)

    End Sub

    ''' <summary>
    ''' Releases resources used by the InkSecureSignature object.
    ''' </summary>
    ''' <param name="disposing">Set to true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)

    End Sub

    ' NOTE: The following procedure is required by the Windows Form Designer.
    ' It can be modified using the Windows Form Designer.
    ' Do not modify it using the code editor.
    Friend WithEvents signaturePanel As System.Windows.Forms.Panel

    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.signaturePanel = New System.Windows.Forms.Panel()
        Me.SuspendLayout()
        '
        'signaturePanel
        '
        Me.signaturePanel.BackColor = System.Drawing.Color.WhiteSmoke
        Me.signaturePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.signaturePanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.signaturePanel.Location = New System.Drawing.Point(0, 0)
        Me.signaturePanel.Name = "signaturePanel"
        Me.signaturePanel.Size = New System.Drawing.Size(327, 180)
        Me.signaturePanel.TabIndex = 0
        '
        'SignatureControl
        '
        Me.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Controls.Add(Me.signaturePanel)
        Me.Name = "SignatureControl"
        Me.Size = New System.Drawing.Size(327, 180)
        Me.ResumeLayout(False)

    End Sub

#End Region
End Class
