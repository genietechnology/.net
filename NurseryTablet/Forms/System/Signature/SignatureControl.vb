﻿'-------------------------------------------------------------------------- 
' 
'  Copyright (c) Microsoft Corporation.  All rights reserved. 
' 
'  File: InkSecureSignature.vb
'
'  Description: User control for safely collecting an ink signature.
'
'-------------------------------------------------------------------------- 

Imports Microsoft.Ink
Imports System.Drawing
Imports System.Drawing.Drawing2D

''' <summary>
''' User control for safely collecting an ink signature.
''' </summary>
Public Class SignatureControl

    Inherits System.Windows.Forms.UserControl

    Private WithEvents signatureInkOverlay As InkOverlay
    Private InkSecureSignatureDataObject As New InkSecureSignatureData
    Private inDesignMode As Boolean = True
    Private signatureReadOnly As Boolean
    Private signersNameCaptionValue As String = "Signer's Name: "
    Private m_SignatureBytes As Byte()

    ''' <summary>
    ''' Occurs when the signature has been fully signed.
    ''' </summary>
    Public Event Signed(ByVal sender As Object, ByVal e As EventArgs)

#Region "Properties"

    Public Property Value() As String

        ' Serialize this object and return it as a string.
        Get
            Dim Serializer As New Xml.Serialization.XmlSerializer(GetType(InkSecureSignatureData))
            ' Create an in-memory stream to hold the serialization.
            Dim Stream As New IO.MemoryStream
            ' Create a StringBuilder object to hold the serialized string.
            Dim SB As New System.Text.StringBuilder
            ' Serialize the object.
            Serializer.Serialize(Stream, InkSecureSignatureDataObject)
            ' Reset the position of the stream back to the start.
            Stream.Position = 0
            ' Create a reader to read the stream.
            Dim Reader As New System.IO.StreamReader(Stream)
            ' Read the stream into the StringBuilder object.
            SB.Append(Reader.ReadToEnd)
            ' Clean up.
            Stream.Close()
            ' Return the serialized string.
            Return SB.ToString
        End Get

        ' Deserialize this object and restore its data.
        Set(ByVal Value As String)
            Try
                ' Create an in-memory stream.
                Dim Stream As New IO.MemoryStream
                ' Create a new writer for the in-memory stream.
                Dim Writer As New IO.StreamWriter(Stream)
                ' Write the passed-in value to the stream.
                Writer.Write(Value)
                ' Ensure that all data has been written.
                Writer.Flush()
                ' Go to the start of the stream.
                Stream.Position = 0

                ' Create a serialization object and deserialize the stream.
                Dim Serializer As New Xml.Serialization.XmlSerializer(GetType(InkSecureSignatureData))
                InkSecureSignatureDataObject = Serializer.Deserialize(Stream)

                ' Check to see if we are loading a signed/accepted signature, or an unaccepted signature.
                If Not InkSecureSignatureDataObject.EncryptedBiometricData Is Nothing AndAlso _
                        InkSecureSignatureDataObject.EncryptedBiometricData.Length > 0 Then
                    ' The signature has already been accepted because we have biometric data.
                    Me.SignatureComplete = True

                    ' Display the signature.
                    Call CreateNewInkOverlay()
                    Call SetDefaultDrawingAttributes(Color.Black)
                    signatureInkOverlay.Ink.Load(InkSecureSignatureDataObject.InkWashedSignature)
                    signaturePanel.Refresh()

                Else
                    ' This signature has NOT been accepted, so display the control in edit mode.
                    signatureReadOnly = False

                    ' Create an Ink region to capture a signature.
                    Call CreateNewInkOverlay()
                    Call SetDefaultDrawingAttributes(Color.Blue)

                    ' Do not collect Ink in design mode, but enable it at runtime.
                    signatureInkOverlay.Enabled = Not inDesignMode

                    ' If there is signature Ink, then load it up.
                    ' The control does not store Ink in the InkSecureSignature field
                    ' until the user clicks Accept, so there will probably
                    ' never be Ink in an unaccepted signature.

                    If Not InkSecureSignatureDataObject.InkSecureSignature Is Nothing Then
                        signatureInkOverlay.Ink.Load(InkSecureSignatureDataObject.InkSecureSignature)
                    End If

                    ' Refresh the signaturePanel and set signer fields.
                    signaturePanel.Refresh()

                End If

            Catch ex As Exception
                Throw ex
            End Try

        End Set

    End Property

    Public Property SignersNameCaption() As String

        Get
            Return signersNameCaptionValue
        End Get

        Set(ByVal Value As String)
            signersNameCaptionValue = Value
        End Set

    End Property

    Public Property SignatureComplete() As Boolean

        Get
            Return signatureReadOnly
        End Get

        Set(ByVal Value As Boolean)
            ' If the signature is already accepted, then exit.
            If (signatureReadOnly = True) Or (Value = signatureReadOnly) Then Exit Property

            ' Because we got this far, Value is True, 
            ' so lock all controls and disable Ink collection.
            If Not signatureInkOverlay Is Nothing Then
                signatureInkOverlay.Enabled = False
            End If

            ' Set the read-only property value.
            signatureReadOnly = Value
        End Set

    End Property

    Public ReadOnly Property SignatureBytes As Byte()
        Get
            Return m_SignatureBytes
        End Get
    End Property

#End Region

#Region "Public Methods"

    Public Sub Clear()
        ' Delete the Strokes collection.
        signatureInkOverlay.Ink.DeleteStrokes(signatureInkOverlay.Ink.Strokes)
        ' Invalidating the panel forces a redraw.
        signaturePanel.Invalidate()
        ' Revalidate the data.
        ValidateData()
    End Sub

    Public Sub Print(ByVal graphics As Graphics, ByVal topLeftPoint As Point)

        ' Starting locations.
        Dim Indentation As Integer = 5
        Dim BottomLineY As Integer = 17
        Dim VerticalLocation As Integer = topLeftPoint.Y

        ' Specify a bordered print area slightly smaller than the control.
        Dim ThisRect As New Rectangle(topLeftPoint.X, topLeftPoint.Y, Me.Width, Me.Height - 20)
        Dim BorderColor As Color = Color.FromArgb(255, 0, 45, 150)
        Dim Renderer As New Microsoft.Ink.Renderer

        With graphics
            .FillRectangle(Brushes.White, ThisRect)
            .DrawRectangle(New Pen(BorderColor), ThisRect)

            ' Draw the bottom line.
            .DrawLine(Pens.Black, _
                Indentation, _
                ThisRect.Height - BottomLineY, _
                ThisRect.Width - (2 * Indentation), _
                ThisRect.Height - BottomLineY)

            If SignatureComplete = False Then
                ' Draw a blank signature line.
                .DrawString("Signed: ", _
                    Me.Font, _
                    New SolidBrush(Color.Black), _
                    ThisRect.Left + Indentation, _
                    ThisRect.Height - BottomLineY + 1)
            Else
                ' Draw header text and washed Ink.
                .DrawString("RSA Encrypted Digital Biometric Signature", _
                    Me.Font, _
                    New SolidBrush(Color.Blue), _
                    ThisRect.Left + 3, _
                    VerticalLocation + 3)

                graphics.SmoothingMode = SmoothingMode.AntiAlias
                graphics.CompositingMode = CompositingMode.SourceOver
                graphics.CompositingQuality = CompositingQuality.HighQuality
                graphics.InterpolationMode = InterpolationMode.HighQualityBilinear

                Dim da As New DrawingAttributes(Color.Black)
                da.AntiAliased = True
                da.FitToCurve = True
                da.RasterOperation = RasterOperation.Black
                For Each Stroke As Stroke In signatureInkOverlay.Ink.Strokes
                    Renderer.Draw(graphics, Stroke, da)
                Next

                .DrawString("Signed By: " & _
                    InkSecureSignatureDataObject.SignersName & " on " & _
                    InkSecureSignatureDataObject.SignerAcceptedOn.ToShortDateString & " at " & _
                    InkSecureSignatureDataObject.SignerAcceptedOn.ToShortTimeString, _
                    Me.Font, _
                    New SolidBrush(Color.Blue), _
                    ThisRect.Left + Indentation, _
                    ThisRect.Height - BottomLineY + 1)
            End If
        End With

    End Sub

    Public Sub Reset()
        Me.Value = "<InkSecureSignatureData/>"
    End Sub

    Public Sub AcceptSignature()

        With InkSecureSignatureDataObject
            ' Save the serialized Ink to the SignatureData.InkSecureSignature property
            ' for encryption by the Biometric Encryption Provider for Ink.
            .InkSecureSignature = signatureInkOverlay.Ink.Save(PersistenceFormat.InkSerializedFormat, CompressionMode.Default)
            .SignerAcceptedOn = Now
        End With

        Dim BiometricEncryptionProvider As New BiometricEncryptionProviderForInk

        Try

            ' Wash and encrypt the signature data.
            BiometricEncryptionProvider.Encrypt(InkSecureSignatureDataObject)

            ' Stop collecting Ink and show the washed Ink.
            Me.SignatureComplete = True
            Call CreateNewInkOverlay()
            Call SetDefaultDrawingAttributes(Color.Black)
            signatureInkOverlay.Ink.Load(InkSecureSignatureDataObject.InkWashedSignature)
            signaturePanel.Refresh()

            'store the signature bytes
            m_SignatureBytes = signatureInkOverlay.Ink.Save(PersistenceFormat.Gif, CompressionMode.Default)

            ' Tell the calling form that the control is done processing.
            RaiseEvent Signed(Me, New EventArgs)

        Catch ex As Exception
            MsgBox("Unable to encrypt ink signature.  The exception was " & ex.ToString, MessageBoxIcon.Error, "Unable to Encrypt Biometric")
        End Try

    End Sub

#End Region

#Region "Private Methods"

    Private Sub SetDefaultDrawingAttributes(ByVal color As System.Drawing.Color)

        With signatureInkOverlay.DefaultDrawingAttributes
            .Color = color          ' Color.
            .AntiAliased = True     ' Smooth.
            .FitToCurve = False     ' Set to not round (modify) the Stroke.
            .PenTip = PenTip.Ball   ' Ball Point.
            .Width = 25             ' Size.
        End With

    End Sub

    Private Sub CreateNewInkOverlay()

        If Not signatureInkOverlay Is Nothing Then
            signatureInkOverlay.Enabled = False
            signatureInkOverlay.Ink.DeleteStrokes(signatureInkOverlay.Ink.Strokes)
            signatureInkOverlay.AttachedControl = Nothing
            signatureInkOverlay.Dispose()
            signatureInkOverlay = Nothing
        End If

        signatureInkOverlay = New InkOverlay
        signatureInkOverlay.AttachedControl = signaturePanel
        signatureInkOverlay.DefaultDrawingAttributes.AntiAliased = True
        signatureInkOverlay.DefaultDrawingAttributes.FitToCurve = True

    End Sub

    Private Sub ValidateData()
        'acceptButton.Enabled = (signatureInkOverlay.Ink.Strokes.Count > 0) And (signersNameTextBox.Text.Length > 0)
    End Sub

#End Region

#Region "ControlHandlers"

    Private Sub SignaturePanel_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles signaturePanel.Paint

        ' Draw the lines and the label.
        Dim DashedLinePen As New Pen(New Drawing2D.HatchBrush(HatchStyle.DashedVertical, SystemColors.Control, SystemColors.ControlDark))
        Dim BottomLineY As Integer = signaturePanel.Height - 20
        Dim TopLineY As Integer = 10

        e.Graphics.DrawLine(DashedLinePen, 0, BottomLineY, signaturePanel.Width, BottomLineY)
        e.Graphics.DrawLine(DashedLinePen, 0, TopLineY, signaturePanel.Width, TopLineY)

    End Sub

    Private Sub InkSecureSignature_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ' Determine Runtime versus Design mode, which controls Ink collection.
        ' If this control is on a form and the Site is Nothing, then it may be runtime.
        ' The final determination is made by looking at Site.DesignMode.
        If Me.FindForm.Site Is Nothing Then
            inDesignMode = False
        Else
            inDesignMode = (Me.FindForm.Site.DesignMode)
        End If

        ' Create a new InkOverlay.
        If signatureInkOverlay Is Nothing Then
            Call CreateNewInkOverlay()
            Call SetDefaultDrawingAttributes(Color.Blue)
        End If

        ' Enable Ink collection if we are not in design mode 
        ' and the signature is not complete.
        If Not inDesignMode AndAlso Not SignatureComplete Then
            signatureInkOverlay.Enabled = True
        Else
            signatureInkOverlay.Enabled = False
        End If

    End Sub

    Private Sub signatureInkOverlay_Stroke(ByVal sender As Object, ByVal e As Microsoft.Ink.InkCollectorStrokeEventArgs) Handles signatureInkOverlay.Stroke

        ' First, check to ensure that this is the first Stroke, otherwise exit.
        If signatureInkOverlay.Ink.Strokes.Count > 1 Then Exit Sub

        ' Set the Acquired Signature Start On to Now.
        InkSecureSignatureDataObject.AcquiredSignatureStartOn = Now

        ' Call ValidateData to see if all of the required
        ' criteria has been met to "accept" the signature.
        ValidateData()

    End Sub

    Private Sub signersNameTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ValidateData()
    End Sub

    Private Sub signatureInkOverlay_CursorInRange(ByVal sender As Object, ByVal e As Microsoft.Ink.InkCollectorCursorInRangeEventArgs) Handles signatureInkOverlay.CursorInRange
        If signatureInkOverlay.Enabled Then
            If e.Cursor.Inverted = True Then
                signatureInkOverlay.EditingMode = InkOverlayEditingMode.Delete
            Else
                signatureInkOverlay.EditingMode = InkOverlayEditingMode.Ink
            End If
        End If
    End Sub

    Private Sub InkSecureSignature_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Resize
        signaturePanel.Invalidate()
    End Sub

#End Region

End Class
