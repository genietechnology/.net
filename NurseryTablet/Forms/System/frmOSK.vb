﻿Imports DevComponents.DotNetBar.Keyboard

Public Class frmOSK

    Private m_ReturnValue As String = ""
    Private m_AllowReturn As Boolean = True
    Private m_AllowPunc As Boolean = True
    Private m_ListName As String = ""
    Private m_MaxLength As Integer = 0
    Private m_QuickTexts As New List(Of String)

#Region "Properties"

    Public WriteOnly Property DefaultValue() As String
        Set(ByVal value As String)
            txtText.Text = value
            m_ReturnValue = value
        End Set
    End Property

    Public WriteOnly Property AllowReturn() As Boolean
        Set(ByVal value As Boolean)
            m_AllowReturn = value
        End Set
    End Property

    Public WriteOnly Property ApplicationListName() As String
        Set(ByVal value As String)
            m_ListName = value
        End Set
    End Property

    Public WriteOnly Property AllowPunc() As Boolean
        Set(ByVal value As Boolean)
            m_AllowPunc = value
        End Set
    End Property

    Public ReadOnly Property ReturnValue() As String
        Get
            Return m_ReturnValue
        End Get
    End Property

    Public Property MaxLength() As Integer
        Get
            Return m_MaxLength
        End Get
        Set(ByVal value As Integer)
            m_MaxLength = value
            txtText.Properties.MaxLength = value
        End Set
    End Property

#End Region

    Private Sub frmOSK_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        SetupKeyboard()
        SetupQuickText()
        txtText.SelectionStart = txtText.Text.Length + 1
    End Sub

    Private Sub SetupQuickText()

        kcQuickText.Keyboard.Layouts.Clear()
        Dim _Layout As New LinearKeyboardLayout
        kcQuickText.Keyboard.Layouts.Add(_Layout)

        If m_ListName = "" Then Exit Sub

        Dim _Lists As List(Of NurseryGenieData.AppList) = Business.Lists.GetList(m_ListName)
        If _Lists IsNot Nothing Then
            For Each _i In _Lists
                m_QuickTexts.Add(_i.Name)
            Next
        End If

        PopulateQuickText(_Layout, 0)

    End Sub

    Private Sub PopulateQuickText(ByRef Layout As LinearKeyboardLayout, ByVal StartItem As Integer)

        If StartItem + 1 > m_QuickTexts.Count Then Exit Sub

        Dim _i As Integer = StartItem
        Dim _EndItem As Integer = StartItem + 8

        AddItem(Layout, _i)
        AddItem(Layout, _i)
        AddItem(Layout, _i)
        AddItem(Layout, _i)
        AddItem(Layout, _i)
        AddItem(Layout, _i)
        AddItem(Layout, _i)
        AddItem(Layout, _i)

        If _EndItem < m_QuickTexts.Count Then
            Layout.AddLine()
            Layout.AddKey("More...", "{More}", )
        End If

    End Sub

    Private Sub AddItem(ByRef Layout As LinearKeyboardLayout, ByRef ItemNumber As Integer)

        If ItemNumber + 1 > m_QuickTexts.Count Then
            Layout.AddKey("")
        Else
            Dim _Text As String = m_QuickTexts.Item(ItemNumber)
            Layout.AddKey(_Text, _Text)
        End If

        Layout.AddLine()
        ItemNumber += 1

    End Sub

    Private Sub SetupKeyboard()

        Dim _Layout As LinearKeyboardLayout
        kc.Keyboard.Layouts.Clear()

        '*****************************************************************************************************

        _Layout = New LinearKeyboardLayout
        kc.Keyboard.Layouts.Add(_Layout)

        _Layout.AddKey("1")
        _Layout.AddKey("2")
        _Layout.AddKey("3")
        _Layout.AddKey("4")
        _Layout.AddKey("5")
        _Layout.AddKey("6")
        _Layout.AddKey("7")
        _Layout.AddKey("8")
        _Layout.AddKey("9")
        _Layout.AddKey("0")
        _Layout.AddKey("Backspace", "{BACKSPACE}", , 21, , KeyStyle.Dark)

        _Layout.AddLine()
        _Layout.AddSpace(4)

        _Layout.AddKey("q")
        _Layout.AddKey("w")
        _Layout.AddKey("e")
        _Layout.AddKey("r")
        _Layout.AddKey("t")
        _Layout.AddKey("y")
        _Layout.AddKey("u")
        _Layout.AddKey("i")
        _Layout.AddKey("o")
        _Layout.AddKey("p")

        _Layout.AddKey("Enter", "{ENTER}", , 17, 32, KeyStyle.Dark)

        _Layout.AddLine()
        _Layout.AddSpace(8)

        _Layout.AddKey("a")
        _Layout.AddKey("s")
        _Layout.AddKey("d")
        _Layout.AddKey("f")
        _Layout.AddKey("g")
        _Layout.AddKey("h")
        _Layout.AddKey("j")
        _Layout.AddKey("k")
        _Layout.AddKey("l")

        _Layout.AddLine()

        _Layout.AddKey("Shift", "", , 14, , KeyStyle.Pressed, 1)
        _Layout.AddKey("z")
        _Layout.AddKey("x")
        _Layout.AddKey("c")
        _Layout.AddKey("v")
        _Layout.AddKey("b")
        _Layout.AddKey("n")
        _Layout.AddKey("m")
        _Layout.AddKey(",", , , , , KeyStyle.Dark)
        _Layout.AddKey(".", , , , , KeyStyle.Dark)

        _Layout.AddLine()
        _Layout.AddSpace(19)

        _Layout.AddKey(":-)", "", , , , KeyStyle.Pressed, 2)
        _Layout.AddKey("Space", "^ ", , 43, , KeyStyle.Dark)
        _Layout.AddKey("?", , , , , KeyStyle.Dark)
        _Layout.AddKey("@", , , , , KeyStyle.Dark)
        _Layout.AddKey("Accept", "{Accept}", , 17, , KeyStyle.Pressed)
        _Layout.AddKey("Cancel", "{Cancel}", , 17, , KeyStyle.Pressed)

        '*****************************************************************************************************

        _Layout = New LinearKeyboardLayout
        kc.Keyboard.Layouts.Add(_Layout)

        _Layout.AddKey("!", layout:=KeyboardLayout.PreviousLayout)

        _Layout.AddLine()
        _Layout.AddSpace(4)

        _Layout.AddKey("Q", layout:=KeyboardLayout.PreviousLayout)
        _Layout.AddKey("W", layout:=KeyboardLayout.PreviousLayout)
        _Layout.AddKey("E", layout:=KeyboardLayout.PreviousLayout)
        _Layout.AddKey("R", layout:=KeyboardLayout.PreviousLayout)
        _Layout.AddKey("T", layout:=KeyboardLayout.PreviousLayout)
        _Layout.AddKey("Y", layout:=KeyboardLayout.PreviousLayout)
        _Layout.AddKey("U", layout:=KeyboardLayout.PreviousLayout)
        _Layout.AddKey("I", layout:=KeyboardLayout.PreviousLayout)
        _Layout.AddKey("O", layout:=KeyboardLayout.PreviousLayout)
        _Layout.AddKey("P", layout:=KeyboardLayout.PreviousLayout)

        _Layout.AddKey("Enter", "{ENTER}", , 17, 30, KeyStyle.Dark)

        _Layout.AddLine()
        _Layout.AddSpace(8)

        _Layout.AddKey("A", layout:=KeyboardLayout.PreviousLayout)
        _Layout.AddKey("S", layout:=KeyboardLayout.PreviousLayout)
        _Layout.AddKey("D", layout:=KeyboardLayout.PreviousLayout)
        _Layout.AddKey("F", layout:=KeyboardLayout.PreviousLayout)
        _Layout.AddKey("G", layout:=KeyboardLayout.PreviousLayout)
        _Layout.AddKey("H", layout:=KeyboardLayout.PreviousLayout)
        _Layout.AddKey("J", layout:=KeyboardLayout.PreviousLayout)
        _Layout.AddKey("K", layout:=KeyboardLayout.PreviousLayout)
        _Layout.AddKey("L", layout:=KeyboardLayout.PreviousLayout)

        _Layout.AddLine()

        _Layout.AddKey("Z", layout:=KeyboardLayout.PreviousLayout)
        _Layout.AddKey("X", layout:=KeyboardLayout.PreviousLayout)
        _Layout.AddKey("C", layout:=KeyboardLayout.PreviousLayout)
        _Layout.AddKey("V", layout:=KeyboardLayout.PreviousLayout)
        _Layout.AddKey("B", layout:=KeyboardLayout.PreviousLayout)
        _Layout.AddKey("N", layout:=KeyboardLayout.PreviousLayout)
        _Layout.AddKey("M", layout:=KeyboardLayout.PreviousLayout)

        _Layout.AddLine()

        _Layout.AddKey("Space", "^ ", , 43, , KeyStyle.Dark)
        _Layout.AddKey("Accept", "{Accept}", , 17, , KeyStyle.Pressed)
        _Layout.AddKey("Cancel", "{Cancel}", , 17, , KeyStyle.Pressed)

        '*****************************************************************************************************

        _Layout = New LinearKeyboardLayout
        kc.Keyboard.Layouts.Add(_Layout)

        _Layout.AddKey(":-)", ":-{)}", layout:=KeyboardLayout.PreviousLayout)
        _Layout.AddKey(":-|", ":-{|}", layout:=KeyboardLayout.PreviousLayout)
        _Layout.AddKey(":-(", ":-{(}", layout:=KeyboardLayout.PreviousLayout)

        '*****************************************************************************************************

    End Sub

    Private Sub kc_SendingKey(sender As Object, e As KeyboardKeyCancelEventArgs) Handles kc.SendingKey

        Select Case e.Key

            Case "{Accept}"

                e.Cancel = True
                m_ReturnValue = txtText.Text
                Me.Close()

            Case "{Cancel}"
                e.Cancel = True
                Me.Close()

        End Select

    End Sub
End Class
