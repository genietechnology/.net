﻿Imports NurseryTablet.SharedModule
Imports System.Drawing
Imports NurseryTablet.SharedModule.Enums

Public Class frmAddRemove

    Private m_Grid As New List(Of GridIncident)
    Private m_Incidents As List(Of NurseryGenieData.Incident)
    Private m_ReturnID As String = ""
    Private m_ReturnMode As Enums.AddRemoveReturnMode = Enums.AddRemoveReturnMode.NotSet

    Public Sub New(ByVal Incidents As List(Of NurseryGenieData.Incident), ByVal AddText As String, ByVal RemoveText As String, ByVal FormCaption As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        btnAdd.Text = AddText
        btnRemove.Text = RemoveText
        Me.Text = FormCaption + " (Double-Tap to View Detail)"

        m_Incidents = Incidents

    End Sub

    Public ReadOnly Property ReturnMode As AddRemoveReturnMode
        Get
            Return m_ReturnMode
        End Get
    End Property

    Public ReadOnly Property ReturnID As String
        Get
            Return m_ReturnID
        End Get
    End Property

    Private Sub frmAddRemove_Load(sender As Object, e As EventArgs) Handles Me.Load
        DisplayRecord()
    End Sub

    Private Sub DisplayRecord()

        If m_Incidents Is Nothing Then Exit Sub

        tgRecords.HideFirstColumn = True

        For Each _i In m_Incidents
            m_Grid.Add(New GridIncident(_i.ID.Value, _i.Details, _i.Location))
        Next

        tgRecords.Populate(m_Grid)

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click

        m_ReturnMode = Enums.AddRemoveReturnMode.NotSet
        m_ReturnID = ""

        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()

    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click

        m_ReturnMode = Enums.AddRemoveReturnMode.AddRecord
        m_ReturnID = ""

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()

    End Sub

    Private Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click

        If tgRecords.CurrentRow Is Nothing Then Exit Sub

        If Msgbox("Are you sure you want to Remove this Record?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Remove Record") = Windows.Forms.DialogResult.Yes Then

            m_ReturnMode = Enums.AddRemoveReturnMode.RemoveRecord
            m_ReturnID = tgRecords.CurrentRow("ID").ToString

            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()

        End If

    End Sub

    Private Sub EditRecord()

        If tgRecords.CurrentRow Is Nothing Then Exit Sub

        m_ReturnMode = Enums.AddRemoveReturnMode.EditRecord

        Dim _I As GridIncident = CType(tgRecords.CurrentRow, GridIncident)
        m_ReturnID = _I.ID.ToString

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()

    End Sub

    Private Sub tgRecords_GridDoubleClick(sender As Object, e As EventArgs) Handles tgRecords.GridDoubleClick
        EditRecord()
    End Sub

    Private Class GridIncident

        Public Sub New(ByVal ID As Guid, ByVal Details As String, ByVal Location As String)
            Me.ID = ID
            Me.Details = Details
            Me.Location = Location
        End Sub

        Public Property ID As Guid
        Public Property Details As String
        Public Property Location As String

    End Class

End Class
