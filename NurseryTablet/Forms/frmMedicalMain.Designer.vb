﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMedicalMain
    Inherits frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMedicalMain))
        Me.btnLogMedicine = New DevExpress.XtraEditors.SimpleButton()
        Me.btnExit = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMedicationRequest = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSuncream = New DevExpress.XtraEditors.SimpleButton()
        Me.btnLogAccident = New DevExpress.XtraEditors.SimpleButton()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        '
        'btnLogMedicine
        '
        Me.btnLogMedicine.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLogMedicine.Appearance.Options.UseFont = True
        Me.btnLogMedicine.Image = CType(resources.GetObject("btnLogMedicine.Image"), System.Drawing.Image)
        Me.btnLogMedicine.Location = New System.Drawing.Point(521, 12)
        Me.btnLogMedicine.Name = "btnLogMedicine"
        Me.btnLogMedicine.Size = New System.Drawing.Size(490, 90)
        Me.btnLogMedicine.TabIndex = 13
        Me.btnLogMedicine.Text = "Log Un-Scheduled Medicine"
        '
        'btnExit
        '
        Me.btnExit.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Appearance.Options.UseFont = True
        Me.btnExit.Image = CType(resources.GetObject("btnExit.Image"), System.Drawing.Image)
        Me.btnExit.Location = New System.Drawing.Point(14, 225)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(997, 90)
        Me.btnExit.TabIndex = 23
        Me.btnExit.Text = "Back"
        '
        'btnMedicationRequest
        '
        Me.btnMedicationRequest.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMedicationRequest.Appearance.Options.UseFont = True
        Me.btnMedicationRequest.Image = CType(resources.GetObject("btnMedicationRequest.Image"), System.Drawing.Image)
        Me.btnMedicationRequest.Location = New System.Drawing.Point(14, 12)
        Me.btnMedicationRequest.Name = "btnMedicationRequest"
        Me.btnMedicationRequest.Size = New System.Drawing.Size(490, 90)
        Me.btnMedicationRequest.TabIndex = 12
        Me.btnMedicationRequest.Text = "Create Medication Request"
        '
        'btnSuncream
        '
        Me.btnSuncream.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSuncream.Appearance.Options.UseFont = True
        Me.btnSuncream.Location = New System.Drawing.Point(14, 118)
        Me.btnSuncream.Name = "btnSuncream"
        Me.btnSuncream.Size = New System.Drawing.Size(490, 90)
        Me.btnSuncream.TabIndex = 14
        Me.btnSuncream.Text = "Log Suncream"
        '
        'btnLogAccident
        '
        Me.btnLogAccident.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLogAccident.Appearance.Options.UseFont = True
        Me.btnLogAccident.Image = CType(resources.GetObject("btnLogAccident.Image"), System.Drawing.Image)
        Me.btnLogAccident.Location = New System.Drawing.Point(521, 118)
        Me.btnLogAccident.Name = "btnLogAccident"
        Me.btnLogAccident.Size = New System.Drawing.Size(490, 90)
        Me.btnLogAccident.TabIndex = 20
        Me.btnLogAccident.Text = "Log Incident"
        '
        'frmMedicalMain
        '
        Me.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.Appearance.Options.UseBackColor = True
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1024, 330)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnLogAccident)
        Me.Controls.Add(Me.btnLogMedicine)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnMedicationRequest)
        Me.Controls.Add(Me.btnSuncream)
        Me.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMedicalMain"
        Me.Text = "Nursery Management"
        Me.Controls.SetChildIndex(Me.btnSuncream, 0)
        Me.Controls.SetChildIndex(Me.btnMedicationRequest, 0)
        Me.Controls.SetChildIndex(Me.btnExit, 0)
        Me.Controls.SetChildIndex(Me.btnLogMedicine, 0)
        Me.Controls.SetChildIndex(Me.btnLogAccident, 0)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnLogMedicine As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnExit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMedicationRequest As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSuncream As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnLogAccident As DevExpress.XtraEditors.SimpleButton
End Class
