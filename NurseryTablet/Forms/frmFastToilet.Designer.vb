﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFastToilet
    Inherits frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.tbRooms = New DevExpress.XtraBars.Navigation.TileBar()
        Me.tbRoomGroup = New DevExpress.XtraBars.Navigation.TileBarGroup()
        Me.timSleepCheck = New System.Windows.Forms.Timer(Me.components)
        Me.sc = New DevExpress.XtraEditors.XtraScrollableControl()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAccept = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupControl2.Controls.Add(Me.tbRooms)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.ShowCaption = False
        Me.GroupControl2.Size = New System.Drawing.Size(200, 437)
        Me.GroupControl2.TabIndex = 87
        Me.GroupControl2.Text = "GroupControl2"
        '
        'tbRooms
        '
        Me.tbRooms.AllowDrag = False
        Me.tbRooms.AllowItemHover = False
        Me.tbRooms.AllowSelectedItem = True
        Me.tbRooms.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbRooms.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        Me.tbRooms.Groups.Add(Me.tbRoomGroup)
        Me.tbRooms.ItemSize = 80
        Me.tbRooms.Location = New System.Drawing.Point(2, 2)
        Me.tbRooms.Name = "tbRooms"
        Me.tbRooms.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.tbRooms.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollButtons
        Me.tbRooms.Size = New System.Drawing.Size(196, 433)
        Me.tbRooms.TabIndex = 88
        Me.tbRooms.VerticalContentAlignment = DevExpress.Utils.VertAlignment.Top
        '
        'tbRoomGroup
        '
        Me.tbRoomGroup.Name = "tbRoomGroup"
        '
        'sc
        '
        Me.sc.AllowTouchScroll = True
        Me.sc.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.sc.Location = New System.Drawing.Point(218, 12)
        Me.sc.Name = "sc"
        Me.sc.Size = New System.Drawing.Size(954, 386)
        Me.sc.TabIndex = 88
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Image = Global.NurseryTablet.My.Resources.Resources.cancel_32
        Me.btnCancel.Location = New System.Drawing.Point(1027, 405)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(145, 44)
        Me.btnCancel.TabIndex = 90
        Me.btnCancel.Text = "Cancel"
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAccept.Image = Global.NurseryTablet.My.Resources.Resources.success_32
        Me.btnAccept.Location = New System.Drawing.Point(876, 405)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(145, 44)
        Me.btnAccept.TabIndex = 89
        Me.btnAccept.Text = "Accept"
        '
        'frmFastToilet
        '
        Me.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.Appearance.Options.UseBackColor = True
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(11.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1184, 461)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnAccept)
        Me.Controls.Add(Me.sc)
        Me.Controls.Add(Me.GroupControl2)
        Me.Name = "frmFastToilet"
        Me.Text = "Toilet Trip"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Controls.SetChildIndex(Me.GroupControl2, 0)
        Me.Controls.SetChildIndex(Me.sc, 0)
        Me.Controls.SetChildIndex(Me.btnAccept, 0)
        Me.Controls.SetChildIndex(Me.btnCancel, 0)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents tbRooms As DevExpress.XtraBars.Navigation.TileBar
    Friend WithEvents tbRoomGroup As DevExpress.XtraBars.Navigation.TileBarGroup
    Friend WithEvents timSleepCheck As System.Windows.Forms.Timer
    Friend WithEvents sc As DevExpress.XtraEditors.XtraScrollableControl
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAccept As DevExpress.XtraEditors.SimpleButton
End Class
