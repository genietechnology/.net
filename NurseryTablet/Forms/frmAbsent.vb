﻿Option Strict On

Imports NurseryTablet.Business
Imports NurseryTablet.SharedModule

Public Class frmAbsent

    Private m_DayID As String = Day.TodayIDString

    Private m_ChildID As String = ""
    Private m_ChildName As String = ""
    Private m_ChildGroupID As String

    Private m_FamilyID As String
    Private m_ChildRecord As NurseryGenieData.Child = Nothing

    Public Sub New(ByVal ChildID As String, ByVal ChildName As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_ChildID = ChildID
        m_ChildName = ChildName

    End Sub

    Private Sub frmAbsent_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtChild.Text = m_ChildName
        SetChildDetails()
    End Sub

    Private Sub SetChildDetails()

        m_ChildRecord = Business.Child.ReturnChildByID(m_ChildID)
        If m_ChildRecord IsNot Nothing Then
            m_ChildGroupID = m_ChildRecord.GroupID.ToString
            m_FamilyID = m_ChildRecord.FamilyID.ToString
        End If

    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click

        If Not CheckAnswers Then Exit Sub

        btnAccept.Enabled = False
        Application.DoEvents()

        SharedModule.LogActivity(TodayID, m_ChildID, m_ChildName, SharedModule.EnumActivityType.Absence, fldReason.ValueText, _
                                 fldContact.ValueText, "", "", _
                                 fldStaff.ValueID, fldStaff.ValueText, fldComments.ValueText)

        Me.DialogResult = Windows.Forms.DialogResult.OK

        btnAccept.Enabled = True
        Application.DoEvents()

    End Sub

    Private Function CheckAnswers() As Boolean

        If fldContact.ValueText = "" Then
            Msgbox("Please select the contact reporting the absence.", MessageBoxIcon.Exclamation, "Log Absence")
            Return False
        End If

        If fldReason.ValueText = "" Then
            Msgbox("Please enter the reason for absence.", MessageBoxIcon.Exclamation, "Log Absence")
            Return False
        End If

        If fldStaff.ValueText = "" Then
            Msgbox("Please select the staff member recording the absence.", MessageBoxIcon.Exclamation, "Log Absence")
            Return False
        End If

        Return True

    End Function

    Private Sub fldStaff_ButtonClick(sender As Object, e As EventArgs) Handles fldStaff.ButtonClick

        Dim _P As Pair = Business.Staff.FindStaff(Enums.PersonMode.OnlyCheckedIn)
        If _P IsNot Nothing Then
            fldStaff.ValueID = _P.Code
            fldStaff.ValueText = _P.Text
        End If

    End Sub

    Private Sub fldContact_ButtonClick(sender As Object, e As EventArgs) Handles fldContact.ButtonClick

        Dim _Contacts As List(Of NurseryGenieData.Contact) = Business.Child.GetContacts(m_ChildID)
        If _Contacts IsNot Nothing Then

            Dim _ContactList As New List(Of Pair)
            For Each _C In _Contacts
                _ContactList.Add(New Pair(_C.ID.ToString, _C.Fullname))
            Next

            Dim _P As Pair = ReturnButtons(_ContactList, "Select Contact")
            If _P IsNot Nothing Then
                fldContact.ValueID = _P.Code
                fldContact.ValueText = _P.Text
            End If

        End If

    End Sub

    Private Sub fldComments_AfterButtonClick(sender As Object, e As EventArgs) Handles fldComments.AfterButtonClick
        txtComments.Text = fldComments.ValueText
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub
End Class