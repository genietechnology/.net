﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAbsent
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAbsent))
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.panBottom = New DevExpress.XtraEditors.PanelControl()
        Me.Splitter2 = New System.Windows.Forms.Splitter()
        Me.txtChild = New DevExpress.XtraEditors.TextEdit()
        Me.txtComments = New DevExpress.XtraEditors.MemoEdit()
        Me.btnAccept = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.fldComments = New NurseryTablet.Field()
        Me.fldStaff = New NurseryTablet.Field()
        Me.fldReason = New NurseryTablet.Field()
        Me.fldContact = New NurseryTablet.Field()
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panBottom.SuspendLayout()
        CType(Me.txtChild.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtComments.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(14, 15)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(44, 25)
        Me.LabelControl2.TabIndex = 0
        Me.LabelControl2.Text = "Child"
        '
        'panBottom
        '
        Me.panBottom.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panBottom.Appearance.Options.UseFont = True
        Me.panBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.panBottom.Controls.Add(Me.Splitter2)
        Me.panBottom.Controls.Add(Me.btnAccept)
        Me.panBottom.Controls.Add(Me.btnCancel)
        Me.panBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panBottom.Location = New System.Drawing.Point(0, 395)
        Me.panBottom.Name = "panBottom"
        Me.panBottom.Size = New System.Drawing.Size(1019, 56)
        Me.panBottom.TabIndex = 0
        '
        'Splitter2
        '
        Me.Splitter2.Location = New System.Drawing.Point(0, 0)
        Me.Splitter2.Name = "Splitter2"
        Me.Splitter2.Size = New System.Drawing.Size(3, 56)
        Me.Splitter2.TabIndex = 0
        Me.Splitter2.TabStop = False
        '
        'txtChild
        '
        Me.txtChild.Location = New System.Drawing.Point(169, 12)
        Me.txtChild.Name = "txtChild"
        Me.txtChild.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtChild.Properties.Appearance.Options.UseFont = True
        Me.txtChild.Properties.AutoHeight = False
        Me.txtChild.Properties.ReadOnly = True
        Me.txtChild.Size = New System.Drawing.Size(835, 32)
        Me.txtChild.TabIndex = 1
        '
        'txtComments
        '
        Me.txtComments.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtComments.Location = New System.Drawing.Point(14, 278)
        Me.txtComments.Name = "txtComments"
        Me.txtComments.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtComments.Properties.Appearance.Options.UseFont = True
        Me.txtComments.Size = New System.Drawing.Size(990, 111)
        Me.txtComments.TabIndex = 102
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAccept.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAccept.Appearance.Options.UseFont = True
        Me.btnAccept.Image = CType(resources.GetObject("btnAccept.Image"), System.Drawing.Image)
        Me.btnAccept.Location = New System.Drawing.Point(551, 3)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(300, 45)
        Me.btnAccept.TabIndex = 0
        Me.btnAccept.Text = "Mark as Absent"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Image = CType(resources.GetObject("btnCancel.Image"), System.Drawing.Image)
        Me.btnCancel.Location = New System.Drawing.Point(857, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(150, 45)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "Cancel"
        '
        'fldComments
        '
        Me.fldComments.ButtonCustom = False
        Me.fldComments.ButtonSQL = Nothing
        Me.fldComments.ButtonText = ""
        Me.fldComments.ButtonType = NurseryTablet.Field.EnumButtonType.Enter
        Me.fldComments.ButtonWidth = 50.0!
        Me.fldComments.FamilyID = Nothing
        Me.fldComments.HideText = False
        Me.fldComments.LabelText = "Comments"
        Me.fldComments.LabelWidth = 200.0!
        Me.fldComments.Location = New System.Drawing.Point(12, 221)
        Me.fldComments.MaximumSize = New System.Drawing.Size(1000, 51)
        Me.fldComments.MinimumSize = New System.Drawing.Size(210, 51)
        Me.fldComments.Name = "fldComments"
        Me.fldComments.QuickTextList = Nothing
        Me.fldComments.Size = New System.Drawing.Size(995, 51)
        Me.fldComments.TabIndex = 5
        Me.fldComments.ValueID = Nothing
        Me.fldComments.ValueMaxLength = 0
        Me.fldComments.ValueText = ""
        '
        'fldStaff
        '
        Me.fldStaff.ButtonCustom = True
        Me.fldStaff.ButtonSQL = Nothing
        Me.fldStaff.ButtonText = ""
        Me.fldStaff.ButtonType = NurseryTablet.Field.EnumButtonType.Lookup
        Me.fldStaff.ButtonWidth = 50.0!
        Me.fldStaff.FamilyID = Nothing
        Me.fldStaff.HideText = False
        Me.fldStaff.LabelText = "Staff"
        Me.fldStaff.LabelWidth = 200.0!
        Me.fldStaff.Location = New System.Drawing.Point(12, 50)
        Me.fldStaff.MaximumSize = New System.Drawing.Size(1000, 51)
        Me.fldStaff.MinimumSize = New System.Drawing.Size(210, 51)
        Me.fldStaff.Name = "fldStaff"
        Me.fldStaff.QuickTextList = Nothing
        Me.fldStaff.Size = New System.Drawing.Size(995, 51)
        Me.fldStaff.TabIndex = 4
        Me.fldStaff.ValueID = Nothing
        Me.fldStaff.ValueMaxLength = 0
        Me.fldStaff.ValueText = ""
        '
        'fldReason
        '
        Me.fldReason.ButtonCustom = False
        Me.fldReason.ButtonSQL = Nothing
        Me.fldReason.ButtonText = ""
        Me.fldReason.ButtonType = NurseryTablet.Field.EnumButtonType.Enter
        Me.fldReason.ButtonWidth = 50.0!
        Me.fldReason.FamilyID = Nothing
        Me.fldReason.HideText = False
        Me.fldReason.LabelText = "Reason for Absence"
        Me.fldReason.LabelWidth = 200.0!
        Me.fldReason.Location = New System.Drawing.Point(12, 164)
        Me.fldReason.MaximumSize = New System.Drawing.Size(1000, 51)
        Me.fldReason.MinimumSize = New System.Drawing.Size(210, 51)
        Me.fldReason.Name = "fldReason"
        Me.fldReason.QuickTextList = "Child Absence Types"
        Me.fldReason.Size = New System.Drawing.Size(995, 51)
        Me.fldReason.TabIndex = 3
        Me.fldReason.ValueID = Nothing
        Me.fldReason.ValueMaxLength = 0
        Me.fldReason.ValueText = ""
        '
        'fldContact
        '
        Me.fldContact.ButtonCustom = True
        Me.fldContact.ButtonSQL = Nothing
        Me.fldContact.ButtonText = ""
        Me.fldContact.ButtonType = NurseryTablet.Field.EnumButtonType.Lookup
        Me.fldContact.ButtonWidth = 50.0!
        Me.fldContact.FamilyID = Nothing
        Me.fldContact.HideText = False
        Me.fldContact.LabelText = "Contact"
        Me.fldContact.LabelWidth = 200.0!
        Me.fldContact.Location = New System.Drawing.Point(12, 107)
        Me.fldContact.MaximumSize = New System.Drawing.Size(1000, 51)
        Me.fldContact.MinimumSize = New System.Drawing.Size(210, 51)
        Me.fldContact.Name = "fldContact"
        Me.fldContact.QuickTextList = Nothing
        Me.fldContact.Size = New System.Drawing.Size(995, 51)
        Me.fldContact.TabIndex = 2
        Me.fldContact.ValueID = Nothing
        Me.fldContact.ValueMaxLength = 0
        Me.fldContact.ValueText = ""
        '
        'frmAbsent
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1019, 451)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtComments)
        Me.Controls.Add(Me.fldComments)
        Me.Controls.Add(Me.fldStaff)
        Me.Controls.Add(Me.fldReason)
        Me.Controls.Add(Me.fldContact)
        Me.Controls.Add(Me.txtChild)
        Me.Controls.Add(Me.panBottom)
        Me.Controls.Add(Me.LabelControl2)
        Me.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmAbsent"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panBottom.ResumeLayout(False)
        CType(Me.txtChild.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtComments.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Protected Friend WithEvents panBottom As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnAccept As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtChild As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Splitter2 As System.Windows.Forms.Splitter
    Friend WithEvents fldContact As NurseryTablet.Field
    Friend WithEvents fldReason As NurseryTablet.Field
    Friend WithEvents fldStaff As NurseryTablet.Field
    Friend WithEvents fldComments As NurseryTablet.Field
    Friend WithEvents txtComments As DevExpress.XtraEditors.MemoEdit
End Class
