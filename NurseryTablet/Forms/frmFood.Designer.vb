﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFood
    Inherits NurseryTablet.frmBaseChild

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFood))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnFood8 = New System.Windows.Forms.Button()
        Me.btnFood7 = New System.Windows.Forms.Button()
        Me.btnFood6 = New System.Windows.Forms.Button()
        Me.btnFood5 = New System.Windows.Forms.Button()
        Me.btnFood4 = New System.Windows.Forms.Button()
        Me.btnFood3 = New System.Windows.Forms.Button()
        Me.btnFood2 = New System.Windows.Forms.Button()
        Me.btnFood1 = New System.Windows.Forms.Button()
        Me.btnNone8 = New System.Windows.Forms.Button()
        Me.btnSome8 = New System.Windows.Forms.Button()
        Me.btnMost8 = New System.Windows.Forms.Button()
        Me.btnAll8 = New System.Windows.Forms.Button()
        Me.btnNone7 = New System.Windows.Forms.Button()
        Me.btnSome7 = New System.Windows.Forms.Button()
        Me.btnMost7 = New System.Windows.Forms.Button()
        Me.btnAll7 = New System.Windows.Forms.Button()
        Me.btnNone6 = New System.Windows.Forms.Button()
        Me.btnSome6 = New System.Windows.Forms.Button()
        Me.btnMost6 = New System.Windows.Forms.Button()
        Me.btnAll6 = New System.Windows.Forms.Button()
        Me.btnNone5 = New System.Windows.Forms.Button()
        Me.btnSome5 = New System.Windows.Forms.Button()
        Me.btnMost5 = New System.Windows.Forms.Button()
        Me.btnAll5 = New System.Windows.Forms.Button()
        Me.btnNone4 = New System.Windows.Forms.Button()
        Me.btnSome4 = New System.Windows.Forms.Button()
        Me.btnMost4 = New System.Windows.Forms.Button()
        Me.btnAll4 = New System.Windows.Forms.Button()
        Me.btnNone3 = New System.Windows.Forms.Button()
        Me.btnSome3 = New System.Windows.Forms.Button()
        Me.btnMost3 = New System.Windows.Forms.Button()
        Me.btnAll3 = New System.Windows.Forms.Button()
        Me.btnNone2 = New System.Windows.Forms.Button()
        Me.btnSome2 = New System.Windows.Forms.Button()
        Me.btnMost2 = New System.Windows.Forms.Button()
        Me.btnAll2 = New System.Windows.Forms.Button()
        Me.btnNone1 = New System.Windows.Forms.Button()
        Me.btnSome1 = New System.Windows.Forms.Button()
        Me.btnMost1 = New System.Windows.Forms.Button()
        Me.btnAll1 = New System.Windows.Forms.Button()
        Me.btnBreakfast = New System.Windows.Forms.Button()
        Me.btnSnack = New System.Windows.Forms.Button()
        Me.btnLunch = New System.Windows.Forms.Button()
        Me.btnTea = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.btnLunchDessert = New System.Windows.Forms.Button()
        Me.btnTeaDessert = New System.Windows.Forms.Button()
        Me.btnRemoveAll = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnFood8)
        Me.GroupBox1.Controls.Add(Me.btnFood7)
        Me.GroupBox1.Controls.Add(Me.btnFood6)
        Me.GroupBox1.Controls.Add(Me.btnFood5)
        Me.GroupBox1.Controls.Add(Me.btnFood4)
        Me.GroupBox1.Controls.Add(Me.btnFood3)
        Me.GroupBox1.Controls.Add(Me.btnFood2)
        Me.GroupBox1.Controls.Add(Me.btnFood1)
        Me.GroupBox1.Controls.Add(Me.btnNone8)
        Me.GroupBox1.Controls.Add(Me.btnSome8)
        Me.GroupBox1.Controls.Add(Me.btnMost8)
        Me.GroupBox1.Controls.Add(Me.btnAll8)
        Me.GroupBox1.Controls.Add(Me.btnNone7)
        Me.GroupBox1.Controls.Add(Me.btnSome7)
        Me.GroupBox1.Controls.Add(Me.btnMost7)
        Me.GroupBox1.Controls.Add(Me.btnAll7)
        Me.GroupBox1.Controls.Add(Me.btnNone6)
        Me.GroupBox1.Controls.Add(Me.btnSome6)
        Me.GroupBox1.Controls.Add(Me.btnMost6)
        Me.GroupBox1.Controls.Add(Me.btnAll6)
        Me.GroupBox1.Controls.Add(Me.btnNone5)
        Me.GroupBox1.Controls.Add(Me.btnSome5)
        Me.GroupBox1.Controls.Add(Me.btnMost5)
        Me.GroupBox1.Controls.Add(Me.btnAll5)
        Me.GroupBox1.Controls.Add(Me.btnNone4)
        Me.GroupBox1.Controls.Add(Me.btnSome4)
        Me.GroupBox1.Controls.Add(Me.btnMost4)
        Me.GroupBox1.Controls.Add(Me.btnAll4)
        Me.GroupBox1.Controls.Add(Me.btnNone3)
        Me.GroupBox1.Controls.Add(Me.btnSome3)
        Me.GroupBox1.Controls.Add(Me.btnMost3)
        Me.GroupBox1.Controls.Add(Me.btnAll3)
        Me.GroupBox1.Controls.Add(Me.btnNone2)
        Me.GroupBox1.Controls.Add(Me.btnSome2)
        Me.GroupBox1.Controls.Add(Me.btnMost2)
        Me.GroupBox1.Controls.Add(Me.btnAll2)
        Me.GroupBox1.Controls.Add(Me.btnNone1)
        Me.GroupBox1.Controls.Add(Me.btnSome1)
        Me.GroupBox1.Controls.Add(Me.btnMost1)
        Me.GroupBox1.Controls.Add(Me.btnAll1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 138)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1000, 337)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Meal Detail"
        '
        'btnFood8
        '
        Me.btnFood8.Location = New System.Drawing.Point(11, 298)
        Me.btnFood8.Name = "btnFood8"
        Me.btnFood8.Size = New System.Drawing.Size(529, 33)
        Me.btnFood8.TabIndex = 35
        Me.btnFood8.UseVisualStyleBackColor = True
        '
        'btnFood7
        '
        Me.btnFood7.Location = New System.Drawing.Point(11, 259)
        Me.btnFood7.Name = "btnFood7"
        Me.btnFood7.Size = New System.Drawing.Size(529, 33)
        Me.btnFood7.TabIndex = 30
        Me.btnFood7.UseVisualStyleBackColor = True
        '
        'btnFood6
        '
        Me.btnFood6.Location = New System.Drawing.Point(11, 220)
        Me.btnFood6.Name = "btnFood6"
        Me.btnFood6.Size = New System.Drawing.Size(529, 33)
        Me.btnFood6.TabIndex = 25
        Me.btnFood6.UseVisualStyleBackColor = True
        '
        'btnFood5
        '
        Me.btnFood5.Location = New System.Drawing.Point(11, 181)
        Me.btnFood5.Name = "btnFood5"
        Me.btnFood5.Size = New System.Drawing.Size(529, 33)
        Me.btnFood5.TabIndex = 20
        Me.btnFood5.UseVisualStyleBackColor = True
        '
        'btnFood4
        '
        Me.btnFood4.Location = New System.Drawing.Point(11, 142)
        Me.btnFood4.Name = "btnFood4"
        Me.btnFood4.Size = New System.Drawing.Size(529, 33)
        Me.btnFood4.TabIndex = 15
        Me.btnFood4.UseVisualStyleBackColor = True
        '
        'btnFood3
        '
        Me.btnFood3.Location = New System.Drawing.Point(11, 103)
        Me.btnFood3.Name = "btnFood3"
        Me.btnFood3.Size = New System.Drawing.Size(529, 33)
        Me.btnFood3.TabIndex = 10
        Me.btnFood3.UseVisualStyleBackColor = True
        '
        'btnFood2
        '
        Me.btnFood2.Location = New System.Drawing.Point(11, 64)
        Me.btnFood2.Name = "btnFood2"
        Me.btnFood2.Size = New System.Drawing.Size(529, 33)
        Me.btnFood2.TabIndex = 5
        Me.btnFood2.UseVisualStyleBackColor = True
        '
        'btnFood1
        '
        Me.btnFood1.Location = New System.Drawing.Point(11, 25)
        Me.btnFood1.Name = "btnFood1"
        Me.btnFood1.Size = New System.Drawing.Size(529, 33)
        Me.btnFood1.TabIndex = 0
        Me.btnFood1.UseVisualStyleBackColor = True
        '
        'btnNone8
        '
        Me.btnNone8.Location = New System.Drawing.Point(548, 298)
        Me.btnNone8.Name = "btnNone8"
        Me.btnNone8.Size = New System.Drawing.Size(107, 33)
        Me.btnNone8.TabIndex = 36
        Me.btnNone8.Text = "None"
        Me.btnNone8.UseVisualStyleBackColor = True
        '
        'btnSome8
        '
        Me.btnSome8.Location = New System.Drawing.Point(661, 298)
        Me.btnSome8.Name = "btnSome8"
        Me.btnSome8.Size = New System.Drawing.Size(107, 33)
        Me.btnSome8.TabIndex = 37
        Me.btnSome8.Text = "Some"
        Me.btnSome8.UseVisualStyleBackColor = True
        '
        'btnMost8
        '
        Me.btnMost8.Location = New System.Drawing.Point(774, 298)
        Me.btnMost8.Name = "btnMost8"
        Me.btnMost8.Size = New System.Drawing.Size(107, 33)
        Me.btnMost8.TabIndex = 38
        Me.btnMost8.Text = "Most"
        Me.btnMost8.UseVisualStyleBackColor = True
        '
        'btnAll8
        '
        Me.btnAll8.Location = New System.Drawing.Point(887, 298)
        Me.btnAll8.Name = "btnAll8"
        Me.btnAll8.Size = New System.Drawing.Size(107, 33)
        Me.btnAll8.TabIndex = 39
        Me.btnAll8.Text = "All"
        Me.btnAll8.UseVisualStyleBackColor = True
        '
        'btnNone7
        '
        Me.btnNone7.Location = New System.Drawing.Point(548, 259)
        Me.btnNone7.Name = "btnNone7"
        Me.btnNone7.Size = New System.Drawing.Size(107, 33)
        Me.btnNone7.TabIndex = 31
        Me.btnNone7.Text = "None"
        Me.btnNone7.UseVisualStyleBackColor = True
        '
        'btnSome7
        '
        Me.btnSome7.Location = New System.Drawing.Point(661, 259)
        Me.btnSome7.Name = "btnSome7"
        Me.btnSome7.Size = New System.Drawing.Size(107, 33)
        Me.btnSome7.TabIndex = 32
        Me.btnSome7.Text = "Some"
        Me.btnSome7.UseVisualStyleBackColor = True
        '
        'btnMost7
        '
        Me.btnMost7.Location = New System.Drawing.Point(774, 259)
        Me.btnMost7.Name = "btnMost7"
        Me.btnMost7.Size = New System.Drawing.Size(107, 33)
        Me.btnMost7.TabIndex = 33
        Me.btnMost7.Text = "Most"
        Me.btnMost7.UseVisualStyleBackColor = True
        '
        'btnAll7
        '
        Me.btnAll7.Location = New System.Drawing.Point(887, 259)
        Me.btnAll7.Name = "btnAll7"
        Me.btnAll7.Size = New System.Drawing.Size(107, 33)
        Me.btnAll7.TabIndex = 34
        Me.btnAll7.Text = "All"
        Me.btnAll7.UseVisualStyleBackColor = True
        '
        'btnNone6
        '
        Me.btnNone6.Location = New System.Drawing.Point(548, 220)
        Me.btnNone6.Name = "btnNone6"
        Me.btnNone6.Size = New System.Drawing.Size(107, 33)
        Me.btnNone6.TabIndex = 26
        Me.btnNone6.Text = "None"
        Me.btnNone6.UseVisualStyleBackColor = True
        '
        'btnSome6
        '
        Me.btnSome6.Location = New System.Drawing.Point(661, 220)
        Me.btnSome6.Name = "btnSome6"
        Me.btnSome6.Size = New System.Drawing.Size(107, 33)
        Me.btnSome6.TabIndex = 27
        Me.btnSome6.Text = "Some"
        Me.btnSome6.UseVisualStyleBackColor = True
        '
        'btnMost6
        '
        Me.btnMost6.Location = New System.Drawing.Point(774, 220)
        Me.btnMost6.Name = "btnMost6"
        Me.btnMost6.Size = New System.Drawing.Size(107, 33)
        Me.btnMost6.TabIndex = 28
        Me.btnMost6.Text = "Most"
        Me.btnMost6.UseVisualStyleBackColor = True
        '
        'btnAll6
        '
        Me.btnAll6.Location = New System.Drawing.Point(887, 220)
        Me.btnAll6.Name = "btnAll6"
        Me.btnAll6.Size = New System.Drawing.Size(107, 33)
        Me.btnAll6.TabIndex = 29
        Me.btnAll6.Text = "All"
        Me.btnAll6.UseVisualStyleBackColor = True
        '
        'btnNone5
        '
        Me.btnNone5.Location = New System.Drawing.Point(548, 181)
        Me.btnNone5.Name = "btnNone5"
        Me.btnNone5.Size = New System.Drawing.Size(107, 33)
        Me.btnNone5.TabIndex = 21
        Me.btnNone5.Text = "None"
        Me.btnNone5.UseVisualStyleBackColor = True
        '
        'btnSome5
        '
        Me.btnSome5.Location = New System.Drawing.Point(661, 181)
        Me.btnSome5.Name = "btnSome5"
        Me.btnSome5.Size = New System.Drawing.Size(107, 33)
        Me.btnSome5.TabIndex = 22
        Me.btnSome5.Text = "Some"
        Me.btnSome5.UseVisualStyleBackColor = True
        '
        'btnMost5
        '
        Me.btnMost5.Location = New System.Drawing.Point(774, 181)
        Me.btnMost5.Name = "btnMost5"
        Me.btnMost5.Size = New System.Drawing.Size(107, 33)
        Me.btnMost5.TabIndex = 23
        Me.btnMost5.Text = "Most"
        Me.btnMost5.UseVisualStyleBackColor = True
        '
        'btnAll5
        '
        Me.btnAll5.Location = New System.Drawing.Point(887, 181)
        Me.btnAll5.Name = "btnAll5"
        Me.btnAll5.Size = New System.Drawing.Size(107, 33)
        Me.btnAll5.TabIndex = 24
        Me.btnAll5.Text = "All"
        Me.btnAll5.UseVisualStyleBackColor = True
        '
        'btnNone4
        '
        Me.btnNone4.Location = New System.Drawing.Point(548, 142)
        Me.btnNone4.Name = "btnNone4"
        Me.btnNone4.Size = New System.Drawing.Size(107, 33)
        Me.btnNone4.TabIndex = 16
        Me.btnNone4.Text = "None"
        Me.btnNone4.UseVisualStyleBackColor = True
        '
        'btnSome4
        '
        Me.btnSome4.Location = New System.Drawing.Point(661, 142)
        Me.btnSome4.Name = "btnSome4"
        Me.btnSome4.Size = New System.Drawing.Size(107, 33)
        Me.btnSome4.TabIndex = 17
        Me.btnSome4.Text = "Some"
        Me.btnSome4.UseVisualStyleBackColor = True
        '
        'btnMost4
        '
        Me.btnMost4.Location = New System.Drawing.Point(774, 142)
        Me.btnMost4.Name = "btnMost4"
        Me.btnMost4.Size = New System.Drawing.Size(107, 33)
        Me.btnMost4.TabIndex = 18
        Me.btnMost4.Text = "Most"
        Me.btnMost4.UseVisualStyleBackColor = True
        '
        'btnAll4
        '
        Me.btnAll4.Location = New System.Drawing.Point(887, 142)
        Me.btnAll4.Name = "btnAll4"
        Me.btnAll4.Size = New System.Drawing.Size(107, 33)
        Me.btnAll4.TabIndex = 19
        Me.btnAll4.Text = "All"
        Me.btnAll4.UseVisualStyleBackColor = True
        '
        'btnNone3
        '
        Me.btnNone3.Location = New System.Drawing.Point(548, 103)
        Me.btnNone3.Name = "btnNone3"
        Me.btnNone3.Size = New System.Drawing.Size(107, 33)
        Me.btnNone3.TabIndex = 11
        Me.btnNone3.Text = "None"
        Me.btnNone3.UseVisualStyleBackColor = True
        '
        'btnSome3
        '
        Me.btnSome3.Location = New System.Drawing.Point(661, 103)
        Me.btnSome3.Name = "btnSome3"
        Me.btnSome3.Size = New System.Drawing.Size(107, 33)
        Me.btnSome3.TabIndex = 12
        Me.btnSome3.Text = "Some"
        Me.btnSome3.UseVisualStyleBackColor = True
        '
        'btnMost3
        '
        Me.btnMost3.Location = New System.Drawing.Point(774, 103)
        Me.btnMost3.Name = "btnMost3"
        Me.btnMost3.Size = New System.Drawing.Size(107, 33)
        Me.btnMost3.TabIndex = 13
        Me.btnMost3.Text = "Most"
        Me.btnMost3.UseVisualStyleBackColor = True
        '
        'btnAll3
        '
        Me.btnAll3.Location = New System.Drawing.Point(887, 103)
        Me.btnAll3.Name = "btnAll3"
        Me.btnAll3.Size = New System.Drawing.Size(107, 33)
        Me.btnAll3.TabIndex = 14
        Me.btnAll3.Text = "All"
        Me.btnAll3.UseVisualStyleBackColor = True
        '
        'btnNone2
        '
        Me.btnNone2.Location = New System.Drawing.Point(548, 64)
        Me.btnNone2.Name = "btnNone2"
        Me.btnNone2.Size = New System.Drawing.Size(107, 33)
        Me.btnNone2.TabIndex = 6
        Me.btnNone2.Text = "None"
        Me.btnNone2.UseVisualStyleBackColor = True
        '
        'btnSome2
        '
        Me.btnSome2.Location = New System.Drawing.Point(661, 64)
        Me.btnSome2.Name = "btnSome2"
        Me.btnSome2.Size = New System.Drawing.Size(107, 33)
        Me.btnSome2.TabIndex = 7
        Me.btnSome2.Text = "Some"
        Me.btnSome2.UseVisualStyleBackColor = True
        '
        'btnMost2
        '
        Me.btnMost2.Location = New System.Drawing.Point(774, 64)
        Me.btnMost2.Name = "btnMost2"
        Me.btnMost2.Size = New System.Drawing.Size(107, 33)
        Me.btnMost2.TabIndex = 8
        Me.btnMost2.Text = "Most"
        Me.btnMost2.UseVisualStyleBackColor = True
        '
        'btnAll2
        '
        Me.btnAll2.Location = New System.Drawing.Point(887, 64)
        Me.btnAll2.Name = "btnAll2"
        Me.btnAll2.Size = New System.Drawing.Size(107, 33)
        Me.btnAll2.TabIndex = 9
        Me.btnAll2.Text = "All"
        Me.btnAll2.UseVisualStyleBackColor = True
        '
        'btnNone1
        '
        Me.btnNone1.BackColor = System.Drawing.Color.LightCoral
        Me.btnNone1.Location = New System.Drawing.Point(548, 25)
        Me.btnNone1.Name = "btnNone1"
        Me.btnNone1.Size = New System.Drawing.Size(107, 33)
        Me.btnNone1.TabIndex = 1
        Me.btnNone1.Text = "None"
        Me.btnNone1.UseVisualStyleBackColor = False
        '
        'btnSome1
        '
        Me.btnSome1.BackColor = System.Drawing.Color.Orange
        Me.btnSome1.Location = New System.Drawing.Point(661, 25)
        Me.btnSome1.Name = "btnSome1"
        Me.btnSome1.Size = New System.Drawing.Size(107, 33)
        Me.btnSome1.TabIndex = 2
        Me.btnSome1.Text = "Some"
        Me.btnSome1.UseVisualStyleBackColor = False
        '
        'btnMost1
        '
        Me.btnMost1.BackColor = System.Drawing.Color.Yellow
        Me.btnMost1.Location = New System.Drawing.Point(774, 25)
        Me.btnMost1.Name = "btnMost1"
        Me.btnMost1.Size = New System.Drawing.Size(107, 33)
        Me.btnMost1.TabIndex = 3
        Me.btnMost1.Text = "Most"
        Me.btnMost1.UseVisualStyleBackColor = False
        '
        'btnAll1
        '
        Me.btnAll1.BackColor = System.Drawing.Color.LightGreen
        Me.btnAll1.Location = New System.Drawing.Point(887, 25)
        Me.btnAll1.Name = "btnAll1"
        Me.btnAll1.Size = New System.Drawing.Size(107, 33)
        Me.btnAll1.TabIndex = 4
        Me.btnAll1.Text = "All"
        Me.btnAll1.UseVisualStyleBackColor = False
        '
        'btnBreakfast
        '
        Me.btnBreakfast.Location = New System.Drawing.Point(12, 99)
        Me.btnBreakfast.Name = "btnBreakfast"
        Me.btnBreakfast.Size = New System.Drawing.Size(160, 33)
        Me.btnBreakfast.TabIndex = 8
        Me.btnBreakfast.Text = "Breakfast"
        Me.btnBreakfast.UseVisualStyleBackColor = True
        '
        'btnSnack
        '
        Me.btnSnack.Location = New System.Drawing.Point(178, 99)
        Me.btnSnack.Name = "btnSnack"
        Me.btnSnack.Size = New System.Drawing.Size(160, 33)
        Me.btnSnack.TabIndex = 9
        Me.btnSnack.Text = "Snack"
        Me.btnSnack.UseVisualStyleBackColor = True
        '
        'btnLunch
        '
        Me.btnLunch.Location = New System.Drawing.Point(344, 99)
        Me.btnLunch.Name = "btnLunch"
        Me.btnLunch.Size = New System.Drawing.Size(160, 33)
        Me.btnLunch.TabIndex = 10
        Me.btnLunch.Text = "Lunch"
        Me.btnLunch.UseVisualStyleBackColor = True
        '
        'btnTea
        '
        Me.btnTea.Location = New System.Drawing.Point(676, 99)
        Me.btnTea.Name = "btnTea"
        Me.btnTea.Size = New System.Drawing.Size(160, 33)
        Me.btnTea.TabIndex = 12
        Me.btnTea.Text = "Tea"
        Me.btnTea.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Image = CType(resources.GetObject("btnAdd.Image"), System.Drawing.Image)
        Me.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAdd.Location = New System.Drawing.Point(12, 481)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(266, 45)
        Me.btnAdd.TabIndex = 15
        Me.btnAdd.Text = "Add Food Item"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnLunchDessert
        '
        Me.btnLunchDessert.Location = New System.Drawing.Point(510, 99)
        Me.btnLunchDessert.Name = "btnLunchDessert"
        Me.btnLunchDessert.Size = New System.Drawing.Size(160, 33)
        Me.btnLunchDessert.TabIndex = 11
        Me.btnLunchDessert.Text = "Lunch Dessert"
        Me.btnLunchDessert.UseVisualStyleBackColor = True
        '
        'btnTeaDessert
        '
        Me.btnTeaDessert.Location = New System.Drawing.Point(842, 99)
        Me.btnTeaDessert.Name = "btnTeaDessert"
        Me.btnTeaDessert.Size = New System.Drawing.Size(160, 33)
        Me.btnTeaDessert.TabIndex = 13
        Me.btnTeaDessert.Text = "Tea Dessert"
        Me.btnTeaDessert.UseVisualStyleBackColor = True
        '
        'btnRemoveAll
        '
        Me.btnRemoveAll.Image = CType(resources.GetObject("btnRemoveAll.Image"), System.Drawing.Image)
        Me.btnRemoveAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRemoveAll.Location = New System.Drawing.Point(284, 481)
        Me.btnRemoveAll.Name = "btnRemoveAll"
        Me.btnRemoveAll.Size = New System.Drawing.Size(266, 45)
        Me.btnRemoveAll.TabIndex = 17
        Me.btnRemoveAll.Text = "Remove All Food Items"
        Me.btnRemoveAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRemoveAll.UseVisualStyleBackColor = True
        '
        'frmFood
        '
        Me.ClientSize = New System.Drawing.Size(1024, 538)
        Me.Controls.Add(Me.btnRemoveAll)
        Me.Controls.Add(Me.btnTeaDessert)
        Me.Controls.Add(Me.btnLunchDessert)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.btnTea)
        Me.Controls.Add(Me.btnLunch)
        Me.Controls.Add(Me.btnSnack)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnBreakfast)
        Me.Name = "frmFood"
        Me.Text = "Food Register"
        Me.Controls.SetChildIndex(Me.btnBreakfast, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.btnSnack, 0)
        Me.Controls.SetChildIndex(Me.btnLunch, 0)
        Me.Controls.SetChildIndex(Me.btnTea, 0)
        Me.Controls.SetChildIndex(Me.btnAdd, 0)
        Me.Controls.SetChildIndex(Me.btnLunchDessert, 0)
        Me.Controls.SetChildIndex(Me.btnTeaDessert, 0)
        Me.Controls.SetChildIndex(Me.btnRemoveAll, 0)
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnBreakfast As System.Windows.Forms.Button
    Friend WithEvents btnSnack As System.Windows.Forms.Button
    Friend WithEvents btnLunch As System.Windows.Forms.Button
    Friend WithEvents btnTea As System.Windows.Forms.Button
    Friend WithEvents btnNone2 As System.Windows.Forms.Button
    Friend WithEvents btnSome2 As System.Windows.Forms.Button
    Friend WithEvents btnMost2 As System.Windows.Forms.Button
    Friend WithEvents btnAll2 As System.Windows.Forms.Button
    Friend WithEvents btnNone1 As System.Windows.Forms.Button
    Friend WithEvents btnSome1 As System.Windows.Forms.Button
    Friend WithEvents btnMost1 As System.Windows.Forms.Button
    Friend WithEvents btnAll1 As System.Windows.Forms.Button
    Friend WithEvents btnNone8 As System.Windows.Forms.Button
    Friend WithEvents btnSome8 As System.Windows.Forms.Button
    Friend WithEvents btnMost8 As System.Windows.Forms.Button
    Friend WithEvents btnAll8 As System.Windows.Forms.Button
    Friend WithEvents btnNone7 As System.Windows.Forms.Button
    Friend WithEvents btnSome7 As System.Windows.Forms.Button
    Friend WithEvents btnMost7 As System.Windows.Forms.Button
    Friend WithEvents btnAll7 As System.Windows.Forms.Button
    Friend WithEvents btnNone6 As System.Windows.Forms.Button
    Friend WithEvents btnSome6 As System.Windows.Forms.Button
    Friend WithEvents btnMost6 As System.Windows.Forms.Button
    Friend WithEvents btnAll6 As System.Windows.Forms.Button
    Friend WithEvents btnNone5 As System.Windows.Forms.Button
    Friend WithEvents btnSome5 As System.Windows.Forms.Button
    Friend WithEvents btnMost5 As System.Windows.Forms.Button
    Friend WithEvents btnAll5 As System.Windows.Forms.Button
    Friend WithEvents btnNone4 As System.Windows.Forms.Button
    Friend WithEvents btnSome4 As System.Windows.Forms.Button
    Friend WithEvents btnMost4 As System.Windows.Forms.Button
    Friend WithEvents btnAll4 As System.Windows.Forms.Button
    Friend WithEvents btnNone3 As System.Windows.Forms.Button
    Friend WithEvents btnSome3 As System.Windows.Forms.Button
    Friend WithEvents btnMost3 As System.Windows.Forms.Button
    Friend WithEvents btnAll3 As System.Windows.Forms.Button
    Friend WithEvents btnFood8 As System.Windows.Forms.Button
    Friend WithEvents btnFood7 As System.Windows.Forms.Button
    Friend WithEvents btnFood6 As System.Windows.Forms.Button
    Friend WithEvents btnFood5 As System.Windows.Forms.Button
    Friend WithEvents btnFood4 As System.Windows.Forms.Button
    Friend WithEvents btnFood3 As System.Windows.Forms.Button
    Friend WithEvents btnFood2 As System.Windows.Forms.Button
    Friend WithEvents btnFood1 As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents btnLunchDessert As System.Windows.Forms.Button
    Friend WithEvents btnTeaDessert As System.Windows.Forms.Button
    Friend WithEvents btnRemoveAll As System.Windows.Forms.Button

End Class
