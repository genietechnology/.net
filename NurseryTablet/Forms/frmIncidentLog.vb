﻿Imports NurseryTablet.SharedModule
Imports System.Drawing

Public Class frmIncidentLog

    Implements IAddRemoveChildForm

    Public Sub DrillDown(ChildID As String, RecordID As String) Implements IAddRemoveChildForm.DrillDown
        Me.ChildID = ChildID
        Me.RecordID = RecordID
        If Not Me.IsNew Then DisplayRecord()
    End Sub

    Private m_StaffSignature As New Signature
    Private m_ViewingBodyMap As Boolean = True
    Private m_Photo As Image = Nothing
    Private m_BodyMap As Image = Nothing
    Private m_IncidentType = "Accident"
    Private m_Time As Date? = Nothing

    Private Sub frmIncidentLog_Load(sender As Object, e As EventArgs) Handles Me.Load

        SetCMDs()

        If m_BodyMap Is Nothing Then
            m_BodyMap = My.Resources.ResourceManager.GetObject("Body")
            picMap.Image = m_BodyMap
        End If

    End Sub

    Private Sub DisplayRecord()

        If ChildID = "" Then Exit Sub

        Dim _Incident As NurseryGenieData.Incident = Business.Incident.GetIncident(Me.RecordID)
        If _Incident IsNot Nothing Then

            With _Incident

                m_IncidentType = .IncidentType

                m_Time = .Stamp
                fldTime.ValueText = Format(.Stamp, "ddd dd MMM yyyy HH:mm")

                fldLocation.ValueText = .Location
                fldDetails.ValueText = .Details
                fldInjury.ValueText = .Injury
                fldTreatment.ValueText = .Treatment
                fldAction.ValueText = .Action

                fldWitness.FamilyID = Me.FamilyID
                fldWitness.ValueID = .SigWitness.ToString()
                fldWitness.ValueText = Business.Signatures.GetPersonName(.SigWitness)

                m_BodyMap = MediaHandler.ReturnImage(.BodyMap)
                m_Photo = MediaHandler.ReturnImage(.Photo)

                m_ViewingBodyMap = False
                ToggleView()

            End With

        End If

    End Sub

    Protected Overrides Function BeforeCommitUpdate() As Boolean
        If HasActivity() Then
            Return CheckAnswers()
        Else
            Return False
        End If
    End Function

    Private Function HasActivity() As Boolean

        If fldTime.ValueText <> "" Then
            Return True
        End If

        If fldLocation.ValueText <> "" Then
            Return True
        End If

        If fldDetails.ValueText <> "" Then
            Return True
        End If

        If fldInjury.ValueText <> "" Then
            Return True
        End If

        If fldTreatment.ValueText <> "" Then
            Return True
        End If

        If fldAction.ValueText <> "" Then
            Return True
        End If

        If m_StaffSignature.SignatureCaptured Then
            Return True
        End If

        If fldWitness.ValueID <> "" Then
            If fldWitness.SignatureCapture.SignatureCaptured Then
                Return True
            End If
        End If

        Return False

    End Function

    Private Function CheckAnswers() As Boolean

        If fldTime.ValueText = "" Then
            Msgbox("Please enter the time.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If fldLocation.ValueText = "" Then
            Msgbox("Please enter the location.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If fldDetails.ValueText = "" Then
            Msgbox("Please enter what happened.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If fldInjury.ValueText = "" Then
            Msgbox("Please enter the injury suffered.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If fldTreatment.ValueText = "" Then
            Msgbox("Please enter the treatment given.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If fldAction.ValueText = "" Then
            Msgbox("Please enter the action taken.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If Not m_StaffSignature.SignatureCaptured Then
            Msgbox("Please ensure you sign this entry.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If fldWitness.ValueID <> "" Then
            If Not fldWitness.SignatureCapture.SignatureCaptured Then
                Msgbox("Please ensure the witness countersigns.", MessageBoxIcon.Exclamation, "Mandatory Field")
                Return False
            End If
        End If

        Return True

    End Function

    Private Sub Clear()
        fldTime.Clear()
        fldLocation.Clear()
        fldDetails.Clear()
        fldInjury.Clear()
        fldTreatment.Clear()
        fldAction.Clear()
        fldWitness.Clear()
        picMap.Image = My.Resources.ResourceManager.GetObject("Body")
    End Sub

    Protected Overrides Sub CommitUpdate()

        Dim _Incident As NurseryGenieData.Incident = Nothing

        If Me.IsNew Then
            _Incident = New NurseryGenieData.Incident
            _Incident.ID = Guid.NewGuid()
            _Incident.DayId = New Guid(TodayID)
            _Incident.ChildId = New Guid(ChildID)
            _Incident.ChildName = ChildName
        Else
            _Incident = Business.Incident.GetIncident(Me.RecordID)
        End If

        If _Incident IsNot Nothing Then

            With _Incident

                .StaffId = New Guid(scrStaff.ValueID)
                .StaffName = scrStaff.ValueText

                .IncidentType = m_IncidentType
                .Location = fldLocation.ValueText
                .Details = fldDetails.ValueText
                .Injury = fldInjury.ValueText
                .Treatment = fldTreatment.ValueText
                .Action = fldAction.ValueText

                If m_StaffSignature.SignatureCaptured Then
                    If m_StaffSignature.StoreSignature Then
                        .SigStaff = m_StaffSignature.SignatureID
                    End If
                End If

                .Photo = MediaHandler.ReturnByteArray(m_Photo)
                .BodyMap = MediaHandler.ReturnByteArray(m_BodyMap)

                If .SigWitness.HasValue = False AndAlso fldWitness.ValueID <> "" Then
                    If fldWitness.SignatureCapture.SignatureCaptured Then
                        If fldWitness.SignatureCapture.StoreSignature Then
                            .SigWitness = fldWitness.SignatureCapture.SignatureID
                        End If
                    End If
                End If

                If fldTime.ValueText <> "" Then
                    .Stamp = fldTime.ValueDateTime
                Else
                    .Stamp = Nothing
                End If

                If Me.IsNew Then
                    Business.Incident.CreateIncident(_Incident)
                Else
                    Business.Incident.UpdateIncident(_Incident)
                End If

            End With

        End If

    End Sub

    Private Sub SetLayout()

        Select Case m_IncidentType

            Case "Accident"
                fldTime.LabelText = "Time"
                fldTime.ButtonType = Field.EnumButtonType.Time
                fldWitness.Visible = True

            Case "Prior Injury"
                fldTime.LabelText = "Date && Time"
                fldTime.ButtonType = Field.EnumButtonType.DateTime
                fldWitness.Visible = False

            Case "Incident"
                fldTime.LabelText = "Time"
                fldTime.ButtonType = Field.EnumButtonType.Time
                fldWitness.Visible = True

        End Select

    End Sub

    Private Sub SetCMDs()

        If m_ViewingBodyMap Then
            btnToggleView.Text = "View Photo"
            If m_Photo Is Nothing Then
                btnToggleView.Enabled = False
            Else
                btnToggleView.Enabled = True
            End If
        Else
            btnToggleView.Text = "View Body Map"
            btnToggleView.Enabled = True
        End If

    End Sub

    Private Sub btnMark_Click(sender As Object, e As EventArgs) Handles btnMark.Click

        Dim _frmBodyMap As New frmBodyMap
        _frmBodyMap.BodyImage = picMap.Image
        _frmBodyMap.ShowDialog()

        If _frmBodyMap.DialogResult = Windows.Forms.DialogResult.OK Then
            m_BodyMap = _frmBodyMap.BodyImage
            picMap.Image = _frmBodyMap.BodyImage
        End If

        _frmBodyMap.Dispose()
        _frmBodyMap = Nothing

    End Sub

    Private Sub btnPhoto_Click(sender As Object, e As EventArgs) Handles btnPhoto.Click
        Dim _Path As String = MediaHandler.CaptureNow()
        m_Photo = MediaHandler.ReturnImage(_Path)
        If m_Photo IsNot Nothing Then
            m_ViewingBodyMap = True
            ToggleView()
        End If
    End Sub

    Private Sub btnSig_Click(sender As Object, e As EventArgs) Handles btnSig.Click
        m_StaffSignature.CaptureStaffSignature(False)
    End Sub

    Private Sub btnToggleView_Click(sender As Object, e As EventArgs) Handles btnToggleView.Click
        ToggleView()
    End Sub

    Private Sub ToggleView()

        If m_ViewingBodyMap Then
            m_ViewingBodyMap = False
            picMap.Image = m_Photo
        Else
            m_ViewingBodyMap = True
            picMap.Image = m_BodyMap
        End If

        SetCMDs()

    End Sub

    Private Sub btnToggleMode_Click(sender As Object, e As EventArgs) Handles btnToggleMode.Click
        Dim _P As Pair = ReturnButtonSQL("Accident|Prior Injury|Incident", "Please select a Mode")
        If _P IsNot Nothing Then
            m_IncidentType = _P.Text
            Me.Text = "Log " + m_IncidentType
            SetLayout()
        End If
    End Sub

End Class
