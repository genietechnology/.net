﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRequests
    Inherits NurseryTablet.frmBaseChild

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRequests))
        Me.gbxRequest = New System.Windows.Forms.GroupBox()
        Me.btnRequest7 = New System.Windows.Forms.Button()
        Me.btnRequest8 = New System.Windows.Forms.Button()
        Me.btnRequest9 = New System.Windows.Forms.Button()
        Me.btnRequest10 = New System.Windows.Forms.Button()
        Me.btnRequest11 = New System.Windows.Forms.Button()
        Me.btnRequest12 = New System.Windows.Forms.Button()
        Me.btnRequest6 = New System.Windows.Forms.Button()
        Me.btnRequest5 = New System.Windows.Forms.Button()
        Me.btnRequest4 = New System.Windows.Forms.Button()
        Me.btnRequest3 = New System.Windows.Forms.Button()
        Me.btnRequest2 = New System.Windows.Forms.Button()
        Me.btnRequest1 = New System.Windows.Forms.Button()
        Me.lblRequest12 = New System.Windows.Forms.Label()
        Me.lblRequest11 = New System.Windows.Forms.Label()
        Me.lblRequest10 = New System.Windows.Forms.Label()
        Me.lblRequest9 = New System.Windows.Forms.Label()
        Me.lblRequest8 = New System.Windows.Forms.Label()
        Me.lblRequest7 = New System.Windows.Forms.Label()
        Me.lblRequest6 = New System.Windows.Forms.Label()
        Me.lblRequest5 = New System.Windows.Forms.Label()
        Me.lblRequest4 = New System.Windows.Forms.Label()
        Me.lblRequest3 = New System.Windows.Forms.Label()
        Me.lblRequest2 = New System.Windows.Forms.Label()
        Me.lblRequest1 = New System.Windows.Forms.Label()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.gbxRequest.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxRequest
        '
        Me.gbxRequest.Controls.Add(Me.btnRequest7)
        Me.gbxRequest.Controls.Add(Me.btnRequest8)
        Me.gbxRequest.Controls.Add(Me.btnRequest9)
        Me.gbxRequest.Controls.Add(Me.btnRequest10)
        Me.gbxRequest.Controls.Add(Me.btnRequest11)
        Me.gbxRequest.Controls.Add(Me.btnRequest12)
        Me.gbxRequest.Controls.Add(Me.btnRequest6)
        Me.gbxRequest.Controls.Add(Me.btnRequest5)
        Me.gbxRequest.Controls.Add(Me.btnRequest4)
        Me.gbxRequest.Controls.Add(Me.btnRequest3)
        Me.gbxRequest.Controls.Add(Me.btnRequest2)
        Me.gbxRequest.Controls.Add(Me.btnRequest1)
        Me.gbxRequest.Controls.Add(Me.lblRequest12)
        Me.gbxRequest.Controls.Add(Me.lblRequest11)
        Me.gbxRequest.Controls.Add(Me.lblRequest10)
        Me.gbxRequest.Controls.Add(Me.lblRequest9)
        Me.gbxRequest.Controls.Add(Me.lblRequest8)
        Me.gbxRequest.Controls.Add(Me.lblRequest7)
        Me.gbxRequest.Controls.Add(Me.lblRequest6)
        Me.gbxRequest.Controls.Add(Me.lblRequest5)
        Me.gbxRequest.Controls.Add(Me.lblRequest4)
        Me.gbxRequest.Controls.Add(Me.lblRequest3)
        Me.gbxRequest.Controls.Add(Me.lblRequest2)
        Me.gbxRequest.Controls.Add(Me.lblRequest1)
        Me.gbxRequest.Controls.Add(Me.btnAdd)
        Me.gbxRequest.Location = New System.Drawing.Point(17, 90)
        Me.gbxRequest.Name = "gbxRequest"
        Me.gbxRequest.Size = New System.Drawing.Size(995, 382)
        Me.gbxRequest.TabIndex = 10
        Me.gbxRequest.TabStop = False
        '
        'btnRequest7
        '
        Me.btnRequest7.Image = CType(resources.GetObject("btnRequest7.Image"), System.Drawing.Image)
        Me.btnRequest7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRequest7.Location = New System.Drawing.Point(859, 22)
        Me.btnRequest7.Name = "btnRequest7"
        Me.btnRequest7.Size = New System.Drawing.Size(130, 45)
        Me.btnRequest7.TabIndex = 43
        Me.btnRequest7.Text = "Remove"
        Me.btnRequest7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRequest7.UseVisualStyleBackColor = True
        '
        'btnRequest8
        '
        Me.btnRequest8.Image = CType(resources.GetObject("btnRequest8.Image"), System.Drawing.Image)
        Me.btnRequest8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRequest8.Location = New System.Drawing.Point(859, 73)
        Me.btnRequest8.Name = "btnRequest8"
        Me.btnRequest8.Size = New System.Drawing.Size(130, 45)
        Me.btnRequest8.TabIndex = 44
        Me.btnRequest8.Text = "Remove"
        Me.btnRequest8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRequest8.UseVisualStyleBackColor = True
        '
        'btnRequest9
        '
        Me.btnRequest9.Image = CType(resources.GetObject("btnRequest9.Image"), System.Drawing.Image)
        Me.btnRequest9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRequest9.Location = New System.Drawing.Point(859, 124)
        Me.btnRequest9.Name = "btnRequest9"
        Me.btnRequest9.Size = New System.Drawing.Size(130, 45)
        Me.btnRequest9.TabIndex = 45
        Me.btnRequest9.Text = "Remove"
        Me.btnRequest9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRequest9.UseVisualStyleBackColor = True
        '
        'btnRequest10
        '
        Me.btnRequest10.Image = CType(resources.GetObject("btnRequest10.Image"), System.Drawing.Image)
        Me.btnRequest10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRequest10.Location = New System.Drawing.Point(859, 175)
        Me.btnRequest10.Name = "btnRequest10"
        Me.btnRequest10.Size = New System.Drawing.Size(130, 45)
        Me.btnRequest10.TabIndex = 46
        Me.btnRequest10.Text = "Remove"
        Me.btnRequest10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRequest10.UseVisualStyleBackColor = True
        '
        'btnRequest11
        '
        Me.btnRequest11.Image = CType(resources.GetObject("btnRequest11.Image"), System.Drawing.Image)
        Me.btnRequest11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRequest11.Location = New System.Drawing.Point(859, 226)
        Me.btnRequest11.Name = "btnRequest11"
        Me.btnRequest11.Size = New System.Drawing.Size(130, 45)
        Me.btnRequest11.TabIndex = 47
        Me.btnRequest11.Text = "Remove"
        Me.btnRequest11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRequest11.UseVisualStyleBackColor = True
        '
        'btnRequest12
        '
        Me.btnRequest12.Image = CType(resources.GetObject("btnRequest12.Image"), System.Drawing.Image)
        Me.btnRequest12.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRequest12.Location = New System.Drawing.Point(859, 277)
        Me.btnRequest12.Name = "btnRequest12"
        Me.btnRequest12.Size = New System.Drawing.Size(130, 45)
        Me.btnRequest12.TabIndex = 48
        Me.btnRequest12.Text = "Remove"
        Me.btnRequest12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRequest12.UseVisualStyleBackColor = True
        '
        'btnRequest6
        '
        Me.btnRequest6.Image = CType(resources.GetObject("btnRequest6.Image"), System.Drawing.Image)
        Me.btnRequest6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRequest6.Location = New System.Drawing.Point(354, 277)
        Me.btnRequest6.Name = "btnRequest6"
        Me.btnRequest6.Size = New System.Drawing.Size(130, 45)
        Me.btnRequest6.TabIndex = 42
        Me.btnRequest6.Text = "Remove"
        Me.btnRequest6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRequest6.UseVisualStyleBackColor = True
        '
        'btnRequest5
        '
        Me.btnRequest5.Image = CType(resources.GetObject("btnRequest5.Image"), System.Drawing.Image)
        Me.btnRequest5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRequest5.Location = New System.Drawing.Point(354, 226)
        Me.btnRequest5.Name = "btnRequest5"
        Me.btnRequest5.Size = New System.Drawing.Size(130, 45)
        Me.btnRequest5.TabIndex = 41
        Me.btnRequest5.Text = "Remove"
        Me.btnRequest5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRequest5.UseVisualStyleBackColor = True
        '
        'btnRequest4
        '
        Me.btnRequest4.Image = CType(resources.GetObject("btnRequest4.Image"), System.Drawing.Image)
        Me.btnRequest4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRequest4.Location = New System.Drawing.Point(354, 175)
        Me.btnRequest4.Name = "btnRequest4"
        Me.btnRequest4.Size = New System.Drawing.Size(130, 45)
        Me.btnRequest4.TabIndex = 40
        Me.btnRequest4.Text = "Remove"
        Me.btnRequest4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRequest4.UseVisualStyleBackColor = True
        '
        'btnRequest3
        '
        Me.btnRequest3.Image = CType(resources.GetObject("btnRequest3.Image"), System.Drawing.Image)
        Me.btnRequest3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRequest3.Location = New System.Drawing.Point(354, 124)
        Me.btnRequest3.Name = "btnRequest3"
        Me.btnRequest3.Size = New System.Drawing.Size(130, 45)
        Me.btnRequest3.TabIndex = 39
        Me.btnRequest3.Text = "Remove"
        Me.btnRequest3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRequest3.UseVisualStyleBackColor = True
        '
        'btnRequest2
        '
        Me.btnRequest2.Image = CType(resources.GetObject("btnRequest2.Image"), System.Drawing.Image)
        Me.btnRequest2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRequest2.Location = New System.Drawing.Point(354, 73)
        Me.btnRequest2.Name = "btnRequest2"
        Me.btnRequest2.Size = New System.Drawing.Size(130, 45)
        Me.btnRequest2.TabIndex = 38
        Me.btnRequest2.Text = "Remove"
        Me.btnRequest2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRequest2.UseVisualStyleBackColor = True
        '
        'btnRequest1
        '
        Me.btnRequest1.Image = CType(resources.GetObject("btnRequest1.Image"), System.Drawing.Image)
        Me.btnRequest1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRequest1.Location = New System.Drawing.Point(354, 22)
        Me.btnRequest1.Name = "btnRequest1"
        Me.btnRequest1.Size = New System.Drawing.Size(130, 45)
        Me.btnRequest1.TabIndex = 37
        Me.btnRequest1.Text = "Remove"
        Me.btnRequest1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRequest1.UseVisualStyleBackColor = True
        '
        'lblRequest12
        '
        Me.lblRequest12.AutoSize = True
        Me.lblRequest12.Location = New System.Drawing.Point(511, 287)
        Me.lblRequest12.Name = "lblRequest12"
        Me.lblRequest12.Size = New System.Drawing.Size(327, 25)
        Me.lblRequest12.TabIndex = 36
        Me.lblRequest12.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblRequest11
        '
        Me.lblRequest11.AutoSize = True
        Me.lblRequest11.Location = New System.Drawing.Point(511, 236)
        Me.lblRequest11.Name = "lblRequest11"
        Me.lblRequest11.Size = New System.Drawing.Size(327, 25)
        Me.lblRequest11.TabIndex = 35
        Me.lblRequest11.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblRequest10
        '
        Me.lblRequest10.AutoSize = True
        Me.lblRequest10.Location = New System.Drawing.Point(511, 185)
        Me.lblRequest10.Name = "lblRequest10"
        Me.lblRequest10.Size = New System.Drawing.Size(327, 25)
        Me.lblRequest10.TabIndex = 34
        Me.lblRequest10.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblRequest9
        '
        Me.lblRequest9.AutoSize = True
        Me.lblRequest9.Location = New System.Drawing.Point(511, 134)
        Me.lblRequest9.Name = "lblRequest9"
        Me.lblRequest9.Size = New System.Drawing.Size(327, 25)
        Me.lblRequest9.TabIndex = 33
        Me.lblRequest9.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblRequest8
        '
        Me.lblRequest8.AutoSize = True
        Me.lblRequest8.Location = New System.Drawing.Point(511, 83)
        Me.lblRequest8.Name = "lblRequest8"
        Me.lblRequest8.Size = New System.Drawing.Size(327, 25)
        Me.lblRequest8.TabIndex = 32
        Me.lblRequest8.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblRequest7
        '
        Me.lblRequest7.AutoSize = True
        Me.lblRequest7.Location = New System.Drawing.Point(511, 32)
        Me.lblRequest7.Name = "lblRequest7"
        Me.lblRequest7.Size = New System.Drawing.Size(327, 25)
        Me.lblRequest7.TabIndex = 31
        Me.lblRequest7.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblRequest6
        '
        Me.lblRequest6.AutoSize = True
        Me.lblRequest6.Location = New System.Drawing.Point(6, 287)
        Me.lblRequest6.Name = "lblRequest6"
        Me.lblRequest6.Size = New System.Drawing.Size(327, 25)
        Me.lblRequest6.TabIndex = 12
        Me.lblRequest6.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblRequest5
        '
        Me.lblRequest5.AutoSize = True
        Me.lblRequest5.Location = New System.Drawing.Point(6, 236)
        Me.lblRequest5.Name = "lblRequest5"
        Me.lblRequest5.Size = New System.Drawing.Size(327, 25)
        Me.lblRequest5.TabIndex = 11
        Me.lblRequest5.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblRequest4
        '
        Me.lblRequest4.AutoSize = True
        Me.lblRequest4.Location = New System.Drawing.Point(6, 185)
        Me.lblRequest4.Name = "lblRequest4"
        Me.lblRequest4.Size = New System.Drawing.Size(327, 25)
        Me.lblRequest4.TabIndex = 10
        Me.lblRequest4.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblRequest3
        '
        Me.lblRequest3.AutoSize = True
        Me.lblRequest3.Location = New System.Drawing.Point(6, 134)
        Me.lblRequest3.Name = "lblRequest3"
        Me.lblRequest3.Size = New System.Drawing.Size(327, 25)
        Me.lblRequest3.TabIndex = 9
        Me.lblRequest3.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblRequest2
        '
        Me.lblRequest2.AutoSize = True
        Me.lblRequest2.Location = New System.Drawing.Point(6, 83)
        Me.lblRequest2.Name = "lblRequest2"
        Me.lblRequest2.Size = New System.Drawing.Size(327, 25)
        Me.lblRequest2.TabIndex = 8
        Me.lblRequest2.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblRequest1
        '
        Me.lblRequest1.AutoSize = True
        Me.lblRequest1.Location = New System.Drawing.Point(6, 32)
        Me.lblRequest1.Name = "lblRequest1"
        Me.lblRequest1.Size = New System.Drawing.Size(327, 25)
        Me.lblRequest1.TabIndex = 7
        Me.lblRequest1.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'btnAdd
        '
        Me.btnAdd.Image = CType(resources.GetObject("btnAdd.Image"), System.Drawing.Image)
        Me.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAdd.Location = New System.Drawing.Point(6, 328)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(983, 45)
        Me.btnAdd.TabIndex = 1
        Me.btnAdd.Text = "Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'frmRequests
        '
        Me.ClientSize = New System.Drawing.Size(1024, 538)
        Me.Controls.Add(Me.gbxRequest)
        Me.Name = "frmRequests"
        Me.Text = "Request Records"
        Me.Controls.SetChildIndex(Me.gbxRequest, 0)
        Me.gbxRequest.ResumeLayout(False)
        Me.gbxRequest.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbxRequest As System.Windows.Forms.GroupBox
    Friend WithEvents lblRequest6 As System.Windows.Forms.Label
    Friend WithEvents lblRequest5 As System.Windows.Forms.Label
    Friend WithEvents lblRequest4 As System.Windows.Forms.Label
    Friend WithEvents lblRequest3 As System.Windows.Forms.Label
    Friend WithEvents lblRequest2 As System.Windows.Forms.Label
    Friend WithEvents lblRequest1 As System.Windows.Forms.Label
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents lblRequest12 As System.Windows.Forms.Label
    Friend WithEvents lblRequest11 As System.Windows.Forms.Label
    Friend WithEvents lblRequest10 As System.Windows.Forms.Label
    Friend WithEvents lblRequest9 As System.Windows.Forms.Label
    Friend WithEvents lblRequest8 As System.Windows.Forms.Label
    Friend WithEvents lblRequest7 As System.Windows.Forms.Label
    Friend WithEvents btnRequest7 As System.Windows.Forms.Button
    Friend WithEvents btnRequest8 As System.Windows.Forms.Button
    Friend WithEvents btnRequest9 As System.Windows.Forms.Button
    Friend WithEvents btnRequest10 As System.Windows.Forms.Button
    Friend WithEvents btnRequest11 As System.Windows.Forms.Button
    Friend WithEvents btnRequest12 As System.Windows.Forms.Button
    Friend WithEvents btnRequest6 As System.Windows.Forms.Button
    Friend WithEvents btnRequest5 As System.Windows.Forms.Button
    Friend WithEvents btnRequest4 As System.Windows.Forms.Button
    Friend WithEvents btnRequest3 As System.Windows.Forms.Button
    Friend WithEvents btnRequest2 As System.Windows.Forms.Button
    Friend WithEvents btnRequest1 As System.Windows.Forms.Button

End Class
