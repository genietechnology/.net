﻿Option Strict On

Imports DevExpress.XtraBars.Navigation
Imports DevExpress.XtraEditors
Imports NurseryTablet.SharedModule

Public Class frmSleepManager

    Private m_SelectedLocation As String = ""

    Private m_CheckPeriod As Integer = 10
    Private m_CheckWarn As Integer = 8

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub frmSleepManager_Load(sender As Object, e As EventArgs) Handles Me.Load

        PopulateGroups()
        PopulateDashboardTiles()

        timSleepCheck.Interval = 60000

    End Sub

    Private Sub PopulateDashboardTiles()

        Dim _Activity As List(Of NurseryGenieData.Activity) = Business.Activity.ReturnActivity(EnumActivityType.Asleep)
        If _Activity IsNot Nothing Then
            dshAsleep.LabelMiddle = _Activity.Count.ToString
        Else
            dshAsleep.LabelMiddle = "0"
        End If

        DisplayLastCheck()

    End Sub

    Private Sub DisplayLastCheck()

        If dshAsleep.LabelMiddle = "0" Then

            timSleepCheck.Stop()

            dshCheck.LabelMiddle = "N/A"
            dshCheck.Colour = DashboardLabel.EnumColourCode.Green
            btnCheck.Enabled = False

        Else


            Dim _Activity As List(Of NurseryGenieData.Activity) = Business.Activity.ReturnActivity(EnumActivityType.SleepCheck)
            If _Activity IsNot Nothing AndAlso _Activity.Count > 0 Then

                Dim _A As NurseryGenieData.Activity = _Activity.First

                Dim _LastCheck As Date? = _A.Stamp
                If _LastCheck.HasValue Then

                    Dim _Diff As Long = DateDiff(DateInterval.Minute, _LastCheck.Value, Now)
                    dshCheck.LabelMiddle = _Diff.ToString

                    If _Diff >= m_CheckPeriod Then
                        dshCheck.Colour = DashboardLabel.EnumColourCode.Red
                    Else
                        If _Diff >= m_CheckWarn Then
                            dshCheck.Colour = DashboardLabel.EnumColourCode.Amber
                        Else
                            dshCheck.Colour = DashboardLabel.EnumColourCode.Green
                        End If
                    End If

                    btnCheck.Enabled = True
                    timSleepCheck.Start()

                Else
                    dshCheck.Colour = DashboardLabel.EnumColourCode.Green
                    dshCheck.LabelMiddle = "N/A"
                End If

            Else
                dshCheck.Colour = DashboardLabel.EnumColourCode.Green
                dshCheck.LabelMiddle = "N/A"
            End If

        End If

    End Sub

    Private Sub PopulateSleep()

        Dim _Children As List(Of Business.Sleeper) = Nothing

        If radAsleep.Checked Then
            _Children = Business.Child.GetSleepers(m_SelectedLocation, Business.Child.EnumSleepStatus.Asleep)
        Else
            _Children = Business.Child.GetSleepers(m_SelectedLocation, Business.Child.EnumSleepStatus.Awake)
        End If

        tg.Items.Clear()
        btnAsleep.Enabled = False

        If _Children IsNot Nothing Then

            For Each _C As Business.Sleeper In _Children

                Dim _i As New TileBarItem
                With _i

                    .Name = "t_" + _C.ChildID
                    .ItemSize = TileBarItemSize.Wide

                    .TextAlignment = TileItemContentAlignment.TopLeft
                    .AppearanceItem.Normal.ForeColor = Color.Black
                    .AppearanceItem.Normal.BackColor = txtLightYellow.BackColor
                    .AppearanceItem.Selected.BackColor = txtYellow.BackColor
                    .Text = _C.ChildName

                    If radAsleep.Checked Then

                        .Tag = _C.ActivityRecord.ID.ToString

                        Dim _Started As Date = _C.ActivityRecord.Stamp
                        Dim _Diff As Long = DateDiff(DateInterval.Minute, _Started, Now)

                        Dim _SleepStart As New TileItemElement
                        With _SleepStart
                            .TextAlignment = TileItemContentAlignment.BottomLeft
                            .Appearance.Normal.FontSizeDelta = -1
                            .Text = Format(_Started, "HH:mm").ToString
                        End With

                        Dim _MinsAsleep As New TileItemElement
                        With _MinsAsleep
                            .TextAlignment = TileItemContentAlignment.MiddleRight
                            .Appearance.Normal.FontSizeDelta = 28
                            .Text = _Diff.ToString
                        End With

                        _i.Elements.Add(_MinsAsleep)
                        _i.Elements.Add(_SleepStart)

                    End If

                    AddHandler _i.ItemClick, AddressOf ChildClick

                End With

                tg.Items.Add(_i)

            Next

            If _Children.Count > 0 Then
                btnAsleep.Enabled = True
                If radAsleep.Checked Then
                    btnAsleep.Text = "Child Awake"
                Else
                    btnAsleep.Text = "Child Asleep"
                End If
            End If

        End If

    End Sub

    Private Sub PerformSleepCheck()

        Dim _StaffID As String = ""
        Dim _StaffName As String = ""

        If Parameters.CurrentStaffID <> "" Then
            _StaffID = Parameters.CurrentStaffID
            _StaffName = Parameters.CurrentStaffName
        Else
            Dim _Staff As Pair = Business.Staff.FindStaff(SharedModule.Enums.PersonMode.OnlyCheckedIn)
            If _Staff IsNot Nothing Then
                _StaffID = _Staff.Code
                _StaffName = _Staff.Text
            End If
        End If

        If _StaffID <> "" AndAlso _StaffName <> "" Then
            StoreSleepCheck(_StaffID, _StaffName)
        End If

    End Sub

    Private Sub StoreSleepCheck(ByVal StaffID As String, ByVal StaffName As String)

        Me.Cursor = Cursors.WaitCursor
        btnCheck.Enabled = False
        Application.DoEvents()


        Dim _Sleepers As List(Of Business.Sleeper) = Nothing
        _Sleepers = Business.Child.GetSleepers(m_SelectedLocation, Business.Child.EnumSleepStatus.Asleep)

        If _Sleepers IsNot Nothing Then
            For Each _s As Business.Sleeper In _Sleepers
                SharedModule.LogSleepCheck(Parameters.TodayID, _s.ChildID, _s.ChildName, "", StaffID, StaffName)
            Next
        End If

        PopulateDashboardTiles()

        Me.Cursor = Cursors.Default
        Application.DoEvents()

    End Sub

    Private Sub PopulateGroups()

        PopulateTileBar(Business.Rooms.GetRoomsAsPair)

        If Parameters.DefaultRoom <> "" Then
            For Each _i As TileBarItem In tbRoomGroup.Items
                Dim _Text As String = _i.Elements(0).Text
                If _Text = Parameters.DefaultRoom Then
                    tbRooms.SelectedItem = _i
                    _i.PerformItemClick()
                    Exit Sub
                End If
            Next
        Else
            For Each _i As TileBarItem In tbRoomGroup.Items
                tbRooms.SelectedItem = _i
                _i.PerformItemClick()
                Exit Sub
            Next
        End If

    End Sub

    Private Sub PopulateTileBar(ByVal Pairs As List(Of Pair))

        tbRoomGroup.Items.Clear()

        For Each _P In Pairs

            Dim _i As New TileBarItem
            With _i

                .Name = "t_" + _P.Code

                AddHandler _i.ItemClick, AddressOf GroupItemClick

                .TextAlignment = TileItemContentAlignment.TopLeft
                .AppearanceItem.Normal.ForeColor = Color.Black
                .AppearanceItem.Normal.BackColor = txtLightYellow.BackColor
                .AppearanceItem.Selected.BackColor = txtYellow.BackColor
                .Text = _P.Text

            End With

            tbRoomGroup.Items.Add(_i)

        Next

    End Sub

    Private Sub ChildClick(sender As Object, e As TileItemEventArgs)
        If e.Item.Checked Then
            e.Item.Checked = False
        Else
            e.Item.Checked = True
        End If
    End Sub

    Private Sub GroupItemClick(sender As Object, e As TileItemEventArgs)
        m_SelectedLocation = e.Item.Text
        PopulateSleep()
    End Sub

    Private Sub frmSleepManager_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        tbRooms.WideTileWidth = tbRooms.Width - 40
    End Sub

    Private Sub dshCheck_DoubleClick(sender As Object, e As DashboardLabel.DashboardEventArgs) Handles dshCheck.DoubleClick
        PerformSleepCheck()
    End Sub

    Private Sub radAsleep_CheckedChanged(sender As Object, e As EventArgs) Handles radAsleep.CheckedChanged
        If radAsleep.Checked Then PopulateSleep()
    End Sub

    Private Sub radAwake_CheckedChanged(sender As Object, e As EventArgs) Handles radAwake.CheckedChanged
        If radAwake.Checked Then PopulateSleep()
    End Sub

    Private Sub timSleepCheck_Tick(sender As Object, e As EventArgs) Handles timSleepCheck.Tick

        Me.Cursor = Cursors.WaitCursor
        Application.DoEvents()

        PopulateDashboardTiles()
        PopulateSleep()

        Me.Cursor = Cursors.Default
        Application.DoEvents()

    End Sub

    Private Function ItemsSelected() As Boolean

        For Each _i As TileBarItem In tg.Items
            If _i.Checked Then Return True
        Next

        Return False

    End Function

    Private Sub btnAsleep_Click(sender As Object, e As EventArgs) Handles btnAsleep.Click

        If Not ItemsSelected() Then
            Msgbox("Please select at least one Child.", MessageBoxIcon.Exclamation, "Item Check")
            Exit Sub
        End If

        Dim _StaffID As String = ""
        Dim _StaffName As String = ""

        If Parameters.CurrentStaffID <> "" Then
            _StaffID = Parameters.CurrentStaffID
            _StaffName = Parameters.CurrentStaffName
        Else
            Dim _Staff As Pair = Business.Staff.FindStaff(SharedModule.Enums.PersonMode.OnlyCheckedIn)
            If _Staff IsNot Nothing Then
                _StaffID = _Staff.Code
                _StaffName = _Staff.Text
            End If
        End If

        If _StaffID <> "" AndAlso _StaffName <> "" Then

            btnAsleep.Enabled = False

            Me.Cursor = Cursors.WaitCursor
            Application.DoEvents()

            For Each _i As TileBarItem In tg.Items

                If _i.Checked Then

                    Dim _ChildID As String = _i.Name.Substring(2)
                    Dim _ChildName As String = _i.Elements(0).Text

                    If radAsleep.Checked Then
                        Dim _ID As String = _i.Tag.ToString
                        SharedModule.ConvertToSleep(_ID, _StaffID, _StaffName)
                    Else
                        SharedModule.LogAsleep(Parameters.TodayID, _ChildID, _ChildName, m_SelectedLocation, _StaffID, _StaffName)
                    End If

                End If

            Next

            StoreSleepCheck(_StaffID, _StaffName)

            PopulateSleep()
            PopulateDashboardTiles()

            Me.Cursor = Cursors.Default
            Application.DoEvents()

        End If

    End Sub

    Private Sub btnCheck_Click(sender As Object, e As EventArgs) Handles btnCheck.Click
        PerformSleepCheck()
    End Sub
End Class