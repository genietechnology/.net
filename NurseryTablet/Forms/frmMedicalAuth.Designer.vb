﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMedicalAuth
    Inherits NurseryTablet.frmBaseChildAddRemove

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMedicalAuth))
        Me.btnSigStaff = New System.Windows.Forms.Button()
        Me.btnSigContact = New System.Windows.Forms.Button()
        Me.lblMilk1 = New System.Windows.Forms.Label()
        Me.lblMilk2 = New System.Windows.Forms.Label()
        Me.lblMilk3 = New System.Windows.Forms.Label()
        Me.lblMilk4 = New System.Windows.Forms.Label()
        Me.lblMilk5 = New System.Windows.Forms.Label()
        Me.btnLastParent = New System.Windows.Forms.Button()
        Me.lblContact = New System.Windows.Forms.Label()
        Me.btnDosage = New System.Windows.Forms.Button()
        Me.lblNature = New System.Windows.Forms.Label()
        Me.btnMedicine = New System.Windows.Forms.Button()
        Me.lblMedicine = New System.Windows.Forms.Label()
        Me.btnNature = New System.Windows.Forms.Button()
        Me.lblDosage = New System.Windows.Forms.Label()
        Me.btnContact = New System.Windows.Forms.Button()
        Me.lblLastParent = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblFrequency = New System.Windows.Forms.Label()
        Me.btnFrequency = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblDosagesDue = New System.Windows.Forms.Label()
        Me.gbxMilk = New System.Windows.Forms.GroupBox()
        Me.btnAsRequired = New System.Windows.Forms.Button()
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panBottom.SuspendLayout()
        Me.gbxMilk.SuspendLayout()
        Me.SuspendLayout()
        '
        'panBottom
        '
        Me.panBottom.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panBottom.Appearance.Options.UseFont = True
        Me.panBottom.Controls.Add(Me.btnSigStaff)
        Me.panBottom.Controls.Add(Me.btnSigContact)
        Me.panBottom.Controls.SetChildIndex(Me.btnSigContact, 0)
        Me.panBottom.Controls.SetChildIndex(Me.btnSigStaff, 0)
        '
        'btnSigStaff
        '
        Me.btnSigStaff.Image = CType(resources.GetObject("btnSigStaff.Image"), System.Drawing.Image)
        Me.btnSigStaff.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSigStaff.Location = New System.Drawing.Point(17, 3)
        Me.btnSigStaff.Name = "btnSigStaff"
        Me.btnSigStaff.Size = New System.Drawing.Size(200, 45)
        Me.btnSigStaff.TabIndex = 31
        Me.btnSigStaff.Text = "Staff Signature"
        Me.btnSigStaff.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSigStaff.UseVisualStyleBackColor = True
        '
        'btnSigContact
        '
        Me.btnSigContact.Image = CType(resources.GetObject("btnSigContact.Image"), System.Drawing.Image)
        Me.btnSigContact.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSigContact.Location = New System.Drawing.Point(223, 3)
        Me.btnSigContact.Name = "btnSigContact"
        Me.btnSigContact.Size = New System.Drawing.Size(200, 45)
        Me.btnSigContact.TabIndex = 42
        Me.btnSigContact.Text = "Contact Signature"
        Me.btnSigContact.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSigContact.UseVisualStyleBackColor = True
        '
        'lblMilk1
        '
        Me.lblMilk1.AutoSize = True
        Me.lblMilk1.Location = New System.Drawing.Point(6, 32)
        Me.lblMilk1.Name = "lblMilk1"
        Me.lblMilk1.Size = New System.Drawing.Size(77, 25)
        Me.lblMilk1.TabIndex = 7
        Me.lblMilk1.Text = "Contact"
        '
        'lblMilk2
        '
        Me.lblMilk2.AutoSize = True
        Me.lblMilk2.Location = New System.Drawing.Point(6, 83)
        Me.lblMilk2.Name = "lblMilk2"
        Me.lblMilk2.Size = New System.Drawing.Size(149, 25)
        Me.lblMilk2.TabIndex = 8
        Me.lblMilk2.Text = "Nature of Illness"
        '
        'lblMilk3
        '
        Me.lblMilk3.AutoSize = True
        Me.lblMilk3.Location = New System.Drawing.Point(6, 134)
        Me.lblMilk3.Name = "lblMilk3"
        Me.lblMilk3.Size = New System.Drawing.Size(189, 25)
        Me.lblMilk3.TabIndex = 9
        Me.lblMilk3.Text = "Medicine to be given"
        '
        'lblMilk4
        '
        Me.lblMilk4.AutoSize = True
        Me.lblMilk4.Location = New System.Drawing.Point(6, 185)
        Me.lblMilk4.Name = "lblMilk4"
        Me.lblMilk4.Size = New System.Drawing.Size(75, 25)
        Me.lblMilk4.TabIndex = 10
        Me.lblMilk4.Text = "Dosage"
        '
        'lblMilk5
        '
        Me.lblMilk5.AutoSize = True
        Me.lblMilk5.Location = New System.Drawing.Point(6, 236)
        Me.lblMilk5.Name = "lblMilk5"
        Me.lblMilk5.Size = New System.Drawing.Size(246, 25)
        Me.lblMilk5.TabIndex = 11
        Me.lblMilk5.Text = "Last Administered by Parent"
        '
        'btnLastParent
        '
        Me.btnLastParent.Image = CType(resources.GetObject("btnLastParent.Image"), System.Drawing.Image)
        Me.btnLastParent.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLastParent.Location = New System.Drawing.Point(859, 226)
        Me.btnLastParent.Name = "btnLastParent"
        Me.btnLastParent.Size = New System.Drawing.Size(130, 45)
        Me.btnLastParent.TabIndex = 29
        Me.btnLastParent.Text = "Select"
        Me.btnLastParent.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLastParent.UseVisualStyleBackColor = True
        '
        'lblContact
        '
        Me.lblContact.AutoSize = True
        Me.lblContact.Location = New System.Drawing.Point(270, 32)
        Me.lblContact.Name = "lblContact"
        Me.lblContact.Size = New System.Drawing.Size(579, 25)
        Me.lblContact.TabIndex = 31
        Me.lblContact.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'btnDosage
        '
        Me.btnDosage.Image = CType(resources.GetObject("btnDosage.Image"), System.Drawing.Image)
        Me.btnDosage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDosage.Location = New System.Drawing.Point(859, 175)
        Me.btnDosage.Name = "btnDosage"
        Me.btnDosage.Size = New System.Drawing.Size(130, 45)
        Me.btnDosage.TabIndex = 28
        Me.btnDosage.Text = "Enter"
        Me.btnDosage.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDosage.UseVisualStyleBackColor = True
        '
        'lblNature
        '
        Me.lblNature.AutoSize = True
        Me.lblNature.Location = New System.Drawing.Point(270, 83)
        Me.lblNature.Name = "lblNature"
        Me.lblNature.Size = New System.Drawing.Size(579, 25)
        Me.lblNature.TabIndex = 32
        Me.lblNature.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'btnMedicine
        '
        Me.btnMedicine.Image = CType(resources.GetObject("btnMedicine.Image"), System.Drawing.Image)
        Me.btnMedicine.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMedicine.Location = New System.Drawing.Point(859, 124)
        Me.btnMedicine.Name = "btnMedicine"
        Me.btnMedicine.Size = New System.Drawing.Size(130, 45)
        Me.btnMedicine.TabIndex = 27
        Me.btnMedicine.Text = "Enter"
        Me.btnMedicine.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMedicine.UseVisualStyleBackColor = True
        '
        'lblMedicine
        '
        Me.lblMedicine.AutoSize = True
        Me.lblMedicine.Location = New System.Drawing.Point(270, 134)
        Me.lblMedicine.Name = "lblMedicine"
        Me.lblMedicine.Size = New System.Drawing.Size(579, 25)
        Me.lblMedicine.TabIndex = 33
        Me.lblMedicine.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'btnNature
        '
        Me.btnNature.Image = CType(resources.GetObject("btnNature.Image"), System.Drawing.Image)
        Me.btnNature.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNature.Location = New System.Drawing.Point(859, 73)
        Me.btnNature.Name = "btnNature"
        Me.btnNature.Size = New System.Drawing.Size(130, 45)
        Me.btnNature.TabIndex = 26
        Me.btnNature.Text = "Enter"
        Me.btnNature.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNature.UseVisualStyleBackColor = True
        '
        'lblDosage
        '
        Me.lblDosage.AutoSize = True
        Me.lblDosage.Location = New System.Drawing.Point(270, 185)
        Me.lblDosage.Name = "lblDosage"
        Me.lblDosage.Size = New System.Drawing.Size(579, 25)
        Me.lblDosage.TabIndex = 34
        Me.lblDosage.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'btnContact
        '
        Me.btnContact.Image = CType(resources.GetObject("btnContact.Image"), System.Drawing.Image)
        Me.btnContact.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnContact.Location = New System.Drawing.Point(859, 22)
        Me.btnContact.Name = "btnContact"
        Me.btnContact.Size = New System.Drawing.Size(130, 45)
        Me.btnContact.TabIndex = 25
        Me.btnContact.Text = "Select"
        Me.btnContact.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnContact.UseVisualStyleBackColor = True
        '
        'lblLastParent
        '
        Me.lblLastParent.AutoSize = True
        Me.lblLastParent.Location = New System.Drawing.Point(270, 236)
        Me.lblLastParent.Name = "lblLastParent"
        Me.lblLastParent.Size = New System.Drawing.Size(579, 25)
        Me.lblLastParent.TabIndex = 35
        Me.lblLastParent.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 287)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(164, 25)
        Me.Label3.TabIndex = 37
        Me.Label3.Text = "Frequency (hours)"
        '
        'lblFrequency
        '
        Me.lblFrequency.AutoSize = True
        Me.lblFrequency.Location = New System.Drawing.Point(270, 287)
        Me.lblFrequency.Name = "lblFrequency"
        Me.lblFrequency.Size = New System.Drawing.Size(435, 25)
        Me.lblFrequency.TabIndex = 39
        Me.lblFrequency.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'btnFrequency
        '
        Me.btnFrequency.Image = CType(resources.GetObject("btnFrequency.Image"), System.Drawing.Image)
        Me.btnFrequency.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnFrequency.Location = New System.Drawing.Point(859, 277)
        Me.btnFrequency.Name = "btnFrequency"
        Me.btnFrequency.Size = New System.Drawing.Size(130, 45)
        Me.btnFrequency.TabIndex = 41
        Me.btnFrequency.Text = "Enter"
        Me.btnFrequency.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnFrequency.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 338)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(122, 25)
        Me.Label5.TabIndex = 42
        Me.Label5.Text = "Dosages Due"
        '
        'lblDosagesDue
        '
        Me.lblDosagesDue.AutoSize = True
        Me.lblDosagesDue.Location = New System.Drawing.Point(270, 338)
        Me.lblDosagesDue.Name = "lblDosagesDue"
        Me.lblDosagesDue.Size = New System.Drawing.Size(579, 25)
        Me.lblDosagesDue.TabIndex = 43
        Me.lblDosagesDue.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'gbxMilk
        '
        Me.gbxMilk.Controls.Add(Me.btnAsRequired)
        Me.gbxMilk.Controls.Add(Me.lblDosagesDue)
        Me.gbxMilk.Controls.Add(Me.Label5)
        Me.gbxMilk.Controls.Add(Me.btnFrequency)
        Me.gbxMilk.Controls.Add(Me.lblFrequency)
        Me.gbxMilk.Controls.Add(Me.Label3)
        Me.gbxMilk.Controls.Add(Me.lblLastParent)
        Me.gbxMilk.Controls.Add(Me.btnContact)
        Me.gbxMilk.Controls.Add(Me.lblDosage)
        Me.gbxMilk.Controls.Add(Me.btnNature)
        Me.gbxMilk.Controls.Add(Me.lblMedicine)
        Me.gbxMilk.Controls.Add(Me.btnMedicine)
        Me.gbxMilk.Controls.Add(Me.lblNature)
        Me.gbxMilk.Controls.Add(Me.btnDosage)
        Me.gbxMilk.Controls.Add(Me.lblContact)
        Me.gbxMilk.Controls.Add(Me.btnLastParent)
        Me.gbxMilk.Controls.Add(Me.lblMilk5)
        Me.gbxMilk.Controls.Add(Me.lblMilk4)
        Me.gbxMilk.Controls.Add(Me.lblMilk3)
        Me.gbxMilk.Controls.Add(Me.lblMilk2)
        Me.gbxMilk.Controls.Add(Me.lblMilk1)
        Me.gbxMilk.Location = New System.Drawing.Point(17, 90)
        Me.gbxMilk.Name = "gbxMilk"
        Me.gbxMilk.Size = New System.Drawing.Size(995, 382)
        Me.gbxMilk.TabIndex = 11
        Me.gbxMilk.TabStop = False
        '
        'btnAsRequired
        '
        Me.btnAsRequired.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAsRequired.Location = New System.Drawing.Point(719, 277)
        Me.btnAsRequired.Name = "btnAsRequired"
        Me.btnAsRequired.Size = New System.Drawing.Size(130, 45)
        Me.btnAsRequired.TabIndex = 44
        Me.btnAsRequired.Text = "As Required"
        Me.btnAsRequired.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAsRequired.UseVisualStyleBackColor = True
        '
        'frmMedicalAuth
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(1024, 538)
        Me.Controls.Add(Me.gbxMilk)
        Me.Name = "frmMedicalAuth"
        Me.Text = "Medicine Authorisation"
        Me.Controls.SetChildIndex(Me.panBottom, 0)
        Me.Controls.SetChildIndex(Me.gbxMilk, 0)
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panBottom.ResumeLayout(False)
        Me.gbxMilk.ResumeLayout(False)
        Me.gbxMilk.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblNextDue As System.Windows.Forms.Label
    Friend WithEvents btnNextDue As System.Windows.Forms.Button
    Friend WithEvents lblMilk6 As System.Windows.Forms.Label
    Friend WithEvents btnSigStaff As System.Windows.Forms.Button
    Friend WithEvents btnSigContact As System.Windows.Forms.Button
    Friend WithEvents lblMilk1 As System.Windows.Forms.Label
    Friend WithEvents lblMilk2 As System.Windows.Forms.Label
    Friend WithEvents lblMilk3 As System.Windows.Forms.Label
    Friend WithEvents lblMilk4 As System.Windows.Forms.Label
    Friend WithEvents lblMilk5 As System.Windows.Forms.Label
    Friend WithEvents btnLastParent As System.Windows.Forms.Button
    Friend WithEvents lblContact As System.Windows.Forms.Label
    Friend WithEvents btnDosage As System.Windows.Forms.Button
    Friend WithEvents lblNature As System.Windows.Forms.Label
    Friend WithEvents btnMedicine As System.Windows.Forms.Button
    Friend WithEvents lblMedicine As System.Windows.Forms.Label
    Friend WithEvents btnNature As System.Windows.Forms.Button
    Friend WithEvents lblDosage As System.Windows.Forms.Label
    Friend WithEvents btnContact As System.Windows.Forms.Button
    Friend WithEvents lblLastParent As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblFrequency As System.Windows.Forms.Label
    Friend WithEvents btnFrequency As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblDosagesDue As System.Windows.Forms.Label
    Friend WithEvents gbxMilk As System.Windows.Forms.GroupBox
    Friend WithEvents btnAsRequired As System.Windows.Forms.Button

End Class
