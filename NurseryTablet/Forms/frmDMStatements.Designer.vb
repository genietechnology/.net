﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDMStatements
    Inherits frmBaseNew

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDMStatements))
        Me.ic = New DevExpress.Utils.ImageCollection(Me.components)
        Me.tbAge = New DevExpress.XtraBars.Navigation.TileBar()
        Me.panBottom = New DevExpress.XtraEditors.PanelControl()
        Me.txtCyan = New DevExpress.XtraEditors.TextEdit()
        Me.txtGreen = New DevExpress.XtraEditors.TextEdit()
        Me.txtYellow = New DevExpress.XtraEditors.TextEdit()
        Me.txtOrange = New DevExpress.XtraEditors.TextEdit()
        Me.txtRed = New DevExpress.XtraEditors.TextEdit()
        Me.btnAccept = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.icAges = New DevExpress.Utils.ImageCollection(Me.components)
        Me.panTop = New DevExpress.XtraEditors.PanelControl()
        Me.tbArea = New DevExpress.XtraBars.Navigation.TileBar()
        Me.tbTop = New DevExpress.XtraBars.Navigation.TileBar()
        Me.tbAreaGroup = New DevExpress.XtraBars.Navigation.TileBarGroup()
        Me.tbLeft = New DevExpress.XtraBars.Navigation.TileBar()
        Me.tbAgeGroup = New DevExpress.XtraBars.Navigation.TileBarGroup()
        Me.tbItems = New DevExpress.XtraBars.Navigation.TileBar()
        Me.tbItemsGroup = New DevExpress.XtraBars.Navigation.TileBarGroup()
        Me.lblSelected = New DevExpress.XtraEditors.LabelControl()
        CType(Me.ic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panBottom.SuspendLayout()
        CType(Me.txtCyan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOrange.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.icAges, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.panTop, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ic
        '
        Me.ic.ImageSize = New System.Drawing.Size(32, 32)
        Me.ic.ImageStream = CType(resources.GetObject("ic.ImageStream"), DevExpress.Utils.ImageCollectionStreamer)
        Me.ic.InsertGalleryImage("add", "images/actions/add_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_32x32.png"), 0)
        Me.ic.Images.SetKeyName(0, "add")
        Me.ic.InsertGalleryImage("remove", "images/actions/remove_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/remove_32x32.png"), 1)
        Me.ic.Images.SetKeyName(1, "remove")
        '
        'tbAge
        '
        Me.tbAge.AllowDrag = False
        Me.tbAge.Dock = System.Windows.Forms.DockStyle.Left
        Me.tbAge.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        Me.tbAge.Location = New System.Drawing.Point(0, 0)
        Me.tbAge.Name = "tbAge"
        Me.tbAge.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollButtons
        Me.tbAge.Size = New System.Drawing.Size(240, 150)
        Me.tbAge.TabIndex = 0
        '
        'panBottom
        '
        Me.panBottom.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panBottom.Appearance.Options.UseFont = True
        Me.panBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.panBottom.Controls.Add(Me.txtCyan)
        Me.panBottom.Controls.Add(Me.txtGreen)
        Me.panBottom.Controls.Add(Me.txtYellow)
        Me.panBottom.Controls.Add(Me.txtOrange)
        Me.panBottom.Controls.Add(Me.txtRed)
        Me.panBottom.Controls.Add(Me.btnAccept)
        Me.panBottom.Controls.Add(Me.btnCancel)
        Me.panBottom.Controls.Add(Me.lblSelected)
        Me.panBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panBottom.Location = New System.Drawing.Point(0, 454)
        Me.panBottom.Name = "panBottom"
        Me.panBottom.Size = New System.Drawing.Size(676, 56)
        Me.panBottom.TabIndex = 9
        '
        'txtCyan
        '
        Me.txtCyan.Location = New System.Drawing.Point(329, 6)
        Me.txtCyan.Name = "txtCyan"
        Me.txtCyan.Properties.Appearance.BackColor = System.Drawing.Color.Blue
        Me.txtCyan.Properties.Appearance.Options.UseBackColor = True
        Me.txtCyan.Size = New System.Drawing.Size(23, 20)
        Me.txtCyan.TabIndex = 15
        Me.txtCyan.Visible = False
        '
        'txtGreen
        '
        Me.txtGreen.Location = New System.Drawing.Point(300, 6)
        Me.txtGreen.Name = "txtGreen"
        Me.txtGreen.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtGreen.Properties.Appearance.Options.UseBackColor = True
        Me.txtGreen.Size = New System.Drawing.Size(23, 20)
        Me.txtGreen.TabIndex = 14
        Me.txtGreen.Visible = False
        '
        'txtYellow
        '
        Me.txtYellow.Location = New System.Drawing.Point(271, 6)
        Me.txtYellow.Name = "txtYellow"
        Me.txtYellow.Properties.Appearance.BackColor = System.Drawing.Color.Gold
        Me.txtYellow.Properties.Appearance.Options.UseBackColor = True
        Me.txtYellow.Size = New System.Drawing.Size(23, 20)
        Me.txtYellow.TabIndex = 13
        Me.txtYellow.Visible = False
        '
        'txtOrange
        '
        Me.txtOrange.Location = New System.Drawing.Point(242, 6)
        Me.txtOrange.Name = "txtOrange"
        Me.txtOrange.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtOrange.Properties.Appearance.Options.UseBackColor = True
        Me.txtOrange.Size = New System.Drawing.Size(23, 20)
        Me.txtOrange.TabIndex = 12
        Me.txtOrange.Visible = False
        '
        'txtRed
        '
        Me.txtRed.Location = New System.Drawing.Point(213, 6)
        Me.txtRed.Name = "txtRed"
        Me.txtRed.Properties.Appearance.BackColor = System.Drawing.Color.Red
        Me.txtRed.Properties.Appearance.Options.UseBackColor = True
        Me.txtRed.Size = New System.Drawing.Size(23, 20)
        Me.txtRed.TabIndex = 11
        Me.txtRed.Visible = False
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAccept.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAccept.Appearance.Options.UseFont = True
        Me.btnAccept.Image = Global.NurseryTablet.My.Resources.Resources.success_32
        Me.btnAccept.Location = New System.Drawing.Point(358, 2)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(150, 45)
        Me.btnAccept.TabIndex = 0
        Me.btnAccept.Text = "Accept"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Image = Global.NurseryTablet.My.Resources.Resources.cancel_32
        Me.btnCancel.Location = New System.Drawing.Point(514, 2)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(150, 45)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "Cancel"
        '
        'icAges
        '
        Me.icAges.ImageSize = New System.Drawing.Size(175, 175)
        Me.icAges.ImageStream = CType(resources.GetObject("icAges.ImageStream"), DevExpress.Utils.ImageCollectionStreamer)
        Me.icAges.Images.SetKeyName(0, "11")
        Me.icAges.Images.SetKeyName(1, "20")
        Me.icAges.Images.SetKeyName(2, "26")
        Me.icAges.Images.SetKeyName(3, "36")
        Me.icAges.Images.SetKeyName(4, "50")
        Me.icAges.Images.SetKeyName(5, "60")
        '
        'panTop
        '
        Me.panTop.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panTop.Appearance.Options.UseFont = True
        Me.panTop.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.panTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.panTop.Location = New System.Drawing.Point(0, 0)
        Me.panTop.Name = "panTop"
        Me.panTop.Size = New System.Drawing.Size(676, 56)
        Me.panTop.TabIndex = 14
        '
        'tbArea
        '
        Me.tbArea.AllowDrag = False
        Me.tbArea.AppearanceText.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbArea.AppearanceText.Options.UseFont = True
        Me.tbArea.Dock = System.Windows.Forms.DockStyle.Top
        Me.tbArea.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        Me.tbArea.Location = New System.Drawing.Point(0, 0)
        Me.tbArea.Name = "tbArea"
        Me.tbArea.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollButtons
        Me.tbArea.Size = New System.Drawing.Size(240, 150)
        Me.tbArea.TabIndex = 0
        '
        'tbTop
        '
        Me.tbTop.AllowDrag = False
        Me.tbTop.AppearanceText.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbTop.AppearanceText.Options.UseFont = True
        Me.tbTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.tbTop.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        Me.tbTop.Groups.Add(Me.tbAreaGroup)
        Me.tbTop.ItemSize = 80
        Me.tbTop.Location = New System.Drawing.Point(0, 56)
        Me.tbTop.MaxId = 9
        Me.tbTop.Name = "tbTop"
        Me.tbTop.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.None
        Me.tbTop.ShowGroupText = False
        Me.tbTop.Size = New System.Drawing.Size(676, 100)
        Me.tbTop.TabIndex = 15
        Me.tbTop.Text = "TileBar1"
        Me.tbTop.WideTileWidth = 200
        '
        'tbAreaGroup
        '
        Me.tbAreaGroup.Name = "tbAreaGroup"
        '
        'tbLeft
        '
        Me.tbLeft.AllowDrag = False
        Me.tbLeft.AppearanceText.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbLeft.AppearanceText.Options.UseFont = True
        Me.tbLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.tbLeft.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        Me.tbLeft.Groups.Add(Me.tbAgeGroup)
        Me.tbLeft.ItemSize = 80
        Me.tbLeft.Location = New System.Drawing.Point(0, 156)
        Me.tbLeft.MaxId = 10
        Me.tbLeft.Name = "tbLeft"
        Me.tbLeft.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.tbLeft.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.None
        Me.tbLeft.ShowGroupText = False
        Me.tbLeft.Size = New System.Drawing.Size(223, 298)
        Me.tbLeft.TabIndex = 16
        Me.tbLeft.Text = "tbAge"
        Me.tbLeft.WideTileWidth = 200
        '
        'tbAgeGroup
        '
        Me.tbAgeGroup.Name = "tbAgeGroup"
        '
        'tbItems
        '
        Me.tbItems.AllowDrag = False
        Me.tbItems.AppearanceText.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbItems.AppearanceText.Options.UseFont = True
        Me.tbItems.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbItems.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        Me.tbItems.Groups.Add(Me.tbItemsGroup)
        Me.tbItems.ItemSize = 80
        Me.tbItems.Location = New System.Drawing.Point(223, 156)
        Me.tbItems.MaxId = 9
        Me.tbItems.Name = "tbItems"
        Me.tbItems.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.tbItems.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.None
        Me.tbItems.ShowGroupText = False
        Me.tbItems.Size = New System.Drawing.Size(453, 298)
        Me.tbItems.TabIndex = 17
        Me.tbItems.Text = "TileBar2"
        Me.tbItems.WideTileWidth = 200
        '
        'tbItemsGroup
        '
        Me.tbItemsGroup.Name = "tbItemsGroup"
        '
        'lblSelected
        '
        Me.lblSelected.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelected.Location = New System.Drawing.Point(12, 13)
        Me.lblSelected.Name = "lblSelected"
        Me.lblSelected.Size = New System.Drawing.Size(149, 25)
        Me.lblSelected.TabIndex = 16
        Me.lblSelected.Text = "No items selected"
        '
        'frmDMStatements
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(11.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(676, 510)
        Me.Controls.Add(Me.tbItems)
        Me.Controls.Add(Me.tbLeft)
        Me.Controls.Add(Me.tbTop)
        Me.Controls.Add(Me.panTop)
        Me.Controls.Add(Me.panBottom)
        Me.Name = "frmDMStatements"
        Me.Text = "frmStatementPicker"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.ic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panBottom.ResumeLayout(False)
        Me.panBottom.PerformLayout()
        CType(Me.txtCyan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOrange.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.icAges, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.panTop, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ic As DevExpress.Utils.ImageCollection
    Friend WithEvents tbAge As DevExpress.XtraBars.Navigation.TileBar
    Protected Friend WithEvents panBottom As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnAccept As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtCyan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtGreen As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtYellow As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtOrange As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRed As DevExpress.XtraEditors.TextEdit
    Friend WithEvents icAges As DevExpress.Utils.ImageCollection
    Protected Friend WithEvents panTop As DevExpress.XtraEditors.PanelControl
    Friend WithEvents tbArea As DevExpress.XtraBars.Navigation.TileBar
    Friend WithEvents tbTop As DevExpress.XtraBars.Navigation.TileBar
    Friend WithEvents tbAreaGroup As DevExpress.XtraBars.Navigation.TileBarGroup
    Friend WithEvents tbLeft As DevExpress.XtraBars.Navigation.TileBar
    Friend WithEvents tbAgeGroup As DevExpress.XtraBars.Navigation.TileBarGroup
    Friend WithEvents tbItems As DevExpress.XtraBars.Navigation.TileBar
    Friend WithEvents tbItemsGroup As DevExpress.XtraBars.Navigation.TileBarGroup
    Friend WithEvents lblSelected As DevExpress.XtraEditors.LabelControl
End Class
