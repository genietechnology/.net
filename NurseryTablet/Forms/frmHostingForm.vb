﻿Imports DevExpress.XtraEditors
Imports DevExpress.XtraBars.Navigation

Imports NurseryTablet.SharedModule

Public Class frmHostingForm

    Private WithEvents m_HostedPanel As BaseHostedPanel
    Private m_FilterChildID As String = ""
    Private m_FilterChildName As String = ""

    Private Enum EnumBar
        Group
        Staff
        Children
    End Enum

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub New(ByVal HostedPanel As BaseHostedPanel, ByVal FilterChildID As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_HostedPanel = HostedPanel
        m_FilterChildID = FilterChildID

    End Sub

    Private Sub frmHostingForm_Load(sender As Object, e As EventArgs) Handles Me.Load

        If m_FilterChildID <> "" Then
            m_FilterChildName = Business.Child.ReturnName(m_FilterChildID)
            tbChildren.Visible = False
            tbGroups.Visible = False
        Else
            PopulateGroups()
        End If

        PopulateStaff()

        ResizeTiles()

        m_HostedPanel.Initialise()

        panHost.Controls.Add(m_HostedPanel)
        panHost.Controls(0).Dock = DockStyle.Fill

        If m_FilterChildID <> "" Then
            m_HostedPanel.OnChildChanged(m_FilterChildID, m_FilterChildName)
        End If

    End Sub

    Private Sub PopulateChildren(ByVal Location As String)

        tbChildGroup.Items.Clear()

        Dim _ChildList As List(Of NurseryGenieData.Child) = Business.Child.GetCheckedIn(Location)
        If _ChildList IsNot Nothing Then

            For Each _Child In _ChildList

                Dim _i As New TileBarItem
                Dim _Colour As Color = Business.Child.ReturnColour(_Child.AllergyRating, _Child.DietRestrict)

                With _i
                    .Name = "t_" + _Child.ID.ToString
                    .Text = _Child.FullName
                    .AppearanceItem.Normal.ForeColor = Color.Black
                    .AppearanceItem.Normal.FontSizeDelta = 4
                    .AppearanceItem.Normal.FontStyleDelta = FontStyle.Bold
                    .TextAlignment = TileItemContentAlignment.TopLeft
                    AddHandler _i.ItemClick, AddressOf ChildItemClick
                End With

                If _Colour <> Color.Black Then
                    _i.AppearanceItem.Normal.BackColor = _Colour
                End If

                Dim _bl As New TileItemElement
                With _bl
                    .Text = Business.Child.BuildAllergyText(_Child.AllergyRating, _Child.DietRestrict, True)
                    .Appearance.Normal.FontSizeDelta = -2
                    .TextAlignment = TileItemContentAlignment.BottomLeft
                End With
                _i.Elements.Add(_bl)

                tbChildGroup.Items.Add(_i)

            Next

            SelectFirstTile(tbChildren, tbChildGroup)

        End If

    End Sub

    Private Sub PopulateStaff()

        PopulateStaffBar()

        If Parameters.CurrentStaffName <> "" Then
            For Each _i As TileBarItem In tbStaffGroup.Items
                Dim _Name As String = _i.Elements(0).Text
                If _Name = Parameters.CurrentStaffName Then
                    tbStaff.SelectedItem = _i
                    _i.PerformItemClick()
                    Exit Sub
                End If
            Next
        Else
            SelectFirstTile(tbStaff, tbStaffGroup)
        End If

    End Sub

    Private Sub PopulateGroups()

        PopulateGroupBar()

        If Parameters.DefaultRoom <> "" Then
            For Each _i As TileBarItem In tbGroupGroup.Items
                Dim _Text As String = _i.Elements(0).Text
                If _Text = Parameters.DefaultRoom Then
                    tbGroups.SelectedItem = _i
                    _i.PerformItemClick()
                    Exit Sub
                End If
            Next
        Else
            For Each _i As TileBarItem In tbGroupGroup.Items
                tbGroups.SelectedItem = _i
                _i.PerformItemClick()
                Exit Sub
            Next
        End If

    End Sub

    Private Sub SelectFirstTile(ByRef Bar As TileBar, ByRef BarGroup As TileBarGroup)
        For Each _i As TileBarItem In BarGroup.Items
            Bar.SelectedItem = _i
            _i.PerformItemClick()
            Exit Sub
        Next
    End Sub

    Private Sub PopulateGroupBar()

        tbGroupGroup.Items.Clear()
        m_HostedPanel.ClearSelectedGroup()

        Dim _RoomList As List(Of NurseryGenieData.SiteRoom) = Business.Rooms.GetRooms
        If _RoomList IsNot Nothing Then

            For Each _Room In _RoomList

                Dim _i As New TileBarItem
                With _i

                    .Name = "t_" + _Room.ID.ToString

                    AddHandler _i.ItemClick, AddressOf GroupItemClick

                    .TextAlignment = TileItemContentAlignment.TopLeft
                    .AppearanceItem.Normal.ForeColor = Color.Black
                    .AppearanceItem.Normal.BackColor = txtLightYellow.BackColor
                    .AppearanceItem.Selected.BackColor = txtYellow.BackColor
                    .Text = _Room.Name

                End With

                tbGroupGroup.Items.Add(_i)

            Next

        End If

    End Sub

    Private Sub PopulateStaffBar()

        tbStaffGroup.Items.Clear()
        m_HostedPanel.ClearSelectedStaff()

        Dim _StaffList As List(Of NurseryGenieData.Staff) = Business.Staff.GetCheckedIn
        If _StaffList IsNot Nothing Then

            For Each _Staff In _StaffList

                Dim _i As New TileBarItem
                With _i

                    .Name = "t_" + _Staff.ID.ToString

                    AddHandler _i.ItemClick, AddressOf StaffItemClick

                    .Text = _Staff.Name
                    .AppearanceItem.Normal.BackColor = txtLightPink.BackColor
                    .AppearanceItem.Selected.BackColor = txtPink.BackColor

                    .Elements(0).Appearance.Normal.FontSizeDelta = 1

                End With

                tbStaffGroup.Items.Add(_i)

            Next

        End If

    End Sub

    Private Sub GroupItemClick(sender As Object, e As TileItemEventArgs)
        'If Not m_HostedPanel.Initialised Then Exit Sub
        Dim _id As String = e.Item.Name.Substring(2)
        m_HostedPanel.OnGroupChanged(_id, e.Item.Text)
    End Sub

    Private Sub ChildItemClick(sender As Object, e As TileItemEventArgs)
        'If Not m_HostedPanel.Initialised Then Exit Sub
        Dim _id As String = e.Item.Name.Substring(2)
        m_HostedPanel.OnChildChanged(_id, e.Item.Text)
    End Sub

    Private Sub StaffItemClick(sender As Object, e As TileItemEventArgs)
        'If Not m_HostedPanel.Initialised Then Exit Sub
        Dim _id As String = e.Item.Name.Substring(2)
        m_HostedPanel.OnStaffChanged(_id, e.Item.Text)
    End Sub

    Private Sub frmBaseMax_ResizeEnd(sender As Object, e As EventArgs) Handles Me.ResizeEnd
        ResizeTiles()
    End Sub

    Private Sub ResizeTiles()
        tbChildren.WideTileWidth = tbChildren.Width - 50
        tbStaff.WideTileWidth = tbStaff.Width - 50
    End Sub

    Private Sub m_HostedPanel_Accepted(sender As Object, e As EventArgs) Handles m_HostedPanel.Accepted
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub m_HostedPanel_GroupChanged(sender As Object, e As RecordChangedArgs) Handles m_HostedPanel.GroupChanged
        PopulateChildren(e.ValueText)
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click
        m_HostedPanel.OnAcceptClicked()
    End Sub
End Class

