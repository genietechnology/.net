﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRegister
    Inherits NurseryTablet.frmBaseNew

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.radStaff = New DevExpress.XtraEditors.CheckEdit()
        Me.radChildren = New DevExpress.XtraEditors.CheckEdit()
        Me.btnCheckIn = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCheckOut = New DevExpress.XtraEditors.SimpleButton()
        Me.txtCyan = New DevExpress.XtraEditors.TextEdit()
        Me.txtGreen = New DevExpress.XtraEditors.TextEdit()
        Me.txtYellow = New DevExpress.XtraEditors.TextEdit()
        Me.txtOrange = New DevExpress.XtraEditors.TextEdit()
        Me.txtRed = New DevExpress.XtraEditors.TextEdit()
        Me.btnClose = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.radDueIn = New DevExpress.XtraEditors.CheckEdit()
        Me.radAbsent = New DevExpress.XtraEditors.CheckEdit()
        Me.radCheckedOut = New DevExpress.XtraEditors.CheckEdit()
        Me.radCheckedIn = New DevExpress.XtraEditors.CheckEdit()
        Me.tgRegister = New NurseryTablet.TouchGrid()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        CType(Me.radStaff.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radChildren.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCyan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOrange.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.radDueIn.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radAbsent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radCheckedOut.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radCheckedIn.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl2
        '
        Me.GroupControl2.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl2.Appearance.Options.UseFont = True
        Me.GroupControl2.Controls.Add(Me.TableLayoutPanel2)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 10)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.ShowCaption = False
        Me.GroupControl2.Size = New System.Drawing.Size(186, 95)
        Me.GroupControl2.TabIndex = 0
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.radStaff, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.radChildren, 0, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(2, 2)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(182, 91)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'radStaff
        '
        Me.radStaff.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radStaff.Location = New System.Drawing.Point(3, 48)
        Me.radStaff.Name = "radStaff"
        Me.radStaff.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radStaff.Properties.Appearance.Options.UseFont = True
        Me.radStaff.Properties.Appearance.Options.UseTextOptions = True
        Me.radStaff.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.radStaff.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.radStaff.Properties.AutoHeight = False
        Me.radStaff.Properties.Caption = "Staff"
        Me.radStaff.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radStaff.Properties.RadioGroupIndex = 1
        Me.radStaff.Size = New System.Drawing.Size(176, 40)
        Me.radStaff.TabIndex = 1
        Me.radStaff.TabStop = False
        '
        'radChildren
        '
        Me.radChildren.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radChildren.EditValue = True
        Me.radChildren.Location = New System.Drawing.Point(3, 3)
        Me.radChildren.Name = "radChildren"
        Me.radChildren.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radChildren.Properties.Appearance.Options.UseFont = True
        Me.radChildren.Properties.Appearance.Options.UseTextOptions = True
        Me.radChildren.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.radChildren.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.radChildren.Properties.AutoHeight = False
        Me.radChildren.Properties.Caption = "Children"
        Me.radChildren.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radChildren.Properties.RadioGroupIndex = 1
        Me.radChildren.Size = New System.Drawing.Size(176, 39)
        Me.radChildren.TabIndex = 0
        '
        'btnCheckIn
        '
        Me.btnCheckIn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCheckIn.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCheckIn.Appearance.Options.UseFont = True
        Me.btnCheckIn.Image = Global.NurseryTablet.My.Resources.Resources.success_32
        Me.btnCheckIn.Location = New System.Drawing.Point(12, 405)
        Me.btnCheckIn.Name = "btnCheckIn"
        Me.btnCheckIn.Size = New System.Drawing.Size(230, 45)
        Me.btnCheckIn.TabIndex = 8
        Me.btnCheckIn.Text = "Check In"
        '
        'btnCheckOut
        '
        Me.btnCheckOut.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCheckOut.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCheckOut.Appearance.Options.UseFont = True
        Me.btnCheckOut.Image = Global.NurseryTablet.My.Resources.Resources.delete_32
        Me.btnCheckOut.Location = New System.Drawing.Point(248, 405)
        Me.btnCheckOut.Name = "btnCheckOut"
        Me.btnCheckOut.Size = New System.Drawing.Size(230, 45)
        Me.btnCheckOut.TabIndex = 9
        Me.btnCheckOut.Text = "Check Out"
        '
        'txtCyan
        '
        Me.txtCyan.Location = New System.Drawing.Point(128, 376)
        Me.txtCyan.Name = "txtCyan"
        Me.txtCyan.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtCyan.Properties.Appearance.Options.UseBackColor = True
        Me.txtCyan.Size = New System.Drawing.Size(23, 20)
        Me.txtCyan.TabIndex = 7
        Me.txtCyan.Visible = False
        '
        'txtGreen
        '
        Me.txtGreen.Location = New System.Drawing.Point(99, 376)
        Me.txtGreen.Name = "txtGreen"
        Me.txtGreen.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtGreen.Properties.Appearance.Options.UseBackColor = True
        Me.txtGreen.Size = New System.Drawing.Size(23, 20)
        Me.txtGreen.TabIndex = 6
        Me.txtGreen.Visible = False
        '
        'txtYellow
        '
        Me.txtYellow.Location = New System.Drawing.Point(70, 376)
        Me.txtYellow.Name = "txtYellow"
        Me.txtYellow.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtYellow.Properties.Appearance.Options.UseBackColor = True
        Me.txtYellow.Size = New System.Drawing.Size(23, 20)
        Me.txtYellow.TabIndex = 5
        Me.txtYellow.Visible = False
        '
        'txtOrange
        '
        Me.txtOrange.Location = New System.Drawing.Point(41, 376)
        Me.txtOrange.Name = "txtOrange"
        Me.txtOrange.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtOrange.Properties.Appearance.Options.UseBackColor = True
        Me.txtOrange.Size = New System.Drawing.Size(23, 20)
        Me.txtOrange.TabIndex = 4
        Me.txtOrange.Visible = False
        '
        'txtRed
        '
        Me.txtRed.Location = New System.Drawing.Point(12, 376)
        Me.txtRed.Name = "txtRed"
        Me.txtRed.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtRed.Properties.Appearance.Options.UseBackColor = True
        Me.txtRed.Size = New System.Drawing.Size(23, 20)
        Me.txtRed.TabIndex = 3
        Me.txtRed.Visible = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(1092, 405)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(230, 45)
        Me.btnClose.TabIndex = 12
        Me.btnClose.Text = "Close"
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl1.Appearance.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.TableLayoutPanel1)
        Me.GroupControl1.Location = New System.Drawing.Point(204, 10)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(1118, 95)
        Me.GroupControl1.TabIndex = 1
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 5
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.radDueIn, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.radAbsent, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.radCheckedOut, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.radCheckedIn, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(2, 2)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1114, 91)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'radDueIn
        '
        Me.radDueIn.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radDueIn.Location = New System.Drawing.Point(225, 3)
        Me.radDueIn.Name = "radDueIn"
        Me.radDueIn.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radDueIn.Properties.Appearance.Options.UseFont = True
        Me.radDueIn.Properties.AutoHeight = False
        Me.radDueIn.Properties.Caption = "Due In"
        Me.radDueIn.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radDueIn.Properties.RadioGroupIndex = 2
        Me.radDueIn.Size = New System.Drawing.Size(216, 39)
        Me.radDueIn.TabIndex = 3
        Me.radDueIn.TabStop = False
        '
        'radAbsent
        '
        Me.radAbsent.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radAbsent.Location = New System.Drawing.Point(447, 3)
        Me.radAbsent.Name = "radAbsent"
        Me.radAbsent.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAbsent.Properties.Appearance.Options.UseFont = True
        Me.radAbsent.Properties.AutoHeight = False
        Me.radAbsent.Properties.Caption = "Absent"
        Me.radAbsent.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radAbsent.Properties.RadioGroupIndex = 2
        Me.radAbsent.Size = New System.Drawing.Size(216, 39)
        Me.radAbsent.TabIndex = 2
        Me.radAbsent.TabStop = False
        '
        'radCheckedOut
        '
        Me.radCheckedOut.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radCheckedOut.Location = New System.Drawing.Point(3, 48)
        Me.radCheckedOut.Name = "radCheckedOut"
        Me.radCheckedOut.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radCheckedOut.Properties.Appearance.Options.UseFont = True
        Me.radCheckedOut.Properties.AutoHeight = False
        Me.radCheckedOut.Properties.Caption = "Checked Out"
        Me.radCheckedOut.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radCheckedOut.Properties.RadioGroupIndex = 2
        Me.radCheckedOut.Size = New System.Drawing.Size(216, 40)
        Me.radCheckedOut.TabIndex = 1
        Me.radCheckedOut.TabStop = False
        '
        'radCheckedIn
        '
        Me.radCheckedIn.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radCheckedIn.EditValue = True
        Me.radCheckedIn.Location = New System.Drawing.Point(3, 3)
        Me.radCheckedIn.Name = "radCheckedIn"
        Me.radCheckedIn.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radCheckedIn.Properties.Appearance.Options.UseFont = True
        Me.radCheckedIn.Properties.AutoHeight = False
        Me.radCheckedIn.Properties.Caption = "Checked In"
        Me.radCheckedIn.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radCheckedIn.Properties.RadioGroupIndex = 2
        Me.radCheckedIn.Size = New System.Drawing.Size(216, 39)
        Me.radCheckedIn.TabIndex = 0
        '
        'tgRegister
        '
        Me.tgRegister.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tgRegister.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tgRegister.Appearance.Options.UseFont = True
        Me.tgRegister.HideFirstColumn = False
        Me.tgRegister.Location = New System.Drawing.Point(12, 116)
        Me.tgRegister.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.tgRegister.Name = "tgRegister"
        Me.tgRegister.Size = New System.Drawing.Size(1310, 280)
        Me.tgRegister.TabIndex = 2
        '
        'frmRegister
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
        Me.ClientSize = New System.Drawing.Size(1334, 462)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.btnCheckIn)
        Me.Controls.Add(Me.btnCheckOut)
        Me.Controls.Add(Me.txtCyan)
        Me.Controls.Add(Me.txtGreen)
        Me.Controls.Add(Me.txtYellow)
        Me.Controls.Add(Me.txtOrange)
        Me.Controls.Add(Me.txtRed)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.tgRegister)
        Me.Name = "frmRegister"
        Me.Text = "Register"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        CType(Me.radStaff.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radChildren.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCyan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOrange.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.radDueIn.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radAbsent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radCheckedOut.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radCheckedIn.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tgRegister As NurseryTablet.TouchGrid
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents radCheckedOut As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents radCheckedIn As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txtRed As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtOrange As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtYellow As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtGreen As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCyan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btnCheckIn As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCheckOut As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents radStaff As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents radChildren As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents radAbsent As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents radDueIn As DevExpress.XtraEditors.CheckEdit

End Class
