﻿
Imports NurseryTablet.Business

Public Class frmVisitor

    Private m_Signature As New Signature
    Private m_DayID As String = Day.TodayIDString
    Private m_ID As Guid? = Nothing

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_ID = Guid.NewGuid

    End Sub

    Private Sub frmVisitor_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click

        If Not CheckAnswers Then Exit Sub

        btnAccept.Enabled = False
        Application.DoEvents()

        Business.Register.RegisterTransaction(m_ID.ToString, SharedModule.Enums.PersonType.Visitor, fldName.ValueText, SharedModule.Enums.InOut.CheckIn, SharedModule.Enums.RegisterVia.Register, fldVisiting.ValueText, fldCompany.ValueText, fldCarReg.ValueText, "")

        If m_Signature.SignatureCaptured Then
            m_Signature.StoreSignature()
        End If

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()

        btnAccept.Enabled = True
        Application.DoEvents()

    End Sub

    Private Function CheckAnswers() As Boolean

        If fldName.ValueText = "" Then
            Msgbox("Please enter the visitor's name.", MessageBoxIcon.Exclamation, "Check-In Visitor")
            Return False
        End If

        If fldVisiting.ValueText = "" Then
            Msgbox("Please enter who this person is visiting.", MessageBoxIcon.Exclamation, "Check-In Visitor")
            Return False
        End If

        If Not m_Signature.SignatureCaptured Then
            Msgbox("Please supply a signature.", MessageBoxIcon.Exclamation, "Check-In Visitor")
            Return False
        End If

        Return True

    End Function

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnSig_Click(sender As Object, e As EventArgs) Handles btnSig.Click
        If fldName.ValueText <> "" Then
            m_Signature.CaptureVisitorSignature(False, m_ID, fldName.ValueText)
        End If
    End Sub
End Class