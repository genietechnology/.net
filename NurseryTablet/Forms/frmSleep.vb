﻿Imports NurseryTablet.Business

Public Class frmSleep

    Private m_StartHours As String
    Private m_StartMinutes As String
    Private m_EndHours As String
    Private m_EndMinutes As String
    Private m_ButtonNumber As Integer

    Private Sub frmSleep_AcceptChanged(sender As Object, e As AcceptArgs) Handles Me.AcceptChanged
        btnAdd.Enabled = e.Enabled
    End Sub

    Private Sub frmSleep_ChildChanged(sender As Object, e As EventArgs) Handles Me.ChildChanged
        Clear()
    End Sub

    Private Sub Clear()
        ClearSleepButtons()
        ClearButtons()
        SetButtons(False)
    End Sub

    Private Sub ClearSleepButtons()

        m_ButtonNumber = 0

        btnSleep1.Enabled = False
        btnSleep1.BackColor = System.Drawing.SystemColors.Control
        btnSleep1.Text = ""
        btnSleep2.Enabled = False
        btnSleep2.BackColor = System.Drawing.SystemColors.Control
        btnSleep2.Text = ""
        btnSleep3.Enabled = False
        btnSleep3.BackColor = System.Drawing.SystemColors.Control
        btnSleep3.Text = ""
        btnSleep4.Enabled = False
        btnSleep4.BackColor = System.Drawing.SystemColors.Control
        btnSleep4.Text = ""
        btnSleep5.Enabled = False
        btnSleep5.BackColor = System.Drawing.SystemColors.Control
        btnSleep5.Text = ""
        btnSleep6.Enabled = False
        btnSleep6.BackColor = System.Drawing.SystemColors.Control
        btnSleep6.Text = ""

        btnRemoveSleep.Enabled = False

    End Sub

    Protected Overrides Sub DisplayRecord()

        ClearSleepButtons()

        Dim _Activity As List(Of NurseryGenieData.Activity) = Business.Activity.ReturnActivity(ChildID, Activity.EnumActivityType.Sleep, SharedModule.Enums.EnumActivityOrder.Value2)
        If _Activity IsNot Nothing Then

            If _Activity.Count > 0 Then

                btnRemoveSleep.Enabled = True

                Dim _i As Integer = 0
                For Each _A In _Activity

                    _i += 1

                    If _i = 1 Then
                        btnSleep1.Enabled = True
                        btnSleep1.Text = _A.Value1
                        m_ButtonNumber = 1
                        DisplayTimes(1)
                    End If

                    If _i = 2 Then
                        btnSleep2.Enabled = True
                        btnSleep2.Text = _A.Value1
                        m_ButtonNumber = 2
                    End If

                    If _i = 3 Then
                        btnSleep3.Enabled = True
                        btnSleep3.Text = _A.Value1
                        m_ButtonNumber = 3
                    End If

                    If _i = 4 Then
                        btnSleep4.Enabled = True
                        btnSleep4.Text = _A.Value1
                        m_ButtonNumber = 4
                    End If

                    If _i = 5 Then
                        btnSleep5.Enabled = True
                        btnSleep5.Text = _A.Value1
                        m_ButtonNumber = 5
                    End If

                    If _i = 6 Then
                        btnSleep6.Enabled = True
                        btnSleep6.Text = _A.Value1
                        m_ButtonNumber = 6
                    End If

                Next

                SetButtons(True)

            Else
                ClearButtons()
                SetButtons(False)
            End If

        End If

    End Sub

    Protected Overrides Sub CommitUpdate()

        SharedModule.DeleteSleep(Day.TodayIDString, txtChild.Tag.ToString)

        If btnSleep1.Text <> "" Then SharedModule.LogSleep(Day.TodayIDString, txtChild.Tag.ToString, txtChild.Text, btnSleep1.Text, txtStaff.Tag.ToString, txtStaff.Text)
        If btnSleep2.Text <> "" Then SharedModule.LogSleep(Day.TodayIDString, txtChild.Tag.ToString, txtChild.Text, btnSleep2.Text, txtStaff.Tag.ToString, txtStaff.Text)
        If btnSleep3.Text <> "" Then SharedModule.LogSleep(Day.TodayIDString, txtChild.Tag.ToString, txtChild.Text, btnSleep3.Text, txtStaff.Tag.ToString, txtStaff.Text)
        If btnSleep4.Text <> "" Then SharedModule.LogSleep(Day.TodayIDString, txtChild.Tag.ToString, txtChild.Text, btnSleep4.Text, txtStaff.Tag.ToString, txtStaff.Text)
        If btnSleep5.Text <> "" Then SharedModule.LogSleep(Day.TodayIDString, txtChild.Tag.ToString, txtChild.Text, btnSleep5.Text, txtStaff.Tag.ToString, txtStaff.Text)
        If btnSleep6.Text <> "" Then SharedModule.LogSleep(Day.TodayIDString, txtChild.Tag.ToString, txtChild.Text, btnSleep6.Text, txtStaff.Tag.ToString, txtStaff.Text)

    End Sub

    Private Sub DisplayTimes(ButtonNumber As Integer)

        btnSleep1.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnSleep2.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnSleep3.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnSleep4.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnSleep5.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnSleep6.BackColor = Color.FromKnownColor(KnownColor.Control)

        m_ButtonNumber = ButtonNumber

        Dim _btn As Button = Nothing
        If ButtonNumber = 1 Then _btn = btnSleep1
        If ButtonNumber = 2 Then _btn = btnSleep2
        If ButtonNumber = 3 Then _btn = btnSleep3
        If ButtonNumber = 4 Then _btn = btnSleep4
        If ButtonNumber = 5 Then _btn = btnSleep5
        If ButtonNumber = 6 Then _btn = btnSleep6

        _btn.BackColor = Color.Yellow
        _btn.Enabled = True

        Dim _Start As String = Mid(_btn.Text, 1, 5)
        Dim _End As String = Mid(_btn.Text, 7)

        ClearButtons()
        SetTime(_Start, True)
        SetTime(_End, False)

    End Sub

    Private Sub SetTime(Time As String, StartTime As Boolean)

        Dim _Hours As String = Mid(Time, 1, 2)
        Dim _Mins As String = Mid(Time, 4, 2)

        SetHours(_Hours, StartTime)
        SetMinutes(_Mins, StartTime)

        If StartTime Then
            m_StartHours = _Hours
            m_StartMinutes = _Mins
        Else
            m_EndHours = _Hours
            m_EndMinutes = _Mins
        End If

    End Sub

    Private Sub BuildTime()

        Dim _Time As String = m_StartHours + ":" + m_StartMinutes + "-" + m_EndHours + ":" + m_EndMinutes

        If m_ButtonNumber = 1 Then btnSleep1.Text = _Time
        If m_ButtonNumber = 2 Then btnSleep2.Text = _Time
        If m_ButtonNumber = 3 Then btnSleep3.Text = _Time
        If m_ButtonNumber = 4 Then btnSleep4.Text = _Time
        If m_ButtonNumber = 5 Then btnSleep5.Text = _Time
        If m_ButtonNumber = 6 Then btnSleep6.Text = _Time

    End Sub

    Private Sub SetButtons(ByVal Enabled As Boolean)
        SetStartButtons(Enabled)
        SetEndButtons(Enabled)
    End Sub

    Private Sub SetStartButtons(ByVal Enabled As Boolean)
        btnStartH8.Enabled = Enabled
        btnStartH9.Enabled = Enabled
        btnStartH10.Enabled = Enabled
        btnStartH11.Enabled = Enabled
        btnStartH12.Enabled = Enabled
        btnStartH13.Enabled = Enabled
        btnStartH14.Enabled = Enabled
        btnStartH15.Enabled = Enabled
        btnStartH16.Enabled = Enabled
        btnStartH17.Enabled = Enabled
        btnStartH18.Enabled = Enabled
        btnStartH19.Enabled = Enabled
        btnStartM0.Enabled = Enabled
        btnStartM5.Enabled = Enabled
        btnStartM10.Enabled = Enabled
        btnStartM15.Enabled = Enabled
        btnStartM20.Enabled = Enabled
        btnStartM25.Enabled = Enabled
        btnStartM30.Enabled = Enabled
        btnStartM35.Enabled = Enabled
        btnStartM40.Enabled = Enabled
        btnStartM45.Enabled = Enabled
        btnStartM50.Enabled = Enabled
        btnStartM55.Enabled = Enabled
    End Sub

    Private Sub SetEndButtons(ByVal Enabled As Boolean)
        btnEndH8.Enabled = Enabled
        btnEndH9.Enabled = Enabled
        btnEndH10.Enabled = Enabled
        btnEndH11.Enabled = Enabled
        btnEndH12.Enabled = Enabled
        btnEndH13.Enabled = Enabled
        btnEndH14.Enabled = Enabled
        btnEndH15.Enabled = Enabled
        btnEndH16.Enabled = Enabled
        btnEndH17.Enabled = Enabled
        btnEndH18.Enabled = Enabled
        btnEndH19.Enabled = Enabled
        btnEndM0.Enabled = Enabled
        btnEndM5.Enabled = Enabled
        btnEndM10.Enabled = Enabled
        btnEndM15.Enabled = Enabled
        btnEndM20.Enabled = Enabled
        btnEndM25.Enabled = Enabled
        btnEndM30.Enabled = Enabled
        btnEndM35.Enabled = Enabled
        btnEndM40.Enabled = Enabled
        btnEndM45.Enabled = Enabled
        btnEndM50.Enabled = Enabled
        btnEndM55.Enabled = Enabled
    End Sub

    Private Sub ClearStartHours()
        btnStartH8.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnStartH9.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnStartH10.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnStartH11.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnStartH12.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnStartH13.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnStartH14.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnStartH15.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnStartH16.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnStartH17.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnStartH18.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnStartH19.BackColor = Color.FromKnownColor(KnownColor.Control)
    End Sub

    Private Sub ClearStartMinutes()
        btnStartM0.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnStartM5.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnStartM10.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnStartM15.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnStartM20.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnStartM25.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnStartM30.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnStartM35.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnStartM40.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnStartM45.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnStartM50.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnStartM55.BackColor = Color.FromKnownColor(KnownColor.Control)
    End Sub

    Private Sub ClearEndHours()
        btnEndH8.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnEndH9.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnEndH10.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnEndH11.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnEndH12.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnEndH13.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnEndH14.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnEndH15.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnEndH16.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnEndH17.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnEndH18.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnEndH19.BackColor = Color.FromKnownColor(KnownColor.Control)
    End Sub

    Private Sub ClearEndMinutes()
        btnEndM0.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnEndM5.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnEndM10.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnEndM15.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnEndM20.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnEndM25.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnEndM30.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnEndM35.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnEndM40.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnEndM45.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnEndM50.BackColor = Color.FromKnownColor(KnownColor.Control)
        btnEndM55.BackColor = Color.FromKnownColor(KnownColor.Control)
    End Sub

    Private Sub ClearButtons()
        ClearStartHours()
        ClearStartMinutes()
        ClearEndHours()
        ClearEndMinutes()
    End Sub

    Private Sub SetHours(Hours As String, StartTime As Boolean)

        If Hours = "08" Then
            If StartTime Then
                btnStartH8.BackColor = Color.Yellow
            Else
                btnEndH8.BackColor = Color.Yellow
            End If
        End If

        If Hours = "09" Then
            If StartTime Then
                btnStartH9.BackColor = Color.Yellow
            Else
                btnEndH9.BackColor = Color.Yellow
            End If
        End If

        If Hours = "10" Then
            If StartTime Then
                btnStartH10.BackColor = Color.Yellow
            Else
                btnEndH10.BackColor = Color.Yellow
            End If
        End If

        If Hours = "11" Then
            If StartTime Then
                btnStartH11.BackColor = Color.Yellow
            Else
                btnEndH11.BackColor = Color.Yellow
            End If
        End If

        If Hours = "12" Then
            If StartTime Then
                btnStartH12.BackColor = Color.Yellow
            Else
                btnEndH12.BackColor = Color.Yellow
            End If
        End If

        If Hours = "13" Then
            If StartTime Then
                btnStartH13.BackColor = Color.Yellow
            Else
                btnEndH13.BackColor = Color.Yellow
            End If
        End If

        If Hours = "14" Then
            If StartTime Then
                btnStartH14.BackColor = Color.Yellow
            Else
                btnEndH14.BackColor = Color.Yellow
            End If
        End If

        If Hours = "15" Then
            If StartTime Then
                btnStartH15.BackColor = Color.Yellow
            Else
                btnEndH15.BackColor = Color.Yellow
            End If
        End If

        If Hours = "16" Then
            If StartTime Then
                btnStartH16.BackColor = Color.Yellow
            Else
                btnEndH16.BackColor = Color.Yellow
            End If
        End If

        If Hours = "17" Then
            If StartTime Then
                btnStartH17.BackColor = Color.Yellow
            Else
                btnEndH17.BackColor = Color.Yellow
            End If
        End If

        If Hours = "18" Then
            If StartTime Then
                btnStartH18.BackColor = Color.Yellow
            Else
                btnEndH18.BackColor = Color.Yellow
            End If
        End If

        If Hours = "19" Then
            If StartTime Then
                btnStartH19.BackColor = Color.Yellow
            Else
                btnEndH19.BackColor = Color.Yellow
            End If
        End If

    End Sub

    Private Sub SetMinutes(Minutes As String, StartTime As Boolean)

        If Minutes = "00" Then
            If StartTime Then
                btnStartM0.BackColor = Color.Yellow
            Else
                btnEndM0.BackColor = Color.Yellow
            End If
        End If

        If Minutes = "05" Then
            If StartTime Then
                btnStartM5.BackColor = Color.Yellow
            Else
                btnEndM5.BackColor = Color.Yellow
            End If
        End If

        If Minutes = "10" Then
            If StartTime Then
                btnStartM10.BackColor = Color.Yellow
            Else
                btnEndM10.BackColor = Color.Yellow
            End If
        End If

        If Minutes = "15" Then
            If StartTime Then
                btnStartM15.BackColor = Color.Yellow
            Else
                btnEndM15.BackColor = Color.Yellow
            End If
        End If

        If Minutes = "20" Then
            If StartTime Then
                btnStartM20.BackColor = Color.Yellow
            Else
                btnEndM20.BackColor = Color.Yellow
            End If
        End If

        If Minutes = "25" Then
            If StartTime Then
                btnStartM25.BackColor = Color.Yellow
            Else
                btnEndM25.BackColor = Color.Yellow
            End If
        End If

        If Minutes = "30" Then
            If StartTime Then
                btnStartM30.BackColor = Color.Yellow
            Else
                btnEndM30.BackColor = Color.Yellow
            End If
        End If

        If Minutes = "35" Then
            If StartTime Then
                btnStartM35.BackColor = Color.Yellow
            Else
                btnEndM35.BackColor = Color.Yellow
            End If
        End If

        If Minutes = "40" Then
            If StartTime Then
                btnStartM40.BackColor = Color.Yellow
            Else
                btnEndM40.BackColor = Color.Yellow
            End If
        End If

        If Minutes = "45" Then
            If StartTime Then
                btnStartM45.BackColor = Color.Yellow
            Else
                btnEndM45.BackColor = Color.Yellow
            End If
        End If

        If Minutes = "50" Then
            If StartTime Then
                btnStartM50.BackColor = Color.Yellow
            Else
                btnEndM50.BackColor = Color.Yellow
            End If
        End If

        If Minutes = "55" Then
            If StartTime Then
                btnStartM55.BackColor = Color.Yellow
            Else
                btnEndM55.BackColor = Color.Yellow
            End If
        End If

    End Sub

#Region "Button Clicks"

    Private Sub btnStartH9_Click(sender As System.Object, e As System.EventArgs) Handles btnStartH9.Click
        ClearStartHours()
        btnStartH9.BackColor = Color.Yellow
        m_StartHours = "09"
        BuildTime()
    End Sub

    Private Sub btnStartH10_Click(sender As System.Object, e As System.EventArgs) Handles btnStartH10.Click
        ClearStartHours()
        btnStartH10.BackColor = Color.Yellow
        m_StartHours = "10"
        BuildTime()
    End Sub

    Private Sub btnStartH11_Click(sender As System.Object, e As System.EventArgs) Handles btnStartH11.Click
        ClearStartHours()
        btnStartH11.BackColor = Color.Yellow
        m_StartHours = "11"
        BuildTime()
    End Sub

    Private Sub btnStartH12_Click(sender As System.Object, e As System.EventArgs) Handles btnStartH12.Click
        ClearStartHours()
        btnStartH12.BackColor = Color.Yellow
        m_StartHours = "12"
        BuildTime()
    End Sub

    Private Sub btnStartH13_Click(sender As System.Object, e As System.EventArgs) Handles btnStartH13.Click
        ClearStartHours()
        btnStartH13.BackColor = Color.Yellow
        m_StartHours = "13"
        BuildTime()
    End Sub

    Private Sub btnStartH14_Click(sender As System.Object, e As System.EventArgs) Handles btnStartH14.Click
        ClearStartHours()
        btnStartH14.BackColor = Color.Yellow
        m_StartHours = "14"
        BuildTime()
    End Sub

    Private Sub btnStartH15_Click(sender As System.Object, e As System.EventArgs) Handles btnStartH15.Click
        ClearStartHours()
        btnStartH15.BackColor = Color.Yellow
        m_StartHours = "15"
        BuildTime()
    End Sub

    Private Sub btnStartH16_Click(sender As System.Object, e As System.EventArgs) Handles btnStartH16.Click
        ClearStartHours()
        btnStartH16.BackColor = Color.Yellow
        m_StartHours = "16"
        BuildTime()
    End Sub

    Private Sub btnStartM0_Click(sender As System.Object, e As System.EventArgs) Handles btnStartM0.Click
        ClearStartMinutes()
        btnStartM0.BackColor = Color.Yellow
        m_StartMinutes = "00"
        BuildTime()
    End Sub

    Private Sub btnStartM5_Click(sender As System.Object, e As System.EventArgs) Handles btnStartM5.Click
        ClearStartMinutes()
        btnStartM5.BackColor = Color.Yellow
        m_StartMinutes = "05"
        BuildTime()
    End Sub

    Private Sub btnStartM10_Click(sender As System.Object, e As System.EventArgs) Handles btnStartM10.Click
        ClearStartMinutes()
        btnStartM10.BackColor = Color.Yellow
        m_StartMinutes = "10"
        BuildTime()
    End Sub

    Private Sub btnStartM15_Click(sender As System.Object, e As System.EventArgs) Handles btnStartM15.Click
        ClearStartMinutes()
        btnStartM15.BackColor = Color.Yellow
        m_StartMinutes = "15"
        BuildTime()
    End Sub

    Private Sub btnStartM20_Click(sender As System.Object, e As System.EventArgs) Handles btnStartM20.Click
        ClearStartMinutes()
        btnStartM20.BackColor = Color.Yellow
        m_StartMinutes = "20"
        BuildTime()
    End Sub

    Private Sub btnStartM25_Click(sender As System.Object, e As System.EventArgs) Handles btnStartM25.Click
        ClearStartMinutes()
        btnStartM25.BackColor = Color.Yellow
        m_StartMinutes = "25"
        BuildTime()
    End Sub

    Private Sub btnStartM30_Click(sender As System.Object, e As System.EventArgs) Handles btnStartM30.Click
        ClearStartMinutes()
        btnStartM30.BackColor = Color.Yellow
        m_StartMinutes = "30"
        BuildTime()
    End Sub

    Private Sub btnStartM35_Click(sender As System.Object, e As System.EventArgs) Handles btnStartM35.Click
        ClearStartMinutes()
        btnStartM35.BackColor = Color.Yellow
        m_StartMinutes = "35"
        BuildTime()
    End Sub

    Private Sub btnStartM40_Click(sender As System.Object, e As System.EventArgs) Handles btnStartM40.Click
        ClearStartMinutes()
        btnStartM40.BackColor = Color.Yellow
        m_StartMinutes = "40"
        BuildTime()
    End Sub

    Private Sub btnStartM45_Click(sender As System.Object, e As System.EventArgs) Handles btnStartM45.Click
        ClearStartMinutes()
        btnStartM45.BackColor = Color.Yellow
        m_StartMinutes = "45"
        BuildTime()
    End Sub

    Private Sub btnStartM50_Click(sender As System.Object, e As System.EventArgs) Handles btnStartM50.Click
        ClearStartMinutes()
        btnStartM50.BackColor = Color.Yellow
        m_StartMinutes = "50"
        BuildTime()
    End Sub

    Private Sub btnStartM55_Click(sender As System.Object, e As System.EventArgs) Handles btnStartM55.Click
        ClearStartMinutes()
        btnStartM55.BackColor = Color.Yellow
        m_StartMinutes = "55"
        BuildTime()
    End Sub

    Private Sub btnEndH9_Click(sender As System.Object, e As System.EventArgs) Handles btnEndH9.Click
        ClearEndHours()
        btnEndH9.BackColor = Color.Yellow
        m_EndHours = "09"
        BuildTime()
    End Sub

    Private Sub btnEndH10_Click(sender As System.Object, e As System.EventArgs) Handles btnEndH10.Click
        ClearEndHours()
        btnEndH10.BackColor = Color.Yellow
        m_EndHours = "10"
        BuildTime()
    End Sub

    Private Sub btnEndH11_Click(sender As System.Object, e As System.EventArgs) Handles btnEndH11.Click
        ClearEndHours()
        btnEndH11.BackColor = Color.Yellow
        m_EndHours = "11"
        BuildTime()
    End Sub

    Private Sub btnEndH12_Click(sender As System.Object, e As System.EventArgs) Handles btnEndH12.Click
        ClearEndHours()
        btnEndH12.BackColor = Color.Yellow
        m_EndHours = "12"
        BuildTime()
    End Sub

    Private Sub btnEndH13_Click(sender As System.Object, e As System.EventArgs) Handles btnEndH13.Click
        ClearEndHours()
        btnEndH13.BackColor = Color.Yellow
        m_EndHours = "13"
        BuildTime()
    End Sub

    Private Sub btnEndH14_Click(sender As System.Object, e As System.EventArgs) Handles btnEndH14.Click
        ClearEndHours()
        btnEndH14.BackColor = Color.Yellow
        m_EndHours = "14"
        BuildTime()
    End Sub

    Private Sub btnEndH15_Click(sender As System.Object, e As System.EventArgs) Handles btnEndH15.Click
        ClearEndHours()
        btnEndH15.BackColor = Color.Yellow
        m_EndHours = "15"
        BuildTime()
    End Sub

    Private Sub btnEndH16_Click(sender As System.Object, e As System.EventArgs) Handles btnEndH16.Click
        ClearEndHours()
        btnEndH16.BackColor = Color.Yellow
        m_EndHours = "16"
        BuildTime()
    End Sub

    Private Sub btnEndM0_Click(sender As System.Object, e As System.EventArgs) Handles btnEndM0.Click
        ClearEndMinutes()
        btnEndM0.BackColor = Color.Yellow
        m_EndMinutes = "00"
        BuildTime()
    End Sub

    Private Sub btnEndM5_Click(sender As System.Object, e As System.EventArgs) Handles btnEndM5.Click
        ClearEndMinutes()
        btnEndM5.BackColor = Color.Yellow
        m_EndMinutes = "05"
        BuildTime()
    End Sub

    Private Sub btnEndM10_Click(sender As System.Object, e As System.EventArgs) Handles btnEndM10.Click
        ClearEndMinutes()
        btnEndM10.BackColor = Color.Yellow
        m_EndMinutes = "10"
        BuildTime()
    End Sub

    Private Sub btnEndM15_Click(sender As System.Object, e As System.EventArgs) Handles btnEndM15.Click
        ClearEndMinutes()
        btnEndM15.BackColor = Color.Yellow
        m_EndMinutes = "15"
        BuildTime()
    End Sub

    Private Sub btnEndM20_Click(sender As System.Object, e As System.EventArgs) Handles btnEndM20.Click
        ClearEndMinutes()
        btnEndM20.BackColor = Color.Yellow
        m_EndMinutes = "20"
        BuildTime()
    End Sub

    Private Sub btnEndM25_Click(sender As System.Object, e As System.EventArgs) Handles btnEndM25.Click
        ClearEndMinutes()
        btnEndM25.BackColor = Color.Yellow
        m_EndMinutes = "25"
        BuildTime()
    End Sub

    Private Sub btnEndM30_Click(sender As System.Object, e As System.EventArgs) Handles btnEndM30.Click
        ClearEndMinutes()
        btnEndM30.BackColor = Color.Yellow
        m_EndMinutes = "30"
        BuildTime()
    End Sub

    Private Sub btnEndM35_Click(sender As System.Object, e As System.EventArgs) Handles btnEndM35.Click
        ClearEndMinutes()
        btnEndM35.BackColor = Color.Yellow
        m_EndMinutes = "35"
        BuildTime()
    End Sub

    Private Sub btnEndM40_Click(sender As System.Object, e As System.EventArgs) Handles btnEndM40.Click
        ClearEndMinutes()
        btnEndM40.BackColor = Color.Yellow
        m_EndMinutes = "40"
        BuildTime()
    End Sub

    Private Sub btnEndM45_Click(sender As System.Object, e As System.EventArgs) Handles btnEndM45.Click
        ClearEndMinutes()
        btnEndM45.BackColor = Color.Yellow
        m_EndMinutes = "45"
        BuildTime()
    End Sub

    Private Sub btnEndM50_Click(sender As System.Object, e As System.EventArgs) Handles btnEndM50.Click
        ClearEndMinutes()
        btnEndM50.BackColor = Color.Yellow
        m_EndMinutes = "50"
        BuildTime()
    End Sub

    Private Sub btnEndM55_Click(sender As System.Object, e As System.EventArgs) Handles btnEndM55.Click
        ClearEndMinutes()
        btnEndM55.BackColor = Color.Yellow
        m_EndMinutes = "55"
        BuildTime()
    End Sub

    Private Sub btnStartH8_Click(sender As Object, e As EventArgs) Handles btnStartH8.Click
        ClearStartHours()
        btnStartH8.BackColor = Color.Yellow
        m_StartHours = "08"
        BuildTime()
    End Sub

    Private Sub btnStartH17_Click(sender As Object, e As EventArgs) Handles btnStartH17.Click
        ClearStartHours()
        btnStartH17.BackColor = Color.Yellow
        m_StartHours = "17"
        BuildTime()
    End Sub

    Private Sub btnStartH18_Click(sender As Object, e As EventArgs) Handles btnStartH18.Click
        ClearStartHours()
        btnStartH18.BackColor = Color.Yellow
        m_StartHours = "18"
        BuildTime()
    End Sub

    Private Sub btnStartH19_Click(sender As Object, e As EventArgs) Handles btnStartH19.Click
        ClearStartHours()
        btnStartH19.BackColor = Color.Yellow
        m_StartHours = "19"
        BuildTime()
    End Sub

    Private Sub btnEndH8_Click(sender As Object, e As EventArgs) Handles btnEndH8.Click
        ClearEndHours()
        btnEndH8.BackColor = Color.Yellow
        m_EndHours = "08"
        BuildTime()
    End Sub

    Private Sub btnEndH17_Click(sender As Object, e As EventArgs) Handles btnEndH17.Click
        ClearEndHours()
        btnEndH17.BackColor = Color.Yellow
        m_EndHours = "17"
        BuildTime()
    End Sub

    Private Sub btnEndH18_Click(sender As Object, e As EventArgs) Handles btnEndH18.Click
        ClearEndHours()
        btnEndH18.BackColor = Color.Yellow
        m_EndHours = "18"
        BuildTime()
    End Sub

    Private Sub btnEndH19_Click(sender As Object, e As EventArgs) Handles btnEndH19.Click
        ClearEndHours()
        btnEndH19.BackColor = Color.Yellow
        m_EndHours = "19"
        BuildTime()
    End Sub

#End Region

    Private Sub btnRemoveSleep_Click(sender As System.Object, e As System.EventArgs) Handles btnRemoveSleep.Click

        If Msgbox("Are you sure you want to delete this sleep record?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Confirm Delete") = MsgBoxResult.Yes Then

            Dim _btn As Button = Nothing
            If m_ButtonNumber = 1 Then _btn = btnSleep1
            If m_ButtonNumber = 2 Then _btn = btnSleep2
            If m_ButtonNumber = 3 Then _btn = btnSleep3
            If m_ButtonNumber = 4 Then _btn = btnSleep4
            If m_ButtonNumber = 5 Then _btn = btnSleep5
            If m_ButtonNumber = 6 Then _btn = btnSleep6

            _btn.Text = ""
            _btn.BackColor = Color.FromKnownColor(KnownColor.Control)
            _btn.Enabled = False

            m_StartHours = ""
            m_StartMinutes = ""
            m_EndHours = ""
            m_EndMinutes = ""

            m_ButtonNumber -= 1

            CommitUpdate()
            DisplayRecord()

        End If

    End Sub

    Private Sub btnAdd_Click(sender As System.Object, e As System.EventArgs) Handles btnAdd.Click

        If m_ButtonNumber = 6 Then
            MsgBox("You cannot add more than 6 sleep records per day.", MessageBoxIcon.Exclamation, "Maximum Sleep Records")
            Exit Sub
        End If

        btnRemoveSleep.Enabled = True

        m_ButtonNumber += 1

        Dim _btn As Button = Nothing
        If m_ButtonNumber = 1 Then _btn = btnSleep1
        If m_ButtonNumber = 2 Then _btn = btnSleep2
        If m_ButtonNumber = 3 Then _btn = btnSleep3
        If m_ButtonNumber = 4 Then _btn = btnSleep4
        If m_ButtonNumber = 5 Then _btn = btnSleep5
        If m_ButtonNumber = 6 Then _btn = btnSleep6

        _btn.Text = "09:00-10:00"
        DisplayTimes(m_ButtonNumber)
        SetButtons(True)

    End Sub

    Private Sub btnSleep1_Click(sender As System.Object, e As System.EventArgs) Handles btnSleep1.Click
        DisplayTimes(1)
    End Sub

    Private Sub btnSleep2_Click(sender As System.Object, e As System.EventArgs) Handles btnSleep2.Click
        DisplayTimes(2)
    End Sub

    Private Sub btnSleep3_Click(sender As System.Object, e As System.EventArgs) Handles btnSleep3.Click
        DisplayTimes(3)
    End Sub

    Private Sub btnSleep4_Click(sender As System.Object, e As System.EventArgs) Handles btnSleep4.Click
        DisplayTimes(4)
    End Sub

    Private Sub btnSleep5_Click(sender As System.Object, e As System.EventArgs) Handles btnSleep5.Click
        DisplayTimes(5)
    End Sub

    Private Sub btnSleep6_Click(sender As System.Object, e As System.EventArgs) Handles btnSleep6.Click
        DisplayTimes(6)
    End Sub

End Class
