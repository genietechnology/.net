﻿Imports DevExpress.XtraEditors
Imports DevExpress.XtraBars.Navigation
Imports NurseryTablet.SharedModule

Public Class frmDMStatements

    Public Enum EnumDisplayMode
        DevelopmentMatterStatements
        COEL
    End Enum

    Private Enum EnumBar
        Age
        Area
        Item
    End Enum

    Public Sub New(DisplayMode As EnumDisplayMode)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_DisplayMode = DisplayMode

    End Sub

    Public ReadOnly Property SelectedItems As List(Of Pair)
        Get
            Return m_SelectedItems
        End Get
    End Property

    Private m_SelectedItems As New List(Of Pair)
    Private m_DisplayMode As EnumDisplayMode = EnumDisplayMode.DevelopmentMatterStatements
    Private m_SelectedAreaID As String = ""
    Private m_SelectedAgeID As String = ""

    Private Sub frmStatementPicker_Load(sender As Object, e As EventArgs) Handles Me.Load
        Populate()
    End Sub

    Private Sub Populate()

        Dim _SQL As String = ""

        If m_DisplayMode = EnumDisplayMode.DevelopmentMatterStatements Then

            Me.Text = "Select Development Matters Statements"

            _SQL = "select id, name, months_to from EYAgeBands order by months_from"
            PopulateTileBar(tbAgeGroup, _SQL, EnumBar.Age)

            _SQL = "select id, name, short_name, aspect, colour from EYAreas where type in ('P','S') order by display_seq"
            PopulateTileBar(tbAreaGroup, _SQL, EnumBar.Area)

        Else

            Me.Text = "Select Characteristics of Effective Learning"

            _SQL = "select id, name, short_name, aspect, colour from EYAreas where type in ('COEL') order by display_seq"
            PopulateTileBar(tbAreaGroup, _SQL, EnumBar.Area)

            tbLeft.Hide()

        End If

    End Sub

    Private Sub PopulateTileBar(ByRef BarGroup As TileBarGroup, ByVal _SQL As String, ByVal Bar As EnumBar)

        BarGroup.Items.Clear()

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _i As New TileBarItem
                With _i

                    .Name = "tbi_" + _DR.Item("ID").ToString

                    Select Case Bar

                        Case EnumBar.Age

                            AddHandler _i.ItemClick, AddressOf AgeItemClick

                            .TextAlignment = TileItemContentAlignment.TopLeft
                            .AppearanceItem.Normal.ForeColor = Color.Black
                            .AppearanceItem.Normal.BackColor = Color.FromArgb(255, 202, 4)
                            .Text = _DR.Item("name").ToString

                            .Image = icAges.Images(_DR.Item("months_to").ToString)
                            .ImageScaleMode = TileItemImageScaleMode.Squeeze
                            .ImageToTextAlignment = TileControlImageToTextAlignment.Left

                        Case EnumBar.Area

                            AddHandler _i.ItemClick, AddressOf AreaItemClick

                            .TextAlignment = TileItemContentAlignment.TopLeft
                            .Text = _DR.Item("short_name").ToString

                            .Elements(0).Appearance.Normal.FontSizeDelta = 1

                            Dim _BackColour As Color = ReturnColour(_DR.Item("colour").ToString)
                            .AppearanceItem.Normal.BackColor = _BackColour

                            Dim _e As New TileItemElement

                            With _e
                                .Text = _DR.Item("aspect").ToString
                                .TextAlignment = TileItemContentAlignment.BottomLeft
                                .Appearance.Normal.FontSizeDelta = -4
                                .Appearance.Normal.BackColor = _BackColour
                            End With

                            _i.Elements.Add(_e)

                        Case EnumBar.Item

                            AddHandler _i.ItemDoubleClick, AddressOf ItemDoubleClick

                            .TextAlignment = TileItemContentAlignment.TopLeft
                            .Text = _DR.Item("statement").ToString

                            '.AppearanceItem.Normal.BackColor = txtRed.BackColor

                            .Elements(0).Appearance.Normal.FontSizeDelta = -2

                            'Dim _Add As New TileItemElement
                            'With _Add
                            '    .Image = ic.Images("add")
                            '    .ImageAlignment = TileItemContentAlignment.TopRight
                            'End With

                            'Dim _Remove As New TileItemElement
                            'With _Remove
                            '    .Image = ic.Images("remove")
                            '    .ImageAlignment = TileItemContentAlignment.BottomRight
                            'End With

                            Dim _Count As New TileItemElement
                            With _Count
                                .TextAlignment = TileItemContentAlignment.MiddleRight
                                .Appearance.Normal.FontSizeDelta = 28
                                .Text = "0"
                            End With

                            Dim _Status As New TileItemElement
                            With _Status
                                .TextAlignment = TileItemContentAlignment.MiddleRight
                                .TextLocation = New Point(-50, 0)
                                .Appearance.Normal.FontSizeDelta = 0
                                .Text = "Emerging"
                            End With

                            '_i.Elements.Add(_Add)
                            '_i.Elements.Add(_Remove)

                            _i.Elements.Add(_Count)
                            _i.Elements.Add(_Status)

                    End Select

                End With

                BarGroup.Items.Add(_i)

            Next

        End If


    End Sub

    Private Function ReturnColour(ByVal ColourIn As String) As Color

        If ColourIn = "" Then Return Nothing

        Dim _RGB As String() = ColourIn.Split(",")
        If _RGB.Count = 3 Then
            Return Color.FromArgb(CInt(_RGB(0)), CInt(_RGB(1)), CInt(_RGB(2)))
        End If

    End Function

    Private Sub AgeItemClick(sender As Object, e As TileItemEventArgs)
        m_SelectedAgeID = e.Item.Name.Substring(4)
        DisplayItems()
    End Sub

    Private Sub AreaItemClick(sender As Object, e As TileItemEventArgs)
        m_SelectedAreaID = e.Item.Name.Substring(4)
        DisplayItems()
    End Sub

    Private Sub ItemDoubleClick(sender As Object, e As TileItemEventArgs)
        If Msgbox("Are you sure you want to select the following item?" + vbCrLf + vbCrLf + e.Item.Text + vbCrLf, MessageBoxButtons.YesNo, MessageBoxIcon.Question, "Confirm item") Then
            m_SelectedItems.Add(New Pair(e.Item.Name.Substring(4), e.Item.Text))
            If m_SelectedItems.Count = 1 Then
                lblSelected.Text = "1 item selected"
            Else
                lblSelected.Text = m_SelectedItems.Count.ToString + " items selected"
            End If
        End If
    End Sub

    Private Sub DisplayItems()

        If m_SelectedAreaID = "" Then Exit Sub

        Dim _SQL As String = ""

        If m_DisplayMode = EnumDisplayMode.DevelopmentMatterStatements Then

            If m_SelectedAgeID = "" Then Exit Sub

            _SQL += "select id, statement from EYStatements"
            _SQL += " where area_id = '" + m_SelectedAreaID + "'"
            _SQL += " and age_id = '" + m_SelectedAgeID + "'"
            _SQL += " order by display_seq"

        Else

            _SQL += "select id, statement from EYStatements"
            _SQL += " where area_id = '" + m_SelectedAreaID + "'"
            _SQL += " order by display_seq"

        End If

        PopulateTileBar(tbItemsGroup, _SQL, EnumBar.Item)

    End Sub

    Private Sub frmStatementPicker_ResizeEnd(sender As Object, e As EventArgs) Handles Me.Resize
        tbItems.WideTileWidth = tbItems.Width - 50
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click

        If m_SelectedItems.Count = 0 Then
            Msgbox("You have not selected any items...", MessageBoxIcon.Exclamation, "Accept")
            Exit Sub
        End If

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()

    End Sub

End Class