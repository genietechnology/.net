﻿Imports NurseryTablet.SharedModule

Public Class frmMedicalMain

    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub btnMedicationRequest_Click(sender As System.Object, e As System.EventArgs) Handles btnMedicationRequest.Click
        btnMedicationRequest.Enabled = False
        ChildMedicalAuth()
        btnMedicationRequest.Enabled = True
    End Sub

    Private Sub btnLogMedicine_Click(sender As Object, e As EventArgs) Handles btnLogMedicine.Click

        btnLogMedicine.Enabled = False

        Dim _Return As Pair = Business.Child.FindChildByGroup(Enums.PersonMode.OnlyCheckedIn)
        If _Return IsNot Nothing Then
            If _Return.Code <> "" Then
                SharedModule.LogMedicine(frmMedicalLog.EnumMode.CreateLog, _Return.Code)
            End If
        End If

        btnLogMedicine.Enabled = True

    End Sub

    Private Sub btnLogAccident_Click(sender As Object, e As EventArgs) Handles btnLogAccident.Click
        btnLogAccident.Enabled = False
        SharedModule.ChildIncidents()
        btnLogAccident.Enabled = True
    End Sub

    Private Sub btnSuncream_Click(sender As Object, e As EventArgs) Handles btnSuncream.Click
        btnSuncream.Enabled = False
        SharedModule.ChildSuncream()
        btnSuncream.Enabled = True
    End Sub
End Class
