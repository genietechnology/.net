﻿Imports NurseryTablet.SharedModule

Public Class frmEYFSAss

    Public Enum EnumKeyType
        Observation
        Assessment
    End Enum

    Public Enum EnumListType
        DevelopmentMatters
        COEL
    End Enum

    Private m_ChildID As String = ""

    Private m_KeyType As EnumKeyType
    Private m_KeyID As String = ""

    Private m_ObsID As String = ""
    Private m_AssID As String = ""

    Private m_DMs As New List(Of Pair)
    Private m_COELs As New List(Of Pair)

    Public Sub New(ByVal ChildID As String, ByVal KeyType As EnumKeyType, ByVal KeyID As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_ChildID = ChildID
        m_KeyType = KeyType
        m_KeyID = KeyID

    End Sub

    Private Sub frmEYFSAss_Load(sender As Object, e As EventArgs) Handles Me.Load

        Me.Cursor = Cursors.WaitCursor
        Application.DoEvents()

        btnAdd.Hide()
        btnDelete.Hide()

        fldChild.ValueID = m_ChildID
        fldChild.ValueText = Business.ChildHandler.ReturnName(m_ChildID)

        If m_KeyType = EnumKeyType.Observation Then

            m_ObsID = m_KeyID
            m_AssID = Guid.NewGuid.ToString

            fldStatus.ValueText = "Draft"
            radWB3.Checked = True
            radI3.Checked = True

        Else
            m_AssID = m_KeyID
            DisplayAssessment()
        End If

        Me.Cursor = Cursors.Default
        Application.DoEvents()

    End Sub

    Private Sub DisplayAssessment()

        Dim _SQL As String

        _SQL = "select * from Ass" & _
               " where id = '" & m_AssID & "'"

        Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
        If Not _DR Is Nothing Then

            m_ObsID = _DR("obs_id").ToString

            fldAssessor.ValueID = _DR("staff_id").ToString
            fldAssessor.ValueText = Business.Staff.ReturnName(fldAssessor.ValueID)

            fldDateTime.ValueText = Format(CDate(_DR("ass_date")), "dd/MM/yyyy HH:mm")
            fldStatus.ValueText = _DR("ass_status").ToString

            If _DR("ls_involvement").ToString = "1" Then radI1.Checked = True
            If _DR("ls_involvement").ToString = "2" Then radI2.Checked = True
            If _DR("ls_involvement").ToString = "3" Then radI3.Checked = True
            If _DR("ls_involvement").ToString = "4" Then radI4.Checked = True
            If _DR("ls_involvement").ToString = "5" Then radI5.Checked = True

            If _DR("ls_wellbeing").ToString = "1" Then radWB1.Checked = True
            If _DR("ls_wellbeing").ToString = "2" Then radWB2.Checked = True
            If _DR("ls_wellbeing").ToString = "3" Then radWB3.Checked = True
            If _DR("ls_wellbeing").ToString = "4" Then radWB4.Checked = True
            If _DR("ls_wellbeing").ToString = "5" Then radWB5.Checked = True

            fldComments.ValueText = _DR("comments").ToString
            txtComments.Text = _DR("comments").ToString

            fldNextSteps.ValueText = _DR("next_steps").ToString
            txtNextSteps.Text = _DR("next_steps").ToString

            _DR = Nothing

            InitialiseList(EnumListType.DevelopmentMatters)
            PopulateDM()

            InitialiseList(EnumListType.COEL)
            PopulateCOEL()

        End If

    End Sub

    Private Sub InitialiseList(ByVal ListType As EnumListType)

        Dim _SQL As String = ""

        _SQL += "select s.id, s.statement from AssStatements ass"
        _SQL += " left join EYStatements s on s.ID = ass.statement_id"
        _SQL += " left join EYAreas a on a.ID = s.area_id"
        _SQL += " where ass.ass_id = '" + m_AssID + "'"

        If ListType = EnumListType.DevelopmentMatters Then
            _SQL += " and a.type in ('P','S')"
            m_DMs.Clear()
        Else
            _SQL += " and a.type in ('COEL')"
            m_COELs.Clear()
        End If

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If Not _DT Is Nothing Then

            For Each _DR As DataRow In _DT.Rows
                If ListType = EnumListType.DevelopmentMatters Then
                    m_DMs.Add(New Pair(_DR.Item("id").ToString, _DR.Item("statement").ToString))
                Else
                    m_COELs.Add(New Pair(_DR.Item("id").ToString, _DR.Item("statement").ToString))
                End If
            Next

            _DT.Dispose()
            _DT = Nothing

        End If

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click

        Select Case tc.SelectedTabPage.Name

            Case "tabDM"

                Dim _frm As New frmDMStatements(frmDMStatements.EnumDisplayMode.DevelopmentMatterStatements)
                _frm.ShowDialog()

                If _frm.DialogResult = Windows.Forms.DialogResult.OK Then

                    m_DMs = _frm.SelectedItems

                    _frm.Dispose()
                    _frm = Nothing

                    PopulateDM()

                End If

            Case "tabCOEL"

                Dim _frm As New frmDMStatements(frmDMStatements.EnumDisplayMode.COEL)
                _frm.ShowDialog()

                If _frm.DialogResult = Windows.Forms.DialogResult.OK Then

                    m_COELs = _frm.SelectedItems

                    _frm.Dispose()
                    _frm = Nothing

                    PopulateCOEL()

                End If

        End Select

    End Sub

    Private Sub PopulateDM()
        tgDM.HideFirstColumn = True
        tgDM.Populate(m_DMs)
        tgDM.Columns(1).Caption = "Selected Statements"
    End Sub

    Private Sub PopulateCOEL()
        tgCOEL.HideFirstColumn = True
        tgCOEL.Populate(m_COELs)
        tgCOEL.Columns(1).Caption = "Selected Characteristics"
    End Sub

    Private Sub tc_SelectedPageChanged(sender As Object, e As DevExpress.XtraTab.TabPageChangedEventArgs) Handles tc.SelectedPageChanged

        Select Case e.Page.Name

            Case "tabDM"
                btnAdd.Show()
                btnDelete.Show()

            Case "tabCOEL"
                btnAdd.Show()
                btnDelete.Show()

            Case Else
                btnAdd.Hide()
                btnDelete.Hide()

        End Select

    End Sub

    Private Sub fldAssessor_ButtonClick(sender As Object, e As EventArgs) Handles fldAssessor.ButtonClick
        Dim _Return As Pair = Business.Staff.FindStaff(Enums.PersonMode.OnlyCheckedIn)
        If Not _Return Is Nothing AndAlso _Return.Code <> "" Then
            fldAssessor.ValueID = _Return.Code
            fldAssessor.ValueText = _Return.Text
        End If
    End Sub

    Private Sub fldStatus_ButtonClick(sender As Object, e As EventArgs) Handles fldStatus.ButtonClick

        Dim _Buttons As New List(Of Pair)
        _Buttons.Add(New Pair("Draft", "Draft"))
        _Buttons.Add(New Pair("Complete", "Complete"))
        _Buttons.Add(New Pair("Publish", "Publish"))

        Dim _Return As Pair = SharedModule.ReturnButtonSQL(_Buttons, "Change Status")
        If Not _Return Is Nothing AndAlso _Return.Code <> "" Then
            fldStatus.ValueID = _Return.Code
            fldStatus.ValueText = _Return.Text
        End If

    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click

        btnAccept.Enabled = False
        Me.Cursor = Cursors.WaitCursor

        If Save() Then
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        End If

        btnAccept.Enabled = True
        Me.Cursor = Cursors.Default

    End Sub

    Private Function Save() As Boolean

        If Not CheckAnswers() Then Return False

        Dim _SQL As String

        _SQL = "select * from Ass" & _
               " where ID = '" & m_AssID.ToString & "'"

        Dim _DA As IDataAdapter = DAL.ReturnDataAdapter(_SQL)
        If Not _DA Is Nothing Then

            Dim _DR As DataRow
            Dim _DS As New DataSet
            _DA.Fill(_DS)

            If _DS.Tables(0).Rows.Count = 0 Then
                _DR = _DS.Tables(0).NewRow
                _DR("id") = m_ObsID
                _DR("day_id") = New Guid(TodayID)
                _DR("child_id") = m_ChildID
                _DR("obs_id") = m_ObsID
            Else
                _DR = _DS.Tables(0).Rows(0)
            End If

            _DR("staff_id") = fldAssessor.ValueID
            _DR("ass_date") = fldDateTime.ValueText
            _DR("ass_status") = fldStatus.ValueText
            _DR("comments") = fldComments.ValueText

            Dim _Scale As Integer = 0

            If radI1.Checked Then _Scale = 1
            If radI2.Checked Then _Scale = 2
            If radI3.Checked Then _Scale = 3
            If radI4.Checked Then _Scale = 4
            If radI5.Checked Then _Scale = 5

            _DR("ls_involvement") = _Scale
            _DR("ls_involvement_desc") = ReturnScaleDesc(_Scale)

            _Scale = 0
            If radWB1.Checked Then _Scale = 1
            If radWB2.Checked Then _Scale = 2
            If radWB3.Checked Then _Scale = 3
            If radWB4.Checked Then _Scale = 4
            If radWB5.Checked Then _Scale = 5

            _DR("ls_wellbeing") = _Scale
            _DR("ls_wellbeing_desc") = ReturnScaleDesc(_Scale)

            _DR("next_steps") = fldNextSteps.ValueText

            _DR("stamp") = Now

            If _DS.Tables(0).Rows.Count = 0 Then _DS.Tables(0).Rows.Add(_DR)

            DAL.UpdateDB(_DA, _DS)

            SaveStatements()

        End If

        Return True

    End Function

    Private Function ReturnScaleDesc(ByVal Scale As Integer) As String
        If Scale = 1 Then Return "Extremely Low"
        If Scale = 2 Then Return "Low"
        If Scale = 3 Then Return "Moderate"
        If Scale = 4 Then Return "High"
        If Scale = 5 Then Return "Extremely High"
        Return ""
    End Function

    Private Sub SaveStatements()

        Dim _SQL As String = ""

        _SQL = "delete from AssStatements where ass_id = '" + m_AssID + "'"
        DAL.ExecuteCommand(_SQL)

        For Each _p In m_DMs
            InsertStatement(_p.Code)
        Next

        For Each _p In m_COELs
            InsertStatement(_p.Code)
        Next

    End Sub

    Private Sub InsertStatement(ByVal StatementID As String)

        Dim _SQL As String = ""
        _SQL += "insert into AssStatements values"
        _SQL += " ("
        _SQL += "'" + Guid.NewGuid.ToString + "',"
        _SQL += "'" + m_AssID + "',"
        _SQL += "'" + StatementID + "'"
        _SQL += ")"

        DAL.ExecuteCommand(_SQL)

    End Sub

    Private Function CheckAnswers() As Boolean

        If fldAssessor.ValueText = "" Then
            Msgbox("Please select the Assessor.", MessageBoxIcon.Exclamation, "Save Assessment")
            Return False
        End If

        If fldDateTime.ValueText = "" Then
            Msgbox("Please select the Date and Time of the Assessment.", MessageBoxIcon.Exclamation, "Save Assessment")
            Return False
        End If

        If fldComments.ValueText = "" Then
            Msgbox("Please enter comments.", MessageBoxIcon.Exclamation, "Save Assessment")
            Return False
        End If

        If fldNextSteps.ValueText = "" Then
            If Msgbox("You have not selected any next steps - Do you want to Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Save Assessment") = Windows.Forms.DialogResult.No Then
                Return False
            End If
        End If

        If m_DMs.Count = 0 Then
            Msgbox("You have not selected any Development Matter Statements.", MessageBoxIcon.Exclamation, "Save Assessment")
            Return False
        End If

        If m_COELs.Count = 0 Then
            If Msgbox("You have not selected any COEL items - Do you want to Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Save Assessment") = Windows.Forms.DialogResult.No Then
                Return False
            End If
        End If

        Return True

    End Function

    Private Sub fldComments_AfterButtonClick(sender As Object, e As EventArgs) Handles fldComments.AfterButtonClick
        txtComments.Text = fldComments.ValueText
    End Sub

    Private Sub fldNextSteps_AfterButtonClick(sender As Object, e As EventArgs) Handles fldNextSteps.AfterButtonClick
        txtNextSteps.Text = fldNextSteps.ValueText
    End Sub

End Class