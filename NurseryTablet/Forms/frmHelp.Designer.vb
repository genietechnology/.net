﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHelp
    Inherits NurseryTablet.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnGotoAssist = New DevExpress.XtraEditors.SimpleButton()
        Me.btnPingService = New DevExpress.XtraEditors.SimpleButton()
        Me.btnScreenConnect = New DevExpress.XtraEditors.SimpleButton()
        Me.btnClose = New DevExpress.XtraEditors.SimpleButton()
        Me.btnPingGoogle = New DevExpress.XtraEditors.SimpleButton()
        Me.btnHelpCentre = New DevExpress.XtraEditors.SimpleButton()
        Me.btnIPConfig = New DevExpress.XtraEditors.SimpleButton()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        '
        'btnGotoAssist
        '
        Me.btnGotoAssist.Location = New System.Drawing.Point(15, 109)
        Me.btnGotoAssist.Margin = New System.Windows.Forms.Padding(6)
        Me.btnGotoAssist.Name = "btnGotoAssist"
        Me.btnGotoAssist.Size = New System.Drawing.Size(360, 90)
        Me.btnGotoAssist.TabIndex = 7
        Me.btnGotoAssist.Text = "Launch Goto Assist"
        '
        'btnPingService
        '
        Me.btnPingService.Location = New System.Drawing.Point(381, 13)
        Me.btnPingService.Name = "btnPingService"
        Me.btnPingService.Size = New System.Drawing.Size(360, 90)
        Me.btnPingService.TabIndex = 15
        Me.btnPingService.Text = "Ping Service"
        '
        'btnScreenConnect
        '
        Me.btnScreenConnect.Location = New System.Drawing.Point(15, 204)
        Me.btnScreenConnect.Name = "btnScreenConnect"
        Me.btnScreenConnect.Size = New System.Drawing.Size(360, 90)
        Me.btnScreenConnect.TabIndex = 11
        Me.btnScreenConnect.Text = "Launch Screen Connect"
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(15, 300)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(727, 90)
        Me.btnClose.TabIndex = 22
        Me.btnClose.Text = "Close"
        '
        'btnPingGoogle
        '
        Me.btnPingGoogle.Location = New System.Drawing.Point(381, 109)
        Me.btnPingGoogle.Name = "btnPingGoogle"
        Me.btnPingGoogle.Size = New System.Drawing.Size(360, 90)
        Me.btnPingGoogle.TabIndex = 4
        Me.btnPingGoogle.Text = "Ping Google"
        '
        'btnHelpCentre
        '
        Me.btnHelpCentre.Location = New System.Drawing.Point(15, 13)
        Me.btnHelpCentre.Name = "btnHelpCentre"
        Me.btnHelpCentre.Size = New System.Drawing.Size(360, 90)
        Me.btnHelpCentre.TabIndex = 3
        Me.btnHelpCentre.Text = "Launch Speed Test"
        '
        'btnIPConfig
        '
        Me.btnIPConfig.Location = New System.Drawing.Point(381, 205)
        Me.btnIPConfig.Name = "btnIPConfig"
        Me.btnIPConfig.Size = New System.Drawing.Size(360, 90)
        Me.btnIPConfig.TabIndex = 78
        Me.btnIPConfig.Text = "IP Config"
        '
        'frmHelp
        '
        Me.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.Appearance.Options.UseBackColor = True
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(754, 398)
        Me.Controls.Add(Me.btnIPConfig)
        Me.Controls.Add(Me.btnGotoAssist)
        Me.Controls.Add(Me.btnPingService)
        Me.Controls.Add(Me.btnScreenConnect)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnPingGoogle)
        Me.Controls.Add(Me.btnHelpCentre)
        Me.Name = "frmHelp"
        Me.Text = "Help Menu"
        Me.Controls.SetChildIndex(Me.btnHelpCentre, 0)
        Me.Controls.SetChildIndex(Me.btnPingGoogle, 0)
        Me.Controls.SetChildIndex(Me.btnClose, 0)
        Me.Controls.SetChildIndex(Me.btnScreenConnect, 0)
        Me.Controls.SetChildIndex(Me.btnPingService, 0)
        Me.Controls.SetChildIndex(Me.btnGotoAssist, 0)
        Me.Controls.SetChildIndex(Me.btnIPConfig, 0)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnPingGoogle As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnHelpCentre As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnPingService As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnScreenConnect As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnGotoAssist As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnIPConfig As DevExpress.XtraEditors.SimpleButton

End Class
