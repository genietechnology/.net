﻿Imports NurseryTablet.SharedModule

Public Class frmChildInfo

    Private m_ChildID As String
    Private m_ChildName As String
    Private m_ChildPair As New SharedModule.Pair
    Private m_ChildInOut As Enums.InOut
    Private m_FamilyID As String = ""
    Private m_Location As String = ""

    Private m_Feedback As Boolean = False
    Private m_FeedbackText As String = ""

    Private m_CrossChecked As Boolean = False

    Private m_Basic As New List(Of ChildBasic)
    Private m_Medical As New List(Of ChildMedical)

    Private Class ChildBasic
        Property Forename() As String
        Property Surname() As String
        Property DOB() As Date
        Property Gender As String
        Property Group() As String
        Property Nappies() As Boolean
    End Class

    Private Class ChildMedical
        Property SEN() As Boolean
        Property DietRestriction() As String
        Property DietNotes() As String
        Property AllergyRating() As String
        Property AllergyNotes() As String
        Property Medication() As String
        Property MedicalNotes() As String
        Property Doctor() As String
        Property Surgery() As String
        Property SurgeryTel() As String
        Property HealthVisitor() As String
        Property HealthVisitorTel() As String
        Property HealthVisitorMobile() As String
    End Class

    Public Sub New(ByVal ChildID As String, ByVal ChildName As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_ChildID = ChildID
        m_ChildName = ChildName

    End Sub

    Private Sub frmChild_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        DisplayDetails()
    End Sub

    Private Sub DisplayDetails()

        Dim _ChildRecord As NurseryGenieData.Child = Business.Child.ReturnChildByID(m_ChildID)
        With _ChildRecord

            Dim _C As New ChildBasic
            _C.Forename = .Forename
            _C.Surname = .Surname
            _C.DOB = .DOB
            _C.Gender = .Gender
            _C.Group = .GroupName
            _C.Nappies = .Nappies

            m_Basic.Add(_C)
            vgBasic.DataSource = m_Basic

            Dim _M As New ChildMedical
            '_M.SEN = .SEN
            _M.AllergyRating = .AllergyRating
            _M.DietRestriction = .DietRestrict
            _M.DietNotes = .DietNotes
            _M.AllergyRating = .AllergyRating
            _M.AllergyNotes = .AllergyNotes
            _M.Medication = .Medication
            _M.MedicalNotes = .MedicalNotes
            _M.Doctor = .Doctor
            _M.Surgery = .DoctorSurgery
            _M.SurgeryTel = .DoctorTel
            _M.HealthVisitor = .HVName
            _M.HealthVisitorTel = .HVTel
            _M.HealthVisitorMobile = .HVMobile

            m_Medical.Add(_M)
            vgMedical.DataSource = m_Medical

        End With

        AllergyMatrix()

    End Sub

    Private Sub AllergyMatrix()

        'Dim _SQL As String = ""
        '_SQL += "select AppListItems.name as 'Allergy', cast(iif(ChildAttrib.attribute = AppListItems.name, 1, 0) as bit) as ' ' from AppListItems"
        '_SQL += " left join AppLists on AppLists.ID = AppListItems.list_id"
        '_SQL += " left join ChildAttrib on ChildAttrib.category = 'Allergies'"
        '_SQL += " and ChildAttrib.child_id = '" + m_ChildID + "'"
        '_SQL += " and ChildAttrib.attribute = AppListItems.name"
        '_SQL += " where AppLists.name = 'Allergy Matrix'"
        '_SQL += " order by AppListItems.seq, AppListItems.name"

        'tgAllergiesConsent.Clear()
        'tgAllergiesConsent.Populate(_SQL)

    End Sub

    Private Sub ConsentMatrix()

        'Dim _SQL As String = ""

        '_SQL += "select i.name as 'Consent', isnull(c.given, 0) as ' ' from AppListItems i"
        '_SQL += " left join AppLists l on l.ID = i.list_id"
        '_SQL += " left join ChildConsent c on c.consent = i.name and c.child_id = '" + m_ChildID + "'"
        '_SQL += " where l.name = 'Consent'"
        '_SQL += " order by i.ref_1, i.name"

        'tgAllergiesConsent.Clear()
        'tgAllergiesConsent.Populate(_SQL)

    End Sub

    Private Sub btnAllergyMatrix_Click(sender As Object, e As EventArgs) Handles btnAllergyMatrix.Click
        AllergyMatrix()
    End Sub

    Private Sub btnConsentMatrix_Click(sender As Object, e As EventArgs) Handles btnConsentMatrix.Click
        ConsentMatrix()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

End Class
