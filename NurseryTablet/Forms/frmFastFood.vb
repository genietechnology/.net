﻿Option Strict On

Imports DevExpress.XtraBars.Navigation
Imports DevExpress.XtraEditors
Imports NurseryTablet.SharedModule
Imports NurseryTablet.NurseryGenieData

Public Class frmFastFood

    Private m_SelectedMealID As String = ""
    Private m_SelectedMealType As String = ""
    Private m_SelectedMealName As String = ""
    Private m_SelectedLocation As String = ""

    Private Sub frmFastFood_Load(sender As Object, e As EventArgs) Handles Me.Load

        SetupMeals()
        SetMeal("B")

        PopulateGroups()

    End Sub

    Private Sub frmFastFood_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        tbRooms.WideTileWidth = tbRooms.Width - 40
    End Sub

    Private Sub PopulateChildren()

        Me.Cursor = Cursors.WaitCursor
        Application.DoEvents()

        sc.Controls.Clear()

        Dim _Checked As List(Of NurseryGenieData.Child) = Business.Child.GetCheckedIn
        If _Checked IsNot Nothing Then
            For Each _C As NurseryGenieData.Child In _Checked
                Dim _MealStatus As String = ReturnMealStatus(_C.ID.ToString)
                AddChild(_C.ID.ToString, _C.FullName, _MealStatus, _C.AllergyRating, _C.AllergyNotes, _C.Medication, _C.MedicalNotes)
            Next
        End If

        Me.Cursor = Cursors.Default
        Application.DoEvents()

    End Sub

    Private Sub AddChild(ByVal ChildID As String, ByVal ChildName As String, ByVal MealStatus As String, ByVal AllergyRating As String, ByVal MedAllergies As String, ByVal MedMedication As String, ByVal MedNotes As String)
        Dim _Item As New FoodControl(ChildID, ChildName, MealStatus)
        _Item.Dock = DockStyle.Top
        _Item.BringToFront()
        sc.Controls.Add(_Item)
    End Sub

    Private Function ReturnMealStatus(ByVal ChildID As String) As String

        Dim _Return As String = "0"

        Dim _Status = From _FR As FoodRegister In DataBase.Tables.FoodRegister _
                      Where _FR.DayID = New Guid(TodayID) _
                      And _FR.ChildID = New Guid(ChildID) _
                      And _FR.MealType = m_SelectedMealType _
                      And _FR.MealID = New Guid(m_SelectedMealID)

        If _Status IsNot Nothing Then
            If _Status.Count = 1 Then
                _Return = _Status.First.FoodStatus.ToString
            End If
        End If

        Return _Return

    End Function

    Private Sub PopulateGroups()

        PopulateTileBar(Business.Rooms.GetRoomsAsPair)

        If Parameters.DefaultRoom <> "" Then
            For Each _i As TileBarItem In tbRoomGroup.Items
                Dim _Text As String = _i.Elements(0).Text
                If _Text = Parameters.DefaultRoom Then
                    tbRooms.SelectedItem = _i
                    _i.PerformItemClick()
                    Exit Sub
                End If
            Next
        Else
            For Each _i As TileBarItem In tbRoomGroup.Items
                tbRooms.SelectedItem = _i
                _i.PerformItemClick()
                Exit Sub
            Next
        End If

    End Sub

    Private Sub PopulateTileBar(ByVal Pairs As List(Of Pair))

        tbRoomGroup.Items.Clear()

        For Each _P In Pairs

            Dim _i As New TileBarItem
            With _i

                .Name = "t_" + _P.Code

                AddHandler _i.ItemClick, AddressOf GroupItemClick

                .TextAlignment = TileItemContentAlignment.TopLeft
                .AppearanceItem.Normal.ForeColor = Color.Black
                .AppearanceItem.Normal.BackColor = txtLightYellow.BackColor
                .AppearanceItem.Selected.BackColor = txtYellow.BackColor
                .Text = _P.Text

            End With

            tbRoomGroup.Items.Add(_i)

        Next

    End Sub

    Private Sub GroupItemClick(sender As Object, e As TileItemEventArgs)
        m_SelectedLocation = e.Item.Text
        Save()
        PopulateChildren()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click

        If ValidateEntry() Then

            Dim _StaffID As String = ""
            Dim _StaffName As String = ""

            If Parameters.CurrentStaffID <> "" Then
                _StaffID = Parameters.CurrentStaffID
                _StaffName = Parameters.CurrentStaffName
            Else
                Dim _Staff As Pair = Business.Staff.FindStaff(Enums.PersonMode.OnlyCheckedIn)
                If _Staff IsNot Nothing Then
                    _StaffID = _Staff.Code
                    _StaffName = _Staff.Text
                End If
            End If

            If _StaffID <> "" Then
                Save()
                Me.DialogResult = Windows.Forms.DialogResult.OK
                Me.Close()
            End If

        End If

    End Sub

    Private Function ValidateEntry() As Boolean
        Return True
    End Function

    Private Sub Save()

        If m_SelectedMealID = "" Then Exit Sub
        If m_SelectedMealType = "" Then Exit Sub
        If m_SelectedMealName = "" Then Exit Sub

        For Each _fc As FoodControl In sc.Controls

            LogFood(Parameters.TodayID, _fc.FoodID, _fc.FoodName,
                     m_SelectedMealID, m_SelectedMealType, m_SelectedMealName,
                     m_SelectedMealID, m_SelectedMealName, _fc.FoodStatus)

        Next

    End Sub

    Private Sub SetupMeals()

        Dim _Day As NurseryGenieData.Day = DataBase.DayRecord
        If _Day IsNot Nothing Then

            Dim _BreakfastID As String = _Day.BreakfastId.ToString
            If _BreakfastID <> "" Then
                btnBreakfast.Enabled = True
                btnBreakfast.Tag = _BreakfastID
                btnBreakfast.AccessibleDescription = _Day.BreakfastName
            Else
                btnBreakfast.Enabled = False
            End If

            Dim _SnackID As String = _Day.SnackId.ToString
            If _SnackID <> "" Then
                btnAM.Enabled = True
                btnAM.Tag = _SnackID
                btnAM.AccessibleDescription = _Day.SnackName
            Else
                btnAM.Enabled = False
            End If

            Dim _LunchID As String = _Day.LunchId.ToString
            If _LunchID <> "" Then
                btnLunch.Enabled = True
                btnLunch.Tag = _LunchID
                btnLunch.AccessibleDescription = _Day.LunchName
            Else
                btnLunch.Enabled = False
            End If

            Dim _LunchDessertID As String = _Day.LunchDesId.ToString
            If _LunchDessertID <> "" Then
                btnLunchDessert.Enabled = True
                btnLunchDessert.Tag = _LunchDessertID
                btnLunchDessert.AccessibleDescription = _Day.LunchDesName
            Else
                btnLunchDessert.Enabled = False
            End If

            Dim _SnackPMID As String = _Day.PMSnackId.ToString
            If _SnackPMID <> "" Then
            btnPM.Enabled = True
            btnPM.Tag = _SnackPMID
                btnPM.AccessibleDescription = _Day.PMSnackName
        Else
            btnPM.Enabled = False
        End If

        Dim _TeaID As String = _Day.TeaId.ToString
        If _TeaID <> "" Then
            btnTea.Enabled = True
            btnTea.Tag = _TeaID
            btnTea.AccessibleDescription = _Day.TeaName
        Else
            btnTea.Enabled = False
        End If

        Dim _TeaDessertID As String = _Day.TeaDesId.ToString
        If _TeaDessertID <> "" Then
            btnTeaDessert.Enabled = True
            btnTeaDessert.Tag = _TeaDessertID
            btnTeaDessert.AccessibleDescription = _Day.TeaDesName
        Else
            btnTeaDessert.Enabled = False
        End If

        End If

    End Sub

    Private Sub btnBreakfast_Click(sender As Object, e As EventArgs) Handles btnBreakfast.Click
        SetMeal("B")
    End Sub

    Private Sub btnAM_Click(sender As Object, e As EventArgs) Handles btnAM.Click
        SetMeal("S")
    End Sub

    Private Sub btnLunch_Click(sender As Object, e As EventArgs) Handles btnLunch.Click
        SetMeal("L")
    End Sub

    Private Sub btnLunchDessert_Click(sender As Object, e As EventArgs) Handles btnLunchDessert.Click
        SetMeal("LD")
    End Sub

    Private Sub btnPM_Click(sender As Object, e As EventArgs) Handles btnPM.Click
        SetMeal("SP")
    End Sub

    Private Sub btnTea_Click(sender As Object, e As EventArgs) Handles btnTea.Click
        SetMeal("T")
    End Sub

    Private Sub btnTeaDessert_Click(sender As Object, e As EventArgs) Handles btnTeaDessert.Click
        SetMeal("TD")
    End Sub

    Private Function CleanString(ByVal ObjectIn As Object) As String
        If ObjectIn Is Nothing Then
            Return ""
        Else
            Return ObjectIn.ToString
        End If
    End Function

    Private Sub SetMeal(ByVal MealType As String)

        Save()

        m_SelectedMealType = MealType
        ResetMealButtons()

        Select Case MealType

            Case "B"
                m_SelectedMealID = CleanString(btnBreakfast.Tag)
                m_SelectedMealName = btnBreakfast.AccessibleDescription
                btnBreakfast.Appearance.BackColor = txtYellow.BackColor

            Case "S"
                m_SelectedMealID = CleanString(btnAM.Tag)
                m_SelectedMealName = btnAM.AccessibleDescription
                btnAM.Appearance.BackColor = txtYellow.BackColor

            Case "L"
                m_SelectedMealID = CleanString(btnLunch.Tag)
                m_SelectedMealName = btnLunch.AccessibleDescription
                btnLunch.Appearance.BackColor = txtYellow.BackColor

            Case "LD"
                m_SelectedMealID = CleanString(btnLunchDessert.Tag)
                m_SelectedMealName = btnLunchDessert.AccessibleDescription
                btnLunchDessert.Appearance.BackColor = txtYellow.BackColor

            Case "SP"
                m_SelectedMealID = CleanString(btnPM.Tag)
                m_SelectedMealName = btnPM.AccessibleDescription
                btnPM.Appearance.BackColor = txtYellow.BackColor

            Case "T"
                m_SelectedMealID = CleanString(btnTea.Tag)
                m_SelectedMealName = btnTea.AccessibleDescription
                btnTea.Appearance.BackColor = txtYellow.BackColor

            Case "TD"
                m_SelectedMealID = CleanString(btnTeaDessert.Tag)
                m_SelectedMealName = btnTeaDessert.AccessibleDescription
                btnTeaDessert.Appearance.BackColor = txtYellow.BackColor

        End Select

        lblMealDetails.Text = m_SelectedMealName
        PopulateChildren()

    End Sub

    Private Sub ResetMealButtons()
        btnBreakfast.Appearance.Reset()
        btnAM.Appearance.Reset()
        btnLunch.Appearance.Reset()
        btnLunchDessert.Appearance.Reset()
        btnPM.Appearance.Reset()
        btnTea.Appearance.Reset()
        btnTeaDessert.Appearance.Reset()
    End Sub

End Class