﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLDAssessment
    Inherits NurseryTablet.frmBaseChild

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLDAssessment))
        Me.gbxMilk = New System.Windows.Forms.GroupBox()
        Me.btnComments = New System.Windows.Forms.Button()
        Me.txtComments = New System.Windows.Forms.TextBox()
        Me.btnStep = New System.Windows.Forms.Button()
        Me.lblPhotoNumber = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnPhotoNumber = New System.Windows.Forms.Button()
        Me.btnArea = New System.Windows.Forms.Button()
        Me.lblStep = New System.Windows.Forms.Label()
        Me.lblArea = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblStepLabel = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnPhoto = New System.Windows.Forms.Button()
        Me.gbxMilk.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxMilk
        '
        Me.gbxMilk.Controls.Add(Me.btnComments)
        Me.gbxMilk.Controls.Add(Me.txtComments)
        Me.gbxMilk.Controls.Add(Me.btnStep)
        Me.gbxMilk.Controls.Add(Me.lblPhotoNumber)
        Me.gbxMilk.Controls.Add(Me.Label5)
        Me.gbxMilk.Controls.Add(Me.btnPhotoNumber)
        Me.gbxMilk.Controls.Add(Me.btnArea)
        Me.gbxMilk.Controls.Add(Me.lblStep)
        Me.gbxMilk.Controls.Add(Me.lblArea)
        Me.gbxMilk.Controls.Add(Me.Label4)
        Me.gbxMilk.Controls.Add(Me.lblStepLabel)
        Me.gbxMilk.Controls.Add(Me.Label3)
        Me.gbxMilk.Location = New System.Drawing.Point(17, 90)
        Me.gbxMilk.Name = "gbxMilk"
        Me.gbxMilk.Size = New System.Drawing.Size(995, 382)
        Me.gbxMilk.TabIndex = 12
        Me.gbxMilk.TabStop = False
        '
        'btnComments
        '
        Me.btnComments.Image = CType(resources.GetObject("btnComments.Image"), System.Drawing.Image)
        Me.btnComments.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnComments.Location = New System.Drawing.Point(859, 131)
        Me.btnComments.Name = "btnComments"
        Me.btnComments.Size = New System.Drawing.Size(130, 45)
        Me.btnComments.TabIndex = 46
        Me.btnComments.Text = "Enter"
        Me.btnComments.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnComments.UseVisualStyleBackColor = True
        '
        'txtComments
        '
        Me.txtComments.Location = New System.Drawing.Point(275, 131)
        Me.txtComments.Multiline = True
        Me.txtComments.Name = "txtComments"
        Me.txtComments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtComments.Size = New System.Drawing.Size(574, 191)
        Me.txtComments.TabIndex = 45
        '
        'btnStep
        '
        Me.btnStep.Image = CType(resources.GetObject("btnStep.Image"), System.Drawing.Image)
        Me.btnStep.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnStep.Location = New System.Drawing.Point(859, 73)
        Me.btnStep.Name = "btnStep"
        Me.btnStep.Size = New System.Drawing.Size(130, 45)
        Me.btnStep.TabIndex = 44
        Me.btnStep.Text = "Select"
        Me.btnStep.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnStep.UseVisualStyleBackColor = True
        '
        'lblPhotoNumber
        '
        Me.lblPhotoNumber.AutoSize = True
        Me.lblPhotoNumber.Location = New System.Drawing.Point(270, 338)
        Me.lblPhotoNumber.Name = "lblPhotoNumber"
        Me.lblPhotoNumber.Size = New System.Drawing.Size(579, 25)
        Me.lblPhotoNumber.TabIndex = 43
        Me.lblPhotoNumber.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 338)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(136, 25)
        Me.Label5.TabIndex = 42
        Me.Label5.Text = "Photo Number"
        '
        'btnPhotoNumber
        '
        Me.btnPhotoNumber.Image = CType(resources.GetObject("btnPhotoNumber.Image"), System.Drawing.Image)
        Me.btnPhotoNumber.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPhotoNumber.Location = New System.Drawing.Point(859, 328)
        Me.btnPhotoNumber.Name = "btnPhotoNumber"
        Me.btnPhotoNumber.Size = New System.Drawing.Size(130, 45)
        Me.btnPhotoNumber.TabIndex = 41
        Me.btnPhotoNumber.Text = "Enter"
        Me.btnPhotoNumber.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPhotoNumber.UseVisualStyleBackColor = True
        '
        'btnArea
        '
        Me.btnArea.Image = CType(resources.GetObject("btnArea.Image"), System.Drawing.Image)
        Me.btnArea.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnArea.Location = New System.Drawing.Point(859, 22)
        Me.btnArea.Name = "btnArea"
        Me.btnArea.Size = New System.Drawing.Size(130, 45)
        Me.btnArea.TabIndex = 25
        Me.btnArea.Text = "Select"
        Me.btnArea.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnArea.UseVisualStyleBackColor = True
        '
        'lblStep
        '
        Me.lblStep.AutoSize = True
        Me.lblStep.Location = New System.Drawing.Point(270, 83)
        Me.lblStep.Name = "lblStep"
        Me.lblStep.Size = New System.Drawing.Size(579, 25)
        Me.lblStep.TabIndex = 32
        Me.lblStep.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblArea
        '
        Me.lblArea.AutoSize = True
        Me.lblArea.Location = New System.Drawing.Point(270, 32)
        Me.lblArea.Name = "lblArea"
        Me.lblArea.Size = New System.Drawing.Size(579, 25)
        Me.lblArea.TabIndex = 31
        Me.lblArea.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 134)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(102, 25)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Comments"
        '
        'lblStepLabel
        '
        Me.lblStepLabel.AutoSize = True
        Me.lblStepLabel.Location = New System.Drawing.Point(6, 83)
        Me.lblStepLabel.Name = "lblStepLabel"
        Me.lblStepLabel.Size = New System.Drawing.Size(103, 25)
        Me.lblStepLabel.TabIndex = 8
        Me.lblStepLabel.Text = "Step / Item"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 32)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(183, 25)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Developmental Area"
        '
        'btnPhoto
        '
        Me.btnPhoto.Image = CType(resources.GetObject("btnPhoto.Image"), System.Drawing.Image)
        Me.btnPhoto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPhoto.Location = New System.Drawing.Point(17, 481)
        Me.btnPhoto.Name = "btnPhoto"
        Me.btnPhoto.Size = New System.Drawing.Size(205, 45)
        Me.btnPhoto.TabIndex = 44
        Me.btnPhoto.Text = "Capture Moment"
        Me.btnPhoto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPhoto.UseVisualStyleBackColor = True
        '
        'frmLDAssessment
        '
        Me.ClientSize = New System.Drawing.Size(1024, 538)
        Me.Controls.Add(Me.btnPhoto)
        Me.Controls.Add(Me.gbxMilk)
        Me.Name = "frmLDAssessment"
        Me.Text = "Observations"
        Me.Controls.SetChildIndex(Me.gbxMilk, 0)
        Me.Controls.SetChildIndex(Me.btnPhoto, 0)
        Me.gbxMilk.ResumeLayout(False)
        Me.gbxMilk.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbxMilk As System.Windows.Forms.GroupBox
    Friend WithEvents lblPhotoNumber As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnPhotoNumber As System.Windows.Forms.Button
    Friend WithEvents btnArea As System.Windows.Forms.Button
    Friend WithEvents lblStep As System.Windows.Forms.Label
    Friend WithEvents lblArea As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblStepLabel As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnStep As System.Windows.Forms.Button
    Friend WithEvents txtComments As System.Windows.Forms.TextBox
    Friend WithEvents btnComments As System.Windows.Forms.Button
    Friend WithEvents btnPhoto As System.Windows.Forms.Button

End Class
