﻿Imports NurseryTablet.Business
Imports NurseryTablet.SharedModule

Public Class frmCrossCheck

    Private m_DayID As String = Day.TodayIDString
    Private m_Mode As EnumMode

    Private m_ChildID As String = ""
    Private m_ChildName As String = ""
    Private m_ChildGroupID As String

    Private m_Nappies As Boolean
    Private m_Milk As Boolean
    Private m_BabyFood As Boolean
    Private m_OffMenu As Boolean
    Private m_FamilyID As String

    Private m_Sibblings As New List(Of NurseryGenieData.Child)

    Private m_CrossChecked As Boolean = False
    Private m_MedicineOrIncident As Boolean = False

    Private Enum EnumColourMode
        None
        PositiveGreen
        PositiveRed
    End Enum

    Public Enum EnumMode
        CrossCheck
        CheckOut
    End Enum

    Public Sub New(ByVal ChildID As String, ByVal ChildName As String, ByVal Mode As EnumMode)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_ChildID = ChildID
        m_ChildName = ChildName
        m_Mode = Mode

    End Sub

    Private Sub frmCrossCheck_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        txtChild.Text = m_ChildName
        txtCheckedBy.Text = ""

        'have we cross-checked already?
        Dim _A As NurseryGenieData.Activity = Nothing
        If Business.Activity.ReturnActivityItem(m_ChildID, SharedModule.EnumActivityType.CrossCheck, _A) Then
            m_CrossChecked = True
            txtCheckedBy.Text = _A.StaffName
            txtCheckedBy.BackColor = SharedModule.ColourGreen
        Else
            txtCheckedBy.Text = "Not Checked"
            txtCheckedBy.BackColor = SharedModule.ColourRed
        End If

        If m_Mode = EnumMode.CheckOut Then
            btnAccept.Text = "Check Out"
        Else
            btnAccept.Text = "Approve Cross-Check"
            btnAccept.Enabled = Not m_CrossChecked
        End If

        SetChildDetails()
        DisplayStats()
        PopulateSiblings()

    End Sub

    Private Sub PopulateSiblings()

        m_Sibblings.Clear()
        m_Sibblings = Business.Child.GetSiblings(m_ChildID)

    End Sub

    Private Sub SetChildDetails()

        Dim _C As NurseryGenieData.Child = Business.Child.ReturnChildByID(m_ChildID)
        If _C IsNot Nothing Then

            'used for drilldown
            m_ChildGroupID = _C.GroupID.ToString()
            m_FamilyID = _C.FamilyID.ToString()

            m_Nappies = _C.Nappies
            m_Milk = _C.Milk
            m_BabyFood = _C.BabyFood
            m_OffMenu = _C.OffMenu

        End If

    End Sub

    Private Sub DisplayStats()

        '******************************************************************************************************************************

        SharedModule.ReturnFeedback(m_DayID, m_ChildID, txtFeedback.Text)

        '******************************************************************************************************************************

        Dim _Breakfast As Integer = SharedModule.ReturnFoodCount(m_DayID, m_ChildID, EnumMealType.Breakfast)
        SetTextBox(txtBreakfast, _Breakfast, EnumColourMode.PositiveGreen)

        Dim _Snack As Integer = SharedModule.ReturnFoodCount(m_DayID, m_ChildID, EnumMealType.Snack)
        SetTextBox(txtSnack, _Snack, EnumColourMode.PositiveGreen)

        Dim _Lunch As Integer = SharedModule.ReturnFoodCount(m_DayID, m_ChildID, EnumMealType.Lunch)
        SetTextBox(txtLunch, _Lunch, EnumColourMode.PositiveGreen)

        Dim _LunchDessert As Integer = SharedModule.ReturnFoodCount(m_DayID, m_ChildID, EnumMealType.LunchDessert)
        SetTextBox(txtLunchDessert, _LunchDessert, EnumColourMode.PositiveGreen)

        Dim _Tea As Integer = SharedModule.ReturnFoodCount(m_DayID, m_ChildID, EnumMealType.Tea)
        SetTextBox(txtTea, _Tea, EnumColourMode.PositiveGreen)

        Dim _TeaDessert As Integer = SharedModule.ReturnFoodCount(m_DayID, m_ChildID, EnumMealType.TeaDessert)
        SetTextBox(txtTeaDessert, _TeaDessert, EnumColourMode.PositiveGreen)

        '******************************************************************************************************************************

        Dim _Toilet As Integer = SharedModule.ReturnActivityCount(m_DayID, m_ChildID, SharedModule.EnumActivityType.Toilet)
        If m_Nappies Then
            SetTextBox(txtNappies, _Toilet, EnumColourMode.PositiveGreen)
        Else
            SetTextBox(txtNappies, _Toilet, EnumColourMode.None)
        End If

        Dim _Sleep As Integer = SharedModule.ReturnActivityCount(m_DayID, m_ChildID, SharedModule.EnumActivityType.Sleep)
        SetTextBox(txtSleep, _Sleep, EnumColourMode.None)

        Dim _Medication As Integer = SharedModule.ReturnMedicineCount(m_DayID, m_ChildID)
        SetTextBox(txtMedication, _Medication, EnumColourMode.PositiveRed)

        Dim _Incident As Integer = SharedModule.ReturnIncidentCount(m_DayID, m_ChildID)
        SetTextBox(txtIncidents, _Incident, EnumColourMode.PositiveRed)

        Dim _Suncream As Integer = SharedModule.ReturnActivityCount(m_DayID, m_ChildID, SharedModule.EnumActivityType.Suncream)
        SetTextBox(txtSuncream, _Suncream, EnumColourMode.None)

        Dim _Milk As Integer = SharedModule.ReturnActivityCount(m_DayID, m_ChildID, SharedModule.EnumActivityType.Milk)
        If m_Milk Then
            SetTextBox(txtMilk, _Milk, EnumColourMode.PositiveGreen)
        Else
            SetTextBox(txtMilk, _Milk, EnumColourMode.None)
        End If

        Dim _Obs As Integer = SharedModule.ReturnObservationCount(m_DayID, m_ChildID)
        SetTextBox(txtObservations, _Obs, EnumColourMode.None)

    End Sub

    Private Sub SetTextBox(ByRef TextBoxIn As DevExpress.XtraEditors.TextEdit, ByVal CountIn As Integer, ByVal ColourMode As EnumColourMode)

        Dim _Text As String = ""
        Dim _Backcolour As Drawing.Color

        Select Case CountIn

            Case 0
                _Text = "None"
                If ColourMode = EnumColourMode.PositiveGreen Then
                    _Backcolour = SharedModule.ColourRed
                Else
                    _Backcolour = SharedModule.ColourGreen
                End If

            Case 1
                _Text = "1 item"
                If ColourMode = EnumColourMode.PositiveGreen Then
                    _Backcolour = SharedModule.ColourGreen
                Else
                    _Backcolour = SharedModule.ColourRed
                End If

            Case Else
                _Text = CountIn.ToString + " items"
                If ColourMode = EnumColourMode.PositiveGreen Then
                    _Backcolour = SharedModule.ColourGreen
                Else
                    _Backcolour = SharedModule.ColourRed
                End If

        End Select

        If ColourMode = EnumColourMode.None Then
            TextBoxIn.ResetBackColor()
        Else
            TextBoxIn.BackColor = _Backcolour
        End If

        TextBoxIn.Text = _Text

    End Sub

    Private Sub ProcessCrossCheck()

        Dim _S As New Signature

        If _S.CaptureStaffSignature(True) Then

            SharedModule.LogActivity(Day.TodayIDString, m_ChildID, m_ChildName, SharedModule.EnumActivityType.CrossCheck, "Cross-Check Completed", _S.SignatureID.ToString, , , _S.PersonID.ToString, _S.PersonName)
            _S = Nothing

            Me.DialogResult = Windows.Forms.DialogResult.OK

        End If

        _S = Nothing

    End Sub

    Private Sub ProcessCheckOut()

        'ensure we have completed the cross checks if they are mandatory
        If Parameters.CrossCheckMandatory Then
            If Not m_CrossChecked Then
                Msgbox("Cross Check not completed - Check Out Cancelled.", MessageBoxIcon.Error, "Check Out")
                Exit Sub
            End If
        End If

        '********************************************************************************************************************************************************************************************************

        Dim _CheckOutSiblings As Boolean = False

        'check if there are any sibblings checked in
        If m_Sibblings.Count > 0 Then

            Dim _Children As String = ""
            For Each _s In m_Sibblings
                _Children += _s.Forename + vbCrLf
            Next

            Dim _Mess As String = "Do you wish to Check Out the following siblings?" + vbCrLf + vbCrLf + _Children

            If Msgbox(_Mess, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Check Out") = Windows.Forms.DialogResult.Yes Then

                'ensure all the sibblings have been cross-checked
                If Parameters.CrossCheckMandatory Then

                    Dim _NotCrossChecked As String = ""
                    Dim _Medicine As String = ""
                    Dim _Incidents As String = ""

                    For Each _s In m_Sibblings
                        If Not ChildCrossChecked(_s.ID.ToString()) Then _NotCrossChecked += _s.Forename + vbCrLf
                        If Not ChildHadMedicine(_s.ID.ToString()) Then _Medicine += _s.Forename + vbCrLf
                        If Not ChildHadIncident(_s.ID.ToString()) Then _Incidents += _s.Forename + vbCrLf
                    Next

                    If _NotCrossChecked <> "" Then
                        _Mess = "The following Children have not been Cross Checked:" + vbCrLf + vbCrLf + _NotCrossChecked + vbCrLf + "Check Out Cancelled"
                        Msgbox(_Mess, MessageBoxIcon.Error, "Check Out")
                        Exit Sub
                    End If

                    If _Medicine <> "" Then
                        _Mess = "The following Children had medicine, they must be Checked Out manually:" + vbCrLf + vbCrLf + _Medicine + vbCrLf + "Check Out Cancelled"
                        Msgbox(_Mess, MessageBoxIcon.Error, "Check Out")
                        Exit Sub
                    End If

                    If _Incidents <> "" Then
                        _Mess = "The following Children where involved in an incident, they must be Checked Out manually:" + vbCrLf + vbCrLf + _Incidents + vbCrLf + "Check Out Cancelled"
                        Msgbox(_Mess, MessageBoxIcon.Error, "Check Out")
                        Exit Sub
                    End If

                    _CheckOutSiblings = True
                    For Each _s In m_Sibblings
                        Business.Register.RegisterTransaction(_s.ID.ToString(), Enums.PersonType.Child, _s.FullName, Enums.InOut.CheckOut, Enums.RegisterVia.CheckOut)
                    Next

                End If

            End If

        End If

        '********************************************************************************************************************************************************************************************************

        'check if this child was involved in an incident or accident
        Dim _MedicineGiven As Boolean = ChildHadMedicine(m_ChildID)
        Dim _IncidentLogged As Boolean = ChildHadIncident(m_ChildID)

        If _MedicineGiven Or _IncidentLogged Then

            Dim _Mess As String = ""

            'medication only
            If _MedicineGiven = True AndAlso _IncidentLogged = False Then
                _Mess = "Your child was given medication today."
            End If

            'incident only
            If _MedicineGiven = False AndAlso _IncidentLogged = True Then
                _Mess = "Your child was involved in an incident today."
            End If

            'both
            If _MedicineGiven = True AndAlso _IncidentLogged = True Then
                _Mess = "Your child was involved in an incident today and was also given medication."
            End If

            _Mess += vbCrLf + vbCrLf + "Please tap OK so we can capture your signature..."

            Msgbox(_Mess, MessageBoxIcon.Exclamation, "Signature Required")

            'capture the parent's signature
            Dim _Sig As New Signature
            _Sig.CaptureParentSignature(True, m_FamilyID)

            If _Sig.SignatureCaptured Then

                'update the medication authorisation record
                Business.Medicine.UpdateMedicationAuth(m_ChildID, _Sig)

                'update the incident record
                Business.Incident.UpdateIncident(m_ChildID, _Sig)

                'create a MEDSIG activity
                If _MedicineGiven Then
                    SharedModule.LogActivity(Day.TodayIDString, m_ChildID, m_ChildName, SharedModule.EnumActivityType.CheckOutMedication, "Contact Signature for Medication", _Sig.SignatureID.ToString)
                End If

                'create a INCSIG activity
                If _IncidentLogged Then
                    SharedModule.LogActivity(Day.TodayIDString, m_ChildID, m_ChildName, SharedModule.EnumActivityType.CheckOutIncident, "Contact Signature for Incident", _Sig.SignatureID.ToString)
                End If

            Else
                Msgbox("Signature declined. Check Out cancelled.", MessageBoxIcon.Exclamation, "Signature Declined")
                Me.DialogResult = Windows.Forms.DialogResult.Cancel
                Me.Close()
                Exit Sub
            End If

        End If

        '********************************************************************************************************************************************************************************************************

        'proceed with check-out
        Business.Register.RegisterTransaction(m_ChildID, Enums.PersonType.Child, m_ChildName, Enums.InOut.CheckOut, Enums.RegisterVia.CheckOut)

        'persist the data locally
        DataBase.PersistDatabaseLocally()

        Do
            If DataBase.Sync(DataBase.EnumSyncOperation.UploadOnly, False) Then

                SendReports(m_ChildID)
                If _CheckOutSiblings Then
                    For Each _s In m_Sibblings
                        SendReports(_s.ID.ToString)
                    Next
                End If
                Msgbox("Check Out Completed Successfully.", MessageBoxIcon.Information, "Check Out")
                Exit Do

            Else
                If Msgbox("Check Out has completed - but we were unable to Upload changes to the Server. Do you want to try again?",
                          MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Check Out") = DialogResult.No Then Exit Do
            End If
        Loop

        '********************************************************************************************************************************************************************************************************

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()

    End Sub

    Private Sub SendReports(ByVal ChildID As String)
        ServiceHandler.SendReport(ChildID)
    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click

        btnAccept.Enabled = False
        btnCancel.Enabled = False
        btnReport.Enabled = False

        Me.Cursor = Cursors.WaitCursor
        Application.DoEvents()

        If m_Mode = EnumMode.CrossCheck Then
            ProcessCrossCheck()
        Else
            ProcessCheckOut()
        End If

        btnAccept.Enabled = True
        btnCancel.Enabled = True
        btnReport.Enabled = True

        Me.Cursor = Cursors.Default
        Application.DoEvents()

    End Sub

    Private Function ChildCrossChecked(ByVal ChildID As String) As Boolean
        Return SharedModule.HasActivity(Day.TodayIDString, ChildID, SharedModule.EnumActivityType.CrossCheck)
    End Function

    Private Function ChildHadMedicine(ByVal ChildID As String) As Boolean
        Dim _Medicine As Integer = SharedModule.ReturnMedicineCount(Day.TodayIDString, ChildID)
        If _Medicine > 0 Then Return True
        Return False
    End Function

    Private Function ChildHadIncident(ByVal ChildID As String) As Boolean
        Dim _Incident As Integer = SharedModule.ReturnIncidentCount(Day.TodayIDString, ChildID)
        If _Incident > 0 Then Return True
        Return False
    End Function

    Private Sub txtNappies_DoubleClick(sender As Object, e As EventArgs) Handles txtNappies.DoubleClick
        SharedModule.ChildToilet(m_ChildID)
    End Sub

    Private Sub txtSleep_DoubleClick(sender As Object, e As EventArgs) Handles txtSleep.DoubleClick
        SharedModule.ChildSleep(m_ChildID)
    End Sub

    Private Sub txtMedication_DoubleClick(sender As Object, e As EventArgs) Handles txtMedication.DoubleClick
        SharedModule.ChildMedicationLog(m_ChildID)
    End Sub

    Private Sub txtIncidents_DoubleClick(sender As Object, e As EventArgs) Handles txtIncidents.DoubleClick
        SharedModule.ChildIncidents(m_ChildID)
    End Sub

    Private Sub txtSuncream_DoubleClick(sender As Object, e As EventArgs) Handles txtSuncream.DoubleClick
        SharedModule.ChildSuncream(m_ChildID)
    End Sub

    Private Sub txtMilk_DoubleClick(sender As Object, e As EventArgs) Handles txtMilk.DoubleClick
        SharedModule.ChildMilk(m_ChildID)
    End Sub

    Private Sub txtBreakfast_DoubleClick(sender As Object, e As EventArgs) Handles txtBreakfast.DoubleClick, txtSnack.DoubleClick, txtLunch.DoubleClick, txtLunchDessert.DoubleClick, txtTea.DoubleClick, txtTeaDessert.DoubleClick
        SharedModule.ChildFood(m_ChildID)
    End Sub

    Private Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnReport.Click

        btnReport.Enabled = False
        btnAccept.Enabled = False
        btnCancel.Enabled = False

        Cursor.Current = Cursors.WaitCursor
        Application.DoEvents()

        DataBase.PersistDatabaseLocally()

        If DataBase.Sync(DataBase.EnumSyncOperation.UploadOnly, False) Then
            ServiceHandler.ViewReport(m_ChildID)
        End If

        btnReport.Enabled = True
        btnAccept.Enabled = True
        btnCancel.Enabled = True

        Cursor.Current = Cursors.Default
        Application.DoEvents()

    End Sub

End Class