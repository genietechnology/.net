﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmToilet
    Inherits NurseryTablet.frmBaseChild

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmToilet))
        Me.gbxWet = New System.Windows.Forms.GroupBox()
        Me.lblToilet6 = New System.Windows.Forms.Label()
        Me.lblToilet5 = New System.Windows.Forms.Label()
        Me.lblToilet4 = New System.Windows.Forms.Label()
        Me.lblToilet3 = New System.Windows.Forms.Label()
        Me.lblToilet2 = New System.Windows.Forms.Label()
        Me.lblToilet1 = New System.Windows.Forms.Label()
        Me.btnToilet6 = New System.Windows.Forms.Button()
        Me.btnToilet5 = New System.Windows.Forms.Button()
        Me.btnToilet4 = New System.Windows.Forms.Button()
        Me.btnToilet3 = New System.Windows.Forms.Button()
        Me.btnToilet2 = New System.Windows.Forms.Button()
        Me.btnToilet1 = New System.Windows.Forms.Button()
        Me.gbxSoiled = New System.Windows.Forms.GroupBox()
        Me.lblToilet12 = New System.Windows.Forms.Label()
        Me.lblToilet11 = New System.Windows.Forms.Label()
        Me.btnToilet7 = New System.Windows.Forms.Button()
        Me.lblToilet10 = New System.Windows.Forms.Label()
        Me.btnToilet8 = New System.Windows.Forms.Button()
        Me.lblToilet9 = New System.Windows.Forms.Label()
        Me.btnToilet9 = New System.Windows.Forms.Button()
        Me.lblToilet8 = New System.Windows.Forms.Label()
        Me.btnToilet10 = New System.Windows.Forms.Button()
        Me.lblToilet7 = New System.Windows.Forms.Label()
        Me.btnToilet11 = New System.Windows.Forms.Button()
        Me.btnToilet12 = New System.Windows.Forms.Button()
        Me.btnDry = New System.Windows.Forms.Button()
        Me.btnWet = New System.Windows.Forms.Button()
        Me.btnSoiled = New System.Windows.Forms.Button()
        Me.btnLoose = New System.Windows.Forms.Button()
        Me.btnNappy = New System.Windows.Forms.Button()
        Me.btnCream = New System.Windows.Forms.Button()
        Me.btnPotty = New System.Windows.Forms.Button()
        Me.btnToilet = New System.Windows.Forms.Button()
        Me.gbxWet.SuspendLayout()
        Me.gbxSoiled.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        '
        'gbxWet
        '
        Me.gbxWet.Controls.Add(Me.lblToilet6)
        Me.gbxWet.Controls.Add(Me.lblToilet5)
        Me.gbxWet.Controls.Add(Me.lblToilet4)
        Me.gbxWet.Controls.Add(Me.lblToilet3)
        Me.gbxWet.Controls.Add(Me.lblToilet2)
        Me.gbxWet.Controls.Add(Me.lblToilet1)
        Me.gbxWet.Controls.Add(Me.btnToilet6)
        Me.gbxWet.Controls.Add(Me.btnToilet5)
        Me.gbxWet.Controls.Add(Me.btnToilet4)
        Me.gbxWet.Controls.Add(Me.btnToilet3)
        Me.gbxWet.Controls.Add(Me.btnToilet2)
        Me.gbxWet.Controls.Add(Me.btnToilet1)
        Me.gbxWet.Location = New System.Drawing.Point(17, 93)
        Me.gbxWet.Name = "gbxWet"
        Me.gbxWet.Size = New System.Drawing.Size(490, 331)
        Me.gbxWet.TabIndex = 8
        Me.gbxWet.TabStop = False
        '
        'lblToilet6
        '
        Me.lblToilet6.AutoSize = True
        Me.lblToilet6.Location = New System.Drawing.Point(6, 287)
        Me.lblToilet6.Name = "lblToilet6"
        Me.lblToilet6.Size = New System.Drawing.Size(435, 25)
        Me.lblToilet6.TabIndex = 12
        Me.lblToilet6.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblToilet5
        '
        Me.lblToilet5.AutoSize = True
        Me.lblToilet5.Location = New System.Drawing.Point(6, 236)
        Me.lblToilet5.Name = "lblToilet5"
        Me.lblToilet5.Size = New System.Drawing.Size(435, 25)
        Me.lblToilet5.TabIndex = 11
        Me.lblToilet5.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblToilet4
        '
        Me.lblToilet4.AutoSize = True
        Me.lblToilet4.Location = New System.Drawing.Point(6, 185)
        Me.lblToilet4.Name = "lblToilet4"
        Me.lblToilet4.Size = New System.Drawing.Size(435, 25)
        Me.lblToilet4.TabIndex = 10
        Me.lblToilet4.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblToilet3
        '
        Me.lblToilet3.AutoSize = True
        Me.lblToilet3.Location = New System.Drawing.Point(6, 134)
        Me.lblToilet3.Name = "lblToilet3"
        Me.lblToilet3.Size = New System.Drawing.Size(435, 25)
        Me.lblToilet3.TabIndex = 9
        Me.lblToilet3.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblToilet2
        '
        Me.lblToilet2.AutoSize = True
        Me.lblToilet2.Location = New System.Drawing.Point(6, 83)
        Me.lblToilet2.Name = "lblToilet2"
        Me.lblToilet2.Size = New System.Drawing.Size(435, 25)
        Me.lblToilet2.TabIndex = 8
        Me.lblToilet2.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblToilet1
        '
        Me.lblToilet1.AutoSize = True
        Me.lblToilet1.Location = New System.Drawing.Point(6, 32)
        Me.lblToilet1.Name = "lblToilet1"
        Me.lblToilet1.Size = New System.Drawing.Size(435, 25)
        Me.lblToilet1.TabIndex = 7
        Me.lblToilet1.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'btnToilet6
        '
        Me.btnToilet6.Image = CType(resources.GetObject("btnToilet6.Image"), System.Drawing.Image)
        Me.btnToilet6.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnToilet6.Location = New System.Drawing.Point(444, 277)
        Me.btnToilet6.Name = "btnToilet6"
        Me.btnToilet6.Size = New System.Drawing.Size(40, 45)
        Me.btnToilet6.TabIndex = 6
        Me.btnToilet6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnToilet6.UseVisualStyleBackColor = True
        '
        'btnToilet5
        '
        Me.btnToilet5.Image = CType(resources.GetObject("btnToilet5.Image"), System.Drawing.Image)
        Me.btnToilet5.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnToilet5.Location = New System.Drawing.Point(444, 226)
        Me.btnToilet5.Name = "btnToilet5"
        Me.btnToilet5.Size = New System.Drawing.Size(40, 45)
        Me.btnToilet5.TabIndex = 5
        Me.btnToilet5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnToilet5.UseVisualStyleBackColor = True
        '
        'btnToilet4
        '
        Me.btnToilet4.Image = CType(resources.GetObject("btnToilet4.Image"), System.Drawing.Image)
        Me.btnToilet4.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnToilet4.Location = New System.Drawing.Point(444, 175)
        Me.btnToilet4.Name = "btnToilet4"
        Me.btnToilet4.Size = New System.Drawing.Size(40, 45)
        Me.btnToilet4.TabIndex = 4
        Me.btnToilet4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnToilet4.UseVisualStyleBackColor = True
        '
        'btnToilet3
        '
        Me.btnToilet3.Image = CType(resources.GetObject("btnToilet3.Image"), System.Drawing.Image)
        Me.btnToilet3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnToilet3.Location = New System.Drawing.Point(444, 124)
        Me.btnToilet3.Name = "btnToilet3"
        Me.btnToilet3.Size = New System.Drawing.Size(40, 45)
        Me.btnToilet3.TabIndex = 3
        Me.btnToilet3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnToilet3.UseVisualStyleBackColor = True
        '
        'btnToilet2
        '
        Me.btnToilet2.Image = CType(resources.GetObject("btnToilet2.Image"), System.Drawing.Image)
        Me.btnToilet2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnToilet2.Location = New System.Drawing.Point(444, 73)
        Me.btnToilet2.Name = "btnToilet2"
        Me.btnToilet2.Size = New System.Drawing.Size(40, 45)
        Me.btnToilet2.TabIndex = 2
        Me.btnToilet2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnToilet2.UseVisualStyleBackColor = True
        '
        'btnToilet1
        '
        Me.btnToilet1.Image = CType(resources.GetObject("btnToilet1.Image"), System.Drawing.Image)
        Me.btnToilet1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnToilet1.Location = New System.Drawing.Point(444, 22)
        Me.btnToilet1.Name = "btnToilet1"
        Me.btnToilet1.Size = New System.Drawing.Size(40, 45)
        Me.btnToilet1.TabIndex = 0
        Me.btnToilet1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnToilet1.UseVisualStyleBackColor = True
        '
        'gbxSoiled
        '
        Me.gbxSoiled.Controls.Add(Me.lblToilet12)
        Me.gbxSoiled.Controls.Add(Me.lblToilet11)
        Me.gbxSoiled.Controls.Add(Me.btnToilet7)
        Me.gbxSoiled.Controls.Add(Me.lblToilet10)
        Me.gbxSoiled.Controls.Add(Me.btnToilet8)
        Me.gbxSoiled.Controls.Add(Me.lblToilet9)
        Me.gbxSoiled.Controls.Add(Me.btnToilet9)
        Me.gbxSoiled.Controls.Add(Me.lblToilet8)
        Me.gbxSoiled.Controls.Add(Me.btnToilet10)
        Me.gbxSoiled.Controls.Add(Me.lblToilet7)
        Me.gbxSoiled.Controls.Add(Me.btnToilet11)
        Me.gbxSoiled.Controls.Add(Me.btnToilet12)
        Me.gbxSoiled.Location = New System.Drawing.Point(522, 93)
        Me.gbxSoiled.Name = "gbxSoiled"
        Me.gbxSoiled.Size = New System.Drawing.Size(490, 331)
        Me.gbxSoiled.TabIndex = 9
        Me.gbxSoiled.TabStop = False
        '
        'lblToilet12
        '
        Me.lblToilet12.AutoSize = True
        Me.lblToilet12.Location = New System.Drawing.Point(6, 287)
        Me.lblToilet12.Name = "lblToilet12"
        Me.lblToilet12.Size = New System.Drawing.Size(435, 25)
        Me.lblToilet12.TabIndex = 24
        Me.lblToilet12.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblToilet11
        '
        Me.lblToilet11.AutoSize = True
        Me.lblToilet11.Location = New System.Drawing.Point(6, 236)
        Me.lblToilet11.Name = "lblToilet11"
        Me.lblToilet11.Size = New System.Drawing.Size(435, 25)
        Me.lblToilet11.TabIndex = 23
        Me.lblToilet11.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'btnToilet7
        '
        Me.btnToilet7.Image = CType(resources.GetObject("btnToilet7.Image"), System.Drawing.Image)
        Me.btnToilet7.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnToilet7.Location = New System.Drawing.Point(444, 22)
        Me.btnToilet7.Name = "btnToilet7"
        Me.btnToilet7.Size = New System.Drawing.Size(40, 45)
        Me.btnToilet7.TabIndex = 13
        Me.btnToilet7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnToilet7.UseVisualStyleBackColor = True
        '
        'lblToilet10
        '
        Me.lblToilet10.AutoSize = True
        Me.lblToilet10.Location = New System.Drawing.Point(6, 185)
        Me.lblToilet10.Name = "lblToilet10"
        Me.lblToilet10.Size = New System.Drawing.Size(435, 25)
        Me.lblToilet10.TabIndex = 22
        Me.lblToilet10.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'btnToilet8
        '
        Me.btnToilet8.Image = CType(resources.GetObject("btnToilet8.Image"), System.Drawing.Image)
        Me.btnToilet8.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnToilet8.Location = New System.Drawing.Point(444, 73)
        Me.btnToilet8.Name = "btnToilet8"
        Me.btnToilet8.Size = New System.Drawing.Size(40, 45)
        Me.btnToilet8.TabIndex = 14
        Me.btnToilet8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnToilet8.UseVisualStyleBackColor = True
        '
        'lblToilet9
        '
        Me.lblToilet9.AutoSize = True
        Me.lblToilet9.Location = New System.Drawing.Point(6, 134)
        Me.lblToilet9.Name = "lblToilet9"
        Me.lblToilet9.Size = New System.Drawing.Size(435, 25)
        Me.lblToilet9.TabIndex = 21
        Me.lblToilet9.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'btnToilet9
        '
        Me.btnToilet9.Image = CType(resources.GetObject("btnToilet9.Image"), System.Drawing.Image)
        Me.btnToilet9.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnToilet9.Location = New System.Drawing.Point(444, 124)
        Me.btnToilet9.Name = "btnToilet9"
        Me.btnToilet9.Size = New System.Drawing.Size(40, 45)
        Me.btnToilet9.TabIndex = 15
        Me.btnToilet9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnToilet9.UseVisualStyleBackColor = True
        '
        'lblToilet8
        '
        Me.lblToilet8.AutoSize = True
        Me.lblToilet8.Location = New System.Drawing.Point(6, 83)
        Me.lblToilet8.Name = "lblToilet8"
        Me.lblToilet8.Size = New System.Drawing.Size(435, 25)
        Me.lblToilet8.TabIndex = 20
        Me.lblToilet8.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'btnToilet10
        '
        Me.btnToilet10.Image = CType(resources.GetObject("btnToilet10.Image"), System.Drawing.Image)
        Me.btnToilet10.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnToilet10.Location = New System.Drawing.Point(444, 175)
        Me.btnToilet10.Name = "btnToilet10"
        Me.btnToilet10.Size = New System.Drawing.Size(40, 45)
        Me.btnToilet10.TabIndex = 16
        Me.btnToilet10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnToilet10.UseVisualStyleBackColor = True
        '
        'lblToilet7
        '
        Me.lblToilet7.AutoSize = True
        Me.lblToilet7.Location = New System.Drawing.Point(6, 32)
        Me.lblToilet7.Name = "lblToilet7"
        Me.lblToilet7.Size = New System.Drawing.Size(435, 25)
        Me.lblToilet7.TabIndex = 19
        Me.lblToilet7.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'btnToilet11
        '
        Me.btnToilet11.Image = CType(resources.GetObject("btnToilet11.Image"), System.Drawing.Image)
        Me.btnToilet11.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnToilet11.Location = New System.Drawing.Point(444, 226)
        Me.btnToilet11.Name = "btnToilet11"
        Me.btnToilet11.Size = New System.Drawing.Size(40, 45)
        Me.btnToilet11.TabIndex = 17
        Me.btnToilet11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnToilet11.UseVisualStyleBackColor = True
        '
        'btnToilet12
        '
        Me.btnToilet12.Image = CType(resources.GetObject("btnToilet12.Image"), System.Drawing.Image)
        Me.btnToilet12.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnToilet12.Location = New System.Drawing.Point(444, 277)
        Me.btnToilet12.Name = "btnToilet12"
        Me.btnToilet12.Size = New System.Drawing.Size(40, 45)
        Me.btnToilet12.TabIndex = 18
        Me.btnToilet12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnToilet12.UseVisualStyleBackColor = True
        '
        'btnDry
        '
        Me.btnDry.Image = CType(resources.GetObject("btnDry.Image"), System.Drawing.Image)
        Me.btnDry.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDry.Location = New System.Drawing.Point(393, 430)
        Me.btnDry.Name = "btnDry"
        Me.btnDry.Size = New System.Drawing.Size(120, 45)
        Me.btnDry.TabIndex = 2
        Me.btnDry.Text = "Dry"
        Me.btnDry.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDry.UseVisualStyleBackColor = True
        '
        'btnWet
        '
        Me.btnWet.Image = CType(resources.GetObject("btnWet.Image"), System.Drawing.Image)
        Me.btnWet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnWet.Location = New System.Drawing.Point(519, 430)
        Me.btnWet.Name = "btnWet"
        Me.btnWet.Size = New System.Drawing.Size(120, 45)
        Me.btnWet.TabIndex = 10
        Me.btnWet.Text = "Wet"
        Me.btnWet.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnWet.UseVisualStyleBackColor = True
        '
        'btnSoiled
        '
        Me.btnSoiled.Image = CType(resources.GetObject("btnSoiled.Image"), System.Drawing.Image)
        Me.btnSoiled.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSoiled.Location = New System.Drawing.Point(645, 430)
        Me.btnSoiled.Name = "btnSoiled"
        Me.btnSoiled.Size = New System.Drawing.Size(120, 45)
        Me.btnSoiled.TabIndex = 11
        Me.btnSoiled.Text = "Soiled"
        Me.btnSoiled.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSoiled.UseVisualStyleBackColor = True
        '
        'btnLoose
        '
        Me.btnLoose.Image = CType(resources.GetObject("btnLoose.Image"), System.Drawing.Image)
        Me.btnLoose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLoose.Location = New System.Drawing.Point(771, 430)
        Me.btnLoose.Name = "btnLoose"
        Me.btnLoose.Size = New System.Drawing.Size(120, 45)
        Me.btnLoose.TabIndex = 12
        Me.btnLoose.Text = "Loose"
        Me.btnLoose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLoose.UseVisualStyleBackColor = True
        '
        'btnNappy
        '
        Me.btnNappy.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNappy.Location = New System.Drawing.Point(17, 430)
        Me.btnNappy.Name = "btnNappy"
        Me.btnNappy.Size = New System.Drawing.Size(106, 45)
        Me.btnNappy.TabIndex = 13
        Me.btnNappy.Text = "Nappy"
        Me.btnNappy.UseVisualStyleBackColor = True
        '
        'btnCream
        '
        Me.btnCream.Image = CType(resources.GetObject("btnCream.Image"), System.Drawing.Image)
        Me.btnCream.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCream.Location = New System.Drawing.Point(897, 430)
        Me.btnCream.Name = "btnCream"
        Me.btnCream.Size = New System.Drawing.Size(120, 45)
        Me.btnCream.TabIndex = 14
        Me.btnCream.Text = "Cream"
        Me.btnCream.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCream.UseVisualStyleBackColor = True
        '
        'btnPotty
        '
        Me.btnPotty.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPotty.Location = New System.Drawing.Point(129, 430)
        Me.btnPotty.Name = "btnPotty"
        Me.btnPotty.Size = New System.Drawing.Size(106, 45)
        Me.btnPotty.TabIndex = 20
        Me.btnPotty.Text = "Potty"
        Me.btnPotty.UseVisualStyleBackColor = True
        '
        'btnToilet
        '
        Me.btnToilet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnToilet.Location = New System.Drawing.Point(241, 430)
        Me.btnToilet.Name = "btnToilet"
        Me.btnToilet.Size = New System.Drawing.Size(106, 45)
        Me.btnToilet.TabIndex = 21
        Me.btnToilet.Text = "Toilet"
        Me.btnToilet.UseVisualStyleBackColor = True
        '
        'frmToilet
        '
        Me.ClientSize = New System.Drawing.Size(1024, 538)
        Me.Controls.Add(Me.btnToilet)
        Me.Controls.Add(Me.btnPotty)
        Me.Controls.Add(Me.btnCream)
        Me.Controls.Add(Me.btnNappy)
        Me.Controls.Add(Me.btnLoose)
        Me.Controls.Add(Me.btnSoiled)
        Me.Controls.Add(Me.btnWet)
        Me.Controls.Add(Me.btnDry)
        Me.Controls.Add(Me.gbxSoiled)
        Me.Controls.Add(Me.gbxWet)
        Me.Name = "frmToilet"
        Me.Text = "Nappies & Toilet"
        Me.Controls.SetChildIndex(Me.gbxWet, 0)
        Me.Controls.SetChildIndex(Me.gbxSoiled, 0)
        Me.Controls.SetChildIndex(Me.btnDry, 0)
        Me.Controls.SetChildIndex(Me.btnWet, 0)
        Me.Controls.SetChildIndex(Me.btnSoiled, 0)
        Me.Controls.SetChildIndex(Me.btnLoose, 0)
        Me.Controls.SetChildIndex(Me.btnNappy, 0)
        Me.Controls.SetChildIndex(Me.btnCream, 0)
        Me.Controls.SetChildIndex(Me.btnPotty, 0)
        Me.Controls.SetChildIndex(Me.btnToilet, 0)
        Me.gbxWet.ResumeLayout(False)
        Me.gbxWet.PerformLayout()
        Me.gbxSoiled.ResumeLayout(False)
        Me.gbxSoiled.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbxWet As System.Windows.Forms.GroupBox
    Friend WithEvents gbxSoiled As System.Windows.Forms.GroupBox
    Friend WithEvents btnToilet2 As System.Windows.Forms.Button
    Friend WithEvents btnToilet1 As System.Windows.Forms.Button
    Friend WithEvents lblToilet2 As System.Windows.Forms.Label
    Friend WithEvents lblToilet1 As System.Windows.Forms.Label
    Friend WithEvents btnToilet6 As System.Windows.Forms.Button
    Friend WithEvents btnToilet5 As System.Windows.Forms.Button
    Friend WithEvents btnToilet4 As System.Windows.Forms.Button
    Friend WithEvents btnToilet3 As System.Windows.Forms.Button
    Friend WithEvents lblToilet6 As System.Windows.Forms.Label
    Friend WithEvents lblToilet5 As System.Windows.Forms.Label
    Friend WithEvents lblToilet4 As System.Windows.Forms.Label
    Friend WithEvents lblToilet3 As System.Windows.Forms.Label
    Friend WithEvents lblToilet12 As System.Windows.Forms.Label
    Friend WithEvents lblToilet11 As System.Windows.Forms.Label
    Friend WithEvents btnToilet7 As System.Windows.Forms.Button
    Friend WithEvents lblToilet10 As System.Windows.Forms.Label
    Friend WithEvents btnToilet8 As System.Windows.Forms.Button
    Friend WithEvents lblToilet9 As System.Windows.Forms.Label
    Friend WithEvents btnToilet9 As System.Windows.Forms.Button
    Friend WithEvents lblToilet8 As System.Windows.Forms.Label
    Friend WithEvents btnToilet10 As System.Windows.Forms.Button
    Friend WithEvents lblToilet7 As System.Windows.Forms.Label
    Friend WithEvents btnToilet11 As System.Windows.Forms.Button
    Friend WithEvents btnToilet12 As System.Windows.Forms.Button
    Friend WithEvents btnDry As System.Windows.Forms.Button
    Friend WithEvents btnWet As System.Windows.Forms.Button
    Friend WithEvents btnSoiled As System.Windows.Forms.Button
    Friend WithEvents btnLoose As System.Windows.Forms.Button
    Friend WithEvents btnNappy As System.Windows.Forms.Button
    Friend WithEvents btnCream As System.Windows.Forms.Button
    Friend WithEvents btnPotty As System.Windows.Forms.Button
    Friend WithEvents btnToilet As System.Windows.Forms.Button

End Class
