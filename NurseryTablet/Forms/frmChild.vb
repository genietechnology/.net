﻿Imports NurseryTablet.SharedModule

Public Class frmChild

    Private m_ChildID As String
    Private m_ChildName As String
    Private m_ChildPair As New Pair
    Private m_ChildInOut As Enums.InOut

    Private m_ChildRecord As NurseryGenieData.Child
    Private m_Basic As New List(Of ChildBasic)
    Private m_Medical As New List(Of ChildMedical)

    Private m_Feedback As Boolean = False
    Private m_FeedbackText As String = ""

    Public Property ChildID() As String
        Get
            Return m_ChildID
        End Get
        Set(ByVal value As String)
            m_ChildID = value
        End Set
    End Property

    Public Property ChildName() As String
        Get
            Return m_ChildName
        End Get
        Set(ByVal value As String)
            m_ChildName = value
        End Set
    End Property

    Private Sub frmChild_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        DisplayDetails()
    End Sub

    Private Sub DisplayDetails()

        m_ChildRecord = Business.Child.ReturnChildByID(m_ChildID)

        m_ChildPair.Code = m_ChildID
        m_ChildPair.Text = m_ChildName

        m_ChildInOut = PersonInorOut(m_ChildID)

        Dim _ChildRecord As NurseryGenieData.Child = Business.Child.ReturnChildByID(m_ChildID)
        With _ChildRecord

            Dim _C As New ChildBasic
            _C.Forename = .Forename
            _C.Surname = .Surname
            _C.DOB = .DOB
            _C.Gender = .Gender
            _C.Group = .GroupName
            _C.Nappies = .Nappies

            m_Basic.Add(_C)
            vgBasic.DataSource = m_Basic

            Dim _M As New ChildMedical
            _M.AllergyRating = .AllergyRating
            _M.DietRestriction = .DietRestrict
            _M.DietNotes = .DietNotes
            _M.AllergyRating = .AllergyRating
            _M.AllergyNotes = .AllergyNotes
            _M.Medication = .Medication
            _M.MedicalNotes = .MedicalNotes
            _M.Doctor = .Doctor
            _M.Surgery = .DoctorSurgery
            _M.SurgeryTel = .DoctorTel
            _M.HealthVisitor = .HVName
            _M.HealthVisitorTel = .HVTel
            _M.HealthVisitorMobile = .HVMobile

            m_Medical.Add(_M)
            vgMedical.DataSource = m_Medical

            Dim _ConsentItems = From _Consent As NurseryGenieData.ChildAttribute In _ChildRecord.Attributes
                                Where _Consent.ChildID = _ChildRecord.ID And _Consent.AttributeType = "Consent"
                                Order By _Consent.Description

            PopulateGrid(Of NurseryGenieData.ChildAttribute)(tgConsent, _ConsentItems)

            Dim _AllergyItems = From _Allergy As NurseryGenieData.ChildAttribute In _ChildRecord.Attributes
                                Where _Allergy.ChildID = _ChildRecord.ID And _Allergy.AttributeType = "AllergyMatrix"
                                Order By _Allergy.Description

            PopulateGrid(Of NurseryGenieData.ChildAttribute)(tgAllergies, _AllergyItems)

        End With

        SetButtons()

    End Sub

    Private Sub PopulateGrid(Of ChildAttribute)(ByRef GridControl As TouchGrid, ByRef Attributes As IOrderedEnumerable(Of NurseryGenieData.ChildAttribute))

        Dim _List As New List(Of String)
        If Attributes IsNot Nothing Then

            For Each _A As NurseryGenieData.ChildAttribute In Attributes
                _List.Add(_A.Description)
            Next

        End If

        GridControl.Populate(_List)

    End Sub

    Private Sub SetButtons()

        Dim _CheckedIn As Boolean = False
        If m_ChildInOut = Enums.InOut.CheckIn Then _CheckedIn = True

        btnCheckInChild.Enabled = Not _CheckedIn
        btnCheckOutChild.Enabled = _CheckedIn
        btnMoveRoom.Enabled = Not Parameters.DisableLocation

        btnPhoto.Enabled = False

        btnToilet.Enabled = _CheckedIn
        btnMilk.Enabled = _CheckedIn

        btnObs.Enabled = False
        btnFood.Enabled = _CheckedIn
        btnSleep.Enabled = _CheckedIn

        btnFeedback.Enabled = _CheckedIn
        btnIncidents.Enabled = _CheckedIn
        btnSuncream.Enabled = False
        btnRequests.Enabled = _CheckedIn

        btnReport.Enabled = True
        btnContacts.Enabled = True

        btnLogMedicine.Enabled = _CheckedIn
        btnCrossCheck.Enabled = _CheckedIn
        btnMedAuth.Enabled = _CheckedIn

    End Sub

    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub btnToilet_Click(sender As System.Object, e As System.EventArgs) Handles btnToilet.Click
        btnToilet.Enabled = False
        ChildToilet(m_ChildID)
        btnToilet.Enabled = True
    End Sub

    Private Sub btnFood_Click(sender As System.Object, e As System.EventArgs) Handles btnFood.Click
        btnFood.Enabled = False
        ChildFood(m_ChildID)
        btnFood.Enabled = True
    End Sub

    Private Sub btnSleep_Click(sender As System.Object, e As System.EventArgs) Handles btnSleep.Click
        btnSleep.Enabled = False
        ChildSleep(m_ChildID)
        btnSleep.Enabled = True
    End Sub

    Private Sub btnRequests_Click(sender As System.Object, e As System.EventArgs) Handles btnRequests.Click
        btnRequests.Enabled = False
        ChildRequests(m_ChildID)
        btnRequests.Enabled = True
    End Sub

    Private Sub btnMilk_Click(sender As System.Object, e As System.EventArgs) Handles btnMilk.Click
        btnMilk.Enabled = False
        ChildMilk(m_ChildID)
        btnMilk.Enabled = True
    End Sub

    Private Sub btnCheckInChild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheckInChild.Click
        If Not CheckinsPermitted() Then Exit Sub
        Business.Register.RegisterTransaction(m_ChildID, Enums.PersonType.Child, m_ChildName, Enums.InOut.CheckIn, Enums.RegisterVia.Child)
        m_ChildInOut = PersonInorOut(m_ChildID)
        SetButtons()
    End Sub

    Private Sub btnCheckOutChild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheckOutChild.Click
        If Not SharedModule.CheckinsPermitted Then Exit Sub
        If SharedModule.DoCheckOut(Me.ChildID, Me.ChildName) Then
            Me.Close()
        End If
    End Sub

    Private Sub btnLogMedicine_Click(sender As Object, e As EventArgs) Handles btnLogMedicine.Click
        SharedModule.LogMedicine(frmMedicalLog.EnumMode.CreateLog, m_ChildID)
    End Sub

    Private Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnReport.Click

        btnReport.Enabled = False
        Me.Cursor = Cursors.AppStarting

        ServiceHandler.ViewReport(m_ChildID)

        btnReport.Enabled = True
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub btnContacts_Click(sender As Object, e As EventArgs) Handles btnContacts.Click

        Dim _Contacts As List(Of NurseryGenieData.Contact) = Business.Child.GetContacts(m_ChildID)
        If _Contacts IsNot Nothing AndAlso _Contacts.Count > 0 Then

            Dim _List As New List(Of GridContact)

            For Each _C As NurseryGenieData.Contact In _Contacts
                Dim _GC As New GridContact(_C)
                _List.add(_GC)
            Next

            SharedModule.DisplayGridData("Contacts for " + ChildName, _List)

        End If

    End Sub

    'Private Sub btnPhoto_Click(sender As Object, e As EventArgs) Handles btnPhoto.Click

    '    Dim _Result As Integer = 0
    '    Dim _File As String = MediaHandler.CaptureNow

    '    'If _File <> "" Then

    '    '    btnPhoto.Enabled = False
    '    '    Me.Cursor = Cursors.WaitCursor
    '    '    Application.DoEvents()

    '    '    Dim _F As New FileDetail(_File)
    '    '    Dim _M As New MediaContent(MediaHandler.EnumCaptureCategory.Media, m_ChildID, _F)

    '    '    Dim _ID As Guid? = MediaHandler.SaveMedia(_M)

    '    '    Dim _Service As New NurseryGenieService.NurseryGenieLocalClient

    '    '    Try
    '    '        _result = _Service.AttachChildPhoto(m_ChildID, _ID.Value.ToString)
    '    '        _Service.Close()

    '    '    Catch tex As TimeoutException
    '    '        If Parameters.DebugMode Then
    '    '            Msgbox(tex.Message, MessageBoxIcon.Error, "TimeOut Exception")
    '    '        Else
    '    '            Msgbox("Unable to attach photo.", MessageBoxIcon.Error, "Attach Photo")
    '    '        End If
    '    '        _Service.Abort()

    '    '    Catch cex As ServiceModel.CommunicationException
    '    '        If Parameters.DebugMode Then
    '    '            Msgbox(cex.Message, MessageBoxIcon.Error, "Communication Exception")
    '    '        Else
    '    '            Msgbox("Unable to attach photo.", MessageBoxIcon.Error, "Attach Photo")
    '    '        End If
    '    '        _Service.Abort()

    '    '    End Try

    '    '    _Service.Close()

    '    'End If

    '    btnPhoto.Enabled = True
    '    Me.Cursor = Cursors.Default
    '    Application.DoEvents()

    '    If _Result = 1 Then
    '        Msgbox("Profile picture updated successfully.", MessageBoxIcon.Information, "Attach Photo")
    '    End If

    'End Sub

    Private Sub btnFeedback_Click(sender As Object, e As EventArgs) Handles btnFeedback.Click

        m_FeedbackText = SharedModule.OSK("Personal Feedback:", True, True, m_FeedbackText)

        If m_FeedbackText = "" Then
            m_Feedback = False
            DeleteActivity(Parameters.TodayID, Me.ChildID, EnumActivityType.Feedback)
        Else
            m_Feedback = True
            DeleteActivity(Parameters.TodayID, Me.ChildID, EnumActivityType.Feedback)
            LogActivity(Parameters.TodayID, Me.ChildID, Me.ChildName, EnumActivityType.Feedback, "", Notes:=m_FeedbackText)
        End If

    End Sub

    Private Sub btnIncidents_Click(sender As Object, e As EventArgs) Handles btnIncidents.Click
        btnIncidents.Enabled = False
        SharedModule.ChildIncidents(m_ChildID)
        btnIncidents.Enabled = True
    End Sub

    Private Sub btnMedAuth_Click(sender As Object, e As EventArgs) Handles btnMedAuth.Click
        btnIncidents.Enabled = False
        SharedModule.ChildMedicalAuth(m_ChildID)
        btnIncidents.Enabled = True
    End Sub

    Private Class GridContact

        Public Sub New(ByVal Contact As NurseryGenieData.Contact)
            Me.Forename = Contact.Forename
            Me.Surname = Contact.Surname
            Me.Relationship = Contact.Relationship
            Me.TelHome = Contact.TelHome
            Me.TelWork = Contact.JobTel
            Me.TelMobile = Contact.TelMobile
            Me.Parent = Contact.Parent
            Me.CanCollect = Contact.Collect
            Me.Emergency = Contact.EmerCont
            Me.Password = Contact.Password
        End Sub

        Property Forename As String
        Property Surname As String
        Property Relationship As String
        Property TelHome As String
        Property TelWork As String
        Property TelMobile As String
        Property Parent As Boolean
        Property CanCollect As Boolean
        Property Emergency As Boolean
        Property Password As String

    End Class

    Private Class ChildBasic
        Property Forename() As String
        Property Surname() As String
        Property DOB() As Date
        Property Gender As String
        Property Group() As String
        Property Nappies() As Boolean
    End Class

    Private Class ChildMedical
        Property SEN() As Boolean
        Property DietRestriction() As String
        Property DietNotes() As String
        Property AllergyRating() As String
        Property AllergyNotes() As String
        Property Medication() As String
        Property MedicalNotes() As String
        Property Doctor() As String
        Property Surgery() As String
        Property SurgeryTel() As String
        Property HealthVisitor() As String
        Property HealthVisitorTel() As String
        Property HealthVisitorMobile() As String
    End Class

    Private Sub DisplayText(ByRef sender As Object, e As EventArgs)
        Dim _Grid As DevExpress.XtraVerticalGrid.VGridControl = CType(sender, DevExpress.XtraVerticalGrid.VGridControl)
        Dim _pt As Point = _Grid.PointToClient(Control.MousePosition)
        Dim _info As DevExpress.XtraVerticalGrid.VGridHitInfo = _Grid.CalcHitInfo(_pt)
        If _info.Row IsNot Nothing Then
            Dim _text As String = _Grid.GetCellDisplayText(_info.Row, _info.RecordIndex)
            Msgbox(_text, MessageBoxIcon.Information, "More Information")
        End If
    End Sub

    Private Sub vgMedical_DoubleClick(sender As Object, e As EventArgs) Handles vgMedical.DoubleClick
        DisplayText(sender, e)
    End Sub

    Private Sub vgBasic_DoubleClick(sender As Object, e As EventArgs) Handles vgBasic.DoubleClick
        DisplayText(sender, e)
    End Sub

End Class
