﻿Imports NurseryTablet.NurseryGenieData
Imports NurseryTablet.SharedModule

Public Class frmMedicalAuth

    Implements IAddRemoveChildForm

    Public Sub DrillDown(ChildID As String, RecordID As String) Implements IAddRemoveChildForm.DrillDown

        Me.ChildID = ChildID
        Me.RecordID = RecordID

        Clear()

        If Not Me.IsNew Then DisplayRecord()

    End Sub

    Private Sub DisplayRecord()

        Clear()

        If Me.ChildID = "" Then Exit Sub

        Dim _Auth As NurseryGenieData.MedicineAuth = Business.Medicine.GetMedicationAuth(Me.RecordID)
        If _Auth IsNot Nothing Then

            lblContact.Tag = _Auth.ContactId.ToString
            lblContact.Text = _Auth.ContactName
            btnSigContact.Tag = _Auth.ContactSig.ToString

            scrStaff.ValueID = _Auth.StaffId.ToString
            scrStaff.ValueText = _Auth.StaffName
            btnSigStaff.Tag = _Auth.StaffSig.ToString

            lblNature.Text = _Auth.Illness
            lblMedicine.Text = _Auth.Medicine
            lblDosage.Text = _Auth.Dosage

            If Not _Auth.LastGiven.HasValue Then
                lblLastParent.Text = "Not Applicable"
            Else
                lblLastParent.Text = Format(_Auth.LastGiven, "HH:mm")
            End If

            If _Auth.Frequency Is Nothing Then
                lblFrequency.Text = "Not Applicable"
            Else
                If _Auth.Frequency = "" Then
                    lblFrequency.Text = "Not Applicable"
                Else
                    lblFrequency.Text = _Auth.Frequency
                End If
            End If

            If _Auth.DosagesDue Is Nothing Then
                lblDosagesDue.Text = "As Required"
            Else
                If _Auth.Frequency = "" Then
                    lblDosagesDue.Text = "As Required"
                Else
                    lblDosagesDue.Text = _Auth.DosagesDue
                End If
            End If

        End If

    End Sub

    Protected Overrides Function BeforeCommitUpdate() As Boolean
        If HasActivity() Then
            Return CheckAnswers()
        Else
            Return True
        End If
    End Function

    Private Function HasActivity() As Boolean

        If lblContact.Text <> "" Then Return True

        If lblNature.Text <> "" Then Return True
        If lblMedicine.Text <> "" Then Return True
        If lblDosage.Text <> "" Then Return True
        If lblLastParent.Text <> "" Then Return True
        If lblFrequency.Text <> "" Then Return True
        If lblDosagesDue.Text <> "" Then Return True

        Return False

    End Function

    Private Function CheckAnswers() As Boolean

        'check we have completed everything
        If lblContact.Text = "" Then
            Msgbox("Please select the contact.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If lblNature.Text = "" Then
            Msgbox("Please enter the nature of the illness.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If lblMedicine.Text = "" Then
            Msgbox("Please enter the name of the medicine to be given.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If lblDosage.Text = "" Then
            Msgbox("Please enter the dosage to be given.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If lblLastParent.Text = "" Then
            Msgbox("Please select the time the last dosage was given by the parent.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If lblFrequency.Text = "" Then
            Msgbox("Please select the frequency between dosages (in hours).", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        'check we have the signatures
        If btnSigContact.Tag = "" Then
            Msgbox("Contact signature is mandatory.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If btnSigStaff.Tag = "" Then
            Msgbox("Staff signature is mandatory.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        Return True

    End Function

    Protected Overrides Sub CommitUpdate()

        Dim _Auth As MedicineAuth = Nothing

        If Me.IsNew Then
            _Auth = New MedicineAuth
            _Auth.ID = Guid.NewGuid()
            _Auth.DayId = New Guid(TodayID)
            _Auth.ChildId = New Guid(ChildID)
            _Auth.ChildName = ChildName
        Else
            _Auth = Business.Medicine.GetMedicationAuth(Me.RecordID)
        End If

        With _Auth

            .ContactId = lblContact.Tag
            .ContactName = lblContact.Text
            .ContactSig = btnSigContact.Tag

            .StaffId = New Guid(scrStaff.ValueID)
            .StaffName = scrStaff.ValueText
            .StaffSig = btnSigStaff.Tag

            .Illness = lblNature.Text
            .Medicine = lblMedicine.Text
            .Dosage = lblDosage.Text

            If lblLastParent.Text = "Not Applicable" Then
                .LastGiven = Nothing
                .Frequency = ""
                .DosagesDue = ""
            Else
                .LastGiven = lblLastParent.Text
                .Frequency = lblFrequency.Text
                .DosagesDue = lblDosagesDue.Text
            End If

            .Stamp = Now

        End With

        If Me.IsNew Then
            Business.Medicine.CreateMedicationAuth(_Auth)
        Else
            Business.Medicine.UpdateMedicationAuth(_Auth)
        End If

        Business.Medicine.DeleteLogsByAuthorisation(_Auth.ID.ToString)
        CalculateTimes(True, _Auth.ID)

    End Sub

    Private Sub Clear()
        lblContact.Text = ""
        lblNature.Text = ""
        lblMedicine.Text = ""
        lblDosage.Text = ""
        lblLastParent.Text = ""
        lblFrequency.Text = ""
        btnSigStaff.Tag = ""
        btnSigContact.Tag = ""
        lblDosagesDue.Text = ""
    End Sub

    Private Sub btnContact_Click(sender As Object, e As EventArgs) Handles btnContact.Click

        Dim _SQL As String = "select id, fullname from Contacts" & _
                             " where Contacts.family_id = '" + FamilyID + "'"

        Dim _P As Pair = SharedModule.ReturnButtonSQL(_SQL, "Select Contact")
        If _P IsNot Nothing Then
            lblContact.Tag = _P.Code
            lblContact.Text = _P.Text
        End If

    End Sub

    Private Sub btnNature_Click(sender As Object, e As EventArgs) Handles btnNature.Click
        lblNature.Text = OSK("Nature of Illness", False, False, lblNature.Text, "Nature of Illness", 100)
    End Sub

    Private Sub btnMedicine_Click(sender As Object, e As EventArgs) Handles btnMedicine.Click
        lblMedicine.Text = OSK("Medicine to be given", False, False, lblMedicine.Text, "Medicine", 100)
    End Sub

    Private Sub btnDosage_Click(sender As Object, e As EventArgs) Handles btnDosage.Click
        lblDosage.Text = OSK("Dosage", False, True, lblDosage.Text, "Dosage", 100)
    End Sub

    Private Sub btnLastParent_Click(sender As Object, e As EventArgs) Handles btnLastParent.Click
        lblLastParent.Text = TimeEntry(, True)
        CalculateTimes(False)
    End Sub

    Private Sub btnNextDue_Click(sender As Object, e As EventArgs)
        lblNextDue.Text = TimeEntry()
    End Sub

    Private Sub btnFrequency_Click(sender As Object, e As EventArgs) Handles btnFrequency.Click
        lblFrequency.Text = NumberEntry("Frequency")
        CalculateTimes(False)
    End Sub

    Private Sub btnSigContact_Click(sender As Object, e As EventArgs) Handles btnSigContact.Click
        LegacySignatureCapture(btnSigContact.Tag)
    End Sub

    Private Sub btnSigStaff_Click(sender As Object, e As EventArgs) Handles btnSigStaff.Click
        LegacySignatureCapture(btnSigStaff.Tag)
    End Sub

    Private Sub CalculateTimes(ByVal GenerateLogs As Boolean, Optional ByVal AuthID As Guid? = Nothing)

        If lblLastParent.Text = "" Then Exit Sub
        If lblLastParent.Text = "Not Applicable" Then Exit Sub
        If lblFrequency.Text = "" Then Exit Sub
        If lblFrequency.Text = "Not Applicable" Then Exit Sub

        Dim _Freq As Double = lblFrequency.Text
        Dim _Dosages As String = ""

        Dim _LastTime As Date = DateAndTime.Today + " 23:59:00"
        Dim _LastGiven As Date = DateAndTime.Today + " " + lblLastParent.Text

        Dim _Next As Date = _LastGiven
        _Next = DateAdd(DateInterval.Hour, _Freq, _Next)

        While _Next.Day = _LastTime.Day

            If GenerateLogs Then CreateMedicineLog(_Next, AuthID)

            If _Dosages = "" Then
                _Dosages += Format(_Next, "HH:mm").ToString
            Else
                _Dosages += ", " + Format(_Next, "HH:mm").ToString
            End If

            _Next = DateAdd(DateInterval.Hour, _Freq, _Next)

        End While

        lblDosagesDue.Text = _Dosages

    End Sub

    Private Sub CreateMedicineLog(ByVal NextDate As Date, ByVal AuthID As Guid)

        Dim _Log As New NurseryGenieData.MedicineLog
        With _Log
            .ID = Guid.NewGuid
            .DayId = New Guid(TodayID)
            .AuthId = AuthID
            .Due = NextDate
            .ChildId = New Guid(ChildID)
            .ChildName = ChildName
            .Illness = lblNature.Text
            .Medicine = lblMedicine.Text
            .Dosage = lblDosage.Text
            .Button = Me.ChildForename + " @ " + Format(NextDate, "HH:mm").ToString
        End With

        Business.Medicine.StoreLog(_Log)

    End Sub

    Private Sub btnAsRequired_Click(sender As Object, e As EventArgs) Handles btnAsRequired.Click
        lblFrequency.Text = "Not Applicable"
        lblDosagesDue.Text = "As Required"
    End Sub

End Class
