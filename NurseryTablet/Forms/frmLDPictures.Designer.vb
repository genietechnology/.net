﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLDPictures
    Inherits frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.sldImage = New DevExpress.XtraEditors.Controls.ImageSlider()
        CType(Me.sldImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(692, 469)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(320, 57)
        Me.btnCancel.TabIndex = 39
        Me.btnCancel.Text = "Close"
        '
        'sldImage
        '
        Me.sldImage.CurrentImageIndex = -1
        Me.sldImage.LayoutMode = DevExpress.Utils.Drawing.ImageLayoutMode.ZoomInside
        Me.sldImage.Location = New System.Drawing.Point(12, 12)
        Me.sldImage.Name = "sldImage"
        Me.sldImage.Size = New System.Drawing.Size(1000, 451)
        Me.sldImage.TabIndex = 40
        Me.sldImage.UseDisabledStatePainter = True
        '
        'frmLDPictures
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(11.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1024, 538)
        Me.Controls.Add(Me.sldImage)
        Me.Controls.Add(Me.btnCancel)
        Me.Name = "frmLDPictures"
        Me.Text = "frmLDPictures"
        CType(Me.sldImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents sldImage As DevExpress.XtraEditors.Controls.ImageSlider
End Class
