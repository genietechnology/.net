﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCrossCheck
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCrossCheck))
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.panBottom = New DevExpress.XtraEditors.PanelControl()
        Me.Splitter2 = New System.Windows.Forms.Splitter()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.txtChild = New DevExpress.XtraEditors.TextEdit()
        Me.txtSleep = New DevExpress.XtraEditors.TextEdit()
        Me.txtMedication = New DevExpress.XtraEditors.TextEdit()
        Me.txtIncidents = New DevExpress.XtraEditors.TextEdit()
        Me.txtNappies = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.txtSuncream = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.txtObservations = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.txtMilk = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.txtFeedback = New DevExpress.XtraEditors.MemoEdit()
        Me.txtBreakfast = New DevExpress.XtraEditors.TextEdit()
        Me.txtSnack = New DevExpress.XtraEditors.TextEdit()
        Me.txtLunch = New DevExpress.XtraEditors.TextEdit()
        Me.txtLunchDessert = New DevExpress.XtraEditors.TextEdit()
        Me.txtTea = New DevExpress.XtraEditors.TextEdit()
        Me.txtTeaDessert = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.txtCheckedBy = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.btnReport = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAccept = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panBottom.SuspendLayout()
        CType(Me.txtChild.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSleep.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMedication.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtIncidents.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNappies.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSuncream.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObservations.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMilk.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFeedback.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBreakfast.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSnack.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLunch.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLunchDessert.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTea.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTeaDessert.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCheckedBy.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(12, 15)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(44, 25)
        Me.LabelControl2.TabIndex = 0
        Me.LabelControl2.Text = "Child"
        '
        'panBottom
        '
        Me.panBottom.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panBottom.Appearance.Options.UseFont = True
        Me.panBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.panBottom.Controls.Add(Me.btnReport)
        Me.panBottom.Controls.Add(Me.Splitter2)
        Me.panBottom.Controls.Add(Me.btnAccept)
        Me.panBottom.Controls.Add(Me.btnCancel)
        Me.panBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panBottom.Location = New System.Drawing.Point(0, 395)
        Me.panBottom.Name = "panBottom"
        Me.panBottom.Size = New System.Drawing.Size(1019, 56)
        Me.panBottom.TabIndex = 0
        '
        'Splitter2
        '
        Me.Splitter2.Location = New System.Drawing.Point(0, 0)
        Me.Splitter2.Name = "Splitter2"
        Me.Splitter2.Size = New System.Drawing.Size(3, 56)
        Me.Splitter2.TabIndex = 0
        Me.Splitter2.TabStop = False
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(12, 205)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(95, 25)
        Me.LabelControl1.TabIndex = 9
        Me.LabelControl1.Text = "Medication"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.Location = New System.Drawing.Point(12, 91)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(42, 25)
        Me.LabelControl3.TabIndex = 3
        Me.LabelControl3.Text = "Food"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl4.Location = New System.Drawing.Point(12, 167)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(118, 25)
        Me.LabelControl4.TabIndex = 7
        Me.LabelControl4.Text = "Sleep Records"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl5.Location = New System.Drawing.Point(12, 243)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(76, 25)
        Me.LabelControl5.TabIndex = 11
        Me.LabelControl5.Text = "Incidents"
        '
        'txtChild
        '
        Me.txtChild.Location = New System.Drawing.Point(169, 12)
        Me.txtChild.Name = "txtChild"
        Me.txtChild.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtChild.Properties.Appearance.Options.UseFont = True
        Me.txtChild.Properties.AutoHeight = False
        Me.txtChild.Properties.ReadOnly = True
        Me.txtChild.Size = New System.Drawing.Size(417, 32)
        Me.txtChild.TabIndex = 1
        '
        'txtSleep
        '
        Me.txtSleep.Location = New System.Drawing.Point(169, 164)
        Me.txtSleep.Name = "txtSleep"
        Me.txtSleep.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSleep.Properties.Appearance.Options.UseFont = True
        Me.txtSleep.Properties.AutoHeight = False
        Me.txtSleep.Properties.ReadOnly = True
        Me.txtSleep.Size = New System.Drawing.Size(276, 32)
        Me.txtSleep.TabIndex = 8
        '
        'txtMedication
        '
        Me.txtMedication.Location = New System.Drawing.Point(169, 202)
        Me.txtMedication.Name = "txtMedication"
        Me.txtMedication.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMedication.Properties.Appearance.Options.UseFont = True
        Me.txtMedication.Properties.AutoHeight = False
        Me.txtMedication.Properties.ReadOnly = True
        Me.txtMedication.Size = New System.Drawing.Size(276, 32)
        Me.txtMedication.TabIndex = 10
        '
        'txtIncidents
        '
        Me.txtIncidents.Location = New System.Drawing.Point(169, 240)
        Me.txtIncidents.Name = "txtIncidents"
        Me.txtIncidents.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIncidents.Properties.Appearance.Options.UseFont = True
        Me.txtIncidents.Properties.AutoHeight = False
        Me.txtIncidents.Properties.ReadOnly = True
        Me.txtIncidents.Size = New System.Drawing.Size(276, 32)
        Me.txtIncidents.TabIndex = 12
        '
        'txtNappies
        '
        Me.txtNappies.Location = New System.Drawing.Point(169, 126)
        Me.txtNappies.Name = "txtNappies"
        Me.txtNappies.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNappies.Properties.Appearance.Options.UseFont = True
        Me.txtNappies.Properties.AutoHeight = False
        Me.txtNappies.Properties.ReadOnly = True
        Me.txtNappies.Size = New System.Drawing.Size(276, 32)
        Me.txtNappies.TabIndex = 6
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl6.Location = New System.Drawing.Point(12, 129)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(133, 25)
        Me.LabelControl6.TabIndex = 5
        Me.LabelControl6.Text = "Toilet / Nappies"
        '
        'txtSuncream
        '
        Me.txtSuncream.Location = New System.Drawing.Point(169, 278)
        Me.txtSuncream.Name = "txtSuncream"
        Me.txtSuncream.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSuncream.Properties.Appearance.Options.UseFont = True
        Me.txtSuncream.Properties.AutoHeight = False
        Me.txtSuncream.Properties.ReadOnly = True
        Me.txtSuncream.Size = New System.Drawing.Size(276, 32)
        Me.txtSuncream.TabIndex = 14
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl7.Location = New System.Drawing.Point(12, 281)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(84, 25)
        Me.LabelControl7.TabIndex = 13
        Me.LabelControl7.Text = "Suncream"
        '
        'txtObservations
        '
        Me.txtObservations.Location = New System.Drawing.Point(169, 354)
        Me.txtObservations.Name = "txtObservations"
        Me.txtObservations.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObservations.Properties.Appearance.Options.UseFont = True
        Me.txtObservations.Properties.AutoHeight = False
        Me.txtObservations.Properties.ReadOnly = True
        Me.txtObservations.Size = New System.Drawing.Size(276, 32)
        Me.txtObservations.TabIndex = 18
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl8.Location = New System.Drawing.Point(12, 357)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(110, 25)
        Me.LabelControl8.TabIndex = 17
        Me.LabelControl8.Text = "Observations"
        '
        'txtMilk
        '
        Me.txtMilk.Location = New System.Drawing.Point(169, 316)
        Me.txtMilk.Name = "txtMilk"
        Me.txtMilk.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMilk.Properties.Appearance.Options.UseFont = True
        Me.txtMilk.Properties.AutoHeight = False
        Me.txtMilk.Properties.ReadOnly = True
        Me.txtMilk.Size = New System.Drawing.Size(276, 32)
        Me.txtMilk.TabIndex = 16
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl9.Location = New System.Drawing.Point(12, 319)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(37, 25)
        Me.LabelControl9.TabIndex = 15
        Me.LabelControl9.Text = "Milk"
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl10.Location = New System.Drawing.Point(451, 129)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(161, 25)
        Me.LabelControl10.TabIndex = 2
        Me.LabelControl10.Text = "Personal Feedback:"
        '
        'txtFeedback
        '
        Me.txtFeedback.Location = New System.Drawing.Point(451, 164)
        Me.txtFeedback.Name = "txtFeedback"
        Me.txtFeedback.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFeedback.Properties.Appearance.Options.UseFont = True
        Me.txtFeedback.Properties.ReadOnly = True
        Me.txtFeedback.Size = New System.Drawing.Size(558, 222)
        Me.txtFeedback.TabIndex = 19
        '
        'txtBreakfast
        '
        Me.txtBreakfast.Location = New System.Drawing.Point(169, 88)
        Me.txtBreakfast.Name = "txtBreakfast"
        Me.txtBreakfast.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBreakfast.Properties.Appearance.Options.UseFont = True
        Me.txtBreakfast.Properties.AutoHeight = False
        Me.txtBreakfast.Properties.ReadOnly = True
        Me.txtBreakfast.Size = New System.Drawing.Size(135, 32)
        Me.txtBreakfast.TabIndex = 20
        '
        'txtSnack
        '
        Me.txtSnack.Location = New System.Drawing.Point(310, 88)
        Me.txtSnack.Name = "txtSnack"
        Me.txtSnack.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSnack.Properties.Appearance.Options.UseFont = True
        Me.txtSnack.Properties.AutoHeight = False
        Me.txtSnack.Properties.ReadOnly = True
        Me.txtSnack.Size = New System.Drawing.Size(135, 32)
        Me.txtSnack.TabIndex = 21
        '
        'txtLunch
        '
        Me.txtLunch.Location = New System.Drawing.Point(451, 88)
        Me.txtLunch.Name = "txtLunch"
        Me.txtLunch.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLunch.Properties.Appearance.Options.UseFont = True
        Me.txtLunch.Properties.AutoHeight = False
        Me.txtLunch.Properties.ReadOnly = True
        Me.txtLunch.Size = New System.Drawing.Size(135, 32)
        Me.txtLunch.TabIndex = 22
        '
        'txtLunchDessert
        '
        Me.txtLunchDessert.Location = New System.Drawing.Point(592, 88)
        Me.txtLunchDessert.Name = "txtLunchDessert"
        Me.txtLunchDessert.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLunchDessert.Properties.Appearance.Options.UseFont = True
        Me.txtLunchDessert.Properties.AutoHeight = False
        Me.txtLunchDessert.Properties.ReadOnly = True
        Me.txtLunchDessert.Size = New System.Drawing.Size(135, 32)
        Me.txtLunchDessert.TabIndex = 23
        '
        'txtTea
        '
        Me.txtTea.Location = New System.Drawing.Point(733, 88)
        Me.txtTea.Name = "txtTea"
        Me.txtTea.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTea.Properties.Appearance.Options.UseFont = True
        Me.txtTea.Properties.AutoHeight = False
        Me.txtTea.Properties.ReadOnly = True
        Me.txtTea.Size = New System.Drawing.Size(135, 32)
        Me.txtTea.TabIndex = 24
        '
        'txtTeaDessert
        '
        Me.txtTeaDessert.Location = New System.Drawing.Point(874, 88)
        Me.txtTeaDessert.Name = "txtTeaDessert"
        Me.txtTeaDessert.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTeaDessert.Properties.Appearance.Options.UseFont = True
        Me.txtTeaDessert.Properties.AutoHeight = False
        Me.txtTeaDessert.Properties.ReadOnly = True
        Me.txtTeaDessert.Size = New System.Drawing.Size(135, 32)
        Me.txtTeaDessert.TabIndex = 25
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl11.Location = New System.Drawing.Point(169, 57)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(135, 25)
        Me.LabelControl11.TabIndex = 26
        Me.LabelControl11.Text = "Breakfast"
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl12.Location = New System.Drawing.Point(310, 57)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(135, 25)
        Me.LabelControl12.TabIndex = 27
        Me.LabelControl12.Text = "Snack"
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl13.Location = New System.Drawing.Point(451, 57)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(135, 25)
        Me.LabelControl13.TabIndex = 28
        Me.LabelControl13.Text = "Lunch"
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl14.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl14.Location = New System.Drawing.Point(592, 57)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(135, 25)
        Me.LabelControl14.TabIndex = 29
        Me.LabelControl14.Text = "Lunch Dessert"
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl15.Location = New System.Drawing.Point(733, 57)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(135, 25)
        Me.LabelControl15.TabIndex = 30
        Me.LabelControl15.Text = "Tea"
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl16.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl16.Location = New System.Drawing.Point(874, 57)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(135, 25)
        Me.LabelControl16.TabIndex = 31
        Me.LabelControl16.Text = "Tea Dessert"
        '
        'txtCheckedBy
        '
        Me.txtCheckedBy.Location = New System.Drawing.Point(733, 12)
        Me.txtCheckedBy.Name = "txtCheckedBy"
        Me.txtCheckedBy.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCheckedBy.Properties.Appearance.Options.UseFont = True
        Me.txtCheckedBy.Properties.AutoHeight = False
        Me.txtCheckedBy.Properties.ReadOnly = True
        Me.txtCheckedBy.Size = New System.Drawing.Size(276, 32)
        Me.txtCheckedBy.TabIndex = 32
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl17.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl17.Location = New System.Drawing.Point(592, 15)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(135, 25)
        Me.LabelControl17.TabIndex = 33
        Me.LabelControl17.Text = "Checked By:"
        '
        'btnReport
        '
        Me.btnReport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReport.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReport.Appearance.Options.UseFont = True
        Me.btnReport.Image = Global.NurseryTablet.My.Resources.Resources.report_32
        Me.btnReport.Location = New System.Drawing.Point(12, 3)
        Me.btnReport.Name = "btnReport"
        Me.btnReport.Size = New System.Drawing.Size(192, 45)
        Me.btnReport.TabIndex = 2
        Me.btnReport.Text = "View Report"
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAccept.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAccept.Appearance.Options.UseFont = True
        Me.btnAccept.Image = CType(resources.GetObject("btnAccept.Image"), System.Drawing.Image)
        Me.btnAccept.Location = New System.Drawing.Point(516, 3)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(335, 45)
        Me.btnAccept.TabIndex = 0
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Image = CType(resources.GetObject("btnCancel.Image"), System.Drawing.Image)
        Me.btnCancel.Location = New System.Drawing.Point(857, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(150, 45)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "Cancel"
        '
        'frmCrossCheck
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1019, 451)
        Me.ControlBox = False
        Me.Controls.Add(Me.LabelControl17)
        Me.Controls.Add(Me.txtCheckedBy)
        Me.Controls.Add(Me.LabelControl16)
        Me.Controls.Add(Me.LabelControl15)
        Me.Controls.Add(Me.LabelControl14)
        Me.Controls.Add(Me.LabelControl13)
        Me.Controls.Add(Me.LabelControl12)
        Me.Controls.Add(Me.LabelControl11)
        Me.Controls.Add(Me.txtTeaDessert)
        Me.Controls.Add(Me.txtTea)
        Me.Controls.Add(Me.txtLunchDessert)
        Me.Controls.Add(Me.txtLunch)
        Me.Controls.Add(Me.txtSnack)
        Me.Controls.Add(Me.txtBreakfast)
        Me.Controls.Add(Me.LabelControl10)
        Me.Controls.Add(Me.txtObservations)
        Me.Controls.Add(Me.LabelControl8)
        Me.Controls.Add(Me.txtMilk)
        Me.Controls.Add(Me.LabelControl9)
        Me.Controls.Add(Me.txtSuncream)
        Me.Controls.Add(Me.LabelControl7)
        Me.Controls.Add(Me.LabelControl6)
        Me.Controls.Add(Me.txtNappies)
        Me.Controls.Add(Me.txtIncidents)
        Me.Controls.Add(Me.txtMedication)
        Me.Controls.Add(Me.txtSleep)
        Me.Controls.Add(Me.txtChild)
        Me.Controls.Add(Me.LabelControl5)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.panBottom)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.txtFeedback)
        Me.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmCrossCheck"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panBottom.ResumeLayout(False)
        CType(Me.txtChild.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSleep.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMedication.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtIncidents.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNappies.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSuncream.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObservations.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMilk.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFeedback.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBreakfast.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSnack.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLunch.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLunchDessert.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTea.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTeaDessert.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCheckedBy.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Protected Friend WithEvents panBottom As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnAccept As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtChild As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSleep As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtMedication As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtIncidents As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNappies As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtSuncream As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Splitter2 As System.Windows.Forms.Splitter
    Friend WithEvents txtObservations As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtMilk As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtFeedback As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtBreakfast As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSnack As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLunch As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLunchDessert As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtTea As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtTeaDessert As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtCheckedBy As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnReport As DevExpress.XtraEditors.SimpleButton
End Class
