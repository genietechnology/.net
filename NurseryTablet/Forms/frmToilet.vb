﻿
Option Strict On

Imports NurseryTablet.Business
Imports NurseryTablet.SharedModule.Enums

Public Class frmToilet

    Private m_Method As String = ""
    Private m_ButtonColour As Drawing.Color

    Private Sub frmToilet_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        m_ButtonColour = btnPotty.BackColor
        btnPotty.Enabled = Not Parameters.DisablePotty
    End Sub

    Protected Overrides Sub DisplayRecord()
        DisplayToilet()
    End Sub

    Protected Overrides Sub CommitUpdate()
        'override stub
    End Sub

    Private Sub frmToilet_AcceptChanged(sender As Object, e As AcceptArgs) Handles Me.AcceptChanged
        SetButtons(e.Enabled)
    End Sub

    Private Sub frmToilet_ChildChanged(sender As Object, e As EventArgs) Handles Me.ChildChanged

        If Me.Nappies Then
            SetNappy()
        Else
            SetToilet()
        End If

        DisplayToilet()

    End Sub

    Private Sub SetButtons(ByVal Enabled As Boolean)
        btnDry.Enabled = Enabled
        btnWet.Enabled = Enabled
        btnSoiled.Enabled = Enabled
        btnLoose.Enabled = Enabled
        btnNappy.Enabled = Enabled
        btnCream.Enabled = Enabled
    End Sub

    Private Sub Clear()

        gbxWet.Tag = 0
        gbxSoiled.Tag = 0

        Dim i As Integer = 1
        For i = 1 To 12
            SetControl("lblToilet", i, "", "", False)
            SetControl("btnToilet", i, "", "", False)
        Next

    End Sub

    Private Sub SetControl(ByVal ControlName As String, ByVal ControlIndex As Integer, ByVal Text As String, ByVal Tag As String, ByVal Visible As Boolean)

        Dim _name As String = ControlName + ControlIndex.ToString
        Dim _ctrl As Control() = Me.Controls.Find(_name, True)
        If _ctrl.Length = 1 Then
            _ctrl(0).Text = Text
            _ctrl(0).Tag = Tag
            _ctrl(0).Visible = Visible
        End If

    End Sub

    Private Sub DisplayToilet()

        Clear()

        If Not Me.ChildrenPopulated Then Exit Sub
        If Me.ChildID = "" Then Exit Sub

        Cursor.Current = Cursors.WaitCursor

        Dim _Activity As List(Of NurseryGenieData.Activity) = Business.Activity.ReturnActivity(Me.ChildID, Activity.EnumActivityType.Toilet, EnumActivityOrder.Stamp)
        If _Activity IsNot Nothing Then

            Dim _Count As Integer = 0

            For Each _A In _Activity
                _Count += 1
                SetControl("lblToilet", _Count, _A.Description, _A.ID.ToString, True)
                SetControl("btnToilet", _Count, "", _A.ToString, True)
            Next

        End If

        Cursor.Current = Cursors.Default

    End Sub

    Private Sub LogToilet(ByVal TypeIn As String)

        If Not CheckStaff() Then Exit Sub

        Dim _Nappies As String = ""
        Dim _Description As String = ReturnDescription(TypeIn) + " @ " + Format(Now, "HH:mm") + " by " + txtStaff.Text

        If Nappies Then _Nappies = "NAP"

        SharedModule.LogActivity(Day.TodayIDString, txtChild.Tag.ToString, txtChild.Text, Activity.EnumActivityType.Toilet, _Description, TypeIn, _Nappies, , txtStaff.Tag.ToString, txtStaff.Text)
        DisplayToilet()

    End Sub

    Private Function ReturnDescription(ByVal TypeIn As String) As String

        If TypeIn = "CREAM" Then Return "Cream Applied"

        Dim _Method As String = " (" + m_Method + ")"

        If TypeIn = "DRY" Then Return "Dry" + _Method
        If TypeIn = "WET" Then Return "Wet" + _Method
        If TypeIn = "SOIL" Then Return "Soiled" + _Method
        If TypeIn = "LOOSE" Then Return "Loose" + _Method

        Return "Unknown"

    End Function

    Private Sub DeleteTransaction(ByVal TransactionID As String)
        If Msgbox("Are you sure you want to delete this record?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Confirm Delete") = MsgBoxResult.Yes Then

            Cursor.Current = Cursors.WaitCursor

            Business.Activity.DeleteActivity(TransactionID)
            DisplayToilet()

            Cursor.Current = Cursors.Default

        End If
    End Sub

#Region "Remove Buttons"

    Private Sub btnWet1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnToilet1.Click
        DeleteTransaction(btnToilet1.Tag.ToString)
    End Sub

    Private Sub btnWet2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnToilet2.Click
        DeleteTransaction(btnToilet2.Tag.ToString)
    End Sub

    Private Sub btnWet3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnToilet3.Click
        DeleteTransaction(btnToilet3.Tag.ToString)
    End Sub

    Private Sub btnWet4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnToilet4.Click
        DeleteTransaction(btnToilet4.Tag.ToString)
    End Sub

    Private Sub btnWet5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnToilet5.Click
        DeleteTransaction(btnToilet5.Tag.ToString)
    End Sub

    Private Sub btnWet6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnToilet6.Click
        DeleteTransaction(btnToilet6.Tag.ToString)
    End Sub

    Private Sub btnSoiled1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnToilet7.Click
        DeleteTransaction(btnToilet7.Tag.ToString)
    End Sub

    Private Sub btnSoiled2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnToilet8.Click
        DeleteTransaction(btnToilet8.Tag.ToString)
    End Sub

    Private Sub btnSoiled3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnToilet9.Click
        DeleteTransaction(btnToilet9.Tag.ToString)
    End Sub

    Private Sub btnSoiled4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnToilet10.Click
        DeleteTransaction(btnToilet10.Tag.ToString)
    End Sub

    Private Sub btnSoiled5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnToilet11.Click
        DeleteTransaction(btnToilet11.Tag.ToString)
    End Sub

    Private Sub btnSoiled6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnToilet12.Click
        DeleteTransaction(btnToilet12.Tag.ToString)
    End Sub

#End Region

    Private Sub btnNappy_Click(sender As Object, e As EventArgs) Handles btnNappy.Click
        SetNappy()
    End Sub

    Private Sub btnPotty_Click(sender As Object, e As EventArgs) Handles btnPotty.Click
        ClearMethod()
        m_Method = "Potty"
        btnPotty.BackColor = Color.Yellow
    End Sub

    Private Sub btnToilet_Click(sender As Object, e As EventArgs) Handles btnToilet.Click
        SetToilet()
    End Sub

    Private Sub SetNappy()
        ClearMethod()
        m_Method = "Nappy"
        btnNappy.BackColor = Color.Yellow
    End Sub

    Private Sub SetToilet()
        ClearMethod()
        m_Method = "Toilet"
        btnToilet.BackColor = Color.Yellow
    End Sub

    Private Sub ClearMethod()
        btnNappy.BackColor = m_ButtonColour
        btnPotty.BackColor = m_ButtonColour
        btnToilet.BackColor = m_ButtonColour
    End Sub

    Private Sub btnDry_Click(sender As Object, e As EventArgs) Handles btnDry.Click
        LogToilet("DRY")
    End Sub

    Private Sub btnWet_Click(sender As Object, e As EventArgs) Handles btnWet.Click
        LogToilet("WET")
    End Sub

    Private Sub btnSoiled_Click(sender As Object, e As EventArgs) Handles btnSoiled.Click
        LogToilet("SOIL")
    End Sub

    Private Sub btnLoose_Click(sender As Object, e As EventArgs) Handles btnLoose.Click
        LogToilet("LOOSE")
    End Sub

    Private Sub btnCream_Click(sender As Object, e As EventArgs) Handles btnCream.Click
        LogToilet("CREAM")
    End Sub

End Class
