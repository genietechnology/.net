﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSleepManager
    Inherits frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnClose = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAsleep = New DevExpress.XtraEditors.SimpleButton()
        Me.dshCheck = New NurseryTablet.DashboardLabel()
        Me.dshAsleep = New NurseryTablet.DashboardLabel()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.tbRooms = New DevExpress.XtraBars.Navigation.TileBar()
        Me.tbRoomGroup = New DevExpress.XtraBars.Navigation.TileBarGroup()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.radAwake = New DevExpress.XtraEditors.CheckEdit()
        Me.radAsleep = New DevExpress.XtraEditors.CheckEdit()
        Me.btnCheck = New DevExpress.XtraEditors.SimpleButton()
        Me.timSleepCheck = New System.Windows.Forms.Timer(Me.components)
        Me.tcChildren = New DevExpress.XtraEditors.TileControl()
        Me.tg = New DevExpress.XtraEditors.TileGroup()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.radAwake.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radAsleep.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(718, 404)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(200, 45)
        Me.btnClose.TabIndex = 84
        Me.btnClose.Text = "Close"
        '
        'btnAsleep
        '
        Me.btnAsleep.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAsleep.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAsleep.Appearance.Options.UseFont = True
        Me.btnAsleep.Image = Global.NurseryTablet.My.Resources.Resources.success_32
        Me.btnAsleep.Location = New System.Drawing.Point(218, 404)
        Me.btnAsleep.Name = "btnAsleep"
        Me.btnAsleep.Size = New System.Drawing.Size(200, 45)
        Me.btnAsleep.TabIndex = 80
        Me.btnAsleep.Text = "Child Asleep"
        '
        'dshCheck
        '
        Me.dshCheck.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dshCheck.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.dshCheck.Appearance.Options.UseBackColor = True
        Me.dshCheck.Colour = NurseryTablet.DashboardLabel.EnumColourCode.Green
        Me.dshCheck.LabelBottom = "Minutes"
        Me.dshCheck.LabelMiddle = "55"
        Me.dshCheck.LabelTop = "Last Check"
        Me.dshCheck.Location = New System.Drawing.Point(768, 12)
        Me.dshCheck.Name = "dshCheck"
        Me.dshCheck.Size = New System.Drawing.Size(150, 150)
        Me.dshCheck.TabIndex = 85
        '
        'dshAsleep
        '
        Me.dshAsleep.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dshAsleep.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.dshAsleep.Appearance.Options.UseBackColor = True
        Me.dshAsleep.Colour = NurseryTablet.DashboardLabel.EnumColourCode.Green
        Me.dshAsleep.LabelBottom = "This Site"
        Me.dshAsleep.LabelMiddle = "55"
        Me.dshAsleep.LabelTop = "Asleep"
        Me.dshAsleep.Location = New System.Drawing.Point(612, 12)
        Me.dshAsleep.Name = "dshAsleep"
        Me.dshAsleep.Size = New System.Drawing.Size(150, 150)
        Me.dshAsleep.TabIndex = 86
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupControl2.Controls.Add(Me.tbRooms)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.ShowCaption = False
        Me.GroupControl2.Size = New System.Drawing.Size(200, 437)
        Me.GroupControl2.TabIndex = 87
        Me.GroupControl2.Text = "GroupControl2"
        '
        'tbRooms
        '
        Me.tbRooms.AllowDrag = False
        Me.tbRooms.AllowItemHover = False
        Me.tbRooms.AllowSelectedItem = True
        Me.tbRooms.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbRooms.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        Me.tbRooms.Groups.Add(Me.tbRoomGroup)
        Me.tbRooms.ItemSize = 80
        Me.tbRooms.Location = New System.Drawing.Point(2, 2)
        Me.tbRooms.Name = "tbRooms"
        Me.tbRooms.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.tbRooms.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollButtons
        Me.tbRooms.Size = New System.Drawing.Size(196, 433)
        Me.tbRooms.TabIndex = 88
        Me.tbRooms.VerticalContentAlignment = DevExpress.Utils.VertAlignment.Top
        '
        'tbRoomGroup
        '
        Me.tbRoomGroup.Name = "tbRoomGroup"
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.radAwake)
        Me.GroupControl1.Controls.Add(Me.radAsleep)
        Me.GroupControl1.Location = New System.Drawing.Point(218, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(388, 148)
        Me.GroupControl1.TabIndex = 88
        Me.GroupControl1.Text = "GroupControl1"
        '
        'radAwake
        '
        Me.radAwake.Location = New System.Drawing.Point(207, 5)
        Me.radAwake.Name = "radAwake"
        Me.radAwake.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAwake.Properties.Appearance.Options.UseFont = True
        Me.radAwake.Properties.Appearance.Options.UseTextOptions = True
        Me.radAwake.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.radAwake.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.radAwake.Properties.AutoHeight = False
        Me.radAwake.Properties.Caption = "Children Awake"
        Me.radAwake.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radAwake.Properties.RadioGroupIndex = 2
        Me.radAwake.Size = New System.Drawing.Size(180, 138)
        Me.radAwake.TabIndex = 91
        Me.radAwake.TabStop = False
        '
        'radAsleep
        '
        Me.radAsleep.EditValue = True
        Me.radAsleep.Location = New System.Drawing.Point(5, 5)
        Me.radAsleep.Name = "radAsleep"
        Me.radAsleep.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAsleep.Properties.Appearance.Options.UseFont = True
        Me.radAsleep.Properties.AutoHeight = False
        Me.radAsleep.Properties.Caption = "Children Asleep"
        Me.radAsleep.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radAsleep.Properties.RadioGroupIndex = 2
        Me.radAsleep.Size = New System.Drawing.Size(180, 138)
        Me.radAsleep.TabIndex = 90
        '
        'btnCheck
        '
        Me.btnCheck.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCheck.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCheck.Appearance.Options.UseFont = True
        Me.btnCheck.Image = Global.NurseryTablet.My.Resources.Resources.alarm_32
        Me.btnCheck.Location = New System.Drawing.Point(425, 404)
        Me.btnCheck.Name = "btnCheck"
        Me.btnCheck.Size = New System.Drawing.Size(200, 45)
        Me.btnCheck.TabIndex = 90
        Me.btnCheck.Text = "Sleep Check"
        '
        'timSleepCheck
        '
        '
        'tcChildren
        '
        Me.tcChildren.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tcChildren.DragSize = New System.Drawing.Size(0, 0)
        Me.tcChildren.Groups.Add(Me.tg)
        Me.tcChildren.Location = New System.Drawing.Point(218, 166)
        Me.tcChildren.Name = "tcChildren"
        Me.tcChildren.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.tcChildren.Size = New System.Drawing.Size(700, 232)
        Me.tcChildren.TabIndex = 91
        Me.tcChildren.Text = "TileControl1"
        '
        'tg
        '
        Me.tg.Name = "tg"
        '
        'frmSleepManager
        '
        Me.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.Appearance.Options.UseBackColor = True
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(11.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(930, 461)
        Me.Controls.Add(Me.tcChildren)
        Me.Controls.Add(Me.btnCheck)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.dshAsleep)
        Me.Controls.Add(Me.dshCheck)
        Me.Controls.Add(Me.btnAsleep)
        Me.Controls.Add(Me.btnClose)
        Me.Name = "frmSleepManager"
        Me.Text = "Sleep Management"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Controls.SetChildIndex(Me.btnClose, 0)
        Me.Controls.SetChildIndex(Me.btnAsleep, 0)
        Me.Controls.SetChildIndex(Me.dshCheck, 0)
        Me.Controls.SetChildIndex(Me.dshAsleep, 0)
        Me.Controls.SetChildIndex(Me.GroupControl2, 0)
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        Me.Controls.SetChildIndex(Me.btnCheck, 0)
        Me.Controls.SetChildIndex(Me.tcChildren, 0)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.radAwake.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radAsleep.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnAsleep As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dshCheck As NurseryTablet.DashboardLabel
    Friend WithEvents dshAsleep As NurseryTablet.DashboardLabel
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents tbRooms As DevExpress.XtraBars.Navigation.TileBar
    Friend WithEvents tbRoomGroup As DevExpress.XtraBars.Navigation.TileBarGroup
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents radAwake As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents radAsleep As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents btnCheck As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents timSleepCheck As System.Windows.Forms.Timer
    Friend WithEvents tcChildren As DevExpress.XtraEditors.TileControl
    Friend WithEvents tg As DevExpress.XtraEditors.TileGroup
End Class
