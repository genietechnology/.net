﻿Imports System.IO
Imports System.Runtime.Serialization
Imports System.Security.Cryptography
Imports System.Security.Cryptography.X509Certificates
Imports DevExpress.XtraBars.Ribbon.Accessible
Imports NurseryTablet.NurseryGenieData

Public Class DataBase

    Private Shared ReadOnly EncryptionKey As Byte() = {1, 2, 2, 1, 2, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 1, 2, 2, 1, 2, 6}
    Private Shared ReadOnly EncryptionIV As Byte() = {1, 2, 2, 1, 2, 6, 7, 8, 9, 0, 1, 2, 2, 1, 2, 6}
    Private Shared DBPath As String = Application.StartupPath + "\DB\"

    Public Shared DayRecord As Day = Nothing

    Public Enum EnumSyncOperation
        DownloadAll
        DownloadDay
        SyncAll
        UploadOnly
    End Enum

    Public Class Tables

        Public Shared Parameters As New List(Of TabletParam)
        Public Shared Sites As New List(Of Site)
        Public Shared Lists As New List(Of AppList)
        Public Shared SiteRooms As New List(Of SiteRoom)
        Public Shared Food As New List(Of Food)
        Public Shared Requests As New List(Of Requests)

        Public Shared Children As New List(Of Child)
        Public Shared Staff As New List(Of Staff)
        Public Shared Contacts As New List(Of Contact)

        Public Shared Activity As New List(Of Activity)
        Public Shared Register As New List(Of Register)
        Public Shared FoodRegister As New List(Of FoodRegister)

        Public Shared Bookings As New List(Of Booking)

        Public Shared Incidents As New List(Of Incident)
        Public Shared Media As New List(Of Media)
        Public Shared Signatures As New List(Of NurseryGenieData.Signature)

    End Class

    Public Shared Function CheckOtherLocalData() As Boolean
        LoadFromXML("Requests", Tables.Requests)
        LoadFromXML("Media", Tables.Media)
        LoadFromXML("Incidents", Tables.Incidents)
        LoadFromXML("Signatures", Tables.Signatures)
    End Function

    Public Shared Function CheckForLocalData() As Boolean

        'check the local data path exists, if it doesn't we need to create it
        If Not Directory.Exists(DBPath) Then
            Directory.CreateDirectory(DBPath)
            Return False
        Else

            'check we have the things we need to work without synchronising...
            If Not LoadFromXML("TabletParams", Tables.Parameters) Then Return False
            If Not LoadFromXML("Sites", Tables.Sites) Then Return False
            If Not LoadFromXML("Lists", Tables.Lists) Then Return False
            If Not LoadFromXML("SiteRooms", Tables.SiteRooms) Then Return False
            If Not LoadFromXML("Children", Tables.Children) Then Return False
            If Not LoadFromXML("Contacts", Tables.Contacts) Then Return False
            If Not LoadFromXML("Staff", Tables.Staff) Then Return False
            If Not LoadFromXML("Food", Tables.Food) Then Return False
            If Not LoadFromXML("Day", DayRecord) Then Return False
            If Not LoadFromXML("Register", Tables.Register) Then Return False
            If Not LoadFromXML("Activity", Tables.Activity) Then Return False
            If Not LoadFromXML("FoodRegister", Tables.FoodRegister) Then Return False
            If Not LoadFromXML("Bookings", Tables.Bookings) Then Return False

            Return True

        End If

    End Function

    Public Shared Function Sync(ByVal Operation As EnumSyncOperation, ByVal Prompt As Boolean) As Boolean

        If Msgbox("We need Synchronise data with your Computer." + vbCrLf + _
                  "Please ensure you have enabled WIFI and you have a good signal..." + vbCrLf + vbCrLf + _
                  "Click Yes to Process or No to Cancel.", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Sync Required") = MsgBoxResult.No Then

            Return False

        End If

        Dim _frm As New frmSync(Operation)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            Return True
        Else
            If _frm.m_ErrorMessage <> "" Then Msgbox(_frm.m_ErrorMessage, MessageBoxIcon.Error, "Sync")
            Return False
        End If

    End Function

    Public Shared Function PersistDatabaseLocally() As Boolean

        If Not SaveToXML(Of List(Of TabletParam))(Tables.Parameters, "TabletParams") Then Return False
        If Not SaveToXML(Of List(Of Site))(Tables.Sites, "Sites") Then Return False
        If Not SaveToXML(Of List(Of AppList))(Tables.Lists, "Lists") Then Return False
        If Not SaveToXML(Of List(Of Food))(Tables.Food, "Food") Then Return False
        If Not SaveToXML(Of List(Of Requests))(Tables.Requests, "Requests") Then Return False


        If Not SaveToXML(Of List(Of SiteRoom))(Tables.SiteRooms, "SiteRooms") Then Return False
        If Not SaveToXML(Of List(Of Child))(Tables.Children, "Children") Then Return False
        If Not SaveToXML(Of List(Of Contact))(Tables.Contacts, "Contacts") Then Return False
        If Not SaveToXML(Of List(Of Staff))(Tables.Staff, "Staff") Then Return False
        If Not SaveToXML(Of List(Of Booking))(Tables.Bookings, "Bookings") Then Return False

        If Not SaveToXML(Of Day)(DayRecord, "Day") Then Return False

        If Not PersistRegisterLocally() Then Return False
        If Not PersistActivityLocally() Then Return False
        If Not PersistFoodRegisterLocally() Then Return False
        If Not PersistIncidentsLocally() Then Return False
        If Not PersistMediaLocally() Then Return False
        If Not PersistSignaturesLocally() Then Return False

        Return True

    End Function

    Public Shared Function PersistFoodRegisterLocally() As Boolean
        Return SaveToXML(Of List(Of FoodRegister))(Tables.FoodRegister, "FoodRegister")
    End Function

    Public Shared Function PersistRegisterLocally() As Boolean
        Return SaveToXML(Of List(Of Register))(Tables.Register, "Register")
    End Function

    Public Shared Function PersistActivityLocally() As Boolean
        Return SaveToXML(Of List(Of Activity))(Tables.Activity, "Activity")
    End Function

    Public Shared Function PersistMediaLocally() As Boolean
        Return SaveToXML(Of List(Of Media))(Tables.Media, "Media")
    End Function

    Public Shared Function PersistIncidentsLocally() As Boolean
        Return SaveToXML(Of List(Of Incident))(Tables.Incidents, "Incidents")
    End Function

    Public Shared Function PersistSignaturesLocally() As Boolean
        Return SaveToXML(Of List(Of NurseryGenieData.Signature))(Tables.Signatures, "Signatures")
    End Function

    Private Shared Function SaveToXML(Of T)(TableToPersist As T, ByVal TableName As String) As Boolean

        Dim _OK As Boolean = False
        Dim _File As String = DBPath + TableName + ".xml"
        Dim _AESCSP As New AesCryptoServiceProvider

        Try

            Using _MemoryStream As MemoryStream = New MemoryStream

                Dim _DataContractSerializer As New DataContractSerializer(GetType(T))
                _DataContractSerializer.WriteObject(_MemoryStream, TableToPersist)
                _MemoryStream.Position = 0

                Using _MemoryStreamReader As New StreamReader(_MemoryStream)
                    Using _FileStream As New FileStream(_File, FileMode.Create, FileAccess.Write, FileShare.None)
                        Using _CryptoTransform = _AESCSP.CreateEncryptor(EncryptionKey, EncryptionIV)
                            Using _CryptoStream = New CryptoStream(_FileStream, _CryptoTransform, CryptoStreamMode.Write)
                                Using _CryptoStreamWriter = New StreamWriter(_CryptoStream)
                                    Dim _DataToEncrypt = _MemoryStreamReader.ReadToEnd()
                                    _CryptoStreamWriter.Write(_DataToEncrypt)
                                    _CryptoStreamWriter.Flush()
                                    _OK = True
                                End Using
                            End Using
                        End Using
                    End Using
                End Using

            End Using

        Catch ex As Exception
            _OK = False
        End Try

        Return _OK

    End Function

    Private Shared Function LoadFromXML(Of T)(ByVal TableName As String, ByRef ObjectToReturn As T) As Boolean

        Dim _File As String = DBPath + TableName + ".xml"
        Dim _AESCSP As New AesCryptoServiceProvider

        Try

            Using _FileStream As FileStream = File.Open(_File, FileMode.Open)
                Using _CryptoTransform = _AESCSP.CreateDecryptor(EncryptionKey, EncryptionIV)
                    Using _CryptoStream = New CryptoStream(_FileStream, _CryptoTransform, CryptoStreamMode.Read)
                        Using _CryptoStreamReader = New StreamReader(_CryptoStream)
                            Using _MemoryStream = New MemoryStream
                                Using _MemoryStreamWriter = New StreamWriter(_MemoryStream)

                                    Dim _Decrypted = _CryptoStreamReader.ReadToEnd()
                                    _MemoryStreamWriter.Write(_Decrypted)
                                    _MemoryStreamWriter.Flush()
                                    _MemoryStream.Position = 0

                                    Dim _Serializer = New DataContractSerializer(GetType(T))
                                    ObjectToReturn = _Serializer.ReadObject(_MemoryStream)
                                    Return True

                                End Using
                            End Using
                        End Using
                    End Using
                End Using
            End Using

        Catch ex As Exception
            Return False
        End Try

    End Function

End Class
