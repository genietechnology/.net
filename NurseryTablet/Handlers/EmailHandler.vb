﻿Imports System.Net.Mail
Imports System.Text.RegularExpressions

Public Class EmailHandler

    Private m_SendStatus As Integer = -1 'assume something goes wrong
    Private m_SendErrors As String = "Unhandled Exception"

    Public Property Recipient() As String
    Public Property SenderEmail() As String
    Public Property SenderName() As String
    Public Property Subject() As String
    Public Property Body() As String
    Public Property AttachmentPath() As String

    Public ReadOnly Property SendStatus As Integer
        Get
            Return m_SendStatus
        End Get
    End Property

    Public ReadOnly Property SendErrors As String
        Get
            Return m_SendErrors
        End Get
    End Property

    Public Shared Function AddressIsValid(ByVal EmailAddress As String) As Boolean
        Static _Exp As New Regex("^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$")
        Return _Exp.IsMatch(EmailAddress)
    End Function

    Public Shared Function SendEmail(ByVal Recipient As String, ByVal Subject As String, ByVal Body As String, Optional ByVal AttachmentPath As String = "", Optional ByVal SenderEmail As String = "", Optional ByVal SenderName As String = "") As Boolean

        Dim _Handler As New EmailHandler
        With _Handler
            .Recipient = Recipient
            .Subject = Subject
            .Body = Body
            .AttachmentPath = AttachmentPath
            .SenderEmail = SenderEmail
            .SenderName = SenderName
            .Send()
        End With

        _Handler = Nothing

        Return True

    End Function

    Public Sub Send()

        Dim _SMTPSenderEmail As String = SenderEmail
        Dim _SMTPSenderName As String = SenderName
        Dim _SMTPServer As String = "smtp.mandrillapp.com"
        Dim _SMTPPort As Integer = 587
        Dim _SMTPSSL As Boolean = True

        'default the sender details if not supplied
        If _SMTPSenderEmail = "" Then _SMTPSenderEmail = ""
        If _SMTPSenderName = "" Then _SMTPSenderName = ""

        Dim _SMTPAuthentication As Boolean = True
        Dim _SMTPUser As String = "support@caresoftware.co.uk"
        Dim _SMTPPassword As String = "Rd35yx2S5CwbbNkwLiu-Bw"

        Dim _SenderAddress As MailAddress = SetEmailAddress(_SMTPSenderEmail, _SMTPSenderName)
        If _SenderAddress IsNot Nothing Then

            Dim _RecipientAddress As MailAddress = SetEmailAddress(Recipient)
            If _RecipientAddress IsNot Nothing Then

                Dim _msg As New MailMessage(_SenderAddress, _RecipientAddress)
                With _msg
                    .Subject = Subject
                    .Body = Body
                End With

                If AttachmentPath <> "" Then
                    If IO.File.Exists(AttachmentPath) Then
                        _msg.Attachments.Add(New Attachment(AttachmentPath))
                    End If
                End If

                Dim _client As New SmtpClient
                _client.Host = _SMTPServer
                _client.Port = _SMTPPort
                _client.EnableSsl = _SMTPSSL

                If _SMTPAuthentication Then

                    Dim _credentials As New Net.NetworkCredential
                    _credentials.UserName = _SMTPUser
                    _credentials.Password = _SMTPPassword

                    _client.UseDefaultCredentials = False
                    _client.Credentials = _credentials

                End If

                Try
                    _client.Send(_msg)
                    m_SendStatus = 0
                    m_SendErrors = ""

                Catch ex As SmtpException
                    m_SendStatus = -4
                    m_SendErrors = ex.Message
                End Try

                _msg.Dispose()
                _msg = Nothing

                _client = Nothing

            Else
                'receipient address problem
                m_SendStatus = -3
                m_SendErrors = "Invalid Recipient Address"
            End If

        Else
            'sender address problem
            m_SendStatus = -2
            m_SendErrors = "Invalid Sender Address"
        End If

    End Sub

    Private Function SetEmailAddress(ByVal EmailAddress As String, Optional ByVal DisplayName As String = "") As MailAddress

        Dim _Address As MailAddress = Nothing

        Try
            If DisplayName = "" Then
                _Address = New MailAddress(EmailAddress)
            Else
                _Address = New MailAddress(EmailAddress, DisplayName)
            End If

        Catch ex As Exception
            _Address = Nothing
        End Try

        Return _Address

    End Function


End Class
