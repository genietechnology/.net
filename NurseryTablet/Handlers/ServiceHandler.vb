﻿Imports System.Security.Cryptography.X509Certificates
Imports System.ServiceModel

Public Class ServiceHandler

    Public Shared Function PingStandalone() As Boolean

        Dim _Version As String = ""

        Try

            Dim _ServiceClient As NurseryGenieData.NurseryGenieDataClient = ReturnService()
            _Version = _ServiceClient.Ping(Parameters.APIKey, Parameters.APIPassword, Parameters.Computer)
            _ServiceClient.Close()

            If _Version = "" Then
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Shared Function ReturnService() As NurseryGenieData.NurseryGenieDataClient

        Dim _Service As New NurseryGenieData.NurseryGenieDataClient
        _Service.ClientCredentials.ClientCertificate.Certificate = New X509Certificate2(My.Settings.CertPath, My.Settings.CertPassword)

        Return _Service

    End Function

    Public Shared Function Ping(ByRef ServiceClient As NurseryGenieData.NurseryGenieDataClient) As Integer

        Dim _Version As String = ""

        Try

            _Version = ServiceClient.Ping(Parameters.APIKey, Parameters.APIPassword, Parameters.Computer)

            If _Version = "" Then
                Return -1
            Else
                Return 0
            End If

        Catch ex As Exception

            If ex.InnerException IsNot Nothing Then

                If TypeOf (ex.InnerException) Is System.Net.Sockets.SocketException Then

                    Dim _SE As System.Net.Sockets.SocketException = CType(ex.InnerException, System.Net.Sockets.SocketException)
                    Select Case _SE.ErrorCode

                        Case 10061
                            'SERVICE COULD NOT BE CONTACTED
                            Return -2

                        Case Else
                            Return -3

                    End Select

                Else
                    'NOT A SOCKET EXCEPTION
                    Return -4
                End If

            Else
                'NO INNER EXCEPTION
                Return -99
            End If

        End Try

    End Function

    Public Shared Sub ViewReport(ByVal ChildID As String)

        Cursor.Current = Cursors.WaitCursor
        Application.DoEvents()

        Dim _Service = New NurseryGenieService.NurseryGenieLocalClient
        Dim _File As NurseryGenieService.GeneratedFile = Nothing

        Try
            _File = _Service.GetReportPDF(Parameters.APIKey, Parameters.APIPassword, Parameters.Computer, ChildID)
            _Service.Close()

        Catch tex As TimeoutException
            Cursor.Current = Cursors.Default
            Application.DoEvents()
            If Parameters.DebugMode Then
                Msgbox(tex.Message, MessageBoxIcon.Error, "TimeOut Exception")
            Else
                Msgbox("Unable to download Report File.", MessageBoxIcon.Error, "Download Report")
            End If
            _Service.Abort()

        Catch cex As CommunicationException
            Cursor.Current = Cursors.Default
            Application.DoEvents()
            If Parameters.DebugMode Then
                Msgbox(cex.Message, MessageBoxIcon.Error, "Communication Exception")
            Else
                Msgbox("Unable to download Report File.", MessageBoxIcon.Error, "Download Report")
            End If
            _Service.Abort()
        End Try

        If _File IsNot Nothing Then

            Dim _PDF As String = SharedModule.TempFolderPath + "\" + _File.FileName
            IO.File.WriteAllBytes(_PDF, _File.FileData)

            If IO.File.Exists(_PDF) Then
                Dim _frm As New frmPDFViewer(_PDF)
                _frm.Show()
            End If

        Else
            Msgbox("No report was generated.", MessageBoxIcon.Error, "Download Report")
        End If

    End Sub

    Public Shared Sub SendReport(ByVal ChildID As String)

        Dim _Service = New NurseryGenieService.NurseryGenieLocalClient
        Dim _Result As Integer = 0

        Try
            _Result = _Service.EmailReports(Parameters.APIKey, Parameters.APIPassword, Parameters.Computer, ChildID)
            _Service.Close()

            If _Result < 0 Then
                Msgbox("Service Error: " + _Result.ToString + " - Unable to Email Report.", MessageBoxIcon.Error, "Email Report")
            End If

            If _Result = 2 Then
                Msgbox("Report not sent: " + _Result.ToString + " - No contacts with email addresses who want reports.", MessageBoxIcon.Error, "Email Report")
            End If

        Catch tex As TimeoutException
            Msgbox("Timeout Error - Unable to Email Report.", MessageBoxIcon.Error, "Email Report")
            _Service.Abort()

        Catch cex As CommunicationException
            Msgbox("Communucation Error - Unable to Email Report.", MessageBoxIcon.Error, "Email Report")
            _Service.Abort()
        End Try

    End Sub

End Class
