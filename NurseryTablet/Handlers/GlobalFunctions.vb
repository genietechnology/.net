﻿Module GlobalFunctions

    Public Function Msgbox(ByVal MessageText As String, ByVal Icon As MessageBoxIcon, ByVal Title As String) As DialogResult
        Return DevExpress.XtraEditors.XtraMessageBox.Show(MessageText, Title, MessageBoxButtons.OK, Icon)
    End Function

    Public Function Msgbox(ByVal MessageText As String, ByVal Buttons As MessageBoxButtons, ByVal Icon As MessageBoxIcon, ByVal Title As String) As DialogResult
        Return DevExpress.XtraEditors.XtraMessageBox.Show(MessageText, Title, Buttons, Icon)
    End Function

    Public Function Msgbox(ByVal MessageText As String) As DialogResult
        Return DevExpress.XtraEditors.XtraMessageBox.Show(MessageText, "Nursery Genie", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Function

    Public Sub LoadHostedPanel(ByVal FormCaption As String, ByVal PanelIn As BaseHostedPanel, ByVal ChildIDFilter As String)
        Dim _frmHost As New frmHostingForm(PanelIn, ChildIDFilter)
        _frmHost.Text = FormCaption
        _frmHost.ShowDialog()
    End Sub

End Module
