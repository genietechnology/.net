﻿Imports System.Windows.Forms

Public Class ErrorHandler

    Public Shared Sub Activate()

        Try
            If Debugger.IsAttached Then Exit Sub

            'WILL BE USED FOR GLOBAL ERROR TRAPPING AT A LATER DATE

            'AddHandler Application.ThreadException, AddressOf ThreadException
            'AddHandler System.AppDomain.CurrentDomain.UnhandledException, AddressOf UnhandledExeception

        Catch ex As Exception

        End Try

    End Sub

    Public Shared Sub DeActivate()

    End Sub

    Private Shared Sub UnhandledExeception(sender As Object, e As UnhandledExceptionEventArgs)
        Try
            ManageException(CType(e.ExceptionObject, Exception))
        Catch ex As Exception

        End Try
    End Sub

    Private Shared Sub ThreadException(sender As Object, e As Threading.ThreadExceptionEventArgs)
        Try
            ManageException(e.Exception)
        Catch ex As Exception

        End Try
    End Sub

    Private Shared Sub ManageException(ByRef Exception As System.Exception)

        Msgbox(Exception.Message)
        Windows.Forms.Application.Exit()

    End Sub

    Public Shared Function ReturnBaseHierarchy(ByVal Base As System.Type) As String
        Dim _BaseChain As String = GetBase(Base, "")
        Return _BaseChain
    End Function

    Private Shared Function GetBase(ByVal Base As System.Type, ByRef BaseChain As String) As String

        If Not Base.BaseType Is Nothing Then
            If Base.FullName <> "System.Windows.Forms.Form" Then
                BaseChain += ">> " & Base.FullName & vbCrLf
                GetBase(Base.BaseType, BaseChain)
            End If
        End If

        Return BaseChain

    End Function

End Class
