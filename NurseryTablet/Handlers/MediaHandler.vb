﻿Imports NurseryTablet.Business

Public Class MediaHandler

    Public Enum EnumCaptureCategory
        Observation
        Accident
        Media
    End Enum

    Public Enum EnumCaptureMethod
        Video
        Photo
        Audio
    End Enum

    Public Shared Function SaveMedia(ByVal Media As List(Of MediaContent)) As Boolean
        For Each _m In Media
            SaveMedia(_m)
        Next
    End Function

    Public Shared Function SaveMedia(ByVal Media As MediaContent) As Guid?

        Dim _ID As Guid = Guid.NewGuid()

        Dim _M As New NurseryGenieData.Media
        With _M
            .ID = _ID
            .DayId = New Guid(Day.TodayIDString)
            .KeyType = Media.KeyType
            .KeyId = New Guid(Media.KeyID)
            .Data = Media.Data
            .DeviceName = Media.DeviceName
            .FilePath = Media.FilePath
            .FileSize = Media.FileSize
            .FileExt = Media.FileExt
            .Stamp = Now
        End With

        If Not Business.Media.CreateMedia(_M) Then
            _ID = Nothing
        End If

        Return _ID

    End Function

    Public Shared Function ReturnByteArray(ByVal FilePath As String) As Byte()

        Dim _bytes As Byte() = Nothing

        If FilePath <> "" Then
            If IO.File.Exists(FilePath) Then
                _bytes = IO.File.ReadAllBytes(FilePath)
            End If
        End If

        Return _bytes

    End Function

    Public Shared Function ReturnByteArray(ByRef ImageObject As Drawing.Image) As Byte()

        Dim _bytes As Byte() = Nothing

        If ImageObject IsNot Nothing Then

            Try
                Dim _ms As IO.MemoryStream = New IO.MemoryStream()
                ImageObject.Save(_ms, System.Drawing.Imaging.ImageFormat.Png)
                _bytes = _ms.GetBuffer

                _ms.Close()
                _ms.Dispose()

            Catch ex As Exception

            End Try

        End If

        Return _bytes

    End Function

    Public Shared Function ReturnImage(ByVal PhotoPath As String) As Image

        If PhotoPath = "" Then Return Nothing

        'we put the file in a byte array so the file is not in use (so we can delete it etc)
        Dim _bytes As Byte() = ReturnByteArray(PhotoPath)
        Dim _image As Image = ReturnImage(_bytes)

        _bytes = Nothing

        Return _image

    End Function

    Public Shared Function ReturnImage(ByVal ByteArrayIn As Object) As Image

        If ByteArrayIn Is Nothing Then Return Nothing
        If TypeOf ByteArrayIn Is Byte() Then

            Try

                Dim _Bytes As Byte() = CType(ByteArrayIn, Byte())
                Dim _ms As New IO.MemoryStream(_Bytes)
                Dim _Image As Image = Image.FromStream(_ms)

                _Bytes = Nothing
                _ms.Close()
                _ms.Dispose()

                Return _Image

            Catch ex As Exception
                Return Nothing
            End Try

        Else
            Return Nothing
        End If

    End Function

    Public Shared Function CaptureMultiple() As List(Of FileDetail)

        Dim _Return As List(Of FileDetail) = Nothing

        If Parameters.Capture.CaptureEnabled Then

            If Parameters.Capture.CameraCommand <> "" Then

                Dim _frmCapture As New frmMediaCapture(True)
                _frmCapture.ShowDialog()

                _Return = _frmCapture.SelectedFiles

                _frmCapture.Dispose()
                _frmCapture = Nothing

            Else
                Msgbox("Camera Command not set in configuration file. Aborted.", MessageBoxIcon.Exclamation, "Media Capture")
            End If

        Else
            Msgbox("Media Capture has been disabled on this device.", MessageBoxIcon.Exclamation, "Media Capture")
        End If

        Return _Return

    End Function

    Public Shared Function CaptureNow() As String

        Dim _Return As String = ""
        Dim _Files As List(Of FileDetail) = CaptureMultiple()
        If _Files IsNot Nothing Then

            Select Case _Files.Count

                Case 0
                    'nothing selected

                Case 1
                    _Return = _Files(0).FullPath

                Case Else
                    Msgbox("Multiple pictures detected. Please select just one picture.", MessageBoxIcon.Exclamation, "Multiple Pictures")

            End Select

        End If

        Return _Return

    End Function

    Public Shared Sub ClearCameraRoll(ByVal PromptUser As Boolean)

        If PromptUser Then
            If Msgbox("Clear the Camera Roll?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, "Clear Camera Roll") = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        For Each _f As String In IO.Directory.GetFiles(SharedModule.CameraRollPath)
            FileSystem.Kill(_f)
        Next _f

    End Sub

End Class
