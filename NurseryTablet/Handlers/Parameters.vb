﻿
Public Class Parameters

    'LEAVE THESE AS VARIABLES, SO WE KNOW THEY COME FROM THE APP.CONFIG
    '*************************************************************************
    Public Shared ServiceIP As String = ""
    Public Shared ServicePort As String = ""
    '*************************************************************************

    Public Shared ReadOnly Property Computer As String
        Get
            Return My.Computer.Name
        End Get
    End Property

    Public Shared Property APIKey As String = ""
    Public Shared Property APIPassword As String = ""

    '*************************************************************************

    Public Shared Property DefaultRoom As String = ""
    Public Shared Property DisableLocation As Boolean = False
    Public Shared Property SiteID As String = ""
    Public Shared Property SiteIDQuotes As String = ""

    '*************************************************************************
    Public Shared Property CurrentStaffID As String = ""
    Public Shared Property CurrentStaffName As String = ""
    '*************************************************************************

    Public Shared Property DebugMode As Boolean = False
    Public Shared Property SpellChecking As Boolean = False
    Public Shared Property UseBasicMeals As Boolean = False
    Public Shared Property CrossCheckMandatory As Boolean = False

    Public Shared Property DisableCheckIns As Boolean = False
    Public Shared Property DisablePotty As Boolean = False

    Public Shared Property MaximiseForms As Boolean = False

    '*************************************************************************
    Public Shared ReadOnly Property TodayID As String
        Get
            Return SharedModule.TodayID
        End Get
    End Property
    Public Shared Property TodaySQLDate As String = Format(Date.Today, "yyyy-MM-dd")
    Public Shared Property TodaySQLStart As String = TodaySQLDate & " 00:00:00.000"
    Public Shared Property TodaySQLEnd As String = TodaySQLDate & " 23:59:59.000"
    '*************************************************************************

    Public Shared Property UnlockMode As SharedModule.EnumUnlockMode = SharedModule.EnumUnlockMode.Code
    Public Shared Property UnlockCode As String = "1234"

    Public Shared Property SortBySurname As Boolean = False

    Public Class Paths
        Public Shared Property SharedFolderPath As String = ""
    End Class

    Public Class Capture
        Public Shared Property CaptureEnabled As Boolean = False
        Public Shared Property CameraCommand As String = ""
        Public Shared Property CameraDevice As String = ""
    End Class

End Class
