﻿Option Strict On

Imports System.IO
Imports DevExpress.Data.Helpers
Imports NurseryTablet.NurseryGenieData
Imports Day = NurseryTablet.Business.Day
Imports Activity = NurseryTablet.Business.Activity

Public Class SharedModule

#Region "Enums"

    Public Enum EnumMealType
        Breakfast
        Snack
        Lunch
        LunchDessert
        PMSnack
        Tea
        TeaDessert
        BabyFood
    End Enum

    Public Enum EnumUsage
        StandardTouch
        StandardTablet
        ClubMode
        Foyer
        StandardFromFoyerMode
    End Enum

    Public Enum EnumActivityType
        Toilet
        Milk
        Sleep
        Request
        Suncream
        Feedback
        CrossCheck
        CheckOutMedication
        CheckOutIncident
        Absence
        Asleep
        SleepCheck
        Payment
        Unlock
        LogOff
        Behavior
    End Enum

    Public Enum EnumSMSActivityType
        Toilet
        Milk
        Sleep
        Food
    End Enum

    Public Enum EnumUnlockMode
        Code
        PIN
        Disabled
    End Enum

    Public Enum EnumCheckMode
        SinglePerson
        MultiPerson
        SignPerson
    End Enum

#End Region

    Public Const g_BabyFoodMealID As String = "3BF50230-A1CA-4652-B878-C4770F9A5BF0"

#Region "Properties"

    Public Shared Property UsageMode As EnumUsage
    Public Shared Property CameraRollPath As String = My.Computer.FileSystem.SpecialDirectories.MyPictures + "\" + "Camera Roll"
    Public Shared Property FlashCardPath As String
    Public Shared Property TempFolderPath As String = Environment.CurrentDirectory + "\temp"

    Public Shared ReadOnly Property TodayID As String
        Get
            Return Day.TodayIDString
        End Get
    End Property

    Public Shared ReadOnly Property Version() As String
        Get
            Return Application.ProductVersion
        End Get
    End Property

    Public Shared Property ColourRed As Drawing.Color = Color.LightCoral
    Public Shared Property ColourYellow As Drawing.Color = Color.Yellow
    Public Shared Property ColourGreen As Drawing.Color = Color.LightGreen

#End Region

#Region "Methods"

    Public Shared Function UnlockScreen() As Boolean

        Dim _Return As Boolean = False
        Dim _Entered As String = ""

        Select Case Parameters.UnlockMode

            Case EnumUnlockMode.Disabled
                _Return = True

            Case EnumUnlockMode.Code
                _Entered = NumberEntry("Please enter the Unlock Code", "", False, False)
                If _Entered = Parameters.UnlockCode Then
                    _Return = True
                End If

            Case EnumUnlockMode.PIN
                _Entered = NumberEntry("Please enter your Staff PIN code", "", False, False)

        End Select

        Return _Return

    End Function

    Public Shared Sub ZoomText(ByVal TextIn As String)

        If TextIn.Trim = "" Then Exit Sub

        Dim frmText As New frmTextView(TextIn)
        frmText.ShowDialog()

        frmText.Dispose()
        frmText = Nothing

    End Sub

    Public Shared Sub DisplayGridData(ByVal Caption As String, ByVal DataIn As IEnumerable)

        If DataIn Is Nothing Then Exit Sub

        Dim frmGrid As New frmGrid(DataIn)
        frmGrid.Text = Caption
        frmGrid.ShowDialog()

        frmGrid.Dispose()
        frmGrid = Nothing

    End Sub

    Public Shared Function DayExists() As Boolean

        Dim _Return As Boolean = False
        Dim _ID As String = ""

        'If Parameters.UseDirectSQL Then
        '    _ID = ReturnDayIDFromDB()
        'Else
        '    If Parameters.UseWebServices Then
        '        Dim _GUID As Guid? = ServiceHandler.ReturnDayID
        '        If _GUID.HasValue Then
        '            _ID = _GUID.Value.ToString
        '        Else
        '            _ID = ""
        '        End If
        '    Else
        '        _ID = ReturnDayIDFromDB()
        '    End If
        'End If

        'If _ID <> "" Then
        '    _Return = True
        '    Parameters.TodayID = _ID
        'Else
        '    Parameters.TodayID = ""
        '    Msgbox("Today's Menu has not been created.", MessageBoxIcon.Exclamation, "Menu Missing")
        'End If

        Return _Return

    End Function

    Public Shared Function OSK(Optional ByVal FormCaption As String = "On Screen Keyboard", Optional ByVal AllowReturn As Boolean = False, _
                               Optional ByVal AllowPunctuation As Boolean = False, Optional ByVal DefaultValue As String = "", _
                               Optional ByVal ApplicationListName As String = "", Optional MaxLength As Integer = 0) As String

        Dim _Return As String = ""
        Dim _OSK As New frmOSK
        With _OSK
            .ApplicationListName = ApplicationListName
            .AllowPunc = AllowPunctuation
            .AllowReturn = AllowReturn
            .DefaultValue = DefaultValue
            .MaxLength = MaxLength
            .Text = FormCaption
            .ShowDialog()
        End With

        _Return = _OSK.ReturnValue

        _OSK.Dispose()
        _OSK = Nothing

        Return _Return

    End Function

    Public Shared Function NumberEntry(Optional ByVal FormCaption As String = "Number Entry", _
                                       Optional ByVal DefaultValue As String = "", _
                                       Optional ByVal AllowDecimals As Boolean = False, _
                                       Optional ByVal ShowCharacters As Boolean = True) As String

        Dim _Return As String = ""
        Dim _Keypad As New frmNumbersOnly
        With _Keypad
            .DefaultValue = DefaultValue
            .AllowDecimals = AllowDecimals
            .ShowCharacters = ShowCharacters
            .Text = FormCaption
            .ShowDialog()
        End With

        _Return = _Keypad.ReturnValue

        _Keypad.Dispose()
        _Keypad = Nothing

        Return _Return

    End Function

    Public Shared Function TimeEntry(Optional ByVal FormCaption As String = "TimeEntry", Optional ByVal NA As Boolean = False) As String

        Dim _Return As String = ""
        Dim _TE As New frmTimeEntry
        With _TE
            .Text = FormCaption
            .NA = NA
            .ShowDialog()
        End With

        _Return = _TE.ReturnValue

        _TE.Dispose()
        _TE = Nothing

        Return _Return

    End Function

    Public Shared Function DateEntry(Optional ByVal FormCaption As String = "Please select a Date") As String

        Dim _Return As String = ""
        Dim _DE As New frmDateEntry
        With _DE
            .Text = FormCaption
            .ShowDialog()
        End With

        _Return = _DE.ReturnValue

        _DE.Dispose()
        _DE = Nothing

        Return _Return

    End Function

    Public Shared Sub Message(ByVal MessageText As String, Optional ByVal Warning As Boolean = False)
        If MessageText = "" Then Exit Sub
        If Warning Then
            Msgbox(MessageText, MessageBoxIcon.Exclamation, "")
        Else
            Msgbox(MessageText, MessageBoxIcon.Information, "")
        End If
    End Sub

    <Obsolete("No longer supported", False)>
    Public Shared Function ReturnButtonSQL(ByVal SQLQuery As String, ByVal FormCaption As String) As Pair

        Dim _Buttons As New List(Of Pair)

        'If SQLQuery IsNot Nothing Then

        '    If SQLQuery.Contains("|") Then

        '        'split string by pipe
        '        Dim _Items As String() = Split(SQLQuery, "|")
        '        If _Items IsNot Nothing Then
        '            For Each _Item As String In _Items
        '                Dim _Values As String() = Split(_Item, "=")
        '                If _Values IsNot Nothing Then
        '                    If _Values.Count = 1 Then
        '                        _Buttons.Add(New Pair(_Values(0), ""))
        '                    Else
        '                        _Buttons.Add(New Pair(_Values(0), _Values(1)))
        '                    End If
        '                End If
        '            Next
        '        End If

        '    Else

        '        Dim _DT As DataTable = DAL.ReturnDataTable(SQLQuery)
        '        If _DT IsNot Nothing Then
        '            For Each _DR As DataRow In _DT.Rows
        '                If _DR.ItemArray.Count = 1 Then
        '                    _Buttons.Add(New Pair(_DR.Item(0).ToString, ""))
        '                Else
        '                    _Buttons.Add(New Pair(_DR.Item(0).ToString, _DR.Item(1).ToString))
        '                End If
        '            Next
        '        End If

        '    End If

        'If _Buttons.Count > 0 Then
        '    Return SetupButtonForm(_Buttons, FormCaption)
        'End If

        Return Nothing

    End Function

    Public Shared Function ReturnButtons(ByVal Buttons As List(Of Pair), ByVal FormCaption As String) As Pair

        Dim _Return As Pair = Nothing

        Dim _frm As New frmButtonsTiled
        _frm.Text = FormCaption
        _frm.PopulateMode = frmButtonsTiled.EnumPopulateMode.Custom
        _frm.ButtonCollection = Buttons
        _frm.MultiSelect = False
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            _Return = _frm.SelectedPair
        End If

        _frm.Dispose()
        _frm = Nothing

        Return _Return

    End Function

    Public Shared Function ReturnMultipleButtons(ByVal Buttons As List(Of Pair), ByVal FormCaption As String) As List(Of Pair)

        Dim _Return As List(Of Pair) = Nothing

        Dim _frm As New frmButtonsTiled
        _frm.Text = FormCaption
        _frm.PopulateMode = frmButtonsTiled.EnumPopulateMode.Custom
        _frm.ButtonCollection = Buttons
        _frm.MultiSelect = True
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            _Return = _frm.SelectedPairs
        End If

        _frm.Dispose()
        _frm = Nothing

        Return _Return

    End Function

    Public Shared Sub ChildToilet(Optional ByVal ChildID As String = "")
        Dim _Toilet As New HostedToilet
        LoadHostedPanel("Toileting", _Toilet, ChildID)
    End Sub

    Public Shared Sub ChildMilk(Optional ByVal ChildID As String = "")
        Dim _Milk As New HostedMilk
        LoadHostedPanel("Milk Recording", _Milk, ChildID)
    End Sub

    Public Shared Sub ChildFood(Optional ByVal ChildID As String = "")
        If ChildID <> "" OrElse Parameters.UseBasicMeals = False Then
            Dim _Food As New HostedFood
            LoadHostedPanel("Food Recording", _Food, ChildID)
        Else
            Dim _frm As New frmFastFood
            _frm.ShowDialog()
        End If
    End Sub

    Public Shared Sub ChildSleep(Optional ByVal ChildID As String = "")
        Dim _Sleep As New HostedSleep
        LoadHostedPanel("Sleep Recording", _Sleep, ChildID)
    End Sub

    Public Shared Sub ChildRequests(Optional ByVal ChildID As String = "")
        'Dim _frm As New frmRequests
        '_frm.Show()
        'If ChildID <> "" Then _frm.DrillDown(ChildID)
        '_frm.BringToFront()
    End Sub

    Public Shared Sub ChildMedicationLog(ByVal ChildID As String)

    End Sub

    Public Shared Sub ChildIncidents(Optional ByVal ChildID As String = "")

        Dim _ChildID As String = ChildID
        If _ChildID = "" Then
            Dim _Return As Pair = Business.Child.FindChildByGroup(Enums.PersonMode.OnlyCheckedIn, True)
            If Not _Return Is Nothing AndAlso _Return.Code <> "" Then
                _ChildID = _Return.Code
            End If
        End If

        If _ChildID = "" Then Exit Sub

        Dim _Incidents As List(Of NurseryGenieData.Incident) = Business.Incident.GetIncidents(TodayID, _ChildID)

        Dim _frmAR As New frmAddRemove(_Incidents, "New Incident", "Remove Incident", "View Incidents")
        _frmAR.ShowDialog()

        If _frmAR.DialogResult = DialogResult.OK Then

            Select Case _frmAR.ReturnMode

                Case Enums.AddRemoveReturnMode.AddRecord, Enums.AddRemoveReturnMode.EditRecord

                    Dim _frmInc As New frmIncidentLog()
                    _frmInc.DrillDown(_ChildID, _frmAR.ReturnID)
                    _frmInc.ShowDialog()

                    _frmInc.Dispose()
                    _frmInc = Nothing

                Case Enums.AddRemoveReturnMode.RemoveRecord
                    Business.Incident.DeleteIncident(_frmAR.ReturnID)

            End Select

        End If

        _frmAR.Dispose()
        _frmAR = Nothing

    End Sub

    Public Shared Sub ChildBehavior(Optional ByVal ChildID As String = "")
        'Dim _Behavior As New HostedBehavior
        'LoadHostedPanel(_Behavior, ChildID)
    End Sub

    Public Shared Sub ChildMedicalAuth(Optional ByVal ChildID As String = "")

        'Dim _ChildID As String = ChildID
        'If _ChildID = "" Then
        '    Dim _Return As Pair = Business.Child.FindChildByGroup(Enums.PersonMode.OnlyCheckedIn, True)
        '    If Not _Return Is Nothing AndAlso _Return.Code <> "" Then
        '        _ChildID = _Return.Code
        '    End If
        'End If

        'If _ChildID = "" Then Exit Sub

        'Dim _SQL As String = ""

        '_SQL += "select ID, illness as 'Illness', medicine as 'Medicine', dosage as 'Dosage', dosages_due as 'Due', staff_name as 'Staff'"
        '_SQL += " from MedicineAuth"
        '_SQL += " where day_id = '" + SharedModule.TodayID + "'"
        '_SQL += " and child_id = '" + _ChildID + "'"
        '_SQL += " order by last_given"

        'Dim _frmAR As New frmAddRemove(_SQL, "New Authorisation", "Remove Authorisation", "Medicine Authorisations")
        '_frmAR.ShowDialog()

        'If _frmAR.DialogResult = DialogResult.OK Then

        '    Select Case _frmAR.ReturnMode

        '        Case Enums.AddRemoveReturnMode.AddRecord, Enums.AddRemoveReturnMode.EditRecord

        '            Dim _frmAuth As New frmMedicalAuth
        '            _frmAuth.DrillDown(_ChildID, _frmAR.ReturnID)
        '            _frmAuth.ShowDialog()

        '            _frmAuth.Dispose()
        '            _frmAuth = Nothing

        '        Case Enums.AddRemoveReturnMode.RemoveRecord

        '            '_SQL = "delete from MedicineAuth where ID = '" + _frmAR.ReturnID + "'"
        '            'DAL.ExecuteCommand(_SQL)

        '            '_SQL = "delete from MedicineLog where auth_id = '" + _frmAR.ReturnID + "'"
        '            'DAL.ExecuteCommand(_SQL)

        '    End Select

        'End If

        '_frmAR.Dispose()
        '_frmAR = Nothing

    End Sub

    Public Shared Sub ChildSuncream(Optional ByVal ChildID As String = "")
        Msgbox("Our Suncream Logging is nearly ready :-)" + vbCrLf + vbCrLf + "We'll be in touch when the function is available.")
    End Sub

    Public Shared Function MedicationDueQuery(Optional ByVal ForceNurseryWideCheck As Boolean = False) As String

        Dim _SQL As String = ""
        Dim _RoomFilter As String = ""

        _SQL += "select m.ID, m.button, m.due, m.medicine"
        _SQL += " from MedicineLog m"
        _SQL += " left join Day d on d.ID = m.day_id"
        _SQL += " left join Children c on c.ID = m.child_id"
        _SQL += " where m.day_id = '" + Day.TodayIDString + "'"
        _SQL += " and m.staff_id IS NULL"

        If _RoomFilter <> "" Then _SQL += _RoomFilter

        _SQL += " and"
        _SQL += " (select count (*) from Register r where r.date = d.date and r.person_id = m.child_id and r.in_out = 'I') <>"
        _SQL += " (select count (*) from Register r where r.date = d.date and r.person_id = m.child_id and r.in_out = 'O')"
        _SQL += " order by m.due"

        Return _SQL

    End Function

    Public Shared Function LogMedicine(ByVal Mode As frmMedicalLog.EnumMode, ByVal ID As String) As Boolean

        'ViewLog = 0 'viewing from child record or list
        'EditLog = 1 'editting a log that has been generated by creating a request
        'CreateLog = 2 'create ad-hoc medicine i.e. giving calpol for temp

        Dim _Return As Boolean = False
        Dim _frm As New frmMedicalLog(Mode, ID)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            _Return = True
        End If

        _frm.Dispose()
        _frm = Nothing

        Return _Return

    End Function

#End Region

#Region "Transactions"

    Public Shared Sub LogActivity(ByVal DayID As String, ByVal KeyID As String, ByVal KeyName As String, _
                              ByVal ActivityType As SharedModule.EnumActivityType, ByVal Description As String, _
                              Optional ByVal Value1 As String = "", Optional ByVal Value2 As String = "", Optional ByVal Value3 As String = "", _
                              Optional ByVal StaffID As String = "", Optional ByVal StaffName As String = "", Optional ByVal Notes As String = "")

        Dim _A As New NurseryGenieData.Activity
        With _A

            .ID = Guid.NewGuid
            .DayID = New Guid(DayID)

            .KeyID = New Guid(KeyID)
            .KeyName = KeyName
            .KeyType = Business.Activity.ReturnActivityTypeCode(ActivityType)

            .Description = Description

            .Value1 = Value1
            .Value2 = Value2
            .Value3 = Value3

            .Stamp = Now

            If StaffID <> "" Then .StaffID = New Guid(StaffID)
            .StaffName = StaffName

            .Notes = Notes

        End With

        DataBase.Tables.Activity.Add(_A)
        DataBase.PersistActivityLocally()

    End Sub

    Public Shared Function PersonInorOut(ByVal PersonID As String) As Enums.InOut

        Dim _Status As Enums.AttendanceStatus = PersonStatus(PersonID)
        If _Status = Enums.AttendanceStatus.CheckedIn Then
            Return Enums.InOut.CheckIn
        Else
            Return Enums.InOut.CheckOut
        End If

    End Function

    Public Shared Function PersonStatus(ByVal PersonID As String) As Enums.AttendanceStatus

        Dim _Ins As Integer = 0
        Dim _Outs As Integer = 0

        Dim _Q = From _F As NurseryGenieData.Register In DataBase.Tables.Register
                 Where _F.RegisterDate = Today _
                 And _F.PersonID = New Guid(PersonID)

        If _Q IsNot Nothing Then

            For Each _R In _Q
                If _R.InOut = "I" Then _Ins += 1
                If _R.InOut = "O" Then _Outs += 1
            Next

        End If

        If _Ins > 0 Then
            If _Ins > _Outs Then
                Return Enums.AttendanceStatus.CheckedIn
            Else
                Return Enums.AttendanceStatus.CheckedOut
            End If
        Else
            If Business.Child.IsAbsent(PersonID) Then
                Return Enums.AttendanceStatus.Absent
            Else
                Return Enums.AttendanceStatus.NotArrived
            End If
        End If

    End Function

    ''' <summary>
    ''' Person location will only return a location if a child is checked in
    ''' </summary>
    ''' <param name="PersonID"></param>
    ''' <returns></returns>
    Public Shared Function PersonLocation(ByVal PersonID As String) As String

        Dim _Q = From _F As NurseryGenieData.Register In DataBase.Tables.Register
                 Where _F.RegisterDate = Today _
                 And _F.PersonID = New Guid(PersonID) _
                 And _F.InOut = "I"
                 Order By _F.StampIn Descending

        If _Q IsNot Nothing Then
                For Each _R In _Q
                    Return _R.Location
                Next
            End If

        Return ""

    End Function

    Public Shared Sub LogFood(ByVal DayID As String, ByVal ChildID As String, ByVal ChildName As String,
                              ByVal MealID As String, ByVal MealType As String, ByVal MealName As String,
                              ByVal FoodID As String, ByVal FoodName As String, ByVal FoodStatus As String)

        Dim _IsNew As Boolean = True
        Dim _FoodRegister As New NurseryGenieData.FoodRegister

        Dim _Q = From _F As NurseryGenieData.FoodRegister In DataBase.Tables.FoodRegister
                 Where _F.DayID = New Guid(DayID) _
                 And _F.ChildID = New Guid(ChildID) _
                 And _F.MealType = MealType _
                 And _F.MealID = New Guid(MealID) _
                 And _F.FoodID = New Guid(FoodID)

        If _Q IsNot Nothing Then
            If _Q.Count = 1 Then
                _IsNew = False
                _FoodRegister = _Q.First
            Else
                _FoodRegister.ID = Guid.NewGuid
            End If
        End If

        With _FoodRegister
            .DayID = New Guid(DayID)
            .ChildID = New Guid(ChildID)
            .ChildName = ChildName
            .MealID = New Guid(MealID)
            .MealType = MealType
            .MealName = MealName
            .FoodID = New Guid(FoodID)
            .FoodName = FoodName
            .FoodStatus = FoodStatus
        End With

        If _IsNew Then
            DataBase.Tables.FoodRegister.Add(_FoodRegister)
        Else
            DataBase.Tables.FoodRegister.Remove(_FoodRegister)
            DataBase.Tables.FoodRegister.Add(_FoodRegister)
        End If

        DataBase.PersistFoodRegisterLocally()

    End Sub

    Public Shared Sub DeleteSleep(ByVal DayID As String, ByVal ChildID As String)
        DeleteActivity(DayID, ChildID, SharedModule.EnumActivityType.Sleep)
    End Sub

    Public Shared Sub DeleteMilk(ByVal DayID As String, ByVal ChildID As String)
        DeleteActivity(DayID, ChildID, SharedModule.EnumActivityType.Milk)
    End Sub

    Public Shared Sub DeleteRequests(ByVal DayID As String, ByVal ChildID As String)
        DeleteActivity(DayID, ChildID, SharedModule.EnumActivityType.Request)
    End Sub

    Public Shared Sub DeleteActivity(ByVal DayID As String, ByVal ChildID As String, ByVal ActivityType As SharedModule.EnumActivityType)
        For Each _A As NurseryGenieData.Activity In DataBase.Tables.Activity
            If _A.DayID = New Guid(DayID) AndAlso _A.KeyID = New Guid(ChildID) AndAlso _A.KeyType = Business.Activity.ReturnActivityTypeCode(ActivityType) Then
                DataBase.Tables.Activity.Remove(_A)
            End If
        Next
    End Sub

    Public Shared Sub DeleteActivity(ByVal ActivityID As String)

        Dim _Activity As NurseryGenieData.Activity = Nothing
        For Each _A As NurseryGenieData.Activity In DataBase.Tables.Activity
            If _A.ID = New Guid(ActivityID) Then
                _Activity = _A
                Exit For
            End If
        Next

        If _Activity IsNot Nothing Then
            DataBase.Tables.Activity.Remove(_Activity)
        End If

    End Sub

    Public Shared Sub LogSleep(ByVal DayID As String, ByVal ChildID As String, ByVal ChildName As String, ByVal Times As String, _
                               ByVal StaffID As String, ByVal StaffName As String)

        Dim _Start As String = Mid(Times, 1, 5)
        Dim _End As String = Mid(Times, Times.Length - 4)

        LogActivity(DayID, ChildID, ChildName, SharedModule.EnumActivityType.Sleep, "Sleep", Times, _Start, _End, StaffID, StaffName)

    End Sub

    Public Shared Sub LogMilk(ByVal DayID As String, ByVal ChildID As String, ByVal ChildName As String, ByVal floz As Double, Stamp As String, _
                              ByVal StaffID As String, ByVal StaffName As String)

        Dim _Desc As String = floz.ToString + " floz @ " + Stamp
        LogActivity(DayID, ChildID, ChildName, SharedModule.EnumActivityType.Milk, _Desc, floz.ToString + " floz", floz.ToString, Stamp, StaffID, StaffName)

    End Sub

    Public Shared Sub LogBehavior(ByVal DayID As String, ByVal ChildID As String, ByVal ChildName As String, Behavior As String, ByVal Location As String,
                                  ByVal StaffID As String, ByVal StaffName As String)

        Dim _Time As String = Format(Now, "HH:mm").ToString
        Dim _Desc As String = Behavior + " - " + Location + " @ " + _Time
        LogActivity(DayID, ChildID, ChildName, SharedModule.EnumActivityType.Behavior, _Desc, Behavior, Location, _Time, StaffID, StaffName)

    End Sub

    Public Shared Sub LogRequest(ByVal DayID As String, ByVal ChildID As String, ByVal ChildName As String, _
                                 ByVal RequestID As String, ByVal RequestDescription As String, _
                                 ByVal StaffID As String, ByVal StaffName As String)

        LogActivity(DayID, ChildID, ChildName, SharedModule.EnumActivityType.Request, RequestDescription, RequestID, , , StaffID, StaffName)

    End Sub

    Public Shared Sub LogFeedback(ByVal DayID As String, ByVal ChildID As String, ByVal ChildName As String, _
                                 ByVal Feedback As String)

        LogActivity(DayID, ChildID, ChildName, SharedModule.EnumActivityType.Feedback, "Personal Feedback", , , , , , Feedback)

    End Sub

    Public Shared Function ReturnFeedback(ByVal DayID As String, ByVal ChildID As String, ByRef Feedback As String) As Boolean

        Dim _Activity As NurseryGenieData.Activity = Nothing
        If ReturnActivity(DayID, ChildID, SharedModule.EnumActivityType.Feedback, _Activity) Then
            Feedback = _Activity.Notes
        Else
            Return False
        End If

    End Function

    Public Shared Function ReturnActivityCount(ByVal DayID As String, ByVal ChildID As String, ByVal ActivityType As SharedModule.EnumActivityType) As Integer

        Dim _Count As Integer = 0
        For Each _A As NurseryGenieData.Activity In DataBase.Tables.Activity
            If _A.DayID = New Guid(DayID) AndAlso _A.KeyID = New Guid(ChildID) AndAlso _A.KeyType = Business.Activity.ReturnActivityTypeCode(ActivityType) Then
                _Count += 1
            End If
        Next

        Return _Count

    End Function

    Public Shared Function ReturnFoodCount(ByVal DayID As String, ByVal ChildID As String, MealType As EnumMealType) As Integer

        Dim _Count As Integer = 0
        For Each _F As NurseryGenieData.FoodRegister In DataBase.Tables.FoodRegister
            If _F.DayID = New Guid(DayID) AndAlso _F.ChildID = New Guid(ChildID) AndAlso _F.MealType = Business.Food.ReturnMealCode(MealType) Then
                _Count += 1
            End If
        Next

        Return _Count

    End Function

    Public Shared Function ReturnIncidentCount(ByVal DayID As String, ByVal ChildID As String) As Integer

        Dim _Return As Integer = 0
        'Dim _SQL As String = "select count(*) from Incidents" & _
        '                     " where day_id = '" & DayID & "'" & _
        '                     " and child_id = '" & ChildID & "'"

        'Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
        'If Not _DR Is Nothing Then
        '    If _DR.Item(0).ToString <> "" Then
        '        _Return = CInt(_DR.Item(0))
        '    End If
        'End If

        Return _Return

    End Function

    Public Shared Function ReturnMedicineCount(ByVal DayID As String, ByVal ChildID As String) As Integer

        Dim _Return As Integer = 0
        'Dim _SQL As String = "select count(*) from MedicineLog" & _
        '                     " where day_id = '" & DayID & "'" & _
        '                     " and child_id = '" & ChildID & "'" & _
        '                     " and staff_name is not null"

        'Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
        'If Not _DR Is Nothing Then
        '    If _DR.Item(0).ToString <> "" Then
        '        _Return = CInt(_DR.Item(0))
        '    End If
        'End If

        Return _Return

    End Function

    Public Shared Function ReturnObservationCount(ByVal DayID As String, ByVal ChildID As String) As Integer
        Return 0
    End Function

    Public Shared Function HasActivity(ByVal DayID As String, ByVal ChildID As String, ByVal ActivityType As SharedModule.EnumActivityType) As Boolean
        If ReturnActivityCount(DayID, ChildID, ActivityType) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Shared Function ReturnActivity(ByVal DayID As String, ByVal ChildID As String, ByVal ActivityType As SharedModule.EnumActivityType, ByRef Activity As NurseryGenieData.Activity) As Boolean

        For Each _A As NurseryGenieData.Activity In DataBase.Tables.Activity
            If _A.DayID = New Guid(DayID) AndAlso _A.KeyID = New Guid(ChildID) AndAlso _A.KeyType = Business.Activity.ReturnActivityTypeCode(ActivityType) Then
                Activity = _A
                Return True
            End If
        Next

        Return False

    End Function

#End Region

    Public Shared Function CheckinsPermitted() As Boolean
        If Parameters.DisableCheckIns Then
            Msgbox("Register functionality has been disable on this device." + vbCrLf + "Please ensure you are using the correct Device.", MessageBoxIcon.Exclamation, "Register Functionality")
        Else
            Return True
        End If
    End Function

    Public Shared Function DoCheckOut(ByVal ChildID As String, ByVal ChildName As String) As Boolean

        If Not CheckinsPermitted() Then Return False

        Dim _Return As Boolean = False
        Dim _frm As New frmCrossCheck(ChildID, ChildName, frmCrossCheck.EnumMode.CheckOut)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then _Return = True

        _frm.Dispose()
        _frm = Nothing

        Return _Return

    End Function

    Public Shared Function LegacySignatureCapture(ByRef SignatureData As String) As Boolean

        Dim _Return As Boolean = False

        Dim _Signature As New frmSignature
        With _Signature
            .SignatureData = SignatureData
            .ShowDialog()
        End With

        If _Signature.DialogResult = DialogResult.OK Then
            SignatureData = _Signature.SignatureData
            _Return = True
        End If

        _Signature.Dispose()
        _Signature = Nothing

        Return _Return

    End Function

    Public Shared Function ReturnRoomFilter(ByVal PersonType As Enums.PersonType) As String

        Dim _RoomName As String = ""

        If Parameters.DefaultRoom <> "" Then
            For Each _R As SiteRoom In DataBase.Tables.SiteRooms
                If _R.Name = Parameters.DefaultRoom Then
                    Return _R.Name
                End If
            Next
        End If

        If _RoomName = "" Then

            Dim _Group As Pair = ReturnRoom(Parameters.SiteID, Enums.PersonType.Child)
            If _Group Is Nothing Then Return Nothing
            If _Group.Code = "" Then Return Nothing

            _RoomName = _Group.Text

        End If

        Return _RoomName

    End Function

    Public Shared Function ReturnRoom(ByVal SiteID As String, ByVal PersonType As Enums.PersonType, Optional ByVal ExcludedRoom As String = "") As Pair

        Dim _Buttons As New List(Of Pair)
        For Each _Room As SiteRoom In DataBase.Tables.SiteRooms
            _Buttons.Add(New Pair(_Room.ID.ToString, _Room.Name))
        Next

        Return ReturnButtons(_Buttons, "Select Room")

    End Function

    Public Shared Function ChooseFoodItem() As Pair

        Dim _SQL As String
        Dim _Pair As Pair = Nothing

        Dim _GroupID As String = ""
        Dim _GroupName As String = ""

        'get food group
        _SQL = "select distinct group_id, group_name from Food order by group_name"

        _Pair = ReturnButtonSQL(_SQL, "Select Food Group")
        If _Pair Is Nothing Then
            Return Nothing
        Else
            _GroupID = _Pair.Code
            _GroupName = _Pair.Text
            _Pair = Nothing
        End If

        _SQL = "select id, name from Food where group_id = '" + _GroupID + "' order by name"

        Return ReturnButtonSQL(_SQL, "Select Food")

    End Function

    Public Shared Sub LogAsleep(ByVal DayID As String, ByVal ChildID As String, ByVal ChildName As String, ByVal Location As String, _
                                ByVal StaffID As String, ByVal StaffName As String)

        Dim _Desc As String = ChildName + " Asleep"
        LogActivity(DayID, ChildID, ChildName, EnumActivityType.Asleep, _Desc, Location, , , StaffID, StaffName)

    End Sub

    Public Shared Sub LogSleepCheck(ByVal DayID As String, ByVal ChildID As String, ByVal ChildName As String, ByVal Location As String, _
                                    ByVal StaffID As String, ByVal StaffName As String)

        Dim _Desc As String = ChildName + " - Sleep Check"
        LogActivity(DayID, ChildID, ChildName, EnumActivityType.SleepCheck, _Desc, Location, , , StaffID, StaffName)

    End Sub

    Public Shared Sub ConvertToSleep(ByVal ID As String, ByVal StaffID As String, ByVal StaffName As String)

        Dim _Activity As NurseryGenieData.Activity = Business.Activity.ReturnActivityItem(ID)
        If _Activity IsNot Nothing Then

            Dim _Started As Date = _Activity.Stamp
            Dim _Ended As Date = Now

            'store the location and staff details for the original asleep
            Dim _Notes As String = _Activity.StaffName + " - " + _Activity.Value1

            'change to a sleep record
            Dim _Desc As String = "Sleep"
            Dim _Value1 As String = Format(_Started, "HH:mm").ToString + "-" + Format(_Ended, "HH:mm").ToString
            Dim _Value2 As String = Format(_Started, "HH:mm").ToString
            Dim _Value3 As String = Format(_Ended, "HH:mm").ToString

            Business.Activity.UpdateActivity(ID, EnumActivityType.Sleep, _Desc, Now, _Value1, _Value2, _Value3, StaffID, StaffName, _Notes)

        End If

    End Sub

    Public Class Enums

        Public Enum AddRemoveReturnMode
            NotSet
            AddRecord
            EditRecord
            RemoveRecord
        End Enum

        Public Enum PersonMode
            Everyone
            OnlyCheckedIn
            OnlyCheckedOut
        End Enum

        Public Enum InOut
            CheckIn
            CheckOut
        End Enum

        Public Enum PersonType
            Child
            Staff
            Visitor
            Contact
        End Enum

        Public Enum ToiletType
            WetNappy
            SoiledNappy
            Wet
            Soiled
        End Enum

        Public Enum RegisterVia
            Menu
            Child
            Register
            CheckOut
            MoveLogic
            Paxton
            UnlockPIN
        End Enum

        Public Enum AttendanceStatus
            CheckedIn
            CheckedOut
            Absent
            NotArrived
        End Enum

    End Class

    Private Shared Function AttendanceStatus() As Object
        Throw New NotImplementedException
    End Function

End Class
