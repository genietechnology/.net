﻿Imports System.Data

Namespace Business

    Public Class Register
        Inherits BusinessObjectBase

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Date As Date?
        Dim m_Type As String
        Dim m_InOut As String
        Dim m_PersonId As Guid?
        Dim m_Name As String
        Dim m_StampIn As DateTime?
        Dim m_StampOut As DateTime?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Date = Nothing
                ._Type = ""
                ._InOut = ""
                ._PersonId = Nothing
                ._Name = ""
                ._StampIn = Nothing
                ._StampOut = Nothing

            End With

        End Sub

        Public Sub New(ByVal ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Date = Nothing
                ._Type = ""
                ._InOut = ""
                ._PersonId = Nothing
                ._Name = ""
                ._StampIn = Nothing
                ._StampOut = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <FieldName("ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <FieldName("date")> _
        Public Property _Date() As Date?
            Get
                Return m_Date
            End Get
            Set(ByVal value As Date?)
                m_Date = value
            End Set
        End Property

        <FieldName("type")> _
        Public Property _Type() As String
            Get
                Return m_Type
            End Get
            Set(ByVal value As String)
                m_Type = value
            End Set
        End Property

        <FieldName("in_out")> _
        Public Property _InOut() As String
            Get
                Return m_InOut
            End Get
            Set(ByVal value As String)
                m_InOut = value
            End Set
        End Property

        <FieldName("person_id")> _
        Public Property _PersonId() As Guid?
            Get
                Return m_PersonId
            End Get
            Set(ByVal value As Guid?)
                m_PersonId = value
            End Set
        End Property

        <FieldName("name")> _
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <FieldName("stamp_in")> _
        Public Property _StampIn() As DateTime?
            Get
                Return m_StampIn
            End Get
            Set(ByVal value As DateTime?)
                m_StampIn = value
            End Set
        End Property

        <FieldName("stamp_out")> _
        Public Property _StampOut() As DateTime?
            Get
                Return m_StampOut
            End Get
            Set(ByVal value As DateTime?)
                m_StampOut = value
            End Set
        End Property

#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Register

            Dim _Register As Register
            _Register = PropertiesFromData(DAL.GetRowbyID("getRegisterbyID", ID))
            Return _Register

        End Function

        Public Shared Function SaveRegister(ByVal Register As Register) As Guid
            DAL.SaveRecord(Register, "register")
        End Function

#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Register

            Dim _R As New Register()

            If DR IsNot Nothing Then
                With DR
                    _R.IsNew = False
                    _R.IsDeleted = False
                    _R._ID = GetGUID(DR("ID"))
                    _R._Date = GetDate(DR("date"))
                    _R._Type = DR("type").ToString.Trim
                    _R._InOut = DR("in_out").ToString.Trim
                    _R._PersonId = GetGUID(DR("person_id"))
                    _R._Name = DR("name").ToString.Trim
                    _R._StampIn = GetDateTime(DR("stamp_in"))
                    _R._StampOut = GetDateTime(DR("stamp_out"))

                End With
            End If

            Return _R

        End Function

#End Region

    End Class

End Namespace
