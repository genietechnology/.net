﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class HostedBehavior
    Inherits BaseHostedPanel

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(HostedBehavior))
        Me.btnOther = New DevExpress.XtraEditors.SimpleButton()
        Me.btnToilet = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCorridor = New DevExpress.XtraEditors.SimpleButton()
        Me.btnClass = New DevExpress.XtraEditors.SimpleButton()
        Me.sc = New DevExpress.XtraEditors.XtraScrollableControl()
        Me.panTop = New DevExpress.XtraEditors.PanelControl()
        Me.btnMore = New DevExpress.XtraEditors.SimpleButton()
        Me.btnKick = New DevExpress.XtraEditors.SimpleButton()
        Me.btnGrab = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSwear = New DevExpress.XtraEditors.SimpleButton()
        Me.btnPush = New DevExpress.XtraEditors.SimpleButton()
        Me.btnThrow = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.panTop, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panTop.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnOther
        '
        Me.btnOther.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnOther.Location = New System.Drawing.Point(89, 32)
        Me.btnOther.Name = "btnOther"
        Me.btnOther.Size = New System.Drawing.Size(80, 23)
        Me.btnOther.TabIndex = 30
        Me.btnOther.Text = "Other"
        '
        'btnToilet
        '
        Me.btnToilet.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnToilet.Location = New System.Drawing.Point(89, 3)
        Me.btnToilet.Name = "btnToilet"
        Me.btnToilet.Size = New System.Drawing.Size(80, 23)
        Me.btnToilet.TabIndex = 29
        Me.btnToilet.Text = "Toilet"
        '
        'btnCorridor
        '
        Me.btnCorridor.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnCorridor.Location = New System.Drawing.Point(3, 32)
        Me.btnCorridor.Name = "btnCorridor"
        Me.btnCorridor.Size = New System.Drawing.Size(80, 23)
        Me.btnCorridor.TabIndex = 28
        Me.btnCorridor.Text = "Corridor"
        '
        'btnClass
        '
        Me.btnClass.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnClass.Location = New System.Drawing.Point(3, 3)
        Me.btnClass.Name = "btnClass"
        Me.btnClass.Size = New System.Drawing.Size(80, 23)
        Me.btnClass.TabIndex = 26
        Me.btnClass.Text = "Classroom"
        '
        'sc
        '
        Me.sc.AllowTouchScroll = True
        Me.sc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.sc.Location = New System.Drawing.Point(0, 59)
        Me.sc.Name = "sc"
        Me.sc.Size = New System.Drawing.Size(506, 103)
        Me.sc.TabIndex = 98
        '
        'panTop
        '
        Me.panTop.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.panTop.Controls.Add(Me.btnMore)
        Me.panTop.Controls.Add(Me.btnKick)
        Me.panTop.Controls.Add(Me.btnOther)
        Me.panTop.Controls.Add(Me.btnToilet)
        Me.panTop.Controls.Add(Me.btnCorridor)
        Me.panTop.Controls.Add(Me.btnClass)
        Me.panTop.Controls.Add(Me.btnGrab)
        Me.panTop.Controls.Add(Me.btnSwear)
        Me.panTop.Controls.Add(Me.btnPush)
        Me.panTop.Controls.Add(Me.btnThrow)
        Me.panTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.panTop.Location = New System.Drawing.Point(0, 0)
        Me.panTop.Name = "panTop"
        Me.panTop.Size = New System.Drawing.Size(506, 59)
        Me.panTop.TabIndex = 97
        '
        'btnMore
        '
        Me.btnMore.Appearance.Image = CType(resources.GetObject("btnMore.Appearance.Image"), System.Drawing.Image)
        Me.btnMore.Appearance.Options.UseImage = True
        Me.btnMore.Appearance.Options.UseTextOptions = True
        Me.btnMore.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnMore.Image = CType(resources.GetObject("btnMore.Image"), System.Drawing.Image)
        Me.btnMore.Location = New System.Drawing.Point(367, 32)
        Me.btnMore.Name = "btnMore"
        Me.btnMore.Size = New System.Drawing.Size(90, 23)
        Me.btnMore.TabIndex = 32
        Me.btnMore.Text = "More..."
        '
        'btnKick
        '
        Me.btnKick.Appearance.Image = CType(resources.GetObject("btnKick.Appearance.Image"), System.Drawing.Image)
        Me.btnKick.Appearance.Options.UseImage = True
        Me.btnKick.Appearance.Options.UseTextOptions = True
        Me.btnKick.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnKick.Image = CType(resources.GetObject("btnKick.Image"), System.Drawing.Image)
        Me.btnKick.Location = New System.Drawing.Point(367, 3)
        Me.btnKick.Name = "btnKick"
        Me.btnKick.Size = New System.Drawing.Size(90, 23)
        Me.btnKick.TabIndex = 31
        Me.btnKick.Text = "Kick"
        '
        'btnGrab
        '
        Me.btnGrab.Appearance.Image = CType(resources.GetObject("btnGrab.Appearance.Image"), System.Drawing.Image)
        Me.btnGrab.Appearance.Options.UseImage = True
        Me.btnGrab.Appearance.Options.UseTextOptions = True
        Me.btnGrab.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnGrab.Image = CType(resources.GetObject("btnGrab.Image"), System.Drawing.Image)
        Me.btnGrab.Location = New System.Drawing.Point(271, 32)
        Me.btnGrab.Name = "btnGrab"
        Me.btnGrab.Size = New System.Drawing.Size(90, 23)
        Me.btnGrab.TabIndex = 25
        Me.btnGrab.Text = "Grab"
        '
        'btnSwear
        '
        Me.btnSwear.Appearance.Image = CType(resources.GetObject("btnSwear.Appearance.Image"), System.Drawing.Image)
        Me.btnSwear.Appearance.Options.UseImage = True
        Me.btnSwear.Appearance.Options.UseTextOptions = True
        Me.btnSwear.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnSwear.Image = CType(resources.GetObject("btnSwear.Image"), System.Drawing.Image)
        Me.btnSwear.Location = New System.Drawing.Point(271, 3)
        Me.btnSwear.Name = "btnSwear"
        Me.btnSwear.Size = New System.Drawing.Size(90, 23)
        Me.btnSwear.TabIndex = 24
        Me.btnSwear.Text = "Swear"
        '
        'btnPush
        '
        Me.btnPush.Appearance.Image = CType(resources.GetObject("btnPush.Appearance.Image"), System.Drawing.Image)
        Me.btnPush.Appearance.Options.UseImage = True
        Me.btnPush.Appearance.Options.UseTextOptions = True
        Me.btnPush.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnPush.Image = CType(resources.GetObject("btnPush.Image"), System.Drawing.Image)
        Me.btnPush.Location = New System.Drawing.Point(175, 32)
        Me.btnPush.Name = "btnPush"
        Me.btnPush.Size = New System.Drawing.Size(90, 23)
        Me.btnPush.TabIndex = 23
        Me.btnPush.Text = "Push"
        '
        'btnThrow
        '
        Me.btnThrow.Appearance.Image = CType(resources.GetObject("btnThrow.Appearance.Image"), System.Drawing.Image)
        Me.btnThrow.Appearance.Options.UseImage = True
        Me.btnThrow.Appearance.Options.UseTextOptions = True
        Me.btnThrow.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnThrow.Image = CType(resources.GetObject("btnThrow.Image"), System.Drawing.Image)
        Me.btnThrow.Location = New System.Drawing.Point(175, 3)
        Me.btnThrow.Name = "btnThrow"
        Me.btnThrow.Size = New System.Drawing.Size(90, 23)
        Me.btnThrow.TabIndex = 22
        Me.btnThrow.Text = "Throw"
        '
        'HostedBehavior
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.sc)
        Me.Controls.Add(Me.panTop)
        Me.Name = "HostedBehavior"
        Me.Size = New System.Drawing.Size(506, 162)
        CType(Me.panTop, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panTop.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnOther As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnToilet As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCorridor As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnClass As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnGrab As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSwear As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnPush As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnThrow As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sc As DevExpress.XtraEditors.XtraScrollableControl
    Friend WithEvents panTop As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnMore As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnKick As DevExpress.XtraEditors.SimpleButton
End Class
