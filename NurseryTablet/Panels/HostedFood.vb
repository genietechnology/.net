﻿Option Strict On

Imports DevExpress.XtraBars.Navigation
Imports DevExpress.XtraEditors
Imports NurseryTablet.SharedModule
Imports System.Reflection

Public Class HostedFood

    Private m_SelectedMealID As String = ""
    Private m_SelectedMealType As String = ""
    Private m_SelectedMealName As String = ""

    Private Sub HostedFood_Accepting(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles Me.Accepting
        SaveFood()
    End Sub

    Private Sub HostedFood_Initialising() Handles Me.Initialising
        SetupMeals()
        SelectFirstMeal()
    End Sub

    Private Sub SelectFirstMeal()
        If btnBreakfast.Enabled AndAlso btnBreakfast.Tag.ToString <> "" Then SetMeal("B") : Exit Sub
        If btnAM.Enabled AndAlso btnAM.Tag.ToString <> "" Then SetMeal("S") : Exit Sub
        If btnLunch.Enabled AndAlso btnLunch.Tag.ToString <> "" Then SetMeal("L") : Exit Sub
        If btnLunchDessert.Enabled AndAlso btnLunchDessert.Tag.ToString <> "" Then SetMeal("LD") : Exit Sub
        If btnPM.Enabled AndAlso btnPM.Tag.ToString <> "" Then SetMeal("SP") : Exit Sub
        If btnTea.Enabled AndAlso btnTea.Tag.ToString <> "" Then SetMeal("T") : Exit Sub
        If btnTeaDessert.Enabled AndAlso btnTeaDessert.Tag.ToString <> "" Then SetMeal("TD") : Exit Sub
    End Sub

    Private Sub HostedFood_ChildChanged(sender As Object, e As RecordChangedArgs) Handles Me.ChildChanged
        PopulateItems()
    End Sub

    Private Sub HostedFood_ChildChanging(sender As Object, e As RecordChangingArgs) Handles Me.ChildChanging
        SaveFood()
    End Sub

    Private Sub HostedFood_GroupChanged(sender As Object, e As RecordChangedArgs) Handles Me.GroupChanged
        sc.Controls.Clear()
    End Sub

    Private Sub HostedFood_GroupChanging(sender As Object, e As RecordChangingArgs) Handles Me.GroupChanging
        SaveFood()
    End Sub

    Private Sub AddFoodItem(ByVal FoodID As String, ByVal FoodName As String, ByVal Status As String)
        Dim _Item As New FoodControl(FoodID, FoodName, Status)
        _Item.Dock = DockStyle.Top
        _Item.BringToFront()
        sc.Controls.Add(_Item)
    End Sub

    Private Sub SaveFood()

        If Not Me.SelectedChild.Populated Then Exit Sub

        If m_SelectedMealID = "" Then Exit Sub
        If m_SelectedMealType = "" Then Exit Sub
        If m_SelectedMealName = "" Then Exit Sub

        For Each _i As FoodControl In sc.Controls

            LogFood(Parameters.TodayID, SelectedChild.ID, SelectedChild.Name,
                    m_SelectedMealID, m_SelectedMealType, m_SelectedMealName,
                    _i.FoodID, _i.FoodName, _i.FoodStatus)

        Next

    End Sub

    Private Sub PopulateItems()

        sc.Controls.Clear()
        If m_SelectedMealID = "" Then Exit Sub

        'check if we have generated the FoodRegister yet
        If Business.Food.FoodItemsExist(SelectedChild.ID, m_SelectedMealID, m_SelectedMealType) Then
            Dim _Items As List(Of Business.Food.FoodItemStatus) = Business.Food.GetFoodRegister(SelectedChild.ID, m_SelectedMealID, m_SelectedMealType)
            If _Items IsNot Nothing AndAlso _Items.Count > 0 Then
                For Each _Item As Business.Food.FoodItemStatus In _Items
                    AddFoodItem(_Item.FoodID.ToString, _Item.FoodName, _Item.Status.ToString)
                Next
            End If
        Else
            GenerateFoodItems()
        End If

    End Sub

    Private Sub GenerateFoodItems()
        Dim _Items As List(Of NurseryGenieData.Food) = Business.Food.GetMealComponents(m_SelectedMealID)
        If _Items IsNot Nothing AndAlso _Items.Count > 0 Then
            For Each _I As NurseryGenieData.Food In _Items
                AddFoodItem(_I.FoodID.ToString, _I.FoodName, "X")
            Next
        End If
    End Sub

    Private Sub SetupMeals()

        Dim _Day As NurseryGenieData.Day = DataBase.DayRecord
        If _Day IsNot Nothing Then

            Dim _BreakfastID As String = _Day.BreakfastId.ToString
            If _BreakfastID <> "" Then
                btnBreakfast.Enabled = True
                btnBreakfast.Tag = _BreakfastID
                btnBreakfast.AccessibleDescription = _Day.BreakfastName
            Else
                btnBreakfast.Enabled = False
            End If

            Dim _SnackID As String = _Day.SnackId.ToString
            If _SnackID <> "" Then
                btnAM.Enabled = True
                btnAM.Tag = _SnackID
                btnAM.AccessibleDescription = _Day.SnackName
            Else
                btnAM.Enabled = False
            End If

            Dim _LunchID As String = _Day.LunchId.ToString
            If _LunchID <> "" Then
                btnLunch.Enabled = True
                btnLunch.Tag = _LunchID
                btnLunch.AccessibleDescription = _Day.LunchName
            Else
                btnLunch.Enabled = False
            End If

            Dim _LunchDessertID As String = _Day.LunchDesId.ToString
            If _LunchDessertID <> "" Then
                btnLunchDessert.Enabled = True
                btnLunchDessert.Tag = _LunchDessertID
                btnLunchDessert.AccessibleDescription = _Day.LunchDesName
            Else
                btnLunchDessert.Enabled = False
            End If

            Dim _SnackPMID As String = _Day.PMSnackId.ToString
            If _SnackPMID <> "" Then
                btnPM.Enabled = True
                btnPM.Tag = _SnackPMID
                btnPM.AccessibleDescription = _Day.PMSnackName
            Else
                btnPM.Enabled = False
            End If

            Dim _TeaID As String = _Day.TeaId.ToString
            If _TeaID <> "" Then
                btnTea.Enabled = True
                btnTea.Tag = _TeaID
                btnTea.AccessibleDescription = _Day.TeaName
            Else
                btnTea.Enabled = False
            End If

            Dim _TeaDessertID As String = _Day.TeaDesId.ToString
            If _TeaDessertID <> "" Then
                btnTeaDessert.Enabled = True
                btnTeaDessert.Tag = _TeaDessertID
                btnTeaDessert.AccessibleDescription = _Day.TeaDesName
            Else
                btnTeaDessert.Enabled = False
            End If

        End If

    End Sub

    Private Sub btnBreakfast_Click(sender As Object, e As EventArgs) Handles btnBreakfast.Click
        SetMeal("B")
    End Sub

    Private Sub btnAM_Click(sender As Object, e As EventArgs) Handles btnAM.Click
        SetMeal("S")
    End Sub

    Private Sub btnLunch_Click(sender As Object, e As EventArgs) Handles btnLunch.Click
        SetMeal("L")
    End Sub

    Private Sub btnLunchDessert_Click(sender As Object, e As EventArgs) Handles btnLunchDessert.Click
        SetMeal("LD")
    End Sub

    Private Sub btnPM_Click(sender As Object, e As EventArgs) Handles btnPM.Click
        SetMeal("SP")
    End Sub

    Private Sub btnTea_Click(sender As Object, e As EventArgs) Handles btnTea.Click
        SetMeal("T")
    End Sub

    Private Sub btnTeaDessert_Click(sender As Object, e As EventArgs) Handles btnTeaDessert.Click
        SetMeal("TD")
    End Sub

    Private Sub SetMeal(ByVal MealType As String)

        If SelectedChild.Populated Then
            SaveFood()
        End If

        m_SelectedMealType = MealType
        ResetMealButtons()

        Select Case MealType

            Case "B"
                m_SelectedMealID = btnBreakfast.Tag.ToString
                m_SelectedMealName = btnBreakfast.AccessibleDescription
                btnBreakfast.Appearance.BackColor = txtYellow.BackColor

            Case "S"
                m_SelectedMealID = btnAM.Tag.ToString
                m_SelectedMealName = btnAM.AccessibleDescription
                btnAM.Appearance.BackColor = txtYellow.BackColor

            Case "L"
                m_SelectedMealID = btnLunch.Tag.ToString
                m_SelectedMealName = btnLunch.AccessibleDescription
                btnLunch.Appearance.BackColor = txtYellow.BackColor

            Case "LD"
                m_SelectedMealID = btnLunchDessert.Tag.ToString
                m_SelectedMealName = btnLunchDessert.AccessibleDescription
                btnLunchDessert.Appearance.BackColor = txtYellow.BackColor

            Case "SP"
                m_SelectedMealID = btnPM.Tag.ToString
                m_SelectedMealName = btnPM.AccessibleDescription
                btnPM.Appearance.BackColor = txtYellow.BackColor

            Case "T"
                m_SelectedMealID = btnTea.Tag.ToString
                m_SelectedMealName = btnTea.AccessibleDescription
                btnTea.Appearance.BackColor = txtYellow.BackColor

            Case "TD"
                m_SelectedMealID = btnTeaDessert.Tag.ToString
                m_SelectedMealName = btnTeaDessert.AccessibleDescription
                btnTeaDessert.Appearance.BackColor = txtYellow.BackColor

        End Select

        If SelectedChild.Populated Then
            PopulateItems()
        End If

    End Sub

    Private Sub ResetMealButtons()
        btnBreakfast.Appearance.Reset()
        btnAM.Appearance.Reset()
        btnLunch.Appearance.Reset()
        btnLunchDessert.Appearance.Reset()
        btnPM.Appearance.Reset()
        btnTea.Appearance.Reset()
        btnTeaDessert.Appearance.Reset()
    End Sub

    Private Sub btnAddFood_Click(sender As Object, e As EventArgs) Handles btnAddFood.Click
        Dim _P As Pair = ChooseFoodItem()
        If _P IsNot Nothing Then
            AddFoodItem(_P.Code, _P.Text, "3")
        End If
    End Sub
End Class
