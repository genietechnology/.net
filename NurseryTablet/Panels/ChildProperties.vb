﻿Public Class ChildProperties

    Private m_Populated As Boolean = False
    Public ReadOnly Property Populated As Boolean
        Get
            Return m_Populated
        End Get
    End Property

    Public Property ID As String
    Public Property Name As String
    Public Property Forename As String
    Public Property BabyFood As Boolean
    Public Property OffMenu As Boolean
    Public Property Nappies As Boolean

    Public Sub Clear()
        m_Populated = False
        ID = ""
        Name = ""
        Forename = ""
        BabyFood = False
        OffMenu = False
        Nappies = True
    End Sub

    Public Sub Retreive(ByVal ChildID As String)

        Dim _C As NurseryGenieData.Child = Business.Child.ReturnChildByID(ChildID)
        If _C IsNot Nothing Then

            ID = ChildID
            Name = _C.FullName
            Forename = _C.Forename
            BabyFood = _C.BabyFood
            OffMenu = _C.OffMenu
            Nappies = _C.Nappies

            m_Populated = True

        Else
            m_Populated = False
        End If

    End Sub

End Class
