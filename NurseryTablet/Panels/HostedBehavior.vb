﻿Imports NurseryTablet.SharedModule

Public Class HostedBehavior

    Private m_ChildID As String = ""
    Private m_Location As String = ""
    Private m_Behaviors As New List(Of Pair)
    Private m_Locations As New List(Of Pair)

    Private Sub HostedBehavior_Load(sender As Object, e As EventArgs) Handles Me.Load
        m_Behaviors = Business.Lists.GetListAsPairs("Exhibited Behaviour")
        m_Locations = Business.Lists.GetListAsPairs("Locations")
        SetClass()
    End Sub

    Private Sub HostedSleep_ChildChanged(sender As Object, e As RecordChangedArgs) Handles Me.ChildChanged
        m_ChildID = e.ValueID
        DisplayBehavior(e.ValueID)
    End Sub

    Private Sub LogBehavior(ByVal Behavior As String)
        SharedModule.LogBehavior(SharedModule.TodayID, SelectedChild.ID, SelectedChild.Name, Behavior, m_Location, SelectedStaff.StaffID, SelectedStaff.StaffName)
        DisplayBehavior(m_ChildID)
    End Sub

    Private Sub DisplayBehavior(ByVal ChildID As String)

        sc.Controls.Clear()

        Dim _Activity As List(Of NurseryGenieData.Activity) = Business.Activity.ReturnActivity(ChildID, EnumActivityType.Behavior)
        If _Activity IsNot Nothing Then

            For Each _A As NurseryGenieData.Activity In _Activity

                Dim _ID As String = _A.ID.ToString

                Dim _Item As New ItemControl(_ID, _A.Description, False, True)
                With _Item
                    .ChildID = ChildID
                    .Value1 = _A.Value1
                    .Value2 = _A.Value2
                    .Value3 = _A.Value3
                    .Stamp = _A.Stamp
                    .Dock = DockStyle.Top
                    .BringToFront()
                End With

                AddHandler _Item.DeleteClick, AddressOf DeleteClick

                sc.Controls.Add(_Item)

            Next

        End If

    End Sub

    Private Sub DeleteClick(sender As Object, e As EventArgs)

        If Msgbox("Are you sure you want to delete this Behavior Record?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Confirm Delete") = DialogResult.Yes Then
            Dim _I As ItemControl = CType(sender, ItemControl)
            If _I IsNot Nothing Then
                If _I.ActivtyID <> "" Then
                    SharedModule.DeleteActivity(_I.ActivtyID)
                    DisplayBehavior(_I.ChildID)
                End If
            End If
        End If

    End Sub

    Private Sub btnPush_Click(sender As Object, e As EventArgs) Handles btnPush.Click
        LogBehavior("Pushing")
    End Sub

    Private Sub btnSwear_Click(sender As Object, e As EventArgs) Handles btnSwear.Click
        LogBehavior("Swearing")
    End Sub

    Private Sub btnGrab_Click(sender As Object, e As EventArgs) Handles btnGrab.Click
        LogBehavior("Grabbing")
    End Sub

    Private Sub btnKick_Click(sender As Object, e As EventArgs) Handles btnKick.Click
        LogBehavior("Kicking")
    End Sub

    Private Sub btnMore_Click(sender As Object, e As EventArgs) Handles btnMore.Click
        Dim _P As SharedModule.Pair = SharedModule.ReturnButtons(m_Behaviors, "Select Exhibited Behavior")
        If _P IsNot Nothing Then
            LogBehavior(_P.Text)
        End If
    End Sub

    Private Sub btnThrow_Click(sender As Object, e As EventArgs) Handles btnThrow.Click
        LogBehavior("Throwing")
    End Sub

    Private Sub btnClass_Click(sender As Object, e As EventArgs) Handles btnClass.Click
        SetClass()
    End Sub

    Private Sub btnCorridor_Click(sender As Object, e As EventArgs) Handles btnCorridor.Click
        m_Location = "Corridor"
        btnClass.Appearance.Reset()
        btnCorridor.Appearance.BackColor = Color.Yellow
        btnToilet.Appearance.Reset()
        btnOther.Appearance.Reset()
    End Sub

    Private Sub btnToilet_Click(sender As Object, e As EventArgs) Handles btnToilet.Click
        m_Location = "Toilet"
        btnClass.Appearance.Reset()
        btnCorridor.Appearance.Reset()
        btnToilet.Appearance.BackColor = Color.Yellow
        btnOther.Appearance.Reset()
    End Sub

    Private Sub btnOther_Click(sender As Object, e As EventArgs) Handles btnOther.Click

        Dim _P As SharedModule.Pair = SharedModule.ReturnButtons(m_Locations, "Select Location")
        If _P IsNot Nothing Then
            m_Location = _P.Text
            btnClass.Appearance.Reset()
            btnCorridor.Appearance.Reset()
            btnToilet.Appearance.Reset()
            btnOther.Appearance.BackColor = Color.Yellow
        End If

    End Sub

    Private Sub SetClass()
        m_Location = "Classroom"
        btnClass.Appearance.BackColor = Color.Yellow
        btnCorridor.Appearance.Reset()
        btnToilet.Appearance.Reset()
        btnOther.Appearance.Reset()
    End Sub

End Class
