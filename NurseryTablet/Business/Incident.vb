﻿Namespace Business

    Public Class Incident

        Public Shared Sub DeleteIncident(ByVal IncidentID As String)

        End Sub

        Public Shared Sub UpdateIncident(ByVal IncidentIn As NurseryGenieData.Incident)

        End Sub

        Public Shared Sub CreateIncident(ByVal IncidentIn As NurseryGenieData.Incident)

            Dim _I As New NurseryGenieData.Incident
            With _I

                .ID = IncidentIn.ID

                .DayId = IncidentIn.DayId
                .ChildId = IncidentIn.ChildId
                .ChildName = IncidentIn.ChildName

                .StaffId = IncidentIn.StaffId
                .StaffName = IncidentIn.StaffName

                .IncidentType = IncidentIn.IncidentType
                .Location = IncidentIn.Location
                .Details = IncidentIn.Details
                .Injury = IncidentIn.Injury
                .Treatment = IncidentIn.Treatment
                .Action = IncidentIn.Action

                .SigStaff = IncidentIn.SigStaff
                .SigWitness = IncidentIn.SigWitness

                .Photo = IncidentIn.Photo
                .BodyMap = IncidentIn.BodyMap

                .Stamp = Now

            End With

            DataBase.Tables.Incidents.Add(_I)
            DataBase.PersistIncidentsLocally()

        End Sub

        Public Shared Function GetIncidents(ByVal DayID As String, ChildID As String) As List(Of NurseryGenieData.Incident)

            Dim _Incidents = From _I As NurseryGenieData.Incident In DataBase.Tables.Incidents
                             Where _I.DayId = New Guid(DayID) _
                             And _I.ChildId = New Guid(ChildID) _
                             Order By _I.Stamp

            If _Incidents IsNot Nothing Then
                If _Incidents.Count > 0 Then
                    Return _Incidents.ToList
                End If
            End If

            Return Nothing

        End Function

        Public Shared Function GetIncident(ByVal ID As String) As NurseryGenieData.Incident

            Dim _Incidents = From _I As NurseryGenieData.Incident In DataBase.Tables.Incidents
                             Where _I.ID = New Guid(ID)

            If _Incidents IsNot Nothing AndAlso _Incidents.Count = 1 Then
                Return _Incidents.First
            End If

            Return Nothing

        End Function

        Public Shared Sub UpdateIncident(ByVal ChildID As String, ByRef SignatureCapture As Signature)

            'Dim _SQL As String = ""
            '_SQL += "select * from Incidents"
            '_SQL += " where day_id = '" + Parameters.TodayID + "'"
            '_SQL += " and child_id = '" + ChildID + "'"

            'Dim _DA As IDataAdapter = DAL.ReturnDataAdapter(_SQL)
            'If Not _DA Is Nothing Then

            '    Dim _DR As DataRow = Nothing
            '    Dim _DS As New DataSet
            '    _DA.Fill(_DS)

            '    If _DS.Tables(0).Rows.Count > 0 Then
            '        _DR = _DS.Tables(0).Rows(0)
            '        _DR("sig_contact") = SignatureCapture.SignatureID
            '        DAL.UpdateDB(_DA, _DS)
            '    End If

            '    _DR = Nothing

            '    _DS.Dispose()
            '    _DS = Nothing

            '    _DA = Nothing

            'End If

        End Sub

    End Class

End Namespace

