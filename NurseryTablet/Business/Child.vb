﻿Imports DevExpress.XtraScheduler.Native
Imports NurseryTablet.SharedModule

Namespace Business

    Public Class Child

        Public Enum EnumSleepStatus
            Asleep
            Awake
        End Enum

        Public Shared Function IsAbsent(ByVal ChildID As String) As Boolean
            Dim _Absence As List(Of NurseryGenieData.Activity) = Business.Activity.ReturnActivity(ChildID, SharedModule.EnumActivityType.Absence)
            If _Absence IsNot Nothing Then
                If _Absence.Count > 0 Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        End Function

        Public Shared Function GetCheckedIn() As List(Of NurseryGenieData.Child)

            Dim _Children = From _C As NurseryGenieData.Child In DataBase.Tables.Children
                            Order By _C.FullName Descending

            If _Children IsNot Nothing Then

                Dim _Return As New List(Of NurseryGenieData.Child)

                For Each _C In _Children
                    If PersonInorOut(_C.ID.ToString) = Enums.InOut.CheckIn Then
                        _Return.Add(_C)
                    End If
                Next

                If _Return.Count > 0 Then
                    Return _Return
                Else
                    Return Nothing
                End If

            Else
                Return Nothing
            End If

        End Function

        Public Shared Function GetCheckedIn(ByVal RoomName As String) As List(Of NurseryGenieData.Child)

            Dim _Children = From _C As NurseryGenieData.Child In DataBase.Tables.Children
                            Order By _C.FullName Descending

            If _Children IsNot Nothing Then

                Dim _Return As New List(Of NurseryGenieData.Child)

                For Each _C In _Children
                    If PersonStatus(_C.ID.ToString) = Enums.AttendanceStatus.CheckedIn Then
                        Dim _CurrentLocation As String = PersonLocation(_C.ID.ToString)
                        If _CurrentLocation <> "" Then
                            'we have a location for the child
                            If _CurrentLocation = RoomName Then
                                _Return.Add(_C)
                            End If
                        Else
                            _Return.Add(_C)
                        End If
                    End If
                Next

                If _Return.Count > 0 Then
                    Return _Return
                Else
                    Return Nothing
                End If

            Else
                Return Nothing
            End If

        End Function

        Public Shared Function GetSleepers(ByVal GroupName As String, ByVal Status As EnumSleepStatus) As List(Of Business.Sleeper)

            Dim _Children As List(Of NurseryGenieData.Child) = GetCheckedIn(GroupName)
            If _Children IsNot Nothing Then

                Dim _Return As New List(Of Business.Sleeper)

                For Each _C In _Children

                    Dim _Activity As NurseryGenieData.Activity = Nothing
                    Dim _Sleeping As Boolean = Business.Activity.ReturnActivityItem(_C.ID.ToString, EnumActivityType.Asleep, _Activity)

                    If Status = EnumSleepStatus.Asleep AndAlso _Sleeping Then
                        _Return.Add(New Business.Sleeper(_C.ID.ToString, _C.FullName, _Activity))
                    End If

                    If Status = EnumSleepStatus.Awake AndAlso _Sleeping = False Then
                        _Return.Add(New Business.Sleeper(_C.ID.ToString, _C.FullName))
                    End If

                Next

                If _Return.Count > 0 Then
                    Return _Return
                Else
                    Return Nothing
                End If

            Else
                Return Nothing
            End If

        End Function

        Public Shared Function GetCheckedInPairs(ByVal GroupName As String) As List(Of Pair)

            Dim _Children = From _C As NurseryGenieData.Child In DataBase.Tables.Children
                            Where _C.GroupName = GroupName
                            Order By _C.FullName

            If _Children IsNot Nothing Then

                Dim _Return As New List(Of Pair)

                For Each _C In _Children
                    If PersonInorOut(_C.ID.ToString) = Enums.InOut.CheckIn Then
                        _Return.Add(New Pair(_C.ID.ToString, _C.FullName))
                    End If
                Next

                If _Return.Count > 0 Then
                    Return _Return
                Else
                    Return Nothing
                End If

            Else
                Return Nothing
            End If

        End Function

        Public Shared Function GetSiblings(ByVal ChildID As String) As List(Of NurseryGenieData.Child)

            'Dim _SQL As String = ""
            '_SQL = "select id, fullname from Children where family_id = '" + m_FamilyID + "' and id <> '" + m_ChildID + "'"

            Return New List(Of NurseryGenieData.Child)

        End Function

        Public Shared Function GetContacts(ByVal ChildID As String) As List(Of NurseryGenieData.Contact)

            Dim _FamilyID As String = ReturnFamilyID(ChildID)

            Dim _Contacts = From _C As NurseryGenieData.Contact In DataBase.Tables.Contacts
                            Where _C.FamilyId = New Guid(_FamilyID) Order By _C.PrimaryCont Descending, _C.Forename

            If _Contacts IsNot Nothing Then
                If _Contacts.Count > 0 Then
                    Return _Contacts.ToList()
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If

        End Function

        'Public Shared Function ReturnLocation(ByVal ChildID As String)

        '    Dim _Return As String = ""
        '    'Dim _SQL As String = ""

        '    'If Parameters.DisableLocation Then
        '    '    _SQL += "select group_name as 'location' from Children"
        '    '    _SQL += " where ID = '" + ChildID + "'"
        '    'Else
        '    '    _SQL += "select location from RegisterSummary"
        '    '    _SQL += " where day_id = '" + TodayID + "'"
        '    '    _SQL += " and person_type = 'C'"
        '    '    _SQL += " and person_id = '" + ChildID + "'"
        '    'End If

        '    'Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
        '    'If _DR IsNot Nothing Then
        '    '    _Return = _DR.Item("location").ToString
        '    '    _DR = Nothing
        '    'End If

        '    Return _Return

        'End Function

        Public Shared Function ReturnName(ByVal ChildID As String) As String

            Dim _Children = From _C As NurseryGenieData.Child In DataBase.Tables.Children
                            Where _C.ID = New Guid(ChildID)

            If _Children IsNot Nothing Then
                Return _Children.First.FullName
            Else
                Return ""
            End If

        End Function

        Public Shared Sub DrillDown(ByVal ChildID As String, ByVal ChildName As String)

            Dim _frm As New frmChild
            _frm.ChildID = ChildID
            _frm.ChildName = ChildName

            _frm.ShowDialog()

        End Sub

        Public Shared Sub DrillDownModal(ByVal ChildID As String, ByVal ChildName As String)

            Dim _frm As New frmChild
            _frm.ChildID = ChildID
            _frm.ChildName = ChildName

            _frm.ShowDialog()

            _frm.Dispose()
            _frm = Nothing

        End Sub

        Public Shared Function ReturnChildByID(ByVal ChildID As String) As NurseryGenieData.Child

            Dim _Children = From _C As NurseryGenieData.Child In DataBase.Tables.Children
                            Where _C.ID = New Guid(ChildID)

            If _Children IsNot Nothing Then
                Return _Children.First()
            Else
                Return Nothing
            End If

        End Function

        Public Shared Function ReturnFamilyID(ByVal ChildID As String) As String

            Dim _Children = From _C As NurseryGenieData.Child In DataBase.Tables.Children
                            Where _C.ID = New Guid(ChildID)

            If _Children IsNot Nothing Then
                Return _Children.First.FamilyID.ToString()
            Else
                Return ""
            End If

        End Function

        Public Shared Function FindChildByGroup(ByVal Mode As SharedModule.Enums.PersonMode, Optional ByVal AnyDay As Boolean = False) As Pair
            Return SharedModule.ReturnButtons(GetButtons(Mode, AnyDay, False), "Select Child")
        End Function

        Public Shared Function FindMultipleChildrenByGroup(ByVal Mode As SharedModule.Enums.PersonMode, Optional ByVal AnyDay As Boolean = False) As List(Of Pair)
            Return SharedModule.ReturnMultipleButtons(GetButtons(Mode, AnyDay, True), "Select Child")
        End Function

        Private Shared Function GetButtons(ByVal Mode As SharedModule.Enums.PersonMode, ByVal AnyDay As Boolean, ByVal Multiselect As Boolean) As List(Of Pair)

            Dim _Room As String = ReturnRoomFilter(Enums.PersonType.Child)
            If _Room = "" Then Return Nothing

            Dim _Return As List(Of Pair) = Nothing
            Dim _Children As Object

            If AnyDay Then

                If NurseryTablet.Parameters.SortBySurname Then

                    _Children = From _C As NurseryGenieData.Child In DataBase.Tables.Children
                                Where _C.GroupName = _Room
                                Order By _C.Surname, _C.Forename
                                Select _C

                Else

                    _Children = From _C As NurseryGenieData.Child In DataBase.Tables.Children
                                Where _C.GroupName = _Room
                                Order By _C.FullName
                                Select _C

                End If

            Else

                If NurseryTablet.Parameters.SortBySurname Then

                    _Children = From _C As NurseryGenieData.Child In DataBase.Tables.Children
                                Join _B As NurseryGenieData.Booking In DataBase.Tables.Bookings On _C.ID Equals _B.ChildId
                                Where _B.RoomName = _Room And _B.BookingDate = Today
                                Order By _C.Surname, _C.Forename
                                Select _C

                Else

                    _Children = From _C As NurseryGenieData.Child In DataBase.Tables.Children
                                Join _B As NurseryGenieData.Booking In DataBase.Tables.Bookings On _B.ChildId Equals _C.ID
                                Where _B.RoomName = _Room And _B.BookingDate = Today
                                Order By _C.FullName
                                Select _C

                End If

            End If

            If _Children IsNot Nothing Then

                Dim _Buttons As New List(Of SharedModule.Pair)

                For Each _C As NurseryGenieData.Child In _Children

                    Dim _Name As String = _C.FullName
                    If NurseryTablet.Parameters.SortBySurname Then _Name = _C.Surname + ", " + _C.Forename

                    If Not AnyDay Then

                        'we only want children due in today
                        If Today.DayOfWeek = DayOfWeek.Monday AndAlso _C.Monday = False Then Continue For
                        If Today.DayOfWeek = DayOfWeek.Tuesday AndAlso _C.Tuesday = False Then Continue For
                        If Today.DayOfWeek = DayOfWeek.Wednesday AndAlso _C.Wednesday = False Then Continue For
                        If Today.DayOfWeek = DayOfWeek.Thursday AndAlso _C.Thursday = False Then Continue For
                        If Today.DayOfWeek = DayOfWeek.Friday AndAlso _C.Friday = False Then Continue For
                        If Today.DayOfWeek = DayOfWeek.Saturday AndAlso _C.Saturday = False Then Continue For
                        If Today.DayOfWeek = DayOfWeek.Sunday AndAlso _C.Sunday = False Then Continue For

                    End If

                    Dim _PersonID As String = _C.ID.ToString()
                    Dim _Status As Enums.InOut = PersonInorOut(_PersonID)

                    Select Case Mode

                        Case Enums.PersonMode.Everyone
                            _Buttons.Add(New Pair(_PersonID.ToString, _Name))

                        Case Enums.PersonMode.OnlyCheckedIn
                            If _Status = Enums.InOut.CheckIn Then
                                _Buttons.Add(New Pair(_PersonID.ToString, _Name))
                            End If

                        Case Enums.PersonMode.OnlyCheckedOut
                            If _Status = Enums.InOut.CheckOut Then
                                _Buttons.Add(New Pair(_PersonID.ToString, _Name))
                            End If

                    End Select

                Next

                If _Buttons.Count = 0 Then
                    Return Nothing
                Else
                    Return _Buttons
                End If

            End If

            Return _Return

        End Function

        Public Shared Function ReturnColour(ByVal AllergyRating As String, ByVal DietRestrictions As String) As Color

            Dim _Allergy As String = AllergyRating.Trim
            If _Allergy = "None" Then _Allergy = ""

            If _Allergy <> "" Then
                If _Allergy = "Serious" Then Return frmBase.txtRed.BackColor
                If _Allergy = "Moderate" Then Return frmBase.txtOrange.BackColor
            Else

                Dim _Diet As String = DietRestrictions.Trim
                If _Diet = "None" Then _Diet = ""

                If _Diet <> "" Then
                    Return frmBase.txtYellow.BackColor
                Else
                    Return Color.Black
                End If

            End If

        End Function

        Public Shared Function BuildAllergyText(ByVal AllergyRating As String, ByVal DietRestriction As String, ByVal Abbreviate As Boolean) As String

            Dim _Allergy As String = AllergyRating.Trim
            If _Allergy = "None" Then _Allergy = ""

            Dim _Diet As String = DietRestriction.Trim
            If _Diet = "None" Then _Diet = ""

            If _Allergy + _Diet = "" Then Return ""

            Dim _Return As String = ""

            If _Diet <> "" Then

                If _Return <> "" Then _Return += ", "

                If Abbreviate Then
                    _Return += "DR: " + _Diet
                Else
                    _Return += "Diet Restriction: " + _Diet
                End If

            End If

            Return _Return

        End Function

    End Class

End Namespace

