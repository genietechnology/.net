﻿Namespace Business

    Public Class Register

        Public Shared Function ReturnCheckedInCount(ByVal PersonType As SharedModule.Enums.PersonType) As Integer

            Dim _RegisterType As String = "C"
            If PersonType = SharedModule.Enums.PersonType.Staff Then _RegisterType = "S"
            If PersonType = SharedModule.Enums.PersonType.Visitor Then _RegisterType = "V"

            Dim _Register = From _R As NurseryGenieData.Register In DataBase.Tables.Register
                            Where _R.RegisterDate = Today And _R.RegisterType = _RegisterType

            If _Register IsNot Nothing Then
                Return _Register.Count()
            Else
                Return 0
            End If

        End Function

        Public Shared Sub RegisterTransaction(ByVal PersonID As String, ByVal PersonType As SharedModule.Enums.PersonType, _
                                              ByVal PersonName As String, ByVal InOut As SharedModule.Enums.InOut, ByVal Via As SharedModule.Enums.RegisterVia)

            RegisterTransaction(PersonID, PersonType, PersonName, InOut, Via, "", "", "", "")

        End Sub

        Public Shared Sub RegisterTransaction(ByVal PersonID As String, ByVal PersonType As SharedModule.Enums.PersonType, _
                                              ByVal PersonName As String, ByVal InOut As SharedModule.Enums.InOut, ByVal Via As SharedModule.Enums.RegisterVia, _
                                              ByVal Location As String, ByVal Ref1 As String, ByVal Ref2 As String, ByVal Ref3 As String)

            Dim _Location As String = ""
            Dim _PersonType As String = ""
            Dim _InOut As String = ""
            Dim _Status As String = ""
            Dim _Time As Date = DateAndTime.Now

            If PersonType = SharedModule.Enums.PersonType.Child Then _PersonType = "C"
            If PersonType = SharedModule.Enums.PersonType.Staff Then _PersonType = "S"
            If PersonType = SharedModule.Enums.PersonType.Visitor Then _PersonType = "V"

            If InOut = SharedModule.Enums.InOut.CheckIn Then _InOut = "I"
            If InOut = SharedModule.Enums.InOut.CheckOut Then _InOut = "O"

            Dim _ID As Guid = Guid.NewGuid

            Dim _R As New NurseryGenieData.Register
            With _R

                .ID = _ID
                .RegisterDate = Date.Today
                .RegisterType = _PersonType
                .InOut = _InOut
                .PersonID = New Guid(PersonID)
                .PersonName = PersonName

                If InOut = SharedModule.Enums.InOut.CheckIn Then
                    .StampIn = _Time
                    .StampOut = Nothing
                Else
                    .StampIn = Nothing
                    .StampOut = _Time
                End If

                .Location = _Location

                .Ref1 = Ref1
                .Ref2 = Ref2
                .Ref3 = SharedModule.Version + " | " + My.Computer.Name + "|" + Via.ToString

            End With

            DataBase.Tables.Register.Add(_R)
            DataBase.PersistRegisterLocally()

        End Sub

    End Class

End Namespace

