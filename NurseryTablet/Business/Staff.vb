﻿Imports NurseryTablet.SharedModule

Namespace Business

    Public Class Staff

        Public Shared Function GetCheckedIn() As List(Of NurseryGenieData.Staff)

            Dim _Staff = From _S As NurseryGenieData.Staff In DataBase.Tables.Staff
                            Order By _S.Name Descending

            If _Staff IsNot Nothing Then

                Dim _Return As New List(Of NurseryGenieData.Staff)

                For Each _S In _Staff
                    If PersonInorOut(_S.ID.ToString) = Enums.InOut.CheckIn Then
                        _Return.Add(_S)
                    End If
                Next

                If _Return.Count > 0 Then
                    Return _Return
                Else
                    Return Nothing
                End If

            Else
                Return Nothing
            End If

        End Function

        Public Shared Function ReturnName(ByVal StaffID As String) As String

        End Function

        Public Shared Function FindMultipleStaff(ByVal Mode As SharedModule.Enums.PersonMode) As List(Of Pair)
            Return SharedModule.ReturnMultipleButtons(GetButtons(Mode), "Select Staff")
        End Function

        Public Shared Function FindStaff(ByVal Mode As SharedModule.Enums.PersonMode) As Pair
            Return SharedModule.ReturnButtons(GetButtons(Mode), "Select Staff")
        End Function

        Private Shared Function GetButtons(ByVal Mode As SharedModule.Enums.PersonMode) As List(Of Pair)

            Dim _Buttons As New List(Of Pair)

            Dim _Q As Object

            If NurseryTablet.Parameters.SortBySurname Then
                _Q = From _S As NurseryGenieData.Staff In DataBase.Tables.Staff Order By _S.SurnameForename
            Else
                _Q = From _S As NurseryGenieData.Staff In DataBase.Tables.Staff Order By _S.Name
            End If

            If _Q IsNot Nothing Then

                For Each _S As NurseryGenieData.Staff In _Q

                    Dim _Name As String = _S.Name
                    If NurseryTablet.Parameters.SortBySurname Then _Name = _S.SurnameForename

                    If Mode = Enums.PersonMode.Everyone Then
                        _Buttons.Add(New Pair(_S.ID.ToString, _Name))
                    Else

                        Dim _Status As Enums.InOut = PersonInorOut(_S.ID.ToString)

                        If Mode = Enums.PersonMode.OnlyCheckedIn Then
                            If _Status = Enums.InOut.CheckIn Then
                                _Buttons.Add(New Pair(_S.ID.ToString, _Name))
                            End If
                        Else
                            If _Status = Enums.InOut.CheckOut Then
                                _Buttons.Add(New Pair(_S.ID.ToString, _Name))
                            End If
                        End If
                    End If

                Next

            End If

            If _Buttons.Count = 0 Then
                Return Nothing
            Else
                Return _Buttons
            End If

        End Function

    End Class

End Namespace
