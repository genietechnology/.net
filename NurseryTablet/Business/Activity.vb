﻿Imports NurseryTablet.SharedModule

Namespace Business

    Public Class Activity

        Public Shared Function ReturnActivity(ByVal ChildID As String, ByVal ActivityType As SharedModule.EnumActivityType) As List(Of NurseryGenieData.Activity)

            Dim _Activity = From _A As NurseryGenieData.Activity In DataBase.Tables.Activity
                            Where _A.DayID = Day.TodayID _
                            And _A.KeyID = New Guid(ChildID) _
                            And _A.KeyType = ReturnActivityTypeCode(ActivityType)
                            Order By _A.Stamp Descending

            If _Activity IsNot Nothing Then
                If _Activity.Count > 0 Then
                    Return _Activity.ToList()
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If

        End Function

        Public Shared Function ReturnActivity(ByVal ActivityType As SharedModule.EnumActivityType) As List(Of NurseryGenieData.Activity)

            Dim _Activity = From _A As NurseryGenieData.Activity In DataBase.Tables.Activity
                            Where _A.DayID = Day.TodayID _
                            And _A.KeyType = ReturnActivityTypeCode(ActivityType)
                            Order By _A.Stamp Descending

            If _Activity IsNot Nothing Then
                If _Activity.Count > 0 Then
                    Return _Activity.ToList()
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If

        End Function

        Public Shared Function ActivityExists(ByVal ChildID As String, ByVal ActivityType As SharedModule.EnumActivityType) As Boolean
            Dim _Activity As List(Of NurseryGenieData.Activity) = ReturnActivity(ChildID, ActivityType)
            If _Activity IsNot Nothing AndAlso _Activity.Count > 0 Then
                Return True
            End If
            Return False
        End Function

        Public Shared Function ReturnActivityItem(ByVal ChildID As String, ByVal ActivityType As SharedModule.EnumActivityType, ByRef ActivityData As NurseryGenieData.Activity) As Boolean

            Dim _Activity = From _A As NurseryGenieData.Activity In DataBase.Tables.Activity
                            Where _A.KeyID = New Guid(ChildID) And _A.KeyType = ReturnActivityTypeCode(ActivityType)

            If _Activity IsNot Nothing AndAlso _Activity.Count > 0 Then
                ActivityData = _Activity.First
                Return True
            Else
                ActivityData = Nothing
                Return False
            End If

        End Function

        Public Shared Function ReturnActivityItem(ByVal ActivityID As String) As NurseryGenieData.Activity

            Dim _Activity = From _A As NurseryGenieData.Activity In DataBase.Tables.Activity
                            Where _A.ID = New Guid(ActivityID)

            If _Activity IsNot Nothing Then
                Return _Activity.First
            Else
                Return Nothing
            End If

        End Function

        Public Shared Sub DeleteActivity(ByVal ActivityID As String)
            Dim _Activity As NurseryGenieData.Activity = ReturnActivityItem(ActivityID)
            If _Activity IsNot Nothing Then
                DataBase.Tables.Activity.Remove(_Activity)
                DataBase.PersistActivityLocally()
            End If
        End Sub

        Public Shared Sub UpdateActivity(ByVal ActivityID As String, Optional ByVal Description As String = "", Optional Stamp As Date? = Nothing, _
                                 Optional ByVal Value1 As String = "", Optional ByVal Value2 As String = "", Optional ByVal Value3 As String = "", _
                                 Optional ByVal StaffID As String = "", Optional ByVal StaffName As String = "", Optional ByVal Notes As String = "")

            For Each _a As NurseryGenieData.Activity In DataBase.Tables.Activity

                If _a.ID = New Guid(ActivityID) Then

                    If Description <> "" Then _a.Description = Description
                    If Stamp.HasValue Then _a.Stamp = Stamp
                    If Value1 <> "" Then _a.Value1 = Value1
                    If Value2 <> "" Then _a.Value2 = Value2
                    If Value3 <> "" Then _a.Value3 = Value3
                    If StaffID <> "" Then _a.StaffID = New Guid(StaffID)
                    If StaffName <> "" Then _a.StaffName = StaffName
                    If Notes <> "" Then _a.Notes = Notes

                    DataBase.PersistActivityLocally()

                    Exit Sub

                End If

            Next

        End Sub

        Public Shared Sub UpdateActivity(ByVal ActivityID As String, ByVal ActivityType As SharedModule.EnumActivityType, _
                                         Optional ByVal Description As String = "", Optional Stamp As Date? = Nothing, _
                                         Optional ByVal Value1 As String = "", Optional ByVal Value2 As String = "", Optional ByVal Value3 As String = "", _
                                         Optional ByVal StaffID As String = "", Optional ByVal StaffName As String = "", Optional ByVal Notes As String = "")

            For Each _a As NurseryGenieData.Activity In DataBase.Tables.Activity

                If _a.ID = New Guid(ActivityID) Then

                    _a.KeyType = ReturnActivityTypeCode(ActivityType)
                    If Description <> "" Then _a.Description = Description
                    If Stamp.HasValue Then _a.Stamp = Stamp
                    If Value1 <> "" Then _a.Value1 = Value1
                    If Value2 <> "" Then _a.Value2 = Value2
                    If Value3 <> "" Then _a.Value3 = Value3
                    If StaffID <> "" Then _a.StaffID = New Guid(StaffID)
                    If StaffName <> "" Then _a.StaffName = StaffName
                    If Notes <> "" Then _a.Notes = Notes

                    DataBase.PersistActivityLocally()

                    Exit Sub

                End If

            Next

        End Sub

        Public Shared Function ReturnActivityTypeCode(ByVal ActivityType As EnumActivityType) As String
            If ActivityType = EnumActivityType.CrossCheck Then Return "CROSSCHECK"
            If ActivityType = EnumActivityType.Feedback Then Return "FEEDBACK"
            If ActivityType = EnumActivityType.Milk Then Return "MILK"
            If ActivityType = EnumActivityType.Request Then Return "REQ"
            If ActivityType = EnumActivityType.Sleep Then Return "SLEEP"
            If ActivityType = EnumActivityType.Suncream Then Return "SUNCREAM"
            If ActivityType = EnumActivityType.Toilet Then Return "TOILET"
            If ActivityType = EnumActivityType.CheckOutMedication Then Return "COMEDICINE"
            If ActivityType = EnumActivityType.CheckOutIncident Then Return "COINCIDENT"
            If ActivityType = EnumActivityType.Absence Then Return "ABSENCE"
            If ActivityType = EnumActivityType.Asleep Then Return "ASLEEP"
            If ActivityType = EnumActivityType.SleepCheck Then Return "SLEEPCHECK"
            If ActivityType = EnumActivityType.Payment Then Return "PAYMENT"
            If ActivityType = EnumActivityType.Unlock Then Return "UNLOCK"
            If ActivityType = EnumActivityType.LogOff Then Return "LOGOFF"
            If ActivityType = EnumActivityType.Behavior Then Return "BEHAVIOR"
            Return "!!ERROR!!"
        End Function
    End Class

End Namespace
