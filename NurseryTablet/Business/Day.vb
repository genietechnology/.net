﻿Imports Microsoft.VisualBasic.Devices
Imports NurseryTablet.NurseryGenieData
Imports NurseryTablet.DataBase

Namespace Business

    Public Class Day

        Private Shared m_DayID As Guid? = Nothing
        Private Shared m_DayRecord As NurseryGenieData.Day = Nothing

        Public Shared ReadOnly Property TodayIDString As String
            Get
                If m_DayID.HasValue Then
                    Return m_DayID.Value.ToString()
                Else
                    Return ""
                End If
            End Get
        End Property

        Public Shared ReadOnly Property TodayID() As Guid?
            Get
                Return m_DayID
            End Get
        End Property

        Public Shared ReadOnly Property Record As NurseryGenieData.Day
            Get
                Return m_DayRecord
            End Get
        End Property

        Public Shared Function Setup() As Boolean

            If DataBase.DayRecord IsNot Nothing Then
                m_DayRecord = DataBase.DayRecord
                m_DayID = m_DayRecord.ID
                Return True
            Else
                Return False
            End If

        End Function

    End Class

End Namespace

