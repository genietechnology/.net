﻿Namespace Business
    Public Class Sleeper

        Public Sub New(ByVal ChildID As String, ByVal ChildName As String)
            Me.ChildID = ChildID
            Me.ChildName = ChildName
            Me.ActivityRecord = Nothing
        End Sub

        Public Sub New(ByVal ChildID As String, ByVal ChildName As String, ByVal ActivityData As NurseryGenieData.Activity)
            Me.ChildID = ChildID
            Me.ChildName = ChildName
            Me.ActivityRecord = ActivityData
        End Sub

        Public Property ChildID As String
        Public Property ChildName As String
        Public Property ActivityRecord As NurseryGenieData.Activity

    End Class

End Namespace
