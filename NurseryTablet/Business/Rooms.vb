﻿Imports NurseryTablet.SharedModule

Namespace Business

    Public Class Rooms

        Public Shared Function GetRooms() As List(Of NurseryGenieData.SiteRoom)
            If DataBase.Tables.SiteRooms IsNot Nothing AndAlso DataBase.Tables.SiteRooms.Count > 0 Then
                Return DataBase.Tables.SiteRooms.ToList
            Else
                Return Nothing
            End If
        End Function

        Public Shared Function GetRoomsAsPair() As List(Of Pair)
            Dim _Rooms As List(Of NurseryGenieData.SiteRoom) = GetRooms()
            If _Rooms IsNot Nothing Then
                Dim _List As New List(Of Pair)
                For Each _r As NurseryGenieData.SiteRoom In _Rooms
                    _List.Add(New Pair(_r.ID.ToString, _r.Name))
                Next
                Return _List
            Else
                Return Nothing
            End If
        End Function


    End Class

End Namespace

