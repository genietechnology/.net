﻿Imports NurseryTablet.SharedModule

Namespace Business

    Public Class Food

        Public Enum EnumMode
            Normal
            BabyFood
            OffMenu
        End Enum

        Public Class FoodItemStatus
            Public Property FoodID As Guid
            Public Property FoodName As String
            Public Property Status As String
        End Class

        Public Shared Function GetMealComponents(ByVal MealID As String) As List(Of NurseryGenieData.Food)

            Dim _Q = From _I As NurseryGenieData.Food In DataBase.Tables.Food _
                     Where _I.MealID = New Guid(MealID) _
                     Order By _I.FoodName

            If _Q IsNot Nothing Then
                Return _Q.ToList
            Else
                Return Nothing
            End If

        End Function

        Public Shared Function FoodItemsExist(ByVal ChildID As String, ByVal MealID As String, ByVal MealType As String) As Boolean
            Dim _Items As List(Of FoodItemStatus) = GetFoodRegister(ChildID, MealID, MealType)
            If _Items IsNot Nothing AndAlso _Items.Count > 0 Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function GetFoodRegister(ByVal ChildID As String, ByVal MealID As String, ByVal MealCode As String) As List(Of FoodItemStatus)

            '_SQL += "select food_id, food_name, food_status from FoodRegister"
            '_SQL += " where day_id = '" & TodayID & "'"
            '_SQL += " and child_id = '" & SelectedChild.ID & "'"
            '_SQL += " and meal_id = '" & m_SelectedMealID & "'"
            '_SQL += " and meal_type = '" & m_SelectedMealType & "'"
            '_SQL += " and meal_id <> food_id"
            '_SQL += " order by food_name desc"

            Dim _MealType As EnumMealType = ReturnMealTypeEnum(MealCode)
            Dim _Return As New List(Of FoodItemStatus)
            Dim _FoodItems As Object

            Select Case _MealType

                Case EnumMealType.Breakfast
                    _FoodItems = From _I As NurseryGenieData.Food In DataBase.Tables.Food
                                 Where _I.MealID = New Guid(MealID) And _I.Breakfast = True Order By _I.FoodName

                Case EnumMealType.Snack
                    _FoodItems = From _I As NurseryGenieData.Food In DataBase.Tables.Food
                                 Where _I.MealID = New Guid(MealID) And _I.Snack = True Order By _I.FoodName

                Case EnumMealType.Lunch
                    _FoodItems = From _I As NurseryGenieData.Food In DataBase.Tables.Food
                                 Where _I.MealID = New Guid(MealID) And _I.Lunch = True Order By _I.FoodName

                Case EnumMealType.LunchDessert, EnumMealType.TeaDessert
                    _FoodItems = From _I As NurseryGenieData.Food In DataBase.Tables.Food
                                 Where _I.MealID = New Guid(MealID) And _I.Dessert = True Order By _I.FoodName

                Case EnumMealType.Tea
                    _FoodItems = From _I As NurseryGenieData.Food In DataBase.Tables.Food
                                 Where _I.MealID = New Guid(MealID) And _I.Tea = True Order By _I.FoodName

            End Select

            If _FoodItems IsNot Nothing Then
                For Each _F As NurseryGenieData.Food In _FoodItems

                    Dim _FI As New FoodItemStatus
                    _FI.FoodID = _F.FoodID
                    _FI.FoodName = _F.FoodName
                    _FI.Status = ReturnStatus(ChildID, _MealType, _FI.FoodID.ToString)

                    _Return.Add(_FI)

                Next
            End If

            Return _Return

        End Function

        Public Shared Sub ClearFoodItems(ByVal ChildID As String, ByVal MealID As String)

            '     _SQL = "DELETE from FoodRegister" & _
            '" where day_id = '" & TodayID & "'" & _
            '" and child_id = '" & txtChild.Tag.ToString & "'" & _
            '" and meal_id = '" & _BTN.Tag.ToString & "'"

        End Sub

        Public Shared Function ReturnStatus(ByVal ChildID As String, MealType As EnumMealType, ByVal FoodID As String) As String

            Dim _MealCode As String = ReturnMealCode(MealType)
            Dim _Return As Integer = -1

            Dim _Q = From _R As NurseryGenieData.FoodRegister In DataBase.Tables.FoodRegister
                     Where _R.ChildID = New Guid(ChildID) And _R.MealType = _MealCode And _R.FoodID = New Guid(FoodID)

            If _Q IsNot Nothing Then
                If _Q.Count = 1 Then
                    _Return = _Q.First.FoodStatus
                End If
            End If

            Return _Return

        End Function

        Public Shared Function ReturnMealCode(ByVal MealName As String) As String

            'baby food
            If MealName = "J" Then Return "J"

            If MealName = "Breakfast" Then Return "B"
            If MealName = "Snack" Then Return "S"
            If MealName = "Lunch" Then Return "L"
            If MealName = "Lunch Dessert" Then Return "LD"
            If MealName = "Tea" Then Return "T"
            If MealName = "Tea Dessert" Then Return "TD"

            Return "X"

        End Function

        Public Shared Function ReturnMealCode(ByVal MealType As EnumMealType) As String

            'baby food
            If MealType = EnumMealType.BabyFood Then Return "J"

            If MealType = EnumMealType.Breakfast Then Return "B"
            If MealType = EnumMealType.Snack Then Return "S"
            If MealType = EnumMealType.Lunch Then Return "L"
            If MealType = EnumMealType.LunchDessert Then Return "LD"
            If MealType = EnumMealType.Tea Then Return "T"
            If MealType = EnumMealType.TeaDessert Then Return "TD"

            Return Nothing

        End Function

        Public Shared Function ReturnMealTypeEnum(ByVal MealCode As String) As EnumMealType

            'baby food
            If MealCode = "J" Then Return EnumMealType.BabyFood

            If MealCode = "B" Then Return EnumMealType.Breakfast
            If MealCode = "S" Then Return EnumMealType.Snack
            If MealCode = "L" Then Return EnumMealType.Lunch
            If MealCode = "LD" Then Return EnumMealType.LunchDessert
            If MealCode = "T" Then Return EnumMealType.Tea
            If MealCode = "TD" Then Return EnumMealType.TeaDessert

            Return Nothing

        End Function

    End Class

End Namespace

