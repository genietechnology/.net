﻿Option Strict On

Namespace Business

    Public Class Lists

        Public Shared Function GetList(ByVal ListName As String) As List(Of NurseryGenieData.AppList)

            Dim _Items = From _I As NurseryGenieData.AppList In DataBase.Tables.Lists
                         Where _I.ListName = ListName
                         Order By _I.Seq Descending

            If _Items IsNot Nothing Then
                If _Items.Count > 0 Then
                    Return _Items.ToList
                End If
            End If

            Return Nothing

        End Function

        Public Shared Function GetListAsPairs(ByVal ListName As String) As List(Of SharedModule.Pair)

            Dim _Pairs As List(Of SharedModule.Pair) = Nothing

            Dim _Items = From _I As NurseryGenieData.AppList In DataBase.Tables.Lists
                         Where _I.ListName = ListName
                         Order By _I.Seq

            If _Items IsNot Nothing AndAlso _Items.Count > 0 Then

                _Pairs = New List(Of SharedModule.Pair)

                For Each _i In _Items
                    _Pairs.Add(New SharedModule.Pair(_i.ID.ToString, _i.Name))
                Next

            End If

            Return _Pairs

        End Function


    End Class

End Namespace

