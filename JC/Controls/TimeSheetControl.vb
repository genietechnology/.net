﻿Imports Care.Global
Imports Care.Shared

Public Class TimeSheetControl

    Private m_DayOfWeek As DayOfWeek
    Private m_Index As Integer

    Private m_VanID As Guid? = Nothing
    Private m_JobID As Guid? = Nothing

    Private m_TSStart As TimeSpan? = Nothing
    Private m_TSFinish As TimeSpan? = Nothing
    Private m_TSYard As TimeSpan? = Nothing

    Public Class SelectedItemArgs

        Inherits EventArgs

        Public Sub New(SelectedIndex As Integer)
            Me.SelectedIndex = SelectedIndex
        End Sub

        Property SelectedIndex As Integer

    End Class

    Public Event ItemSelected(ByVal sender As Object, e As SelectedItemArgs)

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        lblVan.Text = ""
        lblMatch.Text = ""
        lblJob.Text = ""
        lblItemIndex.Text = ""

        cbxDay.AddItem("Monday")
        cbxDay.AddItem("Tuesday")
        cbxDay.AddItem("Wednesday")
        cbxDay.AddItem("Thursday")
        cbxDay.AddItem("Friday")
        cbxDay.AddItem("Saturday")
        cbxDay.AddItem("Sunday")

    End Sub

#Region "Properties"

    Property HighlightSelected As Boolean

    Public ReadOnly Property TSStart As TimeSpan?
        Get
            Return m_TSStart
        End Get
    End Property

    Public ReadOnly Property TSFinish As TimeSpan?
        Get
            Return m_TSFinish
        End Get
    End Property

    Public ReadOnly Property TSYard As TimeSpan?
        Get
            Return m_TSYard
        End Get
    End Property

    Public ReadOnly Property JobID As Guid?
        Get
            Return m_JobID
        End Get
    End Property

    Public ReadOnly Property VanID As Guid?
        Get
            Return m_VanID
        End Get
    End Property

    Property ItemIndex As Integer
        Get
            Return m_Index
        End Get
        Set(value As Integer)
            m_Index = value
            lblItemIndex.Text = value.ToString
        End Set
    End Property

    Property VanNo As String
        Get
            Return txtVan.Text
        End Get
        Set(value As String)
            txtVan.Text = value
            GetVanDesc()
        End Set
    End Property

    Property DayOfWeek As DayOfWeek
        Get
            Return m_DayOfWeek
        End Get
        Set(value As DayOfWeek)
            m_DayOfWeek = value
            cbxDay.Text = value.ToString
        End Set
    End Property

    Property JobNo As String
        Get
            Return txtJob.Text
        End Get
        Set(value As String)
            txtJob.Text = value
            GetJobDesc()
        End Set
    End Property

    Property What As String
        Get
            Return cbxWhat.Text
        End Get
        Set(value As String)
            cbxWhat.Text = value
        End Set
    End Property

    Property StartTime As String
        Get
            Return txtStart.Text
        End Get
        Set(value As String)
            txtStart.Text = value
            m_TSStart = ReturnTS(txtStart.Text)
        End Set
    End Property

    Property FinishTime As String
        Get
            Return txtFinish.Text
        End Get
        Set(value As String)
            txtFinish.Text = value
            m_TSFinish = ReturnTS(txtFinish.Text)
        End Set
    End Property

    Property YardTime As String
        Get
            Return txtYard.Text
        End Get
        Set(value As String)
            txtYard.Text = value
            m_TSYard = ReturnTS(txtYard.Text)
        End Set
    End Property

#End Region

#Region "Public Methods"

    Public Sub PopulateTrades()
        cbxWhat.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("TRADES"))
    End Sub

    Public Sub SetChangeColour()
        txtVan.BackColor = Session.ChangeColour
        cbxDay.BackColor = Session.ChangeColour
        txtJob.BackColor = Session.ChangeColour
        cbxWhat.BackColor = Session.ChangeColour
        txtStart.BackColor = Session.ChangeColour
        txtFinish.BackColor = Session.ChangeColour
        txtYard.BackColor = Session.ChangeColour
    End Sub

    Public Sub CopyNextDay()
        m_DayOfWeek = CType(m_DayOfWeek + 1, DayOfWeek)
        cbxDay.Text = m_DayOfWeek.ToString
    End Sub

    Public Sub NextDay()

        m_DayOfWeek = CType(m_DayOfWeek + 1, DayOfWeek)
        cbxDay.Text = m_DayOfWeek.ToString

        txtJob.Text = ""
        lblJob.Text = ""

        txtStart.Text = ""
        txtFinish.Text = ""
        txtYard.Text = ""

        txtJob.Focus()

    End Sub

    Public Sub Clear()

        m_Index = 0
        m_DayOfWeek = DayOfWeek.Monday

        txtVan.Text = ""
        lblVan.Text = ""
        m_VanID = Nothing

        cbxDay.Text = "Monday"
        cbxWhat.Text = ""

        txtJob.Text = ""
        lblJob.Text = ""
        m_JobID = Nothing

        txtStart.Text = ""
        txtFinish.Text = ""
        txtYard.Text = ""

    End Sub

    Public Sub SameDay()
        txtJob.Text = ""
        lblJob.Text = ""
        txtStart.Text = ""
        txtFinish.Text = ""
        txtYard.Text = ""
        txtJob.Focus()
    End Sub

    Public Function ValidateEntry() As Boolean

        'blank fields
        If txtVan.Text = "" Then Return False
        If cbxDay.Text = "" Then Return False
        If txtJob.Text = "" Then Return False
        If cbxWhat.Text = "" Then Return False
        If txtStart.Text = "" Then Return False
        If txtFinish.Text = "" Then Return False

        If lblVan.Text = "" Then Return False
        If lblJob.Text = "" Then Return False

        If Not m_TSStart.HasValue Then Return False
        If Not m_TSFinish.HasValue Then Return False

        If m_TSStart.Value > m_TSFinish.Value Then
            Return False
        End If

        Return True

    End Function

#End Region

#Region "Control Events"

    Private Sub txtVan_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtVan.Validating
        GetVanDesc()
    End Sub

    Private Sub txtJob_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtJob.Validating
        GetJobDesc()
    End Sub

    Private Sub cbxDay_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxDay.SelectedIndexChanged
        If cbxDay.SelectedIndex = 0 Then m_DayOfWeek = DayOfWeek.Monday
        If cbxDay.SelectedIndex = 1 Then m_DayOfWeek = DayOfWeek.Tuesday
        If cbxDay.SelectedIndex = 2 Then m_DayOfWeek = DayOfWeek.Wednesday
        If cbxDay.SelectedIndex = 3 Then m_DayOfWeek = DayOfWeek.Thursday
        If cbxDay.SelectedIndex = 4 Then m_DayOfWeek = DayOfWeek.Friday
        If cbxDay.SelectedIndex = 5 Then m_DayOfWeek = DayOfWeek.Saturday
        If cbxDay.SelectedIndex = 6 Then m_DayOfWeek = DayOfWeek.Sunday
    End Sub

    Private Sub TimeSheet_Leave(sender As Object, e As EventArgs) Handles Me.Leave
        If HighlightSelected Then Me.ResetBackColor()
    End Sub

    Private Sub TimeSheet_Enter(sender As Object, e As EventArgs) Handles Me.Enter
        If HighlightSelected Then Me.BackColor = Drawing.Color.LightGray
        RaiseEvent ItemSelected(Me, New SelectedItemArgs(m_Index))
    End Sub

    Private Sub txtStart_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtStart.Validating
        m_TSStart = ReturnTS(txtStart.Text)
    End Sub

    Private Sub txtFinish_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtFinish.Validating
        m_TSFinish = ReturnTS(txtFinish.Text)
    End Sub

    Private Sub txtYard_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtYard.Validating
        m_TSYard = ReturnTS(txtYard.Text)
    End Sub

#End Region

    Private Sub GetJobDesc()
        If txtJob.Text <> "" Then
            Dim _J As Business.Job = Business.Job.RetreiveByJobNo(txtJob.Text)
            If _J IsNot Nothing Then
                m_JobID = _J._ID
                lblJob.Text = _J._RskFullname + ", " + _J._RskAddress1
            Else
                lblJob.Text = ""
            End If
        Else
            lblJob.Text = ""
        End If
    End Sub

    Private Sub GetVanDesc()
        If txtVan.Text <> "" Then
            If txtVan.Text = "9999" Then
                lblVan.Text = "N/A"
            Else
                Dim _P As Business.Plant = Business.Plant.RetreiveByNo(txtVan.Text)
                If _P IsNot Nothing Then
                    m_VanID = _P._ID
                    lblVan.Text = _P._Name
                Else
                    lblVan.Text = "VAN NOT FOUND!"
                End If
            End If
        Else
            lblVan.Text = ""
        End If
    End Sub

    Private Function ReturnTS(ByVal StringIn As String) As TimeSpan?

        If StringIn = "" Then Return Nothing

        Try
            Dim _DT As DateTime? = DateTime.Parse(StringIn)
            If _DT.HasValue Then
                Return _DT.Value.TimeOfDay
            Else
                Return Nothing
            End If

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

End Class
