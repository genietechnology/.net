﻿Imports Care.Global
Imports Care.Data
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraEditors.Calendar
Imports System.ComponentModel
Imports DevExpress.Utils

<DefaultEvent("DoubleClick")>
Public Class YearView

    Public Class YearViewDoubleClickArgs
        Inherits EventArgs
        Property HitDate As Date?
    End Class

    Public Shadows Event DoubleClick(sender As Object, e As YearViewDoubleClickArgs)

    Public Enum EnumMode
        LabourDensity
    End Enum

    Private Enum EnumColour
        Green
        Yellow
        Orange
        Red
        Grey
    End Enum

    Private m_IsRuntime As Boolean = False
    Private m_IsPopulated As Boolean = False
    Private m_DateFrom As Date = Nothing
    Private m_Mode As EnumMode = EnumMode.LabourDensity

    Private m_LabourDensity As New List(Of LabourDensity)

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        gbxLegend.Hide()

    End Sub

#Region "Properties"

    <Category("_Appearance")>
    Public Property CalendarFont As Font
        Get
            Return MyDateNav.Font
        End Get
        Set(value As Font)
            MyDateNav.Font = value
        End Set
    End Property

#End Region

    Public Sub Clear()
        MyStorage.Appointments.Clear()
        MyDateNav.Refresh()
    End Sub

    Private Sub DisplayLegend()

        'gbxLegend.Hide()

        Select Case m_Mode

            Case EnumMode.LabourDensity

                'Case EnumMode.Absence
                '    lblRed.Text = "Sickness"
                '    lblYellow.Text = "Holidays"
                '    lblGreen.Text = "Maternity"
                '    lblBlue.Text = "Other"

                '    gbxLegend.Show()

        End Select

    End Sub

    Public Sub Populate(ByVal JobID As Guid)

        m_Mode = EnumMode.LabourDensity
        m_LabourDensity.Clear()

        Dim _SQL As String = ""

        _SQL += "select ts_date, count(*) as 'count' from Timesheets"
        _SQL += " where ts_job_id = '" + JobID.ToString + "'"
        _SQL += " group by ts_date"
        _SQL += " order by ts_date desc"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows
                Dim _LD As New LabourDensity
                _LD.CalendarDate = ValueHandler.ConvertDate(_DR.Item("ts_date"))
                _LD.Headcount = ValueHandler.ConvertInteger(_DR.Item("count"))
                m_LabourDensity.Add(_LD)
            Next

            MyDateNav.SchedulerControl.Start = DateSerial(2017, 3, 1)

            MyStorage.Appointments.Mappings.End = "CalendarDate"
            MyStorage.Appointments.Mappings.Start = "CalendarDate"

            MyStorage.Appointments.DataSource = m_LabourDensity
            MyDateNav.Refresh()

            m_IsPopulated = True

            DisplayLegend()

        End If

    End Sub

    Public Sub Print()
        Dim link As New PrintableComponentLink(New PrintingSystem(CType(MyDateNav, IContainer)))
        link.Landscape = True
        link.PrintingSystem.Document.AutoFitToPagesWidth = 1
        link.ShowPreviewDialog()
    End Sub

    Private Sub MyDateNav_CustomDrawDayNumberCell(sender As Object, e As DevExpress.XtraEditors.Calendar.CustomDrawDayNumberCellEventArgs) Handles MyDateNav.CustomDrawDayNumberCell

        If Me.DesignMode Then Exit Sub
        If Not m_IsPopulated Then Exit Sub

        Try

            Select Case m_Mode

                Case EnumMode.LabourDensity

                    Dim _LabourDensity As LabourDensity = ReturnLabourDensity(e.Date)
                    If _LabourDensity IsNot Nothing Then

                        Dim _Colour As EnumColour = EnumColour.Grey

                        If _LabourDensity.Headcount > 0 Then

                            If _LabourDensity.Headcount < 2 Then _Colour = EnumColour.Green
                            If _LabourDensity.Headcount > 2 AndAlso _LabourDensity.Headcount <= 4 Then _Colour = EnumColour.Yellow
                            If _LabourDensity.Headcount > 4 AndAlso _LabourDensity.Headcount <= 6 Then _Colour = EnumColour.Orange
                            If _LabourDensity.Headcount > 6 Then _Colour = EnumColour.Red

                            e.Graphics.FillRectangle(ReturnBrush(_Colour), e.Bounds)

                        Else

                        End If

                    End If

            End Select

        Catch ex As Exception

        End Try

    End Sub

    Private Function ReturnBrush(ByVal Colour As EnumColour) As SolidBrush
        If Colour = EnumColour.Yellow Then Return New SolidBrush(txtYellow.BackColor)
        If Colour = EnumColour.Orange Then Return New SolidBrush(txtOrange.BackColor)
        If Colour = EnumColour.Red Then Return New SolidBrush(txtRed.BackColor)
        Return New SolidBrush(txtGreen.BackColor)
    End Function

    Private Function ReturnLabourDensity(ByVal DateIn As Date) As LabourDensity

        Dim _Q As IEnumerable(Of LabourDensity) = From _LD As LabourDensity In m_LabourDensity Where _LD.CalendarDate = DateIn
        If _Q IsNot Nothing Then
            If _Q.Count = 0 Then
                Return Nothing
            Else
                Return _Q.First
            End If
        End If

        Return Nothing

    End Function

    Private Sub MyDateNav_DoubleClick(sender As Object, e As EventArgs) Handles MyDateNav.DoubleClick

        Dim _a As MouseEventArgs = CType(e, MouseEventArgs)
        Dim _h As CalendarHitInfo = MyDateNav.GetHitInfo(_a)

        If _h.HitTest = CalendarHitInfoType.MonthNumber Then

            Dim _e As New YearViewDoubleClickArgs
            _e.HitDate = _h.HitDate

            RaiseEvent DoubleClick(sender, _e)

        End If

    End Sub

    Private Sub MyDateNav_MouseMove(sender As Object, e As MouseEventArgs) Handles MyDateNav.MouseMove

        Dim _chi As CalendarHitInfo = MyDateNav.GetHitInfo(e)
        If _chi.HitObject IsNot Nothing Then

            Dim _stt As SuperToolTip = GetToolTipInfo(_chi)
            If _stt IsNot Nothing Then

                Dim _ttci As New ToolTipControlInfo()

                _ttci.ToolTipType = ToolTipType.SuperTip
                _ttci.IconType = ToolTipIconType.Information
                _ttci.SuperTip = _stt
                _ttci.ToolTipPosition = MousePosition
                _ttci.Interval = 500
                _ttci.Object = _chi.HitDate

                ToolTipController1.ShowHint(_ttci)

            End If

        End If

    End Sub

    Private Function GetToolTipInfo(ByVal chi As CalendarHitInfo) As SuperToolTip

        Select Case m_Mode

            Case EnumMode.LabourDensity

                Dim _Title As String = ""
                Dim _Tip As String = ""

                If GetTooltip(chi.HitDate, _Title, _Tip) Then

                    Dim _stt As New SuperToolTip()

                    Dim _TitleItem As New ToolTipTitleItem()
                    _TitleItem.Text = _Title

                    Dim _TipItem As New ToolTipItem()
                    _TipItem.Text = _Tip

                    _stt.Items.Add(_TitleItem)
                    _stt.Items.Add(_TipItem)

                    Return _stt

                End If

            Case Else
                Return Nothing

        End Select

        Return Nothing

    End Function

    Private Function GetTooltip(ByVal HitDate As Date, ByRef Title As String, ByRef Tip As String) As Boolean

        Dim _SQL As String = ""


        Return False
    End Function

    Private Sub MyDateNav_MouseWheel(sender As Object, e As MouseEventArgs) Handles MyDateNav.MouseWheel

        Me.Cursor = Cursors.WaitCursor
        MyDateNav.BeginUpdate()

        Dim _CurrentDate As Date = MyDateNav.GetStartDate

        If e.Delta > 0 Then
            'scroll up
            MyDateNav.DateTime = _CurrentDate.AddMonths(-1)
        Else
            'scroll down
            MyDateNav.DateTime = MyDateNav.GetEndDate.AddMonths(1)
            MyDateNav.DateTime = _CurrentDate.AddMonths(1)
        End If

        Me.Cursor = Cursors.Default
        MyDateNav.EndUpdate()

    End Sub

    Private Sub MyDateNav_MouseLeave(sender As Object, e As EventArgs) Handles MyDateNav.MouseLeave
        ToolTipController1.HideHint()
    End Sub

    Private Class LabourDensity
        Property CalendarDate As Date?
        Property Headcount As Integer
    End Class

End Class
