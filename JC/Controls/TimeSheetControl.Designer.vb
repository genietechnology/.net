﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TimeSheetControl
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblItemIndex = New Care.Controls.CareLabel(Me.components)
        Me.lblMatch = New Care.Controls.CareLabel(Me.components)
        Me.lblJob = New Care.Controls.CareLabel(Me.components)
        Me.lblVan = New Care.Controls.CareLabel(Me.components)
        Me.txtYard = New Care.Controls.CareTextBox(Me.components)
        Me.txtFinish = New Care.Controls.CareTextBox(Me.components)
        Me.txtStart = New Care.Controls.CareTextBox(Me.components)
        Me.txtJob = New Care.Controls.CareTextBox(Me.components)
        Me.cbxDay = New Care.Controls.CareComboBox(Me.components)
        Me.cbxWhat = New Care.Controls.CareComboBox(Me.components)
        Me.txtVan = New Care.Controls.CareTextBox(Me.components)
        CType(Me.txtYard.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFinish.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJob.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxDay.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxWhat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtVan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblItemIndex
        '
        Me.lblItemIndex.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblItemIndex.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblItemIndex.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblItemIndex.Location = New System.Drawing.Point(590, 31)
        Me.lblItemIndex.Name = "lblItemIndex"
        Me.lblItemIndex.Size = New System.Drawing.Size(19, 15)
        Me.lblItemIndex.TabIndex = 10
        Me.lblItemIndex.Text = "<i>"
        Me.lblItemIndex.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblItemIndex.Visible = False
        '
        'lblMatch
        '
        Me.lblMatch.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMatch.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblMatch.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblMatch.Location = New System.Drawing.Point(189, 31)
        Me.lblMatch.Name = "lblMatch"
        Me.lblMatch.Size = New System.Drawing.Size(51, 15)
        Me.lblMatch.TabIndex = 8
        Me.lblMatch.Text = "<Match>"
        Me.lblMatch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJob
        '
        Me.lblJob.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblJob.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblJob.Location = New System.Drawing.Point(252, 31)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(76, 15)
        Me.lblJob.TabIndex = 9
        Me.lblJob.Text = "<Job Details>"
        Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblVan
        '
        Me.lblVan.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVan.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblVan.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblVan.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblVan.Location = New System.Drawing.Point(3, 31)
        Me.lblVan.Name = "lblVan"
        Me.lblVan.Size = New System.Drawing.Size(180, 16)
        Me.lblVan.TabIndex = 7
        Me.lblVan.Text = "<Van Details>"
        Me.lblVan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtYard
        '
        Me.txtYard.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtYard.EnterMoveNextControl = True
        Me.txtYard.Location = New System.Drawing.Point(590, 3)
        Me.txtYard.MaxLength = 5
        Me.txtYard.Name = "txtYard"
        Me.txtYard.NumericAllowNegatives = False
        Me.txtYard.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.Time
        Me.txtYard.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtYard.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtYard.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtYard.Properties.Appearance.Options.UseFont = True
        Me.txtYard.Properties.Appearance.Options.UseTextOptions = True
        Me.txtYard.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtYard.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic
        Me.txtYard.Properties.Mask.EditMask = "(0?\d|1\d|2[0-3])\:[0-5]\d"
        Me.txtYard.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtYard.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtYard.Properties.MaxLength = 5
        Me.txtYard.Size = New System.Drawing.Size(57, 22)
        Me.txtYard.TabIndex = 6
        Me.txtYard.Tag = "R"
        Me.txtYard.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtYard.ToolTipText = ""
        '
        'txtFinish
        '
        Me.txtFinish.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtFinish.EnterMoveNextControl = True
        Me.txtFinish.Location = New System.Drawing.Point(527, 3)
        Me.txtFinish.MaxLength = 5
        Me.txtFinish.Name = "txtFinish"
        Me.txtFinish.NumericAllowNegatives = False
        Me.txtFinish.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.Time
        Me.txtFinish.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFinish.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFinish.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtFinish.Properties.Appearance.Options.UseFont = True
        Me.txtFinish.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFinish.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFinish.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic
        Me.txtFinish.Properties.Mask.EditMask = "(0?\d|1\d|2[0-3])\:[0-5]\d"
        Me.txtFinish.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtFinish.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFinish.Properties.MaxLength = 5
        Me.txtFinish.Size = New System.Drawing.Size(57, 22)
        Me.txtFinish.TabIndex = 5
        Me.txtFinish.Tag = "R"
        Me.txtFinish.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtFinish.ToolTipText = ""
        '
        'txtStart
        '
        Me.txtStart.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtStart.EnterMoveNextControl = True
        Me.txtStart.Location = New System.Drawing.Point(464, 3)
        Me.txtStart.MaxLength = 5
        Me.txtStart.Name = "txtStart"
        Me.txtStart.NumericAllowNegatives = False
        Me.txtStart.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.Time
        Me.txtStart.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStart.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtStart.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtStart.Properties.Appearance.Options.UseFont = True
        Me.txtStart.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStart.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtStart.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic
        Me.txtStart.Properties.Mask.EditMask = "(0?\d|1\d|2[0-3])\:[0-5]\d"
        Me.txtStart.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtStart.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtStart.Properties.MaxLength = 5
        Me.txtStart.Size = New System.Drawing.Size(57, 22)
        Me.txtStart.TabIndex = 4
        Me.txtStart.Tag = "R"
        Me.txtStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtStart.ToolTipText = ""
        '
        'txtJob
        '
        Me.txtJob.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtJob.EnterMoveNextControl = True
        Me.txtJob.Location = New System.Drawing.Point(189, 3)
        Me.txtJob.MaxLength = 5
        Me.txtJob.Name = "txtJob"
        Me.txtJob.NumericAllowNegatives = False
        Me.txtJob.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtJob.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtJob.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtJob.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtJob.Properties.Appearance.Options.UseFont = True
        Me.txtJob.Properties.Appearance.Options.UseTextOptions = True
        Me.txtJob.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtJob.Properties.Mask.EditMask = "##########;"
        Me.txtJob.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtJob.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtJob.Properties.MaxLength = 5
        Me.txtJob.Size = New System.Drawing.Size(57, 22)
        Me.txtJob.TabIndex = 2
        Me.txtJob.Tag = "R"
        Me.txtJob.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtJob.ToolTipText = ""
        '
        'cbxDay
        '
        Me.cbxDay.AllowBlank = False
        Me.cbxDay.DataSource = Nothing
        Me.cbxDay.DisplayMember = Nothing
        Me.cbxDay.EnterMoveNextControl = True
        Me.cbxDay.Location = New System.Drawing.Point(66, 3)
        Me.cbxDay.Name = "cbxDay"
        Me.cbxDay.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxDay.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxDay.Properties.Appearance.Options.UseFont = True
        Me.cbxDay.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxDay.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxDay.SelectedValue = Nothing
        Me.cbxDay.Size = New System.Drawing.Size(117, 22)
        Me.cbxDay.TabIndex = 1
        Me.cbxDay.Tag = "AE"
        Me.cbxDay.ValueMember = Nothing
        '
        'cbxWhat
        '
        Me.cbxWhat.AllowBlank = False
        Me.cbxWhat.DataSource = Nothing
        Me.cbxWhat.DisplayMember = Nothing
        Me.cbxWhat.EnterMoveNextControl = True
        Me.cbxWhat.Location = New System.Drawing.Point(252, 3)
        Me.cbxWhat.Name = "cbxWhat"
        Me.cbxWhat.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxWhat.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxWhat.Properties.Appearance.Options.UseFont = True
        Me.cbxWhat.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxWhat.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxWhat.SelectedValue = Nothing
        Me.cbxWhat.Size = New System.Drawing.Size(206, 22)
        Me.cbxWhat.TabIndex = 3
        Me.cbxWhat.Tag = "AE"
        Me.cbxWhat.ValueMember = Nothing
        '
        'txtVan
        '
        Me.txtVan.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtVan.EnterMoveNextControl = True
        Me.txtVan.Location = New System.Drawing.Point(3, 3)
        Me.txtVan.MaxLength = 4
        Me.txtVan.Name = "txtVan"
        Me.txtVan.NumericAllowNegatives = False
        Me.txtVan.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtVan.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtVan.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtVan.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtVan.Properties.Appearance.Options.UseFont = True
        Me.txtVan.Properties.Appearance.Options.UseTextOptions = True
        Me.txtVan.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtVan.Properties.Mask.EditMask = "##########;"
        Me.txtVan.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtVan.Properties.Mask.SaveLiteral = False
        Me.txtVan.Properties.Mask.ShowPlaceHolders = False
        Me.txtVan.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtVan.Properties.MaxLength = 4
        Me.txtVan.Size = New System.Drawing.Size(57, 22)
        Me.txtVan.TabIndex = 0
        Me.txtVan.Tag = "R"
        Me.txtVan.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtVan.ToolTipText = ""
        '
        'TimeSheetControl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.lblItemIndex)
        Me.Controls.Add(Me.lblMatch)
        Me.Controls.Add(Me.lblJob)
        Me.Controls.Add(Me.lblVan)
        Me.Controls.Add(Me.txtYard)
        Me.Controls.Add(Me.txtFinish)
        Me.Controls.Add(Me.txtStart)
        Me.Controls.Add(Me.txtJob)
        Me.Controls.Add(Me.cbxDay)
        Me.Controls.Add(Me.cbxWhat)
        Me.Controls.Add(Me.txtVan)
        Me.MaximumSize = New System.Drawing.Size(650, 50)
        Me.MinimumSize = New System.Drawing.Size(650, 50)
        Me.Name = "TimeSheetControl"
        Me.Size = New System.Drawing.Size(650, 50)
        CType(Me.txtYard.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFinish.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJob.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxDay.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxWhat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtVan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cbxWhat As Care.Controls.CareComboBox
    Friend WithEvents txtVan As Care.Controls.CareTextBox
    Friend WithEvents cbxDay As Care.Controls.CareComboBox
    Friend WithEvents txtJob As Care.Controls.CareTextBox
    Friend WithEvents txtStart As Care.Controls.CareTextBox
    Friend WithEvents txtFinish As Care.Controls.CareTextBox
    Friend WithEvents txtYard As Care.Controls.CareTextBox
    Friend WithEvents lblVan As Care.Controls.CareLabel
    Friend WithEvents lblJob As Care.Controls.CareLabel
    Friend WithEvents lblMatch As Care.Controls.CareLabel
    Friend WithEvents lblItemIndex As Care.Controls.CareLabel
End Class
