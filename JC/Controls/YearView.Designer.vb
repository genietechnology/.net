﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class YearView
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim TimeRuler1 As DevExpress.XtraScheduler.TimeRuler = New DevExpress.XtraScheduler.TimeRuler()
        Dim TimeRuler2 As DevExpress.XtraScheduler.TimeRuler = New DevExpress.XtraScheduler.TimeRuler()
        Me.txtYellow = New DevExpress.XtraEditors.TextEdit()
        Me.txtRed = New DevExpress.XtraEditors.TextEdit()
        Me.txtGreen = New DevExpress.XtraEditors.TextEdit()
        Me.txtOrange = New DevExpress.XtraEditors.TextEdit()
        Me.MyScheduler = New DevExpress.XtraScheduler.SchedulerControl()
        Me.MyStorage = New DevExpress.XtraScheduler.SchedulerStorage(Me.components)
        Me.ToolTipController1 = New DevExpress.Utils.ToolTipController(Me.components)
        Me.gbxLegend = New Care.Controls.CareFrame(Me.components)
        Me.lblBlue = New Care.Controls.CareLabel(Me.components)
        Me.lblRed = New Care.Controls.CareLabel(Me.components)
        Me.lblYellow = New Care.Controls.CareLabel(Me.components)
        Me.lblGreen = New Care.Controls.CareLabel(Me.components)
        Me.MyDateNav = New DevExpress.XtraScheduler.DateNavigator()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOrange.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MyScheduler, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MyStorage, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxLegend, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxLegend.SuspendLayout()
        CType(Me.MyDateNav, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MyDateNav.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtYellow
        '
        Me.txtYellow.Location = New System.Drawing.Point(287, 9)
        Me.txtYellow.Name = "txtYellow"
        Me.txtYellow.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtYellow.Properties.Appearance.Options.UseBackColor = True
        Me.txtYellow.Properties.ReadOnly = True
        Me.txtYellow.Size = New System.Drawing.Size(20, 20)
        Me.txtYellow.TabIndex = 9
        Me.txtYellow.TabStop = False
        '
        'txtRed
        '
        Me.txtRed.Location = New System.Drawing.Point(9, 9)
        Me.txtRed.Name = "txtRed"
        Me.txtRed.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtRed.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRed.Properties.Appearance.Options.UseBackColor = True
        Me.txtRed.Properties.Appearance.Options.UseFont = True
        Me.txtRed.Properties.ReadOnly = True
        Me.txtRed.Size = New System.Drawing.Size(20, 20)
        Me.txtRed.TabIndex = 8
        Me.txtRed.TabStop = False
        '
        'txtGreen
        '
        Me.txtGreen.Location = New System.Drawing.Point(442, 9)
        Me.txtGreen.Name = "txtGreen"
        Me.txtGreen.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtGreen.Properties.Appearance.Options.UseBackColor = True
        Me.txtGreen.Properties.ReadOnly = True
        Me.txtGreen.Size = New System.Drawing.Size(20, 20)
        Me.txtGreen.TabIndex = 6
        Me.txtGreen.TabStop = False
        '
        'txtOrange
        '
        Me.txtOrange.Location = New System.Drawing.Point(156, 9)
        Me.txtOrange.Name = "txtOrange"
        Me.txtOrange.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtOrange.Properties.Appearance.Options.UseBackColor = True
        Me.txtOrange.Properties.ReadOnly = True
        Me.txtOrange.Size = New System.Drawing.Size(20, 20)
        Me.txtOrange.TabIndex = 5
        Me.txtOrange.TabStop = False
        '
        'MyScheduler
        '
        Me.MyScheduler.Location = New System.Drawing.Point(400, 3)
        Me.MyScheduler.Name = "MyScheduler"
        Me.MyScheduler.Size = New System.Drawing.Size(286, 255)
        Me.MyScheduler.Start = New Date(2016, 3, 1, 0, 0, 0, 0)
        Me.MyScheduler.Storage = Me.MyStorage
        Me.MyScheduler.TabIndex = 5
        Me.MyScheduler.Text = "SchedulerControl1"
        Me.MyScheduler.ToolTipController = Me.ToolTipController1
        Me.MyScheduler.Views.DayView.TimeRulers.Add(TimeRuler1)
        Me.MyScheduler.Views.GanttView.Enabled = False
        Me.MyScheduler.Views.MonthView.Enabled = False
        Me.MyScheduler.Views.TimelineView.Enabled = False
        Me.MyScheduler.Views.WeekView.Enabled = False
        Me.MyScheduler.Views.WorkWeekView.Enabled = False
        Me.MyScheduler.Views.WorkWeekView.TimeRulers.Add(TimeRuler2)
        '
        'gbxLegend
        '
        Me.gbxLegend.Controls.Add(Me.lblBlue)
        Me.gbxLegend.Controls.Add(Me.lblRed)
        Me.gbxLegend.Controls.Add(Me.lblYellow)
        Me.gbxLegend.Controls.Add(Me.lblGreen)
        Me.gbxLegend.Controls.Add(Me.txtOrange)
        Me.gbxLegend.Controls.Add(Me.txtYellow)
        Me.gbxLegend.Controls.Add(Me.txtRed)
        Me.gbxLegend.Controls.Add(Me.txtGreen)
        Me.gbxLegend.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.gbxLegend.Location = New System.Drawing.Point(0, 275)
        Me.gbxLegend.Name = "gbxLegend"
        Me.gbxLegend.ShowCaption = False
        Me.gbxLegend.Size = New System.Drawing.Size(689, 36)
        Me.gbxLegend.TabIndex = 10
        Me.gbxLegend.Text = "GroupControl1"
        '
        'lblBlue
        '
        Me.lblBlue.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblBlue.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblBlue.Location = New System.Drawing.Point(468, 11)
        Me.lblBlue.Name = "lblBlue"
        Me.lblBlue.Size = New System.Drawing.Size(45, 15)
        Me.lblBlue.TabIndex = 18
        Me.lblBlue.Text = "1-2 boys"
        Me.lblBlue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRed
        '
        Me.lblRed.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblRed.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblRed.Location = New System.Drawing.Point(35, 12)
        Me.lblRed.Name = "lblRed"
        Me.lblRed.Size = New System.Drawing.Size(45, 15)
        Me.lblRed.TabIndex = 17
        Me.lblRed.Text = "7 boys +"
        Me.lblRed.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblYellow
        '
        Me.lblYellow.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblYellow.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblYellow.Location = New System.Drawing.Point(313, 11)
        Me.lblYellow.Name = "lblYellow"
        Me.lblYellow.Size = New System.Drawing.Size(45, 15)
        Me.lblYellow.TabIndex = 16
        Me.lblYellow.Text = "3-4 boys"
        Me.lblYellow.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGreen
        '
        Me.lblGreen.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblGreen.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblGreen.Location = New System.Drawing.Point(182, 11)
        Me.lblGreen.Name = "lblGreen"
        Me.lblGreen.Size = New System.Drawing.Size(45, 15)
        Me.lblGreen.TabIndex = 15
        Me.lblGreen.Text = "5-6 boys"
        Me.lblGreen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'MyDateNav
        '
        Me.MyDateNav.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MyDateNav.Appearance.Options.UseFont = True
        Me.MyDateNav.BoldAppointmentDates = False
        Me.MyDateNav.CalendarAppearance.DayCellHighlighted.BackColor = System.Drawing.Color.Transparent
        Me.MyDateNav.CalendarAppearance.DayCellHighlighted.BackColor2 = System.Drawing.Color.Transparent
        Me.MyDateNav.CalendarAppearance.DayCellHighlighted.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.MyDateNav.CalendarAppearance.DayCellHighlighted.Options.UseBackColor = True
        Me.MyDateNav.CalendarAppearance.DayCellHighlighted.Options.UseFont = True
        Me.MyDateNav.CalendarAppearance.DayCellSpecial.FontStyleDelta = System.Drawing.FontStyle.Bold
        Me.MyDateNav.CalendarAppearance.DayCellSpecial.Options.UseFont = True
        Me.MyDateNav.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.MyDateNav.CellSize = New System.Drawing.Size(20, 20)
        Me.MyDateNav.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MyDateNav.DrawCellLines = True
        Me.MyDateNav.FirstDayOfWeek = System.DayOfWeek.Monday
        Me.MyDateNav.HighlightSelection = False
        Me.MyDateNav.HighlightTodayCellWhenSelected = False
        Me.MyDateNav.Location = New System.Drawing.Point(0, 0)
        Me.MyDateNav.Name = "MyDateNav"
        Me.MyDateNav.NavigationMode = DevExpress.XtraScheduler.DateNavigationMode.ScrollCalendar
        Me.MyDateNav.SchedulerControl = Me.MyScheduler
        Me.MyDateNav.SelectionMode = DevExpress.XtraEditors.Repository.CalendarSelectionMode.[Single]
        Me.MyDateNav.ShowFooter = False
        Me.MyDateNav.ShowHeader = False
        Me.MyDateNav.ShowMonthNavigationButtons = DevExpress.Utils.DefaultBoolean.[False]
        Me.MyDateNav.ShowWeekNumbers = False
        Me.MyDateNav.ShowYearNavigationButtons = DevExpress.Utils.DefaultBoolean.[False]
        Me.MyDateNav.Size = New System.Drawing.Size(689, 275)
        Me.MyDateNav.TabIndex = 11
        '
        'YearView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.MyDateNav)
        Me.Controls.Add(Me.gbxLegend)
        Me.Controls.Add(Me.MyScheduler)
        Me.Name = "YearView"
        Me.Size = New System.Drawing.Size(689, 311)
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOrange.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MyScheduler, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MyStorage, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxLegend, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxLegend.ResumeLayout(False)
        Me.gbxLegend.PerformLayout()
        CType(Me.MyDateNav.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MyDateNav, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MyScheduler As DevExpress.XtraScheduler.SchedulerControl
    Friend WithEvents MyStorage As DevExpress.XtraScheduler.SchedulerStorage
    Friend WithEvents txtRed As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtGreen As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtOrange As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtYellow As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ToolTipController1 As DevExpress.Utils.ToolTipController
    Friend WithEvents MyDateNav As DevExpress.XtraScheduler.DateNavigator
    Friend WithEvents lblBlue As Care.Controls.CareLabel
    Friend WithEvents lblRed As Care.Controls.CareLabel
    Friend WithEvents lblYellow As Care.Controls.CareLabel
    Friend WithEvents lblGreen As Care.Controls.CareLabel
    Friend WithEvents gbxLegend As Care.Controls.CareFrame
End Class
