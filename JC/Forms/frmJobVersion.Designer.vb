﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmJobVersion
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.txtComments = New DevExpress.XtraEditors.MemoEdit()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.txtValue = New Care.Controls.CareTextBox(Me.components)
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.cdtVersionDate = New Care.Controls.CareDateTime(Me.components)
        Me.btnCreate = New Care.Controls.CareButton(Me.components)
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.cbxReason = New Care.Controls.CareComboBox(Me.components)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtComments.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.txtValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.cdtVersionDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtVersionDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxReason.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.txtComments)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 105)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(636, 232)
        Me.GroupControl1.TabIndex = 1
        Me.GroupControl1.Text = "Comments"
        '
        'txtComments
        '
        Me.txtComments.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtComments.Location = New System.Drawing.Point(5, 23)
        Me.txtComments.Name = "txtComments"
        Me.txtComments.Properties.AccessibleName = "Comments"
        Me.txtComments.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtComments.Properties.Appearance.Options.UseFont = True
        Me.txtComments.Properties.MaxLength = 1024
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtComments, True)
        Me.txtComments.Size = New System.Drawing.Size(626, 204)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtComments, OptionsSpelling1)
        Me.txtComments.TabIndex = 0
        Me.txtComments.Tag = "AEM"
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.CareLabel1)
        Me.GroupControl4.Controls.Add(Me.txtValue)
        Me.GroupControl4.Location = New System.Drawing.Point(12, 343)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(636, 57)
        Me.GroupControl4.TabIndex = 2
        Me.GroupControl4.Text = "Version Value"
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(420, 30)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(71, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Version Value"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtValue
        '
        Me.txtValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtValue.EnterMoveNextControl = True
        Me.txtValue.Location = New System.Drawing.Point(497, 27)
        Me.txtValue.MaxLength = 14
        Me.txtValue.Name = "txtValue"
        Me.txtValue.NumericAllowNegatives = True
        Me.txtValue.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtValue.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtValue.Properties.AccessibleName = "Version Value"
        Me.txtValue.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtValue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtValue.Properties.Appearance.Options.UseFont = True
        Me.txtValue.Properties.Appearance.Options.UseTextOptions = True
        Me.txtValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtValue.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txtValue.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtValue.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtValue.Properties.MaxLength = 14
        Me.txtValue.Size = New System.Drawing.Size(130, 22)
        Me.txtValue.TabIndex = 1
        Me.txtValue.Tag = "AEM"
        Me.txtValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtValue.ToolTipText = ""
        '
        'GroupControl5
        '
        Me.GroupControl5.Controls.Add(Me.cbxReason)
        Me.GroupControl5.Controls.Add(Me.CareLabel4)
        Me.GroupControl5.Controls.Add(Me.CareLabel2)
        Me.GroupControl5.Controls.Add(Me.cdtVersionDate)
        Me.GroupControl5.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(636, 87)
        Me.GroupControl5.TabIndex = 0
        Me.GroupControl5.Text = "Version Details"
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(14, 31)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(66, 15)
        Me.CareLabel2.TabIndex = 2
        Me.CareLabel2.Text = "Version Date"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtVersionDate
        '
        Me.cdtVersionDate.EditValue = Nothing
        Me.cdtVersionDate.EnterMoveNextControl = True
        Me.cdtVersionDate.Location = New System.Drawing.Point(111, 28)
        Me.cdtVersionDate.Name = "cdtVersionDate"
        Me.cdtVersionDate.Properties.AccessibleName = "Version Date"
        Me.cdtVersionDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtVersionDate.Properties.Appearance.Options.UseFont = True
        Me.cdtVersionDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtVersionDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtVersionDate.Size = New System.Drawing.Size(100, 22)
        Me.cdtVersionDate.TabIndex = 3
        Me.cdtVersionDate.Tag = "AEM"
        Me.cdtVersionDate.Value = Nothing
        '
        'btnCreate
        '
        Me.btnCreate.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCreate.Appearance.Options.UseFont = True
        Me.btnCreate.Location = New System.Drawing.Point(472, 406)
        Me.btnCreate.Name = "btnCreate"
        Me.btnCreate.Size = New System.Drawing.Size(85, 25)
        Me.btnCreate.TabIndex = 3
        Me.btnCreate.Tag = ""
        Me.btnCreate.Text = "OK"
        '
        'btnClose
        '
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(563, 406)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(85, 25)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Tag = ""
        Me.btnClose.Text = "Cancel"
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(14, 59)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(80, 15)
        Me.CareLabel4.TabIndex = 4
        Me.CareLabel4.Text = "Version Reason"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxReason
        '
        Me.cbxReason.AllowBlank = False
        Me.cbxReason.DataSource = Nothing
        Me.cbxReason.DisplayMember = Nothing
        Me.cbxReason.EnterMoveNextControl = True
        Me.cbxReason.Location = New System.Drawing.Point(111, 56)
        Me.cbxReason.Name = "cbxReason"
        Me.cbxReason.Properties.AccessibleName = "Version Reason"
        Me.cbxReason.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxReason.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxReason.Properties.Appearance.Options.UseFont = True
        Me.cbxReason.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxReason.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxReason.SelectedValue = Nothing
        Me.cbxReason.Size = New System.Drawing.Size(516, 22)
        Me.cbxReason.TabIndex = 5
        Me.cbxReason.Tag = "AEM"
        Me.cbxReason.ValueMember = Nothing
        '
        'frmJobVersion
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(658, 438)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnCreate)
        Me.Controls.Add(Me.GroupControl5)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.GroupControl1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmJobVersion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmInvoice"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.txtComments.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.txtValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.cdtVersionDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtVersionDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxReason.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtComments As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents txtValue As Care.Controls.CareTextBox
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnCreate As Care.Controls.CareButton
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents cdtVersionDate As Care.Controls.CareDateTime
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents cbxReason As Care.Controls.CareComboBox
End Class
