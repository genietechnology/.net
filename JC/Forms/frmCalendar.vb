﻿
Option Strict On

Imports Care.Data
Imports Care.Global
Imports DevExpress.XtraScheduler
Imports System.ComponentModel
Imports System.Drawing

Public Class frmCalendar

    Public Enum EnumAppointmentType
        Custom = 0
        PrivateWork = 1
        Insurance = 2
        Holiday = 3
        Sickness = 4
        Absence = 5
    End Enum

    Public Enum EnumComplianceType
        RiskAction = 0
        RiskAssessment = 1
        WarrantyExpiry = 2
        Service = 3
        Test = 4
        Inspection = 5
    End Enum

    Public Class CustomAppointment
        Public Property StartTime() As Date
        Public Property EndTime() As Date
        Public Property Subject() As String
        Public Property Status() As Integer
        Public Property Description() As String
        Public Property Label() As Integer
        Public Property Location() As String
        Public Property AllDay() As Boolean
        Public Property EventType() As Integer
        Public Property RecurrenceInfo() As String
        Public Property ReminderInfo() As String
        Public Property OwnerId() As Object
        Public Property ID As Guid?
    End Class

    Private m_Loading As Boolean = True
    Private m_Sites As New Dictionary(Of Integer, String)
    Private m_Appointments As New BindingList(Of CustomAppointment)()

    Public Sub New()
        InitializeComponent()
        SchedulerStorage1.Resources.ColorSaving = ColorSavingType.Color
    End Sub

    Private Sub frmCalendar_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Session.WaitFormShow()
        SaveAppointments()
        Session.WaitFormClose()
    End Sub

    Private Sub frmCalendar_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        Session.WaitFormShow()

        GenerateEvents(m_Appointments)
        Me.SchedulerStorage1.Appointments.DataSource = m_Appointments

        m_Loading = False

        Session.WaitFormClose()

    End Sub

    Private Sub frmCalendar_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        cbxView.Items.Add("Standard")
        cbxView.Items.Add("Compliance")
        bbiView.EditValue = "Standard"

        Dim _M As AppointmentMappingInfo = Me.SchedulerStorage1.Appointments.Mappings
        _M.Start = "StartTime"
        _M.End = "EndTime"
        _M.Subject = "Subject"
        _M.AllDay = "AllDay"
        _M.Description = "Description"
        _M.Label = "Label"
        _M.Location = "Location"
        _M.RecurrenceInfo = "RecurrenceInfo"
        _M.ReminderInfo = "ReminderInfo"
        _M.ResourceId = "OwnerId"
        _M.Status = "Status"
        _M.Type = "EventType"

        ChangeView()
        SchedulerControl1.Start = Date.Now

    End Sub

    Private Sub ChangeView()

        SchedulerStorage1.Appointments.Labels.Clear()

        Select Case bbiView.EditValue.ToString

            Case "Standard"
                SchedulerStorage1.Appointments.Labels.Add(Color.Silver, "Custom")
                SchedulerStorage1.Appointments.Labels.Add(Color.LightGreen, "Private Work")
                SchedulerStorage1.Appointments.Labels.Add(Color.Khaki, "Insurance")
                SchedulerStorage1.Appointments.Labels.Add(Color.CornflowerBlue, "Holiday")
                SchedulerStorage1.Appointments.Labels.Add(Color.OrangeRed, "Sickness")
                SchedulerStorage1.Appointments.Labels.Add(Color.LightCoral, "Absence")
                SchedulerStorage1.Appointments.Labels.Add(Color.Orange, "Leaver")
                SchedulerStorage1.Appointments.Labels.Add(Color.Plum, "Birthday")
                SchedulerStorage1.Appointments.Labels.Add(Color.Yellow, "Staff Holiday")

            Case "Compliance"
                SchedulerStorage1.Appointments.Labels.Add(Color.OrangeRed, "Risk Actions")
                SchedulerStorage1.Appointments.Labels.Add(Color.LightCoral, "Risk Assessment")
                SchedulerStorage1.Appointments.Labels.Add(Color.LightGreen, "Warranty Expires")
                SchedulerStorage1.Appointments.Labels.Add(Color.CornflowerBlue, "Service")
                SchedulerStorage1.Appointments.Labels.Add(Color.Yellow, "Test")
                SchedulerStorage1.Appointments.Labels.Add(Color.Plum, "Inspection")

        End Select

        chkTypes.Items.Clear()
        For Each _L In SchedulerStorage1.Appointments.Labels
            chkTypes.Items.Add(_L.Id, _L.DisplayName, System.Windows.Forms.CheckState.Checked, True)
        Next

    End Sub

    Private Sub GenerateEvents(ByVal Appointments As BindingList(Of CustomAppointment))

        Select Case bbiView.EditValue.ToString

            Case "Standard"
                GenerateStandardEvents(Appointments)

            Case "Compliance"
                GenerateComplianceEvents(Appointments)

        End Select

    End Sub

    Private Sub GenerateComplianceEvents(ByVal Appointments As BindingList(Of CustomAppointment))

        Appointments.Clear()

        '***************************************************************************************************************************************************

        'Dim _Equipment As List(Of Business.Equipment) = Business.Equipment.RetreiveAll
        'For Each _E In _Equipment

        '    If _E._Warranty AndAlso _E._WarrantyExpires.HasValue Then
        '        Dim _Subject As String = _E._Name + " - Warranty Expiry"
        '        Appointments.Add(CreateAllDayEvent(_E._WarrantyExpires.Value, _Subject, EnumComplianceType.WarrantyExpiry))
        '    End If

        '    If _E._Serv AndAlso _E._ServNext.HasValue Then
        '        Dim _Subject As String = _E._Name + " - Service Due"
        '        Appointments.Add(CreateAllDayEvent(_E._ServNext.Value, _Subject, EnumComplianceType.Service))
        '    End If

        '    If _E._Test AndAlso _E._TestNext.HasValue Then
        '        Dim _Subject As String = _E._Name + " - Test Due"
        '        Appointments.Add(CreateAllDayEvent(_E._TestNext.Value, _Subject, EnumComplianceType.Test))
        '    End If

        '    If _E._Insp AndAlso _E._InspNext.HasValue Then
        '        Dim _Subject As String = _E._Name + " - Inspection Due"
        '        Appointments.Add(CreateAllDayEvent(_E._InspNext.Value, _Subject, EnumComplianceType.Inspection))
        '    End If

        'Next

        '***************************************************************************************************************************************************

        'SchedulerStorage1.Appointments.Labels.Add(Color.OrangeRed, "Risk Actions")
        'SchedulerStorage1.Appointments.Labels.Add(Color.LightCoral, "Risk Assessment")

        'Dim _RiskAssessments As List(Of Business.Risk) = Business.Risk.RetreiveAll
        'For Each _R In _RiskAssessments

        'Next

        '***************************************************************************************************************************************************

    End Sub

    Private Sub GenerateStandardEvents(ByVal Appointments As BindingList(Of CustomAppointment))

        Dim _SQL As String = ""

        Appointments.Clear()

        '***************************************************************************************************************************************************
        'custom appointments
        Dim _Apps As List(Of Care.Shared.Business.Appointment) = Care.Shared.Business.Appointment.RetreiveAll
        If _Apps IsNot Nothing Then
            For Each _A In _Apps
                Appointments.Add(CreateEvent(_A._ID.Value, _A._StartDate, _A._EndDate, _A._Subject, _A._Description, _A._Location, EnumAppointmentType.Custom, False))
            Next
        End If

        '***************************************************************************************************************************************************
        'staff sickness & holidays

        '_SQL = ""
        '_SQL += "select a.abs_type + ' - ' + s.fullname as 'desc', a.abs_type, a.abs_from, a.abs_to, a.abs_reason, a.abs_notes from StaffAbsence a "
        '_SQL += " left join Staff s on s.ID = a.staff_id"
        '_SQL += " where s.site_name = '" + bbiSite.EditValue.ToString + "'"
        '_SQL += " and s.status = 'C'"
        '_SQL += " and a.abs_type in ('Sickness','Annual Leave')"

        'Dim _DTAbsence As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        'If _DTAbsence IsNot Nothing Then
        '    For Each _DR As DataRow In _DTAbsence.Rows
        '        If _DR.Item("abs_type").ToString = "Sickness" Then

        '            Appointments.Add(CreateEvent(Nothing, ValueHandler.ConvertDate(_DR.Item("abs_from")), ValueHandler.ConvertDate(_DR.Item("abs_to")),
        '                            _DR.Item("desc").ToString, _DR.Item("abs_notes").ToString, _DR.Item("abs_reason").ToString, EnumAppointmentType.StaffSickness, True))

        '        Else

        '            Appointments.Add(CreateEvent(Nothing, ValueHandler.ConvertDate(_DR.Item("abs_from")), ValueHandler.ConvertDate(_DR.Item("abs_to")),
        '                            _DR.Item("desc").ToString, _DR.Item("abs_notes").ToString, "", EnumAppointmentType.StaffHoliday, True))

        '        End If
        '    Next
        'End If

        '***************************************************************************************************************************************************
        'child holidays

        '_SQL = ""
        '_SQL += "select c.fullname, from_date, to_date from ChildHolidays h"
        '_SQL += " left join Children c on c.ID = h.child_id"
        '_SQL += " where c.site_name = '" + bbiSite.EditValue.ToString + "'"
        '_SQL += " and c.status <> 'Left'"

        'Dim _DTHols As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        'If _DTHols IsNot Nothing Then
        '    For Each _DR As DataRow In _DTHols.Rows

        '        Appointments.Add(CreateEvent(Nothing, ValueHandler.ConvertDate(_DR.Item("from_date")), ValueHandler.ConvertDate(_DR.Item("to_date")),
        '                          "Holiday - " + _DR.Item("fullname").ToString, "", "", EnumAppointmentType.ChildHoliday, True))

        '    Next
        'End If

        '***************************************************************************************************************************************************
        'child absence
        '_SQL = ""
        '_SQL += "select a.key_name, a.description, a.notes, a.stamp from Activity a"
        '_SQL += " left join Children c on c.ID = a.key_id"
        '_SQL += " where c.site_name = '" + bbiSite.EditValue.ToString + "'"
        '_SQL += " and a.type = 'ABSENCE'"

        'Dim _DTChildAbs As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        'If _DTChildAbs IsNot Nothing Then
        '    For Each _DR As DataRow In _DTChildAbs.Rows

        '        Appointments.Add(CreateEvent(Nothing, ValueHandler.ConvertDate(_DR.Item("stamp")), ValueHandler.ConvertDate(_DR.Item("stamp")),
        '                        "Absence - " + _DR.Item("key_name").ToString, _DR.Item("notes").ToString, _DR.Item("description").ToString, EnumAppointmentType.ChildAbsence, True))

        '    Next
        'End If

        '***************************************************************************************************************************************************
        'viewings

        '_SQL = ""
        '_SQL += "select date_viewing, contact_fullname, child_fullname from Leads"
        '_SQL += " where site_name = '" + bbiSite.EditValue.ToString + "'"
        '_SQL += " and date_viewing is not null"

        'Dim _DTLeads As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        'If _DTLeads IsNot Nothing Then
        '    For Each _DR As DataRow In _DTLeads.Rows

        '        Dim _Time As Date? = ValueHandler.ConvertDate(_DR.Item("date_viewing"))
        '        If _Time.HasValue Then

        '            If _Time.Value.TimeOfDay.Hours = 0 Then

        '                Appointments.Add(CreateEvent(Nothing, ValueHandler.ConvertDate(_DR.Item("date_viewing")), _Time,
        '                                            "Viewing - " + _DR.Item("contact_fullname").ToString, "", _DR.Item("child_fullname").ToString, EnumAppointmentType.Viewing, True))

        '            Else

        '                Appointments.Add(CreateEvent(Nothing, ValueHandler.ConvertDate(_DR.Item("date_viewing")), _Time,
        '                                            "Viewing - " + _DR.Item("contact_fullname").ToString, "", _DR.Item("child_fullname").ToString, EnumAppointmentType.Viewing, False))

        '            End If


        '        End If


        '    Next
        'End If

        '***************************************************************************************************************************************************

    End Sub

    Private Sub SaveAppointments()

        Session.CursorWaiting()

        For Each _App In m_Appointments

            If _App.Label = 0 Then

                Dim _A As New Care.Shared.Business.Appointment

                If _App.ID.HasValue Then
                    _A = Care.Shared.Business.Appointment.RetreiveByID(_App.ID.Value)
                End If

                With _A
                    ._CalendarType = "Nursery"
                    ._StartDate = _App.StartTime
                    ._EndDate = _App.EndTime
                    ._Subject = _App.Subject
                    ._Type = _App.EventType
                    ._Status = _App.Status
                    ._Label = _App.Label
                    ._Allday = _App.AllDay
                    ._Location = _App.Location
                    ._Description = _App.Description
                    ._ReminderDetails = _App.ReminderInfo
                    ._RecurrenceDetails = _App.RecurrenceInfo
                    .Store()
                End With

            End If

        Next

        Session.CursorDefault()

    End Sub

    Private Function CreateAllDayEvent(ByVal EventDate As Date, ByVal subject As String, ByVal label As EnumAppointmentType) As CustomAppointment

        Dim _A As New CustomAppointment()
        _A.Subject = subject
        _A.StartTime = EventDate
        _A.EndTime = EventDate
        _A.AllDay = True
        _A.Status = 0
        _A.Label = label

        'If label = EnumAppointmentType.Birthday Then
        '    Dim _r As New RecurrenceInfo
        '    _r.AllDay = True
        '    _r.DayNumber = EventDate.Day
        '    _r.Month = EventDate.Month
        '    _r.Type = RecurrenceType.Yearly
        '    _r.Range = RecurrenceRange.NoEndDate
        '    _A.RecurrenceInfo = _r.ToXml
        'End If

        Return _A

    End Function

    Private Function CreateAllDayEvent(ByVal EventDate As Date, ByVal subject As String, ByVal label As EnumComplianceType) As CustomAppointment

        Dim _A As New CustomAppointment()
        _A.Subject = subject
        _A.StartTime = EventDate
        _A.EndTime = EventDate
        _A.AllDay = True
        _A.Status = 0
        _A.Label = label

        Return _A

    End Function

    Private Function CreateEvent(ByVal ID As Guid, ByVal FromDate As Date?, ByVal ToDate As Date?, ByVal Subject As String, ByVal Description As String,
                                 ByVal Location As String, ByVal Label As EnumAppointmentType, ByVal AllDay As Boolean) As CustomAppointment

        If FromDate.HasValue AndAlso ToDate.HasValue Then

            Dim _A As New CustomAppointment()

            _A.StartTime = FromDate.Value

            If FromDate.Value = ToDate.Value Then
                _A.EndTime = ToDate.Value
            Else
                'check if the end date is midnight, if it is, extend the day + 1
                If IsMidnight(ToDate) Then
                    _A.EndTime = ToDate.Value.AddDays(1)
                End If
            End If

            _A.ID = ID
            _A.Subject = Subject
            _A.Description = Description
            _A.Location = Location

            _A.AllDay = AllDay
            _A.Status = 0
            _A.Label = Label

            Return _A

        Else
            Return Nothing
        End If

    End Function

    Private Function IsMidnight(ByVal DateIn As Date?) As Boolean
        If DateIn.HasValue Then
            If DateIn.Value.TimeOfDay.Hours = 0 AndAlso DateIn.Value.TimeOfDay.Minutes = 0 Then
                Return True
            End If
        End If
        Return False
    End Function

    Private Sub chkTypes_ItemCheck(sender As Object, e As DevExpress.XtraEditors.Controls.ItemCheckEventArgs) Handles chkTypes.ItemCheck
        If Not m_Loading Then
            SchedulerControl1.RefreshData()
        End If
    End Sub

    Private Sub SchedulerStorage1_FilterAppointment(sender As Object, e As PersistentObjectCancelEventArgs) Handles SchedulerStorage1.FilterAppointment

        If Not m_Loading Then

            Dim _A As CustomAppointment = CType(e.Object.GetSourceObject(SchedulerStorage1), CustomAppointment)

            Dim _OK As Boolean = False
            For _i = 0 To chkTypes.CheckedItemsCount - 1
                If chkTypes.CheckedItems(_i).ToString = _A.Label.ToString Then
                    _OK = True
                End If
            Next

            If _OK = False Then
                e.Cancel = True
            End If

        End If

    End Sub

    Private Sub bbiSite_EditValueChanged(sender As Object, e As EventArgs) Handles bbiSite.EditValueChanged

        If Not m_Loading Then

            Session.WaitFormShow()

            GenerateEvents(m_Appointments)
            SchedulerControl1.RefreshData()

            Session.WaitFormClose()

        End If

    End Sub

    Private Sub bbiView_EditValueChanged(sender As Object, e As EventArgs) Handles bbiView.EditValueChanged

        If Not m_Loading Then

            Session.WaitFormShow()

            ChangeView()
            GenerateEvents(m_Appointments)
            SchedulerControl1.RefreshData()

            Session.WaitFormClose()

        End If

    End Sub
End Class

