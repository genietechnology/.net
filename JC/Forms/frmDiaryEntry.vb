﻿Option Strict On

Imports Care.Global

Public Class frmDiaryEntry

    Private Enum EnumAction
        Job
        Absent
        Sick
        Holiday
    End Enum

    Private m_Entries As New List(Of Entry)

    Private Sub frmDiaryEntry_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cbxWeather.AddItem("Clear")
        cbxWeather.AddItem("Sunny")
        cbxWeather.AddItem("Cloudy")
        cbxWeather.AddItem("Showers")
        cbxWeather.AddItem("Rain")
        cbxWeather.AddItem("Frost")
        cbxWeather.AddItem("Snow")

        Clear()
        btnDelete.Enabled = False
        btnCommit.Enabled = False

    End Sub

#Region "Controls"

    Private Sub txtEmployee_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtEmployee.Validating
        If txtEmployee.Text <> "" Then
            Dim _E As Business.Employee = Business.Employee.RetreiveByName(txtEmployee.Text)
            If _E IsNot Nothing Then
                txtEmployee.Tag = _E._ID.ToString
                txtEmployee.Text = _E._Code
                txtEmployeeName.Text = _E._Fullname
            Else
                txtEmployee.Tag = ""
                txtEmployee.Text = ""
                txtEmployeeName.Text = ""
            End If
        Else
            txtEmployee.Tag = ""
            txtEmployee.Text = ""
            txtEmployeeName.Text = ""
        End If
        ValidateEntry()
    End Sub

    Private Sub txtJob_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtJob.Validating
        If txtJob.Text <> "" Then
            Dim _J As Business.Job = Business.Job.RetreiveByJobNo(txtJob.Text)
            If _J IsNot Nothing Then
                txtJob.Tag = _J._ID.ToString
                txtJob.Text = _J._JobNo.ToString
                txtJobName.Text = _J._RskFullname + ", " + _J._RskAddress1
            Else
                txtJob.Tag = ""
                txtJob.Text = ""
                txtJobName.Text = ""
            End If
        Else
            txtJob.Tag = ""
            txtJob.Text = ""
            txtJobName.Text = ""
        End If
        ValidateEntry()
    End Sub

    Private Sub btnEmployeeSearch_Click(sender As Object, e As EventArgs) Handles btnEmployeeSearch.Click
        Dim _ID As String = Business.Employee.FindActive
        If _ID <> "" Then
            Dim _E As Business.Employee = Business.Employee.RetreiveByID(New Guid(_ID))
            If _E IsNot Nothing Then
                txtEmployee.Tag = _E._ID.ToString
                txtEmployee.Text = _E._Code
                txtEmployeeName.Text = _E._Fullname
            End If
        End If
        ValidateEntry()
    End Sub

    Private Sub btnJobSearch_Click(sender As Object, e As EventArgs) Handles btnJobSearch.Click
        Dim _ID As String = Business.Job.FindAll
        If _ID <> "" Then
            Dim _J As Business.Job = Business.Job.RetreiveByID(New Guid(_ID))
            If _J IsNot Nothing Then
                txtJob.Tag = _J._ID.ToString
                txtJob.Text = _J._JobNo.ToString
                txtJobName.Text = _J._RskFullname + ", " + _J._RskAddress1
            End If
        End If
        ValidateEntry()
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        AddEntry(EnumAction.Job)
    End Sub

    Private Sub btnAbsent_Click(sender As Object, e As EventArgs) Handles btnAbsent.Click
        AddEntry(EnumAction.Absent)
    End Sub

    Private Sub btnSick_Click(sender As Object, e As EventArgs) Handles btnSick.Click
        AddEntry(EnumAction.Sick)
    End Sub

    Private Sub btnHoliday_Click(sender As Object, e As EventArgs) Handles btnHoliday.Click
        AddEntry(EnumAction.Holiday)
    End Sub

    Private Sub btnCommit_Click(sender As Object, e As EventArgs) Handles btnCommit.Click
        CommitUpdate
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click

        If cgEntries.RecordCount = 0 Then Exit Sub
        If cgEntries.FocusedRowHandle < 0 Then Exit Sub

        If CareMessage("Are you sure you want to delete this entry?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Delete") = DialogResult.Yes Then
            Dim _o As Object = cgEntries.GetRowObject(cgEntries.FocusedRowHandle)
            If _o IsNot Nothing Then
                Dim _e As Entry = CType(_o, Entry)
                m_Entries.Remove(_e)
                PopulateGrid()
            End If
        End If

    End Sub

#End Region

    Private Sub AddEntry(ByVal Action As EnumAction)

        Dim _e As New Entry
        _e.Action = Action
        _e.EmployeeID = txtEmployee.Tag.ToString
        _e.EmployeeName = txtEmployeeName.Text
        _e.JobID = txtJob.Tag.ToString
        _e.JobNo = txtJob.Text
        _e.JobName = txtJobName.Text

        m_Entries.Add(_e)

        PopulateGrid

    End Sub

    Private Sub PopulateGrid()

        cgEntries.Populate(m_Entries)

        cgEntries.Columns("EmployeeID").Visible = False
        cgEntries.Columns("JobID").Visible = False

        SetGridButtons()
        Clear()
        txtEmployee.Focus()

    End Sub

    Private Sub SetGridButtons()
        If m_Entries.Count > 0 Then
            btnCommit.Enabled = True
            btnDelete.Enabled = True
        Else
            btnCommit.Enabled = False
            btnDelete.Enabled = False
        End If
    End Sub

    Private Sub ValidateEntry()

        If txtEmployeeName.Text <> "" Then

            btnAbsent.Enabled = True
            btnSick.Enabled = True
            btnHoliday.Enabled = True

            If txtJobName.Text <> "" Then
                btnAdd.Enabled = True
            Else
                btnAdd.Enabled = False
            End If

        Else
            btnAbsent.Enabled = False
            btnSick.Enabled = False
            btnHoliday.Enabled = False
            btnAdd.Enabled = False
        End If

    End Sub

    Private Sub SetCMDs(ByVal Enabled As Boolean)
        btnAdd.Enabled = Enabled
        btnAbsent.Enabled = Enabled
        btnSick.Enabled = Enabled
        btnHoliday.Enabled = Enabled
    End Sub

    Private Sub Clear()

        txtJob.Tag = ""
        txtJob.Text = ""
        txtJobName.Text = ""
        txtEmployee.Tag = ""
        txtEmployee.Text = ""
        txtEmployeeName.Text = ""

        btnAdd.Enabled = False
        btnAbsent.Enabled = False
        btnSick.Enabled = False
        btnHoliday.Enabled = False

    End Sub

    Private Sub CommitUpdate()

        For Each _e In m_Entries

            Dim _d As New Business.Diary
            _d._DayDate = cdtDate.Value
            _d._Weather = cbxWeather.Text
            _d._EmpId = New Guid(_e.EmployeeID)
            _d._Status = _e.Action.ToString

            If _e.Action = EnumAction.Job Then
                _d._JobId = New Guid(_e.JobID)
            End If

            _d.Store()

        Next

        m_Entries.Clear()

        cdtDate.Text = ""
        cbxWeather.SelectedIndex = -1

        Clear()
        PopulateGrid()

        cdtDate.Focus()

    End Sub

    Private Class Entry
        Property EmployeeID As String
        Property EmployeeName As String
        Property Action As EnumAction
        Property JobID As String
        Property JobNo As String
        Property JobName As String
    End Class

End Class