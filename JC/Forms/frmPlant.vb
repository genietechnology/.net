﻿Option Strict On

Imports Care.Global
Imports Care.Data
Imports Care.Shared

Public Class frmPlant

    Private m_Plant As Business.Plant
    Private m_PlantIDQuoted As String = ""

    Private Sub frmJobs_Load(sender As Object, e As EventArgs) Handles Me.Load

        PopulateLists()

        btnDeploy.Enabled = False
        btnBackInYard.Enabled = False

    End Sub

    Private Sub PopulateLists()

        cbxPlantType.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Plant Types"))
        cbxPlantCat.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Plant Categories"))
        cbxArea.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("AREA CODES"))
        cbxInsurer.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Plant Insurers"))
        cbxRecovery.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Plant Recovery"))
        cbxMake.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Plant Makes"))

    End Sub

    Protected Overrides Sub SetBindings()

        m_Plant = New Business.Plant
        bs.DataSource = m_Plant

        '******************************************************************************************************************************
        txtAJNo.DataBindings.Add("Text", bs, "_AjNo")
        txtRegNo.DataBindings.Add("Text", bs, "_RegNo")
        txtName.DataBindings.Add("Text", bs, "_Name")
        txtNickname.DataBindings.Add("Text", bs, "_Nickname")

        cbxPlantType.DataBindings.Add("Text", bs, "_PlantType")
        cbxPlantCat.DataBindings.Add("Text", bs, "_PlantCat")
        cbxArea.DataBindings.Add("Text", bs, "_Area")

        txtNo1.DataBindings.Add("Text", bs, "_No1")
        txtNo2.DataBindings.Add("Text", bs, "_No2")

        cbxMake.DataBindings.Add("Text", bs, "_Make")
        txtModel.DataBindings.Add("Text", bs, "_Model")
        txtYear.DataBindings.Add("Text", bs, "_YearManu")
        txtMark.DataBindings.Add("Text", bs, "_Mark")

        cdtPurcDate.DataBindings.Add("Text", bs, "_PurcDate", True)
        txtPurcCost.DataBindings.Add("Text", bs, "_PurcCost")
        txtPurcEbay.DataBindings.Add("Text", bs, "_PurcEbay")

        cbxInsurer.DataBindings.Add("Text", bs, "_Insurer")
        cbxRecovery.DataBindings.Add("Text", bs, "_Recovery")
        chkTracker.DataBindings.Add("Checked", bs, "_Tracker")

        txtLastService.DataBindings.Add("Text", bs, "_LastService")
        txtLastMOT.DataBindings.Add("Text", bs, "_LastMot")
        txtLastTax.DataBindings.Add("Text", bs, "_TaxLast")
        txtLastMaint.DataBindings.Add("Text", bs, "_LastMaint")
        txtLastCheck.DataBindings.Add("Text", bs, "_LastCheck")

        chkHire.DataBindings.Add("Checked", bs, "_Hire")
        txtHireCost.DataBindings.Add("Text", bs, "_HireCost")

        '******************************************************************************************************************************

    End Sub

    Protected Overrides Sub FindRecord()
        Dim _ReturnValue As String = Business.Plant.Find
        If _ReturnValue <> "" Then
            DisplayRecord(New Guid(_ReturnValue))
        End If
    End Sub

    Private Sub DisplayRecord(ByVal PlantID As Guid)

        m_PlantIDQuoted = "'" + PlantID.ToString + "'"

        MyBase.RecordID = PlantID
        MyBase.RecordPopulated = True

        m_Plant = Business.Plant.RetreiveByID(PlantID)
        bs.DataSource = m_Plant

        If m_Plant._Photo IsNot Nothing Then
            pic.Image = DocumentHandler.GetImagefromByteArray(m_Plant._Photo)
        Else
            pic.Clear()
        End If

        btnDeploy.Enabled = True
        btnBackInYard.Enabled = True

        BuildTitleText()
        DisplayLocation

    End Sub

    Private Sub ctMain_Selecting(sender As Object, e As DevExpress.XtraTab.TabPageCancelEventArgs) Handles ctMain.Selecting

        If m_Plant Is Nothing Then Exit Sub
        Select Case e.Page.Name

            Case "tabPersonal"
                Me.ToolbarMode = ToolbarEnum.Standard

            Case "tabDescriptions"
                Me.ToolbarMode = ToolbarEnum.FindandEdit

            Case "tabVersions"
                Me.ToolbarMode = ToolbarEnum.FindandEdit

            Case "tabMaterials"
                Me.ToolbarMode = ToolbarEnum.FindOnly

            Case "tabLabour"
                Me.ToolbarMode = ToolbarEnum.FindOnly

            Case "tabInvoices"
                Me.ToolbarMode = ToolbarEnum.FindandEdit

            Case "tabActivity"
                DisplayActivity()
                Me.ToolbarMode = ToolbarEnum.FindOnly

            Case "tabHire"
                DisplayHire()
                Me.ToolbarMode = ToolbarEnum.FindandEdit

        End Select

    End Sub

    Private Sub DisplayActivity()

        Dim _SQL As String = ""
        _SQL += "select ID, act_text as 'Description', act_user as 'User', act_stamp as 'Stamp'"
        _SQL += " from PlantActivity"
        _SQL += " where plant_id = " + m_PlantIDQuoted
        _SQL += " order by stamp desc"

        cgActivity.HideFirstColumn = True
        cgActivity.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub DisplayHire()

        Dim _SQL As String = ""
        _SQL += "select d.ID, d.date_from as 'From', d.date_to as 'To', j.cus_fullname as 'Customer', j.rsk_fullname as 'Risk', d.days as 'Duration', d.cost as 'Cost'"
        _SQL += " from PlantDuration d"
        _SQL += " left join Jobs j on j.ID = d.job_id"
        _SQL += " where plant_id = " + m_PlantIDQuoted
        _SQL += " order by d.date_from desc"

        cgHire.HideFirstColumn = True
        cgHire.Populate(Session.ConnectionString, _SQL)

    End Sub

    Protected Overrides Sub AfterAdd()
        pic.Clear()
    End Sub

    Protected Overrides Sub CommitUpdate()

        m_Plant = CType(bs.Item(bs.Position), Business.Plant)

        'image already stored
        If m_Plant._Photo IsNot Nothing Then

            'image removed
            If Not pic.HasPhoto Then
                m_Plant = Nothing
                pic.Clear()
            Else
                m_Plant._Photo = DocumentHandler.ImageToByteArray(pic.Image)
            End If

        Else
            'insert image into document managment
            If pic.Image IsNot Nothing Then
                m_Plant._Photo = DocumentHandler.ImageToByteArray(pic.Image)
            End If
        End If

        m_Plant.Store()

        BuildTitleText()

    End Sub

    Private Sub BuildTitleText()
        Me.Text = "Plant (" + m_Plant._AjNo.ToString + ") " + m_Plant._Name
    End Sub

    'Private Sub DisplayVersionsGrid()

    '    Dim _SQL As String = ""
    '    _SQL += "select ID, ver_date, ver_reason, ver_value, ver_user, ver_stamp, ver_notes"
    '    _SQL += " from JobVersions"
    '    _SQL += " where job_id = " + m_PlantIDQuoted
    '    _SQL += " order by ver_stamp desc"

    '    cgVersions.HideFirstColumn = True
    '    cgVersions.Populate(Session.ConnectionString, _SQL)

    '    cgVersions.Columns("ver_date").Caption = "Date"
    '    cgVersions.Columns("ver_reason").Caption = "Reason"
    '    cgVersions.Columns("ver_value").Caption = "Value"
    '    cgVersions.Columns("ver_user").Caption = "User"
    '    cgVersions.Columns("ver_stamp").Caption = "Stamp"

    '    cgVersions.Columns("ver_notes").Visible = False
    '    cgVersions.PreviewColumn = "ver_notes"

    'End Sub

    Private Sub btnDeploy_Click(sender As Object, e As EventArgs) Handles btnDeploy.Click
        m_Plant.BookToJob(Today)
        DisplayLocation
    End Sub

    Private Sub DisplayLocation()

        txtLocation.Text = m_Plant._Location

        If m_Plant._LocationStamp.HasValue Then
            txtLocationStamp.Text = Format(m_Plant._LocationStamp, "dd/MM/yyyy HH:mm")
        Else
            txtLocationStamp.Text = ""
        End If

    End Sub

    Private Sub btnBackInYard_Click(sender As Object, e As EventArgs) Handles btnBackInYard.Click
        m_Plant.BackInYard(Today)
        DisplayLocation()
    End Sub
End Class
