﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPlant
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.txtNo2 = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel12 = New Care.Controls.CareLabel(Me.components)
        Me.txtNo1 = New Care.Controls.CareTextBox(Me.components)
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.txtNickname = New Care.Controls.CareTextBox(Me.components)
        Me.cbxArea = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.cbxPlantCat = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.txtRegNo = New Care.Controls.CareTextBox(Me.components)
        Me.cbxPlantType = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel10 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.txtName = New Care.Controls.CareTextBox(Me.components)
        Me.txtAJNo = New Care.Controls.CareTextBox(Me.components)
        Me.ctMain = New Care.Controls.CareTab(Me.components)
        Me.tabPlant = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl9 = New DevExpress.XtraEditors.GroupControl()
        Me.chkHire = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel29 = New Care.Controls.CareLabel(Me.components)
        Me.txtHireCost = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel30 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl8 = New DevExpress.XtraEditors.GroupControl()
        Me.txtLocationStamp = New Care.Controls.CareTextBox(Me.components)
        Me.txtLocation = New Care.Controls.CareTextBox(Me.components)
        Me.GroupControl7 = New DevExpress.XtraEditors.GroupControl()
        Me.CareLabel22 = New Care.Controls.CareLabel(Me.components)
        Me.txtLastCheck = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel21 = New Care.Controls.CareLabel(Me.components)
        Me.txtLastMaint = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel20 = New Care.Controls.CareLabel(Me.components)
        Me.txtLastTax = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel18 = New Care.Controls.CareLabel(Me.components)
        Me.txtLastMOT = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel19 = New Care.Controls.CareLabel(Me.components)
        Me.txtLastService = New Care.Controls.CareTextBox(Me.components)
        Me.GroupControl6 = New DevExpress.XtraEditors.GroupControl()
        Me.cbxRecovery = New Care.Controls.CareComboBox(Me.components)
        Me.cbxInsurer = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel24 = New Care.Controls.CareLabel(Me.components)
        Me.chkTracker = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel17 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.cdtPurcDate = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel23 = New Care.Controls.CareLabel(Me.components)
        Me.txtPurcCost = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel13 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel15 = New Care.Controls.CareLabel(Me.components)
        Me.txtPurcEbay = New Care.Controls.CareTextBox(Me.components)
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.pic = New Care.Controls.PhotoControl()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.cbxMake = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel16 = New Care.Controls.CareLabel(Me.components)
        Me.txtYear = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.txtModel = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.txtMark = New Care.Controls.CareTextBox(Me.components)
        Me.tabService = New DevExpress.XtraTab.XtraTabPage()
        Me.CareGrid2 = New Care.Controls.CareGrid()
        Me.tabHire = New DevExpress.XtraTab.XtraTabPage()
        Me.cgHire = New Care.Controls.CareGrid()
        Me.tabActivity = New DevExpress.XtraTab.XtraTabPage()
        Me.cgActivity = New Care.Controls.CareGrid()
        Me.tabInvoices = New DevExpress.XtraTab.XtraTabPage()
        Me.cgInvoices = New Care.Controls.CareGrid()
        Me.tabScans = New DevExpress.XtraTab.XtraTabPage()
        Me.CareGrid1 = New Care.Controls.CareGrid()
        Me.btnDeploy = New Care.Controls.CareButton(Me.components)
        Me.btnBackInYard = New Care.Controls.CareButton(Me.components)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.txtNo2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNo1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtNickname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxArea.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxPlantCat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRegNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxPlantType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAJNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ctMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ctMain.SuspendLayout()
        Me.tabPlant.SuspendLayout()
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl9.SuspendLayout()
        CType(Me.chkHire.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtHireCost.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl8.SuspendLayout()
        CType(Me.txtLocationStamp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLocation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl7.SuspendLayout()
        CType(Me.txtLastCheck.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLastMaint.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLastTax.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLastMOT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLastService.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl6.SuspendLayout()
        CType(Me.cbxRecovery.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxInsurer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkTracker.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.cdtPurcDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtPurcDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPurcCost.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPurcEbay.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.cbxMake.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtModel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMark.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabService.SuspendLayout()
        Me.tabHire.SuspendLayout()
        Me.tabActivity.SuspendLayout()
        Me.tabInvoices.SuspendLayout()
        Me.tabScans.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(757, 3)
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(666, 3)
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnBackInYard)
        Me.Panel1.Controls.Add(Me.btnDeploy)
        Me.Panel1.Location = New System.Drawing.Point(0, 517)
        Me.Panel1.Size = New System.Drawing.Size(854, 36)
        Me.Panel1.TabIndex = 1
        Me.Panel1.Controls.SetChildIndex(Me.btnOK, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnCancel, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnDeploy, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnBackInYard, 0)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.CareLabel4)
        Me.GroupControl4.Controls.Add(Me.txtNo2)
        Me.GroupControl4.Controls.Add(Me.CareLabel12)
        Me.GroupControl4.Controls.Add(Me.txtNo1)
        Me.GroupControl4.Location = New System.Drawing.Point(7, 128)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(334, 86)
        Me.GroupControl4.TabIndex = 1
        Me.GroupControl4.Text = "Chassis / Serial Numbers"
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(9, 58)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(53, 15)
        Me.CareLabel4.TabIndex = 2
        Me.CareLabel4.Text = "Number 2"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNo2
        '
        Me.txtNo2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNo2.EditValue = ""
        Me.txtNo2.EnterMoveNextControl = True
        Me.txtNo2.Location = New System.Drawing.Point(85, 55)
        Me.txtNo2.MaxLength = 20
        Me.txtNo2.Name = "txtNo2"
        Me.txtNo2.NumericAllowNegatives = False
        Me.txtNo2.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtNo2.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtNo2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtNo2.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtNo2.Properties.Appearance.Options.UseFont = True
        Me.txtNo2.Properties.Appearance.Options.UseTextOptions = True
        Me.txtNo2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtNo2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNo2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtNo2.Properties.MaxLength = 20
        Me.txtNo2.Size = New System.Drawing.Size(240, 22)
        Me.txtNo2.TabIndex = 3
        Me.txtNo2.Tag = "AE"
        Me.txtNo2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNo2.ToolTipText = ""
        '
        'CareLabel12
        '
        Me.CareLabel12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel12.Location = New System.Drawing.Point(9, 30)
        Me.CareLabel12.Name = "CareLabel12"
        Me.CareLabel12.Size = New System.Drawing.Size(53, 15)
        Me.CareLabel12.TabIndex = 0
        Me.CareLabel12.Text = "Number 1"
        Me.CareLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNo1
        '
        Me.txtNo1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNo1.EditValue = ""
        Me.txtNo1.EnterMoveNextControl = True
        Me.txtNo1.Location = New System.Drawing.Point(85, 27)
        Me.txtNo1.MaxLength = 20
        Me.txtNo1.Name = "txtNo1"
        Me.txtNo1.NumericAllowNegatives = False
        Me.txtNo1.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtNo1.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtNo1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtNo1.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtNo1.Properties.Appearance.Options.UseFont = True
        Me.txtNo1.Properties.Appearance.Options.UseTextOptions = True
        Me.txtNo1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtNo1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNo1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtNo1.Properties.MaxLength = 20
        Me.txtNo1.Size = New System.Drawing.Size(240, 22)
        Me.txtNo1.TabIndex = 1
        Me.txtNo1.Tag = "AE"
        Me.txtNo1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNo1.ToolTipText = ""
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.CareLabel5)
        Me.GroupControl1.Controls.Add(Me.txtNickname)
        Me.GroupControl1.Controls.Add(Me.cbxArea)
        Me.GroupControl1.Controls.Add(Me.CareLabel6)
        Me.GroupControl1.Controls.Add(Me.cbxPlantCat)
        Me.GroupControl1.Controls.Add(Me.CareLabel7)
        Me.GroupControl1.Controls.Add(Me.CareLabel8)
        Me.GroupControl1.Controls.Add(Me.txtRegNo)
        Me.GroupControl1.Controls.Add(Me.cbxPlantType)
        Me.GroupControl1.Controls.Add(Me.CareLabel9)
        Me.GroupControl1.Controls.Add(Me.CareLabel10)
        Me.GroupControl1.Controls.Add(Me.CareLabel14)
        Me.GroupControl1.Controls.Add(Me.txtName)
        Me.GroupControl1.Controls.Add(Me.txtAJNo)
        Me.GroupControl1.Location = New System.Drawing.Point(7, 7)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(619, 115)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "General Information"
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(9, 86)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(54, 15)
        Me.CareLabel5.TabIndex = 6
        Me.CareLabel5.Text = "Nickname"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNickname
        '
        Me.txtNickname.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtNickname.EditValue = ""
        Me.txtNickname.EnterMoveNextControl = True
        Me.txtNickname.Location = New System.Drawing.Point(85, 83)
        Me.txtNickname.MaxLength = 30
        Me.txtNickname.Name = "txtNickname"
        Me.txtNickname.NumericAllowNegatives = False
        Me.txtNickname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtNickname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtNickname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtNickname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtNickname.Properties.Appearance.Options.UseFont = True
        Me.txtNickname.Properties.Appearance.Options.UseTextOptions = True
        Me.txtNickname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtNickname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtNickname.Properties.MaxLength = 30
        Me.txtNickname.Size = New System.Drawing.Size(240, 22)
        Me.txtNickname.TabIndex = 7
        Me.txtNickname.Tag = "AE"
        Me.txtNickname.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNickname.ToolTipText = ""
        '
        'cbxArea
        '
        Me.cbxArea.AllowBlank = False
        Me.cbxArea.DataSource = Nothing
        Me.cbxArea.DisplayMember = Nothing
        Me.cbxArea.EnterMoveNextControl = True
        Me.cbxArea.Location = New System.Drawing.Point(432, 83)
        Me.cbxArea.Name = "cbxArea"
        Me.cbxArea.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxArea.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxArea.Properties.Appearance.Options.UseFont = True
        Me.cbxArea.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxArea.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxArea.SelectedValue = Nothing
        Me.cbxArea.Size = New System.Drawing.Size(177, 22)
        Me.cbxArea.TabIndex = 13
        Me.cbxArea.Tag = "AEM"
        Me.cbxArea.ValueMember = Nothing
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(348, 86)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(24, 15)
        Me.CareLabel6.TabIndex = 12
        Me.CareLabel6.Text = "Area"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxPlantCat
        '
        Me.cbxPlantCat.AllowBlank = False
        Me.cbxPlantCat.DataSource = Nothing
        Me.cbxPlantCat.DisplayMember = Nothing
        Me.cbxPlantCat.EnterMoveNextControl = True
        Me.cbxPlantCat.Location = New System.Drawing.Point(432, 55)
        Me.cbxPlantCat.Name = "cbxPlantCat"
        Me.cbxPlantCat.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxPlantCat.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxPlantCat.Properties.Appearance.Options.UseFont = True
        Me.cbxPlantCat.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxPlantCat.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxPlantCat.SelectedValue = Nothing
        Me.cbxPlantCat.Size = New System.Drawing.Size(177, 22)
        Me.cbxPlantCat.TabIndex = 11
        Me.cbxPlantCat.Tag = "AEM"
        Me.cbxPlantCat.ValueMember = Nothing
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(348, 58)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(78, 15)
        Me.CareLabel7.TabIndex = 10
        Me.CareLabel7.Text = "Plant Category"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(180, 30)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(39, 15)
        Me.CareLabel8.TabIndex = 2
        Me.CareLabel8.Text = "Reg No"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRegNo
        '
        Me.txtRegNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRegNo.EditValue = ""
        Me.txtRegNo.EnterMoveNextControl = True
        Me.txtRegNo.Location = New System.Drawing.Point(225, 27)
        Me.txtRegNo.MaxLength = 10
        Me.txtRegNo.Name = "txtRegNo"
        Me.txtRegNo.NumericAllowNegatives = False
        Me.txtRegNo.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRegNo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRegNo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRegNo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRegNo.Properties.Appearance.Options.UseFont = True
        Me.txtRegNo.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRegNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRegNo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRegNo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRegNo.Properties.MaxLength = 10
        Me.txtRegNo.Size = New System.Drawing.Size(100, 22)
        Me.txtRegNo.TabIndex = 3
        Me.txtRegNo.Tag = "AE"
        Me.txtRegNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRegNo.ToolTipText = ""
        '
        'cbxPlantType
        '
        Me.cbxPlantType.AllowBlank = False
        Me.cbxPlantType.DataSource = Nothing
        Me.cbxPlantType.DisplayMember = Nothing
        Me.cbxPlantType.EnterMoveNextControl = True
        Me.cbxPlantType.Location = New System.Drawing.Point(432, 27)
        Me.cbxPlantType.Name = "cbxPlantType"
        Me.cbxPlantType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxPlantType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxPlantType.Properties.Appearance.Options.UseFont = True
        Me.cbxPlantType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxPlantType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxPlantType.SelectedValue = Nothing
        Me.cbxPlantType.Size = New System.Drawing.Size(177, 22)
        Me.cbxPlantType.TabIndex = 9
        Me.cbxPlantType.Tag = "AEM"
        Me.cbxPlantType.ValueMember = Nothing
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(348, 30)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(56, 15)
        Me.CareLabel9.TabIndex = 8
        Me.CareLabel9.Text = "Plant Type"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel10
        '
        Me.CareLabel10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel10.Location = New System.Drawing.Point(9, 58)
        Me.CareLabel10.Name = "CareLabel10"
        Me.CareLabel10.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel10.TabIndex = 4
        Me.CareLabel10.Text = "Name"
        Me.CareLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(9, 30)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(31, 15)
        Me.CareLabel14.TabIndex = 0
        Me.CareLabel14.Text = "AJ No"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtName.EditValue = ""
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(85, 55)
        Me.txtName.MaxLength = 100
        Me.txtName.Name = "txtName"
        Me.txtName.NumericAllowNegatives = False
        Me.txtName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtName.Properties.MaxLength = 100
        Me.txtName.Size = New System.Drawing.Size(240, 22)
        Me.txtName.TabIndex = 5
        Me.txtName.Tag = "AEM"
        Me.txtName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'txtAJNo
        '
        Me.txtAJNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtAJNo.EditValue = ""
        Me.txtAJNo.EnterMoveNextControl = True
        Me.txtAJNo.Location = New System.Drawing.Point(85, 27)
        Me.txtAJNo.MaxLength = 5
        Me.txtAJNo.Name = "txtAJNo"
        Me.txtAJNo.NumericAllowNegatives = False
        Me.txtAJNo.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtAJNo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtAJNo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtAJNo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtAJNo.Properties.Appearance.Options.UseFont = True
        Me.txtAJNo.Properties.Appearance.Options.UseTextOptions = True
        Me.txtAJNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtAJNo.Properties.Mask.EditMask = "##########;"
        Me.txtAJNo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtAJNo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtAJNo.Properties.MaxLength = 5
        Me.txtAJNo.Size = New System.Drawing.Size(61, 22)
        Me.txtAJNo.TabIndex = 1
        Me.txtAJNo.Tag = "AM"
        Me.txtAJNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtAJNo.ToolTipText = ""
        '
        'ctMain
        '
        Me.ctMain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ctMain.Location = New System.Drawing.Point(12, 53)
        Me.ctMain.Name = "ctMain"
        Me.ctMain.SelectedTabPage = Me.tabPlant
        Me.ctMain.Size = New System.Drawing.Size(835, 461)
        Me.ctMain.TabIndex = 0
        Me.ctMain.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabPlant, Me.tabService, Me.tabHire, Me.tabActivity, Me.tabInvoices, Me.tabScans})
        '
        'tabPlant
        '
        Me.tabPlant.Controls.Add(Me.GroupControl9)
        Me.tabPlant.Controls.Add(Me.GroupControl8)
        Me.tabPlant.Controls.Add(Me.GroupControl7)
        Me.tabPlant.Controls.Add(Me.GroupControl6)
        Me.tabPlant.Controls.Add(Me.GroupControl5)
        Me.tabPlant.Controls.Add(Me.GroupControl3)
        Me.tabPlant.Controls.Add(Me.GroupControl2)
        Me.tabPlant.Controls.Add(Me.GroupControl1)
        Me.tabPlant.Controls.Add(Me.GroupControl4)
        Me.tabPlant.Name = "tabPlant"
        Me.tabPlant.Size = New System.Drawing.Size(829, 433)
        Me.tabPlant.Text = "Plant"
        '
        'GroupControl9
        '
        Me.GroupControl9.Controls.Add(Me.chkHire)
        Me.GroupControl9.Controls.Add(Me.CareLabel29)
        Me.GroupControl9.Controls.Add(Me.txtHireCost)
        Me.GroupControl9.Controls.Add(Me.CareLabel30)
        Me.GroupControl9.Location = New System.Drawing.Point(632, 306)
        Me.GroupControl9.Name = "GroupControl9"
        Me.GroupControl9.Size = New System.Drawing.Size(189, 120)
        Me.GroupControl9.TabIndex = 8
        Me.GroupControl9.Text = "Hire"
        '
        'chkHire
        '
        Me.chkHire.EnterMoveNextControl = True
        Me.chkHire.Location = New System.Drawing.Point(64, 28)
        Me.chkHire.Name = "chkHire"
        Me.chkHire.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkHire.Properties.Appearance.Options.UseFont = True
        Me.chkHire.Properties.AutoWidth = True
        Me.chkHire.Properties.Caption = ""
        Me.chkHire.Size = New System.Drawing.Size(19, 19)
        Me.chkHire.TabIndex = 1
        Me.chkHire.Tag = "AE"
        '
        'CareLabel29
        '
        Me.CareLabel29.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel29.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel29.Location = New System.Drawing.Point(8, 58)
        Me.CareLabel29.Name = "CareLabel29"
        Me.CareLabel29.Size = New System.Drawing.Size(49, 15)
        Me.CareLabel29.TabIndex = 2
        Me.CareLabel29.Text = "Day Price"
        Me.CareLabel29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtHireCost
        '
        Me.txtHireCost.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtHireCost.EditValue = ""
        Me.txtHireCost.EnterMoveNextControl = True
        Me.txtHireCost.Location = New System.Drawing.Point(64, 55)
        Me.txtHireCost.MaxLength = 14
        Me.txtHireCost.Name = "txtHireCost"
        Me.txtHireCost.NumericAllowNegatives = False
        Me.txtHireCost.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtHireCost.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtHireCost.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtHireCost.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtHireCost.Properties.Appearance.Options.UseFont = True
        Me.txtHireCost.Properties.Appearance.Options.UseTextOptions = True
        Me.txtHireCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtHireCost.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtHireCost.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtHireCost.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtHireCost.Properties.MaxLength = 14
        Me.txtHireCost.Size = New System.Drawing.Size(116, 22)
        Me.txtHireCost.TabIndex = 3
        Me.txtHireCost.Tag = "AE"
        Me.txtHireCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtHireCost.ToolTipText = ""
        '
        'CareLabel30
        '
        Me.CareLabel30.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel30.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel30.Location = New System.Drawing.Point(8, 30)
        Me.CareLabel30.Name = "CareLabel30"
        Me.CareLabel30.Size = New System.Drawing.Size(34, 15)
        Me.CareLabel30.TabIndex = 0
        Me.CareLabel30.Text = "Hired?"
        Me.CareLabel30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl8
        '
        Me.GroupControl8.Controls.Add(Me.txtLocationStamp)
        Me.GroupControl8.Controls.Add(Me.txtLocation)
        Me.GroupControl8.Location = New System.Drawing.Point(632, 7)
        Me.GroupControl8.Name = "GroupControl8"
        Me.GroupControl8.Size = New System.Drawing.Size(189, 115)
        Me.GroupControl8.TabIndex = 6
        Me.GroupControl8.Text = "Last Known Location"
        '
        'txtLocationStamp
        '
        Me.txtLocationStamp.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtLocationStamp.EditValue = ""
        Me.txtLocationStamp.EnterMoveNextControl = True
        Me.txtLocationStamp.Location = New System.Drawing.Point(8, 55)
        Me.txtLocationStamp.MaxLength = 0
        Me.txtLocationStamp.Name = "txtLocationStamp"
        Me.txtLocationStamp.NumericAllowNegatives = False
        Me.txtLocationStamp.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtLocationStamp.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLocationStamp.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtLocationStamp.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtLocationStamp.Properties.Appearance.Options.UseFont = True
        Me.txtLocationStamp.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLocationStamp.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtLocationStamp.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtLocationStamp.Size = New System.Drawing.Size(172, 22)
        Me.txtLocationStamp.TabIndex = 2
        Me.txtLocationStamp.Tag = "R"
        Me.txtLocationStamp.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtLocationStamp.ToolTipText = ""
        '
        'txtLocation
        '
        Me.txtLocation.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtLocation.EditValue = ""
        Me.txtLocation.EnterMoveNextControl = True
        Me.txtLocation.Location = New System.Drawing.Point(8, 27)
        Me.txtLocation.MaxLength = 0
        Me.txtLocation.Name = "txtLocation"
        Me.txtLocation.NumericAllowNegatives = False
        Me.txtLocation.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtLocation.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLocation.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtLocation.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtLocation.Properties.Appearance.Options.UseFont = True
        Me.txtLocation.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLocation.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtLocation.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtLocation.Size = New System.Drawing.Size(172, 22)
        Me.txtLocation.TabIndex = 1
        Me.txtLocation.Tag = "R"
        Me.txtLocation.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtLocation.ToolTipText = ""
        '
        'GroupControl7
        '
        Me.GroupControl7.Controls.Add(Me.CareLabel22)
        Me.GroupControl7.Controls.Add(Me.txtLastCheck)
        Me.GroupControl7.Controls.Add(Me.CareLabel21)
        Me.GroupControl7.Controls.Add(Me.txtLastMaint)
        Me.GroupControl7.Controls.Add(Me.CareLabel20)
        Me.GroupControl7.Controls.Add(Me.txtLastTax)
        Me.GroupControl7.Controls.Add(Me.CareLabel18)
        Me.GroupControl7.Controls.Add(Me.txtLastMOT)
        Me.GroupControl7.Controls.Add(Me.CareLabel19)
        Me.GroupControl7.Controls.Add(Me.txtLastService)
        Me.GroupControl7.Location = New System.Drawing.Point(632, 128)
        Me.GroupControl7.Name = "GroupControl7"
        Me.GroupControl7.Size = New System.Drawing.Size(189, 169)
        Me.GroupControl7.TabIndex = 7
        Me.GroupControl7.Text = "Last Activity"
        '
        'CareLabel22
        '
        Me.CareLabel22.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel22.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel22.Location = New System.Drawing.Point(8, 142)
        Me.CareLabel22.Name = "CareLabel22"
        Me.CareLabel22.Size = New System.Drawing.Size(33, 15)
        Me.CareLabel22.TabIndex = 8
        Me.CareLabel22.Text = "Check"
        Me.CareLabel22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLastCheck
        '
        Me.txtLastCheck.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtLastCheck.EditValue = ""
        Me.txtLastCheck.EnterMoveNextControl = True
        Me.txtLastCheck.Location = New System.Drawing.Point(64, 139)
        Me.txtLastCheck.MaxLength = 0
        Me.txtLastCheck.Name = "txtLastCheck"
        Me.txtLastCheck.NumericAllowNegatives = False
        Me.txtLastCheck.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtLastCheck.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLastCheck.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtLastCheck.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtLastCheck.Properties.Appearance.Options.UseFont = True
        Me.txtLastCheck.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLastCheck.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtLastCheck.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtLastCheck.Size = New System.Drawing.Size(116, 22)
        Me.txtLastCheck.TabIndex = 9
        Me.txtLastCheck.Tag = "R"
        Me.txtLastCheck.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtLastCheck.ToolTipText = ""
        '
        'CareLabel21
        '
        Me.CareLabel21.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel21.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel21.Location = New System.Drawing.Point(8, 114)
        Me.CareLabel21.Name = "CareLabel21"
        Me.CareLabel21.Size = New System.Drawing.Size(34, 15)
        Me.CareLabel21.TabIndex = 6
        Me.CareLabel21.Text = "Maint."
        Me.CareLabel21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLastMaint
        '
        Me.txtLastMaint.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtLastMaint.EditValue = ""
        Me.txtLastMaint.EnterMoveNextControl = True
        Me.txtLastMaint.Location = New System.Drawing.Point(64, 111)
        Me.txtLastMaint.MaxLength = 0
        Me.txtLastMaint.Name = "txtLastMaint"
        Me.txtLastMaint.NumericAllowNegatives = False
        Me.txtLastMaint.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtLastMaint.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLastMaint.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtLastMaint.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtLastMaint.Properties.Appearance.Options.UseFont = True
        Me.txtLastMaint.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLastMaint.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtLastMaint.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtLastMaint.Size = New System.Drawing.Size(116, 22)
        Me.txtLastMaint.TabIndex = 7
        Me.txtLastMaint.Tag = "R"
        Me.txtLastMaint.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtLastMaint.ToolTipText = ""
        '
        'CareLabel20
        '
        Me.CareLabel20.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel20.Location = New System.Drawing.Point(8, 86)
        Me.CareLabel20.Name = "CareLabel20"
        Me.CareLabel20.Size = New System.Drawing.Size(18, 15)
        Me.CareLabel20.TabIndex = 4
        Me.CareLabel20.Text = "Tax"
        Me.CareLabel20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLastTax
        '
        Me.txtLastTax.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtLastTax.EditValue = ""
        Me.txtLastTax.EnterMoveNextControl = True
        Me.txtLastTax.Location = New System.Drawing.Point(64, 83)
        Me.txtLastTax.MaxLength = 0
        Me.txtLastTax.Name = "txtLastTax"
        Me.txtLastTax.NumericAllowNegatives = False
        Me.txtLastTax.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtLastTax.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLastTax.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtLastTax.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtLastTax.Properties.Appearance.Options.UseFont = True
        Me.txtLastTax.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLastTax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtLastTax.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtLastTax.Size = New System.Drawing.Size(116, 22)
        Me.txtLastTax.TabIndex = 5
        Me.txtLastTax.Tag = "R"
        Me.txtLastTax.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtLastTax.ToolTipText = ""
        '
        'CareLabel18
        '
        Me.CareLabel18.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel18.Location = New System.Drawing.Point(8, 58)
        Me.CareLabel18.Name = "CareLabel18"
        Me.CareLabel18.Size = New System.Drawing.Size(27, 15)
        Me.CareLabel18.TabIndex = 2
        Me.CareLabel18.Text = "MOT"
        Me.CareLabel18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLastMOT
        '
        Me.txtLastMOT.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtLastMOT.EditValue = ""
        Me.txtLastMOT.EnterMoveNextControl = True
        Me.txtLastMOT.Location = New System.Drawing.Point(64, 55)
        Me.txtLastMOT.MaxLength = 0
        Me.txtLastMOT.Name = "txtLastMOT"
        Me.txtLastMOT.NumericAllowNegatives = False
        Me.txtLastMOT.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtLastMOT.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLastMOT.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtLastMOT.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtLastMOT.Properties.Appearance.Options.UseFont = True
        Me.txtLastMOT.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLastMOT.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtLastMOT.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtLastMOT.Size = New System.Drawing.Size(116, 22)
        Me.txtLastMOT.TabIndex = 3
        Me.txtLastMOT.Tag = "R"
        Me.txtLastMOT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtLastMOT.ToolTipText = ""
        '
        'CareLabel19
        '
        Me.CareLabel19.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel19.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel19.Location = New System.Drawing.Point(8, 30)
        Me.CareLabel19.Name = "CareLabel19"
        Me.CareLabel19.Size = New System.Drawing.Size(37, 15)
        Me.CareLabel19.TabIndex = 0
        Me.CareLabel19.Text = "Service"
        Me.CareLabel19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLastService
        '
        Me.txtLastService.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtLastService.EditValue = ""
        Me.txtLastService.EnterMoveNextControl = True
        Me.txtLastService.Location = New System.Drawing.Point(64, 27)
        Me.txtLastService.MaxLength = 0
        Me.txtLastService.Name = "txtLastService"
        Me.txtLastService.NumericAllowNegatives = False
        Me.txtLastService.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtLastService.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLastService.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtLastService.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtLastService.Properties.Appearance.Options.UseFont = True
        Me.txtLastService.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLastService.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtLastService.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtLastService.Size = New System.Drawing.Size(116, 22)
        Me.txtLastService.TabIndex = 1
        Me.txtLastService.Tag = "R"
        Me.txtLastService.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtLastService.ToolTipText = ""
        '
        'GroupControl6
        '
        Me.GroupControl6.Controls.Add(Me.cbxRecovery)
        Me.GroupControl6.Controls.Add(Me.cbxInsurer)
        Me.GroupControl6.Controls.Add(Me.CareLabel24)
        Me.GroupControl6.Controls.Add(Me.chkTracker)
        Me.GroupControl6.Controls.Add(Me.CareLabel11)
        Me.GroupControl6.Controls.Add(Me.CareLabel17)
        Me.GroupControl6.Location = New System.Drawing.Point(347, 128)
        Me.GroupControl6.Name = "GroupControl6"
        Me.GroupControl6.Size = New System.Drawing.Size(279, 106)
        Me.GroupControl6.TabIndex = 4
        Me.GroupControl6.Text = "Tax, Insurance and Tracking"
        '
        'cbxRecovery
        '
        Me.cbxRecovery.AllowBlank = False
        Me.cbxRecovery.DataSource = Nothing
        Me.cbxRecovery.DisplayMember = Nothing
        Me.cbxRecovery.EnterMoveNextControl = True
        Me.cbxRecovery.Location = New System.Drawing.Point(92, 55)
        Me.cbxRecovery.Name = "cbxRecovery"
        Me.cbxRecovery.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxRecovery.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxRecovery.Properties.Appearance.Options.UseFont = True
        Me.cbxRecovery.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxRecovery.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxRecovery.SelectedValue = Nothing
        Me.cbxRecovery.Size = New System.Drawing.Size(177, 22)
        Me.cbxRecovery.TabIndex = 3
        Me.cbxRecovery.Tag = "AE"
        Me.cbxRecovery.ValueMember = Nothing
        '
        'cbxInsurer
        '
        Me.cbxInsurer.AllowBlank = False
        Me.cbxInsurer.DataSource = Nothing
        Me.cbxInsurer.DisplayMember = Nothing
        Me.cbxInsurer.EnterMoveNextControl = True
        Me.cbxInsurer.Location = New System.Drawing.Point(92, 27)
        Me.cbxInsurer.Name = "cbxInsurer"
        Me.cbxInsurer.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxInsurer.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxInsurer.Properties.Appearance.Options.UseFont = True
        Me.cbxInsurer.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxInsurer.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxInsurer.SelectedValue = Nothing
        Me.cbxInsurer.Size = New System.Drawing.Size(177, 22)
        Me.cbxInsurer.TabIndex = 1
        Me.cbxInsurer.Tag = "AE"
        Me.cbxInsurer.ValueMember = Nothing
        '
        'CareLabel24
        '
        Me.CareLabel24.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel24.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel24.Location = New System.Drawing.Point(8, 85)
        Me.CareLabel24.Name = "CareLabel24"
        Me.CareLabel24.Size = New System.Drawing.Size(42, 15)
        Me.CareLabel24.TabIndex = 4
        Me.CareLabel24.Text = "Tracked"
        Me.CareLabel24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkTracker
        '
        Me.chkTracker.EnterMoveNextControl = True
        Me.chkTracker.Location = New System.Drawing.Point(92, 83)
        Me.chkTracker.Name = "chkTracker"
        Me.chkTracker.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkTracker.Properties.Appearance.Options.UseFont = True
        Me.chkTracker.Properties.AutoWidth = True
        Me.chkTracker.Properties.Caption = ""
        Me.chkTracker.Size = New System.Drawing.Size(19, 19)
        Me.chkTracker.TabIndex = 5
        Me.chkTracker.Tag = "AE"
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(8, 58)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(48, 15)
        Me.CareLabel11.TabIndex = 2
        Me.CareLabel11.Text = "Recovery"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel17
        '
        Me.CareLabel17.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel17.Location = New System.Drawing.Point(8, 30)
        Me.CareLabel17.Name = "CareLabel17"
        Me.CareLabel17.Size = New System.Drawing.Size(36, 15)
        Me.CareLabel17.TabIndex = 0
        Me.CareLabel17.Text = "Insurer"
        Me.CareLabel17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl5
        '
        Me.GroupControl5.Controls.Add(Me.cdtPurcDate)
        Me.GroupControl5.Controls.Add(Me.CareLabel23)
        Me.GroupControl5.Controls.Add(Me.txtPurcCost)
        Me.GroupControl5.Controls.Add(Me.CareLabel13)
        Me.GroupControl5.Controls.Add(Me.CareLabel15)
        Me.GroupControl5.Controls.Add(Me.txtPurcEbay)
        Me.GroupControl5.Location = New System.Drawing.Point(7, 341)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(334, 85)
        Me.GroupControl5.TabIndex = 3
        Me.GroupControl5.Text = "Purchase Details"
        '
        'cdtPurcDate
        '
        Me.cdtPurcDate.EditValue = Nothing
        Me.cdtPurcDate.EnterMoveNextControl = True
        Me.cdtPurcDate.Location = New System.Drawing.Point(85, 27)
        Me.cdtPurcDate.Name = "cdtPurcDate"
        Me.cdtPurcDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtPurcDate.Properties.Appearance.Options.UseFont = True
        Me.cdtPurcDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtPurcDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtPurcDate.Size = New System.Drawing.Size(99, 22)
        Me.cdtPurcDate.TabIndex = 1
        Me.cdtPurcDate.Tag = "AE"
        Me.cdtPurcDate.Value = Nothing
        '
        'CareLabel23
        '
        Me.CareLabel23.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel23.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel23.Location = New System.Drawing.Point(234, 30)
        Me.CareLabel23.Name = "CareLabel23"
        Me.CareLabel23.Size = New System.Drawing.Size(24, 15)
        Me.CareLabel23.TabIndex = 2
        Me.CareLabel23.Text = "Cost"
        Me.CareLabel23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPurcCost
        '
        Me.txtPurcCost.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtPurcCost.EditValue = ""
        Me.txtPurcCost.EnterMoveNextControl = True
        Me.txtPurcCost.Location = New System.Drawing.Point(264, 27)
        Me.txtPurcCost.MaxLength = 14
        Me.txtPurcCost.Name = "txtPurcCost"
        Me.txtPurcCost.NumericAllowNegatives = False
        Me.txtPurcCost.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtPurcCost.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPurcCost.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPurcCost.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtPurcCost.Properties.Appearance.Options.UseFont = True
        Me.txtPurcCost.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPurcCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPurcCost.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtPurcCost.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtPurcCost.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPurcCost.Properties.MaxLength = 14
        Me.txtPurcCost.Size = New System.Drawing.Size(60, 22)
        Me.txtPurcCost.TabIndex = 3
        Me.txtPurcCost.Tag = "AE"
        Me.txtPurcCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPurcCost.ToolTipText = ""
        '
        'CareLabel13
        '
        Me.CareLabel13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel13.Location = New System.Drawing.Point(8, 58)
        Me.CareLabel13.Name = "CareLabel13"
        Me.CareLabel13.Size = New System.Drawing.Size(45, 15)
        Me.CareLabel13.TabIndex = 4
        Me.CareLabel13.Text = "eBay Ref"
        Me.CareLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel15
        '
        Me.CareLabel15.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel15.Location = New System.Drawing.Point(8, 30)
        Me.CareLabel15.Name = "CareLabel15"
        Me.CareLabel15.Size = New System.Drawing.Size(24, 15)
        Me.CareLabel15.TabIndex = 0
        Me.CareLabel15.Text = "Date"
        Me.CareLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPurcEbay
        '
        Me.txtPurcEbay.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtPurcEbay.EditValue = ""
        Me.txtPurcEbay.EnterMoveNextControl = True
        Me.txtPurcEbay.Location = New System.Drawing.Point(85, 55)
        Me.txtPurcEbay.MaxLength = 20
        Me.txtPurcEbay.Name = "txtPurcEbay"
        Me.txtPurcEbay.NumericAllowNegatives = False
        Me.txtPurcEbay.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtPurcEbay.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPurcEbay.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPurcEbay.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtPurcEbay.Properties.Appearance.Options.UseFont = True
        Me.txtPurcEbay.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPurcEbay.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPurcEbay.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPurcEbay.Properties.MaxLength = 20
        Me.txtPurcEbay.Size = New System.Drawing.Size(239, 22)
        Me.txtPurcEbay.TabIndex = 5
        Me.txtPurcEbay.Tag = "AE"
        Me.txtPurcEbay.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPurcEbay.ToolTipText = ""
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.pic)
        Me.GroupControl3.Location = New System.Drawing.Point(347, 240)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(279, 186)
        Me.GroupControl3.TabIndex = 5
        Me.GroupControl3.Text = "Photo"
        '
        'pic
        '
        Me.pic.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pic.DocumentID = Nothing
        Me.pic.Image = Nothing
        Me.pic.ImageLocation = Nothing
        Me.pic.Location = New System.Drawing.Point(2, 20)
        Me.pic.Name = "pic"
        Me.pic.Padding = New System.Windows.Forms.Padding(1, 1, 0, 0)
        Me.pic.Size = New System.Drawing.Size(275, 164)
        Me.pic.TabIndex = 0
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.cbxMake)
        Me.GroupControl2.Controls.Add(Me.CareLabel16)
        Me.GroupControl2.Controls.Add(Me.txtYear)
        Me.GroupControl2.Controls.Add(Me.CareLabel1)
        Me.GroupControl2.Controls.Add(Me.txtModel)
        Me.GroupControl2.Controls.Add(Me.CareLabel2)
        Me.GroupControl2.Controls.Add(Me.CareLabel3)
        Me.GroupControl2.Controls.Add(Me.txtMark)
        Me.GroupControl2.Location = New System.Drawing.Point(7, 220)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(334, 115)
        Me.GroupControl2.TabIndex = 2
        Me.GroupControl2.Text = "Plant Details"
        '
        'cbxMake
        '
        Me.cbxMake.AllowBlank = False
        Me.cbxMake.DataSource = Nothing
        Me.cbxMake.DisplayMember = Nothing
        Me.cbxMake.EnterMoveNextControl = True
        Me.cbxMake.Location = New System.Drawing.Point(85, 27)
        Me.cbxMake.Name = "cbxMake"
        Me.cbxMake.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxMake.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxMake.Properties.Appearance.Options.UseFont = True
        Me.cbxMake.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxMake.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxMake.SelectedValue = Nothing
        Me.cbxMake.Size = New System.Drawing.Size(239, 22)
        Me.cbxMake.TabIndex = 1
        Me.cbxMake.Tag = "AEM"
        Me.cbxMake.ValueMember = Nothing
        '
        'CareLabel16
        '
        Me.CareLabel16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel16.Location = New System.Drawing.Point(8, 86)
        Me.CareLabel16.Name = "CareLabel16"
        Me.CareLabel16.Size = New System.Drawing.Size(23, 15)
        Me.CareLabel16.TabIndex = 4
        Me.CareLabel16.Text = "Year"
        Me.CareLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtYear
        '
        Me.txtYear.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtYear.EditValue = ""
        Me.txtYear.EnterMoveNextControl = True
        Me.txtYear.Location = New System.Drawing.Point(85, 83)
        Me.txtYear.MaxLength = 4
        Me.txtYear.Name = "txtYear"
        Me.txtYear.NumericAllowNegatives = False
        Me.txtYear.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtYear.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtYear.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtYear.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtYear.Properties.Appearance.Options.UseFont = True
        Me.txtYear.Properties.Appearance.Options.UseTextOptions = True
        Me.txtYear.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtYear.Properties.Mask.EditMask = "##########;"
        Me.txtYear.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtYear.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtYear.Properties.MaxLength = 4
        Me.txtYear.Size = New System.Drawing.Size(60, 22)
        Me.txtYear.TabIndex = 5
        Me.txtYear.Tag = "AE"
        Me.txtYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtYear.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(8, 58)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(34, 15)
        Me.CareLabel1.TabIndex = 2
        Me.CareLabel1.Text = "Model"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtModel
        '
        Me.txtModel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtModel.EditValue = ""
        Me.txtModel.EnterMoveNextControl = True
        Me.txtModel.Location = New System.Drawing.Point(85, 55)
        Me.txtModel.MaxLength = 40
        Me.txtModel.Name = "txtModel"
        Me.txtModel.NumericAllowNegatives = False
        Me.txtModel.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtModel.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtModel.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtModel.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtModel.Properties.Appearance.Options.UseFont = True
        Me.txtModel.Properties.Appearance.Options.UseTextOptions = True
        Me.txtModel.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtModel.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtModel.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtModel.Properties.MaxLength = 40
        Me.txtModel.Size = New System.Drawing.Size(239, 22)
        Me.txtModel.TabIndex = 3
        Me.txtModel.Tag = "AEM"
        Me.txtModel.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtModel.ToolTipText = ""
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(8, 30)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(29, 15)
        Me.CareLabel2.TabIndex = 0
        Me.CareLabel2.Text = "Make"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(194, 86)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(64, 15)
        Me.CareLabel3.TabIndex = 6
        Me.CareLabel3.Text = "Mark / Dash"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMark
        '
        Me.txtMark.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMark.EditValue = ""
        Me.txtMark.EnterMoveNextControl = True
        Me.txtMark.Location = New System.Drawing.Point(264, 83)
        Me.txtMark.MaxLength = 5
        Me.txtMark.Name = "txtMark"
        Me.txtMark.NumericAllowNegatives = False
        Me.txtMark.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtMark.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtMark.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtMark.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtMark.Properties.Appearance.Options.UseFont = True
        Me.txtMark.Properties.Appearance.Options.UseTextOptions = True
        Me.txtMark.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtMark.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMark.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtMark.Properties.MaxLength = 5
        Me.txtMark.Size = New System.Drawing.Size(60, 22)
        Me.txtMark.TabIndex = 7
        Me.txtMark.Tag = "AE"
        Me.txtMark.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtMark.ToolTipText = ""
        '
        'tabService
        '
        Me.tabService.Controls.Add(Me.CareGrid2)
        Me.tabService.Name = "tabService"
        Me.tabService.Size = New System.Drawing.Size(829, 433)
        Me.tabService.Text = "Servicing && Maintenance"
        '
        'CareGrid2
        '
        Me.CareGrid2.AllowBuildColumns = True
        Me.CareGrid2.AllowEdit = False
        Me.CareGrid2.AllowHorizontalScroll = False
        Me.CareGrid2.AllowMultiSelect = False
        Me.CareGrid2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CareGrid2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareGrid2.Appearance.Options.UseFont = True
        Me.CareGrid2.AutoSizeByData = True
        Me.CareGrid2.DisableAutoSize = False
        Me.CareGrid2.DisableDataFormatting = False
        Me.CareGrid2.FocusedRowHandle = -2147483648
        Me.CareGrid2.HideFirstColumn = False
        Me.CareGrid2.Location = New System.Drawing.Point(7, 7)
        Me.CareGrid2.Name = "CareGrid2"
        Me.CareGrid2.PreviewColumn = ""
        Me.CareGrid2.QueryID = Nothing
        Me.CareGrid2.RowAutoHeight = False
        Me.CareGrid2.SearchAsYouType = True
        Me.CareGrid2.ShowAutoFilterRow = False
        Me.CareGrid2.ShowFindPanel = False
        Me.CareGrid2.ShowGroupByBox = True
        Me.CareGrid2.ShowLoadingPanel = False
        Me.CareGrid2.ShowNavigator = False
        Me.CareGrid2.Size = New System.Drawing.Size(815, 419)
        Me.CareGrid2.TabIndex = 1
        '
        'tabHire
        '
        Me.tabHire.Controls.Add(Me.cgHire)
        Me.tabHire.Name = "tabHire"
        Me.tabHire.Size = New System.Drawing.Size(829, 433)
        Me.tabHire.Text = "Hire"
        '
        'cgHire
        '
        Me.cgHire.AllowBuildColumns = True
        Me.cgHire.AllowEdit = False
        Me.cgHire.AllowHorizontalScroll = False
        Me.cgHire.AllowMultiSelect = False
        Me.cgHire.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgHire.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgHire.Appearance.Options.UseFont = True
        Me.cgHire.AutoSizeByData = True
        Me.cgHire.DisableAutoSize = False
        Me.cgHire.DisableDataFormatting = False
        Me.cgHire.FocusedRowHandle = -2147483648
        Me.cgHire.HideFirstColumn = False
        Me.cgHire.Location = New System.Drawing.Point(7, 7)
        Me.cgHire.Name = "cgHire"
        Me.cgHire.PreviewColumn = ""
        Me.cgHire.QueryID = Nothing
        Me.cgHire.RowAutoHeight = False
        Me.cgHire.SearchAsYouType = True
        Me.cgHire.ShowAutoFilterRow = False
        Me.cgHire.ShowFindPanel = False
        Me.cgHire.ShowGroupByBox = True
        Me.cgHire.ShowLoadingPanel = False
        Me.cgHire.ShowNavigator = False
        Me.cgHire.Size = New System.Drawing.Size(815, 419)
        Me.cgHire.TabIndex = 2
        '
        'tabActivity
        '
        Me.tabActivity.Controls.Add(Me.cgActivity)
        Me.tabActivity.Name = "tabActivity"
        Me.tabActivity.Size = New System.Drawing.Size(829, 433)
        Me.tabActivity.Text = "Activity"
        '
        'cgActivity
        '
        Me.cgActivity.AllowBuildColumns = True
        Me.cgActivity.AllowEdit = False
        Me.cgActivity.AllowHorizontalScroll = False
        Me.cgActivity.AllowMultiSelect = False
        Me.cgActivity.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgActivity.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgActivity.Appearance.Options.UseFont = True
        Me.cgActivity.AutoSizeByData = True
        Me.cgActivity.DisableAutoSize = False
        Me.cgActivity.DisableDataFormatting = False
        Me.cgActivity.FocusedRowHandle = -2147483648
        Me.cgActivity.HideFirstColumn = False
        Me.cgActivity.Location = New System.Drawing.Point(7, 7)
        Me.cgActivity.Name = "cgActivity"
        Me.cgActivity.PreviewColumn = ""
        Me.cgActivity.QueryID = Nothing
        Me.cgActivity.RowAutoHeight = False
        Me.cgActivity.SearchAsYouType = True
        Me.cgActivity.ShowAutoFilterRow = False
        Me.cgActivity.ShowFindPanel = False
        Me.cgActivity.ShowGroupByBox = True
        Me.cgActivity.ShowLoadingPanel = False
        Me.cgActivity.ShowNavigator = False
        Me.cgActivity.Size = New System.Drawing.Size(815, 419)
        Me.cgActivity.TabIndex = 1
        '
        'tabInvoices
        '
        Me.tabInvoices.Controls.Add(Me.cgInvoices)
        Me.tabInvoices.Name = "tabInvoices"
        Me.tabInvoices.Size = New System.Drawing.Size(829, 433)
        Me.tabInvoices.Text = "Purchase Invoices"
        '
        'cgInvoices
        '
        Me.cgInvoices.AllowBuildColumns = True
        Me.cgInvoices.AllowEdit = False
        Me.cgInvoices.AllowHorizontalScroll = False
        Me.cgInvoices.AllowMultiSelect = False
        Me.cgInvoices.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgInvoices.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgInvoices.Appearance.Options.UseFont = True
        Me.cgInvoices.AutoSizeByData = True
        Me.cgInvoices.DisableAutoSize = False
        Me.cgInvoices.DisableDataFormatting = False
        Me.cgInvoices.FocusedRowHandle = -2147483648
        Me.cgInvoices.HideFirstColumn = False
        Me.cgInvoices.Location = New System.Drawing.Point(7, 7)
        Me.cgInvoices.Name = "cgInvoices"
        Me.cgInvoices.PreviewColumn = ""
        Me.cgInvoices.QueryID = Nothing
        Me.cgInvoices.RowAutoHeight = False
        Me.cgInvoices.SearchAsYouType = True
        Me.cgInvoices.ShowAutoFilterRow = False
        Me.cgInvoices.ShowFindPanel = False
        Me.cgInvoices.ShowGroupByBox = True
        Me.cgInvoices.ShowLoadingPanel = False
        Me.cgInvoices.ShowNavigator = False
        Me.cgInvoices.Size = New System.Drawing.Size(815, 419)
        Me.cgInvoices.TabIndex = 0
        '
        'tabScans
        '
        Me.tabScans.Controls.Add(Me.CareGrid1)
        Me.tabScans.Name = "tabScans"
        Me.tabScans.Size = New System.Drawing.Size(829, 433)
        Me.tabScans.Text = "Scans"
        '
        'CareGrid1
        '
        Me.CareGrid1.AllowBuildColumns = True
        Me.CareGrid1.AllowEdit = False
        Me.CareGrid1.AllowHorizontalScroll = False
        Me.CareGrid1.AllowMultiSelect = False
        Me.CareGrid1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CareGrid1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareGrid1.Appearance.Options.UseFont = True
        Me.CareGrid1.AutoSizeByData = True
        Me.CareGrid1.DisableAutoSize = False
        Me.CareGrid1.DisableDataFormatting = False
        Me.CareGrid1.FocusedRowHandle = -2147483648
        Me.CareGrid1.HideFirstColumn = False
        Me.CareGrid1.Location = New System.Drawing.Point(7, 7)
        Me.CareGrid1.Name = "CareGrid1"
        Me.CareGrid1.PreviewColumn = ""
        Me.CareGrid1.QueryID = Nothing
        Me.CareGrid1.RowAutoHeight = False
        Me.CareGrid1.SearchAsYouType = True
        Me.CareGrid1.ShowAutoFilterRow = False
        Me.CareGrid1.ShowFindPanel = False
        Me.CareGrid1.ShowGroupByBox = True
        Me.CareGrid1.ShowLoadingPanel = False
        Me.CareGrid1.ShowNavigator = False
        Me.CareGrid1.Size = New System.Drawing.Size(815, 419)
        Me.CareGrid1.TabIndex = 1
        '
        'btnDeploy
        '
        Me.btnDeploy.Location = New System.Drawing.Point(12, 3)
        Me.btnDeploy.Name = "btnDeploy"
        Me.btnDeploy.Size = New System.Drawing.Size(120, 25)
        Me.btnDeploy.TabIndex = 3
        Me.btnDeploy.Text = "Deploy to Job"
        '
        'btnBackInYard
        '
        Me.btnBackInYard.Location = New System.Drawing.Point(138, 3)
        Me.btnBackInYard.Name = "btnBackInYard"
        Me.btnBackInYard.Size = New System.Drawing.Size(120, 25)
        Me.btnBackInYard.TabIndex = 4
        Me.btnBackInYard.Text = "Back in Yard"
        '
        'frmPlant
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(854, 553)
        Me.Controls.Add(Me.ctMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.MaximizeBox = True
        Me.Name = "frmPlant"
        Me.Text = "frmPlant"
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.ctMain, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.txtNo2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNo1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtNickname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxArea.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxPlantCat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRegNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxPlantType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAJNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ctMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ctMain.ResumeLayout(False)
        Me.tabPlant.ResumeLayout(False)
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl9.ResumeLayout(False)
        Me.GroupControl9.PerformLayout()
        CType(Me.chkHire.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtHireCost.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl8.ResumeLayout(False)
        CType(Me.txtLocationStamp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLocation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl7.ResumeLayout(False)
        Me.GroupControl7.PerformLayout()
        CType(Me.txtLastCheck.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLastMaint.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLastTax.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLastMOT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLastService.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl6.ResumeLayout(False)
        Me.GroupControl6.PerformLayout()
        CType(Me.cbxRecovery.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxInsurer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkTracker.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.cdtPurcDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtPurcDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPurcCost.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPurcEbay.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.cbxMake.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtModel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMark.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabService.ResumeLayout(False)
        Me.tabHire.ResumeLayout(False)
        Me.tabActivity.ResumeLayout(False)
        Me.tabInvoices.ResumeLayout(False)
        Me.tabScans.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel12 As Care.Controls.CareLabel
    Friend WithEvents txtNo1 As Care.Controls.CareTextBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents txtNo2 As Care.Controls.CareTextBox
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents txtNickname As Care.Controls.CareTextBox
    Friend WithEvents cbxArea As Care.Controls.CareComboBox
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents cbxPlantCat As Care.Controls.CareComboBox
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents txtRegNo As Care.Controls.CareTextBox
    Friend WithEvents cbxPlantType As Care.Controls.CareComboBox
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents CareLabel10 As Care.Controls.CareLabel
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents txtAJNo As Care.Controls.CareTextBox
    Friend WithEvents ctMain As Care.Controls.CareTab
    Friend WithEvents tabPlant As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl7 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel22 As Care.Controls.CareLabel
    Friend WithEvents txtLastCheck As Care.Controls.CareTextBox
    Friend WithEvents CareLabel21 As Care.Controls.CareLabel
    Friend WithEvents txtLastMaint As Care.Controls.CareTextBox
    Friend WithEvents CareLabel20 As Care.Controls.CareLabel
    Friend WithEvents txtLastTax As Care.Controls.CareTextBox
    Friend WithEvents CareLabel18 As Care.Controls.CareLabel
    Friend WithEvents txtLastMOT As Care.Controls.CareTextBox
    Friend WithEvents CareLabel19 As Care.Controls.CareLabel
    Friend WithEvents txtLastService As Care.Controls.CareTextBox
    Friend WithEvents GroupControl6 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents CareLabel17 As Care.Controls.CareLabel
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel23 As Care.Controls.CareLabel
    Friend WithEvents txtPurcCost As Care.Controls.CareTextBox
    Friend WithEvents CareLabel13 As Care.Controls.CareLabel
    Friend WithEvents CareLabel15 As Care.Controls.CareLabel
    Friend WithEvents txtPurcEbay As Care.Controls.CareTextBox
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel16 As Care.Controls.CareLabel
    Friend WithEvents txtYear As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents txtModel As Care.Controls.CareTextBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents txtMark As Care.Controls.CareTextBox
    Friend WithEvents tabActivity As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cbxRecovery As Care.Controls.CareComboBox
    Friend WithEvents cbxInsurer As Care.Controls.CareComboBox
    Friend WithEvents CareLabel24 As Care.Controls.CareLabel
    Friend WithEvents chkTracker As Care.Controls.CareCheckBox
    Friend WithEvents pic As Care.Controls.PhotoControl
    Friend WithEvents tabService As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabInvoices As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cbxMake As Care.Controls.CareComboBox
    Friend WithEvents cdtPurcDate As Care.Controls.CareDateTime
    Friend WithEvents cgActivity As Care.Controls.CareGrid
    Friend WithEvents CareGrid2 As Care.Controls.CareGrid
    Friend WithEvents cgInvoices As Care.Controls.CareGrid
    Friend WithEvents tabScans As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents CareGrid1 As Care.Controls.CareGrid
    Friend WithEvents GroupControl8 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtLocationStamp As Care.Controls.CareTextBox
    Friend WithEvents txtLocation As Care.Controls.CareTextBox
    Friend WithEvents btnDeploy As Care.Controls.CareButton
    Friend WithEvents btnBackInYard As Care.Controls.CareButton
    Friend WithEvents GroupControl9 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents chkHire As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel29 As Care.Controls.CareLabel
    Friend WithEvents txtHireCost As Care.Controls.CareTextBox
    Friend WithEvents CareLabel30 As Care.Controls.CareLabel
    Friend WithEvents tabHire As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cgHire As Care.Controls.CareGrid
End Class
