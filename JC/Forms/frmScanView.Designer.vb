﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmScanView
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.pic = New DevExpress.XtraEditors.PictureEdit()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.btn60 = New Care.Controls.CareButton(Me.components)
        Me.btn80 = New Care.Controls.CareButton(Me.components)
        Me.btn40 = New Care.Controls.CareButton(Me.components)
        Me.btn100 = New Care.Controls.CareButton(Me.components)
        Me.btnRight = New Care.Controls.CareButton(Me.components)
        Me.btnLeft = New Care.Controls.CareButton(Me.components)
        Me.btnPrint = New Care.Controls.CareButton(Me.components)
        CType(Me.pic.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'pic
        '
        Me.pic.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pic.Location = New System.Drawing.Point(0, 34)
        Me.pic.Name = "pic"
        Me.pic.Properties.AllowScrollOnMouseWheel = DevExpress.Utils.DefaultBoolean.[False]
        Me.pic.Properties.AllowScrollViaMouseDrag = True
        Me.pic.Properties.AllowZoomOnMouseWheel = DevExpress.Utils.DefaultBoolean.[True]
        Me.pic.Properties.ReadOnly = True
        Me.pic.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.pic.Properties.ShowScrollBars = True
        Me.pic.Properties.ShowZoomSubMenu = DevExpress.Utils.DefaultBoolean.[True]
        Me.pic.Properties.ZoomingOperationMode = DevExpress.XtraEditors.Repository.ZoomingOperationMode.MouseWheel
        Me.pic.Size = New System.Drawing.Size(984, 627)
        Me.pic.TabIndex = 3
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.btnPrint)
        Me.GroupControl2.Controls.Add(Me.btn60)
        Me.GroupControl2.Controls.Add(Me.btn80)
        Me.GroupControl2.Controls.Add(Me.btn40)
        Me.GroupControl2.Controls.Add(Me.btn100)
        Me.GroupControl2.Controls.Add(Me.btnRight)
        Me.GroupControl2.Controls.Add(Me.btnLeft)
        Me.GroupControl2.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl2.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.ShowCaption = False
        Me.GroupControl2.Size = New System.Drawing.Size(984, 34)
        Me.GroupControl2.TabIndex = 2
        Me.GroupControl2.Text = "GroupControl2"
        '
        'btn60
        '
        Me.btn60.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn60.Location = New System.Drawing.Point(789, 6)
        Me.btn60.Name = "btn60"
        Me.btn60.Size = New System.Drawing.Size(40, 23)
        Me.btn60.TabIndex = 3
        Me.btn60.TabStop = False
        Me.btn60.Text = "60%"
        '
        'btn80
        '
        Me.btn80.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn80.Location = New System.Drawing.Point(835, 6)
        Me.btn80.Name = "btn80"
        Me.btn80.Size = New System.Drawing.Size(40, 23)
        Me.btn80.TabIndex = 7
        Me.btn80.TabStop = False
        Me.btn80.Text = "80%"
        '
        'btn40
        '
        Me.btn40.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn40.Location = New System.Drawing.Point(743, 6)
        Me.btn40.Name = "btn40"
        Me.btn40.Size = New System.Drawing.Size(40, 23)
        Me.btn40.TabIndex = 1
        Me.btn40.TabStop = False
        Me.btn40.Text = "40%"
        '
        'btn100
        '
        Me.btn100.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn100.Location = New System.Drawing.Point(881, 6)
        Me.btn100.Name = "btn100"
        Me.btn100.Size = New System.Drawing.Size(40, 23)
        Me.btn100.TabIndex = 5
        Me.btn100.TabStop = False
        Me.btn100.Text = "100%"
        '
        'btnRight
        '
        Me.btnRight.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRight.Location = New System.Drawing.Point(956, 6)
        Me.btnRight.Name = "btnRight"
        Me.btnRight.Size = New System.Drawing.Size(23, 23)
        Me.btnRight.TabIndex = 6
        Me.btnRight.TabStop = False
        Me.btnRight.Text = ">"
        '
        'btnLeft
        '
        Me.btnLeft.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnLeft.Location = New System.Drawing.Point(927, 6)
        Me.btnLeft.Name = "btnLeft"
        Me.btnLeft.Size = New System.Drawing.Size(23, 23)
        Me.btnLeft.TabIndex = 3
        Me.btnLeft.TabStop = False
        Me.btnLeft.Text = "<"
        '
        'btnPrint
        '
        Me.btnPrint.Location = New System.Drawing.Point(5, 6)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(40, 23)
        Me.btnPrint.TabIndex = 8
        Me.btnPrint.TabStop = False
        Me.btnPrint.Text = "Print"
        '
        'frmScanView
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(984, 661)
        Me.Controls.Add(Me.pic)
        Me.Controls.Add(Me.GroupControl2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.MinimumSize = New System.Drawing.Size(400, 200)
        Me.Name = "frmScanView"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Scan Viewer"
        CType(Me.pic.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents pic As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btn60 As Care.Controls.CareButton
    Friend WithEvents btn80 As Care.Controls.CareButton
    Friend WithEvents btn40 As Care.Controls.CareButton
    Friend WithEvents btn100 As Care.Controls.CareButton
    Friend WithEvents btnRight As Care.Controls.CareButton
    Friend WithEvents btnLeft As Care.Controls.CareButton
    Friend WithEvents btnPrint As Care.Controls.CareButton
End Class
