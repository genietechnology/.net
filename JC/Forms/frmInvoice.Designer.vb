﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInvoice
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.txtContactName = New Care.Controls.CareTextBox(Me.components)
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.txtInvoiceText = New DevExpress.XtraEditors.MemoEdit()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.txtNet = New Care.Controls.CareTextBox(Me.components)
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.btnFind = New Care.Controls.CareButton(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.txtCompanyName = New Care.Controls.CareTextBox(Me.components)
        Me.txtAccountNumber = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl6 = New DevExpress.XtraEditors.GroupControl()
        Me.txtAddress = New Care.Address.CareAddress()
        Me.btnCreate = New Care.Controls.CareButton(Me.components)
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.radNew = New Care.Controls.CareRadioButton(Me.components)
        Me.radFind = New Care.Controls.CareRadioButton(Me.components)
        CType(Me.txtContactName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtInvoiceText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.txtNet.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.txtCompanyName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAccountNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl6.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.radNew.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radFind.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'txtContactName
        '
        Me.txtContactName.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtContactName.EnterMoveNextControl = True
        Me.txtContactName.Location = New System.Drawing.Point(118, 84)
        Me.txtContactName.MaxLength = 0
        Me.txtContactName.Name = "txtContactName"
        Me.txtContactName.NumericAllowNegatives = False
        Me.txtContactName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtContactName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtContactName.Properties.AccessibleName = ""
        Me.txtContactName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtContactName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtContactName.Properties.Appearance.Options.UseFont = True
        Me.txtContactName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtContactName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtContactName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtContactName.Size = New System.Drawing.Size(223, 22)
        Me.txtContactName.TabIndex = 5
        Me.txtContactName.Tag = "A"
        Me.txtContactName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtContactName.ToolTipText = ""
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.txtInvoiceText)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 179)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(636, 299)
        Me.GroupControl1.TabIndex = 4
        Me.GroupControl1.Text = "Invoice Text"
        '
        'txtInvoiceText
        '
        Me.txtInvoiceText.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtInvoiceText.Location = New System.Drawing.Point(5, 23)
        Me.txtInvoiceText.Name = "txtInvoiceText"
        Me.txtInvoiceText.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInvoiceText.Properties.Appearance.Options.UseFont = True
        Me.txtInvoiceText.Properties.MaxLength = 1024
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtInvoiceText, True)
        Me.txtInvoiceText.Size = New System.Drawing.Size(626, 271)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtInvoiceText, OptionsSpelling1)
        Me.txtInvoiceText.TabIndex = 0
        Me.txtInvoiceText.Tag = "AEM"
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.CareLabel1)
        Me.GroupControl4.Controls.Add(Me.txtNet)
        Me.GroupControl4.Location = New System.Drawing.Point(12, 484)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(636, 57)
        Me.GroupControl4.TabIndex = 5
        Me.GroupControl4.Text = "Invoice Value"
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(466, 30)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(66, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Net Amount"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNet
        '
        Me.txtNet.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtNet.EnterMoveNextControl = True
        Me.txtNet.Location = New System.Drawing.Point(538, 27)
        Me.txtNet.MaxLength = 0
        Me.txtNet.Name = "txtNet"
        Me.txtNet.NumericAllowNegatives = False
        Me.txtNet.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtNet.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtNet.Properties.AccessibleName = ""
        Me.txtNet.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtNet.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtNet.Properties.Appearance.Options.UseFont = True
        Me.txtNet.Properties.Appearance.Options.UseTextOptions = True
        Me.txtNet.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtNet.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtNet.Size = New System.Drawing.Size(89, 22)
        Me.txtNet.TabIndex = 1
        Me.txtNet.Tag = "AEM"
        Me.txtNet.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNet.ToolTipText = ""
        '
        'GroupControl5
        '
        Me.GroupControl5.Controls.Add(Me.btnFind)
        Me.GroupControl5.Controls.Add(Me.CareLabel2)
        Me.GroupControl5.Controls.Add(Me.CareLabel3)
        Me.GroupControl5.Controls.Add(Me.txtCompanyName)
        Me.GroupControl5.Controls.Add(Me.txtAccountNumber)
        Me.GroupControl5.Controls.Add(Me.txtContactName)
        Me.GroupControl5.Controls.Add(Me.CareLabel4)
        Me.GroupControl5.Location = New System.Drawing.Point(12, 47)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(353, 126)
        Me.GroupControl5.TabIndex = 0
        Me.GroupControl5.Text = "Account Details"
        '
        'btnFind
        '
        Me.btnFind.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnFind.Appearance.Options.UseFont = True
        Me.btnFind.Location = New System.Drawing.Point(291, 26)
        Me.btnFind.Name = "btnFind"
        Me.btnFind.Size = New System.Drawing.Size(50, 25)
        Me.btnFind.TabIndex = 6
        Me.btnFind.Tag = ""
        Me.btnFind.Text = "Find"
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(10, 87)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(77, 15)
        Me.CareLabel2.TabIndex = 4
        Me.CareLabel2.Text = "Contact Name"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(10, 31)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(92, 15)
        Me.CareLabel3.TabIndex = 0
        Me.CareLabel3.Text = "Account Number"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCompanyName
        '
        Me.txtCompanyName.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtCompanyName.EnterMoveNextControl = True
        Me.txtCompanyName.Location = New System.Drawing.Point(118, 56)
        Me.txtCompanyName.MaxLength = 0
        Me.txtCompanyName.Name = "txtCompanyName"
        Me.txtCompanyName.NumericAllowNegatives = False
        Me.txtCompanyName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtCompanyName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCompanyName.Properties.AccessibleName = "Risk Surname"
        Me.txtCompanyName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtCompanyName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtCompanyName.Properties.Appearance.Options.UseFont = True
        Me.txtCompanyName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCompanyName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtCompanyName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtCompanyName.Size = New System.Drawing.Size(223, 22)
        Me.txtCompanyName.TabIndex = 3
        Me.txtCompanyName.Tag = "AM"
        Me.txtCompanyName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCompanyName.ToolTipText = ""
        '
        'txtAccountNumber
        '
        Me.txtAccountNumber.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtAccountNumber.EnterMoveNextControl = True
        Me.txtAccountNumber.Location = New System.Drawing.Point(118, 28)
        Me.txtAccountNumber.MaxLength = 0
        Me.txtAccountNumber.Name = "txtAccountNumber"
        Me.txtAccountNumber.NumericAllowNegatives = False
        Me.txtAccountNumber.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtAccountNumber.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtAccountNumber.Properties.AccessibleName = ""
        Me.txtAccountNumber.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtAccountNumber.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtAccountNumber.Properties.Appearance.Options.UseFont = True
        Me.txtAccountNumber.Properties.Appearance.Options.UseTextOptions = True
        Me.txtAccountNumber.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtAccountNumber.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtAccountNumber.Size = New System.Drawing.Size(167, 22)
        Me.txtAccountNumber.TabIndex = 1
        Me.txtAccountNumber.Tag = "AM"
        Me.txtAccountNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtAccountNumber.ToolTipText = ""
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(10, 59)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(87, 15)
        Me.CareLabel4.TabIndex = 2
        Me.CareLabel4.Text = "Company Name"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl6
        '
        Me.GroupControl6.Controls.Add(Me.txtAddress)
        Me.GroupControl6.Location = New System.Drawing.Point(371, 47)
        Me.GroupControl6.Name = "GroupControl6"
        Me.GroupControl6.Size = New System.Drawing.Size(277, 126)
        Me.GroupControl6.TabIndex = 1
        Me.GroupControl6.Text = "Address"
        '
        'txtAddress
        '
        Me.txtAddress.AccessibleName = "Risk Address"
        Me.txtAddress.Address = ""
        Me.txtAddress.AddressBlock = ""
        Me.txtAddress.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAddress.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.Appearance.Options.UseFont = True
        Me.txtAddress.County = ""
        Me.txtAddress.DisplayMode = Care.Address.CareAddress.EnumDisplayMode.SingleControl
        Me.txtAddress.Location = New System.Drawing.Point(11, 28)
        Me.txtAddress.MaxLength = 500
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.PostCode = ""
        Me.txtAddress.ReadOnly = False
        Me.txtAddress.Size = New System.Drawing.Size(255, 89)
        Me.txtAddress.TabIndex = 1
        Me.txtAddress.Tag = "AM"
        Me.txtAddress.Town = ""
        '
        'btnCreate
        '
        Me.btnCreate.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCreate.Appearance.Options.UseFont = True
        Me.btnCreate.Location = New System.Drawing.Point(364, 547)
        Me.btnCreate.Name = "btnCreate"
        Me.btnCreate.Size = New System.Drawing.Size(139, 25)
        Me.btnCreate.TabIndex = 6
        Me.btnCreate.Tag = ""
        Me.btnCreate.Text = "Create Invoice"
        '
        'btnClose
        '
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(509, 547)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(139, 25)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Tag = ""
        Me.btnClose.Text = "Close"
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.radNew)
        Me.GroupControl2.Controls.Add(Me.radFind)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.ShowCaption = False
        Me.GroupControl2.Size = New System.Drawing.Size(636, 29)
        Me.GroupControl2.TabIndex = 8
        Me.GroupControl2.Text = "Invoice Value"
        '
        'radNew
        '
        Me.radNew.Location = New System.Drawing.Point(122, 5)
        Me.radNew.Name = "radNew"
        Me.radNew.Properties.AutoWidth = True
        Me.radNew.Properties.Caption = "Create New Account"
        Me.radNew.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radNew.Properties.RadioGroupIndex = 0
        Me.radNew.Size = New System.Drawing.Size(121, 19)
        Me.radNew.TabIndex = 6
        Me.radNew.TabStop = False
        '
        'radFind
        '
        Me.radFind.EditValue = True
        Me.radFind.Location = New System.Drawing.Point(5, 5)
        Me.radFind.Name = "radFind"
        Me.radFind.Properties.AutoWidth = True
        Me.radFind.Properties.Caption = "Find Sage Account"
        Me.radFind.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radFind.Properties.RadioGroupIndex = 0
        Me.radFind.Size = New System.Drawing.Size(111, 19)
        Me.radFind.TabIndex = 5
        '
        'frmInvoice
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(658, 578)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnCreate)
        Me.Controls.Add(Me.GroupControl6)
        Me.Controls.Add(Me.GroupControl5)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.GroupControl1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmInvoice"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmInvoice"
        CType(Me.txtContactName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.txtInvoiceText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.txtNet.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.txtCompanyName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAccountNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl6.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.radNew.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radFind.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtContactName As Care.Controls.CareTextBox
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtInvoiceText As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents txtNet As Care.Controls.CareTextBox
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents txtCompanyName As Care.Controls.CareTextBox
    Friend WithEvents txtAccountNumber As Care.Controls.CareTextBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents GroupControl6 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtAddress As Care.Address.CareAddress
    Friend WithEvents btnCreate As Care.Controls.CareButton
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents btnFind As Care.Controls.CareButton
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents radNew As Care.Controls.CareRadioButton
    Friend WithEvents radFind As Care.Controls.CareRadioButton
End Class
