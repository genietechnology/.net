﻿Imports Care.Global
Imports Care.Data
Imports Care.Shared

Public Class frmSuppliers

    Private m_Supplier As Business.Supplier

    Private Sub frmSuppliers_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub

    Protected Overrides Sub SetBindings()

        m_Supplier = New Business.Supplier
        bs.DataSource = m_Supplier

        '******************************************************************************************************************************
        txtCode.DataBindings.Add("Text", bs, "_SupCode")
        txtName.DataBindings.Add("Text", bs, "_SupName")
        txtContact.DataBindings.Add("Text", bs, "_SupContact")
        '******************************************************************************************************************************

    End Sub

    Protected Overrides Sub FindRecord()
        Dim _ReturnValue As String = Business.Supplier.Find
        If _ReturnValue <> "" Then
            DisplayRecord(New Guid(_ReturnValue))
        End If
    End Sub

    Private Sub DisplayRecord(ByVal PlantID As Guid)

        MyBase.RecordID = PlantID
        MyBase.RecordPopulated = True

        m_Supplier = Business.Supplier.RetreiveByID(PlantID)
        bs.DataSource = m_Supplier

        txtAddress.Text = m_Supplier._SupAddress
        txtTel.Text = m_Supplier._SupTel
        txtEmail.Text = m_Supplier._SupEmail

        BuildTitleText()

    End Sub

    Protected Overrides Sub CommitUpdate()

        m_Supplier = CType(bs.Item(bs.Position), Business.Supplier)

        m_Supplier._SupAddress = txtAddress.Text
        m_Supplier._SupTel = txtTel.Text
        m_Supplier._SupEmail = txtEmail.Text

        m_Supplier.Store()

        BuildTitleText()

    End Sub

    Private Sub BuildTitleText()
        Me.Text = "Supplier (" + m_Supplier._SupCode.ToString + ") " + m_Supplier._SupName
    End Sub

End Class
