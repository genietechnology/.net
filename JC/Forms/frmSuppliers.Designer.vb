﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSuppliers
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.txtTel = New Care.[Shared].CareTelephoneNumber()
        Me.txtEmail = New Care.[Shared].CareEmailAddress()
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.txtAddress = New Care.Address.CareAddress()
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.txtContact = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel10 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.txtName = New Care.Controls.CareTextBox(Me.components)
        Me.txtCode = New Care.Controls.CareTextBox(Me.components)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtContact.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(377, 3)
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(286, 3)
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(0, 306)
        Me.Panel1.Size = New System.Drawing.Size(474, 36)
        Me.Panel1.TabIndex = 1
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Controls.Add(Me.txtTel)
        Me.GroupControl1.Controls.Add(Me.txtEmail)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.txtAddress)
        Me.GroupControl1.Controls.Add(Me.CareLabel5)
        Me.GroupControl1.Controls.Add(Me.txtContact)
        Me.GroupControl1.Controls.Add(Me.CareLabel10)
        Me.GroupControl1.Controls.Add(Me.CareLabel14)
        Me.GroupControl1.Controls.Add(Me.txtName)
        Me.GroupControl1.Controls.Add(Me.txtCode)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 53)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(450, 244)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "General Information"
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(9, 218)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(56, 15)
        Me.CareLabel3.TabIndex = 10
        Me.CareLabel3.Text = "Telephone"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTel
        '
        Me.txtTel.AccessibleName = "Telephone"
        Me.txtTel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTel.Appearance.Options.UseFont = True
        Me.txtTel.IsMobile = False
        Me.txtTel.Location = New System.Drawing.Point(85, 214)
        Me.txtTel.MaxLength = 20
        Me.txtTel.MinimumSize = New System.Drawing.Size(0, 22)
        Me.txtTel.Name = "txtTel"
        Me.txtTel.ReadOnly = False
        Me.txtTel.Size = New System.Drawing.Size(348, 22)
        Me.txtTel.TabIndex = 11
        Me.txtTel.Tag = "AE"
        '
        'txtEmail
        '
        Me.txtEmail.AccessibleName = "Email Address"
        Me.txtEmail.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail.Appearance.Options.UseFont = True
        Me.txtEmail.Location = New System.Drawing.Point(85, 186)
        Me.txtEmail.MaxLength = 100
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.NoButton = False
        Me.txtEmail.NoColours = False
        Me.txtEmail.NoValidate = False
        Me.txtEmail.ReadOnly = False
        Me.txtEmail.Size = New System.Drawing.Size(348, 22)
        Me.txtEmail.TabIndex = 9
        Me.txtEmail.Tag = "AE"
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(9, 117)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(42, 15)
        Me.CareLabel1.TabIndex = 6
        Me.CareLabel1.Text = "Address"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(9, 189)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(29, 15)
        Me.CareLabel2.TabIndex = 8
        Me.CareLabel2.Text = "Email"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAddress
        '
        Me.txtAddress.AccessibleName = "Address"
        Me.txtAddress.Address = ""
        Me.txtAddress.AddressBlock = ""
        Me.txtAddress.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.Appearance.Options.UseFont = True
        Me.txtAddress.County = ""
        Me.txtAddress.DisplayMode = Care.Address.CareAddress.EnumDisplayMode.SingleControl
        Me.txtAddress.Location = New System.Drawing.Point(85, 93)
        Me.txtAddress.MaxLength = 500
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.PostCode = ""
        Me.txtAddress.ReadOnly = False
        Me.txtAddress.Size = New System.Drawing.Size(348, 87)
        Me.txtAddress.TabIndex = 7
        Me.txtAddress.Tag = "AE"
        Me.txtAddress.Town = ""
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.CareLabel5.Location = New System.Drawing.Point(9, 68)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(42, 15)
        Me.CareLabel5.TabIndex = 4
        Me.CareLabel5.Text = "Contact"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtContact
        '
        Me.txtContact.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtContact.EditValue = ""
        Me.txtContact.EnterMoveNextControl = True
        Me.txtContact.Location = New System.Drawing.Point(85, 65)
        Me.txtContact.MaxLength = 30
        Me.txtContact.Name = "txtContact"
        Me.txtContact.NumericAllowNegatives = False
        Me.txtContact.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtContact.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtContact.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtContact.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtContact.Properties.Appearance.Options.UseFont = True
        Me.txtContact.Properties.Appearance.Options.UseTextOptions = True
        Me.txtContact.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtContact.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtContact.Properties.MaxLength = 30
        Me.txtContact.Size = New System.Drawing.Size(348, 22)
        Me.txtContact.TabIndex = 5
        Me.txtContact.Tag = "AE"
        Me.txtContact.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtContact.ToolTipText = ""
        '
        'CareLabel10
        '
        Me.CareLabel10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel10.Location = New System.Drawing.Point(9, 40)
        Me.CareLabel10.Name = "CareLabel10"
        Me.CareLabel10.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel10.TabIndex = 2
        Me.CareLabel10.Text = "Name"
        Me.CareLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(9, 12)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(28, 15)
        Me.CareLabel14.TabIndex = 0
        Me.CareLabel14.Text = "Code"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtName.EditValue = ""
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(85, 37)
        Me.txtName.MaxLength = 30
        Me.txtName.Name = "txtName"
        Me.txtName.NumericAllowNegatives = False
        Me.txtName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AccessibleName = "Supplier Name"
        Me.txtName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtName.Properties.MaxLength = 30
        Me.txtName.Size = New System.Drawing.Size(348, 22)
        Me.txtName.TabIndex = 3
        Me.txtName.Tag = "AEM"
        Me.txtName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'txtCode
        '
        Me.txtCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCode.EditValue = ""
        Me.txtCode.EnterMoveNextControl = True
        Me.txtCode.Location = New System.Drawing.Point(85, 9)
        Me.txtCode.MaxLength = 10
        Me.txtCode.Name = "txtCode"
        Me.txtCode.NumericAllowNegatives = False
        Me.txtCode.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.None
        Me.txtCode.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCode.Properties.AccessibleName = "Supplier Code"
        Me.txtCode.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtCode.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtCode.Properties.Appearance.Options.UseFont = True
        Me.txtCode.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtCode.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCode.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtCode.Properties.MaxLength = 10
        Me.txtCode.Size = New System.Drawing.Size(101, 22)
        Me.txtCode.TabIndex = 1
        Me.txtCode.Tag = "AM"
        Me.txtCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCode.ToolTipText = ""
        '
        'frmSuppliers
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(474, 342)
        Me.Controls.Add(Me.GroupControl1)
        Me.Name = "frmSuppliers"
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtContact.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents txtContact As Care.Controls.CareTextBox
    Friend WithEvents CareLabel10 As Care.Controls.CareLabel
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents txtCode As Care.Controls.CareTextBox
    Friend WithEvents txtEmail As Care.Shared.CareEmailAddress
    Friend WithEvents txtTel As Care.Shared.CareTelephoneNumber
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents txtAddress As Care.Address.CareAddress
End Class
