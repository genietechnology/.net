﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMaterialEntry
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnDelete = New Care.Controls.CareButton(Me.components)
        Me.btnAddNewJob = New Care.Controls.CareButton(Me.components)
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.btnFindJob = New Care.Controls.CareButton(Me.components)
        Me.txtJobDesc = New Care.Controls.CareTextBox(Me.components)
        Me.txtNo = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.btnSameJob = New Care.Controls.CareButton(Me.components)
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.txtSellPrice = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.txtUOM = New Care.Controls.CareTextBox(Me.components)
        Me.txtFactor = New Care.Controls.CareTextBox(Me.components)
        Me.btnFindStock = New Care.Controls.CareButton(Me.components)
        Me.txtStockDesc = New Care.Controls.CareTextBox(Me.components)
        Me.txtTotalPrice = New Care.Controls.CareTextBox(Me.components)
        Me.txtPrice = New Care.Controls.CareTextBox(Me.components)
        Me.txtQty = New Care.Controls.CareTextBox(Me.components)
        Me.txtQuickfind = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.cdtDate = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.cgMaterials = New Care.Controls.CareGrid()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.txtJobDesc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.txtSellPrice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUOM.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFactor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStockDesc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalPrice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPrice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtQty.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtQuickfind.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.cdtDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(224, 128)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(100, 25)
        Me.btnDelete.TabIndex = 5
        Me.btnDelete.Text = "Delete Selected"
        '
        'btnAddNewJob
        '
        Me.btnAddNewJob.Location = New System.Drawing.Point(118, 128)
        Me.btnAddNewJob.Name = "btnAddNewJob"
        Me.btnAddNewJob.Size = New System.Drawing.Size(100, 25)
        Me.btnAddNewJob.TabIndex = 4
        Me.btnAddNewJob.Text = "Add, New Job"
        '
        'GroupControl4
        '
        Me.GroupControl4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl4.Controls.Add(Me.btnFindJob)
        Me.GroupControl4.Controls.Add(Me.txtJobDesc)
        Me.GroupControl4.Controls.Add(Me.txtNo)
        Me.GroupControl4.Controls.Add(Me.CareLabel9)
        Me.GroupControl4.Location = New System.Drawing.Point(176, 12)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.ShowCaption = False
        Me.GroupControl4.Size = New System.Drawing.Size(573, 39)
        Me.GroupControl4.TabIndex = 1
        Me.GroupControl4.Text = "GroupControl4"
        '
        'btnFindJob
        '
        Me.btnFindJob.Location = New System.Drawing.Point(142, 7)
        Me.btnFindJob.Name = "btnFindJob"
        Me.btnFindJob.Size = New System.Drawing.Size(57, 25)
        Me.btnFindJob.TabIndex = 2
        Me.btnFindJob.TabStop = False
        Me.btnFindJob.Text = "Find Job"
        '
        'txtJobDesc
        '
        Me.txtJobDesc.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtJobDesc.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtJobDesc.EnterMoveNextControl = True
        Me.txtJobDesc.Location = New System.Drawing.Point(205, 9)
        Me.txtJobDesc.MaxLength = 0
        Me.txtJobDesc.Name = "txtJobDesc"
        Me.txtJobDesc.NumericAllowNegatives = False
        Me.txtJobDesc.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtJobDesc.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtJobDesc.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtJobDesc.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtJobDesc.Properties.Appearance.Options.UseFont = True
        Me.txtJobDesc.Properties.Appearance.Options.UseTextOptions = True
        Me.txtJobDesc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtJobDesc.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtJobDesc.Size = New System.Drawing.Size(357, 20)
        Me.txtJobDesc.TabIndex = 3
        Me.txtJobDesc.Tag = "R"
        Me.txtJobDesc.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtJobDesc.ToolTipText = ""
        '
        'txtNo
        '
        Me.txtNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtNo.EnterMoveNextControl = True
        Me.txtNo.Location = New System.Drawing.Point(36, 9)
        Me.txtNo.MaxLength = 10
        Me.txtNo.Name = "txtNo"
        Me.txtNo.NumericAllowNegatives = False
        Me.txtNo.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtNo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtNo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtNo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtNo.Properties.Appearance.Options.UseFont = True
        Me.txtNo.Properties.Appearance.Options.UseTextOptions = True
        Me.txtNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtNo.Properties.Mask.EditMask = "##########;"
        Me.txtNo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtNo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtNo.Properties.MaxLength = 10
        Me.txtNo.Size = New System.Drawing.Size(100, 22)
        Me.txtNo.TabIndex = 1
        Me.txtNo.Tag = "A"
        Me.txtNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNo.ToolTipText = ""
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(9, 12)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(18, 15)
        Me.CareLabel9.TabIndex = 0
        Me.CareLabel9.Text = "Job"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSameJob
        '
        Me.btnSameJob.Location = New System.Drawing.Point(12, 128)
        Me.btnSameJob.Name = "btnSameJob"
        Me.btnSameJob.Size = New System.Drawing.Size(100, 25)
        Me.btnSameJob.TabIndex = 3
        Me.btnSameJob.Text = "Add, Same Job"
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl2.Controls.Add(Me.txtSellPrice)
        Me.GroupControl2.Controls.Add(Me.CareLabel5)
        Me.GroupControl2.Controls.Add(Me.CareLabel4)
        Me.GroupControl2.Controls.Add(Me.CareLabel3)
        Me.GroupControl2.Controls.Add(Me.txtUOM)
        Me.GroupControl2.Controls.Add(Me.txtFactor)
        Me.GroupControl2.Controls.Add(Me.btnFindStock)
        Me.GroupControl2.Controls.Add(Me.txtStockDesc)
        Me.GroupControl2.Controls.Add(Me.txtTotalPrice)
        Me.GroupControl2.Controls.Add(Me.txtPrice)
        Me.GroupControl2.Controls.Add(Me.txtQty)
        Me.GroupControl2.Controls.Add(Me.txtQuickfind)
        Me.GroupControl2.Controls.Add(Me.CareLabel6)
        Me.GroupControl2.Controls.Add(Me.CareLabel1)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 57)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.ShowCaption = False
        Me.GroupControl2.Size = New System.Drawing.Size(737, 65)
        Me.GroupControl2.TabIndex = 2
        Me.GroupControl2.Text = "GroupControl2"
        '
        'txtSellPrice
        '
        Me.txtSellPrice.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtSellPrice.EnterMoveNextControl = True
        Me.txtSellPrice.Location = New System.Drawing.Point(313, 8)
        Me.txtSellPrice.MaxLength = 0
        Me.txtSellPrice.Name = "txtSellPrice"
        Me.txtSellPrice.NumericAllowNegatives = False
        Me.txtSellPrice.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSellPrice.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSellPrice.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSellPrice.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSellPrice.Properties.Appearance.Options.UseFont = True
        Me.txtSellPrice.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSellPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSellPrice.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSellPrice.Size = New System.Drawing.Size(65, 22)
        Me.txtSellPrice.TabIndex = 4
        Me.txtSellPrice.Tag = "R"
        Me.txtSellPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSellPrice.ToolTipText = ""
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(476, 39)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(56, 15)
        Me.CareLabel5.TabIndex = 12
        Me.CareLabel5.Text = "Total Price"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(185, 39)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(51, 15)
        Me.CareLabel4.TabIndex = 8
        Me.CareLabel4.Text = "Unit Price"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(9, 39)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(19, 15)
        Me.CareLabel3.TabIndex = 6
        Me.CareLabel3.Text = "Qty"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtUOM
        '
        Me.txtUOM.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtUOM.EnterMoveNextControl = True
        Me.txtUOM.Location = New System.Drawing.Point(242, 8)
        Me.txtUOM.MaxLength = 0
        Me.txtUOM.Name = "txtUOM"
        Me.txtUOM.NumericAllowNegatives = False
        Me.txtUOM.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtUOM.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtUOM.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtUOM.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtUOM.Properties.Appearance.Options.UseFont = True
        Me.txtUOM.Properties.Appearance.Options.UseTextOptions = True
        Me.txtUOM.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtUOM.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtUOM.Size = New System.Drawing.Size(65, 22)
        Me.txtUOM.TabIndex = 3
        Me.txtUOM.Tag = "R"
        Me.txtUOM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtUOM.ToolTipText = ""
        '
        'txtFactor
        '
        Me.txtFactor.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtFactor.EnterMoveNextControl = True
        Me.txtFactor.Location = New System.Drawing.Point(384, 36)
        Me.txtFactor.MaxLength = 14
        Me.txtFactor.Name = "txtFactor"
        Me.txtFactor.NumericAllowNegatives = False
        Me.txtFactor.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtFactor.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFactor.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFactor.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtFactor.Properties.Appearance.Options.UseFont = True
        Me.txtFactor.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFactor.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFactor.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtFactor.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtFactor.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFactor.Properties.MaxLength = 14
        Me.txtFactor.Size = New System.Drawing.Size(65, 22)
        Me.txtFactor.TabIndex = 11
        Me.txtFactor.Tag = "A"
        Me.txtFactor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtFactor.ToolTipText = ""
        '
        'btnFindStock
        '
        Me.btnFindStock.Location = New System.Drawing.Point(173, 8)
        Me.btnFindStock.Name = "btnFindStock"
        Me.btnFindStock.Size = New System.Drawing.Size(63, 22)
        Me.btnFindStock.TabIndex = 2
        Me.btnFindStock.TabStop = False
        Me.btnFindStock.Text = "Find Stock"
        '
        'txtStockDesc
        '
        Me.txtStockDesc.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtStockDesc.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtStockDesc.EnterMoveNextControl = True
        Me.txtStockDesc.Location = New System.Drawing.Point(384, 8)
        Me.txtStockDesc.MaxLength = 0
        Me.txtStockDesc.Name = "txtStockDesc"
        Me.txtStockDesc.NumericAllowNegatives = False
        Me.txtStockDesc.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtStockDesc.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStockDesc.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtStockDesc.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtStockDesc.Properties.Appearance.Options.UseFont = True
        Me.txtStockDesc.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStockDesc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtStockDesc.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtStockDesc.Size = New System.Drawing.Size(342, 20)
        Me.txtStockDesc.TabIndex = 5
        Me.txtStockDesc.Tag = "R"
        Me.txtStockDesc.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtStockDesc.ToolTipText = ""
        '
        'txtTotalPrice
        '
        Me.txtTotalPrice.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtTotalPrice.EnterMoveNextControl = True
        Me.txtTotalPrice.Location = New System.Drawing.Point(538, 36)
        Me.txtTotalPrice.MaxLength = 0
        Me.txtTotalPrice.Name = "txtTotalPrice"
        Me.txtTotalPrice.NumericAllowNegatives = False
        Me.txtTotalPrice.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtTotalPrice.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTotalPrice.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtTotalPrice.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtTotalPrice.Properties.Appearance.Options.UseFont = True
        Me.txtTotalPrice.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTotalPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtTotalPrice.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTotalPrice.Size = New System.Drawing.Size(89, 22)
        Me.txtTotalPrice.TabIndex = 13
        Me.txtTotalPrice.Tag = "R"
        Me.txtTotalPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTotalPrice.ToolTipText = ""
        '
        'txtPrice
        '
        Me.txtPrice.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtPrice.EnterMoveNextControl = True
        Me.txtPrice.Location = New System.Drawing.Point(242, 36)
        Me.txtPrice.MaxLength = 14
        Me.txtPrice.Name = "txtPrice"
        Me.txtPrice.NumericAllowNegatives = False
        Me.txtPrice.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtPrice.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPrice.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPrice.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtPrice.Properties.Appearance.Options.UseFont = True
        Me.txtPrice.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPrice.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtPrice.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtPrice.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPrice.Properties.MaxLength = 14
        Me.txtPrice.Size = New System.Drawing.Size(65, 22)
        Me.txtPrice.TabIndex = 9
        Me.txtPrice.Tag = "A"
        Me.txtPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPrice.ToolTipText = ""
        '
        'txtQty
        '
        Me.txtQty.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtQty.EnterMoveNextControl = True
        Me.txtQty.Location = New System.Drawing.Point(67, 36)
        Me.txtQty.MaxLength = 14
        Me.txtQty.Name = "txtQty"
        Me.txtQty.NumericAllowNegatives = False
        Me.txtQty.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtQty.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtQty.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtQty.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtQty.Properties.Appearance.Options.UseFont = True
        Me.txtQty.Properties.Appearance.Options.UseTextOptions = True
        Me.txtQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtQty.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtQty.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtQty.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtQty.Properties.MaxLength = 14
        Me.txtQty.Size = New System.Drawing.Size(100, 22)
        Me.txtQty.TabIndex = 7
        Me.txtQty.Tag = "A"
        Me.txtQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtQty.ToolTipText = ""
        '
        'txtQuickfind
        '
        Me.txtQuickfind.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtQuickfind.EnterMoveNextControl = True
        Me.txtQuickfind.Location = New System.Drawing.Point(67, 8)
        Me.txtQuickfind.MaxLength = 10
        Me.txtQuickfind.Name = "txtQuickfind"
        Me.txtQuickfind.NumericAllowNegatives = False
        Me.txtQuickfind.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtQuickfind.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtQuickfind.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtQuickfind.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtQuickfind.Properties.Appearance.Options.UseFont = True
        Me.txtQuickfind.Properties.Appearance.Options.UseTextOptions = True
        Me.txtQuickfind.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtQuickfind.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtQuickfind.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtQuickfind.Properties.MaxLength = 10
        Me.txtQuickfind.Size = New System.Drawing.Size(100, 22)
        Me.txtQuickfind.TabIndex = 1
        Me.txtQuickfind.Tag = "A"
        Me.txtQuickfind.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtQuickfind.ToolTipText = ""
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(345, 39)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(33, 15)
        Me.CareLabel6.TabIndex = 10
        Me.CareLabel6.Text = "Factor"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(9, 11)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(52, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Quickfind"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.cdtDate)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(158, 39)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "GroupControl1"
        '
        'cdtDate
        '
        Me.cdtDate.EditValue = Nothing
        Me.cdtDate.EnterMoveNextControl = True
        Me.cdtDate.Location = New System.Drawing.Point(48, 9)
        Me.cdtDate.Name = "cdtDate"
        Me.cdtDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtDate.Properties.Appearance.Options.UseFont = True
        Me.cdtDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDate.Size = New System.Drawing.Size(100, 22)
        Me.cdtDate.TabIndex = 1
        Me.cdtDate.Tag = "A"
        Me.cdtDate.Value = Nothing
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(9, 12)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(24, 15)
        Me.CareLabel2.TabIndex = 0
        Me.CareLabel2.Text = "Date"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cgMaterials
        '
        Me.cgMaterials.AllowBuildColumns = True
        Me.cgMaterials.AllowEdit = False
        Me.cgMaterials.AllowHorizontalScroll = False
        Me.cgMaterials.AllowMultiSelect = False
        Me.cgMaterials.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgMaterials.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgMaterials.Appearance.Options.UseFont = True
        Me.cgMaterials.AutoSizeByData = True
        Me.cgMaterials.DisableAutoSize = False
        Me.cgMaterials.DisableDataFormatting = False
        Me.cgMaterials.FocusedRowHandle = -2147483648
        Me.cgMaterials.HideFirstColumn = False
        Me.cgMaterials.Location = New System.Drawing.Point(12, 159)
        Me.cgMaterials.Name = "cgMaterials"
        Me.cgMaterials.PreviewColumn = ""
        Me.cgMaterials.QueryID = Nothing
        Me.cgMaterials.RowAutoHeight = False
        Me.cgMaterials.SearchAsYouType = True
        Me.cgMaterials.ShowAutoFilterRow = False
        Me.cgMaterials.ShowFindPanel = False
        Me.cgMaterials.ShowGroupByBox = False
        Me.cgMaterials.ShowLoadingPanel = False
        Me.cgMaterials.ShowNavigator = True
        Me.cgMaterials.Size = New System.Drawing.Size(737, 554)
        Me.cgMaterials.TabIndex = 8
        '
        'frmMaterialEntry
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(761, 725)
        Me.Controls.Add(Me.cgMaterials)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnAddNewJob)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.btnSameJob)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.Name = "frmMaterialEntry"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.txtJobDesc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.txtSellPrice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUOM.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFactor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStockDesc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalPrice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPrice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtQty.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtQuickfind.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.cdtDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnDelete As Care.Controls.CareButton
    Friend WithEvents btnAddNewJob As Care.Controls.CareButton
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents btnSameJob As Care.Controls.CareButton
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cdtDate As Care.Controls.CareDateTime
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents btnFindJob As Care.Controls.CareButton
    Friend WithEvents txtJobDesc As Care.Controls.CareTextBox
    Friend WithEvents txtNo As Care.Controls.CareTextBox
    Friend WithEvents txtTotalPrice As Care.Controls.CareTextBox
    Friend WithEvents txtPrice As Care.Controls.CareTextBox
    Friend WithEvents txtQty As Care.Controls.CareTextBox
    Friend WithEvents txtQuickfind As Care.Controls.CareTextBox
    Friend WithEvents cgMaterials As Care.Controls.CareGrid
    Friend WithEvents txtUOM As Care.Controls.CareTextBox
    Friend WithEvents txtFactor As Care.Controls.CareTextBox
    Friend WithEvents btnFindStock As Care.Controls.CareButton
    Friend WithEvents txtStockDesc As Care.Controls.CareTextBox
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents txtSellPrice As Care.Controls.CareTextBox
End Class
