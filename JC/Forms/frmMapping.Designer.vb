﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMapping
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ImageTilesLayer1 As DevExpress.XtraMap.ImageTilesLayer = New DevExpress.XtraMap.ImageTilesLayer()
        Dim BingMapDataProvider1 As DevExpress.XtraMap.BingMapDataProvider = New DevExpress.XtraMap.BingMapDataProvider()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.DashboardLabel1 = New Care.Controls.DashboardLabel()
        Me.DashboardLabel2 = New Care.Controls.DashboardLabel()
        Me.DashboardLabel3 = New Care.Controls.DashboardLabel()
        Me.DashboardLabel4 = New Care.Controls.DashboardLabel()
        Me.DashboardLabel5 = New Care.Controls.DashboardLabel()
        Me.MyMap = New DevExpress.XtraMap.MapControl()
        Me.timRefresh = New System.Windows.Forms.Timer(Me.components)
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.MyMap, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 5
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.DashboardLabel1, 4, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.DashboardLabel2, 4, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.DashboardLabel3, 4, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.DashboardLabel4, 4, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.DashboardLabel5, 4, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.MyMap, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(918, 650)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'DashboardLabel1
        '
        Me.DashboardLabel1.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.DashboardLabel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DashboardLabel1.LabelBottom = "Bottom"
        Me.DashboardLabel1.LabelMiddle = "55"
        Me.DashboardLabel1.LabelTop = "Top"
        Me.DashboardLabel1.Location = New System.Drawing.Point(735, 3)
        Me.DashboardLabel1.Name = "DashboardLabel1"
        Me.DashboardLabel1.Size = New System.Drawing.Size(180, 124)
        Me.DashboardLabel1.TabIndex = 0
        '
        'DashboardLabel2
        '
        Me.DashboardLabel2.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.DashboardLabel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DashboardLabel2.LabelBottom = "Bottom"
        Me.DashboardLabel2.LabelMiddle = "55"
        Me.DashboardLabel2.LabelTop = "Top"
        Me.DashboardLabel2.Location = New System.Drawing.Point(735, 133)
        Me.DashboardLabel2.Name = "DashboardLabel2"
        Me.DashboardLabel2.Size = New System.Drawing.Size(180, 124)
        Me.DashboardLabel2.TabIndex = 1
        '
        'DashboardLabel3
        '
        Me.DashboardLabel3.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.DashboardLabel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DashboardLabel3.LabelBottom = "Bottom"
        Me.DashboardLabel3.LabelMiddle = "55"
        Me.DashboardLabel3.LabelTop = "Top"
        Me.DashboardLabel3.Location = New System.Drawing.Point(735, 263)
        Me.DashboardLabel3.Name = "DashboardLabel3"
        Me.DashboardLabel3.Size = New System.Drawing.Size(180, 124)
        Me.DashboardLabel3.TabIndex = 2
        '
        'DashboardLabel4
        '
        Me.DashboardLabel4.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.DashboardLabel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DashboardLabel4.LabelBottom = "Bottom"
        Me.DashboardLabel4.LabelMiddle = "55"
        Me.DashboardLabel4.LabelTop = "Top"
        Me.DashboardLabel4.Location = New System.Drawing.Point(735, 393)
        Me.DashboardLabel4.Name = "DashboardLabel4"
        Me.DashboardLabel4.Size = New System.Drawing.Size(180, 124)
        Me.DashboardLabel4.TabIndex = 3
        '
        'DashboardLabel5
        '
        Me.DashboardLabel5.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.DashboardLabel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DashboardLabel5.LabelBottom = "Bottom"
        Me.DashboardLabel5.LabelMiddle = "55"
        Me.DashboardLabel5.LabelTop = "Top"
        Me.DashboardLabel5.Location = New System.Drawing.Point(735, 523)
        Me.DashboardLabel5.Name = "DashboardLabel5"
        Me.DashboardLabel5.Size = New System.Drawing.Size(180, 124)
        Me.DashboardLabel5.TabIndex = 4
        '
        'MyMap
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.MyMap, 4)
        Me.MyMap.Dock = System.Windows.Forms.DockStyle.Fill
        BingMapDataProvider1.BingKey = "AmeXh0oiUBaToLYQjpW_AMV7vNt7DRhHM9fqMmjWQNUGajBe0SMSTo4CGx-QmMK4"
        BingMapDataProvider1.TileSource = Nothing
        ImageTilesLayer1.DataProvider = BingMapDataProvider1
        Me.MyMap.Layers.Add(ImageTilesLayer1)
        Me.MyMap.Location = New System.Drawing.Point(3, 3)
        Me.MyMap.Name = "MyMap"
        Me.TableLayoutPanel1.SetRowSpan(Me.MyMap, 5)
        Me.MyMap.Size = New System.Drawing.Size(726, 644)
        Me.MyMap.TabIndex = 5
        '
        'timRefresh
        '
        Me.timRefresh.Interval = 10000
        '
        'frmMapping
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(918, 650)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.LoadMaximised = True
        Me.Name = "frmMapping"
        Me.Text = "frmMapping"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.MyMap, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents DashboardLabel1 As Care.Controls.DashboardLabel
    Friend WithEvents DashboardLabel2 As Care.Controls.DashboardLabel
    Friend WithEvents DashboardLabel3 As Care.Controls.DashboardLabel
    Friend WithEvents DashboardLabel4 As Care.Controls.DashboardLabel
    Friend WithEvents DashboardLabel5 As Care.Controls.DashboardLabel
    Friend WithEvents MyMap As DevExpress.XtraMap.MapControl
    Friend WithEvents timRefresh As System.Windows.Forms.Timer
End Class
