﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDiaryEntry
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxMatch = New DevExpress.XtraEditors.GroupControl()
        Me.btnHoliday = New Care.Controls.CareButton(Me.components)
        Me.btnSick = New Care.Controls.CareButton(Me.components)
        Me.btnAbsent = New Care.Controls.CareButton(Me.components)
        Me.btnAdd = New Care.Controls.CareButton(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.txtEmployeeName = New Care.Controls.CareTextBox(Me.components)
        Me.btnEmployeeSearch = New Care.Controls.CareButton(Me.components)
        Me.txtEmployee = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.txtJobName = New Care.Controls.CareTextBox(Me.components)
        Me.btnJobSearch = New Care.Controls.CareButton(Me.components)
        Me.txtJob = New Care.Controls.CareTextBox(Me.components)
        Me.gbxDetails = New DevExpress.XtraEditors.GroupControl()
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.cdtDate = New Care.Controls.CareDateTime(Me.components)
        Me.cbxWeather = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.cgEntries = New Care.Controls.CareGrid()
        Me.btnCommit = New Care.Controls.CareButton(Me.components)
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.btnDelete = New Care.Controls.CareButton(Me.components)
        CType(Me.gbxMatch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxMatch.SuspendLayout()
        CType(Me.txtEmployeeName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEmployee.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJobName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJob.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxDetails.SuspendLayout()
        CType(Me.cdtDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxWeather.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'gbxMatch
        '
        Me.gbxMatch.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxMatch.Controls.Add(Me.btnHoliday)
        Me.gbxMatch.Controls.Add(Me.btnSick)
        Me.gbxMatch.Controls.Add(Me.btnAbsent)
        Me.gbxMatch.Controls.Add(Me.btnAdd)
        Me.gbxMatch.Controls.Add(Me.CareLabel4)
        Me.gbxMatch.Controls.Add(Me.txtEmployeeName)
        Me.gbxMatch.Controls.Add(Me.btnEmployeeSearch)
        Me.gbxMatch.Controls.Add(Me.txtEmployee)
        Me.gbxMatch.Controls.Add(Me.CareLabel3)
        Me.gbxMatch.Controls.Add(Me.txtJobName)
        Me.gbxMatch.Controls.Add(Me.btnJobSearch)
        Me.gbxMatch.Controls.Add(Me.txtJob)
        Me.gbxMatch.Location = New System.Drawing.Point(14, 63)
        Me.gbxMatch.Name = "gbxMatch"
        Me.gbxMatch.ShowCaption = False
        Me.gbxMatch.Size = New System.Drawing.Size(794, 67)
        Me.gbxMatch.TabIndex = 1
        '
        'btnHoliday
        '
        Me.btnHoliday.Location = New System.Drawing.Point(635, 35)
        Me.btnHoliday.Name = "btnHoliday"
        Me.btnHoliday.Size = New System.Drawing.Size(78, 23)
        Me.btnHoliday.TabIndex = 11
        Me.btnHoliday.TabStop = False
        Me.btnHoliday.Text = "Holiday"
        '
        'btnSick
        '
        Me.btnSick.Location = New System.Drawing.Point(551, 35)
        Me.btnSick.Name = "btnSick"
        Me.btnSick.Size = New System.Drawing.Size(78, 23)
        Me.btnSick.TabIndex = 10
        Me.btnSick.TabStop = False
        Me.btnSick.Text = "Sick"
        '
        'btnAbsent
        '
        Me.btnAbsent.Location = New System.Drawing.Point(467, 35)
        Me.btnAbsent.Name = "btnAbsent"
        Me.btnAbsent.Size = New System.Drawing.Size(78, 23)
        Me.btnAbsent.TabIndex = 9
        Me.btnAbsent.TabStop = False
        Me.btnAbsent.Text = "Absent"
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(383, 35)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(78, 23)
        Me.btnAdd.TabIndex = 8
        Me.btnAdd.TabStop = False
        Me.btnAdd.Text = "Add"
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(9, 11)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(52, 15)
        Me.CareLabel4.TabIndex = 0
        Me.CareLabel4.Text = "Employee"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmployeeName
        '
        Me.txtEmployeeName.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtEmployeeName.EnterMoveNextControl = True
        Me.txtEmployeeName.Location = New System.Drawing.Point(9, 36)
        Me.txtEmployeeName.MaxLength = 0
        Me.txtEmployeeName.Name = "txtEmployeeName"
        Me.txtEmployeeName.NumericAllowNegatives = False
        Me.txtEmployeeName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtEmployeeName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtEmployeeName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtEmployeeName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtEmployeeName.Properties.Appearance.Options.UseFont = True
        Me.txtEmployeeName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtEmployeeName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtEmployeeName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtEmployeeName.Properties.ReadOnly = True
        Me.txtEmployeeName.Properties.Tag = "R"
        Me.txtEmployeeName.Size = New System.Drawing.Size(234, 22)
        Me.txtEmployeeName.TabIndex = 3
        Me.txtEmployeeName.TabStop = False
        Me.txtEmployeeName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtEmployeeName.ToolTipText = ""
        '
        'btnEmployeeSearch
        '
        Me.btnEmployeeSearch.Location = New System.Drawing.Point(165, 7)
        Me.btnEmployeeSearch.Name = "btnEmployeeSearch"
        Me.btnEmployeeSearch.Size = New System.Drawing.Size(78, 23)
        Me.btnEmployeeSearch.TabIndex = 2
        Me.btnEmployeeSearch.TabStop = False
        Me.btnEmployeeSearch.Text = "Search"
        '
        'txtEmployee
        '
        Me.txtEmployee.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEmployee.EnterMoveNextControl = True
        Me.txtEmployee.Location = New System.Drawing.Point(67, 8)
        Me.txtEmployee.MaxLength = 0
        Me.txtEmployee.Name = "txtEmployee"
        Me.txtEmployee.NumericAllowNegatives = False
        Me.txtEmployee.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtEmployee.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtEmployee.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtEmployee.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtEmployee.Properties.Appearance.Options.UseFont = True
        Me.txtEmployee.Properties.Appearance.Options.UseTextOptions = True
        Me.txtEmployee.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtEmployee.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEmployee.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtEmployee.Size = New System.Drawing.Size(92, 22)
        Me.txtEmployee.TabIndex = 1
        Me.txtEmployee.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtEmployee.ToolTipText = ""
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(261, 11)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(18, 15)
        Me.CareLabel3.TabIndex = 4
        Me.CareLabel3.Text = "Job"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJobName
        '
        Me.txtJobName.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtJobName.EnterMoveNextControl = True
        Me.txtJobName.Location = New System.Drawing.Point(467, 8)
        Me.txtJobName.MaxLength = 0
        Me.txtJobName.Name = "txtJobName"
        Me.txtJobName.NumericAllowNegatives = False
        Me.txtJobName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtJobName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtJobName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtJobName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtJobName.Properties.Appearance.Options.UseFont = True
        Me.txtJobName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtJobName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtJobName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtJobName.Properties.ReadOnly = True
        Me.txtJobName.Properties.Tag = "R"
        Me.txtJobName.Size = New System.Drawing.Size(320, 22)
        Me.txtJobName.TabIndex = 7
        Me.txtJobName.TabStop = False
        Me.txtJobName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtJobName.ToolTipText = ""
        '
        'btnJobSearch
        '
        Me.btnJobSearch.Location = New System.Drawing.Point(383, 7)
        Me.btnJobSearch.Name = "btnJobSearch"
        Me.btnJobSearch.Size = New System.Drawing.Size(78, 23)
        Me.btnJobSearch.TabIndex = 6
        Me.btnJobSearch.TabStop = False
        Me.btnJobSearch.Text = "Search"
        '
        'txtJob
        '
        Me.txtJob.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtJob.EnterMoveNextControl = True
        Me.txtJob.Location = New System.Drawing.Point(285, 7)
        Me.txtJob.MaxLength = 10
        Me.txtJob.Name = "txtJob"
        Me.txtJob.NumericAllowNegatives = False
        Me.txtJob.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtJob.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtJob.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtJob.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtJob.Properties.Appearance.Options.UseFont = True
        Me.txtJob.Properties.Appearance.Options.UseTextOptions = True
        Me.txtJob.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtJob.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtJob.Properties.Mask.EditMask = "##########;"
        Me.txtJob.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtJob.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtJob.Properties.MaxLength = 10
        Me.txtJob.Size = New System.Drawing.Size(92, 22)
        Me.txtJob.TabIndex = 5
        Me.txtJob.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtJob.ToolTipText = ""
        '
        'gbxDetails
        '
        Me.gbxDetails.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxDetails.Controls.Add(Me.CareLabel1)
        Me.gbxDetails.Controls.Add(Me.cdtDate)
        Me.gbxDetails.Controls.Add(Me.cbxWeather)
        Me.gbxDetails.Controls.Add(Me.CareLabel2)
        Me.gbxDetails.Location = New System.Drawing.Point(14, 14)
        Me.gbxDetails.Name = "gbxDetails"
        Me.gbxDetails.ShowCaption = False
        Me.gbxDetails.Size = New System.Drawing.Size(794, 39)
        Me.gbxDetails.TabIndex = 0
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(9, 13)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(24, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Date"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtDate
        '
        Me.cdtDate.EditValue = Nothing
        Me.cdtDate.EnterMoveNextControl = True
        Me.cdtDate.Location = New System.Drawing.Point(67, 9)
        Me.cdtDate.Name = "cdtDate"
        Me.cdtDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtDate.Properties.Appearance.Options.UseFont = True
        Me.cdtDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDate.Size = New System.Drawing.Size(92, 22)
        Me.cdtDate.TabIndex = 1
        Me.cdtDate.Tag = "AE"
        Me.cdtDate.Value = Nothing
        '
        'cbxWeather
        '
        Me.cbxWeather.AllowBlank = False
        Me.cbxWeather.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxWeather.DataSource = Nothing
        Me.cbxWeather.DisplayMember = Nothing
        Me.cbxWeather.EnterMoveNextControl = True
        Me.cbxWeather.Location = New System.Drawing.Point(285, 9)
        Me.cbxWeather.Name = "cbxWeather"
        Me.cbxWeather.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxWeather.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxWeather.Properties.Appearance.Options.UseFont = True
        Me.cbxWeather.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxWeather.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxWeather.SelectedValue = Nothing
        Me.cbxWeather.Size = New System.Drawing.Size(502, 22)
        Me.cbxWeather.TabIndex = 3
        Me.cbxWeather.ValueMember = Nothing
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(235, 12)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(44, 15)
        Me.CareLabel2.TabIndex = 2
        Me.CareLabel2.Text = "Weather"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cgEntries
        '
        Me.cgEntries.AllowBuildColumns = True
        Me.cgEntries.AllowEdit = False
        Me.cgEntries.AllowHorizontalScroll = False
        Me.cgEntries.AllowMultiSelect = False
        Me.cgEntries.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgEntries.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgEntries.Appearance.Options.UseFont = True
        Me.cgEntries.AutoSizeByData = True
        Me.cgEntries.DisableAutoSize = False
        Me.cgEntries.DisableDataFormatting = False
        Me.cgEntries.FocusedRowHandle = -2147483648
        Me.cgEntries.HideFirstColumn = False
        Me.cgEntries.Location = New System.Drawing.Point(9, 8)
        Me.cgEntries.Name = "cgEntries"
        Me.cgEntries.PreviewColumn = ""
        Me.cgEntries.QueryID = Nothing
        Me.cgEntries.RowAutoHeight = False
        Me.cgEntries.SearchAsYouType = True
        Me.cgEntries.ShowAutoFilterRow = False
        Me.cgEntries.ShowFindPanel = False
        Me.cgEntries.ShowGroupByBox = False
        Me.cgEntries.ShowLoadingPanel = False
        Me.cgEntries.ShowNavigator = False
        Me.cgEntries.Size = New System.Drawing.Size(778, 394)
        Me.cgEntries.TabIndex = 0
        '
        'btnCommit
        '
        Me.btnCommit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCommit.Location = New System.Drawing.Point(115, 408)
        Me.btnCommit.Name = "btnCommit"
        Me.btnCommit.Size = New System.Drawing.Size(100, 23)
        Me.btnCommit.TabIndex = 1
        Me.btnCommit.TabStop = False
        Me.btnCommit.Text = "Commit Diary"
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.btnDelete)
        Me.GroupControl1.Controls.Add(Me.btnCommit)
        Me.GroupControl1.Controls.Add(Me.cgEntries)
        Me.GroupControl1.Location = New System.Drawing.Point(14, 136)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(795, 439)
        Me.GroupControl1.TabIndex = 2
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.Location = New System.Drawing.Point(9, 408)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(100, 23)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.TabStop = False
        Me.btnDelete.Text = "Delete Item"
        '
        'frmDiaryEntry
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(823, 588)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.gbxDetails)
        Me.Controls.Add(Me.gbxMatch)
        Me.LoadMaximised = True
        Me.Margin = New System.Windows.Forms.Padding(3, 5, 3, 5)
        Me.Name = "frmDiaryEntry"
        Me.Text = "frmDiaryEntry"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.gbxMatch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxMatch.ResumeLayout(False)
        Me.gbxMatch.PerformLayout()
        CType(Me.txtEmployeeName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEmployee.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJobName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJob.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxDetails.ResumeLayout(False)
        Me.gbxDetails.PerformLayout()
        CType(Me.cdtDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxWeather.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents gbxMatch As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtJobName As Care.Controls.CareTextBox
    Friend WithEvents btnJobSearch As Care.Controls.CareButton
    Friend WithEvents txtJob As Care.Controls.CareTextBox
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents gbxDetails As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents cdtDate As Care.Controls.CareDateTime
    Friend WithEvents cbxWeather As Care.Controls.CareComboBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents txtEmployeeName As Care.Controls.CareTextBox
    Friend WithEvents btnEmployeeSearch As Care.Controls.CareButton
    Friend WithEvents txtEmployee As Care.Controls.CareTextBox
    Friend WithEvents btnAdd As Care.Controls.CareButton
    Friend WithEvents btnHoliday As Care.Controls.CareButton
    Friend WithEvents btnSick As Care.Controls.CareButton
    Friend WithEvents btnAbsent As Care.Controls.CareButton
    Friend WithEvents cgEntries As Care.Controls.CareGrid
    Friend WithEvents btnCommit As Care.Controls.CareButton
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnDelete As Care.Controls.CareButton
End Class
