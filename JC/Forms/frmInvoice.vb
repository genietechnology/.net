﻿Imports Care.Global
Imports Care.Shared

Public Class frmInvoice

    Private m_New As Boolean = False
    Private m_Financials As Care.Financials.Engine
    Private m_FinancialsLinked As Boolean = False

    Public Sub New(ByVal JobID As Guid, ByVal InvoiceID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        If InvoiceID.HasValue Then
            Me.Text = "Amend Invoice"
        Else
            Me.Text = "Create New Invoice"
            m_New = True
        End If

    End Sub

    Private Sub frmInvoice_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'm_Financials = New Care.Financials.Engine(ReturnFinancialsIntegration)
        FindMode()
    End Sub

    Private Sub FindMode()
        MyControls.SetControls(ControlHandler.Mode.Locked, Me.Controls)
        MyControls.SetControls(ControlHandler.Mode.Edit, Me.Controls)
        txtAccountNumber.BackColor = Session.FindColour
        btnFind.Enabled = True
        txtAccountNumber.Focus()
    End Sub

    Private Sub radFind_CheckedChanged(sender As Object, e As EventArgs) Handles radFind.CheckedChanged
        If radFind.Checked Then FindMode()
    End Sub

    Private Sub radNew_CheckedChanged(sender As Object, e As EventArgs) Handles radNew.CheckedChanged
        If radNew.Checked Then MyControls.SetControls(ControlHandler.Mode.Add, Me.Controls)
        btnFind.Enabled = False
        txtAccountNumber.Focus()
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnFind_Click(sender As Object, e As EventArgs) Handles btnFind.Click

    End Sub

End Class