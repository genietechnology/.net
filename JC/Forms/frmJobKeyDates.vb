﻿Option Strict On

Imports Care.Global
Imports Care.Shared

Public Class frmJobKeyDates

    Private m_Job As Business.Job

    Public Sub New(ByVal JobRecord As Business.Job)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_Job = JobRecord

    End Sub

    Private Sub frmJobKeyDates_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MyControls.SetControls(ControlHandler.Mode.Edit, Me.Controls)
        DisplayRecord()
    End Sub

    Private Sub DisplayRecord()

        Me.Text = m_Job._JobNo.ToString + " - " + m_Job._RskFullname

        cdtSurvey.Value = m_Job._KeySurvey
        cdtEstSub.Value = m_Job._KeyEstsub
        cdtApproval.Value = m_Job._KeyApproval
        cdtWorksOrderRcd.Value = m_Job._KeyWorksorder
        cdtWorksStartDate.Value = m_Job._KeyWork
        cdtCompletion.Value = m_Job._JobEnd
        cdtCompCertSent.Value = m_Job._KeyCcSent
        cdtCompCertRcd.Value = m_Job._KeyCcRcd
        cdtPaid.Value = m_Job._KeyPaid

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click

        Dim _Feedback As Boolean = False

        ChangeJobStatus(cdtSurvey, m_Job._KeySurvey, "KEY_SURVEY", "Survey date entered")

        If ChangeJobStatus(cdtEstSub, m_Job._KeyEstsub, "KEY_ESTSUB", "Estimate submission date entered") Then

            If CareMessage("Is a Drying Company required for this Job?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Mark as Paid") = DialogResult.Yes Then
                m_Job._Dc = True
                m_Job._DcRecd = "N"
            Else
                m_Job._Dc = False
            End If

            m_Job._DcAskUser = Session.CurrentUser.UserCode
            m_Job._DcAskStamp = Now

        End If

        ChangeJobStatus(cdtApproval, m_Job._KeyApproval, "KEY_APPROVAL", "Estimate approval date entered")
        ChangeJobStatus(cdtWorksOrderRcd, m_Job._KeyWorksorder, "KEY_WORKSORDER", "Works order received")
        ChangeJobStatus(cdtWorksStartDate, m_Job._KeyWork, "KEY_WORK", "Works commencement date entered")

        If ChangeJobStatus(cdtCompletion, m_Job._JobEnd, "KEY_COMPLETE", "Completion date entered") Then
            SendJobCompleteEmail
        End If

        ChangeJobStatus(cdtCompCertSent, m_Job._KeyCcSent, "KEY_CCSENT", "Completion certificate sent")

        If ChangeJobStatus(cdtCompCertRcd, m_Job._KeyCcRcd, "KEY_CCRCD", "Completion certificate received") Then
            _Feedback = True
        End If

        If cdtPaid.Text <> "" Then
            If m_Job._KeyPaid.HasValue Then
                If CanMarkAsPaid() Then
                    If cdtPaid.Value <> m_Job._KeyPaid.Value Then
                        m_Job._JobStatusCode = ParameterHandler.ReturnString("KEY_PAID")
                        m_Job._JobStatus = ListHandler.ReturnItemUsingRef1("Job Status", m_Job._JobStatusCode).Name
                        Business.Job.CreateActivity(m_Job._ID.Value, "Paid date changed", cdtPaid.Value)
                        m_Job._KeyPaid = cdtPaid.Value
                    End If
                End If
            Else
                If CareMessage("You have entered a paid date. Do you wish to continue?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Mark as Paid") = DialogResult.Yes Then
                    'check if current user is permitted to mark jobs as paid
                    If CanMarkAsPaid() Then
                        m_Job._JobStatusCode = ParameterHandler.ReturnString("KEY_PAID")
                        m_Job._JobStatus = ListHandler.ReturnItemUsingRef1("Job Status", m_Job._JobStatusCode).Name
                        Business.Job.CreateActivity(m_Job._ID.Value, "Paid date entered", cdtPaid.Value)
                        m_Job._KeyPaid = cdtPaid.Value
                    Else
                        CareMessage("You do not have permissions to mark a job as paid.", MessageBoxIcon.Exclamation, "Mark as Paid")
                        Exit Sub
                    End If
                End If
            End If
        Else
            m_Job._KeyPaid = Nothing
        End If

        m_Job.Store()

        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()

    End Sub

    Private Sub SendJobCompleteEmail()

    End Sub

    Private Function CanMarkAsPaid() As Boolean
        Dim _li As ListHandler.ListItem = ListHandler.ReturnItemUsingRef1("Job Status", ParameterHandler.ReturnString("KEY_PAID"))
        If _li IsNot Nothing Then
            If _li.Ref2.ToLower.Contains(Session.CurrentUser.UserCode.ToLower) Then
                Return True
            End If
        End If
        Return False
    End Function

    Private Function ChangeJobStatus(ByVal DateControlIn As Care.Controls.CareDateTime, ByRef DBDateValue As Date?, ByVal ParamStatus As String, ByVal ActivityText As String) As Boolean
        Dim _Return As Boolean = False
        If DateControlIn.Text <> "" Then
            If Not DBDateValue.HasValue Then
                Dim _Code As String = ParameterHandler.ReturnString(ParamStatus)
                If _Code = "" Then
                    CareMessage("Warning - No Job Status found using " + ParamStatus, MessageBoxIcon.Exclamation, "Change Job Status")
                    _Return = False
                Else
                    m_Job._JobStatusCode = _Code
                    m_Job._JobStatus = ListHandler.ReturnItemUsingRef1("Job Status", m_Job._JobStatusCode).Name
                    Business.Job.CreateActivity(m_Job._ID.Value, ActivityText, DateControlIn.Value)
                    _Return = True
                End If
            End If
            DBDateValue = DateControlIn.Value
        Else
            DBDateValue = Nothing
        End If
        Return _Return
    End Function

End Class