﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmJobPrint
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnJobDesc = New Care.Controls.CareButton(Me.components)
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.btnEstimatorsNotes = New Care.Controls.CareButton(Me.components)
        Me.btnDirections = New Care.Controls.CareButton(Me.components)
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'btnJobDesc
        '
        Me.btnJobDesc.Location = New System.Drawing.Point(12, 12)
        Me.btnJobDesc.Name = "btnJobDesc"
        Me.btnJobDesc.Size = New System.Drawing.Size(160, 25)
        Me.btnJobDesc.TabIndex = 0
        Me.btnJobDesc.Text = "Print Job Description"
        '
        'btnClose
        '
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(12, 105)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(160, 25)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "Close"
        '
        'btnEstimatorsNotes
        '
        Me.btnEstimatorsNotes.Location = New System.Drawing.Point(12, 74)
        Me.btnEstimatorsNotes.Name = "btnEstimatorsNotes"
        Me.btnEstimatorsNotes.Size = New System.Drawing.Size(160, 25)
        Me.btnEstimatorsNotes.TabIndex = 2
        Me.btnEstimatorsNotes.Text = "Print Estimators Notes"
        '
        'btnDirections
        '
        Me.btnDirections.Location = New System.Drawing.Point(12, 43)
        Me.btnDirections.Name = "btnDirections"
        Me.btnDirections.Size = New System.Drawing.Size(160, 25)
        Me.btnDirections.TabIndex = 1
        Me.btnDirections.Text = "Print Directions Only"
        '
        'frmJobPrint
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(184, 141)
        Me.Controls.Add(Me.btnDirections)
        Me.Controls.Add(Me.btnEstimatorsNotes)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnJobDesc)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmJobPrint"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Print Options"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnJobDesc As Care.Controls.CareButton
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents btnEstimatorsNotes As Care.Controls.CareButton
    Friend WithEvents btnDirections As Care.Controls.CareButton
End Class
