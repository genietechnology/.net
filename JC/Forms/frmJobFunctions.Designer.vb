﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmJobFunctions
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnHold = New Care.Controls.CareButton(Me.components)
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.btnLink = New Care.Controls.CareButton(Me.components)
        Me.btnCostAnal = New Care.Controls.CareButton(Me.components)
        Me.btnFeedback = New Care.Controls.CareButton(Me.components)
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'btnHold
        '
        Me.btnHold.Location = New System.Drawing.Point(12, 12)
        Me.btnHold.Name = "btnHold"
        Me.btnHold.Size = New System.Drawing.Size(160, 25)
        Me.btnHold.TabIndex = 0
        Me.btnHold.Text = "Hold Job"
        '
        'btnClose
        '
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(12, 136)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(160, 25)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Text = "Close"
        '
        'btnLink
        '
        Me.btnLink.Location = New System.Drawing.Point(12, 74)
        Me.btnLink.Name = "btnLink"
        Me.btnLink.Size = New System.Drawing.Size(160, 25)
        Me.btnLink.TabIndex = 2
        Me.btnLink.Text = "Link this Job"
        '
        'btnCostAnal
        '
        Me.btnCostAnal.Location = New System.Drawing.Point(12, 43)
        Me.btnCostAnal.Name = "btnCostAnal"
        Me.btnCostAnal.Size = New System.Drawing.Size(160, 25)
        Me.btnCostAnal.TabIndex = 1
        Me.btnCostAnal.Text = "Costing Analysis"
        '
        'btnFeedback
        '
        Me.btnFeedback.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnFeedback.Location = New System.Drawing.Point(12, 105)
        Me.btnFeedback.Name = "btnFeedback"
        Me.btnFeedback.Size = New System.Drawing.Size(160, 25)
        Me.btnFeedback.TabIndex = 3
        Me.btnFeedback.Text = "Customer Feedback Survey"
        '
        'frmJobFunctions
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(184, 172)
        Me.Controls.Add(Me.btnFeedback)
        Me.Controls.Add(Me.btnCostAnal)
        Me.Controls.Add(Me.btnLink)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnHold)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmJobFunctions"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Job Functions"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnHold As Care.Controls.CareButton
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents btnLink As Care.Controls.CareButton
    Friend WithEvents btnCostAnal As Care.Controls.CareButton
    Friend WithEvents btnFeedback As Care.Controls.CareButton
End Class
