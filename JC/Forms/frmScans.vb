﻿Option Strict On

Imports Care.Global
Imports Care.Data
Imports Care.Shared

Imports System.Text
Imports System.IO
Imports System.Reflection
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraTab
Imports DevExpress.XtraGrid.Views.Base

Public Class frmScans

    Private Enum EnumStatus
        Queried
        AwaitingAuth
        Duplicate
        Authorised
        Paid
    End Enum

    Private m_Scan As Business.Scan
    Private m_DocConnectionString As String

    Private m_TargetID As Guid? = Nothing
    Private m_OwnerCode As String = ""
    Private m_OwnerName As String = ""

    Private Sub frmScans_Load(sender As Object, e As EventArgs) Handles Me.Load

        m_DocConnectionString = Session.ConnectionString.Replace("jc", "doc")

        cbxDocType.AddItem("Supplier Invoice")
        cbxDocType.AddItem("Supplier Credit")
        cbxDocType.AddItem("Contractor Invoice")
        cbxDocType.AddItem("Contractor Credit")

        cbxPaymentMethod.AddItem("On Account")
        cbxPaymentMethod.AddItem("Paid - Paypal")
        cbxPaymentMethod.AddItem("Paid - Credit Card")
        cbxPaymentMethod.AddItem("Paid - Cash")
        cbxPaymentMethod.AddItem("Paid - BACS")
        cbxPaymentMethod.AddItem("Paid - Cheque")

        cbxCategory.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Document Category"))
        cbxCategory.AllowBlank = True

        cbxSelectIntray.PopulateWithSQL(Session.ConnectionString, "select username, fullname from AppUsers order by fullname")
        cbxSendIntray.PopulateWithSQL(Session.ConnectionString, "select username, fullname from AppUsers order by fullname")
        cbxWorkType.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("INVOICE / CREDIT CATEGORIES"))
        cbxSupplier.PopulateWithSQL(Session.ConnectionString, "select ID, sup_name from Suppliers order by sup_name")

        cbxCategory.Enabled = True
        cbxCategory.BackColor = Session.FindColour

        txtRef.Enabled = True
        txtRef.BackColor = Session.FindColour()

        SetMatch()
        Clear()

        gbxDetails.Visible = False
        gbxButtons.Visible = False

        PopulateCombo(cbx1)
        PopulateCombo(cbx2)
        PopulateCombo(cbx3)
        PopulateCombo(cbx4)
        PopulateCombo(cbx5)
        PopulateCombo(cbx6)
        PopulateCombo(cbx7)
        PopulateCombo(cbx8)
        PopulateCombo(cbx9)
        PopulateCombo(cbx10)
        PopulateCombo(cbx11)
        PopulateCombo(cbx12)

    End Sub

    Private Sub PopulateCombo(ByRef Combo As Care.Controls.CareComboBox)
        Combo.AddItem(" ")
        Combo.AddItem("OK")
        Combo.AddItem("Rate")
        Combo.AddItem("Price")
        Combo.AddItem("5 %")
        Combo.AddItem("10 %")
        Combo.AddItem("15 %")
        Combo.AddItem("20 %")
        Combo.AddItem("25 %")
        Combo.AddItem("30 %")
        Combo.AddItem("35 %")
        Combo.AddItem("40 %")
        Combo.AddItem("45 %")
        Combo.AddItem("50 %")
        Combo.AddItem("55 %")
        Combo.AddItem("60 %")
        Combo.AddItem("65 %")
        Combo.AddItem("70 %")
        Combo.AddItem("75 %")
        Combo.AddItem("80 %")
        Combo.AddItem("85 %")
        Combo.AddItem("90 %")
        Combo.AddItem("95 %")
    End Sub

    Private Sub frmScans_ResizeEnd(sender As Object, e As EventArgs) Handles Me.ResizeEnd
        MoveSplitter()
    End Sub

    Private Sub frmScans_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        PopulateScans()
        MoveSplitter()
    End Sub

    Private Sub SetDetails()
        If txtName.Text = "" Then
            gbxDetails.Enabled = False
        Else
            gbxDetails.Enabled = True
        End If
    End Sub

    Private Sub MoveSplitter()
        '10% of the height
        Dim _10 As Integer = sccNotes.Height \ 10
        sccNotes.SplitterPosition = sccNotes.Height - _10
    End Sub

    Private Sub PopulateScans(Optional PersistPosition As Boolean = False)

        Session.CursorWaiting()

        Clear()

        Dim _SQL As String = ""

        If ctView.SelectedTabPage.Name = "tabViewImage" Then
            If PersistPosition Then cgDocuments.PersistRowPosition()
        Else
            If PersistPosition Then cgDetail.PersistRowPosition()
        End If

        If chkTop20.Checked Then
            _SQL += "select top 20"
        Else
            _SQL += "select"
        End If

        _SQL += " ID, doc_image as 'Preview',"
        _SQL += " matched_owner, matched_owner_name, doc_status, matched_target, matched_target_id, matched_target_no, matched_target_multi, matched_name, matched_ref1,"
        _SQL += " doc_date, doc_type, doc_supplier_name, doc_desc, doc_ref, doc_net, chk_dn, chk_used, chk_price_reg, chk_price, doc_priority"
        _SQL += " from Scans"
        _SQL += " where scanned_file <> ''"

        If radQScanned.Checked Then _SQL += " and doc_status = 'Scanned'"
        If radQMatched.Checked Then _SQL += " and doc_status = 'Matched'"
        If radQDuplicates.Checked Then _SQL += " and doc_status = 'Duplicate'"
        If radQueried.Checked Then _SQL += " and doc_status = 'Queried'"
        If radQAwaitingAuth.Checked Then _SQL += " and doc_status = 'Awaiting Authorisation'"
        If radQAuthorised.Checked Then _SQL += " and doc_status = 'Authorised'"
        If radPaid.Checked Then _SQL += " and doc_status = 'Paid'"

        If radAllTrays.Checked Then
            'no filter needed
        Else
            If radSpecificTray.Checked AndAlso cbxSelectIntray.Text <> "" Then
                _SQL += " and doc_intray_name = '" + cbxSelectIntray.Text + "'"
            Else
                If radMyIntray.Checked Then
                    _SQL += " and doc_intray = '" + Session.CurrentUser.UserCode + "'"
                Else
                    _SQL += " and doc_intray = ''"
                End If
            End If
        End If

        If radQScanned.Checked Then
            _SQL += " order by doc_priority desc, scanned_stamp desc"
        Else
            If radQMatched.Checked Then
                _SQL += " order by doc_priority desc, matched_stamp desc"
            Else
                If radQAuthorised.Checked Then
                    _SQL += " order by doc_priority desc, authorised_stamp desc"
                Else
                    _SQL += " order by doc_priority desc, last_stamp desc"
                End If
            End If
        End If

        If ctView.SelectedTabPage.Name = "tabViewImage" Then

            If PersistPosition Then cgDocuments.PersistRowPosition()

            cgDocuments.HideFirstColumn = True
            cgDocuments.Populate(m_DocConnectionString, _SQL)

            cgDocuments.Columns("matched_owner").Visible = False
            cgDocuments.Columns("matched_owner_name").Visible = False
            cgDocuments.Columns("doc_status").Visible = False
            cgDocuments.Columns("matched_target").Visible = False
            cgDocuments.Columns("matched_target_id").Visible = False
            cgDocuments.Columns("matched_target_no").Visible = False
            cgDocuments.Columns("matched_target_multi").Visible = False
            cgDocuments.Columns("matched_name").Visible = False
            cgDocuments.Columns("matched_ref1").Visible = False
            cgDocuments.Columns("doc_date").Visible = False
            cgDocuments.Columns("doc_type").Visible = False
            cgDocuments.Columns("doc_supplier_name").Visible = False
            cgDocuments.Columns("doc_desc").Visible = False
            cgDocuments.Columns("doc_ref").Visible = False
            cgDocuments.Columns("doc_net").Visible = False
            cgDocuments.Columns("chk_dn").Visible = False
            cgDocuments.Columns("chk_used").Visible = False
            cgDocuments.Columns("chk_price_reg").Visible = False
            cgDocuments.Columns("chk_price").Visible = False
            cgDocuments.Columns("doc_priority").Visible = False

            If PersistPosition Then cgDocuments.RestoreRowPosition()

            If cgDocuments.RecordCount > 0 Then

                cgDocuments.MoveFirst()

                If radQScanned.Checked Then
                    gbxDetails.Visible = False
                    gbxButtons.Visible = False
                Else
                    'any other stage, the document will be matched
                    gbxDetails.Visible = True
                    gbxButtons.Visible = True
                End If

            Else
                pic.Image = Nothing
            End If

        Else

            If PersistPosition Then cgDetail.PersistRowPosition()

            cgDetail.HideFirstColumn = True
            cgDetail.Populate(m_DocConnectionString, _SQL)

            cgDetail.Columns("Preview").Visible = False
            cgDetail.Columns("matched_owner").Visible = False
            cgDetail.Columns("matched_owner_name").Visible = True
            cgDetail.Columns("matched_target").Visible = True
            cgDetail.Columns("matched_target_id").Visible = False
            cgDetail.Columns("matched_target_no").Visible = True
            cgDetail.Columns("matched_target_multi").Visible = True
            cgDetail.Columns("matched_name").Visible = True
            cgDetail.Columns("matched_ref1").Visible = True
            cgDetail.Columns("doc_date").Visible = True
            cgDetail.Columns("doc_type").Visible = True
            cgDetail.Columns("doc_supplier_name").Visible = True
            cgDetail.Columns("doc_desc").Visible = True
            cgDetail.Columns("doc_ref").Visible = True
            cgDetail.Columns("doc_net").Visible = True
            cgDetail.Columns("chk_dn").Visible = True
            cgDetail.Columns("chk_used").Visible = True
            cgDetail.Columns("chk_price_reg").Visible = True
            cgDetail.Columns("chk_price").Visible = True
            cgDetail.Columns("doc_priority").Visible = True

            If PersistPosition Then cgDetail.RestoreRowPosition()

            If cgDetail.RecordCount > 0 Then

                cgDetail.MoveFirst()

                If radQScanned.Checked Then
                    gbxDetails.Visible = False
                    gbxButtons.Visible = False
                Else
                    'any other stage, the document will be matched
                    gbxDetails.Visible = True
                    gbxButtons.Visible = True
                End If

            Else
                pic.Image = Nothing
            End If
        End If

        Session.CursorDefault()

    End Sub

    Private Function ValidateEntry() As Boolean

        If radMatchCategory.Checked Then
            If cbxCategory.Text = "" Then
                CareMessage("Please select a category.", MessageBoxIcon.Exclamation, "Validate Entry")
                Return False
            End If
        Else

            If txtRef.Text = "" Then
                If radMatchJob.Checked Then
                    CareMessage("Please enter a Job Number.", MessageBoxIcon.Exclamation, "Validate Entry")
                    Return False
                Else
                    CareMessage("Please enter a Vehicle Registration.", MessageBoxIcon.Exclamation, "Validate Entry")
                    Return False
                End If
            Else
                If Not m_TargetID.HasValue Then
                    CareMessage("Invalid Reference!", MessageBoxIcon.Exclamation, "Validate Entry")
                    Return False
                End If
            End If

        End If

        'if we get this far, check we have matched the rest of the document details
        If Not radMatchCategory.Checked Then

            If cdtDocDate.Text = "" Then
                CareMessage("Please enter the document date.", MessageBoxIcon.Exclamation, "Validate Entry")
                Return False
            End If

            If cbxDocType.Text = "" Then
                CareMessage("Please select a document type.", MessageBoxIcon.Exclamation, "Validate Entry")
                Return False
            End If

            If cbxSupplier.Text = "" Then
                CareMessage("Please select a supplier.", MessageBoxIcon.Exclamation, "Validate Entry")
                Return False
            End If

            If cbxWorkType.Text = "" Then
                CareMessage("Please select an item description.", MessageBoxIcon.Exclamation, "Validate Entry")
                Return False
            End If

            If cbxPaymentMethod.Text = "" Then
                CareMessage("Please select a payment method.", MessageBoxIcon.Exclamation, "Validate Entry")
                Return False
            End If

            If txtNet.Text = "" Then
                CareMessage("Please enter the Net (before VAT) value.", MessageBoxIcon.Exclamation, "Validate Entry")
                Return False
            End If

            Dim _Value As Decimal = ValueHandler.ConvertDecimal(txtNet.Text)

            If cbxDocType.Text.Contains("Credit") Then
                If _Value > 0 Then
                    CareMessage("Please enter the credit amount as a negative value.", MessageBoxIcon.Exclamation, "Validate Entry")
                    Return False
                End If
            Else
                If _Value < 0 Then
                    CareMessage("Please enter the invoice amount as a positive value.", MessageBoxIcon.Exclamation, "Validate Entry")
                    Return False
                End If
            End If

            'ensure we have not changed the target
            If m_Scan IsNot Nothing Then

                If m_Scan._MatchedTarget = "Job" And Not radMatchJob.Checked Then
                    CareMessage("You cannot amend the Target Type.", MessageBoxIcon.Exclamation, "Validate Entry")
                    Return False
                End If

                If m_Scan._MatchedTarget = "Plant" And Not radMatchPlant.Checked Then
                    CareMessage("You cannot amend the Target Type.", MessageBoxIcon.Exclamation, "Validate Entry")
                    Return False
                End If

                If m_Scan._MatchedTarget = "Category" And Not radMatchCategory.Checked Then
                    CareMessage("You cannot amend the Target Type.", MessageBoxIcon.Exclamation, "Validate Entry")
                    Return False
                End If




            End If


            End If

        Return True

    End Function

    Private Sub SaveDocument()

        If Not ValidateEntry() Then Exit Sub
        If m_Scan Is Nothing Then Exit Sub

        Dim _NewMatch As Boolean = False
        With m_Scan

            If ._MatchedOwner = "" Then _NewMatch = True

            If m_TargetID.HasValue Then

                If radMatchJob.Checked Then
                    ._MatchedTarget = "Job"
                    ._MatchedTargetId = m_TargetID
                    ._MatchedTargetNo = txtRef.Text
                    ._MatchedName = txtName.Text
                    ._MatchedRef1 = txtRef1.Text
                    ._MatchedOwner = m_OwnerCode
                    ._MatchedOwnerName = m_OwnerName
                Else
                    If radMatchPlant.Checked Then
                        ._MatchedTarget = "Plant"
                        ._MatchedTargetId = m_TargetID
                        ._MatchedTargetNo = txtRef.Text
                        ._MatchedName = txtName.Text
                        ._MatchedRef1 = txtRef1.Text
                        ._MatchedOwner = m_OwnerCode
                        ._MatchedOwnerName = m_OwnerName
                    Else
                        If radMatchCategory.Checked Then
                            ._MatchedTarget = "Category"
                            ._MatchedTargetId = Nothing
                            ._MatchedTargetNo = ""
                            ._MatchedName = cbxCategory.Text
                            ._MatchedRef1 = ""
                            ._MatchedOwner = ""
                            ._MatchedOwnerName = ""
                        End If
                    End If
                End If

            End If

            If ._MatchedBy = "" Then
                ._MatchedBy = Session.CurrentUser.UserCode
                ._MatchedStamp = Now
            End If

            'new match, so we set the in-tray to the document owner
            If _NewMatch Then

                ._DocStatus = "Matched"
                ._DocIntray = m_OwnerCode
                ._DocIntrayName = m_OwnerName

                If ._DocIntray.ToUpper = "AJ" Then ._DocStatus = "Awaiting Authorisation"

            End If

            ._DocDate = cdtDocDate.Value
            ._DocType = cbxDocType.Text
            ._DocSupplierId = New Guid(cbxSupplier.SelectedValue.ToString)
            ._DocSupplierName = cbxSupplier.Text
            ._DocDesc = cbxWorkType.Text
            ._DocRef = txtDocRef.Text
            ._PaymentMethod = cbxPaymentMethod.Text
            ._DocNet = ValueHandler.ConvertDecimal(txtNet.Text)

            ._ChkDn = chkDN.Checked
            ._ChkUsed = chkUsedJob.Checked
            ._ChkPriceReg = chkPreNeg.Checked
            ._ChkPrice = chkPrice.Checked
            ._DocPriority = chkFastTrack.Checked

            ._LastUser = Session.CurrentUser.UserCode
            ._LastStamp = Now

            ._DocImage = DocumentHandler.ImageToByteArray(pic.Image)

            .Store(m_DocConnectionString)

            .UpdateTarget()

        End With

    End Sub

    Private Sub SaveImageOnly()

        If m_Scan IsNot Nothing Then
            With m_Scan
                ._DocImage = DocumentHandler.ImageToByteArray(pic.Image)
                .Store(m_DocConnectionString)
            End With
        End If

        PopulateScans()

    End Sub

    Private Sub cgDocuments_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles cgDocuments.FocusedRowChanged
        HandleFocusedRowChanged(cgDocuments, e)
    End Sub

    Private Sub HandleFocusedRowChanged(ByRef GridControl As Care.Controls.CareGrid, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs)

        If e Is Nothing Then Exit Sub
        If e.FocusedRowHandle < 0 Then Exit Sub

        Dim _ID As String = GridControl.GetRowCellValue(e.FocusedRowHandle, "ID").ToString
        If _ID <> "" Then

            m_Scan = Business.Scan.RetreiveByID(m_DocConnectionString, New Guid(_ID))

            If Not m_Scan Is Nothing Then

                Dim _img As Byte() = CType(GridControl.GetRowCellValue(e.FocusedRowHandle, "Preview"), Byte())
                pic.Image = Care.Shared.DocumentHandler.GetImagefromByteArray(_img)

                DisplayScanDetails()

            End If

        End If

    End Sub

    Private Sub DisplayComments(ByVal DocID As Guid?)

        txtComments.Text = ""

        If DocID = Nothing Then Exit Sub
        If Not DocID.HasValue Then Exit Sub

        Dim _SQL As String = ""
        _SQL += "select ID, user_name as 'User', stamp as 'Stamp', comments from ScanComments"
        _SQL += " where scan_id = '" + DocID.ToString + "'"
        _SQL += " order by stamp desc"

        cgComments.HideFirstColumn = True
        cgComments.Populate(m_DocConnectionString, _SQL)

        If cgComments IsNot Nothing Then
            cgComments.Columns("comments").Visible = False
            cgComments.Columns("Stamp").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            cgComments.Columns("Stamp").DisplayFormat.FormatString = "dd/MM/yyyy HH:mm"
            btnNoteDelete.Enabled = cgComments.RecordCount > 0
        End If

    End Sub

    Private Sub DisplayScanDetails()

        If Not m_Scan Is Nothing Then

            Clear()

            With m_Scan

                lblWho.Text = "Current Tray: " + m_Scan._DocIntrayName + "   |   Owner: " + m_Scan._MatchedOwnerName + "   |   Status: " + m_Scan._DocStatus
                DisplayComments(m_Scan._ID)

                If m_Scan._DocStatus = "Matched" OrElse m_Scan._DocStatus = "Categorised" OrElse m_Scan._DocStatus = "Queried" OrElse m_Scan._DocStatus = "Awaiting Authorisation" OrElse m_Scan._DocStatus = "Authorised" Then
                    sccPictureNotes.SetPanelCollapsed(False)
                Else
                    sccPictureNotes.SetPanelCollapsed(True)
                End If

                If m_Scan._DocStatus = "Authorised" OrElse m_Scan._DocStatus = "Paid" Then
                    btnKeith.Enabled = False
                    btnPassAuth.Enabled = False
                    btnReturnToController.Enabled = False
                Else

                    'if the document is already with keith, we don't want to send it to him again...
                    If m_Scan._DocIntray.ToUpper = "KH" Then
                        btnKeith.Enabled = False
                    Else
                        btnKeith.Enabled = True
                    End If

                    'if the document is already with aj, we don't want to send it to him again...
                    If m_Scan._DocIntray.ToUpper = "AJ" Then
                        btnPassAuth.Enabled = False
                    Else
                        btnPassAuth.Enabled = True
                    End If

                    'if the document owner and intray is the same, we cannot send it back...
                    If m_Scan._DocIntray.ToUpper = m_Scan._MatchedOwner.ToUpper Then
                        btnReturnToController.Enabled = False
                    Else
                        btnReturnToController.Enabled = True
                    End If

                End If

                If ._DocStatus = "Scanned" Then


                Else

                    If ._MatchedTarget = "Category" Then
                        radMatchCategory.Checked = True
                        m_TargetID = Nothing
                        cbxCategory.Text = ._MatchedName
                    Else

                        If ._MatchedTarget = "Job" Then
                            radMatchJob.Checked = True
                        Else
                            radMatchPlant.Checked = True
                        End If

                        m_TargetID = ._MatchedTargetId

                        m_OwnerCode = ._MatchedOwner
                        m_OwnerName = ._MatchedOwnerName
                        txtRef.Text = ._MatchedTargetNo
                        txtName.Text = ._MatchedName
                        txtRef1.Text = ._MatchedRef1

                        cdtDocDate.Value = ._DocDate
                        cbxDocType.Text = ._DocType
                        cbxSupplier.Text = ._DocSupplierName
                        cbxWorkType.Text = ._DocDesc
                        cbxPaymentMethod.Text = ._PaymentMethod
                        txtDocRef.Text = ._DocRef
                        txtNet.Text = Format(._DocNet, "0.00")

                        chkDN.Checked = ._ChkDn
                        chkUsedJob.Checked = ._ChkUsed
                        chkPrice.Checked = ._ChkPrice
                        chkPreNeg.Checked = ._ChkPriceReg
                        chkFastTrack.Checked = ._DocPriority

                        If ._DocStatus = "Matched" OrElse ._DocStatus = "Duplicate" OrElse ._DocStatus = "Queried" Then
                            btnPassAuth.Enabled = True
                        Else
                            btnPassAuth.Enabled = False
                        End If

                    End If

                    ToggleEdit(True)

                End If

            End With

        End If

    End Sub

#Region "Label Printing"

    Private Function GetLetter(ByVal Number As Long) As String

        Dim _Count As Long = 0
        Dim _Temp As String = ""
        Dim _Digits As String = ""

        _Digits = " " & "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

        _Count = (Number - 1) \ (Len(_Digits) - 1)
        Number = Number - (_Count * (Len(_Digits) - 1))
        If _Count > 0 Then _Temp = GetLetter(_Count)
        GetLetter = _Temp + Mid$(_Digits, CInt(Number + 1), 1)

    End Function

    Private Sub PrintLabel()

        Dim _ExePath As String = My.Application.Info.DirectoryPath + "\AJLabelPrint.exe"

        If IO.File.Exists(_ExePath) Then

            Dim _Number As String = InputBox("ENTER NUMBER")
            Dim _BarCode As String = GetLetter(CLng(_Number))
            Dim _PaddedBarCode As String = _BarCode.PadLeft(5, CChar("0"))

            Dim _psi As New System.Diagnostics.ProcessStartInfo
            _psi.FileName = _ExePath

            _psi.Arguments = _PaddedBarCode + " " + _Number

            Dim _p As New System.Diagnostics.Process
            _p.StartInfo = _psi
            _p.Start()
            _p.WaitForExit()

            MessageBox.Show(_p.ExitCode.ToString)
            _p.Close()

        Else
            MessageBox.Show("Unable to find AJLabelPrint.exe!")
        End If

    End Sub

#End Region

    Private Sub radMatchJob_CheckedChanged(sender As Object, e As EventArgs) Handles radMatchJob.CheckedChanged
        If radMatchJob.Checked Then SetMatch()
    End Sub

    Private Sub radMatchPlant_CheckedChanged(sender As Object, e As EventArgs) Handles radMatchPlant.CheckedChanged
        If radMatchPlant.Checked Then SetMatch()
    End Sub

    Private Sub SetMatch()
        If radMatchCategory.Checked Then
            gbxDetails.Enabled = False
            cbxCategory.Show()
            cbxCategory.Focus()
            txtRef.Hide()
            btnSearch.Hide()
            txtName.Text = ""
            txtRef1.Text = ""
        Else
            gbxDetails.Enabled = True
            cbxCategory.Hide()
            txtRef.Show()
            btnSearch.Show()
            txtRef.Focus()
        End If
    End Sub

    Private Sub radMatchCategory_CheckedChanged(sender As Object, e As EventArgs) Handles radMatchCategory.CheckedChanged
        If radMatchCategory.Checked Then SetMatch()
    End Sub

    Private Function ContainsLetters(ByVal StringToCheck As String) As Boolean
        For Each _c As Char In StringToCheck.ToCharArray
            If Char.IsLetter(_c) Then Return True
        Next
        Return False
    End Function

    Private Sub SetJobDetails(ByRef Job As Business.Job)

        m_TargetID = Job._ID
        m_OwnerCode = Job._ContCode
        m_OwnerName = Job._ContName

        txtRef.Text = Job._JobNo.ToString
        txtName.Text = Job._RskFullname
        txtRef1.Text = Job._RskAddress1

        cbxSendIntray.Text = Job._ContName

        btnUpdate.Text = "Send to " + Job._ContName

        ToggleEdit(True)
        cdtDocDate.Focus()

    End Sub

    Private Sub SetPlantDetails(ByRef Plant As Business.Plant)

        m_TargetID = Plant._ID
        m_OwnerCode = "andrew"
        m_OwnerName = "Andrew Jones"
        cbxSendIntray.Text = "Andrew Jones"

        txtRef.Text = Plant._AjNo.ToString
        txtName.Text = Plant._Name
        txtRef1.Text = Plant._RegNo

        btnUpdate.Text = "Send to Andrew Jones"

        ToggleEdit(True)
        cdtDocDate.Focus()

    End Sub

    Private Sub txtRef_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtRef.Validating

        If txtRef.Text <> "" Then

            If radMatchJob.Checked Then

                If Not ContainsLetters(txtRef.Text) Then

                    Dim _j As Business.Job = Business.Job.RetreiveByJobNo(txtRef.Text)
                    If _j IsNot Nothing Then
                        SetJobDetails(_j)
                    Else
                        CareMessage("Job does Not exist - please check the number.", MessageBoxIcon.Exclamation, "Find Job")
                        Clear()
                    End If

                Else
                    CareMessage("Invalid Job Number - please check the number.", MessageBoxIcon.Exclamation, "Find Job")
                    Clear()
                End If

            End If

            If radMatchPlant.Checked Then

                Dim _p As Business.Plant = Nothing

                If ContainsLetters(txtRef.Text) Then
                    _p = Business.Plant.RetreiveByReg(txtRef.Text)
                Else
                    _p = Business.Plant.RetreiveByNo(txtRef.Text)
                End If

                If _p IsNot Nothing Then
                    SetPlantDetails(_p)
                Else
                    CareMessage("Invalid Plant - please check the registration Or number.", MessageBoxIcon.Exclamation, "Find Job")
                    Clear()
                End If

            End If

        Else
            Clear()
        End If

    End Sub

    Private Sub Clear()
        ClearDetails()
        ClearChecks()
        ClearDocumentDetail()
        ClearPricingNotes()
        ClearNotes()
        btnUpdate.Text = "Update"
        ToggleEdit(False)
    End Sub

    Private Sub ClearNotes()
        cgComments.Clear()
        txtComments.Text = ""
        btnNoteAdd.Enabled = False
        btnNoteSave.Enabled = False
        btnNoteDelete.Enabled = False
    End Sub

    Private Sub ClearPricingNotes()

        cbx1.SelectedIndex = -1
        cbx2.SelectedIndex = -1
        cbx3.SelectedIndex = -1
        cbx4.SelectedIndex = -1
        cbx5.SelectedIndex = -1
        cbx6.SelectedIndex = -1
        cbx7.SelectedIndex = -1
        cbx8.SelectedIndex = -1
        cbx9.SelectedIndex = -1
        cbx10.SelectedIndex = -1
        cbx11.SelectedIndex = -1
        cbx12.SelectedIndex = -1

        txt1.Text = ""
        txt2.Text = ""
        txt3.Text = ""
        txt4.Text = ""
        txt5.Text = ""
        txt6.Text = ""
        txt7.Text = ""
        txt8.Text = ""
        txt9.Text = ""
        txt10.Text = ""
        txt11.Text = ""
        txt12.Text = ""

    End Sub

    Private Sub ClearDetails()
        txtRef.Text = ""
        cbxCategory.Text = ""
        txtName.Text = ""
        txtRef1.Text = ""
    End Sub

    Private Sub ClearChecks()
        chkDN.Checked = False
        chkPreNeg.Checked = False
        chkPrice.Checked = False
        chkUsedJob.Checked = False
        chkFastTrack.Checked = False
    End Sub

    Private Sub ClearDocumentDetail()
        cdtDocDate.Text = ""
        cbxDocType.Text = ""
        cbxSupplier.Text = ""
        cbxWorkType.Text = ""
        cbxPaymentMethod.Text = ""
        txtDocRef.Text = ""
        txtNet.Text = ""
    End Sub

    Private Sub ToggleEdit(ByVal Enabled As Boolean)

        btnSplit.Enabled = Enabled
        btnNoteAdd.Enabled = Enabled
        btnNoteSave.Enabled = False

        If Enabled Then

            gbxDetails.Visible = True

            chkDN.BackColor = Session.ChangeColour
            chkPreNeg.BackColor = Session.ChangeColour
            chkPrice.BackColor = Session.ChangeColour
            chkUsedJob.BackColor = Session.ChangeColour
            chkFastTrack.BackColor = Session.ChangeColour

            cdtDocDate.BackColor = Session.ChangeColour
            cbxDocType.BackColor = Session.ChangeColour
            cbxSupplier.BackColor = Session.ChangeColour
            cbxWorkType.BackColor = Session.ChangeColour
            cbxPaymentMethod.BackColor = Session.ChangeColour
            txtDocRef.BackColor = Session.ChangeColour
            txtNet.BackColor = Session.ChangeColour

        Else

            chkDN.ResetBackColor()
            chkPreNeg.ResetBackColor()
            chkPrice.ResetBackColor()
            chkUsedJob.ResetBackColor()
            chkFastTrack.ResetBackColor()

            cdtDocDate.ResetBackColor()
            cbxDocType.ResetBackColor()
            cbxSupplier.ResetBackColor()
            cbxWorkType.ResetBackColor()
            cbxPaymentMethod.ResetBackColor()
            txtDocRef.ResetBackColor()
            txtNet.ResetBackColor()

            ToggleNote(False)

        End If

    End Sub

#Region "Buttons"

    Private Sub btnLeft_Click(sender As Object, e As EventArgs) Handles btnLeft.Click
        Dim b As System.Drawing.Bitmap = CType(pic.Image, Bitmap)
        b.RotateFlip(System.Drawing.RotateFlipType.Rotate90FlipXY)
        pic.Image = b
        SaveImageOnly()
    End Sub

    Private Sub btnRight_Click(sender As Object, e As EventArgs) Handles btnRight.Click
        Dim b As System.Drawing.Bitmap = CType(pic.Image, Bitmap)
        b.RotateFlip(System.Drawing.RotateFlipType.Rotate90FlipNone)
        pic.Image = b
        SaveImageOnly()
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If m_Scan IsNot Nothing Then
            If CareMessage("Are you sure you want to Delete this document?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Delete") = System.Windows.Forms.DialogResult.Yes Then
                Dim _SQL As String = "delete from Scans where ID = '" + m_Scan._ID.ToString + "'"
                DAL.ExecuteSQL(m_DocConnectionString, _SQL)
                PopulateScans()
            End If
        End If
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click

        If radMatchJob.Checked Then
            Dim _ID As String = Business.Job.FindAll
            If _ID <> "" Then
                Dim _J As Business.Job = Business.Job.RetreiveByID(New Guid(_ID))
                If _J IsNot Nothing Then
                    SetJobDetails(_J)
                End If
            End If
        End If

        If radMatchPlant.Checked Then
            Dim _ID As String = Business.Plant.Find
            If _ID <> "" Then
                Dim _P As Business.Plant = Business.Plant.RetreiveByID(New Guid(_ID))
                If _P IsNot Nothing Then
                    SetPlantDetails(_P)
                End If
            End If
        End If

        If radLink.Checked Then

            If txtRef.Text = "" Then
                CareMessage("Please ensure you enter the page number for the linked document.", MessageBoxIcon.Exclamation, "Page Number")
            Else

                If Not ContainsLetters(txtRef.Text) Then

                    Dim _PageNo As Integer = ValueHandler.ConvertInteger(txtRef.Text)

                    If _PageNo > 1 Then

                        Dim _SQL As String = ""
                        _SQL += "select doc_date as 'Doc Date', doc_ref as 'Ref', doc_type as 'Type', doc_supplier_name as 'Supplier',"
                        _SQL += " doc_desc as 'Description', doc_net as 'Value' from Scans"

                        Dim _Find As New GenericFind
                        With _Find
                            .Caption = "Find Scan"
                            .PopulateOnLoad = True
                            .ConnectionString = m_DocConnectionString
                            .GridSQL = _SQL
                            .GridWhereClause = " and doc_status <> 'Linked'"
                            .GridOrderBy = "order by last_stamp desc"
                            .ReturnField = "ID"
                            .FormWidth = 1000
                            .Show()
                        End With

                        If _Find.ReturnValue <> "" Then
                            If CareMessage("Are you sure you want to Link this Document?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Link") = DialogResult.Yes Then
                                LinkDocument(New Guid(_Find.ReturnValue), ValueHandler.ConvertByte(txtRef.Text))
                            End If
                        End If

                        _Find = Nothing

                    Else
                        CareMessage("Please enter a page number greater than 1.", MessageBoxIcon.Exclamation, "Page Number")
                    End If

                Else
                    CareMessage("Please enter a valid page number.", MessageBoxIcon.Exclamation, "Page Number")
                End If

                Clear()

            End If

        End If

    End Sub

    Private Sub btnPay_Click(sender As Object, e As EventArgs) Handles btnAuthorise.Click
        If Session.CurrentUser.UserCode.ToUpper = "AJ" Then
            If CareMessage("AJ, are you sure you want to Authorise this one for Payment?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Authorise") = DialogResult.Yes Then
                ChangeDocumentStatus(EnumStatus.Authorised, "julie", "Julie Jones")
            End If
        Else
            CareMessage("Only that complete bastard AJ can pass invoices...", MessageBoxIcon.Exclamation, "Pass")
        End If
    End Sub

    Private Sub btnQuery_Click(sender As Object, e As EventArgs) Handles btnQuery.Click
        ChangeDocumentStatus(EnumStatus.Queried)
    End Sub

    Private Sub btnDuplicate_Click(sender As Object, e As EventArgs) Handles btnDuplicate.Click
        ChangeDocumentStatus(EnumStatus.Duplicate)
    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        SaveDocument()
        PopulateScans()
    End Sub

    Private Sub ChangeDocumentStatus(ByVal Status As EnumStatus)
        ChangeDocumentStatus(Status, "", "")
    End Sub

    Private Sub LinkDocument(ByVal MasterDocumentID As Guid, ByVal Page As Byte)

        If m_Scan Is Nothing Then Exit Sub

        m_Scan._DocStatus = "Linked"
        m_Scan._PageId = MasterDocumentID
        m_Scan._PageNumber = Page

        m_Scan._LastUser = Session.CurrentUser.UserCode
        m_Scan._LastStamp = Now

        m_Scan.Store(m_DocConnectionString)

        PopulateScans()

    End Sub

    Private Sub ChangeDocumentStatus(ByVal Status As EnumStatus, ByVal InTrayUser As String, ByVal InTrayName As String)

        If m_Scan Is Nothing Then Exit Sub

        m_Scan._DocStatus = Status.ToString

        If InTrayUser <> "" AndAlso InTrayName <> "" Then
            m_Scan._DocIntray = InTrayUser
            m_Scan._DocIntrayName = InTrayName
        End If

        If Status = EnumStatus.Authorised Then
            m_Scan._AuthorisedStamp = Now
        End If

        m_Scan._LastUser = Session.CurrentUser.UserCode
        m_Scan._LastStamp = Now

        m_Scan.Store(m_DocConnectionString)

        PopulateScans()

    End Sub

#End Region

#Region "Radio Buttons"

    Private Sub radQScanned_CheckedChanged(sender As Object, e As EventArgs) Handles radQScanned.CheckedChanged
        If radQScanned.Checked Then PopulateScans()
    End Sub

    Private Sub radQCategorised_CheckedChanged(sender As Object, e As EventArgs) Handles radQMatched.CheckedChanged
        If radQMatched.Checked Then PopulateScans()
    End Sub

    Private Sub radQDuplicates_CheckedChanged(sender As Object, e As EventArgs) Handles radQDuplicates.CheckedChanged
        If radQDuplicates.Checked Then PopulateScans()
    End Sub

    Private Sub radQAwaitingAuth_CheckedChanged(sender As Object, e As EventArgs) Handles radQAwaitingAuth.CheckedChanged
        If radQAwaitingAuth.Checked Then PopulateScans()
    End Sub

    Private Sub radQueried_CheckedChanged(sender As Object, e As EventArgs) Handles radQueried.CheckedChanged
        If radQueried.Checked Then PopulateScans()
    End Sub

    Private Sub radQAuthorised_CheckedChanged(sender As Object, e As EventArgs) Handles radQAuthorised.CheckedChanged
        If radQAuthorised.Checked Then PopulateScans()
    End Sub

    Private Sub radMyIntray_CheckedChanged(sender As Object, e As EventArgs) Handles radMyIntray.CheckedChanged
        If radMyIntray.Checked Then PopulateScans()
    End Sub

    Private Sub radAll_CheckedChanged(sender As Object, e As EventArgs) Handles radCompanyTray.CheckedChanged
        If radCompanyTray.Checked Then PopulateScans()
    End Sub

    Private Sub btnSend_Click(sender As Object, e As EventArgs) Handles btnSend.Click

        If m_Scan IsNot Nothing AndAlso cbxSendIntray.Text <> "" Then

            btnSend.Enabled = False
            Session.CursorWaiting()

            m_Scan._DocIntray = cbxSendIntray.SelectedValue.ToString
            m_Scan._DocIntrayName = cbxSendIntray.Text

            m_Scan._LastUser = Session.CurrentUser.UserCode
            m_Scan._LastStamp = Now

            m_Scan.Store(m_DocConnectionString)

            PopulateScans()

            cbxSendIntray.Text = ""

            btnSend.Enabled = True
            Session.CursorDefault()

        End If

    End Sub

    Private Sub txtName_TextChanged(sender As Object, e As EventArgs) Handles txtName.TextChanged
        SetDetails()
    End Sub

    Private Sub btnPassAuth_Click(sender As Object, e As EventArgs) Handles btnPassAuth.Click

        If m_Scan IsNot Nothing Then

            If CareMessage("Are you sure you want to pass this document for Authorisation?", MessageBoxIcon.Question, MessageBoxButtons.YesNo, "Send to Authorise") = DialogResult.Yes Then

                btnPassAuth.Enabled = False
                Session.CursorWaiting()

                m_Scan._DocStatus = "Awaiting Authorisation"
                m_Scan._DocIntray = "AJ"
                m_Scan._DocIntrayName = "Andrew Jones"
                m_Scan.Store(m_DocConnectionString)

                PopulateScans()

                btnPassAuth.Enabled = True
                Session.CursorDefault()

            End If

        End If

    End Sub

    Private Sub btn100_Click(sender As Object, e As EventArgs) Handles btn100.Click
        pic.Properties.ZoomPercent = 100
    End Sub

    Private Sub pic_MouseWheel(sender As Object, e As MouseEventArgs) Handles pic.MouseWheel

        Dim _factor As Decimal = 0.05D
        pic.Properties.ZoomPercent += e.Delta * _factor
        DevExpress.Utils.DXMouseEventArgs.GetMouseArgs(e).Handled = True

        If pic.Properties.ZoomPercent < 40 Then
            pic.Properties.ZoomPercent = 40
        Else
            If pic.Properties.ZoomPercent > 120 Then
                pic.Properties.ZoomPercent = 120
            End If
        End If

    End Sub

    Private Sub btn80_Click(sender As Object, e As EventArgs) Handles btn80.Click
        pic.Properties.ZoomPercent = 80
    End Sub

    Private Sub btn40_Click(sender As Object, e As EventArgs) Handles btn40.Click
        pic.Properties.ZoomPercent = 40
    End Sub

    Private Sub btn60_Click(sender As Object, e As EventArgs) Handles btn60.Click
        pic.Properties.ZoomPercent = 60
    End Sub

    Private Sub btnGeneratePriceNote_Click(sender As Object, e As EventArgs) Handles btnGeneratePriceNote.Click

        Dim _Note As String = ""

        GenerateLine(_Note, 1, cbx1, txt1)
        GenerateLine(_Note, 2, cbx2, txt2)
        GenerateLine(_Note, 3, cbx3, txt3)
        GenerateLine(_Note, 4, cbx4, txt4)
        GenerateLine(_Note, 5, cbx5, txt5)
        GenerateLine(_Note, 6, cbx6, txt6)
        GenerateLine(_Note, 7, cbx7, txt7)
        GenerateLine(_Note, 8, cbx8, txt8)
        GenerateLine(_Note, 9, cbx9, txt9)
        GenerateLine(_Note, 10, cbx10, txt10)
        GenerateLine(_Note, 11, cbx11, txt11)
        GenerateLine(_Note, 12, cbx12, txt12)

        If AddComment(m_Scan._ID.Value, _Note) Then
            DisplayComments(m_Scan._ID)
        End If

    End Sub

    Private Function AddComment(ByVal DocID As Guid?, ByVal Comment As String) As Boolean

        If DocID IsNot Nothing AndAlso DocID.HasValue AndAlso Comment.Length > 0 Then

            Dim _c As New Business.ScanComment
            _c._ScanId = DocID.Value
            _c._Comments = Comment
            _c._UserId = Session.CurrentUser.ID
            _c._UserName = Session.CurrentUser.FullName
            _c._Stamp = Now
            _c.Store(m_DocConnectionString)

            Return True

        Else
            Return False
        End If

    End Function

    Private Sub GenerateLine(ByRef NoteText As String, ByVal LineNo As Integer, ByRef Combo As Care.Controls.CareComboBox, ByRef Txt As Care.Controls.CareTextBox)

        If Combo.SelectedIndex < 1 Then Exit Sub

        Select Case Combo.Text

            Case "OK"
                NoteText += "Line Number " + LineNo.ToString + " is OK." + vbCrLf

            Case "Price"
                If Txt.Text <> "" Then
                    NoteText += "Line Number " + LineNo.ToString + " should have a total price of £" + Txt.Text + vbCrLf
                End If

            Case "Rate"
                If Txt.Text <> "" Then
                    NoteText += "Line Number " + LineNo.ToString + " should priced using £" + Txt.Text + " for the rate." + vbCrLf
                End If

            Case Else
                NoteText += "Line Number " + LineNo.ToString + " needs to be discounted " + Combo.Text + vbCrLf

        End Select

    End Sub

    Private Sub btnPaid_Click(sender As Object, e As EventArgs) Handles btnPaid.Click

        Select Case Session.CurrentUser.UserCode.ToUpper

            Case "AJ"
                If CareMessage("AJ, are you sure you want to mark this as PAID?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Mark As Paid") = DialogResult.Yes Then
                    ChangeDocumentStatus(EnumStatus.Paid)
                End If

            Case "JULIE"
                ChangeDocumentStatus(EnumStatus.Paid)

            Case Else
                CareMessage("Only AJ or Julie can mark invoices as Paid...", MessageBoxIcon.Exclamation, "Mark As Paid")

        End Select

    End Sub

    Private Sub radAllTrays_CheckedChanged(sender As Object, e As EventArgs) Handles radAllTrays.CheckedChanged
        If radAllTrays.Checked Then PopulateScans()
    End Sub

    Private Sub radPaid_CheckedChanged(sender As Object, e As EventArgs) Handles radPaid.CheckedChanged
        If radPaid.Checked Then PopulateScans()
    End Sub

    Private Sub radAnyStatus_CheckedChanged(sender As Object, e As EventArgs) Handles radAnyStatus.CheckedChanged
        If radAnyStatus.Checked Then PopulateScans()
    End Sub

    Private Sub cgDetail_FocusedRowChanged(sender As Object, e As FocusedRowChangedEventArgs) Handles cgDetail.FocusedRowChanged
        HandleFocusedRowChanged(cgDetail, e)
    End Sub

    Private Sub cgDetail_GridDoubleClick(sender As Object, e As EventArgs) Handles cgDetail.GridDoubleClick

        If cgDetail.RecordCount = 0 Then Exit Sub

        Dim _ID As String = cgDetail.GetRowCellValue(cgDetail.RowIndex, "ID").ToString
        If _ID <> "" Then

            ctView.SelectedTabPage = tabViewImage
            Application.DoEvents()

            Dim _i As Integer = 0
            For Each _dr As DataRow In CType(cgDocuments.UnderlyingGridControl.DataSource, DataTable).Rows
                If _dr.Item("ID").ToString = _ID Then
                    cgDocuments.FocusedRowHandle = cgDocuments.UnderlyingGridView.GetRowHandle(_i)
                End If
            Next

        End If

    End Sub

    Private Sub ctView_SelectedPageChanged(sender As Object, e As TabPageChangedEventArgs) Handles ctView.SelectedPageChanged
        If e.PrevPage IsNot Nothing Then
            If e.PrevPage.Name = "tabViewImage" Then
                If cgDetail.RecordCount = 0 Then
                    PopulateScans()
                End If
            End If
        End If

    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        Business.Scan.Print(m_Scan._ID.Value, btnPrint.ShiftDown)
    End Sub

    Private Sub btnReturnToController_Click(sender As Object, e As EventArgs) Handles btnReturnToController.Click

        If m_Scan IsNot Nothing Then

            If CareMessage("Are you sure you want to send this document to " + m_Scan._MatchedOwnerName + "?", MessageBoxIcon.Question, MessageBoxButtons.YesNo, "Send to Owner") = DialogResult.Yes Then

                btnReturnToController.Enabled = False
                Session.CursorWaiting()

                m_Scan._DocIntray = m_Scan._MatchedOwner
                m_Scan._DocIntrayName = m_Scan._MatchedOwnerName

                m_Scan._LastUser = Session.CurrentUser.UserCode
                m_Scan._LastStamp = Now

                m_Scan.Store(m_DocConnectionString)

                PopulateScans()

                btnReturnToController.Enabled = True
                Session.CursorDefault()

            End If

        End If

    End Sub

    Private Sub btnKeith_Click(sender As Object, e As EventArgs) Handles btnKeith.Click

        If m_Scan IsNot Nothing Then

            btnKeith.Enabled = False
            Session.CursorWaiting()

            m_Scan._DocStatus = "Queried"
            m_Scan._DocIntray = "KH"
            m_Scan._DocIntrayName = "Keith Hughes"
            m_Scan.Store(m_DocConnectionString)

            PopulateScans()

            btnKeith.Enabled = True
            Session.CursorDefault()

        End If

    End Sub

    Private Sub radSpecificTray_CheckedChanged(sender As Object, e As EventArgs) Handles radSpecificTray.CheckedChanged
        If radSpecificTray.Checked AndAlso cbxSelectIntray.Text <> "" Then
            PopulateScans()
        End If
    End Sub

    Private Sub cbxSelectIntray_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSelectIntray.SelectedIndexChanged
        If radSpecificTray.Checked AndAlso cbxSelectIntray.Text <> "" Then
            PopulateScans()
        End If
    End Sub

    Private Sub btn25_Click(sender As Object, e As EventArgs) Handles btn25.Click
        pic.Properties.ZoomPercent = 25
    End Sub

    Private Sub btnNoteDelete_Click(sender As Object, e As EventArgs) Handles btnNoteDelete.Click
        CareMessage("You do have permission to delete a note.", MessageBoxIcon.Exclamation, "Delete Note")
    End Sub

    Private Sub btnNoteAdd_Click(sender As Object, e As EventArgs) Handles btnNoteAdd.Click
        ToggleNote(True)
    End Sub


    Private Sub ToggleNote(ByVal Enabled As Boolean)

        btnNoteSave.Enabled = Enabled
        btnNoteAdd.Enabled = Not Enabled
        btnNoteDelete.Enabled = Not Enabled
        txtComments.ReadOnly = Not Enabled

        If Enabled Then
            txtComments.Text = ""
            txtComments.BackColor = Session.ChangeColour
            txtComments.Focus()
        Else
            txtComments.ResetBackColor()
        End If

    End Sub

    Private Sub btnNoteSave_Click(sender As Object, e As EventArgs) Handles btnNoteSave.Click
        If AddComment(m_Scan._ID, txtComments.Text) Then
            ToggleNote(False)
            DisplayComments(m_Scan._ID)
        End If
    End Sub

    Private Sub cgComments_FocusedRowChanged(sender As Object, e As FocusedRowChangedEventArgs) Handles cgComments.FocusedRowChanged

        If e Is Nothing Then Exit Sub
        If e.FocusedRowHandle < 0 Then Exit Sub

        txtComments.Text = cgComments.GetRowCellValue(e.FocusedRowHandle, "comments").ToString

    End Sub

    Private Sub chkTop20_CheckedChanged(sender As Object, e As EventArgs) Handles chkTop20.CheckedChanged
        PopulateScans(False)
    End Sub

    Private Sub radLink_CheckedChanged(sender As Object, e As EventArgs) Handles radLink.CheckedChanged
        If radLink.Checked Then SetMatch()
    End Sub

    Private Sub btnSplit_Click(sender As Object, e As EventArgs) Handles btnSplit.Click

        If Not ValidateEntry() Then Exit Sub
        If m_Scan Is Nothing Then Exit Sub

        btnSplit.Enabled = False
        Me.Cursor = Cursors.WaitCursor
        Application.DoEvents()

        SaveDocument()

        Dim _frmScanSplit As New frmScanSplit(m_DocConnectionString, m_Scan._ID.Value)
        _frmScanSplit.ShowDialog()

        btnSplit.Enabled = True

    End Sub

#End Region

End Class

