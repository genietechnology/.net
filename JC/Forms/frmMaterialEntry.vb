﻿Imports Care.Global
Imports Care.Data
Imports Care.Shared

Public Class frmMaterialEntry

    Private Sub frmMaterialEntry_Load(sender As Object, e As EventArgs) Handles Me.Load
        MyControls.SetControls(ControlHandler.Mode.Add, Me.Controls)
        cdtDate.Value = Today
        DisplayMaterials()
    End Sub

    Private Sub frmMaterialEntry_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        txtNo.Focus()
    End Sub

    Private Sub btnFindJob_Click(sender As Object, e As EventArgs) Handles btnFindJob.Click
        Dim _ReturnValue As String = Business.Job.FindCurrentJobs
        If _ReturnValue <> "" Then
            Dim _Job As Business.Job = Business.Job.RetreiveByID(New Guid(_ReturnValue))
            DisplayJob(_Job)
        Else
            ClearJob
        End If
    End Sub

    Private Sub btnFindStock_Click(sender As Object, e As EventArgs) Handles btnFindStock.Click

        Dim _ReturnValue As String = ""
        Dim _Find As New GenericFind
        With _Find
            .Caption = "Find Stock"
            .PopulateOnLoad = True
            .ConnectionString = Session.ConnectionString
            .GridSQL = "select quickfind as 'Quick Find', description as 'Description', sell_unit as 'Selling Unit', sell_price as 'Price' from Stock"
            .GridOrderBy = "order by description"
            .ReturnField = "ID"
            .FormWidth = 900
            .Show()
            _ReturnValue = .ReturnValue
        End With

        If _ReturnValue <> "" Then
            Dim _Stock As Business.Stock = Business.Stock.RetreiveByID(New Guid(_ReturnValue))
            DisplayStock(_Stock)
        Else
            ClearStock
        End If

    End Sub

    Private Sub DisplayJob(ByVal JobRecord As Business.Job)
        If JobRecord IsNot Nothing Then
            txtNo.Tag = JobRecord._ID
            txtNo.Text = JobRecord._JobNo.ToString
            txtJobDesc.Text = JobRecord._RskFullname + ", " + JobRecord._RskAddressLine
        End If
    End Sub

    Private Sub DisplayStock(ByVal StockRecord As Business.Stock)
        txtQuickfind.Tag = StockRecord._ID
        txtQuickfind.Text = StockRecord._Quickfind
        txtStockDesc.Text = StockRecord._Description
        txtUOM.Text = StockRecord._SellUnit
        txtSellPrice.Text = ValueHandler.MoneyAsText(StockRecord._SellPrice)
        txtFactor.Text = "1.00"
        txtQty.Text = "1.00"
        txtPrice.Text = ValueHandler.MoneyAsText(StockRecord._SellPrice)
        txtTotalPrice.Text = ""
        CalculateCosts()
    End Sub

    Private Sub ClearJob()
        txtNo.Tag = Nothing
        txtNo.Text = ""
        txtJobDesc.Text = ""
    End Sub

    Private Sub ClearStock()
        txtQuickfind.Tag = Nothing
        txtQuickfind.Text = ""
        txtStockDesc.Text = ""
        txtUOM.Text = ""
        txtFactor.Text = ""
        txtQty.Text = ""
        txtPrice.Text = ""
        txtTotalPrice.Text = ""
    End Sub

    Private Sub txtNo_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtNo.Validating
        If txtNo.Text = "" Then
            ClearJob()
        Else
            Dim _J As Business.Job = Business.Job.RetreiveByJobNo(txtNo.Text)
            If _J IsNot Nothing Then
                DisplayJob(_J)
            End If
        End If
    End Sub

    Private Sub txtQuickfind_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtQuickfind.Validating
        If txtQuickfind.Text = "" Then
            ClearStock()
        Else
            Dim _S As Business.Stock = Business.Stock.RetreiveByQuickFind(txtQuickfind.Text)
            If _S IsNot Nothing Then
                DisplayStock(_S)
            End If
        End If
    End Sub

    Private Sub CalculateCosts()

        Dim _Qty As Decimal = ValueHandler.ConvertDecimal(txtQty.Text)
        Dim _Factor As Decimal = ValueHandler.ConvertDecimal(txtFactor.Text)
        Dim _SellPrice As Decimal = ValueHandler.ConvertDecimal(txtSellPrice.Text)
        Dim _Price As Decimal = ValueHandler.ConvertDecimal(txtPrice.Text)
        Dim _Total As Decimal = 0

        'are we adjusting the price using a factor?
        'ie half bag etc
        If _Factor <> 1 Then
            _Price = _SellPrice * _Factor
        End If

        If _Qty > 0 AndAlso _Price > 0 Then
            _Total = _Price * _Qty
        End If

        txtTotalPrice.Text = ValueHandler.MoneyAsText(_Total)

    End Sub

    Private Sub txtFactor_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtFactor.Validating
        CalculateCosts
    End Sub

    Private Sub txtQty_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtQty.Validating
        CalculateCosts
    End Sub

    Private Sub txtPrice_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtPrice.Validating
        CalculateCosts
    End Sub

    Private Function ValidateEntry() As Boolean
        Return True
    End Function

    Private Sub Save()

        Dim _M As New Business.Material
        With _M
            ._BookedDate = cdtDate.Value
            ._JobId = New Guid(txtNo.Tag.ToString)
            ._StockId = New Guid(txtQuickfind.Tag.ToString)
            ._Qty = ValueHandler.ConvertDecimal(txtQty.Text)
            ._Factor = ValueHandler.ConvertDecimal(txtFactor.Text)
            ._UnitPrice = ValueHandler.ConvertDecimal(txtPrice.Text)
            ._TotalPrice = ValueHandler.ConvertDecimal(txtTotalPrice.Text)
            .Store()
        End With

    End Sub

    Private Sub DisplayMaterials()

        If cdtDate.Value.HasValue Then

            Dim _SQL As String = ""
            _SQL += "select m.ID, m.qty as 'Qty', s.sell_unit as 'UOM', s.description as 'Description',"
            _SQL += " j.job_no as 'Job No', j.rsk_fullname as 'Name', j.rsk_address_1 as 'Address',"
            _SQL += " m.unit_price as 'Unit Price', m.total_price as 'TOTAL'"
            _SQL += " from Materials m"
            _SQL += " left join Stock s on s.ID = m.stock_id"
            _SQL += " left join Jobs j on j.ID = m.job_id"
            _SQL += " where m.booked_date = " + ValueHandler.SQLDate(cdtDate.Value.Value, True)

            cgMaterials.HideFirstColumn = True
            cgMaterials.Populate(Session.ConnectionString, _SQL)
            cgMaterials.AutoSizeColumns()

        Else
            cgMaterials.Clear()
        End If

    End Sub

    Private Sub btnSameJob_Click(sender As Object, e As EventArgs) Handles btnSameJob.Click
        If ValidateEntry() Then
            Save()
            DisplayMaterials()
            ClearStock()
            txtQuickfind.Focus()
        End If
    End Sub

    Private Sub btnAddNewJob_Click(sender As Object, e As EventArgs) Handles btnAddNewJob.Click
        If ValidateEntry Then
            Save
            DisplayMaterials()
            ClearStock()
            ClearJob()
            txtNo.Focus()
        End If
    End Sub

    Private Sub cdtDate_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles cdtDate.Validating
        DisplayMaterials()
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click

        If cgMaterials Is Nothing Then Exit Sub
        If cgMaterials.RecordCount < 1 Then Exit Sub
        If cgMaterials.CurrentRow("ID").ToString = "" Then Exit Sub

        If CareMessage("Are you sure you want to delete this item?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Delete Item") = DialogResult.Yes Then
            Dim _ID As String = cgMaterials.CurrentRow("ID").ToString
            DAL.ExecuteSQL(Session.ConnectionString, "delete from Materials where ID = '" + _ID + "'")
            DisplayMaterials()
        End If

    End Sub
End Class
