﻿Imports Care.Global
Imports DevExpress.XtraMap
Imports Care.Data

Public Class frmMapping


    Private Const MyBingKey As String = "AmeXh0oiUBaToLYQjpW_AMV7vNt7DRhHM9fqMmjWQNUGajBe0SMSTo4CGx-QmMK4"

    Private m_PrimarySite As String = "122 Bronwydd Road, Carmarthen, SA31 2AR"
    Private m_PrimarySitePoint As GeoPoint = Nothing
    Private m_Storage As New MapItemStorage
    Private m_Addresses As New List(Of AddressRecord)

    Private Sub frmMapping_Load(sender As Object, e As EventArgs) Handles Me.Load

        Session.CursorWaiting()

        Dim _VectorLayer As New VectorItemsLayer
        _VectorLayer.Data = m_Storage

        MyMap.Layers.Add(_VectorLayer)

        'center over the whole UK
        MyMap.CenterPoint = New GeoPoint(53.9239435773579, -3.63607609375002)
        MyMap.ZoomLevel = 6

    End Sub

    Private Sub frmMapping_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        GenerateGeoPoints()
        MapCoordinatesFromDB()

        If m_PrimarySitePoint IsNot Nothing Then
            MyMap.CenterPoint = m_PrimarySitePoint
            MyMap.ZoomLevel = 14
        End If

        Session.CursorDefault()

    End Sub

    Private Sub GenerateGeoPoints()

        Session.CursorWaiting()

        m_Addresses.Clear()

        Dim _Jobs As List(Of Business.Job) = Business.Job.RetreiveCurrent
        For Each _J In _Jobs
            If _J._RskCoordinates = "" Then
                m_Addresses.Add(New AddressRecord("Job", _J._ID.ToString, _J._RskAddress, _J._RskFullname))
            End If
        Next

        timRefresh.Start()

    End Sub

    Private Sub MapCoordinatesFromDB()

        'm_Addresses.Clear()

        'Dim _Jobs As List(Of Business.Job) = Business.Job.RetreiveCurrent
        'For Each _Job In _Jobs

        '    If _Job._RskCoordinates <> "" Then

        '        m_Addresses.Add(New AddressRecord("Job", _Job._ID.ToString, _Job._RskAddress, _Job._RskCoordinates))

        '        'If m_PrimarySite = _Site._Name Then
        '        '    Dim _Pipe As Integer = _Site._Coordinates.IndexOf("|")
        '        '    Dim _Lat As Double = _Site._Coordinates.Substring(0, _Pipe)
        '        '    Dim _Long As Double = _Site._Coordinates.Substring(_Pipe + 1)
        '        '    m_PrimarySitePoint = New GeoPoint(_Lat, _Long)
        '        'End If

        '    End If

        'Next

        'For Each _Add In m_Addresses
        '    AddPin(_Add)
        'Next

    End Sub

    Private Sub AddPin(ByRef AddressRecordIn As AddressRecord)

        If AddressRecordIn.Latitude = 0 Then Exit Sub
        If AddressRecordIn.Longitude = 0 Then Exit Sub

        Dim _Pin As New MapPushpin

        _Pin.Image = My.Resources.Green
        _Pin.Location = New GeoPoint(AddressRecordIn.Latitude, AddressRecordIn.Longitude)

        Dim _Att As New MapItemAttribute
        _Att.Name = "AddressRecord"
        _Att.Value = AddressRecordIn
        _Pin.Attributes.Add(_Att)

        _Pin.ToolTipPattern = AddressRecordIn.ToolTip

        m_Storage.Items.Add(_Pin)

    End Sub

    Private Class AddressRecord

        Private WithEvents _SearchProvider As New BingSearchDataProvider

        Property TableName As String
        Property RecordID As String
        Property Address As String
        Property ToolTip As String
        Property Latitude As Double
        Property Longitude As Double

        Property Searching As Boolean

        Public Sub New(ByVal TableNameIn As String, ByVal RecordIDIn As String, ByVal AddressIn As String, ByVal TooltipIn As String)
            Me.TableName = TableNameIn
            Me.RecordID = RecordIDIn
            Me.Address = FormatAddress(AddressIn)
            Me.ToolTip = TooltipIn
            Me.Longitude = 0
            Me.Latitude = 0
            Me.Searching = False
            GetGEOCode()
        End Sub

        Public Sub New(ByVal TableNameIn As String, ByVal RecordIDIn As String, ByVal AddressIn As String, ByVal TooltipIn As String, ByVal Coordinates As String)

            Me.TableName = TableNameIn
            Me.RecordID = RecordIDIn
            Me.Address = FormatAddress(AddressIn)
            Me.ToolTip = TooltipIn

            If Coordinates <> "" Then

                Dim _Pipe As Integer = Coordinates.IndexOf("|")
                Dim _Lat As String = Coordinates.Substring(0, _Pipe)
                Dim _Long As String = Coordinates.Substring(_Pipe + 1)

                Me.Latitude = CDbl(_Lat)
                Me.Longitude = CDbl(_Long)

            End If

            Me.Searching = False

        End Sub

        Private Function FormatAddress(ByVal AddressIn As String) As String
            Dim _Return As String = AddressIn
            _Return = _Return.Replace(Chr(10), ",")
            _Return = _Return.Replace(Chr(13), ",")
            _Return = _Return.Replace(" ,", ",")
            _Return = _Return.Replace(",,", ",")
            _Return = _Return.Replace(",,,", ",")
            Return _Return
        End Function

        Private Sub GetGEOCode()

            _SearchProvider.BingKey = MyBingKey
            _SearchProvider.SearchOptions.DistanceUnit = DistanceMeasureUnit.Mile
            _SearchProvider.SearchOptions.SearchRadius = 50
            _SearchProvider.SearchOptions.ResultsCount = 5
            _SearchProvider.Search(Me.Address)

            Me.Searching = True

        End Sub

        Private Sub MySearchProvider_SearchCompleted(sender As Object, e As BingSearchCompletedEventArgs) Handles _SearchProvider.SearchCompleted
            Me.Searching = False
            If e.RequestResult IsNot Nothing Then
                If e.RequestResult.ResultCode = RequestResultCode.Success Then
                    If e.RequestResult.SearchRegion IsNot Nothing Then
                        Me.Latitude = e.RequestResult.SearchRegion.Location.Latitude
                        Me.Longitude = e.RequestResult.SearchRegion.Location.Longitude
                    End If
                End If
            End If
        End Sub

    End Class

    Private Sub timRefresh_Tick(sender As Object, e As EventArgs) Handles timRefresh.Tick

        Dim _Searching As Integer = 0
        For Each _A In m_Addresses
            If _A.Searching Then
                _Searching += 1
            Else
                AddPin(_A)
            End If
        Next

        If _Searching = 0 Then

            timRefresh.Stop()

            UpdateDB()

            Session.CursorDefault()

        Else
            timRefresh.Start()
        End If

    End Sub

    Private Sub UpdateDB()

        For Each _A In m_Addresses

            If _A.Latitude <> 0 AndAlso _A.Longitude <> 0 Then
                Dim _Coordinates As String = _A.Latitude.ToString + "|" + _A.Longitude.ToString
                UpdateTable("Jobs", _A.RecordID, _Coordinates)
            End If

        Next

    End Sub

    Private Sub UpdateTable(ByVal TableName As String, ByVal RecordID As String, ByVal Coordinates As String)
        Dim _SQL As String = "update " + TableName + " set rsk_coordinates = '" + Coordinates + "' where ID = '" + RecordID + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)
    End Sub

End Class