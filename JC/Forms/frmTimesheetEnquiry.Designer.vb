﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTimesheetEnquiry
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.btnRefresh = New Care.Controls.CareButton(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.txtVan = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.cdtTimesheetDate = New Care.Controls.CareDateTime(Me.components)
        Me.cbxEmployee = New Care.Controls.CareComboBox(Me.components)
        Me.cgTS = New Care.Controls.CareGrid()
        Me.txtJobNo = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.txtJobDesc = New Care.Controls.CareTextBox(Me.components)
        Me.btnFind = New Care.Controls.CareButton(Me.components)
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.txtVan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtTimesheetDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtTimesheetDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxEmployee.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJobNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJobDesc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl5
        '
        Me.GroupControl5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl5.Controls.Add(Me.btnFind)
        Me.GroupControl5.Controls.Add(Me.txtJobDesc)
        Me.GroupControl5.Controls.Add(Me.txtJobNo)
        Me.GroupControl5.Controls.Add(Me.CareLabel5)
        Me.GroupControl5.Controls.Add(Me.btnRefresh)
        Me.GroupControl5.Controls.Add(Me.CareLabel4)
        Me.GroupControl5.Controls.Add(Me.txtVan)
        Me.GroupControl5.Controls.Add(Me.CareLabel3)
        Me.GroupControl5.Controls.Add(Me.CareLabel1)
        Me.GroupControl5.Controls.Add(Me.cdtTimesheetDate)
        Me.GroupControl5.Controls.Add(Me.cbxEmployee)
        Me.GroupControl5.Location = New System.Drawing.Point(12, 9)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.ShowCaption = False
        Me.GroupControl5.Size = New System.Drawing.Size(830, 67)
        Me.GroupControl5.TabIndex = 0
        Me.GroupControl5.Text = "GroupControl5"
        '
        'btnRefresh
        '
        Me.btnRefresh.Location = New System.Drawing.Point(684, 35)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(60, 25)
        Me.btnRefresh.TabIndex = 12
        Me.btnRefresh.Text = "Refresh"
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(224, 12)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(52, 15)
        Me.CareLabel4.TabIndex = 8
        Me.CareLabel4.Text = "Employee"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtVan
        '
        Me.txtVan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtVan.EnterMoveNextControl = True
        Me.txtVan.Location = New System.Drawing.Point(111, 37)
        Me.txtVan.MaxLength = 0
        Me.txtVan.Name = "txtVan"
        Me.txtVan.NumericAllowNegatives = False
        Me.txtVan.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtVan.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtVan.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtVan.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtVan.Properties.Appearance.Options.UseFont = True
        Me.txtVan.Properties.Appearance.Options.UseTextOptions = True
        Me.txtVan.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtVan.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtVan.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtVan.Size = New System.Drawing.Size(91, 22)
        Me.txtVan.TabIndex = 5
        Me.txtVan.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtVan.ToolTipText = ""
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(76, 40)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(20, 15)
        Me.CareLabel3.TabIndex = 4
        Me.CareLabel3.Text = "Van"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(12, 12)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(84, 15)
        Me.CareLabel1.TabIndex = 2
        Me.CareLabel1.Text = "Timesheet After"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtTimesheetDate
        '
        Me.cdtTimesheetDate.EditValue = Nothing
        Me.cdtTimesheetDate.EnterMoveNextControl = True
        Me.cdtTimesheetDate.Location = New System.Drawing.Point(111, 9)
        Me.cdtTimesheetDate.Name = "cdtTimesheetDate"
        Me.cdtTimesheetDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtTimesheetDate.Properties.Appearance.Options.UseFont = True
        Me.cdtTimesheetDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtTimesheetDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtTimesheetDate.Size = New System.Drawing.Size(91, 22)
        Me.cdtTimesheetDate.TabIndex = 3
        Me.cdtTimesheetDate.Tag = "AE"
        Me.cdtTimesheetDate.Value = Nothing
        '
        'cbxEmployee
        '
        Me.cbxEmployee.AllowBlank = False
        Me.cbxEmployee.DataSource = Nothing
        Me.cbxEmployee.DisplayMember = Nothing
        Me.cbxEmployee.EnterMoveNextControl = True
        Me.cbxEmployee.Location = New System.Drawing.Point(282, 9)
        Me.cbxEmployee.Name = "cbxEmployee"
        Me.cbxEmployee.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxEmployee.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxEmployee.Properties.Appearance.Options.UseFont = True
        Me.cbxEmployee.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxEmployee.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxEmployee.SelectedValue = Nothing
        Me.cbxEmployee.Size = New System.Drawing.Size(330, 22)
        Me.cbxEmployee.TabIndex = 9
        Me.cbxEmployee.Tag = "AE"
        Me.cbxEmployee.ValueMember = Nothing
        '
        'cgTS
        '
        Me.cgTS.AllowBuildColumns = True
        Me.cgTS.AllowEdit = False
        Me.cgTS.AllowHorizontalScroll = False
        Me.cgTS.AllowMultiSelect = False
        Me.cgTS.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgTS.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgTS.Appearance.Options.UseFont = True
        Me.cgTS.AutoSizeByData = True
        Me.cgTS.DisableAutoSize = False
        Me.cgTS.DisableDataFormatting = False
        Me.cgTS.FocusedRowHandle = -2147483648
        Me.cgTS.HideFirstColumn = False
        Me.cgTS.Location = New System.Drawing.Point(12, 82)
        Me.cgTS.Name = "cgTS"
        Me.cgTS.PreviewColumn = ""
        Me.cgTS.QueryID = Nothing
        Me.cgTS.RowAutoHeight = False
        Me.cgTS.SearchAsYouType = True
        Me.cgTS.ShowAutoFilterRow = False
        Me.cgTS.ShowFindPanel = True
        Me.cgTS.ShowGroupByBox = False
        Me.cgTS.ShowLoadingPanel = False
        Me.cgTS.ShowNavigator = True
        Me.cgTS.Size = New System.Drawing.Size(830, 350)
        Me.cgTS.TabIndex = 1
        '
        'txtJobNo
        '
        Me.txtJobNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtJobNo.EnterMoveNextControl = True
        Me.txtJobNo.Location = New System.Drawing.Point(282, 37)
        Me.txtJobNo.MaxLength = 0
        Me.txtJobNo.Name = "txtJobNo"
        Me.txtJobNo.NumericAllowNegatives = False
        Me.txtJobNo.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtJobNo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtJobNo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtJobNo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtJobNo.Properties.Appearance.Options.UseFont = True
        Me.txtJobNo.Properties.Appearance.Options.UseTextOptions = True
        Me.txtJobNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtJobNo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtJobNo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtJobNo.Size = New System.Drawing.Size(76, 22)
        Me.txtJobNo.TabIndex = 7
        Me.txtJobNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtJobNo.ToolTipText = ""
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(258, 40)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(18, 15)
        Me.CareLabel5.TabIndex = 6
        Me.CareLabel5.Text = "Job"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJobDesc
        '
        Me.txtJobDesc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtJobDesc.EnterMoveNextControl = True
        Me.txtJobDesc.Location = New System.Drawing.Point(364, 37)
        Me.txtJobDesc.MaxLength = 0
        Me.txtJobDesc.Name = "txtJobDesc"
        Me.txtJobDesc.NumericAllowNegatives = False
        Me.txtJobDesc.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtJobDesc.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtJobDesc.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtJobDesc.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtJobDesc.Properties.Appearance.Options.UseFont = True
        Me.txtJobDesc.Properties.Appearance.Options.UseTextOptions = True
        Me.txtJobDesc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtJobDesc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtJobDesc.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtJobDesc.Properties.ReadOnly = True
        Me.txtJobDesc.Size = New System.Drawing.Size(248, 22)
        Me.txtJobDesc.TabIndex = 10
        Me.txtJobDesc.TabStop = False
        Me.txtJobDesc.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtJobDesc.ToolTipText = ""
        '
        'btnFind
        '
        Me.btnFind.Location = New System.Drawing.Point(618, 35)
        Me.btnFind.Name = "btnFind"
        Me.btnFind.Size = New System.Drawing.Size(60, 25)
        Me.btnFind.TabIndex = 11
        Me.btnFind.Text = "Find Job"
        '
        'frmTimesheetEnquiry
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(854, 444)
        Me.Controls.Add(Me.cgTS)
        Me.Controls.Add(Me.GroupControl5)
        Me.LoadMaximised = True
        Me.Name = "frmTimesheetEnquiry"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.txtVan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtTimesheetDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtTimesheetDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxEmployee.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJobNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJobDesc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cgTS As Care.Controls.CareGrid
    Friend WithEvents cbxEmployee As Care.Controls.CareComboBox
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents cdtTimesheetDate As Care.Controls.CareDateTime
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents txtVan As Care.Controls.CareTextBox
    Friend WithEvents btnRefresh As Care.Controls.CareButton
    Friend WithEvents txtJobNo As Care.Controls.CareTextBox
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents btnFind As Care.Controls.CareButton
    Friend WithEvents txtJobDesc As Care.Controls.CareTextBox
End Class
