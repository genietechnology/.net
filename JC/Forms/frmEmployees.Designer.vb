﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmEmployees
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.txtTel = New Care.[Shared].CareTelephoneNumber()
        Me.txtEmail = New Care.[Shared].CareEmailAddress()
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.txtNickname = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel10 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.txtFullname = New Care.Controls.CareTextBox(Me.components)
        Me.txtCode = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.txtRate = New Care.Controls.CareTextBox(Me.components)
        Me.chkActive = New Care.Controls.CareCheckBox(Me.components)
        Me.chkOffice = New Care.Controls.CareCheckBox(Me.components)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtNickname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFullname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkActive.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkOffice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(377, 3)
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(286, 3)
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(0, 246)
        Me.Panel1.Size = New System.Drawing.Size(474, 36)
        Me.Panel1.TabIndex = 1
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.chkOffice)
        Me.GroupControl1.Controls.Add(Me.chkActive)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Controls.Add(Me.txtRate)
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Controls.Add(Me.txtTel)
        Me.GroupControl1.Controls.Add(Me.txtEmail)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.CareLabel5)
        Me.GroupControl1.Controls.Add(Me.txtNickname)
        Me.GroupControl1.Controls.Add(Me.CareLabel10)
        Me.GroupControl1.Controls.Add(Me.CareLabel14)
        Me.GroupControl1.Controls.Add(Me.txtFullname)
        Me.GroupControl1.Controls.Add(Me.txtCode)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 53)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(450, 183)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "General Information"
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(9, 153)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(56, 15)
        Me.CareLabel3.TabIndex = 10
        Me.CareLabel3.Text = "Telephone"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTel
        '
        Me.txtTel.AccessibleName = "Telephone"
        Me.txtTel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTel.Appearance.Options.UseFont = True
        Me.txtTel.IsMobile = False
        Me.txtTel.Location = New System.Drawing.Point(85, 149)
        Me.txtTel.MaxLength = 20
        Me.txtTel.MinimumSize = New System.Drawing.Size(0, 22)
        Me.txtTel.Name = "txtTel"
        Me.txtTel.ReadOnly = False
        Me.txtTel.Size = New System.Drawing.Size(348, 22)
        Me.txtTel.TabIndex = 11
        Me.txtTel.Tag = "AE"
        '
        'txtEmail
        '
        Me.txtEmail.AccessibleName = "Email Address"
        Me.txtEmail.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail.Appearance.Options.UseFont = True
        Me.txtEmail.Location = New System.Drawing.Point(85, 121)
        Me.txtEmail.MaxLength = 100
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.NoButton = False
        Me.txtEmail.NoColours = False
        Me.txtEmail.NoValidate = False
        Me.txtEmail.ReadOnly = False
        Me.txtEmail.Size = New System.Drawing.Size(348, 22)
        Me.txtEmail.TabIndex = 9
        Me.txtEmail.Tag = "AE"
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(9, 124)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(29, 15)
        Me.CareLabel2.TabIndex = 8
        Me.CareLabel2.Text = "Email"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.CareLabel5.Location = New System.Drawing.Point(9, 68)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(54, 15)
        Me.CareLabel5.TabIndex = 4
        Me.CareLabel5.Text = "Nickname"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNickname
        '
        Me.txtNickname.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtNickname.EditValue = ""
        Me.txtNickname.EnterMoveNextControl = True
        Me.txtNickname.Location = New System.Drawing.Point(85, 65)
        Me.txtNickname.MaxLength = 30
        Me.txtNickname.Name = "txtNickname"
        Me.txtNickname.NumericAllowNegatives = False
        Me.txtNickname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtNickname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtNickname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtNickname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtNickname.Properties.Appearance.Options.UseFont = True
        Me.txtNickname.Properties.Appearance.Options.UseTextOptions = True
        Me.txtNickname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtNickname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtNickname.Properties.MaxLength = 30
        Me.txtNickname.Size = New System.Drawing.Size(348, 22)
        Me.txtNickname.TabIndex = 5
        Me.txtNickname.Tag = "AE"
        Me.txtNickname.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNickname.ToolTipText = ""
        '
        'CareLabel10
        '
        Me.CareLabel10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel10.Location = New System.Drawing.Point(9, 40)
        Me.CareLabel10.Name = "CareLabel10"
        Me.CareLabel10.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel10.TabIndex = 2
        Me.CareLabel10.Text = "Name"
        Me.CareLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(9, 12)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(28, 15)
        Me.CareLabel14.TabIndex = 0
        Me.CareLabel14.Text = "Code"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFullname
        '
        Me.txtFullname.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtFullname.EditValue = ""
        Me.txtFullname.EnterMoveNextControl = True
        Me.txtFullname.Location = New System.Drawing.Point(85, 37)
        Me.txtFullname.MaxLength = 30
        Me.txtFullname.Name = "txtFullname"
        Me.txtFullname.NumericAllowNegatives = False
        Me.txtFullname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtFullname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFullname.Properties.AccessibleName = "Supplier Name"
        Me.txtFullname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFullname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtFullname.Properties.Appearance.Options.UseFont = True
        Me.txtFullname.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFullname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFullname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFullname.Properties.MaxLength = 30
        Me.txtFullname.Size = New System.Drawing.Size(348, 22)
        Me.txtFullname.TabIndex = 3
        Me.txtFullname.Tag = "AEM"
        Me.txtFullname.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtFullname.ToolTipText = ""
        '
        'txtCode
        '
        Me.txtCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCode.EditValue = ""
        Me.txtCode.EnterMoveNextControl = True
        Me.txtCode.Location = New System.Drawing.Point(85, 9)
        Me.txtCode.MaxLength = 10
        Me.txtCode.Name = "txtCode"
        Me.txtCode.NumericAllowNegatives = False
        Me.txtCode.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.None
        Me.txtCode.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCode.Properties.AccessibleName = "Supplier Code"
        Me.txtCode.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtCode.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtCode.Properties.Appearance.Options.UseFont = True
        Me.txtCode.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtCode.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCode.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtCode.Properties.MaxLength = 10
        Me.txtCode.Size = New System.Drawing.Size(101, 22)
        Me.txtCode.TabIndex = 1
        Me.txtCode.Tag = "AM"
        Me.txtCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCode.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(9, 96)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(71, 15)
        Me.CareLabel1.TabIndex = 6
        Me.CareLabel1.Text = "Rate per hour"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRate
        '
        Me.txtRate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRate.EditValue = ""
        Me.txtRate.EnterMoveNextControl = True
        Me.txtRate.Location = New System.Drawing.Point(85, 93)
        Me.txtRate.MaxLength = 14
        Me.txtRate.Name = "txtRate"
        Me.txtRate.NumericAllowNegatives = False
        Me.txtRate.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtRate.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRate.Properties.AccessibleName = "Supplier Code"
        Me.txtRate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRate.Properties.Appearance.Options.UseFont = True
        Me.txtRate.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRate.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRate.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtRate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtRate.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRate.Properties.MaxLength = 14
        Me.txtRate.Size = New System.Drawing.Size(101, 22)
        Me.txtRate.TabIndex = 7
        Me.txtRate.Tag = "AEM"
        Me.txtRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRate.ToolTipText = ""
        '
        'chkActive
        '
        Me.chkActive.EnterMoveNextControl = True
        Me.chkActive.Location = New System.Drawing.Point(378, 10)
        Me.chkActive.Name = "chkActive"
        Me.chkActive.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkActive.Properties.Appearance.Options.UseFont = True
        Me.chkActive.Properties.AutoWidth = True
        Me.chkActive.Properties.Caption = "Active"
        Me.chkActive.Size = New System.Drawing.Size(55, 19)
        Me.chkActive.TabIndex = 13
        Me.chkActive.Tag = "AE"
        '
        'chkOffice
        '
        Me.chkOffice.EnterMoveNextControl = True
        Me.chkOffice.Location = New System.Drawing.Point(284, 10)
        Me.chkOffice.Name = "chkOffice"
        Me.chkOffice.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkOffice.Properties.Appearance.Options.UseFont = True
        Me.chkOffice.Properties.AutoWidth = True
        Me.chkOffice.Properties.Caption = "Office Based"
        Me.chkOffice.Size = New System.Drawing.Size(88, 19)
        Me.chkOffice.TabIndex = 12
        Me.chkOffice.Tag = "AE"
        '
        'frmEmployees
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(474, 282)
        Me.Controls.Add(Me.GroupControl1)
        Me.Name = "frmEmployees"
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtNickname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFullname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkActive.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkOffice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents txtNickname As Care.Controls.CareTextBox
    Friend WithEvents CareLabel10 As Care.Controls.CareLabel
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents txtFullname As Care.Controls.CareTextBox
    Friend WithEvents txtCode As Care.Controls.CareTextBox
    Friend WithEvents txtEmail As Care.Shared.CareEmailAddress
    Friend WithEvents txtTel As Care.Shared.CareTelephoneNumber
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents txtRate As Care.Controls.CareTextBox
    Friend WithEvents chkOffice As Care.Controls.CareCheckBox
    Friend WithEvents chkActive As Care.Controls.CareCheckBox
End Class
