﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmInvoiceManual
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.txtInvoiceText = New DevExpress.XtraEditors.MemoEdit()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.txtNet = New Care.Controls.CareTextBox(Me.components)
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.cdtInvoiceDate = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.txtInvoiceNo = New Care.Controls.CareTextBox(Me.components)
        Me.btnCreate = New Care.Controls.CareButton(Me.components)
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtInvoiceText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.txtNet.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.cdtInvoiceDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtInvoiceDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtInvoiceNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.txtInvoiceText)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 105)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(636, 232)
        Me.GroupControl1.TabIndex = 1
        Me.GroupControl1.Text = "Invoice Text"
        '
        'txtInvoiceText
        '
        Me.txtInvoiceText.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtInvoiceText.Location = New System.Drawing.Point(5, 23)
        Me.txtInvoiceText.Name = "txtInvoiceText"
        Me.txtInvoiceText.Properties.AccessibleName = "Invoice Text"
        Me.txtInvoiceText.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInvoiceText.Properties.Appearance.Options.UseFont = True
        Me.txtInvoiceText.Properties.MaxLength = 1024
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtInvoiceText, True)
        Me.txtInvoiceText.Size = New System.Drawing.Size(626, 204)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtInvoiceText, OptionsSpelling1)
        Me.txtInvoiceText.TabIndex = 0
        Me.txtInvoiceText.Tag = "AEM"
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.CareLabel1)
        Me.GroupControl4.Controls.Add(Me.txtNet)
        Me.GroupControl4.Location = New System.Drawing.Point(12, 343)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(636, 57)
        Me.GroupControl4.TabIndex = 2
        Me.GroupControl4.Text = "Invoice Value"
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(425, 30)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(66, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Net Amount"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNet
        '
        Me.txtNet.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtNet.EnterMoveNextControl = True
        Me.txtNet.Location = New System.Drawing.Point(497, 27)
        Me.txtNet.MaxLength = 14
        Me.txtNet.Name = "txtNet"
        Me.txtNet.NumericAllowNegatives = True
        Me.txtNet.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtNet.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtNet.Properties.AccessibleName = "Invoice Net Amount"
        Me.txtNet.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtNet.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtNet.Properties.Appearance.Options.UseFont = True
        Me.txtNet.Properties.Appearance.Options.UseTextOptions = True
        Me.txtNet.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtNet.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txtNet.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtNet.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtNet.Properties.MaxLength = 14
        Me.txtNet.Size = New System.Drawing.Size(130, 22)
        Me.txtNet.TabIndex = 1
        Me.txtNet.Tag = "AEM"
        Me.txtNet.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNet.ToolTipText = ""
        '
        'GroupControl5
        '
        Me.GroupControl5.Controls.Add(Me.CareLabel2)
        Me.GroupControl5.Controls.Add(Me.cdtInvoiceDate)
        Me.GroupControl5.Controls.Add(Me.CareLabel3)
        Me.GroupControl5.Controls.Add(Me.txtInvoiceNo)
        Me.GroupControl5.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(636, 87)
        Me.GroupControl5.TabIndex = 0
        Me.GroupControl5.Text = "Account Details"
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(10, 59)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(65, 15)
        Me.CareLabel2.TabIndex = 2
        Me.CareLabel2.Text = "Invoice Date"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtInvoiceDate
        '
        Me.cdtInvoiceDate.EditValue = Nothing
        Me.cdtInvoiceDate.EnterMoveNextControl = True
        Me.cdtInvoiceDate.Location = New System.Drawing.Point(129, 56)
        Me.cdtInvoiceDate.Name = "cdtInvoiceDate"
        Me.cdtInvoiceDate.Properties.AccessibleName = "Invoice Date"
        Me.cdtInvoiceDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtInvoiceDate.Properties.Appearance.Options.UseFont = True
        Me.cdtInvoiceDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtInvoiceDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtInvoiceDate.Size = New System.Drawing.Size(100, 22)
        Me.cdtInvoiceDate.TabIndex = 3
        Me.cdtInvoiceDate.Tag = "AEM"
        Me.cdtInvoiceDate.Value = Nothing
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(10, 31)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(113, 15)
        Me.CareLabel3.TabIndex = 0
        Me.CareLabel3.Text = "Sage Invoice Number"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtInvoiceNo
        '
        Me.txtInvoiceNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtInvoiceNo.EnterMoveNextControl = True
        Me.txtInvoiceNo.Location = New System.Drawing.Point(129, 28)
        Me.txtInvoiceNo.MaxLength = 10
        Me.txtInvoiceNo.Name = "txtInvoiceNo"
        Me.txtInvoiceNo.NumericAllowNegatives = False
        Me.txtInvoiceNo.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtInvoiceNo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtInvoiceNo.Properties.AccessibleName = "Sage Invoice Number"
        Me.txtInvoiceNo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtInvoiceNo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtInvoiceNo.Properties.Appearance.Options.UseFont = True
        Me.txtInvoiceNo.Properties.Appearance.Options.UseTextOptions = True
        Me.txtInvoiceNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtInvoiceNo.Properties.Mask.EditMask = "##########;"
        Me.txtInvoiceNo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtInvoiceNo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtInvoiceNo.Properties.MaxLength = 10
        Me.txtInvoiceNo.Size = New System.Drawing.Size(100, 22)
        Me.txtInvoiceNo.TabIndex = 1
        Me.txtInvoiceNo.Tag = "AM"
        Me.txtInvoiceNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtInvoiceNo.ToolTipText = ""
        '
        'btnCreate
        '
        Me.btnCreate.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCreate.Appearance.Options.UseFont = True
        Me.btnCreate.Location = New System.Drawing.Point(472, 406)
        Me.btnCreate.Name = "btnCreate"
        Me.btnCreate.Size = New System.Drawing.Size(85, 25)
        Me.btnCreate.TabIndex = 3
        Me.btnCreate.Tag = ""
        Me.btnCreate.Text = "OK"
        '
        'btnClose
        '
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(563, 406)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(85, 25)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Tag = ""
        Me.btnClose.Text = "Cancel"
        '
        'frmInvoiceManual
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(658, 438)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnCreate)
        Me.Controls.Add(Me.GroupControl5)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.GroupControl1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmInvoiceManual"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmInvoice"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.txtInvoiceText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.txtNet.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.cdtInvoiceDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtInvoiceDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtInvoiceNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtInvoiceText As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents txtNet As Care.Controls.CareTextBox
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents txtInvoiceNo As Care.Controls.CareTextBox
    Friend WithEvents btnCreate As Care.Controls.CareButton
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents cdtInvoiceDate As Care.Controls.CareDateTime
End Class
