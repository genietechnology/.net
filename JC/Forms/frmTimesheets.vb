﻿Option Strict On

Imports Care.Global
Imports JC

Public Class frmTimesheets

    Private m_Employee As Business.Employee
    Private m_SelectedItemIndex As Integer = 0

    Private Sub frmTimesheets_Load(sender As Object, e As EventArgs) Handles Me.Load

        Dim _SQL As String = "select ID, fullname from Employees where active = 1 order by fullname"
        cbxEmployee.PopulateWithSQL(Session.ConnectionString, _SQL)

        cdtWC.BackColor = Session.ChangeColour
        cbxEmployee.BackColor = Session.ChangeColour

        tsEntry.PopulateTrades()
        tsEntry.SetChangeColour()
        tsEntry.Clear()

        SetCMDs()

    End Sub

#Region "Controls"

    Private Sub btnSameDay_Click(sender As Object, e As EventArgs) Handles btnSameDay.Click
        If tsEntry.ValidateEntry Then
            CopyEntry()
            tsEntry.SameDay()
        End If
    End Sub

    Private Sub btnNextDay_Click(sender As Object, e As EventArgs) Handles btnNextDay.Click
        If tsEntry.ValidateEntry Then
            CopyEntry()
            tsEntry.NextDay()
        End If
    End Sub

    Private Sub btnCopy_Click(sender As Object, e As EventArgs) Handles btnCopy.Click
        If tsEntry.ValidateEntry Then
            CopyEntry()
            tsEntry.CopyNextDay()
        End If
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If CareMessage("Are you sure you want to delete the selected entry?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Delete Entry") = DialogResult.Yes Then
            For Each _ts As TimeSheetControl In sc.Controls
                If _ts.ItemIndex = m_SelectedItemIndex Then
                    sc.Controls.Remove(_ts)
                    Exit For
                End If
            Next
            SetCMDs()
        End If
    End Sub

    Private Sub tsEntry_ItemSelected(sender As Object, e As TimeSheetControl.SelectedItemArgs) Handles tsEntry.ItemSelected
        m_SelectedItemIndex = 0
    End Sub

    Private Sub btnSaveNew_Click(sender As Object, e As EventArgs) Handles btnSaveNew.Click

        If ValidateTimeSheet() Then

            SaveTimeSheet()

            sc.Controls.Clear()
            tsEntry.Clear()

            cbxEmployee.Text = ""
            cbxEmployee.Focus()

        Else
            CareMessage("Timesheet Validation Error - Please check.", MessageBoxIcon.Exclamation, "Validate Timesheet Entry")
        End If

    End Sub

    Private Sub btnSaveExit_Click(sender As Object, e As EventArgs) Handles btnSaveExit.Click
        If ValidateTimeSheet() Then
            SaveTimeSheet()
            Me.Close()
        End If
    End Sub

#End Region

    Private Sub CopyEntry()

        Dim _Ts As New TimeSheetControl
        With _Ts
            .PopulateTrades()
            .HighlightSelected = True
            .Dock = DockStyle.Top
            .ItemIndex = sc.Controls.Count + 1
            .VanNo = tsEntry.VanNo
            .DayOfWeek = tsEntry.DayOfWeek
            .JobNo = tsEntry.JobNo
            .What = tsEntry.What
            .StartTime = tsEntry.StartTime
            .FinishTime = tsEntry.FinishTime
            .YardTime = tsEntry.YardTime
        End With

        AddHandler _Ts.ItemSelected, AddressOf ItemSelected

        sc.Controls.Add(_Ts)
        sc.Controls.SetChildIndex(_Ts, 0)

        SetCMDs()

    End Sub

    Private Sub ItemSelected(sender As Object, e As TimeSheetControl.SelectedItemArgs)
        m_SelectedItemIndex = e.SelectedIndex
    End Sub

    Private Sub SetCMDs()
        If sc.Controls.Count > 0 Then
            btnDelete.Enabled = True
            btnSaveExit.Enabled = True
            btnSaveNew.Enabled = True
        Else
            btnDelete.Enabled = False
            btnSaveExit.Enabled = False
            btnSaveNew.Enabled = False
        End If
    End Sub

    Private Sub SaveTimeSheet()

        For Each _ts As TimeSheetControl In sc.Controls

            Dim _t As New Business.Timesheet
            With _t

                ._TsDate = GetDate(_ts.DayOfWeek)
                ._TsEmpId = m_Employee._ID
                ._TsEmpRate = m_Employee._Rate
                ._TsVanId = _ts.VanID
                ._TsJobId = _ts.JobID
                ._TsWhat = _ts.What

                ._TsJobStart = _ts.TSStart
                ._TsJobFinish = _ts.TSFinish
                ._TsJobDuration = CalculateHours(._TsJobStart, ._TsJobFinish)
                ._TsCost = ._TsJobDuration * ._TsEmpRate

                If _ts.TSStart.HasValue AndAlso _ts.TSFinish.HasValue AndAlso _ts.TSYard.HasValue Then
                    'if we have a start, finish and yard time - its one timesheet for the day or the last timesheet for a given day
                    ._TsDayStart = _ts.TSStart
                    ._TsDayFinish = _ts.TSYard
                    ._TsDayDuration = CalculateHours(._TsDayStart, ._TsDayFinish) 'day duration is stored here so we know how long the travel time back to the yard was
                    ._TsCost = ._TsDayDuration * ._TsEmpRate
                Else
                    If _ts.TSStart.HasValue AndAlso _ts.TSFinish.HasValue AndAlso Not _ts.TSYard.HasValue Then
                        ._TsDayStart = Nothing
                        ._TsDayFinish = Nothing
                        ._TsDayDuration = 0
                    End If
                End If

                .Store()

            End With

        Next

    End Sub

    Private Function CalculateHours(ByVal StartTime As TimeSpan?, ByVal FinishTime As TimeSpan?) As Decimal
        Dim _Mins As Double = CalculateMins(StartTime, FinishTime)
        If _Mins = 0 Then
            Return 0
        Else
            Return CDec(_Mins / 60)
        End If
    End Function

    Private Function CalculateMins(ByVal StartTime As TimeSpan?, ByVal FinishTime As TimeSpan?) As Double
        If Not StartTime.HasValue OrElse Not FinishTime.HasValue Then Return 0
        Dim _Diff As TimeSpan = FinishTime.Value - StartTime.Value
        Return _Diff.TotalMinutes
    End Function

    Private Function GetDate(ByVal Weekday As DayOfWeek) As Date?
        Dim _Date As Date = cdtWC.Value.Value
        If Weekday = DayOfWeek.Tuesday Then _Date = _Date.AddDays(1)
        If Weekday = DayOfWeek.Wednesday Then _Date = _Date.AddDays(2)
        If Weekday = DayOfWeek.Thursday Then _Date = _Date.AddDays(3)
        If Weekday = DayOfWeek.Friday Then _Date = _Date.AddDays(4)
        If Weekday = DayOfWeek.Saturday Then _Date = _Date.AddDays(5)
        If Weekday = DayOfWeek.Sunday Then _Date = _Date.AddDays(6)
        Return _Date
    End Function

    Private Function ValidateTimeSheet() As Boolean

        If cdtWC.Text = "" Then
            CareMessage("Please enter a Week Commencing Date.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If cdtWC.Value.HasValue AndAlso cdtWC.Value.Value.DayOfWeek <> DayOfWeek.Monday Then
            CareMessage("Please ensure the Week Commencing Date is a Monday.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If cbxEmployee.Text = "" Then
            CareMessage("Please select an Employee.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        Dim _Return As Boolean = True
        Dim _Entries As New List(Of TimesheetValidation)

        For Each _ts As TimeSheetControl In sc.Controls
            If _ts.ValidateEntry Then
                InsertDay(_Entries, _ts.DayOfWeek, _ts.TSYard)
            Else
                _Return = False
            End If
        Next

        If _Return Then

            'now ensure each day has a "yard time"
            'there could be several entries for one employee for each day (multiple jobs), but there must be one "back at yard" record
            For Each _te As TimesheetValidation In _Entries
                If _te.YardTimeCount <> 1 Then
                    _Return = False
                    Exit For
                End If
            Next

            Return _Return

        Else
            Return False
        End If

    End Function

    Private Sub InsertDay(ByRef Entries As List(Of TimesheetValidation), ByVal DayOfWeekIn As DayOfWeek, YardTimeIn As TimeSpan?)

        Dim _Found As Boolean = False

        For Each _e In Entries
            'do we have an entry for this day of the week?
            If _e.DOW = DayOfWeekIn Then
                If YardTimeIn.HasValue Then _e.YardTimeCount += 1
                _Found = True
            End If
        Next

        If Not _Found Then
            Entries.Add(New TimesheetValidation(DayOfWeekIn, YardTimeIn.HasValue))
        End If

    End Sub

    Private Sub cbxEmployee_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxEmployee.SelectedIndexChanged
        If cbxEmployee.Text <> "" Then
            Dim _e As Business.Employee = Business.Employee.RetreiveByID(New Guid(cbxEmployee.SelectedValue.ToString))
            If _e IsNot Nothing Then
                m_Employee = _e
            Else
                m_Employee = Nothing
            End If
        Else
            m_Employee = Nothing
        End If
    End Sub

    Private Class TimesheetValidation

        Property DOW As DayOfWeek
        Property YardTimeCount As Integer

        Public Sub New(ByVal DayOfWeekIn As DayOfWeek, YardTimePresent As Boolean)
            Me.DOW = DayOfWeekIn
            If YardTimePresent Then
                Me.YardTimeCount = 1
            Else
                Me.YardTimeCount = 0
            End If
        End Sub

    End Class

End Class