﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmContact
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl3 = New Care.Controls.CareFrame(Me.components)
        Me.txtEmailOther = New Care.[Shared].CareEmailAddress()
        Me.txtEmail = New Care.[Shared].CareEmailAddress()
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox7 = New Care.Controls.CareFrame(Me.components)
        Me.txtJobTitle = New Care.Controls.CareTextBox(Me.components)
        Me.Label18 = New Care.Controls.CareLabel(Me.components)
        Me.txtCompany = New Care.Controls.CareTextBox(Me.components)
        Me.Label20 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox2 = New Care.Controls.CareFrame(Me.components)
        Me.txtTelWork = New Care.[Shared].CareTelephoneNumber()
        Me.txtTelOther = New Care.[Shared].CareTelephoneNumber()
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.txtTelHome = New Care.[Shared].CareTelephoneNumber()
        Me.txtTelMobile = New Care.[Shared].CareTelephoneNumber()
        Me.Label8 = New Care.Controls.CareLabel(Me.components)
        Me.Label9 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox1 = New Care.Controls.CareFrame(Me.components)
        Me.txtRelationship = New Care.Controls.CareTextBox(Me.components)
        Me.Label4 = New Care.Controls.CareLabel(Me.components)
        Me.txtSurname = New Care.Controls.CareTextBox(Me.components)
        Me.Label2 = New Care.Controls.CareLabel(Me.components)
        Me.txtForename = New Care.Controls.CareTextBox(Me.components)
        Me.Label1 = New Care.Controls.CareLabel(Me.components)
        Me.txtFullname = New Care.Controls.CareTextBox(Me.components)
        Me.Label17 = New Care.Controls.CareLabel(Me.components)
        Me.btnOK = New Care.Controls.CareButton(Me.components)
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        Me.CareButton1 = New Care.Controls.CareButton(Me.components)
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.GroupBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox7.SuspendLayout()
        CType(Me.txtJobTitle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCompany.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtRelationship.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtForename.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFullname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl3.AppearanceCaption.Options.UseFont = True
        Me.GroupControl3.Controls.Add(Me.txtEmailOther)
        Me.GroupControl3.Controls.Add(Me.txtEmail)
        Me.GroupControl3.Controls.Add(Me.CareLabel4)
        Me.GroupControl3.Controls.Add(Me.CareLabel6)
        Me.GroupControl3.Location = New System.Drawing.Point(12, 333)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(530, 93)
        Me.GroupControl3.TabIndex = 4
        Me.GroupControl3.Text = "Email Addresses"
        '
        'txtEmailOther
        '
        Me.txtEmailOther.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtEmailOther.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmailOther.Appearance.Options.UseFont = True
        Me.txtEmailOther.Location = New System.Drawing.Point(113, 60)
        Me.txtEmailOther.MaxLength = 100
        Me.txtEmailOther.Name = "txtEmailOther"
        Me.txtEmailOther.NoButton = False
        Me.txtEmailOther.NoColours = False
        Me.txtEmailOther.NoValidate = False
        Me.txtEmailOther.ReadOnly = False
        Me.txtEmailOther.Size = New System.Drawing.Size(409, 22)
        Me.txtEmailOther.TabIndex = 3
        Me.txtEmailOther.Tag = "AE"
        '
        'txtEmail
        '
        Me.txtEmail.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtEmail.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail.Appearance.Options.UseFont = True
        Me.txtEmail.Location = New System.Drawing.Point(113, 32)
        Me.txtEmail.MaxLength = 100
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.NoButton = False
        Me.txtEmail.NoColours = False
        Me.txtEmail.NoValidate = False
        Me.txtEmail.ReadOnly = False
        Me.txtEmail.Size = New System.Drawing.Size(409, 22)
        Me.txtEmail.TabIndex = 1
        Me.txtEmail.Tag = "AE"
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(8, 64)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(73, 15)
        Me.CareLabel4.TabIndex = 2
        Me.CareLabel4.Text = "Work Address"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(8, 36)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(86, 15)
        Me.CareLabel6.TabIndex = 0
        Me.CareLabel6.Text = "Primary Address"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox7
        '
        Me.GroupBox7.AppearanceCaption.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox7.AppearanceCaption.Options.UseFont = True
        Me.GroupBox7.Controls.Add(Me.txtJobTitle)
        Me.GroupBox7.Controls.Add(Me.Label18)
        Me.GroupBox7.Controls.Add(Me.txtCompany)
        Me.GroupBox7.Controls.Add(Me.Label20)
        Me.GroupBox7.Location = New System.Drawing.Point(12, 145)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(530, 85)
        Me.GroupBox7.TabIndex = 2
        Me.GroupBox7.Text = "Employment Information"
        '
        'txtJobTitle
        '
        Me.txtJobTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtJobTitle.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtJobTitle.EnterMoveNextControl = True
        Me.txtJobTitle.Location = New System.Drawing.Point(113, 56)
        Me.txtJobTitle.MaxLength = 40
        Me.txtJobTitle.Name = "txtJobTitle"
        Me.txtJobTitle.NumericAllowNegatives = False
        Me.txtJobTitle.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtJobTitle.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtJobTitle.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtJobTitle.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobTitle.Properties.Appearance.Options.UseFont = True
        Me.txtJobTitle.Properties.Appearance.Options.UseTextOptions = True
        Me.txtJobTitle.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtJobTitle.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtJobTitle.Properties.MaxLength = 40
        Me.txtJobTitle.Size = New System.Drawing.Size(409, 22)
        Me.txtJobTitle.TabIndex = 3
        Me.txtJobTitle.Tag = "AE"
        Me.txtJobTitle.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtJobTitle.ToolTipText = ""
        '
        'Label18
        '
        Me.Label18.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label18.Location = New System.Drawing.Point(8, 59)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(44, 15)
        Me.Label18.TabIndex = 2
        Me.Label18.Text = "Job Title"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCompany
        '
        Me.txtCompany.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCompany.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtCompany.EnterMoveNextControl = True
        Me.txtCompany.Location = New System.Drawing.Point(113, 28)
        Me.txtCompany.MaxLength = 40
        Me.txtCompany.Name = "txtCompany"
        Me.txtCompany.NumericAllowNegatives = False
        Me.txtCompany.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtCompany.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCompany.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtCompany.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCompany.Properties.Appearance.Options.UseFont = True
        Me.txtCompany.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCompany.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtCompany.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtCompany.Properties.MaxLength = 40
        Me.txtCompany.Size = New System.Drawing.Size(409, 22)
        Me.txtCompany.TabIndex = 1
        Me.txtCompany.Tag = "AE"
        Me.txtCompany.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCompany.ToolTipText = ""
        '
        'Label20
        '
        Me.Label20.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label20.Location = New System.Drawing.Point(8, 31)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(87, 15)
        Me.Label20.TabIndex = 0
        Me.Label20.Text = "Company Name"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox2
        '
        Me.GroupBox2.AppearanceCaption.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.AppearanceCaption.Options.UseFont = True
        Me.GroupBox2.Controls.Add(Me.txtTelWork)
        Me.GroupBox2.Controls.Add(Me.txtTelOther)
        Me.GroupBox2.Controls.Add(Me.CareLabel1)
        Me.GroupBox2.Controls.Add(Me.CareLabel3)
        Me.GroupBox2.Controls.Add(Me.txtTelHome)
        Me.GroupBox2.Controls.Add(Me.txtTelMobile)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 236)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(530, 91)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.Text = "Telephone Numbers"
        '
        'txtTelWork
        '
        Me.txtTelWork.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTelWork.Appearance.Options.UseFont = True
        Me.txtTelWork.IsMobile = False
        Me.txtTelWork.Location = New System.Drawing.Point(348, 32)
        Me.txtTelWork.MaxLength = 15
        Me.txtTelWork.MinimumSize = New System.Drawing.Size(0, 22)
        Me.txtTelWork.Name = "txtTelWork"
        Me.txtTelWork.ReadOnly = False
        Me.txtTelWork.Size = New System.Drawing.Size(173, 22)
        Me.txtTelWork.TabIndex = 5
        Me.txtTelWork.Tag = "AE"
        '
        'txtTelOther
        '
        Me.txtTelOther.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTelOther.Appearance.Options.UseFont = True
        Me.txtTelOther.IsMobile = False
        Me.txtTelOther.Location = New System.Drawing.Point(348, 60)
        Me.txtTelOther.MaxLength = 15
        Me.txtTelOther.MinimumSize = New System.Drawing.Size(0, 22)
        Me.txtTelOther.Name = "txtTelOther"
        Me.txtTelOther.ReadOnly = False
        Me.txtTelOther.Size = New System.Drawing.Size(173, 22)
        Me.txtTelOther.TabIndex = 7
        Me.txtTelOther.Tag = "AE"
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(314, 64)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(30, 15)
        Me.CareLabel1.TabIndex = 6
        Me.CareLabel1.Text = "Other"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(314, 35)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(28, 15)
        Me.CareLabel3.TabIndex = 4
        Me.CareLabel3.Text = "Work"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTelHome
        '
        Me.txtTelHome.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTelHome.Appearance.Options.UseFont = True
        Me.txtTelHome.IsMobile = False
        Me.txtTelHome.Location = New System.Drawing.Point(113, 32)
        Me.txtTelHome.MaxLength = 15
        Me.txtTelHome.MinimumSize = New System.Drawing.Size(0, 22)
        Me.txtTelHome.Name = "txtTelHome"
        Me.txtTelHome.ReadOnly = False
        Me.txtTelHome.Size = New System.Drawing.Size(173, 22)
        Me.txtTelHome.TabIndex = 1
        Me.txtTelHome.Tag = "AE"
        '
        'txtTelMobile
        '
        Me.txtTelMobile.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTelMobile.Appearance.Options.UseFont = True
        Me.txtTelMobile.IsMobile = True
        Me.txtTelMobile.Location = New System.Drawing.Point(113, 60)
        Me.txtTelMobile.MaxLength = 15
        Me.txtTelMobile.MinimumSize = New System.Drawing.Size(0, 22)
        Me.txtTelMobile.Name = "txtTelMobile"
        Me.txtTelMobile.ReadOnly = False
        Me.txtTelMobile.Size = New System.Drawing.Size(173, 22)
        Me.txtTelMobile.TabIndex = 3
        Me.txtTelMobile.Tag = "AE"
        '
        'Label8
        '
        Me.Label8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label8.Location = New System.Drawing.Point(8, 64)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(37, 15)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "Mobile"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label9.Location = New System.Drawing.Point(8, 35)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(33, 15)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Home"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtRelationship)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtSurname)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtForename)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtFullname)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.ShowCaption = False
        Me.GroupBox1.Size = New System.Drawing.Size(530, 127)
        Me.GroupBox1.TabIndex = 0
        '
        'txtRelationship
        '
        Me.txtRelationship.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtRelationship.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtRelationship.EnterMoveNextControl = True
        Me.txtRelationship.Location = New System.Drawing.Point(113, 96)
        Me.txtRelationship.MaxLength = 40
        Me.txtRelationship.Name = "txtRelationship"
        Me.txtRelationship.NumericAllowNegatives = False
        Me.txtRelationship.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRelationship.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRelationship.Properties.AccessibleName = "Relationship"
        Me.txtRelationship.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRelationship.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRelationship.Properties.Appearance.Options.UseFont = True
        Me.txtRelationship.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRelationship.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRelationship.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRelationship.Properties.MaxLength = 40
        Me.txtRelationship.Size = New System.Drawing.Size(406, 22)
        Me.txtRelationship.TabIndex = 7
        Me.txtRelationship.Tag = "AETM"
        Me.txtRelationship.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRelationship.ToolTipText = ""
        '
        'Label4
        '
        Me.Label4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label4.Location = New System.Drawing.Point(8, 99)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(65, 15)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Relationship"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSurname
        '
        Me.txtSurname.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSurname.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtSurname.EnterMoveNextControl = True
        Me.txtSurname.Location = New System.Drawing.Point(113, 67)
        Me.txtSurname.MaxLength = 40
        Me.txtSurname.Name = "txtSurname"
        Me.txtSurname.NumericAllowNegatives = False
        Me.txtSurname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSurname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSurname.Properties.AccessibleName = "Surname"
        Me.txtSurname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSurname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSurname.Properties.Appearance.Options.UseFont = True
        Me.txtSurname.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSurname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSurname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSurname.Properties.MaxLength = 40
        Me.txtSurname.Size = New System.Drawing.Size(406, 22)
        Me.txtSurname.TabIndex = 5
        Me.txtSurname.Tag = "AETM"
        Me.txtSurname.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSurname.ToolTipText = ""
        '
        'Label2
        '
        Me.Label2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label2.Location = New System.Drawing.Point(8, 70)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(47, 15)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Surname"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtForename
        '
        Me.txtForename.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtForename.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtForename.EnterMoveNextControl = True
        Me.txtForename.Location = New System.Drawing.Point(113, 40)
        Me.txtForename.MaxLength = 40
        Me.txtForename.Name = "txtForename"
        Me.txtForename.NumericAllowNegatives = False
        Me.txtForename.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtForename.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtForename.Properties.AccessibleName = "Forename"
        Me.txtForename.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtForename.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtForename.Properties.Appearance.Options.UseFont = True
        Me.txtForename.Properties.Appearance.Options.UseTextOptions = True
        Me.txtForename.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtForename.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtForename.Properties.MaxLength = 40
        Me.txtForename.Size = New System.Drawing.Size(406, 22)
        Me.txtForename.TabIndex = 3
        Me.txtForename.Tag = "AETM"
        Me.txtForename.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtForename.ToolTipText = ""
        '
        'Label1
        '
        Me.Label1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label1.Location = New System.Drawing.Point(8, 43)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 15)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Forename"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFullname
        '
        Me.txtFullname.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtFullname.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtFullname.EnterMoveNextControl = True
        Me.txtFullname.Location = New System.Drawing.Point(113, 12)
        Me.txtFullname.MaxLength = 0
        Me.txtFullname.Name = "txtFullname"
        Me.txtFullname.NumericAllowNegatives = False
        Me.txtFullname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtFullname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFullname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFullname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFullname.Properties.Appearance.Options.UseFont = True
        Me.txtFullname.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFullname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFullname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFullname.Size = New System.Drawing.Size(406, 22)
        Me.txtFullname.TabIndex = 1
        Me.txtFullname.Tag = "R"
        Me.txtFullname.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtFullname.ToolTipText = ""
        '
        'Label17
        '
        Me.Label17.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label17.Location = New System.Drawing.Point(8, 15)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(49, 15)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Fullname"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(376, 434)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(80, 25)
        Me.btnOK.TabIndex = 6
        Me.btnOK.Tag = ""
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(462, 434)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(80, 25)
        Me.btnCancel.TabIndex = 7
        Me.btnCancel.Tag = "Y"
        Me.btnCancel.Text = "Cancel"
        '
        'CareButton1
        '
        Me.CareButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CareButton1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareButton1.Appearance.Options.UseFont = True
        Me.CareButton1.Location = New System.Drawing.Point(12, 434)
        Me.CareButton1.Name = "CareButton1"
        Me.CareButton1.Size = New System.Drawing.Size(80, 25)
        Me.CareButton1.TabIndex = 5
        Me.CareButton1.Tag = ""
        Me.CareButton1.Text = "Edit"
        Me.CareButton1.Visible = False
        '
        'frmContact
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(553, 466)
        Me.Controls.Add(Me.CareButton1)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.GroupBox7)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Margin = New System.Windows.Forms.Padding(3, 5, 3, 5)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmContact"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmContact"
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.GroupBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        CType(Me.txtJobTitle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCompany.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.txtRelationship.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtForename.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFullname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupControl3 As Care.Controls.CareFrame
    Friend WithEvents txtEmailOther As Care.Shared.CareEmailAddress
    Friend WithEvents txtEmail As Care.Shared.CareEmailAddress
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents GroupBox7 As Care.Controls.CareFrame
    Friend WithEvents txtJobTitle As Care.Controls.CareTextBox
    Friend WithEvents Label18 As Care.Controls.CareLabel
    Friend WithEvents txtCompany As Care.Controls.CareTextBox
    Friend WithEvents Label20 As Care.Controls.CareLabel
    Friend WithEvents GroupBox2 As Care.Controls.CareFrame
    Friend WithEvents txtTelWork As Care.Shared.CareTelephoneNumber
    Friend WithEvents txtTelOther As Care.Shared.CareTelephoneNumber
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents txtTelHome As Care.Shared.CareTelephoneNumber
    Friend WithEvents txtTelMobile As Care.Shared.CareTelephoneNumber
    Friend WithEvents Label8 As Care.Controls.CareLabel
    Friend WithEvents Label9 As Care.Controls.CareLabel
    Friend WithEvents GroupBox1 As Care.Controls.CareFrame
    Friend WithEvents txtRelationship As Care.Controls.CareTextBox
    Friend WithEvents Label4 As Care.Controls.CareLabel
    Friend WithEvents txtSurname As Care.Controls.CareTextBox
    Friend WithEvents Label2 As Care.Controls.CareLabel
    Friend WithEvents txtForename As Care.Controls.CareTextBox
    Friend WithEvents Label1 As Care.Controls.CareLabel
    Friend WithEvents txtFullname As Care.Controls.CareTextBox
    Friend WithEvents Label17 As Care.Controls.CareLabel
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents CareButton1 As Care.Controls.CareButton
End Class
