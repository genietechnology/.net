﻿Option Strict On
Imports Care.Global
Imports Care.Shared

Public Class frmStock

    Private m_Stock As Business.Stock

    Private Sub frmStock_Load(sender As Object, e As EventArgs) Handles Me.Load
        MyControls.SetControls(ControlHandler.Mode.Add, Me.Controls)
        gbxItem.Hide()
        PopulateGrid()
    End Sub

    Private Sub PopulateGrid()

        Dim _SQL As String = ""

        _SQL += "select ID, quickfind as 'Quick Find', description as 'Item', sell_unit as 'UOM', sell_last_price as 'Last Price', sell_price as 'Price'"
        _SQL += " from Stock"
        _SQL += " order by description"

        cgStock.HideFirstColumn = True
        cgStock.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub cgStock_GridDoubleClick(sender As Object, e As EventArgs) Handles cgStock.GridDoubleClick

        Dim _ID As Guid? = ReturnDialogID(cgStock, "ID")
        If _ID.HasValue Then
            m_Stock = Business.Stock.RetreiveByID(_ID.Value)
            If m_Stock IsNot Nothing Then

                With m_Stock
                    txtQuickfind.Text = ._Quickfind
                    txtStockDesc.Text = ._Description
                    txtUOM.Text = ._SellUnit
                    txtPrice.Text = ValueHandler.MoneyAsText(._SellPrice)
                End With

                gbxItem.Show()
                txtQuickfind.Focus()

            End If
        End If

    End Sub

    Private Function ReturnDialogID(ByRef GridIn As Care.Controls.CareGrid, ByVal IDColumn As String) As Guid?

        If GridIn Is Nothing Then Return Nothing
        If GridIn.RecordCount < 1 Then Return Nothing
        If GridIn.CurrentRow(IDColumn).ToString = "" Then Return Nothing

        Return New Guid(GridIn.CurrentRow(IDColumn).ToString)

    End Function

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click

        With m_Stock
            ._Quickfind = txtQuickfind.Text
            ._Description = txtStockDesc.Text
            ._SellUnit = txtUOM.Text
            ._SellPrice = ValueHandler.ConvertDecimal(txtPrice.Text)
            .Store()
        End With

        gbxItem.Hide()

        cgStock.PersistRowPosition()
        PopulateGrid()
        cgStock.RestoreRowPosition()

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        gbxItem.Hide()
    End Sub

End Class
