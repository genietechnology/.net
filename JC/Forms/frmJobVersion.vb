﻿Imports Care.Global
Imports Care.Shared

Public Class frmJobVersion

    Private m_New As Boolean = False
    Private m_JobID As Guid = Nothing
    Private m_Version As New Business.JobVersion

    Public Sub New(ByVal JobID As Guid, ByVal VersionID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_JobID = JobID

        If VersionID.HasValue Then
            Me.Text = "Amend Version"
            m_Version = Business.JobVersion.RetreiveByID(VersionID.Value)
        Else
            Me.Text = "Create New Version"
            m_New = True
        End If

    End Sub

    Private Sub frmInvoice_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cbxReason.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("VERSION REASONS"))

        If m_New Then
            MyControls.SetControls(ControlHandler.Mode.Add, Me.Controls)
        Else
            MyControls.SetControls(ControlHandler.Mode.Edit, Me.Controls)
            DisplayRecord()
        End If

    End Sub

    Private Sub DisplayRecord()
        cdtVersionDate.Value = m_Version._VerDate
        cbxReason.Text = m_Version._VerReason
        txtComments.Text = m_Version._VerNotes
        txtValue.Text = ValueHandler.MoneyAsText(m_Version._VerValue)
    End Sub

    Private Sub SaveInvoice()

        With m_Version

            If m_New Then
                ._JobId = m_JobID
                ._VerUser = Session.CurrentUser.UserCode
                ._VerStamp = Now
            End If

            ._VerDate = cdtVersionDate.Value
            ._VerReason = cbxReason.Text
            ._VerNotes = txtComments.Text
            ._VerValue = ValueHandler.ConvertDecimal(txtValue.Text)

            .Store()

        End With

    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnCreate_Click(sender As Object, e As EventArgs) Handles btnCreate.Click
        If MyControls.Validate(Me.Controls) Then
            SaveInvoice()
            Me.DialogResult = DialogResult.OK
            Me.Close()
        End If
    End Sub
End Class