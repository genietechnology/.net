﻿Imports Care.Data
Imports Care.Global
Imports Care.Shared

Public Class frmImport

    Dim m_JC As String = "Data Source=AJ-SVR-01;Initial Catalog=job;User Id=careuser;Password=careuser;Application Name=Care.Framework;Workstation ID=JAMES-W530;Connect Timeout=90;"
    Dim m_DocConnectionString As String = Session.ConnectionString.Replace("jc", "doc")

    Dim m_Employees As New List(Of Business.Employee)

    Private Sub frmImport_Load(sender As Object, e As EventArgs) Handles Me.Load
        m_Employees = Business.Employee.RetreiveAll
    End Sub

    Private Sub CareButton1_Click(sender As Object, e As EventArgs) Handles CareButton1.Click

        If InputBox("Type Run to continue.").ToUpper <> "RUN" Then Exit Sub

        Dim _SQL As String = "delete from AppLists"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from AppListItems"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        '************************************************************************************************************************************************

        _SQL = "select ell_gl_code, ell_gl_desc from ellgenlst order by ell_gl_desc"

        Dim _DTLists As DataTable = DAL.GetDataTablefromSQL(m_JC, _SQL)
        If Not _DTLists Is Nothing Then

            For Each _DR As DataRow In _DTLists.Rows

                CreateList(_DR.Item("ell_gl_desc").ToString)

                _SQL = "select ell_gd_code, ell_gd_desc from ellgendat where ell_gd_list = '" + _DR.Item("ell_gl_code").ToString + "'"

                Dim _DTItems As DataTable = DAL.GetDataTablefromSQL(m_JC, _SQL)
                If Not _DTItems Is Nothing Then
                    For Each _DRItem As DataRow In _DTItems.Rows
                        CreateListItem(_DR.Item("ell_gl_desc").ToString, _DRItem.Item("ell_gd_desc").ToString, 0, _DRItem.Item("ell_gd_code").ToString)
                    Next
                End If

            Next

        End If

        '************************************************************************************************************************************************

        _SQL = "select aj_sec_code, aj_sec_desc from ajsecfle order by aj_sec_desc"

        Dim _DTSectors As DataTable = DAL.GetDataTablefromSQL(m_JC, _SQL)
        If Not _DTSectors Is Nothing Then

            CreateList("SECTORS")

            For Each _DR As DataRow In _DTSectors.Rows
                CreateListItem("SECTORS", _DR.Item("aj_sec_desc").ToString, 0, _DR.Item("aj_sec_code").ToString)
            Next

        End If

        CareMessage("DONE!")

    End Sub

    Private Sub CreateList(ByVal ListName As String)
        If Not ListHandler.ListExists(ListName) Then ListHandler.CreateList(ListName, 0)
    End Sub

    Private Sub CreateListItem(ByVal ListName As String, ByVal ItemName As String, ByVal DisplaySeq As Integer, Optional ByVal Ref1 As String = "")
        If ListHandler.ListExists(ListName) Then
            Dim _ListID As String = ""
            If DAL.ReturnField(Session.ConnectionString, "AppLists", "name", ListName, "ID", _ListID) Then
                If Not ListHandler.ItemExists(ListName, ItemName) Then ListHandler.CreateItem(_ListID, ItemName, DisplaySeq, Ref1)
            End If
        End If
    End Sub

    Private Sub CareButton2_Click(sender As Object, e As EventArgs) Handles CareButton2.Click

        If InputBox("Type Run to continue.").ToUpper <> "RUN" Then Exit Sub

        Dim _SQL As String = ""

        _SQL = "delete from Jobs"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from JobActivity"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from Contacts"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from JobContacts"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from JobFeedback"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from PurchaseInvoices"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from JobInvSales"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from JobLinks"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from JobNotes"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from JobReminders"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from Timesheets"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from JobVersions"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        '************************************************************************************************************************************************

        _SQL = "select * from ajjobfle order by aj_job_no desc"

        Dim _DTJobs As DataTable = DAL.GetDataTablefromSQL(m_JC, _SQL)
        If Not _DTJobs Is Nothing Then

            Session.SetupProgressBar("Importing Jobs...", _DTJobs.Rows.Count)

            For Each _DR As DataRow In _DTJobs.Rows

                Dim _ID As Guid = Guid.NewGuid

                Dim _j As New Business.Job
                With _j

                    ._ID = _ID
                    ._JobNo = ValueHandler.ConvertInteger(_DR.Item("aj_job_no"))
                    ._JobStatusCode = _DR.Item("aj_job_sts_code").ToString
                    ._JobStatus = _DR.Item("aj_job_sts_desc").ToString
                    ._Sector = ListHandler.ReturnItemUsingRef1("SECTORS", _DR.Item("aj_job_sector").ToString).Name

                    ._JobStart = ValueHandler.ConvertDate(_DR.Item("aj_job_start_date"))
                    ._JobEnd = ValueHandler.ConvertDate(_DR.Item("aj_job_end_date"))

                    ._Area = _DR.Item("aj_job_ara_desc").ToString
                    ._ContCode = _DR.Item("aj_job_cont_code").ToString
                    ._ContName = _DR.Item("aj_job_cont_desc").ToString

                    ._InsCompany = _DR.Item("aj_job_ins_desc").ToString
                    ._LossAdj = _DR.Item("aj_job_ins_loss_desc").ToString
                    ._InsRef = _DR.Item("aj_job_ins_ref").ToString
                    ._InsClaim = _DR.Item("aj_job_ins_claim").ToString

                    ._DescJob = _DR.Item("aj_job_desc").ToString
                    ._DescDirections = _DR.Item("aj_job_desc_dir").ToString
                    ._DescEst = _DR.Item("aj_job_desc_est").ToString
                    ._DescTools = _DR.Item("aj_job_desc_tools").ToString

                    ._CusRef = _DR.Item("aj_job_cust_ref").ToString
                    ._CusTitle = _DR.Item("aj_job_cust_title").ToString
                    ._CusInits = _DR.Item("aj_job_cust_inits").ToString
                    ._CusSurname = _DR.Item("aj_job_cust_surname").ToString
                    ._CusFullname = _DR.Item("aj_job_cust_fullname").ToString
                    ._CusAddress = BuildAddress(True, _DR.Item("aj_job_cust_add1").ToString, _DR.Item("aj_job_cust_add2").ToString, _DR.Item("aj_job_cust_add3").ToString, _DR.Item("aj_job_cust_add4").ToString, _DR.Item("aj_job_cust_pcode").ToString)
                    ._CusAddressLine = BuildAddress(False, _DR.Item("aj_job_cust_add1").ToString, _DR.Item("aj_job_cust_add2").ToString, _DR.Item("aj_job_cust_add3").ToString, _DR.Item("aj_job_cust_add4").ToString, _DR.Item("aj_job_cust_pcode").ToString)
                    ._CusAddress1 = _DR.Item("aj_job_cust_add1").ToString
                    ._CusPostcode = _DR.Item("aj_job_cust_pcode").ToString

                    ._RskTitle = _DR.Item("aj_job_prop_title").ToString
                    ._RskInits = _DR.Item("aj_job_prop_inits").ToString
                    ._RskSurname = _DR.Item("aj_job_prop_surname").ToString
                    ._RskFullname = _DR.Item("aj_job_prop_fullname").ToString
                    ._RskAddress = BuildAddress(True, _DR.Item("aj_job_prop_add1").ToString, _DR.Item("aj_job_prop_add2").ToString, _DR.Item("aj_job_prop_add3").ToString, _DR.Item("aj_job_prop_add4").ToString, _DR.Item("aj_job_prop_pcode").ToString)
                    ._RskAddressLine = BuildAddress(False, _DR.Item("aj_job_prop_add1").ToString, _DR.Item("aj_job_prop_add2").ToString, _DR.Item("aj_job_prop_add3").ToString, _DR.Item("aj_job_prop_add4").ToString, _DR.Item("aj_job_prop_pcode").ToString)
                    ._RskAddress1 = _DR.Item("aj_job_prop_add1").ToString
                    ._RskPostcode = _DR.Item("aj_job_prop_pcode").ToString

                    ._CusTels = ""
                    If _DR.Item("aj_job_cust_tel_home").ToString.Trim <> "" Then ._CusTels += _DR.Item("aj_job_cust_tel_home").ToString.Trim + " | "
                    If _DR.Item("aj_job_cust_tel_work").ToString.Trim <> "" Then ._CusTels += _DR.Item("aj_job_cust_tel_work").ToString.Trim + " | "
                    If _DR.Item("aj_job_cust_tel_mobile").ToString.Trim <> "" Then ._CusTels += _DR.Item("aj_job_cust_tel_mobile").ToString.Trim + " | "

                    ._RskTels = ""
                    If _DR.Item("aj_job_prop_tel_home").ToString.Trim <> "" Then ._RskTels += _DR.Item("aj_job_prop_tel_home").ToString.Trim + " | "
                    If _DR.Item("aj_job_prop_tel_work").ToString.Trim <> "" Then ._RskTels += _DR.Item("aj_job_prop_tel_work").ToString.Trim + " | "
                    If _DR.Item("aj_job_prop_tel_mobile").ToString.Trim <> "" Then ._RskTels += _DR.Item("aj_job_prop_tel_mobile").ToString.Trim + " | "

                    ._KeySurvey = ValueHandler.ConvertDate(_DR.Item("aj_job_kdt_survey"))
                    ._KeyEstsub = ValueHandler.ConvertDate(_DR.Item("aj_job_kdt_estsub"))
                    ._KeyWork = ValueHandler.ConvertDate(_DR.Item("aj_job_kdt_work"))
                    ._KeyApproval = ValueHandler.ConvertDate(_DR.Item("aj_job_kdt_approval"))
                    ._KeyPaid = ValueHandler.ConvertDate(_DR.Item("aj_job_kdt_paid"))
                    ._KeyWorksorder = ValueHandler.ConvertDate(_DR.Item("aj_job_kdt_worksorder"))
                    ._KeyCcSent = ValueHandler.ConvertDate(_DR.Item("aj_job_kdt_cc_sent"))
                    ._KeyCcRcd = ValueHandler.ConvertDate(_DR.Item("aj_job_kdt_cc_recd"))

                    ._Dc = ValueHandler.ConvertBoolean(_DR.Item("aj_job_dc"))
                    ._DcAskUser = _DR.Item("aj_job_dc_ask_user").ToString
                    ._DcAskStamp = ValueHandler.ConvertDate(_DR.Item("aj_job_dc_ask_stamp"))
                    ._DcRecd = _DR.Item("aj_job_dc_recd").ToString
                    ._DcRecdUser = _DR.Item("aj_job_dc_recd_user").ToString
                    ._DcRecdStamp = ValueHandler.ConvertDate(_DR.Item("aj_job_dc_recd_stamp"))

                    ._MupMat = ValueHandler.ConvertDecimal(_DR.Item("aj_job_oncost_mat"))
                    ._MupLab = ValueHandler.ConvertDecimal(_DR.Item("aj_job_oncost_lab"))
                    ._MupSub = ValueHandler.ConvertDecimal(_DR.Item("aj_job_oncost_sub"))
                    ._MupLeak = ValueHandler.ConvertDecimal(_DR.Item("aj_job_oncost_leak"))

                    ._Hold = ValueHandler.ConvertBoolean(_DR.Item("aj_job_hold"))
                    ._HoldDate = ValueHandler.ConvertDate(_DR.Item("aj_job_hold_date"))
                    ._HoldReason = _DR.Item("aj_job_hold_reas").ToString

                    ._Hide = ValueHandler.ConvertBoolean(_DR.Item("aj_job_hide"))

                    ._Notes = _DR.Item("aj_job_notes").ToString
                    ._JobFolder = "\\aj-svr-01\Data\Job Files\" + ._JobNo.ToString

                    Dim _Contacts As New List(Of Business.Contact)
                    ImportContacts(_ID, ._RskFullname, _DR, _Contacts)

                    ImportActivity(_ID, ._JobNo)
                    ImportFeedback(_ID, ._JobNo)
                    ImportPurchaseInvoices(_ID, ._JobNo)
                    ImportLinks(_ID, ._JobNo)
                    ImportNotes(_ID, ._JobNo)
                    ImportReminders(_ID, ._JobNo)
                    ImportSalesInvoices(_ID, ._JobNo)
                    ImportTimesheets(_ID, ._JobNo)
                    ImportVersions(_ID, ._JobNo)

                    .GenerateTelephoneNumbers(_Contacts)

                    .Store()

                End With

                Session.StepProgressBar()

            Next

        End If

        CareMessage("DONE!")

    End Sub

    Private Sub ImportContacts(ByVal JobID As Guid, ByVal RiskName As String, ByRef _DR As DataRow, ByRef Contacts As List(Of Business.Contact))

        For _i As Integer = 1 To 9

            Dim _NameField As String = "aj_job_cont_" + _i.ToString + "_name"
            Dim _NumberField As String = "aj_job_cont_" + _i.ToString + "_no"
            Dim _NotesField As String = "aj_job_cont_" + _i.ToString + "_notes"

            Dim _NameValue As String = _DR.Item(_NameField).ToString.Trim
            Dim _NumberValue As String = _DR.Item(_NumberField).ToString.Trim
            Dim _NotesValue As String = _DR.Item(_NotesField).ToString.Trim

            If _NameValue = "" AndAlso _NumberValue = "" AndAlso _NotesValue = "" Then
                Exit For
            End If

            Dim _Name As String = _NameValue
            Dim _NumberType As String = ""
            Dim _Relationship As String = ""
            Dim _GenericName As Boolean = False
            Dim _Email As String = ""

            'check if we have an email address
            If _NameValue.Contains("@") OrElse _NumberValue.Contains("@") OrElse _NotesValue.Contains("@") Then
                If _NameValue.Contains("@") Then _Email = _NameValue
                If _NumberValue.Contains("@") Then _Email = _NumberValue
                If _NotesValue.Contains("@") Then _Email = _NotesValue
                If _NameValue.ToUpper = "EMAIL" OrElse _NameValue.ToUpper = "E-MAIL" Then
                    _Name = ""
                    _GenericName = True
                End If
            End If

            'extract the number type
            If _NameValue.ToUpper = "HOME" OrElse _Name.ToUpper = "PH HOME" Then
                _NumberType = "Home"
                _GenericName = True
            End If

            If _NameValue.ToUpper = "WORK" OrElse _Name.ToUpper = "PH WORK" Then
                _NumberType = "Work"
                _GenericName = True
            End If

            If _NameValue.ToUpper = "MOBILE" OrElse _NameValue.ToUpper = "MOB" OrElse _NameValue.ToUpper = "PH MOB" Then
                _NumberType = "Mobile"
                _GenericName = True
            End If

            If _NotesValue.ToUpper = "HOME" Then _NumberType = "Home"
            If _NotesValue.ToUpper = "PH HOME" Then _NumberType = "Home"
            If _NotesValue.ToUpper = "WORK" Then _NumberType = "Work"
            If _NotesValue.ToUpper = "PH MOBILE" Then _NumberType = "Mobile"
            If _NotesValue.ToUpper = "MOBILE" Then _NumberType = "Mobile"
            If _NotesValue.ToUpper = "PH MOB" Then _NumberType = "Mobile"
            If _NotesValue.ToUpper = "MOB" Then _NumberType = "Mobile"


            'if we cannot find the number type in the text, we try working it out from the number
            If _NumberValue <> "" Then
                If _NumberValue.StartsWith("07") Then
                    _NumberType = "Mobile"
                Else
                    If _NumberValue.StartsWith("01") Or _NumberValue.StartsWith("02") Or _NumberValue.StartsWith("03") Or _NumberValue.StartsWith("08") Then
                        _NumberType = "Home"
                    End If
                End If
            End If

            'if we get here, and we don't know what type of information we have, we just pop it in the notes field
            If _NumberType = "" AndAlso _Email = "" Then
                'discard, duff data
            Else

                'get relationship
                If _NameValue.ToUpper = "CUSTOMER" Then
                    _Relationship = "Customer"
                    _GenericName = True
                End If

                If _NameValue.ToUpper = "PH" OrElse _NameValue.ToUpper = "PH MOB" Then
                    _Relationship = "Policy Holder"
                    _GenericName = True
                End If

                If _NameValue.ToUpper = "RISK" Then
                    _Relationship = "Risk"
                    _GenericName = True
                End If

                If _NotesValue.ToUpper = "CUSTOMER" Then _Relationship = "Customer"
                If _NotesValue.ToUpper = "PH" Then _Relationship = "Policy Holder"
                If _NotesValue.ToUpper = "RISK" Then _Relationship = "Risk"

                'risk name is defaulted if we have nothing to go by
                If _GenericName OrElse _Name = "" Then
                    _Name = RiskName + " ?"
                Else
                    _Name = _NameValue
                End If

                If _Name = "" Then
                    'Stop
                Else
                    If (_NumberValue <> "" AndAlso _NumberType <> "") OrElse _Email <> "" Then
                        Contacts.Add(CreateContact(JobID, _Name, _NumberValue, _NumberType, _Relationship, _Email))
                    Else
                        'discard, duff data
                    End If
                End If

            End If

        Next

    End Sub

    Private Function CreateContact(ByVal JobID As Guid, ByVal Name As String, ByVal Value As String, ByVal ValueType As String, ByVal Relationship As String, ByVal Email As String) As Business.Contact

        Dim _ContactID As Guid = Guid.NewGuid

        Dim _C As New Business.Contact
        With _C

            ._ID = _ContactID
            ._Fullname = Name
            ._Relationship = Relationship
            ._Email = Email

            If ValueType = "Home" Then ._TelHome = Value.Trim
            If ValueType = "Work" Then ._TelWork = Value.Trim
            If ValueType = "Mobile" Then ._TelMobile = Value.Trim

            If ._TelHome <> "" Then
                ._TelAll += "H: " + ._TelHome
            End If

            If ._TelMobile <> "" Then
                If ._TelAll = "" Then
                    ._TelAll += "M: " + ._TelMobile
                Else
                    ._TelAll += " M: " + ._TelMobile
                End If
            End If

            If ._TelWork <> "" Then
                If ._TelAll = "" Then
                    ._TelAll += "W: " + ._TelWork
                Else
                    ._TelAll += " W: " + ._TelWork
                End If
            End If

            If ._TelOther <> "" Then
                If ._TelAll = "" Then
                    ._TelAll += "O: " + ._TelWork
                Else
                    ._TelAll += " O: " + ._TelWork
                End If
            End If

            .Store()

        End With

        Dim _JC As New Business.JobContact
        With _JC
            ._JobId = JobID
            ._ContactId = _ContactID
            .Store()
        End With

        Return _C

    End Function

    Private Function BuildAddress(ByVal SeparateLines As Boolean, ByVal Add1 As String, ByVal Add2 As String, ByVal Add3 As String, ByVal Add4 As String, ByVal PostCode As String) As String

        Dim _Splitter As String = vbCrLf
        If SeparateLines = False Then _Splitter = ", "

        Dim _Return As String = ""

        If Add1.Trim <> "" Then _Return += Add1.Trim

        If Add2.Trim <> "" Then
            If _Return <> "" Then _Return += _Splitter
            _Return += Add2.Trim
        End If

        If Add3.Trim <> "" Then
            If _Return <> "" Then _Return += _Splitter
            _Return += Add3.Trim
        End If

        If Add4.Trim <> "" Then
            If _Return <> "" Then _Return += _Splitter
            _Return += Add4.Trim
        End If

        If PostCode.Trim <> "" Then
            If _Return <> "" Then _Return += _Splitter
            _Return += PostCode.Trim
        End If

        Return _Return

    End Function

    Private Sub ImportActivity(ByVal JobID As Guid, ByVal JobNo As Integer)

    End Sub

    Private Sub ImportFeedback(ByVal JobID As Guid, ByVal JobNo As Integer)

    End Sub

    Private Sub ImportPurchaseInvoices(ByVal JobID As Guid, ByVal JobNo As Integer)

        Dim _SQL As String = ""
        _SQL = "select * from ajjobinv where aj_inv_job = " + JobNo.ToString

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(m_JC, _SQL)
        If Not _DT Is Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _Inv As New Business.PurchaseDocument
                With _Inv
                    ._TargetType = "Job"
                    ._TargetId = JobID
                    ._InvDate = ValueHandler.ConvertDate(_DR.Item("aj_inv_date"))
                    ._InvType = _DR.Item("aj_inv_type").ToString
                    ._InvEnteredDate = ValueHandler.ConvertDate(_DR.Item("aj_inv_date_entered"))
                    ._InvCategory = _DR.Item("aj_inv_cat_desc").ToString
                    ._InvSuppName = _DR.Item("aj_inv_supp_name").ToString
                    ._InvDesc = _DR.Item("aj_inv_desc").ToString
                    ._InvValue = ValueHandler.ConvertDecimal(_DR.Item("aj_inv_value"))
                    ._InvEnteredUser = _DR.Item("aj_inv_user_code").ToString
                    ._InvEnteredUserName = _DR.Item("aj_inv_user_name").ToString
                    ._InvEnteredStamp = ValueHandler.ConvertDate(_DR.Item("aj_inv_stamp"))
                    .Store()
                End With

            Next

        End If

    End Sub

    Private Sub ImportLinks(ByVal JobID As Guid, ByVal JobNo As Integer)

        Dim _SQL As String = ""
        _SQL = "select * from ajjoblnk where aj_lnk_job = " + JobNo.ToString

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(m_JC, _SQL)
        If Not _DT Is Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _L As New Business.JobLink
                With _L
                    ._JobId = JobID
                    ._LinkedJobId = Business.Job.RetreiveIDByJobNo(_DR.Item("aj_lnk_target_job").ToString)
                    .Store()
                End With

            Next

        End If

    End Sub

    Private Sub ImportNotes(ByVal JobID As Guid, ByVal JobNo As Integer)

        Dim _SQL As String = ""
        _SQL = "select * from ajjobnte where aj_nte_job = " + JobNo.ToString

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(m_JC, _SQL)
        If Not _DT Is Nothing Then
            For Each _DR As DataRow In _DT.Rows
                Dim _N As New Care.Shared.Business.AppNote
                With _N
                    ._RecordId = JobID
                    ._Subject = _DR.Item("aj_nte_subject").ToString
                    ._Body = _DR.Item("aj_nte_text").ToString
                    ._Stamp = ValueHandler.ConvertDate(_DR.Item("aj_nte_stamp"))
                    .Store()
                End With
            Next
        End If

    End Sub

    Private Sub ImportReminders(ByVal JobID As Guid, ByVal JobNo As Integer)

    End Sub

    Private Sub ImportSalesInvoices(ByVal JobID As Guid, ByVal JobNo As Integer)

        Dim _SQL As String = ""
        _SQL = "select * from ajjobsag where aj_sag_job = " + JobNo.ToString

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(m_JC, _SQL)
        If Not _DT Is Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _SI As New Business.JobInvSale
                With _SI

                    ._JobId = JobID

                    ._InvDate = ValueHandler.ConvertDate(_DR.Item("aj_sag_date"))
                    ._InvNo = _DR.Item("aj_sag_sage_no").ToString
                    ._InvNet = ValueHandler.ConvertDecimal(_DR.Item("aj_sag_nett"))

                    ._InvNotes = _DR.Item("aj_sag_notes").ToString

                    ._InvUser = _DR.Item("aj_sag_user").ToString
                    ._InvStamp = ValueHandler.ConvertDate(_DR.Item("aj_sag_stamp"))

                    .Store()

                End With

            Next

        End If

    End Sub

    Private Sub ImportTimesheets(ByVal JobID As Guid, ByVal JobNo As Integer)

        Dim _SQL As String = ""
        _SQL = "select * from ajtsfle where aj_ts_job = " + JobNo.ToString

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(m_JC, _SQL)
        If Not _DT Is Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _TS As New Business.Timesheet
                With _TS

                    ._TsDate = ValueHandler.ConvertDate(_DR.Item("aj_ts_date"))

                    Dim _e As Business.Employee = ReturnEmployee(_DR.Item("aj_ts_emp").ToString)
                    If _e IsNot Nothing Then
                        ._TsEmpId = _e._ID
                        ._TsEmpRate = ValueHandler.ConvertDecimal(_DR.Item("aj_ts_emp_rate"))
                    Else
                        ._TsEmpId = Nothing
                        ._TsEmpRate = 0
                    End If

                    ._TsVanId = Nothing
                    ._TsJobId = JobID
                    ._TsWhat = _DR.Item("aj_ts_task_desc").ToString

                    ._TsDayStart = ValueHandler.ConvertDate(_DR.Item("aj_ts_start")).Value.TimeOfDay
                    ._TsDayFinish = ValueHandler.ConvertDate(_DR.Item("aj_ts_finish")).Value.TimeOfDay
                    ._TsDayDuration = ValueHandler.ConvertDecimal(_DR.Item("aj_ts_dur"))

                    ._TsJobStart = Nothing
                    ._TsJobFinish = Nothing
                    ._TsJobDuration = 0

                    ._TsCost = ValueHandler.ConvertDecimal(_DR.Item("aj_ts_cost"))

                    .Store()

                End With

            Next

        End If

    End Sub

    Private Function ReturnEmployee(ByVal EmployeeCode As String) As Business.Employee
        For Each _e In m_Employees
            If _e._Code.ToUpper = EmployeeCode.ToUpper Then
                Return _e
            End If
        Next
        Return Nothing
    End Function

    Private Sub ImportVersions(ByVal JobID As Guid, ByVal JobNo As Integer)

        Dim _SQL As String = ""
        _SQL = "select * from ajjobver where aj_ver_job = " + JobNo.ToString

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(m_JC, _SQL)
        If Not _DT Is Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _Ver As New Business.JobVersion
                With _Ver
                    ._JobId = JobID
                    ._VerDate = ValueHandler.ConvertDate(_DR.Item("aj_ver_date"))
                    ._VerReason = _DR.Item("aj_ver_reas_desc").ToString
                    ._VerValue = ValueHandler.ConvertDecimal(_DR.Item("aj_ver_value"))
                    ._VerUser = _DR.Item("aj_ver_user").ToString
                    ._VerStamp = ValueHandler.ConvertDate(_DR.Item("aj_ver_stamp"))
                    ._VerNotes = _DR.Item("aj_ver_notes").ToString
                    .Store()
                End With

            Next

        End If

    End Sub

    Private Sub CareButton3_Click(sender As Object, e As EventArgs) Handles CareButton3.Click

        If InputBox("Type Run to continue.").ToUpper <> "RUN" Then Exit Sub

        Dim _SQL As String = ""

        _SQL = "delete from Suppliers"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        '************************************************************************************************************************************************

        _SQL = "select * from ajsupfle order by aj_sup_code"

        Dim _DTSups As DataTable = DAL.GetDataTablefromSQL(m_JC, _SQL)
        If Not _DTSups Is Nothing Then

            Session.SetupProgressBar("Importing Suppliers...", _DTSups.Rows.Count)

            For Each _DR As DataRow In _DTSups.Rows

                Dim _s As New Business.Supplier
                With _s
                    ._SupCode = _DR.Item("aj_sup_code").ToString
                    ._SupName = _DR.Item("aj_sup_name").ToString
                    ._SupAddress = BuildAddress(True, _DR.Item("aj_sup_add1").ToString, _DR.Item("aj_sup_add2").ToString, _DR.Item("aj_sup_add3").ToString, _DR.Item("aj_sup_add4").ToString, _DR.Item("aj_sup_pcode").ToString)
                    ._SupPostcode = _DR.Item("aj_sup_pcode").ToString
                    ._SupContact = _DR.Item("aj_sup_contact").ToString
                    ._SupTel = _DR.Item("aj_sup_tel").ToString
                    .Store()
                End With

                Session.StepProgressBar()

            Next

            _DTSups.Dispose()
            _DTSups = Nothing

        End If

    End Sub

    Private Sub CareButton4_Click(sender As Object, e As EventArgs) Handles CareButton4.Click


        If InputBox("Type Run to continue.").ToUpper <> "RUN" Then Exit Sub

        Dim _SQL As String = ""

        _SQL = "delete from Insurers"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        '************************************************************************************************************************************************

        _SQL = "select * from ajinsfle order by aj_ins_code"

        Dim _DTSups As DataTable = DAL.GetDataTablefromSQL(m_JC, _SQL)
        If Not _DTSups Is Nothing Then

            Session.SetupProgressBar("Importing Insurers...", _DTSups.Rows.Count)

            For Each _DR As DataRow In _DTSups.Rows

                Dim _s As New Business.Insurer
                With _s
                    ._InsCode = _DR.Item("aj_ins_code").ToString
                    ._InsName = _DR.Item("aj_ins_name").ToString
                    ._InsThresh = ValueHandler.ConvertDecimal(_DR.Item("aj_ins_thresh"))
                    ._InsLower = ValueHandler.ConvertDecimal(_DR.Item("aj_ins_lower"))
                    ._InsUpper = ValueHandler.ConvertDecimal(_DR.Item("aj_ins_upper"))
                    .Store()
                End With

                Session.StepProgressBar()

            Next

            _DTSups.Dispose()
            _DTSups = Nothing

        End If


    End Sub

    Private Sub CareButton5_Click(sender As Object, e As EventArgs) Handles CareButton5.Click

        If InputBox("Type Run to continue.").ToUpper <> "RUN" Then Exit Sub

        Dim _SQL As String = ""

        _SQL = "delete from Employees"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        '************************************************************************************************************************************************

        _SQL = "select * from ajempfle order by aj_emp_code"

        Dim _DTSups As DataTable = DAL.GetDataTablefromSQL(m_JC, _SQL)
        If Not _DTSups Is Nothing Then

            Session.SetupProgressBar("Importing Employees...", _DTSups.Rows.Count)

            For Each _DR As DataRow In _DTSups.Rows

                Dim _s As New Business.Employee
                With _s
                    ._Code = _DR.Item("aj_emp_code").ToString
                    ._Fullname = _DR.Item("aj_emp_name").ToString
                    ._TradeCode = _DR.Item("aj_emp_trd_code").ToString
                    ._TradeDesc = _DR.Item("aj_emp_trd_desc").ToString
                    ._Rate = ValueHandler.ConvertDecimal(_DR.Item("aj_emp_rate"))
                    ._Active = ValueHandler.ConvertBoolean(_DR.Item("aj_emp_active"))
                    ._Office = ValueHandler.ConvertBoolean(_DR.Item("aj_emp_office"))
                    .Store()
                End With

                Session.StepProgressBar()

            Next

            _DTSups.Dispose()
            _DTSups = Nothing

        End If

    End Sub

    Private Sub CareButton6_Click(sender As Object, e As EventArgs) Handles CareButton6.Click

        Dim _entries As List(Of Business.Timesheet) = Business.Timesheet.RetreiveAll

        Session.SetupProgressBar("Processing", _entries.Count)
        For Each _ts As Business.Timesheet In _entries

            'old records, we have day start and finish, nothing else
            If _ts._TsDayStart.HasValue AndAlso _ts._TsDayFinish.HasValue AndAlso _ts._TsJobFinish.HasValue = False Then
                _ts._TsDayDuration = CalculateHours(_ts._TsDayStart, _ts._TsDayFinish)
            Else
                'new records
                If _ts._TsDayStart.HasValue AndAlso _ts._TsDayFinish.HasValue AndAlso _ts._TsJobFinish.HasValue Then
                    'if we have a start, finish and yard time - its one timesheet for the day or the last timesheet for a given day
                    _ts._TsJobDuration = CalculateHours(_ts._TsDayStart, _ts._TsJobFinish)
                    _ts._TsDayDuration = CalculateHours(_ts._TsDayStart, _ts._TsDayFinish)
                Else

                    If _ts._TsDayStart.HasValue AndAlso _ts._TsDayFinish.HasValue = False AndAlso _ts._TsJobFinish.HasValue Then

                        _ts._TsJobDuration = CalculateHours(_ts._TsDayStart, _ts._TsJobFinish)

                        If _ts._TsCost = 0 AndAlso _ts._TsJobDuration > 0 Then
                            'if we don't have a cost, we calculate it now
                            _ts._TsCost = _ts._TsJobDuration * _ts._TsEmpRate
                        End If

                    End If
                End If

            End If

            _ts.Store()
            Session.StepProgressBar()

        Next

        CareMessage("DONE!")

    End Sub


    Private Function CalculateHours(ByVal StartTime As TimeSpan?, ByVal FinishTime As TimeSpan?) As Decimal
        Dim _Mins As Double = CalculateMins(StartTime, FinishTime)
        If _Mins = 0 Then
            Return 0
        Else
            Return CDec(_Mins / 60)
        End If
    End Function

    Private Function CalculateMins(ByVal StartTime As TimeSpan?, ByVal FinishTime As TimeSpan?) As Double
        If Not StartTime.HasValue OrElse Not FinishTime.HasValue Then Return 0
        Dim _Diff As TimeSpan = FinishTime.Value - StartTime.Value
        Return _Diff.TotalMinutes
    End Function

    Private Sub CareButton7_Click(sender As Object, e As EventArgs) Handles CareButton7.Click

        If InputBox("Type Run to continue.").ToUpper <> "RUN" Then Exit Sub

        Dim _DummyID As New Guid("D7D99512-DC85-4AFE-9576-B3B1427F3C92")

        Dim _SQL As String = ""
        _SQL += "select doc_id from PurchaseDocuments d"
        _SQL += " where d.doc_id is not null"
        _SQL += " group by d.doc_id"
        _SQL += " having count(*) > 1"
        _SQL += " order by count(*) desc"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If Not _DT Is Nothing Then

            Session.SetupProgressBar("Processing Duplicates...", _DT.Rows.Count)

            For Each _DR As DataRow In _DT.Rows

                'check if the duplicates go across more than one job

                _SQL = ""
                _SQL += "select target_id, inv_value from PurchaseDocuments d"
                _SQL += " where d.doc_id = '" + _DR.Item("doc_id").ToString + "'"
                _SQL += " group by target_id, inv_value"

                Dim _DTDups As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
                If Not _DTDups Is Nothing Then

                    If _DTDups.Rows.Count = 1 Then

                        'only one job, so we can retarget them all and save "where they were"

                        _SQL = ""
                        _SQL += "select id from PurchaseDocuments d"
                        _SQL += " where doc_id = '" + _DR.Item("doc_id").ToString + "'"
                        _SQL += " order by inv_entered_stamp"

                        Dim _DTDocs As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
                        If Not _DTDocs Is Nothing Then

                            For _i = 1 To _DTDocs.Rows.Count - 1

                                Dim _id As New Guid(_DTDocs.Rows(_i).Item(0).ToString)
                                Dim _d As Business.PurchaseDocument = Business.PurchaseDocument.RetreiveByID(_id)
                                _d._InvDesc = _d._ID.ToString
                                _d._TargetId = _DummyID
                                _d.Store()

                                _i += 1

                            Next

                        End If

                    End If

                End If

                Session.StepProgressBar()

            Next

            _DT.Dispose()
            _DT = Nothing

        End If

    End Sub
End Class