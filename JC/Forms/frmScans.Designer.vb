﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmScans
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmScans))
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.gbxHeader = New DevExpress.XtraEditors.GroupControl()
        Me.radAnyStatus = New Care.Controls.CareRadioButton(Me.components)
        Me.radPaid = New Care.Controls.CareRadioButton(Me.components)
        Me.radQAwaitingAuth = New Care.Controls.CareRadioButton(Me.components)
        Me.radQAuthorised = New Care.Controls.CareRadioButton(Me.components)
        Me.radQMatched = New Care.Controls.CareRadioButton(Me.components)
        Me.radQueried = New Care.Controls.CareRadioButton(Me.components)
        Me.radQDuplicates = New Care.Controls.CareRadioButton(Me.components)
        Me.radQScanned = New Care.Controls.CareRadioButton(Me.components)
        Me.cbxSupplier = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.txtNet = New Care.Controls.CareTextBox(Me.components)
        Me.cbxWorkType = New Care.Controls.CareComboBox(Me.components)
        Me.txtRef1 = New Care.Controls.CareTextBox(Me.components)
        Me.txtName = New Care.Controls.CareTextBox(Me.components)
        Me.cbxSendIntray = New Care.Controls.CareComboBox(Me.components)
        Me.btnSearch = New Care.Controls.CareButton(Me.components)
        Me.txtRef = New Care.Controls.CareTextBox(Me.components)
        Me.cbxCategory = New Care.Controls.CareComboBox(Me.components)
        Me.radMatchPlant = New Care.Controls.CareRadioButton(Me.components)
        Me.radMatchCategory = New Care.Controls.CareRadioButton(Me.components)
        Me.radMatchJob = New Care.Controls.CareRadioButton(Me.components)
        Me.gbxMatch = New DevExpress.XtraEditors.GroupControl()
        Me.radLink = New Care.Controls.CareRadioButton(Me.components)
        Me.btnDuplicate = New Care.Controls.CareButton(Me.components)
        Me.btnDelete = New Care.Controls.CareButton(Me.components)
        Me.btnQuery = New Care.Controls.CareButton(Me.components)
        Me.gbxDetails = New DevExpress.XtraEditors.GroupControl()
        Me.btnSplit = New Care.Controls.CareButton(Me.components)
        Me.cbxPaymentMethod = New Care.Controls.CareComboBox(Me.components)
        Me.txtDocRef = New Care.Controls.CareTextBox(Me.components)
        Me.chkFastTrack = New Care.Controls.CareCheckBox(Me.components)
        Me.chkPrice = New Care.Controls.CareCheckBox(Me.components)
        Me.chkUsedJob = New Care.Controls.CareCheckBox(Me.components)
        Me.chkPreNeg = New Care.Controls.CareCheckBox(Me.components)
        Me.chkDN = New Care.Controls.CareCheckBox(Me.components)
        Me.cdtDocDate = New Care.Controls.CareDateTime(Me.components)
        Me.cbxDocType = New Care.Controls.CareComboBox(Me.components)
        Me.btnUpdate = New Care.Controls.CareButton(Me.components)
        Me.btnSend = New Care.Controls.CareButton(Me.components)
        Me.gbxButtons = New DevExpress.XtraEditors.GroupControl()
        Me.btnPrint = New Care.Controls.CareButton(Me.components)
        Me.btnReturnToController = New Care.Controls.CareButton(Me.components)
        Me.btnKeith = New Care.Controls.CareButton(Me.components)
        Me.btnPassAuth = New Care.Controls.CareButton(Me.components)
        Me.btnAuthorise = New Care.Controls.CareButton(Me.components)
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.chkTop20 = New Care.Controls.CareCheckBox(Me.components)
        Me.radSpecificTray = New Care.Controls.CareRadioButton(Me.components)
        Me.cbxSelectIntray = New Care.Controls.CareComboBox(Me.components)
        Me.radAllTrays = New Care.Controls.CareRadioButton(Me.components)
        Me.radMyIntray = New Care.Controls.CareRadioButton(Me.components)
        Me.radCompanyTray = New Care.Controls.CareRadioButton(Me.components)
        Me.ctView = New Care.Controls.CareTab(Me.components)
        Me.tabViewImage = New DevExpress.XtraTab.XtraTabPage()
        Me.sccVertical = New DevExpress.XtraEditors.SplitContainerControl()
        Me.cgDocuments = New Care.Controls.CareGrid()
        Me.sccNotes = New DevExpress.XtraEditors.SplitContainerControl()
        Me.sccPictureNotes = New DevExpress.XtraEditors.SplitContainerControl()
        Me.pic = New DevExpress.XtraEditors.PictureEdit()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.btnPaid = New Care.Controls.CareButton(Me.components)
        Me.btnGeneratePriceNote = New Care.Controls.CareButton(Me.components)
        Me.CareLabel18 = New Care.Controls.CareLabel(Me.components)
        Me.txt9 = New Care.Controls.CareTextBox(Me.components)
        Me.cbx9 = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel19 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel20 = New Care.Controls.CareLabel(Me.components)
        Me.txt12 = New Care.Controls.CareTextBox(Me.components)
        Me.cbx12 = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel21 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel22 = New Care.Controls.CareLabel(Me.components)
        Me.txt11 = New Care.Controls.CareTextBox(Me.components)
        Me.cbx11 = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel23 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel24 = New Care.Controls.CareLabel(Me.components)
        Me.txt10 = New Care.Controls.CareTextBox(Me.components)
        Me.cbx10 = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel25 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel10 = New Care.Controls.CareLabel(Me.components)
        Me.txt5 = New Care.Controls.CareTextBox(Me.components)
        Me.cbx5 = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel12 = New Care.Controls.CareLabel(Me.components)
        Me.txt8 = New Care.Controls.CareTextBox(Me.components)
        Me.cbx8 = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel13 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.txt7 = New Care.Controls.CareTextBox(Me.components)
        Me.cbx7 = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel15 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel16 = New Care.Controls.CareLabel(Me.components)
        Me.txt6 = New Care.Controls.CareTextBox(Me.components)
        Me.cbx6 = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel17 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.txt1 = New Care.Controls.CareTextBox(Me.components)
        Me.cbx1 = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.txt4 = New Care.Controls.CareTextBox(Me.components)
        Me.cbx4 = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.txt3 = New Care.Controls.CareTextBox(Me.components)
        Me.cbx3 = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.txt2 = New Care.Controls.CareTextBox(Me.components)
        Me.cbx2 = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.btnNoteAdd = New Care.Controls.CareButton(Me.components)
        Me.btnNoteSave = New Care.Controls.CareButton(Me.components)
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.btnNoteDelete = New Care.Controls.CareButton(Me.components)
        Me.cgComments = New Care.Controls.CareGrid()
        Me.txtComments = New DevExpress.XtraEditors.MemoEdit()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.btn25 = New Care.Controls.CareButton(Me.components)
        Me.lblWho = New Care.Controls.CareLabel(Me.components)
        Me.btn60 = New Care.Controls.CareButton(Me.components)
        Me.btn80 = New Care.Controls.CareButton(Me.components)
        Me.btn40 = New Care.Controls.CareButton(Me.components)
        Me.btn100 = New Care.Controls.CareButton(Me.components)
        Me.btnRight = New Care.Controls.CareButton(Me.components)
        Me.btnLeft = New Care.Controls.CareButton(Me.components)
        Me.tabViewDetail = New DevExpress.XtraTab.XtraTabPage()
        Me.cgDetail = New Care.Controls.CareGrid()
        CType(Me.gbxHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxHeader.SuspendLayout()
        CType(Me.radAnyStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radPaid.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radQAwaitingAuth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radQAuthorised.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radQMatched.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radQueried.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radQDuplicates.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radQScanned.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSupplier.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNet.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxWorkType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRef1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSendIntray.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRef.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxCategory.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radMatchPlant.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radMatchCategory.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radMatchJob.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxMatch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxMatch.SuspendLayout()
        CType(Me.radLink.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxDetails.SuspendLayout()
        CType(Me.cbxPaymentMethod.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDocRef.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkFastTrack.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkPrice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkUsedJob.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkPreNeg.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkDN.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDocDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDocDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxDocType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxButtons, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxButtons.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.chkTop20.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radSpecificTray.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSelectIntray.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radAllTrays.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radMyIntray.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radCompanyTray.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ctView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ctView.SuspendLayout()
        Me.tabViewImage.SuspendLayout()
        CType(Me.sccVertical, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.sccVertical.SuspendLayout()
        CType(Me.sccNotes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.sccNotes.SuspendLayout()
        CType(Me.sccPictureNotes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.sccPictureNotes.SuspendLayout()
        CType(Me.pic.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.txt9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbx9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt12.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbx12.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt11.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbx11.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbx10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbx5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbx8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbx7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbx6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbx1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbx4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbx3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbx2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.txtComments.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        Me.tabViewDetail.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'gbxHeader
        '
        Me.gbxHeader.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxHeader.Controls.Add(Me.radAnyStatus)
        Me.gbxHeader.Controls.Add(Me.radPaid)
        Me.gbxHeader.Controls.Add(Me.radQAwaitingAuth)
        Me.gbxHeader.Controls.Add(Me.radQAuthorised)
        Me.gbxHeader.Controls.Add(Me.radQMatched)
        Me.gbxHeader.Controls.Add(Me.radQueried)
        Me.gbxHeader.Controls.Add(Me.radQDuplicates)
        Me.gbxHeader.Controls.Add(Me.radQScanned)
        Me.gbxHeader.Location = New System.Drawing.Point(575, 12)
        Me.gbxHeader.Name = "gbxHeader"
        Me.gbxHeader.ShowCaption = False
        Me.gbxHeader.Size = New System.Drawing.Size(597, 37)
        Me.gbxHeader.TabIndex = 1
        '
        'radAnyStatus
        '
        Me.radAnyStatus.Location = New System.Drawing.Point(514, 8)
        Me.radAnyStatus.Name = "radAnyStatus"
        Me.radAnyStatus.Properties.AutoWidth = True
        Me.radAnyStatus.Properties.Caption = "Any Status"
        Me.radAnyStatus.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radAnyStatus.Properties.RadioGroupIndex = 0
        Me.radAnyStatus.Size = New System.Drawing.Size(75, 19)
        Me.radAnyStatus.TabIndex = 7
        Me.radAnyStatus.TabStop = False
        '
        'radPaid
        '
        Me.radPaid.Location = New System.Drawing.Point(466, 8)
        Me.radPaid.Name = "radPaid"
        Me.radPaid.Properties.AutoWidth = True
        Me.radPaid.Properties.Caption = "Paid"
        Me.radPaid.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radPaid.Properties.RadioGroupIndex = 0
        Me.radPaid.Size = New System.Drawing.Size(42, 19)
        Me.radPaid.TabIndex = 6
        Me.radPaid.TabStop = False
        '
        'radQAwaitingAuth
        '
        Me.radQAwaitingAuth.Location = New System.Drawing.Point(287, 8)
        Me.radQAwaitingAuth.Name = "radQAwaitingAuth"
        Me.radQAwaitingAuth.Properties.AutoWidth = True
        Me.radQAwaitingAuth.Properties.Caption = "Awaiting Auth."
        Me.radQAwaitingAuth.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radQAwaitingAuth.Properties.RadioGroupIndex = 0
        Me.radQAwaitingAuth.Size = New System.Drawing.Size(93, 19)
        Me.radQAwaitingAuth.TabIndex = 4
        Me.radQAwaitingAuth.TabStop = False
        '
        'radQAuthorised
        '
        Me.radQAuthorised.Location = New System.Drawing.Point(386, 8)
        Me.radQAuthorised.Name = "radQAuthorised"
        Me.radQAuthorised.Properties.AutoWidth = True
        Me.radQAuthorised.Properties.Caption = "Authorised"
        Me.radQAuthorised.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radQAuthorised.Properties.RadioGroupIndex = 0
        Me.radQAuthorised.Size = New System.Drawing.Size(74, 19)
        Me.radQAuthorised.TabIndex = 5
        Me.radQAuthorised.TabStop = False
        '
        'radQMatched
        '
        Me.radQMatched.Location = New System.Drawing.Point(75, 8)
        Me.radQMatched.Name = "radQMatched"
        Me.radQMatched.Properties.AutoWidth = True
        Me.radQMatched.Properties.Caption = "Matched"
        Me.radQMatched.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radQMatched.Properties.RadioGroupIndex = 0
        Me.radQMatched.Size = New System.Drawing.Size(63, 19)
        Me.radQMatched.TabIndex = 1
        Me.radQMatched.TabStop = False
        '
        'radQueried
        '
        Me.radQueried.Location = New System.Drawing.Point(221, 8)
        Me.radQueried.Name = "radQueried"
        Me.radQueried.Properties.AutoWidth = True
        Me.radQueried.Properties.Caption = "Queried"
        Me.radQueried.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radQueried.Properties.RadioGroupIndex = 0
        Me.radQueried.Size = New System.Drawing.Size(60, 19)
        Me.radQueried.TabIndex = 3
        Me.radQueried.TabStop = False
        '
        'radQDuplicates
        '
        Me.radQDuplicates.Location = New System.Drawing.Point(144, 8)
        Me.radQDuplicates.Name = "radQDuplicates"
        Me.radQDuplicates.Properties.AutoWidth = True
        Me.radQDuplicates.Properties.Caption = "Duplicates"
        Me.radQDuplicates.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radQDuplicates.Properties.RadioGroupIndex = 0
        Me.radQDuplicates.Size = New System.Drawing.Size(71, 19)
        Me.radQDuplicates.TabIndex = 2
        Me.radQDuplicates.TabStop = False
        '
        'radQScanned
        '
        Me.radQScanned.EditValue = True
        Me.radQScanned.Location = New System.Drawing.Point(6, 8)
        Me.radQScanned.Name = "radQScanned"
        Me.radQScanned.Properties.AutoWidth = True
        Me.radQScanned.Properties.Caption = "Scanned"
        Me.radQScanned.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radQScanned.Properties.RadioGroupIndex = 0
        Me.radQScanned.Size = New System.Drawing.Size(63, 19)
        Me.radQScanned.TabIndex = 0
        '
        'cbxSupplier
        '
        Me.cbxSupplier.AllowBlank = False
        Me.cbxSupplier.DataSource = Nothing
        Me.cbxSupplier.DisplayMember = Nothing
        Me.cbxSupplier.EnterMoveNextControl = True
        Me.cbxSupplier.Location = New System.Drawing.Point(5, 30)
        Me.cbxSupplier.Name = "cbxSupplier"
        Me.cbxSupplier.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSupplier.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxSupplier.Properties.Appearance.Options.UseFont = True
        Me.cbxSupplier.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSupplier.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSupplier.SelectedValue = Nothing
        Me.cbxSupplier.Size = New System.Drawing.Size(200, 22)
        Me.cbxSupplier.TabIndex = 2
        Me.cbxSupplier.ValueMember = Nothing
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.Options.UseFont = True
        Me.CareLabel2.Appearance.Options.UseTextOptions = True
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(103, 133)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(19, 15)
        Me.CareLabel2.TabIndex = 7
        Me.CareLabel2.Text = "Net"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNet
        '
        Me.txtNet.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNet.EnterMoveNextControl = True
        Me.txtNet.Location = New System.Drawing.Point(128, 130)
        Me.txtNet.MaxLength = 14
        Me.txtNet.Name = "txtNet"
        Me.txtNet.NumericAllowNegatives = True
        Me.txtNet.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtNet.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtNet.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtNet.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtNet.Properties.Appearance.Options.UseFont = True
        Me.txtNet.Properties.Appearance.Options.UseTextOptions = True
        Me.txtNet.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtNet.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNet.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txtNet.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtNet.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtNet.Properties.MaxLength = 14
        Me.txtNet.Size = New System.Drawing.Size(77, 22)
        Me.txtNet.TabIndex = 8
        Me.txtNet.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNet.ToolTipText = ""
        '
        'cbxWorkType
        '
        Me.cbxWorkType.AllowBlank = False
        Me.cbxWorkType.DataSource = Nothing
        Me.cbxWorkType.DisplayMember = Nothing
        Me.cbxWorkType.EnterMoveNextControl = True
        Me.cbxWorkType.Location = New System.Drawing.Point(5, 55)
        Me.cbxWorkType.Name = "cbxWorkType"
        Me.cbxWorkType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxWorkType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxWorkType.Properties.Appearance.Options.UseFont = True
        Me.cbxWorkType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxWorkType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxWorkType.SelectedValue = Nothing
        Me.cbxWorkType.Size = New System.Drawing.Size(200, 22)
        Me.cbxWorkType.TabIndex = 3
        Me.cbxWorkType.ValueMember = Nothing
        '
        'txtRef1
        '
        Me.txtRef1.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtRef1.EnterMoveNextControl = True
        Me.txtRef1.Location = New System.Drawing.Point(5, 112)
        Me.txtRef1.MaxLength = 0
        Me.txtRef1.Name = "txtRef1"
        Me.txtRef1.NumericAllowNegatives = False
        Me.txtRef1.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRef1.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRef1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRef1.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRef1.Properties.Appearance.Options.UseFont = True
        Me.txtRef1.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRef1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRef1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRef1.Properties.ReadOnly = True
        Me.txtRef1.Properties.Tag = "R"
        Me.txtRef1.Size = New System.Drawing.Size(200, 22)
        Me.txtRef1.TabIndex = 7
        Me.txtRef1.TabStop = False
        Me.txtRef1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRef1.ToolTipText = ""
        '
        'txtName
        '
        Me.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(5, 84)
        Me.txtName.MaxLength = 0
        Me.txtName.Name = "txtName"
        Me.txtName.NumericAllowNegatives = False
        Me.txtName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtName.Properties.ReadOnly = True
        Me.txtName.Properties.Tag = "R"
        Me.txtName.Size = New System.Drawing.Size(200, 22)
        Me.txtName.TabIndex = 6
        Me.txtName.TabStop = False
        Me.txtName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'cbxSendIntray
        '
        Me.cbxSendIntray.AllowBlank = False
        Me.cbxSendIntray.DataSource = Nothing
        Me.cbxSendIntray.DisplayMember = Nothing
        Me.cbxSendIntray.EnterMoveNextControl = True
        Me.cbxSendIntray.Location = New System.Drawing.Point(5, 7)
        Me.cbxSendIntray.Name = "cbxSendIntray"
        Me.cbxSendIntray.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSendIntray.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxSendIntray.Properties.Appearance.Options.UseFont = True
        Me.cbxSendIntray.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSendIntray.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSendIntray.SelectedValue = Nothing
        Me.cbxSendIntray.Size = New System.Drawing.Size(162, 22)
        Me.cbxSendIntray.TabIndex = 0
        Me.cbxSendIntray.ValueMember = Nothing
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(87, 55)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(75, 23)
        Me.btnSearch.TabIndex = 4
        Me.btnSearch.TabStop = False
        Me.btnSearch.Text = "Search"
        '
        'txtRef
        '
        Me.txtRef.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRef.EnterMoveNextControl = True
        Me.txtRef.Location = New System.Drawing.Point(5, 56)
        Me.txtRef.MaxLength = 0
        Me.txtRef.Name = "txtRef"
        Me.txtRef.NumericAllowNegatives = False
        Me.txtRef.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRef.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRef.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRef.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRef.Properties.Appearance.Options.UseFont = True
        Me.txtRef.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRef.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRef.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRef.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRef.Size = New System.Drawing.Size(76, 22)
        Me.txtRef.TabIndex = 3
        Me.txtRef.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRef.ToolTipText = ""
        '
        'cbxCategory
        '
        Me.cbxCategory.AllowBlank = False
        Me.cbxCategory.DataSource = Nothing
        Me.cbxCategory.DisplayMember = Nothing
        Me.cbxCategory.EnterMoveNextControl = True
        Me.cbxCategory.Location = New System.Drawing.Point(5, 56)
        Me.cbxCategory.Name = "cbxCategory"
        Me.cbxCategory.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxCategory.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxCategory.Properties.Appearance.Options.UseFont = True
        Me.cbxCategory.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxCategory.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxCategory.SelectedValue = Nothing
        Me.cbxCategory.Size = New System.Drawing.Size(200, 22)
        Me.cbxCategory.TabIndex = 5
        Me.cbxCategory.ValueMember = Nothing
        '
        'radMatchPlant
        '
        Me.radMatchPlant.Location = New System.Drawing.Point(95, 5)
        Me.radMatchPlant.Name = "radMatchPlant"
        Me.radMatchPlant.Properties.AutoWidth = True
        Me.radMatchPlant.Properties.Caption = "Match to Plant"
        Me.radMatchPlant.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radMatchPlant.Properties.RadioGroupIndex = 0
        Me.radMatchPlant.Size = New System.Drawing.Size(91, 19)
        Me.radMatchPlant.TabIndex = 1
        Me.radMatchPlant.TabStop = False
        '
        'radMatchCategory
        '
        Me.radMatchCategory.Location = New System.Drawing.Point(5, 31)
        Me.radMatchCategory.Name = "radMatchCategory"
        Me.radMatchCategory.Properties.AutoWidth = True
        Me.radMatchCategory.Properties.Caption = "Match to Category"
        Me.radMatchCategory.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radMatchCategory.Properties.RadioGroupIndex = 0
        Me.radMatchCategory.Size = New System.Drawing.Size(112, 19)
        Me.radMatchCategory.TabIndex = 2
        Me.radMatchCategory.TabStop = False
        '
        'radMatchJob
        '
        Me.radMatchJob.EditValue = True
        Me.radMatchJob.Location = New System.Drawing.Point(5, 5)
        Me.radMatchJob.Name = "radMatchJob"
        Me.radMatchJob.Properties.AutoWidth = True
        Me.radMatchJob.Properties.Caption = "Match to Job"
        Me.radMatchJob.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radMatchJob.Properties.RadioGroupIndex = 0
        Me.radMatchJob.Size = New System.Drawing.Size(84, 19)
        Me.radMatchJob.TabIndex = 0
        '
        'gbxMatch
        '
        Me.gbxMatch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxMatch.Controls.Add(Me.radLink)
        Me.gbxMatch.Controls.Add(Me.btnDuplicate)
        Me.gbxMatch.Controls.Add(Me.btnDelete)
        Me.gbxMatch.Controls.Add(Me.radMatchJob)
        Me.gbxMatch.Controls.Add(Me.txtRef1)
        Me.gbxMatch.Controls.Add(Me.radMatchPlant)
        Me.gbxMatch.Controls.Add(Me.txtName)
        Me.gbxMatch.Controls.Add(Me.radMatchCategory)
        Me.gbxMatch.Controls.Add(Me.btnSearch)
        Me.gbxMatch.Controls.Add(Me.txtRef)
        Me.gbxMatch.Controls.Add(Me.cbxCategory)
        Me.gbxMatch.Location = New System.Drawing.Point(961, 55)
        Me.gbxMatch.Name = "gbxMatch"
        Me.gbxMatch.ShowCaption = False
        Me.gbxMatch.Size = New System.Drawing.Size(210, 168)
        Me.gbxMatch.TabIndex = 2
        '
        'radLink
        '
        Me.radLink.Location = New System.Drawing.Point(123, 31)
        Me.radLink.Name = "radLink"
        Me.radLink.Properties.AutoWidth = True
        Me.radLink.Properties.Caption = "Link"
        Me.radLink.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radLink.Properties.RadioGroupIndex = 0
        Me.radLink.Size = New System.Drawing.Size(40, 19)
        Me.radLink.TabIndex = 11
        Me.radLink.TabStop = False
        '
        'btnDuplicate
        '
        Me.btnDuplicate.Location = New System.Drawing.Point(6, 140)
        Me.btnDuplicate.Name = "btnDuplicate"
        Me.btnDuplicate.Size = New System.Drawing.Size(95, 23)
        Me.btnDuplicate.TabIndex = 9
        Me.btnDuplicate.Text = "Duplicate?"
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(110, 140)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(95, 23)
        Me.btnDelete.TabIndex = 10
        Me.btnDelete.Text = "Delete"
        '
        'btnQuery
        '
        Me.btnQuery.Location = New System.Drawing.Point(224, 6)
        Me.btnQuery.Name = "btnQuery"
        Me.btnQuery.Size = New System.Drawing.Size(45, 23)
        Me.btnQuery.TabIndex = 11
        Me.btnQuery.Text = "Query"
        '
        'gbxDetails
        '
        Me.gbxDetails.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxDetails.Controls.Add(Me.btnSplit)
        Me.gbxDetails.Controls.Add(Me.cbxPaymentMethod)
        Me.gbxDetails.Controls.Add(Me.txtDocRef)
        Me.gbxDetails.Controls.Add(Me.chkFastTrack)
        Me.gbxDetails.Controls.Add(Me.chkPrice)
        Me.gbxDetails.Controls.Add(Me.chkUsedJob)
        Me.gbxDetails.Controls.Add(Me.chkPreNeg)
        Me.gbxDetails.Controls.Add(Me.chkDN)
        Me.gbxDetails.Controls.Add(Me.cdtDocDate)
        Me.gbxDetails.Controls.Add(Me.cbxDocType)
        Me.gbxDetails.Controls.Add(Me.btnUpdate)
        Me.gbxDetails.Controls.Add(Me.cbxSupplier)
        Me.gbxDetails.Controls.Add(Me.cbxWorkType)
        Me.gbxDetails.Controls.Add(Me.txtNet)
        Me.gbxDetails.Controls.Add(Me.CareLabel2)
        Me.gbxDetails.Location = New System.Drawing.Point(962, 229)
        Me.gbxDetails.Name = "gbxDetails"
        Me.gbxDetails.ShowCaption = False
        Me.gbxDetails.Size = New System.Drawing.Size(210, 266)
        Me.gbxDetails.TabIndex = 3
        '
        'btnSplit
        '
        Me.btnSplit.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSplit.Appearance.Options.UseFont = True
        Me.btnSplit.Location = New System.Drawing.Point(5, 130)
        Me.btnSplit.Name = "btnSplit"
        Me.btnSplit.Size = New System.Drawing.Size(95, 22)
        Me.btnSplit.TabIndex = 6
        Me.btnSplit.TabStop = False
        Me.btnSplit.Text = "Split"
        '
        'cbxPaymentMethod
        '
        Me.cbxPaymentMethod.AllowBlank = False
        Me.cbxPaymentMethod.DataSource = Nothing
        Me.cbxPaymentMethod.DisplayMember = Nothing
        Me.cbxPaymentMethod.EnterMoveNextControl = True
        Me.cbxPaymentMethod.Location = New System.Drawing.Point(5, 80)
        Me.cbxPaymentMethod.Name = "cbxPaymentMethod"
        Me.cbxPaymentMethod.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxPaymentMethod.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxPaymentMethod.Properties.Appearance.Options.UseFont = True
        Me.cbxPaymentMethod.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxPaymentMethod.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxPaymentMethod.SelectedValue = Nothing
        Me.cbxPaymentMethod.Size = New System.Drawing.Size(200, 22)
        Me.cbxPaymentMethod.TabIndex = 4
        Me.cbxPaymentMethod.ValueMember = Nothing
        '
        'txtDocRef
        '
        Me.txtDocRef.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDocRef.EnterMoveNextControl = True
        Me.txtDocRef.Location = New System.Drawing.Point(5, 105)
        Me.txtDocRef.MaxLength = 20
        Me.txtDocRef.Name = "txtDocRef"
        Me.txtDocRef.NumericAllowNegatives = False
        Me.txtDocRef.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.None
        Me.txtDocRef.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDocRef.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtDocRef.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtDocRef.Properties.Appearance.Options.UseFont = True
        Me.txtDocRef.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDocRef.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtDocRef.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDocRef.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDocRef.Properties.MaxLength = 20
        Me.txtDocRef.Size = New System.Drawing.Size(200, 22)
        Me.txtDocRef.TabIndex = 5
        Me.txtDocRef.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDocRef.ToolTipText = ""
        '
        'chkFastTrack
        '
        Me.chkFastTrack.EnterMoveNextControl = True
        Me.chkFastTrack.Location = New System.Drawing.Point(9, 160)
        Me.chkFastTrack.Name = "chkFastTrack"
        Me.chkFastTrack.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkFastTrack.Properties.Appearance.Options.UseFont = True
        Me.chkFastTrack.Properties.AutoWidth = True
        Me.chkFastTrack.Properties.Caption = "Fast Track"
        Me.chkFastTrack.Size = New System.Drawing.Size(75, 19)
        Me.chkFastTrack.TabIndex = 9
        '
        'chkPrice
        '
        Me.chkPrice.EnterMoveNextControl = True
        Me.chkPrice.Location = New System.Drawing.Point(9, 187)
        Me.chkPrice.Name = "chkPrice"
        Me.chkPrice.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkPrice.Properties.Appearance.Options.UseFont = True
        Me.chkPrice.Properties.AutoWidth = True
        Me.chkPrice.Properties.Caption = "Price Correct"
        Me.chkPrice.Size = New System.Drawing.Size(90, 19)
        Me.chkPrice.TabIndex = 10
        '
        'chkUsedJob
        '
        Me.chkUsedJob.EnterMoveNextControl = True
        Me.chkUsedJob.Location = New System.Drawing.Point(9, 212)
        Me.chkUsedJob.Name = "chkUsedJob"
        Me.chkUsedJob.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkUsedJob.Properties.Appearance.Options.UseFont = True
        Me.chkUsedJob.Properties.AutoWidth = True
        Me.chkUsedJob.Properties.Caption = "Used for Job"
        Me.chkUsedJob.Size = New System.Drawing.Size(87, 19)
        Me.chkUsedJob.TabIndex = 11
        '
        'chkPreNeg
        '
        Me.chkPreNeg.EnterMoveNextControl = True
        Me.chkPreNeg.Location = New System.Drawing.Point(116, 212)
        Me.chkPreNeg.Name = "chkPreNeg"
        Me.chkPreNeg.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkPreNeg.Properties.Appearance.Options.UseFont = True
        Me.chkPreNeg.Properties.AutoWidth = True
        Me.chkPreNeg.Properties.Caption = "Pre Neg."
        Me.chkPreNeg.Size = New System.Drawing.Size(67, 19)
        Me.chkPreNeg.TabIndex = 13
        '
        'chkDN
        '
        Me.chkDN.EnterMoveNextControl = True
        Me.chkDN.Location = New System.Drawing.Point(116, 187)
        Me.chkDN.Name = "chkDN"
        Me.chkDN.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkDN.Properties.Appearance.Options.UseFont = True
        Me.chkDN.Properties.AutoWidth = True
        Me.chkDN.Properties.Caption = "Delivery Note"
        Me.chkDN.Size = New System.Drawing.Size(93, 19)
        Me.chkDN.TabIndex = 12
        '
        'cdtDocDate
        '
        Me.cdtDocDate.EditValue = Nothing
        Me.cdtDocDate.EnterMoveNextControl = True
        Me.cdtDocDate.Location = New System.Drawing.Point(5, 5)
        Me.cdtDocDate.Name = "cdtDocDate"
        Me.cdtDocDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtDocDate.Properties.Appearance.Options.UseFont = True
        Me.cdtDocDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDocDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDocDate.Size = New System.Drawing.Size(84, 22)
        Me.cdtDocDate.TabIndex = 0
        Me.cdtDocDate.Tag = "AE"
        Me.cdtDocDate.Value = Nothing
        '
        'cbxDocType
        '
        Me.cbxDocType.AllowBlank = False
        Me.cbxDocType.DataSource = Nothing
        Me.cbxDocType.DisplayMember = Nothing
        Me.cbxDocType.EnterMoveNextControl = True
        Me.cbxDocType.Location = New System.Drawing.Point(95, 5)
        Me.cbxDocType.Name = "cbxDocType"
        Me.cbxDocType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxDocType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxDocType.Properties.Appearance.Options.UseFont = True
        Me.cbxDocType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxDocType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxDocType.SelectedValue = Nothing
        Me.cbxDocType.Size = New System.Drawing.Size(110, 22)
        Me.cbxDocType.TabIndex = 1
        Me.cbxDocType.ValueMember = Nothing
        '
        'btnUpdate
        '
        Me.btnUpdate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnUpdate.Location = New System.Drawing.Point(5, 238)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(200, 23)
        Me.btnUpdate.TabIndex = 14
        Me.btnUpdate.Text = "Update"
        '
        'btnSend
        '
        Me.btnSend.Location = New System.Drawing.Point(173, 6)
        Me.btnSend.Name = "btnSend"
        Me.btnSend.Size = New System.Drawing.Size(45, 23)
        Me.btnSend.TabIndex = 1
        Me.btnSend.TabStop = False
        Me.btnSend.Text = "Send"
        '
        'gbxButtons
        '
        Me.gbxButtons.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxButtons.Controls.Add(Me.btnPrint)
        Me.gbxButtons.Controls.Add(Me.btnReturnToController)
        Me.gbxButtons.Controls.Add(Me.btnKeith)
        Me.gbxButtons.Controls.Add(Me.btnPassAuth)
        Me.gbxButtons.Location = New System.Drawing.Point(962, 501)
        Me.gbxButtons.Name = "gbxButtons"
        Me.gbxButtons.ShowCaption = False
        Me.gbxButtons.Size = New System.Drawing.Size(210, 151)
        Me.gbxButtons.TabIndex = 4
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.Location = New System.Drawing.Point(4, 36)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(200, 23)
        Me.btnPrint.TabIndex = 0
        Me.btnPrint.Text = "Print this Scan"
        '
        'btnReturnToController
        '
        Me.btnReturnToController.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnReturnToController.Location = New System.Drawing.Point(4, 65)
        Me.btnReturnToController.Name = "btnReturnToController"
        Me.btnReturnToController.Size = New System.Drawing.Size(200, 23)
        Me.btnReturnToController.TabIndex = 1
        Me.btnReturnToController.Text = "Return to Controller"
        '
        'btnKeith
        '
        Me.btnKeith.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnKeith.Location = New System.Drawing.Point(4, 94)
        Me.btnKeith.Name = "btnKeith"
        Me.btnKeith.Size = New System.Drawing.Size(200, 23)
        Me.btnKeith.TabIndex = 2
        Me.btnKeith.Text = "Send to Keith as Query"
        '
        'btnPassAuth
        '
        Me.btnPassAuth.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPassAuth.Location = New System.Drawing.Point(4, 123)
        Me.btnPassAuth.Name = "btnPassAuth"
        Me.btnPassAuth.Size = New System.Drawing.Size(200, 23)
        Me.btnPassAuth.TabIndex = 3
        Me.btnPassAuth.Text = "Send to AJ to Authorise"
        '
        'btnAuthorise
        '
        Me.btnAuthorise.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAuthorise.Location = New System.Drawing.Point(5, 389)
        Me.btnAuthorise.Name = "btnAuthorise"
        Me.btnAuthorise.Size = New System.Drawing.Size(200, 23)
        Me.btnAuthorise.TabIndex = 25
        Me.btnAuthorise.Text = "Authorise"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.chkTop20)
        Me.GroupControl1.Controls.Add(Me.radSpecificTray)
        Me.GroupControl1.Controls.Add(Me.cbxSelectIntray)
        Me.GroupControl1.Controls.Add(Me.radAllTrays)
        Me.GroupControl1.Controls.Add(Me.radMyIntray)
        Me.GroupControl1.Controls.Add(Me.radCompanyTray)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(557, 37)
        Me.GroupControl1.TabIndex = 0
        '
        'chkTop20
        '
        Me.chkTop20.EditValue = True
        Me.chkTop20.EnterMoveNextControl = True
        Me.chkTop20.Location = New System.Drawing.Point(494, 8)
        Me.chkTop20.Name = "chkTop20"
        Me.chkTop20.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkTop20.Properties.Appearance.Options.UseFont = True
        Me.chkTop20.Properties.AutoWidth = True
        Me.chkTop20.Properties.Caption = "Top 20"
        Me.chkTop20.Size = New System.Drawing.Size(58, 19)
        Me.chkTop20.TabIndex = 5
        Me.chkTop20.ToolTip = resources.GetString("chkTop20.ToolTip")
        Me.chkTop20.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.chkTop20.ToolTipTitle = "Top 20"
        '
        'radSpecificTray
        '
        Me.radSpecificTray.Location = New System.Drawing.Point(167, 8)
        Me.radSpecificTray.Name = "radSpecificTray"
        Me.radSpecificTray.Properties.AutoWidth = True
        Me.radSpecificTray.Properties.Caption = "Specific"
        Me.radSpecificTray.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radSpecificTray.Properties.RadioGroupIndex = 0
        Me.radSpecificTray.Size = New System.Drawing.Size(58, 19)
        Me.radSpecificTray.TabIndex = 3
        Me.radSpecificTray.TabStop = False
        '
        'cbxSelectIntray
        '
        Me.cbxSelectIntray.AllowBlank = False
        Me.cbxSelectIntray.DataSource = Nothing
        Me.cbxSelectIntray.DisplayMember = Nothing
        Me.cbxSelectIntray.EnterMoveNextControl = True
        Me.cbxSelectIntray.Location = New System.Drawing.Point(231, 7)
        Me.cbxSelectIntray.Name = "cbxSelectIntray"
        Me.cbxSelectIntray.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSelectIntray.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxSelectIntray.Properties.Appearance.Options.UseFont = True
        Me.cbxSelectIntray.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSelectIntray.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSelectIntray.SelectedValue = Nothing
        Me.cbxSelectIntray.Size = New System.Drawing.Size(257, 22)
        Me.cbxSelectIntray.TabIndex = 4
        Me.cbxSelectIntray.ValueMember = Nothing
        '
        'radAllTrays
        '
        Me.radAllTrays.Location = New System.Drawing.Point(128, 8)
        Me.radAllTrays.Name = "radAllTrays"
        Me.radAllTrays.Properties.AutoWidth = True
        Me.radAllTrays.Properties.Caption = "All"
        Me.radAllTrays.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radAllTrays.Properties.RadioGroupIndex = 0
        Me.radAllTrays.Size = New System.Drawing.Size(33, 19)
        Me.radAllTrays.TabIndex = 2
        Me.radAllTrays.TabStop = False
        '
        'radMyIntray
        '
        Me.radMyIntray.EditValue = True
        Me.radMyIntray.Location = New System.Drawing.Point(5, 8)
        Me.radMyIntray.Name = "radMyIntray"
        Me.radMyIntray.Properties.AutoWidth = True
        Me.radMyIntray.Properties.Caption = "Mine"
        Me.radMyIntray.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radMyIntray.Properties.RadioGroupIndex = 0
        Me.radMyIntray.Size = New System.Drawing.Size(44, 19)
        Me.radMyIntray.TabIndex = 0
        '
        'radCompanyTray
        '
        Me.radCompanyTray.Location = New System.Drawing.Point(55, 8)
        Me.radCompanyTray.Name = "radCompanyTray"
        Me.radCompanyTray.Properties.AutoWidth = True
        Me.radCompanyTray.Properties.Caption = "Company"
        Me.radCompanyTray.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radCompanyTray.Properties.RadioGroupIndex = 0
        Me.radCompanyTray.Size = New System.Drawing.Size(67, 19)
        Me.radCompanyTray.TabIndex = 1
        Me.radCompanyTray.TabStop = False
        '
        'ctView
        '
        Me.ctView.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ctView.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Left
        Me.ctView.HeaderOrientation = DevExpress.XtraTab.TabOrientation.Vertical
        Me.ctView.Location = New System.Drawing.Point(12, 55)
        Me.ctView.Name = "ctView"
        Me.ctView.SelectedTabPage = Me.tabViewImage
        Me.ctView.Size = New System.Drawing.Size(949, 601)
        Me.ctView.TabIndex = 5
        Me.ctView.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabViewImage, Me.tabViewDetail})
        '
        'tabViewImage
        '
        Me.tabViewImage.Controls.Add(Me.sccVertical)
        Me.tabViewImage.Name = "tabViewImage"
        Me.tabViewImage.Size = New System.Drawing.Size(920, 595)
        Me.tabViewImage.Text = "Image View"
        '
        'sccVertical
        '
        Me.sccVertical.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.sccVertical.Location = New System.Drawing.Point(3, 3)
        Me.sccVertical.Name = "sccVertical"
        Me.sccVertical.Panel1.Controls.Add(Me.cgDocuments)
        Me.sccVertical.Panel1.Text = "Panel1"
        Me.sccVertical.Panel2.Controls.Add(Me.sccNotes)
        Me.sccVertical.Panel2.Controls.Add(Me.GroupControl2)
        Me.sccVertical.Panel2.Text = "Panel2"
        Me.sccVertical.Size = New System.Drawing.Size(914, 589)
        Me.sccVertical.SplitterPosition = 114
        Me.sccVertical.TabIndex = 5
        Me.sccVertical.Text = "SplitContainerControl1"
        '
        'cgDocuments
        '
        Me.cgDocuments.AllowBuildColumns = True
        Me.cgDocuments.AllowEdit = False
        Me.cgDocuments.AllowHorizontalScroll = False
        Me.cgDocuments.AllowMultiSelect = False
        Me.cgDocuments.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgDocuments.Appearance.Options.UseFont = True
        Me.cgDocuments.AutoSizeByData = True
        Me.cgDocuments.DisableAutoSize = False
        Me.cgDocuments.DisableDataFormatting = False
        Me.cgDocuments.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cgDocuments.FocusedRowHandle = -2147483648
        Me.cgDocuments.HideFirstColumn = False
        Me.cgDocuments.Location = New System.Drawing.Point(0, 0)
        Me.cgDocuments.Name = "cgDocuments"
        Me.cgDocuments.PreviewColumn = ""
        Me.cgDocuments.QueryID = Nothing
        Me.cgDocuments.RowAutoHeight = True
        Me.cgDocuments.SearchAsYouType = True
        Me.cgDocuments.ShowAutoFilterRow = False
        Me.cgDocuments.ShowFindPanel = False
        Me.cgDocuments.ShowGroupByBox = False
        Me.cgDocuments.ShowLoadingPanel = False
        Me.cgDocuments.ShowNavigator = False
        Me.cgDocuments.Size = New System.Drawing.Size(114, 589)
        Me.cgDocuments.TabIndex = 0
        '
        'sccNotes
        '
        Me.sccNotes.Dock = System.Windows.Forms.DockStyle.Fill
        Me.sccNotes.Horizontal = False
        Me.sccNotes.Location = New System.Drawing.Point(0, 34)
        Me.sccNotes.Name = "sccNotes"
        Me.sccNotes.Panel1.Controls.Add(Me.sccPictureNotes)
        Me.sccNotes.Panel1.Text = "Panel1"
        Me.sccNotes.Panel2.Controls.Add(Me.btnNoteAdd)
        Me.sccNotes.Panel2.Controls.Add(Me.btnNoteSave)
        Me.sccNotes.Panel2.Controls.Add(Me.GroupControl4)
        Me.sccNotes.Panel2.Controls.Add(Me.txtComments)
        Me.sccNotes.Panel2.Text = "Panel2"
        Me.sccNotes.Size = New System.Drawing.Size(795, 555)
        Me.sccNotes.SplitterPosition = 450
        Me.sccNotes.TabIndex = 1
        Me.sccNotes.Text = "SplitContainerControl2"
        '
        'sccPictureNotes
        '
        Me.sccPictureNotes.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2
        Me.sccPictureNotes.Dock = System.Windows.Forms.DockStyle.Fill
        Me.sccPictureNotes.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2
        Me.sccPictureNotes.IsSplitterFixed = True
        Me.sccPictureNotes.Location = New System.Drawing.Point(0, 0)
        Me.sccPictureNotes.Name = "sccPictureNotes"
        Me.sccPictureNotes.Panel1.Controls.Add(Me.pic)
        Me.sccPictureNotes.Panel1.Text = "Panel1"
        Me.sccPictureNotes.Panel2.Controls.Add(Me.GroupControl3)
        Me.sccPictureNotes.Panel2.Text = "Panel2"
        Me.sccPictureNotes.Size = New System.Drawing.Size(795, 450)
        Me.sccPictureNotes.SplitterPosition = 210
        Me.sccPictureNotes.TabIndex = 0
        Me.sccPictureNotes.Text = "SplitContainerControl1"
        '
        'pic
        '
        Me.pic.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pic.Location = New System.Drawing.Point(0, 0)
        Me.pic.Name = "pic"
        Me.pic.Properties.AllowScrollOnMouseWheel = DevExpress.Utils.DefaultBoolean.[False]
        Me.pic.Properties.AllowScrollViaMouseDrag = True
        Me.pic.Properties.AllowZoomOnMouseWheel = DevExpress.Utils.DefaultBoolean.[True]
        Me.pic.Properties.ReadOnly = True
        Me.pic.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.pic.Properties.ShowScrollBars = True
        Me.pic.Properties.ShowZoomSubMenu = DevExpress.Utils.DefaultBoolean.[True]
        Me.pic.Properties.ZoomingOperationMode = DevExpress.XtraEditors.Repository.ZoomingOperationMode.MouseWheel
        Me.pic.Size = New System.Drawing.Size(580, 450)
        Me.pic.TabIndex = 1
        '
        'GroupControl3
        '
        Me.GroupControl3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl3.Controls.Add(Me.btnPaid)
        Me.GroupControl3.Controls.Add(Me.btnGeneratePriceNote)
        Me.GroupControl3.Controls.Add(Me.CareLabel18)
        Me.GroupControl3.Controls.Add(Me.txt9)
        Me.GroupControl3.Controls.Add(Me.cbx9)
        Me.GroupControl3.Controls.Add(Me.CareLabel19)
        Me.GroupControl3.Controls.Add(Me.CareLabel20)
        Me.GroupControl3.Controls.Add(Me.txt12)
        Me.GroupControl3.Controls.Add(Me.cbx12)
        Me.GroupControl3.Controls.Add(Me.CareLabel21)
        Me.GroupControl3.Controls.Add(Me.CareLabel22)
        Me.GroupControl3.Controls.Add(Me.txt11)
        Me.GroupControl3.Controls.Add(Me.cbx11)
        Me.GroupControl3.Controls.Add(Me.CareLabel23)
        Me.GroupControl3.Controls.Add(Me.CareLabel24)
        Me.GroupControl3.Controls.Add(Me.txt10)
        Me.GroupControl3.Controls.Add(Me.cbx10)
        Me.GroupControl3.Controls.Add(Me.CareLabel25)
        Me.GroupControl3.Controls.Add(Me.CareLabel10)
        Me.GroupControl3.Controls.Add(Me.txt5)
        Me.GroupControl3.Controls.Add(Me.cbx5)
        Me.GroupControl3.Controls.Add(Me.CareLabel11)
        Me.GroupControl3.Controls.Add(Me.CareLabel12)
        Me.GroupControl3.Controls.Add(Me.txt8)
        Me.GroupControl3.Controls.Add(Me.cbx8)
        Me.GroupControl3.Controls.Add(Me.CareLabel13)
        Me.GroupControl3.Controls.Add(Me.CareLabel14)
        Me.GroupControl3.Controls.Add(Me.txt7)
        Me.GroupControl3.Controls.Add(Me.cbx7)
        Me.GroupControl3.Controls.Add(Me.CareLabel15)
        Me.GroupControl3.Controls.Add(Me.CareLabel16)
        Me.GroupControl3.Controls.Add(Me.txt6)
        Me.GroupControl3.Controls.Add(Me.cbx6)
        Me.GroupControl3.Controls.Add(Me.CareLabel17)
        Me.GroupControl3.Controls.Add(Me.CareLabel8)
        Me.GroupControl3.Controls.Add(Me.btnAuthorise)
        Me.GroupControl3.Controls.Add(Me.txt1)
        Me.GroupControl3.Controls.Add(Me.cbx1)
        Me.GroupControl3.Controls.Add(Me.CareLabel9)
        Me.GroupControl3.Controls.Add(Me.CareLabel6)
        Me.GroupControl3.Controls.Add(Me.txt4)
        Me.GroupControl3.Controls.Add(Me.cbx4)
        Me.GroupControl3.Controls.Add(Me.CareLabel7)
        Me.GroupControl3.Controls.Add(Me.CareLabel4)
        Me.GroupControl3.Controls.Add(Me.txt3)
        Me.GroupControl3.Controls.Add(Me.cbx3)
        Me.GroupControl3.Controls.Add(Me.CareLabel5)
        Me.GroupControl3.Controls.Add(Me.CareLabel3)
        Me.GroupControl3.Controls.Add(Me.txt2)
        Me.GroupControl3.Controls.Add(Me.cbx2)
        Me.GroupControl3.Controls.Add(Me.CareLabel1)
        Me.GroupControl3.Location = New System.Drawing.Point(0, 3)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.ShowCaption = False
        Me.GroupControl3.Size = New System.Drawing.Size(210, 447)
        Me.GroupControl3.TabIndex = 5
        '
        'btnPaid
        '
        Me.btnPaid.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPaid.Location = New System.Drawing.Point(5, 418)
        Me.btnPaid.Name = "btnPaid"
        Me.btnPaid.Size = New System.Drawing.Size(200, 23)
        Me.btnPaid.TabIndex = 58
        Me.btnPaid.Text = "Mark Document as Paid"
        '
        'btnGeneratePriceNote
        '
        Me.btnGeneratePriceNote.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnGeneratePriceNote.Location = New System.Drawing.Point(5, 360)
        Me.btnGeneratePriceNote.Name = "btnGeneratePriceNote"
        Me.btnGeneratePriceNote.Size = New System.Drawing.Size(200, 23)
        Me.btnGeneratePriceNote.TabIndex = 57
        Me.btnGeneratePriceNote.Text = "Generate Pricing Notes"
        '
        'CareLabel18
        '
        Me.CareLabel18.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel18.Appearance.Options.UseFont = True
        Me.CareLabel18.Appearance.Options.UseTextOptions = True
        Me.CareLabel18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel18.Location = New System.Drawing.Point(5, 236)
        Me.CareLabel18.Name = "CareLabel18"
        Me.CareLabel18.Size = New System.Drawing.Size(6, 15)
        Me.CareLabel18.TabIndex = 56
        Me.CareLabel18.Text = "9"
        Me.CareLabel18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt9
        '
        Me.txt9.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt9.EnterMoveNextControl = True
        Me.txt9.Location = New System.Drawing.Point(128, 233)
        Me.txt9.MaxLength = 14
        Me.txt9.Name = "txt9"
        Me.txt9.NumericAllowNegatives = True
        Me.txt9.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txt9.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt9.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txt9.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txt9.Properties.Appearance.Options.UseFont = True
        Me.txt9.Properties.Appearance.Options.UseTextOptions = True
        Me.txt9.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txt9.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt9.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txt9.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt9.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt9.Properties.MaxLength = 14
        Me.txt9.Size = New System.Drawing.Size(77, 22)
        Me.txt9.TabIndex = 18
        Me.txt9.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txt9.ToolTipText = ""
        '
        'cbx9
        '
        Me.cbx9.AllowBlank = False
        Me.cbx9.DataSource = Nothing
        Me.cbx9.DisplayMember = Nothing
        Me.cbx9.EnterMoveNextControl = True
        Me.cbx9.Location = New System.Drawing.Point(23, 233)
        Me.cbx9.Name = "cbx9"
        Me.cbx9.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbx9.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbx9.Properties.Appearance.Options.UseFont = True
        Me.cbx9.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbx9.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbx9.SelectedValue = Nothing
        Me.cbx9.Size = New System.Drawing.Size(67, 22)
        Me.cbx9.TabIndex = 17
        Me.cbx9.ValueMember = Nothing
        '
        'CareLabel19
        '
        Me.CareLabel19.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel19.Appearance.Options.UseFont = True
        Me.CareLabel19.Appearance.Options.UseTextOptions = True
        Me.CareLabel19.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel19.Location = New System.Drawing.Point(96, 236)
        Me.CareLabel19.Name = "CareLabel19"
        Me.CareLabel19.Size = New System.Drawing.Size(26, 15)
        Me.CareLabel19.TabIndex = 54
        Me.CareLabel19.Text = "Price"
        Me.CareLabel19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel20
        '
        Me.CareLabel20.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel20.Appearance.Options.UseFont = True
        Me.CareLabel20.Appearance.Options.UseTextOptions = True
        Me.CareLabel20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel20.Location = New System.Drawing.Point(5, 320)
        Me.CareLabel20.Name = "CareLabel20"
        Me.CareLabel20.Size = New System.Drawing.Size(12, 15)
        Me.CareLabel20.TabIndex = 52
        Me.CareLabel20.Text = "12"
        Me.CareLabel20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt12
        '
        Me.txt12.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt12.EnterMoveNextControl = True
        Me.txt12.Location = New System.Drawing.Point(128, 317)
        Me.txt12.MaxLength = 14
        Me.txt12.Name = "txt12"
        Me.txt12.NumericAllowNegatives = True
        Me.txt12.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txt12.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt12.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txt12.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txt12.Properties.Appearance.Options.UseFont = True
        Me.txt12.Properties.Appearance.Options.UseTextOptions = True
        Me.txt12.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txt12.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt12.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txt12.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt12.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt12.Properties.MaxLength = 14
        Me.txt12.Size = New System.Drawing.Size(77, 22)
        Me.txt12.TabIndex = 24
        Me.txt12.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txt12.ToolTipText = ""
        '
        'cbx12
        '
        Me.cbx12.AllowBlank = False
        Me.cbx12.DataSource = Nothing
        Me.cbx12.DisplayMember = Nothing
        Me.cbx12.EnterMoveNextControl = True
        Me.cbx12.Location = New System.Drawing.Point(23, 317)
        Me.cbx12.Name = "cbx12"
        Me.cbx12.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbx12.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbx12.Properties.Appearance.Options.UseFont = True
        Me.cbx12.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbx12.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbx12.SelectedValue = Nothing
        Me.cbx12.Size = New System.Drawing.Size(67, 22)
        Me.cbx12.TabIndex = 23
        Me.cbx12.ValueMember = Nothing
        '
        'CareLabel21
        '
        Me.CareLabel21.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel21.Appearance.Options.UseFont = True
        Me.CareLabel21.Appearance.Options.UseTextOptions = True
        Me.CareLabel21.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel21.Location = New System.Drawing.Point(96, 320)
        Me.CareLabel21.Name = "CareLabel21"
        Me.CareLabel21.Size = New System.Drawing.Size(26, 15)
        Me.CareLabel21.TabIndex = 50
        Me.CareLabel21.Text = "Price"
        Me.CareLabel21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel22
        '
        Me.CareLabel22.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel22.Appearance.Options.UseFont = True
        Me.CareLabel22.Appearance.Options.UseTextOptions = True
        Me.CareLabel22.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel22.Location = New System.Drawing.Point(5, 292)
        Me.CareLabel22.Name = "CareLabel22"
        Me.CareLabel22.Size = New System.Drawing.Size(12, 15)
        Me.CareLabel22.TabIndex = 48
        Me.CareLabel22.Text = "11"
        Me.CareLabel22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt11
        '
        Me.txt11.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt11.EnterMoveNextControl = True
        Me.txt11.Location = New System.Drawing.Point(128, 289)
        Me.txt11.MaxLength = 14
        Me.txt11.Name = "txt11"
        Me.txt11.NumericAllowNegatives = True
        Me.txt11.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txt11.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt11.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txt11.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txt11.Properties.Appearance.Options.UseFont = True
        Me.txt11.Properties.Appearance.Options.UseTextOptions = True
        Me.txt11.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txt11.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt11.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txt11.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt11.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt11.Properties.MaxLength = 14
        Me.txt11.Size = New System.Drawing.Size(77, 22)
        Me.txt11.TabIndex = 22
        Me.txt11.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txt11.ToolTipText = ""
        '
        'cbx11
        '
        Me.cbx11.AllowBlank = False
        Me.cbx11.DataSource = Nothing
        Me.cbx11.DisplayMember = Nothing
        Me.cbx11.EnterMoveNextControl = True
        Me.cbx11.Location = New System.Drawing.Point(23, 289)
        Me.cbx11.Name = "cbx11"
        Me.cbx11.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbx11.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbx11.Properties.Appearance.Options.UseFont = True
        Me.cbx11.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbx11.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbx11.SelectedValue = Nothing
        Me.cbx11.Size = New System.Drawing.Size(67, 22)
        Me.cbx11.TabIndex = 21
        Me.cbx11.ValueMember = Nothing
        '
        'CareLabel23
        '
        Me.CareLabel23.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel23.Appearance.Options.UseFont = True
        Me.CareLabel23.Appearance.Options.UseTextOptions = True
        Me.CareLabel23.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel23.Location = New System.Drawing.Point(96, 292)
        Me.CareLabel23.Name = "CareLabel23"
        Me.CareLabel23.Size = New System.Drawing.Size(26, 15)
        Me.CareLabel23.TabIndex = 46
        Me.CareLabel23.Text = "Price"
        Me.CareLabel23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel24
        '
        Me.CareLabel24.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel24.Appearance.Options.UseFont = True
        Me.CareLabel24.Appearance.Options.UseTextOptions = True
        Me.CareLabel24.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel24.Location = New System.Drawing.Point(5, 264)
        Me.CareLabel24.Name = "CareLabel24"
        Me.CareLabel24.Size = New System.Drawing.Size(12, 15)
        Me.CareLabel24.TabIndex = 44
        Me.CareLabel24.Text = "10"
        Me.CareLabel24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt10
        '
        Me.txt10.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt10.EnterMoveNextControl = True
        Me.txt10.Location = New System.Drawing.Point(128, 261)
        Me.txt10.MaxLength = 14
        Me.txt10.Name = "txt10"
        Me.txt10.NumericAllowNegatives = True
        Me.txt10.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txt10.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt10.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txt10.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txt10.Properties.Appearance.Options.UseFont = True
        Me.txt10.Properties.Appearance.Options.UseTextOptions = True
        Me.txt10.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txt10.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt10.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txt10.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt10.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt10.Properties.MaxLength = 14
        Me.txt10.Size = New System.Drawing.Size(77, 22)
        Me.txt10.TabIndex = 20
        Me.txt10.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txt10.ToolTipText = ""
        '
        'cbx10
        '
        Me.cbx10.AllowBlank = False
        Me.cbx10.DataSource = Nothing
        Me.cbx10.DisplayMember = Nothing
        Me.cbx10.EnterMoveNextControl = True
        Me.cbx10.Location = New System.Drawing.Point(23, 261)
        Me.cbx10.Name = "cbx10"
        Me.cbx10.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbx10.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbx10.Properties.Appearance.Options.UseFont = True
        Me.cbx10.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbx10.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbx10.SelectedValue = Nothing
        Me.cbx10.Size = New System.Drawing.Size(67, 22)
        Me.cbx10.TabIndex = 19
        Me.cbx10.ValueMember = Nothing
        '
        'CareLabel25
        '
        Me.CareLabel25.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel25.Appearance.Options.UseFont = True
        Me.CareLabel25.Appearance.Options.UseTextOptions = True
        Me.CareLabel25.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel25.Location = New System.Drawing.Point(96, 264)
        Me.CareLabel25.Name = "CareLabel25"
        Me.CareLabel25.Size = New System.Drawing.Size(26, 15)
        Me.CareLabel25.TabIndex = 42
        Me.CareLabel25.Text = "Price"
        Me.CareLabel25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel10
        '
        Me.CareLabel10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel10.Appearance.Options.UseFont = True
        Me.CareLabel10.Appearance.Options.UseTextOptions = True
        Me.CareLabel10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel10.Location = New System.Drawing.Point(5, 124)
        Me.CareLabel10.Name = "CareLabel10"
        Me.CareLabel10.Size = New System.Drawing.Size(6, 15)
        Me.CareLabel10.TabIndex = 40
        Me.CareLabel10.Text = "5"
        Me.CareLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt5
        '
        Me.txt5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt5.EnterMoveNextControl = True
        Me.txt5.Location = New System.Drawing.Point(128, 121)
        Me.txt5.MaxLength = 14
        Me.txt5.Name = "txt5"
        Me.txt5.NumericAllowNegatives = True
        Me.txt5.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txt5.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt5.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txt5.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txt5.Properties.Appearance.Options.UseFont = True
        Me.txt5.Properties.Appearance.Options.UseTextOptions = True
        Me.txt5.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txt5.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt5.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txt5.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt5.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt5.Properties.MaxLength = 14
        Me.txt5.Size = New System.Drawing.Size(77, 22)
        Me.txt5.TabIndex = 10
        Me.txt5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txt5.ToolTipText = ""
        '
        'cbx5
        '
        Me.cbx5.AllowBlank = False
        Me.cbx5.DataSource = Nothing
        Me.cbx5.DisplayMember = Nothing
        Me.cbx5.EnterMoveNextControl = True
        Me.cbx5.Location = New System.Drawing.Point(23, 121)
        Me.cbx5.Name = "cbx5"
        Me.cbx5.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbx5.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbx5.Properties.Appearance.Options.UseFont = True
        Me.cbx5.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbx5.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbx5.SelectedValue = Nothing
        Me.cbx5.Size = New System.Drawing.Size(67, 22)
        Me.cbx5.TabIndex = 9
        Me.cbx5.ValueMember = Nothing
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.Options.UseFont = True
        Me.CareLabel11.Appearance.Options.UseTextOptions = True
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(96, 124)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(26, 15)
        Me.CareLabel11.TabIndex = 38
        Me.CareLabel11.Text = "Price"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel12
        '
        Me.CareLabel12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel12.Appearance.Options.UseFont = True
        Me.CareLabel12.Appearance.Options.UseTextOptions = True
        Me.CareLabel12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel12.Location = New System.Drawing.Point(5, 208)
        Me.CareLabel12.Name = "CareLabel12"
        Me.CareLabel12.Size = New System.Drawing.Size(6, 15)
        Me.CareLabel12.TabIndex = 36
        Me.CareLabel12.Text = "8"
        Me.CareLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt8
        '
        Me.txt8.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt8.EnterMoveNextControl = True
        Me.txt8.Location = New System.Drawing.Point(128, 205)
        Me.txt8.MaxLength = 14
        Me.txt8.Name = "txt8"
        Me.txt8.NumericAllowNegatives = True
        Me.txt8.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txt8.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt8.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txt8.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txt8.Properties.Appearance.Options.UseFont = True
        Me.txt8.Properties.Appearance.Options.UseTextOptions = True
        Me.txt8.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txt8.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt8.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txt8.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt8.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt8.Properties.MaxLength = 14
        Me.txt8.Size = New System.Drawing.Size(77, 22)
        Me.txt8.TabIndex = 16
        Me.txt8.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txt8.ToolTipText = ""
        '
        'cbx8
        '
        Me.cbx8.AllowBlank = False
        Me.cbx8.DataSource = Nothing
        Me.cbx8.DisplayMember = Nothing
        Me.cbx8.EnterMoveNextControl = True
        Me.cbx8.Location = New System.Drawing.Point(23, 205)
        Me.cbx8.Name = "cbx8"
        Me.cbx8.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbx8.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbx8.Properties.Appearance.Options.UseFont = True
        Me.cbx8.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbx8.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbx8.SelectedValue = Nothing
        Me.cbx8.Size = New System.Drawing.Size(67, 22)
        Me.cbx8.TabIndex = 15
        Me.cbx8.ValueMember = Nothing
        '
        'CareLabel13
        '
        Me.CareLabel13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel13.Appearance.Options.UseFont = True
        Me.CareLabel13.Appearance.Options.UseTextOptions = True
        Me.CareLabel13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel13.Location = New System.Drawing.Point(96, 208)
        Me.CareLabel13.Name = "CareLabel13"
        Me.CareLabel13.Size = New System.Drawing.Size(26, 15)
        Me.CareLabel13.TabIndex = 34
        Me.CareLabel13.Text = "Price"
        Me.CareLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel14.Appearance.Options.UseFont = True
        Me.CareLabel14.Appearance.Options.UseTextOptions = True
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(5, 180)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(6, 15)
        Me.CareLabel14.TabIndex = 32
        Me.CareLabel14.Text = "7"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt7
        '
        Me.txt7.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt7.EnterMoveNextControl = True
        Me.txt7.Location = New System.Drawing.Point(128, 177)
        Me.txt7.MaxLength = 14
        Me.txt7.Name = "txt7"
        Me.txt7.NumericAllowNegatives = True
        Me.txt7.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txt7.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt7.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txt7.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txt7.Properties.Appearance.Options.UseFont = True
        Me.txt7.Properties.Appearance.Options.UseTextOptions = True
        Me.txt7.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txt7.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt7.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txt7.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt7.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt7.Properties.MaxLength = 14
        Me.txt7.Size = New System.Drawing.Size(77, 22)
        Me.txt7.TabIndex = 14
        Me.txt7.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txt7.ToolTipText = ""
        '
        'cbx7
        '
        Me.cbx7.AllowBlank = False
        Me.cbx7.DataSource = Nothing
        Me.cbx7.DisplayMember = Nothing
        Me.cbx7.EnterMoveNextControl = True
        Me.cbx7.Location = New System.Drawing.Point(23, 177)
        Me.cbx7.Name = "cbx7"
        Me.cbx7.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbx7.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbx7.Properties.Appearance.Options.UseFont = True
        Me.cbx7.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbx7.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbx7.SelectedValue = Nothing
        Me.cbx7.Size = New System.Drawing.Size(67, 22)
        Me.cbx7.TabIndex = 13
        Me.cbx7.ValueMember = Nothing
        '
        'CareLabel15
        '
        Me.CareLabel15.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel15.Appearance.Options.UseFont = True
        Me.CareLabel15.Appearance.Options.UseTextOptions = True
        Me.CareLabel15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel15.Location = New System.Drawing.Point(96, 180)
        Me.CareLabel15.Name = "CareLabel15"
        Me.CareLabel15.Size = New System.Drawing.Size(26, 15)
        Me.CareLabel15.TabIndex = 30
        Me.CareLabel15.Text = "Price"
        Me.CareLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel16
        '
        Me.CareLabel16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel16.Appearance.Options.UseFont = True
        Me.CareLabel16.Appearance.Options.UseTextOptions = True
        Me.CareLabel16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel16.Location = New System.Drawing.Point(5, 152)
        Me.CareLabel16.Name = "CareLabel16"
        Me.CareLabel16.Size = New System.Drawing.Size(6, 15)
        Me.CareLabel16.TabIndex = 28
        Me.CareLabel16.Text = "6"
        Me.CareLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt6
        '
        Me.txt6.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt6.EnterMoveNextControl = True
        Me.txt6.Location = New System.Drawing.Point(128, 149)
        Me.txt6.MaxLength = 14
        Me.txt6.Name = "txt6"
        Me.txt6.NumericAllowNegatives = True
        Me.txt6.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txt6.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt6.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txt6.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txt6.Properties.Appearance.Options.UseFont = True
        Me.txt6.Properties.Appearance.Options.UseTextOptions = True
        Me.txt6.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txt6.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt6.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txt6.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt6.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt6.Properties.MaxLength = 14
        Me.txt6.Size = New System.Drawing.Size(77, 22)
        Me.txt6.TabIndex = 12
        Me.txt6.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txt6.ToolTipText = ""
        '
        'cbx6
        '
        Me.cbx6.AllowBlank = False
        Me.cbx6.DataSource = Nothing
        Me.cbx6.DisplayMember = Nothing
        Me.cbx6.EnterMoveNextControl = True
        Me.cbx6.Location = New System.Drawing.Point(23, 149)
        Me.cbx6.Name = "cbx6"
        Me.cbx6.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbx6.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbx6.Properties.Appearance.Options.UseFont = True
        Me.cbx6.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbx6.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbx6.SelectedValue = Nothing
        Me.cbx6.Size = New System.Drawing.Size(67, 22)
        Me.cbx6.TabIndex = 11
        Me.cbx6.ValueMember = Nothing
        '
        'CareLabel17
        '
        Me.CareLabel17.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel17.Appearance.Options.UseFont = True
        Me.CareLabel17.Appearance.Options.UseTextOptions = True
        Me.CareLabel17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel17.Location = New System.Drawing.Point(96, 152)
        Me.CareLabel17.Name = "CareLabel17"
        Me.CareLabel17.Size = New System.Drawing.Size(26, 15)
        Me.CareLabel17.TabIndex = 26
        Me.CareLabel17.Text = "Price"
        Me.CareLabel17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.Options.UseFont = True
        Me.CareLabel8.Appearance.Options.UseTextOptions = True
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(5, 12)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(6, 15)
        Me.CareLabel8.TabIndex = 24
        Me.CareLabel8.Text = "1"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt1
        '
        Me.txt1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt1.EnterMoveNextControl = True
        Me.txt1.Location = New System.Drawing.Point(128, 9)
        Me.txt1.MaxLength = 14
        Me.txt1.Name = "txt1"
        Me.txt1.NumericAllowNegatives = True
        Me.txt1.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txt1.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txt1.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txt1.Properties.Appearance.Options.UseFont = True
        Me.txt1.Properties.Appearance.Options.UseTextOptions = True
        Me.txt1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txt1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt1.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt1.Properties.MaxLength = 14
        Me.txt1.Size = New System.Drawing.Size(77, 22)
        Me.txt1.TabIndex = 2
        Me.txt1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txt1.ToolTipText = ""
        '
        'cbx1
        '
        Me.cbx1.AllowBlank = False
        Me.cbx1.DataSource = Nothing
        Me.cbx1.DisplayMember = Nothing
        Me.cbx1.EnterMoveNextControl = True
        Me.cbx1.Location = New System.Drawing.Point(23, 9)
        Me.cbx1.Name = "cbx1"
        Me.cbx1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbx1.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbx1.Properties.Appearance.Options.UseFont = True
        Me.cbx1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbx1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbx1.SelectedValue = Nothing
        Me.cbx1.Size = New System.Drawing.Size(67, 22)
        Me.cbx1.TabIndex = 1
        Me.cbx1.ValueMember = Nothing
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel9.Appearance.Options.UseFont = True
        Me.CareLabel9.Appearance.Options.UseTextOptions = True
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(96, 12)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(26, 15)
        Me.CareLabel9.TabIndex = 22
        Me.CareLabel9.Text = "Price"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.Options.UseFont = True
        Me.CareLabel6.Appearance.Options.UseTextOptions = True
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(5, 96)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(6, 15)
        Me.CareLabel6.TabIndex = 20
        Me.CareLabel6.Text = "4"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt4
        '
        Me.txt4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt4.EnterMoveNextControl = True
        Me.txt4.Location = New System.Drawing.Point(128, 93)
        Me.txt4.MaxLength = 14
        Me.txt4.Name = "txt4"
        Me.txt4.NumericAllowNegatives = True
        Me.txt4.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txt4.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt4.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txt4.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txt4.Properties.Appearance.Options.UseFont = True
        Me.txt4.Properties.Appearance.Options.UseTextOptions = True
        Me.txt4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txt4.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt4.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txt4.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt4.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt4.Properties.MaxLength = 14
        Me.txt4.Size = New System.Drawing.Size(77, 22)
        Me.txt4.TabIndex = 8
        Me.txt4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txt4.ToolTipText = ""
        '
        'cbx4
        '
        Me.cbx4.AllowBlank = False
        Me.cbx4.DataSource = Nothing
        Me.cbx4.DisplayMember = Nothing
        Me.cbx4.EnterMoveNextControl = True
        Me.cbx4.Location = New System.Drawing.Point(23, 93)
        Me.cbx4.Name = "cbx4"
        Me.cbx4.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbx4.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbx4.Properties.Appearance.Options.UseFont = True
        Me.cbx4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbx4.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbx4.SelectedValue = Nothing
        Me.cbx4.Size = New System.Drawing.Size(67, 22)
        Me.cbx4.TabIndex = 7
        Me.cbx4.ValueMember = Nothing
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel7.Appearance.Options.UseFont = True
        Me.CareLabel7.Appearance.Options.UseTextOptions = True
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(96, 96)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(26, 15)
        Me.CareLabel7.TabIndex = 18
        Me.CareLabel7.Text = "Price"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.Options.UseFont = True
        Me.CareLabel4.Appearance.Options.UseTextOptions = True
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(5, 68)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(6, 15)
        Me.CareLabel4.TabIndex = 16
        Me.CareLabel4.Text = "3"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt3
        '
        Me.txt3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt3.EnterMoveNextControl = True
        Me.txt3.Location = New System.Drawing.Point(128, 65)
        Me.txt3.MaxLength = 14
        Me.txt3.Name = "txt3"
        Me.txt3.NumericAllowNegatives = True
        Me.txt3.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txt3.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt3.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txt3.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txt3.Properties.Appearance.Options.UseFont = True
        Me.txt3.Properties.Appearance.Options.UseTextOptions = True
        Me.txt3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txt3.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt3.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txt3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt3.Properties.MaxLength = 14
        Me.txt3.Size = New System.Drawing.Size(77, 22)
        Me.txt3.TabIndex = 6
        Me.txt3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txt3.ToolTipText = ""
        '
        'cbx3
        '
        Me.cbx3.AllowBlank = False
        Me.cbx3.DataSource = Nothing
        Me.cbx3.DisplayMember = Nothing
        Me.cbx3.EnterMoveNextControl = True
        Me.cbx3.Location = New System.Drawing.Point(23, 65)
        Me.cbx3.Name = "cbx3"
        Me.cbx3.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbx3.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbx3.Properties.Appearance.Options.UseFont = True
        Me.cbx3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbx3.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbx3.SelectedValue = Nothing
        Me.cbx3.Size = New System.Drawing.Size(67, 22)
        Me.cbx3.TabIndex = 5
        Me.cbx3.ValueMember = Nothing
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.Options.UseFont = True
        Me.CareLabel5.Appearance.Options.UseTextOptions = True
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(96, 68)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(26, 15)
        Me.CareLabel5.TabIndex = 14
        Me.CareLabel5.Text = "Price"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.Options.UseFont = True
        Me.CareLabel3.Appearance.Options.UseTextOptions = True
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(5, 40)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(6, 15)
        Me.CareLabel3.TabIndex = 12
        Me.CareLabel3.Text = "2"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt2
        '
        Me.txt2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt2.EnterMoveNextControl = True
        Me.txt2.Location = New System.Drawing.Point(128, 37)
        Me.txt2.MaxLength = 14
        Me.txt2.Name = "txt2"
        Me.txt2.NumericAllowNegatives = True
        Me.txt2.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txt2.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txt2.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txt2.Properties.Appearance.Options.UseFont = True
        Me.txt2.Properties.Appearance.Options.UseTextOptions = True
        Me.txt2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txt2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt2.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txt2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt2.Properties.MaxLength = 14
        Me.txt2.Size = New System.Drawing.Size(77, 22)
        Me.txt2.TabIndex = 4
        Me.txt2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txt2.ToolTipText = ""
        '
        'cbx2
        '
        Me.cbx2.AllowBlank = False
        Me.cbx2.DataSource = Nothing
        Me.cbx2.DisplayMember = Nothing
        Me.cbx2.EnterMoveNextControl = True
        Me.cbx2.Location = New System.Drawing.Point(23, 37)
        Me.cbx2.Name = "cbx2"
        Me.cbx2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbx2.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbx2.Properties.Appearance.Options.UseFont = True
        Me.cbx2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbx2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbx2.SelectedValue = Nothing
        Me.cbx2.Size = New System.Drawing.Size(67, 22)
        Me.cbx2.TabIndex = 3
        Me.cbx2.ValueMember = Nothing
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.Options.UseFont = True
        Me.CareLabel1.Appearance.Options.UseTextOptions = True
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(96, 40)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(26, 15)
        Me.CareLabel1.TabIndex = 4
        Me.CareLabel1.Text = "Price"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnNoteAdd
        '
        Me.btnNoteAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNoteAdd.Location = New System.Drawing.Point(748, 48)
        Me.btnNoteAdd.Name = "btnNoteAdd"
        Me.btnNoteAdd.Size = New System.Drawing.Size(47, 23)
        Me.btnNoteAdd.TabIndex = 10
        Me.btnNoteAdd.Text = "Add"
        '
        'btnNoteSave
        '
        Me.btnNoteSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNoteSave.Location = New System.Drawing.Point(748, 76)
        Me.btnNoteSave.Name = "btnNoteSave"
        Me.btnNoteSave.Size = New System.Drawing.Size(47, 23)
        Me.btnNoteSave.TabIndex = 9
        Me.btnNoteSave.Text = "Save"
        '
        'GroupControl4
        '
        Me.GroupControl4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupControl4.Controls.Add(Me.btnNoteDelete)
        Me.GroupControl4.Controls.Add(Me.cgComments)
        Me.GroupControl4.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.ShowCaption = False
        Me.GroupControl4.Size = New System.Drawing.Size(294, 99)
        Me.GroupControl4.TabIndex = 8
        '
        'btnNoteDelete
        '
        Me.btnNoteDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNoteDelete.Location = New System.Drawing.Point(242, 71)
        Me.btnNoteDelete.Name = "btnNoteDelete"
        Me.btnNoteDelete.Size = New System.Drawing.Size(47, 23)
        Me.btnNoteDelete.TabIndex = 10
        Me.btnNoteDelete.Text = "Delete"
        '
        'cgComments
        '
        Me.cgComments.AllowBuildColumns = True
        Me.cgComments.AllowEdit = False
        Me.cgComments.AllowHorizontalScroll = False
        Me.cgComments.AllowMultiSelect = False
        Me.cgComments.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgComments.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgComments.Appearance.Options.UseFont = True
        Me.cgComments.AutoSizeByData = True
        Me.cgComments.DisableAutoSize = False
        Me.cgComments.DisableDataFormatting = False
        Me.cgComments.FocusedRowHandle = -2147483648
        Me.cgComments.HideFirstColumn = False
        Me.cgComments.Location = New System.Drawing.Point(5, 5)
        Me.cgComments.Name = "cgComments"
        Me.cgComments.PreviewColumn = ""
        Me.cgComments.QueryID = Nothing
        Me.cgComments.RowAutoHeight = True
        Me.cgComments.SearchAsYouType = False
        Me.cgComments.ShowAutoFilterRow = False
        Me.cgComments.ShowFindPanel = False
        Me.cgComments.ShowGroupByBox = False
        Me.cgComments.ShowLoadingPanel = False
        Me.cgComments.ShowNavigator = False
        Me.cgComments.Size = New System.Drawing.Size(231, 89)
        Me.cgComments.TabIndex = 5
        '
        'txtComments
        '
        Me.txtComments.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtComments.Location = New System.Drawing.Point(300, 0)
        Me.txtComments.Name = "txtComments"
        Me.txtComments.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtComments.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtComments, True)
        Me.txtComments.Size = New System.Drawing.Size(442, 100)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtComments, OptionsSpelling1)
        Me.txtComments.TabIndex = 6
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.btn25)
        Me.GroupControl2.Controls.Add(Me.lblWho)
        Me.GroupControl2.Controls.Add(Me.btnQuery)
        Me.GroupControl2.Controls.Add(Me.btn60)
        Me.GroupControl2.Controls.Add(Me.btn80)
        Me.GroupControl2.Controls.Add(Me.btn40)
        Me.GroupControl2.Controls.Add(Me.btn100)
        Me.GroupControl2.Controls.Add(Me.btnSend)
        Me.GroupControl2.Controls.Add(Me.btnRight)
        Me.GroupControl2.Controls.Add(Me.cbxSendIntray)
        Me.GroupControl2.Controls.Add(Me.btnLeft)
        Me.GroupControl2.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl2.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.ShowCaption = False
        Me.GroupControl2.Size = New System.Drawing.Size(795, 34)
        Me.GroupControl2.TabIndex = 0
        Me.GroupControl2.Text = "GroupControl2"
        '
        'btn25
        '
        Me.btn25.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn25.Location = New System.Drawing.Point(508, 6)
        Me.btn25.Name = "btn25"
        Me.btn25.Size = New System.Drawing.Size(40, 23)
        Me.btn25.TabIndex = 12
        Me.btn25.TabStop = False
        Me.btn25.Text = "25%"
        '
        'lblWho
        '
        Me.lblWho.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblWho.Appearance.Options.UseFont = True
        Me.lblWho.Appearance.Options.UseTextOptions = True
        Me.lblWho.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblWho.Location = New System.Drawing.Point(275, 10)
        Me.lblWho.Name = "lblWho"
        Me.lblWho.Size = New System.Drawing.Size(191, 15)
        Me.lblWho.TabIndex = 8
        Me.lblWho.Text = "Owner: Joe Bloggs, Tray: John Smith"
        Me.lblWho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btn60
        '
        Me.btn60.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn60.Location = New System.Drawing.Point(600, 6)
        Me.btn60.Name = "btn60"
        Me.btn60.Size = New System.Drawing.Size(40, 23)
        Me.btn60.TabIndex = 3
        Me.btn60.TabStop = False
        Me.btn60.Text = "60%"
        '
        'btn80
        '
        Me.btn80.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn80.Location = New System.Drawing.Point(646, 6)
        Me.btn80.Name = "btn80"
        Me.btn80.Size = New System.Drawing.Size(40, 23)
        Me.btn80.TabIndex = 7
        Me.btn80.TabStop = False
        Me.btn80.Text = "80%"
        '
        'btn40
        '
        Me.btn40.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn40.Location = New System.Drawing.Point(554, 6)
        Me.btn40.Name = "btn40"
        Me.btn40.Size = New System.Drawing.Size(40, 23)
        Me.btn40.TabIndex = 1
        Me.btn40.TabStop = False
        Me.btn40.Text = "40%"
        '
        'btn100
        '
        Me.btn100.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn100.Location = New System.Drawing.Point(692, 6)
        Me.btn100.Name = "btn100"
        Me.btn100.Size = New System.Drawing.Size(40, 23)
        Me.btn100.TabIndex = 5
        Me.btn100.TabStop = False
        Me.btn100.Text = "100%"
        '
        'btnRight
        '
        Me.btnRight.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRight.Location = New System.Drawing.Point(767, 6)
        Me.btnRight.Name = "btnRight"
        Me.btnRight.Size = New System.Drawing.Size(23, 23)
        Me.btnRight.TabIndex = 6
        Me.btnRight.TabStop = False
        Me.btnRight.Text = ">"
        '
        'btnLeft
        '
        Me.btnLeft.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnLeft.Location = New System.Drawing.Point(738, 6)
        Me.btnLeft.Name = "btnLeft"
        Me.btnLeft.Size = New System.Drawing.Size(23, 23)
        Me.btnLeft.TabIndex = 3
        Me.btnLeft.TabStop = False
        Me.btnLeft.Text = "<"
        '
        'tabViewDetail
        '
        Me.tabViewDetail.Controls.Add(Me.cgDetail)
        Me.tabViewDetail.Name = "tabViewDetail"
        Me.tabViewDetail.Size = New System.Drawing.Size(920, 595)
        Me.tabViewDetail.Text = "Detail View"
        '
        'cgDetail
        '
        Me.cgDetail.AllowBuildColumns = True
        Me.cgDetail.AllowEdit = False
        Me.cgDetail.AllowHorizontalScroll = False
        Me.cgDetail.AllowMultiSelect = False
        Me.cgDetail.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgDetail.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgDetail.Appearance.Options.UseFont = True
        Me.cgDetail.AutoSizeByData = True
        Me.cgDetail.DisableAutoSize = False
        Me.cgDetail.DisableDataFormatting = False
        Me.cgDetail.FocusedRowHandle = -2147483648
        Me.cgDetail.HideFirstColumn = False
        Me.cgDetail.Location = New System.Drawing.Point(3, 3)
        Me.cgDetail.Name = "cgDetail"
        Me.cgDetail.PreviewColumn = ""
        Me.cgDetail.QueryID = Nothing
        Me.cgDetail.RowAutoHeight = True
        Me.cgDetail.SearchAsYouType = True
        Me.cgDetail.ShowAutoFilterRow = True
        Me.cgDetail.ShowFindPanel = True
        Me.cgDetail.ShowGroupByBox = False
        Me.cgDetail.ShowLoadingPanel = False
        Me.cgDetail.ShowNavigator = True
        Me.cgDetail.Size = New System.Drawing.Size(914, 589)
        Me.cgDetail.TabIndex = 1
        '
        'frmScans
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(1184, 664)
        Me.Controls.Add(Me.ctView)
        Me.Controls.Add(Me.gbxHeader)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.gbxButtons)
        Me.Controls.Add(Me.gbxDetails)
        Me.Controls.Add(Me.gbxMatch)
        Me.Name = "frmScans"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.gbxHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxHeader.ResumeLayout(False)
        Me.gbxHeader.PerformLayout()
        CType(Me.radAnyStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radPaid.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radQAwaitingAuth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radQAuthorised.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radQMatched.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radQueried.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radQDuplicates.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radQScanned.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSupplier.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNet.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxWorkType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRef1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSendIntray.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRef.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxCategory.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radMatchPlant.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radMatchCategory.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radMatchJob.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxMatch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxMatch.ResumeLayout(False)
        Me.gbxMatch.PerformLayout()
        CType(Me.radLink.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxDetails.ResumeLayout(False)
        Me.gbxDetails.PerformLayout()
        CType(Me.cbxPaymentMethod.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDocRef.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkFastTrack.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkPrice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkUsedJob.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkPreNeg.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkDN.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDocDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDocDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxDocType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxButtons, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxButtons.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.chkTop20.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radSpecificTray.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSelectIntray.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radAllTrays.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radMyIntray.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radCompanyTray.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ctView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ctView.ResumeLayout(False)
        Me.tabViewImage.ResumeLayout(False)
        CType(Me.sccVertical, System.ComponentModel.ISupportInitialize).EndInit()
        Me.sccVertical.ResumeLayout(False)
        CType(Me.sccNotes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.sccNotes.ResumeLayout(False)
        CType(Me.sccPictureNotes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.sccPictureNotes.ResumeLayout(False)
        CType(Me.pic.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.txt9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbx9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt12.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbx12.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt11.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbx11.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbx10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbx5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbx8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbx7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbx6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbx1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbx4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbx3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbx2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        CType(Me.txtComments.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        Me.tabViewDetail.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxHeader As DevExpress.XtraEditors.GroupControl
    Friend WithEvents radQDuplicates As Care.Controls.CareRadioButton
    Friend WithEvents radQScanned As Care.Controls.CareRadioButton
    Friend WithEvents radQMatched As Care.Controls.CareRadioButton
    Friend WithEvents radQueried As Care.Controls.CareRadioButton
    Friend WithEvents cbxSendIntray As Care.Controls.CareComboBox
    Friend WithEvents btnSearch As Care.Controls.CareButton
    Friend WithEvents txtRef As Care.Controls.CareTextBox
    Friend WithEvents cbxCategory As Care.Controls.CareComboBox
    Friend WithEvents radMatchPlant As Care.Controls.CareRadioButton
    Friend WithEvents radMatchCategory As Care.Controls.CareRadioButton
    Friend WithEvents radMatchJob As Care.Controls.CareRadioButton
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents txtNet As Care.Controls.CareTextBox
    Friend WithEvents cbxWorkType As Care.Controls.CareComboBox
    Friend WithEvents txtRef1 As Care.Controls.CareTextBox
    Friend WithEvents cbxSupplier As Care.Controls.CareComboBox
    Friend WithEvents gbxMatch As DevExpress.XtraEditors.GroupControl
    Friend WithEvents gbxDetails As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnUpdate As Care.Controls.CareButton
    Friend WithEvents gbxButtons As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnAuthorise As Care.Controls.CareButton
    Friend WithEvents radQAwaitingAuth As Care.Controls.CareRadioButton
    Friend WithEvents radQAuthorised As Care.Controls.CareRadioButton
    Friend WithEvents cbxDocType As Care.Controls.CareComboBox
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents radMyIntray As Care.Controls.CareRadioButton
    Friend WithEvents radCompanyTray As Care.Controls.CareRadioButton
    Friend WithEvents ctView As Care.Controls.CareTab
    Friend WithEvents tabViewImage As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents sccVertical As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents cgDocuments As Care.Controls.CareGrid
    Friend WithEvents sccNotes As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnRight As Care.Controls.CareButton
    Friend WithEvents btnLeft As Care.Controls.CareButton
    Friend WithEvents tabViewDetail As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cgDetail As Care.Controls.CareGrid
    Friend WithEvents btnSend As Care.Controls.CareButton
    Friend WithEvents cdtDocDate As Care.Controls.CareDateTime
    Friend WithEvents chkPrice As Care.Controls.CareCheckBox
    Friend WithEvents chkUsedJob As Care.Controls.CareCheckBox
    Friend WithEvents chkPreNeg As Care.Controls.CareCheckBox
    Friend WithEvents chkDN As Care.Controls.CareCheckBox
    Friend WithEvents btnPassAuth As Care.Controls.CareButton
    Friend WithEvents btn100 As Care.Controls.CareButton
    Friend WithEvents btn80 As Care.Controls.CareButton
    Friend WithEvents btn40 As Care.Controls.CareButton
    Friend WithEvents btn60 As Care.Controls.CareButton
    Friend WithEvents btnDuplicate As Care.Controls.CareButton
    Friend WithEvents btnQuery As Care.Controls.CareButton
    Friend WithEvents btnDelete As Care.Controls.CareButton
    Friend WithEvents sccPictureNotes As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents pic As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel18 As Care.Controls.CareLabel
    Friend WithEvents txt9 As Care.Controls.CareTextBox
    Friend WithEvents cbx9 As Care.Controls.CareComboBox
    Friend WithEvents CareLabel19 As Care.Controls.CareLabel
    Friend WithEvents CareLabel20 As Care.Controls.CareLabel
    Friend WithEvents txt12 As Care.Controls.CareTextBox
    Friend WithEvents cbx12 As Care.Controls.CareComboBox
    Friend WithEvents CareLabel21 As Care.Controls.CareLabel
    Friend WithEvents CareLabel22 As Care.Controls.CareLabel
    Friend WithEvents txt11 As Care.Controls.CareTextBox
    Friend WithEvents cbx11 As Care.Controls.CareComboBox
    Friend WithEvents CareLabel23 As Care.Controls.CareLabel
    Friend WithEvents CareLabel24 As Care.Controls.CareLabel
    Friend WithEvents txt10 As Care.Controls.CareTextBox
    Friend WithEvents cbx10 As Care.Controls.CareComboBox
    Friend WithEvents CareLabel25 As Care.Controls.CareLabel
    Friend WithEvents CareLabel10 As Care.Controls.CareLabel
    Friend WithEvents txt5 As Care.Controls.CareTextBox
    Friend WithEvents cbx5 As Care.Controls.CareComboBox
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents CareLabel12 As Care.Controls.CareLabel
    Friend WithEvents txt8 As Care.Controls.CareTextBox
    Friend WithEvents cbx8 As Care.Controls.CareComboBox
    Friend WithEvents CareLabel13 As Care.Controls.CareLabel
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents txt7 As Care.Controls.CareTextBox
    Friend WithEvents cbx7 As Care.Controls.CareComboBox
    Friend WithEvents CareLabel15 As Care.Controls.CareLabel
    Friend WithEvents CareLabel16 As Care.Controls.CareLabel
    Friend WithEvents txt6 As Care.Controls.CareTextBox
    Friend WithEvents cbx6 As Care.Controls.CareComboBox
    Friend WithEvents CareLabel17 As Care.Controls.CareLabel
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents txt1 As Care.Controls.CareTextBox
    Friend WithEvents cbx1 As Care.Controls.CareComboBox
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents txt4 As Care.Controls.CareTextBox
    Friend WithEvents cbx4 As Care.Controls.CareComboBox
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents txt3 As Care.Controls.CareTextBox
    Friend WithEvents cbx3 As Care.Controls.CareComboBox
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents txt2 As Care.Controls.CareTextBox
    Friend WithEvents cbx2 As Care.Controls.CareComboBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents btnGeneratePriceNote As Care.Controls.CareButton
    Friend WithEvents btnPaid As Care.Controls.CareButton
    Friend WithEvents radAnyStatus As Care.Controls.CareRadioButton
    Friend WithEvents radPaid As Care.Controls.CareRadioButton
    Friend WithEvents radAllTrays As Care.Controls.CareRadioButton
    Friend WithEvents radSpecificTray As Care.Controls.CareRadioButton
    Friend WithEvents cbxSelectIntray As Care.Controls.CareComboBox
    Friend WithEvents btnPrint As Care.Controls.CareButton
    Friend WithEvents btnReturnToController As Care.Controls.CareButton
    Friend WithEvents btnKeith As Care.Controls.CareButton
    Friend WithEvents lblWho As Care.Controls.CareLabel
    Friend WithEvents btn25 As Care.Controls.CareButton
    Friend WithEvents txtComments As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cgComments As Care.Controls.CareGrid
    Friend WithEvents txtDocRef As Care.Controls.CareTextBox
    Friend WithEvents chkFastTrack As Care.Controls.CareCheckBox
    Friend WithEvents btnNoteSave As Care.Controls.CareButton
    Friend WithEvents btnNoteAdd As Care.Controls.CareButton
    Friend WithEvents btnNoteDelete As Care.Controls.CareButton
    Friend WithEvents chkTop20 As Care.Controls.CareCheckBox
    Friend WithEvents radLink As Care.Controls.CareRadioButton
    Friend WithEvents cbxPaymentMethod As Care.Controls.CareComboBox
    Friend WithEvents btnSplit As Care.Controls.CareButton
End Class
