﻿Option Strict On
Imports Care.Global
Imports DevExpress.XtraGrid.Views.Grid

Public Class frmCostEnquiry

    Private Sub frmCostEnquiry_Load(sender As Object, e As EventArgs) Handles Me.Load
        RefreshGrid()
    End Sub

    Private Sub cgCosts_GridDoubleClick(sender As Object, e As EventArgs) Handles cgCosts.GridDoubleClick

    End Sub

    Private Sub radCurrent_CheckedChanged(sender As Object, e As EventArgs) Handles radCurrent.CheckedChanged
        RefreshGrid()
    End Sub

    Private Sub RefreshGrid()

        Dim _SQL As String = ""
        Dim _TotalCosts As String = "isnull((select SUM(p.inv_value) from PurchaseDocuments p where p.target_id = j.ID),0) + isnull((select SUM(t.ts_cost) from Timesheets t where t.ts_job_id = j.ID),0)"

        _SQL += "select j.ID, job_no, j.sector, j.cont_code, rsk_fullname, rsk_address_1,"
        _SQL += vbCrLf
        _SQL += " (select top 1 t.ts_date from Timesheets t where t.ts_job_id = j.ID And t.ts_what <> 'Surveying' order by t.ts_date) as 'first_ts',"
        _SQL += vbCrLf
        _SQL += " (select top 1 p.inv_date from PurchaseDocuments p where p.target_id = j.ID order by p.inv_date) As 'first_mat',"
        _SQL += vbCrLf
        _SQL += " (select top 1 t.ts_date from Timesheets t where t.ts_job_id = j.ID order by t.ts_date desc) as 'last_ts',"
        _SQL += vbCrLf
        _SQL += " (select top 1 p.inv_date from PurchaseDocuments p where p.target_id = j.ID order by p.inv_date desc) As 'last_mat',"
        _SQL += vbCrLf
        _SQL += " isnull((select SUM(t.ts_cost) from Timesheets t where t.ts_job_id = j.ID),0) As 'labour',"
        _SQL += vbCrLf
        _SQL += " isnull((Select SUM(p.inv_value) from PurchaseDocuments p where p.target_id = j.ID),0) As 'materials',"
        _SQL += vbCrLf
        _SQL += " " + _TotalCosts + " As 'costs',"
        _SQL += vbCrLf
        _SQL += " isnull((Select SUM(s.inv_net) from JobInvSales s where s.job_id = j.ID),0) As 'invoiced',"
        _SQL += vbCrLf
        _SQL += " isnull((select SUM(s.inv_net) from JobInvSales s where s.job_id = j.ID),0)"
        _SQL += vbCrLf
        _SQL += " - (" + _TotalCosts + ") as 'invoiced_vs_cost',"
        _SQL += vbCrLf
        _SQL += " isnull((select top 1 v.ver_value from JobVersions v where v.job_id = j.ID order by v.ver_date desc),0) As 'version',"
        _SQL += vbCrLf
        _SQL += " isnull((Select SUM(s.inv_net) from JobInvSales s where s.job_id = j.ID),0)"
        _SQL += vbCrLf
        _SQL += " - isnull((select top 1 v.ver_value from JobVersions v where v.job_id = j.ID order by v.ver_date desc),0) as 'invoiced_vs_version'"
        _SQL += vbCrLf
        _SQL += " from Jobs j"
        _SQL += vbCrLf
        _SQL += " where job_status = 'Current'"
        _SQL += " and hide = 0"
        _SQL += vbCrLf
        _SQL += " order by job_no"

        cgCosts.Populate(Session.ConnectionString, _SQL)

        cgCosts.Columns("ID").Visible = False
        cgCosts.Columns("job_no").Caption = "Job No"
        cgCosts.Columns("sector").Caption = "Sector"
        cgCosts.Columns("cont_code").Caption = "Cont."
        cgCosts.Columns("rsk_fullname").Caption = "Name"
        cgCosts.Columns("rsk_address_1").Caption = "Address"
        cgCosts.Columns("first_ts").Caption = "1st Timesheet"
        cgCosts.Columns("first_mat").Caption = "1st Materials"
        cgCosts.Columns("last_ts").Caption = "Latest Timesheet"
        cgCosts.Columns("last_mat").Caption = "Latest Materials"
        cgCosts.Columns("labour").Caption = "Labour TD"
        cgCosts.Columns("labour").DisplayFormat.FormatString = "c2"
        cgCosts.Columns("materials").Caption = "Materials TD"
        cgCosts.Columns("materials").DisplayFormat.FormatString = "c2"
        cgCosts.Columns("costs").Caption = "Costs TD"
        cgCosts.Columns("costs").DisplayFormat.FormatString = "c2"
        cgCosts.Columns("invoiced").Caption = "Invoiced TD"
        cgCosts.Columns("invoiced").DisplayFormat.FormatString = "c2"
        cgCosts.Columns("invoiced_vs_cost").Caption = "Invoiced vs Cost"
        cgCosts.Columns("invoiced_vs_cost").DisplayFormat.FormatString = "c2"
        cgCosts.Columns("version").Caption = "Version"
        cgCosts.Columns("version").DisplayFormat.FormatString = "c2"
        cgCosts.Columns("invoiced_vs_version").Caption = "Invoiced vs Version"
        cgCosts.Columns("invoiced_vs_version").DisplayFormat.FormatString = "c2"

    End Sub

    Private Sub cgCosts_RowCellStyle(sender As Object, e As RowCellStyleEventArgs) Handles cgCosts.RowCellStyle

        If e.RowHandle < 0 Then Exit Sub

        Dim _Invoiced As Decimal = ValueHandler.ConvertDecimal(cgCosts.GetRowCellValue(e.RowHandle, "invoiced"))
        Dim _Cost As Decimal = ValueHandler.ConvertDecimal(cgCosts.GetRowCellValue(e.RowHandle, "costs"))
        Dim _Version As Decimal = ValueHandler.ConvertDecimal(cgCosts.GetRowCellValue(e.RowHandle, "version"))

        Select Case e.Column.FieldName

            Case "invoiced_vs_cost"
                If _Invoiced > _Cost Then
                    e.Appearance.BackColor = Color.LightGreen
                Else
                    e.Appearance.BackColor = Color.LightCoral
                End If

            Case Else
                e.Appearance.Reset()

        End Select

    End Sub

End Class