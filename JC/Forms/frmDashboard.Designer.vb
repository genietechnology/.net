﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDashboard
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.DashboardLabel1 = New Care.Controls.DashboardLabel()
        Me.DashboardLabel2 = New Care.Controls.DashboardLabel()
        Me.DashboardLabel3 = New Care.Controls.DashboardLabel()
        Me.DashboardLabel4 = New Care.Controls.DashboardLabel()
        Me.DashboardLabel5 = New Care.Controls.DashboardLabel()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.CareGrid1 = New Care.Controls.CareGrid()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 5
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.DashboardLabel1, 4, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.DashboardLabel2, 4, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.DashboardLabel3, 4, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.DashboardLabel4, 4, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.DashboardLabel5, 4, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.GroupControl1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.CareGrid1, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1052, 779)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'DashboardLabel1
        '
        Me.DashboardLabel1.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.DashboardLabel1.Appearance.Options.UseBackColor = True
        Me.DashboardLabel1.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.DashboardLabel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DashboardLabel1.LabelBottom = "Bottom"
        Me.DashboardLabel1.LabelMiddle = "55"
        Me.DashboardLabel1.LabelTop = "Top"
        Me.DashboardLabel1.Location = New System.Drawing.Point(843, 3)
        Me.DashboardLabel1.Name = "DashboardLabel1"
        Me.DashboardLabel1.Size = New System.Drawing.Size(206, 149)
        Me.DashboardLabel1.TabIndex = 0
        '
        'DashboardLabel2
        '
        Me.DashboardLabel2.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.DashboardLabel2.Appearance.Options.UseBackColor = True
        Me.DashboardLabel2.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.DashboardLabel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DashboardLabel2.LabelBottom = "Bottom"
        Me.DashboardLabel2.LabelMiddle = "55"
        Me.DashboardLabel2.LabelTop = "Top"
        Me.DashboardLabel2.Location = New System.Drawing.Point(843, 158)
        Me.DashboardLabel2.Name = "DashboardLabel2"
        Me.DashboardLabel2.Size = New System.Drawing.Size(206, 149)
        Me.DashboardLabel2.TabIndex = 1
        '
        'DashboardLabel3
        '
        Me.DashboardLabel3.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.DashboardLabel3.Appearance.Options.UseBackColor = True
        Me.DashboardLabel3.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.DashboardLabel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DashboardLabel3.LabelBottom = "Bottom"
        Me.DashboardLabel3.LabelMiddle = "55"
        Me.DashboardLabel3.LabelTop = "Top"
        Me.DashboardLabel3.Location = New System.Drawing.Point(843, 313)
        Me.DashboardLabel3.Name = "DashboardLabel3"
        Me.DashboardLabel3.Size = New System.Drawing.Size(206, 149)
        Me.DashboardLabel3.TabIndex = 2
        '
        'DashboardLabel4
        '
        Me.DashboardLabel4.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.DashboardLabel4.Appearance.Options.UseBackColor = True
        Me.DashboardLabel4.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.DashboardLabel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DashboardLabel4.LabelBottom = "Bottom"
        Me.DashboardLabel4.LabelMiddle = "55"
        Me.DashboardLabel4.LabelTop = "Top"
        Me.DashboardLabel4.Location = New System.Drawing.Point(843, 468)
        Me.DashboardLabel4.Name = "DashboardLabel4"
        Me.DashboardLabel4.Size = New System.Drawing.Size(206, 149)
        Me.DashboardLabel4.TabIndex = 3
        '
        'DashboardLabel5
        '
        Me.DashboardLabel5.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.DashboardLabel5.Appearance.Options.UseBackColor = True
        Me.DashboardLabel5.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.DashboardLabel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DashboardLabel5.LabelBottom = "Bottom"
        Me.DashboardLabel5.LabelMiddle = "55"
        Me.DashboardLabel5.LabelTop = "Top"
        Me.DashboardLabel5.Location = New System.Drawing.Point(843, 623)
        Me.DashboardLabel5.Name = "DashboardLabel5"
        Me.DashboardLabel5.Size = New System.Drawing.Size(206, 153)
        Me.DashboardLabel5.TabIndex = 4
        '
        'GroupControl1
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.GroupControl1, 4)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl1.Location = New System.Drawing.Point(3, 3)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(834, 149)
        Me.GroupControl1.TabIndex = 5
        Me.GroupControl1.Text = "GroupControl1"
        '
        'CareGrid1
        '
        Me.CareGrid1.AllowBuildColumns = True
        Me.CareGrid1.AllowEdit = False
        Me.CareGrid1.AllowHorizontalScroll = False
        Me.CareGrid1.AllowMultiSelect = False
        Me.CareGrid1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareGrid1.Appearance.Options.UseFont = True
        Me.CareGrid1.AutoSizeByData = True
        Me.TableLayoutPanel1.SetColumnSpan(Me.CareGrid1, 4)
        Me.CareGrid1.DisableAutoSize = False
        Me.CareGrid1.DisableDataFormatting = False
        Me.CareGrid1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CareGrid1.FocusedRowHandle = -2147483648
        Me.CareGrid1.HideFirstColumn = False
        Me.CareGrid1.Location = New System.Drawing.Point(3, 158)
        Me.CareGrid1.Name = "CareGrid1"
        Me.CareGrid1.PreviewColumn = ""
        Me.CareGrid1.QueryID = Nothing
        Me.CareGrid1.RowAutoHeight = False
        Me.TableLayoutPanel1.SetRowSpan(Me.CareGrid1, 4)
        Me.CareGrid1.SearchAsYouType = True
        Me.CareGrid1.ShowAutoFilterRow = False
        Me.CareGrid1.ShowFindPanel = False
        Me.CareGrid1.ShowGroupByBox = True
        Me.CareGrid1.ShowLoadingPanel = False
        Me.CareGrid1.ShowNavigator = False
        Me.CareGrid1.Size = New System.Drawing.Size(834, 618)
        Me.CareGrid1.TabIndex = 6
        '
        'frmDashboard
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1052, 779)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Margin = New System.Windows.Forms.Padding(3, 5, 3, 5)
        Me.Name = "frmDashboard"
        Me.Text = "frmDashboard"
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents DashboardLabel1 As Care.Controls.DashboardLabel
    Friend WithEvents DashboardLabel2 As Care.Controls.DashboardLabel
    Friend WithEvents DashboardLabel3 As Care.Controls.DashboardLabel
    Friend WithEvents DashboardLabel4 As Care.Controls.DashboardLabel
    Friend WithEvents DashboardLabel5 As Care.Controls.DashboardLabel
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareGrid1 As Care.Controls.CareGrid
End Class
