﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmJobKeyDates
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.cdtPaid = New Care.Controls.CareDateTime(Me.components)
        Me.cdtCompCertRcd = New Care.Controls.CareDateTime(Me.components)
        Me.cdtCompCertSent = New Care.Controls.CareDateTime(Me.components)
        Me.cdtCompletion = New Care.Controls.CareDateTime(Me.components)
        Me.cdtWorksStartDate = New Care.Controls.CareDateTime(Me.components)
        Me.cdtWorksOrderRcd = New Care.Controls.CareDateTime(Me.components)
        Me.cdtApproval = New Care.Controls.CareDateTime(Me.components)
        Me.cdtEstSub = New Care.Controls.CareDateTime(Me.components)
        Me.cdtSurvey = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.btnOK = New Care.Controls.CareButton(Me.components)
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.cdtPaid.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtPaid.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtCompCertRcd.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtCompCertRcd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtCompCertSent.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtCompCertSent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtCompletion.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtCompletion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtWorksStartDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtWorksStartDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtWorksOrderRcd.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtWorksOrderRcd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtApproval.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtApproval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtEstSub.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtEstSub.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtSurvey.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtSurvey.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.CareLabel7)
        Me.GroupControl1.Controls.Add(Me.CareLabel8)
        Me.GroupControl1.Controls.Add(Me.CareLabel9)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Controls.Add(Me.CareLabel5)
        Me.GroupControl1.Controls.Add(Me.CareLabel6)
        Me.GroupControl1.Controls.Add(Me.cdtPaid)
        Me.GroupControl1.Controls.Add(Me.cdtCompCertRcd)
        Me.GroupControl1.Controls.Add(Me.cdtCompCertSent)
        Me.GroupControl1.Controls.Add(Me.cdtCompletion)
        Me.GroupControl1.Controls.Add(Me.cdtWorksStartDate)
        Me.GroupControl1.Controls.Add(Me.cdtWorksOrderRcd)
        Me.GroupControl1.Controls.Add(Me.cdtApproval)
        Me.GroupControl1.Controls.Add(Me.cdtEstSub)
        Me.GroupControl1.Controls.Add(Me.cdtSurvey)
        Me.GroupControl1.Controls.Add(Me.CareLabel4)
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 9)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(294, 264)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "GroupControl1"
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(9, 236)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(50, 15)
        Me.CareLabel7.TabIndex = 16
        Me.CareLabel7.Text = "Paid Date"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(9, 208)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(170, 15)
        Me.CareLabel8.TabIndex = 14
        Me.CareLabel8.Text = "Completion Certificate Received"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(9, 180)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(146, 15)
        Me.CareLabel9.TabIndex = 12
        Me.CareLabel9.Text = "Completion Certificate Sent"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(9, 152)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(90, 15)
        Me.CareLabel1.TabIndex = 10
        Me.CareLabel1.Text = "Completion Date"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(9, 124)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(87, 15)
        Me.CareLabel5.TabIndex = 8
        Me.CareLabel5.Text = "Works Start Date"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(9, 96)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(116, 15)
        Me.CareLabel6.TabIndex = 6
        Me.CareLabel6.Text = "Works Order Received"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtPaid
        '
        Me.cdtPaid.EditValue = Nothing
        Me.cdtPaid.EnterMoveNextControl = True
        Me.cdtPaid.Location = New System.Drawing.Point(185, 233)
        Me.cdtPaid.Name = "cdtPaid"
        Me.cdtPaid.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtPaid.Properties.Appearance.Options.UseFont = True
        Me.cdtPaid.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtPaid.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtPaid.Size = New System.Drawing.Size(100, 22)
        Me.cdtPaid.TabIndex = 17
        Me.cdtPaid.Tag = "AE"
        Me.cdtPaid.Value = Nothing
        '
        'cdtCompCertRcd
        '
        Me.cdtCompCertRcd.EditValue = Nothing
        Me.cdtCompCertRcd.EnterMoveNextControl = True
        Me.cdtCompCertRcd.Location = New System.Drawing.Point(185, 205)
        Me.cdtCompCertRcd.Name = "cdtCompCertRcd"
        Me.cdtCompCertRcd.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtCompCertRcd.Properties.Appearance.Options.UseFont = True
        Me.cdtCompCertRcd.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtCompCertRcd.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtCompCertRcd.Size = New System.Drawing.Size(100, 22)
        Me.cdtCompCertRcd.TabIndex = 15
        Me.cdtCompCertRcd.Tag = "AE"
        Me.cdtCompCertRcd.Value = Nothing
        '
        'cdtCompCertSent
        '
        Me.cdtCompCertSent.EditValue = Nothing
        Me.cdtCompCertSent.EnterMoveNextControl = True
        Me.cdtCompCertSent.Location = New System.Drawing.Point(185, 177)
        Me.cdtCompCertSent.Name = "cdtCompCertSent"
        Me.cdtCompCertSent.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtCompCertSent.Properties.Appearance.Options.UseFont = True
        Me.cdtCompCertSent.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtCompCertSent.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtCompCertSent.Size = New System.Drawing.Size(100, 22)
        Me.cdtCompCertSent.TabIndex = 13
        Me.cdtCompCertSent.Tag = "AE"
        Me.cdtCompCertSent.Value = Nothing
        '
        'cdtCompletion
        '
        Me.cdtCompletion.EditValue = Nothing
        Me.cdtCompletion.EnterMoveNextControl = True
        Me.cdtCompletion.Location = New System.Drawing.Point(185, 149)
        Me.cdtCompletion.Name = "cdtCompletion"
        Me.cdtCompletion.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtCompletion.Properties.Appearance.Options.UseFont = True
        Me.cdtCompletion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtCompletion.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtCompletion.Size = New System.Drawing.Size(100, 22)
        Me.cdtCompletion.TabIndex = 11
        Me.cdtCompletion.Tag = "AE"
        Me.cdtCompletion.Value = Nothing
        '
        'cdtWorksStartDate
        '
        Me.cdtWorksStartDate.EditValue = Nothing
        Me.cdtWorksStartDate.EnterMoveNextControl = True
        Me.cdtWorksStartDate.Location = New System.Drawing.Point(185, 121)
        Me.cdtWorksStartDate.Name = "cdtWorksStartDate"
        Me.cdtWorksStartDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtWorksStartDate.Properties.Appearance.Options.UseFont = True
        Me.cdtWorksStartDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtWorksStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtWorksStartDate.Size = New System.Drawing.Size(100, 22)
        Me.cdtWorksStartDate.TabIndex = 9
        Me.cdtWorksStartDate.Tag = "AE"
        Me.cdtWorksStartDate.Value = Nothing
        '
        'cdtWorksOrderRcd
        '
        Me.cdtWorksOrderRcd.EditValue = Nothing
        Me.cdtWorksOrderRcd.EnterMoveNextControl = True
        Me.cdtWorksOrderRcd.Location = New System.Drawing.Point(185, 93)
        Me.cdtWorksOrderRcd.Name = "cdtWorksOrderRcd"
        Me.cdtWorksOrderRcd.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtWorksOrderRcd.Properties.Appearance.Options.UseFont = True
        Me.cdtWorksOrderRcd.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtWorksOrderRcd.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtWorksOrderRcd.Size = New System.Drawing.Size(100, 22)
        Me.cdtWorksOrderRcd.TabIndex = 7
        Me.cdtWorksOrderRcd.Tag = "AE"
        Me.cdtWorksOrderRcd.Value = Nothing
        '
        'cdtApproval
        '
        Me.cdtApproval.EditValue = Nothing
        Me.cdtApproval.EnterMoveNextControl = True
        Me.cdtApproval.Location = New System.Drawing.Point(185, 65)
        Me.cdtApproval.Name = "cdtApproval"
        Me.cdtApproval.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtApproval.Properties.Appearance.Options.UseFont = True
        Me.cdtApproval.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtApproval.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtApproval.Size = New System.Drawing.Size(100, 22)
        Me.cdtApproval.TabIndex = 5
        Me.cdtApproval.Tag = "AE"
        Me.cdtApproval.Value = Nothing
        '
        'cdtEstSub
        '
        Me.cdtEstSub.EditValue = Nothing
        Me.cdtEstSub.EnterMoveNextControl = True
        Me.cdtEstSub.Location = New System.Drawing.Point(185, 37)
        Me.cdtEstSub.Name = "cdtEstSub"
        Me.cdtEstSub.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtEstSub.Properties.Appearance.Options.UseFont = True
        Me.cdtEstSub.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtEstSub.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtEstSub.Size = New System.Drawing.Size(100, 22)
        Me.cdtEstSub.TabIndex = 3
        Me.cdtEstSub.Tag = "AE"
        Me.cdtEstSub.Value = Nothing
        '
        'cdtSurvey
        '
        Me.cdtSurvey.EditValue = Nothing
        Me.cdtSurvey.EnterMoveNextControl = True
        Me.cdtSurvey.Location = New System.Drawing.Point(185, 9)
        Me.cdtSurvey.Name = "cdtSurvey"
        Me.cdtSurvey.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtSurvey.Properties.Appearance.Options.UseFont = True
        Me.cdtSurvey.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtSurvey.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtSurvey.Size = New System.Drawing.Size(100, 22)
        Me.cdtSurvey.TabIndex = 1
        Me.cdtSurvey.Tag = "AE"
        Me.cdtSurvey.Value = Nothing
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(9, 68)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(75, 15)
        Me.CareLabel4.TabIndex = 4
        Me.CareLabel4.Text = "Approval Date"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(9, 40)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(136, 15)
        Me.CareLabel3.TabIndex = 2
        Me.CareLabel3.Text = "Estimate Submission Date"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(9, 12)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(62, 15)
        Me.CareLabel2.TabIndex = 0
        Me.CareLabel2.Text = "Survey Date"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(130, 280)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(85, 23)
        Me.btnOK.TabIndex = 1
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(221, 280)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 23)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "Cancel"
        '
        'frmJobKeyDates
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(319, 311)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.GroupControl1)
        Me.Margin = New System.Windows.Forms.Padding(3, 5, 3, 5)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmJobKeyDates"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = ""
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.cdtPaid.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtPaid.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtCompCertRcd.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtCompCertRcd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtCompCertSent.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtCompCertSent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtCompletion.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtCompletion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtWorksStartDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtWorksStartDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtWorksOrderRcd.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtWorksOrderRcd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtApproval.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtApproval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtEstSub.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtEstSub.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtSurvey.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtSurvey.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cdtSurvey As Care.Controls.CareDateTime
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents cdtPaid As Care.Controls.CareDateTime
    Friend WithEvents cdtCompCertRcd As Care.Controls.CareDateTime
    Friend WithEvents cdtCompCertSent As Care.Controls.CareDateTime
    Friend WithEvents cdtCompletion As Care.Controls.CareDateTime
    Friend WithEvents cdtWorksStartDate As Care.Controls.CareDateTime
    Friend WithEvents cdtWorksOrderRcd As Care.Controls.CareDateTime
    Friend WithEvents cdtApproval As Care.Controls.CareDateTime
    Friend WithEvents cdtEstSub As Care.Controls.CareDateTime
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents btnCancel As Care.Controls.CareButton
End Class
