﻿Imports Care.Global
Imports Care.Shared

Public Class frmInvoiceManual

    Private m_New As Boolean = False
    Private m_JobID As Guid = Nothing
    Private m_Invoice As New Business.JobInvSale

    Public Sub New(ByVal JobID As Guid, ByVal InvoiceID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_JobID = JobID

        If InvoiceID.HasValue Then
            Me.Text = "Amend Invoice"
            m_Invoice = Business.JobInvSale.RetreiveByID(InvoiceID.Value)
        Else
            Me.Text = "Create New Invoice"
            m_New = True
        End If

    End Sub

    Private Sub frmInvoice_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If m_New Then
            MyControls.SetControls(ControlHandler.Mode.Add, Me.Controls)
        Else
            MyControls.SetControls(ControlHandler.Mode.Edit, Me.Controls)
            DisplayRecord
        End If
    End Sub

    Private Sub DisplayRecord()
        txtInvoiceNo.Text = m_Invoice._InvNo
        cdtInvoiceDate.Value = m_Invoice._InvDate
        txtInvoiceText.Text = m_Invoice._InvNotes
        txtNet.Text = ValueHandler.MoneyAsText(m_Invoice._InvNet)
    End Sub

    Private Sub SaveInvoice()

        With m_Invoice

            If m_New Then
                ._JobId = m_JobID
                ._InvUser = Session.CurrentUser.UserCode
                ._InvStamp = Now
            End If

            ._InvNo = txtInvoiceNo.Text
            ._InvDate = cdtInvoiceDate.Value
            ._InvNotes = txtInvoiceText.Text
            ._InvNet = ValueHandler.ConvertDecimal(txtNet.Text)

            .Store()

        End With

    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnCreate_Click(sender As Object, e As EventArgs) Handles btnCreate.Click
        If MyControls.Validate(Me.Controls) Then
            SaveInvoice()
            Me.DialogResult = DialogResult.OK
            Me.Close()
        End If
    End Sub
End Class