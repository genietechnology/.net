﻿Imports Care.Global
Imports Care.Shared

Public Class frmContact
    Private m_New As Boolean = True
    Private m_ContactID As Guid? = Nothing
    Private m_Contact As New Business.Contact

    Public ReadOnly Property ReturnContactID As Guid?
        Get
            Return m_ContactID
        End Get
    End Property

    Public Sub New(ByVal ContactID As Guid?, ByVal EditMode As Boolean)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_ContactID = ContactID

        If m_ContactID.HasValue Then
            Me.Text = "Edit Contact"
            DisplayRecord()
            m_New = False
        Else
            Me.Text = "Create New Contact"
            m_ContactID = Guid.NewGuid
        End If

    End Sub

    Private Sub frmContact_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MyControls.SetControls(ControlHandler.Mode.Edit, Me.Controls)
    End Sub

    Private Sub DisplayRecord()

        m_Contact = Business.Contact.RetreiveByID(m_ContactID.Value)
        With m_Contact

            txtForename.Text = ._Forename
            txtSurname.Text = ._Surname
            txtFullname.Text = ._Fullname
            txtRelationship.Text = ._Relationship

            txtCompany.Text = ._Company
            txtJobTitle.Text = ._Jobtitle

            txtTelHome.Text = ._TelHome
            txtTelMobile.Text = ._TelMobile
            txtTelWork.Text = ._TelWork
            txtTelOther.Text = ._TelOther

            txtEmail.Text = ._Email
            txtEmailOther.Text = ._EmailOther

        End With

    End Sub

    Private Sub SaveContact()

        With m_Contact

            If m_New Then ._ID = m_ContactID

            ._Forename = txtForename.Text
            ._Surname = txtSurname.Text
            ._Fullname = ._Forename + " " + ._Surname

            ._Relationship = txtRelationship.Text

            ._Company = txtCompany.Text
            ._Jobtitle = txtJobTitle.Text

            ._TelHome = txtTelHome.Text
            ._TelMobile = txtTelMobile.Text
            ._TelWork = txtTelWork.Text
            ._TelOther = txtTelOther.Text

            ._TelAll = ""

            If ._TelHome <> "" Then
                ._TelAll += "H: " + ._TelHome
            End If

            If ._TelMobile <> "" Then
                If ._TelAll = "" Then
                    ._TelAll += "M: " + ._TelMobile
                Else
                    ._TelAll += " M: " + ._TelMobile
                End If
            End If

            If ._TelWork <> "" Then
                If ._TelAll = "" Then
                    ._TelAll += "W: " + ._TelWork
                Else
                    ._TelAll += " W: " + ._TelWork
                End If
            End If

            If ._TelOther <> "" Then
                If ._TelAll = "" Then
                    ._TelAll += "O: " + ._TelOther
                Else
                    ._TelAll += " O: " + ._TelOther
                End If
            End If

            ._Email = txtEmail.Text
            ._EmailOther = txtEmailOther.Text

            .Store()

        End With

    End Sub

    Private Sub frmContact_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        txtForename.Focus()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        If MyControls.Validate(Me.Controls) Then
            SaveContact
            Me.DialogResult = DialogResult.OK
            Me.Close()
        End If
    End Sub
End Class