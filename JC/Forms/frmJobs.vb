﻿Option Strict On

Imports Care.Global
Imports Care.Data
Imports Care.Shared
Imports DevExpress.XtraTab

Public Class frmJobs

    Private m_Job As Business.Job
    Private m_JobPath As String = ""
    Private m_JobIDQuoted As String = ""
    Private m_PicFolders As New Dictionary(Of String, String)
    Private m_PicFiles As New Dictionary(Of String, String)
    Private m_DrillDownID As Guid? = Nothing

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub New(ByVal JobID As Guid)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_DrillDownID = JobID

    End Sub

    Private Sub frmJobs_Load(sender As Object, e As EventArgs) Handles Me.Load

        PopulateLists()
        lblOnHold.Hide()

        icoRetentions.Icon = CareIcon.EnumIcon.Blank
        icoValuations.Icon = CareIcon.EnumIcon.Blank

        SetCMDs(False)
        btnCopyRisk.Enabled = False
        cgContacts.ButtonsEnabled = True

        If m_DrillDownID.HasValue Then
            DisplayRecord(m_DrillDownID.Value)
            ctMain.EnableTabs()
        Else
            ctMain.DisableTabs()
        End If

    End Sub

    Private Sub SetCMDs(ByVal Enabled As Boolean)
        btnWorkFile.Enabled = Enabled
        btnPrint.Enabled = Enabled
        btnKeyDates.Enabled = Enabled
        btnFunctions.Enabled = Enabled
    End Sub

    Private Sub PopulateLists()

        cbxRiskTitle.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Titles"))
        cbxCustTitle.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Titles"))

        cbxSector.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("SECTORS"))
        cbxArea.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("AREA CODES"))
        cbxController.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("CONTROLLERS"))
        cbxLossAdj.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("LOSS ADJUSTERS"))

        cbxInsurer.PopulateWithSQL(Session.ConnectionString, "select ins_name from Insurers order by ins_name")

    End Sub

    Protected Overrides Sub SetBindings()

        m_Job = New Business.Job
        bs.DataSource = m_Job

        '******************************************************************************************************************************
        txtNo.DataBindings.Add("Text", bs, "_JobNo")
        cbxArea.DataBindings.Add("Text", bs, "_Area")
        cbxController.DataBindings.Add("Text", bs, "_ContName")

        cbxInsurer.DataBindings.Add("Text", bs, "_InsCompany")
        cbxLossAdj.DataBindings.Add("Text", bs, "_LossAdj")
        txtInsJob.DataBindings.Add("Text", bs, "_InsRef")
        txtInsClaim.DataBindings.Add("Text", bs, "_InsClaim")

        cdtStart.DataBindings.Add("Value", bs, "_JobStart", True)

        '******************************************************************************************************************************

    End Sub

    Protected Overrides Sub FindRecord()

        Dim _ReturnValue As String = Business.Job.FindAll()

        If _ReturnValue <> "" Then
            DisplayRecord(New Guid(_ReturnValue))
            ctMain.EnableTabs()
        End If

    End Sub

    Private Sub BuildTileBar()
        Me.Text = "Job (" + m_Job._JobNo.ToString + ") " + m_Job._RskFullname + ", " + m_Job._RskAddress1 + " - " + m_Job._JobStatus
    End Sub

    Protected Overrides Sub AfterAdd()
        Me.Text = "Create New Job"
        cbxSector.Text = "Private - Building"
        txtStatus.Text = "CARRY OUT SURVEY"
        btnCopyRisk.Enabled = True
        cgContacts.Clear()
        cgContacts.ButtonsEnabled = False
    End Sub

    Protected Overrides Sub AfterEdit()
        btnCopyRisk.Enabled = True
        cgVersions.ButtonsEnabled = True
        cgSalesInvoices.ButtonsEnabled = True
        cgContacts.ButtonsEnabled = True
    End Sub

    Protected Overrides Sub AfterAcceptChanges()
        AfterCommitOrCancel()
    End Sub

    Protected Overrides Sub AfterCancelChanges()
        AfterCommitOrCancel()
    End Sub

    Private Sub AfterCommitOrCancel()
        BuildTileBar()
        btnCopyRisk.Enabled = False
        cgSalesInvoices.ButtonsEnabled = False
        cgVersions.ButtonsEnabled = False
    End Sub

    Private Sub DisplayRecord(ByVal JobID As Guid)

        m_JobIDQuoted = "'" + JobID.ToString + "'"

        MyBase.RecordID = JobID
        MyBase.RecordPopulated = True

        m_Job = Business.Job.RetreiveByID(JobID)
        bs.DataSource = m_Job

        m_JobPath = m_Job._JobFolder

        tabLinks.PageVisible = m_Job.IsLinked

        BuildTileBar()
        DisplayTab(ctMain.SelectedTabPage.Name)

        SetCMDs(True)

    End Sub

    Private Sub DisplayJob()

        With m_Job

            lblOnHold.Visible = ._Hold

            If ._Hold Then
                lblOnHold.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Exclamation
                lblOnHold.ToolTipTitle = "On Hold from " + Format(._HoldDate, "dd/MM/yy")
                lblOnHold.ToolTip = ._HoldReason
            Else
                lblOnHold.ToolTipIconType = DevExpress.Utils.ToolTipIconType.None
                lblOnHold.ToolTipTitle = ""
                lblOnHold.ToolTip = ""
            End If

            cbxSector.Text = ._Sector
            txtStatus.Text = ._JobStatus

            If ._KeyWorksorder.HasValue Then
                txtWORcd.Text = Format(._KeyWorksorder, "dd/MM/yy")
                lblWorksOrder.ForeColor = Color.Black
            Else
                txtWORcd.Text = ""
                lblWorksOrder.ForeColor = Color.Red
            End If

            If ._KeyCcSent.HasValue Then
                If ._KeyCcRcd.HasValue Then
                    txtCompCert.Text = Format(._KeyCcRcd, "dd/MM/yy")
                    lblCompCert.ForeColor = Color.Black
                Else
                    txtCompCert.Text = "Sent-" + Format(._KeyCcSent, "dd/MM/yy")
                    lblCompCert.ForeColor = Color.Red
                End If
            Else
                txtCompCert.Text = ""
                lblCompCert.ForeColor = Color.Black
            End If

            cbxRiskTitle.Text = ._RskTitle
            txtRiskForename.Text = ._RskForename
            txtRiskSurname.Text = ._RskSurname
            txtRiskAddress.Text = ._RskAddress

            cbxCustTitle.Text = ._CusTitle
            txtCustForename.Text = ._CusForename
            txtCustSurname.Text = ._CusSurname
            txtCustAddress.Text = ._CusAddress

        End With

        DisplayCosts()
        DryingPrompt()
        DisplayContacts()

    End Sub

    Private Sub DisplayCosts()

        m_Job.CalculateCosts()

        txtLabCount.EditValue = m_Job.LabourHours
        txtLabSub.EditValue = m_Job.LabourCost
        txtLabTotal.EditValue = m_Job.LabourTotalOn

        txtMatCount.EditValue = m_Job.PurchasesCount
        txtMatSub.EditValue = m_Job.PurchasesCost
        txtMatTotal.EditValue = m_Job.PurchasesTotalOn

        txtYardCount.EditValue = m_Job.YardMaterialsCount
        txtYardSub.EditValue = m_Job.YardMaterialsCost
        txtYardTotal.EditValue = m_Job.YardMaterialsTotalOn

        txtConCount.EditValue = m_Job.ContractorsCount
        txtConSub.EditValue = m_Job.ContractorsCost
        txtConTotal.EditValue = m_Job.ContractorsTotalOn

        txtSubTotal.EditValue = m_Job.TotalCost
        txtTotalTotal.EditValue = m_Job.TotalCostOn

        txtOverhead.EditValue = m_Job.Leakage
        txtInvoiced.EditValue = m_Job.InvoiceTotal
        txtVersion.EditValue = m_Job.VersionTotal
        txtGrandTotal.EditValue = m_Job.GrandTotal

        Dim _VersionVsGrandTotal As Decimal = m_Job.VersionTotal - m_Job.GrandTotal
        txtGrandTotal.ForeColor = ValueHandler.ReturnValueColour(_VersionVsGrandTotal, False)

        Dim _CostVsInvoiced As Decimal = m_Job.InvoiceTotal - m_Job.GrandTotal
        txtInvoiced.ForeColor = ValueHandler.ReturnValueColour(_CostVsInvoiced, False)

    End Sub

    Private Sub DryingPrompt()

        If m_Job._Dc Then

            'approval date must be entered
            If m_Job._KeyApproval.HasValue Then

                'certificate has not been received
                If m_Job._DcRecd = "" OrElse m_Job._DcRecd = "N" Then

                    lblDryCert.ForeColor = Color.Red
                    txtDryCert.Text = "Not Received"

                    Dim _Mess As String
                    _Mess = "Has a drying certificate been received for this Job?" & vbCrLf & vbCrLf &
                              "Click Yes to mark the certificate as received." & vbCrLf &
                              "Click No to mark the certificate as not received." & vbCrLf &
                              "Click Cancel to mark the certificate as not applicable."

                    Select Case CareMessage(_Mess, MessageBoxIcon.Question, MessageBoxButtons.YesNoCancel, "Drying Certificate")

                        Case System.Windows.Forms.DialogResult.Yes
                            m_Job._DcRecd = "Y"

                        Case System.Windows.Forms.DialogResult.No
                            m_Job._DcRecd = "N"

                        Case System.Windows.Forms.DialogResult.Cancel
                            m_Job._DcRecd = "NA"

                    End Select

                    m_Job._DcAskUser = Session.CurrentUser.UserCode
                    m_Job._DcAskStamp = Now
                    m_Job.Store()

                Else
                    lblDryCert.ForeColor = Color.Black
                    txtDryCert.Text = Format(m_Job._DcAskStamp, "dd/MM/yyyy")
                End If

            End If

        Else
            lblDryCert.ForeColor = Color.Black
            txtDryCert.Text = "Not Required"
        End If

    End Sub

    Private Sub CheckForLinkedJobs()
        If m_Job.IsLinked Then

        End If
    End Sub

    Private Sub ctMain_SelectedPageChanging(sender As Object, e As TabPageChangingEventArgs) Handles ctMain.SelectedPageChanging
        DisplayTab(e.Page.Name)
    End Sub

    Private Sub DisplayTab(ByVal TabPageName As String)

        If m_Job Is Nothing Then Exit Sub
        Select Case TabPageName

            Case "tabMain"
                Me.ToolbarMode = ToolbarEnum.Standard
                DisplayJob()

            Case "tabLinks"
                Me.ToolbarMode = ToolbarEnum.FindOnly
                DisplayLinksGrid()

            Case "tabDescriptions"
                Me.ToolbarMode = ToolbarEnum.FindandEdit
                DisplayDescriptions()

            Case "tabCalendar"
                Me.ToolbarMode = ToolbarEnum.FindandEdit
                yvCalendar.Populate(m_Job._ID.Value)

            Case "tabVersions"
                Me.ToolbarMode = ToolbarEnum.FindandEdit
                DisplayVersionsGrid()

            Case "tabMaterials"
                Me.ToolbarMode = ToolbarEnum.FindOnly
                DisplayMaterialsGrid()

            Case "tabPlant"
                Me.ToolbarMode = ToolbarEnum.FindandEdit
                DisplayPlantGrid()

            Case "tabHire"
                Me.ToolbarMode = ToolbarEnum.FindandEdit
                DisplayHireGrid()

            Case "tabLabour"
                Me.ToolbarMode = ToolbarEnum.FindOnly
                DisplayLabourGrid()

            Case "tabInvoices"
                Me.ToolbarMode = ToolbarEnum.FindandEdit
                DisplaySalesInvoices()

            Case "tabNotes"
                Me.ToolbarMode = ToolbarEnum.FindandEdit
                txtNotes.Text = m_Job._Notes

            Case "tabActivity"
                DisplayActivity()
                Me.ToolbarMode = ToolbarEnum.FindOnly

            Case "tabPhotos"
                Me.ToolbarMode = ToolbarEnum.FindOnly
                PopulatePicFolders()

        End Select

    End Sub

    Protected Overrides Function BeforeCommitUpdate() As Boolean

        'carry out other mandatory checking (insurance etc)

        Return True

    End Function

    Protected Overrides Sub CommitUpdate()

        Select Case ctMain.SelectedTabPage.Name

            Case "tabMain"
                CommitJob()

            Case "tabDescriptions"
                CommitDescriptions()

            Case "tabNotes"
                If m_Job IsNot Nothing Then
                    m_Job._Notes = txtNotes.Text
                    m_Job.Store()
                End If

        End Select

    End Sub

    Private Sub CommitDescriptions()

        m_Job = CType(bs.Item(bs.Position), Business.Job)

        If radDescJob.Checked Then m_Job._DescJob = rich.RTFText
        If radDescDirections.Checked Then m_Job._DescDirections = rich.RTFText
        If radDescEst.Checked Then m_Job._DescEst = rich.RTFText
        If radDescTools.Checked Then m_Job._DescTools = rich.RTFText

        m_Job.Store()

    End Sub

    Private Sub CommitJob()

        m_Job = CType(bs.Item(bs.Position), Business.Job)

        m_Job._JobStatus = txtStatus.Text
        m_Job._Sector = cbxSector.Text

        m_Job._RskAddress = txtRiskAddress.Text
        m_Job._RskTitle = cbxRiskTitle.Text
        m_Job._RskForename = txtRiskForename.Text
        m_Job._RskSurname = txtRiskSurname.Text

        m_Job._CusAddress = txtCustAddress.Text
        m_Job._CusTitle = cbxCustTitle.Text
        m_Job._CusForename = txtCustForename.Text
        m_Job._CusSurname = txtCustSurname.Text

        If Mode = "ADD" Then

            m_Job._ID = Guid.NewGuid

            m_Job._JobStatusCode = "CS"
            m_Job._JobStatus = "CARRY OUT SURVEY"

            'get the job number
            m_Job._JobNo = Business.Job.NextJobNumber

            'copy the risk address to the property if blank
            If m_Job._CusSurname = "" AndAlso m_Job._CusAddress = "" Then
                m_Job._CusAddress = m_Job._RskAddress
                m_Job._CusTitle = m_Job._RskTitle
                m_Job._CusForename = m_Job._RskForename
                m_Job._CusSurname = m_Job._RskSurname
            End If

            'mark-ups from parameters
            m_Job._MupLab = ParameterHandler.ReturnDecimal("MUPLABOUR")
            m_Job._MupMat = ParameterHandler.ReturnDecimal("MUPMATERIALS")
            m_Job._MupSub = ParameterHandler.ReturnDecimal("MUPSUB")
            m_Job._MupLeak = ParameterHandler.ReturnDecimal("MUPLEAK")

            'update the activity
            Business.Job.CreateActivity(m_Job._ID.Value, "Job Created")

            'create the folders
            Dim _CleanSurname As String = m_Job._RskSurname.Replace("'", "")
            Dim _Path As String = "\\aj-svr-01\Data\Job Files\" + m_Job._JobNo.ToString + "_" + _CleanSurname
            m_Job._JobFolder = _Path

            m_Job.CreateFolders(_Path)

        End If

        're-write all the addresses (i.e. line version and address line 1)
        m_Job.SetNamesAndAddresses()
        m_Job.GenerateTelephoneNumbers(Business.Contact.RetreiveByJobID(m_Job._ID.Value))

        m_Job.Store()

    End Sub

#Region "Grids"

    Private Sub DisplayContacts()

        Dim _SQL As String = "select jc.ID, c.ID as 'Contact_ID', c.fullname as 'Name', c.tel_all as 'Contact Numbers', c.relationship as 'Relationship',"
        _SQL += " CAST(CASE WHEN (len(rtrim(c.email)) > 0) THEN 1 ELSE 0 END AS BIT) as 'Email',"
        _SQL += " JC.c_primary as 'Pri', jc.c_billing as 'Bill'"
        _SQL += " from JobContacts jc"
        _SQL += " left join Contacts c on C.ID = jc.contact_id"
        _SQL += " where jc.job_id = " + m_JobIDQuoted
        _SQL += " order by jc.c_primary desc, c.forename, jc.old_name"

        cgContacts.HideFirstColumn = True
        cgContacts.Populate(Session.ConnectionString, _SQL)

        cgContacts.Columns("Contact_ID").Visible = False

        cgContacts.AutoSizeColumns()

    End Sub

    Private Sub DisplayActivity()

        Dim _SQL As String = ""
        _SQL += "select ID, key_date as 'Key Date', activity as 'Description', user_name as 'User', stamp as 'Stamp'"
        _SQL += " from JobActivity"
        _SQL += " where job_id = " + m_JobIDQuoted
        _SQL += " order by stamp desc"

        cgActivity.HideFirstColumn = True
        cgActivity.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub DisplaySalesInvoices()

        Dim _SQL As String = ""
        _SQL += "select ID, inv_date, inv_no, inv_net, inv_user, inv_stamp, inv_notes"
        _SQL += " from JobInvSales"
        _SQL += " where job_id = " + m_JobIDQuoted
        _SQL += " order by inv_stamp desc"

        cgSalesInvoices.HideFirstColumn = True
        cgSalesInvoices.Populate(Session.ConnectionString, _SQL)

        cgSalesInvoices.Columns("inv_date").Caption = "Date"
        cgSalesInvoices.Columns("inv_no").Caption = "Invoice No"
        cgSalesInvoices.Columns("inv_net").Caption = "Value"
        cgSalesInvoices.Columns("inv_user").Caption = "User"
        cgSalesInvoices.Columns("inv_stamp").Caption = "Stamp"
        cgSalesInvoices.Columns("inv_notes").Visible = False
        cgSalesInvoices.PreviewColumn = "inv_notes"

    End Sub

    Private Sub DisplayLinksGrid()

        Dim _SQL As String = ""
        _SQL += "select l.ID, j.job_no as 'Job No', j.job_status as 'Status', j.rsk_fullname as 'Risk Name', j.rsk_address_1 as 'Risk Address',"
        _SQL += " j.cus_fullname as 'Customer Name', j.cus_address_1 as 'Customer Address'"
        _SQL += " from JobLinks l"
        _SQL += " left join Jobs j on j.ID = l.linked_job_id"
        _SQL += " where l.job_id = " + m_JobIDQuoted
        _SQL += " order by j.job_no desc"

        cgLinked.HideFirstColumn = True
        cgLinked.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub DisplayVersionsGrid()

        Dim _SQL As String = ""
        _SQL += "select ID, ver_date, ver_reason, ver_value, ver_user, ver_stamp, ver_notes"
        _SQL += " from JobVersions"
        _SQL += " where job_id = " + m_JobIDQuoted
        _SQL += " order by ver_stamp desc"

        cgVersions.HideFirstColumn = True
        cgVersions.Populate(Session.ConnectionString, _SQL)

        cgVersions.Columns("ver_date").Caption = "Date"
        cgVersions.Columns("ver_reason").Caption = "Reason"
        cgVersions.Columns("ver_value").Caption = "Value"
        cgVersions.Columns("ver_user").Caption = "User"
        cgVersions.Columns("ver_stamp").Caption = "Stamp"
        cgVersions.Columns("ver_notes").Visible = False
        cgVersions.PreviewColumn = "ver_notes"

    End Sub

    Private Sub DisplayMaterialsGrid()

        Dim _SQL As String = ""
        _SQL += "select ID, inv_date, inv_supp_name, inv_category, inv_entered_user, inv_entered_stamp, inv_value, doc_id,"
        _SQL += " cast((select case when doc_id is null then 0 else 1 end) as bit) as 'has_doc'"
        _SQL += " from PurchaseDocuments"
        _SQL += " where target_type = 'Job'"
        _SQL += " and target_id = " + m_JobIDQuoted

        If radInvSup.Checked Then _SQL += " and inv_type = 'I'"
        If radInvSupCred.Checked Then _SQL += " and inv_type = 'C'"
        If radInvCont.Checked Then _SQL += " and inv_type = 'CI'"
        If radInvContCred.Checked Then _SQL += " and inv_type = 'CC'"

        _SQL += " order by inv_date desc"

        cgMaterials.HideFirstColumn = True
        cgMaterials.Populate(Session.ConnectionString, _SQL)

        If cgMaterials.RecordCount > 0 Then
            cgMaterials.Columns("inv_date").Caption = "Date"
            cgMaterials.Columns("inv_supp_name").Caption = "Supplier"
            cgMaterials.Columns("inv_category").Caption = "Description"
            cgMaterials.Columns("inv_entered_user").Caption = "Entered by"
            cgMaterials.Columns("inv_entered_stamp").Caption = "Entered Stamp"
            cgMaterials.Columns("inv_value").Caption = "Invoice Value"
            cgMaterials.Columns("has_doc").Caption = "Scanned"
            cgMaterials.Columns("doc_id").Visible = False
        End If

        btnMatMove.Enabled = cgMaterials.RecordCount > 0
        btnMatDelete.Enabled = cgMaterials.RecordCount > 0

    End Sub

    Private Sub DisplayLabourGrid()

        Dim _SQL As String = ""
        _SQL += "select t.ID, ts_date, e.fullname, ts_what, ts_day_start, ts_job_finish, ts_day_finish, ts_day_duration, ts_cost"
        _SQL += " from Timesheets t"
        _SQL += " left join Employees e on e.id = t.ts_emp_id"
        _SQL += " where ts_job_id = " + m_JobIDQuoted
        _SQL += " order by ts_date desc"

        cgLabour.HideFirstColumn = True
        cgLabour.Populate(Session.ConnectionString, _SQL)

        cgLabour.Columns("ts_date").Caption = "Date"
        cgLabour.Columns("fullname").Caption = "Employee"
        cgLabour.Columns("ts_what").Caption = "Task"
        cgLabour.Columns("ts_day_start").Caption = "Started"
        cgLabour.Columns("ts_job_finish").Caption = "Finished"
        cgLabour.Columns("ts_day_finish").Caption = "Yard"
        cgLabour.Columns("ts_day_duration").Caption = "Duration"
        cgLabour.Columns("ts_cost").Caption = "Cost"

    End Sub

    Private Sub DisplayPlantGrid()

        Dim _SQL As String = ""
        _SQL += "select ID, name, plant_type, plant_cat, location_stamp"
        _SQL += " from Plant"
        _SQL += " where location_job = " + m_JobIDQuoted
        _SQL += " order by location_stamp desc"

        cgPlant.HideFirstColumn = True
        cgPlant.Populate(Session.ConnectionString, _SQL)

        cgPlant.Columns("name").Caption = "Plant"
        cgPlant.Columns("plant_type").Caption = "Type"
        cgPlant.Columns("plant_cat").Caption = "Category"
        cgPlant.Columns("location_stamp").Caption = "Deployed"

    End Sub

    Private Sub DisplayHireGrid()

        Dim _SQL As String = ""
        _SQL += "select d.ID, p.name, d.date_from, d.date_to, d.days, d.cost"
        _SQL += " from PlantDuration d"
        _SQL += " left join Plant p on p.ID = d.plant_id"
        _SQL += " where d.job_id = " + m_JobIDQuoted
        _SQL += " order by d.date_from desc"

        cgHire.HideFirstColumn = True
        cgHire.Populate(Session.ConnectionString, _SQL)

        cgHire.Columns("name").Caption = "Plant"
        cgHire.Columns("date_from").Caption = "Date From"
        cgHire.Columns("date_to").Caption = "Date To"
        cgHire.Columns("days").Caption = "Days"
        cgHire.Columns("cost").Caption = "Cost"

    End Sub

    Private Sub DisplayInvoicingNotes()

        Dim _SQL As String = ""

        _SQL += "select m.booked_date as 'Date', s.description as 'Description', s.sell_unit as 'UOM',"
        _SQL += " m.qty as 'Qty', m.unit_price as 'Rate', m.total_price as 'Total Price'"
        _SQL += " from Materials m"
        _SQL += " left join Stock s On s.ID = m.stock_id"
        _SQL += " where m.job_id = " + m_JobIDQuoted
        _SQL += " order by s.description"

        cgMaterials.HideFirstColumn = False
        cgMaterials.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub DisplayInvoicingNotesSummary()

        Dim _SQL As String = ""

        _SQL += "select m.booked_date As 'Date', count(*) as 'Item Count', sum(m.total_price) as 'Total Value'"
        _SQL += " from Materials m"
        _SQL += " where m.job_id = " + m_JobIDQuoted
        _SQL += " group by m.booked_date"

        cgMaterials.HideFirstColumn = False
        cgMaterials.Populate(Session.ConnectionString, _SQL)

    End Sub

#End Region

#Region "Pictures"

    Private Sub PopulatePicFolders()

        m_PicFolders.Clear()

        Dim _JobPath As String = m_JobPath + "\Photos"

        If IO.Directory.Exists(_JobPath) Then

            m_PicFolders.Add(_JobPath, "Root Folder")

            Dim _Dirs As List(Of String) = IO.Directory.GetDirectories(_JobPath, "*", IO.SearchOption.AllDirectories).ToList
            For Each _d In _Dirs

                Dim _Files As List(Of String) = IO.Directory.GetFiles(_d).ToList
                For Each _f In _Files

                    Dim _file As New IO.FileInfo(_f)
                    Select Case _file.Extension.ToLower

                        Case ".jpg", ".jpeg", ".tiff", ".png"
                            m_PicFolders.Add(_d, New IO.DirectoryInfo(_d).Name)
                            Exit For

                    End Select

                Next

            Next

            cgFolders.HideFirstColumn = True
            cgFolders.Populate(m_PicFolders)
            cgFolders.MoveFirst()

        End If

    End Sub

    Private Sub cgFolders_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles cgFolders.FocusedRowChanged

        If e Is Nothing Then Exit Sub
        If e.FocusedRowHandle < 0 Then Exit Sub

        Dim _d As KeyValuePair(Of String, String) = CType(cgFolders.GetRowObject(e.FocusedRowHandle), KeyValuePair(Of String, String))
        PopulateFilesGrid(_d.Key)

    End Sub

    Private Sub PopulateFilesGrid(ByVal FolderPath As String)

        m_PicFiles.Clear()

        For Each _f In IO.Directory.EnumerateFiles(FolderPath)

            Dim _file As New IO.FileInfo(_f)
            Select Case _file.Extension.ToLower

                Case ".jpg", ".jpeg", ".tiff", ".png"
                    m_PicFiles.Add(_f, New IO.FileInfo(_f).Name)

            End Select

        Next

        cgFiles.HideFirstColumn = True
        cgFiles.Populate(m_PicFiles)
        cgFiles.MoveFirst()

    End Sub

    Private Sub cgFiles_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles cgFiles.FocusedRowChanged

        If e Is Nothing Then Exit Sub
        If e.FocusedRowHandle < 0 Then Exit Sub

        Dim _d As KeyValuePair(Of String, String) = CType(cgFiles.GetRowObject(e.FocusedRowHandle), KeyValuePair(Of String, String))
        ShowPicture(_d.Key)

    End Sub

    Private Sub ShowPicture(ByVal PicturePath As String)

        Dim _file As New IO.FileInfo(PicturePath)
        Select Case _file.Extension.ToLower

            Case ".jpg", ".jpeg", ".tiff", ".png"
                Try
                    pic.Load(PicturePath)

                Catch ex As Exception

                End Try

        End Select

    End Sub

#End Region

#Region "Job Descriptions"

    Private Sub radDescJob_CheckedChanged(sender As Object, e As EventArgs) Handles radDescJob.CheckedChanged
        If radDescJob.Checked Then DisplayDescriptions()
    End Sub

    Private Sub radDescDirections_CheckedChanged(sender As Object, e As EventArgs) Handles radDescDirections.CheckedChanged
        If radDescDirections.Checked Then DisplayDescriptions()
    End Sub

    Private Sub radDescEst_CheckedChanged(sender As Object, e As EventArgs) Handles radDescEst.CheckedChanged
        If radDescEst.Checked Then DisplayDescriptions()
    End Sub

    Private Sub radDescTools_CheckedChanged(sender As Object, e As EventArgs) Handles radDescTools.CheckedChanged
        If radDescTools.Checked Then DisplayDescriptions()
    End Sub

    Private Sub DisplayDescriptions()
        If radDescJob.Checked Then rich.RTFText = m_Job._DescJob
        If radDescDirections.Checked Then rich.RTFText = m_Job._DescDirections
        If radDescEst.Checked Then rich.RTFText = m_Job._DescEst
        If radDescTools.Checked Then rich.RTFText = m_Job._DescTools
    End Sub

#End Region

#Region "Supplier Invoices"

    Private Sub radInvAll_CheckedChanged(sender As Object, e As EventArgs) Handles radInvAll.CheckedChanged
        If radInvAll.Checked Then DisplayMaterialsGrid()
    End Sub

    Private Sub radInvSup_CheckedChanged(sender As Object, e As EventArgs) Handles radInvSup.CheckedChanged
        If radInvSup.Checked Then DisplayMaterialsGrid()
    End Sub

    Private Sub radInvSupCred_CheckedChanged(sender As Object, e As EventArgs) Handles radInvSupCred.CheckedChanged
        If radInvSupCred.Checked Then DisplayMaterialsGrid()
    End Sub

    Private Sub radInvCont_CheckedChanged(sender As Object, e As EventArgs) Handles radInvCont.CheckedChanged
        If radInvCont.Checked Then DisplayMaterialsGrid()
    End Sub

    Private Sub radInvContCred_CheckedChanged(sender As Object, e As EventArgs) Handles radInvContCred.CheckedChanged
        If radInvContCred.Checked Then DisplayMaterialsGrid()
    End Sub

    Private Sub radInvInvNotes_CheckedChanged(sender As Object, e As EventArgs) Handles radInvInvNotes.CheckedChanged
        If radInvInvNotes.Checked Then DisplayInvoicingNotes()
    End Sub

    Private Sub radInvSummaryInvNotes_CheckedChanged(sender As Object, e As EventArgs) Handles radInvSummaryInvNotes.CheckedChanged
        If radInvSummaryInvNotes.Checked Then DisplayInvoicingNotesSummary()
    End Sub

#End Region

#Region "Buttons"

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click

        Dim _frm As New frmJobPrint(m_Job._ID.Value)
        _frm.ShowDialog()

        _frm.Dispose()
        _frm = Nothing

    End Sub

    Private Sub btnFunctions_Click(sender As Object, e As EventArgs) Handles btnFunctions.Click

        Dim _frm As New frmJobFunctions(m_Job)
        _frm.ShowDialog()

        If _frm.DialogResult = System.Windows.Forms.DialogResult.OK Then
            DisplayRecord(m_Job._ID.Value)
        End If

        _frm.Dispose()
        _frm = Nothing

    End Sub

    Private Sub btnWorkFile_Click(sender As Object, e As EventArgs) Handles btnWorkFile.Click
        If m_JobPath = "" Then
            CareMessage("Job Path is blank.", MessageBoxIcon.Exclamation, "Open Work File")
        Else
            If IO.Directory.Exists(m_JobPath) Then
                Try
                    Process.Start(m_JobPath)
                Catch ex As Exception
                    CareMessage("Error opening Job Path: " + m_JobPath, MessageBoxIcon.Exclamation, "Open Work File")
                End Try
            Else
                CareMessage("Job Path does not exist: " + m_JobPath, MessageBoxIcon.Exclamation, "Open Work File")
            End If
        End If
    End Sub

    Private Sub btnKeyDates_Click(sender As Object, e As EventArgs) Handles btnKeyDates.Click

        Dim _frm As New frmJobKeyDates(m_Job)
        _frm.ShowDialog()

        If _frm.DialogResult = System.Windows.Forms.DialogResult.OK Then
            DisplayRecord(m_Job._ID.Value)
        End If

        _frm.Dispose()
        _frm = Nothing

    End Sub

    Private Sub btnStatus_Click(sender As Object, e As EventArgs)

        Dim _frm As New frmJobStatus(m_Job)
        _frm.ShowDialog()

        If _frm.DialogResult = System.Windows.Forms.DialogResult.OK Then
            DisplayRecord(m_Job._ID.Value)
        End If

        _frm.Dispose()
        _frm = Nothing

    End Sub

    Private Sub btnCopyRisk_Click(sender As Object, e As EventArgs) Handles btnCopyRisk.Click
        CopyRiskToCustomer()
    End Sub

    Private Sub CopyRiskToCustomer()
        txtCustAddress.Text = txtRiskAddress.Text
        cbxCustTitle.Text = cbxRiskTitle.Text
        txtCustForename.Text = txtRiskForename.Text
        txtCustSurname.Text = txtRiskSurname.Text
    End Sub

    Private Sub cgContacts_EditClick(sender As Object, e As EventArgs) Handles cgContacts.EditClick
        Dim _ID As Guid? = ReturnDialogID(cgContacts, "Contact_ID")
        If _ID IsNot Nothing Then
            If ContactDialog(_ID) IsNot Nothing Then
                DisplayContacts()
            End If
        End If
    End Sub

    Private Sub cgContacts_AddClick(sender As Object, e As EventArgs) Handles cgContacts.AddClick

        Dim _ContactID As Guid? = Nothing

        If CareMessage("Have we dealt With this Contact before?", MessageBoxIcon.Question, MessageBoxButtons.YesNo, "Existing Contact?") = DialogResult.Yes Then
            _ContactID = ReturnContactFromSearch()
        Else
            _ContactID = ContactDialog(Nothing)
        End If

        If _ContactID IsNot Nothing Then

            'link contact to job
            m_Job.LinkContact(_ContactID.Value)

            DisplayContacts()

        End If

    End Sub

    Private Function ReturnContactFromSearch() As Guid?

        Dim _SQL As String = ""

        _SQL += "Select fullname As 'Name', relationship as 'Relationship', company as 'Company', jobtitle as 'Job Title',"
        _SQL += " tel_home as 'H', tel_mobile as 'M', tel_work as 'W', tel_other as 'O' from Contacts"

        Dim _ReturnValue As String = ""

        Dim _Find As New GenericFind
        With _Find
            .Caption = "Find Contact"
            .PopulateOnLoad = True
            .ConnectionString = Session.ConnectionString
            .GridSQL = _SQL
            .GridOrderBy = "order by forename, surname"
            .ReturnField = "ID"
            .FormWidth = 900
            .Show()
            _ReturnValue = .ReturnValue
        End With

        If _Find.ReturnValue = "" Then
            Return Nothing
        Else
            Return New Guid(_Find.ReturnValue)
        End If

    End Function

    Private Sub cgContacts_RemoveClick(sender As Object, e As EventArgs) Handles cgContacts.RemoveClick
        Dim _ID As Guid? = ReturnDialogID(cgContacts, "ID")
        If _ID IsNot Nothing Then
            If CareMessage("Are you sure you want to Remove this Contact from this Job?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Remove Contact") = DialogResult.Yes Then
                m_Job.UnLinkContact(_ID.Value)
                DisplayContacts()
            End If
        End If
    End Sub

    Private Function ReturnDialogID(ByRef GridIn As Care.Controls.CareGrid, ByVal IDColumn As String) As Guid?

        If GridIn Is Nothing Then Return Nothing
        If GridIn.RecordCount < 1 Then Return Nothing
        If GridIn.CurrentRow(IDColumn).ToString = "" Then Return Nothing

        Return New Guid(GridIn.CurrentRow(IDColumn).ToString)

    End Function

    Private Function ReturnDialogID(ByRef GridIn As Care.Controls.CareGridWithButtons, ByVal IDColumn As String) As Guid?

        If GridIn Is Nothing Then Return Nothing
        If GridIn.RecordCount < 1 Then Return Nothing
        If GridIn.CurrentRow(IDColumn).ToString = "" Then Return Nothing

        Return New Guid(GridIn.CurrentRow(IDColumn).ToString)

    End Function

    Private Function ContactDialog(ByVal ContactID As Guid?) As Guid?

        Dim _frm As New frmContact(ContactID, True)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            Return _frm.ReturnContactID
        Else
            Return Nothing
        End If

    End Function

    Private Sub btnContactPrimary_Click(sender As Object, e As EventArgs) Handles btnContactPrimary.Click
        Dim _ID As Guid? = ReturnDialogID(cgContacts, "ID")
        If _ID IsNot Nothing Then
            m_Job.SetPrimaryContact(_ID.Value)
            DisplayContacts()
        End If
    End Sub

    Private Sub btnContactBilling_Click(sender As Object, e As EventArgs) Handles btnContactBilling.Click
        Dim _ID As Guid? = ReturnDialogID(cgContacts, "ID")
        If _ID IsNot Nothing Then
            m_Job.SetBillingContact(_ID.Value)
            DisplayContacts()
        End If
    End Sub

    Private Sub cgSalesInvoices_EditClick(sender As Object, e As EventArgs) Handles cgSalesInvoices.EditClick

        Dim _ID As Guid? = ReturnDialogID(cgSalesInvoices, "ID")
        If _ID IsNot Nothing Then

            Dim _frm As New frmInvoiceManual(m_Job._ID.Value, _ID)
            _frm.ShowDialog()

            If _frm.DialogResult = DialogResult.OK Then
                DisplaySalesInvoices()
            End If

        End If

    End Sub

    Private Sub cgSalesInvoices_AddClick(sender As Object, e As EventArgs) Handles cgSalesInvoices.AddClick

        Dim _frm As New frmInvoiceManual(m_Job._ID.Value, Nothing)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            DisplaySalesInvoices()
        End If

    End Sub

    Private Sub cgSalesInvoices_RemoveClick(sender As Object, e As EventArgs) Handles cgSalesInvoices.RemoveClick
        CareMessage("You cannot remove a Sales Invoice from a Job Record.", MessageBoxIcon.Exclamation, "Remove Invoice")
    End Sub

    Private Sub btnChangeStatus_Click(sender As Object, e As EventArgs) Handles btnChangeStatus.Click

        Dim _frm As New frmJobStatus(m_Job)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            m_Job._JobStatusCode = _frm.SelectedStatusCode
            m_Job._JobStatus = _frm.SelectedStatusName
            m_Job.Store()
            DisplayRecord(m_Job._ID.Value)
        End If

    End Sub

    Private Sub cgVersions_EditClick(sender As Object, e As EventArgs) Handles cgVersions.EditClick

        Dim _ID As Guid? = ReturnDialogID(cgVersions, "ID")
        If _ID IsNot Nothing Then

            Dim _frm As New frmJobVersion(m_Job._ID.Value, _ID)
            _frm.ShowDialog()

            If _frm.DialogResult = DialogResult.OK Then
                DisplayVersionsGrid()
            End If

        End If

    End Sub

    Private Sub cgVersions_AddClick(sender As Object, e As EventArgs) Handles cgVersions.AddClick

        Dim _frm As New frmJobVersion(m_Job._ID.Value, Nothing)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            DisplayVersionsGrid()
        End If

    End Sub

    Private Sub cgVersions_RemoveClick(sender As Object, e As EventArgs) Handles cgVersions.RemoveClick
        CareMessage("You cannot remove a Version from a Job Record.", MessageBoxIcon.Exclamation, "Remove Version")
    End Sub

    Private Sub cgMaterials_GridDoubleClick(sender As Object, e As EventArgs) Handles cgMaterials.GridDoubleClick
        Dim _ID As Guid? = ReturnDialogID(cgMaterials, "doc_id")
        If _ID IsNot Nothing Then
            Business.Scan.ViewDocument(_ID.Value)
        End If
    End Sub

    Private Sub btnMatMove_Click(sender As Object, e As EventArgs) Handles btnMatMove.Click

        Dim _DocID As Guid? = ReturnDialogID(cgMaterials, "doc_id")
        If _DocID IsNot Nothing Then
            If CareMessage("Are you sure you want to Move this Purchase Document to another Job?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Move Purchase Document") = DialogResult.Yes Then

                btnMatMove.Enabled = False
                Application.DoEvents()

                Dim _NewJob As String = Business.Job.FindAll
                If _NewJob <> "" Then
                    Business.PurchaseDocument.MoveToAnotherJob(_DocID, m_Job._ID, New Guid(_NewJob))
                End If

                DisplayMaterialsGrid()

            End If
        End If
    End Sub

    Private Sub btnMatDelete_Click(sender As Object, e As EventArgs) Handles btnMatDelete.Click
        Dim _ID As Guid? = ReturnDialogID(cgMaterials, "doc_id")
        If _ID IsNot Nothing Then
            If InputBox("Please type DELETE to delete this Purchase Document.", "Delete Purchase Document").ToUpper = "DELETE" Then
                btnMatDelete.Enabled = False
                Application.DoEvents()
                Business.PurchaseDocument.DeleteFromJob(_ID, m_Job._ID)
                DisplayMaterialsGrid()
            End If
        End If
    End Sub

#End Region

End Class
