﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmScanSplit
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.cgTargets = New Care.Controls.CareGrid()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.txtDocValue = New Care.Controls.CareTextBox(Me.components)
        Me.txtDocSupplier = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.txtDocDate = New Care.Controls.CareTextBox(Me.components)
        Me.btnAdd = New Care.Controls.CareButton(Me.components)
        Me.btnEdit = New Care.Controls.CareButton(Me.components)
        Me.btnRemove = New Care.Controls.CareButton(Me.components)
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.gbxDoc = New DevExpress.XtraEditors.GroupControl()
        Me.btnDocSave = New Care.Controls.CareButton(Me.components)
        Me.btnDocCancel = New Care.Controls.CareButton(Me.components)
        Me.cbxWorkType = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.txtNet = New Care.Controls.CareTextBox(Me.components)
        Me.radMatchJob = New Care.Controls.CareRadioButton(Me.components)
        Me.txtRef1 = New Care.Controls.CareTextBox(Me.components)
        Me.radMatchPlant = New Care.Controls.CareRadioButton(Me.components)
        Me.txtName = New Care.Controls.CareTextBox(Me.components)
        Me.radMatchCategory = New Care.Controls.CareRadioButton(Me.components)
        Me.btnSearch = New Care.Controls.CareButton(Me.components)
        Me.txtRef = New Care.Controls.CareTextBox(Me.components)
        Me.cbxCategory = New Care.Controls.CareComboBox(Me.components)
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.txtRemain = New Care.Controls.CareTextBox(Me.components)
        Me.lblRemaining = New Care.Controls.CareLabel(Me.components)
        Me.txtAllocated = New Care.Controls.CareTextBox(Me.components)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtDocValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDocSupplier.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDocDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxDoc, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxDoc.SuspendLayout()
        CType(Me.cbxWorkType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNet.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radMatchJob.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRef1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radMatchPlant.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radMatchCategory.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRef.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxCategory.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.txtRemain.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAllocated.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'cgTargets
        '
        Me.cgTargets.AllowBuildColumns = True
        Me.cgTargets.AllowEdit = False
        Me.cgTargets.AllowHorizontalScroll = False
        Me.cgTargets.AllowMultiSelect = False
        Me.cgTargets.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgTargets.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgTargets.Appearance.Options.UseFont = True
        Me.cgTargets.AutoSizeByData = True
        Me.cgTargets.DisableAutoSize = False
        Me.cgTargets.DisableDataFormatting = False
        Me.cgTargets.FocusedRowHandle = -2147483648
        Me.cgTargets.HideFirstColumn = False
        Me.cgTargets.Location = New System.Drawing.Point(12, 57)
        Me.cgTargets.Name = "cgTargets"
        Me.cgTargets.PreviewColumn = ""
        Me.cgTargets.QueryID = Nothing
        Me.cgTargets.RowAutoHeight = True
        Me.cgTargets.SearchAsYouType = True
        Me.cgTargets.ShowAutoFilterRow = False
        Me.cgTargets.ShowFindPanel = False
        Me.cgTargets.ShowGroupByBox = False
        Me.cgTargets.ShowLoadingPanel = False
        Me.cgTargets.ShowNavigator = False
        Me.cgTargets.Size = New System.Drawing.Size(728, 275)
        Me.cgTargets.TabIndex = 1
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.txtDocValue)
        Me.GroupControl1.Controls.Add(Me.txtDocSupplier)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Controls.Add(Me.txtDocDate)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(728, 39)
        Me.GroupControl1.TabIndex = 0
        '
        'CareLabel3
        '
        Me.CareLabel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.Options.UseFont = True
        Me.CareLabel3.Appearance.Options.UseTextOptions = True
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(549, 12)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(88, 15)
        Me.CareLabel3.TabIndex = 4
        Me.CareLabel3.Text = "Document Value"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.Options.UseFont = True
        Me.CareLabel2.Appearance.Options.UseTextOptions = True
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(192, 12)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(43, 15)
        Me.CareLabel2.TabIndex = 2
        Me.CareLabel2.Text = "Supplier"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDocValue
        '
        Me.txtDocValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDocValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDocValue.EnterMoveNextControl = True
        Me.txtDocValue.Location = New System.Drawing.Point(643, 9)
        Me.txtDocValue.MaxLength = 20
        Me.txtDocValue.Name = "txtDocValue"
        Me.txtDocValue.NumericAllowNegatives = False
        Me.txtDocValue.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.None
        Me.txtDocValue.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDocValue.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtDocValue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtDocValue.Properties.Appearance.Options.UseFont = True
        Me.txtDocValue.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDocValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtDocValue.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDocValue.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDocValue.Properties.MaxLength = 20
        Me.txtDocValue.Properties.ReadOnly = True
        Me.txtDocValue.Size = New System.Drawing.Size(77, 22)
        Me.txtDocValue.TabIndex = 5
        Me.txtDocValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDocValue.ToolTipText = ""
        '
        'txtDocSupplier
        '
        Me.txtDocSupplier.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDocSupplier.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDocSupplier.EnterMoveNextControl = True
        Me.txtDocSupplier.Location = New System.Drawing.Point(241, 9)
        Me.txtDocSupplier.MaxLength = 20
        Me.txtDocSupplier.Name = "txtDocSupplier"
        Me.txtDocSupplier.NumericAllowNegatives = False
        Me.txtDocSupplier.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.None
        Me.txtDocSupplier.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDocSupplier.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtDocSupplier.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtDocSupplier.Properties.Appearance.Options.UseFont = True
        Me.txtDocSupplier.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDocSupplier.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtDocSupplier.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDocSupplier.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDocSupplier.Properties.MaxLength = 20
        Me.txtDocSupplier.Properties.ReadOnly = True
        Me.txtDocSupplier.Size = New System.Drawing.Size(287, 22)
        Me.txtDocSupplier.TabIndex = 3
        Me.txtDocSupplier.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDocSupplier.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.Options.UseFont = True
        Me.CareLabel1.Appearance.Options.UseTextOptions = True
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(7, 12)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(83, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Document Date"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDocDate
        '
        Me.txtDocDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDocDate.EnterMoveNextControl = True
        Me.txtDocDate.Location = New System.Drawing.Point(96, 9)
        Me.txtDocDate.MaxLength = 20
        Me.txtDocDate.Name = "txtDocDate"
        Me.txtDocDate.NumericAllowNegatives = False
        Me.txtDocDate.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.None
        Me.txtDocDate.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDocDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtDocDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtDocDate.Properties.Appearance.Options.UseFont = True
        Me.txtDocDate.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDocDate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtDocDate.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDocDate.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDocDate.Properties.MaxLength = 20
        Me.txtDocDate.Properties.ReadOnly = True
        Me.txtDocDate.Size = New System.Drawing.Size(77, 22)
        Me.txtDocDate.TabIndex = 1
        Me.txtDocDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDocDate.ToolTipText = ""
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.Appearance.Options.UseFont = True
        Me.btnAdd.Location = New System.Drawing.Point(12, 383)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(95, 22)
        Me.btnAdd.TabIndex = 4
        Me.btnAdd.TabStop = False
        Me.btnAdd.Text = "Add"
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.Appearance.Options.UseFont = True
        Me.btnEdit.Location = New System.Drawing.Point(113, 383)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(95, 22)
        Me.btnEdit.TabIndex = 5
        Me.btnEdit.TabStop = False
        Me.btnEdit.Text = "Edit"
        '
        'btnRemove
        '
        Me.btnRemove.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemove.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRemove.Appearance.Options.UseFont = True
        Me.btnRemove.Location = New System.Drawing.Point(214, 383)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(95, 22)
        Me.btnRemove.TabIndex = 6
        Me.btnRemove.TabStop = False
        Me.btnRemove.Text = "Remove"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(645, 383)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(95, 22)
        Me.btnClose.TabIndex = 7
        Me.btnClose.TabStop = False
        Me.btnClose.Text = "Close"
        '
        'gbxDoc
        '
        Me.gbxDoc.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.gbxDoc.Controls.Add(Me.btnDocSave)
        Me.gbxDoc.Controls.Add(Me.btnDocCancel)
        Me.gbxDoc.Controls.Add(Me.cbxWorkType)
        Me.gbxDoc.Controls.Add(Me.CareLabel4)
        Me.gbxDoc.Controls.Add(Me.txtNet)
        Me.gbxDoc.Controls.Add(Me.radMatchJob)
        Me.gbxDoc.Controls.Add(Me.txtRef1)
        Me.gbxDoc.Controls.Add(Me.radMatchPlant)
        Me.gbxDoc.Controls.Add(Me.txtName)
        Me.gbxDoc.Controls.Add(Me.radMatchCategory)
        Me.gbxDoc.Controls.Add(Me.btnSearch)
        Me.gbxDoc.Controls.Add(Me.txtRef)
        Me.gbxDoc.Controls.Add(Me.cbxCategory)
        Me.gbxDoc.Location = New System.Drawing.Point(268, 80)
        Me.gbxDoc.Name = "gbxDoc"
        Me.gbxDoc.ShowCaption = False
        Me.gbxDoc.Size = New System.Drawing.Size(215, 244)
        Me.gbxDoc.TabIndex = 2
        Me.gbxDoc.Visible = False
        '
        'btnDocSave
        '
        Me.btnDocSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnDocSave.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDocSave.Appearance.Options.UseFont = True
        Me.btnDocSave.Location = New System.Drawing.Point(9, 206)
        Me.btnDocSave.Name = "btnDocSave"
        Me.btnDocSave.Size = New System.Drawing.Size(95, 22)
        Me.btnDocSave.TabIndex = 11
        Me.btnDocSave.Text = "Save"
        '
        'btnDocCancel
        '
        Me.btnDocCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnDocCancel.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDocCancel.Appearance.Options.UseFont = True
        Me.btnDocCancel.Location = New System.Drawing.Point(110, 206)
        Me.btnDocCancel.Name = "btnDocCancel"
        Me.btnDocCancel.Size = New System.Drawing.Size(95, 22)
        Me.btnDocCancel.TabIndex = 12
        Me.btnDocCancel.Text = "Cancel"
        '
        'cbxWorkType
        '
        Me.cbxWorkType.AllowBlank = False
        Me.cbxWorkType.DataSource = Nothing
        Me.cbxWorkType.DisplayMember = Nothing
        Me.cbxWorkType.EnterMoveNextControl = True
        Me.cbxWorkType.Location = New System.Drawing.Point(5, 140)
        Me.cbxWorkType.Name = "cbxWorkType"
        Me.cbxWorkType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxWorkType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxWorkType.Properties.Appearance.Options.UseFont = True
        Me.cbxWorkType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxWorkType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxWorkType.SelectedValue = Nothing
        Me.cbxWorkType.Size = New System.Drawing.Size(200, 22)
        Me.cbxWorkType.TabIndex = 8
        Me.cbxWorkType.ValueMember = Nothing
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.Options.UseFont = True
        Me.CareLabel4.Appearance.Options.UseTextOptions = True
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(103, 171)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(19, 15)
        Me.CareLabel4.TabIndex = 9
        Me.CareLabel4.Text = "Net"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNet
        '
        Me.txtNet.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNet.EnterMoveNextControl = True
        Me.txtNet.Location = New System.Drawing.Point(128, 168)
        Me.txtNet.MaxLength = 14
        Me.txtNet.Name = "txtNet"
        Me.txtNet.NumericAllowNegatives = True
        Me.txtNet.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtNet.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtNet.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtNet.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtNet.Properties.Appearance.Options.UseFont = True
        Me.txtNet.Properties.Appearance.Options.UseTextOptions = True
        Me.txtNet.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtNet.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNet.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txtNet.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtNet.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtNet.Properties.MaxLength = 14
        Me.txtNet.Size = New System.Drawing.Size(77, 22)
        Me.txtNet.TabIndex = 10
        Me.txtNet.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNet.ToolTipText = ""
        '
        'radMatchJob
        '
        Me.radMatchJob.EditValue = True
        Me.radMatchJob.Location = New System.Drawing.Point(5, 5)
        Me.radMatchJob.Name = "radMatchJob"
        Me.radMatchJob.Properties.AutoWidth = True
        Me.radMatchJob.Properties.Caption = "Match to Job"
        Me.radMatchJob.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radMatchJob.Properties.RadioGroupIndex = 0
        Me.radMatchJob.Size = New System.Drawing.Size(84, 19)
        Me.radMatchJob.TabIndex = 0
        '
        'txtRef1
        '
        Me.txtRef1.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtRef1.EnterMoveNextControl = True
        Me.txtRef1.Location = New System.Drawing.Point(5, 112)
        Me.txtRef1.MaxLength = 0
        Me.txtRef1.Name = "txtRef1"
        Me.txtRef1.NumericAllowNegatives = False
        Me.txtRef1.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRef1.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRef1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRef1.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRef1.Properties.Appearance.Options.UseFont = True
        Me.txtRef1.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRef1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRef1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRef1.Properties.ReadOnly = True
        Me.txtRef1.Properties.Tag = "R"
        Me.txtRef1.Size = New System.Drawing.Size(200, 22)
        Me.txtRef1.TabIndex = 7
        Me.txtRef1.TabStop = False
        Me.txtRef1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRef1.ToolTipText = ""
        '
        'radMatchPlant
        '
        Me.radMatchPlant.Location = New System.Drawing.Point(95, 5)
        Me.radMatchPlant.Name = "radMatchPlant"
        Me.radMatchPlant.Properties.AutoWidth = True
        Me.radMatchPlant.Properties.Caption = "Match to Plant"
        Me.radMatchPlant.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radMatchPlant.Properties.RadioGroupIndex = 0
        Me.radMatchPlant.Size = New System.Drawing.Size(91, 19)
        Me.radMatchPlant.TabIndex = 1
        Me.radMatchPlant.TabStop = False
        '
        'txtName
        '
        Me.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(5, 84)
        Me.txtName.MaxLength = 0
        Me.txtName.Name = "txtName"
        Me.txtName.NumericAllowNegatives = False
        Me.txtName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtName.Properties.ReadOnly = True
        Me.txtName.Properties.Tag = "R"
        Me.txtName.Size = New System.Drawing.Size(200, 22)
        Me.txtName.TabIndex = 6
        Me.txtName.TabStop = False
        Me.txtName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'radMatchCategory
        '
        Me.radMatchCategory.Location = New System.Drawing.Point(5, 31)
        Me.radMatchCategory.Name = "radMatchCategory"
        Me.radMatchCategory.Properties.AutoWidth = True
        Me.radMatchCategory.Properties.Caption = "Match to Category"
        Me.radMatchCategory.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radMatchCategory.Properties.RadioGroupIndex = 0
        Me.radMatchCategory.Size = New System.Drawing.Size(112, 19)
        Me.radMatchCategory.TabIndex = 2
        Me.radMatchCategory.TabStop = False
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(87, 55)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(75, 23)
        Me.btnSearch.TabIndex = 4
        Me.btnSearch.TabStop = False
        Me.btnSearch.Text = "Search"
        '
        'txtRef
        '
        Me.txtRef.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRef.EnterMoveNextControl = True
        Me.txtRef.Location = New System.Drawing.Point(5, 56)
        Me.txtRef.MaxLength = 0
        Me.txtRef.Name = "txtRef"
        Me.txtRef.NumericAllowNegatives = False
        Me.txtRef.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRef.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRef.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRef.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRef.Properties.Appearance.Options.UseFont = True
        Me.txtRef.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRef.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRef.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRef.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRef.Size = New System.Drawing.Size(76, 22)
        Me.txtRef.TabIndex = 3
        Me.txtRef.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRef.ToolTipText = ""
        '
        'cbxCategory
        '
        Me.cbxCategory.AllowBlank = False
        Me.cbxCategory.DataSource = Nothing
        Me.cbxCategory.DisplayMember = Nothing
        Me.cbxCategory.EnterMoveNextControl = True
        Me.cbxCategory.Location = New System.Drawing.Point(5, 56)
        Me.cbxCategory.Name = "cbxCategory"
        Me.cbxCategory.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxCategory.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxCategory.Properties.Appearance.Options.UseFont = True
        Me.cbxCategory.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxCategory.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxCategory.SelectedValue = Nothing
        Me.cbxCategory.Size = New System.Drawing.Size(200, 22)
        Me.cbxCategory.TabIndex = 5
        Me.cbxCategory.ValueMember = Nothing
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl2.Controls.Add(Me.CareLabel6)
        Me.GroupControl2.Controls.Add(Me.txtRemain)
        Me.GroupControl2.Controls.Add(Me.lblRemaining)
        Me.GroupControl2.Controls.Add(Me.txtAllocated)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 338)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.ShowCaption = False
        Me.GroupControl2.Size = New System.Drawing.Size(728, 39)
        Me.GroupControl2.TabIndex = 3
        '
        'CareLabel6
        '
        Me.CareLabel6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.Options.UseFont = True
        Me.CareLabel6.Appearance.Options.UseTextOptions = True
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(587, 12)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(50, 15)
        Me.CareLabel6.TabIndex = 2
        Me.CareLabel6.Text = "Allocated"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRemain
        '
        Me.txtRemain.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtRemain.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRemain.EnterMoveNextControl = True
        Me.txtRemain.Location = New System.Drawing.Point(489, 9)
        Me.txtRemain.MaxLength = 20
        Me.txtRemain.Name = "txtRemain"
        Me.txtRemain.NumericAllowNegatives = False
        Me.txtRemain.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.None
        Me.txtRemain.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRemain.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRemain.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRemain.Properties.Appearance.Options.UseFont = True
        Me.txtRemain.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRemain.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtRemain.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRemain.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRemain.Properties.MaxLength = 20
        Me.txtRemain.Properties.ReadOnly = True
        Me.txtRemain.Size = New System.Drawing.Size(77, 22)
        Me.txtRemain.TabIndex = 1
        Me.txtRemain.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtRemain.ToolTipText = ""
        '
        'lblRemaining
        '
        Me.lblRemaining.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblRemaining.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblRemaining.Appearance.Options.UseFont = True
        Me.lblRemaining.Appearance.Options.UseTextOptions = True
        Me.lblRemaining.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblRemaining.Location = New System.Drawing.Point(426, 12)
        Me.lblRemaining.Name = "lblRemaining"
        Me.lblRemaining.Size = New System.Drawing.Size(57, 15)
        Me.lblRemaining.TabIndex = 0
        Me.lblRemaining.Text = "Remaining"
        Me.lblRemaining.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAllocated
        '
        Me.txtAllocated.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAllocated.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAllocated.EnterMoveNextControl = True
        Me.txtAllocated.Location = New System.Drawing.Point(643, 9)
        Me.txtAllocated.MaxLength = 20
        Me.txtAllocated.Name = "txtAllocated"
        Me.txtAllocated.NumericAllowNegatives = False
        Me.txtAllocated.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.None
        Me.txtAllocated.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtAllocated.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtAllocated.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAllocated.Properties.Appearance.Options.UseFont = True
        Me.txtAllocated.Properties.Appearance.Options.UseTextOptions = True
        Me.txtAllocated.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtAllocated.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAllocated.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtAllocated.Properties.MaxLength = 20
        Me.txtAllocated.Properties.ReadOnly = True
        Me.txtAllocated.Size = New System.Drawing.Size(77, 22)
        Me.txtAllocated.TabIndex = 3
        Me.txtAllocated.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAllocated.ToolTipText = ""
        '
        'frmScanSplit
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(751, 411)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.gbxDoc)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnRemove)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.cgTargets)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmScanSplit"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Split Scan against Multiple Targets"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtDocValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDocSupplier.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDocDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxDoc, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxDoc.ResumeLayout(False)
        Me.gbxDoc.PerformLayout()
        CType(Me.cbxWorkType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNet.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radMatchJob.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRef1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radMatchPlant.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radMatchCategory.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRef.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxCategory.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.txtRemain.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAllocated.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents cgTargets As Care.Controls.CareGrid
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents txtDocValue As Care.Controls.CareTextBox
    Friend WithEvents txtDocSupplier As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents txtDocDate As Care.Controls.CareTextBox
    Friend WithEvents btnAdd As Care.Controls.CareButton
    Friend WithEvents btnEdit As Care.Controls.CareButton
    Friend WithEvents btnRemove As Care.Controls.CareButton
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents gbxDoc As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cbxWorkType As Care.Controls.CareComboBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents txtNet As Care.Controls.CareTextBox
    Friend WithEvents radMatchJob As Care.Controls.CareRadioButton
    Friend WithEvents txtRef1 As Care.Controls.CareTextBox
    Friend WithEvents radMatchPlant As Care.Controls.CareRadioButton
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents radMatchCategory As Care.Controls.CareRadioButton
    Friend WithEvents btnSearch As Care.Controls.CareButton
    Friend WithEvents txtRef As Care.Controls.CareTextBox
    Friend WithEvents cbxCategory As Care.Controls.CareComboBox
    Friend WithEvents btnDocSave As Care.Controls.CareButton
    Friend WithEvents btnDocCancel As Care.Controls.CareButton
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents txtRemain As Care.Controls.CareTextBox
    Friend WithEvents lblRemaining As Care.Controls.CareLabel
    Friend WithEvents txtAllocated As Care.Controls.CareTextBox
End Class
