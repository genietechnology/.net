﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmTimesheets
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.cdtWC = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.tsEntry = New JC.TimeSheetControl()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.sc = New DevExpress.XtraEditors.XtraScrollableControl()
        Me.btnNextDay = New Care.Controls.CareButton(Me.components)
        Me.btnSameDay = New Care.Controls.CareButton(Me.components)
        Me.btnSaveNew = New Care.Controls.CareButton(Me.components)
        Me.btnSaveExit = New Care.Controls.CareButton(Me.components)
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.cbxEmployee = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.btnCopy = New Care.Controls.CareButton(Me.components)
        Me.btnDelete = New Care.Controls.CareButton(Me.components)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.cdtWC.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtWC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.cbxEmployee.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.cdtWC)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(229, 39)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "GroupControl1"
        '
        'cdtWC
        '
        Me.cdtWC.EditValue = Nothing
        Me.cdtWC.EnterMoveNextControl = True
        Me.cdtWC.Location = New System.Drawing.Point(120, 9)
        Me.cdtWC.Name = "cdtWC"
        Me.cdtWC.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtWC.Properties.Appearance.Options.UseFont = True
        Me.cdtWC.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtWC.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtWC.Size = New System.Drawing.Size(100, 22)
        Me.cdtWC.TabIndex = 1
        Me.cdtWC.Tag = "AE"
        Me.cdtWC.Value = Nothing
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(9, 12)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(105, 15)
        Me.CareLabel2.TabIndex = 0
        Me.CareLabel2.Text = "Week Commencing"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.CareLabel8)
        Me.GroupControl2.Controls.Add(Me.CareLabel7)
        Me.GroupControl2.Controls.Add(Me.CareLabel6)
        Me.GroupControl2.Controls.Add(Me.CareLabel5)
        Me.GroupControl2.Controls.Add(Me.CareLabel4)
        Me.GroupControl2.Controls.Add(Me.CareLabel3)
        Me.GroupControl2.Controls.Add(Me.CareLabel1)
        Me.GroupControl2.Controls.Add(Me.tsEntry)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 57)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.ShowCaption = False
        Me.GroupControl2.Size = New System.Drawing.Size(682, 78)
        Me.GroupControl2.TabIndex = 2
        Me.GroupControl2.Text = "GroupControl2"
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(596, 5)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(24, 15)
        Me.CareLabel8.TabIndex = 7
        Me.CareLabel8.Text = "Yard"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(532, 5)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(31, 15)
        Me.CareLabel7.TabIndex = 6
        Me.CareLabel7.Text = "Finish"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(471, 5)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(24, 15)
        Me.CareLabel6.TabIndex = 5
        Me.CareLabel6.Text = "Start"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(259, 5)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(105, 15)
        Me.CareLabel5.TabIndex = 4
        Me.CareLabel5.Text = "Description of Work"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(196, 5)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(37, 15)
        Me.CareLabel4.TabIndex = 3
        Me.CareLabel4.Text = "Job No"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(71, 5)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(20, 15)
        Me.CareLabel3.TabIndex = 2
        Me.CareLabel3.Text = "Day"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(9, 5)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(20, 15)
        Me.CareLabel1.TabIndex = 1
        Me.CareLabel1.Text = "Van"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tsEntry
        '
        Me.tsEntry.DayOfWeek = System.DayOfWeek.Sunday
        Me.tsEntry.FinishTime = ""
        Me.tsEntry.HighlightSelected = False
        Me.tsEntry.ItemIndex = 0
        Me.tsEntry.JobNo = ""
        Me.tsEntry.Location = New System.Drawing.Point(5, 24)
        Me.tsEntry.MaximumSize = New System.Drawing.Size(650, 50)
        Me.tsEntry.MinimumSize = New System.Drawing.Size(650, 50)
        Me.tsEntry.Name = "tsEntry"
        Me.tsEntry.Size = New System.Drawing.Size(650, 50)
        Me.tsEntry.StartTime = ""
        Me.tsEntry.TabIndex = 0
        Me.tsEntry.VanNo = ""
        Me.tsEntry.What = ""
        Me.tsEntry.YardTime = ""
        '
        'GroupControl3
        '
        Me.GroupControl3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl3.Controls.Add(Me.sc)
        Me.GroupControl3.Location = New System.Drawing.Point(8, 172)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.ShowCaption = False
        Me.GroupControl3.Size = New System.Drawing.Size(686, 377)
        Me.GroupControl3.TabIndex = 9
        Me.GroupControl3.Text = "GroupControl3"
        '
        'sc
        '
        Me.sc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.sc.Location = New System.Drawing.Point(2, 2)
        Me.sc.Name = "sc"
        Me.sc.Size = New System.Drawing.Size(682, 373)
        Me.sc.TabIndex = 0
        '
        'btnNextDay
        '
        Me.btnNextDay.Location = New System.Drawing.Point(12, 141)
        Me.btnNextDay.Name = "btnNextDay"
        Me.btnNextDay.Size = New System.Drawing.Size(100, 25)
        Me.btnNextDay.TabIndex = 3
        Me.btnNextDay.Text = "Add, Next Day"
        '
        'btnSameDay
        '
        Me.btnSameDay.Location = New System.Drawing.Point(224, 141)
        Me.btnSameDay.Name = "btnSameDay"
        Me.btnSameDay.Size = New System.Drawing.Size(100, 25)
        Me.btnSameDay.TabIndex = 5
        Me.btnSameDay.Text = "Add, Same Day"
        '
        'btnSaveNew
        '
        Me.btnSaveNew.Location = New System.Drawing.Point(448, 141)
        Me.btnSaveNew.Name = "btnSaveNew"
        Me.btnSaveNew.Size = New System.Drawing.Size(120, 25)
        Me.btnSaveNew.TabIndex = 7
        Me.btnSaveNew.Text = "Save, New TimeSheet"
        '
        'btnSaveExit
        '
        Me.btnSaveExit.Location = New System.Drawing.Point(574, 141)
        Me.btnSaveExit.Name = "btnSaveExit"
        Me.btnSaveExit.Size = New System.Drawing.Size(120, 25)
        Me.btnSaveExit.TabIndex = 8
        Me.btnSaveExit.Text = "Save, Exit"
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.cbxEmployee)
        Me.GroupControl4.Controls.Add(Me.CareLabel9)
        Me.GroupControl4.Location = New System.Drawing.Point(247, 12)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.ShowCaption = False
        Me.GroupControl4.Size = New System.Drawing.Size(447, 39)
        Me.GroupControl4.TabIndex = 1
        Me.GroupControl4.Text = "GroupControl4"
        '
        'cbxEmployee
        '
        Me.cbxEmployee.AllowBlank = False
        Me.cbxEmployee.DataSource = Nothing
        Me.cbxEmployee.DisplayMember = Nothing
        Me.cbxEmployee.EnterMoveNextControl = True
        Me.cbxEmployee.Location = New System.Drawing.Point(67, 8)
        Me.cbxEmployee.Name = "cbxEmployee"
        Me.cbxEmployee.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxEmployee.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxEmployee.Properties.Appearance.Options.UseFont = True
        Me.cbxEmployee.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxEmployee.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxEmployee.SelectedValue = Nothing
        Me.cbxEmployee.Size = New System.Drawing.Size(350, 22)
        Me.cbxEmployee.TabIndex = 1
        Me.cbxEmployee.Tag = "AE"
        Me.cbxEmployee.ValueMember = Nothing
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(9, 12)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(52, 15)
        Me.CareLabel9.TabIndex = 0
        Me.CareLabel9.Text = "Employee"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnCopy
        '
        Me.btnCopy.Location = New System.Drawing.Point(118, 141)
        Me.btnCopy.Name = "btnCopy"
        Me.btnCopy.Size = New System.Drawing.Size(100, 25)
        Me.btnCopy.TabIndex = 4
        Me.btnCopy.Text = "Copy, Next Day"
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(330, 141)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(100, 25)
        Me.btnDelete.TabIndex = 6
        Me.btnDelete.Text = "Delete Selected"
        '
        'frmTimesheets
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(704, 561)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnCopy)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.btnSaveExit)
        Me.Controls.Add(Me.btnSaveNew)
        Me.Controls.Add(Me.btnSameDay)
        Me.Controls.Add(Me.btnNextDay)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.LoadMaximised = True
        Me.Margin = New System.Windows.Forms.Padding(3, 5, 3, 5)
        Me.Name = "frmTimesheets"
        Me.Text = "frmTimesheets"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.cdtWC.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtWC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.cbxEmployee.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cdtWC As Care.Controls.CareDateTime
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents tsEntry As TimeSheetControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents btnNextDay As Care.Controls.CareButton
    Friend WithEvents btnSameDay As Care.Controls.CareButton
    Friend WithEvents btnSaveNew As Care.Controls.CareButton
    Friend WithEvents btnSaveExit As Care.Controls.CareButton
    Friend WithEvents sc As DevExpress.XtraEditors.XtraScrollableControl
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents cbxEmployee As Care.Controls.CareComboBox
    Friend WithEvents btnCopy As Care.Controls.CareButton
    Friend WithEvents btnDelete As Care.Controls.CareButton
End Class
