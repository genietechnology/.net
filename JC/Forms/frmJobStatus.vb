﻿Imports Care.Global
Imports Care.Shared

Public Class frmJobStatus

    Private m_Job As Business.Job

    Public Property SelectedStatusCode As String
    Public Property SelectedStatusName As String

    Public Sub New(ByVal JobRecord As Business.Job)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_Job = JobRecord

    End Sub

    Private Sub frmJobKeyDates_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim _SQL As String = ""

        _SQL += "select i.ref_1 as 'Status Code', i.name as 'Status' from AppListItems i"
        _SQL += " left join AppLists l on l.ID = i.list_id"
        _SQL += " where l.name = 'Job Status'"
        _SQL += " order by i.seq"

        cgStatus.Populate(Session.ConnectionString, _SQL)
        cgStatus.MoveFirst()

    End Sub

    Private Sub SelectStatus()
        Me.SelectedStatusCode = cgStatus.GetRowCellValue(cgStatus.RowIndex, "Status Code").ToString
        Me.SelectedStatusName = cgStatus.GetRowCellValue(cgStatus.RowIndex, "Status").ToString
        Me.DialogResult = DialogResult.OK
        Me.Close()
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        SelectStatus()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub cgStatus_GridDoubleClick(sender As Object, e As EventArgs) Handles cgStatus.GridDoubleClick
        SelectStatus
    End Sub
End Class