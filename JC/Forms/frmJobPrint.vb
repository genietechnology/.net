﻿Option Strict On

Imports Care.Global
Imports Care.Shared

Public Class frmJobPrint

    Private m_JobID As Guid
    Private m_SQL As String = ""

    Public Sub New(ByVal JobID As Guid)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_JobID = JobID

        m_SQL += "select job_no, rsk_fullname, rsk_address, rsk_postcode, all_tels, ins_company, ins_ref, ins_claim,"
        m_SQL += " desc_job, desc_est, desc_directions, desc_tools"
        m_SQL += " from Jobs"
        m_SQL += " where ID = '" + m_JobID.ToString + "'"

    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub SetCMDs(ByVal Enabled As Boolean)
        btnJobDesc.Enabled = Enabled
        btnDirections.Enabled = Enabled
        btnEstimatorsNotes.Enabled = Enabled
        btnClose.Enabled = Enabled
        Application.DoEvents()
    End Sub

    Private Sub btnJobDesc_Click(sender As Object, e As EventArgs) Handles btnJobDesc.Click

        Session.CursorWaiting()
        SetCMDs(False)

        If btnJobDesc.ShiftDown Then
            ReportHandler.DesignReport("JobDesc.repx", Session.ConnectionString, m_SQL)
        Else
            ReportHandler.RunReport("JobDesc.repx", Session.ConnectionString, m_SQL)
        End If

        SetCMDs(True)

    End Sub

    Private Sub btnDirections_Click(sender As Object, e As EventArgs) Handles btnDirections.Click

        SetCMDs(False)
        Session.CursorWaiting()

        If btnJobDesc.ShiftDown Then
            ReportHandler.DesignReport("Directions.repx", Session.ConnectionString, m_SQL)
        Else
            ReportHandler.RunReport("Directions.repx", Session.ConnectionString, m_SQL)
        End If

        SetCMDs(True)

    End Sub

    Private Sub btnEstimatorsNotes_Click(sender As Object, e As EventArgs) Handles btnEstimatorsNotes.Click

        SetCMDs(False)
        Session.CursorWaiting()

        If btnEstimatorsNotes.ShiftDown Then
            ReportHandler.DesignReport("EstimatorsNotes.repx", Session.ConnectionString, m_SQL)
        Else
            ReportHandler.RunReport("EstimatorsNotes.repx", Session.ConnectionString, m_SQL)
        End If

        SetCMDs(True)

    End Sub



End Class