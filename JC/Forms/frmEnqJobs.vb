﻿Imports Care.Global
Imports Care.Data
Imports Care.Shared
Imports DevExpress.XtraGrid.Views.Grid

Public Class frmEnqJobs

    Private Enum EnumCostsMode
        Hidden
        Profit
        WIP
    End Enum

    Private m_CostsMode As EnumCostsMode = EnumCostsMode.Hidden
    Private m_GridData As New List(Of JobRecord)
    Private m_CutOffDate As Date = Today

    Private Sub frmEnqJobs_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cbxMode.AddItem("Display Job Profitability")
        cbxMode.AddItem("Display Work In Progress Report")
        cbxMode.SelectedIndex = 0

        cbxHold.AddItem("Include Held Properties")
        cbxHold.AddItem("Exclude Held Properties")
        cbxHold.AddItem("Held Properties Only")
        cbxHold.SelectedIndex = 0

        cbxSector.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("SECTORS"))
        cbxStatus.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Job Status"))
        cbxArea.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("AREA CODES"))
        cbxInsurer.PopulateWithSQL(Session.ConnectionString, "select ins_name from Insurers order by ins_name")
        cbxController.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("CONTROLLERS"))

        gbxCosts.Hide()

    End Sub

    Private Sub btnShowCosts_Click(sender As Object, e As EventArgs) Handles btnShowCosts.Click
        If m_CostsMode = EnumCostsMode.Hidden Then
            If InputBox("Enter the password:", "Display Costs") = "showmethemoney" Then
                gbxCosts.Show()
                cdtDate.Value = Today
            End If
        Else
            gbxCosts.Visible = True
        End If
    End Sub

    Private Sub btnCostRun_Click(sender As Object, e As EventArgs) Handles btnCostRun.Click

        If cdtDate.Text = "" Then cdtDate.Value = Today
        If cbxMode.SelectedIndex = 0 Then m_CostsMode = EnumCostsMode.Profit
        If cbxMode.SelectedIndex = 1 Then m_CostsMode = EnumCostsMode.WIP

        gbxCosts.Hide()
        Application.DoEvents()

        RefreshGrid()

    End Sub

    Private Sub btnCostCancel_Click(sender As Object, e As EventArgs) Handles btnCostCancel.Click
        gbxCosts.Hide()
    End Sub

    Private Function GenerateIn(StatusList As String) As String

        Dim _Return As String = ""
        Dim _Statuses As List(Of String) = StatusList.Split(CType(",", Char())).ToList

        Dim _i As Integer = 1
        For Each _s In _Statuses
            _Return += "'" + _s.Trim + "'"
            If _i < _Statuses.Count Then _Return += ","
            _i += 1
        Next

        Return _Return

    End Function

    Private Sub RefreshGrid()

        Session.CursorWaiting()
        m_GridData.Clear()

        If cdtDate.Text <> "" AndAlso cdtDate.Value.HasValue Then m_CutOffDate = cdtDate.Value.Value

        Dim _SQL As String = ""

        _SQL += "select ID, job_no, job_status, job_start, job_end, sector, area, cont_name,"
        _SQL += vbCrLf
        _SQL += " cus_fullname, cus_address_1, rsk_fullname, rsk_address_1,"
        _SQL += vbCrLf
        _SQL += " mup_mat, mup_lab, mup_sub, mup_leak"
        _SQL += vbCrLf
        _SQL += " from Jobs"
        _SQL += vbCrLf
        _SQL += " where hide = 0"
        _SQL += vbCrLf

        If cbxSector.Text <> "" Then _SQL += " and sector = '" + cbxSector.Text + "'"
        If cbxInsurer.Text <> "" Then _SQL += " and inc_company = '" + cbxInsurer.Text + "'"

        'hold status
        'If cbxInsurer.Text <> "" Then _SQL += " and inc_company = '" + cbxInsurer.Text + "'"

        If cbxArea.Text <> "" Then _SQL += " and area = '" + cbxInsurer.Text + "'"
        If cbxStatus.Text <> "" Then _SQL += " and job_status in (" + GenerateIn(cbxStatus.Text) + ")"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            Session.SetupProgressBar("Processing Jobs...", _DT.Rows.Count)

            For Each _DR As DataRow In _DT.Rows

                Dim _j As New JobRecord
                _j.JobID = New Guid(_DR.Item("ID").ToString)
                _j.JobNo = ValueHandler.ConvertInteger(_DR.Item("job_no"))

                _j.Status = _DR.Item("job_status").ToString
                _j.Sector = _DR.Item("sector").ToString
                _j.Area = _DR.Item("area").ToString
                _j.Controller = _DR.Item("cont_name").ToString
                _j.Customer = _DR.Item("cus_fullname").ToString
                _j.CustomerAdd1 = _DR.Item("cus_address_1").ToString
                _j.Prop = _DR.Item("rsk_fullname").ToString
                _j.PropAdd1 = _DR.Item("rsk_address_1").ToString
                _j.VersionInitial = GetVersion(_j.JobID, False)
                _j.VersionCurrent = GetVersion(_j.JobID, True)
                _j.Labour = GetLabour(_j.JobID, ValueHandler.ConvertDecimal(_DR.Item("mup_lab")))
                _j.MaterialsPurchased = GetPurchasedMaterials(_j.JobID, ValueHandler.ConvertDecimal(_DR.Item("mup_mat")))
                _j.MaterialsYard = GetYardMaterials(_j.JobID, ValueHandler.ConvertDecimal(_DR.Item("mup_mat")))
                _j.SubCon = GetSubContractor(_j.JobID, ValueHandler.ConvertDecimal(_DR.Item("mup_sub")))
                _j.Total = ValueHandler.MarkUp2DP((_j.Labour + _j.MaterialsPurchased + _j.MaterialsYard + _j.SubCon), ValueHandler.ConvertDecimal(_DR.Item("mup_leak")))
                _j.Profit = _j.VersionCurrent - _j.Total

                If _j.VersionCurrent > 0 Then
                    _j.Pcnt = _j.Profit / _j.VersionCurrent * 100
                Else
                    _j.Pcnt = 0
                End If

                If _j.EndDate.HasValue AndAlso _j.EndDate.Value <= m_CutOffDate Then
                    _j.WIP = 100
                Else
                    If _j.VersionCurrent > 0 Then
                        _j.WIP = _j.Total / _j.VersionCurrent * 100
                    Else
                        _j.WIP = 0
                    End If
                End If

                m_GridData.Add(_j)

                Session.StepProgressBar()

            Next

            cgJobs.Clear()
            cgJobs.Populate(m_GridData)
            FormatGrid()
            Session.HideProgressBar()

        End If

        Session.CursorDefault()

    End Sub

    Private Sub FormatGrid()

        cgJobs.BeginUpdate()

        cgJobs.Columns("JobID").Visible = False

        Select Case m_CostsMode

            Case EnumCostsMode.Hidden
                cgJobs.Columns("VersionInitial").Visible = False
                cgJobs.Columns("VersionCurrent").Visible = False
                cgJobs.Columns("Labour").Visible = False
                cgJobs.Columns("MaterialsPurchased").Visible = False
                cgJobs.Columns("MaterialsYard").Visible = False
                cgJobs.Columns("SubCon").Visible = False
                cgJobs.Columns("Total").Visible = False
                cgJobs.Columns("Profit").Visible = False
                cgJobs.Columns("Pcnt").Visible = False
                cgJobs.Columns("WIP").Visible = False

            Case EnumCostsMode.Profit
                cgJobs.Columns("VersionInitial").Visible = True
                cgJobs.Columns("VersionCurrent").Visible = True
                cgJobs.Columns("Labour").Visible = True
                cgJobs.Columns("MaterialsPurchased").Visible = True
                cgJobs.Columns("MaterialsYard").Visible = True
                cgJobs.Columns("SubCon").Visible = True
                cgJobs.Columns("Total").Visible = True
                cgJobs.Columns("Profit").Visible = True
                cgJobs.Columns("Pcnt").Visible = True
                cgJobs.Columns("WIP").Visible = False

            Case EnumCostsMode.WIP

                cgJobs.Columns("VersionInitial").Visible = False
                cgJobs.Columns("VersionCurrent").Visible = True
                cgJobs.Columns("Labour").Visible = False
                cgJobs.Columns("MaterialsPurchased").Visible = False
                cgJobs.Columns("MaterialsYard").Visible = False
                cgJobs.Columns("SubCon").Visible = False
                cgJobs.Columns("Total").Visible = True
                cgJobs.Columns("Profit").Visible = False
                cgJobs.Columns("Pcnt").Visible = False
                cgJobs.Columns("WIP").Visible = True

        End Select

        cgJobs.EndUpdate()

    End Sub

    Private Function GetVersion(ByVal JobID As Guid, ByVal CurrentVersion As Boolean) As Decimal

        Dim _SQL As String = ""
        _SQL += "Select ver_value from JobVersions"
        _SQL += " where job_id = '" + JobID.ToString + "'"
        _SQL += " order by ver_date"

        If CurrentVersion Then _SQL += " desc"

        Return ValueHandler.ConvertDecimal(DAL.ReturnScalar(Session.ConnectionString, _SQL))

    End Function

    Private Function GetLabour(ByVal JobID As Guid, ByVal Markup As Decimal) As Decimal

        Dim _SQL As String = ""
        _SQL += "select isnull(sum(ts_cost),0) as 'ts_cost' from Timesheets"
        _SQL += " where ts_job_id = '" + JobID.ToString + "'"
        _SQL += " and ts_date <= " + ValueHandler.SQLDate(m_CutOffDate, True)

        Dim _Value As Decimal = ValueHandler.ConvertDecimal(DAL.ReturnScalar(Session.ConnectionString, _SQL))
        Return ValueHandler.MarkUp2DP(_Value, Markup)

    End Function

    Private Function GetPurchasedMaterials(ByVal JobID As Guid, ByVal Markup As Decimal) As Decimal

        Dim _SQL As String = ""
        _SQL += "select isnull(sum(inv_value),0) as 'inv_value' from PurchaseDocuments"
        _SQL += " where target_id = '" + JobID.ToString + "'"
        _SQL += " and inv_date <= " + ValueHandler.SQLDate(m_CutOffDate, True)
        _SQL += " and inv_type in ('I', 'C')"

        Dim _Value As Decimal = ValueHandler.ConvertDecimal(DAL.ReturnScalar(Session.ConnectionString, _SQL))
        Return ValueHandler.MarkUp2DP(_Value, Markup)

    End Function

    Private Function GetYardMaterials(ByVal JobID As Guid, ByVal Markup As Decimal) As Decimal

        Dim _SQL As String = ""
        _SQL += "select isnull(sum(total_price),0) as 'value' from Materials"
        _SQL += " where job_id = '" + JobID.ToString + "'"
        _SQL += " and booked_date <= " + ValueHandler.SQLDate(m_CutOffDate, True)

        Dim _Value As Decimal = ValueHandler.ConvertDecimal(DAL.ReturnScalar(Session.ConnectionString, _SQL))
        Return ValueHandler.MarkUp2DP(_Value, Markup)

    End Function

    Private Function GetSubContractor(ByVal JobID As Guid, ByVal Markup As Decimal) As Decimal

        Dim _SQL As String = ""
        _SQL += "select isnull(sum(inv_value),0) as 'inv_value' from PurchaseDocuments"
        _SQL += " where target_id = '" + JobID.ToString + "'"
        _SQL += " and inv_date <= " + ValueHandler.SQLDate(m_CutOffDate, True)
        _SQL += " and inv_type in ('CI', 'CC')"

        Dim _Value As Decimal = ValueHandler.ConvertDecimal(DAL.ReturnScalar(Session.ConnectionString, _SQL))
        Return ValueHandler.MarkUp2DP(_Value, Markup)

    End Function

    Private Class JobRecord
        Property JobID As Guid
        Property JobNo As Integer
        Property Sector As String
        Property StartDate As Date?
        Property EndDate As Date?
        Property Status As String
        Property Area As String
        Property Controller As String
        Property Customer As String
        Property CustomerAdd1 As String
        Property Prop As String
        Property PropAdd1 As String
        Property VersionInitial As Decimal
        Property VersionCurrent As Decimal
        Property Labour As Decimal
        Property MaterialsPurchased As Decimal
        Property MaterialsYard As Decimal
        Property SubCon As Decimal
        Property Total As Decimal
        Property Profit As Decimal
        Property Pcnt As Decimal
        Property WIP As Decimal
    End Class

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        RefreshGrid()
    End Sub

    Private Sub cgJobs_GridDoubleClick(sender As Object, e As EventArgs) Handles cgJobs.GridDoubleClick
        Dim _ID As String = cgJobs.GetRowCellValue(cgJobs.RowIndex, "JobID").ToString
        If _ID <> "" Then
            Business.Job.DrillDown(New Guid(_ID))
        End If
    End Sub

    Private Sub cgJobs_RowStyle(sender As Object, e As RowStyleEventArgs) Handles cgJobs.RowStyle

        If e.RowHandle < 0 Then Exit Sub
        If m_CostsMode = EnumCostsMode.Profit Then
            Dim _Pcnt As Decimal = ValueHandler.ConvertDecimal(cgJobs.UnderlyingGridView.GetRowCellValue(e.RowHandle, "Pcnt"))
            If _Pcnt >= 5 Then
                e.Appearance.BackColor = txtGreen.BackColor
            Else
                If _Pcnt <= 0 Then
                    e.Appearance.BackColor = txtRed.BackColor
                Else
                    e.Appearance.BackColor = txtYellow.BackColor
                End If
            End If
        End If

    End Sub
End Class