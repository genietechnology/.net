﻿Imports Care.Global
Imports Care.Shared

Public Class frmScanView

    Private m_DocConnectionString As String = ""
    Private m_Document As Business.Scan = Nothing
    Private m_DocID As Guid? = Nothing

    Public Sub New(ByVal DocID As Guid)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_DocID = DocID
        m_DocConnectionString = Session.ConnectionString.Replace("jc", "doc")

    End Sub

    Private Sub frmScanView_Load(sender As Object, e As EventArgs) Handles Me.Load

        Session.CursorWaiting()

        SetCMDs(False)

        If m_DocID.HasValue Then
            m_Document = Business.Scan.RetreiveByID(m_DocConnectionString, m_DocID.Value)
            If Not m_Document Is Nothing AndAlso m_Document._DocImage IsNot Nothing Then
                pic.Image = DocumentHandler.GetImagefromByteArray(m_Document._DocImage)
                pic.Properties.ZoomPercent = 40
                SetCMDs(True)
            End If
        End If

        Session.CursorDefault()

    End Sub

    Private Sub SetCMDs(ByVal Enabled As Boolean)

    End Sub

    Private Sub btn100_Click(sender As Object, e As EventArgs) Handles btn100.Click
        pic.Properties.ZoomPercent = 100
    End Sub

    Private Sub pic_MouseWheel(sender As Object, e As MouseEventArgs) Handles pic.MouseWheel

        Dim _factor As Decimal = 0.05D
        pic.Properties.ZoomPercent += e.Delta * _factor
        DevExpress.Utils.DXMouseEventArgs.GetMouseArgs(e).Handled = True

        If pic.Properties.ZoomPercent < 40 Then
            pic.Properties.ZoomPercent = 40
        Else
            If pic.Properties.ZoomPercent > 120 Then
                pic.Properties.ZoomPercent = 120
            End If
        End If

    End Sub

    Private Sub btn80_Click(sender As Object, e As EventArgs) Handles btn80.Click
        pic.Properties.ZoomPercent = 80
    End Sub

    Private Sub btn40_Click(sender As Object, e As EventArgs) Handles btn40.Click
        pic.Properties.ZoomPercent = 40
    End Sub

    Private Sub btn60_Click(sender As Object, e As EventArgs) Handles btn60.Click
        pic.Properties.ZoomPercent = 60
    End Sub

    Private Sub btnLeft_Click(sender As Object, e As EventArgs) Handles btnLeft.Click
        Dim b As System.Drawing.Bitmap = CType(pic.Image, Bitmap)
        b.RotateFlip(System.Drawing.RotateFlipType.Rotate90FlipXY)
        pic.Image = b
        SaveImageOnly()
    End Sub

    Private Sub btnRight_Click(sender As Object, e As EventArgs) Handles btnRight.Click
        Dim b As System.Drawing.Bitmap = CType(pic.Image, Bitmap)
        b.RotateFlip(System.Drawing.RotateFlipType.Rotate90FlipNone)
        pic.Image = b
        SaveImageOnly()
    End Sub

    Private Sub SaveImageOnly()
        If m_Document IsNot Nothing Then
            With m_Document
                ._DocImage = DocumentHandler.ImageToByteArray(pic.Image)
                .Store(m_DocConnectionString)
            End With
        End If
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        If m_DocID.HasValue Then
            Business.Scan.Print(m_DocID.Value, btnPrint.ShiftDown)
        End If
    End Sub
End Class