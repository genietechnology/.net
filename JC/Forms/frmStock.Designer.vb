﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStock
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.cgStock = New Care.Controls.CareGrid()
        Me.gbxItem = New DevExpress.XtraEditors.GroupControl()
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        Me.btnOK = New Care.Controls.CareButton(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.txtUOM = New Care.Controls.CareTextBox(Me.components)
        Me.txtStockDesc = New Care.Controls.CareTextBox(Me.components)
        Me.txtPrice = New Care.Controls.CareTextBox(Me.components)
        Me.txtQuickfind = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        CType(Me.gbxItem, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxItem.SuspendLayout()
        CType(Me.txtUOM.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStockDesc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPrice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtQuickfind.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'cgStock
        '
        Me.cgStock.AllowBuildColumns = True
        Me.cgStock.AllowEdit = False
        Me.cgStock.AllowHorizontalScroll = False
        Me.cgStock.AllowMultiSelect = False
        Me.cgStock.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgStock.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgStock.Appearance.Options.UseFont = True
        Me.cgStock.AutoSizeByData = True
        Me.cgStock.DisableAutoSize = False
        Me.cgStock.DisableDataFormatting = False
        Me.cgStock.FocusedRowHandle = -2147483648
        Me.cgStock.HideFirstColumn = False
        Me.cgStock.Location = New System.Drawing.Point(12, 12)
        Me.cgStock.Name = "cgStock"
        Me.cgStock.PreviewColumn = ""
        Me.cgStock.QueryID = Nothing
        Me.cgStock.RowAutoHeight = False
        Me.cgStock.SearchAsYouType = True
        Me.cgStock.ShowAutoFilterRow = False
        Me.cgStock.ShowFindPanel = True
        Me.cgStock.ShowGroupByBox = False
        Me.cgStock.ShowLoadingPanel = False
        Me.cgStock.ShowNavigator = True
        Me.cgStock.Size = New System.Drawing.Size(819, 473)
        Me.cgStock.TabIndex = 0
        '
        'gbxItem
        '
        Me.gbxItem.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.gbxItem.Controls.Add(Me.CareLabel3)
        Me.gbxItem.Controls.Add(Me.CareLabel2)
        Me.gbxItem.Controls.Add(Me.btnCancel)
        Me.gbxItem.Controls.Add(Me.btnOK)
        Me.gbxItem.Controls.Add(Me.CareLabel4)
        Me.gbxItem.Controls.Add(Me.txtUOM)
        Me.gbxItem.Controls.Add(Me.txtStockDesc)
        Me.gbxItem.Controls.Add(Me.txtPrice)
        Me.gbxItem.Controls.Add(Me.txtQuickfind)
        Me.gbxItem.Controls.Add(Me.CareLabel1)
        Me.gbxItem.Location = New System.Drawing.Point(53, 200)
        Me.gbxItem.Name = "gbxItem"
        Me.gbxItem.ShowCaption = False
        Me.gbxItem.Size = New System.Drawing.Size(737, 94)
        Me.gbxItem.TabIndex = 1
        Me.gbxItem.Text = "GroupControl2"
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(190, 11)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(60, 15)
        Me.CareLabel3.TabIndex = 2
        Me.CareLabel3.Text = "Description"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(9, 39)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(28, 15)
        Me.CareLabel2.TabIndex = 4
        Me.CareLabel2.Text = "UOM"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(626, 62)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(100, 25)
        Me.btnCancel.TabIndex = 11
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(520, 62)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(100, 25)
        Me.btnOK.TabIndex = 10
        Me.btnOK.Text = "Save"
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(9, 67)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(51, 15)
        Me.CareLabel4.TabIndex = 8
        Me.CareLabel4.Text = "Unit Price"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtUOM
        '
        Me.txtUOM.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtUOM.EnterMoveNextControl = True
        Me.txtUOM.Location = New System.Drawing.Point(67, 36)
        Me.txtUOM.MaxLength = 40
        Me.txtUOM.Name = "txtUOM"
        Me.txtUOM.NumericAllowNegatives = False
        Me.txtUOM.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtUOM.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtUOM.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtUOM.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtUOM.Properties.Appearance.Options.UseFont = True
        Me.txtUOM.Properties.Appearance.Options.UseTextOptions = True
        Me.txtUOM.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtUOM.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtUOM.Properties.MaxLength = 40
        Me.txtUOM.Size = New System.Drawing.Size(100, 22)
        Me.txtUOM.TabIndex = 5
        Me.txtUOM.Tag = "A"
        Me.txtUOM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtUOM.ToolTipText = ""
        '
        'txtStockDesc
        '
        Me.txtStockDesc.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtStockDesc.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtStockDesc.EnterMoveNextControl = True
        Me.txtStockDesc.Location = New System.Drawing.Point(256, 8)
        Me.txtStockDesc.MaxLength = 100
        Me.txtStockDesc.Name = "txtStockDesc"
        Me.txtStockDesc.NumericAllowNegatives = False
        Me.txtStockDesc.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtStockDesc.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStockDesc.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtStockDesc.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtStockDesc.Properties.Appearance.Options.UseFont = True
        Me.txtStockDesc.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStockDesc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtStockDesc.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtStockDesc.Properties.MaxLength = 100
        Me.txtStockDesc.Size = New System.Drawing.Size(470, 20)
        Me.txtStockDesc.TabIndex = 3
        Me.txtStockDesc.Tag = "A"
        Me.txtStockDesc.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtStockDesc.ToolTipText = ""
        '
        'txtPrice
        '
        Me.txtPrice.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtPrice.EnterMoveNextControl = True
        Me.txtPrice.Location = New System.Drawing.Point(67, 64)
        Me.txtPrice.MaxLength = 0
        Me.txtPrice.Name = "txtPrice"
        Me.txtPrice.NumericAllowNegatives = False
        Me.txtPrice.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtPrice.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPrice.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPrice.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtPrice.Properties.Appearance.Options.UseFont = True
        Me.txtPrice.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPrice.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPrice.Size = New System.Drawing.Size(100, 22)
        Me.txtPrice.TabIndex = 9
        Me.txtPrice.Tag = "A"
        Me.txtPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPrice.ToolTipText = ""
        '
        'txtQuickfind
        '
        Me.txtQuickfind.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtQuickfind.EnterMoveNextControl = True
        Me.txtQuickfind.Location = New System.Drawing.Point(67, 8)
        Me.txtQuickfind.MaxLength = 10
        Me.txtQuickfind.Name = "txtQuickfind"
        Me.txtQuickfind.NumericAllowNegatives = False
        Me.txtQuickfind.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtQuickfind.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtQuickfind.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtQuickfind.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtQuickfind.Properties.Appearance.Options.UseFont = True
        Me.txtQuickfind.Properties.Appearance.Options.UseTextOptions = True
        Me.txtQuickfind.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtQuickfind.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtQuickfind.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtQuickfind.Properties.MaxLength = 10
        Me.txtQuickfind.Size = New System.Drawing.Size(100, 22)
        Me.txtQuickfind.TabIndex = 1
        Me.txtQuickfind.Tag = "A"
        Me.txtQuickfind.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtQuickfind.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(9, 11)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(52, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Quickfind"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmStock
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(843, 497)
        Me.Controls.Add(Me.gbxItem)
        Me.Controls.Add(Me.cgStock)
        Me.LoadMaximised = True
        Me.Name = "frmStock"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.gbxItem, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxItem.ResumeLayout(False)
        Me.gbxItem.PerformLayout()
        CType(Me.txtUOM.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStockDesc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPrice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtQuickfind.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents cgStock As Care.Controls.CareGrid
    Friend WithEvents gbxItem As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents txtUOM As Care.Controls.CareTextBox
    Friend WithEvents txtStockDesc As Care.Controls.CareTextBox
    Friend WithEvents txtPrice As Care.Controls.CareTextBox
    Friend WithEvents txtQuickfind As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
End Class
