﻿Imports Care.Global
Imports Care.Shared
Imports Care.Data
Imports DevExpress.XtraGrid.Views.Base

Public Class frmScanSplit

    Private m_DocConnectionString As String = ""
    Private m_DocumentID As Guid
    Private m_TargetID As Guid? = Nothing
    Private m_Scan As Business.Scan

    Public Sub New(ByVal DocumentConnectionString As String, ByVal DocumentID As Guid)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_DocumentID = DocumentID
        m_DocConnectionString = DocumentConnectionString

    End Sub

    Private Sub frmScanSplit_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        txtRef.BackColor = Session.FindColour

        cbxCategory.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Document Category"))
        cbxCategory.BackColor = Session.FindColour
        cbxCategory.AllowBlank = True

        cbxWorkType.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("INVOICE / CREDIT CATEGORIES"))
        cbxWorkType.BackColor = Session.ChangeColour

        txtNet.BackColor = Session.ChangeColour

        Dim _S As Business.Scan = Business.Scan.RetreiveByID(m_DocConnectionString, m_DocumentID)
        If _S IsNot Nothing Then

            m_Scan = _S
            txtDocDate.Text = _S._DocDate.Value.ToShortDateString
            txtDocSupplier.Text = _S._DocSupplierName
            txtDocValue.Text = ValueHandler.MoneyAsText(_S._DocNet)

            DisplayPurchaseDocuments()

        End If

        Session.CursorDefault()

    End Sub

    Private Sub DisplayPurchaseDocuments()

        Session.CursorWaiting()

        Dim _SQL As String = ""

        _SQL += "select d.ID, d.target_type as 'Target Type',"
        _SQL += " case"
        _SQL += " when d.target_type = 'Job' then cast(j.job_no as varchar(10))"
        _SQL += " when d.target_type = 'Plant' then p.reg_no"
        _SQL += " else ' '"
        _SQL += " end"
        _SQL += " as 'Ref',"
        _SQL += " case"
        _SQL += " when d.target_type = 'Job' then j.rsk_fullname + ', ' + j.rsk_address_1"
        _SQL += " when d.target_type = 'Plant' then p.reg_no + ' - ' + p.name"
        _SQL += " else 'Category'"
        _SQL += " end"
        _SQL += " as 'Target',"
        _SQL += " d.inv_desc as 'Work Type',"
        _SQL += " d.inv_value as 'Value'"
        _SQL += " from PurchaseDocuments d"
        _SQL += " left join Jobs j on j.ID = d.target_id"
        _SQL += " left join Plant p on p.ID = d.target_id"
        _SQL += " where doc_id = '" + m_DocumentID.ToString + "'"
        _SQL += " order by inv_date desc"

        cgTargets.HideFirstColumn = True
        cgTargets.Populate(Session.ConnectionString, _SQL)
        cgTargets.AutoSizeColumns()

        _SQL = ""
        _SQL += "select sum(inv_value) as 'total' from PurchaseDocuments"
        _SQL += " where doc_id = '" + m_DocumentID.ToString + "'"

        Dim _Total As Decimal = ValueHandler.ConvertDecimal(DAL.ReturnScalar(Session.ConnectionString, _SQL))
        txtAllocated.Text = ValueHandler.MoneyAsText(_Total)
        txtRemain.Text = ValueHandler.MoneyAsText(m_Scan._DocNet - _Total)

        Session.CursorDefault()

    End Sub

    Private Sub SaveDocument()

        Dim _D As New Business.PurchaseDocument

        If gbxDoc.Tag IsNot Nothing AndAlso gbxDoc.Tag.ToString <> "" Then
            _D = Business.PurchaseDocument.RetreiveByID(New Guid(gbxDoc.Tag.ToString))
        Else

            _D._DocId = m_DocumentID

            If m_Scan._DocType = "Supplier Invoice" Then _D._InvType = "I"
            If m_Scan._DocType = "Supplier Credit" Then _D._InvType = "C"
            If m_Scan._DocType = "Contractor Invoice" Then _D._InvType = "CI"
            If m_Scan._DocType = "Contractor Credit" Then _D._InvType = "CC"

            _D._InvSuppId = m_Scan._DocSupplierId
            _D._InvSuppName = m_Scan._DocSupplierName

        End If

        _D._InvEnteredDate = Today
        _D._InvEnteredStamp = Now
        _D._InvEnteredUser = Session.CurrentUser.UserCode
        _D._InvEnteredUserName = Session.CurrentUser.FullName

        If radMatchCategory.Checked Then _D._TargetType = "Category"
        If radMatchPlant.Checked Then _D._TargetType = "Plant"
        If radMatchJob.Checked Then _D._TargetType = "Job"

        If radMatchCategory.Checked Then
            _D._InvCategory = cbxCategory.Text
        Else
            _D._InvCategory = cbxWorkType.Text
        End If

        _D._TargetId = m_TargetID
        _D._InvDate = m_Scan._DocDate
        _D._InvValue = ValueHandler.ConvertDecimal(txtNet.Text)

        _D.Store()

    End Sub

    Private Sub DeleteDocument()
        If CareMessage("Are you sure you want to Delete this Document (this will not delete the Scan) ?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Delete Document") = DialogResult.Yes Then
            Dim _SQL As String = "delete from PurchaseDocuments where ID = '" + gbxDoc.Tag.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)
        End If
    End Sub

    Private Function ValidateEntry() As Boolean

        If radMatchCategory.Checked Then
            If cbxCategory.Text = "" Then
                CareMessage("Please select a category.", MessageBoxIcon.Exclamation, "Validate Entry")
                Return False
            End If
        Else

            If txtRef.Text = "" Then
                If radMatchJob.Checked Then
                    CareMessage("Please enter a Job Number.", MessageBoxIcon.Exclamation, "Validate Entry")
                    Return False
                Else
                    CareMessage("Please enter a Vehicle Registration.", MessageBoxIcon.Exclamation, "Validate Entry")
                    Return False
                End If
            Else
                If Not m_TargetID.HasValue Then
                    CareMessage("Invalid Reference!", MessageBoxIcon.Exclamation, "Validate Entry")
                    Return False
                End If
            End If

        End If

        'if we get this far, check we have matched the rest of the document details
        If Not radMatchCategory.Checked Then

            If cbxWorkType.Text = "" Then
                CareMessage("Please select an item description.", MessageBoxIcon.Exclamation, "Validate Entry")
                Return False
            End If

            If txtNet.Text = "" Then
                CareMessage("Please enter the Net (before VAT) value.", MessageBoxIcon.Exclamation, "Validate Entry")
                Return False
            End If

            Dim _Value As Decimal = ValueHandler.ConvertDecimal(txtNet.Text)

            If m_Scan._DocType.Contains("Credit") Then
                If _Value > 0 Then
                    CareMessage("Please enter the credit amount as a negative value.", MessageBoxIcon.Exclamation, "Validate Entry")
                    Return False
                End If
            Else
                If _Value < 0 Then
                    CareMessage("Please enter the invoice amount as a positive value.", MessageBoxIcon.Exclamation, "Validate Entry")
                    Return False
                End If
            End If

            'ensure we have not changed the target
            If m_Scan IsNot Nothing Then

                If m_Scan._MatchedTarget = "Job" And Not radMatchJob.Checked Then
                    CareMessage("You cannot amend the Target Type.", MessageBoxIcon.Exclamation, "Validate Entry")
                    Return False
                End If

                If m_Scan._MatchedTarget = "Plant" And Not radMatchPlant.Checked Then
                    CareMessage("You cannot amend the Target Type.", MessageBoxIcon.Exclamation, "Validate Entry")
                    Return False
                End If

                If m_Scan._MatchedTarget = "Category" And Not radMatchCategory.Checked Then
                    CareMessage("You cannot amend the Target Type.", MessageBoxIcon.Exclamation, "Validate Entry")
                    Return False
                End If

            End If

        End If

        Return True

    End Function


    Private Sub SetMatch()
        If radMatchCategory.Checked Then
            cbxCategory.Show()
            cbxCategory.Focus()
            txtRef.Hide()
            btnSearch.Hide()
            txtName.Text = ""
            txtRef1.Text = ""
        Else
            cbxCategory.Hide()
            txtRef.Show()
            btnSearch.Show()
            txtRef.Focus()
        End If
    End Sub

    Private Function ContainsLetters(ByVal StringToCheck As String) As Boolean
        For Each _c As Char In StringToCheck.ToCharArray
            If Char.IsLetter(_c) Then Return True
        Next
        Return False
    End Function

    Private Sub SetJobDetails(ByRef Job As Business.Job)

        m_TargetID = Job._ID

        txtRef.Text = Job._JobNo.ToString
        txtName.Text = Job._RskFullname
        txtRef1.Text = Job._RskAddress1

    End Sub

    Private Sub SetPlantDetails(ByRef Plant As Business.Plant)

        m_TargetID = Plant._ID

        txtRef.Text = Plant._AjNo.ToString
        txtName.Text = Plant._Name
        txtRef1.Text = Plant._RegNo

    End Sub

    Private Sub SetDocDetails(ByVal RowIndex As Integer)

        If cgTargets.GetRowCellValue(RowIndex, "Target Type").ToString = "Job" Then radMatchJob.Checked = True
        If cgTargets.GetRowCellValue(RowIndex, "Target Type").ToString = "Plant" Then radMatchPlant.Checked = True
        If cgTargets.GetRowCellValue(RowIndex, "Target Type").ToString = "Category" Then radMatchCategory.Checked = True
        SetMatch()

        gbxDoc.Tag = cgTargets.GetRowCellValue(RowIndex, "ID").ToString
        txtRef.Text = cgTargets.GetRowCellValue(RowIndex, "Ref").ToString
        ValidateRef()

        cbxWorkType.Text = cgTargets.GetRowCellValue(RowIndex, "Work Type").ToString
        txtNet.Text = ValueHandler.MoneyAsText(ValueHandler.ConvertDecimal(cgTargets.GetRowCellValue(RowIndex, "Value")))

    End Sub

    Private Sub ValidateRef()

        If txtRef.Text <> "" Then

            If radMatchJob.Checked Then

                If Not ContainsLetters(txtRef.Text) Then

                    Dim _j As Business.Job = Business.Job.RetreiveByJobNo(txtRef.Text)
                    If _j IsNot Nothing Then
                        SetJobDetails(_j)
                    Else
                        CareMessage("Job does Not exist - please check the number.", MessageBoxIcon.Exclamation, "Find Job")
                        Clear()
                    End If

                Else
                    CareMessage("Invalid Job Number - please check the number.", MessageBoxIcon.Exclamation, "Find Job")
                    Clear()
                End If

            End If

            If radMatchPlant.Checked Then

                Dim _p As Business.Plant = Nothing

                If ContainsLetters(txtRef.Text) Then
                    _p = Business.Plant.RetreiveByReg(txtRef.Text)
                Else
                    _p = Business.Plant.RetreiveByNo(txtRef.Text)
                End If

                If _p IsNot Nothing Then
                    SetPlantDetails(_p)
                Else
                    CareMessage("Invalid Plant - please check the registration Or number.", MessageBoxIcon.Exclamation, "Find Job")
                    Clear()
                End If

            End If

        Else
            Clear()
        End If

    End Sub

    Private Sub Clear()
        gbxDoc.Tag = ""
        m_TargetID = Nothing
        txtRef.Text = ""
        txtName.Text = ""
        txtRef1.Text = ""
        cbxCategory.Text = ""
        cbxWorkType.Text = ""
        txtNet.Text = ""
    End Sub

    Private Sub ToggleDocument(ByVal Create As Boolean, ByVal Show As Boolean)

        gbxDoc.Visible = Show

        If Show Then
            If Create Then
                radMatchJob.Checked = True
                SetMatch()
                Clear()
                txtRef.Focus()
            Else
                txtNet.Focus()
            End If
        End If

        ToggleFormButtons(Not Show)

    End Sub

    Private Sub ToggleFormButtons(ByVal Enabled As Boolean)

        btnAdd.Enabled = Enabled

        If Enabled Then
            btnEdit.Enabled = cgTargets.RecordCount > 0
            btnRemove.Enabled = cgTargets.RecordCount > 0
        Else
            btnEdit.Enabled = False
            btnRemove.Enabled = False
        End If

        btnClose.Enabled = Enabled

    End Sub

#Region "Controls"

    Private Sub radMatchCategory_CheckedChanged(sender As Object, e As EventArgs) Handles radMatchCategory.CheckedChanged
        If radMatchCategory.Checked Then SetMatch()
    End Sub

    Private Sub radMatchJob_CheckedChanged(sender As Object, e As EventArgs) Handles radMatchJob.CheckedChanged
        If radMatchJob.Checked Then SetMatch()
    End Sub

    Private Sub radMatchPlant_CheckedChanged(sender As Object, e As EventArgs) Handles radMatchPlant.CheckedChanged
        If radMatchPlant.Checked Then SetMatch()
    End Sub

    Private Sub txtRef_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtRef.Validating
        ValidateRef()
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        If ValueHandler.ConvertDecimal(txtRemain.Text) <> 0 Then
            CareMessage("Please ensure you fully allocate the Invoice Amount amoungst Jobs.", MessageBoxIcon.Exclamation, "Allocation Error")
            Exit Sub
        End If
        Me.Close()
    End Sub

    Private Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click
        DeleteDocument()
        DisplayPurchaseDocuments()
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        radMatchJob.Checked = True
        SetMatch()
        ToggleDocument(True, True)
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        ToggleDocument(False, True)
    End Sub

    Private Sub btnDocSave_Click(sender As Object, e As EventArgs) Handles btnDocSave.Click
        If ValidateEntry() Then
            SaveDocument()
            ToggleDocument(False, False)
            DisplayPurchaseDocuments()
        End If
    End Sub

    Private Sub btnDocCancel_Click(sender As Object, e As EventArgs) Handles btnDocCancel.Click
        ToggleDocument(False, False)
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click

        If radMatchJob.Checked Then
            Dim _ID As String = Business.Job.FindAll
            If _ID <> "" Then
                Dim _J As Business.Job = Business.Job.RetreiveByID(New Guid(_ID))
                If _J IsNot Nothing Then
                    SetJobDetails(_J)
                End If
            End If
        End If

        If radMatchPlant.Checked Then
            Dim _ID As String = Business.Plant.Find
            If _ID <> "" Then
                Dim _P As Business.Plant = Business.Plant.RetreiveByID(New Guid(_ID))
                If _P IsNot Nothing Then
                    SetPlantDetails(_P)
                End If
            End If
        End If

    End Sub

    Private Sub cgTargets_FocusedRowChanged(sender As Object, e As FocusedRowChangedEventArgs) Handles cgTargets.FocusedRowChanged
        If e Is Nothing Then Exit Sub
        If e.FocusedRowHandle < 0 Then Exit Sub
        SetDocDetails(e.FocusedRowHandle)
    End Sub

    Private Sub cgTargets_GridDoubleClick(sender As Object, e As EventArgs) Handles cgTargets.GridDoubleClick
        If cgTargets.RecordCount <= 0 Then Exit Sub
        SetDocDetails(cgTargets.FocusedRowHandle)
        ToggleDocument(False, True)
    End Sub

#End Region

End Class