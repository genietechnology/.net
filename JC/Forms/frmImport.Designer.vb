﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.CareButton4 = New Care.Controls.CareButton(Me.components)
        Me.CareButton3 = New Care.Controls.CareButton(Me.components)
        Me.CareButton2 = New Care.Controls.CareButton(Me.components)
        Me.CareButton1 = New Care.Controls.CareButton(Me.components)
        Me.CareButton5 = New Care.Controls.CareButton(Me.components)
        Me.CareButton6 = New Care.Controls.CareButton(Me.components)
        Me.CareButton7 = New Care.Controls.CareButton(Me.components)
        Me.SuspendLayout()
        '
        'CareButton4
        '
        Me.CareButton4.Location = New System.Drawing.Point(12, 70)
        Me.CareButton4.Name = "CareButton4"
        Me.CareButton4.Size = New System.Drawing.Size(260, 23)
        Me.CareButton4.TabIndex = 3
        Me.CareButton4.Text = "Import Insurers"
        Me.CareButton4.Visible = False
        '
        'CareButton3
        '
        Me.CareButton3.Enabled = False
        Me.CareButton3.Location = New System.Drawing.Point(12, 41)
        Me.CareButton3.Name = "CareButton3"
        Me.CareButton3.Size = New System.Drawing.Size(260, 23)
        Me.CareButton3.TabIndex = 2
        Me.CareButton3.Text = "Import Suppliers"
        Me.CareButton3.Visible = False
        '
        'CareButton2
        '
        Me.CareButton2.Location = New System.Drawing.Point(12, 137)
        Me.CareButton2.Name = "CareButton2"
        Me.CareButton2.Size = New System.Drawing.Size(260, 23)
        Me.CareButton2.TabIndex = 1
        Me.CareButton2.Text = "Import Jobs"
        Me.CareButton2.Visible = False
        '
        'CareButton1
        '
        Me.CareButton1.Enabled = False
        Me.CareButton1.Location = New System.Drawing.Point(12, 12)
        Me.CareButton1.Name = "CareButton1"
        Me.CareButton1.Size = New System.Drawing.Size(260, 23)
        Me.CareButton1.TabIndex = 0
        Me.CareButton1.Text = "Create Lists"
        Me.CareButton1.Visible = False
        '
        'CareButton5
        '
        Me.CareButton5.Enabled = False
        Me.CareButton5.Location = New System.Drawing.Point(12, 99)
        Me.CareButton5.Name = "CareButton5"
        Me.CareButton5.Size = New System.Drawing.Size(260, 23)
        Me.CareButton5.TabIndex = 4
        Me.CareButton5.Text = "Import Employees"
        Me.CareButton5.Visible = False
        '
        'CareButton6
        '
        Me.CareButton6.Location = New System.Drawing.Point(296, 12)
        Me.CareButton6.Name = "CareButton6"
        Me.CareButton6.Size = New System.Drawing.Size(260, 23)
        Me.CareButton6.TabIndex = 5
        Me.CareButton6.Text = "Fix Timesheets"
        '
        'CareButton7
        '
        Me.CareButton7.Location = New System.Drawing.Point(296, 41)
        Me.CareButton7.Name = "CareButton7"
        Me.CareButton7.Size = New System.Drawing.Size(260, 23)
        Me.CareButton7.TabIndex = 6
        Me.CareButton7.Text = "Fix Duplicate Job Allocation on Scans"
        '
        'frmImport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(693, 261)
        Me.Controls.Add(Me.CareButton7)
        Me.Controls.Add(Me.CareButton6)
        Me.Controls.Add(Me.CareButton5)
        Me.Controls.Add(Me.CareButton4)
        Me.Controls.Add(Me.CareButton3)
        Me.Controls.Add(Me.CareButton2)
        Me.Controls.Add(Me.CareButton1)
        Me.Name = "frmImport"
        Me.Text = "frmImport"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CareButton1 As Care.Controls.CareButton
    Friend WithEvents CareButton2 As Care.Controls.CareButton
    Friend WithEvents CareButton3 As Care.Controls.CareButton
    Friend WithEvents CareButton4 As Care.Controls.CareButton
    Friend WithEvents CareButton5 As Care.Controls.CareButton
    Friend WithEvents CareButton6 As Care.Controls.CareButton
    Friend WithEvents CareButton7 As Care.Controls.CareButton
End Class
