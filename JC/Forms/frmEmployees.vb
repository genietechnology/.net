﻿Imports Care.Global
Imports Care.Data
Imports Care.Shared

Public Class frmEmployees

    Private m_Employee As Business.Employee

    Protected Overrides Sub SetBindings()

        m_Employee = New Business.Employee
        bs.DataSource = m_Employee

        '******************************************************************************************************************************
        txtCode.DataBindings.Add("Text", bs, "_Code")
        txtFullname.DataBindings.Add("Text", bs, "_Fullname")
        txtNickname.DataBindings.Add("Text", bs, "_Nickname")
        txtRate.DataBindings.Add("Text", bs, "_Rate")
        chkOffice.DataBindings.Add("Checked", bs, "_Office")
        chkActive.DataBindings.Add("Checked", bs, "_Active")
        '******************************************************************************************************************************

    End Sub

    Protected Overrides Sub FindRecord()
        Dim _ReturnValue As String = Business.Employee.Find
        If _ReturnValue <> "" Then
            DisplayRecord(New Guid(_ReturnValue))
        End If
    End Sub

    Private Sub DisplayRecord(ByVal PlantID As Guid)

        MyBase.RecordID = PlantID
        MyBase.RecordPopulated = True

        m_Employee = Business.Employee.RetreiveByID(PlantID)
        bs.DataSource = m_Employee

        txtTel.Text = m_Employee._TelMobile
        txtEmail.Text = m_Employee._Email

        BuildTitleText()

    End Sub

    Protected Overrides Sub CommitUpdate()

        m_Employee = CType(bs.Item(bs.Position), Business.Employee)

        m_Employee._TelMobile = txtTel.Text
        m_Employee._Email = txtEmail.Text

        m_Employee.Store()

        BuildTitleText()

    End Sub

    Private Sub BuildTitleText()
        Me.Text = "Employee (" + m_Employee._Code.ToString + ") " + m_Employee._Fullname
    End Sub

End Class
