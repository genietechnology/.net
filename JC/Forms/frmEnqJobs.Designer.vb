﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEnqJobs
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling2 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling3 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.Panel1 = New DevExpress.XtraEditors.PanelControl()
        Me.btnShowCosts = New Care.Controls.CareButton(Me.components)
        Me.btnJobListing = New Care.Controls.CareButton(Me.components)
        Me.btnJobDesc = New Care.Controls.CareButton(Me.components)
        Me.btnJobComplete = New Care.Controls.CareButton(Me.components)
        Me.btnViewJob = New Care.Controls.CareButton(Me.components)
        Me.gbxDetails = New DevExpress.XtraEditors.GroupControl()
        Me.btnRefresh = New Care.Controls.CareButton(Me.components)
        Me.btnClear = New Care.Controls.CareButton(Me.components)
        Me.cbxController = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.cbxArea = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.cbxStatus = New Care.Controls.CareCheckedComboBox(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.cbxHold = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.cbxInsurer = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.cbxSector = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.cgJobs = New Care.Controls.CareGrid()
        Me.gbxCosts = New DevExpress.XtraEditors.GroupControl()
        Me.btnCostCancel = New Care.Controls.CareButton(Me.components)
        Me.btnCostRun = New Care.Controls.CareButton(Me.components)
        Me.CareLabel12 = New Care.Controls.CareLabel(Me.components)
        Me.cdtDate = New Care.Controls.CareDateTime(Me.components)
        Me.cbxMode = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.txtGreen = New DevExpress.XtraEditors.TextEdit()
        Me.txtRed = New DevExpress.XtraEditors.TextEdit()
        Me.txtYellow = New DevExpress.XtraEditors.TextEdit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.gbxDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxDetails.SuspendLayout()
        CType(Me.cbxController.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxArea.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxHold.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxInsurer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSector.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxCosts, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxCosts.SuspendLayout()
        CType(Me.cdtDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxMode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.Panel1.Controls.Add(Me.txtGreen)
        Me.Panel1.Controls.Add(Me.txtRed)
        Me.Panel1.Controls.Add(Me.txtYellow)
        Me.Panel1.Controls.Add(Me.btnShowCosts)
        Me.Panel1.Controls.Add(Me.btnJobListing)
        Me.Panel1.Controls.Add(Me.btnJobDesc)
        Me.Panel1.Controls.Add(Me.btnJobComplete)
        Me.Panel1.Controls.Add(Me.btnViewJob)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 527)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(984, 34)
        Me.Panel1.TabIndex = 3
        '
        'btnShowCosts
        '
        Me.btnShowCosts.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShowCosts.Appearance.Options.UseFont = True
        Me.btnShowCosts.Location = New System.Drawing.Point(12, 3)
        Me.btnShowCosts.Name = "btnShowCosts"
        Me.btnShowCosts.Size = New System.Drawing.Size(140, 23)
        Me.btnShowCosts.TabIndex = 4
        Me.btnShowCosts.Text = "Show Costs"
        '
        'btnJobListing
        '
        Me.btnJobListing.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJobListing.Appearance.Options.UseFont = True
        Me.btnJobListing.Location = New System.Drawing.Point(596, 3)
        Me.btnJobListing.Name = "btnJobListing"
        Me.btnJobListing.Size = New System.Drawing.Size(140, 23)
        Me.btnJobListing.TabIndex = 3
        Me.btnJobListing.Text = "Print Job Listing"
        Me.btnJobListing.Visible = False
        '
        'btnJobDesc
        '
        Me.btnJobDesc.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJobDesc.Appearance.Options.UseFont = True
        Me.btnJobDesc.Location = New System.Drawing.Point(450, 3)
        Me.btnJobDesc.Name = "btnJobDesc"
        Me.btnJobDesc.Size = New System.Drawing.Size(140, 23)
        Me.btnJobDesc.TabIndex = 2
        Me.btnJobDesc.Text = "Print Job Description"
        Me.btnJobDesc.Visible = False
        '
        'btnJobComplete
        '
        Me.btnJobComplete.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJobComplete.Appearance.Options.UseFont = True
        Me.btnJobComplete.Location = New System.Drawing.Point(304, 3)
        Me.btnJobComplete.Name = "btnJobComplete"
        Me.btnJobComplete.Size = New System.Drawing.Size(140, 23)
        Me.btnJobComplete.TabIndex = 1
        Me.btnJobComplete.Text = "Mark Job Complete"
        Me.btnJobComplete.Visible = False
        '
        'btnViewJob
        '
        Me.btnViewJob.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnViewJob.Appearance.Options.UseFont = True
        Me.btnViewJob.Location = New System.Drawing.Point(158, 3)
        Me.btnViewJob.Name = "btnViewJob"
        Me.btnViewJob.Size = New System.Drawing.Size(140, 23)
        Me.btnViewJob.TabIndex = 0
        Me.btnViewJob.Text = "View Job"
        Me.btnViewJob.Visible = False
        '
        'gbxDetails
        '
        Me.gbxDetails.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxDetails.Controls.Add(Me.btnRefresh)
        Me.gbxDetails.Controls.Add(Me.btnClear)
        Me.gbxDetails.Controls.Add(Me.cbxController)
        Me.gbxDetails.Controls.Add(Me.CareLabel6)
        Me.gbxDetails.Controls.Add(Me.cbxArea)
        Me.gbxDetails.Controls.Add(Me.CareLabel7)
        Me.gbxDetails.Controls.Add(Me.cbxStatus)
        Me.gbxDetails.Controls.Add(Me.CareLabel4)
        Me.gbxDetails.Controls.Add(Me.cbxHold)
        Me.gbxDetails.Controls.Add(Me.CareLabel5)
        Me.gbxDetails.Controls.Add(Me.cbxInsurer)
        Me.gbxDetails.Controls.Add(Me.CareLabel3)
        Me.gbxDetails.Controls.Add(Me.cbxSector)
        Me.gbxDetails.Controls.Add(Me.CareLabel2)
        Me.gbxDetails.Location = New System.Drawing.Point(12, 9)
        Me.gbxDetails.Name = "gbxDetails"
        Me.gbxDetails.ShowCaption = False
        Me.gbxDetails.Size = New System.Drawing.Size(965, 69)
        Me.gbxDetails.TabIndex = 0
        '
        'btnRefresh
        '
        Me.btnRefresh.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRefresh.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefresh.Appearance.Options.UseFont = True
        Me.btnRefresh.Location = New System.Drawing.Point(875, 37)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(80, 23)
        Me.btnRefresh.TabIndex = 13
        Me.btnRefresh.Text = "Refresh"
        '
        'btnClear
        '
        Me.btnClear.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClear.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.Appearance.Options.UseFont = True
        Me.btnClear.Location = New System.Drawing.Point(875, 8)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(80, 23)
        Me.btnClear.TabIndex = 12
        Me.btnClear.Text = "Clear Filters"
        '
        'cbxController
        '
        Me.cbxController.AllowBlank = False
        Me.cbxController.DataSource = Nothing
        Me.cbxController.DisplayMember = Nothing
        Me.cbxController.EnterMoveNextControl = True
        Me.cbxController.Location = New System.Drawing.Point(345, 38)
        Me.cbxController.Name = "cbxController"
        Me.cbxController.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxController.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxController.Properties.Appearance.Options.UseFont = True
        Me.cbxController.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxController.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxController.SelectedValue = Nothing
        Me.cbxController.Size = New System.Drawing.Size(200, 22)
        Me.cbxController.TabIndex = 11
        Me.cbxController.ValueMember = Nothing
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(284, 41)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(53, 15)
        Me.CareLabel6.TabIndex = 10
        Me.CareLabel6.Text = "Controller"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxArea
        '
        Me.cbxArea.AllowBlank = False
        Me.cbxArea.DataSource = Nothing
        Me.cbxArea.DisplayMember = Nothing
        Me.cbxArea.EnterMoveNextControl = True
        Me.cbxArea.Location = New System.Drawing.Point(625, 10)
        Me.cbxArea.Name = "cbxArea"
        Me.cbxArea.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxArea.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxArea.Properties.Appearance.Options.UseFont = True
        Me.cbxArea.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxArea.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxArea.SelectedValue = Nothing
        Me.cbxArea.Size = New System.Drawing.Size(200, 22)
        Me.cbxArea.TabIndex = 9
        Me.cbxArea.ValueMember = Nothing
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(557, 13)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(24, 15)
        Me.CareLabel7.TabIndex = 8
        Me.CareLabel7.Text = "Area"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxStatus
        '
        Me.cbxStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxStatus.DataSource = Nothing
        Me.cbxStatus.DisplayMember = Nothing
        Me.cbxStatus.EditValue = ""
        Me.cbxStatus.EnterMoveNextControl = True
        Me.cbxStatus.Location = New System.Drawing.Point(625, 38)
        Me.cbxStatus.Name = "cbxStatus"
        Me.cbxStatus.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxStatus.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxStatus.Properties.Appearance.Options.UseFont = True
        Me.cbxStatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxStatus.SelectedItems = Nothing
        Me.cbxStatus.Size = New System.Drawing.Size(244, 22)
        Me.cbxStatus.TabIndex = 7
        Me.cbxStatus.ValueMember = Nothing
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(557, 41)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel4.TabIndex = 6
        Me.CareLabel4.Text = "Status"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxHold
        '
        Me.cbxHold.AllowBlank = False
        Me.cbxHold.DataSource = Nothing
        Me.cbxHold.DisplayMember = Nothing
        Me.cbxHold.EnterMoveNextControl = True
        Me.cbxHold.Location = New System.Drawing.Point(345, 10)
        Me.cbxHold.Name = "cbxHold"
        Me.cbxHold.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxHold.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxHold.Properties.Appearance.Options.UseFont = True
        Me.cbxHold.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxHold.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxHold.SelectedValue = Nothing
        Me.cbxHold.Size = New System.Drawing.Size(200, 22)
        Me.cbxHold.TabIndex = 5
        Me.cbxHold.ValueMember = Nothing
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(284, 13)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(26, 15)
        Me.CareLabel5.TabIndex = 4
        Me.CareLabel5.Text = "Hold"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxInsurer
        '
        Me.cbxInsurer.AllowBlank = False
        Me.cbxInsurer.DataSource = Nothing
        Me.cbxInsurer.DisplayMember = Nothing
        Me.cbxInsurer.EnterMoveNextControl = True
        Me.cbxInsurer.Location = New System.Drawing.Point(60, 38)
        Me.cbxInsurer.Name = "cbxInsurer"
        Me.cbxInsurer.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxInsurer.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxInsurer.Properties.Appearance.Options.UseFont = True
        Me.cbxInsurer.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxInsurer.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxInsurer.SelectedValue = Nothing
        Me.cbxInsurer.Size = New System.Drawing.Size(200, 22)
        Me.cbxInsurer.TabIndex = 3
        Me.cbxInsurer.ValueMember = Nothing
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(10, 41)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(36, 15)
        Me.CareLabel3.TabIndex = 2
        Me.CareLabel3.Text = "Insurer"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxSector
        '
        Me.cbxSector.AllowBlank = False
        Me.cbxSector.DataSource = Nothing
        Me.cbxSector.DisplayMember = Nothing
        Me.cbxSector.EnterMoveNextControl = True
        Me.cbxSector.Location = New System.Drawing.Point(60, 10)
        Me.cbxSector.Name = "cbxSector"
        Me.cbxSector.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSector.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxSector.Properties.Appearance.Options.UseFont = True
        Me.cbxSector.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSector.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSector.SelectedValue = Nothing
        Me.cbxSector.Size = New System.Drawing.Size(200, 22)
        Me.cbxSector.TabIndex = 1
        Me.cbxSector.ValueMember = Nothing
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(10, 13)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(33, 15)
        Me.CareLabel2.TabIndex = 0
        Me.CareLabel2.Text = "Sector"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cgJobs
        '
        Me.cgJobs.AllowBuildColumns = True
        Me.cgJobs.AllowEdit = False
        Me.cgJobs.AllowHorizontalScroll = False
        Me.cgJobs.AllowMultiSelect = False
        Me.cgJobs.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgJobs.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgJobs.Appearance.Options.UseFont = True
        Me.cgJobs.AutoSizeByData = True
        Me.cgJobs.DisableAutoSize = False
        Me.cgJobs.DisableDataFormatting = False
        Me.cgJobs.FocusedRowHandle = -2147483648
        Me.cgJobs.HideFirstColumn = False
        Me.cgJobs.Location = New System.Drawing.Point(12, 84)
        Me.cgJobs.Name = "cgJobs"
        Me.cgJobs.PreviewColumn = ""
        Me.cgJobs.QueryID = Nothing
        Me.cgJobs.RowAutoHeight = False
        Me.cgJobs.SearchAsYouType = True
        Me.cgJobs.ShowAutoFilterRow = False
        Me.cgJobs.ShowFindPanel = True
        Me.cgJobs.ShowGroupByBox = True
        Me.cgJobs.ShowLoadingPanel = False
        Me.cgJobs.ShowNavigator = True
        Me.cgJobs.Size = New System.Drawing.Size(965, 437)
        Me.cgJobs.TabIndex = 1
        '
        'gbxCosts
        '
        Me.gbxCosts.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.gbxCosts.Controls.Add(Me.btnCostCancel)
        Me.gbxCosts.Controls.Add(Me.btnCostRun)
        Me.gbxCosts.Controls.Add(Me.CareLabel12)
        Me.gbxCosts.Controls.Add(Me.cdtDate)
        Me.gbxCosts.Controls.Add(Me.cbxMode)
        Me.gbxCosts.Controls.Add(Me.CareLabel14)
        Me.gbxCosts.Location = New System.Drawing.Point(357, 228)
        Me.gbxCosts.Name = "gbxCosts"
        Me.gbxCosts.ShowCaption = False
        Me.gbxCosts.Size = New System.Drawing.Size(270, 104)
        Me.gbxCosts.TabIndex = 2
        '
        'btnCostCancel
        '
        Me.btnCostCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCostCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCostCancel.Appearance.Options.UseFont = True
        Me.btnCostCancel.Location = New System.Drawing.Point(180, 73)
        Me.btnCostCancel.Name = "btnCostCancel"
        Me.btnCostCancel.Size = New System.Drawing.Size(80, 23)
        Me.btnCostCancel.TabIndex = 5
        Me.btnCostCancel.Text = "Cancel"
        '
        'btnCostRun
        '
        Me.btnCostRun.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCostRun.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCostRun.Appearance.Options.UseFont = True
        Me.btnCostRun.Location = New System.Drawing.Point(94, 73)
        Me.btnCostRun.Name = "btnCostRun"
        Me.btnCostRun.Size = New System.Drawing.Size(80, 23)
        Me.btnCostRun.TabIndex = 4
        Me.btnCostRun.Text = "Continue"
        '
        'CareLabel12
        '
        Me.CareLabel12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel12.Location = New System.Drawing.Point(10, 41)
        Me.CareLabel12.Name = "CareLabel12"
        Me.CareLabel12.Size = New System.Drawing.Size(24, 15)
        Me.CareLabel12.TabIndex = 2
        Me.CareLabel12.Text = "Date"
        Me.CareLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtDate
        '
        Me.cdtDate.EditValue = Nothing
        Me.cdtDate.EnterMoveNextControl = True
        Me.cdtDate.Location = New System.Drawing.Point(60, 38)
        Me.cdtDate.Name = "cdtDate"
        Me.cdtDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtDate.Properties.Appearance.Options.UseFont = True
        Me.cdtDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDate.Size = New System.Drawing.Size(92, 22)
        Me.cdtDate.TabIndex = 3
        Me.cdtDate.Tag = "AE"
        Me.cdtDate.Value = Nothing
        '
        'cbxMode
        '
        Me.cbxMode.AllowBlank = False
        Me.cbxMode.DataSource = Nothing
        Me.cbxMode.DisplayMember = Nothing
        Me.cbxMode.EnterMoveNextControl = True
        Me.cbxMode.Location = New System.Drawing.Point(60, 10)
        Me.cbxMode.Name = "cbxMode"
        Me.cbxMode.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxMode.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxMode.Properties.Appearance.Options.UseFont = True
        Me.cbxMode.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxMode.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxMode.SelectedValue = Nothing
        Me.cbxMode.Size = New System.Drawing.Size(200, 22)
        Me.cbxMode.TabIndex = 1
        Me.cbxMode.ValueMember = Nothing
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(10, 13)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(31, 15)
        Me.CareLabel14.TabIndex = 0
        Me.CareLabel14.Text = "Mode"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtGreen
        '
        Me.txtGreen.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtGreen.Location = New System.Drawing.Point(810, 9)
        Me.txtGreen.Name = "txtGreen"
        Me.txtGreen.Properties.Appearance.BackColor = System.Drawing.Color.LightGreen
        Me.txtGreen.Properties.Appearance.Options.UseBackColor = True
        Me.txtGreen.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtGreen, True)
        Me.txtGreen.Size = New System.Drawing.Size(50, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtGreen, OptionsSpelling1)
        Me.txtGreen.TabIndex = 19
        Me.txtGreen.TabStop = False
        Me.txtGreen.Visible = False
        '
        'txtRed
        '
        Me.txtRed.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtRed.Location = New System.Drawing.Point(922, 9)
        Me.txtRed.Name = "txtRed"
        Me.txtRed.Properties.Appearance.BackColor = System.Drawing.Color.LightCoral
        Me.txtRed.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRed.Properties.Appearance.Options.UseBackColor = True
        Me.txtRed.Properties.Appearance.Options.UseFont = True
        Me.txtRed.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtRed, True)
        Me.txtRed.Size = New System.Drawing.Size(50, 22)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtRed, OptionsSpelling2)
        Me.txtRed.TabIndex = 20
        Me.txtRed.TabStop = False
        Me.txtRed.Visible = False
        '
        'txtYellow
        '
        Me.txtYellow.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtYellow.Location = New System.Drawing.Point(866, 9)
        Me.txtYellow.Name = "txtYellow"
        Me.txtYellow.Properties.Appearance.BackColor = System.Drawing.Color.Khaki
        Me.txtYellow.Properties.Appearance.Options.UseBackColor = True
        Me.txtYellow.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtYellow, True)
        Me.txtYellow.Size = New System.Drawing.Size(50, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtYellow, OptionsSpelling3)
        Me.txtYellow.TabIndex = 21
        Me.txtYellow.TabStop = False
        Me.txtYellow.Visible = False
        '
        'frmEnqJobs
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(984, 561)
        Me.Controls.Add(Me.gbxCosts)
        Me.Controls.Add(Me.cgJobs)
        Me.Controls.Add(Me.gbxDetails)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.LoadMaximised = True
        Me.Margin = New System.Windows.Forms.Padding(3, 5, 3, 5)
        Me.Name = "frmEnqJobs"
        Me.Text = ""
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.gbxDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxDetails.ResumeLayout(False)
        Me.gbxDetails.PerformLayout()
        CType(Me.cbxController.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxArea.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxHold.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxInsurer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSector.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxCosts, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxCosts.ResumeLayout(False)
        Me.gbxCosts.PerformLayout()
        CType(Me.cdtDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxMode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Protected WithEvents Panel1 As DevExpress.XtraEditors.PanelControl
    Protected WithEvents btnViewJob As Care.Controls.CareButton
    Friend WithEvents gbxDetails As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cbxSector As Care.Controls.CareComboBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Protected WithEvents btnShowCosts As Care.Controls.CareButton
    Protected WithEvents btnJobListing As Care.Controls.CareButton
    Protected WithEvents btnJobDesc As Care.Controls.CareButton
    Protected WithEvents btnJobComplete As Care.Controls.CareButton
    Friend WithEvents cgJobs As Care.Controls.CareGrid
    Friend WithEvents cbxInsurer As Care.Controls.CareComboBox
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents cbxController As Care.Controls.CareComboBox
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents cbxArea As Care.Controls.CareComboBox
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents cbxStatus As Care.Controls.CareCheckedComboBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents cbxHold As Care.Controls.CareComboBox
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Protected WithEvents btnRefresh As Care.Controls.CareButton
    Protected WithEvents btnClear As Care.Controls.CareButton
    Friend WithEvents gbxCosts As DevExpress.XtraEditors.GroupControl
    Protected WithEvents btnCostCancel As Care.Controls.CareButton
    Protected WithEvents btnCostRun As Care.Controls.CareButton
    Friend WithEvents CareLabel12 As Care.Controls.CareLabel
    Friend WithEvents cdtDate As Care.Controls.CareDateTime
    Friend WithEvents cbxMode As Care.Controls.CareComboBox
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents txtGreen As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRed As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtYellow As DevExpress.XtraEditors.TextEdit
End Class
