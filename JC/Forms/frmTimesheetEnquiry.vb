﻿Option Strict On

Imports Care.Global

Public Class frmTimesheetEnquiry

    Private Sub frmTimesheetEnquiry_Load(sender As Object, e As EventArgs) Handles Me.Load
        cbxEmployee.AllowBlank = True
        cbxEmployee.PopulateWithSQL(Session.ConnectionString, "select ID, fullname from Employees order by fullname")
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        RefreshGrid()
    End Sub

    Private Sub RefreshGrid()

        Dim _SQL As String = ""

        _SQL += ""

        _SQL += "select t.ts_date as 'Date', p.aj_no As 'Van', p.name as 'Van Name', e.fullname as 'Employee',"
        _SQL += " j.job_no as 'Job No', j.rsk_fullname as 'Risk Name', j.rsk_address_1 as 'Risk Address', t.ts_what as 'Work',"
        _SQL += " t.ts_day_start as 'Started', t.ts_job_finish as 'Finished', t.ts_day_finish as 'Yard', t.ts_day_duration as 'Duration', t.ts_cost as 'Cost'"
        _SQL += " from Timesheets t"
        _SQL += " left Join Plant p on p.ID = t.ts_van_id"
        _SQL += " left Join Jobs j on j.ID = t.ts_job_id"
        _SQL += " left Join Employees e on e.ID = t.ts_emp_id"
        _SQL += " where 1=1"

        If cdtTimesheetDate.Text <> "" Then
            _SQL += " and t.ts_date >= " + ValueHandler.SQLDate(cdtTimesheetDate.Value.Value, True)
        End If

        If txtVan.Text <> "" Then
            _SQL += " and p.aj_no = '" + txtVan.Text + "'"
        End If

        If txtJobNo.Text <> "" Then
            _SQL += " and j.job_no = '" + txtJobNo.Text + "'"
        End If

        If cbxEmployee.Text <> "" Then
            _SQL += " and t.ts_emp_id = '" + cbxEmployee.SelectedValue.ToString + "'"
        End If

        _SQL += " order by t.ts_date"

        cgTS.Populate(session.connectionstring, _SQL)

    End Sub

    Private Sub btnFind_Click(sender As Object, e As EventArgs) Handles btnFind.Click
        Dim _Return As String = Business.Job.FindAll
        If _Return <> "" Then
            Dim _j As Business.Job = Business.Job.RetreiveByID(New Guid(_Return))
            If _j IsNot Nothing Then
                txtJobNo.Text = _j._JobNo.ToString
                txtJobDesc.Text = _j._RskFullname + ", " + _j._RskAddress1
            End If
        End If
    End Sub

    Private Sub txtJobNo_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtJobNo.Validating
        If txtJobNo.Text = "" Then
            txtJobNo.Text = ""
            txtJobDesc.Text = ""
        Else
            Dim _j As Business.Job = Business.Job.RetreiveByJobNo(txtJobNo.Text)
            If _j IsNot Nothing Then
                txtJobNo.Text = _j._JobNo.ToString
                txtJobDesc.Text = _j._RskFullname + ", " + _j._RskAddress1
            End If
        End If
    End Sub
End Class
