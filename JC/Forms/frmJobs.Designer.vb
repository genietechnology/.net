﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmJobs
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmJobs))
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.ctMain = New Care.Controls.CareTab(Me.components)
        Me.tabMain = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl10 = New DevExpress.XtraEditors.GroupControl()
        Me.btnCopyRisk = New Care.Controls.CareButton(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.cbxCustTitle = New Care.Controls.CareComboBox(Me.components)
        Me.txtCustForename = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel24 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel25 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel26 = New Care.Controls.CareLabel(Me.components)
        Me.txtCustAddress = New Care.Address.CareAddress()
        Me.txtCustSurname = New Care.Controls.CareTextBox(Me.components)
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.CareLabel31 = New Care.Controls.CareLabel(Me.components)
        Me.txtRiskSurname = New Care.Controls.CareTextBox(Me.components)
        Me.cbxRiskTitle = New Care.Controls.CareComboBox(Me.components)
        Me.txtRiskForename = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel17 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel16 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel15 = New Care.Controls.CareLabel(Me.components)
        Me.txtRiskAddress = New Care.Address.CareAddress()
        Me.GroupControl12 = New DevExpress.XtraEditors.GroupControl()
        Me.icoRetentions = New Care.[Shared].CareIcon()
        Me.btnRetentions = New Care.Controls.CareButton(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.CareCheckBox1 = New Care.Controls.CareCheckBox(Me.components)
        Me.GroupControl9 = New DevExpress.XtraEditors.GroupControl()
        Me.icoValuations = New Care.[Shared].CareIcon()
        Me.btnValuations = New Care.Controls.CareButton(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.chkTracker = New Care.Controls.CareCheckBox(Me.components)
        Me.GroupControl8 = New DevExpress.XtraEditors.GroupControl()
        Me.btnContactBilling = New Care.Controls.CareButton(Me.components)
        Me.btnContactPrimary = New Care.Controls.CareButton(Me.components)
        Me.cgContacts = New Care.Controls.CareGridWithButtons()
        Me.GroupControl7 = New DevExpress.XtraEditors.GroupControl()
        Me.btnChangeStatus = New Care.Controls.CareButton(Me.components)
        Me.cbxController = New Care.Controls.CareComboBox(Me.components)
        Me.cbxArea = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel10 = New Care.Controls.CareLabel(Me.components)
        Me.txtStatus = New Care.Controls.CareTextBox(Me.components)
        Me.GroupControl6 = New DevExpress.XtraEditors.GroupControl()
        Me.lblCompCert = New Care.Controls.CareLabel(Me.components)
        Me.lblDryCert = New Care.Controls.CareLabel(Me.components)
        Me.lblWorksOrder = New Care.Controls.CareLabel(Me.components)
        Me.txtWORcd = New Care.Controls.CareTextBox(Me.components)
        Me.txtCompCert = New Care.Controls.CareTextBox(Me.components)
        Me.txtDryCert = New Care.Controls.CareTextBox(Me.components)
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.cbxLossAdj = New Care.Controls.CareComboBox(Me.components)
        Me.cbxInsurer = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel13 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel12 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.txtInsClaim = New Care.Controls.CareTextBox(Me.components)
        Me.txtInsJob = New Care.Controls.CareTextBox(Me.components)
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.CareLabel32 = New Care.Controls.CareLabel(Me.components)
        Me.txtYardCount = New Care.Controls.CareTextBox(Me.components)
        Me.txtYardSub = New Care.Controls.CareTextBox(Me.components)
        Me.txtYardTotal = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel30 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel29 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel28 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel27 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel23 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel22 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel21 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel20 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel19 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel18 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.txtLabCount = New Care.Controls.CareTextBox(Me.components)
        Me.txtVersion = New Care.Controls.CareTextBox(Me.components)
        Me.txtInvoiced = New Care.Controls.CareTextBox(Me.components)
        Me.txtConCount = New Care.Controls.CareTextBox(Me.components)
        Me.txtMatCount = New Care.Controls.CareTextBox(Me.components)
        Me.txtLabSub = New Care.Controls.CareTextBox(Me.components)
        Me.txtSubTotal = New Care.Controls.CareTextBox(Me.components)
        Me.txtConSub = New Care.Controls.CareTextBox(Me.components)
        Me.txtMatSub = New Care.Controls.CareTextBox(Me.components)
        Me.txtLabTotal = New Care.Controls.CareTextBox(Me.components)
        Me.txtGrandTotal = New Care.Controls.CareTextBox(Me.components)
        Me.txtOverhead = New Care.Controls.CareTextBox(Me.components)
        Me.txtTotalTotal = New Care.Controls.CareTextBox(Me.components)
        Me.txtConTotal = New Care.Controls.CareTextBox(Me.components)
        Me.txtMatTotal = New Care.Controls.CareTextBox(Me.components)
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.lblOnHold = New Care.Controls.CareLabel(Me.components)
        Me.cdtEnd = New Care.Controls.CareDateTime(Me.components)
        Me.cdtStart = New Care.Controls.CareDateTime(Me.components)
        Me.cbxSector = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.txtNo = New Care.Controls.CareTextBox(Me.components)
        Me.tabLinks = New DevExpress.XtraTab.XtraTabPage()
        Me.cgLinked = New Care.Controls.CareGridWithButtons()
        Me.tabDescriptions = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.radDescTools = New Care.Controls.CareRadioButton(Me.components)
        Me.radDescEst = New Care.Controls.CareRadioButton(Me.components)
        Me.radDescDirections = New Care.Controls.CareRadioButton(Me.components)
        Me.radDescJob = New Care.Controls.CareRadioButton(Me.components)
        Me.rich = New Care.Controls.CareRichText()
        Me.tabCalendar = New DevExpress.XtraTab.XtraTabPage()
        Me.yvCalendar = New JC.YearView()
        Me.tabVersions = New DevExpress.XtraTab.XtraTabPage()
        Me.cgVersions = New Care.Controls.CareGridWithButtons()
        Me.tabMaterials = New DevExpress.XtraTab.XtraTabPage()
        Me.btnMatDelete = New Care.Controls.CareButton(Me.components)
        Me.btnMatMove = New Care.Controls.CareButton(Me.components)
        Me.GroupControl11 = New DevExpress.XtraEditors.GroupControl()
        Me.radInvSummaryInvNotes = New Care.Controls.CareRadioButton(Me.components)
        Me.radInvInvNotes = New Care.Controls.CareRadioButton(Me.components)
        Me.radInvContCred = New Care.Controls.CareRadioButton(Me.components)
        Me.radInvCont = New Care.Controls.CareRadioButton(Me.components)
        Me.radInvSupCred = New Care.Controls.CareRadioButton(Me.components)
        Me.radInvSup = New Care.Controls.CareRadioButton(Me.components)
        Me.radInvAll = New Care.Controls.CareRadioButton(Me.components)
        Me.cgMaterials = New Care.Controls.CareGrid()
        Me.tabPlant = New DevExpress.XtraTab.XtraTabPage()
        Me.cgPlant = New Care.Controls.CareGrid()
        Me.tabHire = New DevExpress.XtraTab.XtraTabPage()
        Me.cgHire = New Care.Controls.CareGrid()
        Me.tabLabour = New DevExpress.XtraTab.XtraTabPage()
        Me.cgLabour = New Care.Controls.CareGrid()
        Me.tabInvoices = New DevExpress.XtraTab.XtraTabPage()
        Me.cgSalesInvoices = New Care.Controls.CareGridWithButtons()
        Me.tabNotes = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl13 = New DevExpress.XtraEditors.GroupControl()
        Me.txtNotes = New DevExpress.XtraEditors.MemoEdit()
        Me.tabActivity = New DevExpress.XtraTab.XtraTabPage()
        Me.cgActivity = New Care.Controls.CareGrid()
        Me.tabPhotos = New DevExpress.XtraTab.XtraTabPage()
        Me.pic = New System.Windows.Forms.PictureBox()
        Me.cgFolders = New Care.Controls.CareGrid()
        Me.cgFiles = New Care.Controls.CareGrid()
        Me.btnWorkFile = New Care.Controls.CareButton(Me.components)
        Me.btnPrint = New Care.Controls.CareButton(Me.components)
        Me.btnKeyDates = New Care.Controls.CareButton(Me.components)
        Me.btnFunctions = New Care.Controls.CareButton(Me.components)
        Me.BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.ctMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ctMain.SuspendLayout()
        Me.tabMain.SuspendLayout()
        CType(Me.GroupControl10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl10.SuspendLayout()
        CType(Me.cbxCustTitle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCustForename.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCustSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.txtRiskSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxRiskTitle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRiskForename.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl12.SuspendLayout()
        CType(Me.CareCheckBox1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl9.SuspendLayout()
        CType(Me.chkTracker.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl8.SuspendLayout()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl7.SuspendLayout()
        CType(Me.cbxController.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxArea.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl6.SuspendLayout()
        CType(Me.txtWORcd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCompCert.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDryCert.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.cbxLossAdj.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxInsurer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtInsClaim.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtInsJob.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.txtYardCount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtYardSub.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtYardTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLabCount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtVersion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtInvoiced.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtConCount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMatCount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLabSub.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSubTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtConSub.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMatSub.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLabTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGrandTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOverhead.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtConTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMatTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.cdtEnd.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSector.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabLinks.SuspendLayout()
        Me.tabDescriptions.SuspendLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.radDescTools.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radDescEst.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radDescDirections.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radDescJob.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabCalendar.SuspendLayout()
        Me.tabVersions.SuspendLayout()
        Me.tabMaterials.SuspendLayout()
        CType(Me.GroupControl11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl11.SuspendLayout()
        CType(Me.radInvSummaryInvNotes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radInvInvNotes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radInvContCred.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radInvCont.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radInvSupCred.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radInvSup.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radInvAll.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabPlant.SuspendLayout()
        Me.tabHire.SuspendLayout()
        Me.tabLabour.SuspendLayout()
        Me.tabInvoices.SuspendLayout()
        Me.tabNotes.SuspendLayout()
        CType(Me.GroupControl13, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl13.SuspendLayout()
        CType(Me.txtNotes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabActivity.SuspendLayout()
        Me.tabPhotos.SuspendLayout()
        CType(Me.pic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(1017, 3)
        Me.btnCancel.TabIndex = 5
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(926, 3)
        Me.btnOK.TabIndex = 4
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnFunctions)
        Me.Panel1.Controls.Add(Me.btnKeyDates)
        Me.Panel1.Controls.Add(Me.btnPrint)
        Me.Panel1.Controls.Add(Me.btnWorkFile)
        Me.Panel1.Location = New System.Drawing.Point(0, 575)
        Me.Panel1.Size = New System.Drawing.Size(1114, 36)
        Me.Panel1.TabIndex = 1
        Me.Panel1.Controls.SetChildIndex(Me.btnOK, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnCancel, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnWorkFile, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnPrint, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnKeyDates, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnFunctions, 0)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'ctMain
        '
        Me.ctMain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ctMain.Location = New System.Drawing.Point(12, 53)
        Me.ctMain.Name = "ctMain"
        Me.ctMain.SelectedTabPage = Me.tabMain
        Me.ctMain.Size = New System.Drawing.Size(1093, 520)
        Me.ctMain.TabIndex = 0
        Me.ctMain.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabMain, Me.tabLinks, Me.tabDescriptions, Me.tabCalendar, Me.tabVersions, Me.tabMaterials, Me.tabPlant, Me.tabHire, Me.tabLabour, Me.tabInvoices, Me.tabNotes, Me.tabActivity, Me.tabPhotos})
        '
        'tabMain
        '
        Me.tabMain.Controls.Add(Me.GroupControl10)
        Me.tabMain.Controls.Add(Me.GroupControl3)
        Me.tabMain.Controls.Add(Me.GroupControl12)
        Me.tabMain.Controls.Add(Me.GroupControl9)
        Me.tabMain.Controls.Add(Me.GroupControl8)
        Me.tabMain.Controls.Add(Me.GroupControl7)
        Me.tabMain.Controls.Add(Me.GroupControl6)
        Me.tabMain.Controls.Add(Me.GroupControl4)
        Me.tabMain.Controls.Add(Me.GroupControl2)
        Me.tabMain.Controls.Add(Me.GroupControl1)
        Me.tabMain.Name = "tabMain"
        Me.tabMain.Size = New System.Drawing.Size(1087, 492)
        Me.tabMain.Text = "Job"
        '
        'GroupControl10
        '
        Me.GroupControl10.Controls.Add(Me.btnCopyRisk)
        Me.GroupControl10.Controls.Add(Me.CareLabel7)
        Me.GroupControl10.Controls.Add(Me.cbxCustTitle)
        Me.GroupControl10.Controls.Add(Me.txtCustForename)
        Me.GroupControl10.Controls.Add(Me.CareLabel24)
        Me.GroupControl10.Controls.Add(Me.CareLabel25)
        Me.GroupControl10.Controls.Add(Me.CareLabel26)
        Me.GroupControl10.Controls.Add(Me.txtCustAddress)
        Me.GroupControl10.Controls.Add(Me.txtCustSurname)
        Me.GroupControl10.Location = New System.Drawing.Point(328, 106)
        Me.GroupControl10.Name = "GroupControl10"
        Me.GroupControl10.Size = New System.Drawing.Size(315, 207)
        Me.GroupControl10.TabIndex = 6
        Me.GroupControl10.Text = "Customer Address"
        '
        'btnCopyRisk
        '
        Me.btnCopyRisk.CausesValidation = False
        Me.btnCopyRisk.Location = New System.Drawing.Point(7, 174)
        Me.btnCopyRisk.Name = "btnCopyRisk"
        Me.btnCopyRisk.Size = New System.Drawing.Size(92, 25)
        Me.btnCopyRisk.TabIndex = 8
        Me.btnCopyRisk.Text = "Copy Risk"
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel7.Appearance.Options.UseFont = True
        Me.CareLabel7.Appearance.Options.UseTextOptions = True
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(7, 59)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(53, 15)
        Me.CareLabel7.TabIndex = 2
        Me.CareLabel7.Text = "Forename"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxCustTitle
        '
        Me.cbxCustTitle.AllowBlank = False
        Me.cbxCustTitle.DataSource = Nothing
        Me.cbxCustTitle.DisplayMember = Nothing
        Me.cbxCustTitle.EnterMoveNextControl = True
        Me.cbxCustTitle.Location = New System.Drawing.Point(105, 28)
        Me.cbxCustTitle.Name = "cbxCustTitle"
        Me.cbxCustTitle.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxCustTitle.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxCustTitle.Properties.Appearance.Options.UseFont = True
        Me.cbxCustTitle.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxCustTitle.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxCustTitle.SelectedValue = Nothing
        Me.cbxCustTitle.Size = New System.Drawing.Size(85, 22)
        Me.cbxCustTitle.TabIndex = 1
        Me.cbxCustTitle.Tag = "AE"
        Me.cbxCustTitle.ValueMember = Nothing
        '
        'txtCustForename
        '
        Me.txtCustForename.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtCustForename.EnterMoveNextControl = True
        Me.txtCustForename.Location = New System.Drawing.Point(105, 56)
        Me.txtCustForename.MaxLength = 0
        Me.txtCustForename.Name = "txtCustForename"
        Me.txtCustForename.NumericAllowNegatives = False
        Me.txtCustForename.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtCustForename.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCustForename.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtCustForename.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtCustForename.Properties.Appearance.Options.UseFont = True
        Me.txtCustForename.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCustForename.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtCustForename.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtCustForename.Size = New System.Drawing.Size(201, 22)
        Me.txtCustForename.TabIndex = 3
        Me.txtCustForename.Tag = "AE"
        Me.txtCustForename.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCustForename.ToolTipText = ""
        '
        'CareLabel24
        '
        Me.CareLabel24.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel24.Appearance.Options.UseFont = True
        Me.CareLabel24.Appearance.Options.UseTextOptions = True
        Me.CareLabel24.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel24.Location = New System.Drawing.Point(7, 118)
        Me.CareLabel24.Name = "CareLabel24"
        Me.CareLabel24.Size = New System.Drawing.Size(42, 15)
        Me.CareLabel24.TabIndex = 6
        Me.CareLabel24.Text = "Address"
        Me.CareLabel24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel25
        '
        Me.CareLabel25.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel25.Appearance.Options.UseFont = True
        Me.CareLabel25.Appearance.Options.UseTextOptions = True
        Me.CareLabel25.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel25.Location = New System.Drawing.Point(7, 87)
        Me.CareLabel25.Name = "CareLabel25"
        Me.CareLabel25.Size = New System.Drawing.Size(47, 15)
        Me.CareLabel25.TabIndex = 4
        Me.CareLabel25.Text = "Surname"
        Me.CareLabel25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel26
        '
        Me.CareLabel26.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel26.Appearance.Options.UseFont = True
        Me.CareLabel26.Appearance.Options.UseTextOptions = True
        Me.CareLabel26.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel26.Location = New System.Drawing.Point(7, 30)
        Me.CareLabel26.Name = "CareLabel26"
        Me.CareLabel26.Size = New System.Drawing.Size(23, 15)
        Me.CareLabel26.TabIndex = 0
        Me.CareLabel26.Text = "Title"
        Me.CareLabel26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCustAddress
        '
        Me.txtCustAddress.AccessibleName = "Customer Address"
        Me.txtCustAddress.Address = ""
        Me.txtCustAddress.AddressBlock = ""
        Me.txtCustAddress.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCustAddress.Appearance.Options.UseFont = True
        Me.txtCustAddress.County = ""
        Me.txtCustAddress.DisplayMode = Care.Address.CareAddress.EnumDisplayMode.SingleControl
        Me.txtCustAddress.Location = New System.Drawing.Point(105, 112)
        Me.txtCustAddress.MaxLength = 500
        Me.txtCustAddress.Name = "txtCustAddress"
        Me.txtCustAddress.PostCode = ""
        Me.txtCustAddress.ReadOnly = False
        Me.txtCustAddress.Size = New System.Drawing.Size(201, 87)
        Me.txtCustAddress.TabIndex = 7
        Me.txtCustAddress.Tag = "AEM"
        Me.txtCustAddress.Town = ""
        '
        'txtCustSurname
        '
        Me.txtCustSurname.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtCustSurname.EnterMoveNextControl = True
        Me.txtCustSurname.Location = New System.Drawing.Point(105, 84)
        Me.txtCustSurname.MaxLength = 0
        Me.txtCustSurname.Name = "txtCustSurname"
        Me.txtCustSurname.NumericAllowNegatives = False
        Me.txtCustSurname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtCustSurname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCustSurname.Properties.AccessibleName = "Customer Surname"
        Me.txtCustSurname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtCustSurname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtCustSurname.Properties.Appearance.Options.UseFont = True
        Me.txtCustSurname.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCustSurname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtCustSurname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtCustSurname.Size = New System.Drawing.Size(201, 22)
        Me.txtCustSurname.TabIndex = 5
        Me.txtCustSurname.Tag = "AEM"
        Me.txtCustSurname.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCustSurname.ToolTipText = ""
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.CareLabel31)
        Me.GroupControl3.Controls.Add(Me.txtRiskSurname)
        Me.GroupControl3.Controls.Add(Me.cbxRiskTitle)
        Me.GroupControl3.Controls.Add(Me.txtRiskForename)
        Me.GroupControl3.Controls.Add(Me.CareLabel17)
        Me.GroupControl3.Controls.Add(Me.CareLabel16)
        Me.GroupControl3.Controls.Add(Me.CareLabel15)
        Me.GroupControl3.Controls.Add(Me.txtRiskAddress)
        Me.GroupControl3.Location = New System.Drawing.Point(7, 106)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(315, 207)
        Me.GroupControl3.TabIndex = 5
        Me.GroupControl3.Text = "Risk Address"
        '
        'CareLabel31
        '
        Me.CareLabel31.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel31.Appearance.Options.UseFont = True
        Me.CareLabel31.Appearance.Options.UseTextOptions = True
        Me.CareLabel31.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel31.Location = New System.Drawing.Point(9, 58)
        Me.CareLabel31.Name = "CareLabel31"
        Me.CareLabel31.Size = New System.Drawing.Size(53, 15)
        Me.CareLabel31.TabIndex = 2
        Me.CareLabel31.Text = "Forename"
        Me.CareLabel31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRiskSurname
        '
        Me.txtRiskSurname.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtRiskSurname.EnterMoveNextControl = True
        Me.txtRiskSurname.Location = New System.Drawing.Point(105, 84)
        Me.txtRiskSurname.MaxLength = 0
        Me.txtRiskSurname.Name = "txtRiskSurname"
        Me.txtRiskSurname.NumericAllowNegatives = False
        Me.txtRiskSurname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRiskSurname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRiskSurname.Properties.AccessibleName = "Risk Surname"
        Me.txtRiskSurname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRiskSurname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRiskSurname.Properties.Appearance.Options.UseFont = True
        Me.txtRiskSurname.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRiskSurname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRiskSurname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRiskSurname.Size = New System.Drawing.Size(199, 22)
        Me.txtRiskSurname.TabIndex = 5
        Me.txtRiskSurname.Tag = "AEM"
        Me.txtRiskSurname.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRiskSurname.ToolTipText = ""
        '
        'cbxRiskTitle
        '
        Me.cbxRiskTitle.AllowBlank = False
        Me.cbxRiskTitle.DataSource = Nothing
        Me.cbxRiskTitle.DisplayMember = Nothing
        Me.cbxRiskTitle.EnterMoveNextControl = True
        Me.cbxRiskTitle.Location = New System.Drawing.Point(105, 28)
        Me.cbxRiskTitle.Name = "cbxRiskTitle"
        Me.cbxRiskTitle.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxRiskTitle.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxRiskTitle.Properties.Appearance.Options.UseFont = True
        Me.cbxRiskTitle.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxRiskTitle.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxRiskTitle.SelectedValue = Nothing
        Me.cbxRiskTitle.Size = New System.Drawing.Size(85, 22)
        Me.cbxRiskTitle.TabIndex = 1
        Me.cbxRiskTitle.Tag = "AE"
        Me.cbxRiskTitle.ValueMember = Nothing
        '
        'txtRiskForename
        '
        Me.txtRiskForename.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtRiskForename.EnterMoveNextControl = True
        Me.txtRiskForename.Location = New System.Drawing.Point(105, 56)
        Me.txtRiskForename.MaxLength = 0
        Me.txtRiskForename.Name = "txtRiskForename"
        Me.txtRiskForename.NumericAllowNegatives = False
        Me.txtRiskForename.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRiskForename.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRiskForename.Properties.AccessibleName = ""
        Me.txtRiskForename.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRiskForename.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRiskForename.Properties.Appearance.Options.UseFont = True
        Me.txtRiskForename.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRiskForename.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRiskForename.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRiskForename.Size = New System.Drawing.Size(199, 22)
        Me.txtRiskForename.TabIndex = 3
        Me.txtRiskForename.Tag = "AE"
        Me.txtRiskForename.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRiskForename.ToolTipText = ""
        '
        'CareLabel17
        '
        Me.CareLabel17.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel17.Appearance.Options.UseFont = True
        Me.CareLabel17.Appearance.Options.UseTextOptions = True
        Me.CareLabel17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel17.Location = New System.Drawing.Point(9, 118)
        Me.CareLabel17.Name = "CareLabel17"
        Me.CareLabel17.Size = New System.Drawing.Size(42, 15)
        Me.CareLabel17.TabIndex = 6
        Me.CareLabel17.Text = "Address"
        Me.CareLabel17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel16
        '
        Me.CareLabel16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel16.Appearance.Options.UseFont = True
        Me.CareLabel16.Appearance.Options.UseTextOptions = True
        Me.CareLabel16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel16.Location = New System.Drawing.Point(9, 87)
        Me.CareLabel16.Name = "CareLabel16"
        Me.CareLabel16.Size = New System.Drawing.Size(47, 15)
        Me.CareLabel16.TabIndex = 4
        Me.CareLabel16.Text = "Surname"
        Me.CareLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel15
        '
        Me.CareLabel15.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel15.Appearance.Options.UseFont = True
        Me.CareLabel15.Appearance.Options.UseTextOptions = True
        Me.CareLabel15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel15.Location = New System.Drawing.Point(9, 31)
        Me.CareLabel15.Name = "CareLabel15"
        Me.CareLabel15.Size = New System.Drawing.Size(23, 15)
        Me.CareLabel15.TabIndex = 0
        Me.CareLabel15.Text = "Title"
        Me.CareLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRiskAddress
        '
        Me.txtRiskAddress.AccessibleName = "Risk Address"
        Me.txtRiskAddress.Address = ""
        Me.txtRiskAddress.AddressBlock = ""
        Me.txtRiskAddress.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRiskAddress.Appearance.Options.UseFont = True
        Me.txtRiskAddress.County = ""
        Me.txtRiskAddress.DisplayMode = Care.Address.CareAddress.EnumDisplayMode.SingleControl
        Me.txtRiskAddress.Location = New System.Drawing.Point(105, 112)
        Me.txtRiskAddress.MaxLength = 500
        Me.txtRiskAddress.Name = "txtRiskAddress"
        Me.txtRiskAddress.PostCode = ""
        Me.txtRiskAddress.ReadOnly = False
        Me.txtRiskAddress.Size = New System.Drawing.Size(199, 87)
        Me.txtRiskAddress.TabIndex = 7
        Me.txtRiskAddress.Tag = "AEM"
        Me.txtRiskAddress.Town = ""
        '
        'GroupControl12
        '
        Me.GroupControl12.Controls.Add(Me.icoRetentions)
        Me.GroupControl12.Controls.Add(Me.btnRetentions)
        Me.GroupControl12.Controls.Add(Me.CareLabel6)
        Me.GroupControl12.Controls.Add(Me.CareCheckBox1)
        Me.GroupControl12.Location = New System.Drawing.Point(892, 60)
        Me.GroupControl12.Name = "GroupControl12"
        Me.GroupControl12.ShowCaption = False
        Me.GroupControl12.Size = New System.Drawing.Size(187, 40)
        Me.GroupControl12.TabIndex = 4
        Me.GroupControl12.Text = "GroupControl12"
        '
        'icoRetentions
        '
        Me.icoRetentions.Icon = Care.[Shared].CareIcon.EnumIcon.Cross
        Me.icoRetentions.Location = New System.Drawing.Point(97, 3)
        Me.icoRetentions.MaximumSize = New System.Drawing.Size(32, 32)
        Me.icoRetentions.MinimumSize = New System.Drawing.Size(32, 32)
        Me.icoRetentions.Name = "icoRetentions"
        Me.icoRetentions.Size = New System.Drawing.Size(32, 32)
        Me.icoRetentions.TabIndex = 2
        Me.icoRetentions.TabStop = False
        '
        'btnRetentions
        '
        Me.btnRetentions.Location = New System.Drawing.Point(135, 8)
        Me.btnRetentions.Name = "btnRetentions"
        Me.btnRetentions.Size = New System.Drawing.Size(46, 25)
        Me.btnRetentions.TabIndex = 3
        Me.btnRetentions.Text = "View"
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.Options.UseFont = True
        Me.CareLabel6.Appearance.Options.UseTextOptions = True
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(9, 13)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(56, 15)
        Me.CareLabel6.TabIndex = 0
        Me.CareLabel6.Text = "Retentions"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareCheckBox1
        '
        Me.CareCheckBox1.EnterMoveNextControl = True
        Me.CareCheckBox1.Location = New System.Drawing.Point(72, 9)
        Me.CareCheckBox1.Name = "CareCheckBox1"
        Me.CareCheckBox1.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareCheckBox1.Properties.Appearance.Options.UseFont = True
        Me.CareCheckBox1.Properties.AutoWidth = True
        Me.CareCheckBox1.Properties.Caption = ""
        Me.CareCheckBox1.Size = New System.Drawing.Size(19, 19)
        Me.CareCheckBox1.TabIndex = 1
        Me.CareCheckBox1.Tag = "AE"
        '
        'GroupControl9
        '
        Me.GroupControl9.Controls.Add(Me.icoValuations)
        Me.GroupControl9.Controls.Add(Me.btnValuations)
        Me.GroupControl9.Controls.Add(Me.CareLabel5)
        Me.GroupControl9.Controls.Add(Me.chkTracker)
        Me.GroupControl9.Location = New System.Drawing.Point(892, 5)
        Me.GroupControl9.Name = "GroupControl9"
        Me.GroupControl9.ShowCaption = False
        Me.GroupControl9.Size = New System.Drawing.Size(187, 40)
        Me.GroupControl9.TabIndex = 3
        Me.GroupControl9.Text = "GroupControl9"
        '
        'icoValuations
        '
        Me.icoValuations.Icon = Care.[Shared].CareIcon.EnumIcon.Cross
        Me.icoValuations.Location = New System.Drawing.Point(98, 4)
        Me.icoValuations.MaximumSize = New System.Drawing.Size(32, 32)
        Me.icoValuations.MinimumSize = New System.Drawing.Size(32, 32)
        Me.icoValuations.Name = "icoValuations"
        Me.icoValuations.Size = New System.Drawing.Size(32, 32)
        Me.icoValuations.TabIndex = 2
        Me.icoValuations.TabStop = False
        '
        'btnValuations
        '
        Me.btnValuations.Location = New System.Drawing.Point(136, 8)
        Me.btnValuations.Name = "btnValuations"
        Me.btnValuations.Size = New System.Drawing.Size(45, 25)
        Me.btnValuations.TabIndex = 3
        Me.btnValuations.Text = "View"
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.Options.UseFont = True
        Me.CareLabel5.Appearance.Options.UseTextOptions = True
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(9, 13)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(55, 15)
        Me.CareLabel5.TabIndex = 0
        Me.CareLabel5.Text = "Valuations"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkTracker
        '
        Me.chkTracker.EnterMoveNextControl = True
        Me.chkTracker.Location = New System.Drawing.Point(70, 11)
        Me.chkTracker.Name = "chkTracker"
        Me.chkTracker.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkTracker.Properties.Appearance.Options.UseFont = True
        Me.chkTracker.Properties.AutoWidth = True
        Me.chkTracker.Properties.Caption = ""
        Me.chkTracker.Size = New System.Drawing.Size(19, 19)
        Me.chkTracker.TabIndex = 1
        Me.chkTracker.Tag = "AE"
        '
        'GroupControl8
        '
        Me.GroupControl8.Controls.Add(Me.btnContactBilling)
        Me.GroupControl8.Controls.Add(Me.btnContactPrimary)
        Me.GroupControl8.Controls.Add(Me.cgContacts)
        Me.GroupControl8.Location = New System.Drawing.Point(7, 319)
        Me.GroupControl8.Name = "GroupControl8"
        Me.GroupControl8.Size = New System.Drawing.Size(636, 167)
        Me.GroupControl8.TabIndex = 8
        Me.GroupControl8.Text = "Contacts"
        '
        'btnContactBilling
        '
        Me.btnContactBilling.CausesValidation = False
        Me.btnContactBilling.Location = New System.Drawing.Point(403, 139)
        Me.btnContactBilling.Name = "btnContactBilling"
        Me.btnContactBilling.Size = New System.Drawing.Size(120, 23)
        Me.btnContactBilling.TabIndex = 10
        Me.btnContactBilling.Text = "Make Billing Contact"
        '
        'btnContactPrimary
        '
        Me.btnContactPrimary.CausesValidation = False
        Me.btnContactPrimary.Location = New System.Drawing.Point(277, 139)
        Me.btnContactPrimary.Name = "btnContactPrimary"
        Me.btnContactPrimary.Size = New System.Drawing.Size(120, 23)
        Me.btnContactPrimary.TabIndex = 9
        Me.btnContactPrimary.Text = "Make Primary Contact"
        '
        'cgContacts
        '
        Me.cgContacts.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgContacts.ButtonsEnabled = False
        Me.cgContacts.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgContacts.HideFirstColumn = False
        Me.cgContacts.Location = New System.Drawing.Point(5, 23)
        Me.cgContacts.Name = "cgContacts"
        Me.cgContacts.PreviewColumn = ""
        Me.cgContacts.Size = New System.Drawing.Size(626, 139)
        Me.cgContacts.TabIndex = 0
        '
        'GroupControl7
        '
        Me.GroupControl7.Controls.Add(Me.btnChangeStatus)
        Me.GroupControl7.Controls.Add(Me.cbxController)
        Me.GroupControl7.Controls.Add(Me.cbxArea)
        Me.GroupControl7.Controls.Add(Me.CareLabel8)
        Me.GroupControl7.Controls.Add(Me.CareLabel9)
        Me.GroupControl7.Controls.Add(Me.CareLabel10)
        Me.GroupControl7.Controls.Add(Me.txtStatus)
        Me.GroupControl7.Location = New System.Drawing.Point(328, 5)
        Me.GroupControl7.Name = "GroupControl7"
        Me.GroupControl7.ShowCaption = False
        Me.GroupControl7.Size = New System.Drawing.Size(315, 95)
        Me.GroupControl7.TabIndex = 1
        Me.GroupControl7.Text = "GroupControl7"
        '
        'btnChangeStatus
        '
        Me.btnChangeStatus.Location = New System.Drawing.Point(252, 8)
        Me.btnChangeStatus.Name = "btnChangeStatus"
        Me.btnChangeStatus.Size = New System.Drawing.Size(54, 25)
        Me.btnChangeStatus.TabIndex = 6
        Me.btnChangeStatus.TabStop = False
        Me.btnChangeStatus.Text = "Change"
        '
        'cbxController
        '
        Me.cbxController.AllowBlank = False
        Me.cbxController.DataSource = Nothing
        Me.cbxController.DisplayMember = Nothing
        Me.cbxController.EnterMoveNextControl = True
        Me.cbxController.Location = New System.Drawing.Point(72, 65)
        Me.cbxController.Name = "cbxController"
        Me.cbxController.Properties.AccessibleName = "Controller"
        Me.cbxController.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxController.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxController.Properties.Appearance.Options.UseFont = True
        Me.cbxController.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxController.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxController.SelectedValue = Nothing
        Me.cbxController.Size = New System.Drawing.Size(234, 22)
        Me.cbxController.TabIndex = 5
        Me.cbxController.Tag = "AEM"
        Me.cbxController.ValueMember = Nothing
        '
        'cbxArea
        '
        Me.cbxArea.AllowBlank = False
        Me.cbxArea.DataSource = Nothing
        Me.cbxArea.DisplayMember = Nothing
        Me.cbxArea.EnterMoveNextControl = True
        Me.cbxArea.Location = New System.Drawing.Point(72, 37)
        Me.cbxArea.Name = "cbxArea"
        Me.cbxArea.Properties.AccessibleName = "Area"
        Me.cbxArea.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxArea.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxArea.Properties.Appearance.Options.UseFont = True
        Me.cbxArea.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxArea.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxArea.SelectedValue = Nothing
        Me.cbxArea.Size = New System.Drawing.Size(234, 22)
        Me.cbxArea.TabIndex = 3
        Me.cbxArea.Tag = "AEM"
        Me.cbxArea.ValueMember = Nothing
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.Options.UseFont = True
        Me.CareLabel8.Appearance.Options.UseTextOptions = True
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(9, 68)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(53, 15)
        Me.CareLabel8.TabIndex = 4
        Me.CareLabel8.Text = "Controller"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel9.Appearance.Options.UseFont = True
        Me.CareLabel9.Appearance.Options.UseTextOptions = True
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(9, 40)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(24, 15)
        Me.CareLabel9.TabIndex = 2
        Me.CareLabel9.Text = "Area"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel10
        '
        Me.CareLabel10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel10.Appearance.Options.UseFont = True
        Me.CareLabel10.Appearance.Options.UseTextOptions = True
        Me.CareLabel10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel10.Location = New System.Drawing.Point(9, 12)
        Me.CareLabel10.Name = "CareLabel10"
        Me.CareLabel10.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel10.TabIndex = 0
        Me.CareLabel10.Text = "Status"
        Me.CareLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtStatus
        '
        Me.txtStatus.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtStatus.EditValue = "Status"
        Me.txtStatus.EnterMoveNextControl = True
        Me.txtStatus.Location = New System.Drawing.Point(72, 9)
        Me.txtStatus.MaxLength = 0
        Me.txtStatus.Name = "txtStatus"
        Me.txtStatus.NumericAllowNegatives = False
        Me.txtStatus.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtStatus.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStatus.Properties.AccessibleName = "Status"
        Me.txtStatus.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtStatus.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtStatus.Properties.Appearance.Options.UseFont = True
        Me.txtStatus.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStatus.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtStatus.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtStatus.Size = New System.Drawing.Size(174, 22)
        Me.txtStatus.TabIndex = 1
        Me.txtStatus.Tag = "RM"
        Me.txtStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtStatus.ToolTipText = ""
        '
        'GroupControl6
        '
        Me.GroupControl6.Controls.Add(Me.lblCompCert)
        Me.GroupControl6.Controls.Add(Me.lblDryCert)
        Me.GroupControl6.Controls.Add(Me.lblWorksOrder)
        Me.GroupControl6.Controls.Add(Me.txtWORcd)
        Me.GroupControl6.Controls.Add(Me.txtCompCert)
        Me.GroupControl6.Controls.Add(Me.txtDryCert)
        Me.GroupControl6.Location = New System.Drawing.Point(651, 5)
        Me.GroupControl6.Name = "GroupControl6"
        Me.GroupControl6.ShowCaption = False
        Me.GroupControl6.Size = New System.Drawing.Size(235, 95)
        Me.GroupControl6.TabIndex = 2
        Me.GroupControl6.Text = "GroupControl6"
        '
        'lblCompCert
        '
        Me.lblCompCert.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblCompCert.Appearance.Options.UseFont = True
        Me.lblCompCert.Appearance.Options.UseTextOptions = True
        Me.lblCompCert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblCompCert.Location = New System.Drawing.Point(9, 68)
        Me.lblCompCert.Name = "lblCompCert"
        Me.lblCompCert.Size = New System.Drawing.Size(120, 15)
        Me.lblCompCert.TabIndex = 4
        Me.lblCompCert.Text = "Completion Certificate"
        Me.lblCompCert.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDryCert
        '
        Me.lblDryCert.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblDryCert.Appearance.Options.UseFont = True
        Me.lblDryCert.Appearance.Options.UseTextOptions = True
        Me.lblDryCert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblDryCert.Location = New System.Drawing.Point(9, 40)
        Me.lblDryCert.Name = "lblDryCert"
        Me.lblDryCert.Size = New System.Drawing.Size(92, 15)
        Me.lblDryCert.TabIndex = 2
        Me.lblDryCert.Text = "Drying Certificate"
        Me.lblDryCert.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblWorksOrder
        '
        Me.lblWorksOrder.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblWorksOrder.Appearance.Options.UseFont = True
        Me.lblWorksOrder.Appearance.Options.UseTextOptions = True
        Me.lblWorksOrder.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblWorksOrder.Location = New System.Drawing.Point(9, 12)
        Me.lblWorksOrder.Name = "lblWorksOrder"
        Me.lblWorksOrder.Size = New System.Drawing.Size(116, 15)
        Me.lblWorksOrder.TabIndex = 0
        Me.lblWorksOrder.Text = "Works Order Received"
        Me.lblWorksOrder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtWORcd
        '
        Me.txtWORcd.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtWORcd.EnterMoveNextControl = True
        Me.txtWORcd.Location = New System.Drawing.Point(143, 9)
        Me.txtWORcd.MaxLength = 0
        Me.txtWORcd.Name = "txtWORcd"
        Me.txtWORcd.NumericAllowNegatives = False
        Me.txtWORcd.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtWORcd.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtWORcd.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtWORcd.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtWORcd.Properties.Appearance.Options.UseFont = True
        Me.txtWORcd.Properties.Appearance.Options.UseTextOptions = True
        Me.txtWORcd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtWORcd.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtWORcd.Size = New System.Drawing.Size(85, 22)
        Me.txtWORcd.TabIndex = 1
        Me.txtWORcd.Tag = "R"
        Me.txtWORcd.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtWORcd.ToolTipText = ""
        '
        'txtCompCert
        '
        Me.txtCompCert.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtCompCert.EnterMoveNextControl = True
        Me.txtCompCert.Location = New System.Drawing.Point(143, 65)
        Me.txtCompCert.MaxLength = 0
        Me.txtCompCert.Name = "txtCompCert"
        Me.txtCompCert.NumericAllowNegatives = False
        Me.txtCompCert.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtCompCert.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCompCert.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtCompCert.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtCompCert.Properties.Appearance.Options.UseFont = True
        Me.txtCompCert.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCompCert.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtCompCert.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtCompCert.Size = New System.Drawing.Size(85, 22)
        Me.txtCompCert.TabIndex = 5
        Me.txtCompCert.Tag = "R"
        Me.txtCompCert.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCompCert.ToolTipText = ""
        '
        'txtDryCert
        '
        Me.txtDryCert.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtDryCert.EnterMoveNextControl = True
        Me.txtDryCert.Location = New System.Drawing.Point(143, 37)
        Me.txtDryCert.MaxLength = 0
        Me.txtDryCert.Name = "txtDryCert"
        Me.txtDryCert.NumericAllowNegatives = False
        Me.txtDryCert.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtDryCert.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDryCert.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtDryCert.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtDryCert.Properties.Appearance.Options.UseFont = True
        Me.txtDryCert.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDryCert.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtDryCert.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDryCert.Size = New System.Drawing.Size(85, 22)
        Me.txtDryCert.TabIndex = 3
        Me.txtDryCert.Tag = "R"
        Me.txtDryCert.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDryCert.ToolTipText = ""
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.cbxLossAdj)
        Me.GroupControl4.Controls.Add(Me.cbxInsurer)
        Me.GroupControl4.Controls.Add(Me.CareLabel14)
        Me.GroupControl4.Controls.Add(Me.CareLabel13)
        Me.GroupControl4.Controls.Add(Me.CareLabel12)
        Me.GroupControl4.Controls.Add(Me.CareLabel11)
        Me.GroupControl4.Controls.Add(Me.txtInsClaim)
        Me.GroupControl4.Controls.Add(Me.txtInsJob)
        Me.GroupControl4.Location = New System.Drawing.Point(651, 106)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(428, 112)
        Me.GroupControl4.TabIndex = 7
        Me.GroupControl4.Text = "Insurance Details"
        '
        'cbxLossAdj
        '
        Me.cbxLossAdj.AllowBlank = False
        Me.cbxLossAdj.DataSource = Nothing
        Me.cbxLossAdj.DisplayMember = Nothing
        Me.cbxLossAdj.EnterMoveNextControl = True
        Me.cbxLossAdj.Location = New System.Drawing.Point(86, 55)
        Me.cbxLossAdj.Name = "cbxLossAdj"
        Me.cbxLossAdj.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxLossAdj.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxLossAdj.Properties.Appearance.Options.UseFont = True
        Me.cbxLossAdj.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxLossAdj.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxLossAdj.SelectedValue = Nothing
        Me.cbxLossAdj.Size = New System.Drawing.Size(334, 22)
        Me.cbxLossAdj.TabIndex = 3
        Me.cbxLossAdj.Tag = "AE"
        Me.cbxLossAdj.ValueMember = Nothing
        '
        'cbxInsurer
        '
        Me.cbxInsurer.AllowBlank = False
        Me.cbxInsurer.DataSource = Nothing
        Me.cbxInsurer.DisplayMember = Nothing
        Me.cbxInsurer.EnterMoveNextControl = True
        Me.cbxInsurer.Location = New System.Drawing.Point(86, 27)
        Me.cbxInsurer.Name = "cbxInsurer"
        Me.cbxInsurer.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxInsurer.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxInsurer.Properties.Appearance.Options.UseFont = True
        Me.cbxInsurer.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxInsurer.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxInsurer.SelectedValue = Nothing
        Me.cbxInsurer.Size = New System.Drawing.Size(335, 22)
        Me.cbxInsurer.TabIndex = 1
        Me.cbxInsurer.Tag = "AE"
        Me.cbxInsurer.ValueMember = Nothing
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel14.Appearance.Options.UseFont = True
        Me.CareLabel14.Appearance.Options.UseTextOptions = True
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(196, 86)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(54, 15)
        Me.CareLabel14.TabIndex = 6
        Me.CareLabel14.Text = "Claim Ref."
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel13
        '
        Me.CareLabel13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel13.Appearance.Options.UseFont = True
        Me.CareLabel13.Appearance.Options.UseTextOptions = True
        Me.CareLabel13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel13.Location = New System.Drawing.Point(9, 86)
        Me.CareLabel13.Name = "CareLabel13"
        Me.CareLabel13.Size = New System.Drawing.Size(65, 15)
        Me.CareLabel13.TabIndex = 4
        Me.CareLabel13.Text = "Job Number"
        Me.CareLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel12
        '
        Me.CareLabel12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel12.Appearance.Options.UseFont = True
        Me.CareLabel12.Appearance.Options.UseTextOptions = True
        Me.CareLabel12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel12.Location = New System.Drawing.Point(9, 58)
        Me.CareLabel12.Name = "CareLabel12"
        Me.CareLabel12.Size = New System.Drawing.Size(70, 15)
        Me.CareLabel12.TabIndex = 2
        Me.CareLabel12.Text = "Loss Adjuster"
        Me.CareLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.Options.UseFont = True
        Me.CareLabel11.Appearance.Options.UseTextOptions = True
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(9, 30)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(36, 15)
        Me.CareLabel11.TabIndex = 0
        Me.CareLabel11.Text = "Insurer"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtInsClaim
        '
        Me.txtInsClaim.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtInsClaim.EditValue = ""
        Me.txtInsClaim.EnterMoveNextControl = True
        Me.txtInsClaim.Location = New System.Drawing.Point(256, 83)
        Me.txtInsClaim.MaxLength = 0
        Me.txtInsClaim.Name = "txtInsClaim"
        Me.txtInsClaim.NumericAllowNegatives = False
        Me.txtInsClaim.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtInsClaim.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtInsClaim.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtInsClaim.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtInsClaim.Properties.Appearance.Options.UseFont = True
        Me.txtInsClaim.Properties.Appearance.Options.UseTextOptions = True
        Me.txtInsClaim.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtInsClaim.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtInsClaim.Size = New System.Drawing.Size(164, 22)
        Me.txtInsClaim.TabIndex = 7
        Me.txtInsClaim.Tag = "AE"
        Me.txtInsClaim.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtInsClaim.ToolTipText = ""
        '
        'txtInsJob
        '
        Me.txtInsJob.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtInsJob.EditValue = ""
        Me.txtInsJob.EnterMoveNextControl = True
        Me.txtInsJob.Location = New System.Drawing.Point(86, 83)
        Me.txtInsJob.MaxLength = 0
        Me.txtInsJob.Name = "txtInsJob"
        Me.txtInsJob.NumericAllowNegatives = False
        Me.txtInsJob.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtInsJob.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtInsJob.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtInsJob.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtInsJob.Properties.Appearance.Options.UseFont = True
        Me.txtInsJob.Properties.Appearance.Options.UseTextOptions = True
        Me.txtInsJob.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtInsJob.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtInsJob.Size = New System.Drawing.Size(100, 22)
        Me.txtInsJob.TabIndex = 5
        Me.txtInsJob.Tag = "AE"
        Me.txtInsJob.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtInsJob.ToolTipText = ""
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.CareLabel32)
        Me.GroupControl2.Controls.Add(Me.txtYardCount)
        Me.GroupControl2.Controls.Add(Me.txtYardSub)
        Me.GroupControl2.Controls.Add(Me.txtYardTotal)
        Me.GroupControl2.Controls.Add(Me.CareLabel30)
        Me.GroupControl2.Controls.Add(Me.CareLabel29)
        Me.GroupControl2.Controls.Add(Me.CareLabel28)
        Me.GroupControl2.Controls.Add(Me.CareLabel27)
        Me.GroupControl2.Controls.Add(Me.CareLabel23)
        Me.GroupControl2.Controls.Add(Me.CareLabel22)
        Me.GroupControl2.Controls.Add(Me.CareLabel21)
        Me.GroupControl2.Controls.Add(Me.CareLabel20)
        Me.GroupControl2.Controls.Add(Me.CareLabel19)
        Me.GroupControl2.Controls.Add(Me.CareLabel18)
        Me.GroupControl2.Controls.Add(Me.CareLabel1)
        Me.GroupControl2.Controls.Add(Me.txtLabCount)
        Me.GroupControl2.Controls.Add(Me.txtVersion)
        Me.GroupControl2.Controls.Add(Me.txtInvoiced)
        Me.GroupControl2.Controls.Add(Me.txtConCount)
        Me.GroupControl2.Controls.Add(Me.txtMatCount)
        Me.GroupControl2.Controls.Add(Me.txtLabSub)
        Me.GroupControl2.Controls.Add(Me.txtSubTotal)
        Me.GroupControl2.Controls.Add(Me.txtConSub)
        Me.GroupControl2.Controls.Add(Me.txtMatSub)
        Me.GroupControl2.Controls.Add(Me.txtLabTotal)
        Me.GroupControl2.Controls.Add(Me.txtGrandTotal)
        Me.GroupControl2.Controls.Add(Me.txtOverhead)
        Me.GroupControl2.Controls.Add(Me.txtTotalTotal)
        Me.GroupControl2.Controls.Add(Me.txtConTotal)
        Me.GroupControl2.Controls.Add(Me.txtMatTotal)
        Me.GroupControl2.Location = New System.Drawing.Point(651, 224)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(428, 262)
        Me.GroupControl2.TabIndex = 9
        Me.GroupControl2.Text = "Costing Analysis"
        '
        'CareLabel32
        '
        Me.CareLabel32.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel32.Appearance.Options.UseFont = True
        Me.CareLabel32.Appearance.Options.UseTextOptions = True
        Me.CareLabel32.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel32.Location = New System.Drawing.Point(9, 107)
        Me.CareLabel32.Name = "CareLabel32"
        Me.CareLabel32.Size = New System.Drawing.Size(75, 15)
        Me.CareLabel32.TabIndex = 26
        Me.CareLabel32.Text = "Yard Materials"
        Me.CareLabel32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtYardCount
        '
        Me.txtYardCount.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtYardCount.EnterMoveNextControl = True
        Me.txtYardCount.Location = New System.Drawing.Point(122, 104)
        Me.txtYardCount.MaxLength = 0
        Me.txtYardCount.Name = "txtYardCount"
        Me.txtYardCount.NumericAllowNegatives = True
        Me.txtYardCount.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtYardCount.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtYardCount.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtYardCount.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtYardCount.Properties.Appearance.Options.UseFont = True
        Me.txtYardCount.Properties.Appearance.Options.UseTextOptions = True
        Me.txtYardCount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtYardCount.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtYardCount.Size = New System.Drawing.Size(86, 22)
        Me.txtYardCount.TabIndex = 27
        Me.txtYardCount.Tag = "R"
        Me.txtYardCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtYardCount.ToolTipText = ""
        '
        'txtYardSub
        '
        Me.txtYardSub.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtYardSub.EnterMoveNextControl = True
        Me.txtYardSub.Location = New System.Drawing.Point(214, 104)
        Me.txtYardSub.MaxLength = 14
        Me.txtYardSub.Name = "txtYardSub"
        Me.txtYardSub.NumericAllowNegatives = True
        Me.txtYardSub.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtYardSub.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtYardSub.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtYardSub.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtYardSub.Properties.Appearance.Options.UseFont = True
        Me.txtYardSub.Properties.Appearance.Options.UseTextOptions = True
        Me.txtYardSub.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtYardSub.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txtYardSub.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtYardSub.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtYardSub.Properties.MaxLength = 14
        Me.txtYardSub.Size = New System.Drawing.Size(100, 22)
        Me.txtYardSub.TabIndex = 28
        Me.txtYardSub.Tag = "R"
        Me.txtYardSub.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtYardSub.ToolTipText = ""
        '
        'txtYardTotal
        '
        Me.txtYardTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtYardTotal.EnterMoveNextControl = True
        Me.txtYardTotal.Location = New System.Drawing.Point(320, 104)
        Me.txtYardTotal.MaxLength = 14
        Me.txtYardTotal.Name = "txtYardTotal"
        Me.txtYardTotal.NumericAllowNegatives = True
        Me.txtYardTotal.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtYardTotal.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtYardTotal.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtYardTotal.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtYardTotal.Properties.Appearance.Options.UseFont = True
        Me.txtYardTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.txtYardTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtYardTotal.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txtYardTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtYardTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtYardTotal.Properties.MaxLength = 14
        Me.txtYardTotal.Size = New System.Drawing.Size(100, 22)
        Me.txtYardTotal.TabIndex = 29
        Me.txtYardTotal.Tag = "R"
        Me.txtYardTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtYardTotal.ToolTipText = ""
        '
        'CareLabel30
        '
        Me.CareLabel30.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel30.Appearance.Options.UseFont = True
        Me.CareLabel30.Appearance.Options.UseTextOptions = True
        Me.CareLabel30.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel30.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.CareLabel30.Location = New System.Drawing.Point(320, 28)
        Me.CareLabel30.Name = "CareLabel30"
        Me.CareLabel30.Size = New System.Drawing.Size(100, 15)
        Me.CareLabel30.TabIndex = 2
        Me.CareLabel30.Text = "Total with Uplifts"
        Me.CareLabel30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CareLabel29
        '
        Me.CareLabel29.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel29.Appearance.Options.UseFont = True
        Me.CareLabel29.Appearance.Options.UseTextOptions = True
        Me.CareLabel29.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel29.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.CareLabel29.Location = New System.Drawing.Point(214, 27)
        Me.CareLabel29.Name = "CareLabel29"
        Me.CareLabel29.Size = New System.Drawing.Size(100, 16)
        Me.CareLabel29.TabIndex = 1
        Me.CareLabel29.Text = "Sub Total"
        Me.CareLabel29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CareLabel28
        '
        Me.CareLabel28.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel28.Appearance.Options.UseFont = True
        Me.CareLabel28.Appearance.Options.UseTextOptions = True
        Me.CareLabel28.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel28.Location = New System.Drawing.Point(252, 207)
        Me.CareLabel28.Name = "CareLabel28"
        Me.CareLabel28.Size = New System.Drawing.Size(62, 15)
        Me.CareLabel28.TabIndex = 20
        Me.CareLabel28.Text = "+ Overhead"
        Me.CareLabel28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel27
        '
        Me.CareLabel27.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.CareLabel27.Appearance.Options.UseFont = True
        Me.CareLabel27.Appearance.Options.UseTextOptions = True
        Me.CareLabel27.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel27.Location = New System.Drawing.Point(249, 235)
        Me.CareLabel27.Name = "CareLabel27"
        Me.CareLabel27.Size = New System.Drawing.Size(65, 15)
        Me.CareLabel27.TabIndex = 24
        Me.CareLabel27.Text = "Grand Total"
        Me.CareLabel27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel23
        '
        Me.CareLabel23.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel23.Appearance.Options.UseFont = True
        Me.CareLabel23.Appearance.Options.UseTextOptions = True
        Me.CareLabel23.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel23.Location = New System.Drawing.Point(9, 235)
        Me.CareLabel23.Name = "CareLabel23"
        Me.CareLabel23.Size = New System.Drawing.Size(73, 15)
        Me.CareLabel23.TabIndex = 22
        Me.CareLabel23.Text = "Latest Version"
        Me.CareLabel23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel22
        '
        Me.CareLabel22.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel22.Appearance.Options.UseFont = True
        Me.CareLabel22.Appearance.Options.UseTextOptions = True
        Me.CareLabel22.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel22.Location = New System.Drawing.Point(9, 207)
        Me.CareLabel22.Name = "CareLabel22"
        Me.CareLabel22.Size = New System.Drawing.Size(45, 15)
        Me.CareLabel22.TabIndex = 18
        Me.CareLabel22.Text = "Invoiced"
        Me.CareLabel22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel21
        '
        Me.CareLabel21.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.CareLabel21.Appearance.Options.UseFont = True
        Me.CareLabel21.Appearance.Options.UseTextOptions = True
        Me.CareLabel21.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel21.Location = New System.Drawing.Point(148, 171)
        Me.CareLabel21.Name = "CareLabel21"
        Me.CareLabel21.Size = New System.Drawing.Size(60, 15)
        Me.CareLabel21.TabIndex = 15
        Me.CareLabel21.Text = "Total Costs"
        Me.CareLabel21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel20
        '
        Me.CareLabel20.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel20.Appearance.Options.UseFont = True
        Me.CareLabel20.Appearance.Options.UseTextOptions = True
        Me.CareLabel20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel20.Location = New System.Drawing.Point(9, 135)
        Me.CareLabel20.Name = "CareLabel20"
        Me.CareLabel20.Size = New System.Drawing.Size(88, 15)
        Me.CareLabel20.TabIndex = 11
        Me.CareLabel20.Text = "Contractor Work"
        Me.CareLabel20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel19
        '
        Me.CareLabel19.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel19.Appearance.Options.UseFont = True
        Me.CareLabel19.Appearance.Options.UseTextOptions = True
        Me.CareLabel19.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel19.Location = New System.Drawing.Point(9, 79)
        Me.CareLabel19.Name = "CareLabel19"
        Me.CareLabel19.Size = New System.Drawing.Size(106, 15)
        Me.CareLabel19.TabIndex = 7
        Me.CareLabel19.Text = "Purchased Materials"
        Me.CareLabel19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel18
        '
        Me.CareLabel18.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel18.Appearance.Options.UseFont = True
        Me.CareLabel18.Appearance.Options.UseTextOptions = True
        Me.CareLabel18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel18.Location = New System.Drawing.Point(9, 51)
        Me.CareLabel18.Name = "CareLabel18"
        Me.CareLabel18.Size = New System.Drawing.Size(37, 15)
        Me.CareLabel18.TabIndex = 3
        Me.CareLabel18.Text = "Labour"
        Me.CareLabel18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.Options.UseFont = True
        Me.CareLabel1.Appearance.Options.UseTextOptions = True
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.CareLabel1.Location = New System.Drawing.Point(122, 27)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(86, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Units / Count"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtLabCount
        '
        Me.txtLabCount.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtLabCount.EnterMoveNextControl = True
        Me.txtLabCount.Location = New System.Drawing.Point(122, 48)
        Me.txtLabCount.MaxLength = 14
        Me.txtLabCount.Name = "txtLabCount"
        Me.txtLabCount.NumericAllowNegatives = True
        Me.txtLabCount.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtLabCount.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLabCount.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtLabCount.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtLabCount.Properties.Appearance.Options.UseFont = True
        Me.txtLabCount.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLabCount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtLabCount.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txtLabCount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtLabCount.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtLabCount.Properties.MaxLength = 14
        Me.txtLabCount.Size = New System.Drawing.Size(86, 22)
        Me.txtLabCount.TabIndex = 4
        Me.txtLabCount.Tag = "R"
        Me.txtLabCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtLabCount.ToolTipText = ""
        '
        'txtVersion
        '
        Me.txtVersion.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtVersion.EnterMoveNextControl = True
        Me.txtVersion.Location = New System.Drawing.Point(108, 232)
        Me.txtVersion.MaxLength = 14
        Me.txtVersion.Name = "txtVersion"
        Me.txtVersion.NumericAllowNegatives = True
        Me.txtVersion.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtVersion.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtVersion.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtVersion.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtVersion.Properties.Appearance.Options.UseFont = True
        Me.txtVersion.Properties.Appearance.Options.UseTextOptions = True
        Me.txtVersion.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtVersion.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txtVersion.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtVersion.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtVersion.Properties.MaxLength = 14
        Me.txtVersion.Size = New System.Drawing.Size(100, 22)
        Me.txtVersion.TabIndex = 23
        Me.txtVersion.Tag = "R"
        Me.txtVersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtVersion.ToolTipText = ""
        '
        'txtInvoiced
        '
        Me.txtInvoiced.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtInvoiced.EnterMoveNextControl = True
        Me.txtInvoiced.Location = New System.Drawing.Point(108, 204)
        Me.txtInvoiced.MaxLength = 14
        Me.txtInvoiced.Name = "txtInvoiced"
        Me.txtInvoiced.NumericAllowNegatives = True
        Me.txtInvoiced.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtInvoiced.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtInvoiced.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtInvoiced.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtInvoiced.Properties.Appearance.Options.UseFont = True
        Me.txtInvoiced.Properties.Appearance.Options.UseTextOptions = True
        Me.txtInvoiced.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtInvoiced.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txtInvoiced.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtInvoiced.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtInvoiced.Properties.MaxLength = 14
        Me.txtInvoiced.Size = New System.Drawing.Size(100, 22)
        Me.txtInvoiced.TabIndex = 19
        Me.txtInvoiced.Tag = "R"
        Me.txtInvoiced.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtInvoiced.ToolTipText = ""
        '
        'txtConCount
        '
        Me.txtConCount.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtConCount.EnterMoveNextControl = True
        Me.txtConCount.Location = New System.Drawing.Point(122, 132)
        Me.txtConCount.MaxLength = 0
        Me.txtConCount.Name = "txtConCount"
        Me.txtConCount.NumericAllowNegatives = True
        Me.txtConCount.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtConCount.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtConCount.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtConCount.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtConCount.Properties.Appearance.Options.UseFont = True
        Me.txtConCount.Properties.Appearance.Options.UseTextOptions = True
        Me.txtConCount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtConCount.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtConCount.Size = New System.Drawing.Size(86, 22)
        Me.txtConCount.TabIndex = 12
        Me.txtConCount.Tag = "R"
        Me.txtConCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtConCount.ToolTipText = ""
        '
        'txtMatCount
        '
        Me.txtMatCount.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtMatCount.EnterMoveNextControl = True
        Me.txtMatCount.Location = New System.Drawing.Point(122, 76)
        Me.txtMatCount.MaxLength = 0
        Me.txtMatCount.Name = "txtMatCount"
        Me.txtMatCount.NumericAllowNegatives = True
        Me.txtMatCount.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtMatCount.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtMatCount.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtMatCount.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtMatCount.Properties.Appearance.Options.UseFont = True
        Me.txtMatCount.Properties.Appearance.Options.UseTextOptions = True
        Me.txtMatCount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtMatCount.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtMatCount.Size = New System.Drawing.Size(86, 22)
        Me.txtMatCount.TabIndex = 8
        Me.txtMatCount.Tag = "R"
        Me.txtMatCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtMatCount.ToolTipText = ""
        '
        'txtLabSub
        '
        Me.txtLabSub.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtLabSub.EnterMoveNextControl = True
        Me.txtLabSub.Location = New System.Drawing.Point(214, 48)
        Me.txtLabSub.MaxLength = 14
        Me.txtLabSub.Name = "txtLabSub"
        Me.txtLabSub.NumericAllowNegatives = True
        Me.txtLabSub.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtLabSub.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLabSub.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtLabSub.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtLabSub.Properties.Appearance.Options.UseFont = True
        Me.txtLabSub.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLabSub.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtLabSub.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txtLabSub.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtLabSub.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtLabSub.Properties.MaxLength = 14
        Me.txtLabSub.Size = New System.Drawing.Size(100, 22)
        Me.txtLabSub.TabIndex = 5
        Me.txtLabSub.Tag = "R"
        Me.txtLabSub.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtLabSub.ToolTipText = ""
        '
        'txtSubTotal
        '
        Me.txtSubTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtSubTotal.EnterMoveNextControl = True
        Me.txtSubTotal.Location = New System.Drawing.Point(214, 168)
        Me.txtSubTotal.MaxLength = 14
        Me.txtSubTotal.Name = "txtSubTotal"
        Me.txtSubTotal.NumericAllowNegatives = True
        Me.txtSubTotal.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtSubTotal.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSubTotal.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSubTotal.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtSubTotal.Properties.Appearance.Options.UseFont = True
        Me.txtSubTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSubTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtSubTotal.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txtSubTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtSubTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSubTotal.Properties.MaxLength = 14
        Me.txtSubTotal.Size = New System.Drawing.Size(100, 22)
        Me.txtSubTotal.TabIndex = 16
        Me.txtSubTotal.Tag = "R"
        Me.txtSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSubTotal.ToolTipText = ""
        '
        'txtConSub
        '
        Me.txtConSub.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtConSub.EnterMoveNextControl = True
        Me.txtConSub.Location = New System.Drawing.Point(214, 132)
        Me.txtConSub.MaxLength = 14
        Me.txtConSub.Name = "txtConSub"
        Me.txtConSub.NumericAllowNegatives = True
        Me.txtConSub.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtConSub.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtConSub.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtConSub.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtConSub.Properties.Appearance.Options.UseFont = True
        Me.txtConSub.Properties.Appearance.Options.UseTextOptions = True
        Me.txtConSub.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtConSub.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txtConSub.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtConSub.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtConSub.Properties.MaxLength = 14
        Me.txtConSub.Size = New System.Drawing.Size(100, 22)
        Me.txtConSub.TabIndex = 13
        Me.txtConSub.Tag = "R"
        Me.txtConSub.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtConSub.ToolTipText = ""
        '
        'txtMatSub
        '
        Me.txtMatSub.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtMatSub.EnterMoveNextControl = True
        Me.txtMatSub.Location = New System.Drawing.Point(214, 76)
        Me.txtMatSub.MaxLength = 14
        Me.txtMatSub.Name = "txtMatSub"
        Me.txtMatSub.NumericAllowNegatives = True
        Me.txtMatSub.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtMatSub.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtMatSub.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtMatSub.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtMatSub.Properties.Appearance.Options.UseFont = True
        Me.txtMatSub.Properties.Appearance.Options.UseTextOptions = True
        Me.txtMatSub.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtMatSub.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txtMatSub.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtMatSub.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtMatSub.Properties.MaxLength = 14
        Me.txtMatSub.Size = New System.Drawing.Size(100, 22)
        Me.txtMatSub.TabIndex = 9
        Me.txtMatSub.Tag = "R"
        Me.txtMatSub.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtMatSub.ToolTipText = ""
        '
        'txtLabTotal
        '
        Me.txtLabTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtLabTotal.EnterMoveNextControl = True
        Me.txtLabTotal.Location = New System.Drawing.Point(320, 48)
        Me.txtLabTotal.MaxLength = 14
        Me.txtLabTotal.Name = "txtLabTotal"
        Me.txtLabTotal.NumericAllowNegatives = True
        Me.txtLabTotal.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtLabTotal.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLabTotal.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtLabTotal.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtLabTotal.Properties.Appearance.Options.UseFont = True
        Me.txtLabTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLabTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtLabTotal.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txtLabTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtLabTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtLabTotal.Properties.MaxLength = 14
        Me.txtLabTotal.Size = New System.Drawing.Size(100, 22)
        Me.txtLabTotal.TabIndex = 6
        Me.txtLabTotal.Tag = "R"
        Me.txtLabTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtLabTotal.ToolTipText = ""
        '
        'txtGrandTotal
        '
        Me.txtGrandTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtGrandTotal.EnterMoveNextControl = True
        Me.txtGrandTotal.Location = New System.Drawing.Point(320, 232)
        Me.txtGrandTotal.MaxLength = 14
        Me.txtGrandTotal.Name = "txtGrandTotal"
        Me.txtGrandTotal.NumericAllowNegatives = True
        Me.txtGrandTotal.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtGrandTotal.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtGrandTotal.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtGrandTotal.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtGrandTotal.Properties.Appearance.Options.UseFont = True
        Me.txtGrandTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.txtGrandTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtGrandTotal.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txtGrandTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtGrandTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtGrandTotal.Properties.MaxLength = 14
        Me.txtGrandTotal.Size = New System.Drawing.Size(100, 22)
        Me.txtGrandTotal.TabIndex = 25
        Me.txtGrandTotal.Tag = "R"
        Me.txtGrandTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtGrandTotal.ToolTipText = ""
        '
        'txtOverhead
        '
        Me.txtOverhead.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtOverhead.EnterMoveNextControl = True
        Me.txtOverhead.Location = New System.Drawing.Point(320, 204)
        Me.txtOverhead.MaxLength = 14
        Me.txtOverhead.Name = "txtOverhead"
        Me.txtOverhead.NumericAllowNegatives = True
        Me.txtOverhead.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtOverhead.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtOverhead.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtOverhead.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtOverhead.Properties.Appearance.Options.UseFont = True
        Me.txtOverhead.Properties.Appearance.Options.UseTextOptions = True
        Me.txtOverhead.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtOverhead.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txtOverhead.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtOverhead.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtOverhead.Properties.MaxLength = 14
        Me.txtOverhead.Size = New System.Drawing.Size(100, 22)
        Me.txtOverhead.TabIndex = 21
        Me.txtOverhead.Tag = "R"
        Me.txtOverhead.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtOverhead.ToolTipText = ""
        '
        'txtTotalTotal
        '
        Me.txtTotalTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtTotalTotal.EnterMoveNextControl = True
        Me.txtTotalTotal.Location = New System.Drawing.Point(320, 168)
        Me.txtTotalTotal.MaxLength = 14
        Me.txtTotalTotal.Name = "txtTotalTotal"
        Me.txtTotalTotal.NumericAllowNegatives = True
        Me.txtTotalTotal.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtTotalTotal.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTotalTotal.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtTotalTotal.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtTotalTotal.Properties.Appearance.Options.UseFont = True
        Me.txtTotalTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTotalTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtTotalTotal.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txtTotalTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtTotalTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTotalTotal.Properties.MaxLength = 14
        Me.txtTotalTotal.Size = New System.Drawing.Size(100, 22)
        Me.txtTotalTotal.TabIndex = 17
        Me.txtTotalTotal.Tag = "R"
        Me.txtTotalTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalTotal.ToolTipText = ""
        '
        'txtConTotal
        '
        Me.txtConTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtConTotal.EnterMoveNextControl = True
        Me.txtConTotal.Location = New System.Drawing.Point(320, 132)
        Me.txtConTotal.MaxLength = 14
        Me.txtConTotal.Name = "txtConTotal"
        Me.txtConTotal.NumericAllowNegatives = True
        Me.txtConTotal.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtConTotal.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtConTotal.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtConTotal.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtConTotal.Properties.Appearance.Options.UseFont = True
        Me.txtConTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.txtConTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtConTotal.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txtConTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtConTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtConTotal.Properties.MaxLength = 14
        Me.txtConTotal.Size = New System.Drawing.Size(100, 22)
        Me.txtConTotal.TabIndex = 14
        Me.txtConTotal.Tag = "R"
        Me.txtConTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtConTotal.ToolTipText = ""
        '
        'txtMatTotal
        '
        Me.txtMatTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtMatTotal.EnterMoveNextControl = True
        Me.txtMatTotal.Location = New System.Drawing.Point(320, 76)
        Me.txtMatTotal.MaxLength = 14
        Me.txtMatTotal.Name = "txtMatTotal"
        Me.txtMatTotal.NumericAllowNegatives = True
        Me.txtMatTotal.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtMatTotal.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtMatTotal.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtMatTotal.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtMatTotal.Properties.Appearance.Options.UseFont = True
        Me.txtMatTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.txtMatTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtMatTotal.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txtMatTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtMatTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtMatTotal.Properties.MaxLength = 14
        Me.txtMatTotal.Size = New System.Drawing.Size(100, 22)
        Me.txtMatTotal.TabIndex = 10
        Me.txtMatTotal.Tag = "R"
        Me.txtMatTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtMatTotal.ToolTipText = ""
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.lblOnHold)
        Me.GroupControl1.Controls.Add(Me.cdtEnd)
        Me.GroupControl1.Controls.Add(Me.cdtStart)
        Me.GroupControl1.Controls.Add(Me.cbxSector)
        Me.GroupControl1.Controls.Add(Me.CareLabel4)
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.txtNo)
        Me.GroupControl1.Location = New System.Drawing.Point(7, 5)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(315, 95)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "GroupControl1"
        '
        'lblOnHold
        '
        Me.lblOnHold.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOnHold.Appearance.ForeColor = System.Drawing.Color.Red
        Me.lblOnHold.Appearance.Options.UseFont = True
        Me.lblOnHold.Appearance.Options.UseForeColor = True
        Me.lblOnHold.Appearance.Options.UseTextOptions = True
        Me.lblOnHold.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblOnHold.Location = New System.Drawing.Point(197, 12)
        Me.lblOnHold.Name = "lblOnHold"
        Me.lblOnHold.Size = New System.Drawing.Size(54, 15)
        Me.lblOnHold.TabIndex = 2
        Me.lblOnHold.Text = "ON HOLD"
        Me.lblOnHold.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtEnd
        '
        Me.cdtEnd.EditValue = Nothing
        Me.cdtEnd.EnterMoveNextControl = True
        Me.cdtEnd.Location = New System.Drawing.Point(197, 65)
        Me.cdtEnd.Name = "cdtEnd"
        Me.cdtEnd.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtEnd.Properties.Appearance.Options.UseFont = True
        Me.cdtEnd.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtEnd.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtEnd.Size = New System.Drawing.Size(100, 22)
        Me.cdtEnd.TabIndex = 7
        Me.cdtEnd.Tag = "AE"
        Me.cdtEnd.Value = Nothing
        '
        'cdtStart
        '
        Me.cdtStart.EditValue = Nothing
        Me.cdtStart.EnterMoveNextControl = True
        Me.cdtStart.Location = New System.Drawing.Point(91, 65)
        Me.cdtStart.Name = "cdtStart"
        Me.cdtStart.Properties.AccessibleName = "Start Date"
        Me.cdtStart.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtStart.Properties.Appearance.Options.UseFont = True
        Me.cdtStart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtStart.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtStart.Size = New System.Drawing.Size(100, 22)
        Me.cdtStart.TabIndex = 6
        Me.cdtStart.Tag = "AEM"
        Me.cdtStart.Value = Nothing
        '
        'cbxSector
        '
        Me.cbxSector.AllowBlank = False
        Me.cbxSector.DataSource = Nothing
        Me.cbxSector.DisplayMember = Nothing
        Me.cbxSector.EnterMoveNextControl = True
        Me.cbxSector.Location = New System.Drawing.Point(91, 37)
        Me.cbxSector.Name = "cbxSector"
        Me.cbxSector.Properties.AccessibleName = "Sector"
        Me.cbxSector.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSector.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxSector.Properties.Appearance.Options.UseFont = True
        Me.cbxSector.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSector.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSector.SelectedValue = Nothing
        Me.cbxSector.Size = New System.Drawing.Size(206, 22)
        Me.cbxSector.TabIndex = 4
        Me.cbxSector.Tag = "AEM"
        Me.cbxSector.ValueMember = Nothing
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.Options.UseFont = True
        Me.CareLabel4.Appearance.Options.UseTextOptions = True
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(9, 68)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(76, 15)
        Me.CareLabel4.TabIndex = 5
        Me.CareLabel4.Text = "Start/End Date"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.Options.UseFont = True
        Me.CareLabel3.Appearance.Options.UseTextOptions = True
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(9, 40)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(33, 15)
        Me.CareLabel3.TabIndex = 3
        Me.CareLabel3.Text = "Sector"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.Options.UseFont = True
        Me.CareLabel2.Appearance.Options.UseTextOptions = True
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(9, 12)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(60, 15)
        Me.CareLabel2.TabIndex = 0
        Me.CareLabel2.Text = "Our Job No"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNo
        '
        Me.txtNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtNo.EnterMoveNextControl = True
        Me.txtNo.Location = New System.Drawing.Point(91, 9)
        Me.txtNo.MaxLength = 0
        Me.txtNo.Name = "txtNo"
        Me.txtNo.NumericAllowNegatives = False
        Me.txtNo.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtNo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtNo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtNo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtNo.Properties.Appearance.Options.UseFont = True
        Me.txtNo.Properties.Appearance.Options.UseTextOptions = True
        Me.txtNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtNo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtNo.Size = New System.Drawing.Size(100, 22)
        Me.txtNo.TabIndex = 1
        Me.txtNo.Tag = "R"
        Me.txtNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNo.ToolTipText = ""
        '
        'tabLinks
        '
        Me.tabLinks.Controls.Add(Me.cgLinked)
        Me.tabLinks.Name = "tabLinks"
        Me.tabLinks.Size = New System.Drawing.Size(1087, 492)
        Me.tabLinks.Text = "Linked Jobs"
        '
        'cgLinked
        '
        Me.cgLinked.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgLinked.ButtonsEnabled = False
        Me.cgLinked.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgLinked.HideFirstColumn = False
        Me.cgLinked.Location = New System.Drawing.Point(3, 3)
        Me.cgLinked.Name = "cgLinked"
        Me.cgLinked.PreviewColumn = ""
        Me.cgLinked.Size = New System.Drawing.Size(1081, 486)
        Me.cgLinked.TabIndex = 1
        '
        'tabDescriptions
        '
        Me.tabDescriptions.Controls.Add(Me.GroupControl5)
        Me.tabDescriptions.Controls.Add(Me.rich)
        Me.tabDescriptions.Name = "tabDescriptions"
        Me.tabDescriptions.Size = New System.Drawing.Size(1087, 492)
        Me.tabDescriptions.Text = "Descriptions"
        '
        'GroupControl5
        '
        Me.GroupControl5.Controls.Add(Me.radDescTools)
        Me.GroupControl5.Controls.Add(Me.radDescEst)
        Me.GroupControl5.Controls.Add(Me.radDescDirections)
        Me.GroupControl5.Controls.Add(Me.radDescJob)
        Me.GroupControl5.Location = New System.Drawing.Point(7, 5)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.ShowCaption = False
        Me.GroupControl5.Size = New System.Drawing.Size(813, 34)
        Me.GroupControl5.TabIndex = 1
        Me.GroupControl5.Text = "GroupControl5"
        '
        'radDescTools
        '
        Me.radDescTools.Location = New System.Drawing.Point(293, 7)
        Me.radDescTools.Name = "radDescTools"
        Me.radDescTools.Properties.AutoWidth = True
        Me.radDescTools.Properties.Caption = "Tools and Materials"
        Me.radDescTools.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radDescTools.Properties.RadioGroupIndex = 0
        Me.radDescTools.Size = New System.Drawing.Size(114, 19)
        Me.radDescTools.TabIndex = 7
        Me.radDescTools.TabStop = False
        '
        'radDescEst
        '
        Me.radDescEst.Location = New System.Drawing.Point(184, 7)
        Me.radDescEst.Name = "radDescEst"
        Me.radDescEst.Properties.AutoWidth = True
        Me.radDescEst.Properties.Caption = "Estimators Notes"
        Me.radDescEst.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radDescEst.Properties.RadioGroupIndex = 0
        Me.radDescEst.Size = New System.Drawing.Size(103, 19)
        Me.radDescEst.TabIndex = 6
        Me.radDescEst.TabStop = False
        '
        'radDescDirections
        '
        Me.radDescDirections.Location = New System.Drawing.Point(109, 7)
        Me.radDescDirections.Name = "radDescDirections"
        Me.radDescDirections.Properties.AutoWidth = True
        Me.radDescDirections.Properties.Caption = "Directions"
        Me.radDescDirections.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radDescDirections.Properties.RadioGroupIndex = 0
        Me.radDescDirections.Size = New System.Drawing.Size(69, 19)
        Me.radDescDirections.TabIndex = 5
        Me.radDescDirections.TabStop = False
        '
        'radDescJob
        '
        Me.radDescJob.EditValue = True
        Me.radDescJob.Location = New System.Drawing.Point(8, 7)
        Me.radDescJob.Name = "radDescJob"
        Me.radDescJob.Properties.AutoWidth = True
        Me.radDescJob.Properties.Caption = "Job Description"
        Me.radDescJob.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radDescJob.Properties.RadioGroupIndex = 0
        Me.radDescJob.Size = New System.Drawing.Size(95, 19)
        Me.radDescJob.TabIndex = 4
        '
        'rich
        '
        Me.rich.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rich.HTMLText = resources.GetString("rich.HTMLText")
        Me.rich.Location = New System.Drawing.Point(3, 45)
        Me.rich.Name = "rich"
        Me.rich.RTFText = resources.GetString("rich.RTFText")
        Me.rich.Size = New System.Drawing.Size(1081, 444)
        Me.rich.TabIndex = 0
        '
        'tabCalendar
        '
        Me.tabCalendar.Controls.Add(Me.yvCalendar)
        Me.tabCalendar.Name = "tabCalendar"
        Me.tabCalendar.Size = New System.Drawing.Size(1087, 492)
        Me.tabCalendar.Text = "Calendar"
        '
        'yvCalendar
        '
        Me.yvCalendar.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.yvCalendar.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.yvCalendar.Location = New System.Drawing.Point(3, 3)
        Me.yvCalendar.Name = "yvCalendar"
        Me.yvCalendar.Size = New System.Drawing.Size(1081, 486)
        Me.yvCalendar.TabIndex = 0
        '
        'tabVersions
        '
        Me.tabVersions.Controls.Add(Me.cgVersions)
        Me.tabVersions.Name = "tabVersions"
        Me.tabVersions.Size = New System.Drawing.Size(1087, 492)
        Me.tabVersions.Text = "Versions"
        '
        'cgVersions
        '
        Me.cgVersions.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgVersions.ButtonsEnabled = False
        Me.cgVersions.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgVersions.HideFirstColumn = False
        Me.cgVersions.Location = New System.Drawing.Point(3, 3)
        Me.cgVersions.Name = "cgVersions"
        Me.cgVersions.PreviewColumn = ""
        Me.cgVersions.Size = New System.Drawing.Size(1081, 486)
        Me.cgVersions.TabIndex = 0
        '
        'tabMaterials
        '
        Me.tabMaterials.Controls.Add(Me.btnMatDelete)
        Me.tabMaterials.Controls.Add(Me.btnMatMove)
        Me.tabMaterials.Controls.Add(Me.GroupControl11)
        Me.tabMaterials.Controls.Add(Me.cgMaterials)
        Me.tabMaterials.Name = "tabMaterials"
        Me.tabMaterials.Size = New System.Drawing.Size(1087, 492)
        Me.tabMaterials.Text = "Materials"
        '
        'btnMatDelete
        '
        Me.btnMatDelete.Location = New System.Drawing.Point(129, 464)
        Me.btnMatDelete.Name = "btnMatDelete"
        Me.btnMatDelete.Size = New System.Drawing.Size(120, 25)
        Me.btnMatDelete.TabIndex = 4
        Me.btnMatDelete.Text = "Delete"
        '
        'btnMatMove
        '
        Me.btnMatMove.Location = New System.Drawing.Point(3, 464)
        Me.btnMatMove.Name = "btnMatMove"
        Me.btnMatMove.Size = New System.Drawing.Size(120, 25)
        Me.btnMatMove.TabIndex = 2
        Me.btnMatMove.Text = "Move to Another Job"
        '
        'GroupControl11
        '
        Me.GroupControl11.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl11.Controls.Add(Me.radInvSummaryInvNotes)
        Me.GroupControl11.Controls.Add(Me.radInvInvNotes)
        Me.GroupControl11.Controls.Add(Me.radInvContCred)
        Me.GroupControl11.Controls.Add(Me.radInvCont)
        Me.GroupControl11.Controls.Add(Me.radInvSupCred)
        Me.GroupControl11.Controls.Add(Me.radInvSup)
        Me.GroupControl11.Controls.Add(Me.radInvAll)
        Me.GroupControl11.Location = New System.Drawing.Point(3, 5)
        Me.GroupControl11.Name = "GroupControl11"
        Me.GroupControl11.ShowCaption = False
        Me.GroupControl11.Size = New System.Drawing.Size(1081, 34)
        Me.GroupControl11.TabIndex = 0
        Me.GroupControl11.Text = "GroupControl11"
        '
        'radInvSummaryInvNotes
        '
        Me.radInvSummaryInvNotes.Location = New System.Drawing.Point(314, 7)
        Me.radInvSummaryInvNotes.Name = "radInvSummaryInvNotes"
        Me.radInvSummaryInvNotes.Properties.AutoWidth = True
        Me.radInvSummaryInvNotes.Properties.Caption = "Invoicing Notes (Summarised)"
        Me.radInvSummaryInvNotes.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radInvSummaryInvNotes.Properties.RadioGroupIndex = 0
        Me.radInvSummaryInvNotes.Size = New System.Drawing.Size(164, 19)
        Me.radInvSummaryInvNotes.TabIndex = 3
        Me.radInvSummaryInvNotes.TabStop = False
        '
        'radInvInvNotes
        '
        Me.radInvInvNotes.Location = New System.Drawing.Point(212, 7)
        Me.radInvInvNotes.Name = "radInvInvNotes"
        Me.radInvInvNotes.Properties.AutoWidth = True
        Me.radInvInvNotes.Properties.Caption = "Invoicing Notes"
        Me.radInvInvNotes.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radInvInvNotes.Properties.RadioGroupIndex = 0
        Me.radInvInvNotes.Size = New System.Drawing.Size(96, 19)
        Me.radInvInvNotes.TabIndex = 2
        Me.radInvInvNotes.TabStop = False
        '
        'radInvContCred
        '
        Me.radInvContCred.Location = New System.Drawing.Point(710, 7)
        Me.radInvContCred.Name = "radInvContCred"
        Me.radInvContCred.Properties.AutoWidth = True
        Me.radInvContCred.Properties.Caption = "Contractor Credits"
        Me.radInvContCred.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radInvContCred.Properties.RadioGroupIndex = 0
        Me.radInvContCred.Size = New System.Drawing.Size(111, 19)
        Me.radInvContCred.TabIndex = 6
        Me.radInvContCred.TabStop = False
        '
        'radInvCont
        '
        Me.radInvCont.Location = New System.Drawing.Point(587, 7)
        Me.radInvCont.Name = "radInvCont"
        Me.radInvCont.Properties.AutoWidth = True
        Me.radInvCont.Properties.Caption = "Contractor Invoices"
        Me.radInvCont.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radInvCont.Properties.RadioGroupIndex = 0
        Me.radInvCont.Size = New System.Drawing.Size(117, 19)
        Me.radInvCont.TabIndex = 5
        Me.radInvCont.TabStop = False
        '
        'radInvSupCred
        '
        Me.radInvSupCred.Location = New System.Drawing.Point(484, 7)
        Me.radInvSupCred.Name = "radInvSupCred"
        Me.radInvSupCred.Properties.AutoWidth = True
        Me.radInvSupCred.Properties.Caption = "Supplier Credits"
        Me.radInvSupCred.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radInvSupCred.Properties.RadioGroupIndex = 0
        Me.radInvSupCred.Size = New System.Drawing.Size(97, 19)
        Me.radInvSupCred.TabIndex = 4
        Me.radInvSupCred.TabStop = False
        '
        'radInvSup
        '
        Me.radInvSup.Location = New System.Drawing.Point(103, 7)
        Me.radInvSup.Name = "radInvSup"
        Me.radInvSup.Properties.AutoWidth = True
        Me.radInvSup.Properties.Caption = "Supplier Invoices"
        Me.radInvSup.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radInvSup.Properties.RadioGroupIndex = 0
        Me.radInvSup.Size = New System.Drawing.Size(103, 19)
        Me.radInvSup.TabIndex = 1
        Me.radInvSup.TabStop = False
        '
        'radInvAll
        '
        Me.radInvAll.EditValue = True
        Me.radInvAll.Location = New System.Drawing.Point(8, 7)
        Me.radInvAll.Name = "radInvAll"
        Me.radInvAll.Properties.AutoWidth = True
        Me.radInvAll.Properties.Caption = "All Documents"
        Me.radInvAll.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radInvAll.Properties.RadioGroupIndex = 0
        Me.radInvAll.Size = New System.Drawing.Size(89, 19)
        Me.radInvAll.TabIndex = 0
        '
        'cgMaterials
        '
        Me.cgMaterials.AllowBuildColumns = True
        Me.cgMaterials.AllowEdit = False
        Me.cgMaterials.AllowHorizontalScroll = False
        Me.cgMaterials.AllowMultiSelect = False
        Me.cgMaterials.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgMaterials.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgMaterials.Appearance.Options.UseFont = True
        Me.cgMaterials.AutoSizeByData = True
        Me.cgMaterials.DisableAutoSize = False
        Me.cgMaterials.DisableDataFormatting = False
        Me.cgMaterials.FocusedRowHandle = -2147483648
        Me.cgMaterials.HideFirstColumn = False
        Me.cgMaterials.Location = New System.Drawing.Point(3, 45)
        Me.cgMaterials.Name = "cgMaterials"
        Me.cgMaterials.PreviewColumn = ""
        Me.cgMaterials.QueryID = Nothing
        Me.cgMaterials.RowAutoHeight = False
        Me.cgMaterials.SearchAsYouType = True
        Me.cgMaterials.ShowAutoFilterRow = True
        Me.cgMaterials.ShowFindPanel = True
        Me.cgMaterials.ShowGroupByBox = False
        Me.cgMaterials.ShowLoadingPanel = False
        Me.cgMaterials.ShowNavigator = True
        Me.cgMaterials.Size = New System.Drawing.Size(1081, 413)
        Me.cgMaterials.TabIndex = 1
        '
        'tabPlant
        '
        Me.tabPlant.Controls.Add(Me.cgPlant)
        Me.tabPlant.Name = "tabPlant"
        Me.tabPlant.Size = New System.Drawing.Size(1087, 492)
        Me.tabPlant.Text = "Plant"
        '
        'cgPlant
        '
        Me.cgPlant.AllowBuildColumns = True
        Me.cgPlant.AllowEdit = False
        Me.cgPlant.AllowHorizontalScroll = False
        Me.cgPlant.AllowMultiSelect = False
        Me.cgPlant.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgPlant.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgPlant.Appearance.Options.UseFont = True
        Me.cgPlant.AutoSizeByData = True
        Me.cgPlant.DisableAutoSize = False
        Me.cgPlant.DisableDataFormatting = False
        Me.cgPlant.FocusedRowHandle = -2147483648
        Me.cgPlant.HideFirstColumn = False
        Me.cgPlant.Location = New System.Drawing.Point(3, 3)
        Me.cgPlant.Name = "cgPlant"
        Me.cgPlant.PreviewColumn = ""
        Me.cgPlant.QueryID = Nothing
        Me.cgPlant.RowAutoHeight = False
        Me.cgPlant.SearchAsYouType = True
        Me.cgPlant.ShowAutoFilterRow = True
        Me.cgPlant.ShowFindPanel = True
        Me.cgPlant.ShowGroupByBox = False
        Me.cgPlant.ShowLoadingPanel = False
        Me.cgPlant.ShowNavigator = True
        Me.cgPlant.Size = New System.Drawing.Size(1081, 486)
        Me.cgPlant.TabIndex = 3
        '
        'tabHire
        '
        Me.tabHire.Controls.Add(Me.cgHire)
        Me.tabHire.Name = "tabHire"
        Me.tabHire.Size = New System.Drawing.Size(1087, 492)
        Me.tabHire.Text = "Hire"
        '
        'cgHire
        '
        Me.cgHire.AllowBuildColumns = True
        Me.cgHire.AllowEdit = False
        Me.cgHire.AllowHorizontalScroll = False
        Me.cgHire.AllowMultiSelect = False
        Me.cgHire.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgHire.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgHire.Appearance.Options.UseFont = True
        Me.cgHire.AutoSizeByData = True
        Me.cgHire.DisableAutoSize = False
        Me.cgHire.DisableDataFormatting = False
        Me.cgHire.FocusedRowHandle = -2147483648
        Me.cgHire.HideFirstColumn = False
        Me.cgHire.Location = New System.Drawing.Point(3, 3)
        Me.cgHire.Name = "cgHire"
        Me.cgHire.PreviewColumn = ""
        Me.cgHire.QueryID = Nothing
        Me.cgHire.RowAutoHeight = False
        Me.cgHire.SearchAsYouType = True
        Me.cgHire.ShowAutoFilterRow = True
        Me.cgHire.ShowFindPanel = True
        Me.cgHire.ShowGroupByBox = False
        Me.cgHire.ShowLoadingPanel = False
        Me.cgHire.ShowNavigator = True
        Me.cgHire.Size = New System.Drawing.Size(1081, 486)
        Me.cgHire.TabIndex = 2
        '
        'tabLabour
        '
        Me.tabLabour.Controls.Add(Me.cgLabour)
        Me.tabLabour.Name = "tabLabour"
        Me.tabLabour.Size = New System.Drawing.Size(1087, 492)
        Me.tabLabour.Text = "Labour"
        '
        'cgLabour
        '
        Me.cgLabour.AllowBuildColumns = True
        Me.cgLabour.AllowEdit = False
        Me.cgLabour.AllowHorizontalScroll = False
        Me.cgLabour.AllowMultiSelect = False
        Me.cgLabour.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgLabour.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgLabour.Appearance.Options.UseFont = True
        Me.cgLabour.AutoSizeByData = True
        Me.cgLabour.DisableAutoSize = False
        Me.cgLabour.DisableDataFormatting = False
        Me.cgLabour.FocusedRowHandle = -2147483648
        Me.cgLabour.HideFirstColumn = False
        Me.cgLabour.Location = New System.Drawing.Point(3, 3)
        Me.cgLabour.Name = "cgLabour"
        Me.cgLabour.PreviewColumn = ""
        Me.cgLabour.QueryID = Nothing
        Me.cgLabour.RowAutoHeight = False
        Me.cgLabour.SearchAsYouType = True
        Me.cgLabour.ShowAutoFilterRow = True
        Me.cgLabour.ShowFindPanel = True
        Me.cgLabour.ShowGroupByBox = False
        Me.cgLabour.ShowLoadingPanel = False
        Me.cgLabour.ShowNavigator = True
        Me.cgLabour.Size = New System.Drawing.Size(1081, 486)
        Me.cgLabour.TabIndex = 1
        '
        'tabInvoices
        '
        Me.tabInvoices.Controls.Add(Me.cgSalesInvoices)
        Me.tabInvoices.Name = "tabInvoices"
        Me.tabInvoices.Size = New System.Drawing.Size(1087, 492)
        Me.tabInvoices.Text = "Invoices"
        '
        'cgSalesInvoices
        '
        Me.cgSalesInvoices.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgSalesInvoices.ButtonsEnabled = False
        Me.cgSalesInvoices.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgSalesInvoices.HideFirstColumn = False
        Me.cgSalesInvoices.Location = New System.Drawing.Point(3, 3)
        Me.cgSalesInvoices.Name = "cgSalesInvoices"
        Me.cgSalesInvoices.PreviewColumn = ""
        Me.cgSalesInvoices.Size = New System.Drawing.Size(1081, 486)
        Me.cgSalesInvoices.TabIndex = 1
        '
        'tabNotes
        '
        Me.tabNotes.Controls.Add(Me.GroupControl13)
        Me.tabNotes.Name = "tabNotes"
        Me.tabNotes.Size = New System.Drawing.Size(1087, 492)
        Me.tabNotes.Text = "Notes"
        '
        'GroupControl13
        '
        Me.GroupControl13.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl13.Controls.Add(Me.txtNotes)
        Me.GroupControl13.Location = New System.Drawing.Point(3, 3)
        Me.GroupControl13.Name = "GroupControl13"
        Me.GroupControl13.ShowCaption = False
        Me.GroupControl13.Size = New System.Drawing.Size(1081, 486)
        Me.GroupControl13.TabIndex = 1
        Me.GroupControl13.Text = "GroupControl13"
        '
        'txtNotes
        '
        Me.txtNotes.Location = New System.Drawing.Point(5, 5)
        Me.txtNotes.Name = "txtNotes"
        Me.txtNotes.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNotes.Properties.Appearance.Options.UseFont = True
        Me.txtNotes.Properties.MaxLength = 1024
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtNotes, True)
        Me.txtNotes.Size = New System.Drawing.Size(1071, 476)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtNotes, OptionsSpelling1)
        Me.txtNotes.TabIndex = 5
        Me.txtNotes.Tag = "AE"
        '
        'tabActivity
        '
        Me.tabActivity.Controls.Add(Me.cgActivity)
        Me.tabActivity.Name = "tabActivity"
        Me.tabActivity.Size = New System.Drawing.Size(1087, 492)
        Me.tabActivity.Text = "Activity"
        '
        'cgActivity
        '
        Me.cgActivity.AllowBuildColumns = True
        Me.cgActivity.AllowEdit = False
        Me.cgActivity.AllowHorizontalScroll = False
        Me.cgActivity.AllowMultiSelect = False
        Me.cgActivity.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgActivity.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgActivity.Appearance.Options.UseFont = True
        Me.cgActivity.AutoSizeByData = True
        Me.cgActivity.DisableAutoSize = False
        Me.cgActivity.DisableDataFormatting = False
        Me.cgActivity.FocusedRowHandle = -2147483648
        Me.cgActivity.HideFirstColumn = False
        Me.cgActivity.Location = New System.Drawing.Point(3, 3)
        Me.cgActivity.Name = "cgActivity"
        Me.cgActivity.PreviewColumn = ""
        Me.cgActivity.QueryID = Nothing
        Me.cgActivity.RowAutoHeight = False
        Me.cgActivity.SearchAsYouType = True
        Me.cgActivity.ShowAutoFilterRow = True
        Me.cgActivity.ShowFindPanel = True
        Me.cgActivity.ShowGroupByBox = False
        Me.cgActivity.ShowLoadingPanel = False
        Me.cgActivity.ShowNavigator = True
        Me.cgActivity.Size = New System.Drawing.Size(1081, 486)
        Me.cgActivity.TabIndex = 1
        '
        'tabPhotos
        '
        Me.tabPhotos.Controls.Add(Me.pic)
        Me.tabPhotos.Controls.Add(Me.cgFolders)
        Me.tabPhotos.Controls.Add(Me.cgFiles)
        Me.tabPhotos.Name = "tabPhotos"
        Me.tabPhotos.Size = New System.Drawing.Size(1087, 492)
        Me.tabPhotos.Text = "Photos"
        '
        'pic
        '
        Me.pic.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pic.Location = New System.Drawing.Point(368, 3)
        Me.pic.Name = "pic"
        Me.pic.Size = New System.Drawing.Size(716, 486)
        Me.pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pic.TabIndex = 3
        Me.pic.TabStop = False
        '
        'cgFolders
        '
        Me.cgFolders.AllowBuildColumns = True
        Me.cgFolders.AllowEdit = False
        Me.cgFolders.AllowHorizontalScroll = False
        Me.cgFolders.AllowMultiSelect = False
        Me.cgFolders.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cgFolders.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgFolders.Appearance.Options.UseFont = True
        Me.cgFolders.AutoSizeByData = True
        Me.cgFolders.DisableAutoSize = False
        Me.cgFolders.DisableDataFormatting = False
        Me.cgFolders.FocusedRowHandle = -2147483648
        Me.cgFolders.HideFirstColumn = False
        Me.cgFolders.Location = New System.Drawing.Point(3, 3)
        Me.cgFolders.Name = "cgFolders"
        Me.cgFolders.PreviewColumn = ""
        Me.cgFolders.QueryID = Nothing
        Me.cgFolders.RowAutoHeight = False
        Me.cgFolders.SearchAsYouType = True
        Me.cgFolders.ShowAutoFilterRow = False
        Me.cgFolders.ShowFindPanel = False
        Me.cgFolders.ShowGroupByBox = False
        Me.cgFolders.ShowLoadingPanel = False
        Me.cgFolders.ShowNavigator = False
        Me.cgFolders.Size = New System.Drawing.Size(201, 486)
        Me.cgFolders.TabIndex = 2
        '
        'cgFiles
        '
        Me.cgFiles.AllowBuildColumns = True
        Me.cgFiles.AllowEdit = False
        Me.cgFiles.AllowHorizontalScroll = False
        Me.cgFiles.AllowMultiSelect = False
        Me.cgFiles.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cgFiles.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgFiles.Appearance.Options.UseFont = True
        Me.cgFiles.AutoSizeByData = True
        Me.cgFiles.DisableAutoSize = False
        Me.cgFiles.DisableDataFormatting = False
        Me.cgFiles.FocusedRowHandle = -2147483648
        Me.cgFiles.HideFirstColumn = False
        Me.cgFiles.Location = New System.Drawing.Point(210, 3)
        Me.cgFiles.Name = "cgFiles"
        Me.cgFiles.PreviewColumn = ""
        Me.cgFiles.QueryID = Nothing
        Me.cgFiles.RowAutoHeight = False
        Me.cgFiles.SearchAsYouType = True
        Me.cgFiles.ShowAutoFilterRow = False
        Me.cgFiles.ShowFindPanel = False
        Me.cgFiles.ShowGroupByBox = False
        Me.cgFiles.ShowLoadingPanel = False
        Me.cgFiles.ShowNavigator = False
        Me.cgFiles.Size = New System.Drawing.Size(152, 486)
        Me.cgFiles.TabIndex = 1
        '
        'btnWorkFile
        '
        Me.btnWorkFile.Location = New System.Drawing.Point(3, 3)
        Me.btnWorkFile.Name = "btnWorkFile"
        Me.btnWorkFile.Size = New System.Drawing.Size(120, 25)
        Me.btnWorkFile.TabIndex = 0
        Me.btnWorkFile.Text = "Open Work File"
        '
        'btnPrint
        '
        Me.btnPrint.Location = New System.Drawing.Point(129, 3)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(120, 25)
        Me.btnPrint.TabIndex = 1
        Me.btnPrint.Text = "Print Options"
        '
        'btnKeyDates
        '
        Me.btnKeyDates.Location = New System.Drawing.Point(255, 3)
        Me.btnKeyDates.Name = "btnKeyDates"
        Me.btnKeyDates.Size = New System.Drawing.Size(120, 25)
        Me.btnKeyDates.TabIndex = 2
        Me.btnKeyDates.Text = "Amend Key Dates"
        '
        'btnFunctions
        '
        Me.btnFunctions.Location = New System.Drawing.Point(381, 3)
        Me.btnFunctions.Name = "btnFunctions"
        Me.btnFunctions.Size = New System.Drawing.Size(120, 25)
        Me.btnFunctions.TabIndex = 3
        Me.btnFunctions.Text = "Functions"
        '
        'frmJobs
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1114, 611)
        Me.Controls.Add(Me.ctMain)
        Me.MaximizeBox = True
        Me.MinimumSize = New System.Drawing.Size(870, 650)
        Me.Name = "frmJobs"
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.ctMain, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.ctMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ctMain.ResumeLayout(False)
        Me.tabMain.ResumeLayout(False)
        CType(Me.GroupControl10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl10.ResumeLayout(False)
        Me.GroupControl10.PerformLayout()
        CType(Me.cbxCustTitle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCustForename.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCustSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.txtRiskSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxRiskTitle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRiskForename.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl12.ResumeLayout(False)
        Me.GroupControl12.PerformLayout()
        CType(Me.CareCheckBox1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl9.ResumeLayout(False)
        Me.GroupControl9.PerformLayout()
        CType(Me.chkTracker.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl8.ResumeLayout(False)
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl7.ResumeLayout(False)
        Me.GroupControl7.PerformLayout()
        CType(Me.cbxController.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxArea.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl6.ResumeLayout(False)
        Me.GroupControl6.PerformLayout()
        CType(Me.txtWORcd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCompCert.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDryCert.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.cbxLossAdj.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxInsurer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtInsClaim.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtInsJob.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.txtYardCount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtYardSub.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtYardTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLabCount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtVersion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtInvoiced.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtConCount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMatCount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLabSub.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSubTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtConSub.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMatSub.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLabTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGrandTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOverhead.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtConTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMatTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.cdtEnd.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSector.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabLinks.ResumeLayout(False)
        Me.tabDescriptions.ResumeLayout(False)
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.radDescTools.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radDescEst.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radDescDirections.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radDescJob.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabCalendar.ResumeLayout(False)
        Me.tabVersions.ResumeLayout(False)
        Me.tabMaterials.ResumeLayout(False)
        CType(Me.GroupControl11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl11.ResumeLayout(False)
        Me.GroupControl11.PerformLayout()
        CType(Me.radInvSummaryInvNotes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radInvInvNotes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radInvContCred.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radInvCont.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radInvSupCred.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radInvSup.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radInvAll.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabPlant.ResumeLayout(False)
        Me.tabHire.ResumeLayout(False)
        Me.tabLabour.ResumeLayout(False)
        Me.tabInvoices.ResumeLayout(False)
        Me.tabNotes.ResumeLayout(False)
        CType(Me.GroupControl13, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl13.ResumeLayout(False)
        CType(Me.txtNotes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabActivity.ResumeLayout(False)
        Me.tabPhotos.ResumeLayout(False)
        CType(Me.pic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ctMain As Care.Controls.CareTab
    Friend WithEvents tabMain As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabDescriptions As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents rich As Care.Controls.CareRichText
    Friend WithEvents tabVersions As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cgVersions As Care.Controls.CareGridWithButtons
    Friend WithEvents tabMaterials As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabLabour As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabInvoices As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabActivity As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabPhotos As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents btnKeyDates As Care.Controls.CareButton
    Friend WithEvents btnPrint As Care.Controls.CareButton
    Friend WithEvents btnWorkFile As Care.Controls.CareButton
    Friend WithEvents cgMaterials As Care.Controls.CareGrid
    Friend WithEvents cgLabour As Care.Controls.CareGrid
    Friend WithEvents cgSalesInvoices As Care.Controls.CareGridWithButtons
    Friend WithEvents cgActivity As Care.Controls.CareGrid
    Friend WithEvents cgFiles As Care.Controls.CareGrid
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents txtLabCount As Care.Controls.CareTextBox
    Friend WithEvents txtVersion As Care.Controls.CareTextBox
    Friend WithEvents txtInvoiced As Care.Controls.CareTextBox
    Friend WithEvents txtConCount As Care.Controls.CareTextBox
    Friend WithEvents txtMatCount As Care.Controls.CareTextBox
    Friend WithEvents txtLabSub As Care.Controls.CareTextBox
    Friend WithEvents txtSubTotal As Care.Controls.CareTextBox
    Friend WithEvents txtConSub As Care.Controls.CareTextBox
    Friend WithEvents txtMatSub As Care.Controls.CareTextBox
    Friend WithEvents txtLabTotal As Care.Controls.CareTextBox
    Friend WithEvents txtGrandTotal As Care.Controls.CareTextBox
    Friend WithEvents txtOverhead As Care.Controls.CareTextBox
    Friend WithEvents txtTotalTotal As Care.Controls.CareTextBox
    Friend WithEvents txtConTotal As Care.Controls.CareTextBox
    Friend WithEvents txtMatTotal As Care.Controls.CareTextBox
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtNo As Care.Controls.CareTextBox
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtInsJob As Care.Controls.CareTextBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents GroupControl9 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl8 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl7 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cbxController As Care.Controls.CareComboBox
    Friend WithEvents cbxArea As Care.Controls.CareComboBox
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents CareLabel10 As Care.Controls.CareLabel
    Friend WithEvents txtStatus As Care.Controls.CareTextBox
    Friend WithEvents GroupControl6 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents lblCompCert As Care.Controls.CareLabel
    Friend WithEvents lblDryCert As Care.Controls.CareLabel
    Friend WithEvents lblWorksOrder As Care.Controls.CareLabel
    Friend WithEvents txtWORcd As Care.Controls.CareTextBox
    Friend WithEvents txtCompCert As Care.Controls.CareTextBox
    Friend WithEvents txtDryCert As Care.Controls.CareTextBox
    Friend WithEvents cbxLossAdj As Care.Controls.CareComboBox
    Friend WithEvents cbxInsurer As Care.Controls.CareComboBox
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents CareLabel13 As Care.Controls.CareLabel
    Friend WithEvents CareLabel12 As Care.Controls.CareLabel
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents txtInsClaim As Care.Controls.CareTextBox
    Friend WithEvents CareLabel23 As Care.Controls.CareLabel
    Friend WithEvents CareLabel22 As Care.Controls.CareLabel
    Friend WithEvents CareLabel21 As Care.Controls.CareLabel
    Friend WithEvents CareLabel20 As Care.Controls.CareLabel
    Friend WithEvents CareLabel19 As Care.Controls.CareLabel
    Friend WithEvents CareLabel18 As Care.Controls.CareLabel
    Friend WithEvents cdtEnd As Care.Controls.CareDateTime
    Friend WithEvents cdtStart As Care.Controls.CareDateTime
    Friend WithEvents cbxSector As Care.Controls.CareComboBox
    Friend WithEvents btnFunctions As Care.Controls.CareButton
    Friend WithEvents pic As System.Windows.Forms.PictureBox
    Friend WithEvents cgFolders As Care.Controls.CareGrid
    Friend WithEvents GroupControl11 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents radDescTools As Care.Controls.CareRadioButton
    Friend WithEvents radDescEst As Care.Controls.CareRadioButton
    Friend WithEvents radDescDirections As Care.Controls.CareRadioButton
    Friend WithEvents radDescJob As Care.Controls.CareRadioButton
    Friend WithEvents radInvCont As Care.Controls.CareRadioButton
    Friend WithEvents radInvSupCred As Care.Controls.CareRadioButton
    Friend WithEvents radInvSup As Care.Controls.CareRadioButton
    Friend WithEvents radInvAll As Care.Controls.CareRadioButton
    Friend WithEvents CareLabel30 As Care.Controls.CareLabel
    Friend WithEvents CareLabel29 As Care.Controls.CareLabel
    Friend WithEvents CareLabel28 As Care.Controls.CareLabel
    Friend WithEvents CareLabel27 As Care.Controls.CareLabel
    Friend WithEvents radInvContCred As Care.Controls.CareRadioButton
    Friend WithEvents tabHire As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cgHire As Care.Controls.CareGrid
    Friend WithEvents lblOnHold As Care.Controls.CareLabel
    Friend WithEvents GroupControl12 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnRetentions As Care.Controls.CareButton
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents CareCheckBox1 As Care.Controls.CareCheckBox
    Friend WithEvents btnValuations As Care.Controls.CareButton
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents chkTracker As Care.Controls.CareCheckBox
    Friend WithEvents icoRetentions As Care.Shared.CareIcon
    Friend WithEvents icoValuations As Care.Shared.CareIcon
    Friend WithEvents tabPlant As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cgPlant As Care.Controls.CareGrid
    Friend WithEvents GroupControl10 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnCopyRisk As Care.Controls.CareButton
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents cbxCustTitle As Care.Controls.CareComboBox
    Friend WithEvents txtCustForename As Care.Controls.CareTextBox
    Friend WithEvents CareLabel24 As Care.Controls.CareLabel
    Friend WithEvents CareLabel25 As Care.Controls.CareLabel
    Friend WithEvents CareLabel26 As Care.Controls.CareLabel
    Friend WithEvents txtCustAddress As Care.Address.CareAddress
    Friend WithEvents txtCustSurname As Care.Controls.CareTextBox
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel31 As Care.Controls.CareLabel
    Friend WithEvents txtRiskSurname As Care.Controls.CareTextBox
    Friend WithEvents cbxRiskTitle As Care.Controls.CareComboBox
    Friend WithEvents txtRiskForename As Care.Controls.CareTextBox
    Friend WithEvents CareLabel17 As Care.Controls.CareLabel
    Friend WithEvents CareLabel16 As Care.Controls.CareLabel
    Friend WithEvents CareLabel15 As Care.Controls.CareLabel
    Friend WithEvents txtRiskAddress As Care.Address.CareAddress
    Friend WithEvents cgContacts As Care.Controls.CareGridWithButtons
    Friend WithEvents BindingSource1 As BindingSource
    Friend WithEvents btnContactBilling As Care.Controls.CareButton
    Friend WithEvents tabLinks As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cgLinked As Care.Controls.CareGridWithButtons
    Friend WithEvents radInvSummaryInvNotes As Care.Controls.CareRadioButton
    Friend WithEvents radInvInvNotes As Care.Controls.CareRadioButton
    Friend WithEvents tabNotes As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl13 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtNotes As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents btnContactPrimary As Care.Controls.CareButton
    Friend WithEvents tabCalendar As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents yvCalendar As YearView
    Friend WithEvents btnChangeStatus As Care.Controls.CareButton
    Friend WithEvents CareLabel32 As Care.Controls.CareLabel
    Friend WithEvents txtYardCount As Care.Controls.CareTextBox
    Friend WithEvents txtYardSub As Care.Controls.CareTextBox
    Friend WithEvents txtYardTotal As Care.Controls.CareTextBox
    Friend WithEvents btnMatMove As Care.Controls.CareButton
    Friend WithEvents btnMatDelete As Care.Controls.CareButton
End Class
