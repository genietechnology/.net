﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCostEnquiry
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxHeader = New DevExpress.XtraEditors.GroupControl()
        Me.radCurrent = New Care.Controls.CareRadioButton(Me.components)
        Me.cgCosts = New Care.Controls.CareGrid()
        CType(Me.gbxHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxHeader.SuspendLayout()
        CType(Me.radCurrent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'gbxHeader
        '
        Me.gbxHeader.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxHeader.Controls.Add(Me.radCurrent)
        Me.gbxHeader.Location = New System.Drawing.Point(14, 10)
        Me.gbxHeader.Name = "gbxHeader"
        Me.gbxHeader.ShowCaption = False
        Me.gbxHeader.Size = New System.Drawing.Size(736, 39)
        Me.gbxHeader.TabIndex = 2
        '
        'radCurrent
        '
        Me.radCurrent.EditValue = True
        Me.radCurrent.Location = New System.Drawing.Point(6, 9)
        Me.radCurrent.Name = "radCurrent"
        Me.radCurrent.Properties.AutoWidth = True
        Me.radCurrent.Properties.Caption = "Current Jobs"
        Me.radCurrent.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radCurrent.Properties.RadioGroupIndex = 0
        Me.radCurrent.Size = New System.Drawing.Size(84, 19)
        Me.radCurrent.TabIndex = 0
        '
        'cgCosts
        '
        Me.cgCosts.AllowBuildColumns = True
        Me.cgCosts.AllowEdit = False
        Me.cgCosts.AllowHorizontalScroll = False
        Me.cgCosts.AllowMultiSelect = False
        Me.cgCosts.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgCosts.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgCosts.Appearance.Options.UseFont = True
        Me.cgCosts.AutoSizeByData = True
        Me.cgCosts.DisableAutoSize = False
        Me.cgCosts.DisableDataFormatting = False
        Me.cgCosts.FocusedRowHandle = -2147483648
        Me.cgCosts.HideFirstColumn = False
        Me.cgCosts.Location = New System.Drawing.Point(14, 60)
        Me.cgCosts.Name = "cgCosts"
        Me.cgCosts.PreviewColumn = ""
        Me.cgCosts.QueryID = Nothing
        Me.cgCosts.RowAutoHeight = False
        Me.cgCosts.SearchAsYouType = True
        Me.cgCosts.ShowAutoFilterRow = False
        Me.cgCosts.ShowFindPanel = True
        Me.cgCosts.ShowGroupByBox = False
        Me.cgCosts.ShowLoadingPanel = False
        Me.cgCosts.ShowNavigator = True
        Me.cgCosts.Size = New System.Drawing.Size(736, 487)
        Me.cgCosts.TabIndex = 3
        '
        'frmCostEnquiry
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(764, 561)
        Me.Controls.Add(Me.cgCosts)
        Me.Controls.Add(Me.gbxHeader)
        Me.Margin = New System.Windows.Forms.Padding(3, 5, 3, 5)
        Me.Name = "frmCostEnquiry"
        Me.Text = "frmJobCosts"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.gbxHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxHeader.ResumeLayout(False)
        CType(Me.radCurrent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents gbxHeader As DevExpress.XtraEditors.GroupControl
    Friend WithEvents radCurrent As Care.Controls.CareRadioButton
    Friend WithEvents cgCosts As Care.Controls.CareGrid
End Class
