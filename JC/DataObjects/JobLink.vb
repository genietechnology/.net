﻿'*****************************************************
'Generated 02/03/2017 21:16:04 using Version 1.15.8.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class JobLink
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_JobId As Guid?
        Dim m_LinkedJobId As Guid?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._JobId = Nothing
                ._LinkedJobId = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._JobId = Nothing
                ._LinkedJobId = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@JobId")> _
        Public Property _JobId() As Guid?
            Get
                Return m_JobId
            End Get
            Set(ByVal value As Guid?)
                m_JobId = value
            End Set
        End Property

        <StoredProcParameter("@LinkedJobId")> _
        Public Property _LinkedJobId() As Guid?
            Get
                Return m_LinkedJobId
            End Get
            Set(ByVal value As Guid?)
                m_LinkedJobId = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As JobLink

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getJobLinkbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of JobLink)

            Dim _JobLinkList As List(Of JobLink)
            _JobLinkList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getJobLinkTable"))
            Return _JobLinkList

        End Function

        Public Shared Sub SaveAll(ByVal JobLinkList As List(Of JobLink))

            For Each _JobLink As JobLink In JobLinkList
                SaveRecord(_JobLink)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal JobLink As JobLink) As Guid
            JobLink.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, JobLink, "upsertJobLink")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteJobLinkbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertJobLink")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As JobLink

            Dim _J As JobLink = Nothing

            If DR IsNot Nothing Then
                _J = New JobLink()
                With DR
                    _J.IsNew = False
                    _J.IsDeleted = False
                    _J._ID = GetGUID(DR("ID"))
                    _J._JobId = GetGUID(DR("job_id"))
                    _J._LinkedJobId = GetGUID(DR("linked_job_id"))

                End With
            End If

            Return _J

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of JobLink)

            Dim _JobLinkList As New List(Of JobLink)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _JobLinkList.Add(PropertiesFromData(_DR))
            Next

            Return _JobLinkList

        End Function


#End Region

    End Class

End Namespace
