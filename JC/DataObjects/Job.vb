﻿'*****************************************************
'Generated 24/11/2017 08:08:49 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Job
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_JobNo As Integer
        Dim m_JobStatusCode As String
        Dim m_JobStatus As String
        Dim m_JobStart As Date?
        Dim m_JobEnd As Date?
        Dim m_Sector As String
        Dim m_Area As String
        Dim m_ContCode As String
        Dim m_ContName As String
        Dim m_LossAdj As String
        Dim m_InsCompany As String
        Dim m_InsRef As String
        Dim m_InsClaim As String
        Dim m_DescJob As String
        Dim m_DescDirections As String
        Dim m_DescEst As String
        Dim m_DescTools As String
        Dim m_CusId As Guid?
        Dim m_CusRef As String
        Dim m_CusTitle As String
        Dim m_CusInits As String
        Dim m_CusForename As String
        Dim m_CusSurname As String
        Dim m_CusFullname As String
        Dim m_CusKnownas As String
        Dim m_CusAddress As String
        Dim m_CusAddressLine As String
        Dim m_CusAddress1 As String
        Dim m_CusPostcode As String
        Dim m_CusCoordinates As String
        Dim m_CusTels As String
        Dim m_RskTitle As String
        Dim m_RskInits As String
        Dim m_RskForename As String
        Dim m_RskSurname As String
        Dim m_RskFullname As String
        Dim m_RskKnownas As String
        Dim m_RskAddress As String
        Dim m_RskAddressLine As String
        Dim m_RskAddress1 As String
        Dim m_RskPostcode As String
        Dim m_RskCoordinates As String
        Dim m_RskTels As String
        Dim m_AllTels As String
        Dim m_KeySurvey As Date?
        Dim m_KeyEstsub As Date?
        Dim m_KeyWork As Date?
        Dim m_KeyApproval As Date?
        Dim m_KeyPaid As Date?
        Dim m_KeyWorksorder As Date?
        Dim m_KeyCcSent As Date?
        Dim m_KeyCcRcd As Date?
        Dim m_Dc As Boolean
        Dim m_DcAskUser As String
        Dim m_DcAskStamp As DateTime?
        Dim m_DcRecd As String
        Dim m_DcRecdUser As String
        Dim m_DcRecdStamp As DateTime?
        Dim m_Retention As Boolean
        Dim m_RetentionDle As Date?
        Dim m_Valuation As Boolean
        Dim m_ValuationReminder As Date?
        Dim m_MupMat As Decimal
        Dim m_MupLab As Decimal
        Dim m_MupSub As Decimal
        Dim m_MupLeak As Decimal
        Dim m_Hold As Boolean
        Dim m_HoldDate As Date?
        Dim m_HoldReason As String
        Dim m_Hide As Boolean
        Dim m_JobFolder As String
        Dim m_Notes As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._JobNo = 0
                ._JobStatusCode = ""
                ._JobStatus = ""
                ._JobStart = Nothing
                ._JobEnd = Nothing
                ._Sector = ""
                ._Area = ""
                ._ContCode = ""
                ._ContName = ""
                ._LossAdj = ""
                ._InsCompany = ""
                ._InsRef = ""
                ._InsClaim = ""
                ._DescJob = ""
                ._DescDirections = ""
                ._DescEst = ""
                ._DescTools = ""
                ._CusId = Nothing
                ._CusRef = ""
                ._CusTitle = ""
                ._CusInits = ""
                ._CusForename = ""
                ._CusSurname = ""
                ._CusFullname = ""
                ._CusKnownas = ""
                ._CusAddress = ""
                ._CusAddressLine = ""
                ._CusAddress1 = ""
                ._CusPostcode = ""
                ._CusCoordinates = ""
                ._CusTels = ""
                ._RskTitle = ""
                ._RskInits = ""
                ._RskForename = ""
                ._RskSurname = ""
                ._RskFullname = ""
                ._RskKnownas = ""
                ._RskAddress = ""
                ._RskAddressLine = ""
                ._RskAddress1 = ""
                ._RskPostcode = ""
                ._RskCoordinates = ""
                ._RskTels = ""
                ._AllTels = ""
                ._KeySurvey = Nothing
                ._KeyEstsub = Nothing
                ._KeyWork = Nothing
                ._KeyApproval = Nothing
                ._KeyPaid = Nothing
                ._KeyWorksorder = Nothing
                ._KeyCcSent = Nothing
                ._KeyCcRcd = Nothing
                ._Dc = False
                ._DcAskUser = ""
                ._DcAskStamp = Nothing
                ._DcRecd = ""
                ._DcRecdUser = ""
                ._DcRecdStamp = Nothing
                ._Retention = False
                ._RetentionDle = Nothing
                ._Valuation = False
                ._ValuationReminder = Nothing
                ._MupMat = 0
                ._MupLab = 0
                ._MupSub = 0
                ._MupLeak = 0
                ._Hold = False
                ._HoldDate = Nothing
                ._HoldReason = ""
                ._Hide = False
                ._JobFolder = ""
                ._Notes = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._JobNo = 0
                ._JobStatusCode = ""
                ._JobStatus = ""
                ._JobStart = Nothing
                ._JobEnd = Nothing
                ._Sector = ""
                ._Area = ""
                ._ContCode = ""
                ._ContName = ""
                ._LossAdj = ""
                ._InsCompany = ""
                ._InsRef = ""
                ._InsClaim = ""
                ._DescJob = ""
                ._DescDirections = ""
                ._DescEst = ""
                ._DescTools = ""
                ._CusId = Nothing
                ._CusRef = ""
                ._CusTitle = ""
                ._CusInits = ""
                ._CusForename = ""
                ._CusSurname = ""
                ._CusFullname = ""
                ._CusKnownas = ""
                ._CusAddress = ""
                ._CusAddressLine = ""
                ._CusAddress1 = ""
                ._CusPostcode = ""
                ._CusCoordinates = ""
                ._CusTels = ""
                ._RskTitle = ""
                ._RskInits = ""
                ._RskForename = ""
                ._RskSurname = ""
                ._RskFullname = ""
                ._RskKnownas = ""
                ._RskAddress = ""
                ._RskAddressLine = ""
                ._RskAddress1 = ""
                ._RskPostcode = ""
                ._RskCoordinates = ""
                ._RskTels = ""
                ._AllTels = ""
                ._KeySurvey = Nothing
                ._KeyEstsub = Nothing
                ._KeyWork = Nothing
                ._KeyApproval = Nothing
                ._KeyPaid = Nothing
                ._KeyWorksorder = Nothing
                ._KeyCcSent = Nothing
                ._KeyCcRcd = Nothing
                ._Dc = False
                ._DcAskUser = ""
                ._DcAskStamp = Nothing
                ._DcRecd = ""
                ._DcRecdUser = ""
                ._DcRecdStamp = Nothing
                ._Retention = False
                ._RetentionDle = Nothing
                ._Valuation = False
                ._ValuationReminder = Nothing
                ._MupMat = 0
                ._MupLab = 0
                ._MupSub = 0
                ._MupLeak = 0
                ._Hold = False
                ._HoldDate = Nothing
                ._HoldReason = ""
                ._Hide = False
                ._JobFolder = ""
                ._Notes = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@JobNo")>
        Public Property _JobNo() As Integer
            Get
                Return m_JobNo
            End Get
            Set(ByVal value As Integer)
                m_JobNo = value
            End Set
        End Property

        <StoredProcParameter("@JobStatusCode")>
        Public Property _JobStatusCode() As String
            Get
                Return m_JobStatusCode
            End Get
            Set(ByVal value As String)
                m_JobStatusCode = value
            End Set
        End Property

        <StoredProcParameter("@JobStatus")>
        Public Property _JobStatus() As String
            Get
                Return m_JobStatus
            End Get
            Set(ByVal value As String)
                m_JobStatus = value
            End Set
        End Property

        <StoredProcParameter("@JobStart")>
        Public Property _JobStart() As Date?
            Get
                Return m_JobStart
            End Get
            Set(ByVal value As Date?)
                m_JobStart = value
            End Set
        End Property

        <StoredProcParameter("@JobEnd")>
        Public Property _JobEnd() As Date?
            Get
                Return m_JobEnd
            End Get
            Set(ByVal value As Date?)
                m_JobEnd = value
            End Set
        End Property

        <StoredProcParameter("@Sector")>
        Public Property _Sector() As String
            Get
                Return m_Sector
            End Get
            Set(ByVal value As String)
                m_Sector = value
            End Set
        End Property

        <StoredProcParameter("@Area")>
        Public Property _Area() As String
            Get
                Return m_Area
            End Get
            Set(ByVal value As String)
                m_Area = value
            End Set
        End Property

        <StoredProcParameter("@ContCode")>
        Public Property _ContCode() As String
            Get
                Return m_ContCode
            End Get
            Set(ByVal value As String)
                m_ContCode = value
            End Set
        End Property

        <StoredProcParameter("@ContName")>
        Public Property _ContName() As String
            Get
                Return m_ContName
            End Get
            Set(ByVal value As String)
                m_ContName = value
            End Set
        End Property

        <StoredProcParameter("@LossAdj")>
        Public Property _LossAdj() As String
            Get
                Return m_LossAdj
            End Get
            Set(ByVal value As String)
                m_LossAdj = value
            End Set
        End Property

        <StoredProcParameter("@InsCompany")>
        Public Property _InsCompany() As String
            Get
                Return m_InsCompany
            End Get
            Set(ByVal value As String)
                m_InsCompany = value
            End Set
        End Property

        <StoredProcParameter("@InsRef")>
        Public Property _InsRef() As String
            Get
                Return m_InsRef
            End Get
            Set(ByVal value As String)
                m_InsRef = value
            End Set
        End Property

        <StoredProcParameter("@InsClaim")>
        Public Property _InsClaim() As String
            Get
                Return m_InsClaim
            End Get
            Set(ByVal value As String)
                m_InsClaim = value
            End Set
        End Property

        <StoredProcParameter("@DescJob")>
        Public Property _DescJob() As String
            Get
                Return m_DescJob
            End Get
            Set(ByVal value As String)
                m_DescJob = value
            End Set
        End Property

        <StoredProcParameter("@DescDirections")>
        Public Property _DescDirections() As String
            Get
                Return m_DescDirections
            End Get
            Set(ByVal value As String)
                m_DescDirections = value
            End Set
        End Property

        <StoredProcParameter("@DescEst")>
        Public Property _DescEst() As String
            Get
                Return m_DescEst
            End Get
            Set(ByVal value As String)
                m_DescEst = value
            End Set
        End Property

        <StoredProcParameter("@DescTools")>
        Public Property _DescTools() As String
            Get
                Return m_DescTools
            End Get
            Set(ByVal value As String)
                m_DescTools = value
            End Set
        End Property

        <StoredProcParameter("@CusId")>
        Public Property _CusId() As Guid?
            Get
                Return m_CusId
            End Get
            Set(ByVal value As Guid?)
                m_CusId = value
            End Set
        End Property

        <StoredProcParameter("@CusRef")>
        Public Property _CusRef() As String
            Get
                Return m_CusRef
            End Get
            Set(ByVal value As String)
                m_CusRef = value
            End Set
        End Property

        <StoredProcParameter("@CusTitle")>
        Public Property _CusTitle() As String
            Get
                Return m_CusTitle
            End Get
            Set(ByVal value As String)
                m_CusTitle = value
            End Set
        End Property

        <StoredProcParameter("@CusInits")>
        Public Property _CusInits() As String
            Get
                Return m_CusInits
            End Get
            Set(ByVal value As String)
                m_CusInits = value
            End Set
        End Property

        <StoredProcParameter("@CusForename")>
        Public Property _CusForename() As String
            Get
                Return m_CusForename
            End Get
            Set(ByVal value As String)
                m_CusForename = value
            End Set
        End Property

        <StoredProcParameter("@CusSurname")>
        Public Property _CusSurname() As String
            Get
                Return m_CusSurname
            End Get
            Set(ByVal value As String)
                m_CusSurname = value
            End Set
        End Property

        <StoredProcParameter("@CusFullname")>
        Public Property _CusFullname() As String
            Get
                Return m_CusFullname
            End Get
            Set(ByVal value As String)
                m_CusFullname = value
            End Set
        End Property

        <StoredProcParameter("@CusKnownas")>
        Public Property _CusKnownas() As String
            Get
                Return m_CusKnownas
            End Get
            Set(ByVal value As String)
                m_CusKnownas = value
            End Set
        End Property

        <StoredProcParameter("@CusAddress")>
        Public Property _CusAddress() As String
            Get
                Return m_CusAddress
            End Get
            Set(ByVal value As String)
                m_CusAddress = value
            End Set
        End Property

        <StoredProcParameter("@CusAddressLine")>
        Public Property _CusAddressLine() As String
            Get
                Return m_CusAddressLine
            End Get
            Set(ByVal value As String)
                m_CusAddressLine = value
            End Set
        End Property

        <StoredProcParameter("@CusAddress1")>
        Public Property _CusAddress1() As String
            Get
                Return m_CusAddress1
            End Get
            Set(ByVal value As String)
                m_CusAddress1 = value
            End Set
        End Property

        <StoredProcParameter("@CusPostcode")>
        Public Property _CusPostcode() As String
            Get
                Return m_CusPostcode
            End Get
            Set(ByVal value As String)
                m_CusPostcode = value
            End Set
        End Property

        <StoredProcParameter("@CusCoordinates")>
        Public Property _CusCoordinates() As String
            Get
                Return m_CusCoordinates
            End Get
            Set(ByVal value As String)
                m_CusCoordinates = value
            End Set
        End Property

        <StoredProcParameter("@CusTels")>
        Public Property _CusTels() As String
            Get
                Return m_CusTels
            End Get
            Set(ByVal value As String)
                m_CusTels = value
            End Set
        End Property

        <StoredProcParameter("@RskTitle")>
        Public Property _RskTitle() As String
            Get
                Return m_RskTitle
            End Get
            Set(ByVal value As String)
                m_RskTitle = value
            End Set
        End Property

        <StoredProcParameter("@RskInits")>
        Public Property _RskInits() As String
            Get
                Return m_RskInits
            End Get
            Set(ByVal value As String)
                m_RskInits = value
            End Set
        End Property

        <StoredProcParameter("@RskForename")>
        Public Property _RskForename() As String
            Get
                Return m_RskForename
            End Get
            Set(ByVal value As String)
                m_RskForename = value
            End Set
        End Property

        <StoredProcParameter("@RskSurname")>
        Public Property _RskSurname() As String
            Get
                Return m_RskSurname
            End Get
            Set(ByVal value As String)
                m_RskSurname = value
            End Set
        End Property

        <StoredProcParameter("@RskFullname")>
        Public Property _RskFullname() As String
            Get
                Return m_RskFullname
            End Get
            Set(ByVal value As String)
                m_RskFullname = value
            End Set
        End Property

        <StoredProcParameter("@RskKnownas")>
        Public Property _RskKnownas() As String
            Get
                Return m_RskKnownas
            End Get
            Set(ByVal value As String)
                m_RskKnownas = value
            End Set
        End Property

        <StoredProcParameter("@RskAddress")>
        Public Property _RskAddress() As String
            Get
                Return m_RskAddress
            End Get
            Set(ByVal value As String)
                m_RskAddress = value
            End Set
        End Property

        <StoredProcParameter("@RskAddressLine")>
        Public Property _RskAddressLine() As String
            Get
                Return m_RskAddressLine
            End Get
            Set(ByVal value As String)
                m_RskAddressLine = value
            End Set
        End Property

        <StoredProcParameter("@RskAddress1")>
        Public Property _RskAddress1() As String
            Get
                Return m_RskAddress1
            End Get
            Set(ByVal value As String)
                m_RskAddress1 = value
            End Set
        End Property

        <StoredProcParameter("@RskPostcode")>
        Public Property _RskPostcode() As String
            Get
                Return m_RskPostcode
            End Get
            Set(ByVal value As String)
                m_RskPostcode = value
            End Set
        End Property

        <StoredProcParameter("@RskCoordinates")>
        Public Property _RskCoordinates() As String
            Get
                Return m_RskCoordinates
            End Get
            Set(ByVal value As String)
                m_RskCoordinates = value
            End Set
        End Property

        <StoredProcParameter("@RskTels")>
        Public Property _RskTels() As String
            Get
                Return m_RskTels
            End Get
            Set(ByVal value As String)
                m_RskTels = value
            End Set
        End Property

        <StoredProcParameter("@AllTels")>
        Public Property _AllTels() As String
            Get
                Return m_AllTels
            End Get
            Set(ByVal value As String)
                m_AllTels = value
            End Set
        End Property

        <StoredProcParameter("@KeySurvey")>
        Public Property _KeySurvey() As Date?
            Get
                Return m_KeySurvey
            End Get
            Set(ByVal value As Date?)
                m_KeySurvey = value
            End Set
        End Property

        <StoredProcParameter("@KeyEstsub")>
        Public Property _KeyEstsub() As Date?
            Get
                Return m_KeyEstsub
            End Get
            Set(ByVal value As Date?)
                m_KeyEstsub = value
            End Set
        End Property

        <StoredProcParameter("@KeyWork")>
        Public Property _KeyWork() As Date?
            Get
                Return m_KeyWork
            End Get
            Set(ByVal value As Date?)
                m_KeyWork = value
            End Set
        End Property

        <StoredProcParameter("@KeyApproval")>
        Public Property _KeyApproval() As Date?
            Get
                Return m_KeyApproval
            End Get
            Set(ByVal value As Date?)
                m_KeyApproval = value
            End Set
        End Property

        <StoredProcParameter("@KeyPaid")>
        Public Property _KeyPaid() As Date?
            Get
                Return m_KeyPaid
            End Get
            Set(ByVal value As Date?)
                m_KeyPaid = value
            End Set
        End Property

        <StoredProcParameter("@KeyWorksorder")>
        Public Property _KeyWorksorder() As Date?
            Get
                Return m_KeyWorksorder
            End Get
            Set(ByVal value As Date?)
                m_KeyWorksorder = value
            End Set
        End Property

        <StoredProcParameter("@KeyCcSent")>
        Public Property _KeyCcSent() As Date?
            Get
                Return m_KeyCcSent
            End Get
            Set(ByVal value As Date?)
                m_KeyCcSent = value
            End Set
        End Property

        <StoredProcParameter("@KeyCcRcd")>
        Public Property _KeyCcRcd() As Date?
            Get
                Return m_KeyCcRcd
            End Get
            Set(ByVal value As Date?)
                m_KeyCcRcd = value
            End Set
        End Property

        <StoredProcParameter("@Dc")>
        Public Property _Dc() As Boolean
            Get
                Return m_Dc
            End Get
            Set(ByVal value As Boolean)
                m_Dc = value
            End Set
        End Property

        <StoredProcParameter("@DcAskUser")>
        Public Property _DcAskUser() As String
            Get
                Return m_DcAskUser
            End Get
            Set(ByVal value As String)
                m_DcAskUser = value
            End Set
        End Property

        <StoredProcParameter("@DcAskStamp")>
        Public Property _DcAskStamp() As DateTime?
            Get
                Return m_DcAskStamp
            End Get
            Set(ByVal value As DateTime?)
                m_DcAskStamp = value
            End Set
        End Property

        <StoredProcParameter("@DcRecd")>
        Public Property _DcRecd() As String
            Get
                Return m_DcRecd
            End Get
            Set(ByVal value As String)
                m_DcRecd = value
            End Set
        End Property

        <StoredProcParameter("@DcRecdUser")>
        Public Property _DcRecdUser() As String
            Get
                Return m_DcRecdUser
            End Get
            Set(ByVal value As String)
                m_DcRecdUser = value
            End Set
        End Property

        <StoredProcParameter("@DcRecdStamp")>
        Public Property _DcRecdStamp() As DateTime?
            Get
                Return m_DcRecdStamp
            End Get
            Set(ByVal value As DateTime?)
                m_DcRecdStamp = value
            End Set
        End Property

        <StoredProcParameter("@Retention")>
        Public Property _Retention() As Boolean
            Get
                Return m_Retention
            End Get
            Set(ByVal value As Boolean)
                m_Retention = value
            End Set
        End Property

        <StoredProcParameter("@RetentionDle")>
        Public Property _RetentionDle() As Date?
            Get
                Return m_RetentionDle
            End Get
            Set(ByVal value As Date?)
                m_RetentionDle = value
            End Set
        End Property

        <StoredProcParameter("@Valuation")>
        Public Property _Valuation() As Boolean
            Get
                Return m_Valuation
            End Get
            Set(ByVal value As Boolean)
                m_Valuation = value
            End Set
        End Property

        <StoredProcParameter("@ValuationReminder")>
        Public Property _ValuationReminder() As Date?
            Get
                Return m_ValuationReminder
            End Get
            Set(ByVal value As Date?)
                m_ValuationReminder = value
            End Set
        End Property

        <StoredProcParameter("@MupMat")>
        Public Property _MupMat() As Decimal
            Get
                Return m_MupMat
            End Get
            Set(ByVal value As Decimal)
                m_MupMat = value
            End Set
        End Property

        <StoredProcParameter("@MupLab")>
        Public Property _MupLab() As Decimal
            Get
                Return m_MupLab
            End Get
            Set(ByVal value As Decimal)
                m_MupLab = value
            End Set
        End Property

        <StoredProcParameter("@MupSub")>
        Public Property _MupSub() As Decimal
            Get
                Return m_MupSub
            End Get
            Set(ByVal value As Decimal)
                m_MupSub = value
            End Set
        End Property

        <StoredProcParameter("@MupLeak")>
        Public Property _MupLeak() As Decimal
            Get
                Return m_MupLeak
            End Get
            Set(ByVal value As Decimal)
                m_MupLeak = value
            End Set
        End Property

        <StoredProcParameter("@Hold")>
        Public Property _Hold() As Boolean
            Get
                Return m_Hold
            End Get
            Set(ByVal value As Boolean)
                m_Hold = value
            End Set
        End Property

        <StoredProcParameter("@HoldDate")>
        Public Property _HoldDate() As Date?
            Get
                Return m_HoldDate
            End Get
            Set(ByVal value As Date?)
                m_HoldDate = value
            End Set
        End Property

        <StoredProcParameter("@HoldReason")>
        Public Property _HoldReason() As String
            Get
                Return m_HoldReason
            End Get
            Set(ByVal value As String)
                m_HoldReason = value
            End Set
        End Property

        <StoredProcParameter("@Hide")>
        Public Property _Hide() As Boolean
            Get
                Return m_Hide
            End Get
            Set(ByVal value As Boolean)
                m_Hide = value
            End Set
        End Property

        <StoredProcParameter("@JobFolder")>
        Public Property _JobFolder() As String
            Get
                Return m_JobFolder
            End Get
            Set(ByVal value As String)
                m_JobFolder = value
            End Set
        End Property

        <StoredProcParameter("@Notes")>
        Public Property _Notes() As String
            Get
                Return m_Notes
            End Get
            Set(ByVal value As String)
                m_Notes = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Job

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getJobbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Job

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getJobbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Job)

            Dim _JobList As List(Of Job)
            _JobList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getJobTable"))
            Return _JobList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Job)

            Dim _JobList As List(Of Job)
            _JobList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getJobTable"))
            Return _JobList

        End Function

        Public Shared Sub SaveAll(ByVal JobList As List(Of Job))

            For Each _Job As Job In JobList
                SaveRecord(_Job)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal JobList As List(Of Job))

            For Each _Job As Job In JobList
                SaveRecord(ConnectionString, _Job)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Job As Job) As Guid
            Job.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Job, "upsertJob")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Job As Job) As Guid
            Job.SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Job, "upsertJob")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteJobbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteJobbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertJob")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Me, "upsertJob")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Job

            Dim _J As Job = Nothing

            If DR IsNot Nothing Then
                _J = New Job()
                With DR
                    _J.IsNew = False
                    _J.IsDeleted = False
                    _J._ID = GetGUID(DR("ID"))
                    _J._JobNo = GetInteger(DR("job_no"))
                    _J._JobStatusCode = DR("job_status_code").ToString.Trim
                    _J._JobStatus = DR("job_status").ToString.Trim
                    _J._JobStart = GetDate(DR("job_start"))
                    _J._JobEnd = GetDate(DR("job_end"))
                    _J._Sector = DR("sector").ToString.Trim
                    _J._Area = DR("area").ToString.Trim
                    _J._ContCode = DR("cont_code").ToString.Trim
                    _J._ContName = DR("cont_name").ToString.Trim
                    _J._LossAdj = DR("loss_adj").ToString.Trim
                    _J._InsCompany = DR("ins_company").ToString.Trim
                    _J._InsRef = DR("ins_ref").ToString.Trim
                    _J._InsClaim = DR("ins_claim").ToString.Trim
                    _J._DescJob = DR("desc_job").ToString.Trim
                    _J._DescDirections = DR("desc_directions").ToString.Trim
                    _J._DescEst = DR("desc_est").ToString.Trim
                    _J._DescTools = DR("desc_tools").ToString.Trim
                    _J._CusId = GetGUID(DR("cus_id"))
                    _J._CusRef = DR("cus_ref").ToString.Trim
                    _J._CusTitle = DR("cus_title").ToString.Trim
                    _J._CusInits = DR("cus_inits").ToString.Trim
                    _J._CusForename = DR("cus_forename").ToString.Trim
                    _J._CusSurname = DR("cus_surname").ToString.Trim
                    _J._CusFullname = DR("cus_fullname").ToString.Trim
                    _J._CusKnownas = DR("cus_knownas").ToString.Trim
                    _J._CusAddress = DR("cus_address").ToString.Trim
                    _J._CusAddressLine = DR("cus_address_line").ToString.Trim
                    _J._CusAddress1 = DR("cus_address_1").ToString.Trim
                    _J._CusPostcode = DR("cus_postcode").ToString.Trim
                    _J._CusCoordinates = DR("cus_coordinates").ToString.Trim
                    _J._CusTels = DR("cus_tels").ToString.Trim
                    _J._RskTitle = DR("rsk_title").ToString.Trim
                    _J._RskInits = DR("rsk_inits").ToString.Trim
                    _J._RskForename = DR("rsk_forename").ToString.Trim
                    _J._RskSurname = DR("rsk_surname").ToString.Trim
                    _J._RskFullname = DR("rsk_fullname").ToString.Trim
                    _J._RskKnownas = DR("rsk_knownas").ToString.Trim
                    _J._RskAddress = DR("rsk_address").ToString.Trim
                    _J._RskAddressLine = DR("rsk_address_line").ToString.Trim
                    _J._RskAddress1 = DR("rsk_address_1").ToString.Trim
                    _J._RskPostcode = DR("rsk_postcode").ToString.Trim
                    _J._RskCoordinates = DR("rsk_coordinates").ToString.Trim
                    _J._RskTels = DR("rsk_tels").ToString.Trim
                    _J._AllTels = DR("all_tels").ToString.Trim
                    _J._KeySurvey = GetDate(DR("key_survey"))
                    _J._KeyEstsub = GetDate(DR("key_estsub"))
                    _J._KeyWork = GetDate(DR("key_work"))
                    _J._KeyApproval = GetDate(DR("key_approval"))
                    _J._KeyPaid = GetDate(DR("key_paid"))
                    _J._KeyWorksorder = GetDate(DR("key_worksorder"))
                    _J._KeyCcSent = GetDate(DR("key_cc_sent"))
                    _J._KeyCcRcd = GetDate(DR("key_cc_rcd"))
                    _J._Dc = GetBoolean(DR("dc"))
                    _J._DcAskUser = DR("dc_ask_user").ToString.Trim
                    _J._DcAskStamp = GetDateTime(DR("dc_ask_stamp"))
                    _J._DcRecd = DR("dc_recd").ToString.Trim
                    _J._DcRecdUser = DR("dc_recd_user").ToString.Trim
                    _J._DcRecdStamp = GetDateTime(DR("dc_recd_stamp"))
                    _J._Retention = GetBoolean(DR("retention"))
                    _J._RetentionDle = GetDate(DR("retention_dle"))
                    _J._Valuation = GetBoolean(DR("valuation"))
                    _J._ValuationReminder = GetDate(DR("valuation_reminder"))
                    _J._MupMat = GetDecimal(DR("mup_mat"))
                    _J._MupLab = GetDecimal(DR("mup_lab"))
                    _J._MupSub = GetDecimal(DR("mup_sub"))
                    _J._MupLeak = GetDecimal(DR("mup_leak"))
                    _J._Hold = GetBoolean(DR("hold"))
                    _J._HoldDate = GetDate(DR("hold_date"))
                    _J._HoldReason = DR("hold_reason").ToString.Trim
                    _J._Hide = GetBoolean(DR("hide"))
                    _J._JobFolder = DR("job_folder").ToString.Trim
                    _J._Notes = DR("notes").ToString.Trim

                End With
            End If

            Return _J

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Job)

            Dim _JobList As New List(Of Job)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _JobList.Add(PropertiesFromData(_DR))
            Next

            Return _JobList

        End Function


#End Region

    End Class

End Namespace
