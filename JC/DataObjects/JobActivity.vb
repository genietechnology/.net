﻿'*****************************************************
'Generated 02/03/2017 20:53:17 using Version 1.15.8.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class JobActivity
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_JobId As Guid?
        Dim m_Activity As String
        Dim m_KeyDate As Date?
        Dim m_UserCode As String
        Dim m_UserName As String
        Dim m_Stamp As DateTime?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._JobId = Nothing
                ._Activity = ""
                ._KeyDate = Nothing
                ._UserCode = ""
                ._UserName = ""
                ._Stamp = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._JobId = Nothing
                ._Activity = ""
                ._KeyDate = Nothing
                ._UserCode = ""
                ._UserName = ""
                ._Stamp = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@JobId")> _
        Public Property _JobId() As Guid?
            Get
                Return m_JobId
            End Get
            Set(ByVal value As Guid?)
                m_JobId = value
            End Set
        End Property

        <StoredProcParameter("@Activity")> _
        Public Property _Activity() As String
            Get
                Return m_Activity
            End Get
            Set(ByVal value As String)
                m_Activity = value
            End Set
        End Property

        <StoredProcParameter("@KeyDate")> _
        Public Property _KeyDate() As Date?
            Get
                Return m_KeyDate
            End Get
            Set(ByVal value As Date?)
                m_KeyDate = value
            End Set
        End Property

        <StoredProcParameter("@UserCode")> _
        Public Property _UserCode() As String
            Get
                Return m_UserCode
            End Get
            Set(ByVal value As String)
                m_UserCode = value
            End Set
        End Property

        <StoredProcParameter("@UserName")> _
        Public Property _UserName() As String
            Get
                Return m_UserName
            End Get
            Set(ByVal value As String)
                m_UserName = value
            End Set
        End Property

        <StoredProcParameter("@Stamp")> _
        Public Property _Stamp() As DateTime?
            Get
                Return m_Stamp
            End Get
            Set(ByVal value As DateTime?)
                m_Stamp = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As JobActivity

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getJobActivitybyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of JobActivity)

            Dim _JobActivityList As List(Of JobActivity)
            _JobActivityList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getJobActivityTable"))
            Return _JobActivityList

        End Function

        Public Shared Sub SaveAll(ByVal JobActivityList As List(Of JobActivity))

            For Each _JobActivity As JobActivity In JobActivityList
                SaveRecord(_JobActivity)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal JobActivity As JobActivity) As Guid
            JobActivity.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, JobActivity, "upsertJobActivity")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteJobActivitybyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertJobActivity")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As JobActivity

            Dim _J As JobActivity = Nothing

            If DR IsNot Nothing Then
                _J = New JobActivity()
                With DR
                    _J.IsNew = False
                    _J.IsDeleted = False
                    _J._ID = GetGUID(DR("ID"))
                    _J._JobId = GetGUID(DR("job_id"))
                    _J._Activity = DR("activity").ToString.Trim
                    _J._KeyDate = GetDate(DR("key_date"))
                    _J._UserCode = DR("user_code").ToString.Trim
                    _J._UserName = DR("user_name").ToString.Trim
                    _J._Stamp = GetDateTime(DR("stamp"))

                End With
            End If

            Return _J

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of JobActivity)

            Dim _JobActivityList As New List(Of JobActivity)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _JobActivityList.Add(PropertiesFromData(_DR))
            Next

            Return _JobActivityList

        End Function


#End Region

    End Class

End Namespace
