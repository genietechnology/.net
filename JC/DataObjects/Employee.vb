﻿'*****************************************************
'Generated 11/09/2018 22:07:55 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Employee
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Code As String
        Dim m_Fullname As String
        Dim m_Nickname As String
        Dim m_TradeCode As String
        Dim m_TradeDesc As String
        Dim m_Rate As Decimal
        Dim m_Active As Boolean
        Dim m_Office As Boolean
        Dim m_TelMobile As String
        Dim m_Email As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Code = ""
                ._Fullname = ""
                ._Nickname = ""
                ._TradeCode = ""
                ._TradeDesc = ""
                ._Rate = 0
                ._Active = False
                ._Office = False
                ._TelMobile = ""
                ._Email = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Code = ""
                ._Fullname = ""
                ._Nickname = ""
                ._TradeCode = ""
                ._TradeDesc = ""
                ._Rate = 0
                ._Active = False
                ._Office = False
                ._TelMobile = ""
                ._Email = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Code")>
        Public Property _Code() As String
            Get
                Return m_Code
            End Get
            Set(ByVal value As String)
                m_Code = value
            End Set
        End Property

        <StoredProcParameter("@Fullname")>
        Public Property _Fullname() As String
            Get
                Return m_Fullname
            End Get
            Set(ByVal value As String)
                m_Fullname = value
            End Set
        End Property

        <StoredProcParameter("@Nickname")>
        Public Property _Nickname() As String
            Get
                Return m_Nickname
            End Get
            Set(ByVal value As String)
                m_Nickname = value
            End Set
        End Property

        <StoredProcParameter("@TradeCode")>
        Public Property _TradeCode() As String
            Get
                Return m_TradeCode
            End Get
            Set(ByVal value As String)
                m_TradeCode = value
            End Set
        End Property

        <StoredProcParameter("@TradeDesc")>
        Public Property _TradeDesc() As String
            Get
                Return m_TradeDesc
            End Get
            Set(ByVal value As String)
                m_TradeDesc = value
            End Set
        End Property

        <StoredProcParameter("@Rate")>
        Public Property _Rate() As Decimal
            Get
                Return m_Rate
            End Get
            Set(ByVal value As Decimal)
                m_Rate = value
            End Set
        End Property

        <StoredProcParameter("@Active")>
        Public Property _Active() As Boolean
            Get
                Return m_Active
            End Get
            Set(ByVal value As Boolean)
                m_Active = value
            End Set
        End Property

        <StoredProcParameter("@Office")>
        Public Property _Office() As Boolean
            Get
                Return m_Office
            End Get
            Set(ByVal value As Boolean)
                m_Office = value
            End Set
        End Property

        <StoredProcParameter("@TelMobile")>
        Public Property _TelMobile() As String
            Get
                Return m_TelMobile
            End Get
            Set(ByVal value As String)
                m_TelMobile = value
            End Set
        End Property

        <StoredProcParameter("@Email")>
        Public Property _Email() As String
            Get
                Return m_Email
            End Get
            Set(ByVal value As String)
                m_Email = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Employee

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getEmployeebyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Employee

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getEmployeebyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Employee)

            Dim _EmployeeList As List(Of Employee)
            _EmployeeList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getEmployeeTable"))
            Return _EmployeeList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Employee)

            Dim _EmployeeList As List(Of Employee)
            _EmployeeList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getEmployeeTable"))
            Return _EmployeeList

        End Function

        Public Shared Sub SaveAll(ByVal EmployeeList As List(Of Employee))

            For Each _Employee As Employee In EmployeeList
                SaveRecord(_Employee)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal EmployeeList As List(Of Employee))

            For Each _Employee As Employee In EmployeeList
                SaveRecord(ConnectionString, _Employee)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Employee As Employee) As Guid
            Employee.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Employee, "upsertEmployee")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Employee As Employee) As Guid
            Employee.SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Employee, "upsertEmployee")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteEmployeebyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteEmployeebyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertEmployee")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Me, "upsertEmployee")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Employee

            Dim _E As Employee = Nothing

            If DR IsNot Nothing Then
                _E = New Employee()
                With DR
                    _E.IsNew = False
                    _E.IsDeleted = False
                    _E._ID = GetGUID(DR("ID"))
                    _E._Code = DR("code").ToString.Trim
                    _E._Fullname = DR("fullname").ToString.Trim
                    _E._Nickname = DR("nickname").ToString.Trim
                    _E._TradeCode = DR("trade_code").ToString.Trim
                    _E._TradeDesc = DR("trade_desc").ToString.Trim
                    _E._Rate = GetDecimal(DR("rate"))
                    _E._Active = GetBoolean(DR("active"))
                    _E._Office = GetBoolean(DR("office"))
                    _E._TelMobile = DR("tel_mobile").ToString.Trim
                    _E._Email = DR("email").ToString.Trim

                End With
            End If

            Return _E

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Employee)

            Dim _EmployeeList As New List(Of Employee)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _EmployeeList.Add(PropertiesFromData(_DR))
            Next

            Return _EmployeeList

        End Function


#End Region

    End Class

End Namespace
