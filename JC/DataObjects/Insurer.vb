﻿'*****************************************************
'Generated 21/03/2017 15:12:49 using Version 1.17.2.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Insurer
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_InsCode As String
        Dim m_InsName As String
        Dim m_InsThresh As Decimal
        Dim m_InsLower As Decimal
        Dim m_InsUpper As Decimal

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._InsCode = ""
                ._InsName = ""
                ._InsThresh = 0
                ._InsLower = 0
                ._InsUpper = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._InsCode = ""
                ._InsName = ""
                ._InsThresh = 0
                ._InsLower = 0
                ._InsUpper = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@InsCode")> _
        Public Property _InsCode() As String
            Get
                Return m_InsCode
            End Get
            Set(ByVal value As String)
                m_InsCode = value
            End Set
        End Property

        <StoredProcParameter("@InsName")> _
        Public Property _InsName() As String
            Get
                Return m_InsName
            End Get
            Set(ByVal value As String)
                m_InsName = value
            End Set
        End Property

        <StoredProcParameter("@InsThresh")> _
        Public Property _InsThresh() As Decimal
            Get
                Return m_InsThresh
            End Get
            Set(ByVal value As Decimal)
                m_InsThresh = value
            End Set
        End Property

        <StoredProcParameter("@InsLower")> _
        Public Property _InsLower() As Decimal
            Get
                Return m_InsLower
            End Get
            Set(ByVal value As Decimal)
                m_InsLower = value
            End Set
        End Property

        <StoredProcParameter("@InsUpper")> _
        Public Property _InsUpper() As Decimal
            Get
                Return m_InsUpper
            End Get
            Set(ByVal value As Decimal)
                m_InsUpper = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Insurer

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getInsurerbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Insurer

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getInsurerbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Insurer)

            Dim _InsurerList As List(Of Insurer)
            _InsurerList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getInsurerTable"))
            Return _InsurerList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Insurer)

            Dim _InsurerList As List(Of Insurer)
            _InsurerList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getInsurerTable"))
            Return _InsurerList

        End Function

        Public Shared Sub SaveAll(ByVal InsurerList As List(Of Insurer))

            For Each _Insurer As Insurer In InsurerList
                SaveRecord(_Insurer)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal InsurerList As List(Of Insurer))

            For Each _Insurer As Insurer In InsurerList
                SaveRecord(ConnectionString, _Insurer)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Insurer As Insurer) As Guid
            Insurer.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Insurer, "upsertInsurer")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Insurer As Insurer) As Guid
            Insurer.SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Insurer, "upsertInsurer")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteInsurerbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteInsurerbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertInsurer")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Me, "upsertInsurer")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Insurer

            Dim _I As Insurer = Nothing

            If DR IsNot Nothing Then
                _I = New Insurer()
                With DR
                    _I.IsNew = False
                    _I.IsDeleted = False
                    _I._ID = GetGUID(DR("ID"))
                    _I._InsCode = DR("ins_code").ToString.Trim
                    _I._InsName = DR("ins_name").ToString.Trim
                    _I._InsThresh = GetDecimal(DR("ins_thresh"))
                    _I._InsLower = GetDecimal(DR("ins_lower"))
                    _I._InsUpper = GetDecimal(DR("ins_upper"))

                End With
            End If

            Return _I

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Insurer)

            Dim _InsurerList As New List(Of Insurer)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _InsurerList.Add(PropertiesFromData(_DR))
            Next

            Return _InsurerList

        End Function


#End Region

    End Class

End Namespace
