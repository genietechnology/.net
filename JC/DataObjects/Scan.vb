﻿'*****************************************************
'Generated 11/09/2018 20:01:02 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Scan
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_DocStatus As String
        Dim m_DocIntray As String
        Dim m_DocIntrayName As String
        Dim m_ScannedFile As String
        Dim m_ScannedSize As Long
        Dim m_ScannedStamp As DateTime?
        Dim m_MatchedBy As String
        Dim m_MatchedStamp As DateTime?
        Dim m_MatchedTarget As String
        Dim m_MatchedTargetNo As String
        Dim m_MatchedTargetId As Guid?
        Dim m_MatchedName As String
        Dim m_MatchedRef1 As String
        Dim m_MatchedTargetMulti As Boolean
        Dim m_MatchedOwner As String
        Dim m_MatchedOwnerName As String
        Dim m_DocDate As Date?
        Dim m_DocType As String
        Dim m_DocSupplierId As Guid?
        Dim m_DocSupplierName As String
        Dim m_DocDesc As String
        Dim m_DocNet As Decimal
        Dim m_ChkDn As Boolean
        Dim m_ChkUsed As Boolean
        Dim m_ChkPriceReg As Boolean
        Dim m_ChkPrice As Boolean
        Dim m_LastUser As String
        Dim m_LastStamp As DateTime?
        Dim m_Comments As String
        Dim m_DocImage As Byte()
        Dim m_DocRef As String
        Dim m_DocPriority As Boolean
        Dim m_AuthorisedStamp As DateTime?
        Dim m_PaymentMethod As String
        Dim m_PageId As Guid?
        Dim m_PageNumber As Byte

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._DocStatus = ""
                ._DocIntray = ""
                ._DocIntrayName = ""
                ._ScannedFile = ""
                ._ScannedSize = 0
                ._ScannedStamp = Nothing
                ._MatchedBy = ""
                ._MatchedStamp = Nothing
                ._MatchedTarget = ""
                ._MatchedTargetNo = ""
                ._MatchedTargetId = Nothing
                ._MatchedName = ""
                ._MatchedRef1 = ""
                ._MatchedTargetMulti = False
                ._MatchedOwner = ""
                ._MatchedOwnerName = ""
                ._DocDate = Nothing
                ._DocType = ""
                ._DocSupplierId = Nothing
                ._DocSupplierName = ""
                ._DocDesc = ""
                ._DocNet = 0
                ._ChkDn = False
                ._ChkUsed = False
                ._ChkPriceReg = False
                ._ChkPrice = False
                ._LastUser = ""
                ._LastStamp = Nothing
                ._Comments = ""
                ._DocImage = Nothing
                ._DocRef = ""
                ._DocPriority = False
                ._AuthorisedStamp = Nothing
                ._PaymentMethod = ""
                ._PageId = Nothing
                ._PageNumber = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._DocStatus = ""
                ._DocIntray = ""
                ._DocIntrayName = ""
                ._ScannedFile = ""
                ._ScannedSize = 0
                ._ScannedStamp = Nothing
                ._MatchedBy = ""
                ._MatchedStamp = Nothing
                ._MatchedTarget = ""
                ._MatchedTargetNo = ""
                ._MatchedTargetId = Nothing
                ._MatchedName = ""
                ._MatchedRef1 = ""
                ._MatchedTargetMulti = False
                ._MatchedOwner = ""
                ._MatchedOwnerName = ""
                ._DocDate = Nothing
                ._DocType = ""
                ._DocSupplierId = Nothing
                ._DocSupplierName = ""
                ._DocDesc = ""
                ._DocNet = 0
                ._ChkDn = False
                ._ChkUsed = False
                ._ChkPriceReg = False
                ._ChkPrice = False
                ._LastUser = ""
                ._LastStamp = Nothing
                ._Comments = ""
                ._DocImage = Nothing
                ._DocRef = ""
                ._DocPriority = False
                ._AuthorisedStamp = Nothing
                ._PaymentMethod = ""
                ._PageId = Nothing
                ._PageNumber = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@DocStatus")>
        Public Property _DocStatus() As String
            Get
                Return m_DocStatus
            End Get
            Set(ByVal value As String)
                m_DocStatus = value
            End Set
        End Property

        <StoredProcParameter("@DocIntray")>
        Public Property _DocIntray() As String
            Get
                Return m_DocIntray
            End Get
            Set(ByVal value As String)
                m_DocIntray = value
            End Set
        End Property

        <StoredProcParameter("@DocIntrayName")>
        Public Property _DocIntrayName() As String
            Get
                Return m_DocIntrayName
            End Get
            Set(ByVal value As String)
                m_DocIntrayName = value
            End Set
        End Property

        <StoredProcParameter("@ScannedFile")>
        Public Property _ScannedFile() As String
            Get
                Return m_ScannedFile
            End Get
            Set(ByVal value As String)
                m_ScannedFile = value
            End Set
        End Property

        <StoredProcParameter("@ScannedSize")>
        Public Property _ScannedSize() As Long
            Get
                Return m_ScannedSize
            End Get
            Set(ByVal value As Long)
                m_ScannedSize = value
            End Set
        End Property

        <StoredProcParameter("@ScannedStamp")>
        Public Property _ScannedStamp() As DateTime?
            Get
                Return m_ScannedStamp
            End Get
            Set(ByVal value As DateTime?)
                m_ScannedStamp = value
            End Set
        End Property

        <StoredProcParameter("@MatchedBy")>
        Public Property _MatchedBy() As String
            Get
                Return m_MatchedBy
            End Get
            Set(ByVal value As String)
                m_MatchedBy = value
            End Set
        End Property

        <StoredProcParameter("@MatchedStamp")>
        Public Property _MatchedStamp() As DateTime?
            Get
                Return m_MatchedStamp
            End Get
            Set(ByVal value As DateTime?)
                m_MatchedStamp = value
            End Set
        End Property

        <StoredProcParameter("@MatchedTarget")>
        Public Property _MatchedTarget() As String
            Get
                Return m_MatchedTarget
            End Get
            Set(ByVal value As String)
                m_MatchedTarget = value
            End Set
        End Property

        <StoredProcParameter("@MatchedTargetNo")>
        Public Property _MatchedTargetNo() As String
            Get
                Return m_MatchedTargetNo
            End Get
            Set(ByVal value As String)
                m_MatchedTargetNo = value
            End Set
        End Property

        <StoredProcParameter("@MatchedTargetId")>
        Public Property _MatchedTargetId() As Guid?
            Get
                Return m_MatchedTargetId
            End Get
            Set(ByVal value As Guid?)
                m_MatchedTargetId = value
            End Set
        End Property

        <StoredProcParameter("@MatchedName")>
        Public Property _MatchedName() As String
            Get
                Return m_MatchedName
            End Get
            Set(ByVal value As String)
                m_MatchedName = value
            End Set
        End Property

        <StoredProcParameter("@MatchedRef1")>
        Public Property _MatchedRef1() As String
            Get
                Return m_MatchedRef1
            End Get
            Set(ByVal value As String)
                m_MatchedRef1 = value
            End Set
        End Property

        <StoredProcParameter("@MatchedTargetMulti")>
        Public Property _MatchedTargetMulti() As Boolean
            Get
                Return m_MatchedTargetMulti
            End Get
            Set(ByVal value As Boolean)
                m_MatchedTargetMulti = value
            End Set
        End Property

        <StoredProcParameter("@MatchedOwner")>
        Public Property _MatchedOwner() As String
            Get
                Return m_MatchedOwner
            End Get
            Set(ByVal value As String)
                m_MatchedOwner = value
            End Set
        End Property

        <StoredProcParameter("@MatchedOwnerName")>
        Public Property _MatchedOwnerName() As String
            Get
                Return m_MatchedOwnerName
            End Get
            Set(ByVal value As String)
                m_MatchedOwnerName = value
            End Set
        End Property

        <StoredProcParameter("@DocDate")>
        Public Property _DocDate() As Date?
            Get
                Return m_DocDate
            End Get
            Set(ByVal value As Date?)
                m_DocDate = value
            End Set
        End Property

        <StoredProcParameter("@DocType")>
        Public Property _DocType() As String
            Get
                Return m_DocType
            End Get
            Set(ByVal value As String)
                m_DocType = value
            End Set
        End Property

        <StoredProcParameter("@DocSupplierId")>
        Public Property _DocSupplierId() As Guid?
            Get
                Return m_DocSupplierId
            End Get
            Set(ByVal value As Guid?)
                m_DocSupplierId = value
            End Set
        End Property

        <StoredProcParameter("@DocSupplierName")>
        Public Property _DocSupplierName() As String
            Get
                Return m_DocSupplierName
            End Get
            Set(ByVal value As String)
                m_DocSupplierName = value
            End Set
        End Property

        <StoredProcParameter("@DocDesc")>
        Public Property _DocDesc() As String
            Get
                Return m_DocDesc
            End Get
            Set(ByVal value As String)
                m_DocDesc = value
            End Set
        End Property

        <StoredProcParameter("@DocNet")>
        Public Property _DocNet() As Decimal
            Get
                Return m_DocNet
            End Get
            Set(ByVal value As Decimal)
                m_DocNet = value
            End Set
        End Property

        <StoredProcParameter("@ChkDn")>
        Public Property _ChkDn() As Boolean
            Get
                Return m_ChkDn
            End Get
            Set(ByVal value As Boolean)
                m_ChkDn = value
            End Set
        End Property

        <StoredProcParameter("@ChkUsed")>
        Public Property _ChkUsed() As Boolean
            Get
                Return m_ChkUsed
            End Get
            Set(ByVal value As Boolean)
                m_ChkUsed = value
            End Set
        End Property

        <StoredProcParameter("@ChkPriceReg")>
        Public Property _ChkPriceReg() As Boolean
            Get
                Return m_ChkPriceReg
            End Get
            Set(ByVal value As Boolean)
                m_ChkPriceReg = value
            End Set
        End Property

        <StoredProcParameter("@ChkPrice")>
        Public Property _ChkPrice() As Boolean
            Get
                Return m_ChkPrice
            End Get
            Set(ByVal value As Boolean)
                m_ChkPrice = value
            End Set
        End Property

        <StoredProcParameter("@LastUser")>
        Public Property _LastUser() As String
            Get
                Return m_LastUser
            End Get
            Set(ByVal value As String)
                m_LastUser = value
            End Set
        End Property

        <StoredProcParameter("@LastStamp")>
        Public Property _LastStamp() As DateTime?
            Get
                Return m_LastStamp
            End Get
            Set(ByVal value As DateTime?)
                m_LastStamp = value
            End Set
        End Property

        <StoredProcParameter("@Comments")>
        Public Property _Comments() As String
            Get
                Return m_Comments
            End Get
            Set(ByVal value As String)
                m_Comments = value
            End Set
        End Property

        <StoredProcParameter("@DocImage")>
        Public Property _DocImage() As Byte()
            Get
                Return m_DocImage
            End Get
            Set(ByVal value As Byte())
                m_DocImage = value
            End Set
        End Property

        <StoredProcParameter("@DocRef")>
        Public Property _DocRef() As String
            Get
                Return m_DocRef
            End Get
            Set(ByVal value As String)
                m_DocRef = value
            End Set
        End Property

        <StoredProcParameter("@DocPriority")>
        Public Property _DocPriority() As Boolean
            Get
                Return m_DocPriority
            End Get
            Set(ByVal value As Boolean)
                m_DocPriority = value
            End Set
        End Property

        <StoredProcParameter("@AuthorisedStamp")>
        Public Property _AuthorisedStamp() As DateTime?
            Get
                Return m_AuthorisedStamp
            End Get
            Set(ByVal value As DateTime?)
                m_AuthorisedStamp = value
            End Set
        End Property

        <StoredProcParameter("@PaymentMethod")>
        Public Property _PaymentMethod() As String
            Get
                Return m_PaymentMethod
            End Get
            Set(ByVal value As String)
                m_PaymentMethod = value
            End Set
        End Property

        <StoredProcParameter("@PageId")>
        Public Property _PageId() As Guid?
            Get
                Return m_PageId
            End Get
            Set(ByVal value As Guid?)
                m_PageId = value
            End Set
        End Property

        <StoredProcParameter("@PageNumber")>
        Public Property _PageNumber() As Byte
            Get
                Return m_PageNumber
            End Get
            Set(ByVal value As Byte)
                m_PageNumber = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Scan

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getScanbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Scan

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getScanbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Scan)

            Dim _ScanList As List(Of Scan)
            _ScanList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getScanTable"))
            Return _ScanList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Scan)

            Dim _ScanList As List(Of Scan)
            _ScanList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getScanTable"))
            Return _ScanList

        End Function

        Public Shared Sub SaveAll(ByVal ScanList As List(Of Scan))

            For Each _Scan As Scan In ScanList
                SaveRecord(_Scan)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal ScanList As List(Of Scan))

            For Each _Scan As Scan In ScanList
                SaveRecord(ConnectionString, _Scan)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Scan As Scan) As Guid
            Scan.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Scan, "upsertScan")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Scan As Scan) As Guid
            Scan.SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Scan, "upsertScan")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteScanbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteScanbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertScan")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Me, "upsertScan")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Scan

            Dim _S As Scan = Nothing

            If DR IsNot Nothing Then
                _S = New Scan()
                With DR
                    _S.IsNew = False
                    _S.IsDeleted = False
                    _S._ID = GetGUID(DR("ID"))
                    _S._DocStatus = DR("doc_status").ToString.Trim
                    _S._DocIntray = DR("doc_intray").ToString.Trim
                    _S._DocIntrayName = DR("doc_intray_name").ToString.Trim
                    _S._ScannedFile = DR("scanned_file").ToString.Trim
                    _S._ScannedSize = GetLong(DR("scanned_size"))
                    _S._ScannedStamp = GetDateTime(DR("scanned_stamp"))
                    _S._MatchedBy = DR("matched_by").ToString.Trim
                    _S._MatchedStamp = GetDateTime(DR("matched_stamp"))
                    _S._MatchedTarget = DR("matched_target").ToString.Trim
                    _S._MatchedTargetNo = DR("matched_target_no").ToString.Trim
                    _S._MatchedTargetId = GetGUID(DR("matched_target_id"))
                    _S._MatchedName = DR("matched_name").ToString.Trim
                    _S._MatchedRef1 = DR("matched_ref1").ToString.Trim
                    _S._MatchedTargetMulti = GetBoolean(DR("matched_target_multi"))
                    _S._MatchedOwner = DR("matched_owner").ToString.Trim
                    _S._MatchedOwnerName = DR("matched_owner_name").ToString.Trim
                    _S._DocDate = GetDate(DR("doc_date"))
                    _S._DocType = DR("doc_type").ToString.Trim
                    _S._DocSupplierId = GetGUID(DR("doc_supplier_id"))
                    _S._DocSupplierName = DR("doc_supplier_name").ToString.Trim
                    _S._DocDesc = DR("doc_desc").ToString.Trim
                    _S._DocNet = GetDecimal(DR("doc_net"))
                    _S._ChkDn = GetBoolean(DR("chk_dn"))
                    _S._ChkUsed = GetBoolean(DR("chk_used"))
                    _S._ChkPriceReg = GetBoolean(DR("chk_price_reg"))
                    _S._ChkPrice = GetBoolean(DR("chk_price"))
                    _S._LastUser = DR("last_user").ToString.Trim
                    _S._LastStamp = GetDateTime(DR("last_stamp"))
                    _S._Comments = DR("comments").ToString.Trim
                    _S._DocImage = GetImage(DR("doc_image"))
                    _S._DocRef = DR("doc_ref").ToString.Trim
                    _S._DocPriority = GetBoolean(DR("doc_priority"))
                    _S._AuthorisedStamp = GetDateTime(DR("authorised_stamp"))
                    _S._PaymentMethod = DR("payment_method").ToString.Trim
                    _S._PageId = GetGUID(DR("page_id"))
                    _S._PageNumber = GetByte(DR("page_number"))

                End With
            End If

            Return _S

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Scan)

            Dim _ScanList As New List(Of Scan)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _ScanList.Add(PropertiesFromData(_DR))
            Next

            Return _ScanList

        End Function


#End Region

    End Class

End Namespace
