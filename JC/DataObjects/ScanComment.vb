﻿'*****************************************************
'Generated 02/08/2018 20:38:24 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class ScanComment
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_ScanId As Guid?
        Dim m_UserId As Guid?
        Dim m_UserName As String
        Dim m_Stamp As DateTime?
        Dim m_Comments As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._ScanId = Nothing
                ._UserId = Nothing
                ._UserName = ""
                ._Stamp = Nothing
                ._Comments = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._ScanId = Nothing
                ._UserId = Nothing
                ._UserName = ""
                ._Stamp = Nothing
                ._Comments = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@ScanId")>
        Public Property _ScanId() As Guid?
            Get
                Return m_ScanId
            End Get
            Set(ByVal value As Guid?)
                m_ScanId = value
            End Set
        End Property

        <StoredProcParameter("@UserId")>
        Public Property _UserId() As Guid?
            Get
                Return m_UserId
            End Get
            Set(ByVal value As Guid?)
                m_UserId = value
            End Set
        End Property

        <StoredProcParameter("@UserName")>
        Public Property _UserName() As String
            Get
                Return m_UserName
            End Get
            Set(ByVal value As String)
                m_UserName = value
            End Set
        End Property

        <StoredProcParameter("@Stamp")>
        Public Property _Stamp() As DateTime?
            Get
                Return m_Stamp
            End Get
            Set(ByVal value As DateTime?)
                m_Stamp = value
            End Set
        End Property

        <StoredProcParameter("@Comments")>
        Public Property _Comments() As String
            Get
                Return m_Comments
            End Get
            Set(ByVal value As String)
                m_Comments = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As ScanComment

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getScanCommentbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As ScanComment

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getScanCommentbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of ScanComment)

            Dim _ScanCommentList As List(Of ScanComment)
            _ScanCommentList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getScanCommentTable"))
            Return _ScanCommentList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of ScanComment)

            Dim _ScanCommentList As List(Of ScanComment)
            _ScanCommentList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getScanCommentTable"))
            Return _ScanCommentList

        End Function

        Public Shared Sub SaveAll(ByVal ScanCommentList As List(Of ScanComment))

            For Each _ScanComment As ScanComment In ScanCommentList
                SaveRecord(_ScanComment)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal ScanCommentList As List(Of ScanComment))

            For Each _ScanComment As ScanComment In ScanCommentList
                SaveRecord(ConnectionString, _ScanComment)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal ScanComment As ScanComment) As Guid
            ScanComment.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, ScanComment, "upsertScanComment")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal ScanComment As ScanComment) As Guid
            ScanComment.SetDefaultValues()
            DAL.SaveRecord(ConnectionString, ScanComment, "upsertScanComment")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteScanCommentbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteScanCommentbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertScanComment")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Me, "upsertScanComment")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As ScanComment

            Dim _S As ScanComment = Nothing

            If DR IsNot Nothing Then
                _S = New ScanComment()
                With DR
                    _S.IsNew = False
                    _S.IsDeleted = False
                    _S._ID = GetGUID(DR("ID"))
                    _S._ScanId = GetGUID(DR("scan_id"))
                    _S._UserId = GetGUID(DR("user_id"))
                    _S._UserName = DR("user_name").ToString.Trim
                    _S._Stamp = GetDateTime(DR("stamp"))
                    _S._Comments = DR("comments").ToString.Trim

                End With
            End If

            Return _S

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of ScanComment)

            Dim _ScanCommentList As New List(Of ScanComment)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _ScanCommentList.Add(PropertiesFromData(_DR))
            Next

            Return _ScanCommentList

        End Function


#End Region

    End Class

End Namespace
