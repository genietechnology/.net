﻿'*****************************************************
'Generated 02/03/2017 21:20:15 using Version 1.15.8.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class JobReminder
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_JobId As Guid?
        Dim m_RemCreated As Date?
        Dim m_RemFrom As Date?
        Dim m_RemTo As Date?
        Dim m_RemChecked As Boolean
        Dim m_RemInvoiceProfile As String
        Dim m_RemInvoiceMarkup As Decimal
        Dim m_RemInvoiceTotal As Decimal
        Dim m_RemInvoiceYtd As Decimal
        Dim m_RemComments As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._JobId = Nothing
                ._RemCreated = Nothing
                ._RemFrom = Nothing
                ._RemTo = Nothing
                ._RemChecked = False
                ._RemInvoiceProfile = ""
                ._RemInvoiceMarkup = 0
                ._RemInvoiceTotal = 0
                ._RemInvoiceYtd = 0
                ._RemComments = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._JobId = Nothing
                ._RemCreated = Nothing
                ._RemFrom = Nothing
                ._RemTo = Nothing
                ._RemChecked = False
                ._RemInvoiceProfile = ""
                ._RemInvoiceMarkup = 0
                ._RemInvoiceTotal = 0
                ._RemInvoiceYtd = 0
                ._RemComments = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@JobId")> _
        Public Property _JobId() As Guid?
            Get
                Return m_JobId
            End Get
            Set(ByVal value As Guid?)
                m_JobId = value
            End Set
        End Property

        <StoredProcParameter("@RemCreated")> _
        Public Property _RemCreated() As Date?
            Get
                Return m_RemCreated
            End Get
            Set(ByVal value As Date?)
                m_RemCreated = value
            End Set
        End Property

        <StoredProcParameter("@RemFrom")> _
        Public Property _RemFrom() As Date?
            Get
                Return m_RemFrom
            End Get
            Set(ByVal value As Date?)
                m_RemFrom = value
            End Set
        End Property

        <StoredProcParameter("@RemTo")> _
        Public Property _RemTo() As Date?
            Get
                Return m_RemTo
            End Get
            Set(ByVal value As Date?)
                m_RemTo = value
            End Set
        End Property

        <StoredProcParameter("@RemChecked")> _
        Public Property _RemChecked() As Boolean
            Get
                Return m_RemChecked
            End Get
            Set(ByVal value As Boolean)
                m_RemChecked = value
            End Set
        End Property

        <StoredProcParameter("@RemInvoiceProfile")> _
        Public Property _RemInvoiceProfile() As String
            Get
                Return m_RemInvoiceProfile
            End Get
            Set(ByVal value As String)
                m_RemInvoiceProfile = value
            End Set
        End Property

        <StoredProcParameter("@RemInvoiceMarkup")> _
        Public Property _RemInvoiceMarkup() As Decimal
            Get
                Return m_RemInvoiceMarkup
            End Get
            Set(ByVal value As Decimal)
                m_RemInvoiceMarkup = value
            End Set
        End Property

        <StoredProcParameter("@RemInvoiceTotal")> _
        Public Property _RemInvoiceTotal() As Decimal
            Get
                Return m_RemInvoiceTotal
            End Get
            Set(ByVal value As Decimal)
                m_RemInvoiceTotal = value
            End Set
        End Property

        <StoredProcParameter("@RemInvoiceYtd")> _
        Public Property _RemInvoiceYtd() As Decimal
            Get
                Return m_RemInvoiceYtd
            End Get
            Set(ByVal value As Decimal)
                m_RemInvoiceYtd = value
            End Set
        End Property

        <StoredProcParameter("@RemComments")> _
        Public Property _RemComments() As String
            Get
                Return m_RemComments
            End Get
            Set(ByVal value As String)
                m_RemComments = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As JobReminder

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getJobReminderbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of JobReminder)

            Dim _JobReminderList As List(Of JobReminder)
            _JobReminderList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getJobReminderTable"))
            Return _JobReminderList

        End Function

        Public Shared Sub SaveAll(ByVal JobReminderList As List(Of JobReminder))

            For Each _JobReminder As JobReminder In JobReminderList
                SaveRecord(_JobReminder)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal JobReminder As JobReminder) As Guid
            JobReminder.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, JobReminder, "upsertJobReminder")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteJobReminderbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertJobReminder")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As JobReminder

            Dim _J As JobReminder = Nothing

            If DR IsNot Nothing Then
                _J = New JobReminder()
                With DR
                    _J.IsNew = False
                    _J.IsDeleted = False
                    _J._ID = GetGUID(DR("ID"))
                    _J._JobId = GetGUID(DR("job_id"))
                    _J._RemCreated = GetDate(DR("rem_created"))
                    _J._RemFrom = GetDate(DR("rem_from"))
                    _J._RemTo = GetDate(DR("rem_to"))
                    _J._RemChecked = GetBoolean(DR("rem_checked"))
                    _J._RemInvoiceProfile = DR("rem_invoice_profile").ToString.Trim
                    _J._RemInvoiceMarkup = GetDecimal(DR("rem_invoice_markup"))
                    _J._RemInvoiceTotal = GetDecimal(DR("rem_invoice_total"))
                    _J._RemInvoiceYtd = GetDecimal(DR("rem_invoice_ytd"))
                    _J._RemComments = DR("rem_comments").ToString.Trim

                End With
            End If

            Return _J

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of JobReminder)

            Dim _JobReminderList As New List(Of JobReminder)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _JobReminderList.Add(PropertiesFromData(_DR))
            Next

            Return _JobReminderList

        End Function


#End Region

    End Class

End Namespace
