﻿'*****************************************************
'Generated 02/03/2017 21:16:47 using Version 1.15.8.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class JobNote
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_KeyId As Guid?
        Dim m_NoteSubject As String
        Dim m_NoteText As String
        Dim m_NoteUser As Guid?
        Dim m_NoteStamp As DateTime?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._KeyId = Nothing
                ._NoteSubject = ""
                ._NoteText = ""
                ._NoteUser = Nothing
                ._NoteStamp = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._KeyId = Nothing
                ._NoteSubject = ""
                ._NoteText = ""
                ._NoteUser = Nothing
                ._NoteStamp = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@KeyId")> _
        Public Property _KeyId() As Guid?
            Get
                Return m_KeyId
            End Get
            Set(ByVal value As Guid?)
                m_KeyId = value
            End Set
        End Property

        <StoredProcParameter("@NoteSubject")> _
        Public Property _NoteSubject() As String
            Get
                Return m_NoteSubject
            End Get
            Set(ByVal value As String)
                m_NoteSubject = value
            End Set
        End Property

        <StoredProcParameter("@NoteText")> _
        Public Property _NoteText() As String
            Get
                Return m_NoteText
            End Get
            Set(ByVal value As String)
                m_NoteText = value
            End Set
        End Property

        <StoredProcParameter("@NoteUser")> _
        Public Property _NoteUser() As Guid?
            Get
                Return m_NoteUser
            End Get
            Set(ByVal value As Guid?)
                m_NoteUser = value
            End Set
        End Property

        <StoredProcParameter("@NoteStamp")> _
        Public Property _NoteStamp() As DateTime?
            Get
                Return m_NoteStamp
            End Get
            Set(ByVal value As DateTime?)
                m_NoteStamp = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As JobNote

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getJobNotebyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of JobNote)

            Dim _JobNoteList As List(Of JobNote)
            _JobNoteList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getJobNoteTable"))
            Return _JobNoteList

        End Function

        Public Shared Sub SaveAll(ByVal JobNoteList As List(Of JobNote))

            For Each _JobNote As JobNote In JobNoteList
                SaveRecord(_JobNote)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal JobNote As JobNote) As Guid
            JobNote.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, JobNote, "upsertJobNote")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteJobNotebyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertJobNote")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As JobNote

            Dim _J As JobNote = Nothing

            If DR IsNot Nothing Then
                _J = New JobNote()
                With DR
                    _J.IsNew = False
                    _J.IsDeleted = False
                    _J._ID = GetGUID(DR("ID"))
                    _J._KeyId = GetGUID(DR("key_id"))
                    _J._NoteSubject = DR("note_subject").ToString.Trim
                    _J._NoteText = DR("note_text").ToString.Trim
                    _J._NoteUser = GetGUID(DR("note_user"))
                    _J._NoteStamp = GetDateTime(DR("note_stamp"))

                End With
            End If

            Return _J

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of JobNote)

            Dim _JobNoteList As New List(Of JobNote)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _JobNoteList.Add(PropertiesFromData(_DR))
            Next

            Return _JobNoteList

        End Function


#End Region

    End Class

End Namespace
