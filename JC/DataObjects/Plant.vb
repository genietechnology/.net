﻿'*****************************************************
'Generated 24/07/2017 10:08:36 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Plant
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_AjNo As String
        Dim m_Name As String
        Dim m_Area As String
        Dim m_PlantType As String
        Dim m_PlantCat As String
        Dim m_No1 As String
        Dim m_No2 As String
        Dim m_RegNo As String
        Dim m_Nickname As String
        Dim m_Make As String
        Dim m_Model As String
        Dim m_Mark As String
        Dim m_YearManu As Integer
        Dim m_Location As String
        Dim m_LocationJob As Guid?
        Dim m_LocationStamp As DateTime?
        Dim m_PurcDate As Date?
        Dim m_PurcCost As Decimal
        Dim m_PurcEbay As String
        Dim m_TaxLast As Date?
        Dim m_TaxCost As Decimal
        Dim m_LastMot As Date?
        Dim m_LastService As Date?
        Dim m_LastMaint As Date?
        Dim m_LastCheck As Date?
        Dim m_Insurer As String
        Dim m_Recovery As String
        Dim m_Tracker As Boolean
        Dim m_Hire As Boolean
        Dim m_HireCost As Decimal
        Dim m_Photo As Byte()

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._AjNo = ""
                ._Name = ""
                ._Area = ""
                ._PlantType = ""
                ._PlantCat = ""
                ._No1 = ""
                ._No2 = ""
                ._RegNo = ""
                ._Nickname = ""
                ._Make = ""
                ._Model = ""
                ._Mark = ""
                ._YearManu = 0
                ._Location = ""
                ._LocationJob = Nothing
                ._LocationStamp = Nothing
                ._PurcDate = Nothing
                ._PurcCost = 0
                ._PurcEbay = ""
                ._TaxLast = Nothing
                ._TaxCost = 0
                ._LastMot = Nothing
                ._LastService = Nothing
                ._LastMaint = Nothing
                ._LastCheck = Nothing
                ._Insurer = ""
                ._Recovery = ""
                ._Tracker = False
                ._Hire = False
                ._HireCost = 0
                ._Photo = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._AjNo = ""
                ._Name = ""
                ._Area = ""
                ._PlantType = ""
                ._PlantCat = ""
                ._No1 = ""
                ._No2 = ""
                ._RegNo = ""
                ._Nickname = ""
                ._Make = ""
                ._Model = ""
                ._Mark = ""
                ._YearManu = 0
                ._Location = ""
                ._LocationJob = Nothing
                ._LocationStamp = Nothing
                ._PurcDate = Nothing
                ._PurcCost = 0
                ._PurcEbay = ""
                ._TaxLast = Nothing
                ._TaxCost = 0
                ._LastMot = Nothing
                ._LastService = Nothing
                ._LastMaint = Nothing
                ._LastCheck = Nothing
                ._Insurer = ""
                ._Recovery = ""
                ._Tracker = False
                ._Hire = False
                ._HireCost = 0
                ._Photo = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@AjNo")>
        Public Property _AjNo() As String
            Get
                Return m_AjNo
            End Get
            Set(ByVal value As String)
                m_AjNo = value
            End Set
        End Property

        <StoredProcParameter("@Name")>
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@Area")>
        Public Property _Area() As String
            Get
                Return m_Area
            End Get
            Set(ByVal value As String)
                m_Area = value
            End Set
        End Property

        <StoredProcParameter("@PlantType")>
        Public Property _PlantType() As String
            Get
                Return m_PlantType
            End Get
            Set(ByVal value As String)
                m_PlantType = value
            End Set
        End Property

        <StoredProcParameter("@PlantCat")>
        Public Property _PlantCat() As String
            Get
                Return m_PlantCat
            End Get
            Set(ByVal value As String)
                m_PlantCat = value
            End Set
        End Property

        <StoredProcParameter("@No1")>
        Public Property _No1() As String
            Get
                Return m_No1
            End Get
            Set(ByVal value As String)
                m_No1 = value
            End Set
        End Property

        <StoredProcParameter("@No2")>
        Public Property _No2() As String
            Get
                Return m_No2
            End Get
            Set(ByVal value As String)
                m_No2 = value
            End Set
        End Property

        <StoredProcParameter("@RegNo")>
        Public Property _RegNo() As String
            Get
                Return m_RegNo
            End Get
            Set(ByVal value As String)
                m_RegNo = value
            End Set
        End Property

        <StoredProcParameter("@Nickname")>
        Public Property _Nickname() As String
            Get
                Return m_Nickname
            End Get
            Set(ByVal value As String)
                m_Nickname = value
            End Set
        End Property

        <StoredProcParameter("@Make")>
        Public Property _Make() As String
            Get
                Return m_Make
            End Get
            Set(ByVal value As String)
                m_Make = value
            End Set
        End Property

        <StoredProcParameter("@Model")>
        Public Property _Model() As String
            Get
                Return m_Model
            End Get
            Set(ByVal value As String)
                m_Model = value
            End Set
        End Property

        <StoredProcParameter("@Mark")>
        Public Property _Mark() As String
            Get
                Return m_Mark
            End Get
            Set(ByVal value As String)
                m_Mark = value
            End Set
        End Property

        <StoredProcParameter("@YearManu")>
        Public Property _YearManu() As Integer
            Get
                Return m_YearManu
            End Get
            Set(ByVal value As Integer)
                m_YearManu = value
            End Set
        End Property

        <StoredProcParameter("@Location")>
        Public Property _Location() As String
            Get
                Return m_Location
            End Get
            Set(ByVal value As String)
                m_Location = value
            End Set
        End Property

        <StoredProcParameter("@LocationJob")>
        Public Property _LocationJob() As Guid?
            Get
                Return m_LocationJob
            End Get
            Set(ByVal value As Guid?)
                m_LocationJob = value
            End Set
        End Property

        <StoredProcParameter("@LocationStamp")>
        Public Property _LocationStamp() As DateTime?
            Get
                Return m_LocationStamp
            End Get
            Set(ByVal value As DateTime?)
                m_LocationStamp = value
            End Set
        End Property

        <StoredProcParameter("@PurcDate")>
        Public Property _PurcDate() As Date?
            Get
                Return m_PurcDate
            End Get
            Set(ByVal value As Date?)
                m_PurcDate = value
            End Set
        End Property

        <StoredProcParameter("@PurcCost")>
        Public Property _PurcCost() As Decimal
            Get
                Return m_PurcCost
            End Get
            Set(ByVal value As Decimal)
                m_PurcCost = value
            End Set
        End Property

        <StoredProcParameter("@PurcEbay")>
        Public Property _PurcEbay() As String
            Get
                Return m_PurcEbay
            End Get
            Set(ByVal value As String)
                m_PurcEbay = value
            End Set
        End Property

        <StoredProcParameter("@TaxLast")>
        Public Property _TaxLast() As Date?
            Get
                Return m_TaxLast
            End Get
            Set(ByVal value As Date?)
                m_TaxLast = value
            End Set
        End Property

        <StoredProcParameter("@TaxCost")>
        Public Property _TaxCost() As Decimal
            Get
                Return m_TaxCost
            End Get
            Set(ByVal value As Decimal)
                m_TaxCost = value
            End Set
        End Property

        <StoredProcParameter("@LastMot")>
        Public Property _LastMot() As Date?
            Get
                Return m_LastMot
            End Get
            Set(ByVal value As Date?)
                m_LastMot = value
            End Set
        End Property

        <StoredProcParameter("@LastService")>
        Public Property _LastService() As Date?
            Get
                Return m_LastService
            End Get
            Set(ByVal value As Date?)
                m_LastService = value
            End Set
        End Property

        <StoredProcParameter("@LastMaint")>
        Public Property _LastMaint() As Date?
            Get
                Return m_LastMaint
            End Get
            Set(ByVal value As Date?)
                m_LastMaint = value
            End Set
        End Property

        <StoredProcParameter("@LastCheck")>
        Public Property _LastCheck() As Date?
            Get
                Return m_LastCheck
            End Get
            Set(ByVal value As Date?)
                m_LastCheck = value
            End Set
        End Property

        <StoredProcParameter("@Insurer")>
        Public Property _Insurer() As String
            Get
                Return m_Insurer
            End Get
            Set(ByVal value As String)
                m_Insurer = value
            End Set
        End Property

        <StoredProcParameter("@Recovery")>
        Public Property _Recovery() As String
            Get
                Return m_Recovery
            End Get
            Set(ByVal value As String)
                m_Recovery = value
            End Set
        End Property

        <StoredProcParameter("@Tracker")>
        Public Property _Tracker() As Boolean
            Get
                Return m_Tracker
            End Get
            Set(ByVal value As Boolean)
                m_Tracker = value
            End Set
        End Property

        <StoredProcParameter("@Hire")>
        Public Property _Hire() As Boolean
            Get
                Return m_Hire
            End Get
            Set(ByVal value As Boolean)
                m_Hire = value
            End Set
        End Property

        <StoredProcParameter("@HireCost")>
        Public Property _HireCost() As Decimal
            Get
                Return m_HireCost
            End Get
            Set(ByVal value As Decimal)
                m_HireCost = value
            End Set
        End Property

        <StoredProcParameter("@Photo")>
        Public Property _Photo() As Byte()
            Get
                Return m_Photo
            End Get
            Set(ByVal value As Byte())
                m_Photo = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Plant

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getPlantbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Plant

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getPlantbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Plant)

            Dim _PlantList As List(Of Plant)
            _PlantList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getPlantTable"))
            Return _PlantList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Plant)

            Dim _PlantList As List(Of Plant)
            _PlantList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getPlantTable"))
            Return _PlantList

        End Function

        Public Shared Sub SaveAll(ByVal PlantList As List(Of Plant))

            For Each _Plant As Plant In PlantList
                SaveRecord(_Plant)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal PlantList As List(Of Plant))

            For Each _Plant As Plant In PlantList
                SaveRecord(ConnectionString, _Plant)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Plant As Plant) As Guid
            Plant.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Plant, "upsertPlant")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Plant As Plant) As Guid
            Plant.SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Plant, "upsertPlant")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deletePlantbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deletePlantbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertPlant")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Me, "upsertPlant")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Plant

            Dim _P As Plant = Nothing

            If DR IsNot Nothing Then
                _P = New Plant()
                With DR
                    _P.IsNew = False
                    _P.IsDeleted = False
                    _P._ID = GetGUID(DR("ID"))
                    _P._AjNo = DR("aj_no").ToString.Trim
                    _P._Name = DR("name").ToString.Trim
                    _P._Area = DR("area").ToString.Trim
                    _P._PlantType = DR("plant_type").ToString.Trim
                    _P._PlantCat = DR("plant_cat").ToString.Trim
                    _P._No1 = DR("no_1").ToString.Trim
                    _P._No2 = DR("no_2").ToString.Trim
                    _P._RegNo = DR("reg_no").ToString.Trim
                    _P._Nickname = DR("nickname").ToString.Trim
                    _P._Make = DR("make").ToString.Trim
                    _P._Model = DR("model").ToString.Trim
                    _P._Mark = DR("mark").ToString.Trim
                    _P._YearManu = GetInteger(DR("year_manu"))
                    _P._Location = DR("location").ToString.Trim
                    _P._LocationJob = GetGUID(DR("location_job"))
                    _P._LocationStamp = GetDateTime(DR("location_stamp"))
                    _P._PurcDate = GetDate(DR("purc_date"))
                    _P._PurcCost = GetDecimal(DR("purc_cost"))
                    _P._PurcEbay = DR("purc_ebay").ToString.Trim
                    _P._TaxLast = GetDate(DR("tax_last"))
                    _P._TaxCost = GetDecimal(DR("tax_cost"))
                    _P._LastMot = GetDate(DR("last_mot"))
                    _P._LastService = GetDate(DR("last_service"))
                    _P._LastMaint = GetDate(DR("last_maint"))
                    _P._LastCheck = GetDate(DR("last_check"))
                    _P._Insurer = DR("insurer").ToString.Trim
                    _P._Recovery = DR("recovery").ToString.Trim
                    _P._Tracker = GetBoolean(DR("tracker"))
                    _P._Hire = GetBoolean(DR("hire"))
                    _P._HireCost = GetDecimal(DR("hire_cost"))
                    _P._Photo = GetImage(DR("photo"))

                End With
            End If

            Return _P

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Plant)

            Dim _PlantList As New List(Of Plant)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _PlantList.Add(PropertiesFromData(_DR))
            Next

            Return _PlantList

        End Function


#End Region

    End Class

End Namespace
