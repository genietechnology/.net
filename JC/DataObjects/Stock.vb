﻿'*****************************************************
'Generated 22/11/2017 12:49:50 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Stock
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Quickfind As String
        Dim m_Description As String
        Dim m_SellLastPrice As Date?
        Dim m_SellPrice As Decimal
        Dim m_SellUnit As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Quickfind = ""
                ._Description = ""
                ._SellLastPrice = Nothing
                ._SellPrice = 0
                ._SellUnit = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Quickfind = ""
                ._Description = ""
                ._SellLastPrice = Nothing
                ._SellPrice = 0
                ._SellUnit = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Quickfind")>
        Public Property _Quickfind() As String
            Get
                Return m_Quickfind
            End Get
            Set(ByVal value As String)
                m_Quickfind = value
            End Set
        End Property

        <StoredProcParameter("@Description")>
        Public Property _Description() As String
            Get
                Return m_Description
            End Get
            Set(ByVal value As String)
                m_Description = value
            End Set
        End Property

        <StoredProcParameter("@SellLastPrice")>
        Public Property _SellLastPrice() As Date?
            Get
                Return m_SellLastPrice
            End Get
            Set(ByVal value As Date?)
                m_SellLastPrice = value
            End Set
        End Property

        <StoredProcParameter("@SellPrice")>
        Public Property _SellPrice() As Decimal
            Get
                Return m_SellPrice
            End Get
            Set(ByVal value As Decimal)
                m_SellPrice = value
            End Set
        End Property

        <StoredProcParameter("@SellUnit")>
        Public Property _SellUnit() As String
            Get
                Return m_SellUnit
            End Get
            Set(ByVal value As String)
                m_SellUnit = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Stock

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getStockbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Stock

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getStockbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Stock)

            Dim _StockList As List(Of Stock)
            _StockList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getStockTable"))
            Return _StockList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Stock)

            Dim _StockList As List(Of Stock)
            _StockList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getStockTable"))
            Return _StockList

        End Function

        Public Shared Sub SaveAll(ByVal StockList As List(Of Stock))

            For Each _Stock As Stock In StockList
                SaveRecord(_Stock)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal StockList As List(Of Stock))

            For Each _Stock As Stock In StockList
                SaveRecord(ConnectionString, _Stock)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Stock As Stock) As Guid
            Stock.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Stock, "upsertStock")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Stock As Stock) As Guid
            Stock.SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Stock, "upsertStock")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteStockbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteStockbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertStock")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Me, "upsertStock")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Stock

            Dim _S As Stock = Nothing

            If DR IsNot Nothing Then
                _S = New Stock()
                With DR
                    _S.IsNew = False
                    _S.IsDeleted = False
                    _S._ID = GetGUID(DR("ID"))
                    _S._Quickfind = DR("quickfind").ToString.Trim
                    _S._Description = DR("description").ToString.Trim
                    _S._SellLastPrice = GetDate(DR("sell_last_price"))
                    _S._SellPrice = GetDecimal(DR("sell_price"))
                    _S._SellUnit = DR("sell_unit").ToString.Trim

                End With
            End If

            Return _S

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Stock)

            Dim _StockList As New List(Of Stock)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _StockList.Add(PropertiesFromData(_DR))
            Next

            Return _StockList

        End Function


#End Region

    End Class

End Namespace
