﻿'*****************************************************
'Generated 21/11/2017 12:46:38 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Contact
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Forename As String
        Dim m_Surname As String
        Dim m_Fullname As String
        Dim m_Company As String
        Dim m_Jobtitle As String
        Dim m_Relationship As String
        Dim m_TelMobile As String
        Dim m_TelHome As String
        Dim m_TelWork As String
        Dim m_TelOther As String
        Dim m_TelAll As String
        Dim m_Email As String
        Dim m_EmailOther As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Forename = ""
                ._Surname = ""
                ._Fullname = ""
                ._Company = ""
                ._Jobtitle = ""
                ._Relationship = ""
                ._TelMobile = ""
                ._TelHome = ""
                ._TelWork = ""
                ._TelOther = ""
                ._TelAll = ""
                ._Email = ""
                ._EmailOther = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Forename = ""
                ._Surname = ""
                ._Fullname = ""
                ._Company = ""
                ._Jobtitle = ""
                ._Relationship = ""
                ._TelMobile = ""
                ._TelHome = ""
                ._TelWork = ""
                ._TelOther = ""
                ._TelAll = ""
                ._Email = ""
                ._EmailOther = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Forename")>
        Public Property _Forename() As String
            Get
                Return m_Forename
            End Get
            Set(ByVal value As String)
                m_Forename = value
            End Set
        End Property

        <StoredProcParameter("@Surname")>
        Public Property _Surname() As String
            Get
                Return m_Surname
            End Get
            Set(ByVal value As String)
                m_Surname = value
            End Set
        End Property

        <StoredProcParameter("@Fullname")>
        Public Property _Fullname() As String
            Get
                Return m_Fullname
            End Get
            Set(ByVal value As String)
                m_Fullname = value
            End Set
        End Property

        <StoredProcParameter("@Company")>
        Public Property _Company() As String
            Get
                Return m_Company
            End Get
            Set(ByVal value As String)
                m_Company = value
            End Set
        End Property

        <StoredProcParameter("@Jobtitle")>
        Public Property _Jobtitle() As String
            Get
                Return m_Jobtitle
            End Get
            Set(ByVal value As String)
                m_Jobtitle = value
            End Set
        End Property

        <StoredProcParameter("@Relationship")>
        Public Property _Relationship() As String
            Get
                Return m_Relationship
            End Get
            Set(ByVal value As String)
                m_Relationship = value
            End Set
        End Property

        <StoredProcParameter("@TelMobile")>
        Public Property _TelMobile() As String
            Get
                Return m_TelMobile
            End Get
            Set(ByVal value As String)
                m_TelMobile = value
            End Set
        End Property

        <StoredProcParameter("@TelHome")>
        Public Property _TelHome() As String
            Get
                Return m_TelHome
            End Get
            Set(ByVal value As String)
                m_TelHome = value
            End Set
        End Property

        <StoredProcParameter("@TelWork")>
        Public Property _TelWork() As String
            Get
                Return m_TelWork
            End Get
            Set(ByVal value As String)
                m_TelWork = value
            End Set
        End Property

        <StoredProcParameter("@TelOther")>
        Public Property _TelOther() As String
            Get
                Return m_TelOther
            End Get
            Set(ByVal value As String)
                m_TelOther = value
            End Set
        End Property

        <StoredProcParameter("@TelAll")>
        Public Property _TelAll() As String
            Get
                Return m_TelAll
            End Get
            Set(ByVal value As String)
                m_TelAll = value
            End Set
        End Property

        <StoredProcParameter("@Email")>
        Public Property _Email() As String
            Get
                Return m_Email
            End Get
            Set(ByVal value As String)
                m_Email = value
            End Set
        End Property

        <StoredProcParameter("@EmailOther")>
        Public Property _EmailOther() As String
            Get
                Return m_EmailOther
            End Get
            Set(ByVal value As String)
                m_EmailOther = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Contact

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getContactbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Contact

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getContactbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Contact)

            Dim _ContactList As List(Of Contact)
            _ContactList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getContactTable"))
            Return _ContactList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Contact)

            Dim _ContactList As List(Of Contact)
            _ContactList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getContactTable"))
            Return _ContactList

        End Function

        Public Shared Sub SaveAll(ByVal ContactList As List(Of Contact))

            For Each _Contact As Contact In ContactList
                SaveRecord(_Contact)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal ContactList As List(Of Contact))

            For Each _Contact As Contact In ContactList
                SaveRecord(ConnectionString, _Contact)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Contact As Contact) As Guid
            Contact.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Contact, "upsertContact")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Contact As Contact) As Guid
            Contact.SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Contact, "upsertContact")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteContactbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteContactbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertContact")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Me, "upsertContact")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Contact

            Dim _C As Contact = Nothing

            If DR IsNot Nothing Then
                _C = New Contact()
                With DR
                    _C.IsNew = False
                    _C.IsDeleted = False
                    _C._ID = GetGUID(DR("ID"))
                    _C._Forename = DR("forename").ToString.Trim
                    _C._Surname = DR("surname").ToString.Trim
                    _C._Fullname = DR("fullname").ToString.Trim
                    _C._Company = DR("company").ToString.Trim
                    _C._Jobtitle = DR("jobtitle").ToString.Trim
                    _C._Relationship = DR("relationship").ToString.Trim
                    _C._TelMobile = DR("tel_mobile").ToString.Trim
                    _C._TelHome = DR("tel_home").ToString.Trim
                    _C._TelWork = DR("tel_work").ToString.Trim
                    _C._TelOther = DR("tel_other").ToString.Trim
                    _C._TelAll = DR("tel_all").ToString.Trim
                    _C._Email = DR("email").ToString.Trim
                    _C._EmailOther = DR("email_other").ToString.Trim

                End With
            End If

            Return _C

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Contact)

            Dim _ContactList As New List(Of Contact)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _ContactList.Add(PropertiesFromData(_DR))
            Next

            Return _ContactList

        End Function


#End Region

    End Class

End Namespace
