﻿'*****************************************************
'Generated 09/03/2017 16:01:12 using Version 1.17.2.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class PlantActivity
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_PlantId As Guid?
        Dim m_ActText As String
        Dim m_ActRef1 As String
        Dim m_ActRef2 As String
        Dim m_ActRef3 As String
        Dim m_ActUser As String
        Dim m_ActStamp As DateTime?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._PlantId = Nothing
                ._ActText = ""
                ._ActRef1 = ""
                ._ActRef2 = ""
                ._ActRef3 = ""
                ._ActUser = ""
                ._ActStamp = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._PlantId = Nothing
                ._ActText = ""
                ._ActRef1 = ""
                ._ActRef2 = ""
                ._ActRef3 = ""
                ._ActUser = ""
                ._ActStamp = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@PlantId")> _
        Public Property _PlantId() As Guid?
            Get
                Return m_PlantId
            End Get
            Set(ByVal value As Guid?)
                m_PlantId = value
            End Set
        End Property

        <StoredProcParameter("@ActText")> _
        Public Property _ActText() As String
            Get
                Return m_ActText
            End Get
            Set(ByVal value As String)
                m_ActText = value
            End Set
        End Property

        <StoredProcParameter("@ActRef1")> _
        Public Property _ActRef1() As String
            Get
                Return m_ActRef1
            End Get
            Set(ByVal value As String)
                m_ActRef1 = value
            End Set
        End Property

        <StoredProcParameter("@ActRef2")> _
        Public Property _ActRef2() As String
            Get
                Return m_ActRef2
            End Get
            Set(ByVal value As String)
                m_ActRef2 = value
            End Set
        End Property

        <StoredProcParameter("@ActRef3")> _
        Public Property _ActRef3() As String
            Get
                Return m_ActRef3
            End Get
            Set(ByVal value As String)
                m_ActRef3 = value
            End Set
        End Property

        <StoredProcParameter("@ActUser")> _
        Public Property _ActUser() As String
            Get
                Return m_ActUser
            End Get
            Set(ByVal value As String)
                m_ActUser = value
            End Set
        End Property

        <StoredProcParameter("@ActStamp")> _
        Public Property _ActStamp() As DateTime?
            Get
                Return m_ActStamp
            End Get
            Set(ByVal value As DateTime?)
                m_ActStamp = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As PlantActivity

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getPlantActivitybyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As PlantActivity

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getPlantActivitybyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of PlantActivity)

            Dim _PlantActivityList As List(Of PlantActivity)
            _PlantActivityList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getPlantActivityTable"))
            Return _PlantActivityList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of PlantActivity)

            Dim _PlantActivityList As List(Of PlantActivity)
            _PlantActivityList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getPlantActivityTable"))
            Return _PlantActivityList

        End Function

        Public Shared Sub SaveAll(ByVal PlantActivityList As List(Of PlantActivity))

            For Each _PlantActivity As PlantActivity In PlantActivityList
                SaveRecord(_PlantActivity)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal PlantActivityList As List(Of PlantActivity))

            For Each _PlantActivity As PlantActivity In PlantActivityList
                SaveRecord(ConnectionString, _PlantActivity)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal PlantActivity As PlantActivity) As Guid
            PlantActivity.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, PlantActivity, "upsertPlantActivity")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal PlantActivity As PlantActivity) As Guid
            PlantActivity.SetDefaultValues()
            DAL.SaveRecord(ConnectionString, PlantActivity, "upsertPlantActivity")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deletePlantActivitybyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deletePlantActivitybyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertPlantActivity")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Me, "upsertPlantActivity")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As PlantActivity

            Dim _P As PlantActivity = Nothing

            If DR IsNot Nothing Then
                _P = New PlantActivity()
                With DR
                    _P.IsNew = False
                    _P.IsDeleted = False
                    _P._ID = GetGUID(DR("ID"))
                    _P._PlantId = GetGUID(DR("plant_id"))
                    _P._ActText = DR("act_text").ToString.Trim
                    _P._ActRef1 = DR("act_ref1").ToString.Trim
                    _P._ActRef2 = DR("act_ref2").ToString.Trim
                    _P._ActRef3 = DR("act_ref3").ToString.Trim
                    _P._ActUser = DR("act_user").ToString.Trim
                    _P._ActStamp = GetDateTime(DR("act_stamp"))

                End With
            End If

            Return _P

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of PlantActivity)

            Dim _PlantActivityList As New List(Of PlantActivity)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _PlantActivityList.Add(PropertiesFromData(_DR))
            Next

            Return _PlantActivityList

        End Function


#End Region

    End Class

End Namespace
