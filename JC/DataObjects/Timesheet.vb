﻿'*****************************************************
'Generated 24/07/2017 23:48:17 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Timesheet
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_TsDate As Date?
        Dim m_TsEmpId As Guid?
        Dim m_TsEmpRate As Decimal
        Dim m_TsVanId As Guid?
        Dim m_TsJobId As Guid?
        Dim m_TsWhat As String
        Dim m_TsDayStart As TimeSpan?
        Dim m_TsDayFinish As TimeSpan?
        Dim m_TsDayDuration As Decimal
        Dim m_TsJobStart As TimeSpan?
        Dim m_TsJobFinish As TimeSpan?
        Dim m_TsJobDuration As Decimal
        Dim m_TsCost As Decimal

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._TsDate = Nothing
                ._TsEmpId = Nothing
                ._TsEmpRate = 0
                ._TsVanId = Nothing
                ._TsJobId = Nothing
                ._TsWhat = ""
                ._TsDayStart = Nothing
                ._TsDayFinish = Nothing
                ._TsDayDuration = 0
                ._TsJobStart = Nothing
                ._TsJobFinish = Nothing
                ._TsJobDuration = 0
                ._TsCost = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._TsDate = Nothing
                ._TsEmpId = Nothing
                ._TsEmpRate = 0
                ._TsVanId = Nothing
                ._TsJobId = Nothing
                ._TsWhat = ""
                ._TsDayStart = Nothing
                ._TsDayFinish = Nothing
                ._TsDayDuration = 0
                ._TsJobStart = Nothing
                ._TsJobFinish = Nothing
                ._TsJobDuration = 0
                ._TsCost = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@TsDate")>
        Public Property _TsDate() As Date?
            Get
                Return m_TsDate
            End Get
            Set(ByVal value As Date?)
                m_TsDate = value
            End Set
        End Property

        <StoredProcParameter("@TsEmpId")>
        Public Property _TsEmpId() As Guid?
            Get
                Return m_TsEmpId
            End Get
            Set(ByVal value As Guid?)
                m_TsEmpId = value
            End Set
        End Property

        <StoredProcParameter("@TsEmpRate")>
        Public Property _TsEmpRate() As Decimal
            Get
                Return m_TsEmpRate
            End Get
            Set(ByVal value As Decimal)
                m_TsEmpRate = value
            End Set
        End Property

        <StoredProcParameter("@TsVanId")>
        Public Property _TsVanId() As Guid?
            Get
                Return m_TsVanId
            End Get
            Set(ByVal value As Guid?)
                m_TsVanId = value
            End Set
        End Property

        <StoredProcParameter("@TsJobId")>
        Public Property _TsJobId() As Guid?
            Get
                Return m_TsJobId
            End Get
            Set(ByVal value As Guid?)
                m_TsJobId = value
            End Set
        End Property

        <StoredProcParameter("@TsWhat")>
        Public Property _TsWhat() As String
            Get
                Return m_TsWhat
            End Get
            Set(ByVal value As String)
                m_TsWhat = value
            End Set
        End Property

        <StoredProcParameter("@TsDayStart")>
        Public Property _TsDayStart() As TimeSpan?
            Get
                Return m_TsDayStart
            End Get
            Set(ByVal value As TimeSpan?)
                m_TsDayStart = value
            End Set
        End Property

        <StoredProcParameter("@TsDayFinish")>
        Public Property _TsDayFinish() As TimeSpan?
            Get
                Return m_TsDayFinish
            End Get
            Set(ByVal value As TimeSpan?)
                m_TsDayFinish = value
            End Set
        End Property

        <StoredProcParameter("@TsDayDuration")>
        Public Property _TsDayDuration() As Decimal
            Get
                Return m_TsDayDuration
            End Get
            Set(ByVal value As Decimal)
                m_TsDayDuration = value
            End Set
        End Property

        <StoredProcParameter("@TsJobStart")>
        Public Property _TsJobStart() As TimeSpan?
            Get
                Return m_TsJobStart
            End Get
            Set(ByVal value As TimeSpan?)
                m_TsJobStart = value
            End Set
        End Property

        <StoredProcParameter("@TsJobFinish")>
        Public Property _TsJobFinish() As TimeSpan?
            Get
                Return m_TsJobFinish
            End Get
            Set(ByVal value As TimeSpan?)
                m_TsJobFinish = value
            End Set
        End Property

        <StoredProcParameter("@TsJobDuration")>
        Public Property _TsJobDuration() As Decimal
            Get
                Return m_TsJobDuration
            End Get
            Set(ByVal value As Decimal)
                m_TsJobDuration = value
            End Set
        End Property

        <StoredProcParameter("@TsCost")>
        Public Property _TsCost() As Decimal
            Get
                Return m_TsCost
            End Get
            Set(ByVal value As Decimal)
                m_TsCost = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Timesheet

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getTimesheetbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Timesheet

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getTimesheetbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Timesheet)

            Dim _TimesheetList As List(Of Timesheet)
            _TimesheetList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getTimesheetTable"))
            Return _TimesheetList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Timesheet)

            Dim _TimesheetList As List(Of Timesheet)
            _TimesheetList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getTimesheetTable"))
            Return _TimesheetList

        End Function

        Public Shared Sub SaveAll(ByVal TimesheetList As List(Of Timesheet))

            For Each _Timesheet As Timesheet In TimesheetList
                SaveRecord(_Timesheet)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal TimesheetList As List(Of Timesheet))

            For Each _Timesheet As Timesheet In TimesheetList
                SaveRecord(ConnectionString, _Timesheet)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Timesheet As Timesheet) As Guid
            Timesheet.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Timesheet, "upsertTimesheet")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Timesheet As Timesheet) As Guid
            Timesheet.SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Timesheet, "upsertTimesheet")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteTimesheetbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteTimesheetbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertTimesheet")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Me, "upsertTimesheet")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Timesheet

            Dim _T As Timesheet = Nothing

            If DR IsNot Nothing Then
                _T = New Timesheet()
                With DR
                    _T.IsNew = False
                    _T.IsDeleted = False
                    _T._ID = GetGUID(DR("ID"))
                    _T._TsDate = GetDate(DR("ts_date"))
                    _T._TsEmpId = GetGUID(DR("ts_emp_id"))
                    _T._TsEmpRate = GetDecimal(DR("ts_emp_rate"))
                    _T._TsVanId = GetGUID(DR("ts_van_id"))
                    _T._TsJobId = GetGUID(DR("ts_job_id"))
                    _T._TsWhat = DR("ts_what").ToString.Trim
                    _T._TsDayStart = GetTimeSpan(DR("ts_day_start"))
                    _T._TsDayFinish = GetTimeSpan(DR("ts_day_finish"))
                    _T._TsDayDuration = GetDecimal(DR("ts_day_duration"))
                    _T._TsJobStart = GetTimeSpan(DR("ts_job_start"))
                    _T._TsJobFinish = GetTimeSpan(DR("ts_job_finish"))
                    _T._TsJobDuration = GetDecimal(DR("ts_job_duration"))
                    _T._TsCost = GetDecimal(DR("ts_cost"))

                End With
            End If

            Return _T

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Timesheet)

            Dim _TimesheetList As New List(Of Timesheet)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _TimesheetList.Add(PropertiesFromData(_DR))
            Next

            Return _TimesheetList

        End Function


#End Region

    End Class

End Namespace
