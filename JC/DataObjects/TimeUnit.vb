﻿'*****************************************************
'Generated 27/07/2017 20:17:31 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class TimeUnit
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_UnitType As String
        Dim m_UnitName As String
        Dim m_DateYear As Integer
        Dim m_DateStart As Date?
        Dim m_DateEnd As Date?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._UnitType = ""
                ._UnitName = ""
                ._DateYear = 0
                ._DateStart = Nothing
                ._DateEnd = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._UnitType = ""
                ._UnitName = ""
                ._DateYear = 0
                ._DateStart = Nothing
                ._DateEnd = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@UnitType")>
        Public Property _UnitType() As String
            Get
                Return m_UnitType
            End Get
            Set(ByVal value As String)
                m_UnitType = value
            End Set
        End Property

        <StoredProcParameter("@UnitName")>
        Public Property _UnitName() As String
            Get
                Return m_UnitName
            End Get
            Set(ByVal value As String)
                m_UnitName = value
            End Set
        End Property

        <StoredProcParameter("@DateYear")>
        Public Property _DateYear() As Integer
            Get
                Return m_DateYear
            End Get
            Set(ByVal value As Integer)
                m_DateYear = value
            End Set
        End Property

        <StoredProcParameter("@DateStart")>
        Public Property _DateStart() As Date?
            Get
                Return m_DateStart
            End Get
            Set(ByVal value As Date?)
                m_DateStart = value
            End Set
        End Property

        <StoredProcParameter("@DateEnd")>
        Public Property _DateEnd() As Date?
            Get
                Return m_DateEnd
            End Get
            Set(ByVal value As Date?)
                m_DateEnd = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As TimeUnit

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getTimeUnitbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As TimeUnit

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getTimeUnitbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of TimeUnit)

            Dim _TimeUnitList As List(Of TimeUnit)
            _TimeUnitList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getTimeUnitTable"))
            Return _TimeUnitList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of TimeUnit)

            Dim _TimeUnitList As List(Of TimeUnit)
            _TimeUnitList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getTimeUnitTable"))
            Return _TimeUnitList

        End Function

        Public Shared Sub SaveAll(ByVal TimeUnitList As List(Of TimeUnit))

            For Each _TimeUnit As TimeUnit In TimeUnitList
                SaveRecord(_TimeUnit)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal TimeUnitList As List(Of TimeUnit))

            For Each _TimeUnit As TimeUnit In TimeUnitList
                SaveRecord(ConnectionString, _TimeUnit)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal TimeUnit As TimeUnit) As Guid
            TimeUnit.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, TimeUnit, "upsertTimeUnit")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal TimeUnit As TimeUnit) As Guid
            TimeUnit.SetDefaultValues()
            DAL.SaveRecord(ConnectionString, TimeUnit, "upsertTimeUnit")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteTimeUnitbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteTimeUnitbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertTimeUnit")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Me, "upsertTimeUnit")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As TimeUnit

            Dim _T As TimeUnit = Nothing

            If DR IsNot Nothing Then
                _T = New TimeUnit()
                With DR
                    _T.IsNew = False
                    _T.IsDeleted = False
                    _T._ID = GetGUID(DR("ID"))
                    _T._UnitType = DR("unit_type").ToString.Trim
                    _T._UnitName = DR("unit_name").ToString.Trim
                    _T._DateYear = GetInteger(DR("date_year"))
                    _T._DateStart = GetDate(DR("date_start"))
                    _T._DateEnd = GetDate(DR("date_end"))

                End With
            End If

            Return _T

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of TimeUnit)

            Dim _TimeUnitList As New List(Of TimeUnit)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _TimeUnitList.Add(PropertiesFromData(_DR))
            Next

            Return _TimeUnitList

        End Function


#End Region

    End Class

End Namespace
