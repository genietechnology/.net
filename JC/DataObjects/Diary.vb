﻿'*****************************************************
'Generated 28/07/2017 15:18:49 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Diary
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_DayDate As Date?
        Dim m_Weather As String
        Dim m_EmpId As Guid?
        Dim m_Status As String
        Dim m_JobId As Guid?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._DayDate = Nothing
                ._Weather = ""
                ._EmpId = Nothing
                ._Status = ""
                ._JobId = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._DayDate = Nothing
                ._Weather = ""
                ._EmpId = Nothing
                ._Status = ""
                ._JobId = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@DayDate")>
        Public Property _DayDate() As Date?
            Get
                Return m_DayDate
            End Get
            Set(ByVal value As Date?)
                m_DayDate = value
            End Set
        End Property

        <StoredProcParameter("@Weather")>
        Public Property _Weather() As String
            Get
                Return m_Weather
            End Get
            Set(ByVal value As String)
                m_Weather = value
            End Set
        End Property

        <StoredProcParameter("@EmpId")>
        Public Property _EmpId() As Guid?
            Get
                Return m_EmpId
            End Get
            Set(ByVal value As Guid?)
                m_EmpId = value
            End Set
        End Property

        <StoredProcParameter("@Status")>
        Public Property _Status() As String
            Get
                Return m_Status
            End Get
            Set(ByVal value As String)
                m_Status = value
            End Set
        End Property

        <StoredProcParameter("@JobId")>
        Public Property _JobId() As Guid?
            Get
                Return m_JobId
            End Get
            Set(ByVal value As Guid?)
                m_JobId = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Diary

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getDiarybyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Diary

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getDiarybyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Diary)

            Dim _DiaryList As List(Of Diary)
            _DiaryList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getDiaryTable"))
            Return _DiaryList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Diary)

            Dim _DiaryList As List(Of Diary)
            _DiaryList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getDiaryTable"))
            Return _DiaryList

        End Function

        Public Shared Sub SaveAll(ByVal DiaryList As List(Of Diary))

            For Each _Diary As Diary In DiaryList
                SaveRecord(_Diary)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal DiaryList As List(Of Diary))

            For Each _Diary As Diary In DiaryList
                SaveRecord(ConnectionString, _Diary)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Diary As Diary) As Guid
            Diary.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Diary, "upsertDiary")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Diary As Diary) As Guid
            Diary.SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Diary, "upsertDiary")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteDiarybyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteDiarybyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertDiary")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Me, "upsertDiary")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Diary

            Dim _D As Diary = Nothing

            If DR IsNot Nothing Then
                _D = New Diary()
                With DR
                    _D.IsNew = False
                    _D.IsDeleted = False
                    _D._ID = GetGUID(DR("ID"))
                    _D._DayDate = GetDate(DR("day_date"))
                    _D._Weather = DR("weather").ToString.Trim
                    _D._EmpId = GetGUID(DR("emp_id"))
                    _D._Status = DR("status").ToString.Trim
                    _D._JobId = GetGUID(DR("job_id"))

                End With
            End If

            Return _D

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Diary)

            Dim _DiaryList As New List(Of Diary)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _DiaryList.Add(PropertiesFromData(_DR))
            Next

            Return _DiaryList

        End Function


#End Region

    End Class

End Namespace
