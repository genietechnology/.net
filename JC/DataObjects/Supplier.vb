﻿'*****************************************************
'Generated 08/03/2017 00:27:10 using Version 1.17.2.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Supplier
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_SupCode As String
        Dim m_SupName As String
        Dim m_SupAddress As String
        Dim m_SupPostcode As String
        Dim m_SupContact As String
        Dim m_SupTel As String
        Dim m_SupEmail As String
        Dim m_SupSageRef As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._SupCode = ""
                ._SupName = ""
                ._SupAddress = ""
                ._SupPostcode = ""
                ._SupContact = ""
                ._SupTel = ""
                ._SupEmail = ""
                ._SupSageRef = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._SupCode = ""
                ._SupName = ""
                ._SupAddress = ""
                ._SupPostcode = ""
                ._SupContact = ""
                ._SupTel = ""
                ._SupEmail = ""
                ._SupSageRef = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@SupCode")> _
        Public Property _SupCode() As String
            Get
                Return m_SupCode
            End Get
            Set(ByVal value As String)
                m_SupCode = value
            End Set
        End Property

        <StoredProcParameter("@SupName")> _
        Public Property _SupName() As String
            Get
                Return m_SupName
            End Get
            Set(ByVal value As String)
                m_SupName = value
            End Set
        End Property

        <StoredProcParameter("@SupAddress")> _
        Public Property _SupAddress() As String
            Get
                Return m_SupAddress
            End Get
            Set(ByVal value As String)
                m_SupAddress = value
            End Set
        End Property

        <StoredProcParameter("@SupPostcode")> _
        Public Property _SupPostcode() As String
            Get
                Return m_SupPostcode
            End Get
            Set(ByVal value As String)
                m_SupPostcode = value
            End Set
        End Property

        <StoredProcParameter("@SupContact")> _
        Public Property _SupContact() As String
            Get
                Return m_SupContact
            End Get
            Set(ByVal value As String)
                m_SupContact = value
            End Set
        End Property

        <StoredProcParameter("@SupTel")> _
        Public Property _SupTel() As String
            Get
                Return m_SupTel
            End Get
            Set(ByVal value As String)
                m_SupTel = value
            End Set
        End Property

        <StoredProcParameter("@SupEmail")> _
        Public Property _SupEmail() As String
            Get
                Return m_SupEmail
            End Get
            Set(ByVal value As String)
                m_SupEmail = value
            End Set
        End Property

        <StoredProcParameter("@SupSageRef")> _
        Public Property _SupSageRef() As String
            Get
                Return m_SupSageRef
            End Get
            Set(ByVal value As String)
                m_SupSageRef = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Supplier

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getSupplierbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Supplier

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getSupplierbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Supplier)

            Dim _SupplierList As List(Of Supplier)
            _SupplierList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getSupplierTable"))
            Return _SupplierList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Supplier)

            Dim _SupplierList As List(Of Supplier)
            _SupplierList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getSupplierTable"))
            Return _SupplierList

        End Function

        Public Shared Sub SaveAll(ByVal SupplierList As List(Of Supplier))

            For Each _Supplier As Supplier In SupplierList
                SaveRecord(_Supplier)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal SupplierList As List(Of Supplier))

            For Each _Supplier As Supplier In SupplierList
                SaveRecord(ConnectionString, _Supplier)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Supplier As Supplier) As Guid
            Supplier.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Supplier, "upsertSupplier")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Supplier As Supplier) As Guid
            Supplier.SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Supplier, "upsertSupplier")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteSupplierbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteSupplierbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertSupplier")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Me, "upsertSupplier")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Supplier

            Dim _S As Supplier = Nothing

            If DR IsNot Nothing Then
                _S = New Supplier()
                With DR
                    _S.IsNew = False
                    _S.IsDeleted = False
                    _S._ID = GetGUID(DR("ID"))
                    _S._SupCode = DR("sup_code").ToString.Trim
                    _S._SupName = DR("sup_name").ToString.Trim
                    _S._SupAddress = DR("sup_address").ToString.Trim
                    _S._SupPostcode = DR("sup_postcode").ToString.Trim
                    _S._SupContact = DR("sup_contact").ToString.Trim
                    _S._SupTel = DR("sup_tel").ToString.Trim
                    _S._SupEmail = DR("sup_email").ToString.Trim
                    _S._SupSageRef = DR("sup_sage_ref").ToString.Trim

                End With
            End If

            Return _S

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Supplier)

            Dim _SupplierList As New List(Of Supplier)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _SupplierList.Add(PropertiesFromData(_DR))
            Next

            Return _SupplierList

        End Function


#End Region

    End Class

End Namespace
