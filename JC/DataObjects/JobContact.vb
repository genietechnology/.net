﻿'*****************************************************
'Generated 21/11/2017 14:51:15 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class JobContact
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_JobId As Guid?
        Dim m_OldName As String
        Dim m_OldNo As String
        Dim m_OldNotes As String
        Dim m_ContactId As Guid?
        Dim m_cPrimary As Boolean
        Dim m_cBilling As Boolean

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._JobId = Nothing
                ._OldName = ""
                ._OldNo = ""
                ._OldNotes = ""
                ._ContactId = Nothing
                ._cPrimary = False
                ._cBilling = False

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._JobId = Nothing
                ._OldName = ""
                ._OldNo = ""
                ._OldNotes = ""
                ._ContactId = Nothing
                ._cPrimary = False
                ._cBilling = False

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@JobId")>
        Public Property _JobId() As Guid?
            Get
                Return m_JobId
            End Get
            Set(ByVal value As Guid?)
                m_JobId = value
            End Set
        End Property

        <StoredProcParameter("@OldName")>
        Public Property _OldName() As String
            Get
                Return m_OldName
            End Get
            Set(ByVal value As String)
                m_OldName = value
            End Set
        End Property

        <StoredProcParameter("@OldNo")>
        Public Property _OldNo() As String
            Get
                Return m_OldNo
            End Get
            Set(ByVal value As String)
                m_OldNo = value
            End Set
        End Property

        <StoredProcParameter("@OldNotes")>
        Public Property _OldNotes() As String
            Get
                Return m_OldNotes
            End Get
            Set(ByVal value As String)
                m_OldNotes = value
            End Set
        End Property

        <StoredProcParameter("@ContactId")>
        Public Property _ContactId() As Guid?
            Get
                Return m_ContactId
            End Get
            Set(ByVal value As Guid?)
                m_ContactId = value
            End Set
        End Property

        <StoredProcParameter("@cPrimary")>
        Public Property _cPrimary() As Boolean
            Get
                Return m_cPrimary
            End Get
            Set(ByVal value As Boolean)
                m_cPrimary = value
            End Set
        End Property

        <StoredProcParameter("@cBilling")>
        Public Property _cBilling() As Boolean
            Get
                Return m_cBilling
            End Get
            Set(ByVal value As Boolean)
                m_cBilling = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As JobContact

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getJobContactbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As JobContact

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getJobContactbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of JobContact)

            Dim _JobContactList As List(Of JobContact)
            _JobContactList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getJobContactTable"))
            Return _JobContactList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of JobContact)

            Dim _JobContactList As List(Of JobContact)
            _JobContactList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getJobContactTable"))
            Return _JobContactList

        End Function

        Public Shared Sub SaveAll(ByVal JobContactList As List(Of JobContact))

            For Each _JobContact As JobContact In JobContactList
                SaveRecord(_JobContact)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal JobContactList As List(Of JobContact))

            For Each _JobContact As JobContact In JobContactList
                SaveRecord(ConnectionString, _JobContact)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal JobContact As JobContact) As Guid
            JobContact.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, JobContact, "upsertJobContact")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal JobContact As JobContact) As Guid
            JobContact.SetDefaultValues()
            DAL.SaveRecord(ConnectionString, JobContact, "upsertJobContact")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteJobContactbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteJobContactbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertJobContact")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Me, "upsertJobContact")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As JobContact

            Dim _J As JobContact = Nothing

            If DR IsNot Nothing Then
                _J = New JobContact()
                With DR
                    _J.IsNew = False
                    _J.IsDeleted = False
                    _J._ID = GetGUID(DR("ID"))
                    _J._JobId = GetGUID(DR("job_id"))
                    _J._OldName = DR("old_name").ToString.Trim
                    _J._OldNo = DR("old_no").ToString.Trim
                    _J._OldNotes = DR("old_notes").ToString.Trim
                    _J._ContactId = GetGUID(DR("contact_id"))
                    _J._cPrimary = GetBoolean(DR("c_primary"))
                    _J._cBilling = GetBoolean(DR("c_billing"))

                End With
            End If

            Return _J

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of JobContact)

            Dim _JobContactList As New List(Of JobContact)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _JobContactList.Add(PropertiesFromData(_DR))
            Next

            Return _JobContactList

        End Function


#End Region

    End Class

End Namespace
