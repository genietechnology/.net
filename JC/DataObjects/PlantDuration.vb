﻿'*****************************************************
'Generated 09/03/2017 16:08:49 using Version 1.17.2.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class PlantDuration
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_PlantId As Guid?
        Dim m_JobId As Guid?
        Dim m_DateFrom As Date?
        Dim m_DateTo As Date?
        Dim m_Days As Integer
        Dim m_Cost As Decimal

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._PlantId = Nothing
                ._JobId = Nothing
                ._DateFrom = Nothing
                ._DateTo = Nothing
                ._Days = 0
                ._Cost = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._PlantId = Nothing
                ._JobId = Nothing
                ._DateFrom = Nothing
                ._DateTo = Nothing
                ._Days = 0
                ._Cost = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@PlantId")> _
        Public Property _PlantId() As Guid?
            Get
                Return m_PlantId
            End Get
            Set(ByVal value As Guid?)
                m_PlantId = value
            End Set
        End Property

        <StoredProcParameter("@JobId")> _
        Public Property _JobId() As Guid?
            Get
                Return m_JobId
            End Get
            Set(ByVal value As Guid?)
                m_JobId = value
            End Set
        End Property

        <StoredProcParameter("@DateFrom")> _
        Public Property _DateFrom() As Date?
            Get
                Return m_DateFrom
            End Get
            Set(ByVal value As Date?)
                m_DateFrom = value
            End Set
        End Property

        <StoredProcParameter("@DateTo")> _
        Public Property _DateTo() As Date?
            Get
                Return m_DateTo
            End Get
            Set(ByVal value As Date?)
                m_DateTo = value
            End Set
        End Property

        <StoredProcParameter("@Days")> _
        Public Property _Days() As Integer
            Get
                Return m_Days
            End Get
            Set(ByVal value As Integer)
                m_Days = value
            End Set
        End Property

        <StoredProcParameter("@Cost")> _
        Public Property _Cost() As Decimal
            Get
                Return m_Cost
            End Get
            Set(ByVal value As Decimal)
                m_Cost = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As PlantDuration

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getPlantDurationbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As PlantDuration

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getPlantDurationbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of PlantDuration)

            Dim _PlantDurationList As List(Of PlantDuration)
            _PlantDurationList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getPlantDurationTable"))
            Return _PlantDurationList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of PlantDuration)

            Dim _PlantDurationList As List(Of PlantDuration)
            _PlantDurationList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getPlantDurationTable"))
            Return _PlantDurationList

        End Function

        Public Shared Sub SaveAll(ByVal PlantDurationList As List(Of PlantDuration))

            For Each _PlantDuration As PlantDuration In PlantDurationList
                SaveRecord(_PlantDuration)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal PlantDurationList As List(Of PlantDuration))

            For Each _PlantDuration As PlantDuration In PlantDurationList
                SaveRecord(ConnectionString, _PlantDuration)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal PlantDuration As PlantDuration) As Guid
            PlantDuration.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, PlantDuration, "upsertPlantDuration")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal PlantDuration As PlantDuration) As Guid
            PlantDuration.SetDefaultValues()
            DAL.SaveRecord(ConnectionString, PlantDuration, "upsertPlantDuration")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deletePlantDurationbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deletePlantDurationbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertPlantDuration")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Me, "upsertPlantDuration")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As PlantDuration

            Dim _P As PlantDuration = Nothing

            If DR IsNot Nothing Then
                _P = New PlantDuration()
                With DR
                    _P.IsNew = False
                    _P.IsDeleted = False
                    _P._ID = GetGUID(DR("ID"))
                    _P._PlantId = GetGUID(DR("plant_id"))
                    _P._JobId = GetGUID(DR("job_id"))
                    _P._DateFrom = GetDate(DR("date_from"))
                    _P._DateTo = GetDate(DR("date_to"))
                    _P._Days = GetInteger(DR("days"))
                    _P._Cost = GetDecimal(DR("cost"))

                End With
            End If

            Return _P

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of PlantDuration)

            Dim _PlantDurationList As New List(Of PlantDuration)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _PlantDurationList.Add(PropertiesFromData(_DR))
            Next

            Return _PlantDurationList

        End Function


#End Region

    End Class

End Namespace
