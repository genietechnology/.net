﻿'*****************************************************
'Generated 22/11/2017 16:06:42 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Material
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_BookedDate As Date?
        Dim m_JobId As Guid?
        Dim m_StockId As Guid?
        Dim m_Qty As Decimal
        Dim m_Factor As Decimal
        Dim m_UnitPrice As Decimal
        Dim m_TotalPrice As Decimal

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._BookedDate = Nothing
                ._JobId = Nothing
                ._StockId = Nothing
                ._Qty = 0
                ._Factor = 0
                ._UnitPrice = 0
                ._TotalPrice = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._BookedDate = Nothing
                ._JobId = Nothing
                ._StockId = Nothing
                ._Qty = 0
                ._Factor = 0
                ._UnitPrice = 0
                ._TotalPrice = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@BookedDate")>
        Public Property _BookedDate() As Date?
            Get
                Return m_BookedDate
            End Get
            Set(ByVal value As Date?)
                m_BookedDate = value
            End Set
        End Property

        <StoredProcParameter("@JobId")>
        Public Property _JobId() As Guid?
            Get
                Return m_JobId
            End Get
            Set(ByVal value As Guid?)
                m_JobId = value
            End Set
        End Property

        <StoredProcParameter("@StockId")>
        Public Property _StockId() As Guid?
            Get
                Return m_StockId
            End Get
            Set(ByVal value As Guid?)
                m_StockId = value
            End Set
        End Property

        <StoredProcParameter("@Qty")>
        Public Property _Qty() As Decimal
            Get
                Return m_Qty
            End Get
            Set(ByVal value As Decimal)
                m_Qty = value
            End Set
        End Property

        <StoredProcParameter("@Factor")>
        Public Property _Factor() As Decimal
            Get
                Return m_Factor
            End Get
            Set(ByVal value As Decimal)
                m_Factor = value
            End Set
        End Property

        <StoredProcParameter("@UnitPrice")>
        Public Property _UnitPrice() As Decimal
            Get
                Return m_UnitPrice
            End Get
            Set(ByVal value As Decimal)
                m_UnitPrice = value
            End Set
        End Property

        <StoredProcParameter("@TotalPrice")>
        Public Property _TotalPrice() As Decimal
            Get
                Return m_TotalPrice
            End Get
            Set(ByVal value As Decimal)
                m_TotalPrice = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Material

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getMaterialbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Material

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getMaterialbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of Material)

            Dim _MaterialList As List(Of Material)
            _MaterialList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getMaterialTable"))
            Return _MaterialList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Material)

            Dim _MaterialList As List(Of Material)
            _MaterialList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getMaterialTable"))
            Return _MaterialList

        End Function

        Public Shared Sub SaveAll(ByVal MaterialList As List(Of Material))

            For Each _Material As Material In MaterialList
                SaveRecord(_Material)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal MaterialList As List(Of Material))

            For Each _Material As Material In MaterialList
                SaveRecord(ConnectionString, _Material)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Material As Material) As Guid
            Material.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Material, "upsertMaterial")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Material As Material) As Guid
            Material.SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Material, "upsertMaterial")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteMaterialbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteMaterialbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertMaterial")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Me, "upsertMaterial")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Material

            Dim _M As Material = Nothing

            If DR IsNot Nothing Then
                _M = New Material()
                With DR
                    _M.IsNew = False
                    _M.IsDeleted = False
                    _M._ID = GetGUID(DR("ID"))
                    _M._BookedDate = GetDate(DR("booked_date"))
                    _M._JobId = GetGUID(DR("job_id"))
                    _M._StockId = GetGUID(DR("stock_id"))
                    _M._Qty = GetDecimal(DR("qty"))
                    _M._Factor = GetDecimal(DR("factor"))
                    _M._UnitPrice = GetDecimal(DR("unit_price"))
                    _M._TotalPrice = GetDecimal(DR("total_price"))

                End With
            End If

            Return _M

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Material)

            Dim _MaterialList As New List(Of Material)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _MaterialList.Add(PropertiesFromData(_DR))
            Next

            Return _MaterialList

        End Function


#End Region

    End Class

End Namespace
