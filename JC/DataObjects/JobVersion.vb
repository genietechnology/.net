﻿'*****************************************************
'Generated 02/03/2017 20:51:29 using Version 1.15.8.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class JobVersion
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_JobId As Guid?
        Dim m_VerDate As Date?
        Dim m_VerReason As String
        Dim m_VerValue As Decimal
        Dim m_VerUser As String
        Dim m_VerStamp As DateTime?
        Dim m_VerNotes As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._JobId = Nothing
                ._VerDate = Nothing
                ._VerReason = ""
                ._VerValue = 0
                ._VerUser = ""
                ._VerStamp = Nothing
                ._VerNotes = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._JobId = Nothing
                ._VerDate = Nothing
                ._VerReason = ""
                ._VerValue = 0
                ._VerUser = ""
                ._VerStamp = Nothing
                ._VerNotes = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@JobId")> _
        Public Property _JobId() As Guid?
            Get
                Return m_JobId
            End Get
            Set(ByVal value As Guid?)
                m_JobId = value
            End Set
        End Property

        <StoredProcParameter("@VerDate")> _
        Public Property _VerDate() As Date?
            Get
                Return m_VerDate
            End Get
            Set(ByVal value As Date?)
                m_VerDate = value
            End Set
        End Property

        <StoredProcParameter("@VerReason")> _
        Public Property _VerReason() As String
            Get
                Return m_VerReason
            End Get
            Set(ByVal value As String)
                m_VerReason = value
            End Set
        End Property

        <StoredProcParameter("@VerValue")> _
        Public Property _VerValue() As Decimal
            Get
                Return m_VerValue
            End Get
            Set(ByVal value As Decimal)
                m_VerValue = value
            End Set
        End Property

        <StoredProcParameter("@VerUser")> _
        Public Property _VerUser() As String
            Get
                Return m_VerUser
            End Get
            Set(ByVal value As String)
                m_VerUser = value
            End Set
        End Property

        <StoredProcParameter("@VerStamp")> _
        Public Property _VerStamp() As DateTime?
            Get
                Return m_VerStamp
            End Get
            Set(ByVal value As DateTime?)
                m_VerStamp = value
            End Set
        End Property

        <StoredProcParameter("@VerNotes")> _
        Public Property _VerNotes() As String
            Get
                Return m_VerNotes
            End Get
            Set(ByVal value As String)
                m_VerNotes = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As JobVersion

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getJobVersionbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of JobVersion)

            Dim _JobVersionList As List(Of JobVersion)
            _JobVersionList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getJobVersionTable"))
            Return _JobVersionList

        End Function

        Public Shared Sub SaveAll(ByVal JobVersionList As List(Of JobVersion))

            For Each _JobVersion As JobVersion In JobVersionList
                SaveRecord(_JobVersion)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal JobVersion As JobVersion) As Guid
            JobVersion.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, JobVersion, "upsertJobVersion")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteJobVersionbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertJobVersion")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As JobVersion

            Dim _J As JobVersion = Nothing

            If DR IsNot Nothing Then
                _J = New JobVersion()
                With DR
                    _J.IsNew = False
                    _J.IsDeleted = False
                    _J._ID = GetGUID(DR("ID"))
                    _J._JobId = GetGUID(DR("job_id"))
                    _J._VerDate = GetDate(DR("ver_date"))
                    _J._VerReason = DR("ver_reason").ToString.Trim
                    _J._VerValue = GetDecimal(DR("ver_value"))
                    _J._VerUser = DR("ver_user").ToString.Trim
                    _J._VerStamp = GetDateTime(DR("ver_stamp"))
                    _J._VerNotes = DR("ver_notes").ToString.Trim

                End With
            End If

            Return _J

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of JobVersion)

            Dim _JobVersionList As New List(Of JobVersion)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _JobVersionList.Add(PropertiesFromData(_DR))
            Next

            Return _JobVersionList

        End Function


#End Region

    End Class

End Namespace
