﻿'*****************************************************
'Generated 03/03/2017 16:41:14 using Version 1.15.8.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class JobInvSale
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_JobId As Guid?
        Dim m_InvDate As Date?
        Dim m_InvNo As String
        Dim m_InvNet As Decimal
        Dim m_InvNotes As String
        Dim m_InvUser As String
        Dim m_InvStamp As DateTime?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._JobId = Nothing
                ._InvDate = Nothing
                ._InvNo = ""
                ._InvNet = 0
                ._InvNotes = ""
                ._InvUser = ""
                ._InvStamp = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._JobId = Nothing
                ._InvDate = Nothing
                ._InvNo = ""
                ._InvNet = 0
                ._InvNotes = ""
                ._InvUser = ""
                ._InvStamp = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@JobId")> _
        Public Property _JobId() As Guid?
            Get
                Return m_JobId
            End Get
            Set(ByVal value As Guid?)
                m_JobId = value
            End Set
        End Property

        <StoredProcParameter("@InvDate")> _
        Public Property _InvDate() As Date?
            Get
                Return m_InvDate
            End Get
            Set(ByVal value As Date?)
                m_InvDate = value
            End Set
        End Property

        <StoredProcParameter("@InvNo")> _
        Public Property _InvNo() As String
            Get
                Return m_InvNo
            End Get
            Set(ByVal value As String)
                m_InvNo = value
            End Set
        End Property

        <StoredProcParameter("@InvNet")> _
        Public Property _InvNet() As Decimal
            Get
                Return m_InvNet
            End Get
            Set(ByVal value As Decimal)
                m_InvNet = value
            End Set
        End Property

        <StoredProcParameter("@InvNotes")> _
        Public Property _InvNotes() As String
            Get
                Return m_InvNotes
            End Get
            Set(ByVal value As String)
                m_InvNotes = value
            End Set
        End Property

        <StoredProcParameter("@InvUser")> _
        Public Property _InvUser() As String
            Get
                Return m_InvUser
            End Get
            Set(ByVal value As String)
                m_InvUser = value
            End Set
        End Property

        <StoredProcParameter("@InvStamp")> _
        Public Property _InvStamp() As DateTime?
            Get
                Return m_InvStamp
            End Get
            Set(ByVal value As DateTime?)
                m_InvStamp = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As JobInvSale

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getJobInvSalebyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of JobInvSale)

            Dim _JobInvSaleList As List(Of JobInvSale)
            _JobInvSaleList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getJobInvSaleTable"))
            Return _JobInvSaleList

        End Function

        Public Shared Sub SaveAll(ByVal JobInvSaleList As List(Of JobInvSale))

            For Each _JobInvSale As JobInvSale In JobInvSaleList
                SaveRecord(_JobInvSale)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal JobInvSale As JobInvSale) As Guid
            JobInvSale.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, JobInvSale, "upsertJobInvSale")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteJobInvSalebyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertJobInvSale")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As JobInvSale

            Dim _J As JobInvSale = Nothing

            If DR IsNot Nothing Then
                _J = New JobInvSale()
                With DR
                    _J.IsNew = False
                    _J.IsDeleted = False
                    _J._ID = GetGUID(DR("ID"))
                    _J._JobId = GetGUID(DR("job_id"))
                    _J._InvDate = GetDate(DR("inv_date"))
                    _J._InvNo = DR("inv_no").ToString.Trim
                    _J._InvNet = GetDecimal(DR("inv_net"))
                    _J._InvNotes = DR("inv_notes").ToString.Trim
                    _J._InvUser = DR("inv_user").ToString.Trim
                    _J._InvStamp = GetDateTime(DR("inv_stamp"))

                End With
            End If

            Return _J

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of JobInvSale)

            Dim _JobInvSaleList As New List(Of JobInvSale)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _JobInvSaleList.Add(PropertiesFromData(_DR))
            Next

            Return _JobInvSaleList

        End Function


#End Region

    End Class

End Namespace
