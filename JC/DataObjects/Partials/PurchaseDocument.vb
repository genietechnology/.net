﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Partial Class PurchaseDocument

        Public Shared Sub MoveToAnotherJob(ByVal DocumentID As Guid?, ByVal OldJobID As Guid?, ByVal NewJobID As Guid?)

            Dim _D As PurchaseDocument = Business.PurchaseDocument.RetrieveByDocAndTargetID(DocumentID, OldJobID)
            If _D IsNot Nothing Then

                Dim _OldJob As Business.Job = Business.Job.RetreiveByID(OldJobID.Value)
                Dim _NewJob As Business.Job = Business.Job.RetreiveByID(NewJobID.Value)
                If _OldJob IsNot Nothing AndAlso _NewJob IsNot Nothing Then

                    _D._TargetId = NewJobID
                    _D.Store()

                    Dim _New As String = _NewJob._JobNo.ToString + " - " + _NewJob._RskFullname
                    Business.Job.CreateActivity(OldJobID.Value, "Moved Purchase Document {" + DocumentID.ToString + "} to Job " + _New)

                    Dim _Old As String = _OldJob._JobNo.ToString + " - " + _OldJob._RskFullname
                    Business.Job.CreateActivity(NewJobID.Value, "Moved Purchase Document {" + DocumentID.ToString + "} from Job " + _Old)

                End If

            End If
        End Sub

        Public Shared Sub DeleteFromJob(ByVal DocumentID As Guid?, ByVal CurrentJobID As Guid?)

            Dim _Job As Business.Job = Business.Job.RetreiveByID(CurrentJobID.Value)
            If _Job IsNot Nothing Then

                Dim _SQL As String = ""
                _SQL += "delete from PurchaseDocuments where doc_id = '" + DocumentID.ToString + "' and target_id = '" + CurrentJobID.ToString + "'"
                DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                Dim _Desc As String = _Job._JobNo.ToString + " - " + _Job._RskFullname
                Business.Job.CreateActivity(CurrentJobID.Value, "Deleted Purchase Document {" + DocumentID.ToString + "} from Job " + _Desc)

            End If

        End Sub

        Public Shared Function RetrieveByDocAndTargetID(ByVal DocumentID As Guid?, ByVal TargetID As Guid?) As PurchaseDocument
            Dim _SQL As String = "select * from PurchaseDocuments where doc_id = '" + DocumentID.ToString + "' and target_id = '" + TargetID.ToString + "'"
            Return PropertiesFromData(DAL.GetRowfromSQL(Session.ConnectionString, _SQL))
        End Function

        Public Shared Function RetrieveTargetIDFromDocumentID(ByVal DocumentID As Guid?) As String

            Dim _Return As String = ""
            Dim _SQL As String = "select target_id from PurchaseDocuments where doc_id = '" + DocumentID.ToString + "'"
            Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If _DT IsNot Nothing AndAlso _DT.Rows.Count > 0 Then
                If _DT.Rows.Count = 1 Then
                    _Return = _DT.Rows(0).Item("target_id").ToString
                Else
                    _Return = "MULTIPLE"
                End If
            End If

            _DT.Dispose()
            _DT = Nothing

            Return _Return

        End Function

    End Class

End Namespace

