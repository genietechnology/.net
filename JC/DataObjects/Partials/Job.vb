﻿Option Strict On

Imports Care.Global
Imports Care.Data
Imports Care.Shared

Namespace Business

    Partial Class Job

#Region "Calculation Variables"

        Private m_Linked As Boolean

        Private m_VerCount As Integer
        Private m_VerTotal As Decimal

        Private m_LabHours As Decimal
        Private m_LabCost As Decimal
        Private m_LabTotalOn As Decimal
        Private m_LabTotalOnMarkup As Decimal

        Private m_PurchasesCount As Integer
        Private m_PurchasesTotal As Decimal
        Private m_PurchasesOn As Decimal

        Private m_YardMaterialsCount As Integer
        Private m_YardMaterialsTotal As Decimal
        Private m_YardMaterialsOn As Decimal

        Private m_MatTotalOnMarkup As Decimal

        Private m_ConCount As Integer
        Private m_ConTotal As Decimal
        Private m_ConTotalOn As Decimal
        Private m_ConTotalOnMarkup As Decimal

        Private m_Leakage As Decimal
        Private m_LeakageMarkup As Decimal

        Private m_InvCount As Integer
        Private m_InvTotal As Decimal

        Private m_Total As Decimal
        Private m_TotalOn As Decimal
        Private m_GrandTotal As Decimal

        Private m_DateFilter As Boolean
        Private m_FromDate As Date?
        Private m_ToDate As Date?

#End Region

#Region "Calculation Properties"

        Public ReadOnly Property VersionCount() As Integer
            Get
                Return m_VerCount
            End Get
        End Property

        Public ReadOnly Property VersionTotal() As Decimal
            Get
                Return m_VerTotal
            End Get
        End Property

        Public ReadOnly Property LabourHours() As Decimal
            Get
                Return m_LabHours
            End Get
        End Property

        Public ReadOnly Property LabourCost() As Decimal
            Get
                Return m_LabCost
            End Get
        End Property

        Public ReadOnly Property LabourTotalOn() As Decimal
            Get
                Return m_LabTotalOn
            End Get
        End Property

        Public ReadOnly Property LabourTotalOnMarkup() As Decimal
            Get
                Return m_LabTotalOnMarkup
            End Get
        End Property

        Public ReadOnly Property PurchasesCount() As Integer
            Get
                Return m_PurchasesCount
            End Get
        End Property

        Public ReadOnly Property PurchasesCost() As Decimal
            Get
                Return m_PurchasesTotal
            End Get
        End Property

        Public ReadOnly Property PurchasesTotalOn() As Decimal
            Get
                Return m_PurchasesOn
            End Get
        End Property

        Public ReadOnly Property YardMaterialsCount() As Integer
            Get
                Return m_YardMaterialsCount
            End Get
        End Property

        Public ReadOnly Property YardMaterialsCost() As Decimal
            Get
                Return m_YardMaterialsTotal
            End Get
        End Property

        Public ReadOnly Property YardMaterialsTotalOn() As Decimal
            Get
                Return m_YardMaterialsOn
            End Get
        End Property

        Public ReadOnly Property MaterialsTotalOnMarkup() As Decimal
            Get
                Return m_MatTotalOnMarkup
            End Get
        End Property

        Public ReadOnly Property ContractorsCount() As Integer
            Get
                Return m_ConCount
            End Get
        End Property

        Public ReadOnly Property ContractorsCost() As Decimal
            Get
                Return m_ConTotal
            End Get
        End Property

        Public ReadOnly Property ContractorsTotalOn() As Decimal
            Get
                Return m_ConTotalOn
            End Get
        End Property

        Public ReadOnly Property ContractorsTotalOnMarkup() As Decimal
            Get
                Return m_ConTotalOnMarkup
            End Get
        End Property

        Public ReadOnly Property InvoiceCount() As Integer
            Get
                Return m_InvCount
            End Get
        End Property

        Public ReadOnly Property InvoiceTotal() As Decimal
            Get
                Return m_InvTotal
            End Get
        End Property

        Public ReadOnly Property Leakage() As Decimal
            Get
                Return m_Leakage
            End Get
        End Property

        Public ReadOnly Property LeakageMarkup() As Decimal
            Get
                Return m_LeakageMarkup
            End Get
        End Property

        Public ReadOnly Property TotalCost() As Decimal
            Get
                Return m_Total
            End Get
        End Property

        Public ReadOnly Property TotalCostOn() As Decimal
            Get
                Return m_TotalOn
            End Get
        End Property

        Public ReadOnly Property GrandTotal() As Decimal
            Get
                Return m_GrandTotal
            End Get
        End Property

#End Region

        Public Shared Sub DrillDown(ByVal JobID As Guid)
            Dim _frm As New frmJobs(JobID)
            _frm.Show()
        End Sub

        Public Sub LinkContact(ByVal ContactID As Guid)
            Dim _JC As New JobContact
            With _JC
                ._JobId = Me._ID.Value
                ._ContactId = ContactID
                .Store()
            End With
        End Sub

        Public Sub UnLinkContact(ByVal JobContactID As Guid)
            Dim _SQL As String = "delete from JobContacts where ID = '" + JobContactID.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)
        End Sub

        Public Sub SetPrimaryContact(ByVal JobContactID As Guid)

            Dim _SQL As String = ""
            _SQL += "update JobContacts set c_primary = 0 where job_id = '" + Me._ID.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

            _SQL += "update JobContacts set c_primary = 1 where ID = '" + JobContactID.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        End Sub

        Public Sub SetBillingContact(ByVal JobContactID As Guid)

            Dim _SQL As String = ""
            _SQL += "update JobContacts set c_billing = 0 where job_id = '" + Me._ID.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

            _SQL += "update JobContacts set c_billing = 1 where ID = '" + JobContactID.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        End Sub

        Public Sub SetNamesAndAddresses()

            Me._CusInits = GetInitials(Me._CusForename)
            Me._CusFullname = BuildFullName(Me._CusTitle, Me._CusInits, Me._CusSurname)

            Dim _CustomerLines As List(Of String) = Me._CusAddress.Split(CType(vbCrLf, Char())).ToList

            If _CustomerLines.Count > 0 Then
                Me._CusAddress1 = _CustomerLines.First
                Me._CusAddressLine = ConvertToOneLine(_CustomerLines)
            Else
                Me._CusAddress1 = ""
                Me._CusAddressLine = ""
            End If

            Me._RskInits = GetInitials(Me._RskForename)
            Me._RskFullname = BuildFullName(Me._RskTitle, Me._RskInits, Me._RskSurname)

            Dim _RiskLines As List(Of String) = Me._RskAddress.Split(CType(vbCrLf, Char())).ToList
            If _RiskLines.Count > 0 Then
                Me._RskAddress1 = _RiskLines.First
                Me._RskAddressLine = ConvertToOneLine(_RiskLines)
            Else
                Me._RskAddress1 = ""
                Me._RskAddressLine = ""
            End If

        End Sub

        Private Sub CreateDir(ByVal Path As String, ByVal DirectoryName As String)
            If IO.Directory.Exists(Path) Then
                IO.Directory.CreateDirectory(Path + "\" + DirectoryName)
            End If
        End Sub

        Public Sub CreateFolders(ByVal JobFolderPath As String)

            IO.Directory.CreateDirectory(JobFolderPath)

            CreateDir(JobFolderPath, "Invoicing Notes")
            CreateDir(JobFolderPath, "AJ Price - Quote for Job")
            CreateDir(JobFolderPath, "Chasing Notes")
            CreateDir(JobFolderPath, "Valuations")
            CreateDir(JobFolderPath, "Faxes")
            CreateDir(JobFolderPath, "Emails")
            CreateDir(JobFolderPath, "Letters")
            CreateDir(JobFolderPath, "Memos")
            CreateDir(JobFolderPath, "Photos")
            CreateDir(JobFolderPath, "SET QUOTES FROM SUBCONTRACTORS")
            CreateDir(JobFolderPath, "Completion Photos")

        End Sub

        Public Shared Function NextJobNumber() As Integer

            Dim _SQL As String = "select top 1 job_no from Jobs order by job_no desc"
            Dim _o As Object = DAL.ReturnScalar(Session.ConnectionString, _SQL)

            If _o Is Nothing Then
                Return 1
            Else
                Return ValueHandler.ConvertInteger(_o) + 1
            End If

        End Function

        Public Function IsLinked() As Boolean
            Dim _SQL As String = "select count(*) as 'count' from JobLinks where job_id = '" + Me._ID.ToString + "'"
            Dim _Count As Integer = ValueHandler.ConvertInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))
            If _Count > 0 Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Sub CalculateCosts()

            m_Linked = IsLinked()

            m_LabTotalOnMarkup = Me._MupLab
            m_MatTotalOnMarkup = Me._MupMat
            m_ConTotalOnMarkup = Me._MupSub
            m_LeakageMarkup = Me._MupLeak

            CalculateLabour()
            CalculatePurchases()
            CalculateMaterialsFromYard()
            CalculateSubCons()
            CalculateVersions()
            CalculateInvoices()

            'standard total
            m_Total = m_LabCost + m_PurchasesTotal + m_YardMaterialsTotal + m_ConTotal

            'total with uplifts
            m_TotalOn = m_LabTotalOn + m_PurchasesOn + m_YardMaterialsOn + m_ConTotalOn

            'leakage
            m_Leakage = ValueHandler.PercentageChange(m_TotalOn, m_LeakageMarkup)

            'grand total
            m_GrandTotal = m_TotalOn + m_Leakage

        End Sub

        Public Shared Sub CreateActivity(ByVal JobID As Guid, ByVal ActivityText As String)
            CreateActivity(JobID, ActivityText, Nothing)
        End Sub

        Public Shared Sub CreateActivity(ByVal JobID As Guid, ByVal ActivityText As String, ByVal KeyDate As Date?)

            Dim _a As New Business.JobActivity
            _a._JobId = JobID
            _a._Activity = ActivityText
            _a._KeyDate = KeyDate
            _a._UserCode = Session.CurrentUser.UserCode
            _a._UserName = Session.CurrentUser.FullName
            _a._Stamp = Now

            _a.Store()

        End Sub

#Region "Search Functions"

        Private Shared Function FindSQL() As String
            Dim _SQL As String = ""
            _SQL += "select job_no as 'Job No', job_status_code as 'Status', ins_company as 'Insurer', ins_ref as 'Ref', ins_claim as 'Claim Number',"
            _SQL += " CASE WHEN LEN(rsk_forename) > 0 THEN rsk_forename + ' ' + rsk_surname ELSE rsk_fullname END as 'Risk Name',"
            _SQL += " CASE WHEN LEN(cus_forename) > 0 THEN cus_forename + ' ' + cus_surname ELSE cus_fullname END as 'Customer Name',"
            _SQL += " rsk_address_line As 'Risk Address', all_tels as 'Contact Numbers'"
            _SQL += " from Jobs"
            Return _SQL
        End Function

        Public Shared Function FindAll() As String

            Dim _ReturnValue As String = ""
            Dim _Find As New GenericFind
            With _Find
                .Caption = "Find Job"
                .PopulateOnLoad = True
                .ConnectionString = Session.ConnectionString
                .GridSQL = FindSQL()
                .GridOrderBy = "order by job_no desc"
                .ReturnField = "ID"
                .GridWhereClause = " And hide = 0"
                .FormWidth = 1150
                .ShowAutoFilterRow = True
                .Show()
                _ReturnValue = .ReturnValue
            End With

            Return _ReturnValue

        End Function

        Public Shared Function FindCurrentJobs() As String

            Dim _Status As String = "('X','I','BD','C','XP','LOST','Z')"

            Dim _ReturnValue As String = ""
            Dim _Find As New GenericFind
            With _Find
                .Caption = "Find Job"
                .PopulateOnLoad = True
                .ConnectionString = Session.ConnectionString
                .GridSQL = FindSQL()
                .GridOrderBy = "order by job_no desc"
                .ReturnField = "ID"
                .GridWhereClause = " and job_status_code not in " + _Status + " and hide = 0"
                .FormWidth = 1150
                .ShowAutoFilterRow = True
                .Show()
                _ReturnValue = .ReturnValue
            End With

            Return _ReturnValue

        End Function

#End Region

#Region "RetreiveFunctions"

        Public Shared Function RetreiveByJobNo(ByVal JobNo As String) As Job
            Dim _SQL As String = "select * from Jobs where job_no = " + JobNo
            Return PropertiesFromData(DAL.GetRowfromSQL(Session.ConnectionString, _SQL))
        End Function

        Public Shared Function RetreiveCurrent() As List(Of Job)

            Dim _JobList As List(Of Job)
            _JobList = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, "select * from Jobs where job_status = 'CURRENT' order by job_no"))
            Return _JobList

        End Function

        Public Shared Function RetreiveIDByJobNo(ByVal JobNo As String) As Guid?

            Dim _ID As Object = DAL.ReturnScalar(Session.ConnectionString, "select ID from Jobs where job_no = " + JobNo)
            If _ID IsNot Nothing Then
                Return New Guid(_ID.ToString)
            Else
                Return Nothing
            End If

        End Function

#End Region

#Region "Calculation Helpers"

        Private Function ReturnDateFilter(ByVal DateField As String) As String
            If m_DateFilter Then
                Return " and (" + DateField + " between " + ValueHandler.SQLDate(m_FromDate.Value, True) + " and " + ValueHandler.SQLDate(m_ToDate.Value, True) & ")"
            Else
                Return ""
            End If
        End Function

        Private Sub CalculateLabour()

            Dim _SQL As String = ""

            _SQL += "select isnull(sum(ts_day_duration),0) as 'duration',"
            _SQL += " isnull(sum(ts_cost),0) as 'cost'"
            _SQL += " from Timesheets"
            _SQL += " where ts_job_id = '" + Me._ID.ToString + "'"

            _SQL += ReturnDateFilter("ts_date")

            If m_Linked Then
                _SQL += " or ts_job_id in (select linked_job_id from JobLinks where job_id = '" & Me._ID.ToString & "')"
            End If

            Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
            If _DR IsNot Nothing Then
                m_LabHours = ValueHandler.Convert2DP(_DR.Item("duration"))
                m_LabCost = ValueHandler.Convert2DP(_DR.Item("cost"))
                m_LabTotalOn = ValueHandler.MarkUp(m_LabCost, m_LabTotalOnMarkup)
            Else
                m_LabHours = 0
                m_LabCost = 0
                m_LabTotalOn = 0
            End If

        End Sub

        Private Sub CalculatePurchases()

            Dim _SQL As String = ""

            _SQL += "select isnull(count(inv_value),0) as 'inv_count',"
            _SQL += " isnull(sum(inv_value),0) as 'inv_value'"
            _SQL += " from PurchaseDocuments"
            _SQL += " where target_type = 'Job'"
            _SQL += " and target_id = '" & Me._ID.ToString + "'"
            _SQL += " and inv_type <> 'CI' and inv_type <> 'CC'"

            _SQL = _SQL & ReturnDateFilter("inv_date")

            If m_Linked Then
                _SQL += " or target_id in (select linked_job_id from JobLinks where job_id = '" & Me._ID.ToString & "')"
            End If

            Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
            If _DR IsNot Nothing Then
                m_PurchasesCount = ValueHandler.ConvertInteger(_DR.Item("inv_count"))
                m_PurchasesTotal = ValueHandler.Convert2DP(_DR.Item("inv_value"))
                m_PurchasesOn = ValueHandler.MarkUp(m_PurchasesTotal, m_MatTotalOnMarkup)
            Else
                m_PurchasesCount = 0
                m_PurchasesTotal = 0
                m_PurchasesOn = 0
            End If

        End Sub

        Private Sub CalculateMaterialsFromYard()

            Dim _SQL As String = ""

            _SQL += "select isnull(count(total_price),0) as 'count',"
            _SQL += " isnull(sum(total_price),0) as 'value'"
            _SQL += " from Materials"
            _SQL += " where job_id = '" & Me._ID.ToString + "'"

            _SQL = _SQL & ReturnDateFilter("booked_date")

            If m_Linked Then
                _SQL += " or job_id in (select linked_job_id from JobLinks where job_id = '" & Me._ID.ToString & "')"
            End If

            Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
            If _DR IsNot Nothing Then
                m_YardMaterialsCount = ValueHandler.ConvertInteger(_DR.Item("count"))
                m_YardMaterialsTotal = ValueHandler.Convert2DP(_DR.Item("value"))
                m_YardMaterialsOn = ValueHandler.MarkUp(m_YardMaterialsTotal, m_MatTotalOnMarkup)

            Else
                m_YardMaterialsCount = 0
                m_YardMaterialsTotal = 0
                m_YardMaterialsOn = 0
            End If

        End Sub

        Private Sub CalculateSubCons()

            Dim _SQL As String = ""

            _SQL += "select isnull(count(inv_value),0) as 'inv_count',"
            _SQL += " isnull(sum(inv_value),0) as 'inv_value'"
            _SQL += " from PurchaseDocuments"
            _SQL += " where target_type = 'Job'"
            _SQL += " and target_id = '" & Me._ID.ToString + "'"
            _SQL += " and inv_type <> 'C' and inv_type <> 'I'"

            _SQL = _SQL & ReturnDateFilter("inv_date")

            If m_Linked Then
                _SQL += " or target_id in (select linked_job_id from JobLinks where job_id = '" & Me._ID.ToString & "')"
            End If

            Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
            If _DR IsNot Nothing Then
                m_ConCount = ValueHandler.ConvertInteger(_DR.Item("inv_count"))
                m_ConTotal = ValueHandler.Convert2DP(_DR.Item("inv_value"))
                m_ConTotalOn = ValueHandler.MarkUp(m_ConTotal, m_ConTotalOnMarkup)
            Else
                m_ConCount = 0
                m_ConTotal = 0
                m_ConTotalOn = 0
            End If

        End Sub

        Private Sub CalculateVersions()

            m_VerCount = 0
            m_VerTotal = 0

            'versions / scopes
            Dim _SQL As String = ""

            _SQL += "select ver_value"
            _SQL += " from JobVersions"
            _SQL += " where job_id = '" & Me._ID.ToString + "'"

            _SQL = _SQL & ReturnDateFilter("ver_date")
            _SQL = _SQL & " order by ver_stamp desc"

            Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If _DT IsNot Nothing Then
                For Each _DR As DataRow In _DT.Rows
                    m_VerCount += 1
                    If m_VerCount = 1 Then
                        m_VerTotal = ValueHandler.Convert2DP(_DR.Item("ver_value"))
                    End If
                Next
            End If

            If m_Linked Then

                '_SQL = "select aj_lnk_target_job," & _
                '      " isnull((select top 1 aj_ver_value from ajjobver where aj_ver_job = aj_lnk_target_job order by aj_ver_seq desc),0) as 'value'," & _
                '      " (select count(*) from ajjobver where aj_ver_job = aj_lnk_target_job) as 'count'" & _
                '      " from ajjoblnk" & _
                '      " where aj_lnk_job = " & m_JobNo

                'Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
                'If _DT IsNot Nothing Then
                '    For Each _DRLink As DataRow In _DT.Rows
                '        m_VerCount += ValueHandler.ConvertInteger(_DRLink.Item("count"))
                '        m_VerTotal += ValueHandler.Convert2DP(_DRLink.Item("value"))
                '    Next
                'End If

            End If

        End Sub

        Private Sub CalculateInvoices()

            Dim _SQL As String = ""

            _SQL += "select isnull(count(inv_net),0) as 'inv_count',"
            _SQL += " isnull(sum(inv_net),0) as 'inv_net'"
            _SQL += " from JobInvSales"
            _SQL += " where job_id = '" + Me._ID.ToString + "'"

            _SQL = _SQL & ReturnDateFilter("inv_date")

            If m_Linked Then
                _SQL += " or job_id in (select linked_job_id from JobLinks where job_id = '" & Me._ID.ToString & "')"
            End If

            Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
            If _DR IsNot Nothing Then
                m_InvCount = ValueHandler.ConvertInteger(_DR.Item("inv_count"))
                m_InvTotal = ValueHandler.Convert2DP(_DR.Item("inv_net"))
            Else
                m_InvCount = 0
                m_InvTotal = 0
            End If

        End Sub

#End Region

#Region "Address and Name Helpers"

        Private Function ConvertToOneLine(ByVal AddressIn As List(Of String)) As String

            Dim _Return As String = ""

            For Each _s In AddressIn
                If _s <> "" Then
                    If _Return = "" Then
                        _Return += _s
                    Else
                        _Return += ", " + _s
                    End If
                End If
            Next

            Return _Return

        End Function

        Private Function GetInitials(ByVal Forename As String) As String

            If Forename = "" Then Return ""

            Dim _Return As String = ""
            Dim _Words As String() = Forename.Split(CType(" ", Char()))
            For Each _w In _Words
                _Return += _w.First.ToString.ToUpper
            Next

            Return _Return

        End Function

        Private Function BuildFullName(ByVal Title As String, ByVal Initials As String, ByVal Surname As String) As String

            Dim _Return As String = Title + " " + Initials + " " + Surname

            If Title <> "" AndAlso Initials <> "" AndAlso Surname <> "" Then
                _Return = Title + " " + Initials + " " + Surname
            Else
                If Title = "" AndAlso Initials <> "" AndAlso Surname <> "" Then
                    _Return = Initials + " " + Surname
                Else
                    If Title = "" AndAlso Initials = "" AndAlso Surname <> "" Then
                        _Return = Surname
                    End If
                End If
            End If

            Return _Return

        End Function

        Public Sub GenerateTelephoneNumbers(ByVal Contacts As List(Of Business.Contact))

            Me._AllTels = ""

            If Me._CusTels <> "" Then
                Me._AllTels += Me._CusTels
            End If

            If Me._RskTels <> "" Then
                If Me._AllTels <> "" Then
                    Me._AllTels += "| " + Me._RskTels
                Else
                    Me._AllTels += Me._RskTels
                End If
            End If

            For Each _C In Contacts
                If Me._AllTels <> "" Then
                    Me._AllTels += " " + _C._TelAll
                Else
                    Me._AllTels += _C._TelAll
                End If
            Next

        End Sub

#End Region

    End Class

End Namespace

