﻿Imports Care.Global
Imports Care.Data
Imports Care.Shared

Namespace Business

    Partial Class Plant

        Public Shared Function RetreiveByNo(ByVal AJNo As String) As Plant
            Dim _SQL As String = "select * from Plant where aj_no = " + AJNo
            Return PropertiesFromData(DAL.GetRowfromSQL(Session.ConnectionString, _SQL))
        End Function

        Public Shared Function RetreiveByReg(ByVal RegNo As String) As Plant
            Dim _SQL As String = "select * from Plant where replace(reg_no, ' ', '') = '" + RegNo.Replace(" ", "") + "'"
            Return PropertiesFromData(DAL.GetRowfromSQL(Session.ConnectionString, _SQL))
        End Function

        Public Sub BackInYard(ByVal ActionDate As Date)

            If CareMessage("Book this item back into the Yard?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Back in Yard") = DialogResult.Yes Then

                'check if the item is booked out somewhere already
                'if it is, we need to create a usage transaction for the plant
                If Me._LocationJob.HasValue Then
                    If Me._Hire Then
                        'if its a hired item, we need to calculate the hire period and update the hire transaction
                        Business.PlantDuration.Close(Me._ID.Value, Me._LocationJob.Value, ActionDate, Me._HireCost)
                    End If
                End If

                Me._LocationJob = Nothing
                Me._Location = "Yard"
                Me._LocationStamp = Now

                'create a plant activity transaction for hire start
                Business.Plant.CreateActivity(Me._ID.Value, "In Yard", ActionDate)

                Me.Store()

            End If

        End Sub

        Public Sub BookToJob(ByVal ActionDate As Date)

            'prompt the user to select the target job
            Dim _TargetJobID As String = Business.Job.FindAll
            If _TargetJobID <> "" Then

                Dim _TargetJob As Business.Job = Business.Job.RetreiveByID(New Guid(_TargetJobID))
                If _TargetJob IsNot Nothing Then

                    Dim _Mess As String = ""

                    _Mess += "Are you sure you want to book this item onto:"
                    _Mess += vbCrLf + vbCrLf
                    _Mess += _TargetJob._JobNo.ToString
                    _Mess += vbCrLf + vbCrLf
                    _Mess += _TargetJob._RskFullname
                    _Mess += vbCrLf + vbCrLf
                    _Mess += _TargetJob._RskAddress

                    If CareMessage(_Mess, MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Book Out") = DialogResult.Yes Then

                        'check if the item is booked out somewhere already
                        'if it is, we need to create a usage transaction for the plant
                        If Me._LocationJob.HasValue Then
                            If Me._Hire Then
                                'if its a hired item, we need to calculate the hire period and update the hire transaction
                                Business.PlantDuration.Close(Me._ID.Value, Me._LocationJob.Value, ActionDate, Me._HireCost)
                            End If
                        End If

                        'now we can change the current location of this plant item to the selected job
                        Me._LocationJob = _TargetJob._ID
                        Me._Location = _TargetJob._RskAddress1
                        Me._LocationStamp = Now

                        'start the hire transaction
                        If Me._Hire Then
                            Business.PlantDuration.Create(Me._ID.Value, Me._LocationJob.Value, ActionDate)
                        End If

                        'create a plant activity transaction for hire start
                        Business.Plant.CreateActivity(Me._ID.Value, "Booked onto Job " + _TargetJob._JobNo.ToString + " - " + _TargetJob._RskFullname + ", " + _TargetJob._RskAddress1, ActionDate)

                        'create a job activity transaction for hire start
                        Business.Job.CreateActivity(_TargetJob._ID.Value, Me._Name + " booked out.", ActionDate)

                        Me.Store()

                    End If

                End If

            End If

        End Sub

        Public Shared Sub CreateActivity(ByVal PlantID As Guid, ByVal ActivityText As String, ByVal ActionDate As Date)

            Dim _a As New Business.PlantActivity
            _a._PlantId = PlantID
            _a._ActText = ActivityText
            _a._ActRef1 = ValueHandler.SQLDate(ActionDate)
            _a._ActUser = Session.CurrentUser.UserCode
            _a._ActStamp = Now

            _a.Store()

        End Sub

        Public Shared Function Find() As String

            Dim _SQL As String = ""
            _SQL += "select aj_no as 'AJ No', name as 'Name', nickname as 'Nickname', reg_no as 'Reg',"
            _SQL += " plant_type as 'Type', plant_cat as 'Category', location as 'Location'"
            _SQL += " from Plant"

            Dim _ReturnValue As String = ""
            Dim _Find As New GenericFind
            With _Find
                .Caption = "Find Plant"
                .PopulateOnLoad = True
                .ConnectionString = Session.ConnectionString
                .GridSQL = _SQL
                .GridOrderBy = "order by name"
                .ReturnField = "ID"
                .FormWidth = 900
                .Show()
                Return .ReturnValue
            End With

        End Function

    End Class

End Namespace

