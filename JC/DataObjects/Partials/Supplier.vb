﻿Imports Care.Global
Imports Care.Shared

Namespace Business

    Partial Class Supplier

        Public Shared Function Find() As String

            Dim _ReturnValue As String = ""
            Dim _Find As New GenericFind
            With _Find
                .Caption = "Find Supplier"
                .PopulateOnLoad = True
                .ConnectionString = Session.ConnectionString
                .GridSQL = "select sup_code as 'Code', sup_name as 'Name', sup_contact as 'Contact', sup_tel as 'Tel', sup_email as 'Email' from Suppliers"
                .GridOrderBy = "order by sup_name"
                .ReturnField = "ID"
                .FormWidth = 900
                .Show()
                _ReturnValue = .ReturnValue
            End With

            Return _ReturnValue

        End Function
    End Class

End Namespace
