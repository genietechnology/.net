﻿Option Strict On
Imports Care.Data
Imports Care.Global
Imports Care.Shared

Namespace Business

    Partial Class Employee

        Public Shared Function RetreiveByName(ByVal Name As String) As Employee
            Dim _SQL As String = "select * from Employees where rtrim(fullname) + ' ' + rtrim(nickname) like '%" + Name + "%' order by nickname, fullname"
            Return PropertiesFromData(DAL.GetRowfromSQL(Session.ConnectionString, _SQL))
        End Function

        Public Shared Function FindActive() As String

            Dim _ReturnValue As String = ""
            Dim _Find As New GenericFind
            With _Find
                .Caption = "Find Employee"
                .PopulateOnLoad = True
                .ConnectionString = Session.ConnectionString
                .GridSQL = "select code as 'Code', fullname as 'Name', nickname as 'Nickname', tel_mobile as 'Mobile', email as 'Email' from Employees"
                .GridOrderBy = "order by fullname"
                .ReturnField = "ID"
                .GridWhereClause = " and active = 1"
                .FormWidth = 900
                .Show()
                _ReturnValue = .ReturnValue
            End With

            Return _ReturnValue

        End Function

        Public Shared Function Find() As String

            Dim _ReturnValue As String = ""
            Dim _Find As New GenericFind
            With _Find
                .Caption = "Find Employee"
                .PopulateOnLoad = True
                .ConnectionString = Session.ConnectionString
                .GridSQL = "select code as 'Code', fullname as 'Name', nickname as 'Nickname', tel_mobile as 'Mobile', email as 'Email' from Employees"
                .GridOrderBy = "order by fullname"
                .ReturnField = "ID"
                .GridWhereClause = ""
                .FormWidth = 900
                .Show()
                _ReturnValue = .ReturnValue
            End With

            Return _ReturnValue

        End Function

    End Class


End Namespace

