﻿Imports Care.Global
Imports Care.Data
Imports Care.Shared

Namespace Business

    Partial Class PlantDuration

        Public Shared Function RetreiveByPlantAndJobID(ByVal PlantID As Guid, ByVal JobID As Guid) As PlantDuration

            Dim _SQL As String = ""
            _SQL += "select * from PlantDuration"
            _SQL += " where plant_id = '" + PlantID.ToString + "'"
            _SQL += " and job_id = '" + JobID.ToString + "'"

            Return PropertiesFromData(DAL.GetRowfromSQL(Session.ConnectionString, _SQL))

        End Function

        Public Shared Sub Close(ByVal PlantID As Guid, ByVal JobID As Guid, ByVal DateTo As Date, ByVal HirePrice As Decimal)

            Dim _D As Business.PlantDuration = Business.PlantDuration.RetreiveByPlantAndJobID(PlantID, JobID)
            If _D IsNot Nothing Then
                _D._DateTo = DateTo
                _D._Days = CInt(DateDiff(DateInterval.Day, _D._DateFrom.Value, _D._DateTo.Value)) + 1
                _D._Cost = _D._Days * HirePrice
                _D.Store()
            End If

        End Sub

        Public Shared Sub Create(ByVal PlantID As Guid, ByVal JobID As Guid, ByVal DateFrom As Date)
            Dim _D As New Business.PlantDuration
            _D._PlantId = PlantID
            _D._JobId = JobID
            _D._DateFrom = DateFrom
            _D.Store()
        End Sub

    End Class

End Namespace
