﻿Imports Care.Global
Imports Care.Data
Imports Care.Shared

Namespace Business

    Partial Class Scan

        Public Sub UpdateTarget()
            If Me._MatchedTargetId.HasValue Then
                If Not Me._MatchedTargetMulti Then
                    CreatePurchaseDocument()
                End If
            End If
        End Sub

        Private Sub CreatePurchaseDocument()

            'check if the document already exists
            Dim _PD As PurchaseDocument = PurchaseDocument.RetrieveByDocAndTargetID(Me._ID, Me.m_MatchedTargetId)
            If _PD Is Nothing Then
                'does not exist, we need to create new
                _PD = New PurchaseDocument
            End If

            With _PD

                If _PD.IsNew Then
                    ._DocId = Me._ID
                    ._TargetType = Me._MatchedTarget
                    ._TargetId = Me._MatchedTargetId
                    ._InvEnteredUser = Me._MatchedBy
                    ._InvEnteredDate = Me._MatchedStamp.Value.Date
                    ._InvEnteredStamp = Me._MatchedStamp
                End If

                ._InvDate = Me._DocDate

                If Me._DocType = "Supplier Invoice" Then ._InvType = "I"
                If Me._DocType = "Supplier Credit" Then ._InvType = "C"
                If Me._DocType = "Contractor Invoice" Then ._InvType = "CI"
                If Me._DocType = "Contractor Credit" Then ._InvType = "CC"

                ._InvSuppId = Me._DocSupplierId
                ._InvSuppName = Me._DocSupplierName
                ._InvCategory = Me._DocDesc
                ._InvValue = Me._DocNet

                .Store()

            End With

        End Sub

        Public Shared Sub ViewDocument(ByVal DocID As Guid)
            Session.CursorWaiting()
            Dim _frm As New frmScanView(DocID)
            _frm.Show()
        End Sub

        Public Shared Sub Print(ByVal DocID As Guid, ByVal ShiftDown As Boolean)

            Session.CursorWaiting()

            Dim _dcn As String = Session.ConnectionString.Replace("jc", "doc")
            Dim _SQL As String = "select * from Scans where ID = '" + DocID.ToString + "'"

            If ShiftDown Then
                ReportHandler.DesignReport("ScanPrint.repx", _dcn, _SQL)
            Else
                ReportHandler.RunReport("ScanPrint.repx", _dcn, _SQL)
            End If

            Session.CursorDefault()

        End Sub

    End Class

End Namespace

