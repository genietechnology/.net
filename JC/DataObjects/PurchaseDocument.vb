﻿'*****************************************************
'Generated 25/07/2017 12:34:28 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class PurchaseDocument
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_TargetType As String
        Dim m_TargetId As Guid?
        Dim m_InvDate As Date?
        Dim m_InvType As String
        Dim m_InvCategory As String
        Dim m_InvSuppId As Guid?
        Dim m_InvSuppName As String
        Dim m_InvDesc As String
        Dim m_InvValue As Decimal
        Dim m_InvEnteredDate As Date?
        Dim m_InvEnteredUser As String
        Dim m_InvEnteredUserName As String
        Dim m_InvEnteredStamp As DateTime?
        Dim m_DocId As Guid?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._TargetType = ""
                ._TargetId = Nothing
                ._InvDate = Nothing
                ._InvType = ""
                ._InvCategory = ""
                ._InvSuppId = Nothing
                ._InvSuppName = ""
                ._InvDesc = ""
                ._InvValue = 0
                ._InvEnteredDate = Nothing
                ._InvEnteredUser = ""
                ._InvEnteredUserName = ""
                ._InvEnteredStamp = Nothing
                ._DocId = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._TargetType = ""
                ._TargetId = Nothing
                ._InvDate = Nothing
                ._InvType = ""
                ._InvCategory = ""
                ._InvSuppId = Nothing
                ._InvSuppName = ""
                ._InvDesc = ""
                ._InvValue = 0
                ._InvEnteredDate = Nothing
                ._InvEnteredUser = ""
                ._InvEnteredUserName = ""
                ._InvEnteredStamp = Nothing
                ._DocId = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@TargetType")>
        Public Property _TargetType() As String
            Get
                Return m_TargetType
            End Get
            Set(ByVal value As String)
                m_TargetType = value
            End Set
        End Property

        <StoredProcParameter("@TargetId")>
        Public Property _TargetId() As Guid?
            Get
                Return m_TargetId
            End Get
            Set(ByVal value As Guid?)
                m_TargetId = value
            End Set
        End Property

        <StoredProcParameter("@InvDate")>
        Public Property _InvDate() As Date?
            Get
                Return m_InvDate
            End Get
            Set(ByVal value As Date?)
                m_InvDate = value
            End Set
        End Property

        <StoredProcParameter("@InvType")>
        Public Property _InvType() As String
            Get
                Return m_InvType
            End Get
            Set(ByVal value As String)
                m_InvType = value
            End Set
        End Property

        <StoredProcParameter("@InvCategory")>
        Public Property _InvCategory() As String
            Get
                Return m_InvCategory
            End Get
            Set(ByVal value As String)
                m_InvCategory = value
            End Set
        End Property

        <StoredProcParameter("@InvSuppId")>
        Public Property _InvSuppId() As Guid?
            Get
                Return m_InvSuppId
            End Get
            Set(ByVal value As Guid?)
                m_InvSuppId = value
            End Set
        End Property

        <StoredProcParameter("@InvSuppName")>
        Public Property _InvSuppName() As String
            Get
                Return m_InvSuppName
            End Get
            Set(ByVal value As String)
                m_InvSuppName = value
            End Set
        End Property

        <StoredProcParameter("@InvDesc")>
        Public Property _InvDesc() As String
            Get
                Return m_InvDesc
            End Get
            Set(ByVal value As String)
                m_InvDesc = value
            End Set
        End Property

        <StoredProcParameter("@InvValue")>
        Public Property _InvValue() As Decimal
            Get
                Return m_InvValue
            End Get
            Set(ByVal value As Decimal)
                m_InvValue = value
            End Set
        End Property

        <StoredProcParameter("@InvEnteredDate")>
        Public Property _InvEnteredDate() As Date?
            Get
                Return m_InvEnteredDate
            End Get
            Set(ByVal value As Date?)
                m_InvEnteredDate = value
            End Set
        End Property

        <StoredProcParameter("@InvEnteredUser")>
        Public Property _InvEnteredUser() As String
            Get
                Return m_InvEnteredUser
            End Get
            Set(ByVal value As String)
                m_InvEnteredUser = value
            End Set
        End Property

        <StoredProcParameter("@InvEnteredUserName")>
        Public Property _InvEnteredUserName() As String
            Get
                Return m_InvEnteredUserName
            End Get
            Set(ByVal value As String)
                m_InvEnteredUserName = value
            End Set
        End Property

        <StoredProcParameter("@InvEnteredStamp")>
        Public Property _InvEnteredStamp() As DateTime?
            Get
                Return m_InvEnteredStamp
            End Get
            Set(ByVal value As DateTime?)
                m_InvEnteredStamp = value
            End Set
        End Property

        <StoredProcParameter("@DocId")>
        Public Property _DocId() As Guid?
            Get
                Return m_DocId
            End Get
            Set(ByVal value As Guid?)
                m_DocId = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As PurchaseDocument

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getPurchaseDocumentbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As PurchaseDocument

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getPurchaseDocumentbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of PurchaseDocument)

            Dim _PurchaseDocumentList As List(Of PurchaseDocument)
            _PurchaseDocumentList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getPurchaseDocumentTable"))
            Return _PurchaseDocumentList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of PurchaseDocument)

            Dim _PurchaseDocumentList As List(Of PurchaseDocument)
            _PurchaseDocumentList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getPurchaseDocumentTable"))
            Return _PurchaseDocumentList

        End Function

        Public Shared Sub SaveAll(ByVal PurchaseDocumentList As List(Of PurchaseDocument))

            For Each _PurchaseDocument As PurchaseDocument In PurchaseDocumentList
                SaveRecord(_PurchaseDocument)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal PurchaseDocumentList As List(Of PurchaseDocument))

            For Each _PurchaseDocument As PurchaseDocument In PurchaseDocumentList
                SaveRecord(ConnectionString, _PurchaseDocument)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal PurchaseDocument As PurchaseDocument) As Guid
            PurchaseDocument.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, PurchaseDocument, "upsertPurchaseDocument")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal PurchaseDocument As PurchaseDocument) As Guid
            PurchaseDocument.SetDefaultValues()
            DAL.SaveRecord(ConnectionString, PurchaseDocument, "upsertPurchaseDocument")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deletePurchaseDocumentbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deletePurchaseDocumentbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertPurchaseDocument")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(ConnectionString, Me, "upsertPurchaseDocument")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As PurchaseDocument

            Dim _P As PurchaseDocument = Nothing

            If DR IsNot Nothing Then
                _P = New PurchaseDocument()
                With DR
                    _P.IsNew = False
                    _P.IsDeleted = False
                    _P._ID = GetGUID(DR("ID"))
                    _P._TargetType = DR("target_type").ToString.Trim
                    _P._TargetId = GetGUID(DR("target_id"))
                    _P._InvDate = GetDate(DR("inv_date"))
                    _P._InvType = DR("inv_type").ToString.Trim
                    _P._InvCategory = DR("inv_category").ToString.Trim
                    _P._InvSuppId = GetGUID(DR("inv_supp_id"))
                    _P._InvSuppName = DR("inv_supp_name").ToString.Trim
                    _P._InvDesc = DR("inv_desc").ToString.Trim
                    _P._InvValue = GetDecimal(DR("inv_value"))
                    _P._InvEnteredDate = GetDate(DR("inv_entered_date"))
                    _P._InvEnteredUser = DR("inv_entered_user").ToString.Trim
                    _P._InvEnteredUserName = DR("inv_entered_user_name").ToString.Trim
                    _P._InvEnteredStamp = GetDateTime(DR("inv_entered_stamp"))
                    _P._DocId = GetGUID(DR("doc_id"))

                End With
            End If

            Return _P

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of PurchaseDocument)

            Dim _PurchaseDocumentList As New List(Of PurchaseDocument)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _PurchaseDocumentList.Add(PropertiesFromData(_DR))
            Next

            Return _PurchaseDocumentList

        End Function


#End Region

    End Class

End Namespace
