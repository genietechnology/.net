﻿'*****************************************************
'Generated 02/03/2017 20:54:05 using Version 1.15.8.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class JobFeedback
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_JobId As Guid?
        Dim m_Overall As Integer
        Dim m_CommIns As Integer
        Dim m_CommBld As Integer
        Dim m_Timekeeping As Integer
        Dim m_Quality As Integer
        Dim m_Appearance As Integer
        Dim m_Attitude As Integer
        Dim m_Avg As Decimal
        Dim m_UserCode As String
        Dim m_Stamp As DateTime?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._JobId = Nothing
                ._Overall = 0
                ._CommIns = 0
                ._CommBld = 0
                ._Timekeeping = 0
                ._Quality = 0
                ._Appearance = 0
                ._Attitude = 0
                ._Avg = 0
                ._UserCode = ""
                ._Stamp = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._JobId = Nothing
                ._Overall = 0
                ._CommIns = 0
                ._CommBld = 0
                ._Timekeeping = 0
                ._Quality = 0
                ._Appearance = 0
                ._Attitude = 0
                ._Avg = 0
                ._UserCode = ""
                ._Stamp = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@JobId")> _
        Public Property _JobId() As Guid?
            Get
                Return m_JobId
            End Get
            Set(ByVal value As Guid?)
                m_JobId = value
            End Set
        End Property

        <StoredProcParameter("@Overall")> _
        Public Property _Overall() As Integer
            Get
                Return m_Overall
            End Get
            Set(ByVal value As Integer)
                m_Overall = value
            End Set
        End Property

        <StoredProcParameter("@CommIns")> _
        Public Property _CommIns() As Integer
            Get
                Return m_CommIns
            End Get
            Set(ByVal value As Integer)
                m_CommIns = value
            End Set
        End Property

        <StoredProcParameter("@CommBld")> _
        Public Property _CommBld() As Integer
            Get
                Return m_CommBld
            End Get
            Set(ByVal value As Integer)
                m_CommBld = value
            End Set
        End Property

        <StoredProcParameter("@Timekeeping")> _
        Public Property _Timekeeping() As Integer
            Get
                Return m_Timekeeping
            End Get
            Set(ByVal value As Integer)
                m_Timekeeping = value
            End Set
        End Property

        <StoredProcParameter("@Quality")> _
        Public Property _Quality() As Integer
            Get
                Return m_Quality
            End Get
            Set(ByVal value As Integer)
                m_Quality = value
            End Set
        End Property

        <StoredProcParameter("@Appearance")> _
        Public Property _Appearance() As Integer
            Get
                Return m_Appearance
            End Get
            Set(ByVal value As Integer)
                m_Appearance = value
            End Set
        End Property

        <StoredProcParameter("@Attitude")> _
        Public Property _Attitude() As Integer
            Get
                Return m_Attitude
            End Get
            Set(ByVal value As Integer)
                m_Attitude = value
            End Set
        End Property

        <StoredProcParameter("@Avg")> _
        Public Property _Avg() As Decimal
            Get
                Return m_Avg
            End Get
            Set(ByVal value As Decimal)
                m_Avg = value
            End Set
        End Property

        <StoredProcParameter("@UserCode")> _
        Public Property _UserCode() As String
            Get
                Return m_UserCode
            End Get
            Set(ByVal value As String)
                m_UserCode = value
            End Set
        End Property

        <StoredProcParameter("@Stamp")> _
        Public Property _Stamp() As DateTime?
            Get
                Return m_Stamp
            End Get
            Set(ByVal value As DateTime?)
                m_Stamp = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As JobFeedback

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getJobFeedbackbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of JobFeedback)

            Dim _JobFeedbackList As List(Of JobFeedback)
            _JobFeedbackList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getJobFeedbackTable"))
            Return _JobFeedbackList

        End Function

        Public Shared Sub SaveAll(ByVal JobFeedbackList As List(Of JobFeedback))

            For Each _JobFeedback As JobFeedback In JobFeedbackList
                SaveRecord(_JobFeedback)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal JobFeedback As JobFeedback) As Guid
            JobFeedback.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, JobFeedback, "upsertJobFeedback")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteJobFeedbackbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertJobFeedback")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As JobFeedback

            Dim _J As JobFeedback = Nothing

            If DR IsNot Nothing Then
                _J = New JobFeedback()
                With DR
                    _J.IsNew = False
                    _J.IsDeleted = False
                    _J._ID = GetGUID(DR("ID"))
                    _J._JobId = GetGUID(DR("job_id"))
                    _J._Overall = GetInteger(DR("overall"))
                    _J._CommIns = GetInteger(DR("comm_ins"))
                    _J._CommBld = GetInteger(DR("comm_bld"))
                    _J._Timekeeping = GetInteger(DR("timekeeping"))
                    _J._Quality = GetInteger(DR("quality"))
                    _J._Appearance = GetInteger(DR("appearance"))
                    _J._Attitude = GetInteger(DR("attitude"))
                    _J._Avg = GetDecimal(DR("avg"))
                    _J._UserCode = DR("user_code").ToString.Trim
                    _J._Stamp = GetDateTime(DR("stamp"))

                End With
            End If

            Return _J

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of JobFeedback)

            Dim _JobFeedbackList As New List(Of JobFeedback)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _JobFeedbackList.Add(PropertiesFromData(_DR))
            Next

            Return _JobFeedbackList

        End Function


#End Region

    End Class

End Namespace
