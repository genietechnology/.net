﻿Option Strict On

Imports Care.Data
Imports Care.Shared
Imports Care.Global
Imports System.Security
Imports Care.Security
Imports System.IO
Imports System.Text
Imports Care.Framework.CareSoftware.API

Public Class LicenseManager

    Private Shared m_LicenseFolder As String = ""
    Private Shared m_LicenseFile As String = ""
    Private Shared m_LicenseError As String = ""
    Private Shared m_Terms As String = ""

    Public Shared ReadOnly Property Terms As String
        Get
            Return m_Terms
        End Get
    End Property

    Private Shared Sub SetLicensingFolder()
        m_LicenseFolder = Environment.CurrentDirectory + "\licenses\"
        If Not IO.Directory.Exists(m_LicenseFolder) Then
            IO.Directory.CreateDirectory(m_LicenseFolder)
        End If
    End Sub

    Public Shared Function Validate() As Boolean

        SetLicensingFolder()

        Dim _LicFound As Boolean = False
        Dim _OK As Boolean = False

        For Each _FileName As String In IO.Directory.GetFiles(m_LicenseFolder, "*.lic")
            _LicFound = True
            m_LicenseFile = _FileName
            If ValidateLicense() Then
                _OK = True
                Exit For
            End If
        Next

        If _OK Then
            Return True
        Else
            If _LicFound Then
                'licenses found - but not valid
                CareMessage(m_LicenseError, MessageBoxIcon.Warning, "License Issue")
            Else
                'licenses not found - do you want to import a license
                If CareMessage("License file not found. Do you want to import a license now?", MessageBoxIcon.Warning, MessageBoxButtons.OKCancel, "License Manager") = DialogResult.OK Then
                    If ImportLicense() Then
                        Return Validate
                    End If
                End If
            End If
        End If

        Return False

    End Function

    Private Shared Function ImportLicense() As Boolean

        Dim _Return As Boolean = False
        Dim _dlg As New OpenFileDialog()

        ' setup a dialog;
        _dlg.Filter = "User License Files (*.lic)|*.lic"
        _dlg.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.Desktop
        _dlg.Title = "Select License File"

        If _dlg.ShowDialog() = DialogResult.OK Then

            Try

                Dim _DestinationFile As String = m_LicenseFolder + New FileInfo(_dlg.FileName).Name
                File.Copy(_dlg.FileName, _DestinationFile)

                If File.Exists(_DestinationFile) Then
                    _Return = True
                End If

            Catch
                _Return = False
            End Try

        End If

        Return _Return

    End Function

    Private Shared Function ValidateLicense() As Boolean

        Dim _PK As String = "<DSAKeyValue><P>xC/kxhb/gWaT0D/SCWg3GMHW3VsfJKNf8Nm6sW3Bq8XMD4NQwyBoUjscMhAD2RSSh/O/mCpZpAwUeEvAXTIknNj7QRJmeZsNWDIOmwfkyqP3aInrcdh9QxblBKAL/kMH3VEx8xSuFQwMIHhfyWtYszPJ88bVM2kLl87ECLz039c=</P><Q>j8QaZTGefEWQxyG93sKtJYewkKk=</Q><G>jUJ3UyoSH0SfsWLUB9Lmxer8PRFHkneUzHiwh8wVqaQ6n+2a3DWnSDiahusBKqCopxGi6GC1YcSiIN/UrIHiusXjmKFQjvWoxoqWCIDbNfRwVKPdSyNQpmKkPsSFZBz9eXDOkjdjnuyt6tp+qju6slE0COeGk8tUUGmfdk+uH+w=</G><Y>rBmeC9dbDSmv8w1qi3RnF4ISKL9aUKdGdS0OWUORjWL8LXcR+igELQxo6HkPug/2koTSXXaq9wX0M1KgYmokUeFi9fZCOw9a4QajylwVwHxak/cilVRRnsSUzCR33Py4gTxKnj9ZAwFmRzmMXsGqF8zo8MAlDcv2GbzrO0QT/lA=</Y><J>AAAAAV1YO2/xM6ffh8h7/mL4vxYESdnJGFuT6BjHEwHecJKiQT17pWIRDlq96/QL8/EgiCr66na6q62k/7IvZV+AZlP1v3Xaa2oa+Hu0ry3bPH0i85xJmdHkyVhtHyLjrRSgFGj9W3SbUKchFMSo5g==</J><Seed>IxNAX2wfkKUs4XzjJRlMLAZdqmk=</Seed><PgenCounter>YA==</PgenCounter></DSAKeyValue>"

        Dim _Return As Boolean = False
        Dim _License As License = License.Load(m_LicenseFile)
        If _License IsNot Nothing Then

            Try
                LicenseValidation.ValidateLicense(_License, _PK)

                Dim _T As LicenseTerms = LicenseValidation.GetValidTerms(_License, _PK)

                Dim _SB As New StringBuilder
                _SB.AppendLine("Company Name     : " + _T.CompanyName)
                _SB.AppendLine("Product          : " + _T.CompanyLocation)
                _SB.AppendLine("Computer Name    : " + _T.ComputerName)
                _SB.AppendLine("Start Date       : " + _T.StartDate.ToString)
                _SB.AppendLine("Expiry Date      : " + _T.ExpiryDate.ToString)
                _SB.AppendLine("Days Remaining   : " + DateDiff(DateInterval.Day, Now, _T.ExpiryDate).ToString)

                m_Terms = _SB.ToString()

                _Return = True

            Catch se As SecurityException
                m_LicenseError = se.Message
            End Try

        Else
            m_LicenseError = "License was found - but could not be loaded."
        End If

        Return _Return

    End Function

    Public Shared Sub CheckInOnline()
        If Session.CustomerID <> "" Then
            CheckIn(Session.CustomerID)
        End If
    End Sub

    Private Shared Sub CheckIn(ByVal CustomerID As String)

        'assemble everything we need for checkin
        Dim _ClientData As Core.ClientData = ReturnData(CustomerID)

        Try

            Dim _WS As New Core.CoreClient
            Dim _Response As Core.CheckInResponse = _WS.CheckIn(_ClientData)

            Select Case _Response.Result

                'revoke license
                Case -1
                    If RevokeLicenses() Then
                        _WS.CheckInComplete(_Response.ID)
                        End
                    End If

                Case 0
                    'all OK

                Case 1
                    'new license available
                    If _Response.Data <> "" Then
                        If UpdateLicense(_Response.Data) Then
                            _WS.CheckInComplete(_Response.ID)
                        End If
                    End If

                Case Else
                    'something bad happened

            End Select

        Catch ex As Exception
            'some other exception
            ErrorHandler.LogExceptionToDatabase(ex)
        End Try

    End Sub

    Private Shared Function UpdateLicense(ByVal DataIn As String) As Boolean

        Try

            Dim _Dir As String = Environment.CurrentDirectory + "\licenses\"
            Dim _File As String = _Dir + My.Computer.Name.ToUpper + "_" + Format(Now, "yyyy-MM-dd_HHmm") + ".lic"

            Dim _sw As System.IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter(_File, False)
            _sw.Write(DataIn)
            _sw.Close()
            _sw.Dispose()

            Return True

        Catch ex As Exception
            ErrorHandler.LogExceptionToDatabase(ex)
            Return False
        End Try

    End Function

    Private Shared Function RevokeLicenses() As Boolean

        Try
            Dim _dir As String = Environment.CurrentDirectory + "\licenses\"
            For Each _f As String In IO.Directory.GetFiles(_dir, "*.lic")
                IO.File.Delete(_f)
            Next

        Catch ex As Exception
            ErrorHandler.LogExceptionToDatabase(ex)
            Return False
        End Try

        Return True

    End Function

    Private Shared Function ReturnData(ByVal CustomerID As String) As Core.ClientData

        Dim _Data As New Core.ClientData
        With _Data

            .CustomerID = CustomerID

            .ComputerName = My.Computer.Name
            .UserName = Environment.UserDomainName + "\" + Environment.UserName

            .VersionWorkstation = My.Application.Info.Version.ToString
            .VersionDatabase = ""

            Dim _ScheduledBackup As Guid? = TaskExists("Backup")
            If _ScheduledBackup.HasValue Then

                .BackupScheduled = True

                Dim _Date As Date? = LastSuccessfulResult(_ScheduledBackup.Value)
                If _Date.HasValue Then
                    .BackupLast = _Date.Value
                Else
                    .BackupLast = DateSerial(2000, 12, 31)
                End If

            Else

                .BackupScheduled = False

                Dim _Date As Date? = ParameterHandler.ReturnDate("BACKUPLAST")
                If _Date.HasValue Then
                    .BackupLast = _Date.Value
                Else
                    .BackupLast = DateSerial(2000, 12, 31)
                End If

            End If

            Dim _LiveDriveVersion As String = ""
            If FileExists("C:\Program Files (x86)\Livedrive", "*.exe", "LiveDrive.exe", _LiveDriveVersion) Then
                If ProcessRunning("LiveDrive") Then
                    .LiveDriveState = 1
                Else
                    .LiveDriveState = 0
                End If
            Else
                .LiveDriveState = -1
            End If

            .VersionService = ""
            If FileExists("C:\Program Files (x86)\Care Software", "*.exe", "NurseryGenieService.exe", .VersionService) Then

                If ProcessRunning("GenieWindowsService") Then
                    .ServiceState = 1
                Else
                    .ServiceState = 0
                End If
            Else
                .ServiceState = -1
                .VersionService = ""
            End If

        End With

        Return _Data

    End Function

    Private Shared Function FileExists(ByVal RootFolder As String, ByVal SearchPattern As String, ByVal FileName As String, Optional ByRef Version As String = "") As Boolean

        Try

            If IO.Directory.Exists(RootFolder) Then

                For Each _f As String In IO.Directory.GetFiles(RootFolder, SearchPattern, IO.SearchOption.AllDirectories)
                    Dim _Name As String = IO.Path.GetFileName(_f)
                    If _Name.ToLower = FileName.ToLower Then
                        Version = FileVersionInfo.GetVersionInfo(_f).FileVersion
                        Return True
                    End If
                Next

            End If

        Catch ex As Exception
            ErrorHandler.LogExceptionToDatabase(ex)
        End Try

        Return False

    End Function

    Private Shared Function ProcessRunning(ByVal ServiceName As String) As Boolean

        Try

            For Each p As Process In Process.GetProcesses
                If p.ProcessName.ToLower = ServiceName.ToLower Then
                    Return True
                End If
            Next

        Catch ex As Exception
            ErrorHandler.LogExceptionToDatabase(ex)
        End Try

        Return False

    End Function

    Private Shared Function TaskExists(ByVal NameContains As String) As Guid?

        Dim _Return As Guid? = Nothing

        Try

            Dim _SQL As String = "select ID from AppTasks where name like '%" + NameContains + "%'"
            Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)

            If _DT IsNot Nothing Then

                If _DT.Rows.Count > 0 Then
                    _Return = New Guid(_DT.Rows.Item(0).Item("ID").ToString)
                End If

                _DT.Dispose()
                _DT = Nothing

            End If

        Catch ex As Exception
            ErrorHandler.LogExceptionToDatabase(ex)
        End Try

        Return _Return

    End Function

    Private Shared Function LastSuccessfulResult(ByVal TaskID As Guid) As Date?

        Dim _Return As Date? = Nothing

        Try

            Dim _SQL As String = "select top 1 stamp from AppTaskHistory where task_id = '" + TaskID.ToString + "' and result = 0 order by stamp desc"
            Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)

            If _DT IsNot Nothing Then

                If _DT.Rows.Count > 0 Then
                    _Return = ValueHandler.ConvertDate(_DT.Rows.Item(0).Item("stamp"))
                End If

                _DT.Dispose()
                _DT = Nothing

            End If

        Catch ex As Exception
            ErrorHandler.LogExceptionToDatabase(ex)
        End Try

        Return _Return

    End Function

End Class
