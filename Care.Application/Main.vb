﻿Imports Care.Global
Imports Care.Shared
Imports Care.Data
Imports System.Net

Module Main

    Public Sub Main()

        Dim _AutoLogin As Boolean = False
        Dim _AutoLoginUser As String = ""

        If My.Application.CommandLineArgs.Count < 2 Then
            CareMessage("Invalid Command Line Arguments provided." & vbCrLf &
                        "<Application Code> <Application Name> expected.", MessageBoxIcon.Error, MessageBoxButtons.OK, "Care.Application.Main")
            End
        Else

            Session.ApplicationCode = My.Application.CommandLineArgs(0)
            Session.ApplicationName = My.Application.CommandLineArgs(1)

            If My.Application.CommandLineArgs.Count = 3 Then
                If My.Application.CommandLineArgs(2).ToLower = "/debug" Then
                    StartKernelLogging()
                Else
                    _AutoLogin = True
                    _AutoLoginUser = My.Application.CommandLineArgs(2)
                End If
            End If
        End If

        Cursor.Current = Cursors.AppStarting
        Application.DoEvents()

        KernelLog("Activate ErrorHandler")
        ErrorHandler.Activate()

        ServicePointManager.Expect100Continue = True
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12

        Dim _ConnectionDetails As New ConnectionBuilder
        With _ConnectionDetails
            .Server = My.Settings.SQLServer
            .DB = My.Settings.SQLDatabase
            .User = My.Settings.SQLUser
            .Password = My.Settings.SQLPassword
            .IntegratedSecurity = My.Settings.UseWindowsAuthentication
            .TestConnection()
        End With

        If _ConnectionDetails.ConnectionOK Then

            Session.ConnectionString = _ConnectionDetails.ConnectionString

            If _ConnectionDetails.DB.ToUpper = "AUTH" Then
                If Not Setup.CheckAuthDatabase Then
                    ExitApplication()
                End If
            End If

            DevExpress.UserSkins.BonusSkins.Register()
            DevExpress.Skins.SkinManager.EnableFormSkins()
            DevExpress.Skins.SkinManager.EnableMdiFormSkins()
            DevExpress.LookAndFeel.UserLookAndFeel.Default.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin
            DevExpress.Utils.ToolTipController.DefaultController.AutoPopDelay = 30 * 1000 '30 seconds
            DevExpress.XtraReports.Configuration.Settings.Default.UserDesignerOptions.ReportLoadingRestrictionLevel = DevExpress.XtraReports.UI.RestrictionLevel.Enable
            DevExpress.XtraReports.Configuration.Settings.Default.UserDesignerOptions.DataBindingMode = DevExpress.XtraReports.UI.DataBindingMode.Bindings

            'check license
            If Not LicenseManager.Validate() Then
                ExitApplication()
            Else
                'autologin (re-loading from software update)
                If _AutoLogin Then
                    If Setup.LoginWithUserCode(_AutoLoginUser) Then
                        LoadMDI()
                    End If
                Else
                    KernelLog("Setup.CheckAdminUser")
                    If Setup.CheckAdminUser Then
                        If Setup.LoginViaWindowsCredentials Then
                            LoadMDI()
                        Else
                            If frmLogin.ShowDialog = DialogResult.Yes Then
                                LoadMDI()
                            End If
                        End If
                    Else
                        ExitApplication()
                    End If
                End If
            End If

        Else
            CareMessage("Unable to connect to database.", MessageBoxIcon.Error, "Connection Failure")
        End If

    End Sub

    Private Sub LoadMDI()
        KernelLog("Setup.InitialiseParameters")
        If Setup.InitialiseParameters() Then
            KernelLog("frmMDI.ShowDialog", DebugLogItem.EnumDebugType.LineBreakFirst)
            frmMDI.ShowDialog()
        End If
    End Sub

    Private Sub ExitApplication()
        ErrorHandler.DeActivate()
        End
    End Sub

End Module
