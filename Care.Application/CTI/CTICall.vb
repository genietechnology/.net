﻿Imports Care.Global
Imports Care.Data

Public Class CTICall

    Implements IIncomingCall

    Public Function LogCall(RawNumber As String) As Integer Implements IIncomingCall.LogCall

        Dim _SearchNumber As String = RawNumber
        If RawNumber.StartsWith("+44") Then _SearchNumber = RawNumber.Substring(3)

        Select Case Session.ApplicationCode

            Case "NURSERY"

                Dim _SQL As String = ""
                _SQL += "select family_id, ID, fullname, tel_home, tel_mobile, job_tel from Contacts"
                _SQL += " where replace(tel_home + tel_mobile  + job_tel, ' ', '') like '%" + _SearchNumber + "%'"

                Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
                If _DT IsNot Nothing Then

                    If _DT.Rows.Count > 0 Then

                        Dim _FamilyID As String = _DT.Rows(0).Item("family_id").ToString
                        Dim _ContactID As String = _DT.Rows(0).Item("ID").ToString
                        Dim _Name As String = _DT.Rows(0).Item("fullname").ToString

                        If _DT.Rows.Count = 1 Then
                            Session.CTIPopup(RawNumber, True, _Name, _FamilyID, _ContactID)
                        Else
                            Session.CTIPopup(RawNumber, True, "Multiple Matches Found", _FamilyID, "")
                        End If

                    End If

                End If

        End Select

        Return 0

    End Function

End Class
