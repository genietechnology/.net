﻿Imports System.ServiceModel

<ServiceContract()> _
Public Interface IIncomingCall

    <OperationContract()> _
    Function LogCall(ByVal RawNumber As String) As Integer

End Interface
