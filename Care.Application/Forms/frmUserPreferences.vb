﻿Imports Care.Global
Imports Care.Shared
Imports Care.Data

Public Class frmUserPreferences

    Private Sub frmUserPreferences_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ceChangeColour.EditValue = Session.ChangeColour
        ceFindColour.EditValue = Session.FindColour

        Dim _u As Business.User = Business.User.RetreiveByID(Session.CurrentUser.ID)
        txtSignature.HTMLText = _u._Signature
        _u = Nothing

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click

        Session.ChangeColour = ceChangeColour.Color
        UserPreferenceHandler.SetString("CHANGECOLOUR", ceChangeColour.Text)

        Session.FindColour = ceFindColour.Color
        UserPreferenceHandler.SetString("FINDCOLOUR", ceFindColour.Text)

        Dim _u As Business.User = Business.User.RetreiveByID(Session.CurrentUser.ID)
        _u._Signature = txtSignature.HTMLText
        _u.Store()

        Me.Close()

    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click

        If CareMessage("Are you sure you want to Clear your preferences?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Clear Preferences") = DialogResult.Yes Then

            Dim _SQL As String = $"delete from AppUserPreferences where user_id = '{Session.CurrentUser.ID.ToString}'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

            'default the colours and the theme
            UserPreferenceHandler.Create("CHANGECOLOUR", UserPreferenceHandler.EnumUserPreferenceType.StringType, "Yellow")
            UserPreferenceHandler.Create("FINDCOLOUR", UserPreferenceHandler.EnumUserPreferenceType.StringType, "LightGreen")
            UserPreferenceHandler.Create("THEME", UserPreferenceHandler.EnumUserPreferenceType.StringType, "DevExpress Style")

            Me.Close()

        End If

    End Sub
End Class
