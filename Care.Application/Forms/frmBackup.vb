﻿Imports Care.Global
Imports Care.Shared

Public Class frmBackup

    Private Sub frmBackup_Load(sender As Object, e As System.EventArgs) Handles Me.Load


    End Sub

    Private Sub btnBackup_Click(sender As System.Object, e As System.EventArgs) Handles btnBackup.Click

        SetCMDs(False)
        Session.CursorWaiting()
        Session.SetProgressMessage("Performing Database Backup...")

        Dim _b As New BackupEngine
        _b.Backup()

        SetCMDs(True)
        Session.CursorDefault()

        Session.SetProgressMessage(_b.Status, 5)

        If _b.Result = 0 Then
            CareMessage(_b.Status, MessageBoxIcon.Information, "Backup Database")
        Else
            CareMessage(_b.Status, MessageBoxIcon.Exclamation, "Backup Database")
        End If

    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnOpenFolder_Click(sender As Object, e As EventArgs) Handles btnOpenFolder.Click
        Dim _Path As String = ParameterHandler.ReturnPath("BACKUPPATH")
        If _Path <> "" Then
            Process.Start("explorer.exe", _Path)
        End If
    End Sub

    Private Sub SetCMDs(ByVal Enabled As Boolean)
        btnOpenFolder.Enabled = Enabled
        btnBackup.Enabled = Enabled
        btnClose.Enabled = Enabled
    End Sub

End Class