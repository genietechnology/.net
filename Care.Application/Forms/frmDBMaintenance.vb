﻿Imports Care.Global
Imports Care.Data

Public Class frmDBMaintenance

    Private Sub frmDBMaintenance_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        StatTables()
    End Sub

    Private Sub radStatTables_CheckedChanged(sender As Object, e As EventArgs) Handles radStatTables.CheckedChanged
        StatTables()
    End Sub

    Private Sub radStatIndexes_CheckedChanged(sender As Object, e As EventArgs) Handles radStatIndexes.CheckedChanged
        StatIndexes()
    End Sub

    Private Sub radIndexFrag_CheckedChanged(sender As Object, e As EventArgs) Handles radIndexFrag.CheckedChanged
        ShowFragmentation()
    End Sub

#Region "Grids"

    Private Sub StatTables()

        Dim _SQL As String = ""

        _SQL += "SELECT t.Name AS TableName, s.Name AS SchemaName, p.Rows AS RowCounts, SUM(a.total_pages) * 8 AS TotalSpaceKB,"
        _SQL += " SUM(a.used_pages) * 8 AS UsedSpaceKB, (SUM(a.total_pages) - SUM(a.used_pages)) * 8 AS UnusedSpaceKB"
        _SQL += " FROM sys.tables t"
        _SQL += " INNER JOIN sys.indexes i On t.object_id = i.object_id"
        _SQL += " INNER JOIN sys.partitions p On i.object_id = p.object_id And i.index_id = p.index_id"
        _SQL += " INNER JOIN sys.allocation_units a On p.partition_id = a.container_id"
        _SQL += " LEFT OUTER JOIN sys.schemas s On t.schema_id = s.schema_id"
        _SQL += " WHERE t.Name not like 'dt%' AND t.is_ms_shipped = 0 AND i.object_id > 255"
        _SQL += " GROUP BY t.Name, s.Name, p.Rows"
        _SQL += " ORDER BY t.Name"

        grd.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub StatIndexes()

        Dim _SQL As String = ""

        _SQL += "SELECT OBJECT_NAME(i.object_id) AS TableName, i.name AS IndexName, i.index_id AS IndexID, 8 * SUM(a.used_pages) AS 'Indexsize(KB)'"
        _SQL += " FROM sys.indexes AS i"
        _SQL += " JOIN sys.partitions As p On p.object_id = i.object_id And p.index_id = i.index_id"
        _SQL += " JOIN sys.allocation_units AS a ON a.container_id = p.partition_id"
        _SQL += " WHERE i.is_primary_key = 0 AND i.object_id > 255"
        _SQL += " AND I.name like '%IX_%'"
        _SQL += " GROUP BY i.OBJECT_ID, i.index_id, i.name"
        _SQL += " ORDER BY OBJECT_NAME(i.object_id),i.index_id"

        grd.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub ShowFragmentation()

        Dim _SQL As String = ""

        _SQL += "SELECT DB_NAME([ddips].[database_id]) AS [DatabaseName], OBJECT_NAME([ddips].[object_id]) AS [TableName], [i].[name] AS [IndexName], [ddips].[avg_fragmentation_in_percent]"
        _SQL += " FROM [sys].[dm_db_index_physical_stats] (DB_ID(), NULL, NULL, NULL, NULL) AS ddips"
        _SQL += " INNER JOIN [sys].[indexes] As i On [ddips].[index_id] = [i].[index_id] And [ddips].[object_id] = [i].[object_id]"
        _SQL += " WHERE i.is_primary_key = 0 AND i.object_id > 255"
        _SQL += " AND I.name like '%IX_%'"
        _SQL += " order by [ddips].[avg_fragmentation_in_percent] desc"

        grd.Populate(Session.ConnectionString, _SQL)

    End Sub

#End Region

    Private Function GetGridValue(ByVal Column As String) As String

        If grd.RecordCount = 0 Then Return ""
        Dim _Return As String = ""

        Try
            If grd.CurrentRow.Item(Column).ToString <> "" Then
                _Return = grd.CurrentRow.Item(Column).ToString
            End If

        Catch ex As Exception

        End Try

        Return _Return

    End Function

    Private Sub btnRebuildSelected_Click(sender As Object, e As EventArgs) Handles btnRebuildSelected.Click
        ToggleWait(True)
        Dim _IndexName As String = GetGridValue("IndexName")
        Dim _TableName As String = GetGridValue("TableName")
        If _IndexName <> "" Then
            DAL.IndexReBuild(Session.ConnectionString, _IndexName, _TableName)
            ShowFragmentation()
        End If
        ToggleWait(False)
    End Sub

    Private Sub btnReorganiseSelected_Click(sender As Object, e As EventArgs) Handles btnReorganiseSelected.Click
        ToggleWait(True)
        Dim _IndexName As String = GetGridValue("IndexName")
        Dim _TableName As String = GetGridValue("TableName")
        If _IndexName <> "" Then
            DAL.IndexReOrganise(Session.ConnectionString, _IndexName, _TableName)
            ShowFragmentation()
        End If
        ToggleWait(False)
    End Sub

    Private Sub btnUpdateStatistics_Click(sender As Object, e As EventArgs) Handles btnUpdateStatistics.Click
        ToggleWait(True)
        DAL.ExecuteSP(Session.ConnectionString, "sp_updatestats")
        ToggleWait(False)
    End Sub

    Private Sub btnRebuildAll_Click(sender As Object, e As EventArgs) Handles btnRebuildAll.Click
        ToggleWait(True)
        Dim _db As New Care.Shared.DBMaintenance
        _db.RebuildIndexes()
        _db = Nothing
        ToggleWait(False)
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub ToggleWait(ByVal Working As Boolean)

        If Working Then
            Session.CursorWaiting()
        Else
            Session.CursorDefault()
        End If

        btnRebuildSelected.Enabled = Not Working
        btnReorganiseSelected.Enabled = Not Working
        btnUpdateStatistics.Enabled = Not Working
        btnRebuildAll.Enabled = Not Working
        btnClose.Enabled = Not Working
        Application.DoEvents()

    End Sub

End Class