﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUserPreferences
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUserPreferences))
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.CareLabel2 = New Care.Controls.CareLabel()
        Me.CareLabel1 = New Care.Controls.CareLabel()
        Me.ceFindColour = New DevExpress.XtraEditors.ColorEdit()
        Me.ceChangeColour = New DevExpress.XtraEditors.ColorEdit()
        Me.btnOK = New Care.Controls.CareButton()
        Me.btnCancel = New Care.Controls.CareButton()
        Me.btnClear = New Care.Controls.CareButton()
        Me.CareFrame1 = New Care.Controls.CareFrame()
        Me.txtSignature = New Care.Controls.CareRichText()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.ceFindColour.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceChangeColour.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CareFrame1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CareFrame1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Controls.Add(Me.ceFindColour)
        Me.GroupControl1.Controls.Add(Me.ceChangeColour)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(803, 43)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "GroupControl1"
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.Options.UseFont = True
        Me.CareLabel2.Appearance.Options.UseTextOptions = True
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(503, 14)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(62, 15)
        Me.CareLabel2.TabIndex = 2
        Me.CareLabel2.Text = "Find Colour"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.Options.UseFont = True
        Me.CareLabel1.Appearance.Options.UseTextOptions = True
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(10, 14)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(80, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Change Colour"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ceFindColour
        '
        Me.ceFindColour.EditValue = System.Drawing.Color.Empty
        Me.ceFindColour.Location = New System.Drawing.Point(571, 12)
        Me.ceFindColour.Name = "ceFindColour"
        Me.ceFindColour.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ceFindColour.Properties.ShowColorDialog = False
        Me.ceFindColour.Properties.ShowCustomColors = False
        Me.ceFindColour.Properties.ShowSystemColors = False
        Me.ceFindColour.Size = New System.Drawing.Size(218, 20)
        Me.ceFindColour.TabIndex = 3
        '
        'ceChangeColour
        '
        Me.ceChangeColour.EditValue = System.Drawing.Color.Empty
        Me.ceChangeColour.Location = New System.Drawing.Point(105, 12)
        Me.ceChangeColour.Name = "ceChangeColour"
        Me.ceChangeColour.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ceChangeColour.Properties.ShowColorDialog = False
        Me.ceChangeColour.Properties.ShowCustomColors = False
        Me.ceChangeColour.Properties.ShowSystemColors = False
        Me.ceChangeColour.Size = New System.Drawing.Size(218, 20)
        Me.ceChangeColour.TabIndex = 1
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.Location = New System.Drawing.Point(659, 415)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 3
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(740, 415)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "Cancel"
        '
        'btnClear
        '
        Me.btnClear.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnClear.Location = New System.Drawing.Point(12, 415)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(138, 23)
        Me.btnClear.TabIndex = 2
        Me.btnClear.Text = "Clear My Preferences"
        '
        'CareFrame1
        '
        Me.CareFrame1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CareFrame1.Controls.Add(Me.txtSignature)
        Me.CareFrame1.Location = New System.Drawing.Point(12, 61)
        Me.CareFrame1.Name = "CareFrame1"
        Me.CareFrame1.Size = New System.Drawing.Size(803, 345)
        Me.CareFrame1.TabIndex = 1
        Me.CareFrame1.Text = "Autosignature for Communications"
        '
        'txtSignature
        '
        Me.txtSignature.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSignature.HTMLText = resources.GetString("txtSignature.HTMLText")
        Me.txtSignature.Location = New System.Drawing.Point(10, 26)
        Me.txtSignature.Name = "txtSignature"
        Me.txtSignature.RTFText = resources.GetString("txtSignature.RTFText")
        Me.txtSignature.Size = New System.Drawing.Size(782, 311)
        Me.txtSignature.TabIndex = 0
        '
        'frmUserPreferences
        '
        Me.AcceptButton = Me.btnOK
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(827, 447)
        Me.Controls.Add(Me.CareFrame1)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.GroupControl1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmUserPreferences"
        Me.Text = "User Preferences"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.ceFindColour.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceChangeColour.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CareFrame1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CareFrame1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ceChangeColour As DevExpress.XtraEditors.ColorEdit
    Friend WithEvents ceFindColour As DevExpress.XtraEditors.ColorEdit
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents btnClear As Controls.CareButton
    Friend WithEvents GroupControl1 As Controls.CareFrame
    Friend WithEvents CareFrame1 As Controls.CareFrame
    Friend WithEvents txtSignature As Controls.CareRichText
End Class
