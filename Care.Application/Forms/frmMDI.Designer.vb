﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMDI
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim AlertButton1 As DevExpress.XtraBars.Alerter.AlertButton = New DevExpress.XtraBars.Alerter.AlertButton()
        Dim AlertButton2 As DevExpress.XtraBars.Alerter.AlertButton = New DevExpress.XtraBars.Alerter.AlertButton()
        Dim AlertButton3 As DevExpress.XtraBars.Alerter.AlertButton = New DevExpress.XtraBars.Alerter.AlertButton()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMDI))
        Me.riThemes = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox()
        Me.DockManager1 = New DevExpress.XtraBars.Docking.DockManager(Me.components)
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.TopMenuBar = New DevExpress.XtraBars.Bar()
        Me.mnuFile = New DevExpress.XtraBars.BarSubItem()
        Me.mnuExit = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuData = New DevExpress.XtraBars.BarSubItem()
        Me.mnuDataExplorer = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiReportView = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReportDesigner = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuTools = New DevExpress.XtraBars.BarSubItem()
        Me.mnuChangePassword = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuUserPreferences = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiCallLog = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiPostCodeLookup = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSendEmail = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSendSMS = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSystem = New DevExpress.XtraBars.BarSubItem()
        Me.mnuParameters = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuLists = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReportManager = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuUsers = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuMenuManager = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSecurity = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuTaskScheduler = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuBackup = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuImportExport = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiUpdates = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuDevelopment = New DevExpress.XtraBars.BarSubItem()
        Me.mnuDBDiff = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuDBMaintenance = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuErrors = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuWindow = New DevExpress.XtraBars.BarSubItem()
        Me.BarMdiChildrenListItem1 = New DevExpress.XtraBars.BarMdiChildrenListItem()
        Me.mnuHelp = New DevExpress.XtraBars.BarSubItem()
        Me.mnuChatNow = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuHelpdesk = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuRemoteAssist = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuLicensing = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuAbout = New DevExpress.XtraBars.BarButtonItem()
        Me.bsiComputerName = New DevExpress.XtraBars.BarStaticItem()
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.bsiProgressText = New DevExpress.XtraBars.BarStaticItem()
        Me.beiProgressBar = New DevExpress.XtraBars.BarEditItem()
        Me.riProgress = New DevExpress.XtraEditors.Repository.RepositoryItemProgressBar()
        Me.bsiUserName = New DevExpress.XtraBars.BarStaticItem()
        Me.cbxThemes = New DevExpress.XtraBars.BarEditItem()
        Me.bsiCTIDisabled = New DevExpress.XtraBars.BarStaticItem()
        Me.bsiCTIEnabled = New DevExpress.XtraBars.BarStaticItem()
        Me.bbiChat = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.BarSubItem1 = New DevExpress.XtraBars.BarSubItem()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarSubItem3 = New DevExpress.XtraBars.BarSubItem()
        Me.mnuApplication = New DevExpress.XtraBars.BarSubItem()
        Me.BarLargeButtonItem1 = New DevExpress.XtraBars.BarLargeButtonItem()
        Me.mnuGroups = New DevExpress.XtraBars.BarButtonItem()
        Me.BarEditItem1 = New DevExpress.XtraBars.BarEditItem()
        Me.BarSubItem2 = New DevExpress.XtraBars.BarSubItem()
        Me.BarButtonItem2 = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuNewDoc = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem5 = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuDocumentManagement = New DevExpress.XtraBars.BarSubItem()
        Me.mnuViewDocs = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuLetterMaint = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem6 = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuUpdate = New DevExpress.XtraBars.BarButtonItem()
        Me.bsiUserLabel = New DevExpress.XtraBars.BarStaticItem()
        Me.bbiCalendars = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem3 = New DevExpress.XtraBars.BarButtonItem()
        Me.dpLeft = New DevExpress.XtraBars.Docking.DockPanel()
        Me.DockPanel1_Container = New DevExpress.XtraBars.Docking.ControlContainer()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.navMenu = New DevExpress.XtraNavBar.NavBarControl()
        Me.txtSearch = New DevExpress.XtraEditors.TextEdit()
        Me.dpRight = New DevExpress.XtraBars.Docking.DockPanel()
        Me.DockPanel2_Container = New DevExpress.XtraBars.Docking.ControlContainer()
        Me.webChat = New System.Windows.Forms.WebBrowser()
        Me.SharedDictionaryStorage1 = New DevExpress.XtraSpellChecker.SharedDictionaryStorage(Me.components)
        Me.timMessage = New System.Windows.Forms.Timer(Me.components)
        Me.alcCTI = New DevExpress.XtraBars.Alerter.AlertControl(Me.components)
        Me.bgwSpellChecker = New System.ComponentModel.BackgroundWorker()
        Me.bgwCheckIn = New System.ComponentModel.BackgroundWorker()
        Me.bgwBrowsers = New System.ComponentModel.BackgroundWorker()
        Me.bgwCache = New System.ComponentModel.BackgroundWorker()
        Me.bgwCTI = New System.ComponentModel.BackgroundWorker()
        Me.SplashScreenManager1 = New DevExpress.XtraSplashScreen.SplashScreenManager(Me, GetType(Global.Care.Framework.frmWaitForm), True, True)
        CType(Me.riThemes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DockManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riProgress, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.dpLeft.SuspendLayout()
        Me.DockPanel1_Container.SuspendLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.navMenu, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSearch.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.dpRight.SuspendLayout()
        Me.DockPanel2_Container.SuspendLayout()
        Me.SuspendLayout()
        '
        'riThemes
        '
        Me.riThemes.AutoHeight = False
        Me.riThemes.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.riThemes.Name = "riThemes"
        '
        'DockManager1
        '
        Me.DockManager1.Form = Me
        Me.DockManager1.MenuManager = Me.BarManager1
        Me.DockManager1.RootPanels.AddRange(New DevExpress.XtraBars.Docking.DockPanel() {Me.dpLeft, Me.dpRight})
        Me.DockManager1.TopZIndexControls.AddRange(New String() {"DevExpress.XtraBars.BarDockControl", "DevExpress.XtraBars.StandaloneBarDockControl", "System.Windows.Forms.StatusBar", "DevExpress.XtraBars.Ribbon.RibbonStatusBar", "DevExpress.XtraBars.Ribbon.RibbonControl"})
        '
        'BarManager1
        '
        Me.BarManager1.AllowCustomization = False
        Me.BarManager1.AllowQuickCustomization = False
        Me.BarManager1.AllowShowToolbarsPopup = False
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.TopMenuBar, Me.Bar1})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.DockManager = Me.DockManager1
        Me.BarManager1.DockWindowTabFont = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarSubItem1, Me.BarButtonItem1, Me.BarMdiChildrenListItem1, Me.mnuWindow, Me.BarSubItem3, Me.mnuApplication, Me.mnuTools, Me.mnuSystem, Me.mnuFile, Me.BarLargeButtonItem1, Me.mnuExit, Me.mnuChangePassword, Me.mnuSendEmail, Me.mnuSendSMS, Me.mnuParameters, Me.mnuUsers, Me.mnuMenuManager, Me.mnuGroups, Me.mnuUserPreferences, Me.BarEditItem1, Me.BarSubItem2, Me.cbxThemes, Me.mnuHelp, Me.mnuHelpdesk, Me.mnuRemoteAssist, Me.mnuAbout, Me.BarButtonItem2, Me.mnuBackup, Me.mnuNewDoc, Me.BarButtonItem5, Me.mnuDocumentManagement, Me.mnuLetterMaint, Me.BarButtonItem6, Me.mnuUpdate, Me.mnuViewDocs, Me.bsiUserLabel, Me.bsiProgressText, Me.bsiUserName, Me.beiProgressBar, Me.bbiPostCodeLookup, Me.mnuLists, Me.mnuDataExplorer, Me.mnuData, Me.mnuReportDesigner, Me.bbiCalendars, Me.bsiCTIEnabled, Me.bsiCTIDisabled, Me.bbiChat, Me.mnuLicensing, Me.bbiReportView, Me.mnuSecurity, Me.mnuImportExport, Me.mnuTaskScheduler, Me.mnuReportManager, Me.bsiComputerName, Me.BarButtonItem3, Me.mnuChatNow, Me.bbiCallLog, Me.bbiUpdates, Me.mnuDevelopment, Me.mnuDBDiff, Me.mnuDBMaintenance, Me.mnuErrors})
        Me.BarManager1.MainMenu = Me.TopMenuBar
        Me.BarManager1.MaxItemId = 70
        Me.BarManager1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riProgress})
        '
        'TopMenuBar
        '
        Me.TopMenuBar.BarName = "Main menu"
        Me.TopMenuBar.DockCol = 0
        Me.TopMenuBar.DockRow = 0
        Me.TopMenuBar.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.TopMenuBar.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFile), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuData), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuTools), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSystem), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuDevelopment), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuWindow), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHelp), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.bsiComputerName, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.TopMenuBar.OptionsBar.AllowQuickCustomization = False
        Me.TopMenuBar.OptionsBar.DisableClose = True
        Me.TopMenuBar.OptionsBar.DisableCustomization = True
        Me.TopMenuBar.OptionsBar.DrawDragBorder = False
        Me.TopMenuBar.OptionsBar.MultiLine = True
        Me.TopMenuBar.OptionsBar.UseWholeRow = True
        Me.TopMenuBar.Text = "Main menu"
        '
        'mnuFile
        '
        Me.mnuFile.Caption = "File"
        Me.mnuFile.Id = 9
        Me.mnuFile.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.mnuExit, DevExpress.XtraBars.BarItemPaintStyle.Standard)})
        Me.mnuFile.Name = "mnuFile"
        '
        'mnuExit
        '
        Me.mnuExit.Caption = "Exit"
        Me.mnuExit.Id = 11
        Me.mnuExit.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.logout
        Me.mnuExit.ImageOptions.ImageIndex = 11
        Me.mnuExit.Name = "mnuExit"
        '
        'mnuData
        '
        Me.mnuData.Caption = "Data && Reporting"
        Me.mnuData.Id = 49
        Me.mnuData.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuDataExplorer), New DevExpress.XtraBars.LinkPersistInfo(Me.bbiReportView), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReportDesigner)})
        Me.mnuData.Name = "mnuData"
        '
        'mnuDataExplorer
        '
        Me.mnuDataExplorer.Caption = "Data Explorer"
        Me.mnuDataExplorer.Id = 48
        Me.mnuDataExplorer.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.search_database
        Me.mnuDataExplorer.Name = "mnuDataExplorer"
        '
        'bbiReportView
        '
        Me.bbiReportView.Caption = "Report Centre"
        Me.bbiReportView.Id = 56
        Me.bbiReportView.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.content_tree
        Me.bbiReportView.Name = "bbiReportView"
        '
        'mnuReportDesigner
        '
        Me.mnuReportDesigner.Caption = "Report Designer"
        Me.mnuReportDesigner.Id = 50
        Me.mnuReportDesigner.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.reportdesigner
        Me.mnuReportDesigner.Name = "mnuReportDesigner"
        '
        'mnuTools
        '
        Me.mnuTools.Caption = "Tools"
        Me.mnuTools.Id = 6
        Me.mnuTools.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuChangePassword), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUserPreferences), New DevExpress.XtraBars.LinkPersistInfo(Me.bbiCallLog, True), New DevExpress.XtraBars.LinkPersistInfo(Me.bbiPostCodeLookup), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSendEmail, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSendSMS)})
        Me.mnuTools.Name = "mnuTools"
        '
        'mnuChangePassword
        '
        Me.mnuChangePassword.Caption = "Change Password"
        Me.mnuChangePassword.Id = 12
        Me.mnuChangePassword.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.keys
        Me.mnuChangePassword.ImageOptions.ImageIndex = 2
        Me.mnuChangePassword.Name = "mnuChangePassword"
        '
        'mnuUserPreferences
        '
        Me.mnuUserPreferences.Caption = "User Preferences"
        Me.mnuUserPreferences.Id = 22
        Me.mnuUserPreferences.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.themes
        Me.mnuUserPreferences.ImageOptions.ImageIndex = 5
        Me.mnuUserPreferences.Name = "mnuUserPreferences"
        '
        'bbiCallLog
        '
        Me.bbiCallLog.Caption = "View Call Log"
        Me.bbiCallLog.Id = 64
        Me.bbiCallLog.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.contact_32
        Me.bbiCallLog.Name = "bbiCallLog"
        '
        'bbiPostCodeLookup
        '
        Me.bbiPostCodeLookup.Caption = "Post Code Lookup"
        Me.bbiPostCodeLookup.Id = 46
        Me.bbiPostCodeLookup.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.FAQ
        Me.bbiPostCodeLookup.Name = "bbiPostCodeLookup"
        '
        'mnuSendEmail
        '
        Me.mnuSendEmail.Caption = "Send Email"
        Me.mnuSendEmail.Id = 13
        Me.mnuSendEmail.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.email
        Me.mnuSendEmail.ImageOptions.ImageIndex = 4
        Me.mnuSendEmail.Name = "mnuSendEmail"
        '
        'mnuSendSMS
        '
        Me.mnuSendSMS.Caption = "Send SMS"
        Me.mnuSendSMS.Id = 14
        Me.mnuSendSMS.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.Mobile
        Me.mnuSendSMS.ImageOptions.ImageIndex = 3
        Me.mnuSendSMS.Name = "mnuSendSMS"
        '
        'mnuSystem
        '
        Me.mnuSystem.Caption = "System"
        Me.mnuSystem.Id = 7
        Me.mnuSystem.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuParameters), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuLists), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReportManager), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuUsers, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuMenuManager), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSecurity), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuTaskScheduler, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuBackup), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuImportExport), New DevExpress.XtraBars.LinkPersistInfo(Me.bbiUpdates, True)})
        Me.mnuSystem.Name = "mnuSystem"
        '
        'mnuParameters
        '
        Me.mnuParameters.Caption = "Parameters"
        Me.mnuParameters.Id = 15
        Me.mnuParameters.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.wheel
        Me.mnuParameters.ImageOptions.ImageIndex = 6
        Me.mnuParameters.Name = "mnuParameters"
        '
        'mnuLists
        '
        Me.mnuLists.Caption = "List Manager"
        Me.mnuLists.Id = 47
        Me.mnuLists.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.catalog32
        Me.mnuLists.Name = "mnuLists"
        '
        'mnuReportManager
        '
        Me.mnuReportManager.Caption = "Report Manager"
        Me.mnuReportManager.Id = 60
        Me.mnuReportManager.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.printer_32
        Me.mnuReportManager.Name = "mnuReportManager"
        '
        'mnuUsers
        '
        Me.mnuUsers.Caption = "Users"
        Me.mnuUsers.Id = 16
        Me.mnuUsers.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.users
        Me.mnuUsers.ImageOptions.ImageIndex = 0
        Me.mnuUsers.Name = "mnuUsers"
        '
        'mnuMenuManager
        '
        Me.mnuMenuManager.Caption = "Menu Manager"
        Me.mnuMenuManager.Id = 17
        Me.mnuMenuManager.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.content_tree
        Me.mnuMenuManager.ImageOptions.ImageIndex = 7
        Me.mnuMenuManager.Name = "mnuMenuManager"
        '
        'mnuSecurity
        '
        Me.mnuSecurity.Caption = "Security"
        Me.mnuSecurity.Id = 57
        Me.mnuSecurity.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.locked_32
        Me.mnuSecurity.Name = "mnuSecurity"
        '
        'mnuTaskScheduler
        '
        Me.mnuTaskScheduler.Caption = "Task Scheduler"
        Me.mnuTaskScheduler.Id = 59
        Me.mnuTaskScheduler.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.alarm_32
        Me.mnuTaskScheduler.Name = "mnuTaskScheduler"
        '
        'mnuBackup
        '
        Me.mnuBackup.Caption = "Backup"
        Me.mnuBackup.Id = 31
        Me.mnuBackup.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.accept_database
        Me.mnuBackup.Name = "mnuBackup"
        '
        'mnuImportExport
        '
        Me.mnuImportExport.Caption = "Import/Export"
        Me.mnuImportExport.Id = 58
        Me.mnuImportExport.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.import_export_32
        Me.mnuImportExport.Name = "mnuImportExport"
        '
        'bbiUpdates
        '
        Me.bbiUpdates.Caption = "Check for Updates"
        Me.bbiUpdates.Id = 65
        Me.bbiUpdates.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.uncompress_32
        Me.bbiUpdates.Name = "bbiUpdates"
        '
        'mnuDevelopment
        '
        Me.mnuDevelopment.Caption = "Development"
        Me.mnuDevelopment.Id = 66
        Me.mnuDevelopment.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuDBDiff), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuDBMaintenance), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuErrors)})
        Me.mnuDevelopment.Name = "mnuDevelopment"
        '
        'mnuDBDiff
        '
        Me.mnuDBDiff.Caption = "DB Comparison"
        Me.mnuDBDiff.Id = 67
        Me.mnuDBDiff.Name = "mnuDBDiff"
        '
        'mnuDBMaintenance
        '
        Me.mnuDBMaintenance.Caption = "DB Maintenance"
        Me.mnuDBMaintenance.Id = 68
        Me.mnuDBMaintenance.Name = "mnuDBMaintenance"
        '
        'mnuErrors
        '
        Me.mnuErrors.Caption = "Exception Logs"
        Me.mnuErrors.Id = 69
        Me.mnuErrors.Name = "mnuErrors"
        '
        'mnuWindow
        '
        Me.mnuWindow.Caption = "Window"
        Me.mnuWindow.Id = 3
        Me.mnuWindow.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarMdiChildrenListItem1)})
        Me.mnuWindow.Name = "mnuWindow"
        '
        'BarMdiChildrenListItem1
        '
        Me.BarMdiChildrenListItem1.Caption = "BarMdiChildrenListItem1"
        Me.BarMdiChildrenListItem1.Id = 2
        Me.BarMdiChildrenListItem1.Name = "BarMdiChildrenListItem1"
        '
        'mnuHelp
        '
        Me.mnuHelp.Caption = "Help"
        Me.mnuHelp.Id = 26
        Me.mnuHelp.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuChatNow), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHelpdesk), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuRemoteAssist), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuLicensing, True), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAbout)})
        Me.mnuHelp.Name = "mnuHelp"
        '
        'mnuChatNow
        '
        Me.mnuChatNow.Caption = "Chat Now"
        Me.mnuChatNow.Id = 63
        Me.mnuChatNow.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.communication_32
        Me.mnuChatNow.Name = "mnuChatNow"
        '
        'mnuHelpdesk
        '
        Me.mnuHelpdesk.Caption = "Launch Helpdesk"
        Me.mnuHelpdesk.Id = 27
        Me.mnuHelpdesk.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.help_desk
        Me.mnuHelpdesk.ImageOptions.ImageIndex = 9
        Me.mnuHelpdesk.Name = "mnuHelpdesk"
        '
        'mnuRemoteAssist
        '
        Me.mnuRemoteAssist.Caption = "Launch Remote Assistance"
        Me.mnuRemoteAssist.Id = 28
        Me.mnuRemoteAssist.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.doctor
        Me.mnuRemoteAssist.ImageOptions.ImageIndex = 1
        Me.mnuRemoteAssist.Name = "mnuRemoteAssist"
        '
        'mnuLicensing
        '
        Me.mnuLicensing.Caption = "Licensing"
        Me.mnuLicensing.Id = 55
        Me.mnuLicensing.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.License_32
        Me.mnuLicensing.Name = "mnuLicensing"
        '
        'mnuAbout
        '
        Me.mnuAbout.Caption = "About"
        Me.mnuAbout.Id = 29
        Me.mnuAbout.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.info
        Me.mnuAbout.ImageOptions.ImageIndex = 12
        Me.mnuAbout.Name = "mnuAbout"
        '
        'bsiComputerName
        '
        Me.bsiComputerName.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.bsiComputerName.Caption = "<Computer Name>"
        Me.bsiComputerName.Id = 61
        Me.bsiComputerName.Name = "bsiComputerName"
        '
        'Bar1
        '
        Me.Bar1.BarName = "StatusBar"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar1.FloatLocation = New System.Drawing.Point(475, 662)
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.bsiProgressText), New DevExpress.XtraBars.LinkPersistInfo(Me.beiProgressBar), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.bsiUserName, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.cbxThemes, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.bsiCTIDisabled, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.bsiCTIEnabled, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(Me.bbiChat)})
        Me.Bar1.OptionsBar.AllowQuickCustomization = False
        Me.Bar1.OptionsBar.AutoPopupMode = DevExpress.XtraBars.BarAutoPopupMode.None
        Me.Bar1.OptionsBar.DisableClose = True
        Me.Bar1.OptionsBar.DisableCustomization = True
        Me.Bar1.OptionsBar.DrawDragBorder = False
        Me.Bar1.OptionsBar.UseWholeRow = True
        Me.Bar1.Text = "StatusBar"
        '
        'bsiProgressText
        '
        Me.bsiProgressText.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left
        Me.bsiProgressText.Caption = "Processing..."
        Me.bsiProgressText.Id = 41
        Me.bsiProgressText.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.bsiProgressText.ItemAppearance.Normal.Options.UseFont = True
        Me.bsiProgressText.Name = "bsiProgressText"
        Me.bsiProgressText.TextAlignment = System.Drawing.StringAlignment.Far
        '
        'beiProgressBar
        '
        Me.beiProgressBar.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left
        Me.beiProgressBar.AutoFillWidth = True
        Me.beiProgressBar.Edit = Me.riProgress
        Me.beiProgressBar.Id = 45
        Me.beiProgressBar.Name = "beiProgressBar"
        '
        'riProgress
        '
        Me.riProgress.Name = "riProgress"
        '
        'bsiUserName
        '
        Me.bsiUserName.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.bsiUserName.Caption = "<User Name>"
        Me.bsiUserName.Id = 42
        Me.bsiUserName.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.couple_16
        Me.bsiUserName.Name = "bsiUserName"
        '
        'cbxThemes
        '
        Me.cbxThemes.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.cbxThemes.AllowRightClickInMenu = False
        Me.cbxThemes.Edit = Me.riThemes
        Me.cbxThemes.EditWidth = 150
        Me.cbxThemes.Id = 25
        Me.cbxThemes.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.themes_16
        Me.cbxThemes.Name = "cbxThemes"
        '
        'bsiCTIDisabled
        '
        Me.bsiCTIDisabled.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.bsiCTIDisabled.Id = 53
        Me.bsiCTIDisabled.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.red_16
        Me.bsiCTIDisabled.Name = "bsiCTIDisabled"
        '
        'bsiCTIEnabled
        '
        Me.bsiCTIEnabled.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.bsiCTIEnabled.Id = 52
        Me.bsiCTIEnabled.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.green_16
        Me.bsiCTIEnabled.Name = "bsiCTIEnabled"
        '
        'bbiChat
        '
        Me.bbiChat.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.bbiChat.Id = 54
        Me.bbiChat.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.message_16
        Me.bbiChat.Name = "bbiChat"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Manager = Me.BarManager1
        Me.barDockControlTop.Size = New System.Drawing.Size(737, 22)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 492)
        Me.barDockControlBottom.Manager = Me.BarManager1
        Me.barDockControlBottom.Size = New System.Drawing.Size(737, 31)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 22)
        Me.barDockControlLeft.Manager = Me.BarManager1
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 470)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(737, 22)
        Me.barDockControlRight.Manager = Me.BarManager1
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 470)
        '
        'BarSubItem1
        '
        Me.BarSubItem1.Caption = "Menu"
        Me.BarSubItem1.Id = 0
        Me.BarSubItem1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem1)})
        Me.BarSubItem1.Name = "BarSubItem1"
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "Exit"
        Me.BarButtonItem1.Id = 1
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'BarSubItem3
        '
        Me.BarSubItem3.Caption = "Menu"
        Me.BarSubItem3.Id = 4
        Me.BarSubItem3.Name = "BarSubItem3"
        '
        'mnuApplication
        '
        Me.mnuApplication.Caption = "Application"
        Me.mnuApplication.Id = 5
        Me.mnuApplication.Name = "mnuApplication"
        '
        'BarLargeButtonItem1
        '
        Me.BarLargeButtonItem1.Caption = "Exit"
        Me.BarLargeButtonItem1.Id = 10
        Me.BarLargeButtonItem1.Name = "BarLargeButtonItem1"
        '
        'mnuGroups
        '
        Me.mnuGroups.Caption = "User Groups"
        Me.mnuGroups.Id = 18
        Me.mnuGroups.Name = "mnuGroups"
        '
        'BarEditItem1
        '
        Me.BarEditItem1.Caption = "mnuTheme"
        Me.BarEditItem1.Edit = Me.riThemes
        Me.BarEditItem1.Id = 23
        Me.BarEditItem1.Name = "BarEditItem1"
        '
        'BarSubItem2
        '
        Me.BarSubItem2.Caption = "Theme"
        Me.BarSubItem2.Id = 24
        Me.BarSubItem2.Name = "BarSubItem2"
        '
        'BarButtonItem2
        '
        Me.BarButtonItem2.Caption = "Backup"
        Me.BarButtonItem2.Id = 30
        Me.BarButtonItem2.ImageOptions.ImageIndex = 10
        Me.BarButtonItem2.Name = "BarButtonItem2"
        '
        'mnuNewDoc
        '
        Me.mnuNewDoc.Caption = "Create New Document"
        Me.mnuNewDoc.Id = 32
        Me.mnuNewDoc.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.pen32
        Me.mnuNewDoc.Name = "mnuNewDoc"
        '
        'BarButtonItem5
        '
        Me.BarButtonItem5.Caption = "Create New Document"
        Me.BarButtonItem5.Id = 33
        Me.BarButtonItem5.Name = "BarButtonItem5"
        '
        'mnuDocumentManagement
        '
        Me.mnuDocumentManagement.Caption = "Document Management"
        Me.mnuDocumentManagement.Id = 34
        Me.mnuDocumentManagement.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuNewDoc), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuViewDocs), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuLetterMaint)})
        Me.mnuDocumentManagement.Name = "mnuDocumentManagement"
        '
        'mnuViewDocs
        '
        Me.mnuViewDocs.Caption = "View Documents"
        Me.mnuViewDocs.Id = 38
        Me.mnuViewDocs.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.Open_Folder_Full32
        Me.mnuViewDocs.Name = "mnuViewDocs"
        '
        'mnuLetterMaint
        '
        Me.mnuLetterMaint.Caption = "Letter Maintenance"
        Me.mnuLetterMaint.Id = 35
        Me.mnuLetterMaint.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.email_edit32
        Me.mnuLetterMaint.Name = "mnuLetterMaint"
        '
        'BarButtonItem6
        '
        Me.BarButtonItem6.Caption = "BarButtonItem6"
        Me.BarButtonItem6.Id = 36
        Me.BarButtonItem6.Name = "BarButtonItem6"
        '
        'mnuUpdate
        '
        Me.mnuUpdate.Caption = "Check for Updates"
        Me.mnuUpdate.Id = 37
        Me.mnuUpdate.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.update
        Me.mnuUpdate.Name = "mnuUpdate"
        '
        'bsiUserLabel
        '
        Me.bsiUserLabel.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.bsiUserLabel.Caption = "User"
        Me.bsiUserLabel.Id = 39
        Me.bsiUserLabel.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.users_16
        Me.bsiUserLabel.Name = "bsiUserLabel"
        '
        'bbiCalendars
        '
        Me.bbiCalendars.Caption = "Calendars"
        Me.bbiCalendars.Id = 51
        Me.bbiCalendars.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.calendar_32
        Me.bbiCalendars.Name = "bbiCalendars"
        '
        'BarButtonItem3
        '
        Me.BarButtonItem3.Caption = "Install Updates"
        Me.BarButtonItem3.Id = 62
        Me.BarButtonItem3.Name = "BarButtonItem3"
        '
        'dpLeft
        '
        Me.dpLeft.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.dpLeft.Appearance.Options.UseFont = True
        Me.dpLeft.Controls.Add(Me.DockPanel1_Container)
        Me.dpLeft.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left
        Me.dpLeft.ID = New System.Guid("b1b19c80-73ad-4fed-ba8f-e6fd3bbe37ce")
        Me.dpLeft.Location = New System.Drawing.Point(0, 22)
        Me.dpLeft.Name = "dpLeft"
        Me.dpLeft.Options.ShowCloseButton = False
        Me.dpLeft.Options.ShowMaximizeButton = False
        Me.dpLeft.OriginalSize = New System.Drawing.Size(150, 200)
        Me.dpLeft.Size = New System.Drawing.Size(150, 470)
        Me.dpLeft.Text = "Menu"
        '
        'DockPanel1_Container
        '
        Me.DockPanel1_Container.Controls.Add(Me.PanelControl1)
        Me.DockPanel1_Container.Location = New System.Drawing.Point(4, 23)
        Me.DockPanel1_Container.Name = "DockPanel1_Container"
        Me.DockPanel1_Container.Size = New System.Drawing.Size(141, 443)
        Me.DockPanel1_Container.TabIndex = 0
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.navMenu)
        Me.PanelControl1.Controls.Add(Me.txtSearch)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(141, 443)
        Me.PanelControl1.TabIndex = 15
        '
        'navMenu
        '
        Me.navMenu.Appearance.Background.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.navMenu.Appearance.Background.Options.UseFont = True
        Me.navMenu.Appearance.Button.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.navMenu.Appearance.Button.Options.UseFont = True
        Me.navMenu.Appearance.ButtonDisabled.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.navMenu.Appearance.ButtonDisabled.Options.UseFont = True
        Me.navMenu.Appearance.ButtonHotTracked.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.navMenu.Appearance.ButtonHotTracked.Options.UseFont = True
        Me.navMenu.Appearance.ButtonPressed.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.navMenu.Appearance.ButtonPressed.Options.UseFont = True
        Me.navMenu.Appearance.GroupBackground.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.navMenu.Appearance.GroupBackground.Options.UseFont = True
        Me.navMenu.Appearance.GroupHeader.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.navMenu.Appearance.GroupHeader.Options.UseFont = True
        Me.navMenu.Appearance.GroupHeaderActive.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.navMenu.Appearance.GroupHeaderActive.Options.UseFont = True
        Me.navMenu.Appearance.GroupHeaderHotTracked.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.navMenu.Appearance.GroupHeaderHotTracked.Options.UseFont = True
        Me.navMenu.Appearance.GroupHeaderPressed.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.navMenu.Appearance.GroupHeaderPressed.Options.UseFont = True
        Me.navMenu.Appearance.Hint.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.navMenu.Appearance.Hint.Options.UseFont = True
        Me.navMenu.Appearance.Item.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.navMenu.Appearance.Item.Options.UseFont = True
        Me.navMenu.Appearance.ItemActive.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.navMenu.Appearance.ItemActive.Options.UseFont = True
        Me.navMenu.Appearance.ItemDisabled.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.navMenu.Appearance.ItemDisabled.Options.UseFont = True
        Me.navMenu.Appearance.ItemHotTracked.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.navMenu.Appearance.ItemHotTracked.Options.UseFont = True
        Me.navMenu.Appearance.ItemPressed.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.navMenu.Appearance.ItemPressed.Options.UseFont = True
        Me.navMenu.Appearance.LinkDropTarget.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.navMenu.Appearance.LinkDropTarget.Options.UseFont = True
        Me.navMenu.Appearance.NavigationPaneHeader.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.navMenu.Appearance.NavigationPaneHeader.Options.UseFont = True
        Me.navMenu.Appearance.NavPaneContentButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.navMenu.Appearance.NavPaneContentButton.Options.UseFont = True
        Me.navMenu.Dock = System.Windows.Forms.DockStyle.Fill
        Me.navMenu.DragDropFlags = DevExpress.XtraNavBar.NavBarDragDrop.None
        Me.navMenu.ExplorerBarStretchLastGroup = True
        Me.navMenu.Location = New System.Drawing.Point(2, 22)
        Me.navMenu.Name = "navMenu"
        Me.navMenu.OptionsNavPane.ExpandedWidth = 137
        Me.navMenu.Size = New System.Drawing.Size(137, 419)
        Me.navMenu.TabIndex = 16
        Me.navMenu.Text = "NavBarControl1"
        '
        'txtSearch
        '
        Me.txtSearch.Dock = System.Windows.Forms.DockStyle.Top
        Me.txtSearch.Location = New System.Drawing.Point(2, 2)
        Me.txtSearch.MenuManager = Me.BarManager1
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(137, 20)
        Me.txtSearch.TabIndex = 7
        Me.txtSearch.ToolTip = "Type in here to search our menu system"
        Me.txtSearch.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.txtSearch.ToolTipTitle = "Quick Search"
        '
        'dpRight
        '
        Me.dpRight.Controls.Add(Me.DockPanel2_Container)
        Me.dpRight.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right
        Me.dpRight.ID = New System.Guid("0a384303-1890-4ca6-82f4-5d20b7c21e58")
        Me.dpRight.Location = New System.Drawing.Point(435, 22)
        Me.dpRight.Name = "dpRight"
        Me.dpRight.OriginalSize = New System.Drawing.Size(302, 200)
        Me.dpRight.Size = New System.Drawing.Size(302, 470)
        Me.dpRight.Text = "Web Chat"
        '
        'DockPanel2_Container
        '
        Me.DockPanel2_Container.Controls.Add(Me.webChat)
        Me.DockPanel2_Container.Location = New System.Drawing.Point(5, 23)
        Me.DockPanel2_Container.Name = "DockPanel2_Container"
        Me.DockPanel2_Container.Size = New System.Drawing.Size(293, 443)
        Me.DockPanel2_Container.TabIndex = 0
        '
        'webChat
        '
        Me.webChat.AllowNavigation = False
        Me.webChat.Dock = System.Windows.Forms.DockStyle.Fill
        Me.webChat.IsWebBrowserContextMenuEnabled = False
        Me.webChat.Location = New System.Drawing.Point(0, 0)
        Me.webChat.MinimumSize = New System.Drawing.Size(20, 20)
        Me.webChat.Name = "webChat"
        Me.webChat.Size = New System.Drawing.Size(293, 443)
        Me.webChat.TabIndex = 1
        Me.webChat.WebBrowserShortcutsEnabled = False
        '
        'timMessage
        '
        '
        'alcCTI
        '
        Me.alcCTI.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.alcCTI.AppearanceCaption.Options.UseFont = True
        Me.alcCTI.AppearanceText.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.alcCTI.AppearanceText.Options.UseFont = True
        AlertButton1.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.phone_24
        AlertButton1.Name = "albAnswer"
        AlertButton2.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.deny_24
        AlertButton2.Name = "albDeny"
        AlertButton3.ImageOptions.Image = Global.Care.Framework.My.Resources.Resources.search_24
        AlertButton3.Name = "albDrillDown"
        Me.alcCTI.Buttons.Add(AlertButton1)
        Me.alcCTI.Buttons.Add(AlertButton2)
        Me.alcCTI.Buttons.Add(AlertButton3)
        Me.alcCTI.ShowCloseButton = False
        Me.alcCTI.ShowPinButton = False
        '
        'bgwSpellChecker
        '
        '
        'bgwCheckIn
        '
        '
        'bgwBrowsers
        '
        '
        'bgwCache
        '
        '
        'bgwCTI
        '
        '
        'SplashScreenManager1
        '
        Me.SplashScreenManager1.ClosingDelay = 500
        '
        'frmMDI
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(737, 523)
        Me.Controls.Add(Me.dpRight)
        Me.Controls.Add(Me.dpLeft)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.KeyPreview = True
        Me.Name = "frmMDI"
        Me.Text = "Main"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.riThemes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DockManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riProgress, System.ComponentModel.ISupportInitialize).EndInit()
        Me.dpLeft.ResumeLayout(False)
        Me.DockPanel1_Container.ResumeLayout(False)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.navMenu, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSearch.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.dpRight.ResumeLayout(False)
        Me.DockPanel2_Container.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Friend WithEvents DockManager1 As DevExpress.XtraBars.Docking.DockManager
    Friend WithEvents dpLeft As DevExpress.XtraBars.Docking.DockPanel
    Friend WithEvents DockPanel1_Container As DevExpress.XtraBars.Docking.ControlContainer
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents TopMenuBar As DevExpress.XtraBars.Bar
    Friend WithEvents BarSubItem1 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuFile As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuExit As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuApplication As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuTools As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuChangePassword As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSendEmail As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSendSMS As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSystem As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuParameters As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuUsers As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuGroups As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuMenuManager As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuWindow As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarMdiChildrenListItem1 As DevExpress.XtraBars.BarMdiChildrenListItem
    Friend WithEvents BarSubItem3 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarLargeButtonItem1 As DevExpress.XtraBars.BarLargeButtonItem
    Friend WithEvents mnuUserPreferences As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarEditItem1 As DevExpress.XtraBars.BarEditItem
    Friend WithEvents BarSubItem2 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuHelp As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuHelpdesk As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuRemoteAssist As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuAbout As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents cbxThemes As DevExpress.XtraBars.BarEditItem
    Friend WithEvents BarButtonItem2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuBackup As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuDocumentManagement As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuNewDoc As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem5 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuLetterMaint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem6 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents SharedDictionaryStorage1 As DevExpress.XtraSpellChecker.SharedDictionaryStorage
    Friend WithEvents mnuUpdate As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuViewDocs As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents bsiUserLabel As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents bsiProgressText As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents riProgress As DevExpress.XtraEditors.Repository.RepositoryItemProgressBar
    Friend WithEvents bsiUserName As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents beiProgressBar As DevExpress.XtraBars.BarEditItem
    Friend WithEvents riThemes As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents timMessage As System.Windows.Forms.Timer
    Friend WithEvents bbiPostCodeLookup As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuLists As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuDataExplorer As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuData As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuReportDesigner As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiCalendars As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bsiCTIEnabled As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents alcCTI As DevExpress.XtraBars.Alerter.AlertControl
    Friend WithEvents bsiCTIDisabled As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents bbiChat As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuLicensing As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiReportView As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSecurity As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents txtSearch As DevExpress.XtraEditors.TextEdit
    Friend WithEvents navMenu As DevExpress.XtraNavBar.NavBarControl
    Friend WithEvents mnuImportExport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuTaskScheduler As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReportManager As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bsiComputerName As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents BarButtonItem3 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuChatNow As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiCallLog As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents dpRight As DevExpress.XtraBars.Docking.DockPanel
    Friend WithEvents DockPanel2_Container As DevExpress.XtraBars.Docking.ControlContainer
    Friend WithEvents webChat As WebBrowser
    Friend WithEvents bbiUpdates As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bgwSpellChecker As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgwCheckIn As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgwBrowsers As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgwCache As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgwCTI As System.ComponentModel.BackgroundWorker
    Friend WithEvents SplashScreenManager1 As DevExpress.XtraSplashScreen.SplashScreenManager
    Friend WithEvents mnuDevelopment As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuDBDiff As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuDBMaintenance As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuErrors As DevExpress.XtraBars.BarButtonItem
End Class
