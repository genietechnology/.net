﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDBMaintenance
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.grd = New Care.Controls.CareGrid()
        Me.radStatTables = New Care.Controls.CareRadioButton(Me.components)
        Me.radStatIndexes = New Care.Controls.CareRadioButton(Me.components)
        Me.radIndexFrag = New Care.Controls.CareRadioButton(Me.components)
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.btnRebuildSelected = New Care.Controls.CareButton(Me.components)
        Me.btnReorganiseSelected = New Care.Controls.CareButton(Me.components)
        Me.btnUpdateStatistics = New Care.Controls.CareButton(Me.components)
        Me.btnRebuildAll = New Care.Controls.CareButton(Me.components)
        CType(Me.radStatTables.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radStatIndexes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radIndexFrag.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'grd
        '
        Me.grd.AllowBuildColumns = True
        Me.grd.AllowEdit = False
        Me.grd.AllowHorizontalScroll = False
        Me.grd.AllowMultiSelect = True
        Me.grd.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grd.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grd.Appearance.Options.UseFont = True
        Me.grd.AutoSizeByData = True
        Me.grd.DisableAutoSize = False
        Me.grd.DisableDataFormatting = False
        Me.grd.FocusedRowHandle = -2147483648
        Me.grd.HideFirstColumn = False
        Me.grd.Location = New System.Drawing.Point(12, 36)
        Me.grd.Name = "grd"
        Me.grd.PreviewColumn = ""
        Me.grd.QueryID = Nothing
        Me.grd.RowAutoHeight = False
        Me.grd.SearchAsYouType = True
        Me.grd.ShowAutoFilterRow = False
        Me.grd.ShowFindPanel = False
        Me.grd.ShowGroupByBox = False
        Me.grd.ShowLoadingPanel = False
        Me.grd.ShowNavigator = True
        Me.grd.Size = New System.Drawing.Size(1190, 654)
        Me.grd.TabIndex = 3
        '
        'radStatTables
        '
        Me.radStatTables.EditValue = True
        Me.radStatTables.Location = New System.Drawing.Point(12, 11)
        Me.radStatTables.Name = "radStatTables"
        Me.radStatTables.Properties.AutoWidth = True
        Me.radStatTables.Properties.Caption = "Table Statistics"
        Me.radStatTables.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radStatTables.Properties.RadioGroupIndex = 1
        Me.radStatTables.Size = New System.Drawing.Size(94, 19)
        Me.radStatTables.TabIndex = 0
        '
        'radStatIndexes
        '
        Me.radStatIndexes.Location = New System.Drawing.Point(112, 11)
        Me.radStatIndexes.Name = "radStatIndexes"
        Me.radStatIndexes.Properties.AutoWidth = True
        Me.radStatIndexes.Properties.Caption = "Index Statistics"
        Me.radStatIndexes.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radStatIndexes.Properties.RadioGroupIndex = 1
        Me.radStatIndexes.Size = New System.Drawing.Size(96, 19)
        Me.radStatIndexes.TabIndex = 1
        Me.radStatIndexes.TabStop = False
        '
        'radIndexFrag
        '
        Me.radIndexFrag.Location = New System.Drawing.Point(214, 11)
        Me.radIndexFrag.Name = "radIndexFrag"
        Me.radIndexFrag.Properties.AutoWidth = True
        Me.radIndexFrag.Properties.Caption = "Index Fragmentation"
        Me.radIndexFrag.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radIndexFrag.Properties.RadioGroupIndex = 1
        Me.radIndexFrag.Size = New System.Drawing.Size(123, 19)
        Me.radIndexFrag.TabIndex = 2
        Me.radIndexFrag.TabStop = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClose.Appearance.Options.UseBackColor = True
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(1092, 697)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(110, 27)
        Me.btnClose.TabIndex = 8
        Me.btnClose.Text = "Close"
        '
        'btnRebuildSelected
        '
        Me.btnRebuildSelected.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRebuildSelected.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.btnRebuildSelected.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnRebuildSelected.Appearance.Options.UseBackColor = True
        Me.btnRebuildSelected.Appearance.Options.UseFont = True
        Me.btnRebuildSelected.Location = New System.Drawing.Point(12, 696)
        Me.btnRebuildSelected.Name = "btnRebuildSelected"
        Me.btnRebuildSelected.Size = New System.Drawing.Size(140, 27)
        Me.btnRebuildSelected.TabIndex = 4
        Me.btnRebuildSelected.Text = "Rebuild Selected"
        '
        'btnReorganiseSelected
        '
        Me.btnReorganiseSelected.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnReorganiseSelected.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.btnReorganiseSelected.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnReorganiseSelected.Appearance.Options.UseBackColor = True
        Me.btnReorganiseSelected.Appearance.Options.UseFont = True
        Me.btnReorganiseSelected.Location = New System.Drawing.Point(158, 696)
        Me.btnReorganiseSelected.Name = "btnReorganiseSelected"
        Me.btnReorganiseSelected.Size = New System.Drawing.Size(140, 27)
        Me.btnReorganiseSelected.TabIndex = 5
        Me.btnReorganiseSelected.Text = "Reorganise Selected"
        '
        'btnUpdateStatistics
        '
        Me.btnUpdateStatistics.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateStatistics.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.btnUpdateStatistics.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnUpdateStatistics.Appearance.Options.UseBackColor = True
        Me.btnUpdateStatistics.Appearance.Options.UseFont = True
        Me.btnUpdateStatistics.Location = New System.Drawing.Point(304, 697)
        Me.btnUpdateStatistics.Name = "btnUpdateStatistics"
        Me.btnUpdateStatistics.Size = New System.Drawing.Size(140, 27)
        Me.btnUpdateStatistics.TabIndex = 6
        Me.btnUpdateStatistics.Text = "Update Statistics"
        '
        'btnRebuildAll
        '
        Me.btnRebuildAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRebuildAll.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.btnRebuildAll.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnRebuildAll.Appearance.Options.UseBackColor = True
        Me.btnRebuildAll.Appearance.Options.UseFont = True
        Me.btnRebuildAll.Location = New System.Drawing.Point(450, 697)
        Me.btnRebuildAll.Name = "btnRebuildAll"
        Me.btnRebuildAll.Size = New System.Drawing.Size(140, 27)
        Me.btnRebuildAll.TabIndex = 7
        Me.btnRebuildAll.Text = "Rebuild All Indexes"
        '
        'frmDBMaintenance
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1214, 732)
        Me.Controls.Add(Me.btnRebuildAll)
        Me.Controls.Add(Me.btnUpdateStatistics)
        Me.Controls.Add(Me.btnReorganiseSelected)
        Me.Controls.Add(Me.btnRebuildSelected)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.radIndexFrag)
        Me.Controls.Add(Me.radStatIndexes)
        Me.Controls.Add(Me.radStatTables)
        Me.Controls.Add(Me.grd)
        Me.LoadMaximised = True
        Me.Margin = New System.Windows.Forms.Padding(3, 5, 3, 5)
        Me.Name = "frmDBMaintenance"
        Me.Text = "frmDBMaintenance"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.radStatTables.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radStatIndexes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radIndexFrag.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents grd As Controls.CareGrid
    Friend WithEvents radStatTables As Controls.CareRadioButton
    Friend WithEvents radStatIndexes As Controls.CareRadioButton
    Friend WithEvents radIndexFrag As Controls.CareRadioButton
    Friend WithEvents btnClose As Controls.CareButton
    Friend WithEvents btnRebuildSelected As Controls.CareButton
    Friend WithEvents btnReorganiseSelected As Controls.CareButton
    Friend WithEvents btnUpdateStatistics As Controls.CareButton
    Friend WithEvents btnRebuildAll As Controls.CareButton
End Class
