﻿Imports Care.Global
Imports Care.Shared
Imports System.Diagnostics.Eventing.Reader

Public Class frmLogin

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub frmLogin_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Session.ApplicationCode = "SPARK" Then
            Me.Height = 420
            picCareSoftware.Hide()
            picSpark.Show()
        End If

    End Sub

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        DoLogin()
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub frmLogin_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then Application.Exit()
        If e.KeyCode = Keys.Enter Then DoLogin()
    End Sub

    Private Sub DoLogin()

        If Debugger.IsAttached AndAlso txtUserName.Text = "" Then txtUserName.Text = "admin"

        If txtUserName.Text = "" Then
            CareMessage("Enter your username.", MessageBoxIcon.Exclamation, "Username Mandatory")
            txtUserName.Focus()
            Exit Sub
        End If

        Dim _User As Business.User = Business.User.RetrieveByName(txtUserName.Text)
        If _User IsNot Nothing Then

            Session.CurrentUser = New User()
            Session.CurrentUser.ID = _User._ID.Value
            Session.CurrentUser.UserCode = _User._Username
            Session.CurrentUser.FullName = _User._Fullname
            Session.CurrentUser.Type = _User._Type
            Session.CurrentUser.GroupID = _User._GroupId.ToString
            Session.CurrentUser.Email = _User._Email.ToString

            If _User._Password = "" Then
                If _User.ChangePassword(True) Then
                    Me.DialogResult = DialogResult.Yes
                End If
            Else

                If Debugger.IsAttached AndAlso txtPassword.Text = "" Then txtPassword.Text = EncyptionHandler.DecryptString(_User._Password)

                If txtPassword.Text = "" Then
                    CareMessage("Enter your password.", MessageBoxIcon.Exclamation, "Password Mandatory")
                    txtPassword.Focus()
                    Exit Sub
                End If

                _User.Authenticate(txtUserName.Text, txtPassword.Text)
                If _User.Authenticated Then
                    Me.DialogResult = DialogResult.Yes
                Else
                    CareMessage(_User.AuthenticationError, MessageBoxIcon.Error, "Authentication Error")
                End If

            End If

        Else
            CareMessage("User does not exist.", MessageBoxIcon.Error, "Authentication Error")
        End If

        _User = Nothing

    End Sub

    Private Sub frmLogin_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown

        Session.CursorDefault()
        Me.Focus()
        txtUserName.Focus()
        Application.DoEvents()

    End Sub
End Class
