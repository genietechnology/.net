﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUsers
    Inherits Care.Shared.frmGridMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtUserName = New Care.Controls.CareTextBox()
        Me.txtSurname = New Care.Controls.CareTextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtForename = New Care.Controls.CareTextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtDepartment = New Care.Controls.CareTextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtJobTitle = New Care.Controls.CareTextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtEmail = New Care.Controls.CareTextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtTel = New Care.Controls.CareTextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnReset = New Care.Controls.CareButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxType = New Care.Controls.CareComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.chkDisabled = New Care.Controls.CareCheckBox()
        Me.chkSSO = New Care.Controls.CareCheckBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtSSOUsername = New Care.Controls.CareTextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtPasswordNumeric = New Care.Controls.CareTextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtPasswordSpecial = New Care.Controls.CareTextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtPasswordLower = New Care.Controls.CareTextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtPasswordLength = New Care.Controls.CareTextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.btnPasswordEdit = New Care.Controls.CareButton()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.btnPasswordOK = New Care.Controls.CareButton()
        Me.btnPasswordCancel = New Care.Controls.CareButton()
        Me.txtPasswordUpper = New Care.Controls.CareTextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtPasswordHistory = New Care.Controls.CareTextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtPasswordExpiry = New Care.Controls.CareTextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.gbx.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.txtUserName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtForename.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDepartment.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJobTitle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkDisabled.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSSO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSSOUsername.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtPasswordNumeric.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPasswordSpecial.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPasswordLower.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPasswordLength.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPasswordUpper.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPasswordHistory.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPasswordExpiry.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbx
        '
        Me.gbx.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.gbx.Controls.Add(Me.Label11)
        Me.gbx.Controls.Add(Me.txtSSOUsername)
        Me.gbx.Controls.Add(Me.chkSSO)
        Me.gbx.Controls.Add(Me.Label10)
        Me.gbx.Controls.Add(Me.chkDisabled)
        Me.gbx.Controls.Add(Me.Label9)
        Me.gbx.Controls.Add(Me.Label2)
        Me.gbx.Controls.Add(Me.cbxType)
        Me.gbx.Controls.Add(Me.Label1)
        Me.gbx.Controls.Add(Me.txtEmail)
        Me.gbx.Controls.Add(Me.Label7)
        Me.gbx.Controls.Add(Me.txtTel)
        Me.gbx.Controls.Add(Me.Label8)
        Me.gbx.Controls.Add(Me.txtDepartment)
        Me.gbx.Controls.Add(Me.Label5)
        Me.gbx.Controls.Add(Me.txtJobTitle)
        Me.gbx.Controls.Add(Me.Label6)
        Me.gbx.Controls.Add(Me.txtSurname)
        Me.gbx.Controls.Add(Me.Label3)
        Me.gbx.Controls.Add(Me.txtForename)
        Me.gbx.Controls.Add(Me.Label4)
        Me.gbx.Controls.Add(Me.txtUserName)
        Me.gbx.Location = New System.Drawing.Point(85, 288)
        Me.gbx.Size = New System.Drawing.Size(662, 224)
        Me.gbx.TabIndex = 1
        Me.gbx.Controls.SetChildIndex(Me.txtUserName, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label4, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtForename, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label3, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtSurname, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label6, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtJobTitle, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label5, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtDepartment, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label8, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtTel, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label7, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtEmail, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label1, 0)
        Me.gbx.Controls.SetChildIndex(Me.cbxType, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label2, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label9, 0)
        Me.gbx.Controls.SetChildIndex(Me.chkDisabled, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label10, 0)
        Me.gbx.Controls.SetChildIndex(Me.chkSSO, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtSSOUsername, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label11, 0)
        Me.gbx.Controls.SetChildIndex(Me.Panel2, 0)
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnReset)
        Me.Panel2.Location = New System.Drawing.Point(3, 193)
        Me.Panel2.Size = New System.Drawing.Size(656, 28)
        Me.Panel2.TabIndex = 22
        Me.Panel2.Controls.SetChildIndex(Me.btnCancel, 0)
        Me.Panel2.Controls.SetChildIndex(Me.btnReset, 0)
        Me.Panel2.Controls.SetChildIndex(Me.btnOK, 0)
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(471, 0)
        Me.btnOK.TabIndex = 1
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(562, 0)
        Me.btnCancel.TabIndex = 2
        '
        'Grid
        '
        Me.Grid.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Grid.Appearance.Options.UseFont = True
        Me.Grid.Location = New System.Drawing.Point(7, 183)
        Me.Grid.ShowAutoFilterRow = True
        Me.Grid.ShowFindPanel = True
        Me.Grid.Size = New System.Drawing.Size(815, 384)
        Me.Grid.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(0, 568)
        Me.Panel1.Size = New System.Drawing.Size(834, 35)
        Me.Panel1.TabIndex = 0
        '
        'btnDelete
        '
        Me.btnDelete.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Appearance.Options.UseFont = True
        '
        'btnEdit
        '
        Me.btnEdit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.Appearance.Options.UseFont = True
        '
        'btnAdd
        '
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.Appearance.Options.UseFont = True
        '
        'btnClose
        '
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(7535, 5)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'txtUserName
        '
        Me.txtUserName.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtUserName.EnterMoveNextControl = True
        Me.txtUserName.Location = New System.Drawing.Point(113, 78)
        Me.txtUserName.MaxLength = 10
        Me.txtUserName.Name = "txtUserName"
        Me.txtUserName.NumericAllowNegatives = False
        Me.txtUserName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtUserName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtUserName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtUserName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUserName.Properties.Appearance.Options.UseFont = True
        Me.txtUserName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtUserName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtUserName.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtUserName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtUserName.Properties.MaxLength = 10
        Me.txtUserName.Size = New System.Drawing.Size(201, 22)
        Me.txtUserName.TabIndex = 5
        Me.txtUserName.Tag = "AE"
        Me.txtUserName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtUserName.ToolTipText = ""
        '
        'txtSurname
        '
        Me.txtSurname.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtSurname.EnterMoveNextControl = True
        Me.txtSurname.Location = New System.Drawing.Point(113, 50)
        Me.txtSurname.MaxLength = 30
        Me.txtSurname.Name = "txtSurname"
        Me.txtSurname.NumericAllowNegatives = False
        Me.txtSurname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSurname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSurname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSurname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSurname.Properties.Appearance.Options.UseFont = True
        Me.txtSurname.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSurname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSurname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSurname.Properties.MaxLength = 30
        Me.txtSurname.Size = New System.Drawing.Size(201, 22)
        Me.txtSurname.TabIndex = 3
        Me.txtSurname.Tag = "AE"
        Me.txtSurname.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSurname.ToolTipText = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 53)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 15)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Surname"
        '
        'txtForename
        '
        Me.txtForename.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtForename.EnterMoveNextControl = True
        Me.txtForename.Location = New System.Drawing.Point(113, 22)
        Me.txtForename.MaxLength = 30
        Me.txtForename.Name = "txtForename"
        Me.txtForename.NumericAllowNegatives = False
        Me.txtForename.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtForename.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtForename.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtForename.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtForename.Properties.Appearance.Options.UseFont = True
        Me.txtForename.Properties.Appearance.Options.UseTextOptions = True
        Me.txtForename.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtForename.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtForename.Properties.MaxLength = 30
        Me.txtForename.Size = New System.Drawing.Size(201, 22)
        Me.txtForename.TabIndex = 1
        Me.txtForename.Tag = "AE"
        Me.txtForename.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtForename.ToolTipText = ""
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(6, 25)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 15)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Forename"
        '
        'txtDepartment
        '
        Me.txtDepartment.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtDepartment.EnterMoveNextControl = True
        Me.txtDepartment.Location = New System.Drawing.Point(430, 22)
        Me.txtDepartment.MaxLength = 30
        Me.txtDepartment.Name = "txtDepartment"
        Me.txtDepartment.NumericAllowNegatives = False
        Me.txtDepartment.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtDepartment.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDepartment.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtDepartment.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDepartment.Properties.Appearance.Options.UseFont = True
        Me.txtDepartment.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDepartment.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtDepartment.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDepartment.Properties.MaxLength = 30
        Me.txtDepartment.Size = New System.Drawing.Size(220, 22)
        Me.txtDepartment.TabIndex = 9
        Me.txtDepartment.Tag = "AE"
        Me.txtDepartment.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDepartment.ToolTipText = ""
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(342, 25)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(70, 15)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Department"
        '
        'txtJobTitle
        '
        Me.txtJobTitle.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtJobTitle.EnterMoveNextControl = True
        Me.txtJobTitle.Location = New System.Drawing.Point(430, 50)
        Me.txtJobTitle.MaxLength = 30
        Me.txtJobTitle.Name = "txtJobTitle"
        Me.txtJobTitle.NumericAllowNegatives = False
        Me.txtJobTitle.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtJobTitle.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtJobTitle.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtJobTitle.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobTitle.Properties.Appearance.Options.UseFont = True
        Me.txtJobTitle.Properties.Appearance.Options.UseTextOptions = True
        Me.txtJobTitle.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtJobTitle.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtJobTitle.Properties.MaxLength = 30
        Me.txtJobTitle.Size = New System.Drawing.Size(220, 22)
        Me.txtJobTitle.TabIndex = 11
        Me.txtJobTitle.Tag = "AE"
        Me.txtJobTitle.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtJobTitle.ToolTipText = ""
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(342, 53)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(51, 15)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Job Title"
        '
        'txtEmail
        '
        Me.txtEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtEmail.EnterMoveNextControl = True
        Me.txtEmail.Location = New System.Drawing.Point(430, 106)
        Me.txtEmail.MaxLength = 250
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.NumericAllowNegatives = False
        Me.txtEmail.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtEmail.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtEmail.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtEmail.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail.Properties.Appearance.Options.UseFont = True
        Me.txtEmail.Properties.Appearance.Options.UseTextOptions = True
        Me.txtEmail.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtEmail.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtEmail.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtEmail.Properties.MaxLength = 250
        Me.txtEmail.Size = New System.Drawing.Size(220, 22)
        Me.txtEmail.TabIndex = 15
        Me.txtEmail.Tag = "AE"
        Me.txtEmail.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtEmail.ToolTipText = ""
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(342, 109)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(36, 15)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Email"
        '
        'txtTel
        '
        Me.txtTel.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtTel.EnterMoveNextControl = True
        Me.txtTel.Location = New System.Drawing.Point(430, 78)
        Me.txtTel.MaxLength = 30
        Me.txtTel.Name = "txtTel"
        Me.txtTel.NumericAllowNegatives = False
        Me.txtTel.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtTel.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTel.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtTel.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTel.Properties.Appearance.Options.UseFont = True
        Me.txtTel.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTel.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtTel.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTel.Properties.MaxLength = 30
        Me.txtTel.Size = New System.Drawing.Size(220, 22)
        Me.txtTel.TabIndex = 13
        Me.txtTel.Tag = "AE"
        Me.txtTel.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTel.ToolTipText = ""
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(342, 81)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(62, 15)
        Me.Label8.TabIndex = 12
        Me.Label8.Text = "Telephone"
        '
        'btnReset
        '
        Me.btnReset.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.Appearance.Options.UseFont = True
        Me.btnReset.Location = New System.Drawing.Point(6, 0)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(110, 25)
        Me.btnReset.TabIndex = 0
        Me.btnReset.Text = "Reset Password"
        Me.btnReset.ToolTip = "Resetting an account will allow the user to login without the use of a password."
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 81)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 15)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Username"
        '
        'cbxType
        '
        Me.cbxType.AllowBlank = False
        Me.cbxType.DataSource = Nothing
        Me.cbxType.DisplayMember = Nothing
        Me.cbxType.EnterMoveNextControl = True
        Me.cbxType.Location = New System.Drawing.Point(113, 106)
        Me.cbxType.Name = "cbxType"
        Me.cbxType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxType.Properties.Appearance.Options.UseFont = True
        Me.cbxType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxType.SelectedValue = Nothing
        Me.cbxType.Size = New System.Drawing.Size(201, 22)
        Me.cbxType.TabIndex = 7
        Me.cbxType.Tag = "AE"
        Me.cbxType.ValueMember = Nothing
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 109)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(32, 15)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Type"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(6, 137)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(100, 15)
        Me.Label9.TabIndex = 16
        Me.Label9.Text = "Account Disabled"
        '
        'chkDisabled
        '
        Me.chkDisabled.EnterMoveNextControl = True
        Me.chkDisabled.Location = New System.Drawing.Point(112, 134)
        Me.chkDisabled.Name = "chkDisabled"
        Me.chkDisabled.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkDisabled.Properties.Appearance.Options.UseFont = True
        Me.chkDisabled.Properties.Caption = ""
        Me.chkDisabled.Size = New System.Drawing.Size(24, 19)
        Me.chkDisabled.TabIndex = 17
        Me.chkDisabled.Tag = "AE"
        Me.chkDisabled.ToolTip = "Disabling an account will prevent the user from logging in."
        '
        'chkSSO
        '
        Me.chkSSO.EnterMoveNextControl = True
        Me.chkSSO.Location = New System.Drawing.Point(152, 162)
        Me.chkSSO.Name = "chkSSO"
        Me.chkSSO.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkSSO.Properties.Appearance.Options.UseFont = True
        Me.chkSSO.Properties.Caption = ""
        Me.chkSSO.Size = New System.Drawing.Size(24, 19)
        Me.chkSSO.TabIndex = 19
        Me.chkSSO.Tag = "AE"
        Me.chkSSO.ToolTip = "Disabling an account will prevent the user from logging in."
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(6, 164)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(140, 15)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "Use Windows Credentials"
        '
        'txtSSOUsername
        '
        Me.txtSSOUsername.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtSSOUsername.EnterMoveNextControl = True
        Me.txtSSOUsername.Location = New System.Drawing.Point(296, 159)
        Me.txtSSOUsername.MaxLength = 100
        Me.txtSSOUsername.Name = "txtSSOUsername"
        Me.txtSSOUsername.NumericAllowNegatives = False
        Me.txtSSOUsername.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSSOUsername.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSSOUsername.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSSOUsername.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSSOUsername.Properties.Appearance.Options.UseFont = True
        Me.txtSSOUsername.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSSOUsername.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSSOUsername.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSSOUsername.Properties.MaxLength = 100
        Me.txtSSOUsername.Size = New System.Drawing.Size(354, 22)
        Me.txtSSOUsername.TabIndex = 21
        Me.txtSSOUsername.Tag = "AE"
        Me.txtSSOUsername.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSSOUsername.ToolTipText = ""
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(182, 164)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(109, 15)
        Me.Label11.TabIndex = 20
        Me.Label11.Text = "Domain\UserName"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label22)
        Me.GroupBox1.Controls.Add(Me.txtPasswordNumeric)
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Controls.Add(Me.txtPasswordSpecial)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.txtPasswordLower)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.txtPasswordLength)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.btnPasswordEdit)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.btnPasswordOK)
        Me.GroupBox1.Controls.Add(Me.btnPasswordCancel)
        Me.GroupBox1.Controls.Add(Me.txtPasswordUpper)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.txtPasswordHistory)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.txtPasswordExpiry)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(10, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(815, 165)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Password Requirements"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(17, 68)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(177, 15)
        Me.Label22.TabIndex = 40
        Me.Label22.Text = "Password Minimum Characters"
        '
        'txtPasswordNumeric
        '
        Me.txtPasswordNumeric.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtPasswordNumeric.EnterMoveNextControl = True
        Me.txtPasswordNumeric.Location = New System.Drawing.Point(555, 94)
        Me.txtPasswordNumeric.MaxLength = 2
        Me.txtPasswordNumeric.Name = "txtPasswordNumeric"
        Me.txtPasswordNumeric.NumericAllowNegatives = True
        Me.txtPasswordNumeric.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtPasswordNumeric.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPasswordNumeric.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPasswordNumeric.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPasswordNumeric.Properties.Appearance.Options.UseFont = True
        Me.txtPasswordNumeric.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPasswordNumeric.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPasswordNumeric.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtPasswordNumeric.Properties.Mask.EditMask = "##########"
        Me.txtPasswordNumeric.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtPasswordNumeric.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPasswordNumeric.Properties.MaxLength = 2
        Me.txtPasswordNumeric.Size = New System.Drawing.Size(55, 22)
        Me.txtPasswordNumeric.TabIndex = 37
        Me.txtPasswordNumeric.Tag = "EN"
        Me.txtPasswordNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPasswordNumeric.ToolTipText = ""
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(496, 97)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(53, 15)
        Me.Label20.TabIndex = 36
        Me.Label20.Text = "Numeric"
        '
        'txtPasswordSpecial
        '
        Me.txtPasswordSpecial.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtPasswordSpecial.EnterMoveNextControl = True
        Me.txtPasswordSpecial.Location = New System.Drawing.Point(707, 94)
        Me.txtPasswordSpecial.MaxLength = 2
        Me.txtPasswordSpecial.Name = "txtPasswordSpecial"
        Me.txtPasswordSpecial.NumericAllowNegatives = False
        Me.txtPasswordSpecial.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.None
        Me.txtPasswordSpecial.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPasswordSpecial.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPasswordSpecial.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPasswordSpecial.Properties.Appearance.Options.UseFont = True
        Me.txtPasswordSpecial.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPasswordSpecial.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPasswordSpecial.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtPasswordSpecial.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPasswordSpecial.Properties.MaxLength = 2
        Me.txtPasswordSpecial.Size = New System.Drawing.Size(55, 22)
        Me.txtPasswordSpecial.TabIndex = 35
        Me.txtPasswordSpecial.Tag = "EN"
        Me.txtPasswordSpecial.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPasswordSpecial.ToolTipText = ""
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(657, 97)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(44, 15)
        Me.Label21.TabIndex = 34
        Me.Label21.Text = "Special"
        '
        'txtPasswordLower
        '
        Me.txtPasswordLower.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtPasswordLower.EnterMoveNextControl = True
        Me.txtPasswordLower.Location = New System.Drawing.Point(399, 94)
        Me.txtPasswordLower.MaxLength = 10
        Me.txtPasswordLower.Name = "txtPasswordLower"
        Me.txtPasswordLower.NumericAllowNegatives = True
        Me.txtPasswordLower.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtPasswordLower.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPasswordLower.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPasswordLower.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPasswordLower.Properties.Appearance.Options.UseFont = True
        Me.txtPasswordLower.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPasswordLower.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPasswordLower.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtPasswordLower.Properties.Mask.EditMask = "##########"
        Me.txtPasswordLower.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtPasswordLower.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPasswordLower.Properties.MaxLength = 10
        Me.txtPasswordLower.Size = New System.Drawing.Size(55, 22)
        Me.txtPasswordLower.TabIndex = 33
        Me.txtPasswordLower.Tag = "EN"
        Me.txtPasswordLower.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPasswordLower.ToolTipText = ""
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(326, 97)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(67, 15)
        Me.Label18.TabIndex = 32
        Me.Label18.Text = "Lower Case"
        '
        'txtPasswordLength
        '
        Me.txtPasswordLength.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtPasswordLength.EnterMoveNextControl = True
        Me.txtPasswordLength.Location = New System.Drawing.Point(65, 94)
        Me.txtPasswordLength.MaxLength = 2
        Me.txtPasswordLength.Name = "txtPasswordLength"
        Me.txtPasswordLength.NumericAllowNegatives = True
        Me.txtPasswordLength.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtPasswordLength.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPasswordLength.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPasswordLength.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPasswordLength.Properties.Appearance.Options.UseFont = True
        Me.txtPasswordLength.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPasswordLength.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPasswordLength.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtPasswordLength.Properties.Mask.EditMask = "##########"
        Me.txtPasswordLength.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtPasswordLength.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPasswordLength.Properties.MaxLength = 2
        Me.txtPasswordLength.Size = New System.Drawing.Size(55, 22)
        Me.txtPasswordLength.TabIndex = 31
        Me.txtPasswordLength.Tag = "EN"
        Me.txtPasswordLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPasswordLength.ToolTipText = ""
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(15, 97)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(44, 15)
        Me.Label15.TabIndex = 30
        Me.Label15.Text = "Length"
        '
        'btnPasswordEdit
        '
        Me.btnPasswordEdit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPasswordEdit.Appearance.Options.UseFont = True
        Me.btnPasswordEdit.Location = New System.Drawing.Point(6, 134)
        Me.btnPasswordEdit.Name = "btnPasswordEdit"
        Me.btnPasswordEdit.Size = New System.Drawing.Size(85, 25)
        Me.btnPasswordEdit.TabIndex = 29
        Me.btnPasswordEdit.Text = "Edit"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(338, 32)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(31, 15)
        Me.Label17.TabIndex = 28
        Me.Label17.Text = "days"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(126, 32)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(31, 15)
        Me.Label16.TabIndex = 26
        Me.Label16.Text = "days"
        '
        'btnPasswordOK
        '
        Me.btnPasswordOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPasswordOK.Appearance.Options.UseFont = True
        Me.btnPasswordOK.Location = New System.Drawing.Point(633, 134)
        Me.btnPasswordOK.Name = "btnPasswordOK"
        Me.btnPasswordOK.Size = New System.Drawing.Size(85, 25)
        Me.btnPasswordOK.TabIndex = 25
        Me.btnPasswordOK.Text = "OK"
        '
        'btnPasswordCancel
        '
        Me.btnPasswordCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPasswordCancel.Appearance.Options.UseFont = True
        Me.btnPasswordCancel.Location = New System.Drawing.Point(724, 134)
        Me.btnPasswordCancel.Name = "btnPasswordCancel"
        Me.btnPasswordCancel.Size = New System.Drawing.Size(85, 25)
        Me.btnPasswordCancel.TabIndex = 24
        Me.btnPasswordCancel.Text = "Cancel"
        '
        'txtPasswordUpper
        '
        Me.txtPasswordUpper.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtPasswordUpper.EnterMoveNextControl = True
        Me.txtPasswordUpper.Location = New System.Drawing.Point(229, 94)
        Me.txtPasswordUpper.MaxLength = 2
        Me.txtPasswordUpper.Name = "txtPasswordUpper"
        Me.txtPasswordUpper.NumericAllowNegatives = True
        Me.txtPasswordUpper.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtPasswordUpper.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPasswordUpper.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPasswordUpper.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPasswordUpper.Properties.Appearance.Options.UseFont = True
        Me.txtPasswordUpper.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPasswordUpper.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPasswordUpper.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtPasswordUpper.Properties.Mask.EditMask = "##########"
        Me.txtPasswordUpper.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtPasswordUpper.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPasswordUpper.Properties.MaxLength = 2
        Me.txtPasswordUpper.Size = New System.Drawing.Size(55, 22)
        Me.txtPasswordUpper.TabIndex = 23
        Me.txtPasswordUpper.Tag = "EN"
        Me.txtPasswordUpper.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPasswordUpper.ToolTipText = ""
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(156, 97)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(67, 15)
        Me.Label12.TabIndex = 22
        Me.Label12.Text = "Upper Case"
        '
        'txtPasswordHistory
        '
        Me.txtPasswordHistory.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtPasswordHistory.EnterMoveNextControl = True
        Me.txtPasswordHistory.Location = New System.Drawing.Point(277, 29)
        Me.txtPasswordHistory.MaxLength = 3
        Me.txtPasswordHistory.Name = "txtPasswordHistory"
        Me.txtPasswordHistory.NumericAllowNegatives = True
        Me.txtPasswordHistory.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtPasswordHistory.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPasswordHistory.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPasswordHistory.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPasswordHistory.Properties.Appearance.Options.UseFont = True
        Me.txtPasswordHistory.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPasswordHistory.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPasswordHistory.Properties.Mask.EditMask = "##########"
        Me.txtPasswordHistory.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtPasswordHistory.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPasswordHistory.Properties.MaxLength = 3
        Me.txtPasswordHistory.Size = New System.Drawing.Size(55, 22)
        Me.txtPasswordHistory.TabIndex = 21
        Me.txtPasswordHistory.Tag = "EN"
        Me.txtPasswordHistory.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPasswordHistory.ToolTipText = ""
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(226, 32)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(45, 15)
        Me.Label13.TabIndex = 20
        Me.Label13.Text = "History"
        '
        'txtPasswordExpiry
        '
        Me.txtPasswordExpiry.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtPasswordExpiry.EnterMoveNextControl = True
        Me.txtPasswordExpiry.Location = New System.Drawing.Point(65, 29)
        Me.txtPasswordExpiry.MaxLength = 3
        Me.txtPasswordExpiry.Name = "txtPasswordExpiry"
        Me.txtPasswordExpiry.NumericAllowNegatives = True
        Me.txtPasswordExpiry.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtPasswordExpiry.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPasswordExpiry.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPasswordExpiry.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPasswordExpiry.Properties.Appearance.Options.UseFont = True
        Me.txtPasswordExpiry.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPasswordExpiry.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPasswordExpiry.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPasswordExpiry.Properties.MaxLength = 3
        Me.txtPasswordExpiry.Size = New System.Drawing.Size(55, 22)
        Me.txtPasswordExpiry.TabIndex = 17
        Me.txtPasswordExpiry.Tag = "EN"
        Me.txtPasswordExpiry.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPasswordExpiry.ToolTipText = ""
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(15, 32)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(38, 15)
        Me.Label14.TabIndex = 16
        Me.Label14.Text = "Expiry"
        '
        'frmUsers
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(834, 603)
        Me.Controls.Add(Me.GroupBox1)
        Me.MinimumSize = New System.Drawing.Size(440, 441)
        Me.Name = "frmUsers"
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.Grid, 0)
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.gbx, 0)
        Me.gbx.ResumeLayout(False)
        Me.gbx.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.txtUserName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtForename.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDepartment.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJobTitle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkDisabled.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSSO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSSOUsername.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.txtPasswordNumeric.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPasswordSpecial.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPasswordLower.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPasswordLength.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPasswordUpper.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPasswordHistory.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPasswordExpiry.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtEmail As Care.Controls.CareTextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtTel As Care.Controls.CareTextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtDepartment As Care.Controls.CareTextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtJobTitle As Care.Controls.CareTextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtSurname As Care.Controls.CareTextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtForename As Care.Controls.CareTextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtUserName As Care.Controls.CareTextBox
    Friend WithEvents btnReset As Care.Controls.CareButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbxType As Care.Controls.CareComboBox
    Friend WithEvents chkDisabled As Care.Controls.CareCheckBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtSSOUsername As Care.Controls.CareTextBox
    Friend WithEvents chkSSO As Care.Controls.CareCheckBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents btnPasswordCancel As Care.Controls.CareButton
    Friend WithEvents txtPasswordUpper As Controls.CareTextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents txtPasswordHistory As Controls.CareTextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents txtPasswordExpiry As Controls.CareTextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents btnPasswordOK As Controls.CareButton
    Friend WithEvents Label17 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents btnPasswordEdit As Controls.CareButton
    Friend WithEvents Label22 As Label
    Friend WithEvents txtPasswordNumeric As Controls.CareTextBox
    Friend WithEvents Label20 As Label
    Friend WithEvents txtPasswordSpecial As Controls.CareTextBox
    Friend WithEvents Label21 As Label
    Friend WithEvents txtPasswordLower As Controls.CareTextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents txtPasswordLength As Controls.CareTextBox
    Friend WithEvents Label15 As Label
End Class
