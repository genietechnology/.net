﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSecurity
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.SplitContainerControl2 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.txtGroupID = New Care.Controls.CareTextBox(Me.components)
        Me.cgGroups = New Care.Controls.CareGridWithButtons()
        Me.cgUsers = New Care.Controls.CareGridWithButtons()
        Me.cgForms = New Care.Controls.CareGrid()
        Me.gbxGroup = New Care.Controls.CareFrame()
        Me.btnGroupCancel = New Care.Controls.CareButton(Me.components)
        Me.btnGroupOK = New Care.Controls.CareButton(Me.components)
        Me.txtGroupName = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.btnSecurity = New Care.Controls.CareButton(Me.components)
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.gbxSecurity = New Care.Controls.CareFrame()
        Me.btnSecurityCancel = New Care.Controls.CareButton(Me.components)
        Me.btnSecurityOK = New Care.Controls.CareButton(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.chkDeny = New Care.Controls.CareCheckBox(Me.components)
        Me.chkReadOnly = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        CType(Me.SplitContainerControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl2.SuspendLayout()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.txtGroupID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxGroup, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxGroup.SuspendLayout()
        CType(Me.txtGroupName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxSecurity, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxSecurity.SuspendLayout()
        CType(Me.chkDeny.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkReadOnly.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'SplitContainerControl2
        '
        Me.SplitContainerControl2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainerControl2.Horizontal = False
        Me.SplitContainerControl2.Location = New System.Drawing.Point(12, 12)
        Me.SplitContainerControl2.Name = "SplitContainerControl2"
        Me.SplitContainerControl2.Panel1.Controls.Add(Me.SplitContainerControl1)
        Me.SplitContainerControl2.Panel1.Text = "Panel1"
        Me.SplitContainerControl2.Panel2.Controls.Add(Me.cgForms)
        Me.SplitContainerControl2.Panel2.Text = "Panel2"
        Me.SplitContainerControl2.Size = New System.Drawing.Size(719, 548)
        Me.SplitContainerControl2.SplitterPosition = 223
        Me.SplitContainerControl2.TabIndex = 1
        Me.SplitContainerControl2.Text = "SplitContainerControl2"
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.txtGroupID)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.cgGroups)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.cgUsers)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(719, 223)
        Me.SplitContainerControl1.SplitterPosition = 274
        Me.SplitContainerControl1.TabIndex = 1
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'txtGroupID
        '
        Me.txtGroupID.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtGroupID.Dock = System.Windows.Forms.DockStyle.Top
        Me.txtGroupID.EditValue = "group_id"
        Me.txtGroupID.EnterMoveNextControl = True
        Me.txtGroupID.Location = New System.Drawing.Point(0, 0)
        Me.txtGroupID.MaxLength = 0
        Me.txtGroupID.Name = "txtGroupID"
        Me.txtGroupID.NumericAllowNegatives = False
        Me.txtGroupID.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtGroupID.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtGroupID.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtGroupID.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGroupID.Properties.Appearance.Options.UseFont = True
        Me.txtGroupID.Properties.Appearance.Options.UseTextOptions = True
        Me.txtGroupID.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtGroupID.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtGroupID.ReadOnly = False
        Me.txtGroupID.Size = New System.Drawing.Size(274, 20)
        Me.txtGroupID.TabIndex = 1
        Me.txtGroupID.TabStop = False
        Me.txtGroupID.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtGroupID.ToolTipText = ""
        Me.txtGroupID.Visible = False
        '
        'cgGroups
        '
        Me.cgGroups.ButtonsEnabled = True
        Me.cgGroups.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cgGroups.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgGroups.HideFirstColumn = False
        Me.cgGroups.Location = New System.Drawing.Point(0, 0)
        Me.cgGroups.Name = "cgGroups"
        Me.cgGroups.Size = New System.Drawing.Size(274, 223)
        Me.cgGroups.TabIndex = 0
        '
        'cgUsers
        '
        Me.cgUsers.ButtonsEnabled = True
        Me.cgUsers.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cgUsers.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgUsers.HideFirstColumn = False
        Me.cgUsers.Location = New System.Drawing.Point(0, 0)
        Me.cgUsers.Name = "cgUsers"
        Me.cgUsers.Size = New System.Drawing.Size(440, 223)
        Me.cgUsers.TabIndex = 0
        '
        'cgForms
        '
        Me.cgForms.AllowBuildColumns = True
        Me.cgForms.AllowHorizontalScroll = False
        Me.cgForms.AllowMultiSelect = True
        Me.cgForms.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgForms.Appearance.Options.UseFont = True
        Me.cgForms.AutoSizeByData = True
        Me.cgForms.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cgForms.FocusedRowHandle = -2147483648
        Me.cgForms.HideFirstColumn = False
        Me.cgForms.Location = New System.Drawing.Point(0, 0)
        Me.cgForms.Name = "cgForms"
        Me.cgForms.QueryID = Nothing
        Me.cgForms.RowAutoHeight = False
        Me.cgForms.SearchAsYouType = True
        Me.cgForms.ShowAutoFilterRow = False
        Me.cgForms.ShowFindPanel = False
        Me.cgForms.ShowGroupByBox = True
        Me.cgForms.ShowNavigator = False
        Me.cgForms.Size = New System.Drawing.Size(719, 320)
        Me.cgForms.TabIndex = 1
        '
        'gbxGroup
        '
        Me.gbxGroup.Controls.Add(Me.btnGroupCancel)
        Me.gbxGroup.Controls.Add(Me.btnGroupOK)
        Me.gbxGroup.Controls.Add(Me.txtGroupName)
        Me.gbxGroup.Controls.Add(Me.CareLabel1)
        Me.gbxGroup.Location = New System.Drawing.Point(27, 37)
        Me.gbxGroup.Name = "gbxGroup"
        Me.gbxGroup.Size = New System.Drawing.Size(355, 89)
        Me.gbxGroup.TabIndex = 2
        '
        'btnGroupCancel
        '
        Me.btnGroupCancel.Location = New System.Drawing.Point(270, 59)
        Me.btnGroupCancel.Name = "btnGroupCancel"
        Me.btnGroupCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnGroupCancel.TabIndex = 3
        Me.btnGroupCancel.Text = "Cancel"
        '
        'btnGroupOK
        '
        Me.btnGroupOK.Location = New System.Drawing.Point(189, 59)
        Me.btnGroupOK.Name = "btnGroupOK"
        Me.btnGroupOK.Size = New System.Drawing.Size(75, 23)
        Me.btnGroupOK.TabIndex = 2
        Me.btnGroupOK.Text = "OK"
        '
        'txtGroupName
        '
        Me.txtGroupName.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtGroupName.EnterMoveNextControl = True
        Me.txtGroupName.Location = New System.Drawing.Point(95, 30)
        Me.txtGroupName.MaxLength = 30
        Me.txtGroupName.Name = "txtGroupName"
        Me.txtGroupName.NumericAllowNegatives = False
        Me.txtGroupName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtGroupName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtGroupName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtGroupName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtGroupName.Properties.Appearance.Options.UseFont = True
        Me.txtGroupName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtGroupName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtGroupName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtGroupName.Properties.MaxLength = 30
        Me.txtGroupName.ReadOnly = False
        Me.txtGroupName.Size = New System.Drawing.Size(250, 20)
        Me.txtGroupName.TabIndex = 1
        Me.txtGroupName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtGroupName.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(10, 33)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(68, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Group Name"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSecurity
        '
        Me.btnSecurity.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSecurity.Location = New System.Drawing.Point(12, 567)
        Me.btnSecurity.Name = "btnSecurity"
        Me.btnSecurity.Size = New System.Drawing.Size(93, 23)
        Me.btnSecurity.TabIndex = 4
        Me.btnSecurity.Text = "Change Security"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(656, 567)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 5
        Me.btnClose.Text = "Close"
        '
        'gbxSecurity
        '
        Me.gbxSecurity.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.gbxSecurity.Controls.Add(Me.CareLabel3)
        Me.gbxSecurity.Controls.Add(Me.chkReadOnly)
        Me.gbxSecurity.Controls.Add(Me.chkDeny)
        Me.gbxSecurity.Controls.Add(Me.btnSecurityCancel)
        Me.gbxSecurity.Controls.Add(Me.btnSecurityOK)
        Me.gbxSecurity.Controls.Add(Me.CareLabel2)
        Me.gbxSecurity.Location = New System.Drawing.Point(12, 429)
        Me.gbxSecurity.Name = "gbxSecurity"
        Me.gbxSecurity.Size = New System.Drawing.Size(274, 132)
        Me.gbxSecurity.TabIndex = 3
        '
        'btnSecurityCancel
        '
        Me.btnSecurityCancel.Location = New System.Drawing.Point(184, 97)
        Me.btnSecurityCancel.Name = "btnSecurityCancel"
        Me.btnSecurityCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnSecurityCancel.TabIndex = 3
        Me.btnSecurityCancel.Text = "Cancel"
        '
        'btnSecurityOK
        '
        Me.btnSecurityOK.Location = New System.Drawing.Point(103, 97)
        Me.btnSecurityOK.Name = "btnSecurityOK"
        Me.btnSecurityOK.Size = New System.Drawing.Size(75, 23)
        Me.btnSecurityOK.TabIndex = 2
        Me.btnSecurityOK.Text = "OK"
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(10, 33)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(186, 15)
        Me.CareLabel2.TabIndex = 0
        Me.CareLabel2.Text = "Deny Access to the selected item(s)"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkDeny
        '
        Me.chkDeny.EnterMoveNextControl = True
        Me.chkDeny.Location = New System.Drawing.Point(240, 31)
        Me.chkDeny.Name = "chkDeny"
        Me.chkDeny.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkDeny.Properties.Appearance.Options.UseFont = True
        Me.chkDeny.Properties.Caption = ""
        Me.chkDeny.Size = New System.Drawing.Size(19, 19)
        Me.chkDeny.TabIndex = 4
        '
        'chkReadOnly
        '
        Me.chkReadOnly.EnterMoveNextControl = True
        Me.chkReadOnly.Location = New System.Drawing.Point(240, 61)
        Me.chkReadOnly.Name = "chkReadOnly"
        Me.chkReadOnly.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkReadOnly.Properties.Appearance.Options.UseFont = True
        Me.chkReadOnly.Properties.Caption = ""
        Me.chkReadOnly.Size = New System.Drawing.Size(19, 19)
        Me.chkReadOnly.TabIndex = 6
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(10, 63)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(221, 15)
        Me.CareLabel3.TabIndex = 7
        Me.CareLabel3.Text = "Deny Update rights to the selected item(s)"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmSecurity
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(743, 599)
        Me.Controls.Add(Me.gbxSecurity)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSecurity)
        Me.Controls.Add(Me.gbxGroup)
        Me.Controls.Add(Me.SplitContainerControl2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.LoadMaximised = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSecurity"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.SplitContainerControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl2.ResumeLayout(False)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.txtGroupID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxGroup, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxGroup.ResumeLayout(False)
        Me.gbxGroup.PerformLayout()
        CType(Me.txtGroupName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxSecurity, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxSecurity.ResumeLayout(False)
        Me.gbxSecurity.PerformLayout()
        CType(Me.chkDeny.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkReadOnly.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl2 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents cgGroups As Care.Controls.CareGridWithButtons
    Friend WithEvents cgForms As Care.Controls.CareGrid
    Friend WithEvents cgUsers As Care.Controls.CareGridWithButtons
    Friend WithEvents gbxGroup As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnGroupCancel As Care.Controls.CareButton
    Friend WithEvents btnGroupOK As Care.Controls.CareButton
    Friend WithEvents txtGroupName As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents btnSecurity As Care.Controls.CareButton
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents txtGroupID As Care.Controls.CareTextBox
    Friend WithEvents gbxSecurity As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents chkReadOnly As Care.Controls.CareCheckBox
    Friend WithEvents chkDeny As Care.Controls.CareCheckBox
    Friend WithEvents btnSecurityCancel As Care.Controls.CareButton
    Friend WithEvents btnSecurityOK As Care.Controls.CareButton
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel

End Class
