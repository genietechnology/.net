﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPostCodeLookup
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CareAddress1 = New Care.Address.CareAddress()
        Me.btnCopy = New Care.Controls.CareButton()
        Me.btnClose = New Care.Controls.CareButton()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'CareAddress1
        '
        Me.CareAddress1.Address = ""
        Me.CareAddress1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareAddress1.Appearance.Options.UseFont = True
        Me.CareAddress1.DisplayMode = Care.Address.CareAddress.EnumDisplayMode.SplitControls
        Me.CareAddress1.Location = New System.Drawing.Point(12, 12)
        Me.CareAddress1.MaxLength = 500
        Me.CareAddress1.Name = "CareAddress1"
        Me.CareAddress1.ReadOnly = False
        Me.CareAddress1.Size = New System.Drawing.Size(262, 142)
        Me.CareAddress1.TabIndex = 0
        '
        'btnCopy
        '
        Me.btnCopy.Location = New System.Drawing.Point(118, 160)
        Me.btnCopy.Name = "btnCopy"
        Me.btnCopy.Size = New System.Drawing.Size(75, 23)
        Me.btnCopy.TabIndex = 1
        Me.btnCopy.Text = "Copy"
        Me.btnCopy.ToolTip = "Copy the Address to the Clipboard and Exit"
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(199, 160)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "Close"
        '
        'frmPostCodeLookup
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(286, 192)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnCopy)
        Me.Controls.Add(Me.CareAddress1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPostCodeLookup"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Post Code Lookup"
        Me.TopMost = True
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CareAddress1 As Care.Address.CareAddress
    Friend WithEvents btnCopy As Care.Controls.CareButton
    Friend WithEvents btnClose As Care.Controls.CareButton
End Class
