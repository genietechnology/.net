﻿Imports Care.Global
Imports Care.Data
Imports System.Text
Imports System.IO

Public Class frmImportExport

    Private Sub frmImportExport_Load(sender As Object, e As EventArgs) Handles Me.Load

        Dim _Path As String = Application.StartupPath + "\XMLFiles\"
        If Not IO.Directory.Exists(_Path) Then IO.Directory.CreateDirectory(_Path)

        ofd.InitialDirectory = _Path
        txtPath.Text = _Path

        Dim _SQL As String = "SELECT TABLE_NAME as 'Table Name' FROM information_schema.tables order by TABLE_NAME"
        cgTables.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub Export(ByVal ToCSV As Boolean)

        Dim _Count As Integer = cgTables.UnderlyingGridView.GetSelectedRows.Count
        If _Count < 1 Then Exit Sub

        SetCMDs(False)

        Session.SetupProgressBar("Starting Export", _Count)
        For Each _RowIndex As Integer In cgTables.UnderlyingGridView.GetSelectedRows
            Dim _DR As DataRow = cgTables.GetRow(_RowIndex)
            If _DR IsNot Nothing Then
                Dim _Table As String = _DR.Item(0).ToString
                ExportTable(_Table, ToCSV)
                Session.StepProgressBar("Exported " + _Table)
            End If
        Next

        Session.SetProgressMessage("Export Complete.", 5)

        CareMessage("Table Exported Successfully.", MessageBoxIcon.Information, "Export Data Table")
        SetCMDs(True)

    End Sub

    Private Function ExportTable(ByVal TableName As String, ByVal ToCSV As Boolean) As Boolean

        Dim _Path As String = txtPath.Text
        _Path += TableName

        If ToCSV Then
            _Path += ".csv"
        Else
            _Path += ".xml"
        End If

        Dim _SQL As String = "select * from " + TableName
        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)

        Try

            Dim _sb As New StringBuilder

            If ToCSV Then

                Dim _Columns As String = ""
                For Each _c As DataColumn In _DT.Columns
                    If _Columns = "" Then
                        _Columns += _c.ColumnName
                    Else
                        _Columns += "|" + _c.ColumnName
                    End If
                Next

                _sb.AppendLine(_Columns)

                'export the data
                For Each _DR As DataRow In _DT.Rows
                    Dim _Record As String = ""
                    For Each _item In _DR.ItemArray
                        If _Record = "" Then
                            _Record += CleanData(_item.ToString)
                        Else
                            _Record += "|" + CleanData(_item.ToString)
                        End If
                    Next
                    _sb.AppendLine(_Record)
                Next

                File.WriteAllText(_Path, _sb.ToString())

            Else
                _DT.TableName = TableName
                _DT.WriteXml(_Path, XmlWriteMode.WriteSchema)
            End If

        Catch ex As Exception
            Return False
        End Try

        Return True

    End Function

    Private Function CleanData(ByVal DataIn As String) As String

        Dim _Return As String = DataIn

        'replace | with nothing
        _Return = Replace(_Return, "|", "")

        'replace line feeds with <^>
        _Return = Replace(_Return, vbCrLf, "<CRLF>")
        _Return = Replace(_Return, vbCr, "<CR>")
        _Return = Replace(_Return, vbLf, "<LF>")

        Return _Return

    End Function

    Private Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        Export(False)
    End Sub

    Private Sub SetCMDs(ByVal Enabled As Boolean)

        If Enabled Then
            Me.Cursor = Cursors.Default
        Else
            Me.Cursor = Cursors.WaitCursor
        End If

        Application.DoEvents()

        btnOpenFolder.Enabled = Enabled
        btnImport.Enabled = Enabled
        btnExport.Enabled = Enabled
        btnExportCSV.Enabled = Enabled

    End Sub

    Private Sub btnOpenFolder_Click(sender As Object, e As EventArgs) Handles btnOpenFolder.Click
        Process.Start("explorer.exe", txtPath.Text)
    End Sub

    Private Sub btnImport_Click(sender As Object, e As EventArgs) Handles btnImport.Click

        Dim _table As String = ReturnSelectedTable()
        If _table = "" Then Exit Sub

        If ofd.ShowDialog = DialogResult.OK Then

            If CareMessage("Import " + ofd.SafeFileName + " into " + _table + "?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Import") = DialogResult.Yes Then

                SetCMDs(False)

                Dim _SQL As String = "select * from " + _table
                Dim _cmd As SqlClient.SqlCommand = DAL.ReturnTextCommand(Session.ConnectionString, _SQL)
                Dim _DA As New SqlClient.SqlDataAdapter(_cmd)

                Dim _DT As New DataTable
                _DA.Fill(_DT)

                Dim _ImportedData As New DataTable

                Try
                    _ImportedData.ReadXml(ofd.FileName)
                    _DT.Merge(_ImportedData)

                    Dim _bld As New SqlClient.SqlCommandBuilder(_DA)
                    _bld.ConflictOption = ConflictOption.OverwriteChanges

                    _DA = _bld.DataAdapter
                    _DA.Update(_DT)

                    SetCMDs(True)
                    CareMessage("Table Imported Successfully.", MessageBoxIcon.Information, "Import Data Table")

                Catch ex As Exception
                    SetCMDs(True)
                    CareMessage("Table Import Failed.", MessageBoxIcon.Warning, "Import Data Table")
                End Try


            End If

        End If

    End Sub

    Private Function ReturnSelectedTable() As String

        If cgTables.RecordCount <= 0 Then Return ""
        If cgTables.UnderlyingGridView.SelectedRowsCount <> 1 Then Return ""

        Try
            Return cgTables.CurrentRow("Table Name").ToString
        Catch ex As Exception
            Return ""
        End Try

    End Function

    Private Sub btnExportCSV_Click(sender As Object, e As EventArgs) Handles btnExportCSV.Click
        Export(True)
    End Sub
End Class