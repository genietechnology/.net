﻿Imports Care.Global
Imports System.Reflection
Imports System.IO

Public Class frmAbout

    Private m_DLLInformation As New List(Of DLLInfo)

    Private Sub frmAbout_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then Me.Close()
    End Sub

    Private Sub frmAbout_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        ShowFiles()
    End Sub

    Private Sub ShowFiles()

        Session.CursorWaiting()

        Try

            Dim _Path As String = Path.GetDirectoryName(Assembly.GetExecutingAssembly.Location)

            Dim _Files As String() = Directory.GetFiles(_Path, "*.dll")
            For Each _f As String In _Files
                Dim _info As FileVersionInfo = FileVersionInfo.GetVersionInfo(_f)
                m_DLLInformation.Add(New DLLInfo(_info.CompanyName, Path.GetFileName(_f), _info.ProductVersion))
                _info = Nothing
            Next

            _Files = Nothing
            _Path = Nothing

            grdFiles.Populate(m_DLLInformation)
            grdFiles.Columns("Company").GroupIndex = 1
            grdFiles.Columns("Company").Visible = False
            grdFiles.ExpandAll()

        Catch ex As Exception

        End Try

        Session.CursorDefault()

    End Sub

    Private Class DLLInfo

        Private _Name As String
        Private _Company As String
        Private _Version As String

        Public Sub New(Company As String, Name As String, Version As String)

            If Company <> "Care Software Ltd" Then
                Company = "Care Software Ltd Extensions"
            End If

            If Name.StartsWith("DevExpress.") Then
                Name = Name.Replace("DevExpress.", "")
            End If

            _Company = Company
            _Name = Name
            _Version = Version

        End Sub

        Public Property Name As String
            Get
                Return _Name
            End Get
            Set(value As String)
                _Name = value
            End Set
        End Property

        Public Property Company As String
            Get
                Return _Company
            End Get
            Set(value As String)
                _Company = value
            End Set
        End Property

        Public Property Version As String
            Get
                Return _Version
            End Get
            Set(value As String)
                _Version = value
            End Set
        End Property

    End Class

End Class