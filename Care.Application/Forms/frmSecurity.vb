﻿Imports Care.Global
Imports Care.Shared
Imports Care.Data

Public Class frmSecurity

    Dim m_Loading As Boolean = False
    Dim m_ToggleMode As EnumToggleMode

    Private Enum EnumToggleMode
        Add
        Edit
    End Enum

    Private Sub frmSecurity_Load(sender As Object, e As EventArgs) Handles Me.Load

        m_Loading = True

        txtGroupName.BackColor = Session.ChangeColour
        chkDeny.BackColor = Session.ChangeColour
        chkReadOnly.BackColor = Session.ChangeColour

        gbxGroup.Hide()
        gbxSecurity.Hide()

        DisplayGroups()

        m_Loading = False

    End Sub

    Private Sub cgGroups_AddClick(sender As Object, e As EventArgs) Handles cgGroups.AddClick
        ToggleGroup(EnumToggleMode.Add)
    End Sub

    Private Sub cgGroups_EditClick(sender As Object, e As EventArgs) Handles cgGroups.EditClick
        ToggleGroup(EnumToggleMode.Edit)
    End Sub

    Private Sub cgGroups_RemoveClick(sender As Object, e As EventArgs) Handles cgGroups.RemoveClick

        If cgGroups.RecordCount = 0 Then Exit Sub
        If Not cgGroups.CurrentRow Is Nothing Then

            If cgGroups.CurrentRow.Item(0).ToString <> "" Then

                Dim _GroupID As String = cgGroups.CurrentRow.Item(0).ToString

                If InputBox("Please type DELETE to continue.", "Delete Security Group").ToUpper = "DELETE" Then
                    If CareMessage("Are you sure you want to delete the group " + txtGroupName.Text + "?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Remove Group") = DialogResult.Yes Then

                        Dim _SQL As String = ""

                        _SQL = "delete from AppPerms where parent_id = '" + _GroupID + "'"
                        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                        _SQL = "delete from AppGroups where ID = '" + _GroupID + "'"
                        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                        _SQL = "update AppUsers set group_id = NULL where group_id = '" + _GroupID + "'"
                        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                        DisplayGroups()

                    End If
                End If
            End If
        End If
    End Sub

    Private Sub DisplayGroups()

        Dim _SQL As String = "select id, name as 'Group' from AppGroups order by name"

        cgGroups.HideFirstColumn = True
        cgGroups.Populate(Session.ConnectionString, _SQL)

        If cgGroups.RecordCount > 0 Then
            cgGroups.MoveFirst()
            If cgGroups.CurrentRow.Item(0).ToString <> "" Then
                txtGroupID.Text = cgGroups.CurrentRow.Item(0).ToString
                txtGroupName.Text = cgGroups.CurrentRow.Item(1).ToString
                DisplayUsers()
                DisplayForms()
            End If
        End If

    End Sub

    Private Sub DisplayUsers()

        Dim _SQL As String = ""

        _SQL += "select id, username as 'Username', fullname as 'Name' from AppUsers"
        _SQL += " where group_id = '" & txtGroupID.Text & "'"
        _SQL += " order by fullname"

        cgUsers.HideFirstColumn = True
        cgUsers.Populate(Session.ConnectionString, _SQL)

        If cgUsers.RecordCount > 0 Then
            cgUsers.MoveFirst()
        Else
            cgUsers.Clear()
        End If

    End Sub

    Private Sub DisplayForms()

        Dim _SQL As String = ""

        _SQL += "select"
        _SQL += " p.id as 'ID', m.caption as 'Module', men.caption as 'Menu', f.form_name as 'Form', f.caption as 'Caption',"
        _SQL += " isnull(p.noaccess,0) as 'Denied', isnull(p.readonly,0) as 'Read Only', f.id as 'item_id'"
        _SQL += " from AppForms f"
        _SQL += " left join AppModules m on m.ID = f.module_id"
        _SQL += " left join AppMenus men on men.ID = f.menu_id"
        _SQL += " left join AppPerms p on p.item_id = f.ID and p.parent_id = '" + txtGroupID.Text + "'"
        _SQL += " order by m.display_seq, men.display_seq, f.display_seq"

        cgForms.HideFirstColumn = True
        cgForms.Populate(Session.ConnectionString, _SQL)

        cgForms.Columns("item_id").Visible = False

    End Sub

    Private Sub ToggleGroup(ByVal Mode As EnumToggleMode)

        m_ToggleMode = Mode
        txtGroupName.ReadOnly = False

        If Mode = EnumToggleMode.Add Then
            gbxGroup.Text = "Create New Security Group"
            txtGroupName.Text = ""
            gbxGroup.Tag = ""
        Else
            gbxGroup.Text = "Edit Security Group"
            gbxGroup.Tag = txtGroupID.Text
            txtGroupName.Text = cgGroups.CurrentRow.Item(1).ToString
        End If

        gbxGroup.Show()
        txtGroupName.Focus()

    End Sub

    Private Sub cgGroups_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles cgGroups.FocusedRowChanged
        If m_Loading Then Exit Sub
        If cgGroups.RecordCount = 0 Then Exit Sub
        If Not cgGroups.CurrentRow Is Nothing Then
            If cgGroups.CurrentRow.Item(0).ToString <> "" Then
                txtGroupID.Text = cgGroups.CurrentRow.Item(0).ToString
                txtGroupName.Text = cgGroups.CurrentRow.Item(1).ToString
                DisplayUsers()
                DisplayForms()
            End If
        End If
    End Sub

    Private Sub btnGroupOK_Click(sender As Object, e As EventArgs) Handles btnGroupOK.Click
        SaveGroup()
        DisplayGroups()
        gbxGroup.Hide()
    End Sub

    Private Sub btnGroupCancel_Click(sender As Object, e As EventArgs) Handles btnGroupCancel.Click
        gbxGroup.Hide()
    End Sub

    Private Sub SaveGroup()

        Dim _G As Business.AppGroup = Nothing
        If m_ToggleMode = EnumToggleMode.Add Then
            _G = New Business.AppGroup
            _G._ID = Guid.NewGuid
        Else
            _G = Business.AppGroup.RetreiveByID(New Guid(txtGroupID.Text))
        End If

        With _G
            ._Name = txtGroupName.Text
            .Store()
        End With

    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub cgUsers_AddClick(sender As Object, e As EventArgs) Handles cgUsers.AddClick

        Dim _U As Business.User = Business.User.Find
        If _U IsNot Nothing Then
            If CareMessage("Are you sure you want to add " + _U._Fullname + " into " + txtGroupName.Text + "?", MessageBoxIcon.Question, MessageBoxButtons.YesNo, "Add User") = DialogResult.Yes Then
                _U._GroupId = New Guid(txtGroupID.Text)
                _U.Store()
                _U = Nothing
                DisplayUsers()
            End If
        End If

    End Sub

    Private Sub cgUsers_EditClick(sender As Object, e As EventArgs) Handles cgUsers.EditClick
        'stub
    End Sub

    Private Sub cgUsers_RemoveClick(sender As Object, e As EventArgs) Handles cgUsers.RemoveClick

        If cgUsers.RecordCount = 0 Then Exit Sub

        If Not cgUsers.CurrentRow Is Nothing Then

            If cgUsers.CurrentRow.Item(0).ToString <> "" Then

                Dim _UserID As String = cgUsers.CurrentRow.Item(0).ToString

                Dim _U As Business.User = Business.User.RetreiveByID(New Guid(_UserID))
                If _U IsNot Nothing Then
                    If CareMessage("Are you sure you want to remove " + _U._Fullname + " from " + txtGroupName.Text + "?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Remove User") = DialogResult.Yes Then
                        _U._GroupId = Nothing
                        _U.Store()
                        _U = Nothing
                        DisplayUsers()
                    End If
                End If

            End If

        End If

    End Sub

    Private Sub cgForms_GridDoubleClick(sender As Object, e As EventArgs) Handles cgForms.GridDoubleClick
        ToggleSecurity()
    End Sub

    Private Sub ToggleSecurity()

        chkDeny.Checked = False
        chkReadOnly.Checked = False

        Dim _SelectedRows As Integer() = cgForms.UnderlyingGridView.GetSelectedRows()
        If _SelectedRows.Length > 0 AndAlso _SelectedRows.Length >= 1 Then
            gbxSecurity.Text = _SelectedRows.Length.ToString + " item(s) selected"
            gbxSecurity.Show()
        End If

    End Sub

    Private Sub btnSecurityCancel_Click(sender As Object, e As EventArgs) Handles btnSecurityCancel.Click
        gbxSecurity.Hide()
    End Sub

    Private Sub btnSecurityOK_Click(sender As Object, e As EventArgs) Handles btnSecurityOK.Click

        For Each _RowIndex As Integer In cgForms.UnderlyingGridView.GetSelectedRows()

            Dim _DR As DataRow = cgForms.UnderlyingGridView.GetDataRow(_RowIndex)
            Dim _ID As String = _DR.Item("ID").ToString
            Dim _ItemID As String = _DR.Item("item_id").ToString

            Dim _P As Business.AppPerm = Nothing
            If _ID = "" Then
                _P = New Business.AppPerm
                _P._ParentId = New Guid(txtGroupID.Text)
                _P._ItemId = New Guid(_ItemID)
            Else
                _P = Business.AppPerm.RetreiveByID(New Guid(_ID))
            End If

            With _P
                ._Noaccess = chkDeny.Checked
                ._Readonly = chkReadOnly.Checked
                .Store()
            End With

        Next

        DisplayForms()
        gbxSecurity.Hide()

    End Sub

    Private Sub btnSecurity_Click(sender As Object, e As EventArgs) Handles btnSecurity.Click
        ToggleSecurity()
    End Sub

    Private Sub cgForms_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles cgForms.RowStyle

        Dim _Deny As Boolean = False
        Dim _ReadOnly As Boolean = False

        Dim _View As DevExpress.XtraGrid.Views.Grid.GridView = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView)
        If _View IsNot Nothing Then

            If e.RowHandle >= 0 Then

                _Deny = ValueHandler.ConvertBoolean(_View.GetRowCellValue(e.RowHandle, _View.Columns("Denied")))
                _ReadOnly = ValueHandler.ConvertBoolean(_View.GetRowCellValue(e.RowHandle, _View.Columns("Read Only")))

                If _Deny Then
                    e.Appearance.BackColor = Color.LightCoral
                Else
                    If _ReadOnly Then
                        e.Appearance.BackColor = Color.Khaki
                    Else
                        e.Appearance.BackColor = Color.LightGreen
                    End If
                End If

            End If

        End If

    End Sub

End Class
