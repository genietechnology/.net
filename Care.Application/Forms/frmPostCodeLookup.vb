﻿
Imports Care.Global

Public Class frmPostCodeLookup

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnCopy_Click(sender As System.Object, e As System.EventArgs) Handles btnCopy.Click
        If CareAddress1.Populated Then
            Clipboard.SetText(CareAddress1.Address)
            Me.Close()
        End If
    End Sub

    Private Sub frmPostCodeLookup_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then Me.Close()
    End Sub

End Class