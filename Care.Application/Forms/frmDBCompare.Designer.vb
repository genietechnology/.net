﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDBCompare
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling2 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.GroupBox8 = New Care.Controls.CareFrame(Me.components)
        Me.txtSource = New DevExpress.XtraEditors.MemoEdit()
        Me.GroupControl1 = New Care.Controls.CareFrame(Me.components)
        Me.txtTarget = New DevExpress.XtraEditors.MemoEdit()
        Me.grdDiffs = New Care.Controls.CareGrid()
        Me.btnCompare = New Care.Controls.CareButton(Me.components)
        Me.btnGenerate = New Care.Controls.CareButton(Me.components)
        CType(Me.GroupBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox8.SuspendLayout()
        CType(Me.txtSource.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtTarget.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.txtSource)
        Me.GroupBox8.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(342, 135)
        Me.GroupBox8.TabIndex = 3
        Me.GroupBox8.Text = "Source Database Connection"
        '
        'txtSource
        '
        Me.txtSource.Location = New System.Drawing.Point(12, 28)
        Me.txtSource.Name = "txtSource"
        Me.txtSource.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSource.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtSource, True)
        Me.txtSource.Size = New System.Drawing.Size(319, 98)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtSource, OptionsSpelling1)
        Me.txtSource.TabIndex = 0
        Me.txtSource.Tag = "AE"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.txtTarget)
        Me.GroupControl1.Location = New System.Drawing.Point(360, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(342, 135)
        Me.GroupControl1.TabIndex = 3
        Me.GroupControl1.Text = "Target Database Connection"
        '
        'txtTarget
        '
        Me.txtTarget.Location = New System.Drawing.Point(12, 28)
        Me.txtTarget.Name = "txtTarget"
        Me.txtTarget.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTarget.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtTarget, True)
        Me.txtTarget.Size = New System.Drawing.Size(319, 98)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtTarget, OptionsSpelling2)
        Me.txtTarget.TabIndex = 0
        Me.txtTarget.Tag = "AE"
        '
        'grdDiffs
        '
        Me.grdDiffs.AllowBuildColumns = True
        Me.grdDiffs.AllowEdit = False
        Me.grdDiffs.AllowHorizontalScroll = False
        Me.grdDiffs.AllowMultiSelect = True
        Me.grdDiffs.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdDiffs.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdDiffs.Appearance.Options.UseFont = True
        Me.grdDiffs.AutoSizeByData = True
        Me.grdDiffs.DisableAutoSize = False
        Me.grdDiffs.DisableDataFormatting = False
        Me.grdDiffs.FocusedRowHandle = -2147483648
        Me.grdDiffs.HideFirstColumn = False
        Me.grdDiffs.Location = New System.Drawing.Point(12, 153)
        Me.grdDiffs.Name = "grdDiffs"
        Me.grdDiffs.PreviewColumn = ""
        Me.grdDiffs.QueryID = Nothing
        Me.grdDiffs.RowAutoHeight = False
        Me.grdDiffs.SearchAsYouType = True
        Me.grdDiffs.ShowAutoFilterRow = False
        Me.grdDiffs.ShowFindPanel = False
        Me.grdDiffs.ShowGroupByBox = False
        Me.grdDiffs.ShowLoadingPanel = False
        Me.grdDiffs.ShowNavigator = True
        Me.grdDiffs.Size = New System.Drawing.Size(778, 266)
        Me.grdDiffs.TabIndex = 4
        '
        'btnCompare
        '
        Me.btnCompare.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCompare.Appearance.Options.UseFont = True
        Me.btnCompare.Location = New System.Drawing.Point(708, 12)
        Me.btnCompare.Name = "btnCompare"
        Me.btnCompare.Size = New System.Drawing.Size(85, 23)
        Me.btnCompare.TabIndex = 11
        Me.btnCompare.Text = "Compare"
        '
        'btnGenerate
        '
        Me.btnGenerate.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnGenerate.Appearance.Options.UseFont = True
        Me.btnGenerate.Location = New System.Drawing.Point(708, 42)
        Me.btnGenerate.Name = "btnGenerate"
        Me.btnGenerate.Size = New System.Drawing.Size(85, 23)
        Me.btnGenerate.TabIndex = 12
        Me.btnGenerate.Text = "Generate SQL"
        '
        'frmDBCompare
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(802, 431)
        Me.Controls.Add(Me.btnGenerate)
        Me.Controls.Add(Me.btnCompare)
        Me.Controls.Add(Me.grdDiffs)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.GroupBox8)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.LoadMaximised = True
        Me.Name = "frmDBCompare"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.GroupBox8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox8.ResumeLayout(False)
        CType(Me.txtSource.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.txtTarget.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtSource As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtTarget As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents grdDiffs As Care.Controls.CareGrid
    Friend WithEvents btnCompare As Care.Controls.CareButton
    Friend WithEvents btnGenerate As Care.Controls.CareButton
    Friend WithEvents GroupBox8 As Controls.CareFrame
    Friend WithEvents GroupControl1 As Controls.CareFrame
End Class
