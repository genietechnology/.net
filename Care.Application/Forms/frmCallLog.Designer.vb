﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCallLog
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cgCalls = New Care.Controls.CareGrid()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'cgCalls
        '
        Me.cgCalls.AllowBuildColumns = True
        Me.cgCalls.AllowEdit = False
        Me.cgCalls.AllowHorizontalScroll = False
        Me.cgCalls.AllowMultiSelect = False
        Me.cgCalls.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgCalls.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgCalls.Appearance.Options.UseFont = True
        Me.cgCalls.AutoSizeByData = True
        Me.cgCalls.DisableAutoSize = False
        Me.cgCalls.DisableDataFormatting = False
        Me.cgCalls.FocusedRowHandle = -2147483648
        Me.cgCalls.HideFirstColumn = False
        Me.cgCalls.Location = New System.Drawing.Point(12, 12)
        Me.cgCalls.Name = "cgCalls"
        Me.cgCalls.PreviewColumn = ""
        Me.cgCalls.QueryID = Nothing
        Me.cgCalls.RowAutoHeight = False
        Me.cgCalls.SearchAsYouType = True
        Me.cgCalls.ShowAutoFilterRow = False
        Me.cgCalls.ShowFindPanel = True
        Me.cgCalls.ShowGroupByBox = False
        Me.cgCalls.ShowLoadingPanel = False
        Me.cgCalls.ShowNavigator = True
        Me.cgCalls.Size = New System.Drawing.Size(610, 337)
        Me.cgCalls.TabIndex = 0
        '
        'frmCallLog
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(634, 361)
        Me.Controls.Add(Me.cgCalls)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Name = "frmCallLog"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmCallLog"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cgCalls As Care.Controls.CareGrid
End Class
