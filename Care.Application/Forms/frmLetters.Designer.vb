﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLetters
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling2 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling3 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling4 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.CareLabel13 = New Care.Controls.CareLabel(Me.components)
        Me.chkAdHoc = New Care.Controls.CareCheckBox(Me.components)
        Me.btnPathBrowse = New Care.Controls.CareButton(Me.components)
        Me.btnPathOpen = New Care.Controls.CareButton(Me.components)
        Me.CareLabel10 = New Care.Controls.CareLabel(Me.components)
        Me.txtCode = New Care.Controls.CareTextBox(Me.components)
        Me.chkMulti = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.btnBrowse = New Care.Controls.CareButton(Me.components)
        Me.btnOpen = New Care.Controls.CareButton(Me.components)
        Me.txtSourceFilename = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.txtMultiFilenameAppend = New Care.Controls.CareTextBox(Me.components)
        Me.cbxOutputExt = New Care.Controls.CareComboBox(Me.components)
        Me.txtOutputFilename = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.txtName = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.txtsql_select = New DevExpress.XtraEditors.MemoEdit()
        Me.popFormat = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnu2DP = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDateShort = New System.Windows.Forms.ToolStripMenuItem()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.txtsql_where = New DevExpress.XtraEditors.MemoEdit()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.txtsql_order = New DevExpress.XtraEditors.MemoEdit()
        Me.btnPreviewData = New Care.Controls.CareButton(Me.components)
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.btnINVUDocBrowse = New Care.Controls.CareButton(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.txtINVUFields = New DevExpress.XtraEditors.MemoEdit()
        Me.txtINVUDocPath = New Care.Controls.CareTextBox(Me.components)
        Me.chkINVUActive = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel12 = New Care.Controls.CareLabel(Me.components)
        Me.btnINVUCSVBrowse = New Care.Controls.CareButton(Me.components)
        Me.txtINVUCSVPath = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.btnCopy = New Care.Controls.CareButton(Me.components)
        Me.btnImport = New Care.Controls.CareButton(Me.components)
        Me.btnExport = New Care.Controls.CareButton(Me.components)
        Me.cbxType = New Care.Controls.CareComboBox(Me.components)
        Me.cbxQuery = New Care.Controls.CareComboBox(Me.components)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.chkAdHoc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkMulti.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSourceFilename.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMultiFilenameAppend.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxOutputExt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOutputFilename.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.txtsql_select.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.popFormat.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.txtsql_where.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        CType(Me.txtsql_order.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox7.SuspendLayout()
        CType(Me.txtINVUFields.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtINVUDocPath.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkINVUActive.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtINVUCSVPath.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxQuery.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnExport)
        Me.Panel1.Controls.Add(Me.btnImport)
        Me.Panel1.Controls.Add(Me.btnCopy)
        Me.Panel1.Controls.Add(Me.btnPreviewData)
        Me.Panel1.Location = New System.Drawing.Point(0, 660)
        Me.Panel1.Size = New System.Drawing.Size(814, 35)
        Me.Panel1.TabIndex = 5
        Me.Panel1.Controls.SetChildIndex(Me.btnOK, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnCancel, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnPreviewData, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnCopy, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnImport, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnExport, 0)
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(1338, 5)
        Me.btnOK.TabIndex = 4
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(1429, 5)
        Me.btnCancel.TabIndex = 5
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.Controls.Add(Me.cbxQuery)
        Me.GroupBox3.Controls.Add(Me.cbxType)
        Me.GroupBox3.Controls.Add(Me.CareLabel13)
        Me.GroupBox3.Controls.Add(Me.chkAdHoc)
        Me.GroupBox3.Controls.Add(Me.btnPathBrowse)
        Me.GroupBox3.Controls.Add(Me.btnPathOpen)
        Me.GroupBox3.Controls.Add(Me.CareLabel10)
        Me.GroupBox3.Controls.Add(Me.txtCode)
        Me.GroupBox3.Controls.Add(Me.chkMulti)
        Me.GroupBox3.Controls.Add(Me.CareLabel8)
        Me.GroupBox3.Controls.Add(Me.btnBrowse)
        Me.GroupBox3.Controls.Add(Me.btnOpen)
        Me.GroupBox3.Controls.Add(Me.txtSourceFilename)
        Me.GroupBox3.Controls.Add(Me.CareLabel9)
        Me.GroupBox3.Controls.Add(Me.txtMultiFilenameAppend)
        Me.GroupBox3.Controls.Add(Me.cbxOutputExt)
        Me.GroupBox3.Controls.Add(Me.txtOutputFilename)
        Me.GroupBox3.Controls.Add(Me.CareLabel7)
        Me.GroupBox3.Controls.Add(Me.CareLabel6)
        Me.GroupBox3.Controls.Add(Me.CareLabel5)
        Me.GroupBox3.Controls.Add(Me.txtName)
        Me.GroupBox3.Controls.Add(Me.CareLabel4)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 53)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(790, 197)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        '
        'CareLabel13
        '
        Me.CareLabel13.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CareLabel13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel13.Location = New System.Drawing.Point(566, 25)
        Me.CareLabel13.Name = "CareLabel13"
        Me.CareLabel13.Size = New System.Drawing.Size(182, 15)
        Me.CareLabel13.TabIndex = 24
        Me.CareLabel13.Text = "Show in Ad-Hoc Letter Production"
        Me.CareLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkAdHoc
        '
        Me.chkAdHoc.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkAdHoc.EnterMoveNextControl = True
        Me.chkAdHoc.Location = New System.Drawing.Point(754, 23)
        Me.chkAdHoc.Name = "chkAdHoc"
        Me.chkAdHoc.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAdHoc.Properties.Appearance.Options.UseFont = True
        Me.chkAdHoc.Properties.AutoWidth = True
        Me.chkAdHoc.Properties.Caption = ""
        Me.chkAdHoc.Size = New System.Drawing.Size(19, 19)
        Me.chkAdHoc.TabIndex = 11
        Me.chkAdHoc.Tag = "AE"
        '
        'btnPathBrowse
        '
        Me.btnPathBrowse.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPathBrowse.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPathBrowse.Appearance.Options.UseFont = True
        Me.btnPathBrowse.CausesValidation = False
        Me.btnPathBrowse.Location = New System.Drawing.Point(672, 105)
        Me.btnPathBrowse.Name = "btnPathBrowse"
        Me.btnPathBrowse.Size = New System.Drawing.Size(50, 23)
        Me.btnPathBrowse.TabIndex = 6
        Me.btnPathBrowse.Text = "Browse"
        '
        'btnPathOpen
        '
        Me.btnPathOpen.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPathOpen.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPathOpen.Appearance.Options.UseFont = True
        Me.btnPathOpen.Location = New System.Drawing.Point(728, 105)
        Me.btnPathOpen.Name = "btnPathOpen"
        Me.btnPathOpen.Size = New System.Drawing.Size(50, 23)
        Me.btnPathOpen.TabIndex = 7
        Me.btnPathOpen.Text = "Open"
        '
        'CareLabel10
        '
        Me.CareLabel10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel10.Location = New System.Drawing.Point(13, 25)
        Me.CareLabel10.Name = "CareLabel10"
        Me.CareLabel10.Size = New System.Drawing.Size(61, 15)
        Me.CareLabel10.TabIndex = 15
        Me.CareLabel10.Text = "Letter Code"
        Me.CareLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCode
        '
        Me.txtCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCode.EnterMoveNextControl = True
        Me.txtCode.Location = New System.Drawing.Point(119, 22)
        Me.txtCode.MaxLength = 10
        Me.txtCode.Name = "txtCode"
        Me.txtCode.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCode.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.Properties.Appearance.Options.UseFont = True
        Me.txtCode.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtCode.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCode.Properties.MaxLength = 10
        Me.txtCode.ReadOnly = False
        Me.txtCode.Size = New System.Drawing.Size(104, 22)
        Me.txtCode.TabIndex = 0
        Me.txtCode.Tag = "AEM"
        Me.txtCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCode.ToolTipText = ""
        '
        'chkMulti
        '
        Me.chkMulti.EnterMoveNextControl = True
        Me.chkMulti.Location = New System.Drawing.Point(117, 162)
        Me.chkMulti.Name = "chkMulti"
        Me.chkMulti.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMulti.Properties.Appearance.Options.UseFont = True
        Me.chkMulti.Properties.AutoWidth = True
        Me.chkMulti.Properties.Caption = ""
        Me.chkMulti.Size = New System.Drawing.Size(19, 19)
        Me.chkMulti.TabIndex = 9
        Me.chkMulti.Tag = "AE"
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(13, 165)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(70, 15)
        Me.CareLabel8.TabIndex = 10
        Me.CareLabel8.Text = "Multiple Files"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnBrowse
        '
        Me.btnBrowse.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBrowse.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBrowse.Appearance.Options.UseFont = True
        Me.btnBrowse.CausesValidation = False
        Me.btnBrowse.Location = New System.Drawing.Point(672, 77)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(50, 23)
        Me.btnBrowse.TabIndex = 3
        Me.btnBrowse.Text = "Browse"
        '
        'btnOpen
        '
        Me.btnOpen.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOpen.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOpen.Appearance.Options.UseFont = True
        Me.btnOpen.Location = New System.Drawing.Point(728, 77)
        Me.btnOpen.Name = "btnOpen"
        Me.btnOpen.Size = New System.Drawing.Size(50, 23)
        Me.btnOpen.TabIndex = 4
        Me.btnOpen.Text = "Open"
        '
        'txtSourceFilename
        '
        Me.txtSourceFilename.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSourceFilename.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtSourceFilename.EnterMoveNextControl = True
        Me.txtSourceFilename.Location = New System.Drawing.Point(119, 78)
        Me.txtSourceFilename.MaxLength = 1000
        Me.txtSourceFilename.Name = "txtSourceFilename"
        Me.txtSourceFilename.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSourceFilename.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSourceFilename.Properties.Appearance.Options.UseFont = True
        Me.txtSourceFilename.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSourceFilename.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSourceFilename.Properties.MaxLength = 1000
        Me.txtSourceFilename.ReadOnly = False
        Me.txtSourceFilename.Size = New System.Drawing.Size(547, 22)
        Me.txtSourceFilename.TabIndex = 2
        Me.txtSourceFilename.Tag = "AEM"
        Me.txtSourceFilename.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSourceFilename.ToolTipText = ""
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(156, 164)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(100, 15)
        Me.CareLabel9.TabIndex = 11
        Me.CareLabel9.Text = "Multiple File Name"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMultiFilenameAppend
        '
        Me.txtMultiFilenameAppend.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMultiFilenameAppend.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtMultiFilenameAppend.EnterMoveNextControl = True
        Me.txtMultiFilenameAppend.Location = New System.Drawing.Point(262, 162)
        Me.txtMultiFilenameAppend.MaxLength = 500
        Me.txtMultiFilenameAppend.Name = "txtMultiFilenameAppend"
        Me.txtMultiFilenameAppend.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtMultiFilenameAppend.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMultiFilenameAppend.Properties.Appearance.Options.UseFont = True
        Me.txtMultiFilenameAppend.Properties.Appearance.Options.UseTextOptions = True
        Me.txtMultiFilenameAppend.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtMultiFilenameAppend.Properties.MaxLength = 500
        Me.txtMultiFilenameAppend.ReadOnly = False
        Me.txtMultiFilenameAppend.Size = New System.Drawing.Size(516, 22)
        Me.txtMultiFilenameAppend.TabIndex = 10
        Me.txtMultiFilenameAppend.Tag = "AE"
        Me.txtMultiFilenameAppend.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtMultiFilenameAppend.ToolTip = "Appended to the end of the Output Filename."
        Me.txtMultiFilenameAppend.ToolTipText = "Appended to the end of the Output Filename."
        '
        'cbxOutputExt
        '
        Me.cbxOutputExt.AllowBlank = False
        Me.cbxOutputExt.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxOutputExt.DataSource = Nothing
        Me.cbxOutputExt.DisplayMember = Nothing
        Me.cbxOutputExt.EnterMoveNextControl = True
        Me.cbxOutputExt.Location = New System.Drawing.Point(119, 134)
        Me.cbxOutputExt.Name = "cbxOutputExt"
        Me.cbxOutputExt.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxOutputExt.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxOutputExt.Properties.Appearance.Options.UseFont = True
        Me.cbxOutputExt.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxOutputExt.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxOutputExt.ReadOnly = False
        Me.cbxOutputExt.SelectedValue = Nothing
        Me.cbxOutputExt.Size = New System.Drawing.Size(659, 22)
        Me.cbxOutputExt.TabIndex = 8
        Me.cbxOutputExt.Tag = "AEM"
        Me.cbxOutputExt.ValueMember = Nothing
        '
        'txtOutputFilename
        '
        Me.txtOutputFilename.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtOutputFilename.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtOutputFilename.EnterMoveNextControl = True
        Me.txtOutputFilename.Location = New System.Drawing.Point(119, 106)
        Me.txtOutputFilename.MaxLength = 1000
        Me.txtOutputFilename.Name = "txtOutputFilename"
        Me.txtOutputFilename.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtOutputFilename.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOutputFilename.Properties.Appearance.Options.UseFont = True
        Me.txtOutputFilename.Properties.Appearance.Options.UseTextOptions = True
        Me.txtOutputFilename.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtOutputFilename.Properties.MaxLength = 1000
        Me.txtOutputFilename.ReadOnly = False
        Me.txtOutputFilename.Size = New System.Drawing.Size(547, 22)
        Me.txtOutputFilename.TabIndex = 5
        Me.txtOutputFilename.Tag = "AEM"
        Me.txtOutputFilename.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtOutputFilename.ToolTip = "The name of the Output file excluding the file extension."
        Me.txtOutputFilename.ToolTipText = "The name of the Output file excluding the file extension."
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(13, 137)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(67, 15)
        Me.CareLabel7.TabIndex = 4
        Me.CareLabel7.Text = "Output Type"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(13, 109)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(65, 15)
        Me.CareLabel6.TabIndex = 3
        Me.CareLabel6.Text = "Output Path"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(13, 81)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(57, 15)
        Me.CareLabel5.TabIndex = 2
        Me.CareLabel5.Text = "Source File"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(119, 50)
        Me.txtName.MaxLength = 40
        Me.txtName.Name = "txtName"
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.MaxLength = 40
        Me.txtName.ReadOnly = False
        Me.txtName.Size = New System.Drawing.Size(659, 22)
        Me.txtName.TabIndex = 1
        Me.txtName.Tag = "AEM"
        Me.txtName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(13, 53)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel4.TabIndex = 0
        Me.CareLabel4.Text = "Name"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox4
        '
        Me.GroupBox4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox4.Controls.Add(Me.txtsql_select)
        Me.GroupBox4.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(12, 403)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(790, 95)
        Me.GroupBox4.TabIndex = 2
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "SQL Select"
        '
        'txtsql_select
        '
        Me.txtsql_select.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtsql_select.Location = New System.Drawing.Point(13, 22)
        Me.txtsql_select.Name = "txtsql_select"
        Me.txtsql_select.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsql_select.Properties.Appearance.Options.UseFont = True
        Me.txtsql_select.Properties.ContextMenuStrip = Me.popFormat
        Me.txtsql_select.Properties.MaxLength = 8000
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtsql_select, True)
        Me.txtsql_select.Size = New System.Drawing.Size(765, 63)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtsql_select, OptionsSpelling1)
        Me.txtsql_select.TabIndex = 0
        Me.txtsql_select.Tag = "AEM"
        Me.txtsql_select.UseOptimizedRendering = True
        '
        'popFormat
        '
        Me.popFormat.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnu2DP, Me.mnuDateShort})
        Me.popFormat.Name = "popFormat"
        Me.popFormat.Size = New System.Drawing.Size(186, 48)
        '
        'mnu2DP
        '
        Me.mnu2DP.Name = "mnu2DP"
        Me.mnu2DP.Size = New System.Drawing.Size(185, 22)
        Me.mnu2DP.Text = "Format 2DP (0.00)"
        '
        'mnuDateShort
        '
        Me.mnuDateShort.Name = "mnuDateShort"
        Me.mnuDateShort.Size = New System.Drawing.Size(185, 22)
        Me.mnuDateShort.Text = "Format dd/mm/yyyy"
        '
        'GroupBox5
        '
        Me.GroupBox5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox5.Controls.Add(Me.txtsql_where)
        Me.GroupBox5.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.Location = New System.Drawing.Point(12, 504)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(790, 78)
        Me.GroupBox5.TabIndex = 3
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "SQL Where"
        '
        'txtsql_where
        '
        Me.txtsql_where.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtsql_where.Location = New System.Drawing.Point(13, 22)
        Me.txtsql_where.Name = "txtsql_where"
        Me.txtsql_where.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsql_where.Properties.Appearance.Options.UseFont = True
        Me.txtsql_where.Properties.ContextMenuStrip = Me.popFormat
        Me.txtsql_where.Properties.MaxLength = 8000
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtsql_where, True)
        Me.txtsql_where.Size = New System.Drawing.Size(764, 46)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtsql_where, OptionsSpelling2)
        Me.txtsql_where.TabIndex = 0
        Me.txtsql_where.Tag = "AE"
        Me.txtsql_where.UseOptimizedRendering = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox6.Controls.Add(Me.txtsql_order)
        Me.GroupBox6.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox6.Location = New System.Drawing.Point(12, 588)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(790, 66)
        Me.GroupBox6.TabIndex = 4
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "SQL Order By"
        '
        'txtsql_order
        '
        Me.txtsql_order.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtsql_order.Location = New System.Drawing.Point(13, 22)
        Me.txtsql_order.Name = "txtsql_order"
        Me.txtsql_order.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsql_order.Properties.Appearance.Options.UseFont = True
        Me.txtsql_order.Properties.ContextMenuStrip = Me.popFormat
        Me.txtsql_order.Properties.MaxLength = 500
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtsql_order, True)
        Me.txtsql_order.Size = New System.Drawing.Size(764, 34)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtsql_order, OptionsSpelling3)
        Me.txtsql_order.TabIndex = 0
        Me.txtsql_order.Tag = "AE"
        Me.txtsql_order.UseOptimizedRendering = True
        '
        'btnPreviewData
        '
        Me.btnPreviewData.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreviewData.Appearance.Options.UseFont = True
        Me.btnPreviewData.Location = New System.Drawing.Point(12, 5)
        Me.btnPreviewData.Name = "btnPreviewData"
        Me.btnPreviewData.Size = New System.Drawing.Size(140, 25)
        Me.btnPreviewData.TabIndex = 0
        Me.btnPreviewData.Text = "Preview Mailmerge Data"
        '
        'GroupBox7
        '
        Me.GroupBox7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox7.Controls.Add(Me.btnINVUDocBrowse)
        Me.GroupBox7.Controls.Add(Me.CareLabel14)
        Me.GroupBox7.Controls.Add(Me.txtINVUFields)
        Me.GroupBox7.Controls.Add(Me.txtINVUDocPath)
        Me.GroupBox7.Controls.Add(Me.chkINVUActive)
        Me.GroupBox7.Controls.Add(Me.CareLabel12)
        Me.GroupBox7.Controls.Add(Me.btnINVUCSVBrowse)
        Me.GroupBox7.Controls.Add(Me.txtINVUCSVPath)
        Me.GroupBox7.Controls.Add(Me.CareLabel11)
        Me.GroupBox7.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox7.Location = New System.Drawing.Point(12, 256)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(790, 141)
        Me.GroupBox7.TabIndex = 1
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "INVU Interface"
        '
        'btnINVUDocBrowse
        '
        Me.btnINVUDocBrowse.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnINVUDocBrowse.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnINVUDocBrowse.Appearance.Options.UseFont = True
        Me.btnINVUDocBrowse.CausesValidation = False
        Me.btnINVUDocBrowse.Location = New System.Drawing.Point(730, 44)
        Me.btnINVUDocBrowse.Name = "btnINVUDocBrowse"
        Me.btnINVUDocBrowse.Size = New System.Drawing.Size(50, 23)
        Me.btnINVUDocBrowse.TabIndex = 4
        Me.btnINVUDocBrowse.Text = "Browse"
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(173, 48)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(83, 15)
        Me.CareLabel14.TabIndex = 25
        Me.CareLabel14.Text = "Document Path"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtINVUFields
        '
        Me.txtINVUFields.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtINVUFields.Location = New System.Drawing.Point(14, 74)
        Me.txtINVUFields.Name = "txtINVUFields"
        Me.txtINVUFields.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtINVUFields.Properties.Appearance.Options.UseFont = True
        Me.txtINVUFields.Properties.ContextMenuStrip = Me.popFormat
        Me.txtINVUFields.Properties.MaxLength = 500
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtINVUFields, True)
        Me.txtINVUFields.Size = New System.Drawing.Size(764, 57)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtINVUFields, OptionsSpelling4)
        Me.txtINVUFields.TabIndex = 5
        Me.txtINVUFields.Tag = "AE"
        Me.txtINVUFields.UseOptimizedRendering = True
        '
        'txtINVUDocPath
        '
        Me.txtINVUDocPath.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtINVUDocPath.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtINVUDocPath.EnterMoveNextControl = True
        Me.txtINVUDocPath.Location = New System.Drawing.Point(262, 45)
        Me.txtINVUDocPath.MaxLength = 1000
        Me.txtINVUDocPath.Name = "txtINVUDocPath"
        Me.txtINVUDocPath.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtINVUDocPath.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtINVUDocPath.Properties.Appearance.Options.UseFont = True
        Me.txtINVUDocPath.Properties.Appearance.Options.UseTextOptions = True
        Me.txtINVUDocPath.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtINVUDocPath.Properties.MaxLength = 1000
        Me.txtINVUDocPath.ReadOnly = False
        Me.txtINVUDocPath.Size = New System.Drawing.Size(460, 22)
        Me.txtINVUDocPath.TabIndex = 3
        Me.txtINVUDocPath.Tag = "AE"
        Me.txtINVUDocPath.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtINVUDocPath.ToolTipText = ""
        '
        'chkINVUActive
        '
        Me.chkINVUActive.EnterMoveNextControl = True
        Me.chkINVUActive.Location = New System.Drawing.Point(117, 17)
        Me.chkINVUActive.Name = "chkINVUActive"
        Me.chkINVUActive.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkINVUActive.Properties.Appearance.Options.UseFont = True
        Me.chkINVUActive.Properties.AutoWidth = True
        Me.chkINVUActive.Properties.Caption = ""
        Me.chkINVUActive.Size = New System.Drawing.Size(19, 19)
        Me.chkINVUActive.TabIndex = 0
        Me.chkINVUActive.Tag = "AE"
        '
        'CareLabel12
        '
        Me.CareLabel12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel12.Location = New System.Drawing.Point(13, 20)
        Me.CareLabel12.Name = "CareLabel12"
        Me.CareLabel12.Size = New System.Drawing.Size(33, 15)
        Me.CareLabel12.TabIndex = 23
        Me.CareLabel12.Text = "Active"
        Me.CareLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnINVUCSVBrowse
        '
        Me.btnINVUCSVBrowse.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnINVUCSVBrowse.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnINVUCSVBrowse.Appearance.Options.UseFont = True
        Me.btnINVUCSVBrowse.CausesValidation = False
        Me.btnINVUCSVBrowse.Location = New System.Drawing.Point(730, 16)
        Me.btnINVUCSVBrowse.Name = "btnINVUCSVBrowse"
        Me.btnINVUCSVBrowse.Size = New System.Drawing.Size(50, 23)
        Me.btnINVUCSVBrowse.TabIndex = 2
        Me.btnINVUCSVBrowse.Text = "Browse"
        '
        'txtINVUCSVPath
        '
        Me.txtINVUCSVPath.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtINVUCSVPath.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtINVUCSVPath.EnterMoveNextControl = True
        Me.txtINVUCSVPath.Location = New System.Drawing.Point(262, 17)
        Me.txtINVUCSVPath.MaxLength = 1000
        Me.txtINVUCSVPath.Name = "txtINVUCSVPath"
        Me.txtINVUCSVPath.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtINVUCSVPath.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtINVUCSVPath.Properties.Appearance.Options.UseFont = True
        Me.txtINVUCSVPath.Properties.Appearance.Options.UseTextOptions = True
        Me.txtINVUCSVPath.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtINVUCSVPath.Properties.MaxLength = 1000
        Me.txtINVUCSVPath.ReadOnly = False
        Me.txtINVUCSVPath.Size = New System.Drawing.Size(460, 22)
        Me.txtINVUCSVPath.TabIndex = 1
        Me.txtINVUCSVPath.Tag = "AE"
        Me.txtINVUCSVPath.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtINVUCSVPath.ToolTipText = ""
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(173, 22)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(48, 15)
        Me.CareLabel11.TabIndex = 18
        Me.CareLabel11.Text = "CSV Path"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnCopy
        '
        Me.btnCopy.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCopy.Appearance.Options.UseFont = True
        Me.btnCopy.Location = New System.Drawing.Point(158, 5)
        Me.btnCopy.Name = "btnCopy"
        Me.btnCopy.Size = New System.Drawing.Size(140, 25)
        Me.btnCopy.TabIndex = 1
        Me.btnCopy.Text = "Copy to Clipboard"
        '
        'btnImport
        '
        Me.btnImport.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImport.Appearance.Options.UseFont = True
        Me.btnImport.Location = New System.Drawing.Point(304, 5)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(140, 25)
        Me.btnImport.TabIndex = 2
        Me.btnImport.Text = "Import Letter"
        '
        'btnExport
        '
        Me.btnExport.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.Appearance.Options.UseFont = True
        Me.btnExport.Location = New System.Drawing.Point(450, 5)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.Size = New System.Drawing.Size(140, 25)
        Me.btnExport.TabIndex = 3
        Me.btnExport.Text = "Export Letter"
        '
        'cbxType
        '
        Me.cbxType.AllowBlank = False
        Me.cbxType.DataSource = Nothing
        Me.cbxType.DisplayMember = Nothing
        Me.cbxType.EnterMoveNextControl = True
        Me.cbxType.Location = New System.Drawing.Point(229, 22)
        Me.cbxType.Name = "cbxType"
        Me.cbxType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxType.Properties.Appearance.Options.UseFont = True
        Me.cbxType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxType.ReadOnly = False
        Me.cbxType.SelectedValue = Nothing
        Me.cbxType.Size = New System.Drawing.Size(168, 22)
        Me.cbxType.TabIndex = 25
        Me.cbxType.Tag = "AEM"
        Me.cbxType.ValueMember = Nothing
        '
        'cbxQuery
        '
        Me.cbxQuery.AllowBlank = False
        Me.cbxQuery.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxQuery.DataSource = Nothing
        Me.cbxQuery.DisplayMember = Nothing
        Me.cbxQuery.EnterMoveNextControl = True
        Me.cbxQuery.Location = New System.Drawing.Point(403, 22)
        Me.cbxQuery.Name = "cbxQuery"
        Me.cbxQuery.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxQuery.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxQuery.Properties.Appearance.Options.UseFont = True
        Me.cbxQuery.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxQuery.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxQuery.ReadOnly = False
        Me.cbxQuery.SelectedValue = Nothing
        Me.cbxQuery.Size = New System.Drawing.Size(375, 22)
        Me.cbxQuery.TabIndex = 26
        Me.cbxQuery.Tag = "AEM"
        Me.cbxQuery.ValueMember = Nothing
        '
        'frmLetters
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.ClientSize = New System.Drawing.Size(814, 695)
        Me.Controls.Add(Me.GroupBox7)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.MaximizeBox = True
        Me.MinimumSize = New System.Drawing.Size(830, 726)
        Me.Name = "frmLetters"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.GroupBox3, 0)
        Me.Controls.SetChildIndex(Me.GroupBox4, 0)
        Me.Controls.SetChildIndex(Me.GroupBox5, 0)
        Me.Controls.SetChildIndex(Me.GroupBox6, 0)
        Me.Controls.SetChildIndex(Me.GroupBox7, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.chkAdHoc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkMulti.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSourceFilename.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMultiFilenameAppend.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxOutputExt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOutputFilename.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.txtsql_select.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.popFormat.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        CType(Me.txtsql_where.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        CType(Me.txtsql_order.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        CType(Me.txtINVUFields.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtINVUDocPath.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkINVUActive.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtINVUCSVPath.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxQuery.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents cbxOutputExt As Care.Controls.CareComboBox
    Friend WithEvents txtOutputFilename As Care.Controls.CareTextBox
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents txtsql_select As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents chkMulti As Care.Controls.CareCheckBox
    Friend WithEvents txtMultiFilenameAppend As Care.Controls.CareTextBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents txtsql_where As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents txtsql_order As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents txtSourceFilename As Care.Controls.CareTextBox
    Friend WithEvents btnBrowse As Care.Controls.CareButton
    Friend WithEvents btnOpen As Care.Controls.CareButton
    Friend WithEvents CareLabel10 As Care.Controls.CareLabel
    Friend WithEvents txtCode As Care.Controls.CareTextBox
    Friend WithEvents btnPathBrowse As Care.Controls.CareButton
    Friend WithEvents btnPathOpen As Care.Controls.CareButton
    Friend WithEvents btnPreviewData As Care.Controls.CareButton
    Friend WithEvents popFormat As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnu2DP As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDateShort As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents chkINVUActive As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel12 As Care.Controls.CareLabel
    Friend WithEvents btnINVUCSVBrowse As Care.Controls.CareButton
    Friend WithEvents txtINVUCSVPath As Care.Controls.CareTextBox
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents txtINVUFields As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents btnCopy As Care.Controls.CareButton
    Friend WithEvents CareLabel13 As Care.Controls.CareLabel
    Friend WithEvents chkAdHoc As Care.Controls.CareCheckBox
    Friend WithEvents txtINVUDocPath As Care.Controls.CareTextBox
    Friend WithEvents btnINVUDocBrowse As Care.Controls.CareButton
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents btnExport As Care.Controls.CareButton
    Friend WithEvents btnImport As Care.Controls.CareButton
    Friend WithEvents cbxType As Care.Controls.CareComboBox
    Friend WithEvents cbxQuery As Care.Controls.CareComboBox

End Class
