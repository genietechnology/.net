﻿Imports Care.Global
Imports Care.Data

Public Class frmDBCompare

    Private m_Errors As New List(Of ErrorRecord)
    Private m_Source As String
    Private m_Target As String

    Private Sub frmDBCompare_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtSource.Text = Session.ConnectionString
        txtTarget.Text = Session.ConnectionString
    End Sub

    Private Sub btnCompare_Click(sender As Object, e As EventArgs) Handles btnCompare.Click

        m_Source = txtSource.Text
        m_Target = txtTarget.Text

        subCompare()

    End Sub

    Private Sub subCompare()
        Dim SQL As String
        Dim lngTargetID As Long

        m_Errors.Clear()

        SQL = "select id, name from sysobjects where xtype = 'U' order by name"

        Dim _Tables As DataTable = DAL.GetDataTablefromSQL(m_Source, SQL)
        If _Tables IsNot Nothing Then

            Session.SetupProgressBar("Comparing tables...", _Tables.Rows.Count)
            For Each _DR As DataRow In _Tables.Rows

                Session.SetProgressMessage("Comparing Table: " & _DR.Item("name").ToString)

                lngTargetID = fncTargetExists(_DR.Item("name").ToString)

                If lngTargetID > 0 Then
                    Call subCheckFields(CLng(_DR.Item("id")), lngTargetID, _DR.Item("name").ToString)
                Else
                    Call TableError("MISSING TABLE", _DR.Item("name").ToString)
                End If

                Session.StepProgressBar()

            Next

        End If

        grdDiffs.Populate(m_Errors)

        CareMessage("Comparison Completed", MessageBoxIcon.Information, "DB Compare")

    End Sub

    Private Function fncTargetExists(TableName As String) As Long
        Dim SQL As String

        SQL = "select id from sysobjects where name = '" & TableName & "'"

        Dim _DR As DataRow = DAL.GetRowfromSQL(m_Target, SQL)
        If _DR Is Nothing Then
            Return 0
        Else
            Return CLng(_DR.Item("id"))
        End If

    End Function

    Private Sub subCheckFields(SourceTableID As Long, TargetTableID As Long, TableName As String)
        Dim SQL As String

        SQL = "select syscolumns.colid as 'fieldpos', syscolumns.name as 'fieldname', systypes.name as 'fieldtype'," & _
              " syscolumns.length as 'fieldlength'from syscolumns" & _
              " left join systypes on syscolumns.xtype = systypes.xtype" & _
              " where id = " & SourceTableID & _
              " order by colid"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(m_Source, SQL)
        If _DT IsNot Nothing Then
            For Each _DR As DataRow In _DT.Rows
                If Not fncFieldExists(TargetTableID, CLng(_DR.Item("FieldPos")), _DR.Item("FieldName").ToString, _DR.Item("FieldType").ToString, CLng(_DR.Item("FieldLength")), TableName) Then
                    Call FieldError("MISSING FIELD", TableName, CLng(_DR.Item("FieldPos")), _DR.Item("FieldName").ToString, _DR.Item("FieldType").ToString, CLng(_DR.Item("FieldLength")), 0, "", 0)
                End If
            Next
        End If
    End Sub

    Private Function fncFieldExists(TargetTableID As Long, FieldPosistion As Long, FieldName As String, FieldType As String, FieldLength As Long, TableName As String) As Boolean
        Dim SQL As String

        SQL = "select syscolumns.colid as 'fieldpos', syscolumns.name as 'fieldname', systypes.name as 'fieldtype'," & _
              " syscolumns.length as 'fieldlength' from syscolumns" & _
              " left join systypes on syscolumns.xtype = systypes.xtype" & _
              " where syscolumns.id = " & TargetTableID & _
              " and syscolumns.name = '" & FieldName & "'" & _
              " order by syscolumns.colid"

        Dim _DR As DataRow = DAL.GetRowfromSQL(m_Target, SQL)
        If _DR IsNot Nothing Then

            fncFieldExists = True

            If CLng(_DR.Item("fieldpos")) <> FieldPosistion Then
                Call FieldError("FIELD POSITIONS DO NOT MATCH", TableName, FieldPosistion, FieldName, FieldType, FieldLength, CLng(_DR.Item("fieldpos")), _DR.Item("fieldtype").ToString, CLng(_DR.Item("fieldlength")))
            End If

            If _DR.Item("fieldtype").ToString <> FieldType Then
                Call FieldError("FIELD TYPES DO NOT MATCH", TableName, FieldPosistion, FieldName, FieldType, FieldLength, CLng(_DR.Item("fieldpos")), _DR.Item("fieldtype").ToString, CLng(_DR.Item("fieldlength")))
            End If

            If CLng(_DR.Item("fieldlength")) <> FieldLength Then
                Call FieldError("FIELD LENGTHS DO NOT MATCH", TableName, FieldPosistion, FieldName, FieldType, FieldLength, CLng(_DR.Item("fieldpos")), _DR.Item("fieldtype").ToString, CLng(_DR.Item("fieldlength")))
            End If

            Return True

        Else
            Return False
        End If

    End Function

    Private Function fncGetTableID(TableName As String) As Long

        Dim SQL As String
        SQL = "select id from sysobjects where name = '" & TableName & "'"

        Dim _DR As DataRow = DAL.GetRowfromSQL(m_Source, SQL)
        If _DR Is Nothing Then
            Return 0
        Else
            Return CLng(_DR.Item("id"))
        End If

    End Function

    Private Function fncNullable(FieldName As String, TableID As Long) As String
        Dim SQL As String

        SQL = "select isnullable from syscolumns" & _
                " where name = '" & FieldName & "'" & _
                " and id = " & TableID

        Dim _DR As DataRow = DAL.GetRowfromSQL(m_Source, SQL)
        If _DR IsNot Nothing Then
            If CLng(_DR.Item("isnullable")) = 0 Then
                fncNullable = "NOT NULL"
            Else
                fncNullable = "NULL"
            End If
        Else
            fncNullable = "NULL"
        End If

    End Function

    Private Function fncGetFieldName(ColID As Long, TableID As Long) As String

        Dim SQL As String

        fncGetFieldName = ""

        SQL = "select name from syscolumns" & _
                " where colid = " & ColID & _
                " and id = " & TableID

        Dim _DR As DataRow = DAL.GetRowfromSQL(m_Source, SQL)
        If _DR IsNot Nothing Then
            Return _DR.Item("name").ToString
        End If

    End Function

    Private Function fncGetFieldType(FieldName As String, TableID As Long) As String

        Dim SQL As String

        fncGetFieldType = ""

        SQL = "select syscolumns.name as 'fieldname', systypes.name as 'fieldtype' from syscolumns" & _
            " left join systypes on syscolumns.xtype = systypes.xtype" & _
            " where syscolumns.id = " & TableID & _
            " and syscolumns.name = '" & FieldName & "'"

        Dim _DR As DataRow = DAL.GetRowfromSQL(m_Source, SQL)
        If _DR IsNot Nothing Then
            Return _DR.Item("fieldtype").ToString
        End If

    End Function

    Private Function fncGetFieldLength(FieldName As String, TableID As Long) As Long

        Dim SQL As String

        fncGetFieldLength = 0

        SQL = "select length from syscolumns" & _
                " where name = '" & FieldName & "'" & _
                " and id = " & TableID

        Dim _DR As DataRow = DAL.GetRowfromSQL(m_Source, SQL)
        If _DR IsNot Nothing Then
            Return CLng(_DR.Item("length"))
        End If

    End Function

    Private Sub TableError(ByVal ErrorType As String, ByVal ErrorTable As String)

        Dim _E As New ErrorRecord
        With _E
            .Error_Type = ErrorType
            .Error_Table = ErrorTable
        End With

        m_Errors.Add(_E)

    End Sub

    Private Sub FieldError(ByVal ErrorType As String, ByVal ErrorTable As String, _
                          FieldPosition As Long, ErrorField As String, SourceType As String, SourceLength As Long, _
                          TargetPosition As Long, TargetType As String, TargetLength As Long)

        Dim _E As New ErrorRecord
        With _E
            .Error_Type = ErrorType
            .Error_Table = ErrorTable
            .Error_Field = ErrorField
            .Source_Position = FieldPosition
            .Source_Type = SourceType
            .Source_Length = SourceLength
            .Target_Position = TargetPosition
            .Target_Type = TargetType
            .Target_Length = TargetLength
        End With

        m_Errors.Add(_E)

    End Sub

    Private Class ErrorRecord

        Public Property Error_Type As String
        Public Property Error_Table As String
        Public Property Error_Field As String
        Public Property Source_Position As Long
        Public Property Source_Type As String
        Public Property Source_Length As Long
        Public Property Target_Position As Long
        Public Property Target_Type As String
        Public Property Target_Length As Long

    End Class

    Private Sub btnGenerate_Click(sender As Object, e As EventArgs) Handles btnGenerate.Click

        Dim _Script As String = ""
        Dim _SelectedRows As Integer() = grdDiffs.UnderlyingGridView.GetSelectedRows()
        If _SelectedRows.Length > 0 AndAlso _SelectedRows.Length > 1 Then

            For Each _RowIndex As Integer In _SelectedRows

                Dim _Line As String = ""
                Dim _DR As ErrorRecord = CType(grdDiffs.UnderlyingGridView.GetRow(_RowIndex), ErrorRecord)

                Select Case _DR.Error_Type

                    Case "MISSING FIELD"
                        _Line = "ALTER TABLE " + _DR.Error_Table + " ADD " + _DR.Error_Field + " " + _DR.Source_Type + ReturnLength(_DR.Source_Type, _DR.Source_Length)

                    Case "FIELD LENGTHS DO NOT MATCH"
                        _Line = "ALTER TABLE " + _DR.Error_Table + " ALTER COLUMN " + _DR.Error_Field + " " + _DR.Source_Type + ReturnLength(_DR.Source_Type, _DR.Source_Length)

                    Case Else
                        'stub

                End Select

                If _Line <> "" Then _Script += _Line + vbCrLf

            Next

        End If

        If _Script <> "" Then
            Clipboard.SetText(_Script)
            CareMessage("Script copied to Clipboard.", MessageBoxIcon.Information)
        Else
            CareMessage("Nothing Generated!", MessageBoxIcon.Exclamation, "Generate SQL")
        End If

    End Sub

    Private Function ReturnLength(ByVal FieldType As String, ByVal LengthIn As Long) As String

        Select Case FieldType

            Case "varchar", "char"
                If LengthIn = -1 Then
                    Return "(max)"
                Else
                    Return "(" + LengthIn.ToString + ")"
                End If

            Case Else
                Return ""

        End Select

    End Function

End Class