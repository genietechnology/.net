﻿Imports Care.Global
Imports Care.Shared

Public Class frmErrors

    Private Sub frmErrors_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim _SQL As String = ""
        _SQL += "SELECT stamp, hostname, user_name, windows_user, source, source_version, exception, stack, unhandled from AppErrors order by stamp desc"
        grd.Populate(Session.ConnectionString, _SQL)

        gbxStack.Hide()
        btnEmail.Enabled = Session.SMTPEnabled

    End Sub

    Private Function ReturnValue(ByRef GridIn As Care.Controls.CareGrid, ColumnName As String) As String
        If GridIn Is Nothing Then Return ""
        If GridIn.RecordCount < 1 Then Return ""
        Return GridIn.CurrentRow(ColumnName).ToString
    End Function

    Private Sub btnView_Click(sender As Object, e As EventArgs) Handles btnView.Click
        ViewStackTrace
    End Sub

    Private Sub ViewStackTrace()
        Dim _st As String = ReturnValue(grd, "stack")
        txtStack.Text = _st
        gbxStack.Show()
    End Sub

    Private Sub grd_GridDoubleClick(sender As Object, e As EventArgs) Handles grd.GridDoubleClick
        ViewStackTrace()
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        gbxStack.Hide()
    End Sub

    Private Sub btnCopy_Click(sender As Object, e As EventArgs) Handles btnCopy.Click
        Try
            Clipboard.Clear()
            Clipboard.SetText(ReturnErrorDetails)

        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnEmail_Click(sender As Object, e As EventArgs) Handles btnEmail.Click

        Dim _Result As Integer = 0
        Dim _Errors As String = ""
        Dim _subject As String = $"Exception Details: {ReturnValue(grd, "exception")} on {ReturnValue(grd, "hostname")}"

        Try
            _Result = EmailHandler.SendEmailReturnStatus(New Net.Mail.MailAddress(Session.SMTPSenderEmail), "support@genietechnology.co.uk", _subject, ReturnErrorDetails, _Errors)

        Catch ex As Exception

        End Try

    End Sub

    Private Function ReturnErrorDetails() As String

        Dim _return As String = ""

        _return += "Stamp: " + ReturnValue(grd, "stamp") + vbCrLf
        _return += "Hostname: " + ReturnValue(grd, "hostname") + vbCrLf
        _return += "Windows User: " + ReturnValue(grd, "windows_user") + vbCrLf
        _return += "Application User: " + ReturnValue(grd, "user_name") + vbCrLf
        _return += "Source Version: " + ReturnValue(grd, "source_version") + vbCrLf

        _return += vbCrLf
        _return += "Exception: " + ReturnValue(grd, "exception") + vbCrLf
        _return += vbCrLf
        _return += "Stack Trace: " + ReturnValue(grd, "stack") + vbCrLf

        Return _return

    End Function


End Class