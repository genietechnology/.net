﻿Option Strict On

Imports Care.Global
Imports Care.Shared
Imports Care.Data

Public Class frmLetters

    Dim m_Letter As Care.Shared.Business.AppLetter

    Protected Overrides Sub FindRecord()

        Dim _ReturnValue As String = ""
        Dim _Find As New GenericFind
        With _Find
            .ConnectionString = Session.ConnectionString
            .Caption = "Find Letter"
            .ParentForm = Me
            .PopulateOnLoad = True
            .GridSQL = "select code as 'Letter Code', name as 'Name' from AppLetters"
            .GridOrderBy = "order by name"
            .ReturnField = "ID"
            .Option1Caption = "Name"
            .Option1Field = "name"
            .Show()
            _ReturnValue = .ReturnValue
        End With

        If _ReturnValue <> "" Then
            MyBase.RecordPopulated = True

            m_Letter = Business.AppLetter.RetreiveByID(New Guid(_ReturnValue))

            If m_Letter._Type = "X" Then
                cbxQuery.SelectedValue = m_Letter._QueryId
            Else
                cbxQuery.SelectedIndex = -1
            End If

            bs.DataSource = m_Letter
        End If

    End Sub

    Protected Overrides Sub AfterAdd()
        MyBase.AfterAdd()
        cbxType.SelectedIndex = 0
    End Sub

    Protected Overrides Sub CommitUpdate()

        m_Letter = CType(bs.Item(bs.Position), Business.AppLetter)

        If m_Letter._Type = "X" Then
            m_Letter._QueryId = New Guid(cbxQuery.SelectedValue.ToString)
            m_Letter._Adhoc = False
            m_Letter._SqlSelect = Nothing
            m_Letter._SqlWhere = Nothing
            m_Letter._SqlOrder = Nothing
        Else
            m_Letter._QueryId = Nothing
        End If

        m_Letter.Store()

        'If m_Site.IsNew Then
        '    MyBase.RecordPopulated = True
        '    m_Site = Business.Site.RetreiveByID(m_Site._ID)
        '    bs.DataSource = m_Site
        'End If

    End Sub

    Protected Overrides Sub SetBindings()

        m_Letter = New Business.AppLetter
        bs.DataSource = m_Letter

        txtCode.DataBindings.Add("Text", bs, "_Code")
        cbxType.DataBindings.Add("SelectedValue", bs, "_Type")

        txtName.DataBindings.Add("Text", bs, "_Name")
        chkAdHoc.DataBindings.Add("Checked", bs, "_Adhoc")
        txtSourceFilename.DataBindings.Add("Text", bs, "_SourceFile")
        txtOutputFilename.DataBindings.Add("Text", bs, "_OutputFile")
        cbxOutputExt.DataBindings.Add("SelectedValue", bs, "_OutputType")

        chkMulti.DataBindings.Add("Checked", bs, "_Multi")
        txtMultiFilenameAppend.DataBindings.Add("Text", bs, "_MultiFilenameAppend")

        chkINVUActive.DataBindings.Add("Checked", bs, "_Invu")
        txtINVUCSVPath.DataBindings.Add("Text", bs, "_InvuCSVPath")
        txtINVUDocPath.DataBindings.Add("Text", bs, "_InvuDocPath")
        txtINVUFields.DataBindings.Add("Text", bs, "_InvuFields")

        txtsql_select.DataBindings.Add("Text", bs, "_SqlSelect")
        txtsql_where.DataBindings.Add("Text", bs, "_SqlWhere")
        txtsql_order.DataBindings.Add("Text", bs, "_SqlOrder")

    End Sub

    Private Sub btnBrowse_Click(sender As System.Object, e As System.EventArgs) Handles btnBrowse.Click
        txtSourceFilename.Text = FileHandler.BrowseFile
    End Sub

    Private Sub btnPathBrowse_Click(sender As System.Object, e As System.EventArgs) Handles btnPathBrowse.Click
        txtOutputFilename.Text = FileHandler.BrowseFolder
    End Sub

    Private Sub btnOpen_Click(sender As System.Object, e As System.EventArgs) Handles btnOpen.Click
        WordProcessing.OpenFile(m_Letter._Code, "", "", WordProcessing.EnumBehaviour.HoldMerge, 1)
    End Sub

    Private Sub btnPreviewData_Click(sender As System.Object, e As System.EventArgs) Handles btnPreviewData.Click

        Dim _SQL As String = txtsql_select.Text + " where 1=1 " + txtsql_where.Text + " " + txtsql_order.Text
        If _SQL = "" Then Exit Sub

        Dim _frm As New frmDataPreview(_SQL)
        _frm.Show()

    End Sub

    Private Sub popFormat_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles popFormat.Opening
        If Me.Mode = "" Then e.Cancel = True
    End Sub

    Private Sub mnu2DP_Click(sender As Object, e As System.EventArgs) Handles mnu2DP.Click
        If txtsql_select.SelectionLength > 0 Then Format2DP(txtsql_select.SelectedText)
    End Sub

    Private Sub Format2DP(ByRef SelectedText As String)

        'check there is only one word
        If Split(SelectedText, " ").Length = 1 Then

            'create_total
            'cast(round(create_total, 2, 1) as varchar(10)) as 'create_total'

            Dim _Field As String = SelectedText
            SelectedText = "cast(round(" + _Field + ", 2, 1) as varchar(10)) as '" + _Field + "'"

        End If

    End Sub

    Private Sub FormatDDMMYYYY(ByRef SelectedText As String)

        'check there is only one word
        If Split(SelectedText, " ").Length = 1 Then

            'create_date
            'SELECT CONVERT(VARCHAR(10), GETDATE(), 103)
            Dim _Field As String = SelectedText
            SelectedText = "convert(varchar(10), " + _Field + ", 103) as '" + _Field + "'"

        End If

    End Sub

    Private Sub mnuDateShort_Click(sender As Object, e As System.EventArgs) Handles mnuDateShort.Click
        If txtsql_select.SelectionLength > 0 Then FormatDDMMYYYY(txtsql_select.SelectedText)
    End Sub

    Private Sub frmLetters_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Dim _SQL As String = "select id, title from AppQueries order by title"
        cbxQuery.Visible = False
        cbxQuery.PopulateWithSQL(Session.ConnectionString, _SQL)

        cbxType.AddItem("Application Letter", "A")
        cbxType.AddItem("Data Explorer Letter", "X")

        cbxOutputExt.AddItem("Word Document 97-2003 (.doc)")
        cbxOutputExt.AddItem("Word Document (.docx)")

    End Sub

    Private Sub btnCopy_Click(sender As System.Object, e As System.EventArgs) Handles btnCopy.Click
        Dim _SQL As String = txtsql_select.Text + " where 1=1 " + txtsql_where.Text + " " + txtsql_order.Text
        Clipboard.Clear()
        Clipboard.SetText(_SQL)
    End Sub

    Private Sub btnINVUCSVBrowse_Click(sender As System.Object, e As System.EventArgs) Handles btnINVUCSVBrowse.Click
        txtINVUCSVPath.Text = FileHandler.BrowseFile
    End Sub

    Private Sub btnINVUDocBrowse_Click(sender As System.Object, e As System.EventArgs) Handles btnINVUDocBrowse.Click
        txtINVUDocPath.Text = FileHandler.BrowseFile
    End Sub

    Private Sub btnExport_Click(sender As System.Object, e As System.EventArgs) Handles btnExport.Click
        Dim _Path As String = My.Computer.FileSystem.SpecialDirectories.Desktop + "\" + "LetterDefinition.xml"
        m_Letter.Serialise(_Path)
    End Sub

    Private Sub btnImport_Click(sender As System.Object, e As System.EventArgs) Handles btnImport.Click

        Dim _Cloned As New Business.AppLetter
        Dim _Path As String = My.Computer.FileSystem.SpecialDirectories.Desktop + "\" + "LetterDefinition.xml"

        m_Letter.DeSerialise(_Path, _Cloned)

        _Cloned._ID = Guid.NewGuid
        _Cloned._Name += " (Copy)"

        _Cloned.Store()

    End Sub

    Private Sub cbxType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxType.SelectedIndexChanged
        If cbxType.SelectedIndex = 0 Then
            cbxQuery.Visible = False
            chkAdHoc.Visible = True
        Else
            cbxQuery.Visible = True
            chkAdHoc.Visible = False
        End If
    End Sub
End Class
