﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMenuItems

    Inherits Care.Shared.frmGridMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtFormName = New Care.Controls.CareTextBox()
        Me.txtClassName = New Care.Controls.CareTextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtCaption = New Care.Controls.CareTextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtDisplaySeq = New Care.Controls.CareTextBox()
        Me.txtTooltip = New Care.Controls.CareTextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ImageList1 = New System.Windows.Forms.ImageList()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cbxModule = New Care.Controls.CareComboBox()
        Me.cbxMenu = New Care.Controls.CareComboBox()
        Me.cbxIcon = New DevExpress.XtraEditors.ImageComboBoxEdit()
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.cgModules = New Care.Controls.CareGridWithButtons()
        Me.txtModuleID = New Care.Controls.CareTextBox()
        Me.cgMenus = New Care.Controls.CareGridWithButtons()
        Me.txtMenuID = New Care.Controls.CareTextBox()
        Me.gbxMM = New Care.Controls.CareFrame()
        Me.btnMMCancel = New Care.Controls.CareButton()
        Me.btnMMOK = New Care.Controls.CareButton()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtMMSeq = New Care.Controls.CareTextBox()
        Me.txtMMCaption = New Care.Controls.CareTextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btnCloseButton = New Care.Controls.CareButton()
        Me.gbx.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.txtFormName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClassName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCaption.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDisplaySeq.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTooltip.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxModule.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxMenu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxIcon.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.txtModuleID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMenuID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxMM, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxMM.SuspendLayout()
        CType(Me.txtMMSeq.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMMCaption.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbx
        '
        Me.gbx.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.gbx.Controls.Add(Me.cbxIcon)
        Me.gbx.Controls.Add(Me.cbxMenu)
        Me.gbx.Controls.Add(Me.cbxModule)
        Me.gbx.Controls.Add(Me.Label8)
        Me.gbx.Controls.Add(Me.Label5)
        Me.gbx.Controls.Add(Me.txtDisplaySeq)
        Me.gbx.Controls.Add(Me.txtTooltip)
        Me.gbx.Controls.Add(Me.Label7)
        Me.gbx.Controls.Add(Me.txtCaption)
        Me.gbx.Controls.Add(Me.Label6)
        Me.gbx.Controls.Add(Me.Label3)
        Me.gbx.Controls.Add(Me.txtFormName)
        Me.gbx.Controls.Add(Me.txtClassName)
        Me.gbx.Controls.Add(Me.Label4)
        Me.gbx.Controls.Add(Me.Label2)
        Me.gbx.Controls.Add(Me.Label1)
        Me.gbx.Location = New System.Drawing.Point(12, 274)
        Me.gbx.Size = New System.Drawing.Size(681, 259)
        Me.gbx.TabIndex = 2
        Me.gbx.Controls.SetChildIndex(Me.Label1, 0)
        Me.gbx.Controls.SetChildIndex(Me.Panel2, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label2, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label4, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtClassName, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtFormName, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label3, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label6, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtCaption, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label7, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtTooltip, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtDisplaySeq, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label5, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label8, 0)
        Me.gbx.Controls.SetChildIndex(Me.cbxModule, 0)
        Me.gbx.Controls.SetChildIndex(Me.cbxMenu, 0)
        Me.gbx.Controls.SetChildIndex(Me.cbxIcon, 0)
        '
        'Panel2
        '
        Me.Panel2.Location = New System.Drawing.Point(3, 228)
        Me.Panel2.Size = New System.Drawing.Size(675, 28)
        Me.Panel2.TabIndex = 8
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(490, 0)
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(581, 0)
        '
        'Grid
        '
        Me.Grid.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Grid.Appearance.Options.UseFont = True
        Me.Grid.Location = New System.Drawing.Point(12, 188)
        Me.Grid.Size = New System.Drawing.Size(717, 345)
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnCloseButton)
        Me.Panel1.Location = New System.Drawing.Point(0, 543)
        Me.Panel1.Size = New System.Drawing.Size(740, 35)
        Me.Panel1.TabIndex = 3
        Me.Panel1.Controls.SetChildIndex(Me.btnDelete, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnAdd, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnEdit, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnClose, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnCloseButton, 0)
        '
        'btnDelete
        '
        Me.btnDelete.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Appearance.Options.UseFont = True
        '
        'btnEdit
        '
        Me.btnEdit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.Appearance.Options.UseFont = True
        '
        'btnAdd
        '
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.Appearance.Options.UseFont = True
        '
        'btnClose
        '
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(3794, 5)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Module"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 54)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 15)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Menu"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 112)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 15)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Form Name"
        '
        'txtFormName
        '
        Me.txtFormName.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtFormName.EnterMoveNextControl = True
        Me.txtFormName.Location = New System.Drawing.Point(119, 109)
        Me.txtFormName.MaxLength = 20
        Me.txtFormName.Name = "txtFormName"
        Me.txtFormName.NumericAllowNegatives = False
        Me.txtFormName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtFormName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFormName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFormName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFormName.Properties.Appearance.Options.UseFont = True
        Me.txtFormName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFormName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFormName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFormName.Properties.MaxLength = 20
        Me.txtFormName.ReadOnly = False
        Me.txtFormName.Size = New System.Drawing.Size(200, 22)
        Me.txtFormName.TabIndex = 3
        Me.txtFormName.Tag = "AE"
        Me.txtFormName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtFormName.ToolTipText = ""
        '
        'txtClassName
        '
        Me.txtClassName.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtClassName.EnterMoveNextControl = True
        Me.txtClassName.Location = New System.Drawing.Point(119, 80)
        Me.txtClassName.MaxLength = 20
        Me.txtClassName.Name = "txtClassName"
        Me.txtClassName.NumericAllowNegatives = False
        Me.txtClassName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtClassName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtClassName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtClassName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClassName.Properties.Appearance.Options.UseFont = True
        Me.txtClassName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtClassName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtClassName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtClassName.Properties.MaxLength = 20
        Me.txtClassName.ReadOnly = False
        Me.txtClassName.Size = New System.Drawing.Size(200, 22)
        Me.txtClassName.TabIndex = 2
        Me.txtClassName.Tag = "AE"
        Me.txtClassName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtClassName.ToolTipText = ""
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(6, 83)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(58, 15)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Assembly"
        '
        'txtCaption
        '
        Me.txtCaption.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtCaption.EnterMoveNextControl = True
        Me.txtCaption.Location = New System.Drawing.Point(119, 138)
        Me.txtCaption.MaxLength = 30
        Me.txtCaption.Name = "txtCaption"
        Me.txtCaption.NumericAllowNegatives = False
        Me.txtCaption.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtCaption.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCaption.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtCaption.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCaption.Properties.Appearance.Options.UseFont = True
        Me.txtCaption.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCaption.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtCaption.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtCaption.Properties.MaxLength = 30
        Me.txtCaption.ReadOnly = False
        Me.txtCaption.Size = New System.Drawing.Size(548, 22)
        Me.txtCaption.TabIndex = 5
        Me.txtCaption.Tag = "AE"
        Me.txtCaption.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCaption.ToolTipText = ""
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 141)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(49, 15)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Caption"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(6, 199)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(99, 15)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "Display Sequence"
        '
        'txtDisplaySeq
        '
        Me.txtDisplaySeq.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtDisplaySeq.EnterMoveNextControl = True
        Me.txtDisplaySeq.Location = New System.Drawing.Point(119, 196)
        Me.txtDisplaySeq.MaxLength = 4
        Me.txtDisplaySeq.Name = "txtDisplaySeq"
        Me.txtDisplaySeq.NumericAllowNegatives = False
        Me.txtDisplaySeq.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtDisplaySeq.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDisplaySeq.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtDisplaySeq.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDisplaySeq.Properties.Appearance.Options.UseFont = True
        Me.txtDisplaySeq.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDisplaySeq.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtDisplaySeq.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDisplaySeq.Properties.MaxLength = 4
        Me.txtDisplaySeq.ReadOnly = False
        Me.txtDisplaySeq.Size = New System.Drawing.Size(69, 22)
        Me.txtDisplaySeq.TabIndex = 7
        Me.txtDisplaySeq.Tag = "AE"
        Me.txtDisplaySeq.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDisplaySeq.ToolTipText = ""
        '
        'txtTooltip
        '
        Me.txtTooltip.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtTooltip.EnterMoveNextControl = True
        Me.txtTooltip.Location = New System.Drawing.Point(119, 167)
        Me.txtTooltip.MaxLength = 100
        Me.txtTooltip.Name = "txtTooltip"
        Me.txtTooltip.NumericAllowNegatives = False
        Me.txtTooltip.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtTooltip.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTooltip.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtTooltip.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTooltip.Properties.Appearance.Options.UseFont = True
        Me.txtTooltip.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTooltip.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtTooltip.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTooltip.Properties.MaxLength = 100
        Me.txtTooltip.ReadOnly = False
        Me.txtTooltip.Size = New System.Drawing.Size(548, 22)
        Me.txtTooltip.TabIndex = 6
        Me.txtTooltip.Tag = "AE"
        Me.txtTooltip.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTooltip.ToolTipText = ""
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 170)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(44, 15)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Tooltip"
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(32, 32)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(431, 107)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(30, 15)
        Me.Label8.TabIndex = 8
        Me.Label8.Text = "Icon"
        '
        'cbxModule
        '
        Me.cbxModule.AllowBlank = False
        Me.cbxModule.DataSource = Nothing
        Me.cbxModule.DisplayMember = Nothing
        Me.cbxModule.EnterMoveNextControl = True
        Me.cbxModule.Location = New System.Drawing.Point(119, 23)
        Me.cbxModule.Name = "cbxModule"
        Me.cbxModule.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxModule.Properties.Appearance.Options.UseFont = True
        Me.cbxModule.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxModule.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxModule.ReadOnly = False
        Me.cbxModule.SelectedValue = Nothing
        Me.cbxModule.Size = New System.Drawing.Size(548, 20)
        Me.cbxModule.TabIndex = 0
        Me.cbxModule.Tag = "AE"
        Me.cbxModule.ValueMember = Nothing
        '
        'cbxMenu
        '
        Me.cbxMenu.AllowBlank = False
        Me.cbxMenu.DataSource = Nothing
        Me.cbxMenu.DisplayMember = Nothing
        Me.cbxMenu.EnterMoveNextControl = True
        Me.cbxMenu.Location = New System.Drawing.Point(119, 52)
        Me.cbxMenu.Name = "cbxMenu"
        Me.cbxMenu.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxMenu.Properties.Appearance.Options.UseFont = True
        Me.cbxMenu.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxMenu.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxMenu.ReadOnly = False
        Me.cbxMenu.SelectedValue = Nothing
        Me.cbxMenu.Size = New System.Drawing.Size(548, 20)
        Me.cbxMenu.TabIndex = 1
        Me.cbxMenu.Tag = "AE"
        Me.cbxMenu.ValueMember = Nothing
        '
        'cbxIcon
        '
        Me.cbxIcon.Location = New System.Drawing.Point(467, 97)
        Me.cbxIcon.Name = "cbxIcon"
        Me.cbxIcon.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxIcon.Properties.LargeImages = Me.ImageList1
        Me.cbxIcon.Size = New System.Drawing.Size(200, 34)
        Me.cbxIcon.TabIndex = 4
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainerControl1.Location = New System.Drawing.Point(12, 12)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.cgModules)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.txtModuleID)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.cgMenus)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.txtMenuID)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(715, 170)
        Me.SplitContainerControl1.SplitterPosition = 358
        Me.SplitContainerControl1.TabIndex = 7
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'cgModules
        '
        Me.cgModules.ButtonsEnabled = True
        Me.cgModules.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cgModules.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgModules.HideFirstColumn = False
        Me.cgModules.Location = New System.Drawing.Point(0, 20)
        Me.cgModules.Name = "cgModules"
        Me.cgModules.Size = New System.Drawing.Size(358, 150)
        Me.cgModules.TabIndex = 1
        '
        'txtModuleID
        '
        Me.txtModuleID.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtModuleID.Dock = System.Windows.Forms.DockStyle.Top
        Me.txtModuleID.EditValue = "module_id"
        Me.txtModuleID.EnterMoveNextControl = True
        Me.txtModuleID.Location = New System.Drawing.Point(0, 0)
        Me.txtModuleID.MaxLength = 0
        Me.txtModuleID.Name = "txtModuleID"
        Me.txtModuleID.NumericAllowNegatives = False
        Me.txtModuleID.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtModuleID.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtModuleID.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtModuleID.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtModuleID.Properties.Appearance.Options.UseFont = True
        Me.txtModuleID.Properties.Appearance.Options.UseTextOptions = True
        Me.txtModuleID.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtModuleID.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtModuleID.ReadOnly = False
        Me.txtModuleID.Size = New System.Drawing.Size(358, 20)
        Me.txtModuleID.TabIndex = 0
        Me.txtModuleID.TabStop = False
        Me.txtModuleID.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtModuleID.ToolTipText = ""
        Me.txtModuleID.Visible = False
        '
        'cgMenus
        '
        Me.cgMenus.ButtonsEnabled = True
        Me.cgMenus.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cgMenus.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgMenus.HideFirstColumn = False
        Me.cgMenus.Location = New System.Drawing.Point(0, 20)
        Me.cgMenus.Name = "cgMenus"
        Me.cgMenus.Size = New System.Drawing.Size(352, 150)
        Me.cgMenus.TabIndex = 1
        '
        'txtMenuID
        '
        Me.txtMenuID.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtMenuID.Dock = System.Windows.Forms.DockStyle.Top
        Me.txtMenuID.EditValue = "menu_id"
        Me.txtMenuID.EnterMoveNextControl = True
        Me.txtMenuID.Location = New System.Drawing.Point(0, 0)
        Me.txtMenuID.MaxLength = 0
        Me.txtMenuID.Name = "txtMenuID"
        Me.txtMenuID.NumericAllowNegatives = False
        Me.txtMenuID.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtMenuID.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtMenuID.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtMenuID.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMenuID.Properties.Appearance.Options.UseFont = True
        Me.txtMenuID.Properties.Appearance.Options.UseTextOptions = True
        Me.txtMenuID.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtMenuID.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtMenuID.ReadOnly = False
        Me.txtMenuID.Size = New System.Drawing.Size(352, 20)
        Me.txtMenuID.TabIndex = 0
        Me.txtMenuID.TabStop = False
        Me.txtMenuID.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtMenuID.ToolTipText = ""
        Me.txtMenuID.Visible = False
        '
        'gbxMM
        '
        Me.gbxMM.Controls.Add(Me.btnMMCancel)
        Me.gbxMM.Controls.Add(Me.btnMMOK)
        Me.gbxMM.Controls.Add(Me.Label9)
        Me.gbxMM.Controls.Add(Me.txtMMSeq)
        Me.gbxMM.Controls.Add(Me.txtMMCaption)
        Me.gbxMM.Controls.Add(Me.Label10)
        Me.gbxMM.Location = New System.Drawing.Point(191, 21)
        Me.gbxMM.Name = "gbxMM"
        Me.gbxMM.Size = New System.Drawing.Size(359, 123)
        Me.gbxMM.TabIndex = 0
        '
        'btnMMCancel
        '
        Me.btnMMCancel.Location = New System.Drawing.Point(272, 91)
        Me.btnMMCancel.Name = "btnMMCancel"
        Me.btnMMCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnMMCancel.TabIndex = 5
        Me.btnMMCancel.Text = "Cancel"
        '
        'btnMMOK
        '
        Me.btnMMOK.Location = New System.Drawing.Point(191, 91)
        Me.btnMMOK.Name = "btnMMOK"
        Me.btnMMOK.Size = New System.Drawing.Size(75, 23)
        Me.btnMMOK.TabIndex = 4
        Me.btnMMOK.Text = "OK"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(10, 63)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(58, 15)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "Sequence"
        '
        'txtMMSeq
        '
        Me.txtMMSeq.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtMMSeq.EnterMoveNextControl = True
        Me.txtMMSeq.Location = New System.Drawing.Point(74, 60)
        Me.txtMMSeq.MaxLength = 4
        Me.txtMMSeq.Name = "txtMMSeq"
        Me.txtMMSeq.NumericAllowNegatives = False
        Me.txtMMSeq.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtMMSeq.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtMMSeq.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtMMSeq.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMMSeq.Properties.Appearance.Options.UseFont = True
        Me.txtMMSeq.Properties.Appearance.Options.UseTextOptions = True
        Me.txtMMSeq.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtMMSeq.Properties.Mask.EditMask = "##########;"
        Me.txtMMSeq.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtMMSeq.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtMMSeq.Properties.MaxLength = 4
        Me.txtMMSeq.ReadOnly = False
        Me.txtMMSeq.Size = New System.Drawing.Size(56, 20)
        Me.txtMMSeq.TabIndex = 3
        Me.txtMMSeq.Tag = ""
        Me.txtMMSeq.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtMMSeq.ToolTipText = ""
        '
        'txtMMCaption
        '
        Me.txtMMCaption.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtMMCaption.EnterMoveNextControl = True
        Me.txtMMCaption.Location = New System.Drawing.Point(74, 32)
        Me.txtMMCaption.MaxLength = 30
        Me.txtMMCaption.Name = "txtMMCaption"
        Me.txtMMCaption.NumericAllowNegatives = False
        Me.txtMMCaption.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtMMCaption.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtMMCaption.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtMMCaption.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMMCaption.Properties.Appearance.Options.UseFont = True
        Me.txtMMCaption.Properties.Appearance.Options.UseTextOptions = True
        Me.txtMMCaption.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtMMCaption.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtMMCaption.Properties.MaxLength = 30
        Me.txtMMCaption.ReadOnly = False
        Me.txtMMCaption.Size = New System.Drawing.Size(273, 20)
        Me.txtMMCaption.TabIndex = 1
        Me.txtMMCaption.Tag = ""
        Me.txtMMCaption.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtMMCaption.ToolTipText = ""
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(10, 35)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(49, 15)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Caption"
        '
        'btnCloseButton
        '
        Me.btnCloseButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCloseButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCloseButton.Location = New System.Drawing.Point(644, 5)
        Me.btnCloseButton.Name = "btnCloseButton"
        Me.btnCloseButton.Size = New System.Drawing.Size(85, 25)
        Me.btnCloseButton.TabIndex = 21
        Me.btnCloseButton.Text = "Close"
        '
        'frmMenuItems
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnCloseButton
        Me.ClientSize = New System.Drawing.Size(740, 578)
        Me.Controls.Add(Me.gbxMM)
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Name = "frmMenuItems"
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.Grid, 0)
        Me.Controls.SetChildIndex(Me.gbx, 0)
        Me.Controls.SetChildIndex(Me.SplitContainerControl1, 0)
        Me.Controls.SetChildIndex(Me.gbxMM, 0)
        Me.gbx.ResumeLayout(False)
        Me.gbx.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.txtFormName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClassName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCaption.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDisplaySeq.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTooltip.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxModule.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxMenu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxIcon.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.txtModuleID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMenuID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxMM, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxMM.ResumeLayout(False)
        Me.gbxMM.PerformLayout()
        CType(Me.txtMMSeq.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMMCaption.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtCaption As Care.Controls.CareTextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtFormName As Care.Controls.CareTextBox
    Friend WithEvents txtClassName As Care.Controls.CareTextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtDisplaySeq As Care.Controls.CareTextBox
    Friend WithEvents txtTooltip As Care.Controls.CareTextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cbxModule As Care.Controls.CareComboBox
    Friend WithEvents cbxMenu As Care.Controls.CareComboBox
    Friend WithEvents cbxIcon As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents cgModules As Care.Controls.CareGridWithButtons
    Friend WithEvents txtModuleID As Care.Controls.CareTextBox
    Friend WithEvents cgMenus As Care.Controls.CareGridWithButtons
    Friend WithEvents txtMenuID As Care.Controls.CareTextBox
    Friend WithEvents gbxMM As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnMMCancel As Care.Controls.CareButton
    Friend WithEvents btnMMOK As Care.Controls.CareButton
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtMMSeq As Care.Controls.CareTextBox
    Friend WithEvents txtMMCaption As Care.Controls.CareTextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnCloseButton As Care.Controls.CareButton

End Class
