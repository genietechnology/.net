﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportExport
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportExport))
        Me.btnOpenFolder = New Care.Controls.CareButton()
        Me.txtPath = New Care.Controls.CareTextBox()
        Me.btnExport = New Care.Controls.CareButton()
        Me.GroupBox8 = New Care.Controls.CareFrame()
        Me.cgTables = New Care.Controls.CareGrid()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.Label2 = New Care.Controls.CareLabel()
        Me.btnImport = New Care.Controls.CareButton()
        Me.ofd = New System.Windows.Forms.OpenFileDialog()
        Me.btnExportCSV = New Care.Controls.CareButton()
        CType(Me.txtPath.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox8.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'btnOpenFolder
        '
        Me.btnOpenFolder.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOpenFolder.Appearance.Options.UseFont = True
        Me.btnOpenFolder.Location = New System.Drawing.Point(12, 580)
        Me.btnOpenFolder.Name = "btnOpenFolder"
        Me.btnOpenFolder.Size = New System.Drawing.Size(85, 23)
        Me.btnOpenFolder.TabIndex = 2
        Me.btnOpenFolder.Text = "Open Folder"
        '
        'txtPath
        '
        Me.txtPath.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtPath.EnterMoveNextControl = True
        Me.txtPath.Location = New System.Drawing.Point(120, 29)
        Me.txtPath.MaxLength = 0
        Me.txtPath.Name = "txtPath"
        Me.txtPath.NumericAllowNegatives = False
        Me.txtPath.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtPath.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPath.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPath.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtPath.Properties.Appearance.Options.UseFont = True
        Me.txtPath.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPath.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPath.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPath.Properties.ReadOnly = True
        Me.txtPath.Size = New System.Drawing.Size(362, 22)
        Me.txtPath.TabIndex = 1
        Me.txtPath.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPath.ToolTipText = ""
        '
        'btnExport
        '
        Me.btnExport.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.Appearance.Options.UseFont = True
        Me.btnExport.Location = New System.Drawing.Point(283, 580)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.Size = New System.Drawing.Size(110, 23)
        Me.btnExport.TabIndex = 4
        Me.btnExport.Text = "Export to XML File"
        Me.btnExport.ToolTip = "Export one or more tables to an XML file."
        Me.btnExport.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.btnExport.ToolTipTitle = "Export to XML File"
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.cgTables)
        Me.GroupBox8.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(497, 496)
        Me.GroupBox8.TabIndex = 0
        Me.GroupBox8.Text = "Select Table"
        '
        'cgTables
        '
        Me.cgTables.AllowBuildColumns = True
        Me.cgTables.AllowEdit = False
        Me.cgTables.AllowHorizontalScroll = False
        Me.cgTables.AllowMultiSelect = True
        Me.cgTables.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgTables.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgTables.Appearance.Options.UseFont = True
        Me.cgTables.AutoSizeByData = True
        Me.cgTables.DisableAutoSize = False
        Me.cgTables.DisableDataFormatting = False
        Me.cgTables.FocusedRowHandle = -2147483648
        Me.cgTables.HideFirstColumn = False
        Me.cgTables.Location = New System.Drawing.Point(5, 24)
        Me.cgTables.Name = "cgTables"
        Me.cgTables.PreviewColumn = ""
        Me.cgTables.QueryID = Nothing
        Me.cgTables.RowAutoHeight = False
        Me.cgTables.SearchAsYouType = True
        Me.cgTables.ShowAutoFilterRow = False
        Me.cgTables.ShowFindPanel = False
        Me.cgTables.ShowGroupByBox = False
        Me.cgTables.ShowLoadingPanel = False
        Me.cgTables.ShowNavigator = False
        Me.cgTables.Size = New System.Drawing.Size(487, 467)
        Me.cgTables.TabIndex = 0
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.Label2)
        Me.GroupControl1.Controls.Add(Me.txtPath)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 514)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(497, 60)
        Me.GroupControl1.TabIndex = 1
        Me.GroupControl1.Text = "XML && Table Details"
        '
        'Label2
        '
        Me.Label2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label2.Appearance.Options.UseFont = True
        Me.Label2.Appearance.Options.UseTextOptions = True
        Me.Label2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label2.Location = New System.Drawing.Point(13, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 15)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "XML Path"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnImport
        '
        Me.btnImport.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImport.Appearance.Options.UseFont = True
        Me.btnImport.Location = New System.Drawing.Point(167, 580)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(110, 23)
        Me.btnImport.TabIndex = 3
        Me.btnImport.Text = "Import XML File"
        Me.btnImport.ToolTip = "Select the table you wish to import into." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Ensure only one table is selected."
        Me.btnImport.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.btnImport.ToolTipTitle = "Import XML File"
        '
        'ofd
        '
        Me.ofd.Filter = "XML Files|*.xml|All files|*.*"
        '
        'btnExportCSV
        '
        Me.btnExportCSV.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExportCSV.Appearance.Options.UseFont = True
        Me.btnExportCSV.Location = New System.Drawing.Point(399, 580)
        Me.btnExportCSV.Name = "btnExportCSV"
        Me.btnExportCSV.Size = New System.Drawing.Size(110, 23)
        Me.btnExportCSV.TabIndex = 6
        Me.btnExportCSV.Text = "Export to CSV File"
        Me.btnExportCSV.ToolTip = resources.GetString("btnExportCSV.ToolTip")
        Me.btnExportCSV.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.btnExportCSV.ToolTipTitle = "Export to CSV"
        '
        'frmImportExport
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(520, 612)
        Me.Controls.Add(Me.btnExportCSV)
        Me.Controls.Add(Me.btnImport)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.GroupBox8)
        Me.Controls.Add(Me.btnOpenFolder)
        Me.Controls.Add(Me.btnExport)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportExport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Import / Export Data"
        CType(Me.txtPath.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox8.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnOpenFolder As Care.Controls.CareButton
    Friend WithEvents txtPath As Care.Controls.CareTextBox
    Friend WithEvents btnExport As Care.Controls.CareButton
    Friend WithEvents cgTables As Care.Controls.CareGrid
    Friend WithEvents Label2 As Care.Controls.CareLabel
    Friend WithEvents btnImport As Care.Controls.CareButton
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents btnExportCSV As Controls.CareButton
    Friend WithEvents GroupBox8 As Controls.CareFrame
    Friend WithEvents GroupControl1 As Controls.CareFrame
End Class
