﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmErrors
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.grd = New Care.Controls.CareGrid()
        Me.btnView = New Care.Controls.CareButton(Me.components)
        Me.btnCopy = New Care.Controls.CareButton(Me.components)
        Me.btnEmail = New Care.Controls.CareButton(Me.components)
        Me.gbxStack = New Care.Controls.CareFrame(Me.components)
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.txtStack = New DevExpress.XtraEditors.MemoEdit()
        CType(Me.gbxStack, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxStack.SuspendLayout()
        CType(Me.txtStack.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'grd
        '
        Me.grd.AllowBuildColumns = True
        Me.grd.AllowEdit = False
        Me.grd.AllowHorizontalScroll = False
        Me.grd.AllowMultiSelect = True
        Me.grd.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grd.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grd.Appearance.Options.UseFont = True
        Me.grd.AutoSizeByData = True
        Me.grd.DisableAutoSize = False
        Me.grd.DisableDataFormatting = False
        Me.grd.FocusedRowHandle = -2147483648
        Me.grd.HideFirstColumn = False
        Me.grd.Location = New System.Drawing.Point(12, 41)
        Me.grd.Name = "grd"
        Me.grd.PreviewColumn = ""
        Me.grd.QueryID = Nothing
        Me.grd.RowAutoHeight = False
        Me.grd.SearchAsYouType = True
        Me.grd.ShowAutoFilterRow = False
        Me.grd.ShowFindPanel = False
        Me.grd.ShowGroupByBox = False
        Me.grd.ShowLoadingPanel = False
        Me.grd.ShowNavigator = True
        Me.grd.Size = New System.Drawing.Size(625, 260)
        Me.grd.TabIndex = 3
        '
        'btnView
        '
        Me.btnView.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnView.Appearance.Options.UseFont = True
        Me.btnView.Location = New System.Drawing.Point(12, 9)
        Me.btnView.Name = "btnView"
        Me.btnView.Size = New System.Drawing.Size(131, 23)
        Me.btnView.TabIndex = 0
        Me.btnView.Text = "View Stack Trace"
        '
        'btnCopy
        '
        Me.btnCopy.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCopy.Appearance.Options.UseFont = True
        Me.btnCopy.Location = New System.Drawing.Point(149, 9)
        Me.btnCopy.Name = "btnCopy"
        Me.btnCopy.Size = New System.Drawing.Size(210, 23)
        Me.btnCopy.TabIndex = 1
        Me.btnCopy.Text = "Copy Exception Details to Clipboard"
        '
        'btnEmail
        '
        Me.btnEmail.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnEmail.Appearance.Options.UseFont = True
        Me.btnEmail.Location = New System.Drawing.Point(365, 9)
        Me.btnEmail.Name = "btnEmail"
        Me.btnEmail.Size = New System.Drawing.Size(204, 23)
        Me.btnEmail.TabIndex = 2
        Me.btnEmail.Text = "Email Exception to Service Desk"
        '
        'gbxStack
        '
        Me.gbxStack.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxStack.Controls.Add(Me.btnClose)
        Me.gbxStack.Controls.Add(Me.txtStack)
        Me.gbxStack.Location = New System.Drawing.Point(36, 78)
        Me.gbxStack.Name = "gbxStack"
        Me.gbxStack.Size = New System.Drawing.Size(576, 187)
        Me.gbxStack.TabIndex = 4
        Me.gbxStack.Text = "StackTrace"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(509, 155)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(56, 23)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "Close"
        '
        'txtStack
        '
        Me.txtStack.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtStack.Location = New System.Drawing.Point(12, 28)
        Me.txtStack.Name = "txtStack"
        Me.txtStack.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStack.Properties.Appearance.Options.UseFont = True
        Me.txtStack.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtStack, True)
        Me.txtStack.Size = New System.Drawing.Size(553, 121)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtStack, OptionsSpelling1)
        Me.txtStack.TabIndex = 0
        Me.txtStack.Tag = ""
        '
        'frmErrors
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(649, 313)
        Me.Controls.Add(Me.gbxStack)
        Me.Controls.Add(Me.btnEmail)
        Me.Controls.Add(Me.btnCopy)
        Me.Controls.Add(Me.btnView)
        Me.Controls.Add(Me.grd)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.LoadMaximised = True
        Me.Name = "frmErrors"
        Me.Text = "frmErrors"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.gbxStack, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxStack.ResumeLayout(False)
        CType(Me.txtStack.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents grd As Controls.CareGrid
    Friend WithEvents btnView As Controls.CareButton
    Friend WithEvents btnCopy As Controls.CareButton
    Friend WithEvents btnEmail As Controls.CareButton
    Friend WithEvents gbxStack As Controls.CareFrame
    Friend WithEvents btnClose As Controls.CareButton
    Friend WithEvents txtStack As DevExpress.XtraEditors.MemoEdit
End Class
