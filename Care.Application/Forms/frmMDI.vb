﻿Imports Care.Global
Imports Care.Shared
Imports Care.Data
Imports DevExpress.XtraEditors.Repository
Imports DevExpress.XtraSpellChecker.Native
Imports System.IO
Imports DevExpress.XtraSpellChecker
Imports System.Data.SqlClient
Imports DevExpress.XtraNavBar
Imports System.ServiceModel
Imports System.ComponentModel

Public Class frmMDI

    Implements IMDIForm

    Private m_CTIHost As ServiceHost
    Private m_AutoTheme As String = ""
    Private m_ActiveTheme As String = ""
    Private m_DisableWaitForms As Boolean = False

    Public ReadOnly Property MDIChildrenArray As Form() Implements IMDIForm.MDIChildrenArray
        Get
            Return Me.MdiChildren
        End Get
    End Property

#Region "Public Methods"

    Public Sub EmailExportFile(FilePath As String) Implements IMDIForm.EmailExportFile
        Dim _Body As String = "Please see the attached Excel file export."
        EmailHandler.SendEmailWithAttachment(Nothing, Session.CurrentUser.Email, "Exported Grid", _Body, FilePath)
    End Sub

    Public Sub SetProgressMessage(ByVal MessageText As String, Optional ByVal TimeOut As Integer = 0) Implements IMDIForm.SetProgressMessage

        If MessageText = "" Then
            bsiProgressText.Caption = ""
            bsiProgressText.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        Else

            bsiProgressText.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
            bsiProgressText.Caption = MessageText

            If TimeOut > 0 Then
                timMessage.Interval = TimeOut * 1000
                timMessage.Start()
            End If

        End If

        bsiProgressText.Refresh()
        Application.DoEvents()

    End Sub

    Public Sub SetupProgressBar(ByVal MessageText As String, ByVal MaximumValue As Integer) Implements IMDIForm.SetupProgressBar

        bsiProgressText.Caption = MessageText
        bsiProgressText.Visibility = DevExpress.XtraBars.BarItemVisibility.Always

        beiProgressBar.Visibility = DevExpress.XtraBars.BarItemVisibility.Always

        riProgress.Maximum = MaximumValue
        beiProgressBar.EditValue = 0
        beiProgressBar.Refresh()

    End Sub

    Public Sub StepProgressBar(Optional ByVal MessageText As String = "") Implements IMDIForm.StepProgressBar

        If MessageText <> "" Then bsiProgressText.Caption = MessageText

        Dim _Value As Long = CLng(beiProgressBar.EditValue)
        _Value += 1

        beiProgressBar.EditValue = _Value
        beiProgressBar.Refresh()

    End Sub

    Public Sub HideProgressBar() Implements IMDIForm.HideProgressBar
        bsiProgressText.Caption = ""
        bsiProgressText.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        beiProgressBar.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        timMessage.Stop()
        beiProgressBar.Refresh()
    End Sub

#End Region

#Region "Form Events"

    Private Sub frmMDI_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'DO NOT USE LOAD - IT MESSES WITH THE WAIT FORM
        dpRight.HideImmediately()
        bsiComputerName.Caption = ""
        bsiUserName.Caption = ""
        mnuDevelopment.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
    End Sub

    Private Sub frmMDI_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown

        HideProgressBar()
        Session.MDIFormObject = Me

        bsiUserName.Caption = Session.CurrentUser.FullName
        bsiComputerName.Caption = My.Computer.Name + " running " + My.Application.Info.Version.ToString

        'set customer ID
        SetCustomerID()

        'check runtime matches DB
        SetProgressMessage("Checking database...")
        If CheckVersionAgainstDB() Then

            Session.WaitFormShow()

            'caching parameters needs to happen on the same thread in case there is a clash
            ParameterHandler.CacheParameters()

            'kick off background workers
            bgwSpellChecker.RunWorkerAsync()
            bgwBrowsers.RunWorkerAsync()
            bgwCTI.RunWorkerAsync()
            bgwCache.RunWorkerAsync()
            bgwCheckIn.RunWorkerAsync()

            SetProgressMessage("Loading skins...")
            For Each _s As DevExpress.Skins.SkinContainer In DevExpress.Skins.SkinManager.Default.Skins
                CType(cbxThemes.Edit, RepositoryItemComboBox).Items.Add(_s.SkinName)
            Next

            SetProgressMessage("Loading user preferences...")
            LoadUserPreferences()

            SetSkin()

            'DevExpress.Skins.SkinManager.Default.Skins
            'http://devexpress.com/Support/Center/p/Q296422.aspx

            SetProgressMessage("Loading menus...")
            BuildNewMenuModules()

            If Session.CurrentUser.IsStandardUser Then
                mnuSystem.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
                mnuReportDesigner.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            Else
                mnuSystem.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                mnuReportDesigner.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
            End If

            Me.Text = Session.ApplicationName
            txtSearch.BackColor = Session.FindColour

            Session.WaitFormClose()
            HideProgressBar()

            m_DisableWaitForms = ParameterHandler.ReturnBoolean("DISABLEWAITFORM")

            Cursor.Current = Cursors.Default

            DisplayMessages()

        Else
            End
        End If

    End Sub

    Private Function SetCustomerID() As Boolean

        Dim _Store As Boolean = False
        Dim _ID As String = ""

        If Session.CustomerID = "" Then
            _ID = InputBox("Please enter your Customer ID", "Customer ID", "")
            If _ID <> "" Then
                _Store = True
            Else
                'customer ID not provided
                Return False
            End If
        Else
            _ID = Session.CustomerID
            Return True
        End If

        Dim _GUID As Guid = ValueHandler.ConvertGUID(_ID)
        If Not _GUID = Nothing Then
            'store this value in the parameters
            If _Store Then ParameterHandler.SetString("CUSTOMERID", _ID)
            Return True
        Else
            'invalid ID
            Return False
        End If

    End Function

    Private Sub frmMDI_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If e.CloseReason = CloseReason.UserClosing Then
            If Not ExitCheck() Then
                e.Cancel = True
            Else
                ClearTempFolders()
                SaveUserPreferences()
            End If
        End If
    End Sub

    Private Sub frmMDI_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown

        If e.Control AndAlso e.Shift AndAlso e.KeyCode = Keys.F12 Then
            mnuDevelopment.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        End If

        If e.Control AndAlso e.Shift AndAlso e.KeyCode = Keys.P Then
            LaunchPostCodeLookup()
        End If
    End Sub

#End Region

#Region "Menu System"

    Private Sub BuildNewMenuGroups(GroupName As String, Clear As Boolean)

        navMenu.Groups.Clear()

        Dim _SQL As String = "select id, caption, tooltip from AppMenus" &
                             " where module_id = '" & GroupName & "'" &
                             " order by display_seq"

        Dim _DR As SqlDataReader = DAL.ReturnDataReaderfromSQL(Session.ConnectionString, _SQL)
        If Not _DR Is Nothing Then

            Dim _key As String = ""
            Dim _caption As String = ""
            Dim _tooltip As String = ""
            Dim _tag As String = ""

            While _DR.Read

                _key = _DR.Item("id").ToString
                _caption = _DR.Item("caption").ToString
                _tooltip = _DR.Item("tooltip").ToString

                Dim _G As New DevExpress.XtraNavBar.NavBarGroup
                With _G
                    .Name = _key
                    .Caption = _caption
                    .Hint = _tooltip
                    .GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsText
                    .Tag = _tag
                End With

                navMenu.Groups.Add(_G)

                BuildNewMenuItems(_key)

            End While

            If Clear = False Then

                Dim _Root As New DevExpress.XtraNavBar.NavBarGroup
                With _Root
                    .Name = "MODULES"
                    .Caption = "Modules"
                    .Hint = ""
                    .Tag = ""
                End With

                navMenu.Groups.Add(_Root)

            End If

            _DR = Nothing

        End If

    End Sub

    Private Sub BuildNewMenuItems(ByVal MenuID As String)

        Dim _SQL As String = ""

        _SQL += "select f.class_name, f.form_name, f.caption, f.tooltip, f.icon, f.multiple"

        If Session.CurrentUser.GroupID <> "" Then
            _SQL += ", p.noaccess, p.readonly"
            _SQL += " from AppForms f"
            _SQL += " left join AppPerms p on p.item_id = f.ID"
            _SQL += " and p.parent_id = '" + Session.CurrentUser.GroupID + "'"
        Else
            _SQL += " from AppForms f"
        End If

        _SQL += " where f.menu_id = '" & MenuID & "'"
        _SQL += " order by f.display_seq"

        Dim _DR As SqlDataReader = DAL.ReturnDataReaderfromSQL(Session.ConnectionString, _SQL)
        If Not _DR Is Nothing Then

            Dim _icon As String = ""
            Dim _default As Boolean = False
            While _DR.Read

                Dim _Tag As String = ""
                Dim _Access As Boolean = True

                If Session.CurrentUser.GroupID <> "" Then

                    If ValueHandler.ConvertBoolean(_DR.Item("noaccess")) Then
                        _Access = False
                    End If

                    If ValueHandler.ConvertBoolean(_DR.Item("readonly")) Then
                        _Tag += "R"
                    End If

                End If

                If _Access Then

                    Dim _Item As New DevExpress.XtraNavBar.NavBarItem
                    With _Item
                        .Name = _DR.Item("class_name").ToString & "." & _DR.Item("form_name").ToString
                        .Caption = _DR.Item("caption").ToString
                        .Hint = _DR.Item("tooltip").ToString
                        .Tag = ""
                    End With

                    If ValueHandler.ConvertBoolean(_DR.Item("multiple")) Then
                        _Tag += "M"
                    End If

                    If _DR.Item("icon").ToString <> "" Then
                        _icon = Session.IconFolder + _DR.Item("icon").ToString
                        If Not IO.File.Exists(_icon) Then
                            _icon = Session.IconFolder + "default.ico"
                        End If
                    Else
                        _icon = Session.IconFolder + "default.ico"
                    End If

                    _Item.LargeImage = DevExpress.Utils.ImageCollection.ImageFromFile(_icon)
                    _Item.Tag = _Tag

                    navMenu.Groups(MenuID).ItemLinks.Add(_Item)

                End If

            End While

            _DR = Nothing

        End If

    End Sub

    Private Sub BuildNewMenuModules()

        navMenu.Groups.Clear()

        Dim _SQL As String = "select id, caption, tooltip, icon from AppModules" &
                             " order by display_seq"

        Dim _DR As SqlDataReader = DAL.ReturnDataReaderfromSQL(Session.ConnectionString, _SQL)
        If Not _DR Is Nothing Then

            Dim _G As New DevExpress.XtraNavBar.NavBarGroup
            With _G
                .Name = "Root"
                .Caption = "Modules"
                .Tag = ""
            End With

            navMenu.Groups.Add(_G)

            Dim _name As String = ""
            Dim _key As String = ""
            Dim _caption As String = ""
            Dim _tooltip As String = ""
            Dim _tag As String = ""
            Dim _i As New DevExpress.XtraNavBar.NavBarItem
            Dim _ModuleCount As Integer = 0

            While _DR.Read

                _name = "default"
                If _DR.Item("icon").ToString <> "" Then
                    _name = Mid(_DR.Item("icon").ToString, 1, _DR.Item("icon").ToString.Length - 4)
                End If

                _key = _DR.Item("id").ToString
                _caption = _DR.Item("caption").ToString
                _tooltip = _DR.Item("tooltip").ToString
                _tag = "MODULE"

                With _i
                    .Name = _key
                    .Caption = _caption
                    .Tag = _tag
                End With

                navMenu.Groups("Root").ItemLinks.Add(_i)
                _ModuleCount += 1

            End While

            If _ModuleCount > 0 Then
                If _ModuleCount = 1 Then
                    BuildNewMenuGroups(_i.Name, True)
                End If
                navMenu.Groups(0).Expanded = True
            End If

            _DR = Nothing

        End If

    End Sub

#End Region

#Region "Control Events"

    Private Sub timMessage_Tick(sender As Object, e As System.EventArgs) Handles timMessage.Tick
        HideProgressBar()
    End Sub

    Private Sub navMenu_ActiveGroupChanged(sender As Object, e As DevExpress.XtraNavBar.NavBarGroupEventArgs) Handles navMenu.ActiveGroupChanged

        If e.Group IsNot Nothing Then
            If e.Group.Name = "MODULES" Then
                BuildNewMenuModules()
                'BBar.Groups("Root").Selected = True
            End If
        End If

    End Sub

    Private Sub navMenu_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles navMenu.LinkClicked

        If e.Link.Item.Tag.ToString = "MODULE" Then
            BuildNewMenuGroups(e.Link.Item.Name, False)
        Else
            ShowWaitForm(e.Link.Item.Caption, "Loading please wait...")
            FormHandler.LoadFormUsingReflection(e.Link.Item.Name, e.Link.Item.Caption, e.Link.Item.Tag.ToString)
            CloseWaitForm()
        End If
    End Sub

    Private Sub cbxThemes_EditValueChanged(sender As Object, e As System.EventArgs) Handles cbxThemes.EditValueChanged
        DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle(cbxThemes.EditValue.ToString)
    End Sub

    Private Sub mnuUsers_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuUsers.ItemClick
        Dim _frm As New frmUsers
        FormHandler.ShowMDIForm(_frm, "User Manager", False)
        _frm = Nothing
    End Sub

    Private Sub mnuParameters_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuParameters.ItemClick
        Dim _frm As New frmParameters
        FormHandler.ShowMDIForm(_frm, "Application Parameters", False)
        _frm = Nothing
    End Sub

    Private Sub mnuUserPreferences_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuUserPreferences.ItemClick
        Dim _frm As New frmUserPreferences
        FormHandler.ShowMDIForm(_frm, "User Preferences", False)
        _frm = Nothing
    End Sub

    Private Sub mnuExit_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuExit.ItemClick
        If ExitCheck() Then Shutdown()
    End Sub

    Private Sub mnuMenuManager_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuMenuManager.ItemClick
        Dim _frm As New frmMenuItems
        FormHandler.ShowMDIForm(_frm, "Menu Manager", True)
        _frm = Nothing
        BuildNewMenuModules()
    End Sub

    Private Sub mnuHelpdesk_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuHelpdesk.ItemClick
        LaunchURL("https://genietechnology.freshdesk.com")
    End Sub

    Private Sub mnuRemoteAssist_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRemoteAssist.ItemClick
        LaunchURL("http://connect.caresoftware.co.uk/guest")
    End Sub

    Private Sub mnuNewDoc_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuNewDoc.ItemClick
        WordProcessing.CreateDocument()
    End Sub

    Private Sub mnuLetterMaint_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuLetterMaint.ItemClick
        Dim _frm As New frmLetters
        FormHandler.ShowMDIForm(_frm, "Letter Maintenance", False)
        _frm = Nothing
    End Sub

    Private Sub mnuAbout_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuAbout.ItemClick
        Dim _frm As New frmAbout
        FormHandler.ShowMDIForm(_frm, "About " + Me.Text, True)
        _frm = Nothing
    End Sub

    Private Sub mnuChangePassword_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuChangePassword.ItemClick

        Dim _frm As New frmChangePassword
        _frm.IsReset = False
        _frm.ShowDialog()

        _frm = Nothing

    End Sub

    Private Sub mnuBackup_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuBackup.ItemClick
        BackupForm()
    End Sub

    Private Sub mnuSendEmail_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuSendEmail.ItemClick

        If Session.SMTPEnabled Then
            Dim _frm As New frmSendEmail
            FormHandler.ShowMDIForm(_frm, "Send Email", True)
            _frm = Nothing
        Else
            CareMessage("Email has not been enabled. Check the System Parameters.", MessageBoxIcon.Error, "Send Email")
        End If

    End Sub

    Private Sub mnuSendSMS_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuSendSMS.ItemClick

        If Session.SMSEnabled Then
            Dim _frm As New frmSendSMS
            FormHandler.ShowMDIForm(_frm, "Send SMS", True)
            _frm = Nothing
        Else
            CareMessage("SMS has not been enabled. Check the System Parameters.", MessageBoxIcon.Error, "Send SMS")
        End If

    End Sub

    Private Sub bbiPostCodeLookup_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiPostCodeLookup.ItemClick
        LaunchPostCodeLookup()
    End Sub

    Private Sub LaunchPostCodeLookup()
        Dim _frm As New frmPostCodeLookup
        FormHandler.ShowMDIForm(_frm, "Post Code Lookup", True)
        _frm = Nothing
    End Sub

    Private Sub mnuLists_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuLists.ItemClick
        Dim _frm As New frmListMaintenance
        FormHandler.ShowMDIForm(_frm, "List Manager", False)
        _frm = Nothing
    End Sub

    Private Sub mnuDataExplorer_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuDataExplorer.ItemClick
        Dim _frm As New frmDataExplorer
        FormHandler.ShowMDIForm(_frm, "Data Explorer", False)
        _frm = Nothing
    End Sub

    Private Sub mnuReportDesigner_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuReportDesigner.ItemClick
        SetProgressMessage("Loading Report Designer...")
        Dim _frm As New frmDXReportDesigner
        _frm.Show()
    End Sub

    Private Sub bbiCalendars_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiCalendars.ItemClick

        If ParameterHandler.CheckExists("CALENDARFORM") Then

            Dim _CalendarForm As String = ParameterHandler.ReturnString("CALENDARFORM")
            If _CalendarForm = "" Then
                SetProgressMessage("Parameter [CALENDARFORM] is blank.", 10)
                Exit Sub
            End If

            Try

                SetProgressMessage("Loading Calendars...")
                FormHandler.LoadFormUsingReflection(_CalendarForm, "Calendars")

            Catch ex As Exception
                SetProgressMessage("Error extracting Parameters for [CALENDARFORM].", 10)
            End Try

        Else
            SetProgressMessage("Parameter [CALENDARFORM] not defined.", 10)
        End If

    End Sub

    Private Sub mnuLicensing_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuLicensing.ItemClick
        Dim _frm As New frmLicensing
        _frm.ShowDialog()
        _frm = Nothing
    End Sub

    Private Sub bbiReportView_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiReportView.ItemClick
        ReportHandler.LoadReportCentre()
    End Sub

    Private Sub mnuSecurity_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuSecurity.ItemClick
        Dim _frm As New frmSecurity
        FormHandler.ShowMDIForm(_frm, "Security", False)
        _frm = Nothing
    End Sub

#End Region

#Region "CTI"

    Private Sub SetupCTI()

        If Session.CTIEnabled Then

            Dim _Error As String = ""
            Dim _CTIListening As Boolean = True

            Try
                m_CTIHost = New ServiceHost(GetType(CTICall), New Uri("net.tcp://localhost:9000"))
                m_CTIHost.AddServiceEndpoint(GetType(IIncomingCall), New NetTcpBinding, "LogCall")
                m_CTIHost.Open()

            Catch ex As Exception
                _CTIListening = False
                _Error = ex.Message
            End Try

            If _CTIListening Then
                bbiCallLog.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                bsiCTIEnabled.Hint = "Telephone System Integration Enabled"
                bsiCTIDisabled.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            Else
                bbiCallLog.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
                bsiCTIEnabled.Hint = "Telephone System Integration Error"
                bsiCTIEnabled.Tag = _Error
                bsiCTIDisabled.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            End If

        Else
            bbiCallLog.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            bsiCTIEnabled.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            bsiCTIDisabled.Hint = "Telephone System Integration Disabled"
        End If

    End Sub

    Public Sub CTIPopup(IncomingNumber As String, CallerMatched As Boolean, CallerName As String, Ref1 As String, Ref2 As String) Implements IMDIForm.CTIPopup

        If CallerMatched Then

            Dim _Tag As String = ""

            If Ref2 = "" Then
                _Tag = Ref1
            Else
                _Tag = Ref1 + "|" + Ref2
            End If

            alcCTI.Buttons(0).Visible = True
            alcCTI.Show(Me, CallerName, IncomingNumber, "", Nothing, _Tag)

        Else
            alcCTI.Buttons(0).Visible = False
            alcCTI.Show(Me, "Incoming Call " + IncomingNumber, "Unknown Contact")
        End If

        Session.CTILogCall(IncomingNumber, CallerMatched, CallerName, Ref1, Ref2)

    End Sub

    Private Sub alcCTI_ButtonClick(sender As Object, e As DevExpress.XtraBars.Alerter.AlertButtonClickEventArgs) Handles alcCTI.ButtonClick
        AlertDrillDown(e.Info)
    End Sub

    Private Sub AlertDrillDown(ByVal Info As DevExpress.XtraBars.Alerter.AlertInfo)

        Session.CursorApplicationStarting()

        Select Case Session.ApplicationCode

            Case "NURSERY"
                Dim _ex As System.Exception = Nothing
                Dim _frm As ICTIForm = FormHandler.ReturnInstance(Of ICTIForm)("Nursery", "frmFamily", _ex)
                _frm.DrillDown(Info)

        End Select

    End Sub

    Public Sub CTIShowCallLog() Implements IMDIForm.CTIShowCallLog
        If Session.CTIEnabled AndAlso Session.CTICalls.Count > 0 Then
            Dim _frm As New frmCallLog
            _frm.Show()
        End If
    End Sub

#End Region

    Private Function AddDictionary(ByVal DictionaryPath As String, ByVal GrammarPath As String) As Boolean

        If Not File.Exists(DictionaryPath) Then Return False
        If Not File.Exists(GrammarPath) Then Return False

        Dim _Return As Boolean = False

        Try

            Dim _ood As New SpellCheckerOpenOfficeDictionary
            _ood.DictionaryPath = DictionaryPath
            _ood.GrammarPath = GrammarPath
            _ood.Load()

            If _ood.Loaded Then
                SharedDictionaryStorage1.Dictionaries.Add(_ood)
                _Return = True
            End If

            _ood = Nothing

        Catch ex As Exception
            CareMessage(ex.Message, MessageBoxIcon.Warning, MessageBoxButtons.OK, "Setup SpellChecker")
        End Try

        Return _Return

    End Function

    Private Sub SetSkin()

        'check for halloween
        If Today.Date = DateSerial(Today.Year, 10, 31) Then
            cbxThemes.EditValue = "Pumpkin"
            m_AutoTheme = "Halloween"
        Else
            If Today.Date = DateSerial(Today.Year, 2, 14) Then
                cbxThemes.EditValue = "Valentine"
                m_AutoTheme = "Valentine"
            Else
                cbxThemes.EditValue = m_ActiveTheme
                m_AutoTheme = ""
            End If
        End If

    End Sub

    Private Sub SetSpellChecking()

        If ParameterHandler.ReturnBoolean("SPELL") Then

            Dim _Dictionary As String = ParameterHandler.ReturnString("SPELLDICT")
            Dim _Grammar As String = ParameterHandler.ReturnString("SPELLGRAM")

            If _Dictionary <> "" AndAlso _Grammar <> "" Then
                If AddDictionary(_Dictionary, _Grammar) Then
                    SpellCheckTextControllersManager.Default.RegisterClass(GetType(Care.Controls.CareTextBox), GetType(SimpleTextEditTextController))
                    SpellCheckTextBoxBaseFinderManager.Default.RegisterClass(GetType(Care.Controls.CareTextBox), GetType(TextEditTextBoxFinder))
                    SpellCheckTextControllersManager.Default.RegisterClass(GetType(DevExpress.XtraEditors.MemoEdit), GetType(SimpleTextEditTextController))
                    SpellCheckTextBoxBaseFinderManager.Default.RegisterClass(GetType(DevExpress.XtraEditors.MemoEdit), GetType(TextEditTextBoxFinder))
                    Session.SpellCheck = True
                    Session.SpellCheckDictionaryPath = _Dictionary
                    Session.SpellCheckGrammmarPath = _Grammar
                End If
            End If

        Else
            Session.SpellCheck = False
        End If

    End Sub

    Private Sub ClearTempFolders()
        FileHandler.ClearTemporaryFolder(Session.TempFolder)
    End Sub

    Private Function ExitCheck() As Boolean

        If CareMessage("Are you sure you want to Exit the Application?", MessageBoxIcon.Question, MessageBoxButtons.YesNo, "Exit?") = DialogResult.Yes Then
            If Not BackupCheck() Then
                BackupForm()
                Return True
            Else
                Return True
            End If
        Else
            Return False
        End If

    End Function

    Private Sub Shutdown()
        ClearTempFolders()
        SaveUserPreferences()
        Environment.Exit(0)
    End Sub

    Private Sub LaunchURL(ByVal URL As String)
        Process.Start(URL)
    End Sub

    Private Function BackupCheck() As Boolean

        'Return
        'True  > Allows the user to Exit the Application
        'False > Stops the application from Exitting and calls the Backup Program

        If ParameterHandler.ReturnBoolean("BACKUPCHECK") Then
            Dim _Last As Date? = ParameterHandler.ReturnDate("BACKUPLAST")
            If _Last Is Nothing Then
                If BackupPrompt(0) Then
                    Return False
                Else
                    Return True
                End If
            Else

                Dim _Diff As Long = DateDiff(DateInterval.Day, _Last.Value, Date.Today)
                Dim _MaxDaysBetweenBackups As Integer = ParameterHandler.ReturnInteger("BACKUPDAYS", False)

                If _Diff > _MaxDaysBetweenBackups Then
                    If BackupPrompt(CInt(_Diff)) Then
                        Return False
                    Else
                        Return True
                    End If
                Else
                    Return True
                End If

            End If
        Else
            Return True
        End If

    End Function

    Private Function BackupPrompt(ByVal DaysSinceBackup As Integer) As Boolean

        Dim _Mess As String = ""

        Select Case DaysSinceBackup

            Case 0
                _Mess = "This application has never been backed up. Do you wish to backup now?"

            Case 1
                _Mess = "This application has not been backed up since yesterday. Do you wish to backup now?"

            Case Else
                _Mess = "This application was backed up " + DaysSinceBackup.ToString + " days ago. Do you wish to backup now?"

        End Select

        If CareMessage(_Mess, MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Backup Reminder") = DialogResult.Yes Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Sub BackupForm()

        Dim _frm As New frmBackup
        FormHandler.ShowMDIForm(_frm, "Backup", True)
        _frm = Nothing

    End Sub

    Private Sub DisplayMessages()

        Select Case m_AutoTheme

            Case "Halloween"
                CareMessage("Happy Halloween from all at Care Software!", MessageBoxIcon.Information)

            Case "Valentine"
                CareMessage("Happy Valentines Day from all at Care Software x", MessageBoxIcon.Information)

        End Select

    End Sub

    Private Function CheckVersionAgainstDB() As Boolean

        If Debugger.IsAttached Then Return True

        Dim _Return As Boolean = True

        If ParameterHandler.CheckExists("LASTVERSION") Then

            Dim _DBVersion As String = ParameterHandler.ReturnString("LASTVERSION")

            If _DBVersion = "" Then
                ParameterHandler.SetString("LASTVERSION", My.Application.Info.Version.ToString)
            Else

                Dim _DBVer As New Version(_DBVersion)
                Dim _SoftwareVer As Version = My.Application.Info.Version

                If _SoftwareVer < _DBVer Then

                    _Return = False

                    Dim _Mess As String = ""
                    _Mess += "Your software version does not match the Database."
                    _Mess += vbCrLf
                    _Mess += "Please contact technical support on 01792 685568 to resolve this for you."
                    _Mess += vbCrLf + vbCrLf
                    _Mess += "Please click OK to Exit."

                    CareMessage(_Mess, MessageBoxIcon.Error, "Version Check")

                Else
                    If _SoftwareVer > _DBVer Then
                        ParameterHandler.SetString("LASTVERSION", My.Application.Info.Version.ToString)
                    End If
                End If

            End If

        Else
            ParameterHandler.SetString("LASTVERSION", My.Application.Info.Version.ToString)
        End If

        Return _Return

    End Function

    Private Sub LoadUserPreferences()

        If UserPreferenceHandler.CheckExists("CHANGECOLOUR") Then
            Session.ChangeColour = Drawing.ColorTranslator.FromHtml(UserPreferenceHandler.ReturnString("CHANGECOLOUR"))
        Else
            UserPreferenceHandler.Create("CHANGECOLOUR", UserPreferenceHandler.EnumUserPreferenceType.StringType, "Yellow")
            Session.ChangeColour = Color.Yellow
        End If

        If UserPreferenceHandler.CheckExists("FINDCOLOUR") Then
            Session.FindColour = Drawing.ColorTranslator.FromHtml(UserPreferenceHandler.ReturnString("FINDCOLOUR"))
        Else
            UserPreferenceHandler.Create("FINDCOLOUR", UserPreferenceHandler.EnumUserPreferenceType.StringType, "LightGreen")
            Session.FindColour = Color.LightGreen
        End If

        If UserPreferenceHandler.CheckExists("THEME") Then
            m_ActiveTheme = UserPreferenceHandler.ReturnString("THEME")
        Else
            UserPreferenceHandler.Create("THEME", UserPreferenceHandler.EnumUserPreferenceType.StringType, "DevExpress Style")
            m_ActiveTheme = "DevExpress Style"
        End If

    End Sub

    Private Sub SaveUserPreferences()
        If m_AutoTheme = "" Then
            UserPreferenceHandler.SetString("THEME", cbxThemes.EditValue.ToString)
        End If
    End Sub

    Private Sub txtSearch_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged

        Dim _Expand As Boolean = False
        If txtSearch.Text <> "" Then _Expand = True

        For Each grp As NavBarGroup In navMenu.Groups
            grp.Expanded = _Expand
        Next

        Dim _SearchText As String = txtSearch.Text.ToLower
        For Each item As NavBarItem In navMenu.Items
            item.Visible = item.Caption.ToLower().Contains(_SearchText)
        Next

        If txtSearch.Text = "" Then navMenu.Groups(0).Expanded = True

    End Sub

    Private Sub mnuImportExport_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuImportExport.ItemClick

        Dim _frm As New frmImportExport
        _frm.ShowDialog()

        _frm.Dispose()
        _frm = Nothing

    End Sub

    Private Sub mnuReportManager_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuReportManager.ItemClick
        Dim _frm As New frmReportManager
        FormHandler.ShowMDIForm(_frm, "Report Manager", False)
        _frm = Nothing
    End Sub

    Private Sub mnuTaskScheduler_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuTaskScheduler.ItemClick
        Dim _frm As New frmTasks
        FormHandler.ShowMDIForm(_frm, "Task Scheduler", False)
    End Sub

    Private Sub navMenu_GroupExpanding(sender As Object, e As NavBarGroupCancelEventArgs) Handles navMenu.GroupExpanding
        For Each _g As NavBarGroup In navMenu.Groups
            If _g.Expanded Then _g.Expanded = False
        Next
    End Sub

    Private Sub mnuChatNow_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuChatNow.ItemClick
        Dim _url As String = ParameterHandler.ReturnString("CHATURL")
        If _url = "" Then _url = "https://tawk.to/chat/590e0544af0a926ea7bcd238/page/"
        webChat.Navigate(_url)
        dpRight.Show()
    End Sub

    Private Sub bsiCTIEnabled_ItemDoubleClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bsiCTIEnabled.ItemDoubleClick
        CTIShowCallLog()
    End Sub

    Private Sub bbiCallLog_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiCallLog.ItemClick
        CTIShowCallLog()
    End Sub

    Private Sub SetupCaptions()

        For Each _c In Business.AppControlCaption.RetreiveAll

            Dim _dc As New Dictionary.ControlCaption
            _dc.FormName = _c._FormName
            _dc.ControlName = _c._ControlName
            _dc.ControlCaption = _c._Caption

            Session.DictionaryControlCaptions.Add(_dc)

        Next

    End Sub

    Private Sub bbiCheckUpdates_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiUpdates.ItemClick
        Dim _U As New Updater
        _U.CustomerID = Session.CustomerID
        _U.CheckInteractive()
        _U = Nothing
    End Sub

#Region "Background Workers"

    Private Sub bgwSpellChecker_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgwSpellChecker.DoWork
        SetSpellChecking()
    End Sub

    Private Sub bgwCheckIn_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgwCheckIn.DoWork

        If Session.CustomerID = "" Then Exit Sub

        'check for updates...
        '    Dim _U As New Updater
        '    _U.CustomerID = Session.CustomerID
        '    _U.CheckSilent()
        '    _U = Nothing

        LicenseManager.CheckInOnline()

    End Sub

    Private Sub bgwBrowsers_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgwBrowsers.DoWork
        Try
            Dim _WBF As New WebBrowserFix
            _WBF.Run()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub bgwCache_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgwCache.DoWork
        ListHandler.CacheLists()
        SetupCaptions()
        SetupControlCache()
    End Sub

    Private Sub bgwSpellChecker_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgwSpellChecker.RunWorkerCompleted
        SetProgressMessage("Dictionary setup completed", 3)
    End Sub

    Private Sub bgwCheckIn_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgwCheckIn.RunWorkerCompleted
        SetProgressMessage("Check for Updates completed.", 3)
    End Sub

    Private Sub bgwCache_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgwCache.RunWorkerCompleted
        SetProgressMessage("Cache objects completed.", 3)
    End Sub

    Private Sub bgwBrowsers_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgwBrowsers.RunWorkerCompleted
        SetProgressMessage("Browser customisation completed.", 3)
    End Sub

    Private Sub bgwCTI_DoWork(sender As Object, e As DoWorkEventArgs) Handles bgwCTI.DoWork
        SetupCTI()
    End Sub

    Private Sub bgwCTI_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgwCTI.RunWorkerCompleted
        SetProgressMessage("CTI setup completed.", 3)
    End Sub

#End Region

    Private Shared Sub SetupControlCache()

        Dim _ACSList As List(Of Business.AppControlSecurity) = Business.AppControlSecurity.RetreiveAll
        For Each _acs In _ACSList

            Dim _s As New Dictionary.ControlSecurity

            _s.FormName = _acs._FormName
            _s.ControlName = _acs._ControlName

            If _acs._ControlType = "Frame" Then _s.ControlType = [Global].Dictionary.ControlSecurity.EnumControlType.Frame
            If _acs._ControlType = "Tab" Then _s.ControlType = [Global].Dictionary.ControlSecurity.EnumControlType.Tab

            _s.ControlTabName = _acs._ControlTabName

            If _acs._ControlSecurityType = "User" Then _s.ControlSecurityType = [Global].Dictionary.ControlSecurity.EnumSecurityType.User
            If _acs._ControlSecurityType = "Group" Then _s.ControlSecurityType = [Global].Dictionary.ControlSecurity.EnumSecurityType.Group

            _s.ControlSecurityID = _acs._ControlSecurityId
            _s.ControlSecurityName = _acs._ControlSecurityName

            Session.DictionaryControlSecurity.Add(_s)

        Next

    End Sub

    Private Sub ShowWaitForm(ByVal Caption As String, ByVal Description As String)
        If m_DisableWaitForms Then Exit Sub
        Try
            If Not SplashScreenManager1.IsSplashFormVisible Then
                SplashScreenManager1.ShowWaitForm()
                SplashScreenManager1.SetWaitFormCaption(Caption)
                SplashScreenManager1.SetWaitFormDescription(Description)
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub CloseWaitForm()
        If m_DisableWaitForms Then Exit Sub
        Try
            If SplashScreenManager1.IsSplashFormVisible Then
                SplashScreenManager1.CloseWaitForm()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub mnuDBDiff_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuDBDiff.ItemClick
        Dim _frm As New frmDBCompare
        FormHandler.ShowMDIForm(_frm, "Database Compare", False)
    End Sub

    Private Sub mnuDBMaintenance_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuDBMaintenance.ItemClick
        Dim _frm As New frmDBMaintenance
        FormHandler.ShowMDIForm(_frm, "Database Maintenance", False)
    End Sub

    Private Sub mnuErrors_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuErrors.ItemClick
        Dim _frm As New frmErrors
        FormHandler.ShowMDIForm(_frm, "Errors", False)
    End Sub
End Class
