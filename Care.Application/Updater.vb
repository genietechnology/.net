﻿Imports System.IO
Imports Care.Global

Public Class Updater

    Property CustomerID As String

    Private m_EndpointID As String = ""

    Public Sub CheckSilent()
        If SetEndpointID Then
            CheckForUpdates(False)
        End If
    End Sub

    Public Sub CheckInteractive()
        If SetEndpointID Then
            CheckForUpdates(True)
        Else
            CareMessage("Could not connect to Update server.", MessageBoxIcon.Exclamation)
        End If
    End Sub

    Private Function SetEndpointID() As Boolean

        Dim _Return As Boolean = False

        Try

            Dim _WS As New CareSoftware.API.Update.UpdateClient
            Dim _Response As String = _WS.RegisterEndpoint(Me.CustomerID, My.Computer.Name)

            If _Response.StartsWith("-") Then
                'customerID not found
            Else
                m_EndpointID = _Response
                _Return = True
            End If

            _WS = Nothing

        Catch ex As Exception
            'could not connect to endpoint

        End Try

        Return _Return

    End Function

    Private Sub CheckForUpdates(ByVal Interactive As Boolean)

        Dim _Path As String = ""
        Dim _DownloadUpdater As Boolean = False

        If UpdaterInstalled(_Path) Then
            KillProcess("UpdateClient")
            If IsVersion1(_Path) Then _DownloadUpdater = True
        Else
            _DownloadUpdater = True
        End If

        If _DownloadUpdater Then
            If Not DownloadAndUnPack(_Path) Then
                'updater download failed
                Exit Sub
            End If
        End If

        Dim _ImmediateOnly As Boolean = True

        'if we are running interactively, we would pick up all upgrades, not just immediate ones
        If Interactive Then _ImmediateOnly = False

        If UpdatesAvailable(_ImmediateOnly) Then

            If Interactive Then
                If CareMessage("Updates are available." + vbCrLf + vbCrLf + "Do you wish to exit and install now?", MessageBoxIcon.Question, MessageBoxButtons.YesNo, "Launch Updater") = DialogResult.No Then
                    Exit Sub
                End If
            End If

            LaunchUpdater(_Path)

        Else
            If Interactive Then CareMessage("There are no updates available at the moment.", MessageBoxIcon.Information, "Check for updates")
        End If


    End Sub

    Private Sub LaunchUpdater(ByVal Path As String)

        Dim _Args As String = ""

        _Args += "AUTO"
        _Args += " "
        _Args += Session.CustomerID
        _Args += " "
        _Args += m_EndpointID
        _Args += " "
        _Args += """" + Application.ExecutablePath + """"
        _Args += " "
        _Args += Session.CurrentUser.UserCode
        _Args += " "
        _Args += BuildArgs()

        Dim _p As New System.Diagnostics.Process
        _p = Process.Start(Path, _Args)

        Application.Exit()

    End Sub

    Private Function BuildArgs() As String
        Dim _Return As String = ""
        _Return += Session.ApplicationCode + " " + """" + Session.ApplicationName + """"
        Return _Return
    End Function

    Private Function UpdaterInstalled(ByRef Path As String) As Boolean

        If Environment.Is64BitOperatingSystem Then
            Path = "C:\Program Files (x86)\Care Software\Update Client\UpdateClient.exe"
        Else
            Path = "C:\Program Files\Care Software\Update Client\UpdateClient.exe"
        End If

        If IO.File.Exists(Path) Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Function DownloadAndUnPack(ByVal Path As String) As Boolean

        Dim _Return As Boolean = False
        Dim _UpdatePath As String = Environment.CurrentDirectory + "\updates"
        Dim _PathToZip As String = _UpdatePath + "\UpdateClient.zip"

        'check pack location exists
        If Not IO.Directory.Exists(_UpdatePath) Then IO.Directory.CreateDirectory(_UpdatePath)

        'download the updater and un-pack it
        If DownloadFile("http://api.caresoftware.co.uk/update/packs/installers/UpdateClient.zip", _PathToZip) Then
            If UnpackFile(_PathToZip, Path) Then
                _Return = True
            End If
        End If

        Return _Return

    End Function

    Private Function UnpackFile(ByVal ZipFile As String, ByVal ExtractPath As String) As Boolean

        'path contains the full path incl EXE for the UpdateClient, we just need the folder
        Dim _fi As New FileInfo(ExtractPath)
        Dim _dir As String = _fi.DirectoryName

        Try
            Dim _z As New Ionic.Zip.ZipFile(ZipFile)
            _z.ExtractAll(_dir, Ionic.Zip.ExtractExistingFileAction.OverwriteSilently)

        Catch ex As Exception
            Return False
        End Try

        Return True

    End Function

    Private Function DownloadFile(ByVal URL As String, ByVal Destination As String) As Boolean

        If IO.File.Exists(Destination) Then IO.File.Delete(Destination)

        Dim _Return As Boolean = True
        Dim _WebClient As New System.Net.WebClient

        Try
            _WebClient.DownloadFile(URL, Destination)

        Catch ex As Exception
            _Return = False
        End Try

        If IO.File.Exists(Destination) Then
            _Return = True
        Else
            _Return = False
        End If

        _WebClient.Dispose()
        _WebClient = Nothing

        Return _Return

    End Function

    Private Function IsVersion1(ByVal LocalFile As String) As Boolean
        If GetVersion(LocalFile) = New Version("1.0.0.0") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function GetVersion(ByVal FilePath As String) As Version

        'Typically, a version number is displayed' as "major number.minor number.build number.private part number". 
        'A file version number is a 64-bit number that holds the version number for a file as follows:
        'The first 16 bits are the FileMajorPart number.
        'The next 16 bits are the FileMinorPart number.
        'The third set of 16 bits are the FileBuildPart number.
        'The last 16 bits are the FilePrivatePart number.

        'http://msdn.microsoft.com/en-us/library/91sft6af(v=vs.71).aspx

        Dim _Info As FileVersionInfo = FileVersionInfo.GetVersionInfo(FilePath)
        Dim _Version As New Version(_Info.FileVersion)

        _Info = Nothing

        Return _Version

    End Function

    Private Function KillProcess(ByVal ProcessName As String) As Boolean
        For Each _P As Process In Process.GetProcessesByName(ProcessName)
            _P.Kill()
        Next
        Return True
    End Function

    Private Function UpdatesAvailable(ByVal ImmediateOnly As Boolean) As Boolean

        Dim _Return As Boolean = False
        Dim _Jobs As List(Of CareSoftware.API.Update.Job) = Nothing

        Try
            Dim _WS As New CareSoftware.API.Update.UpdateClient()
            _Jobs = _WS.CheckForUpdates(m_EndpointID, ImmediateOnly).ToList

        Catch ex As Exception

        End Try

        If _Jobs IsNot Nothing AndAlso _Jobs.Count > 0 Then
            _Return = True
        End If

        Return _Return

    End Function

End Class
