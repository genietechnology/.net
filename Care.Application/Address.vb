﻿Imports Care.Global

Public Class Address

    Private m_Premises As String
    Private m_Street As String

    Public Property Premises As String
        Get
            Return m_Premises
        End Get
        Set(value As String)
            m_Premises = value
        End Set
    End Property

    Public Property Street As String
        Get
            Return m_Street
        End Get
        Set(value As String)
            m_Street = value
        End Set
    End Property

End Class
