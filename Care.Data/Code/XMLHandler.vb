﻿Imports System.IO
Imports System.Xml.Serialization

Public Class XMLHandler

    Public Shared Sub Serialise(ByVal XMLPath As String, ByVal ClassIn As Object)

        Dim _sw As New StreamWriter(XMLPath)
        Dim _x As New XmlSerializer(ClassIn.GetType)
        _x.Serialize(_sw, ClassIn)
        _sw.Close()

    End Sub

    Public Sub DeSerialise(Of DataObjectType)(ByVal XMLPath As String, ByRef MyDataObject As DataObjectType)

        Dim _sr As New StreamReader(XMLPath)
        Dim _x As New XmlSerializer(Me.GetType)
        MyDataObject = CType(_x.Deserialize(_sr), DataObjectType)
        _sr.Close()

    End Sub

End Class
