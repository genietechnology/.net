﻿Imports System.Reflection
Imports System.Data.SqlClient

Public Class DAL

#Region "Maintenance Tasks"

    Public Shared Function IndexReOrganise(ByVal ConnectionString As String, ByVal IndexName As String, ByVal TableName As String) As Boolean
        Return ExecuteSQL(ConnectionString, "ALTER INDEX " + IndexName + " ON " + TableName + " REORGANIZE")
    End Function

    Public Shared Function IndexReBuild(ByVal ConnectionString As String, ByVal IndexName As String, ByVal TableName As String) As Boolean
        Return ExecuteSQL(ConnectionString, "ALTER INDEX " + IndexName + " ON " + TableName + " REBUILD")
    End Function

#End Region

#Region "SQL Handlers"

    Private Shared Function ReturnCommand(ByVal ConnectionString As String, ByVal Type As CommandType, ByVal CommandText As String) As SqlCommand

        Dim _cmd As New SqlCommand
        With _cmd
            .Connection = ReturnConnection(ConnectionString)
            .CommandType = Type
            .CommandText = CommandText
            .CommandTimeout = 90
            .Parameters.Clear()
        End With

        Return _cmd

    End Function

    Public Shared Function GetDataReaderfromSQL(ByVal ConnectionString As String, ByVal SQLStatement As String) As SqlDataReader

        If SQLStatement = "" Then Return Nothing

        Dim _dr As SqlDataReader = Nothing
        Dim _cmd As SqlCommand = ReturnCommand(ConnectionString, CommandType.Text, SQLStatement)

        Try
            _dr = _cmd.ExecuteReader

        Catch ex As SqlException

        End Try

        Return _dr

    End Function

    Public Shared Function ReturnConnection(ByVal ConnectionString As String, Optional ByVal Interactive As Boolean = True) As SqlConnection

        Dim _cn As New SqlConnection
        _cn.ConnectionString = ConnectionString

        Try
            _cn.Open()

        Catch ex As Exception
            If Interactive Then
                Msgbox(ConnectionString + vbCrLf + ex.Message, MsgBoxStyle.Critical, "DAL.ReturnConnection")
            End If
            Return Nothing
        End Try

        Return _cn

    End Function

    Public Shared Function ReturnTextCommand(ByVal ConnectionString As String, ByVal CommandText As String) As SqlCommand

        Dim _cmd As New SqlCommand
        With _cmd
            .Connection = ReturnConnection(ConnectionString)
            .CommandType = CommandType.Text
            .CommandText = CommandText
            .CommandTimeout = 90
            .Parameters.Clear()
        End With

        Return _cmd

    End Function

    Private Shared Function ReturnSPCommand(ByVal ConnectionString As String, ByVal ProcedureName As String) As SqlCommand

        Dim _cmd As New SqlCommand
        With _cmd
            .Connection = ReturnConnection(ConnectionString)
            .CommandType = CommandType.StoredProcedure
            .CommandText = ProcedureName
            .CommandTimeout = 90
            .Parameters.Clear()
        End With

        Return _cmd

    End Function

    Private Shared Function ReturnSPCommand(ByVal ConnectionString As String, ByVal ProcedureName As String, ByVal SPList As List(Of SPParam)) As SqlCommand

        Dim _cmd As New SqlCommand
        With _cmd
            .Connection = ReturnConnection(ConnectionString)
            .CommandType = CommandType.StoredProcedure
            .CommandText = ProcedureName
            .CommandTimeout = 90
            .Parameters.Clear()
        End With

        If SPList IsNot Nothing Then

            For Each Param In SPList

                Dim _prm As New SqlParameter
                With _prm
                    .Direction = ParameterDirection.Input
                    .ParameterName = Param.ParameterName
                    .SqlDbType = Param.ParameterType
                    .Value = Param.ParameterValue
                End With
                _cmd.Parameters.Add(_prm)
            Next

        End If

        Return _cmd

    End Function

    Private Shared Function ReturnSPCommand(ByVal ConnectionString As String, ByVal ProcedureName As String, ByVal SPParam As SPParam) As SqlCommand

        Dim _cmd As New SqlCommand
        With _cmd
            .Connection = ReturnConnection(ConnectionString)
            .CommandType = CommandType.StoredProcedure
            .CommandText = ProcedureName
            .CommandTimeout = 90
            .Parameters.Clear()
        End With

        If SPParam IsNot Nothing Then
            Dim _prm As New SqlParameter
            With _prm
                .Direction = ParameterDirection.Input
                .ParameterName = SPParam.ParameterName
                .SqlDbType = SPParam.ParameterType
                .Value = SPParam.ParameterValue
            End With
            _cmd.Parameters.Add(_prm)
        End If

        Return _cmd

    End Function

    Private Shared Function ReturnDataTable(ByRef CommandObject As SqlCommand) As DataTable

        Dim _Return As DataTable = Nothing
        Dim _DT As New DataTable

        Dim _DA As New SqlDataAdapter(CommandObject)

        Try

            _DA.Fill(_DT)
            _Return = _DT

            _DT.Dispose()
            _DT = Nothing

        Catch ex As SqlException
            Msgbox(ex.Message, MsgBoxStyle.Critical, "DAL.ReturnDataTable")

        Finally
            DisposeofCommand(CommandObject)

        End Try

        Return _Return

    End Function

    Private Shared Function ReturnDataRow(ByRef CommandObject As SqlCommand) As DataRow

        Dim _Return As DataRow = Nothing
        Dim _DT As DataTable = ReturnDataTable(CommandObject)
        If _DT IsNot Nothing Then

            _Return = GetRowFromTable(_DT)

            _DT.Dispose()
            _DT = Nothing

        End If

        Return _Return

    End Function

    Public Shared Function ReturnDataReaderfromSQL(ByVal ConnectionString As String, ByVal SQLStatement As String) As SqlDataReader

        If SQLStatement = "" Then Return Nothing

        Dim _dr As SqlDataReader = Nothing
        Dim _cmd As SqlCommand = ReturnTextCommand(ConnectionString, SQLStatement)

        _dr = _cmd.ExecuteReader(CommandBehavior.CloseConnection)

        Return _dr

    End Function

    Public Shared Function ReturnScalar(ByVal ConnectionString As String, ByVal SQLStatement As String) As Object

        Dim _Return As Object = Nothing
        If SQLStatement = "" Then Return Nothing

        Dim _cmd As SqlCommand = ReturnTextCommand(ConnectionString, SQLStatement)
        _Return = _cmd.ExecuteScalar
        DisposeofCommand(_cmd)

        Return _Return

    End Function

    Public Shared Function ReturnInputParameter(ByVal ParameterName As String, ByVal DataType As SqlDbType, ByVal ParameterValue As Object) As SqlParameter

        Dim _Prm As New SqlParameter
        With _Prm
            .ParameterName = ParameterName
            .Direction = ParameterDirection.Input
            .Value = ParameterValue
        End With

        Return _Prm

    End Function

    Public Shared Function CreateParameter(ByVal ParamName As String,
                           ByVal ParamType As System.Data.SqlDbType,
                           ByVal ParamValue As Object
                           ) As SPParam

        Dim prm As New SPParam(ParamName, ParamType, ParamValue)
        Return prm

    End Function

#End Region

#Region "Row Getters"

    Public Shared Function GetRowbyID(ConnectionString As String, ByVal ProcedureName As String, ByVal ID As Guid?) As DataRow

        Dim _Param As New SPParam("@ID", SqlDbType.UniqueIdentifier, ID)
        Dim _cmd As SqlCommand = ReturnSPCommand(ConnectionString, ProcedureName, _Param)

        Dim _DR As DataRow = ReturnDataRow(_cmd)

        DisposeofCommand(_cmd)

        Return _DR

    End Function

    Public Shared Function GetRowfromSQL(ConnectionString As String, ByVal SQLStatement As String) As DataRow

        Dim _cmd As SqlCommand = ReturnTextCommand(ConnectionString, SQLStatement)
        Dim _dr As DataRow = ReturnDataRow(_cmd)

        DisposeofCommand(_cmd)
        Return _dr

    End Function

    Public Shared Function GetRowfromSP(ConnectionString As String, ByVal ProcedureName As String, Optional ByVal Params As List(Of SPParam) = Nothing) As DataRow

        Dim _cmd As SqlCommand = ReturnSPCommand(ConnectionString, ProcedureName, Params)
        Dim _dr As DataRow = ReturnDataRow(_cmd)

        DisposeofCommand(_cmd)
        Return _dr

    End Function

    Public Shared Function GetRowfromSPwithParam(ConnectionString As String, ByVal ProcedureName As String, ByVal ParameterName As String, ByVal ParameterType As SqlDbType, ByVal ParameterValue As Object) As DataRow

        Dim _ParamName As String = ParameterName
        Dim _Param As New SPParam(ParameterName, ParameterType, ParameterValue)
        Dim _cmd As SqlCommand = ReturnSPCommand(ConnectionString, ProcedureName, _Param)

        Dim _dr As DataRow = ReturnDataRow(_cmd)

        _ParamName = Nothing
        DisposeofCommand(_cmd)

        Return _dr

    End Function

#End Region

#Region "Table Getters"

    Public Shared Function GetDataTablefromSP(ConnectionString As String, ByVal ProcedureName As String, Optional ByVal Params As List(Of SPParam) = Nothing) As DataTable

        Dim _cmd As SqlCommand = ReturnSPCommand(ConnectionString, ProcedureName, Params)
        Dim _dt As DataTable = ReturnDataTable(_cmd)

        DisposeofCommand(_cmd)
        Return _dt

    End Function

    Public Shared Function GetDataTablefromSPwithParam(ConnectionString As String, ByVal ProcedureName As String, ByVal ParameterName As String, ByVal ParameterType As SqlDbType, ByVal ParameterValue As Object) As DataTable

        Dim _ParamName As String = ParameterName
        Dim _Param As New SPParam(ParameterName, ParameterType, ParameterValue)
        Dim _cmd As SqlCommand = ReturnSPCommand(ConnectionString, ProcedureName, _Param)

        Dim _dt As DataTable = ReturnDataTable(_cmd)

        DisposeofCommand(_cmd)
        Return _dt

    End Function

    Public Shared Function GetDataTablefromSQL(ByVal ConnectionString As String, ByVal SQLStatement As String) As DataTable

        Dim _cmd As SqlCommand = ReturnTextCommand(ConnectionString, SQLStatement)
        Dim _dt As DataTable = ReturnDataTable(_cmd)

        DisposeofCommand(_cmd)
        Return _dt

    End Function

#End Region

#Region "Updates"

    Public Shared Sub DeleteRecordbyID(Of T As IBusinessEntity)(ByVal ConnectionString As String, ByVal UserID As Guid, ByVal DBVersion As T, ByVal ProcedureName As String, ByVal ID As Guid)

        Dim _Param As New SPParam("@ID", SqlDbType.UniqueIdentifier, ID)
        Dim _cmd As SqlCommand = ReturnSPCommand(ConnectionString, ProcedureName, _Param)

        _cmd.ExecuteNonQuery()

        DisposeofCommand(_cmd)

    End Sub

    Public Shared Sub DeleteRecordbyID(ByVal ConnectionString As String, ByVal ProcedureName As String, ByVal ID As Guid)

        Dim _Param As New SPParam("@ID", SqlDbType.UniqueIdentifier, ID)
        Dim _cmd As SqlCommand = ReturnSPCommand(ConnectionString, ProcedureName, _Param)

        _cmd.ExecuteNonQuery()

        DisposeofCommand(_cmd)

    End Sub

    Private Shared Function ObjectToString(ByVal ObjectIn As Object) As String
        If ObjectIn Is Nothing Then
            Return ""
        Else
            Return ObjectIn.ToString
        End If
    End Function

    Public Shared Sub SaveRecord(Of T As IBusinessEntity)(ByVal ConnectionManager As Care.Connect.ConnectionManagerObject, ByVal UserID As Guid, ByVal BusinessObject As T, ByVal DBVersion As T, ByVal ProcedureName As String)

        'assume we have a change
        Dim _AuditTrail As New List(Of AuditTrailRecord)
        Dim _ID As String = ""
        Dim _NewRecord As Boolean = False
        Dim _AuditResult As Integer = 1

        If ConnectionManager.AuditEnabled AndAlso Not ConnectionManager.AuditTemporaryHalt Then
            _AuditResult = CheckforChanges(_AuditTrail, _ID, _NewRecord, BusinessObject, DBVersion)
        End If

        'audit result will be negative (if an error occurred)
        'or positive (count of changes)
        'we want to store if there was an issue computing the audit trail...
        If _AuditResult <> 0 Then

            Dim _cmd As SqlCommand = ReturnSPCommand(ConnectionManager.ConnectionString, ProcedureName)

            SqlCommandBuilder.DeriveParameters(_cmd)
            FillParameters(BusinessObject, _cmd)

            _cmd.ExecuteNonQuery()

            DisposeofCommand(_cmd)

            If ConnectionManager.AuditEnabled AndAlso Not ConnectionManager.AuditTemporaryHalt Then
                Dim _Operation As String = "Edit Record"
                If _NewRecord Then _Operation = "Create Record"
                WriteAuditTrail(ConnectionManager, _Operation, BusinessObject.ToString, UserID, _ID, ProcedureName, _AuditTrail)
            End If

        Else
            'nothing has changed
        End If

    End Sub

    Private Shared Function CheckforChanges(Of T As IBusinessEntity)(ByRef AuditTrail As List(Of AuditTrailRecord),
                                                                     ByRef RecordID As String, ByRef NewRecord As Boolean,
                                                                     ByVal BusinessObject As T, ByVal DBVersion As T) As Integer
        'assume no changes
        Dim _ReturnValue As Integer = 0

        If BusinessObject IsNot Nothing Then

            Dim _ChangedType As Type = BusinessObject.GetType

            Try

                'if there is no DB version, we must be adding a record
                If DBVersion Is Nothing Then
                    NewRecord = True
                    For Each _p As PropertyInfo In _ChangedType.GetProperties
                        'our properties always start with underscore
                        If _p.Name.StartsWith("_") Then
                            Dim _ChangedValue As String = ObjectToString(_p.GetValue(BusinessObject, Nothing))
                            If _p.Name.ToUpper = "_ID" Then RecordID = _ChangedValue
                            AuditTrail.Add(New AuditTrailRecord(_p.Name, "", _ChangedValue))
                            _ReturnValue += 1
                        End If
                    Next
                Else

                    NewRecord = False
                    Dim _DBType As Type = DBVersion.GetType
                    Dim _ChangedProperties() As PropertyInfo = _ChangedType.GetProperties
                    Dim _DBProperties() As PropertyInfo = _DBType.GetProperties

                    Dim _i As Integer = 0
                    While _i <= _ChangedProperties.Count - 1

                        Dim _PropertyName As String = _DBProperties.ElementAt(_i).Name
                        Dim _PropertyType As String = _DBProperties.ElementAt(_i).PropertyType.ToString
                        Dim _DBValue As String = ObjectToString(_DBProperties.ElementAt(_i).GetValue(DBVersion, Nothing))
                        Dim _ChangedValue As String = ObjectToString(_ChangedProperties.ElementAt(_i).GetValue(BusinessObject, Nothing))

                        If _PropertyName.ToUpper = "_ID" Then RecordID = _ChangedValue
                        If _PropertyType = "System.Decimal" Then

                            Dim _ChangedDec As Decimal? = Decimal.Parse(_ChangedValue)
                            Dim _DBDec As Decimal? = Decimal.Parse(_DBValue)

                            If _ChangedDec <> _DBDec Then
                                AuditTrail.Add(New AuditTrailRecord(_PropertyName, _DBValue, _ChangedValue))
                                _ReturnValue += 1
                            End If

                        Else
                            If _DBValue <> _ChangedValue Then
                                AuditTrail.Add(New AuditTrailRecord(_PropertyName, _DBValue, _ChangedValue))
                                _ReturnValue += 1
                            End If
                        End If

                        _i += 1

                    End While

                End If

            Catch ex As Exception
                _ReturnValue = -2
            End Try
        Else
            _ReturnValue = -1
        End If

        Return _ReturnValue

    End Function

    Private Shared Sub WriteAuditTrail(ByVal ConnectionManager As Care.Connect.ConnectionManagerObject, ByVal Operation As String, ByVal BusinessObjectName As String, ByVal UserID As Guid, ByVal RecordID As String,
                                       ByVal ProcName As String, ByVal AuditTrail As List(Of AuditTrailRecord))

        Try

            Dim _SQL As String = ""
            _SQL += "INSERT INTO AppAuditData (object_operation, object_name, property_name, proc_name, record_id, value_old, value_new, user_id, computer_name, login_name, stamp)" + vbCrLf
            _SQL += "VALUES" + vbCrLf

            Dim _Line As String = ""
            For Each _t As AuditTrailRecord In AuditTrail

                If _Line = "" Then
                    _Line = "("
                Else
                    _Line = ",("
                End If

                _Line += "'" + Operation + "',"
                _Line += "'" + BusinessObjectName + "',"
                _Line += "'" + _t.PropertyName + "',"
                _Line += "'" + ProcName + "',"
                _Line += "'" + RecordID + "',"
                _Line += "'" + _t.DBValue + "',"
                _Line += "'" + _t.NewValue + "',"
                _Line += "'" + UserID.ToString + "',"
                _Line += "'" + My.Computer.Name + "',"
                _Line += "'" + Environment.UserDomainName + "\" + Environment.UserName + "',"
                _Line += "'" + Format(Now, "yyyy-MM-dd HH:mm:ss") + "'"
                _Line += ")"

                _SQL += _Line + vbCrLf

            Next

            Dim _cmd As New SqlCommand
            With _cmd
                .Connection = ReturnConnection(ConnectionManager.ConnectionString)
                .CommandType = CommandType.Text
                .CommandText = _SQL
                .CommandTimeout = 3
                .ExecuteNonQuery()
            End With

        Catch ex As Exception

        End Try

    End Sub

    Public Shared Sub SaveRecord(Of T As IBusinessEntity)(ByVal ConnectionManager As Care.Connect.ConnectionManagerObject, ByVal BusinessObject As T, ByVal ProcedureName As String)

        Dim _cmd As SqlCommand = ReturnSPCommand(ConnectionManager.ConnectionString, ProcedureName)

        SqlCommandBuilder.DeriveParameters(_cmd)
        FillParameters(BusinessObject, _cmd)

        _cmd.ExecuteNonQuery()

        DisposeofCommand(_cmd)

    End Sub

    Public Shared Sub SaveRecord(Of T As IBusinessEntity)(ByVal ConnectionString As String, ByVal BusinessObject As T, ByVal ProcedureName As String)

        Dim _cmd As SqlCommand = ReturnSPCommand(ConnectionString, ProcedureName)

        SqlCommandBuilder.DeriveParameters(_cmd)
        FillParameters(BusinessObject, _cmd)

        _cmd.ExecuteNonQuery()

        DisposeofCommand(_cmd)

    End Sub

    Public Shared Function ExecuteSQL(ByVal ConnectionString As String, ByVal SQLStatement As String) As Boolean

        Dim _Return As Boolean = True
        Dim _cmd As SqlCommand = ReturnTextCommand(ConnectionString, SQLStatement)

        Try
            _cmd.ExecuteNonQuery()

        Catch ex As Exception
            _Return = False
        End Try

        DisposeofCommand(_cmd)
        Return _Return

    End Function

    Public Shared Sub ExecuteSP(ByVal ConnectionString As String, ByVal ProcedureName As String, Optional ByVal ParameterCollection As List(Of SPParam) = Nothing)

        Dim _cmd As SqlCommand = ReturnSPCommand(ConnectionString, ProcedureName, ParameterCollection)
        _cmd.ExecuteNonQuery()

        DisposeofCommand(_cmd)

    End Sub

    Public Shared Sub ExecuteSPwithParam(ByVal ConnectionString As String, ByVal ProcedureName As String, ByVal ParameterName As String, ByVal ParameterType As SqlDbType, ByVal ParameterValue As Object)

        Dim _cmd As SqlCommand = ReturnCommand(ConnectionString, CommandType.StoredProcedure, ProcedureName)

        Dim _Prm As New SqlParameter
        With _Prm
            .ParameterName = ParameterName
            .Direction = ParameterDirection.Input
            .SqlDbType = ParameterType
            .Value = ParameterValue
        End With

        _cmd.Parameters.Add(_Prm)

        _cmd.ExecuteNonQuery()

        DisposeofCommand(_cmd)

    End Sub

#End Region

#Region "Bulk Copy"

    Public Shared Function BulkCopy(ByVal ConnectionString As String, ByVal TableName As String, ByRef DataTableIn As DataTable) As Boolean

        Dim _Return As Boolean = True
        Using Conn As SqlConnection = New SqlConnection(ConnectionString)

            Conn.Open()
            Using s As SqlBulkCopy = New SqlBulkCopy(Conn)

                s.DestinationTableName = TableName
                s.BulkCopyTimeout = 180

                Try
                    s.WriteToServer(DataTableIn)

                Catch ex As Exception
                    _Return = False
                End Try

            End Using

        End Using

        Return _Return

    End Function

#End Region

    Public Shared Function ReturnField(ByVal ConnectionString As String, ByVal TableName As String, ByVal WhereColumn As String,
                                       ByVal WhereValue As String, ByVal ReturnColumn As String, ByRef ReturnValue As String) As Boolean

        Dim _SQL As String = "Select " + ReturnColumn + " from " + TableName + " where " + WhereColumn + " = '" + WhereValue + "'"
        Dim _DR As DataRow = DAL.GetRowfromSQL(ConnectionString, _SQL)
        Dim _Return As Boolean = False

        If _DR IsNot Nothing Then
            Try
                ReturnValue = _DR.Item(0).ToString
                _DR = Nothing
                _Return = True
            Catch ex As Exception

            End Try
        End If

        Return _Return

    End Function

    Public Shared Sub DisposeofCommand(ByRef cmd As SqlCommand)

        If cmd Is Nothing Then Exit Sub

        Try

            With cmd
                .Parameters.Clear()
                .Connection.Close()
                .Connection.Dispose()
                .Dispose()
            End With

            cmd = Nothing

        Catch ex As Exception

        End Try

    End Sub

#Region "Private Helpers"

    Private Shared Function GetRowFromTable(ByVal Table As DataTable) As DataRow

        If Table Is Nothing OrElse Table.Rows.Count = 0 Then
            Return Nothing
        Else
            Return Table.Rows(0)
        End If

    End Function

    Private Shared Sub FillParameters(Of T As IBusinessEntity)(ByVal BusinessObject As T, ByRef CurrentCommand As SqlCommand)

        Dim inputType As Type = BusinessObject.GetType
        Dim typeProperties() As PropertyInfo = inputType.GetProperties
        Dim storedProcAttribute As StoredProcParameterAttribute
        Dim attributes() As Attribute

        For Each parameter As SqlParameter In CurrentCommand.Parameters

            If parameter.ParameterName.ToUpper = "@RETURN_VALUE" Then Continue For

            For Each propInfo As PropertyInfo In typeProperties
                attributes = CType(propInfo.GetCustomAttributes(GetType(StoredProcParameterAttribute), True), Attribute())
                If Not IsNothing(attributes) And attributes.Length = 1 Then
                    storedProcAttribute = CType(attributes(0), StoredProcParameterAttribute)
                    If Not IsNothing(storedProcAttribute) And storedProcAttribute.StoredProcParameter.Length > 0 Then
                        If storedProcAttribute.StoredProcParameter.ToLower = parameter.ParameterName.ToLower Then

                            Select Case parameter.SqlDbType

                                Case SqlDbType.UniqueIdentifier

                                    Dim _GUID As Guid? = CType(propInfo.GetValue(BusinessObject, Nothing), Guid?)
                                    If _GUID.HasValue Then
                                        If _GUID.Value <> Guid.Empty Then
                                            parameter.Value = _GUID
                                        Else
                                            parameter.Value = Nothing
                                        End If
                                    Else
                                        parameter.Value = Nothing
                                    End If

                                Case Else
                                    parameter.Value = propInfo.GetValue(BusinessObject, Nothing)

                            End Select

                            Exit For

                        End If
                    End If
                End If
            Next propInfo
        Next parameter

    End Sub

#End Region

End Class

Public Class ConnectionBuilder

#Region "Variables"

    Private _ApplicationName As String = ""
    Private _Server As String = ""
    Private _DB As String = ""
    Private _User As String = ""
    Private _Password As String = ""
    Private _Integrated As Boolean = False
    Private _ConnectionString As String = ""
    Private _ConnectionOK As Boolean = False

#End Region

#Region "Properties"

    Public Property ApplicationName As String
        Get
            Return _ApplicationName
        End Get
        Set(value As String)
            _ApplicationName = value
        End Set
    End Property

    Public Property Server() As String
        Get
            Return _Server
        End Get
        Set(ByVal value As String)
            _Server = value
        End Set
    End Property

    Public Property DB() As String
        Get
            Return _DB
        End Get
        Set(ByVal value As String)
            _DB = value
        End Set
    End Property

    Public Property User() As String
        Get
            Return _User
        End Get
        Set(ByVal value As String)
            _User = value
        End Set
    End Property

    Public Property Password() As String
        Get
            Return _Password
        End Get
        Set(ByVal value As String)
            _Password = value
        End Set
    End Property

    Public Property IntegratedSecurity() As Boolean
        Get
            Return _Integrated
        End Get
        Set(ByVal value As Boolean)
            _Integrated = value
        End Set
    End Property

    Public ReadOnly Property ConnectionString() As String
        Get
            BuildConnection()
            Return _ConnectionString
        End Get
    End Property

    Public ReadOnly Property ConnectionOK() As Boolean
        Get
            Return _ConnectionOK
        End Get
    End Property

#End Region

    Private Sub BuildConnection()

        _ConnectionString = ""

        _ConnectionString += "Data Source=" + _Server + ";"
        _ConnectionString += "Initial Catalog=" + _DB + ";"

        If _Integrated Then
            _ConnectionString += "Integrated Security=SSPI" + ";"
        Else
            _ConnectionString += "User Id=" + _User + ";"
            _ConnectionString += "Password=" + _Password + ";"
        End If

        _ConnectionString += "Application Name=" + _ApplicationName + ";"
        _ConnectionString += "Workstation ID=" + My.Computer.Name + ";"
        _ConnectionString += "Connect Timeout=90" + ";"

    End Sub

    Public Sub TestConnection()

        Dim _conn As SqlClient.SqlConnection = DAL.ReturnConnection(ConnectionString, False)
        If Not _conn Is Nothing Then
            _conn.Close()
            _conn.Dispose()
            _conn = Nothing
            _ConnectionOK = True
        Else
            _ConnectionOK = False
        End If

    End Sub

    Public Sub TestConnection(ByVal CustomConnectionString As String)

        Dim _conn As SqlClient.SqlConnection = DAL.ReturnConnection(CustomConnectionString, False)
        If Not _conn Is Nothing Then
            _conn.Close()
            _conn.Dispose()
            _conn = Nothing
            _ConnectionOK = True
        Else
            _ConnectionOK = False
        End If

    End Sub

End Class

Public Class SPParam

    Private _ParamName As String
    Private _ParamType As System.Data.SqlDbType
    Private _ParamValue As Object

    Public Sub New(ByVal ParameterName As String, ByVal ParameterType As System.Data.SqlDbType, ByVal ParameterValue As Object)

        'check if the @ symbol is included on the incoming parameter name
        If Not ParameterName.StartsWith("@") Then ParameterName = "@" + ParameterName

        _ParamName = ParameterName
        _ParamType = ParameterType
        _ParamValue = ParameterValue

    End Sub

    Public ReadOnly Property ParameterName() As String
        Get
            Return _ParamName
        End Get
    End Property

    Public ReadOnly Property ParameterType() As SqlDbType
        Get
            Return _ParamType
        End Get
    End Property

    Public ReadOnly Property ParameterValue() As Object
        Get
            Return _ParamValue
        End Get
    End Property

End Class

Public Class AuditTrailRecord

    Property PropertyName As String
    Property DBValue As String
    Property NewValue As String

    Public Sub New(PropertyName As String, DBValue As String, NewValue As String)
        Me.PropertyName = PropertyName
        Me.DBValue = DBValue
        Me.NewValue = NewValue
    End Sub

End Class

Public Class UpdateObject

    Private m_SQL As String
    Private m_Connection As SqlConnection

    Private m_da As SqlDataAdapter
    Private m_dt As New DataTable

    Private m_RowIndex As Integer
    Private m_dr As DataRow

    Private m_BOF As Boolean
    Private m_EOF As Boolean

#Region "Methods"

    Public Function Populate(ByVal ConnectionString As String, ByVal CommandText As String) As Boolean

        Try

            m_SQL = CommandText
            m_Connection = DAL.ReturnConnection(ConnectionString)

            m_da = New SqlDataAdapter(m_SQL, m_Connection)
            With m_da
                .MissingSchemaAction = MissingSchemaAction.AddWithKey
                .AcceptChangesDuringFill = True
                .AcceptChangesDuringUpdate = True
                .ContinueUpdateOnError = False
            End With

            m_da.Fill(m_dt)
            If m_dt.Rows.Count = 0 Then
                m_RowIndex = -1
            Else
                m_RowIndex = 0
            End If

            SetRow()

            Return True

        Catch ex As Exception
            Msgbox("Exception: " + ex.Message + vbCrLf + vbCrLf +
                   "SQL: " + m_SQL, MsgBoxStyle.Exclamation, "UpdateObject.Populate")
            Return False
        End Try

    End Function

    Public Sub AddNew()
        m_RowIndex = -1
        m_dr = m_dt.NewRow
        SetRow()
    End Sub

    Public Sub Delete()

    End Sub

    Public Sub MoveFirst()
        m_RowIndex = 0
        SetRow()
    End Sub

    Public Sub MoveNext()
        m_RowIndex += 1
        SetRow()
    End Sub

    Public Sub MovePrevious()
        m_RowIndex -= 1
        SetRow()
    End Sub

    Public Sub MoveLast()
        m_RowIndex = m_dt.Rows.Count - 1
        SetRow()
    End Sub

    Private Sub SetRow()

        'new row
        If m_RowIndex < 0 Then
            m_BOF = True
            m_EOF = True
            m_dr = m_dt.NewRow
        Else

            If (m_RowIndex + 1) > m_dt.Rows.Count Then
                m_BOF = False
                m_EOF = True
            Else
                m_BOF = False
                m_EOF = False
                m_dr = m_dt.Rows(m_RowIndex)
            End If

        End If

    End Sub

    Public Function Update() As Boolean

        Try

            If m_RowIndex = -1 Then m_dt.Rows.Add(m_dr)

            Dim _bld As New SqlCommandBuilder(m_da)
            _bld.ConflictOption = ConflictOption.OverwriteChanges
            _bld.SetAllValues = False

            m_da = _bld.DataAdapter
            m_da.Update(m_dt)

            _bld.Dispose()
            _bld = Nothing

            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Sub Close()

        Try

            m_dt.Dispose()
            m_dt = Nothing

            m_da.Dispose()
            m_da = Nothing

            m_Connection.Close()
            m_Connection.Dispose()
            m_Connection = Nothing

        Catch ex As Exception

        End Try

    End Sub

#End Region

#Region "Properties"

    Public ReadOnly Property RowCount As Integer
        Get
            Return m_dt.Rows.Count
        End Get
    End Property

    Public ReadOnly Property HasRows As Boolean
        Get
            If m_dt.Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

    Public ReadOnly Property CurrentRow As DataRow
        Get
            Return m_dr
        End Get
    End Property

    Public ReadOnly Property Rows As DataRowCollection
        Get
            Return m_dt.Rows
        End Get
    End Property

    Public ReadOnly Property BOF As Boolean
        Get
            Return m_BOF
        End Get
    End Property

    Public ReadOnly Property EOF As Boolean
        Get
            Return m_EOF
        End Get
    End Property

#End Region

End Class