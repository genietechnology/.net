﻿Public Class CleanData

    Public Shared Function ReturnString(ByVal ObjectIn As Object) As String
        If ObjectIn Is Nothing Then
            Return ""
        Else
            If IsDBNull(ObjectIn) Then
                Return ""
            Else
                Return ObjectIn.ToString
            End If
        End If
    End Function

    Public Shared Function ReturnDecimal(ByVal ObjectIn As Object) As Decimal
        If ObjectIn Is Nothing Then
            Return 0
        Else
            If IsDBNull(ObjectIn) Then
                Return 0
            Else
                Dim _Decimal As Decimal = 0
                If Decimal.TryParse(ObjectIn.ToString, _Decimal) Then
                    Return _Decimal
                Else
                    Return 0
                End If
            End If
        End If

    End Function

    Public Shared Function ReturnInteger(ByVal ObjectIn As Object) As Integer
        If ObjectIn Is Nothing Then
            Return 0
        Else
            If IsDBNull(ObjectIn) Then
                Return 0
            Else
                Dim _Integer As Integer = 0
                If Integer.TryParse(ObjectIn.ToString, _Integer) Then
                    Return _Integer
                Else
                    Return 0
                End If
            End If
        End If

    End Function

    Public Shared Function ReturnBoolean(ByVal ObjectIn As Object) As Boolean
        If ObjectIn Is Nothing Then
            Return False
        Else
            If IsDBNull(ObjectIn) Then
                Return False
            Else
                Dim _Boolean As Boolean = False
                If Boolean.TryParse(ObjectIn.ToString, _Boolean) Then
                    Return _Boolean
                Else
                    Return False
                End If
            End If
        End If

    End Function

    Public Shared Function ReturnDate(ByVal ObjectIn As Object) As Date?
        If ObjectIn Is Nothing Then
            Return Nothing
        Else
            If IsDBNull(ObjectIn) Then
                Return Nothing
            Else
                Dim _Date As Date = Nothing
                If Date.TryParse(ObjectIn.ToString, _Date) Then
                    Return _Date
                Else
                    Return Nothing
                End If
            End If
        End If

    End Function

    Public Shared Function ReturnTimeSpan(ByVal ObjectIn As Object) As TimeSpan?
        If ObjectIn Is Nothing Then
            Return Nothing
        Else
            If IsDBNull(ObjectIn) Then
                Return Nothing
            Else
                Dim _TS As TimeSpan = Nothing
                If TimeSpan.TryParse(ObjectIn.ToString, _TS) Then
                    Return _TS
                Else
                    Return Nothing
                End If
            End If
        End If

    End Function

    Public Shared Function ReturnDouble(ByVal ObjectIn As Object) As Double
        If ObjectIn Is Nothing Then
            Return 0
        Else
            If IsDBNull(ObjectIn) Then
                Return 0
            Else
                Dim _Double As Double = 0
                If Double.TryParse(ObjectIn.ToString, _Double) Then
                    Return _Double
                Else
                    Return 0
                End If
            End If
        End If
    End Function

End Class
