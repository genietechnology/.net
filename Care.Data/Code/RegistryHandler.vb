﻿Imports Microsoft.Win32

Public Class RegistryHandler

    Private Shared Function ReturnRegistryKey(ByVal RegistryPath As String) As RegistryKey
        Dim _regKey As RegistryKey = Registry.CurrentUser.OpenSubKey(RegistryPath, False)
        Return _regKey
    End Function

    Public Shared Function ReturnConnectionfromRegistryKey(ByVal RegistryPath As String) As ConnectionBuilder

        Dim _ConnectionDetails As ConnectionBuilder = Nothing
        Dim _Key As RegistryKey = ReturnRegistryKey(RegistryPath)
        If _Key IsNot Nothing Then

            _ConnectionDetails = New ConnectionBuilder

            Dim _Integrated As String = _Key.GetValue("IntegratedSecurity").ToString
            Dim _IntegratedBool As Boolean = False
            If _Integrated = "1" Or _Integrated = "Y" Or _Integrated = "Yes" Or _Integrated = "T" Or _Integrated = "True" Then
                _IntegratedBool = True
            End If

            With _ConnectionDetails
                .Server = _Key.GetValue("SQLServer").ToString
                .DB = _Key.GetValue("SQLDB").ToString
                .User = _Key.GetValue("SQLUser").ToString
                .Password = _Key.GetValue("SQLPassword").ToString
                .IntegratedSecurity = _IntegratedBool
            End With

        Else
            'no registry
        End If

        Return _ConnectionDetails

    End Function

    Public Shared Function GetMIMEType(ByVal Extension As String) As String

        Dim _Return As String = ""

        'Search all keys under HKEY_CLASSES_ROOT
        For Each subKey As String In Registry.ClassesRoot.GetSubKeyNames()

            If String.IsNullOrEmpty(subKey) Then Continue For
            If subKey.CompareTo(Extension) = 0 Then

                Try

                    'File extension found. Get Default Value
                    Dim _Default As String = Registry.ClassesRoot.OpenSubKey(subKey).GetValue("").ToString()
                    If _Default.Length > 0 Then

                        Dim _Value As String = GetMIMEType(_Default)
                        If _Value.Length > 0 Then
                            _Return = _Value
                        Else
                            _Return = _Default
                        End If

                    End If

                Catch ex As Exception
                    If ex.HResult = -2147467261 Then
                        Return "No default value is associated with the " & subKey & " sub key in the registry."
                    End If


                End Try

            End If

        Next

        Return _Return

    End Function

End Class
