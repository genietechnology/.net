﻿Imports System.IO

Public Class FileHandler

    Public Shared Function ReturnFileName(ByVal FilePath As String) As String

        Dim _Return As String = ""

        Try
            Dim _fi As New FileInfo(FilePath)
            _Return = _fi.Name
            _fi = Nothing

        Catch ex As Exception

        End Try

        Return _Return

    End Function

    Public Shared Function ReturnFriendlyFileSize(ByVal FileSize As Long) As String

        Dim _KB As Long = CLng(FileSize / 1024)
        Dim _MB As Long = CLng(_KB / 1024)

        If _MB > 0 Then
            Return _MB.ToString + " MB"
        Else

            If _KB > 0 Then
                Return _KB.ToString + " KB"
            Else
                Return _KB.ToString + " Bytes"
            End If

        End If

    End Function

    Public Shared Function ReturnTemporaryFile(ByVal TempFolder As String, ByVal Extension As String) As String
        Dim _Ext As String = Extension.Replace(".", "")
        Return TempFolder & Guid.NewGuid.ToString & "." & _Ext
    End Function

    Public Shared Sub OpenFile(ByVal FilePath As String)

        If File.Exists(FilePath) Then

            Dim _Proc As New Process
            _Proc.StartInfo.FileName = FilePath

            Try
                _Proc.Start()

            Catch ex As Exception

            End Try

            _Proc.Dispose()
            _Proc = Nothing

        End If

    End Sub

    Public Shared Sub ClearTemporaryFolder(ByVal TempFolder As String)

        Try
            If Directory.Exists(TempFolder) Then
                For Each _File As String In Directory.GetFiles(TempFolder)
                    File.Delete(_File)
                Next
            End If
        Catch ex As Exception


        End Try

    End Sub

    Public Shared Function BrowseFile() As String

        Dim _ofd As New OpenFileDialog
        If _ofd.ShowDialog = DialogResult.OK Then
            Return _ofd.FileName
        Else
            Return ""
        End If

        _ofd = Nothing

    End Function

    Public Shared Function BrowseFolder() As String

        Dim _fbd As New FolderBrowserDialog
        If _fbd.ShowDialog = DialogResult.OK Then
            Return _fbd.SelectedPath
        Else
            Return ""
        End If

        _fbd = Nothing

    End Function

End Class
