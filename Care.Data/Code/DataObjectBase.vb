﻿Imports System.IO
Imports System.Xml.Serialization
Imports System.Reflection

Public MustInherit Class DataObjectBase

    Private m_IsNew As Boolean = False
    Private m_IsDeleted As Boolean = False

    Public Enum EnumNameFormat
        PropertyExact
        MergeClassAndProperty
    End Enum

    Public Property IsNew() As Boolean
        Get
            Return m_IsNew
        End Get
        Set(ByVal value As Boolean)
            m_IsNew = value
        End Set
    End Property

    Public Property IsDeleted() As Boolean
        Get
            Return m_IsDeleted
        End Get
        Set(ByVal value As Boolean)
            m_IsDeleted = value
        End Set
    End Property

    Public Sub Serialise(ByVal XMLPath As String)

        Dim _sw As New StreamWriter(XMLPath)
        Dim _x As New XmlSerializer(Me.GetType)
        _x.Serialize(_sw, Me)
        _sw.Close()

    End Sub

    Public Sub DeSerialise(Of DataObjectType)(ByVal XMLPath As String, ByRef MyDataObject As DataObjectType)

        Dim _sr As New StreamReader(XMLPath)
        Dim _x As New XmlSerializer(Me.GetType)
        MyDataObject = CType(_x.Deserialize(_sr), DataObjectType)
        _sr.Close()

    End Sub

    Public Function FieldList(ByVal NameFormat As EnumNameFormat) As List(Of Field)

        Dim _t As Type = Me.GetType()
        Dim _Fields As New List(Of Field)
        Dim _Properties() As PropertyInfo = _t.GetProperties()
        For Each _p As PropertyInfo In _Properties
            If _p.CanWrite AndAlso _p.Name.StartsWith("_") Then
                If NameFormat = EnumNameFormat.PropertyExact Then _Fields.Add(New Field(_p.Name, _p.PropertyType.ToString))
                If NameFormat = EnumNameFormat.MergeClassAndProperty Then _Fields.Add(New Field("{" + _t.Name + "." + _p.Name + "}", _p.PropertyType.ToString))
            End If
        Next

        Return _Fields

    End Function

    Public Overridable Sub SetDefaultValues()

    End Sub

#Region "Getters"

    Public Shared Function GetGUID(ByVal ObjectIn As Object) As Guid?

        If ObjectIn Is DBNull.Value Then
            Return Nothing
        Else
            Return CType(ObjectIn, Guid)
        End If

    End Function

    Public Shared Function GetDate(ByVal ObjectIn As Object) As Date?

        If ObjectIn Is DBNull.Value Then
            Return Nothing
        Else
            If CType(ObjectIn, Date) = Date.MinValue Then
                Return Nothing
            Else
                Return CType(ObjectIn, Date)
            End If
        End If

    End Function

    Public Shared Function GetDateTime(ByVal ObjectIn As Object) As Date?

        If ObjectIn Is DBNull.Value Then
            Return Nothing
        Else
            If CType(ObjectIn, Date) = Date.MinValue Then
                Return Nothing
            Else
                Return CType(ObjectIn, Date)
            End If
        End If

    End Function

    Public Shared Function GetTimeSpan(ByVal ObjectIn As Object) As TimeSpan?

        If ObjectIn Is DBNull.Value Then
            Return Nothing
        Else
            If CType(ObjectIn, TimeSpan) = TimeSpan.MinValue Then
                Return Nothing
            Else
                Return CType(ObjectIn, TimeSpan)
            End If
        End If

    End Function

    Public Shared Function GetBoolean(ByVal ObjectIn As Object) As Boolean

        If ObjectIn Is DBNull.Value Then
            Return False
        Else
            Return CType(ObjectIn, Boolean)
        End If

    End Function

    Public Shared Function GetDecimal(ByVal ObjectIn As Object) As Decimal

        If ObjectIn Is DBNull.Value Then
            Return 0
        Else
            Return CType(ObjectIn, Decimal)
        End If

    End Function

    Public Shared Function GetInteger(ByVal ObjectIn As Object) As Integer

        If ObjectIn Is DBNull.Value Then
            Return 0
        Else
            Return CType(ObjectIn, Integer)
        End If

    End Function

    Public Shared Function GetLong(ByVal ObjectIn As Object) As Long

        If ObjectIn Is DBNull.Value Then
            Return 0
        Else
            Return CType(ObjectIn, Long)
        End If

    End Function

    Public Shared Function GetImage(ByVal ObjectIn As Object) As Byte()

        If ObjectIn Is DBNull.Value Then
            Return Nothing
        Else
            Return CType(ObjectIn, Byte())
        End If

    End Function

    Public Shared Function GetXML(ByVal ObjectIn As Object) As String

        If ObjectIn Is DBNull.Value Then
            Return Nothing
        Else
            Return CType(ObjectIn, String)
        End If

    End Function

    Public Shared Function GetByte(ByVal ObjectIn As Object) As Byte

        If ObjectIn Is DBNull.Value Then
            Return Nothing
        Else
            Return CType(ObjectIn, Byte)
        End If

    End Function


#End Region

End Class

