﻿Public Class MergeFields

    Public Shared Function MergeData(ByVal TemplateIn As String, ByVal BusinessObjectsIn As List(Of DataObjectBase)) As String

        Dim _Return As String = TemplateIn

        For Each _BO In BusinessObjectsIn
            MergeData(_Return, _BO)
        Next

        Return _Return

    End Function

    Public Shared Function MergeData(ByVal TemplateIn As String, ByVal BusinessObjectIn As DataObjectBase) As String

        Dim _Return As String = TemplateIn
        For Each _F As Field In BusinessObjectIn.FieldList(DataObjectBase.EnumNameFormat.MergeClassAndProperty)

            If _Return.Contains(_F.Name) Then

                'set propertyname
                Dim _PropertyName As String = _F.Name
                _PropertyName = _PropertyName.Replace("{", "")
                _PropertyName = _PropertyName.Replace("}", "")
                _PropertyName = _PropertyName.Substring(_F.Name.IndexOf("."))

                'get value
                Dim _Value As Object = CallByName(BusinessObjectIn, _PropertyName, CallType.Get)
                Dim _StringValue As String = _Value.ToString

                _Return = _Return.Replace(_F.Name, _StringValue)

            End If

        Next

        Return _Return

    End Function

End Class
