﻿Imports Care.Data

Public Class CareRS

    Private m_DT As DataTable
    Private m_DR As DataRow

    Private m_BOF As Boolean
    Private m_EOF As Boolean
    Private m_RecordCount As Integer
    Private m_RowIndex As Integer

    Public ReadOnly Property BOF As Boolean
        Get
            Return m_BOF
        End Get
    End Property

    Public ReadOnly Property EOF As Boolean
        Get
            Return m_EOF
        End Get
    End Property

    Public ReadOnly Property CurrentRow As DataRow
        Get
            Return m_DR
        End Get
    End Property

    Public ReadOnly Property RecordCount As Integer
        Get
            Return m_RecordCount
        End Get
    End Property

    Public Sub Populate(ConnectionString As String, SQL As String)

        m_DT = DAL.GetDataTablefromSQL(ConnectionString, SQL)

        m_RowIndex = 0
        m_RecordCount = m_DT.Rows.Count

        SetBOFEOF()
        SetRow()

    End Sub

    Private Sub SetRow()
        If m_RecordCount > 0 Then
            m_DR = m_DT.Rows(m_RowIndex)
        Else
            m_DR = Nothing
        End If
    End Sub

    Private Sub SetBOFEOF()

        m_BOF = False
        m_EOF = False

        If m_RecordCount = 0 Then
            m_BOF = True
            m_EOF = True
        Else
            If m_RowIndex < 0 Then
                m_BOF = True
            Else
                If m_RowIndex + 1 > m_RecordCount Then
                    m_EOF = True
                End If
            End If
        End If

    End Sub

    Public Sub AddNew()

    End Sub

    Public Sub Update()

    End Sub

    Public Sub MoveFirst()
        m_RowIndex = 0
        SetRow()
    End Sub

    Public Sub MovePrevious()
        m_RowIndex -= 1
        SetRow()
    End Sub

    Public Sub MoveNext()
        m_RowIndex += 1
        SetRow()
    End Sub

    Public Sub MoveLast()
        m_RowIndex = m_RecordCount - 1
        SetRow()
    End Sub

    Public Sub Close()

    End Sub

End Class
