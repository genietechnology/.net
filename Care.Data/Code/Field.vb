﻿Public Class Field

    Public Sub New(ByVal PropertyName As String, ByVal PropertyType As String)
        Me.Name = PropertyName
        Me.FieldType = PropertyType
    End Sub

    Public Property Name As String
    Public Property FieldType As String

End Class