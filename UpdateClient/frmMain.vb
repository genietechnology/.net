﻿Option Strict On

Imports System.IO
Imports System.ServiceProcess
Imports System.Security.AccessControl
Imports System.Text
Imports Microsoft.SqlServer.Management.Common
Imports Microsoft.SqlServer.Management.Smo
Imports System.Data.SqlClient
Imports System.Threading

Public Class frmMain

    Private Enum EnumStartStop
        StartService
        StopService
    End Enum

    Private Const m_RegPath As String = "HKEY_CURRENT_USER\Software\Care Software\Update Client"

    Private m_CustomerID As String = ""
    Private m_WS As New UpdateService.UpdateClient
    Private m_WorkingFolder As String = My.Application.Info.DirectoryPath + "\packs\"
    Private m_WorkingFolderWithID As String = ""
    Private m_ID As String = ""
    Private m_EndPointID As String = ""
    Private m_Jobs As List(Of UpdateService.Job) = Nothing
    Private m_ConnectionString As String = ""
    Private m_AutoUpdate As Boolean = False
    Private m_AutoEXEPath As String = ""
    Private m_AutoEXEArgs As String = ""
    Private m_AutoEXEUserCode As String = ""

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles Me.Load

        If My.Application.CommandLineArgs.Count = 7 Then

            '0=AUTO
            '1=CUSTOMERID
            '2=ENDPOINTID
            '3="EXE PATH"
            '4="USERCODE"
            '5="APPCODE"
            '6="APPNAME"

            If My.Application.CommandLineArgs(0) = "AUTO" Then
                m_AutoUpdate = True
                m_CustomerID = My.Application.CommandLineArgs(1)
                m_EndPointID = My.Application.CommandLineArgs(2)
                m_AutoEXEPath = My.Application.CommandLineArgs(3)
                m_AutoEXEUserCode = My.Application.CommandLineArgs(4)
                m_AutoEXEArgs = My.Application.CommandLineArgs(5) + " " + """" + My.Application.CommandLineArgs(6) + """" + " " + m_AutoEXEUserCode
            End If

        Else
            'running directly
            m_CustomerID = ReturnRegistryValue(m_RegPath, "CustomerID")
        End If

        Me.Text = "Care Software Update Client Version " + My.Application.Info.Version.ToString
        If m_AutoUpdate Then Me.Text += " - Running Auto Update..."

    End Sub

    Private Sub frmMain_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        If m_AutoUpdate Then
            AutoUpdate()
        Else
            ConnectToService()
            CheckForUpdates()
        End If
    End Sub

    Private Sub AutoUpdate()

        btnCheck.Enabled = False
        btnInstall.Enabled = False
        Application.DoEvents()

        m_Jobs = m_WS.CheckForUpdates(m_EndPointID, True).ToList
        If m_Jobs IsNot Nothing AndAlso m_Jobs.Count > 0 Then
            For Each _J In m_Jobs
                RunJob(_J, True)
            Next
        End If

        Application.DoEvents()
        Thread.Sleep(3000)

        Dim _p As New System.Diagnostics.Process
        _p = Process.Start(m_AutoEXEPath, m_AutoEXEArgs)

        End

    End Sub

    Private Function ReturnRegistryValue(ByVal RegKey As String, RegItem As String) As String
        Dim _o As Object = My.Computer.Registry.GetValue(RegKey, RegItem, Nothing)
        If _o Is Nothing Then
            My.Computer.Registry.SetValue(RegKey, RegItem, "")
            Return ""
        Else
            Return _o.ToString
        End If
    End Function

    Private Sub ConnectToService()

        notRed.Visible = False
        notGreen.Visible = False

        If m_CustomerID = "" Then
            notRed.Visible = True
            notRed.ContextMenuStrip = ctxNoCustomer
        Else

            Try

                Dim _CustomerID As Guid = New Guid(m_CustomerID)

                Dim _Register As String = ""
                _Register = m_WS.RegisterEndpoint(m_CustomerID, My.Computer.Name)

                If _Register.StartsWith("-") Then
                    notRed.Visible = True
                    notRed.ContextMenuStrip = ctxNoCustomer
                    notRed.Text += " - Customer ID not found."
                Else
                    m_EndPointID = _Register
                    notGreen.Visible = True
                    notGreen.Text += " - Connected to Update Server."
                End If

            Catch ex As Exception
                If MsgBox("Customer ID might not be valid - do you want to reset it?", MsgBoxStyle.YesNo, "Error Connecting to Service") = MsgBoxResult.Yes Then
                    m_CustomerID = ""
                    notRed.Visible = True
                    notRed.ContextMenuStrip = ctxNoCustomer
                End If
            End Try

        End If

    End Sub

    Private Sub CheckForUpdates()

        SetCMDs(False)
        txtLog.Text = ""

        m_Jobs = m_WS.CheckForUpdates(m_EndPointID, False).ToList
        If m_Jobs IsNot Nothing Then

            cgUpdates.Populate(m_Jobs)

            cgUpdates.Columns("ID").Visible = False
            cgUpdates.Columns("JobID").Visible = False
            cgUpdates.Columns("AppPath").Visible = False
            cgUpdates.Columns("AppServer").Visible = False
            cgUpdates.Columns("HasPack").Visible = False
            cgUpdates.Columns("Seq").Visible = False
            cgUpdates.Columns("AppCS").Visible = False
            cgUpdates.Columns("Steps").Visible = False

        End If

        SetCMDs(True)

    End Sub

    Private Sub btnCheck_Click(sender As Object, e As EventArgs) Handles btnCheck.Click
        CheckForUpdates()
    End Sub

    Private Sub SetCMDs(ByVal Enabled As Boolean)
        If Enabled Then
            btnCheck.Enabled = True
            cgUpdates.Enabled = True
            If cgUpdates.RecordCount > 0 Then
                btnInstall.Enabled = True
            Else
                btnInstall.Enabled = False
            End If
        Else
            btnCheck.Enabled = False
            btnInstall.Enabled = False
            cgUpdates.Enabled = False
        End If
    End Sub

    Private Sub DoUpdate()

        If cgUpdates.RecordCount > 0 Then

            SetCMDs(False)

            Dim _J As UpdateService.Job = CType(cgUpdates.GetRowObject(cgUpdates.RowIndex), UpdateService.Job)
            If _J IsNot Nothing Then
                RunJob(_J, True)
            End If

            SetCMDs(True)

        End If

    End Sub

    Private Sub btnInstall_Click(sender As Object, e As EventArgs) Handles btnInstall.Click
        DoUpdate()
    End Sub

    Private Sub RunJob(ByVal JobToRun As UpdateService.Job, ByVal Interactive As Boolean)

        txtLog.MaskBox.Clear()

        m_ID = JobToRun.ID
        m_WorkingFolderWithID = m_WorkingFolder + JobToRun.JobID
        m_ConnectionString = BuildConnectionString(JobToRun)

        LogEvent("Processing Job: " + JobToRun.JobName)

        LogEvent("Check Local Path " + m_WorkingFolderWithID)
        If Not My.Computer.FileSystem.DirectoryExists(m_WorkingFolderWithID) Then
            LogEvent("Create Local Path " + m_WorkingFolderWithID)
            My.Computer.FileSystem.CreateDirectory(m_WorkingFolderWithID)
        Else
            LogEvent("Deleting existing files")
            DeleteAllFiles(m_WorkingFolderWithID)
        End If

        If ProcessSteps(JobToRun) Then
            m_WS.LogUpdate(m_ID, 1, txtLog.Text)
            If Interactive Then MsgBox("Update Completed Successfully.", MsgBoxStyle.Information, "Process Update")
        Else
            m_WS.LogUpdate(m_ID, 0, txtLog.Text)
            If Interactive Then MsgBox("Update Failed.", MsgBoxStyle.Exclamation, "Process Update")
        End If

        If Interactive Then CheckForUpdates()

    End Sub

    Private Function BuildConnectionString(ByRef JobIn As UpdateService.Job) As String

        If m_ConnectionString <> "" Then Return m_ConnectionString

        If JobIn.AppCS <> "" Then
            m_ConnectionString = JobIn.AppCS
        Else

            m_ConnectionString = "Data Source=" & JobIn.AppServer & ";" &
                                 "Initial Catalog=<Database>;" &
                                 "User Id=sa;" &
                                 "Password=care122126;"

        End If

        Return m_ConnectionString

    End Function

    Private Sub ParseData(ByRef StepIn As UpdateService.JobStep, ByRef JobIn As UpdateService.Job)
        ParseField(StepIn.Arg1, JobIn, StepIn)
        ParseField(StepIn.Arg2, JobIn, StepIn)
        ParseField(StepIn.Arg3, JobIn, StepIn)
        ParseField(StepIn.Memo, JobIn, StepIn)
    End Sub

    Private Sub ParseField(ByRef StringIn As String, ByRef JobIn As UpdateService.Job, ByVal StepIn As UpdateService.JobStep)

        StringIn = StringIn.Replace("{ID}", JobIn.JobID)
        StringIn = StringIn.Replace("{RunningFolder}", My.Application.Info.DirectoryPath)
        StringIn = StringIn.Replace("{WorkingFolder}", m_WorkingFolderWithID)
        StringIn = StringIn.Replace("{ApplicationFolder}", JobIn.AppPath)

        StringIn = StringIn.Replace("{Arg1}", StepIn.Arg1)
        StringIn = StringIn.Replace("{Arg2}", StepIn.Arg2)
        StringIn = StringIn.Replace("{Arg3}", StepIn.Arg3)

        ParseEnvironmentVars(StringIn)

        ProcessInput(StringIn)

    End Sub

    Private Sub ProcessInput(ByRef StringIn As String)

        '_PromptText will look like this:
        '{Prompt=SomeLabel}

        While StringIn.Contains("{Prompt=")

            Dim _PromptStart As Integer = StringIn.IndexOf("{Prompt=")
            Dim _PromptEnd As Integer = StringIn.IndexOf("}", _PromptStart)
            Dim _PromptText As String = StringIn.Substring(_PromptStart, (_PromptEnd - _PromptStart) + 1)

            Dim _Equals As Integer = _PromptText.IndexOf("=")
            Dim _PromptLabel As String = _PromptText.Substring(_Equals + 1)
            _PromptLabel = _PromptLabel.Substring(0, _PromptLabel.Length - 1)

            Dim _Input As String = InputBox(_PromptLabel)

            'now reconstruct the string
            Dim _Before As String = StringIn.Substring(0, _PromptStart)
            Dim _After As String = StringIn.Substring(_PromptEnd + 1)

            StringIn = _Before + Chr(34) + _Input + Chr(34) + _After

        End While

    End Sub

    Private Sub ParseEnvironmentVars(ByRef StringIn As String)
        StringIn = StringIn.Replace("{ProgramFiles}", Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86))
        StringIn = StringIn.Replace("{System}", Environment.GetFolderPath(Environment.SpecialFolder.System))
        StringIn = StringIn.Replace("{Desktop}", Environment.GetFolderPath(Environment.SpecialFolder.CommonDesktopDirectory))
        StringIn = StringIn.Replace("{Startup}", Environment.GetFolderPath(Environment.SpecialFolder.CommonStartup))
    End Sub

    Private Function ProcessSteps(ByVal Job As UpdateService.Job) As Boolean

        Dim _i As Integer = 1

        For Each _Step In From _j In Job.Steps Order By _j.Seq
            LogEvent("Step: " + _i.ToString + " - " + _Step.Type)
            If Not RunStep(_Step, Job) Then Return False
            _i += 1
        Next

        Return True

    End Function

    Private Function RunStep(ByRef StepIn As UpdateService.JobStep, ByRef JobIn As UpdateService.Job) As Boolean

        ParseData(StepIn, JobIn)

        Select Case StepIn.Type

            Case "Create Folder"
                Return CreateFolders(StepIn.Arg1)

            Case "Download File"
                Return DownloadFile(StepIn.Arg1, StepIn.Arg2)

            Case "SQL Script"
                Return RunSQL(StepIn.Arg1, StepIn.Memo)

            Case "Process Pack"
                Return ProcessPack(StepIn.Arg1, StepIn.Arg2)

            Case "Run Batch File"
                Return ProcessBatchFile(StepIn.Arg1, StepIn.Memo)

            Case "Kill Process"
                Return KillProcess(StepIn.Arg1)

            Case "Start Service"
                Return StartService(StepIn.Arg1)

            Case "Stop Service"
                Return StopService(StepIn.Arg1)

            Case "Write File"
                Return WriteFile(StepIn.Arg1, StepIn.Memo)

            Case "Copy File"
                Return CopyFile(StepIn.Arg1, StepIn.Arg2)

            Case "Move File"
                Return MoveFile(StepIn.Arg1, StepIn.Arg2)

            Case Else
                Return False

        End Select

    End Function

    Private Function CopyFile(ByVal Source As String, ByVal Target As String) As Boolean
        LogEvent("Copying File")
        Try
            IO.File.Copy(Source, Target, True)
        Catch ex As Exception
            LogEvent("Copy Failed: " + ex.Message)
            Return False
        End Try
        LogEvent("Copy OK")
        Return True
    End Function

    Private Function MoveFile(ByVal Source As String, ByVal Target As String) As Boolean
        LogEvent("Moving File")
        Try
            IO.File.Move(Source, Target)
        Catch ex As Exception
            LogEvent("Move Failed: " + ex.Message)
            Return False
        End Try
        LogEvent("Move OK")
        Return True
    End Function

    Private Function RunCommand(ByVal CommandToRun As String) As Boolean

        Dim _p As New Process
        With _p.StartInfo
            .FileName = "cmd.exe"
            .Verb = "runas"
            .UseShellExecute = True
            .CreateNoWindow = True
            .Arguments = CommandToRun
        End With

        _p.Start()

        Return True

    End Function

    Private Function CreateFolders(ByVal FolderPath As String) As Boolean

        Try

            Dim _Users As String = "BUILTIN\Users"
            Dim _DS As New DirectorySecurity()
            _DS.AddAccessRule(New FileSystemAccessRule(_Users, FileSystemRights.FullControl, InheritanceFlags.ObjectInherit, PropagationFlags.InheritOnly, AccessControlType.Allow))

            Directory.CreateDirectory(FolderPath, _DS)

        Catch ex As Exception
            LogEvent("Create Folders: " + ex.Message)
            Return False
        End Try

        Return True

    End Function

    Private Function ToggleService(ByVal ServiceName As String, ByVal StartStop As EnumStartStop) As Boolean

        LogEvent(StartStop.ToString + " Service: " + ServiceName)

        Dim _ServiceFound As Boolean = False

        For Each _S As ServiceController In ServiceController.GetServices(My.Computer.Name)

            If _S.ServiceName = ServiceName Then

                _ServiceFound = True

                Try

                    Select Case _S.Status

                        Case ServiceControllerStatus.Stopped
                            If StartStop = EnumStartStop.StartService Then
                                _S.Start()
                                LogEvent(StartStop.ToString + " Service: " + ServiceName + " OK")
                            End If

                        Case ServiceControllerStatus.StartPending, ServiceControllerStatus.ContinuePending, ServiceControllerStatus.PausePending, ServiceControllerStatus.Paused, ServiceControllerStatus.Running
                            If StartStop = EnumStartStop.StopService Then
                                _S.Stop()
                                LogEvent(StartStop.ToString + " Service: " + ServiceName + " OK")
                            End If

                    End Select

                    Return True

                Catch ex As Exception
                    LogEvent(StartStop.ToString + " Service: " + ServiceName + " Failed")
                    Return False
                End Try

            End If

        Next

        'if we get here, the service doesn't exist, so we return true (as it might not be installed)
        If Not _ServiceFound Then LogEvent(StartStop.ToString + " Service: " + ServiceName + " not found")

        Return True

    End Function

    Private Function StartService(ByVal ServiceName As String) As Boolean
        Return ToggleService(ServiceName, EnumStartStop.StartService)
    End Function

    Private Function StopService(ByVal ServiceName As String) As Boolean
        Return ToggleService(ServiceName, EnumStartStop.StopService)
    End Function

    Private Function DownloadFile(ByVal URL As String, ByVal Destination As String) As Boolean

        If IO.File.Exists(Destination) Then
            LogEvent("File already exists - Download aborted")
            Return True
        Else

            Dim _Return As Boolean = True
            Dim _WebClient As New System.Net.WebClient

            LogEvent("Downloading File")

            Try
                _WebClient.DownloadFile(URL, Destination)

            Catch ex As Exception
                _Return = False
                LogEvent("Error Downloading File")
            End Try

            If IO.File.Exists(Destination) Then
                LogEvent("File Downloaded Successfully")
            Else
                _Return = False
            End If

            _WebClient.Dispose()
            _WebClient = Nothing

            Return _Return

        End If

    End Function

    Private Function RunSQL(ByVal DBName As String, ByVal SQL As String) As Boolean

        LogEvent("RunSQL Started")

        Dim _Return As Boolean = True

        Dim _cs As String = m_ConnectionString
        _cs = _cs.Replace("<Database>", DBName)

        Dim _cn As New SqlConnection(_cs)
        If _cn IsNot Nothing Then

            Dim _svr As New Server(New ServerConnection(_cn))

            Try
                _svr.ConnectionContext.ExecuteNonQuery(SQL)

            Catch ex As Exception
                _Return = False
                Clipboard.SetText(SQL)
                LogEvent("RunSQL Failed:" + vbCrLf + ex.Message + vbCrLf + _cs + vbCrLf + SQL)

            Finally
                _svr.ConnectionContext.Disconnect()
            End Try

        End If

        If _Return Then LogEvent("RunSQL Successful")

        Return _Return

    End Function

    Private Function ProcessPack(ByVal ZipFile As String, ByVal TargetPath As String) As Boolean

        Dim _Return As Boolean = True
        LogEvent("Unzipping File")

        Try
            Dim _z As New Ionic.Zip.ZipFile(ZipFile)
            _z.ExtractAll(TargetPath, Ionic.Zip.ExtractExistingFileAction.OverwriteSilently)
            LogEvent("Zip File Extracted OK")

        Catch ex As Exception
            LogEvent("Error Extracting Zip File")
            _Return = True
        End Try

        Return _Return

    End Function

    Private Function WriteFile(ByVal FilePath As String, ByVal BatchFileContent As String) As Boolean

        LogEvent("Replace Environment Variables")
        ParseEnvironmentVars(BatchFileContent)

        Dim _Return As Boolean = True

        LogEvent("Writing File")

        Dim _sb As New StringBuilder
        For Each _line In BatchFileContent.Split(ControlChars.CrLf.ToCharArray)
            _sb.AppendLine(_line)
        Next

        Try
            IO.File.WriteAllText(FilePath, _sb.ToString)

        Catch ex As Exception
            LogEvent("Error Writing File")
            _Return = False
        End Try

        Return _Return

    End Function

    Private Function ProcessBatchFile(ByVal RunDirectory As String, ByVal BatchFileContent As String) As Boolean

        LogEvent("ProcessBatchFile - Started")

        Dim _Return As Boolean = False
        Dim _File As String = Guid.NewGuid.ToString + ".cmd"
        Dim _FilePath As String = RunDirectory + "\" + _File

        If WriteFile(_FilePath, BatchFileContent) Then

            LogEvent("ProcessBatchFile - Running Batch File")

            Dim _p As New Process
            With _p.StartInfo
                .WorkingDirectory = RunDirectory
                .FileName = _File
                .Verb = "runas"
                .UseShellExecute = True
                .CreateNoWindow = True
                .Arguments = _FilePath
            End With

            Try
                _p.Start()
                _p.WaitForExit()

                LogEvent("ProcessBatchFile - Exit Code: " + _p.ExitCode.ToString)

                If _p.ExitCode = 0 Then
                    _Return = True
                End If

            Catch ex As Exception
                LogEvent("ProcessBatchFile - Error: " + ex.Message)
            End Try

        Else
            'batch file was not created
        End If

        Return _Return

    End Function

    Private Function KillProcess(ByVal ProcessName As String) As Boolean
        For Each _P As Process In Process.GetProcessesByName(ProcessName)
            _P.Kill()
        Next
        Return True
    End Function

    Private Function DeleteAllFiles(ByVal Path As String) As Boolean
        For Each _FilePath As String In My.Computer.FileSystem.GetFiles(Path)
            My.Computer.FileSystem.DeleteFile(_FilePath, FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.SendToRecycleBin)
        Next
        Return True
    End Function

    Private Sub LogEvent(ByVal LogText As String)
        txtLog.MaskBox.AppendText(LogText + vbNewLine)
        Application.DoEvents()
    End Sub

    Private Sub notGreen_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles notGreen.MouseDoubleClick
        ShowMain()
    End Sub

    Private Sub mnuExit_Click(sender As Object, e As EventArgs) Handles mnuExit.Click
        Application.Exit()
    End Sub

    Private Sub mnuSetCustomerID_Click(sender As Object, e As EventArgs) Handles mnuSetCustomerID.Click
        SetCustomerID()
    End Sub

    Private Sub SetCustomerID()
        Dim _CustomerID As String = InputBox("Paste Customer ID", "Set Customer ID")
        If _CustomerID <> "" Then
            m_CustomerID = _CustomerID
            My.Computer.Registry.SetValue(m_RegPath, "CustomerID", m_CustomerID)
            ConnectToService()
        End If
    End Sub

    Private Sub ShowMain()
        Me.Show()
        Me.WindowState = FormWindowState.Normal
    End Sub

    Private Sub mnuExit1_Click(sender As Object, e As EventArgs) Handles mnuExit1.Click
        End
    End Sub

    Private Sub CheckForUpdatesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CheckForUpdatesToolStripMenuItem.Click
        ShowMain()
        CheckForUpdates()
    End Sub

    Private Sub LaunchHelpdeskToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LaunchHelpdeskToolStripMenuItem.Click
        Process.Start("https://support.caresoftware.co.uk/hc")
    End Sub

    Private Sub LaunchRemoteAssistanceToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LaunchRemoteAssistanceToolStripMenuItem.Click
        Process.Start("https://www.fastsupport.com")
    End Sub

    Private Sub cgUpdates_GridDoubleClick(sender As Object, e As EventArgs) Handles cgUpdates.GridDoubleClick
        DoUpdate()
    End Sub

    Private Sub frmMain_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        If Me.WindowState = FormWindowState.Minimized Then
            Me.Hide()
        End If
    End Sub

    Private Sub SetCustomerIDToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SetCustomerIDToolStripMenuItem.Click
        SetCustomerID()
    End Sub

    Private Sub ProcessIntermediateTasks()

        Try

            Dim m_Jobs As List(Of UpdateService.Job) = m_WS.CheckForUpdates(m_EndPointID, True).ToList
            For Each _j In m_Jobs
                RunJob(_j, False)
            Next

        Catch ex As Exception

        End Try

    End Sub

End Class