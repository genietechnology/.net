﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.ctxMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.CheckForUpdatesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.LaunchHelpdeskToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaunchRemoteAssistanceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.SetCustomerIDToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExit = New System.Windows.Forms.ToolStripMenuItem()
        Me.notGreen = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.notRed = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.ctxNoCustomer = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuSetCustomerID = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExit1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.txtLog = New DevExpress.XtraEditors.MemoEdit()
        Me.btnInstall = New Care.Controls.CareButton(Me.components)
        Me.btnCheck = New Care.Controls.CareButton(Me.components)
        Me.cgUpdates = New Care.Controls.CareGrid()
        Me.ctxMenu.SuspendLayout()
        Me.ctxNoCustomer.SuspendLayout()
        CType(Me.txtLog.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ctxMenu
        '
        Me.ctxMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CheckForUpdatesToolStripMenuItem, Me.ToolStripSeparator1, Me.LaunchHelpdeskToolStripMenuItem, Me.LaunchRemoteAssistanceToolStripMenuItem, Me.ToolStripSeparator2, Me.SetCustomerIDToolStripMenuItem, Me.mnuExit})
        Me.ctxMenu.Name = "ctxMenu"
        Me.ctxMenu.Size = New System.Drawing.Size(216, 126)
        '
        'CheckForUpdatesToolStripMenuItem
        '
        Me.CheckForUpdatesToolStripMenuItem.Name = "CheckForUpdatesToolStripMenuItem"
        Me.CheckForUpdatesToolStripMenuItem.Size = New System.Drawing.Size(215, 22)
        Me.CheckForUpdatesToolStripMenuItem.Text = "Check for Updates"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(212, 6)
        '
        'LaunchHelpdeskToolStripMenuItem
        '
        Me.LaunchHelpdeskToolStripMenuItem.Name = "LaunchHelpdeskToolStripMenuItem"
        Me.LaunchHelpdeskToolStripMenuItem.Size = New System.Drawing.Size(215, 22)
        Me.LaunchHelpdeskToolStripMenuItem.Text = "Launch Helpdesk"
        '
        'LaunchRemoteAssistanceToolStripMenuItem
        '
        Me.LaunchRemoteAssistanceToolStripMenuItem.Name = "LaunchRemoteAssistanceToolStripMenuItem"
        Me.LaunchRemoteAssistanceToolStripMenuItem.Size = New System.Drawing.Size(215, 22)
        Me.LaunchRemoteAssistanceToolStripMenuItem.Text = "Launch Remote Assistance"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(212, 6)
        '
        'SetCustomerIDToolStripMenuItem
        '
        Me.SetCustomerIDToolStripMenuItem.Name = "SetCustomerIDToolStripMenuItem"
        Me.SetCustomerIDToolStripMenuItem.Size = New System.Drawing.Size(215, 22)
        Me.SetCustomerIDToolStripMenuItem.Text = "Set Customer ID"
        '
        'mnuExit
        '
        Me.mnuExit.Name = "mnuExit"
        Me.mnuExit.Size = New System.Drawing.Size(215, 22)
        Me.mnuExit.Text = "Exit"
        '
        'notGreen
        '
        Me.notGreen.ContextMenuStrip = Me.ctxMenu
        Me.notGreen.Icon = CType(resources.GetObject("notGreen.Icon"), System.Drawing.Icon)
        Me.notGreen.Text = "Care Software Update"
        '
        'notRed
        '
        Me.notRed.ContextMenuStrip = Me.ctxMenu
        Me.notRed.Icon = CType(resources.GetObject("notRed.Icon"), System.Drawing.Icon)
        Me.notRed.Text = "Care Software Update"
        '
        'ctxNoCustomer
        '
        Me.ctxNoCustomer.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSetCustomerID, Me.mnuExit1})
        Me.ctxNoCustomer.Name = "ctxMenu"
        Me.ctxNoCustomer.Size = New System.Drawing.Size(160, 48)
        '
        'mnuSetCustomerID
        '
        Me.mnuSetCustomerID.Name = "mnuSetCustomerID"
        Me.mnuSetCustomerID.Size = New System.Drawing.Size(159, 22)
        Me.mnuSetCustomerID.Text = "Set Customer ID"
        '
        'mnuExit1
        '
        Me.mnuExit1.Name = "mnuExit1"
        Me.mnuExit1.Size = New System.Drawing.Size(159, 22)
        Me.mnuExit1.Text = "Exit"
        '
        'txtLog
        '
        Me.txtLog.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtLog.Location = New System.Drawing.Point(398, 12)
        Me.txtLog.Name = "txtLog"
        Me.txtLog.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLog.Properties.Appearance.Options.UseFont = True
        Me.txtLog.Size = New System.Drawing.Size(574, 408)
        Me.txtLog.TabIndex = 5
        '
        'btnInstall
        '
        Me.btnInstall.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnInstall.Location = New System.Drawing.Point(138, 429)
        Me.btnInstall.Name = "btnInstall"
        Me.btnInstall.Size = New System.Drawing.Size(120, 23)
        Me.btnInstall.TabIndex = 6
        Me.btnInstall.Text = "Install Update"
        '
        'btnCheck
        '
        Me.btnCheck.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCheck.Location = New System.Drawing.Point(12, 429)
        Me.btnCheck.Name = "btnCheck"
        Me.btnCheck.Size = New System.Drawing.Size(120, 23)
        Me.btnCheck.TabIndex = 4
        Me.btnCheck.Text = "Check for Updates"
        '
        'cgUpdates
        '
        Me.cgUpdates.AllowBuildColumns = True
        Me.cgUpdates.AllowEdit = False
        Me.cgUpdates.AllowHorizontalScroll = False
        Me.cgUpdates.AllowMultiSelect = False
        Me.cgUpdates.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cgUpdates.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgUpdates.Appearance.Options.UseFont = True
        Me.cgUpdates.AutoSizeByData = True
        Me.cgUpdates.DisableAutoSize = False
        Me.cgUpdates.DisableDataFormatting = False
        Me.cgUpdates.FocusedRowHandle = -2147483648
        Me.cgUpdates.HideFirstColumn = False
        Me.cgUpdates.Location = New System.Drawing.Point(12, 12)
        Me.cgUpdates.Name = "cgUpdates"
        Me.cgUpdates.PreviewColumn = ""
        Me.cgUpdates.QueryID = Nothing
        Me.cgUpdates.RowAutoHeight = False
        Me.cgUpdates.SearchAsYouType = True
        Me.cgUpdates.ShowAutoFilterRow = False
        Me.cgUpdates.ShowFindPanel = True
        Me.cgUpdates.ShowGroupByBox = False
        Me.cgUpdates.ShowLoadingPanel = False
        Me.cgUpdates.ShowNavigator = True
        Me.cgUpdates.Size = New System.Drawing.Size(380, 408)
        Me.cgUpdates.TabIndex = 3
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(984, 461)
        Me.Controls.Add(Me.btnInstall)
        Me.Controls.Add(Me.txtLog)
        Me.Controls.Add(Me.btnCheck)
        Me.Controls.Add(Me.cgUpdates)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Care Software Update Client"
        Me.ctxMenu.ResumeLayout(False)
        Me.ctxNoCustomer.ResumeLayout(False)
        CType(Me.txtLog.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ctxMenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents notGreen As System.Windows.Forms.NotifyIcon
    Friend WithEvents CheckForUpdatesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents LaunchHelpdeskToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaunchRemoteAssistanceToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuExit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents notRed As System.Windows.Forms.NotifyIcon
    Friend WithEvents ctxNoCustomer As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuSetCustomerID As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExit1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cgUpdates As Care.Controls.CareGrid
    Friend WithEvents btnCheck As Care.Controls.CareButton
    Friend WithEvents txtLog As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents btnInstall As Care.Controls.CareButton
    Friend WithEvents SetCustomerIDToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
