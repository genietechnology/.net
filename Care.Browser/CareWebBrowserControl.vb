﻿Imports CefSharp.WinForms

Public Class CareWebBrowserControl

    Private m_Browser As ChromiumWebBrowser

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_Browser = New ChromiumWebBrowser("about:blank")
        m_Browser.Dock = System.Windows.Forms.DockStyle.Fill

        Me.Controls.Add(m_Browser)

    End Sub

    Public Sub Navigate(ByVal URL As String)
        m_Browser.Load(URL)
    End Sub

End Class
