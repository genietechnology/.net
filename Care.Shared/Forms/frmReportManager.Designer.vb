﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportManager
    Inherits frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.btnBrowse = New Care.Controls.CareButton(Me.components)
        Me.cbxCategory = New Care.Controls.CareComboBox(Me.components)
        Me.chkAdHoc = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.txtReportPath = New Care.Controls.CareTextBox(Me.components)
        Me.txtReportName = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel22 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.cbxQuery = New Care.Controls.CareComboBox(Me.components)
        Me.gbxCustom = New Care.Controls.CareFrame()
        Me.txtSQLQuery = New DevExpress.XtraEditors.MemoEdit()
        Me.gbxOrderBy = New Care.Controls.CareFrame()
        Me.txtSQLOrder = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl4 = New Care.Controls.CareFrame()
        Me.radQuery = New Care.Controls.CareRadioButton(Me.components)
        Me.radCustom = New Care.Controls.CareRadioButton(Me.components)
        Me.gbxClauses = New Care.Controls.CareFrame()
        Me.cgClauses = New Care.Controls.CareGrid()
        Me.btnRemove = New Care.Controls.CareButton(Me.components)
        Me.btnEdit = New Care.Controls.CareButton(Me.components)
        Me.btnAdd = New Care.Controls.CareButton(Me.components)
        Me.btnImport = New Care.Controls.CareButton(Me.components)
        Me.btnExport = New Care.Controls.CareButton(Me.components)
        Me.sfd = New SaveFileDialog()
        Me.ofd = New OpenFileDialog()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.cbxCategory.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAdHoc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtReportPath.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtReportName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxQuery.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxCustom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxCustom.SuspendLayout()
        CType(Me.txtSQLQuery.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxOrderBy, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxOrderBy.SuspendLayout()
        CType(Me.txtSQLOrder.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.radQuery.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radCustom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxClauses, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxClauses.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(737, 3)
        Me.btnCancel.TabIndex = 3
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(646, 3)
        Me.btnOK.TabIndex = 2
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnExport)
        Me.Panel1.Controls.Add(Me.btnImport)
        Me.Panel1.Location = New System.Drawing.Point(0, 575)
        Me.Panel1.Size = New System.Drawing.Size(834, 36)
        Me.Panel1.TabIndex = 5
        Me.Panel1.Controls.SetChildIndex(Me.btnOK, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnCancel, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnImport, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnExport, 0)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.btnBrowse)
        Me.GroupControl1.Controls.Add(Me.cbxCategory)
        Me.GroupControl1.Controls.Add(Me.chkAdHoc)
        Me.GroupControl1.Controls.Add(Me.CareLabel11)
        Me.GroupControl1.Controls.Add(Me.txtReportPath)
        Me.GroupControl1.Controls.Add(Me.txtReportName)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Controls.Add(Me.CareLabel22)
        Me.GroupControl1.Controls.Add(Me.CareLabel14)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 55)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(810, 98)
        Me.GroupControl1.TabIndex = 0
        '
        'btnBrowse
        '
        Me.btnBrowse.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnBrowse.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnBrowse.Appearance.Options.UseFont = True
        Me.btnBrowse.CausesValidation = False
        Me.btnBrowse.Location = New System.Drawing.Point(725, 65)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(74, 25)
        Me.btnBrowse.TabIndex = 8
        Me.btnBrowse.Tag = ""
        Me.btnBrowse.Text = "Browse"
        '
        'cbxCategory
        '
        Me.cbxCategory.AllowBlank = False
        Me.cbxCategory.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxCategory.DataSource = Nothing
        Me.cbxCategory.DisplayMember = Nothing
        Me.cbxCategory.EnterMoveNextControl = True
        Me.cbxCategory.Location = New System.Drawing.Point(100, 39)
        Me.cbxCategory.Name = "cbxCategory"
        Me.cbxCategory.Properties.AccessibleName = "Tariff Type"
        Me.cbxCategory.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxCategory.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxCategory.Properties.Appearance.Options.UseFont = True
        Me.cbxCategory.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxCategory.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxCategory.SelectedValue = Nothing
        Me.cbxCategory.Size = New System.Drawing.Size(699, 22)
        Me.cbxCategory.TabIndex = 5
        Me.cbxCategory.Tag = "AE"
        Me.cbxCategory.ValueMember = Nothing
        '
        'chkAdHoc
        '
        Me.chkAdHoc.EnterMoveNextControl = True
        Me.chkAdHoc.Location = New System.Drawing.Point(779, 12)
        Me.chkAdHoc.Name = "chkAdHoc"
        Me.chkAdHoc.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAdHoc.Properties.Appearance.Options.UseFont = True
        Me.chkAdHoc.Size = New System.Drawing.Size(20, 19)
        Me.chkAdHoc.TabIndex = 3
        Me.chkAdHoc.Tag = "AE"
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(643, 14)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(130, 15)
        Me.CareLabel11.TabIndex = 2
        Me.CareLabel11.Text = "Allow Ad-Hoc Reporting"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtReportPath
        '
        Me.txtReportPath.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtReportPath.CharacterCasing = CharacterCasing.Normal
        Me.txtReportPath.EnterMoveNextControl = True
        Me.txtReportPath.Location = New System.Drawing.Point(100, 67)
        Me.txtReportPath.MaxLength = 100
        Me.txtReportPath.Name = "txtReportPath"
        Me.txtReportPath.NumericAllowNegatives = False
        Me.txtReportPath.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtReportPath.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtReportPath.Properties.AccessibleName = "Name"
        Me.txtReportPath.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtReportPath.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtReportPath.Properties.Appearance.Options.UseFont = True
        Me.txtReportPath.Properties.Appearance.Options.UseTextOptions = True
        Me.txtReportPath.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtReportPath.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtReportPath.Properties.MaxLength = 100
        Me.txtReportPath.Size = New System.Drawing.Size(619, 22)
        Me.txtReportPath.TabIndex = 7
        Me.txtReportPath.Tag = "AE"
        Me.txtReportPath.TextAlign = HorizontalAlignment.Left
        Me.txtReportPath.ToolTipText = ""
        '
        'txtReportName
        '
        Me.txtReportName.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtReportName.CharacterCasing = CharacterCasing.Normal
        Me.txtReportName.EnterMoveNextControl = True
        Me.txtReportName.Location = New System.Drawing.Point(100, 11)
        Me.txtReportName.MaxLength = 100
        Me.txtReportName.Name = "txtReportName"
        Me.txtReportName.NumericAllowNegatives = False
        Me.txtReportName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtReportName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtReportName.Properties.AccessibleName = "Name"
        Me.txtReportName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtReportName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtReportName.Properties.Appearance.Options.UseFont = True
        Me.txtReportName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtReportName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtReportName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtReportName.Properties.MaxLength = 100
        Me.txtReportName.Size = New System.Drawing.Size(537, 22)
        Me.txtReportName.TabIndex = 1
        Me.txtReportName.Tag = "AE"
        Me.txtReportName.TextAlign = HorizontalAlignment.Left
        Me.txtReportName.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(8, 70)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(62, 15)
        Me.CareLabel1.TabIndex = 6
        Me.CareLabel1.Text = "Report Path"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel22
        '
        Me.CareLabel22.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel22.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel22.Location = New System.Drawing.Point(8, 14)
        Me.CareLabel22.Name = "CareLabel22"
        Me.CareLabel22.Size = New System.Drawing.Size(70, 15)
        Me.CareLabel22.TabIndex = 0
        Me.CareLabel22.Text = "Report Name"
        Me.CareLabel22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(8, 42)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(86, 15)
        Me.CareLabel14.TabIndex = 4
        Me.CareLabel14.Text = "Report Category"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxQuery
        '
        Me.cbxQuery.AllowBlank = False
        Me.cbxQuery.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxQuery.DataSource = Nothing
        Me.cbxQuery.DisplayMember = Nothing
        Me.cbxQuery.EnterMoveNextControl = True
        Me.cbxQuery.Location = New System.Drawing.Point(256, 9)
        Me.cbxQuery.Name = "cbxQuery"
        Me.cbxQuery.Properties.AccessibleName = "Tariff Type"
        Me.cbxQuery.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxQuery.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxQuery.Properties.Appearance.Options.UseFont = True
        Me.cbxQuery.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxQuery.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxQuery.SelectedValue = Nothing
        Me.cbxQuery.Size = New System.Drawing.Size(543, 22)
        Me.cbxQuery.TabIndex = 2
        Me.cbxQuery.Tag = "AE"
        Me.cbxQuery.ValueMember = Nothing
        '
        'gbxCustom
        '
        Me.gbxCustom.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.gbxCustom.Controls.Add(Me.txtSQLQuery)
        Me.gbxCustom.Location = New System.Drawing.Point(12, 203)
        Me.gbxCustom.Name = "gbxCustom"
        Me.gbxCustom.Size = New System.Drawing.Size(810, 198)
        Me.gbxCustom.TabIndex = 2
        Me.gbxCustom.Text = "Custom SQL Query"
        '
        'txtSQLQuery
        '
        Me.txtSQLQuery.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtSQLQuery.Location = New System.Drawing.Point(10, 23)
        Me.txtSQLQuery.Name = "txtSQLQuery"
        Me.txtSQLQuery.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSQLQuery.Properties.Appearance.Options.UseFont = True
        Me.txtSQLQuery.Properties.AppearanceFocused.Font = New System.Drawing.Font("Lucida Console", 9.75!)
        Me.txtSQLQuery.Properties.AppearanceFocused.Options.UseFont = True
        Me.txtSQLQuery.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Lucida Console", 9.75!)
        Me.txtSQLQuery.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtSQLQuery, True)
        Me.txtSQLQuery.Size = New System.Drawing.Size(789, 170)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtSQLQuery, OptionsSpelling1)
        Me.txtSQLQuery.TabIndex = 0
        Me.txtSQLQuery.Tag = "AE"
        '
        'gbxOrderBy
        '
        Me.gbxOrderBy.Anchor = CType(((AnchorStyles.Bottom Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.gbxOrderBy.Controls.Add(Me.txtSQLOrder)
        Me.gbxOrderBy.Controls.Add(Me.CareLabel2)
        Me.gbxOrderBy.Location = New System.Drawing.Point(12, 531)
        Me.gbxOrderBy.Name = "gbxOrderBy"
        Me.gbxOrderBy.ShowCaption = False
        Me.gbxOrderBy.Size = New System.Drawing.Size(810, 38)
        Me.gbxOrderBy.TabIndex = 4
        '
        'txtSQLOrder
        '
        Me.txtSQLOrder.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtSQLOrder.CharacterCasing = CharacterCasing.Normal
        Me.txtSQLOrder.EnterMoveNextControl = True
        Me.txtSQLOrder.Location = New System.Drawing.Point(146, 8)
        Me.txtSQLOrder.MaxLength = 100
        Me.txtSQLOrder.Name = "txtSQLOrder"
        Me.txtSQLOrder.NumericAllowNegatives = False
        Me.txtSQLOrder.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSQLOrder.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSQLOrder.Properties.AccessibleName = "Name"
        Me.txtSQLOrder.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSQLOrder.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSQLOrder.Properties.Appearance.Options.UseFont = True
        Me.txtSQLOrder.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSQLOrder.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSQLOrder.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSQLOrder.Properties.MaxLength = 100
        Me.txtSQLOrder.Size = New System.Drawing.Size(653, 20)
        Me.txtSQLOrder.TabIndex = 1
        Me.txtSQLOrder.Tag = "AE"
        Me.txtSQLOrder.TextAlign = HorizontalAlignment.Left
        Me.txtSQLOrder.ToolTipText = ""
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(8, 11)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(129, 15)
        Me.CareLabel2.TabIndex = 0
        Me.CareLabel2.Text = "Custom Order By Clause"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl4
        '
        Me.GroupControl4.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl4.Controls.Add(Me.radQuery)
        Me.GroupControl4.Controls.Add(Me.radCustom)
        Me.GroupControl4.Controls.Add(Me.cbxQuery)
        Me.GroupControl4.Location = New System.Drawing.Point(12, 159)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.ShowCaption = False
        Me.GroupControl4.Size = New System.Drawing.Size(810, 38)
        Me.GroupControl4.TabIndex = 1
        '
        'radQuery
        '
        Me.radQuery.CausesValidation = False
        Me.radQuery.Location = New System.Drawing.Point(114, 10)
        Me.radQuery.Name = "radQuery"
        Me.radQuery.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.radQuery.Properties.Appearance.Options.UseFont = True
        Me.radQuery.Properties.Appearance.Options.UseTextOptions = True
        Me.radQuery.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radQuery.Properties.AutoWidth = True
        Me.radQuery.Properties.Caption = "Use Predefined Query"
        Me.radQuery.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radQuery.Properties.RadioGroupIndex = 1
        Me.radQuery.Size = New System.Drawing.Size(136, 19)
        Me.radQuery.TabIndex = 1
        Me.radQuery.TabStop = False
        '
        'radCustom
        '
        Me.radCustom.CausesValidation = False
        Me.radCustom.EditValue = True
        Me.radCustom.Location = New System.Drawing.Point(8, 10)
        Me.radCustom.Name = "radCustom"
        Me.radCustom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.radCustom.Properties.Appearance.Options.UseFont = True
        Me.radCustom.Properties.Appearance.Options.UseTextOptions = True
        Me.radCustom.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radCustom.Properties.AutoWidth = True
        Me.radCustom.Properties.Caption = "Custom Query"
        Me.radCustom.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radCustom.Properties.RadioGroupIndex = 1
        Me.radCustom.Size = New System.Drawing.Size(99, 19)
        Me.radCustom.TabIndex = 0
        '
        'gbxClauses
        '
        Me.gbxClauses.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.gbxClauses.Controls.Add(Me.cgClauses)
        Me.gbxClauses.Controls.Add(Me.btnRemove)
        Me.gbxClauses.Controls.Add(Me.btnEdit)
        Me.gbxClauses.Controls.Add(Me.btnAdd)
        Me.gbxClauses.Location = New System.Drawing.Point(12, 407)
        Me.gbxClauses.Name = "gbxClauses"
        Me.gbxClauses.Size = New System.Drawing.Size(810, 118)
        Me.gbxClauses.TabIndex = 3
        Me.gbxClauses.Text = "End-User Clauses"
        '
        'cgClauses
        '
        Me.cgClauses.AllowBuildColumns = True
        Me.cgClauses.AllowEdit = False
        Me.cgClauses.AllowHorizontalScroll = False
        Me.cgClauses.AllowMultiSelect = False
        Me.cgClauses.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgClauses.Appearance.Options.UseFont = True
        Me.cgClauses.AutoSizeByData = True
        Me.cgClauses.DisableAutoSize = False
        Me.cgClauses.DisableDataFormatting = False
        Me.cgClauses.FocusedRowHandle = -2147483648
        Me.cgClauses.HideFirstColumn = False
        Me.cgClauses.Location = New System.Drawing.Point(10, 24)
        Me.cgClauses.Name = "cgClauses"
        Me.cgClauses.PreviewColumn = ""
        Me.cgClauses.QueryID = Nothing
        Me.cgClauses.RowAutoHeight = False
        Me.cgClauses.SearchAsYouType = True
        Me.cgClauses.ShowAutoFilterRow = False
        Me.cgClauses.ShowFindPanel = False
        Me.cgClauses.ShowGroupByBox = False
        Me.cgClauses.ShowLoadingPanel = False
        Me.cgClauses.ShowNavigator = False
        Me.cgClauses.Size = New System.Drawing.Size(709, 87)
        Me.cgClauses.TabIndex = 0
        '
        'btnRemove
        '
        Me.btnRemove.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnRemove.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnRemove.Appearance.Options.UseFont = True
        Me.btnRemove.CausesValidation = False
        Me.btnRemove.Location = New System.Drawing.Point(725, 86)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(74, 25)
        Me.btnRemove.TabIndex = 3
        Me.btnRemove.Tag = ""
        Me.btnRemove.Text = "Remove"
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnEdit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnEdit.Appearance.Options.UseFont = True
        Me.btnEdit.CausesValidation = False
        Me.btnEdit.Location = New System.Drawing.Point(725, 55)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(74, 25)
        Me.btnEdit.TabIndex = 2
        Me.btnEdit.Tag = ""
        Me.btnEdit.Text = "Edit"
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnAdd.Appearance.Options.UseFont = True
        Me.btnAdd.CausesValidation = False
        Me.btnAdd.Location = New System.Drawing.Point(725, 24)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(74, 25)
        Me.btnAdd.TabIndex = 1
        Me.btnAdd.Tag = ""
        Me.btnAdd.Text = "Add"
        '
        'btnImport
        '
        Me.btnImport.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnImport.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnImport.Appearance.Options.UseFont = True
        Me.btnImport.CausesValidation = False
        Me.btnImport.Location = New System.Drawing.Point(12, 3)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(90, 25)
        Me.btnImport.TabIndex = 0
        Me.btnImport.Tag = ""
        Me.btnImport.Text = "Import Pack"
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnExport.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnExport.Appearance.Options.UseFont = True
        Me.btnExport.CausesValidation = False
        Me.btnExport.Location = New System.Drawing.Point(108, 3)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.Size = New System.Drawing.Size(90, 25)
        Me.btnExport.TabIndex = 1
        Me.btnExport.Tag = ""
        Me.btnExport.Text = "Export Pack"
        '
        'ofd
        '
        Me.ofd.FileName = "OpenFileDialog1"
        '
        'frmReportManager
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(834, 611)
        Me.Controls.Add(Me.gbxClauses)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.gbxOrderBy)
        Me.Controls.Add(Me.gbxCustom)
        Me.Controls.Add(Me.GroupControl1)
        Me.Name = "frmReportManager"
        Me.Text = "frmReportManager"
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        Me.Controls.SetChildIndex(Me.gbxCustom, 0)
        Me.Controls.SetChildIndex(Me.gbxOrderBy, 0)
        Me.Controls.SetChildIndex(Me.GroupControl4, 0)
        Me.Controls.SetChildIndex(Me.gbxClauses, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.cbxCategory.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAdHoc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtReportPath.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtReportName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxQuery.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxCustom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxCustom.ResumeLayout(False)
        CType(Me.txtSQLQuery.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxOrderBy, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxOrderBy.ResumeLayout(False)
        Me.gbxOrderBy.PerformLayout()
        CType(Me.txtSQLOrder.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        CType(Me.radQuery.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radCustom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxClauses, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxClauses.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtReportName As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents CareLabel22 As Care.Controls.CareLabel
    Friend WithEvents cbxCategory As Care.Controls.CareComboBox
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents chkAdHoc As Care.Controls.CareCheckBox
    Friend WithEvents cbxQuery As Care.Controls.CareComboBox
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents txtReportPath As Care.Controls.CareTextBox
    Friend WithEvents btnBrowse As Care.Controls.CareButton
    Friend WithEvents gbxCustom As DevExpress.XtraEditors.GroupControl
    Friend WithEvents gbxOrderBy As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtSQLQuery As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtSQLOrder As Care.Controls.CareTextBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents radQuery As Care.Controls.CareRadioButton
    Friend WithEvents radCustom As Care.Controls.CareRadioButton
    Friend WithEvents gbxClauses As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnRemove As Care.Controls.CareButton
    Friend WithEvents btnEdit As Care.Controls.CareButton
    Friend WithEvents btnAdd As Care.Controls.CareButton
    Friend WithEvents cgClauses As Care.Controls.CareGrid
    Friend WithEvents btnExport As Care.Controls.CareButton
    Friend WithEvents btnImport As Care.Controls.CareButton
    Friend WithEvents sfd As SaveFileDialog
    Friend WithEvents ofd As OpenFileDialog
End Class
