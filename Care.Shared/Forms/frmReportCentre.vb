﻿Imports Care.Global
Imports Care.Data
Imports System.ComponentModel
Imports DevExpress.XtraTreeList
Imports System.Windows.Forms
Imports DevExpress.XtraTreeList.ViewInfo
Imports System.Drawing

Public Class frmReportCentre

    Private m_ReportBL As New BindingList(Of ReportRecord)
    Private m_ReportRecord As ReportRecord = Nothing
    Private m_XtraReport As DevExpress.XtraReports.UI.XtraReport = Nothing

    Private Sub frmReportCentre_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session.CurrentUser.Email Is Nothing OrElse Session.CurrentUser.Email = "" Then mnuEmailMe.Enabled = False
        PopulateReportTree()
    End Sub

    Private Sub PopulateReportTree()

        m_ReportBL.Clear()
        m_ReportRecord = Nothing
        m_XtraReport = Nothing

        tlReports.KeyFieldName = "ReportID"
        tlReports.ParentFieldName = "ParentID"

        Dim _SQL As String = ""

        _SQL = "select distinct parent from AppReports where ad_hoc = 1 order by parent"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows
                Dim _Parent As String = _DR.Item("parent").ToString
                Dim _r As New ReportRecord("My Reports", _Parent, _Parent, "", "", "")
                m_ReportBL.Add(_r)
                AddChildNodes(_Parent)
            Next

            tlReports.DataSource = m_ReportBL
            tlReports.Columns("QueryID").Visible = False
            tlReports.Columns("QueryName").Visible = False
            tlReports.Columns("ReportPath").Visible = False

            tlReports.ExpandAll()

        End If

    End Sub

    Private Sub AddChildNodes(ByVal ParentID As String)

        Dim _SQL As String = ""

        _SQL += "select r.id as 'report_id', r.name as 'report_name', r.report_file, q.id as 'query_id', q.title as 'query_name' from AppReports r"
        _SQL += " left join AppQueries q on r.query_id = q.ID"
        _SQL += " where r.parent = '" + ParentID + "'"
        _SQL += " order by r.name"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then
            For Each _DR As DataRow In _DT.Rows
                Dim _r As New ReportRecord(ParentID, _DR.Item("report_id").ToString, _DR.Item("report_name").ToString, _DR.Item("report_file").ToString,
                                           _DR.Item("query_id").ToString, _DR.Item("query_name").ToString)
                m_ReportBL.Add(_r)
            Next
        End If

    End Sub

    Private Sub LoadReport()

        DocumentViewer1.DocumentSource = Nothing

        If IO.File.Exists(m_ReportRecord.ReportPath) Then

            Dim _DT As DataTable = Nothing

            If m_ReportRecord.QueryID = "" Then
                _DT = ReportHandler.GetReportCentreData(New Guid(m_ReportRecord.ReportID))
            Else
                _DT = QueryHandler.GetQueryData(New Guid(m_ReportRecord.QueryID))
            End If

            If _DT IsNot Nothing Then

                Session.CursorWaiting()

                m_XtraReport = New DevExpress.XtraReports.UI.XtraReport
                m_XtraReport.LoadLayout(m_ReportRecord.ReportPath)
                m_XtraReport.DataSource = _DT

                DocumentViewer1.DocumentSource = m_XtraReport
                m_XtraReport.CreateDocument()

                Session.CursorDefault()

            End If

        End If

    End Sub

    Private Sub tlReports_DoubleClick(sender As Object, e As EventArgs) Handles tlReports.DoubleClick
        If SetReportRecord(CType(e, MouseEventArgs).Location) Then
            LoadReport()
        End If
    End Sub

    Private Function SetReportRecord(ByVal Position As Point) As Boolean
        m_ReportRecord = Nothing
        Dim viewInfo As TreeListHitInfo = tlReports.CalcHitInfo(Position)
        If viewInfo.Node IsNot Nothing Then
            If Not viewInfo.Node.HasChildren Then
                m_ReportRecord = CType(tlReports.GetDataRecordByNode(viewInfo.Node), ReportRecord)
                Return True
            End If
        End If
        Return False
    End Function

    Private Class ReportRecord

        Public Sub New(ByVal PID As String, ByVal RID As String, ByVal RName As String, ByVal Path As String,
                       ByVal QID As String, ByVal QName As String)

            ParentID = PID
            ReportID = RID
            ReportName = RName
            QueryID = QID
            QueryName = QName
            ReportPath = Path

        End Sub

        Public Property ParentID As String
        Public Property ReportID As String
        Public Property ReportName As String
        Public Property ReportPath As String
        Public Property QueryID As String
        Public Property QueryName As String

    End Class

    Private Sub tlReports_MouseDown(sender As Object, e As MouseEventArgs) Handles tlReports.MouseDown
        If e.Button = MouseButtons.Right Then
            SetReportRecord(e.Location)
        End If
    End Sub

    Private Sub tlReports_PopupMenuShowing(sender As Object, e As PopupMenuShowingEventArgs) Handles tlReports.PopupMenuShowing

        If Session.CurrentUser.IsAdministrator = False Then
            mnuDefinition.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            mnuDesign.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If

        mnuPopup.ShowPopup(MousePosition)

    End Sub

    Private Sub mnuDefinition_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuDefinition.ItemClick
        If m_ReportRecord IsNot Nothing Then
            Dim _frm As New frmReportManager(New Guid(m_ReportRecord.ReportID))
            _frm.StartPosition = FormStartPosition.CenterScreen
            _frm.ShowDialog()
        End If
    End Sub

    Private Sub mnuDesign_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuDesign.ItemClick
        If m_ReportRecord IsNot Nothing Then
            ReportHandler.DesignReport(New Guid(m_ReportRecord.ReportID))
        End If
    End Sub

    Private Sub mnuRun_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuRun.ItemClick
        If m_ReportRecord IsNot Nothing Then
            LoadReport()
        End If
    End Sub

    Private Sub mnuEmailMe_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuEmailMe.ItemClick

        If m_ReportRecord Is Nothing Then Exit Sub
        If m_XtraReport Is Nothing Then Exit Sub

        mnuEmailMe.Enabled = False
        Session.CursorWaiting()

        Dim _PDF As String = ExportToPDF()
        If _PDF <> "" Then
            EmailHandler.SendEmailWithAttachment(Nothing, Session.CurrentUser.Email, "Exported Report", "Please see the attached report.", _PDF)
        End If

        mnuEmailMe.Enabled = True
        Session.CursorDefault()

    End Sub

    Private Sub mnuEmail_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuEmail.ItemClick

        If m_ReportRecord Is Nothing Then Exit Sub
        If m_XtraReport Is Nothing Then Exit Sub

        Dim _PDF As String = ExportToPDF()
        If _PDF <> "" Then
            Session.CursorWaiting()
            EmailHandler.PreviewEmail(Nothing, "", "Exported Report", "Please see the attached report.", _PDF)
        End If

    End Sub

    Private Function ExportToPDF() As String

        If m_ReportRecord Is Nothing Then Return ""
        If m_XtraReport Is Nothing Then Return ""

        Dim _Path As String = Session.TempFolder + "Export_" + Format(Now, "ddMMyyyyHHmmss") + ".pdf"

        Try
            m_XtraReport.ExportToPdf(_Path)

        Catch ex As Exception
            Return ""
        End Try

        If IO.File.Exists(_Path) Then
            Return _Path
        Else
            Return ""
        End If

    End Function
End Class
