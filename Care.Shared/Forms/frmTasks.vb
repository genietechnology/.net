﻿Imports Care.Global
Imports Care.Data
Imports Care.Shared

Public Class frmTasks

    Private m_Task As Business.AppTask

    Private Sub frmTasks_Load(sender As Object, e As EventArgs) Handles Me.Load

        cbxDay.AddItem("Every Day")
        cbxDay.AddItem("Every Monday")
        cbxDay.AddItem("Every Tuesday")
        cbxDay.AddItem("Every Wednesday")
        cbxDay.AddItem("Every Thursday")
        cbxDay.AddItem("Every Friday")
        cbxDay.AddItem("Every Saturday")
        cbxDay.AddItem("Every Sunday")

        cbxType.AddItem("SQL Command")
        cbxType.AddItem("OS Command")
        cbxType.AddItem("Business Object")

    End Sub

#Region "Overrides"

    Protected Overrides Sub SetBindings()

        m_Task = New Business.AppTask
        bs.DataSource = m_Task

        txtName.DataBindings.Add("Text", bs, "_Name")
        txtDescription.DataBindings.Add("Text", bs, "_Description")

        cbxDay.DataBindings.Add("SelectedIndex", bs, "_Day")
        cbxType.DataBindings.Add("Text", bs, "_TaskType")
        txtObjectName.DataBindings.Add("Text", bs, "_ObjectName")

        txtArgs.DataBindings.Add("Text", bs, "_Args")
        txtCommand.DataBindings.Add("Text", bs, "_Command")

    End Sub

    Protected Overrides Function BeforeDelete() As Boolean
        CareMessage("You cannot delete a Task from here.", MessageBoxIcon.Exclamation, "Delete Task")
        Return False
    End Function

    Protected Overrides Sub CommitDelete()

    End Sub

    Protected Overrides Sub CommitUpdate()

        m_Task = CType(bs.Item(bs.Position), Business.AppTask)

        Dim _Time As DateTime? = ConverttoDateTime(txtTime.Text)
        m_Task._Time = _Time.Value.TimeOfDay

        m_Task.Store()
    End Sub

    Protected Overrides Sub FindRecord()

        Dim _Find As New GenericFind

        Dim _SQL As String = "select name as 'Name', task_type as 'Task Type', [time] as 'Time' from AppTasks"

        With _Find
            .Caption = "Find Task"
            .ParentForm = ParentForm
            .PopulateOnLoad = True
            .ConnectionString = Session.ConnectionString
            .GridSQL = _SQL
            .GridOrderBy = "order by name"
            .ReturnField = "ID"
            .Show()
        End With

        If _Find.ReturnValue <> "" Then

            MyBase.RecordID = New Guid(_Find.ReturnValue)
            MyBase.RecordPopulated = True

            m_Task = Business.AppTask.RetreiveByID(New Guid(_Find.ReturnValue))

            If m_Task._Time.HasValue Then
                txtTime.Text = Format(m_Task._Time.Value.Hours, "00") + ":" + Format(m_Task._Time.Value.Minutes, "00")
            Else
                txtTime.Text = ""
            End If

            bs.DataSource = m_Task

        End If

    End Sub

#End Region

    Private Function ConverttoDateTime(ByVal TextIn As String) As DateTime?

        If TextIn = "" Then Return Nothing

        Try
            Return DateTime.Parse(TextIn)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Private Sub CareTab1_SelectedPageChanged(sender As Object, e As DevExpress.XtraTab.TabPageChangedEventArgs) Handles CareTab1.SelectedPageChanged

        Select Case e.Page.Name

            Case "tabHistory"
                DisplayHistory()

        End Select

    End Sub

    Private Sub DisplayHistory()

        If m_Task Is Nothing Then Exit Sub
        If Not m_Task._ID.HasValue Then Exit Sub

        Dim _SQL As String = "select stamp, result, errors from AppTaskHistory where task_id = '" + m_Task._ID.Value.ToString + "' order by stamp desc"
        cgHistory.Populate(Session.ConnectionString, _SQL)

        cgHistory.Columns("stamp").Caption = "Executed"
        cgHistory.Columns("stamp").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        cgHistory.Columns("stamp").DisplayFormat.FormatString = "dd/MM/yyyy HH:mm:ss"

        cgHistory.Columns("result").Caption = "Result"
        cgHistory.Columns("errors").Visible = False

    End Sub

    Private Sub cgHistory_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles cgHistory.FocusedRowChanged
        If e Is Nothing Then Exit Sub
        If e.FocusedRowHandle > 0 Then
            txtErrors.Text = cgHistory.GetRowCellValue(e.FocusedRowHandle, "errors").ToString
        Else
            txtErrors.Text = ""
        End If
    End Sub

    Private Sub cgHistory_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles cgHistory.RowStyle

        If e.RowHandle < 0 Then Exit Sub

        Select Case cgHistory.UnderlyingGridView.GetRowCellDisplayText(e.RowHandle, "result").ToString

            Case "0"
                e.Appearance.BackColor = txtGreen.BackColor

            Case Else
                e.Appearance.BackColor = txtRed.BackColor

        End Select

    End Sub
End Class
