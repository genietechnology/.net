﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGenericNotes
    Inherits frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGenericNotes))
        Me.txtBody = New Care.Controls.CareRichText()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.txtSubject = New Care.Controls.CareTextBox()
        Me.CareLabel1 = New Care.Controls.CareLabel()
        Me.cgNotes = New Care.Controls.CareGridWithButtons()
        Me.btnCancel = New Care.Controls.CareButton()
        Me.btnOK = New Care.Controls.CareButton()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtSubject.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'txtBody
        '
        Me.txtBody.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtBody.HTMLText = resources.GetString("txtBody.HTMLText")
        Me.txtBody.Location = New System.Drawing.Point(373, 57)
        Me.txtBody.Name = "txtBody"
        Me.txtBody.RTFText = resources.GetString("txtBody.RTFText")
        Me.txtBody.Size = New System.Drawing.Size(599, 366)
        Me.txtBody.TabIndex = 2
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.txtSubject)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Location = New System.Drawing.Point(373, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(599, 39)
        Me.GroupControl1.TabIndex = 1
        '
        'txtSubject
        '
        Me.txtSubject.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtSubject.CharacterCasing = CharacterCasing.Normal
        Me.txtSubject.EnterMoveNextControl = True
        Me.txtSubject.Location = New System.Drawing.Point(57, 9)
        Me.txtSubject.MaxLength = 100
        Me.txtSubject.Name = "txtSubject"
        Me.txtSubject.NumericAllowNegatives = False
        Me.txtSubject.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSubject.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSubject.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSubject.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSubject.Properties.Appearance.Options.UseFont = True
        Me.txtSubject.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSubject.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSubject.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSubject.Properties.MaxLength = 100
        Me.txtSubject.Size = New System.Drawing.Size(530, 20)
        Me.txtSubject.TabIndex = 1
        Me.txtSubject.TextAlign = HorizontalAlignment.Left
        Me.txtSubject.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(12, 12)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(39, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Subject"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cgNotes
        '
        Me.cgNotes.ButtonsEnabled = False
        Me.cgNotes.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgNotes.HideFirstColumn = False
        Me.cgNotes.Location = New System.Drawing.Point(12, 12)
        Me.cgNotes.Name = "cgNotes"
        Me.cgNotes.PreviewColumn = ""
        Me.cgNotes.Size = New System.Drawing.Size(355, 440)
        Me.cgNotes.TabIndex = 0
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnCancel.DialogResult = DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(887, 429)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 23)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnOK.Location = New System.Drawing.Point(796, 429)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(85, 23)
        Me.btnOK.TabIndex = 3
        Me.btnOK.Text = "OK"
        '
        'frmGenericNotes
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(984, 461)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.cgNotes)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.txtBody)
        Me.FormBorderStyle = FormBorderStyle.Sizable
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGenericNotes"
        Me.StartPosition = FormStartPosition.CenterScreen
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtSubject.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtBody As Care.Controls.CareRichText
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtSubject As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents cgNotes As Care.Controls.CareGridWithButtons
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents btnOK As Care.Controls.CareButton

End Class
