﻿Imports Care.Global
Imports Care.Data
Imports Care.Shared

Public Class frmMenuItems

    Dim m_Loading As Boolean = False
    Dim m_Form As Care.Shared.Business.AppForm
    Dim m_ToggleMode As EnumToggleMode

    Private Enum EnumToggleMode
        AddModule
        AddMenu
        EditModule
        EditMenu
    End Enum

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub frmMenuItems_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        m_Loading = True

        DisplayModules()
        PopulateIcons()
        gbx.Visible = False
        gbxMM.Visible = False

        txtMMCaption.BackColor = Session.ChangeColour
        txtMMSeq.BackColor = Session.ChangeColour

        m_Loading = False

    End Sub

    Private Sub PopulateIcons()

        If Session.IconFolder = "" Then Exit Sub

        Dim _i As Integer = 0
        For Each _file As String In IO.Directory.GetFiles(Session.IconFolder, "*.ico")

            Dim _LoadImageOK As Boolean = True

            Try
                ImageList1.Images.Add(Drawing.Image.FromFile(_file))

            Catch ex As Exception
                _LoadImageOK = False

            End Try

            If _LoadImageOK Then
                cbxIcon.Properties.Items.Add(New DevExpress.XtraEditors.Controls.ImageComboBoxItem(IO.Path.GetFileName(_file), _i))
            Else
                cbxIcon.Properties.Items.Add(New DevExpress.XtraEditors.Controls.ImageComboBoxItem(IO.Path.GetFileName(_file)))
            End If

            _i += 1

        Next

    End Sub

    Private Sub DisplayModules()

        Dim _SQL As String = "select id, caption as 'Module', display_seq as 'Seq' from AppModules order by display_seq"

        cbxModule.PopulateWithSQL(Session.ConnectionString, _SQL)

        cgModules.HideFirstColumn = True
        cgModules.Populate(Session.ConnectionString, _SQL)

        If cgModules.RecordCount > 0 Then
            cgModules.MoveFirst()
            If cgModules.CurrentRow.Item(0).ToString <> "" Then DisplayMenu(cgModules.CurrentRow.Item(0).ToString)
        End If

    End Sub

    Private Sub DisplayMenu(ByVal ModuleID As String)

        txtModuleID.Text = ModuleID
        Dim _SQL As String = "select id, caption as 'Menu', display_seq as 'Seq' from AppMenus" & _
                             " where module_id = '" & ModuleID & "'" & _
                             " order by display_seq"

        cgMenus.HideFirstColumn = True
        cgMenus.Populate(Session.ConnectionString, _SQL)

        If cgMenus.RecordCount > 0 Then
            cgMenus.MoveFirst()
            If cgMenus.CurrentRow.Item(0).ToString <> "" Then DisplayForms(cgMenus.CurrentRow.Item(0).ToString)
        Else
            cgMenus.Clear()
            Grid.Clear()
        End If

    End Sub

    Private Sub DisplayForms(ByVal MenuID As String)

        txtMenuID.Text = MenuID
        Dim _SQL As String = "select id, class_name as 'Class', form_name as 'Form Name', caption as 'Caption', icon as 'Icon', display_seq as 'Seq' from AppForms" & _
                             " where menu_id = '" & MenuID & "'" & _
                             " order by display_seq"

        Me.GridSQL = _SQL
        Grid.HideFirstColumn = True
        Grid.AutoSizeColumns()

    End Sub

#Region "Overrides"

    'Protected Friend Overrides Sub SetGrid()
    '    If txtMenuID.Text <> "" Then
    '        DisplayForms(txtMenuID.Text)
    '    Else
    '        DisplayModules()
    '    End If
    'End Sub

    Protected Overrides Sub SetBindings()

        m_Form = New Care.Shared.Business.AppForm
        bs.DataSource = m_Form

        txtClassName.DataBindings.Add("Text", bs, "_classname")
        txtFormName.DataBindings.Add("Text", bs, "_formname")
        txtCaption.DataBindings.Add("Text", bs, "_caption")
        txtTooltip.DataBindings.Add("Text", bs, "_tooltip")
        txtDisplaySeq.DataBindings.Add("Text", bs, "_displayseq")

    End Sub

    Protected Overrides Sub BindToID(ID As System.Guid, ByVal IsNew As Boolean)

        m_Form = Care.Shared.Business.AppForm.RetreiveByID(ID)
        bs.DataSource = m_Form

        cbxModule.SelectedValue = m_Form._ModuleId.ToString
        If cbxMenu.ItemCount > 1 Then cbxMenu.SelectedValue = m_Form._MenuId.ToString

        cbxIcon.Text = m_Form._Icon

    End Sub

    Protected Overrides Sub CommitUpdate()

        Dim _AppForm As Care.Shared.Business.AppForm = CType(bs.Item(bs.Position), Care.Shared.Business.AppForm)
        _AppForm._ModuleId = New Guid(cbxModule.SelectedValue.ToString)
        _AppForm._MenuId = New Guid(cbxMenu.SelectedValue.ToString)
        _AppForm._Icon = cbxIcon.Text

        Care.Shared.Business.AppForm.SaveRecord(_AppForm)

    End Sub

    Protected Overrides Sub CommitDelete(ByVal ID As System.Guid)
        Care.Shared.Business.AppForm.DeleteRecord(ID)
    End Sub

#End Region

    Private Sub cbxModule_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxModule.SelectedIndexChanged

        If cbxModule.SelectedValue Is Nothing Then Exit Sub

        Dim _SQL As String = "select id, caption as 'Menu' from AppMenus" & _
                             " where module_id = '" & cbxModule.SelectedValue.ToString & "'" & _
                             " order by display_seq"

        cbxMenu.PopulateWithSQL(Session.ConnectionString, _SQL)

        If cbxMenu.ItemCount = 1 Then cbxMenu.SelectedIndex = 0

    End Sub

    Private Sub cgMenus_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles cgMenus.FocusedRowChanged
        If m_Loading Then Exit Sub
        If cgMenus.RecordCount = 0 Then Exit Sub
        If Not cgMenus.CurrentRow Is Nothing Then
            If cgMenus.CurrentRow.Item(0).ToString <> "" Then DisplayForms(cgMenus.CurrentRow.Item(0).ToString)
        End If
    End Sub

    Private Sub cgModules_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles cgModules.FocusedRowChanged
        If m_Loading Then Exit Sub
        If cgModules.RecordCount = 0 Then Exit Sub
        If Not cgModules.CurrentRow Is Nothing Then
            If cgModules.CurrentRow.Item(0).ToString <> "" Then DisplayMenu(cgModules.CurrentRow.Item(0).ToString)
        End If
    End Sub

    Private Sub btnCloseButton_Click(sender As Object, e As EventArgs) Handles btnCloseButton.Click
        Me.Close()
    End Sub

    Private Sub cgModules_AddClick(sender As Object, e As EventArgs) Handles cgModules.AddClick
        ToggleMM(EnumToggleMode.AddModule)
    End Sub

    Private Sub cgModules_EditClick(sender As Object, e As EventArgs) Handles cgModules.EditClick
        ToggleMM(EnumToggleMode.EditModule)
    End Sub

    Private Sub cgModules_RemoveClick(sender As Object, e As EventArgs) Handles cgModules.RemoveClick

        If CareMessage("Are you sure you want to remove this Module? It will remove all the menu items!", MessageBoxIcon.Warning, MessageBoxButtons.YesNo, "Remove Module") = Windows.Forms.DialogResult.Yes Then

            Dim _ModuleID As String = cgModules.CurrentRow.Item("id").ToString
            If _ModuleID <> "" Then

                Dim _SQL As String = "delete from AppForms where module_id = '" + _ModuleID + "'"
                DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                _SQL = "delete from AppMenus where module_id = '" + _ModuleID + "'"
                DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                _SQL = "delete from AppModules where ID = '" + _ModuleID + "'"
                DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                _SQL = "delete from AppPerms where item_id not in (select f.ID from AppForms f)"
                DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                cgModules.Clear()
                cgMenus.Clear()
                Grid.Clear()

            End If

        End If

    End Sub

    Private Sub ToggleMM(ByVal Mode As EnumToggleMode)

        m_ToggleMode = Mode

        txtMMCaption.ReadOnly = False
        txtMMSeq.ReadOnly = False

        If Mode.ToString.Contains("Add") Then
            txtMMCaption.Text = ""
            txtMMSeq.Text = ""
            gbxMM.Tag = ""
        Else
            If Mode = EnumToggleMode.EditModule Then
                gbxMM.Tag = txtModuleID.Text
                txtMMCaption.Text = cgModules.CurrentRow.Item(1).ToString
                txtMMSeq.Text = cgModules.CurrentRow.Item(2).ToString
            Else
                gbxMM.Tag = txtMenuID.Text
                txtMMCaption.Text = cgMenus.CurrentRow.Item(1).ToString
                txtMMSeq.Text = cgMenus.CurrentRow.Item(2).ToString
            End If
        End If

        gbxMM.Show()
        txtMMCaption.Focus()

    End Sub

    Private Sub cgMenus_AddClick(sender As Object, e As EventArgs) Handles cgMenus.AddClick
        ToggleMM(EnumToggleMode.AddMenu)
    End Sub

    Private Sub cgMenus_EditClick(sender As Object, e As EventArgs) Handles cgMenus.EditClick
        ToggleMM(EnumToggleMode.EditMenu)
    End Sub

    Private Sub cgMenus_RemoveClick(sender As Object, e As EventArgs) Handles cgMenus.RemoveClick

        If CareMessage("Are you sure you want to remove this Menu? It will remove all the menu items!", MessageBoxIcon.Warning, MessageBoxButtons.YesNo, "Remove Menu") = Windows.Forms.DialogResult.Yes Then

            Dim _MenuID As String = cgMenus.CurrentRow.Item("id").ToString
            If _MenuID <> "" Then

                Dim _SQL As String = "delete from AppForms where menu_id = '" + _MenuID + "'"
                DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                _SQL = "delete from AppMenus where ID = '" + _MenuID + "'"
                DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                _SQL = "delete from AppPerms where item_id not in (select f.ID from AppForms f)"
                DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                DisplayMenu(txtModuleID.Text)

            End If

        End If

    End Sub

    Private Sub btnMMCancel_Click(sender As Object, e As EventArgs) Handles btnMMCancel.Click
        gbxMM.Hide()
    End Sub

    Private Sub btnMMOK_Click(sender As Object, e As EventArgs) Handles btnMMOK.Click

        If m_ToggleMode.ToString.Contains("Module") Then
            SaveModule()
            DisplayModules()
        Else
            SaveMenu()
            DisplayMenu(txtModuleID.Text)
        End If

        gbxMM.Hide()

    End Sub

    Private Sub SaveModule()

        Dim _Module As Business.AppModule = Nothing
        If m_ToggleMode = EnumToggleMode.AddModule Then
            _Module = New Business.AppModule
            _Module._ID = Guid.NewGuid
        Else
            _Module = Business.AppModule.RetreiveByID(New Guid(txtModuleID.Text))
        End If

        With _Module
            ._Caption = txtMMCaption.Text
            ._DisplaySeq = ValueHandler.ConvertInteger(txtMMSeq.Text)
            .Store()
        End With

    End Sub

    Private Sub SaveMenu()

        Dim _Menu As Business.AppMenu = Nothing
        If m_ToggleMode = EnumToggleMode.AddMenu Then
            _Menu = New Business.AppMenu
            _Menu._ID = Guid.NewGuid
            _Menu._ModuleId = New Guid(txtModuleID.Text)
        Else
            _Menu = Business.AppMenu.RetreiveByID(New Guid(txtMenuID.Text))
        End If

        With _Menu
            ._Caption = txtMMCaption.Text
            ._DisplaySeq = ValueHandler.ConvertInteger(txtMMSeq.Text)
            .Store()
        End With

    End Sub

End Class
