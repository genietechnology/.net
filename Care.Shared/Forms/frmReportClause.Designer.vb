﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportClause
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReportClause))
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.CareLabel6 = New Care.Controls.CareLabel()
        Me.txtClauseLookup = New DevExpress.XtraEditors.MemoEdit()
        Me.CareLabel5 = New Care.Controls.CareLabel()
        Me.CareLabel3 = New Care.Controls.CareLabel()
        Me.CareLabel4 = New Care.Controls.CareLabel()
        Me.CareLabel2 = New Care.Controls.CareLabel()
        Me.txtClauseField1 = New Care.Controls.CareTextBox()
        Me.cbxClauseOperator = New Care.Controls.CareComboBox()
        Me.cbxClauseDataType = New Care.Controls.CareComboBox()
        Me.txtClauseSeq = New Care.Controls.CareTextBox()
        Me.chkClauseMandatory = New Care.Controls.CareCheckBox()
        Me.CareLabel11 = New Care.Controls.CareLabel()
        Me.txtClauseField2 = New Care.Controls.CareTextBox()
        Me.txtClauseLabel = New Care.Controls.CareTextBox()
        Me.cbxClauseType = New Care.Controls.CareComboBox()
        Me.label22 = New Care.Controls.CareLabel()
        Me.CareLabel22 = New Care.Controls.CareLabel()
        Me.CareLabel14 = New Care.Controls.CareLabel()
        Me.btnCancel = New Care.Controls.CareButton()
        Me.btnOK = New Care.Controls.CareButton()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtClauseLookup.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClauseField1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxClauseOperator.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxClauseDataType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClauseSeq.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkClauseMandatory.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClauseField2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClauseLabel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxClauseType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.CareLabel6)
        Me.GroupControl1.Controls.Add(Me.txtClauseLookup)
        Me.GroupControl1.Controls.Add(Me.CareLabel5)
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Controls.Add(Me.CareLabel4)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.txtClauseField1)
        Me.GroupControl1.Controls.Add(Me.cbxClauseOperator)
        Me.GroupControl1.Controls.Add(Me.cbxClauseDataType)
        Me.GroupControl1.Controls.Add(Me.txtClauseSeq)
        Me.GroupControl1.Controls.Add(Me.chkClauseMandatory)
        Me.GroupControl1.Controls.Add(Me.CareLabel11)
        Me.GroupControl1.Controls.Add(Me.txtClauseField2)
        Me.GroupControl1.Controls.Add(Me.txtClauseLabel)
        Me.GroupControl1.Controls.Add(Me.cbxClauseType)
        Me.GroupControl1.Controls.Add(Me.label22)
        Me.GroupControl1.Controls.Add(Me.CareLabel22)
        Me.GroupControl1.Controls.Add(Me.CareLabel14)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(810, 278)
        Me.GroupControl1.TabIndex = 0
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(8, 151)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(79, 15)
        Me.CareLabel6.TabIndex = 14
        Me.CareLabel6.Text = "Lookup Source"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel6.ToolTip = resources.GetString("CareLabel6.ToolTip")
        Me.CareLabel6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.CareLabel6.ToolTipTitle = "Lookup Source"
        '
        'txtClauseLookup
        '
        Me.txtClauseLookup.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtClauseLookup.Location = New System.Drawing.Point(146, 151)
        Me.txtClauseLookup.Name = "txtClauseLookup"
        Me.txtClauseLookup.Properties.AccessibleName = "Lookup Source"
        Me.txtClauseLookup.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClauseLookup.Properties.Appearance.Options.UseFont = True
        Me.txtClauseLookup.Properties.AppearanceFocused.Font = New System.Drawing.Font("Lucida Console", 9.75!)
        Me.txtClauseLookup.Properties.AppearanceFocused.Options.UseFont = True
        Me.txtClauseLookup.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Lucida Console", 9.75!)
        Me.txtClauseLookup.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtClauseLookup, True)
        Me.txtClauseLookup.Size = New System.Drawing.Size(653, 89)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtClauseLookup, OptionsSpelling1)
        Me.txtClauseLookup.TabIndex = 15
        Me.txtClauseLookup.Tag = "AE"
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(8, 249)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(51, 15)
        Me.CareLabel5.TabIndex = 16
        Me.CareLabel5.Text = "Sequence"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(420, 70)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(47, 15)
        Me.CareLabel3.TabIndex = 8
        Me.CareLabel3.Text = "Operator"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(8, 70)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(53, 15)
        Me.CareLabel4.TabIndex = 6
        Me.CareLabel4.Text = "Data Type"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(8, 126)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(34, 15)
        Me.CareLabel2.TabIndex = 12
        Me.CareLabel2.Text = "Field 2"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtClauseField1
        '
        Me.txtClauseField1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtClauseField1.CharacterCasing = CharacterCasing.Normal
        Me.txtClauseField1.EnterMoveNextControl = True
        Me.txtClauseField1.Location = New System.Drawing.Point(146, 95)
        Me.txtClauseField1.MaxLength = 100
        Me.txtClauseField1.Name = "txtClauseField1"
        Me.txtClauseField1.NumericAllowNegatives = False
        Me.txtClauseField1.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtClauseField1.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtClauseField1.Properties.AccessibleName = "Field 1"
        Me.txtClauseField1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtClauseField1.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClauseField1.Properties.Appearance.Options.UseFont = True
        Me.txtClauseField1.Properties.Appearance.Options.UseTextOptions = True
        Me.txtClauseField1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtClauseField1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtClauseField1.Properties.MaxLength = 100
        Me.txtClauseField1.Size = New System.Drawing.Size(653, 20)
        Me.txtClauseField1.TabIndex = 11
        Me.txtClauseField1.Tag = "AEM"
        Me.txtClauseField1.TextAlign = HorizontalAlignment.Left
        Me.txtClauseField1.ToolTipText = ""
        '
        'cbxClauseOperator
        '
        Me.cbxClauseOperator.AllowBlank = False
        Me.cbxClauseOperator.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxClauseOperator.DataSource = Nothing
        Me.cbxClauseOperator.DisplayMember = Nothing
        Me.cbxClauseOperator.EnterMoveNextControl = True
        Me.cbxClauseOperator.Location = New System.Drawing.Point(484, 67)
        Me.cbxClauseOperator.Name = "cbxClauseOperator"
        Me.cbxClauseOperator.Properties.AccessibleName = "Operator"
        Me.cbxClauseOperator.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxClauseOperator.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxClauseOperator.Properties.Appearance.Options.UseFont = True
        Me.cbxClauseOperator.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxClauseOperator.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxClauseOperator.SelectedValue = Nothing
        Me.cbxClauseOperator.Size = New System.Drawing.Size(315, 22)
        Me.cbxClauseOperator.TabIndex = 9
        Me.cbxClauseOperator.Tag = "AE"
        Me.cbxClauseOperator.ValueMember = Nothing
        '
        'cbxClauseDataType
        '
        Me.cbxClauseDataType.AllowBlank = False
        Me.cbxClauseDataType.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxClauseDataType.DataSource = Nothing
        Me.cbxClauseDataType.DisplayMember = Nothing
        Me.cbxClauseDataType.EnterMoveNextControl = True
        Me.cbxClauseDataType.Location = New System.Drawing.Point(146, 67)
        Me.cbxClauseDataType.Name = "cbxClauseDataType"
        Me.cbxClauseDataType.Properties.AccessibleName = "Data Type"
        Me.cbxClauseDataType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxClauseDataType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxClauseDataType.Properties.Appearance.Options.UseFont = True
        Me.cbxClauseDataType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxClauseDataType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxClauseDataType.SelectedValue = Nothing
        Me.cbxClauseDataType.Size = New System.Drawing.Size(226, 22)
        Me.cbxClauseDataType.TabIndex = 7
        Me.cbxClauseDataType.Tag = "AEM"
        Me.cbxClauseDataType.ValueMember = Nothing
        '
        'txtClauseSeq
        '
        Me.txtClauseSeq.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtClauseSeq.CharacterCasing = CharacterCasing.Normal
        Me.txtClauseSeq.EnterMoveNextControl = True
        Me.txtClauseSeq.Location = New System.Drawing.Point(146, 246)
        Me.txtClauseSeq.MaxLength = 5
        Me.txtClauseSeq.Name = "txtClauseSeq"
        Me.txtClauseSeq.NumericAllowNegatives = False
        Me.txtClauseSeq.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtClauseSeq.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtClauseSeq.Properties.AccessibleName = "Sequence"
        Me.txtClauseSeq.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtClauseSeq.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtClauseSeq.Properties.Appearance.Options.UseFont = True
        Me.txtClauseSeq.Properties.Appearance.Options.UseTextOptions = True
        Me.txtClauseSeq.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtClauseSeq.Properties.Mask.EditMask = "##########;"
        Me.txtClauseSeq.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtClauseSeq.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtClauseSeq.Properties.MaxLength = 5
        Me.txtClauseSeq.Size = New System.Drawing.Size(71, 22)
        Me.txtClauseSeq.TabIndex = 17
        Me.txtClauseSeq.Tag = "AEM"
        Me.txtClauseSeq.TextAlign = HorizontalAlignment.Left
        Me.txtClauseSeq.ToolTipText = ""
        '
        'chkClauseMandatory
        '
        Me.chkClauseMandatory.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.chkClauseMandatory.EnterMoveNextControl = True
        Me.chkClauseMandatory.Location = New System.Drawing.Point(484, 40)
        Me.chkClauseMandatory.Name = "chkClauseMandatory"
        Me.chkClauseMandatory.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkClauseMandatory.Properties.Appearance.Options.UseFont = True
        Me.chkClauseMandatory.Size = New System.Drawing.Size(19, 19)
        Me.chkClauseMandatory.TabIndex = 5
        Me.chkClauseMandatory.Tag = "AE"
        '
        'CareLabel11
        '
        Me.CareLabel11.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(420, 42)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(58, 15)
        Me.CareLabel11.TabIndex = 4
        Me.CareLabel11.Text = "Mandatory"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtClauseField2
        '
        Me.txtClauseField2.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtClauseField2.CharacterCasing = CharacterCasing.Normal
        Me.txtClauseField2.EnterMoveNextControl = True
        Me.txtClauseField2.Location = New System.Drawing.Point(146, 123)
        Me.txtClauseField2.MaxLength = 100
        Me.txtClauseField2.Name = "txtClauseField2"
        Me.txtClauseField2.NumericAllowNegatives = False
        Me.txtClauseField2.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtClauseField2.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtClauseField2.Properties.AccessibleName = "Field 2"
        Me.txtClauseField2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtClauseField2.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 9.75!)
        Me.txtClauseField2.Properties.Appearance.Options.UseFont = True
        Me.txtClauseField2.Properties.Appearance.Options.UseTextOptions = True
        Me.txtClauseField2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtClauseField2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtClauseField2.Properties.MaxLength = 100
        Me.txtClauseField2.Size = New System.Drawing.Size(653, 20)
        Me.txtClauseField2.TabIndex = 13
        Me.txtClauseField2.Tag = "AE"
        Me.txtClauseField2.TextAlign = HorizontalAlignment.Left
        Me.txtClauseField2.ToolTipText = ""
        '
        'txtClauseLabel
        '
        Me.txtClauseLabel.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtClauseLabel.CharacterCasing = CharacterCasing.Normal
        Me.txtClauseLabel.EnterMoveNextControl = True
        Me.txtClauseLabel.Location = New System.Drawing.Point(146, 11)
        Me.txtClauseLabel.MaxLength = 40
        Me.txtClauseLabel.Name = "txtClauseLabel"
        Me.txtClauseLabel.NumericAllowNegatives = False
        Me.txtClauseLabel.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtClauseLabel.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtClauseLabel.Properties.AccessibleName = "Label"
        Me.txtClauseLabel.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtClauseLabel.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtClauseLabel.Properties.Appearance.Options.UseFont = True
        Me.txtClauseLabel.Properties.Appearance.Options.UseTextOptions = True
        Me.txtClauseLabel.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtClauseLabel.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtClauseLabel.Properties.MaxLength = 40
        Me.txtClauseLabel.Size = New System.Drawing.Size(653, 22)
        Me.txtClauseLabel.TabIndex = 1
        Me.txtClauseLabel.Tag = "AEM"
        Me.txtClauseLabel.TextAlign = HorizontalAlignment.Left
        Me.txtClauseLabel.ToolTipText = ""
        '
        'cbxClauseType
        '
        Me.cbxClauseType.AllowBlank = False
        Me.cbxClauseType.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxClauseType.DataSource = Nothing
        Me.cbxClauseType.DisplayMember = Nothing
        Me.cbxClauseType.EnterMoveNextControl = True
        Me.cbxClauseType.Location = New System.Drawing.Point(146, 39)
        Me.cbxClauseType.Name = "cbxClauseType"
        Me.cbxClauseType.Properties.AccessibleName = "Clause Type"
        Me.cbxClauseType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxClauseType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxClauseType.Properties.Appearance.Options.UseFont = True
        Me.cbxClauseType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxClauseType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxClauseType.SelectedValue = Nothing
        Me.cbxClauseType.Size = New System.Drawing.Size(226, 22)
        Me.cbxClauseType.TabIndex = 3
        Me.cbxClauseType.Tag = "AEM"
        Me.cbxClauseType.ValueMember = Nothing
        '
        'label22
        '
        Me.label22.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.label22.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.label22.Location = New System.Drawing.Point(8, 98)
        Me.label22.Name = "label22"
        Me.label22.Size = New System.Drawing.Size(34, 15)
        Me.label22.TabIndex = 10
        Me.label22.Text = "Field 1"
        Me.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel22
        '
        Me.CareLabel22.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel22.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel22.Location = New System.Drawing.Point(8, 42)
        Me.CareLabel22.Name = "CareLabel22"
        Me.CareLabel22.Size = New System.Drawing.Size(64, 15)
        Me.CareLabel22.TabIndex = 2
        Me.CareLabel22.Text = "Clause Type"
        Me.CareLabel22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(8, 14)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(28, 15)
        Me.CareLabel14.TabIndex = 0
        Me.CareLabel14.Text = "Label"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.CausesValidation = False
        Me.btnCancel.DialogResult = DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(748, 299)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(74, 25)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Tag = ""
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.CausesValidation = False
        Me.btnOK.Location = New System.Drawing.Point(668, 299)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(74, 25)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Tag = ""
        Me.btnOK.Text = "OK"
        '
        'frmReportClause
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(834, 332)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.GroupControl1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmReportClause"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "frmReportClause"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtClauseLookup.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClauseField1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxClauseOperator.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxClauseDataType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClauseSeq.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkClauseMandatory.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClauseField2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClauseLabel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxClauseType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents chkClauseMandatory As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents txtClauseField2 As Care.Controls.CareTextBox
    Friend WithEvents txtClauseLabel As Care.Controls.CareTextBox
    Friend WithEvents cbxClauseType As Care.Controls.CareComboBox
    Friend WithEvents label22 As Care.Controls.CareLabel
    Friend WithEvents CareLabel22 As Care.Controls.CareLabel
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents txtClauseField1 As Care.Controls.CareTextBox
    Friend WithEvents cbxClauseOperator As Care.Controls.CareComboBox
    Friend WithEvents cbxClauseDataType As Care.Controls.CareComboBox
    Friend WithEvents txtClauseSeq As Care.Controls.CareTextBox
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents txtClauseLookup As DevExpress.XtraEditors.MemoEdit
End Class
