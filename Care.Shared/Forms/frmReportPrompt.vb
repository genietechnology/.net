﻿Option Strict On

Imports System.Windows.Forms
Imports Care.Global

Public Class frmReportPrompt

    Private m_Clauses As List(Of Business.AppReportClause)
    Private m_ClauseCount As Integer

    Private m_ClauseNo As Integer
    Private m_CurrentClause As Business.AppReportClause = Nothing

    Private m_ClauseSQL As New List(Of String)
    Private m_ComboMultiColumn As Boolean = False

    Private Enum EnumValueType
        SingleValue
        PairFrom
        PairTo
    End Enum

    Public Sub New(ByVal Clauses As List(Of Business.AppReportClause))

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_Clauses = Clauses
        m_ClauseNo = 1
        m_ClauseCount = Clauses.Count

    End Sub

    Public ReadOnly Property GeneratedClauses As String
        Get

            Dim _Return As String = ""

            For Each _Clause In m_ClauseSQL
                _Return += _Clause
            Next

            Return _Return

        End Get
    End Property

    Private Sub frmReportPrompt_Load(sender As Object, e As EventArgs) Handles Me.Load

        lblMandatory.Text = ""

        gbxTextbox.Top = 12
        gbxCombo.Top = 12
        gbxDate.Top = 12
        gbxDateRange.Top = 12
        gbxRange.Top = 12

        Me.Height = 152

        'set the blank clauses
        For Each _C In m_Clauses
            m_ClauseSQL.Add("")
        Next

        SetControls()

    End Sub

    Private Sub ClearValues()

        cdtValue.Value = Nothing
        cdtValue1.Value = Nothing
        cdtValue2.Value = Nothing

        txtValue.Text = ""
        txtValueFrom.Text = ""
        txtValueTo.Text = ""

        cbxValue.Clear()
        cbxValue.SelectedIndex = -1

    End Sub

    Private Sub SetControls()

        m_CurrentClause = m_Clauses(m_ClauseNo - 1)

        If m_CurrentClause._ClauseMandatory Then
            lblMandatory.Text = "This clause is mandatory."
            lblMandatory.ForeColor = Drawing.Color.Red
        Else
            lblMandatory.Text = "This clause is optional."
            lblMandatory.ForeColor = Drawing.Color.RoyalBlue
        End If

        gbxTextbox.Hide()
        gbxCombo.Hide()
        gbxDate.Hide()
        gbxDateRange.Hide()
        gbxRange.Hide()

        lblCombo.Text = m_CurrentClause._ClauseLabel
        lblTextbox.Text = m_CurrentClause._ClauseLabel
        lblDate.Text = m_CurrentClause._ClauseLabel
        lblDateRange.Text = m_CurrentClause._ClauseLabel
        lblRange.Text = m_CurrentClause._ClauseLabel

        ClearValues()

        If m_CurrentClause._ClauseLookup <> "" Then

            m_ComboMultiColumn = False

            If m_CurrentClause._ClauseLookup.StartsWith("[") AndAlso m_CurrentClause._ClauseLookup.EndsWith("]") Then
                Dim _ListName As String = m_CurrentClause._ClauseLookup
                _ListName = _ListName.Replace("[", "")
                _ListName = _ListName.Replace("]", "")
                cbxValue.PopulateFromListMaintenance(ListHandler.ReturnListFromCache(_ListName))
            Else

                If m_CurrentClause._ClauseLookup.StartsWith("<") AndAlso m_CurrentClause._ClauseLookup.EndsWith(">") Then

                    Dim _ItemsText As String = m_CurrentClause._ClauseLookup
                    _ItemsText = _ItemsText.Replace("<", "")
                    _ItemsText = _ItemsText.Replace(">", "")

                    Dim _Items As String() = _ItemsText.Split(CChar("|"))
                    For Each _Item In _Items
                        cbxValue.AddItem(_Item)
                    Next

                Else
                    'bit bad, but if we have a comma on the SQL statement - we must have more than one column
                    If m_CurrentClause._ClauseLookup.Contains(",") Then m_ComboMultiColumn = True
                    cbxValue.PopulateWithSQL(Session.ConnectionString, m_CurrentClause._ClauseLookup)
                End If
            End If

            gbxCombo.Show()
            cbxValue.Focus()

        Else

            Select Case m_CurrentClause._ClauseDatatype

                Case "Date (00:00 to 23:59)", "Date"
                    If m_CurrentClause._ClauseOperator = "between" Or m_CurrentClause._ClauseOperator = "not between" Then
                        gbxDateRange.Show()
                        cdtValue1.Focus()
                    Else
                        gbxDate.Show()
                        cdtValue.Focus()
                    End If

                Case "String", "Numeric"
                    If m_CurrentClause._ClauseOperator = "between" Or m_CurrentClause._ClauseOperator = "not between" Then
                        gbxRange.Show()
                        txtValueFrom.Focus()
                    Else
                        gbxTextbox.Show()
                        txtValue.Focus()
                    End If

            End Select

        End If

        If m_ClauseNo = 1 Then
            btnPrevious.Enabled = False
        Else
            btnPrevious.Enabled = True
        End If

        If m_ClauseNo = m_ClauseCount Then
            btnNext.Text = "Run"
        Else
            btnNext.Text = "Next >"
        End If

    End Sub

    Private Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNext.Click

        If m_CurrentClause._ClauseMandatory Then

            Dim _Errors As Integer = 0
            Select m_CurrentClause._ClauseOperator

                Case "between", "not between"
                    If m_CurrentClause._ClauseDatatype.Contains("Date") Then
                        _Errors += GetValue(Of Care.Controls.CareDateTime)("", cdtValue1)
                        _Errors += GetValue(Of Care.Controls.CareDateTime)("", cdtValue2)
                    Else
                        _Errors += GetValue(Of Care.Controls.CareTextBox)("", txtValueFrom)
                        _Errors += GetValue(Of Care.Controls.CareTextBox)("", txtValueTo)
                    End If

                Case Else
                    If m_CurrentClause._ClauseDatatype.Contains("Date") Then
                        _Errors += GetValue(Of Care.Controls.CareDateTime)("", cdtValue)
                    Else
                        If m_CurrentClause._ClauseLookup <> "" Then
                            _Errors += GetValue(Of Care.Controls.CareComboBox)("", cbxValue)
                        Else
                            _Errors += GetValue(Of Care.Controls.CareTextBox)("", txtValue)
                        End If
                    End If

            End Select

            If _Errors > 0 Then
                CareMessage("This report criteria is mandatory. Please enter a value.", MessageBoxIcon.Exclamation, "Criteria Mandatory")
                Exit Sub
            End If

        End If

        GenerateClause(m_ClauseNo)

        If m_ClauseNo = m_ClauseCount Then
            If btnNext.ShiftDown Then Clipboard.SetText(Me.GeneratedClauses)
            Me.DialogResult = DialogResult.OK
            Me.Close()
        Else
            m_ClauseNo += 1
            SetControls()
        End If

    End Sub

    Private Sub GenerateClause(ByVal ClauseNo As Integer)

        Dim _Errors As Integer = 0

        Dim _SQL As String = ""
        _SQL += " and "
        _SQL += m_CurrentClause._ClauseField1

        Dim _Value1 As String = ""
        Dim _Value2 As String = ""

        Select Case m_CurrentClause._ClauseOperator

            Case "between", "not between"

                If m_CurrentClause._ClauseDatatype.Contains("Date") Then
                    _Errors += GetValue(Of Care.Controls.CareDateTime)(_Value1, cdtValue1)
                    _Errors += GetValue(Of Care.Controls.CareDateTime)(_Value2, cdtValue2)
                Else
                    _Errors += GetValue(Of Care.Controls.CareTextBox)(_Value1, txtValueFrom)
                    _Errors += GetValue(Of Care.Controls.CareTextBox)(_Value2, txtValueTo)
                End If

                If _Errors = 0 Then
                    _SQL += " " + m_CurrentClause._ClauseOperator + " '" + _Value1 + "' and '" + _Value2 + "'"
                End If

            Case "like", "not like"
                _Errors += GetValue(Of Care.Controls.CareTextBox)(_Value1, txtValue)
                If _Errors = 0 Then _SQL += " " + m_CurrentClause._ClauseOperator + " '%" + _Value1 + "%'"

            Case Else

                If m_CurrentClause._ClauseDatatype.Contains("Date") Then
                    _Errors += GetValue(Of Care.Controls.CareDateTime)(_Value1, cdtValue)
                Else
                    If m_CurrentClause._ClauseLookup <> "" Then
                        _Errors += GetValue(Of Care.Controls.CareComboBox)(_Value1, cbxValue)
                    Else
                        _Errors += GetValue(Of Care.Controls.CareTextBox)(_Value1, txtValue)
                    End If
                End If

                If _Errors = 0 Then _SQL += " " + m_CurrentClause._ClauseOperator + " '" + _Value1 + "'"

        End Select

        If _Errors > 0 Then _SQL = ""
        m_ClauseSQL(m_ClauseNo - 1) = _SQL

    End Sub

    Private Function GetValue(Of T)(ByRef ReturnValue As String, ByVal TargetControl As Object) As Integer

        If TypeOf (TargetControl) Is Care.Controls.CareDateTime Then
            Dim _DC As Care.Controls.CareDateTime = CType(TargetControl, Care.Controls.CareDateTime)

            If FormatValue(ReturnValue, _DC.Value) = 0 Then

                If m_CurrentClause._ClauseDatatype = "Date (00:00 to 23:59)" Then
                    If _DC.Name = "cdtValue1" Then
                        ReturnValue += " 00:00:00.000"
                    Else
                        ReturnValue += " 23:59:59.000"
                    End If
                End If

                Return 0

            Else
                Return 1
            End If
        End If

        If TypeOf (TargetControl) Is Care.Controls.CareComboBox Then

            Dim _cbx As Care.Controls.CareComboBox = CType(TargetControl, Care.Controls.CareComboBox)

            Dim _Text As String = cbxValue.Text
            If m_ComboMultiColumn AndAlso cbxValue.SelectedValue IsNot Nothing Then _Text = cbxValue.SelectedValue.ToString

            If FormatValue(ReturnValue, _Text) = 0 Then
                Return 0
            Else
                Return 1
            End If

        End If

        If TypeOf (TargetControl) Is Care.Controls.CareTextBox Then
            Dim _txt As Care.Controls.CareTextBox = CType(TargetControl, Care.Controls.CareTextBox)
            If FormatValue(ReturnValue, _txt.Text) = 0 Then
                Return 0
            Else
                Return 1
            End If
        End If

        Return 0

    End Function

    Private Function FormatValue(ByRef ReturnValue As String, ByVal ObjectIn As Object) As Integer

        If ObjectIn Is Nothing Then Return 1
        If ObjectIn.ToString = "" Then Return 1

        Select Case m_CurrentClause._ClauseDatatype

            Case "Date (00:00 to 23:59)", "Date"
                ReturnValue = ValueHandler.SQLDate(CDate(ObjectIn))
                Return 0

            Case Else
                ReturnValue = ObjectIn.ToString
                Return 0

        End Select

    End Function

    Private Sub btnPrevious_Click(sender As Object, e As EventArgs) Handles btnPrevious.Click
        m_ClauseNo -= 1
        SetControls()
    End Sub

End Class
