﻿Imports Care.Global

Public Class frmPassword

    Private m_Title As String = "Manager Authorisation"
    Private m_Password As String = ""

    Public Sub New(ByVal Password As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_Password = Password
        Me.Text = m_Title

    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnOK_Click(sender As System.Object, e As System.EventArgs) Handles btnOK.Click
        If PasswordOK() Then
            Me.DialogResult = DialogResult.OK
            Me.Close()
        End If
    End Sub

    Private Function PasswordOK() As Boolean

        If txtPassword.Text = "" Then
            CareMessage("Please enter the password.", MessageBoxIcon.Exclamation, m_Title)
            txtPassword.Focus()
            Return False
        End If

        If txtPassword.Text <> m_Password Then
            CareMessage("Invalid password.", MessageBoxIcon.Exclamation, m_Title)
            txtPassword.Text = ""
            txtPassword.Focus()
            Return False
        End If

        Return True

    End Function

End Class
