﻿Imports Care.Global
Imports Care.Data
Imports Care.Shared

Public Class frmUsers

    Private m_User As Business.User = Nothing

    Private Sub frmUsers_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim _SQL As String = ""
        _SQL += "select id, username as 'Username', fullname as 'Name', type as 'Type',"
        _SQL += " email as 'Email', sso as 'Windows Crd.', sso_username as 'Domain\Username', disabled as 'Disabled'"
        _SQL += " from AppUsers order by fullname"

        Me.GridSQL = _SQL
        gbx.Hide()

        cbxType.AddItem("User")
        cbxType.AddItem("Administrator")
        cbxType.AddItem("Super Administrator")

    End Sub

    Protected Overrides Sub FormatGrid()
        MyBase.Grid.AutoSizeColumns()
    End Sub

    Protected Overrides Sub SetBindings()

        m_User = New Business.User
        bs.DataSource = m_User

        chkDisabled.DataBindings.Add("Checked", bs, "_Disabled")
        txtUserName.DataBindings.Add("Text", bs, "_Username")
        txtForename.DataBindings.Add("Text", bs, "_Forename")
        txtSurname.DataBindings.Add("Text", bs, "_Surname")

        txtJobTitle.DataBindings.Add("Text", bs, "_JobTitle")
        txtDepartment.DataBindings.Add("Text", bs, "_Department")
        txtTel.DataBindings.Add("Text", bs, "_Tel")
        txtEmail.DataBindings.Add("Text", bs, "_Email")

        chkSSO.DataBindings.Add("Checked", bs, "_Sso")
        txtSSOUsername.DataBindings.Add("Text", bs, "_SsoUsername")

    End Sub

    Protected Overrides Sub BindToID(ByVal ID As System.Guid, ByVal IsNew As Boolean)

        If IsNew Then
            m_User = New Business.User
        Else
            m_User = Business.User.RetreiveByID(ID)
        End If

        bs.DataSource = m_User

        cbxType.Text = m_User._Type

    End Sub

    Protected Overrides Function BeforeCommitUpdate() As Boolean
        Return MyBase.BeforeCommitUpdate()
        If txtUserName.Text = "" Then txtUserName.Text = txtForename.Text.ToLower
    End Function

    Protected Overrides Sub CommitUpdate()

        If m_User._Username = "admin" Then
            m_User._Forename = "System"
            m_User._Surname = "Administrator"
        End If

        m_User._Fullname = m_User._Forename + " " + m_User._Surname
        m_User._Type = cbxType.Text

        Business.User.SaveRecord(m_User)

    End Sub

    Protected Overrides Sub AfterAdd()
        cbxType.SelectedIndex = 0
        txtForename.Focus()
    End Sub

    Protected Overrides Sub AfterEdit()
        txtForename.Focus()
    End Sub

    Protected Overrides Sub CommitDelete(ID As System.Guid)
        Dim _SQL As String = "delete from AppUsers where ID = '" + ID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)
    End Sub

    Private Sub btnReset_Click(sender As System.Object, e As System.EventArgs) Handles btnReset.Click

        If CareMessage("Are you sure you want to execute a password reset?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Password Reset") = Windows.Forms.DialogResult.Yes Then
            m_User._Password = ""
            Business.User.SaveRecord(m_User)
            Cancel()
        End If

    End Sub

    Private Sub txtUserName_GotFocus(sender As Object, e As System.EventArgs) Handles txtUserName.GotFocus
        If txtForename.Text <> "" And txtUserName.Text = "" Then txtUserName.Text = txtForename.Text.ToLower
    End Sub

End Class