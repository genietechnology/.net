﻿Imports Care.Global

Public Class frmParameters

    Private m_Parameter As Care.Shared.Business.Parameter

    Private Sub frmParameters_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        cbxType.Clear()
        cbxType.AddItem("String")
        cbxType.AddItem("Boolean")
        cbxType.AddItem("Date")
        cbxType.AddItem("Integer")
        cbxType.AddItem("Long")
        cbxType.AddItem("Decimal")
        cbxType.AddItem("Path")
        cbxType.AddItem("Password")

        Me.GridSQL = "select id, parent as 'Parent', name as 'Name', description as 'Description'," & _
                     " type as 'Type', value as 'Value'" & _
                     " from AppParams" & _
                     " order by Parent, Description"

        gbx.Hide()

    End Sub

    Protected Overrides Sub FormatGrid()

        MyBase.FormatGrid()

        Dim _SIs As New List(Of DevExpress.XtraGrid.Columns.GridColumnSortInfo)
        Dim _SI As New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Grid.UnderlyingGridView.Columns("Parent"), DevExpress.Data.ColumnSortOrder.Ascending)
        _SIs.Add(_SI)

        Grid.UnderlyingGridView.SortInfo.ClearAndAddRange(_SIs.ToArray, 1)

    End Sub

    Protected Overrides Sub AfterEdit()

        cbxType.SelectedValue = m_Parameter._Type

        If m_Parameter._Type = "Password" Then
            txtValue.Text = EncyptionHandler.DecryptString(m_Parameter._Value)
        Else
            txtValue.Text = m_Parameter._Value
        End If

    End Sub

    Protected Overrides Sub SetBindings()

        m_Parameter = New Care.Shared.Business.Parameter
        bs.DataSource = m_Parameter

        txtParent.DataBindings.Add("Text", bs, "_Parent")
        txtName.DataBindings.Add("Text", bs, "_Name")
        txtDescription.DataBindings.Add("Text", bs, "_Description")

    End Sub

    Protected Overrides Sub BindToID(ByVal ID As System.Guid, ByVal IsNew As Boolean)
        If IsNew Then
            m_Parameter = New Care.Shared.Business.Parameter
        Else
            m_Parameter = Care.Shared.Business.Parameter.RetreiveByID(ID)
        End If
        bs.DataSource = m_Parameter
    End Sub

    Protected Overrides Sub CommitUpdate()

        Dim _record As Care.Shared.Business.Parameter = CType(bs.Item(bs.Position), Care.Shared.Business.Parameter)
        _record._Type = cbxType.SelectedValue.ToString

        If _record._Type = "Password" Then
            _record._Value = EncyptionHandler.EncryptString(txtValue.Text)
        Else
            _record._Value = txtValue.Text
        End If

        Care.Shared.Business.Parameter.SaveRecord(_record)

        _record = Nothing

    End Sub

    Protected Overrides Sub CommitDelete(ID As System.Guid)
        MyBase.CommitDelete(ID)
        Care.Shared.Business.Parameter.DeleteRecord(ID)
    End Sub

End Class
