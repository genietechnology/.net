﻿Imports System.Windows.Forms
Imports Care.Global
Imports DevExpress.XtraReports.UI

Public Class frmReportPreview

    Private m_Report As XtraReport

    Public Sub New(ReportIn As XtraReport)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_Report = ReportIn

    End Sub

    Private Sub frmReportPreview_Load(sender As Object, e As EventArgs) Handles Me.Load

        m_Report.CreateDocument()
        Me.DocumentViewer1.DocumentSource = m_Report

        If Session.CurrentUser.Email Is Nothing OrElse Session.CurrentUser.Email = "" Then bbiEmailMe.Enabled = False

    End Sub

    Private Sub bbiEmailMe_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiEmailMe.ItemClick

        bbiEmailMe.Enabled = False
        Session.CursorWaiting()

        Dim _PDF As String = ExportToPDF()
        If _PDF <> "" Then
            EmailHandler.SendEmailWithAttachment(Nothing, Session.CurrentUser.Email, "Exported Report", "Please see the attached report.", _PDF)
        End If

        bbiEmailMe.Enabled = True
        Session.CursorDefault()

    End Sub

    Private Function ExportToPDF() As String

        Dim _Path As String = Session.TempFolder + "Export_" + Format(Now, "ddMMyyyyHHmmss") + ".pdf"

        Try
            m_Report.ExportToPdf(_Path)

        Catch ex As Exception
            Return ""
        End Try

        If IO.File.Exists(_Path) Then
            Return _Path
        Else
            Return ""
        End If

    End Function

    Private Sub bbiEmail_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiEmail.ItemClick
        Dim _PDF As String = ExportToPDF()
        If _PDF <> "" Then
            Session.CursorWaiting()
            EmailHandler.PreviewEmail(Nothing, "", "Exported Report", "Please see the attached report.", _PDF)
        End If
    End Sub

    Private Sub frmReportPreview_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub
End Class