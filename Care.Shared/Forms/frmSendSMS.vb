﻿Imports Care.Global
Imports System.Windows.Forms

Public Class frmSendSMS

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub New(ByVal Number As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        txtTo.Text = Number

    End Sub

#Region "Properties"

    Public ReadOnly Property Message As String
        Get
            Return txtBody.Text
        End Get
    End Property

#End Region

    Private Sub btnSend_Click(sender As System.Object, e As System.EventArgs) Handles btnSend.Click
        SendSMS()
    End Sub

    Private Sub SendSMS()

        If txtTo.Text = "" Then Exit Sub
        If txtBody.Text = "" Then Exit Sub
        If Not ValidateMobile(txtTo.Text, True) Then Exit Sub

        btnSend.Enabled = False

        Dim _e As New Care.Shared.SMSHandler
        With _e
            .MobileNumber = txtTo.Text
            .Message = txtBody.Text
            .Send()
        End With

        _e = Nothing

        btnSend.Enabled = True
        Me.DialogResult = DialogResult.OK

    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.DialogResult = DialogResult.Cancel
    End Sub

    Private Sub frmSendEmail_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If

        If e.KeyCode = Keys.Enter And e.Control Then
            SendSMS()
        End If

    End Sub

    Private Function ValidateMobile(ByRef MobileNo As String, Interact As Boolean) As Boolean

        If MobileNo = "" Then Return False
        Dim _Return As Boolean = True

        MobileNo = MobileNo.Replace(" ", "")

        If MobileNo.Length = 11 Then
            If Not MobileNo.StartsWith("07") Then
                If Interact Then CareMessage("This does not appear to be a mobile number.", MessageBoxIcon.Exclamation, "Validate Number")
                _Return = False
            End If
        Else
            If Interact Then CareMessage("This mobile number is not the correct length.", MessageBoxIcon.Exclamation, "Validate Number")
            _Return = False
        End If

        Return _Return

    End Function

    Private Sub txtBody_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtBody.TextChanged
        If txtBody.Text.Length = 0 Then
            lblChars.Text = "No characters used."
        Else
            lblChars.Text = txtBody.Text.Length.ToString + " characters used."
        End If
    End Sub

    Private Sub frmSendSMS_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        lblChars.Text = ""
    End Sub

    Private Sub txtTo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTo.KeyPress
        If e.KeyChar = vbBack Then Exit Sub
        If Not Char.IsNumber(e.KeyChar) Then e.Handled = True
    End Sub

    Private Sub frmSendSMS_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        If txtTo.Text = "" Then
            txtTo.Focus()
        Else
            txtBody.Focus()
        End If
    End Sub
End Class