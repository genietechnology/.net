﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditQuery
    Inherits frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling2 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.btnSession = New Care.Controls.CareButton()
        Me.CareLabel3 = New Care.Controls.CareLabel()
        Me.CareLabel2 = New Care.Controls.CareLabel()
        Me.txtConnectionString = New DevExpress.XtraEditors.MemoEdit()
        Me.cbxType = New Care.Controls.CareComboBox()
        Me.txtTitle = New Care.Controls.CareTextBox()
        Me.CareLabel1 = New Care.Controls.CareLabel()
        Me.GroupControl2 = New Care.Controls.CareFrame()
        Me.txtQuery = New DevExpress.XtraEditors.MemoEdit()
        Me.Panel1 = New Panel()
        Me.btnDelete = New Care.Controls.CareButton()
        Me.btnOK = New Care.Controls.CareButton()
        Me.btnCancel = New Care.Controls.CareButton()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtConnectionString.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTitle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.txtQuery.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.btnSession)
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.txtConnectionString)
        Me.GroupControl1.Controls.Add(Me.cbxType)
        Me.GroupControl1.Controls.Add(Me.txtTitle)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(615, 196)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Query Properties"
        '
        'btnSession
        '
        Me.btnSession.Location = New System.Drawing.Point(12, 110)
        Me.btnSession.Name = "btnSession"
        Me.btnSession.Size = New System.Drawing.Size(106, 23)
        Me.btnSession.TabIndex = 3
        Me.btnSession.Text = "Use Session"
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(12, 89)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(96, 15)
        Me.CareLabel3.TabIndex = 6
        Me.CareLabel3.Text = "Connection String"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(12, 63)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(26, 15)
        Me.CareLabel2.TabIndex = 5
        Me.CareLabel2.Text = "Type"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtConnectionString
        '
        Me.txtConnectionString.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtConnectionString.Location = New System.Drawing.Point(124, 88)
        Me.txtConnectionString.Name = "txtConnectionString"
        Me.txtConnectionString.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtConnectionString.Properties.Appearance.Options.UseFont = True
        Me.txtConnectionString.Properties.MaxLength = 100
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtConnectionString, True)
        Me.txtConnectionString.Size = New System.Drawing.Size(479, 96)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtConnectionString, OptionsSpelling1)
        Me.txtConnectionString.TabIndex = 2
        Me.txtConnectionString.UseOptimizedRendering = True
        '
        'cbxType
        '
        Me.cbxType.AllowBlank = False
        Me.cbxType.DataSource = Nothing
        Me.cbxType.DisplayMember = Nothing
        Me.cbxType.EnterMoveNextControl = True
        Me.cbxType.Location = New System.Drawing.Point(124, 60)
        Me.cbxType.Name = "cbxType"
        Me.cbxType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxType.Properties.Appearance.Options.UseFont = True
        Me.cbxType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxType.ReadOnly = False
        Me.cbxType.SelectedValue = Nothing
        Me.cbxType.Size = New System.Drawing.Size(178, 20)
        Me.cbxType.TabIndex = 1
        Me.cbxType.ValueMember = Nothing
        '
        'txtTitle
        '
        Me.txtTitle.CharacterCasing = CharacterCasing.Normal
        Me.txtTitle.EnterMoveNextControl = True
        Me.txtTitle.Location = New System.Drawing.Point(124, 32)
        Me.txtTitle.MaxLength = 40
        Me.txtTitle.Name = "txtTitle"
        Me.txtTitle.NumericAllowNegatives = False
        Me.txtTitle.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtTitle.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTitle.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtTitle.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtTitle.Properties.Appearance.Options.UseFont = True
        Me.txtTitle.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTitle.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtTitle.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTitle.Properties.MaxLength = 40
        Me.txtTitle.ReadOnly = False
        Me.txtTitle.Size = New System.Drawing.Size(479, 20)
        Me.txtTitle.TabIndex = 0
        Me.txtTitle.TextAlign = HorizontalAlignment.Left
        Me.txtTitle.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(12, 35)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(23, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Title"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl2.Controls.Add(Me.txtQuery)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 214)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(615, 364)
        Me.GroupControl2.TabIndex = 1
        Me.GroupControl2.Text = "Query Text"
        '
        'txtQuery
        '
        Me.txtQuery.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.SpellChecker1.SetCanCheckText(Me.txtQuery, False)
        Me.txtQuery.Location = New System.Drawing.Point(12, 29)
        Me.txtQuery.Name = "txtQuery"
        Me.txtQuery.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtQuery.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtQuery, False)
        Me.txtQuery.Size = New System.Drawing.Size(591, 324)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtQuery, OptionsSpelling2)
        Me.txtQuery.TabIndex = 0
        Me.txtQuery.UseOptimizedRendering = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnDelete)
        Me.Panel1.Controls.Add(Me.btnOK)
        Me.Panel1.Controls.Add(Me.btnCancel)
        Me.Panel1.Dock = DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 586)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(639, 31)
        Me.Panel1.TabIndex = 2
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(12, 0)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(75, 23)
        Me.btnDelete.TabIndex = 0
        Me.btnDelete.Text = "Delete"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnOK.Location = New System.Drawing.Point(471, 0)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 1
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnCancel.Location = New System.Drawing.Point(552, 0)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "Cancel"
        '
        'frmEditQuery
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(639, 617)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.FormBorderStyle = FormBorderStyle.Sizable
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(655, 655)
        Me.Name = "frmEditQuery"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = ""
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtConnectionString.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTitle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.txtQuery.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtConnectionString As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents cbxType As Care.Controls.CareComboBox
    Friend WithEvents txtTitle As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtQuery As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents btnDelete As Care.Controls.CareButton
    Friend WithEvents btnSession As Care.Controls.CareButton

End Class
