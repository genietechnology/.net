﻿Option Strict On

Public Class frmGenericListFind

    Public Sub New(ByVal FormCaption As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.Text = FormCaption

    End Sub

    Private m_SelectedValue As Object = Nothing

    Public ReadOnly Property ReturnValue As Object
        Get
            Return m_SelectedValue
        End Get
    End Property

    Public Sub Populate(Of T)(ByVal ListIn As List(Of T), ByVal ColumnsToHide As String)

        grdRecords.Populate(ListIn)

        If ColumnsToHide <> "" Then
            For Each _ColumnToHide As String In ColumnsToHide.Split(CChar(","))
                For Each _C As DevExpress.XtraGrid.Columns.GridColumn In grdRecords.Columns
                    If _C.FieldName = _ColumnToHide Then
                        _C.Visible = False
                    End If
                Next
            Next
        End If

        grdRecords.AutoSizeColumns()

    End Sub

    Private Sub grdRecords_GridDoubleClick(sender As Object, e As EventArgs) Handles grdRecords.GridDoubleClick
        SelectRecord()
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnSelect_Click(sender As Object, e As EventArgs) Handles btnSelect.Click
        SelectRecord()
    End Sub

    Private Sub SelectRecord()

        If grdRecords.RecordCount <= 0 Then Exit Sub
        If grdRecords.UnderlyingGridView.FocusedRowHandle < 0 Then Exit Sub

        m_SelectedValue = grdRecords.UnderlyingGridView.GetRow(grdRecords.UnderlyingGridView.FocusedRowHandle)

        Me.DialogResult = DialogResult.OK
        Me.Close()

    End Sub

End Class
