﻿Imports System.IO
Imports Care.Global
Imports Care.Data
Imports System.Windows.Forms

Public Class frmReportManager

    Private m_DrillDownID As Guid? = Nothing
    Private m_Report As Business.AppReport

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub New(ByVal ReportID As Guid)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_DrillDownID = ReportID

    End Sub

    Private Sub frmSites_Load(sender As Object, e As EventArgs) Handles Me.Load

        Me.Text = "Report Manager"

        cbxCategory.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Report Categories"))
        cbxQuery.PopulateWithSQL(Session.ConnectionString, "select id, title from AppQueries order by title")
        SetControls(True)

    End Sub

    Private Sub frmReportManager_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        If m_DrillDownID.HasValue Then
            DisplayRecord(m_DrillDownID.Value)
            ToolbarMode = ToolbarEnum.EditOnly
        End If
    End Sub

#Region "Overrides"

    Protected Overrides Sub SetBindings()

        m_Report = New Business.AppReport
        bs.DataSource = m_Report

        cbxCategory.DataBindings.Add("Text", bs, "_Parent")
        txtReportName.DataBindings.Add("Text", bs, "_Name")
        chkAdHoc.DataBindings.Add("Checked", bs, "_AdHoc")
        txtSQLQuery.DataBindings.Add("Text", bs, "_SqlQuery")
        txtSQLOrder.DataBindings.Add("Text", bs, "_SqlOrder")

    End Sub

    Protected Overrides Sub CommitDelete()

        Dim _SQL As String = ""

        _SQL = "delete from AppReports where ID = '" + m_Report._ID.Value.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from AppReportClauses where report_id = '" + m_Report._ID.Value.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        cgClauses.Clear()

    End Sub

    Protected Overrides Sub AfterEdit()
        SetControls(radCustom.Checked)
        txtReportName.Focus()
    End Sub

    Protected Overrides Sub AfterAdd()
        radCustom.Checked = True
        cgClauses.Clear()
        txtReportName.Focus()
    End Sub

    Protected Overrides Sub AfterAcceptChanges()
        SetControls(radCustom.Checked)
    End Sub

    Protected Overrides Sub AfterCancelChanges()
        SetControls(radCustom.Checked)
    End Sub

    Protected Overrides Sub CommitUpdate()
        m_Report = CType(bs.Item(bs.Position), Business.AppReport)
        m_Report._QueryId = ValueHandler.ConvertGUID(cbxQuery.SelectedValue)
        m_Report._ReportFile = txtReportPath.Text
        m_Report.Store()
    End Sub

    Protected Overrides Sub FindRecord()

        Dim _Find As New GenericFind

        Dim _SQL As String = "select parent as 'Category', name as 'Name', ad_hoc as 'Ad-Hoc' from AppReports"

        With _Find
            .Caption = "Find Report"
            .ParentForm = ParentForm
            .PopulateOnLoad = True
            .ConnectionString = Session.ConnectionString
            .GridSQL = _SQL
            .GridOrderBy = "order by name"
            .ReturnField = "ID"
            .Show()
        End With

        If _Find.ReturnValue <> "" Then
            DisplayRecord(New Guid(_Find.ReturnValue))
        End If

    End Sub

#End Region

    Private Sub DisplayRecord(ByVal ReportID As Guid)

        MyBase.RecordID = ReportID
        MyBase.RecordPopulated = True

        m_Report = Business.AppReport.RetreiveByID(ReportID)
        bs.DataSource = m_Report

        If m_Report._QueryId.HasValue Then
            radQuery.Checked = True
            cbxQuery.SelectedValue = m_Report._QueryId
        Else
            radCustom.Checked = True
        End If

        txtReportPath.Text = m_Report._ReportFile
        DisplayClauses()

        SetControls(radCustom.Checked)

    End Sub

    Private Sub SetControls(CustomQuery As Boolean)

        Dim _Enabled As Boolean = False

        If Mode <> "" Then

            If CustomQuery Then
                _Enabled = True
                cbxQuery.SelectedIndex = -1
                cbxQuery.ReadOnly = True
                cbxQuery.ResetBackColor()
            Else
                cbxQuery.SelectedIndex = 0
                cbxQuery.ReadOnly = False
                cbxQuery.BackColor = Session.ChangeColour
            End If

        End If

        txtSQLQuery.ReadOnly = Not _Enabled
        txtSQLOrder.ReadOnly = Not _Enabled

        If _Enabled Then
            txtSQLQuery.BackColor = Session.ChangeColour
            txtSQLOrder.BackColor = Session.ChangeColour
        Else
            txtSQLQuery.ResetBackColor()
            txtSQLOrder.ResetBackColor()
        End If

        If Mode = "" Then

            gbxCustom.Enabled = True
            gbxClauses.Enabled = True
            gbxOrderBy.Enabled = True

            btnBrowse.Enabled = False
            btnImport.Enabled = True
            btnExport.Enabled = RecordPopulated

        Else

            gbxCustom.Enabled = _Enabled
            gbxClauses.Enabled = _Enabled
            gbxOrderBy.Enabled = _Enabled

            btnBrowse.Enabled = True
            btnImport.Enabled = False
            btnExport.Enabled = False

            If CustomQuery Then
                txtSQLQuery.Focus()
            Else
                cbxQuery.Focus()
            End If

        End If

        btnAdd.Enabled = _Enabled
        btnEdit.Enabled = _Enabled
        btnRemove.Enabled = _Enabled

    End Sub

    Private Sub btnBrowse_Click(sender As Object, e As EventArgs) Handles btnBrowse.Click
        txtReportPath.Text = ReturnReportFile()
    End Sub

    Private Function ReturnReportFile() As String

        Dim _Return As String = ""

        Dim _OFD As New OpenFileDialog
        _OFD.InitialDirectory = Session.ReportFolder
        _OFD.Filter = "Report Files (*.repx)|*.repx"

        If _OFD.ShowDialog = DialogResult.OK Then
            _Return = _OFD.FileName
        End If

        Return _Return

    End Function

    Private Sub radQuery_CheckedChanged(sender As Object, e As EventArgs) Handles radQuery.CheckedChanged
        If radQuery.Checked Then SetControls(False)
    End Sub

    Private Sub radCustom_CheckedChanged(sender As Object, e As EventArgs) Handles radCustom.CheckedChanged
        If radCustom.Checked Then SetControls(True)
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        ShowClause(Nothing)
    End Sub

    Private Sub ShowClause(ByVal ClauseID As Guid?)

        Dim _frm As New frmReportClause(m_Report._ID.Value, ClauseID)

        If ClauseID.HasValue Then
            _frm.Text = "Edit Clause for " + m_Report._Name
        Else
            _frm.Text = "Add New Clause for " + m_Report._Name
        End If

        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            DisplayClauses()
        End If

        _frm.Dispose()

    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Edit()
    End Sub

    Private Sub Edit()

        If cgClauses Is Nothing Then Exit Sub
        If cgClauses.RecordCount < 1 Then Exit Sub
        If cgClauses.CurrentRow(0).ToString = "" Then Exit Sub

        Dim _ID As Guid? = New Guid(cgClauses.CurrentRow(0).ToString)
        ShowClause(_ID)

    End Sub

    Private Sub cgClauses_GridDoubleClick(sender As Object, e As EventArgs) Handles cgClauses.GridDoubleClick
        Edit()
    End Sub

    Private Sub DisplayClauses()

        Dim _SQL As String = ""

        _SQL += "select ID, clause_label as 'Label', clause_type as 'Type', clause_datatype as 'Data Type',"
        _SQL += " clause_operator as 'Operator', clause_field1 as 'Field', clause_mandatory as 'Mandatory'"
        _SQL += " from AppReportClauses"
        _SQL += " where report_id = '" + m_Report._ID.Value.ToString() + "'"
        _SQL += " order by clause_seq"

        cgClauses.HideFirstColumn = True
        cgClauses.Populate(Session.ConnectionString, _SQL)
        cgClauses.AutoSizeColumns()

    End Sub

    Private Sub ImportPack()

        ofd.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.Desktop
        ofd.Filter = "Report Packs|*.pak"

        If ofd.ShowDialog = DialogResult.OK Then

            Dim _ReportPack As New ReportPack
            _ReportPack.DeSerialise(Of ReportPack)(ofd.FileName, _ReportPack)

            If _ReportPack IsNot Nothing Then

                Dim _ImportReport As Business.AppReport = _ReportPack.ReportObject
                Dim _ImportQuery As Business.AppQueries = _ReportPack.QueryObject
                Dim _ImportClauses As List(Of Business.AppReportClause) = _ReportPack.Clauses

                Dim _DBReport As Business.AppReport = Business.AppReport.RetreiveByID(_ImportReport._ID.Value)
                If _DBReport IsNot Nothing Then
                    If CareMessage("This report already exists. Do you want to overwrite it?", MessageBoxIcon.Exclamation, MessageBoxButtons.OKCancel, "Override Report") = DialogResult.Cancel Then
                        Exit Sub
                    End If
                End If

                Session.CursorWaiting()

                _ImportReport.Store()

                If _ImportQuery IsNot Nothing Then
                    _ImportQuery.Store()
                Else
                    Business.AppReportClause.SaveAll(_ImportClauses)
                End If

                'write out the report file
                Dim _ReportFile As String = _ImportReport._ReportFile.Substring(_ImportReport._ReportFile.LastIndexOf("\") + 1)
                Dim _ReportPath As String = Session.ReportFolder + _ReportFile
                Dim _ReportFileCreated As Boolean = ByteArraytoFile(_ReportPack.ReportFileData, _ReportPath)

                Session.CursorDefault()

                If _ReportFileCreated Then
                    CareMessage("Report Pack imported successfully.", MessageBoxIcon.Information, "Import Pack")
                Else
                    CareMessage("Report Pack failed to import.", MessageBoxIcon.Exclamation, "Import Pack")
                End If

            Else
                'deserialise failed
                CareMessage("Unable to extract report pack.", MessageBoxIcon.Exclamation, "Import Pack")
            End If

        Else
            'user didnt select anything
        End If

    End Sub

    Private Sub ExportPack()

        sfd.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.Desktop
        sfd.FileName = m_Report._Name + ".pak"
        sfd.Filter = "Report Packs|*.pak"

        If sfd.ShowDialog = DialogResult.OK Then

            Cursor = Cursors.WaitCursor

            Dim _RP As New ReportPack
            With _RP

                .ReportObject = m_Report

                If m_Report._QueryId.HasValue Then
                    Dim _Q As Business.AppQueries = Business.AppQueries.RetreiveByID(m_Report._QueryId.Value)
                    If _Q IsNot Nothing Then
                        .QueryObject = _Q
                    End If
                Else
                    .Clauses = Business.AppReportClause.RetreiveByReportID(m_Report._ID.Value)
                End If

                .ReportFileData = ReturnByteArray(m_Report._ReportFile)
                .Serialise(sfd.FileName)

                CareMessage("Report Pack exported successfully.", MessageBoxIcon.Information, "Export Pack")

            End With

            Cursor = Cursors.Default

        End If

    End Sub

    Private Function ReturnByteArray(ByVal FilePath As String) As Byte()

        Dim _bytes As Byte() = Nothing

        If FilePath <> "" Then
            If IO.File.Exists(FilePath) Then
                _bytes = IO.File.ReadAllBytes(FilePath)
            End If
        End If

        Return _bytes

    End Function

    Private Function ByteArraytoFile(ByVal Data As Byte(), ByVal FilePath As String) As Boolean

        If Data IsNot Nothing Then

            Try

                Dim _fs As FileStream = New IO.FileStream(FilePath, FileMode.OpenOrCreate, FileAccess.Write)
                Dim _bw As BinaryWriter = New IO.BinaryWriter(_fs)

                _bw.Write(Data)
                _bw.Flush()
                _bw.Close()
                _bw = Nothing

                _fs.Close()
                _fs = Nothing

                Return True

            Catch ex As Exception
                Return False
            End Try

        Else
            Return False
        End If

    End Function

    Public Class ReportPack
        Inherits Care.Data.DataObjectBase
        Public Property ReportObject As Business.AppReport
        Public Property QueryObject As Business.AppQueries
        Public Property Clauses As List(Of Business.AppReportClause)
        Public Property ReportFileData As Byte()
    End Class

    Private Sub btnImport_Click(sender As Object, e As EventArgs) Handles btnImport.Click
        ImportPack()
    End Sub

    Private Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        ExportPack()
    End Sub

    Private Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click

        If cgClauses Is Nothing Then Exit Sub
        If cgClauses.RecordCount < 1 Then Exit Sub
        If cgClauses.CurrentRow(0).ToString = "" Then Exit Sub

        If CareMessage("Are you sure you want to remove this clause?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Remove Clause?") = DialogResult.Yes Then
            Dim _ID As String = cgClauses.CurrentRow(0).ToString
            DAL.ExecuteSQL(Session.ConnectionString, "delete from AppReportClauses where ID = '" + _ID + "'")
            DisplayClauses()
        End If

    End Sub

End Class
