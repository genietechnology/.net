﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportPrompt
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxTextbox = New Care.Controls.CareFrame()
        Me.txtValue = New Care.Controls.CareTextBox(Me.components)
        Me.lblTextbox = New Care.Controls.CareLabel(Me.components)
        Me.gbxDate = New Care.Controls.CareFrame()
        Me.cdtValue = New Care.Controls.CareDateTime(Me.components)
        Me.lblDate = New Care.Controls.CareLabel(Me.components)
        Me.gbxDateRange = New Care.Controls.CareFrame()
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.cdtValue2 = New Care.Controls.CareDateTime(Me.components)
        Me.cdtValue1 = New Care.Controls.CareDateTime(Me.components)
        Me.lblDateRange = New Care.Controls.CareLabel(Me.components)
        Me.gbxCombo = New Care.Controls.CareFrame()
        Me.cbxValue = New Care.Controls.CareComboBox(Me.components)
        Me.lblCombo = New Care.Controls.CareLabel(Me.components)
        Me.btnNext = New Care.Controls.CareButton(Me.components)
        Me.btnPrevious = New Care.Controls.CareButton(Me.components)
        Me.lblMandatory = New Care.Controls.CareLabel(Me.components)
        Me.gbxRange = New Care.Controls.CareFrame()
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.lblRange = New Care.Controls.CareLabel(Me.components)
        Me.txtValueFrom = New Care.Controls.CareTextBox(Me.components)
        Me.txtValueTo = New Care.Controls.CareTextBox(Me.components)
        CType(Me.gbxTextbox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxTextbox.SuspendLayout()
        CType(Me.txtValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxDate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxDate.SuspendLayout()
        CType(Me.cdtValue.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxDateRange, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxDateRange.SuspendLayout()
        CType(Me.cdtValue2.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtValue2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtValue1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtValue1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxCombo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxCombo.SuspendLayout()
        CType(Me.cbxValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxRange, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxRange.SuspendLayout()
        CType(Me.txtValueFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtValueTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'gbxTextbox
        '
        Me.gbxTextbox.Controls.Add(Me.txtValue)
        Me.gbxTextbox.Controls.Add(Me.lblTextbox)
        Me.gbxTextbox.Location = New System.Drawing.Point(12, 77)
        Me.gbxTextbox.Name = "gbxTextbox"
        Me.gbxTextbox.Size = New System.Drawing.Size(382, 59)
        Me.gbxTextbox.TabIndex = 1
        Me.gbxTextbox.Text = "Please enter the criteria"
        '
        'txtValue
        '
        Me.txtValue.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtValue.CharacterCasing = CharacterCasing.Normal
        Me.txtValue.EnterMoveNextControl = True
        Me.txtValue.Location = New System.Drawing.Point(163, 28)
        Me.txtValue.MaxLength = 100
        Me.txtValue.Name = "txtValue"
        Me.txtValue.NumericAllowNegatives = False
        Me.txtValue.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtValue.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtValue.Properties.AccessibleName = "Name"
        Me.txtValue.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtValue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtValue.Properties.Appearance.Options.UseFont = True
        Me.txtValue.Properties.Appearance.Options.UseTextOptions = True
        Me.txtValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtValue.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtValue.Properties.MaxLength = 100
        Me.txtValue.Size = New System.Drawing.Size(209, 20)
        Me.txtValue.TabIndex = 1
        Me.txtValue.Tag = "AE"
        Me.txtValue.TextAlign = HorizontalAlignment.Left
        Me.txtValue.ToolTipText = ""
        '
        'lblTextbox
        '
        Me.lblTextbox.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblTextbox.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblTextbox.Location = New System.Drawing.Point(12, 31)
        Me.lblTextbox.Name = "lblTextbox"
        Me.lblTextbox.Size = New System.Drawing.Size(145, 15)
        Me.lblTextbox.TabIndex = 0
        Me.lblTextbox.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        Me.lblTextbox.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbxDate
        '
        Me.gbxDate.Controls.Add(Me.cdtValue)
        Me.gbxDate.Controls.Add(Me.lblDate)
        Me.gbxDate.Location = New System.Drawing.Point(12, 142)
        Me.gbxDate.Name = "gbxDate"
        Me.gbxDate.Size = New System.Drawing.Size(382, 59)
        Me.gbxDate.TabIndex = 2
        Me.gbxDate.Text = "Please enter the date"
        '
        'cdtValue
        '
        Me.cdtValue.EditValue = New Date(2012, 6, 30, 0, 0, 0, 0)
        Me.cdtValue.EnterMoveNextControl = True
        Me.cdtValue.Location = New System.Drawing.Point(163, 28)
        Me.cdtValue.Name = "cdtValue"
        Me.cdtValue.Properties.AccessibleName = "Date Started"
        Me.cdtValue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cdtValue.Properties.Appearance.Options.UseFont = True
        Me.cdtValue.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtValue.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtValue.Size = New System.Drawing.Size(85, 22)
        Me.cdtValue.TabIndex = 1
        Me.cdtValue.Tag = "AE"
        Me.cdtValue.Value = New Date(2012, 6, 30, 0, 0, 0, 0)
        '
        'lblDate
        '
        Me.lblDate.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblDate.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblDate.Location = New System.Drawing.Point(12, 31)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(145, 15)
        Me.lblDate.TabIndex = 0
        Me.lblDate.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbxDateRange
        '
        Me.gbxDateRange.Controls.Add(Me.CareLabel2)
        Me.gbxDateRange.Controls.Add(Me.cdtValue2)
        Me.gbxDateRange.Controls.Add(Me.cdtValue1)
        Me.gbxDateRange.Controls.Add(Me.lblDateRange)
        Me.gbxDateRange.Location = New System.Drawing.Point(12, 207)
        Me.gbxDateRange.Name = "gbxDateRange"
        Me.gbxDateRange.Size = New System.Drawing.Size(382, 59)
        Me.gbxDateRange.TabIndex = 3
        Me.gbxDateRange.Text = "Please enter the date range"
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(254, 31)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(11, 15)
        Me.CareLabel2.TabIndex = 2
        Me.CareLabel2.Text = "to"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtValue2
        '
        Me.cdtValue2.EditValue = New Date(2012, 6, 30, 0, 0, 0, 0)
        Me.cdtValue2.EnterMoveNextControl = True
        Me.cdtValue2.Location = New System.Drawing.Point(271, 28)
        Me.cdtValue2.Name = "cdtValue2"
        Me.cdtValue2.Properties.AccessibleName = "Date Started"
        Me.cdtValue2.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cdtValue2.Properties.Appearance.Options.UseFont = True
        Me.cdtValue2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtValue2.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtValue2.Size = New System.Drawing.Size(85, 22)
        Me.cdtValue2.TabIndex = 3
        Me.cdtValue2.Tag = "AE"
        Me.cdtValue2.Value = New Date(2012, 6, 30, 0, 0, 0, 0)
        '
        'cdtValue1
        '
        Me.cdtValue1.EditValue = New Date(2012, 6, 30, 0, 0, 0, 0)
        Me.cdtValue1.EnterMoveNextControl = True
        Me.cdtValue1.Location = New System.Drawing.Point(163, 28)
        Me.cdtValue1.Name = "cdtValue1"
        Me.cdtValue1.Properties.AccessibleName = "Date Started"
        Me.cdtValue1.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cdtValue1.Properties.Appearance.Options.UseFont = True
        Me.cdtValue1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtValue1.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtValue1.Size = New System.Drawing.Size(85, 22)
        Me.cdtValue1.TabIndex = 1
        Me.cdtValue1.Tag = "AE"
        Me.cdtValue1.Value = New Date(2012, 6, 30, 0, 0, 0, 0)
        '
        'lblDateRange
        '
        Me.lblDateRange.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblDateRange.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblDateRange.Location = New System.Drawing.Point(12, 31)
        Me.lblDateRange.Name = "lblDateRange"
        Me.lblDateRange.Size = New System.Drawing.Size(145, 15)
        Me.lblDateRange.TabIndex = 0
        Me.lblDateRange.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        Me.lblDateRange.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbxCombo
        '
        Me.gbxCombo.Controls.Add(Me.cbxValue)
        Me.gbxCombo.Controls.Add(Me.lblCombo)
        Me.gbxCombo.Location = New System.Drawing.Point(12, 12)
        Me.gbxCombo.Name = "gbxCombo"
        Me.gbxCombo.Size = New System.Drawing.Size(382, 59)
        Me.gbxCombo.TabIndex = 0
        Me.gbxCombo.Text = "Please select an option"
        '
        'cbxValue
        '
        Me.cbxValue.AllowBlank = False
        Me.cbxValue.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxValue.DataSource = Nothing
        Me.cbxValue.DisplayMember = Nothing
        Me.cbxValue.EnterMoveNextControl = True
        Me.cbxValue.Location = New System.Drawing.Point(163, 28)
        Me.cbxValue.Name = "cbxValue"
        Me.cbxValue.Properties.AccessibleName = "Tariff Type"
        Me.cbxValue.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxValue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxValue.Properties.Appearance.Options.UseFont = True
        Me.cbxValue.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxValue.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxValue.SelectedValue = Nothing
        Me.cbxValue.Size = New System.Drawing.Size(209, 20)
        Me.cbxValue.TabIndex = 1
        Me.cbxValue.Tag = "AE"
        Me.cbxValue.ValueMember = Nothing
        '
        'lblCombo
        '
        Me.lblCombo.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblCombo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblCombo.Location = New System.Drawing.Point(12, 31)
        Me.lblCombo.Name = "lblCombo"
        Me.lblCombo.Size = New System.Drawing.Size(145, 15)
        Me.lblCombo.TabIndex = 0
        Me.lblCombo.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        Me.lblCombo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnNext
        '
        Me.btnNext.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnNext.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnNext.Appearance.Options.UseFont = True
        Me.btnNext.CausesValidation = False
        Me.btnNext.Location = New System.Drawing.Point(320, 337)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(74, 25)
        Me.btnNext.TabIndex = 7
        Me.btnNext.Tag = ""
        Me.btnNext.Text = "Next >"
        '
        'btnPrevious
        '
        Me.btnPrevious.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnPrevious.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnPrevious.Appearance.Options.UseFont = True
        Me.btnPrevious.CausesValidation = False
        Me.btnPrevious.Location = New System.Drawing.Point(240, 337)
        Me.btnPrevious.Name = "btnPrevious"
        Me.btnPrevious.Size = New System.Drawing.Size(74, 25)
        Me.btnPrevious.TabIndex = 6
        Me.btnPrevious.Tag = ""
        Me.btnPrevious.Text = "< Previous"
        '
        'lblMandatory
        '
        Me.lblMandatory.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.lblMandatory.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMandatory.Appearance.ForeColor = System.Drawing.Color.Red
        Me.lblMandatory.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblMandatory.Location = New System.Drawing.Point(12, 342)
        Me.lblMandatory.Name = "lblMandatory"
        Me.lblMandatory.Size = New System.Drawing.Size(203, 15)
        Me.lblMandatory.TabIndex = 5
        Me.lblMandatory.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        Me.lblMandatory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbxRange
        '
        Me.gbxRange.Controls.Add(Me.txtValueTo)
        Me.gbxRange.Controls.Add(Me.txtValueFrom)
        Me.gbxRange.Controls.Add(Me.CareLabel1)
        Me.gbxRange.Controls.Add(Me.lblRange)
        Me.gbxRange.Location = New System.Drawing.Point(12, 272)
        Me.gbxRange.Name = "gbxRange"
        Me.gbxRange.Size = New System.Drawing.Size(382, 59)
        Me.gbxRange.TabIndex = 4
        Me.gbxRange.Text = "Please enter the range"
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(254, 31)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(11, 15)
        Me.CareLabel1.TabIndex = 2
        Me.CareLabel1.Text = "to"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRange
        '
        Me.lblRange.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblRange.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblRange.Location = New System.Drawing.Point(12, 31)
        Me.lblRange.Name = "lblRange"
        Me.lblRange.Size = New System.Drawing.Size(145, 15)
        Me.lblRange.TabIndex = 0
        Me.lblRange.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        Me.lblRange.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtValueFrom
        '
        Me.txtValueFrom.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtValueFrom.CharacterCasing = CharacterCasing.Normal
        Me.txtValueFrom.EnterMoveNextControl = True
        Me.txtValueFrom.Location = New System.Drawing.Point(163, 28)
        Me.txtValueFrom.MaxLength = 100
        Me.txtValueFrom.Name = "txtValueFrom"
        Me.txtValueFrom.NumericAllowNegatives = False
        Me.txtValueFrom.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtValueFrom.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtValueFrom.Properties.AccessibleName = "Name"
        Me.txtValueFrom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtValueFrom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtValueFrom.Properties.Appearance.Options.UseFont = True
        Me.txtValueFrom.Properties.Appearance.Options.UseTextOptions = True
        Me.txtValueFrom.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtValueFrom.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtValueFrom.Properties.MaxLength = 100
        Me.txtValueFrom.Size = New System.Drawing.Size(85, 22)
        Me.txtValueFrom.TabIndex = 3
        Me.txtValueFrom.Tag = "AE"
        Me.txtValueFrom.TextAlign = HorizontalAlignment.Left
        Me.txtValueFrom.ToolTipText = ""
        '
        'txtValueTo
        '
        Me.txtValueTo.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtValueTo.CharacterCasing = CharacterCasing.Normal
        Me.txtValueTo.EnterMoveNextControl = True
        Me.txtValueTo.Location = New System.Drawing.Point(271, 28)
        Me.txtValueTo.MaxLength = 100
        Me.txtValueTo.Name = "txtValueTo"
        Me.txtValueTo.NumericAllowNegatives = False
        Me.txtValueTo.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtValueTo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtValueTo.Properties.AccessibleName = "Name"
        Me.txtValueTo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtValueTo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtValueTo.Properties.Appearance.Options.UseFont = True
        Me.txtValueTo.Properties.Appearance.Options.UseTextOptions = True
        Me.txtValueTo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtValueTo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtValueTo.Properties.MaxLength = 100
        Me.txtValueTo.Size = New System.Drawing.Size(85, 22)
        Me.txtValueTo.TabIndex = 4
        Me.txtValueTo.Tag = "AE"
        Me.txtValueTo.TextAlign = HorizontalAlignment.Left
        Me.txtValueTo.ToolTipText = ""
        '
        'frmReportPrompt
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(404, 371)
        Me.Controls.Add(Me.gbxRange)
        Me.Controls.Add(Me.lblMandatory)
        Me.Controls.Add(Me.btnNext)
        Me.Controls.Add(Me.btnPrevious)
        Me.Controls.Add(Me.gbxCombo)
        Me.Controls.Add(Me.gbxDateRange)
        Me.Controls.Add(Me.gbxDate)
        Me.Controls.Add(Me.gbxTextbox)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmReportPrompt"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = ""
        CType(Me.gbxTextbox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxTextbox.ResumeLayout(False)
        Me.gbxTextbox.PerformLayout()
        CType(Me.txtValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxDate, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxDate.ResumeLayout(False)
        Me.gbxDate.PerformLayout()
        CType(Me.cdtValue.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxDateRange, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxDateRange.ResumeLayout(False)
        Me.gbxDateRange.PerformLayout()
        CType(Me.cdtValue2.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtValue2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtValue1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtValue1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxCombo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxCombo.ResumeLayout(False)
        Me.gbxCombo.PerformLayout()
        CType(Me.cbxValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxRange, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxRange.ResumeLayout(False)
        Me.gbxRange.PerformLayout()
        CType(Me.txtValueFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtValueTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbxTextbox As DevExpress.XtraEditors.GroupControl
    Friend WithEvents gbxDate As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtValue As Care.Controls.CareTextBox
    Friend WithEvents lblTextbox As Care.Controls.CareLabel
    Friend WithEvents cdtValue As Care.Controls.CareDateTime
    Friend WithEvents lblDate As Care.Controls.CareLabel
    Friend WithEvents gbxDateRange As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents cdtValue2 As Care.Controls.CareDateTime
    Friend WithEvents cdtValue1 As Care.Controls.CareDateTime
    Friend WithEvents lblDateRange As Care.Controls.CareLabel
    Friend WithEvents gbxCombo As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cbxValue As Care.Controls.CareComboBox
    Friend WithEvents lblCombo As Care.Controls.CareLabel
    Friend WithEvents btnNext As Care.Controls.CareButton
    Friend WithEvents btnPrevious As Care.Controls.CareButton
    Friend WithEvents lblMandatory As Care.Controls.CareLabel
    Friend WithEvents gbxRange As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtValueTo As Care.Controls.CareTextBox
    Friend WithEvents txtValueFrom As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents lblRange As Care.Controls.CareLabel

End Class
