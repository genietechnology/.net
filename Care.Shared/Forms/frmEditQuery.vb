﻿Imports Care.Global
Imports Care.Data

Public Class frmEditQuery

    Private m_ID As Guid?

    Public Sub New(ByVal ID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_ID = ID

    End Sub

    Private Sub frmEditQuery_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cbxType.AddItem("SQL Query")
        cbxType.AddItem("SQL View")
        cbxType.AddItem("Stored Procedure")

        If m_ID IsNot Nothing Then
            Me.Text = "Edit Query"
            btnDelete.Enabled = True
            DisplayRecord()
        Else
            Me.Text = "Create New Query"
            btnDelete.Enabled = False
        End If

        txtTitle.BackColor = Session.ChangeColour
        cbxType.BackColor = Session.ChangeColour
        txtConnectionString.BackColor = Session.ChangeColour
        txtQuery.BackColor = Session.ChangeColour

    End Sub

    Private Sub DisplayRecord()

        Dim _Query As Business.AppQueries = Business.AppQueries.RetreiveByID(m_ID.Value)
        If _Query IsNot Nothing Then
            txtTitle.Text = _Query._Title
            cbxType.Text = _Query._Type
            txtConnectionString.Text = _Query._Connection
            txtQuery.Text = _Query._Query
        End If

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click

        Dim _Query As Business.AppQueries = Nothing

        If m_ID Is Nothing Then
            _Query = New Business.AppQueries
            _Query._ID = Guid.NewGuid
        Else
            _Query = Business.AppQueries.RetreiveByID(m_ID.Value)
        End If

        With _Query
            ._Title = txtTitle.Text
            ._Type = cbxType.Text
            ._Connection = txtConnectionString.Text
            ._Query = txtQuery.Text
            .Store()
        End With

        Me.Close()

    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If CareMessage("Are you sure you want to Delete this query?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Delete") = DialogResult.Yes Then
            DAL.ExecuteSQL(Session.ConnectionString, "delete from AppQueries where ID = '" + m_ID.Value.ToString + "'")
            Me.Close()
        End If
    End Sub

    Private Sub btnSession_Click(sender As Object, e As EventArgs) Handles btnSession.Click
        txtConnectionString.Text = Session.ConnectionString
    End Sub
End Class
