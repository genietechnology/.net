﻿Option Strict On

Imports Care.Global
Imports Care.Data
Imports System.Windows.Forms

Public Class frmDocument

    Private m_NewDocument As Boolean = True
    Private m_SourceFile As String = ""
    Private m_Letter As Business.AppLetter

    Private m_DataLoaded As Boolean = False
    Private m_AdditionalSelect As String = ""
    Private m_AdditionalWhereClause As String = ""

    Private m_RecordCount As Integer
    Private m_RecordLimit As Integer

    Private m_MergeCompleted As Boolean = False
    Private m_OutputFiles As Boolean = False
    Private m_OutputINVU As Boolean = False

    Private m_SQLSource As String 'debugging
    Private m_DataViewPassedIn As DataView = Nothing

    Public Sub NewDocument()
        m_NewDocument = True
        ResetFlags()
        EnableButtons(False)
    End Sub

    Public Function OpenDocument(DocumentPath As String) As Boolean

        m_NewDocument = False
        m_SourceFile = DocumentPath

        Me.Text = "Document - " + m_SourceFile
        LoadDocument(m_SourceFile)

        If m_Letter Is Nothing Then
            ResetFlags()
            EnableButtons(False)
        End If

        Return True

    End Function

    Public Function OpenDocument(ByVal LetterCode As String, ByVal FilteredRows As DataView) As Boolean

        ResetFlags()

        m_DataViewPassedIn = FilteredRows

        If SetupLetter(LetterCode) Then

            m_SourceFile = m_Letter._SourceFile
            Me.Text = "Document - " + m_SourceFile

            LoadDocument(m_SourceFile)
            ExecuteMailMerge(WordProcessing.EnumBehaviour.Normal)

            Return True

        Else
            Return False
        End If

    End Function

    Public Function OpenDocument(ByVal LetterCode As String, ByVal AdditionalSelect As String, ByVal AdditionalWhereClause As String, ByVal Behaviour As WordProcessing.EnumBehaviour, ByVal RecordLimit As Integer) As Boolean

        ResetFlags()

        m_AdditionalSelect = AdditionalSelect
        m_AdditionalWhereClause = AdditionalWhereClause
        m_RecordLimit = RecordLimit

        If SetupLetter(LetterCode) Then

            m_SourceFile = m_Letter._SourceFile
            Me.Text = "Document - " + m_SourceFile

            LoadDocument(m_SourceFile)
            ExecuteMailMerge(Behaviour)

            Return True

        Else
            Return False
        End If

    End Function

    Private Sub LoadDocument(ByVal SourceFile As String)
        Session.SetProgressMessage("Loading Document...")
        RichEditControl1.LoadDocument(m_SourceFile)
    End Sub

    Private Function SetupLetter(ByVal LetterCode As String) As Boolean

        m_NewDocument = False
        m_Letter = Business.AppLetter.RetreiveByCode(LetterCode)

        If m_Letter IsNot Nothing Then

            If m_Letter._SourceFile.Trim.Length > 0 Then

                m_SourceFile = m_Letter._SourceFile

                If m_Letter._Multi Then
                    MergeToFiles.Enabled = True
                    If m_Letter._Invu Then
                        MergeToFiles.Enabled = False
                        SaveToINVU.Enabled = True
                    Else
                        SaveToINVU.Enabled = False
                    End If
                Else
                    MergeToFiles.Enabled = False
                    SaveToINVU.Enabled = False
                End If

                Me.Text = "Document - " + m_SourceFile

                LoadDocument(m_SourceFile)
                SetMailMergeData()

                Return True

            Else
                Return False
            End If
        Else
            Return False
        End If

    End Function

    Private Sub SetMailMergeData()

        If m_DataViewPassedIn IsNot Nothing Then

            For Each _C As DataColumn In m_DataViewPassedIn.Table.Columns
                _C.ColumnName = _C.ColumnName.ToLower
            Next

            RichEditControl1.Options.MailMerge.DataSource = m_DataViewPassedIn

            m_DataLoaded = True
            m_RecordCount = m_DataViewPassedIn.Count

        Else

            Dim _DT As DataTable = Nothing

            If m_Letter._Type = "X" Then
                _DT = QueryHandler.GetQueryData(m_Letter._QueryId.Value, True)
            Else

                Dim _SQL As String = ""
                _SQL = m_Letter._SqlSelect

                If m_AdditionalSelect <> "" Then
                    _SQL = Replace(_SQL.ToLower, "select ", "select " + m_AdditionalSelect + " ")
                End If

                If m_RecordLimit > 0 Then
                    _SQL = Replace(_SQL.ToLower, "select ", "select top " + m_RecordLimit.ToString + " ")
                End If

                _SQL += " where 1 = 1"
                _SQL += " " + m_Letter._SqlWhere
                _SQL += " " + m_AdditionalWhereClause
                _SQL += " " + m_Letter._SqlOrder

                m_SQLSource = _SQL

                _DT = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)

            End If

            If _DT IsNot Nothing Then

                RichEditControl1.Options.MailMerge.DataSource = _DT

                m_DataLoaded = True
                m_RecordCount = _DT.Rows.Count

                _DT.Dispose()
                _DT = Nothing

            End If

        End If

    End Sub

    Private Sub RichEditControl1_DocumentLoaded(sender As Object, e As EventArgs) Handles RichEditControl1.DocumentLoaded
        Session.HideProgressBar()
    End Sub

    Private Sub RichEditControl1_MailMergeFinished(sender As Object, e As DevExpress.XtraRichEdit.MailMergeFinishedEventArgs) Handles RichEditControl1.MailMergeFinished
        m_MergeCompleted = True
        Session.HideProgressBar()
        Session.SetProgressMessage("Loading Document...")
    End Sub

    Private Sub RichEditControl1_MailMergeRecordFinished(sender As Object, e As DevExpress.XtraRichEdit.MailMergeRecordFinishedEventArgs) Handles RichEditControl1.MailMergeRecordFinished

        If m_OutputFiles Then

            Dim _Append As String = m_Letter._MultiFilenameAppend
            Dim _DR As DataRow = AppendData(_Append, e.RecordIndex)

            Dim _File As String = m_Letter._OutputFile + "\" + m_Letter._Code + _Append + ReturnExtension(m_Letter._OutputType)

            'save this mailmerge record as a separate document.
            e.RecordDocument.SaveDocument(_File, ReturnOutputFormat(m_Letter._OutputType))

            'pass the data and the path for exporting to invu
            If m_OutputINVU Then WordProcessing.CreateINVUCSV(m_Letter, _File, _DR)

        End If

        Session.StepProgressBar()

    End Sub

    Private Function AppendData(ByRef AppendText As String, ByVal RecordIndex As Integer) As DataRow

        Dim _DR As DataRow = Nothing
        While AppendText.Contains(">")

            Dim _Start As Integer = AppendText.IndexOf("<")
            Dim _Before As String = AppendText.Substring(0, _Start)
            Dim _End As Integer = AppendText.IndexOf(">", _Start)
            Dim _After As String = AppendText.Substring(_End + 1)
            Dim _Field As String = Mid(AppendText, _Start + 2, _End - (_Start + 1))
            Dim _FieldValue As String = ""

            If RichEditControl1.Options.MailMerge.DataSource.GetType.Name = "DataTable" Then
                Dim _DT As DataTable = CType(RichEditControl1.Options.MailMerge.DataSource, DataTable)
                _DR = _DT.Rows(RecordIndex)
            End If

            If _DR IsNot Nothing Then
                Try
                    _FieldValue = _DR.Item(_Field).ToString
                Catch ex As Exception
                    _FieldValue = "ERROR_" + RecordIndex.ToString
                End Try
            End If

            AppendText = _Before + _FieldValue + _After

        End While

        Return _DR

    End Function

    Private Sub ExecuteMailMerge(ByVal MergeMode As WordProcessing.EnumBehaviour)

        If MergeMode = WordProcessing.EnumBehaviour.HoldMerge Then Exit Sub

        Select Case MergeMode

            Case WordProcessing.EnumBehaviour.Normal
                m_OutputFiles = True
                m_OutputINVU = False

            Case WordProcessing.EnumBehaviour.SaveToINVU
                m_OutputFiles = True
                m_OutputINVU = True

        End Select

        Me.Cursor = Cursors.WaitCursor
        Application.DoEvents()

        Me.Enabled = False
        Me.Hide()

        Application.DoEvents()

        Dim _FileName As String = m_Letter._Code + "-" + Format(Date.Now, "ddMMyyyy_HHmmss") + ReturnExtension(m_Letter._OutputType)
        RichEditControl1.Document.MailMerge(m_Letter._OutputFile + "\" + _FileName, ReturnOutputFormat(m_Letter._OutputType))

        OpenDocument(m_Letter._OutputFile + "\" + _FileName)

        Me.Enabled = True
        Me.Show()

        Me.Cursor = Cursors.Default
        Application.DoEvents()

        If MergeMode = WordProcessing.EnumBehaviour.SaveToINVU Then
            CareMessage("Files Saved to INVU Pickup Folder.", MessageBoxIcon.Information, MessageBoxButtons.OK, "Save to INVU")
        End If

    End Sub

    Private Function ReturnExtension(OutputType As String) As String
        If OutputType = "Word Document 97-2003 (.doc)" Then Return ".doc"
        Return ".docx"
    End Function

    Private Function ReturnOutputFormat(OutputType As String) As DevExpress.XtraRichEdit.DocumentFormat
        If OutputType = "Word Document 97-2003 (.doc)" Then Return DevExpress.XtraRichEdit.DocumentFormat.Doc
        Return DevExpress.XtraRichEdit.DocumentFormat.OpenXml
    End Function

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        If Me.Cursor = Cursors.Default Then
            Me.Cursor = Cursors.AppStarting
            Application.DoEvents()
        End If

    End Sub

    Private Sub RichEditControl1_MailMergeStarted(sender As Object, e As DevExpress.XtraRichEdit.MailMergeStartedEventArgs) Handles RichEditControl1.MailMergeStarted
        Session.SetupProgressBar("Processing MailMerge Data...", m_RecordCount)
    End Sub

    Private Sub frmDocument_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        Session.SetProgressMessage("")
    End Sub

    Private Sub ResetFlags()
        m_OutputFiles = False
        m_OutputINVU = False
    End Sub

    Private Sub SaveToINVU_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles SaveToINVU.ItemClick
        If SetupLetter(m_Letter._Code) Then
            ExecuteMailMerge(WordProcessing.EnumBehaviour.SaveToINVU)
        End If
    End Sub

    Private Sub MergeToFiles_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles MergeToFiles.ItemClick
        If SetupLetter(m_Letter._Code) Then
            ExecuteMailMerge(WordProcessing.EnumBehaviour.Normal)
        End If
    End Sub

    Private Sub EnableButtons(ByVal Enabled As Boolean)
        MergeToFiles.Enabled = Enabled
        SaveToINVU.Enabled = Enabled
    End Sub

    Private Sub ViewDataSource_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles ViewDataSource.ItemClick
        Dim _frm As New frmDataPreview(M_SQLSource)
        _frm.Show()
    End Sub
End Class