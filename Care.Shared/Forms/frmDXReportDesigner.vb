﻿Imports Care.Global
Imports Care.Data
Imports System.ComponentModel.Design
Imports DevExpress.XtraReports.UserDesigner
Imports DevExpress.XtraReports.UI
Imports System.Windows.Forms
Imports System.IO

Public Class frmDXReportDesigner

    Private Enum EnumReportMode
        FromFile
        FromRepository
    End Enum

    Private m_ReportMode As EnumReportMode = EnumReportMode.FromFile
    Private m_QueryID As Guid? = Nothing
    Private m_ReportID As Guid? = Nothing
    Private m_ReportFile As String = ""

    Private ReadOnly Property ActiveReport As XtraReport
        Get
            Return XrDesignMdiController1.ActiveDesignPanel.Report
        End Get
    End Property

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        AddHandler XrDesignMdiController1.DesignPanelLoaded, AddressOf mdiController_DesignPanelLoaded

    End Sub

    Public Sub New(ByVal ReportID As Guid)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        AddHandler XrDesignMdiController1.DesignPanelLoaded, AddressOf mdiController_DesignPanelLoaded

        m_ReportID = ReportID
        OpenReportByID(ReportID)

    End Sub

    Private Sub frmDXReportDesigner_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Session.HideProgressBar()
        RefreshFieldList()
    End Sub

    Private Sub RefreshFieldList()

        Try
            Dim fieldList As FieldListDockPanel = CType(XrDesignDockManager1(DesignDockPanelType.FieldList), FieldListDockPanel)
            Dim host As IDesignerHost = CType(XrDesignMdiController1.ActiveDesignPanel.GetService(GetType(IDesignerHost)), IDesignerHost)
            fieldList.UpdateDataSource(host)

        Catch ex As Exception

        End Try

    End Sub

    Private Function ReturnXMLSchema() As String

        Dim _Return As String = ""

        Dim _OFD As New OpenFileDialog
        _OFD.InitialDirectory = Session.ReportFolder
        _OFD.Filter = "XML Schemas (*.xsd)|*.xsd"

        If _OFD.ShowDialog = DialogResult.OK Then
            Using r As StreamReader = New StreamReader(_OFD.FileName)
                _Return = r.ReadToEnd
            End Using
        End If

        Return _Return

    End Function

    Private Function CreateTempSchema(ByVal DataTableIn As DataTable) As String

        DataTableIn.TableName = "MyQuery"

        Dim _Return As String = ""
        Using _ms As New MemoryStream

            DataTableIn.WriteXmlSchema(_ms)
            _ms.Position = 0

            Using r As StreamReader = New StreamReader(_ms)
                _Return = r.ReadToEnd
            End Using

        End Using

        Return _Return

    End Function

    Private Sub bbiDataSourceQuery_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiDataSourceQuery.ItemClick

        Dim _ID As Guid? = QueryHandler.SelectQuery
        If _ID IsNot Nothing Then

            m_QueryID = _ID

            'fetch the data
            Dim _DT As DataTable = QueryHandler.GetQueryData(_ID.Value)
            RefreshReportFields(_DT)

        End If

    End Sub

    Private Sub RefreshReportFields(ByRef DT As DataTable)

        If DT IsNot Nothing Then

            'create temp schema from data
            Dim _Schema As String = CreateTempSchema(DT)
            If _Schema <> "" Then
                XrDesignMdiController1.ActiveDesignPanel.Report.DataSourceSchema = _Schema
                XrDesignMdiController1.ActiveDesignPanel.Report.DataSource = DT
                XrDesignMdiController1.ActiveDesignPanel.Report.FillDataSource()
                RefreshFieldList()
            End If

        End If

    End Sub

    Private Sub bbiDataSourceXSD_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiDataSourceXSD.ItemClick
        XrDesignMdiController1.ActiveDesignPanel.Report.DataSourceSchema = Nothing
        XrDesignMdiController1.ActiveDesignPanel.Report.DataSourceSchema = ReturnXMLSchema()
        XrDesignMdiController1.ActiveDesignPanel.Report.FillDataSource()
        RefreshFieldList()
    End Sub

    Public Class OpenCommandHandler

        Implements DevExpress.XtraReports.UserDesigner.ICommandHandler
        Private panel As XRDesignPanel

        Public Sub New(ByVal panel As XRDesignPanel)
            Me.panel = panel
        End Sub

        Public Sub HandleCommand(ByVal command As DevExpress.XtraReports.UserDesigner.ReportCommand, ByVal args() As Object) Implements DevExpress.XtraReports.UserDesigner.ICommandHandler.HandleCommand

            Select Case command

                Case ReportCommand.OpenFile
                    Open()

                Case ReportCommand.SaveFile, ReportCommand.SaveAll, ReportCommand.SaveFileAs
                    SaveReport()

            End Select

        End Sub

        Public Function CanHandleCommand(ByVal command As DevExpress.XtraReports.UserDesigner.ReportCommand, ByRef useNextHandler As Boolean) As Boolean Implements DevExpress.XtraReports.UserDesigner.ICommandHandler.CanHandleCommand

            Select Case command

                Case ReportCommand.OpenFile
                    useNextHandler = False
                    Return True

                Case ReportCommand.SaveFile, ReportCommand.SaveAll, ReportCommand.SaveFileAs
                    useNextHandler = False
                    Return True

            End Select

            Return Not useNextHandler

        End Function

        Private Sub Open()
            Dim _d As New OpenFileDialog
            _d.InitialDirectory = Session.ReportFolder
            _d.Filter = "Report Files (*.repx)|*.repx"
            If _d.ShowDialog = DialogResult.OK Then
                panel.OpenReport(_d.FileName)
            End If
        End Sub

        Private Sub SaveReport()

            If panel.ReportState = ReportState.Changed Then
                If panel.FileName = "" Then
                    DoSaveAs()
                Else
                    If Not DoSave() Then
                        DoSaveAs()
                    End If
                End If
            End If

        End Sub

        Private Function DoSave() As Boolean

            Try
                panel.SaveReport()

            Catch ex As Exception
                Return False
            End Try

            Return True

        End Function

        Private Function DoSaveAs() As Boolean

            Try

                Dim _d As New SaveFileDialog
                _d.InitialDirectory = Session.ReportFolder
                _d.Filter = "Report Files (*.repx)|*.repx"

                If _d.ShowDialog = DialogResult.OK Then
                    panel.SaveReport(_d.FileName)
                End If

            Catch ex As Exception
                Return False
            End Try

            Return True

        End Function

    End Class

    Private Sub mdiController_DesignPanelLoaded(ByVal sender As Object, ByVal e As DesignerLoadedEventArgs)
        Dim panel As XRDesignPanel = CType(sender, XRDesignPanel)
        XrDesignMdiController1.AddCommandHandler(New OpenCommandHandler(panel))
    End Sub

    Private Sub bbiSaveDB_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiSaveDB.ItemClick

        Dim _d As New SaveFileDialog
        _d.InitialDirectory = Session.ReportFolder
        _d.Filter = "Report Files (*.repx)|*.repx"

        If _d.ShowDialog = DialogResult.OK Then

            ActiveReport.SaveLayout(_d.FileName)

            Dim _R As New Business.AppReport
            With _R
                ._Parent = InputBox("Enter Parent", "Save Report to Repository")
                ._Name = InputBox("Enter Report Name", "Save Report to Repository")
                ._QueryId = m_QueryID
                ._ReportFile = _d.FileName
                ._AdHoc = True
                .Store()
            End With

        End If

    End Sub

    Private Sub bbiNewReport_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiNewReport.ItemClick

        m_ReportID = Nothing
        m_QueryID = Nothing
        m_ReportMode = EnumReportMode.FromFile

        Dim _NewReport As New XtraReport
        With _NewReport
            .PaperKind = Drawing.Printing.PaperKind.A4
            .Font = New Drawing.Font("Segoe UI", 10)
            .SnappingMode = SnappingMode.SnapToGrid
        End With

        XrDesignMdiController1.OpenReport(_NewReport)

    End Sub

    Private Sub bbiOpen_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiOpen.ItemClick
        OpenFile()
    End Sub

    Private Sub bbiOpenFile_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiOpenFile.ItemClick
        OpenFile()
    End Sub

    Private Sub OpenFile()

        Dim _d As New OpenFileDialog
        _d.InitialDirectory = Session.ReportFolder
        _d.Filter = "Report Files (*.repx)|*.repx"

        If _d.ShowDialog = DialogResult.OK Then
            OpenReportFile(_d.FileName)
        End If

    End Sub

    Private Sub OpenReportFile(ByVal ReportPath As String)
        XrDesignMdiController1.OpenReport(ReportPath)
        m_ReportFile = ReportPath
    End Sub

    Private Sub bbiOpenDB_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiOpenDB.ItemClick
        Dim _ID As Guid? = ReportHandler.SelectReport(False)
        If _ID IsNot Nothing Then
            OpenReportByID(_ID.Value)
        End If
    End Sub

    Private Sub OpenReportByID(ByVal ReportID As Guid)

        Dim _R As Business.AppReport = Business.AppReport.RetreiveByID(ReportID)
        If _R IsNot Nothing Then

            XrDesignMdiController1.OpenReport(_R._ReportFile)

            Dim _DT As DataTable = Nothing

            If _R._QueryId.ToString = "" Then
                bbiDataSourceQuery.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
                Dim _SQL As String = ""
                _SQL += _R._SqlQuery + " " + _R._SqlOrder
                _DT = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            Else
                bbiDataSourceQuery.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                _DT = QueryHandler.GetQueryData(_R._QueryId.Value)
            End If

            If _DT IsNot Nothing Then
                RefreshReportFields(_DT)
            End If

        End If

    End Sub

    Public Sub LoadReportWithCustomDataSource(Of T)(ByVal ReportFile As String, ByVal ReportData As List(Of T))

        Dim _FilePath As String = Session.ReportFolder + ReportFile
        OpenReportFile(_FilePath)

        XrDesignMdiController1.ActiveDesignPanel.Report.DataSource = ReportData
        RefreshFieldList()

    End Sub

    Public Sub LoadReport(ByVal ReportFile As String, ByVal ConnectionString As String, ByVal SQL As String)

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(ConnectionString, SQL)
        If _DT IsNot Nothing Then

            Dim _FilePath As String = Session.ReportFolder + ReportFile
            OpenReportFile(_FilePath)

            XrDesignMdiController1.ActiveDesignPanel.Report.DataSource = _DT
            RefreshFieldList()

        End If

    End Sub

End Class
