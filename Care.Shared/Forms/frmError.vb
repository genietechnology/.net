﻿Imports Care.Global
Imports System.Windows.Forms

Public Class frmError

    Private m_Ex As Exception

    Public WriteOnly Property ExceptionObject() As System.Exception
        Set(ByVal value As System.Exception)
            m_Ex = value
        End Set
    End Property

    Private Sub frmError_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtFullname.Text = m_Ex.TargetSite.DeclaringType.FullName
        txtBase.Text = ErrorHandler.ReturnBaseHierarchy(m_Ex.TargetSite.ReflectedType)
        txtTargetSite.Text = m_Ex.TargetSite.Name
        txtMessage.Text = m_Ex.Message
        txtStack.Text = m_Ex.StackTrace
        CopyToClip()
    End Sub

    Private Sub CopyToClip()

        Dim _err As String = ""

        _err += "Fullname   : " & txtFullname.Text & vbCrLf & vbCrLf
        _err += "Base: " & vbCrLf & txtBase.Text & vbCrLf
        _err += "Target Site: " & txtTargetSite.Text & vbCrLf & vbCrLf
        _err += "Message    : " & txtMessage.Text & vbCrLf & vbCrLf
        _err += "Stack Trace: " & txtStack.Text & vbCrLf

        Try
            Clipboard.Clear()
            Clipboard.SetText(_err)

        Catch ex As Exception

        End Try

    End Sub

End Class
