﻿Imports Care.Global

Public Class frmDataPreview

    Private m_SQL As String = ""

    Public Sub New(ByVal SQLQuery As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        m_SQL = SQLQuery

    End Sub

    Private Sub frmDataPreview_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Grid.Populate(Session.ConnectionString, m_SQL)
        Cursor.Current = Cursors.Default
    End Sub

End Class