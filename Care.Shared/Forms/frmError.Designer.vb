﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmError
    Inherits Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New Label()
        Me.Label2 = New Label()
        Me.Label3 = New Label()
        Me.Label4 = New Label()
        Me.Label5 = New Label()
        Me.Label6 = New Label()
        Me.Label7 = New Label()
        Me.txtFullname = New Care.Controls.CareTextBox()
        Me.txtTargetSite = New Care.Controls.CareTextBox()
        Me.Label8 = New Label()
        Me.txtMessage = New Care.Controls.CareTextBox()
        Me.PictureBox1 = New PictureBox()
        Me.txtStack = New DevExpress.XtraEditors.MemoEdit()
        Me.txtBase = New DevExpress.XtraEditors.MemoEdit()
        CType(Me.txtFullname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTargetSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMessage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStack.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBase.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(68, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(426, 21)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Oops! The application has encountered an unexpected error."
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(68, 41)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(551, 21)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Please retry the operation again, if the problem persists contact our Help Desk."
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(10, 79)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(414, 21)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "The information below is used by our support department."
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 152)
        Me.Label4.Margin = New Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(56, 15)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Fullname"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(12, 125)
        Me.Label5.Margin = New Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(63, 15)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Target Site"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 260)
        Me.Label6.Margin = New Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(53, 15)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Message"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(12, 290)
        Me.Label7.Margin = New Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(67, 15)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "Stack Trace"
        '
        'txtFullname
        '
        Me.txtFullname.CharacterCasing = CharacterCasing.Normal
        Me.txtFullname.Location = New System.Drawing.Point(101, 149)
        Me.txtFullname.Margin = New Padding(2)
        Me.txtFullname.MaxLength = 0
        Me.txtFullname.Name = "txtFullname"
        Me.txtFullname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFullname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFullname.Properties.Appearance.Options.UseFont = True
        Me.txtFullname.ReadOnly = False
        Me.txtFullname.Size = New System.Drawing.Size(518, 22)
        Me.txtFullname.TabIndex = 9
        Me.txtFullname.TabStop = False
        Me.txtFullname.TextAlign = HorizontalAlignment.Left
        Me.txtFullname.ToolTipText = ""
        '
        'txtTargetSite
        '
        Me.txtTargetSite.CharacterCasing = CharacterCasing.Normal
        Me.txtTargetSite.Location = New System.Drawing.Point(101, 122)
        Me.txtTargetSite.Margin = New Padding(2)
        Me.txtTargetSite.MaxLength = 0
        Me.txtTargetSite.Name = "txtTargetSite"
        Me.txtTargetSite.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTargetSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTargetSite.Properties.Appearance.Options.UseFont = True
        Me.txtTargetSite.ReadOnly = False
        Me.txtTargetSite.Size = New System.Drawing.Size(518, 22)
        Me.txtTargetSite.TabIndex = 11
        Me.txtTargetSite.TabStop = False
        Me.txtTargetSite.TextAlign = HorizontalAlignment.Left
        Me.txtTargetSite.ToolTipText = ""
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(12, 179)
        Me.Label8.Margin = New Padding(2, 0, 2, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(85, 15)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Base Hierarchy"
        '
        'txtMessage
        '
        Me.txtMessage.CharacterCasing = CharacterCasing.Normal
        Me.txtMessage.Location = New System.Drawing.Point(101, 257)
        Me.txtMessage.Margin = New Padding(2)
        Me.txtMessage.MaxLength = 0
        Me.txtMessage.Name = "txtMessage"
        Me.txtMessage.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtMessage.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMessage.Properties.Appearance.Options.UseFont = True
        Me.txtMessage.ReadOnly = False
        Me.txtMessage.Size = New System.Drawing.Size(518, 22)
        Me.txtMessage.TabIndex = 14
        Me.txtMessage.TabStop = False
        Me.txtMessage.TextAlign = HorizontalAlignment.Left
        Me.txtMessage.ToolTipText = ""
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.Care.[Shared].My.Resources.Resources.cancel
        Me.PictureBox1.Location = New System.Drawing.Point(15, 24)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(32, 32)
        Me.PictureBox1.SizeMode = PictureBoxSizeMode.AutoSize
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'txtStack
        '
        Me.txtStack.Location = New System.Drawing.Point(101, 284)
        Me.txtStack.Name = "txtStack"
        Me.txtStack.Size = New System.Drawing.Size(518, 304)
        Me.txtStack.TabIndex = 16
        '
        'txtBase
        '
        Me.txtBase.Location = New System.Drawing.Point(102, 176)
        Me.txtBase.Name = "txtBase"
        Me.txtBase.Size = New System.Drawing.Size(517, 76)
        Me.txtBase.TabIndex = 17
        '
        'frmError
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(631, 600)
        Me.Controls.Add(Me.txtBase)
        Me.Controls.Add(Me.txtStack)
        Me.Controls.Add(Me.txtMessage)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtTargetSite)
        Me.Controls.Add(Me.txtFullname)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmError"
        Me.ShowInTaskbar = False
        Me.StartPosition = FormStartPosition.CenterParent
        Me.Text = "Unhandled Exception"
        Me.TopMost = True
        CType(Me.txtFullname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTargetSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMessage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStack.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBase.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents txtFullname As Care.Controls.CareTextBox
    Friend WithEvents txtTargetSite As Care.Controls.CareTextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txtMessage As Care.Controls.CareTextBox
    Friend WithEvents txtStack As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtBase As DevExpress.XtraEditors.MemoEdit

End Class
