﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCRMActivity
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.GroupControl4 = New Care.Controls.CareFrame()
        Me.memNotes = New DevExpress.XtraRichEdit.RichEditControl()
        Me.CareLabel8 = New Care.Controls.CareLabel()
        Me.txtSubject = New Care.Controls.CareTextBox()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.CareLabel3 = New Care.Controls.CareLabel()
        Me.txtActivityType = New Care.Controls.CareTextBox()
        Me.CareLabel1 = New Care.Controls.CareLabel()
        Me.txtContact = New Care.Controls.CareTextBox()
        Me.CareLabel2 = New Care.Controls.CareLabel()
        Me.txtLinkType = New Care.Controls.CareTextBox()
        Me.GroupControl2 = New Care.Controls.CareFrame()
        Me.radIn = New Care.Controls.CareRadioButton()
        Me.radOut = New Care.Controls.CareRadioButton()
        Me.cdtActionDate = New Care.Controls.CareDateTime()
        Me.CareLabel5 = New Care.Controls.CareLabel()
        Me.CareLabel6 = New Care.Controls.CareLabel()
        Me.GroupControl3 = New Care.Controls.CareFrame()
        Me.chkFollowUp = New Care.Controls.CareCheckBox()
        Me.cdtFollowUp = New Care.Controls.CareDateTime()
        Me.CareLabel4 = New Care.Controls.CareLabel()
        Me.CareLabel7 = New Care.Controls.CareLabel()
        Me.btnCancel = New Care.Controls.CareButton()
        Me.btnOK = New Care.Controls.CareButton()
        Me.btnDelete = New Care.Controls.CareButton()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.txtSubject.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtActivityType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtContact.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLinkType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.radIn.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radOut.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtActionDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtActionDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.chkFollowUp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtFollowUp.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtFollowUp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl4
        '
        Me.GroupControl4.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl4.Controls.Add(Me.memNotes)
        Me.GroupControl4.Controls.Add(Me.CareLabel8)
        Me.GroupControl4.Controls.Add(Me.txtSubject)
        Me.GroupControl4.Location = New System.Drawing.Point(12, 192)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(960, 432)
        Me.GroupControl4.TabIndex = 3
        Me.GroupControl4.Text = "Details"
        '
        'memNotes
        '
        Me.memNotes.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple
        Me.memNotes.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.memNotes.Location = New System.Drawing.Point(10, 55)
        Me.memNotes.Name = "memNotes"
        Me.memNotes.Size = New System.Drawing.Size(939, 370)
        Me.memNotes.SpellChecker = Me.SpellChecker1
        Me.SpellChecker1.SetSpellCheckerOptions(Me.memNotes, OptionsSpelling1)
        Me.memNotes.TabIndex = 2
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(10, 30)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(39, 15)
        Me.CareLabel8.TabIndex = 0
        Me.CareLabel8.Text = "Subject"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSubject
        '
        Me.txtSubject.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtSubject.CharacterCasing = CharacterCasing.Normal
        Me.txtSubject.EnterMoveNextControl = True
        Me.txtSubject.Location = New System.Drawing.Point(100, 27)
        Me.txtSubject.MaxLength = 100
        Me.txtSubject.Name = "txtSubject"
        Me.txtSubject.NumericAllowNegatives = False
        Me.txtSubject.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSubject.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSubject.Properties.AccessibleName = "Contact Forename"
        Me.txtSubject.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSubject.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSubject.Properties.Appearance.Options.UseFont = True
        Me.txtSubject.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSubject.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSubject.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSubject.Properties.MaxLength = 100
        Me.txtSubject.Size = New System.Drawing.Size(849, 22)
        Me.txtSubject.TabIndex = 1
        Me.txtSubject.Tag = ""
        Me.txtSubject.TextAlign = HorizontalAlignment.Left
        Me.txtSubject.ToolTipText = ""
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Controls.Add(Me.txtActivityType)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Controls.Add(Me.txtContact)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.txtLinkType)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(960, 84)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Source Details"
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(322, 30)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(69, 15)
        Me.CareLabel3.TabIndex = 2
        Me.CareLabel3.Text = "Activity Type"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtActivityType
        '
        Me.txtActivityType.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtActivityType.CharacterCasing = CharacterCasing.Normal
        Me.txtActivityType.EnterMoveNextControl = True
        Me.txtActivityType.Location = New System.Drawing.Point(415, 27)
        Me.txtActivityType.MaxLength = 30
        Me.txtActivityType.Name = "txtActivityType"
        Me.txtActivityType.NumericAllowNegatives = False
        Me.txtActivityType.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtActivityType.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtActivityType.Properties.AccessibleName = "Contact Forename"
        Me.txtActivityType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtActivityType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtActivityType.Properties.Appearance.Options.UseFont = True
        Me.txtActivityType.Properties.Appearance.Options.UseTextOptions = True
        Me.txtActivityType.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtActivityType.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtActivityType.Properties.MaxLength = 30
        Me.txtActivityType.Properties.ReadOnly = True
        Me.txtActivityType.Size = New System.Drawing.Size(534, 22)
        Me.txtActivityType.TabIndex = 3
        Me.txtActivityType.Tag = ""
        Me.txtActivityType.TextAlign = HorizontalAlignment.Left
        Me.txtActivityType.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(10, 30)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(51, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Link Type"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtContact
        '
        Me.txtContact.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtContact.CharacterCasing = CharacterCasing.Normal
        Me.txtContact.EnterMoveNextControl = True
        Me.txtContact.Location = New System.Drawing.Point(100, 55)
        Me.txtContact.MaxLength = 30
        Me.txtContact.Name = "txtContact"
        Me.txtContact.NumericAllowNegatives = False
        Me.txtContact.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtContact.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtContact.Properties.AccessibleName = "Contact Surname"
        Me.txtContact.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtContact.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtContact.Properties.Appearance.Options.UseFont = True
        Me.txtContact.Properties.Appearance.Options.UseTextOptions = True
        Me.txtContact.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtContact.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtContact.Properties.MaxLength = 30
        Me.txtContact.Properties.ReadOnly = True
        Me.txtContact.Size = New System.Drawing.Size(849, 22)
        Me.txtContact.TabIndex = 5
        Me.txtContact.Tag = ""
        Me.txtContact.TextAlign = HorizontalAlignment.Left
        Me.txtContact.ToolTipText = ""
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(10, 58)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(42, 15)
        Me.CareLabel2.TabIndex = 4
        Me.CareLabel2.Text = "Contact"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLinkType
        '
        Me.txtLinkType.CharacterCasing = CharacterCasing.Normal
        Me.txtLinkType.EnterMoveNextControl = True
        Me.txtLinkType.Location = New System.Drawing.Point(100, 27)
        Me.txtLinkType.MaxLength = 30
        Me.txtLinkType.Name = "txtLinkType"
        Me.txtLinkType.NumericAllowNegatives = False
        Me.txtLinkType.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtLinkType.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLinkType.Properties.AccessibleName = "Contact Forename"
        Me.txtLinkType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtLinkType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtLinkType.Properties.Appearance.Options.UseFont = True
        Me.txtLinkType.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLinkType.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtLinkType.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtLinkType.Properties.MaxLength = 30
        Me.txtLinkType.Properties.ReadOnly = True
        Me.txtLinkType.Size = New System.Drawing.Size(200, 22)
        Me.txtLinkType.TabIndex = 1
        Me.txtLinkType.Tag = ""
        Me.txtLinkType.TextAlign = HorizontalAlignment.Left
        Me.txtLinkType.ToolTipText = ""
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.radIn)
        Me.GroupControl2.Controls.Add(Me.radOut)
        Me.GroupControl2.Controls.Add(Me.cdtActionDate)
        Me.GroupControl2.Controls.Add(Me.CareLabel5)
        Me.GroupControl2.Controls.Add(Me.CareLabel6)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 102)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(310, 84)
        Me.GroupControl2.TabIndex = 1
        Me.GroupControl2.Text = "Action"
        '
        'radIn
        '
        Me.radIn.CausesValidation = False
        Me.radIn.Location = New System.Drawing.Point(174, 56)
        Me.radIn.Name = "radIn"
        Me.radIn.Properties.Appearance.Options.UseTextOptions = True
        Me.radIn.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radIn.Properties.AutoWidth = True
        Me.radIn.Properties.Caption = "Inbound"
        Me.radIn.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radIn.Properties.RadioGroupIndex = 0
        Me.radIn.Size = New System.Drawing.Size(62, 19)
        Me.radIn.TabIndex = 6
        Me.radIn.TabStop = False
        '
        'radOut
        '
        Me.radOut.CausesValidation = False
        Me.radOut.EditValue = True
        Me.radOut.Location = New System.Drawing.Point(98, 56)
        Me.radOut.Name = "radOut"
        Me.radOut.Properties.Appearance.Options.UseTextOptions = True
        Me.radOut.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radOut.Properties.AutoWidth = True
        Me.radOut.Properties.Caption = "Outbound"
        Me.radOut.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radOut.Properties.RadioGroupIndex = 0
        Me.radOut.Size = New System.Drawing.Size(70, 19)
        Me.radOut.TabIndex = 5
        '
        'cdtActionDate
        '
        Me.cdtActionDate.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtActionDate.EnterMoveNextControl = True
        Me.cdtActionDate.Location = New System.Drawing.Point(100, 27)
        Me.cdtActionDate.Name = "cdtActionDate"
        Me.cdtActionDate.Properties.AccessibleName = "Date of Birth"
        Me.cdtActionDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtActionDate.Properties.Appearance.Options.UseFont = True
        Me.cdtActionDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtActionDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtActionDate.Size = New System.Drawing.Size(135, 22)
        Me.cdtActionDate.TabIndex = 1
        Me.cdtActionDate.Tag = ""
        Me.cdtActionDate.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(10, 30)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(62, 15)
        Me.CareLabel5.TabIndex = 0
        Me.CareLabel5.Text = "Action Date"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(10, 58)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(48, 15)
        Me.CareLabel6.TabIndex = 2
        Me.CareLabel6.Text = "Direction"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl3
        '
        Me.GroupControl3.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl3.Controls.Add(Me.chkFollowUp)
        Me.GroupControl3.Controls.Add(Me.cdtFollowUp)
        Me.GroupControl3.Controls.Add(Me.CareLabel4)
        Me.GroupControl3.Controls.Add(Me.CareLabel7)
        Me.GroupControl3.Location = New System.Drawing.Point(328, 102)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(644, 84)
        Me.GroupControl3.TabIndex = 2
        Me.GroupControl3.Text = "Follow Up"
        '
        'chkFollowUp
        '
        Me.chkFollowUp.EnterMoveNextControl = True
        Me.chkFollowUp.Location = New System.Drawing.Point(123, 28)
        Me.chkFollowUp.Name = "chkFollowUp"
        Me.chkFollowUp.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkFollowUp.Properties.Appearance.Options.UseFont = True
        Me.chkFollowUp.Properties.AutoWidth = True
        Me.chkFollowUp.Properties.Caption = ""
        Me.chkFollowUp.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkFollowUp.Size = New System.Drawing.Size(19, 19)
        Me.chkFollowUp.TabIndex = 1
        Me.chkFollowUp.Tag = ""
        '
        'cdtFollowUp
        '
        Me.cdtFollowUp.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtFollowUp.EnterMoveNextControl = True
        Me.cdtFollowUp.Location = New System.Drawing.Point(125, 55)
        Me.cdtFollowUp.Name = "cdtFollowUp"
        Me.cdtFollowUp.Properties.AccessibleName = "Date of Birth"
        Me.cdtFollowUp.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtFollowUp.Properties.Appearance.Options.UseFont = True
        Me.cdtFollowUp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtFollowUp.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtFollowUp.Size = New System.Drawing.Size(135, 22)
        Me.cdtFollowUp.TabIndex = 3
        Me.cdtFollowUp.Tag = ""
        Me.cdtFollowUp.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(10, 30)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(101, 15)
        Me.CareLabel4.TabIndex = 0
        Me.CareLabel4.Text = "Requires Follow Up"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(10, 58)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(80, 15)
        Me.CareLabel7.TabIndex = 2
        Me.CareLabel7.Text = "Follow Up Date"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.DialogResult = DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(897, 630)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 6
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.DialogResult = DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(816, 630)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 5
        Me.btnOK.Text = "OK"
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnDelete.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Appearance.Options.UseFont = True
        Me.btnDelete.DialogResult = DialogResult.OK
        Me.btnDelete.Location = New System.Drawing.Point(12, 630)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(75, 23)
        Me.btnDelete.TabIndex = 4
        Me.btnDelete.Text = "Delete"
        '
        'frmCRMActivity
        '
        Me.AcceptButton = Me.btnOK
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(984, 661)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.GroupControl1)
        Me.FormBorderStyle = FormBorderStyle.Sizable
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(640, 400)
        Me.Name = "frmCRMActivity"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = ""
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.txtSubject.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtActivityType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtContact.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLinkType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.radIn.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radOut.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtActionDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtActionDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.chkFollowUp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtFollowUp.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtFollowUp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents txtContact As Care.Controls.CareTextBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents txtLinkType As Care.Controls.CareTextBox
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents txtActivityType As Care.Controls.CareTextBox
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents txtSubject As Care.Controls.CareTextBox
    Private WithEvents cdtActionDate As Care.Controls.CareDateTime
    Private WithEvents cdtFollowUp As Care.Controls.CareDateTime
    Friend WithEvents chkFollowUp As Care.Controls.CareCheckBox
    Friend WithEvents radIn As Care.Controls.CareRadioButton
    Friend WithEvents radOut As Care.Controls.CareRadioButton
    Friend WithEvents btnDelete As Care.Controls.CareButton
    Protected Friend WithEvents memNotes As DevExpress.XtraRichEdit.RichEditControl
End Class
