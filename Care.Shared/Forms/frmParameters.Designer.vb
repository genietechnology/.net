﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmParameters
    Inherits Care.Shared.frmGridMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtParent = New Care.Controls.CareTextBox()
        Me.txtName = New Care.Controls.CareTextBox()
        Me.txtDescription = New Care.Controls.CareTextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cbxType = New Care.Controls.CareComboBox()
        Me.txtValue = New DevExpress.XtraEditors.MemoEdit()
        Me.gbx.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.txtParent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescription.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbx
        '
        Me.gbx.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.gbx.Controls.Add(Me.txtValue)
        Me.gbx.Controls.Add(Me.cbxType)
        Me.gbx.Controls.Add(Me.Label5)
        Me.gbx.Controls.Add(Me.Label4)
        Me.gbx.Controls.Add(Me.Label3)
        Me.gbx.Controls.Add(Me.Label2)
        Me.gbx.Controls.Add(Me.txtDescription)
        Me.gbx.Controls.Add(Me.txtName)
        Me.gbx.Controls.Add(Me.Label1)
        Me.gbx.Controls.Add(Me.txtParent)
        Me.gbx.Location = New System.Drawing.Point(27, 89)
        Me.gbx.Size = New System.Drawing.Size(679, 363)
        Me.gbx.Controls.SetChildIndex(Me.txtParent, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label1, 0)
        Me.gbx.Controls.SetChildIndex(Me.Panel2, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtName, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtDescription, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label2, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label3, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label4, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label5, 0)
        Me.gbx.Controls.SetChildIndex(Me.cbxType, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtValue, 0)
        '
        'Panel2
        '
        Me.Panel2.Location = New System.Drawing.Point(3, 332)
        Me.Panel2.Size = New System.Drawing.Size(673, 28)
        Me.Panel2.TabIndex = 5
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(488, 0)
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(579, 0)
        '
        'Grid
        '
        Me.Grid.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Grid.Appearance.Options.UseFont = True
        Me.Grid.ShowFindPanel = True
        Me.Grid.Size = New System.Drawing.Size(715, 509)
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(0, 527)
        Me.Panel1.Size = New System.Drawing.Size(734, 35)
        '
        'btnDelete
        '
        Me.btnDelete.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Appearance.Options.UseFont = True
        '
        'btnEdit
        '
        Me.btnEdit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.Appearance.Options.UseFont = True
        '
        'btnAdd
        '
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.Appearance.Options.UseFont = True
        '
        'btnClose
        '
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(3430, 5)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 15)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Parent"
        '
        'txtParent
        '
        Me.txtParent.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtParent.EnterMoveNextControl = True
        Me.txtParent.Location = New System.Drawing.Point(79, 22)
        Me.txtParent.MaxLength = 30
        Me.txtParent.Name = "txtParent"
        Me.txtParent.NumericAllowNegatives = False
        Me.txtParent.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtParent.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtParent.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtParent.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtParent.Properties.Appearance.Options.UseFont = True
        Me.txtParent.Properties.Appearance.Options.UseTextOptions = True
        Me.txtParent.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtParent.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtParent.Properties.MaxLength = 30
        Me.txtParent.Size = New System.Drawing.Size(170, 22)
        Me.txtParent.TabIndex = 0
        Me.txtParent.Tag = "AE"
        Me.txtParent.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtParent.ToolTipText = ""
        '
        'txtName
        '
        Me.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(79, 51)
        Me.txtName.MaxLength = 20
        Me.txtName.Name = "txtName"
        Me.txtName.NumericAllowNegatives = False
        Me.txtName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtName.Properties.MaxLength = 20
        Me.txtName.Size = New System.Drawing.Size(170, 22)
        Me.txtName.TabIndex = 1
        Me.txtName.Tag = "AEMC"
        Me.txtName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'txtDescription
        '
        Me.txtDescription.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtDescription.EnterMoveNextControl = True
        Me.txtDescription.Location = New System.Drawing.Point(79, 109)
        Me.txtDescription.MaxLength = 30
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.NumericAllowNegatives = False
        Me.txtDescription.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtDescription.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDescription.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtDescription.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.Properties.Appearance.Options.UseFont = True
        Me.txtDescription.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDescription.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtDescription.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDescription.Properties.MaxLength = 30
        Me.txtDescription.Size = New System.Drawing.Size(588, 22)
        Me.txtDescription.TabIndex = 3
        Me.txtDescription.Tag = "AEM"
        Me.txtDescription.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescription.ToolTipText = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 54)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 15)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Name"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 83)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(32, 15)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Type"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 112)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(67, 15)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Description"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 141)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(35, 15)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Value"
        '
        'cbxType
        '
        Me.cbxType.AllowBlank = False
        Me.cbxType.DataSource = Nothing
        Me.cbxType.DisplayMember = Nothing
        Me.cbxType.EnterMoveNextControl = True
        Me.cbxType.Location = New System.Drawing.Point(79, 81)
        Me.cbxType.Name = "cbxType"
        Me.cbxType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxType.Properties.Appearance.Options.UseFont = True
        Me.cbxType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxType.SelectedValue = Nothing
        Me.cbxType.Size = New System.Drawing.Size(170, 22)
        Me.cbxType.TabIndex = 2
        Me.cbxType.Tag = "AEM"
        Me.cbxType.ValueMember = Nothing
        '
        'txtValue
        '
        Me.txtValue.Location = New System.Drawing.Point(79, 137)
        Me.txtValue.Name = "txtValue"
        Me.txtValue.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtValue.Properties.Appearance.Options.UseFont = True
        Me.txtValue.Properties.MaxLength = 1024
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtValue, True)
        Me.txtValue.Size = New System.Drawing.Size(588, 189)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtValue, OptionsSpelling1)
        Me.txtValue.TabIndex = 4
        Me.txtValue.Tag = "AEM"
        '
        'frmParameters
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(734, 562)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.LoadMaximised = True
        Me.MaximizeBox = True
        Me.Name = "frmParameters"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.gbx.ResumeLayout(False)
        Me.gbx.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.txtParent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescription.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDescription As Care.Controls.CareTextBox
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtParent As Care.Controls.CareTextBox
    Friend WithEvents cbxType As Care.Controls.CareComboBox
    Friend WithEvents txtValue As DevExpress.XtraEditors.MemoEdit

End Class
