﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDataPreview
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Grid = New Care.Controls.CareGrid()
        Me.SuspendLayout()
        '
        'Grid
        '
        Me.Grid.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Grid.Appearance.Options.UseFont = True
        Me.Grid.Dock = DockStyle.Fill
        Me.Grid.HideFirstColumn = False
        Me.Grid.Location = New System.Drawing.Point(0, 0)
        Me.Grid.Name = "Grid"
        Me.Grid.ShowGroupByBox = True
        Me.Grid.Size = New System.Drawing.Size(734, 366)
        Me.Grid.TabIndex = 0
        '
        'frmDataPreview
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(734, 366)
        Me.Controls.Add(Me.Grid)
        Me.FormBorderStyle = FormBorderStyle.SizableToolWindow
        Me.Name = "frmDataPreview"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "Data Preview"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Grid As Care.Controls.CareGrid
End Class
