﻿Imports Care.Global
Imports System.Windows.Forms

Public Class frmGenericFind

    Dim m_FormCaption As String
    Dim m_PopulateOnLoad As Boolean
    Dim m_GridReturn As String
    Dim m_ConnectionString As String
    Dim m_GridSQL As String
    Dim m_GridWhere As String
    Dim m_GridOrderBy As String
    Dim m_Option1Field As String
    Dim m_Option1Caption As String
    Dim m_Option2Field As String
    Dim m_Option2Caption As String
    Dim m_Option3Field As String
    Dim m_Option3Caption As String
    Dim m_Option4Field As String
    Dim m_Option4Caption As String

    Dim m_HideFirstColumn As Boolean
    Dim m_Initialised As Boolean = False
    Dim m_SearchType As GenericFind.EnumSearchMode
    Dim m_UseSquareBrackets As Boolean = True
    Dim m_PreviewField As String = ""
    Dim m_AutoFilterRow As Boolean = False

    Public Sub New(ByVal FormCaption As String, ByVal PopulateOnLoad As Boolean,
                   ByVal ReturnField As String, ByVal ConnectionString As String,
                   ByVal GridSQL As String, ByVal GridWhereClause As String, ByVal GridOrderBy As String,
                   ByVal HideFirstColumn As Boolean,
                   ByVal SearchMode As GenericFind.EnumSearchMode,
                   ByVal Option1Field As String, ByVal Option1Caption As String,
                   ByVal Option2Field As String, ByVal Option2Caption As String,
                   ByVal Option3Field As String, ByVal Option3Caption As String,
                   ByVal Option4Field As String, ByVal Option4Caption As String,
                   Optional ByVal FormWidth As Integer = 0,
                   Optional ByVal FormHeight As Integer = 0,
                   Optional ByVal UseSquareBrackets As Boolean = True,
                   Optional ByVal PreviewField As String = "",
                   Optional ByVal ShowAutoFilterRow As Boolean = False)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        m_FormCaption = FormCaption
        m_PopulateOnLoad = PopulateOnLoad
        m_GridReturn = ReturnField
        m_ConnectionString = ConnectionString
        m_GridSQL = GridSQL
        m_GridWhere = GridWhereClause
        m_GridOrderBy = GridOrderBy

        m_UseSquareBrackets = UseSquareBrackets
        m_PreviewField = PreviewField
        m_AutoFilterRow = ShowAutoFilterRow

        m_Option1Field = Option1Field
        m_Option1Caption = Option1Caption
        SetField(m_Option1Field, Option1Field)

        m_Option2Field = Option2Field
        m_Option2Caption = Option2Caption
        SetField(m_Option2Field, Option2Field)

        m_Option3Field = Option3Field
        m_Option3Caption = Option3Caption
        SetField(m_Option3Field, Option3Field)

        m_Option4Field = Option4Field
        m_Option4Caption = Option4Caption
        SetField(m_Option4Field, Option4Field)

        m_HideFirstColumn = HideFirstColumn

        If FormWidth > 0 Then Me.Width = FormWidth
        If FormHeight > 0 Then Me.Height = FormHeight

        m_SearchType = SearchMode
        SetLayout()

        m_Initialised = True

    End Sub

    Private Sub SetField(ByRef mField As String, ByVal FieldValue As String)

        If FieldValue = "" Then
            mField = ""
            Exit Sub
        End If

        If m_UseSquareBrackets Then
            mField = "[" + FieldValue + "]"
        Else
            mField = FieldValue
        End If

    End Sub

    Private Sub SetLayout()

        If m_SearchType = GenericFind.EnumSearchMode.RadioButtons Then

            rad1.Text = m_Option1Caption
            rad1.Tag = m_Option1Field
            If rad1.Tag.ToString = "" Then rad1.Hide()

            rad2.Text = m_Option2Caption
            rad2.Tag = m_Option2Field
            If rad2.Tag.ToString = "" Then rad2.Hide()

            rad3.Text = m_Option3Caption
            rad3.Tag = m_Option3Field
            If rad3.Tag.ToString = "" Then rad3.Hide()

            rad4.Text = m_Option4Caption
            rad4.Tag = m_Option4Field
            If rad4.Tag.ToString = "" Then rad4.Hide()

            grpRadio.Show()
            gex.ShowFindPanel = False

            'gex.Top = 54

        Else
            grpRadio.Hide()
            gex.ShowFindPanel = True
            'gex.Top = 12
        End If

    End Sub

    Private Sub frmGenericFind_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then Me.Close()
    End Sub

    Private Sub frmGenericFind_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Tag = Nothing
        Me.Text = m_FormCaption
        Me.KeyPreview = True

        btnSelect.Enabled = False
        If m_PopulateOnLoad Then Call RefreshGrid()

    End Sub

    Private Sub RefreshGrid()

        If Not m_Initialised Then Exit Sub

        LockScreen(True)

        Dim _SQL As String = ConstructSQL()
        gex.HideFirstColumn = m_HideFirstColumn
        gex.PreviewColumn = m_PreviewField
        gex.ShowAutoFilterRow = m_AutoFilterRow
        gex.Populate(m_ConnectionString, _SQL)

        If m_PreviewField <> "" Then
            gex.Columns(m_PreviewField).Visible = False
        End If

        'gex.AutoSizeColumns()

        If gex.RecordCount > 0 Then
            btnSelect.Enabled = True
        Else
            btnSelect.Enabled = False
        End If

        LockScreen(False)

    End Sub

    Private Sub LockScreen(ByVal LockControls As Boolean)

        If LockControls Then
            Cursor.Current = Cursors.WaitCursor
        Else
            Cursor.Current = Cursors.Default
        End If

        btnSelect.Enabled = Not LockControls
        btnClose.Enabled = Not LockControls

        Application.DoEvents()

    End Sub

    Private Sub SelectRow(ByVal Row As System.Data.DataRow)
        If Row Is Nothing Then Exit Sub
        If Row.Item(0).ToString <> "" Then
            Me.Tag = Row.Item(0).ToString
            AuditHandler.LogAudit(AuditHandler.EnumAuditScope.Record, Me.Name, New Guid(Me.Tag.ToString), Me.Text, AuditHandler.EnumAuditStatus.Success)
            Me.Close()
        End If
    End Sub

    Private Function ConstructSQL() As String

        Dim _SQL As String = m_GridSQL
        Dim _Return As String = ""
        Dim _Clause As String = ""

        If Not _SQL.StartsWith("select * from") Then
            _SQL = Replace(_SQL, "select", "select " & m_GridReturn.ToUpper & " as 'ReturnValue', ", 1, 1)
            m_HideFirstColumn = True
        End If

        _Clause = " WHERE " + m_GridReturn.ToUpper + " IS NOT NULL"
        _Clause += m_GridWhere

        If m_SearchType = GenericFind.EnumSearchMode.RadioButtons Then
            Dim _RadioSQL As String = BuildCriteria()
            If _RadioSQL <> "" Then
                _Clause += " AND (" + _RadioSQL + ")"
            End If
        End If

        _Return = _SQL & " " & _Clause & " " & m_GridOrderBy

        Return _Return

    End Function

    Private Function RadioField() As String
        If rad1.Checked Then Return rad1.Tag.ToString
        If rad2.Checked Then Return rad2.Tag.ToString
        If rad3.Checked Then Return rad3.Tag.ToString
        If rad4.Checked Then Return rad4.Tag.ToString
        Return ""
    End Function

    Private Function BuildCriteria() As String

        Dim _Return As String = ""

        'split the entered search criteria
        For Each _Word As String In txtCriteria.Text.Split(CChar(" "))

            If _Return = "" Then
                _Return += RadioField() + " LIKE '%" + _Word + "%'"
            Else
                _Return += " AND " + RadioField() + " LIKE '%" + _Word + "%'"
            End If

        Next

        Return _Return

    End Function

    Private Sub gex_RowDoubleClick(sender As Object, e As System.EventArgs) Handles gex.GridDoubleClick
        SelectRow(gex.CurrentRow)
    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnSelect_Click(sender As System.Object, e As System.EventArgs) Handles btnSelect.Click
        SelectRow(gex.CurrentRow)
    End Sub

    Private Sub frmGenericFind_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        If m_SearchType = GenericFind.EnumSearchMode.RadioButtons Then
            txtCriteria.Focus()
        Else
            gex.FocusCriteria()
        End If

    End Sub

    Private Sub txtCriteria_Validating(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles txtCriteria.Validating
        If txtCriteria.Text.TrimEnd = "" Then Exit Sub
        RefreshGrid()
    End Sub

    Private Sub rad1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rad1.CheckedChanged
        txtCriteria.Focus()
    End Sub

    Private Sub rad2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rad2.CheckedChanged
        txtCriteria.Focus()
    End Sub

    Private Sub rad3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rad3.CheckedChanged
        txtCriteria.Focus()
    End Sub

    Private Sub rad4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rad4.CheckedChanged
        txtCriteria.Focus()
    End Sub
End Class