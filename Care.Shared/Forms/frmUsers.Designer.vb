﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUsers
    Inherits Care.Shared.frmGridMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtUserName = New Care.Controls.CareTextBox(Me.components)
        Me.txtSurname = New Care.Controls.CareTextBox(Me.components)
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtForename = New Care.Controls.CareTextBox(Me.components)
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtDepartment = New Care.Controls.CareTextBox(Me.components)
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtJobTitle = New Care.Controls.CareTextBox(Me.components)
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtEmail = New Care.Controls.CareTextBox(Me.components)
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtTel = New Care.Controls.CareTextBox(Me.components)
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnReset = New Care.Controls.CareButton(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxType = New Care.Controls.CareComboBox(Me.components)
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.chkDisabled = New Care.Controls.CareCheckBox(Me.components)
        Me.chkSSO = New Care.Controls.CareCheckBox(Me.components)
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtSSOUsername = New Care.Controls.CareTextBox(Me.components)
        Me.Label11 = New System.Windows.Forms.Label()
        Me.gbx.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.txtUserName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtForename.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDepartment.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJobTitle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkDisabled.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSSO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSSOUsername.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbx
        '
        Me.gbx.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.gbx.Controls.Add(Me.Label11)
        Me.gbx.Controls.Add(Me.txtSSOUsername)
        Me.gbx.Controls.Add(Me.chkSSO)
        Me.gbx.Controls.Add(Me.Label10)
        Me.gbx.Controls.Add(Me.chkDisabled)
        Me.gbx.Controls.Add(Me.Label9)
        Me.gbx.Controls.Add(Me.Label2)
        Me.gbx.Controls.Add(Me.cbxType)
        Me.gbx.Controls.Add(Me.Label1)
        Me.gbx.Controls.Add(Me.txtEmail)
        Me.gbx.Controls.Add(Me.Label7)
        Me.gbx.Controls.Add(Me.txtTel)
        Me.gbx.Controls.Add(Me.Label8)
        Me.gbx.Controls.Add(Me.txtDepartment)
        Me.gbx.Controls.Add(Me.Label5)
        Me.gbx.Controls.Add(Me.txtJobTitle)
        Me.gbx.Controls.Add(Me.Label6)
        Me.gbx.Controls.Add(Me.txtSurname)
        Me.gbx.Controls.Add(Me.Label3)
        Me.gbx.Controls.Add(Me.txtForename)
        Me.gbx.Controls.Add(Me.Label4)
        Me.gbx.Controls.Add(Me.txtUserName)
        Me.gbx.Location = New System.Drawing.Point(87, 215)
        Me.gbx.Size = New System.Drawing.Size(662, 224)
        Me.gbx.TabIndex = 1
        Me.gbx.Controls.SetChildIndex(Me.Panel2, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtUserName, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label4, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtForename, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label3, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtSurname, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label6, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtJobTitle, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label5, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtDepartment, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label8, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtTel, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label7, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtEmail, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label1, 0)
        Me.gbx.Controls.SetChildIndex(Me.cbxType, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label2, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label9, 0)
        Me.gbx.Controls.SetChildIndex(Me.chkDisabled, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label10, 0)
        Me.gbx.Controls.SetChildIndex(Me.chkSSO, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtSSOUsername, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label11, 0)
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnReset)
        Me.Panel2.Location = New System.Drawing.Point(3, 193)
        Me.Panel2.Size = New System.Drawing.Size(656, 28)
        Me.Panel2.TabIndex = 22
        Me.Panel2.Controls.SetChildIndex(Me.btnCancel, 0)
        Me.Panel2.Controls.SetChildIndex(Me.btnReset, 0)
        Me.Panel2.Controls.SetChildIndex(Me.btnOK, 0)
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(471, 0)
        Me.btnOK.TabIndex = 1
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(562, 0)
        Me.btnCancel.TabIndex = 2
        '
        'Grid
        '
        Me.Grid.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Grid.Appearance.Options.UseFont = True
        Me.Grid.ShowAutoFilterRow = True
        Me.Grid.ShowFindPanel = True
        Me.Grid.Size = New System.Drawing.Size(815, 561)
        Me.Grid.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(0, 576)
        Me.Panel1.Size = New System.Drawing.Size(834, 35)
        Me.Panel1.TabIndex = 0
        '
        'btnDelete
        '
        Me.btnDelete.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Appearance.Options.UseFont = True
        '
        'btnEdit
        '
        Me.btnEdit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.Appearance.Options.UseFont = True
        '
        'btnAdd
        '
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.Appearance.Options.UseFont = True
        '
        'btnClose
        '
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(2205, 5)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'txtUserName
        '
        Me.txtUserName.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtUserName.EnterMoveNextControl = True
        Me.txtUserName.Location = New System.Drawing.Point(113, 78)
        Me.txtUserName.MaxLength = 10
        Me.txtUserName.Name = "txtUserName"
        Me.txtUserName.NumericAllowNegatives = False
        Me.txtUserName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtUserName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtUserName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtUserName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUserName.Properties.Appearance.Options.UseFont = True
        Me.txtUserName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtUserName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtUserName.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtUserName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtUserName.Properties.MaxLength = 10
        Me.txtUserName.Size = New System.Drawing.Size(201, 22)
        Me.txtUserName.TabIndex = 5
        Me.txtUserName.Tag = "AE"
        Me.txtUserName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtUserName.ToolTipText = ""
        '
        'txtSurname
        '
        Me.txtSurname.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtSurname.EnterMoveNextControl = True
        Me.txtSurname.Location = New System.Drawing.Point(113, 50)
        Me.txtSurname.MaxLength = 30
        Me.txtSurname.Name = "txtSurname"
        Me.txtSurname.NumericAllowNegatives = False
        Me.txtSurname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSurname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSurname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSurname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSurname.Properties.Appearance.Options.UseFont = True
        Me.txtSurname.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSurname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSurname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSurname.Properties.MaxLength = 30
        Me.txtSurname.Size = New System.Drawing.Size(201, 22)
        Me.txtSurname.TabIndex = 3
        Me.txtSurname.Tag = "AE"
        Me.txtSurname.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSurname.ToolTipText = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 53)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 15)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Surname"
        '
        'txtForename
        '
        Me.txtForename.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtForename.EnterMoveNextControl = True
        Me.txtForename.Location = New System.Drawing.Point(113, 22)
        Me.txtForename.MaxLength = 30
        Me.txtForename.Name = "txtForename"
        Me.txtForename.NumericAllowNegatives = False
        Me.txtForename.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtForename.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtForename.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtForename.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtForename.Properties.Appearance.Options.UseFont = True
        Me.txtForename.Properties.Appearance.Options.UseTextOptions = True
        Me.txtForename.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtForename.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtForename.Properties.MaxLength = 30
        Me.txtForename.Size = New System.Drawing.Size(201, 22)
        Me.txtForename.TabIndex = 1
        Me.txtForename.Tag = "AE"
        Me.txtForename.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtForename.ToolTipText = ""
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(6, 25)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 15)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Forename"
        '
        'txtDepartment
        '
        Me.txtDepartment.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtDepartment.EnterMoveNextControl = True
        Me.txtDepartment.Location = New System.Drawing.Point(430, 22)
        Me.txtDepartment.MaxLength = 30
        Me.txtDepartment.Name = "txtDepartment"
        Me.txtDepartment.NumericAllowNegatives = False
        Me.txtDepartment.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtDepartment.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDepartment.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtDepartment.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDepartment.Properties.Appearance.Options.UseFont = True
        Me.txtDepartment.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDepartment.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtDepartment.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDepartment.Properties.MaxLength = 30
        Me.txtDepartment.Size = New System.Drawing.Size(220, 22)
        Me.txtDepartment.TabIndex = 9
        Me.txtDepartment.Tag = "AE"
        Me.txtDepartment.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDepartment.ToolTipText = ""
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(342, 25)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(70, 15)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Department"
        '
        'txtJobTitle
        '
        Me.txtJobTitle.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtJobTitle.EnterMoveNextControl = True
        Me.txtJobTitle.Location = New System.Drawing.Point(430, 50)
        Me.txtJobTitle.MaxLength = 30
        Me.txtJobTitle.Name = "txtJobTitle"
        Me.txtJobTitle.NumericAllowNegatives = False
        Me.txtJobTitle.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtJobTitle.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtJobTitle.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtJobTitle.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobTitle.Properties.Appearance.Options.UseFont = True
        Me.txtJobTitle.Properties.Appearance.Options.UseTextOptions = True
        Me.txtJobTitle.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtJobTitle.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtJobTitle.Properties.MaxLength = 30
        Me.txtJobTitle.Size = New System.Drawing.Size(220, 22)
        Me.txtJobTitle.TabIndex = 11
        Me.txtJobTitle.Tag = "AE"
        Me.txtJobTitle.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtJobTitle.ToolTipText = ""
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(342, 53)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(51, 15)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Job Title"
        '
        'txtEmail
        '
        Me.txtEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtEmail.EnterMoveNextControl = True
        Me.txtEmail.Location = New System.Drawing.Point(430, 106)
        Me.txtEmail.MaxLength = 250
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.NumericAllowNegatives = False
        Me.txtEmail.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtEmail.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtEmail.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtEmail.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail.Properties.Appearance.Options.UseFont = True
        Me.txtEmail.Properties.Appearance.Options.UseTextOptions = True
        Me.txtEmail.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtEmail.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtEmail.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtEmail.Properties.MaxLength = 250
        Me.txtEmail.Size = New System.Drawing.Size(220, 22)
        Me.txtEmail.TabIndex = 15
        Me.txtEmail.Tag = "AE"
        Me.txtEmail.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtEmail.ToolTipText = ""
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(342, 109)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(36, 15)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Email"
        '
        'txtTel
        '
        Me.txtTel.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtTel.EnterMoveNextControl = True
        Me.txtTel.Location = New System.Drawing.Point(430, 78)
        Me.txtTel.MaxLength = 30
        Me.txtTel.Name = "txtTel"
        Me.txtTel.NumericAllowNegatives = False
        Me.txtTel.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtTel.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTel.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtTel.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTel.Properties.Appearance.Options.UseFont = True
        Me.txtTel.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTel.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtTel.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTel.Properties.MaxLength = 30
        Me.txtTel.Size = New System.Drawing.Size(220, 22)
        Me.txtTel.TabIndex = 13
        Me.txtTel.Tag = "AE"
        Me.txtTel.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTel.ToolTipText = ""
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(342, 81)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(62, 15)
        Me.Label8.TabIndex = 12
        Me.Label8.Text = "Telephone"
        '
        'btnReset
        '
        Me.btnReset.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.Appearance.Options.UseFont = True
        Me.btnReset.Location = New System.Drawing.Point(6, 0)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(110, 25)
        Me.btnReset.TabIndex = 0
        Me.btnReset.Text = "Reset Password"
        Me.btnReset.ToolTip = "Resetting an account will allow the user to login without the use of a password."
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 81)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 15)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Username"
        '
        'cbxType
        '
        Me.cbxType.AllowBlank = False
        Me.cbxType.DataSource = Nothing
        Me.cbxType.DisplayMember = Nothing
        Me.cbxType.EnterMoveNextControl = True
        Me.cbxType.Location = New System.Drawing.Point(113, 106)
        Me.cbxType.Name = "cbxType"
        Me.cbxType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxType.Properties.Appearance.Options.UseFont = True
        Me.cbxType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxType.SelectedValue = Nothing
        Me.cbxType.Size = New System.Drawing.Size(201, 22)
        Me.cbxType.TabIndex = 7
        Me.cbxType.Tag = "AE"
        Me.cbxType.ValueMember = Nothing
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 109)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(32, 15)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Type"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(6, 137)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(100, 15)
        Me.Label9.TabIndex = 16
        Me.Label9.Text = "Account Disabled"
        '
        'chkDisabled
        '
        Me.chkDisabled.EnterMoveNextControl = True
        Me.chkDisabled.Location = New System.Drawing.Point(112, 134)
        Me.chkDisabled.Name = "chkDisabled"
        Me.chkDisabled.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkDisabled.Properties.Appearance.Options.UseFont = True
        Me.chkDisabled.Properties.Caption = ""
        Me.chkDisabled.Size = New System.Drawing.Size(24, 19)
        Me.chkDisabled.TabIndex = 17
        Me.chkDisabled.Tag = "AE"
        Me.chkDisabled.ToolTip = "Disabling an account will prevent the user from logging in."
        '
        'chkSSO
        '
        Me.chkSSO.EnterMoveNextControl = True
        Me.chkSSO.Location = New System.Drawing.Point(152, 162)
        Me.chkSSO.Name = "chkSSO"
        Me.chkSSO.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkSSO.Properties.Appearance.Options.UseFont = True
        Me.chkSSO.Properties.Caption = ""
        Me.chkSSO.Size = New System.Drawing.Size(24, 19)
        Me.chkSSO.TabIndex = 19
        Me.chkSSO.Tag = "AE"
        Me.chkSSO.ToolTip = "Disabling an account will prevent the user from logging in."
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(6, 164)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(140, 15)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "Use Windows Credentials"
        '
        'txtSSOUsername
        '
        Me.txtSSOUsername.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtSSOUsername.EnterMoveNextControl = True
        Me.txtSSOUsername.Location = New System.Drawing.Point(296, 159)
        Me.txtSSOUsername.MaxLength = 100
        Me.txtSSOUsername.Name = "txtSSOUsername"
        Me.txtSSOUsername.NumericAllowNegatives = False
        Me.txtSSOUsername.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSSOUsername.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSSOUsername.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSSOUsername.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSSOUsername.Properties.Appearance.Options.UseFont = True
        Me.txtSSOUsername.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSSOUsername.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSSOUsername.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSSOUsername.Properties.MaxLength = 100
        Me.txtSSOUsername.Size = New System.Drawing.Size(354, 22)
        Me.txtSSOUsername.TabIndex = 21
        Me.txtSSOUsername.Tag = "AE"
        Me.txtSSOUsername.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSSOUsername.ToolTipText = ""
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(182, 164)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(109, 15)
        Me.Label11.TabIndex = 20
        Me.Label11.Text = "Domain\UserName"
        '
        'frmUsers
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(834, 611)
        Me.MinimumSize = New System.Drawing.Size(440, 441)
        Me.Name = "frmUsers"
        Me.gbx.ResumeLayout(False)
        Me.gbx.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.txtUserName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtForename.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDepartment.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJobTitle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkDisabled.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSSO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSSOUsername.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtEmail As Care.Controls.CareTextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtTel As Care.Controls.CareTextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtDepartment As Care.Controls.CareTextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtJobTitle As Care.Controls.CareTextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtSurname As Care.Controls.CareTextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtForename As Care.Controls.CareTextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtUserName As Care.Controls.CareTextBox
    Friend WithEvents btnReset As Care.Controls.CareButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbxType As Care.Controls.CareComboBox
    Friend WithEvents chkDisabled As Care.Controls.CareCheckBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtSSOUsername As Care.Controls.CareTextBox
    Friend WithEvents chkSSO As Care.Controls.CareCheckBox
    Friend WithEvents Label10 As System.Windows.Forms.Label

End Class
