﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGenericFind
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tlpLayout = New System.Windows.Forms.TableLayoutPanel()
        Me.grpRadio = New Care.Controls.CareFrame()
        Me.txtCriteria = New Care.Controls.CareTextBox()
        Me.tlpRadio = New System.Windows.Forms.TableLayoutPanel()
        Me.rad2 = New Care.Controls.CareRadioButton()
        Me.rad4 = New Care.Controls.CareRadioButton()
        Me.rad3 = New Care.Controls.CareRadioButton()
        Me.rad1 = New Care.Controls.CareRadioButton()
        Me.CareLabel1 = New Care.Controls.CareLabel()
        Me.gex = New Care.Controls.CareGrid()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnClose = New Care.Controls.CareButton()
        Me.btnSelect = New Care.Controls.CareButton()
        Me.tlpLayout.SuspendLayout()
        CType(Me.grpRadio, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpRadio.SuspendLayout()
        CType(Me.txtCriteria.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tlpRadio.SuspendLayout()
        CType(Me.rad2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rad4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rad3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rad1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'tlpLayout
        '
        Me.tlpLayout.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tlpLayout.ColumnCount = 1
        Me.tlpLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpLayout.Controls.Add(Me.grpRadio, 0, 0)
        Me.tlpLayout.Controls.Add(Me.gex, 0, 1)
        Me.tlpLayout.Location = New System.Drawing.Point(12, 12)
        Me.tlpLayout.Name = "tlpLayout"
        Me.tlpLayout.RowCount = 2
        Me.tlpLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tlpLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tlpLayout.Size = New System.Drawing.Size(500, 384)
        Me.tlpLayout.TabIndex = 0
        '
        'grpRadio
        '
        Me.grpRadio.Controls.Add(Me.txtCriteria)
        Me.grpRadio.Controls.Add(Me.tlpRadio)
        Me.grpRadio.Controls.Add(Me.CareLabel1)
        Me.grpRadio.Dock = System.Windows.Forms.DockStyle.Top
        Me.grpRadio.Location = New System.Drawing.Point(3, 3)
        Me.grpRadio.Margin = New System.Windows.Forms.Padding(3, 3, 3, 6)
        Me.grpRadio.Name = "grpRadio"
        Me.grpRadio.ShowCaption = False
        Me.grpRadio.Size = New System.Drawing.Size(494, 68)
        Me.grpRadio.TabIndex = 0
        Me.grpRadio.Visible = False
        '
        'txtCriteria
        '
        Me.txtCriteria.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCriteria.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCriteria.EnterMoveNextControl = True
        Me.txtCriteria.Location = New System.Drawing.Point(89, 39)
        Me.txtCriteria.MaxLength = 40
        Me.txtCriteria.Name = "txtCriteria"
        Me.txtCriteria.NumericAllowNegatives = False
        Me.txtCriteria.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtCriteria.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCriteria.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtCriteria.Properties.Appearance.BackColor = System.Drawing.Color.PaleGreen
        Me.txtCriteria.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtCriteria.Properties.Appearance.Options.UseBackColor = True
        Me.txtCriteria.Properties.Appearance.Options.UseFont = True
        Me.txtCriteria.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCriteria.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtCriteria.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCriteria.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtCriteria.Properties.MaxLength = 40
        Me.txtCriteria.Size = New System.Drawing.Size(397, 20)
        Me.txtCriteria.TabIndex = 2
        Me.txtCriteria.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCriteria.ToolTipText = ""
        '
        'tlpRadio
        '
        Me.tlpRadio.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tlpRadio.ColumnCount = 4
        Me.tlpRadio.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.tlpRadio.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.tlpRadio.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.tlpRadio.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.tlpRadio.Controls.Add(Me.rad2, 1, 0)
        Me.tlpRadio.Controls.Add(Me.rad4, 3, 0)
        Me.tlpRadio.Controls.Add(Me.rad3, 2, 0)
        Me.tlpRadio.Controls.Add(Me.rad1, 0, 0)
        Me.tlpRadio.Location = New System.Drawing.Point(7, 7)
        Me.tlpRadio.Name = "tlpRadio"
        Me.tlpRadio.RowCount = 1
        Me.tlpRadio.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpRadio.Size = New System.Drawing.Size(482, 26)
        Me.tlpRadio.TabIndex = 0
        '
        'rad2
        '
        Me.rad2.CausesValidation = False
        Me.rad2.Location = New System.Drawing.Point(123, 3)
        Me.rad2.Name = "rad2"
        Me.rad2.Properties.Appearance.Options.UseTextOptions = True
        Me.rad2.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.rad2.Properties.AutoWidth = True
        Me.rad2.Properties.Caption = ""
        Me.rad2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.rad2.Properties.RadioGroupIndex = 0
        Me.rad2.Size = New System.Drawing.Size(19, 19)
        Me.rad2.TabIndex = 1
        Me.rad2.TabStop = False
        '
        'rad4
        '
        Me.rad4.CausesValidation = False
        Me.rad4.Location = New System.Drawing.Point(363, 3)
        Me.rad4.Name = "rad4"
        Me.rad4.Properties.Appearance.Options.UseTextOptions = True
        Me.rad4.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.rad4.Properties.AutoWidth = True
        Me.rad4.Properties.Caption = ""
        Me.rad4.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.rad4.Properties.RadioGroupIndex = 0
        Me.rad4.Size = New System.Drawing.Size(19, 19)
        Me.rad4.TabIndex = 3
        Me.rad4.TabStop = False
        '
        'rad3
        '
        Me.rad3.CausesValidation = False
        Me.rad3.Location = New System.Drawing.Point(243, 3)
        Me.rad3.Name = "rad3"
        Me.rad3.Properties.Appearance.Options.UseTextOptions = True
        Me.rad3.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.rad3.Properties.AutoWidth = True
        Me.rad3.Properties.Caption = ""
        Me.rad3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.rad3.Properties.RadioGroupIndex = 0
        Me.rad3.Size = New System.Drawing.Size(19, 19)
        Me.rad3.TabIndex = 2
        Me.rad3.TabStop = False
        '
        'rad1
        '
        Me.rad1.CausesValidation = False
        Me.rad1.EditValue = True
        Me.rad1.Location = New System.Drawing.Point(3, 3)
        Me.rad1.Name = "rad1"
        Me.rad1.Properties.Appearance.Options.UseTextOptions = True
        Me.rad1.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.rad1.Properties.AutoWidth = True
        Me.rad1.Properties.Caption = ""
        Me.rad1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.rad1.Properties.RadioGroupIndex = 0
        Me.rad1.Size = New System.Drawing.Size(19, 19)
        Me.rad1.TabIndex = 0
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.Options.UseFont = True
        Me.CareLabel1.Appearance.Options.UseTextOptions = True
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(7, 42)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(76, 15)
        Me.CareLabel1.TabIndex = 1
        Me.CareLabel1.Text = "Search Criteria"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gex
        '
        Me.gex.AllowBuildColumns = True
        Me.gex.AllowEdit = False
        Me.gex.AllowHorizontalScroll = False
        Me.gex.AllowMultiSelect = False
        Me.gex.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gex.Appearance.Options.UseFont = True
        Me.gex.AutoSizeByData = True
        Me.gex.DisableAutoSize = False
        Me.gex.DisableDataFormatting = False
        Me.gex.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gex.FocusedRowHandle = -2147483648
        Me.gex.HideFirstColumn = False
        Me.gex.Location = New System.Drawing.Point(3, 80)
        Me.gex.Name = "gex"
        Me.gex.PreviewColumn = ""
        Me.gex.QueryID = Nothing
        Me.gex.RowAutoHeight = False
        Me.gex.SearchAsYouType = True
        Me.gex.ShowAutoFilterRow = False
        Me.gex.ShowFindPanel = True
        Me.gex.ShowGroupByBox = True
        Me.gex.ShowLoadingPanel = False
        Me.gex.ShowNavigator = False
        Me.gex.Size = New System.Drawing.Size(494, 301)
        Me.gex.TabIndex = 1
        Me.gex.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnClose)
        Me.Panel1.Controls.Add(Me.btnSelect)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 402)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(524, 35)
        Me.Panel1.TabIndex = 1
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Location = New System.Drawing.Point(437, 3)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "Close"
        '
        'btnSelect
        '
        Me.btnSelect.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSelect.Location = New System.Drawing.Point(356, 3)
        Me.btnSelect.Name = "btnSelect"
        Me.btnSelect.Size = New System.Drawing.Size(75, 23)
        Me.btnSelect.TabIndex = 0
        Me.btnSelect.Text = "Select"
        '
        'frmGenericFind
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(524, 437)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.tlpLayout)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(540, 475)
        Me.Name = "frmGenericFind"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.tlpLayout.ResumeLayout(False)
        CType(Me.grpRadio, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpRadio.ResumeLayout(False)
        Me.grpRadio.PerformLayout()
        CType(Me.txtCriteria.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tlpRadio.ResumeLayout(False)
        Me.tlpRadio.PerformLayout()
        CType(Me.rad2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rad4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rad3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rad1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CareButton2 As Care.Controls.CareButton
    Friend WithEvents CareButton1 As Care.Controls.CareButton
    Friend WithEvents tlpLayout As TableLayoutPanel
    Friend WithEvents gex As Care.Controls.CareGrid
    Friend WithEvents txtCriteria As Care.Controls.CareTextBox
    Friend WithEvents tlpRadio As TableLayoutPanel
    Friend WithEvents rad2 As Care.Controls.CareRadioButton
    Friend WithEvents rad4 As Care.Controls.CareRadioButton
    Friend WithEvents rad3 As Care.Controls.CareRadioButton
    Friend WithEvents rad1 As Care.Controls.CareRadioButton
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents btnSelect As Care.Controls.CareButton
    Friend WithEvents grpRadio As Controls.CareFrame
End Class
