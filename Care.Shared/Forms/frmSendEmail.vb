﻿Imports Care.Global
Imports System.Windows.Forms
Imports System.Reflection

Public Class frmSendEmail

    Private m_CloseOnSend As Boolean = False
    Private m_AttachmentPath As String = ""

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub New(ByVal EmailTo As String, ByVal CloseOnSend As Boolean)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        txtTo.Text = EmailTo
        m_CloseOnSend = CloseOnSend

    End Sub

    Private Sub frmSendEmail_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        If m_AttachmentPath = "" Then
            lblAttachmentPath.Text = ""
        Else
            lblAttachmentPath.Visible = True
            lblAttachmentPath.Text = IO.Path.GetFileName(m_AttachmentPath)
        End If
    End Sub

#Region "Properties"

    Public Property AttachmentPath As String
        Get
            Return lblAttachmentPath.Text
        End Get
        Set(value As String)
            If value = "" Then
                lblAttachmentPath.Hide()
            Else
                m_AttachmentPath = value
            End If
        End Set
    End Property

    Public Property SenderName As String
        Get
            Return txtTo.Text
        End Get
        Set(value As String)
            txtTo.Text = value
        End Set
    End Property

    Public Property SenderEmail As String
        Get
            Return txtTo.Text
        End Get
        Set(value As String)
            txtTo.Text = value
        End Set
    End Property

    Public Property Recipient As String
        Get
            Return txtTo.Text
        End Get
        Set(value As String)
            txtTo.Text = value
        End Set
    End Property

    Public Property Subject As String
        Get
            Return txtSubject.Text
        End Get
        Set(value As String)
            txtSubject.Text = value
        End Set
    End Property

    Public Property Body As String
        Get
            Return txtBody.Text
        End Get
        Set(value As String)
            txtBody.Text = value
        End Set
    End Property

    Public Property CloseOnSend As Boolean
        Get
            Return m_CloseOnSend
        End Get
        Set(value As Boolean)
            m_CloseOnSend = value
        End Set
    End Property

    Public Property HideTestButton As Boolean
        Get
            Return Not btnTest.Visible
        End Get
        Set(value As Boolean)
            btnTest.Visible = Not value
        End Set
    End Property

#End Region

    Private Sub btnSend_Click(sender As System.Object, e As System.EventArgs) Handles btnSend.Click
        SendEmail()
        If m_CloseOnSend Then Me.DialogResult = DialogResult.OK
    End Sub

    Private Sub SendEmail()

        If txtTo.Text = "" Then Exit Sub
        If txtSubject.Text = "" Then Exit Sub
        If txtBody.Text = "" Then Exit Sub

        txtStatus.Text = "Sending..."
        txtStatus.ForeColor = Drawing.Color.Black

        SetCMDs(False)
        Application.DoEvents()

        Dim _e As New EmailMessage
        With _e

            .Recipient = txtTo.Text
            .Subject = txtSubject.Text
            .BodyFormat = EmailMessage.EnumBodyFormat.HTMLText
            .Body = txtBody.HTMLText

            If lblAttachmentPath.Visible Then
                .AttachmentPath = m_AttachmentPath
            End If

            .Send()

        End With

        If _e.SendStatus = 0 Then
            txtStatus.Text = "Sent Successfully."
            txtStatus.ForeColor = Drawing.Color.Blue
        Else
            txtStatus.ForeColor = Drawing.Color.Red
            txtStatus.Text = _e.SendErrors
        End If

        SetCMDs(True)
        Application.DoEvents()

        _e = Nothing

        btnSend.Enabled = True

    End Sub

    Private Sub SetCMDs(ByVal Enabled As Boolean)
        btnTest.Enabled = Enabled
        btnSend.Enabled = Enabled
        btnClose.Enabled = Enabled
    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.DialogResult = DialogResult.Cancel
    End Sub

    Private Sub btnTo_Click(sender As System.Object, e As System.EventArgs)

        Dim _Find As New Care.Shared.GenericFind

        Dim _SQL As String = "select Children.fullname as 'Child', Contacts.fullname as 'Contact', Contacts.relationship as 'Relationship'," & _
                             " Contacts.email as 'Email'" & _
                             " from Children" & _
                             " left join Family on Family.id = Children.Family_id" & _
                             " left join Contacts on Family.ID = Contacts.Family_ID"

        Dim _Where As String = " and status = 'Current'" & _
                               " and len(Contacts.email) > 0"

        With _Find
            .Caption = "Find Meals"
            .ParentForm = ParentForm
            .PopulateOnLoad = True
            .GridSQL = _SQL
            .GridWhereClause = _Where
            .GridOrderBy = "order by Children.surname, Children.forename"
            .ReturnField = "Contacts.email"
            .Option1Caption = "Child"
            .Option1Field = "Children.fullname"
            .Option2Caption = "Contact"
            .Option2Field = "Contact.fullname"
            .Show()
        End With

        If _Find.ReturnValue <> "" Then
            txtTo.Text = _Find.ReturnValue
            txtSubject.Focus()
        End If

        _Find = Nothing

    End Sub

    Private Sub btnTest_Click(sender As System.Object, e As System.EventArgs) Handles btnTest.Click
        txtTo.Text = "emailtest@caresoftware.co.uk"
        txtSubject.Text = "Test Message"
        txtBody.Text = "This is a test message. Please do not reply..."
        txtStatus.Text = ""
    End Sub

    Private Sub txtStatus_DoubleClick(sender As Object, e As EventArgs) Handles txtStatus.DoubleClick
        CareMessage(txtStatus.Text, MessageBoxIcon.Exclamation, "Send Failed")
    End Sub

    Private Sub frmSendEmail_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Session.CursorDefault()
    End Sub
End Class
