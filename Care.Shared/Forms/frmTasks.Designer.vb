﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTasks
    Inherits frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim OptionsSpelling4 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling5 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling6 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling7 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling2 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling3 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.CareTab1 = New Care.Controls.CareTab()
        Me.tabDef = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl4 = New Care.Controls.CareFrame()
        Me.txtCommand = New DevExpress.XtraEditors.MemoEdit()
        Me.GroupControl3 = New Care.Controls.CareFrame()
        Me.txtArgs = New DevExpress.XtraEditors.MemoEdit()
        Me.GroupControl2 = New Care.Controls.CareFrame()
        Me.CareLabel2 = New Care.Controls.CareLabel()
        Me.CareLabel1 = New Care.Controls.CareLabel()
        Me.cbxType = New Care.Controls.CareComboBox()
        Me.txtObjectName = New Care.Controls.CareTextBox()
        Me.GroupBox1 = New Care.Controls.CareFrame()
        Me.txtTime = New Care.Controls.CareTextBox()
        Me.CareLabel4 = New Care.Controls.CareLabel()
        Me.cbxDay = New Care.Controls.CareComboBox()
        Me.txtDescription = New DevExpress.XtraEditors.MemoEdit()
        Me.CareLabel3 = New Care.Controls.CareLabel()
        Me.Label2 = New Care.Controls.CareLabel()
        Me.txtName = New Care.Controls.CareTextBox()
        Me.Label1 = New Care.Controls.CareLabel()
        Me.tabHistory = New DevExpress.XtraTab.XtraTabPage()
        Me.cgHistory = New Care.Controls.CareGrid()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.txtErrors = New DevExpress.XtraEditors.MemoEdit()
        Me.txtGreen = New DevExpress.XtraEditors.TextEdit()
        Me.txtRed = New DevExpress.XtraEditors.TextEdit()
        Me.txtYellow = New DevExpress.XtraEditors.TextEdit()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.CareTab1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CareTab1.SuspendLayout()
        Me.tabDef.SuspendLayout()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.txtCommand.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.txtArgs.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.cbxType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtObjectName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxDay.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescription.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabHistory.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtErrors.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(698, 3)
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(607, 3)
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.txtGreen)
        Me.Panel1.Controls.Add(Me.txtRed)
        Me.Panel1.Controls.Add(Me.txtYellow)
        Me.Panel1.Location = New System.Drawing.Point(0, 536)
        Me.Panel1.Size = New System.Drawing.Size(795, 36)
        Me.Panel1.TabIndex = 4
        Me.Panel1.Controls.SetChildIndex(Me.btnOK, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnCancel, 0)
        Me.Panel1.Controls.SetChildIndex(Me.txtYellow, 0)
        Me.Panel1.Controls.SetChildIndex(Me.txtRed, 0)
        Me.Panel1.Controls.SetChildIndex(Me.txtGreen, 0)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'CareTab1
        '
        Me.CareTab1.Location = New System.Drawing.Point(12, 53)
        Me.CareTab1.Name = "CareTab1"
        Me.CareTab1.SelectedTabPage = Me.tabDef
        Me.CareTab1.Size = New System.Drawing.Size(775, 483)
        Me.CareTab1.TabIndex = 5
        Me.CareTab1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabDef, Me.tabHistory})
        '
        'tabDef
        '
        Me.tabDef.Controls.Add(Me.GroupControl4)
        Me.tabDef.Controls.Add(Me.GroupControl3)
        Me.tabDef.Controls.Add(Me.GroupControl2)
        Me.tabDef.Controls.Add(Me.GroupBox1)
        Me.tabDef.Name = "tabDef"
        Me.tabDef.Size = New System.Drawing.Size(769, 455)
        Me.tabDef.Text = "Task Definition"
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.txtCommand)
        Me.GroupControl4.Location = New System.Drawing.Point(6, 249)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(756, 200)
        Me.GroupControl4.TabIndex = 7
        Me.GroupControl4.Text = "Command"
        '
        'txtCommand
        '
        Me.txtCommand.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtCommand.Location = New System.Drawing.Point(9, 27)
        Me.txtCommand.Name = "txtCommand"
        Me.txtCommand.Properties.AccessibleName = "Command Text"
        Me.txtCommand.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 11.25!)
        Me.txtCommand.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtCommand, True)
        Me.txtCommand.Size = New System.Drawing.Size(737, 164)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtCommand, OptionsSpelling4)
        Me.txtCommand.TabIndex = 0
        Me.txtCommand.Tag = "AEM"
        Me.txtCommand.UseOptimizedRendering = True
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.txtArgs)
        Me.GroupControl3.Location = New System.Drawing.Point(387, 105)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(375, 138)
        Me.GroupControl3.TabIndex = 6
        Me.GroupControl3.Text = "Arguments"
        '
        'txtArgs
        '
        Me.txtArgs.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtArgs.Location = New System.Drawing.Point(9, 27)
        Me.txtArgs.Name = "txtArgs"
        Me.txtArgs.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtArgs.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtArgs, True)
        Me.txtArgs.Size = New System.Drawing.Size(356, 102)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtArgs, OptionsSpelling5)
        Me.txtArgs.TabIndex = 0
        Me.txtArgs.Tag = "AE"
        Me.txtArgs.UseOptimizedRendering = True
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.CareLabel2)
        Me.GroupControl2.Controls.Add(Me.CareLabel1)
        Me.GroupControl2.Controls.Add(Me.cbxType)
        Me.GroupControl2.Controls.Add(Me.txtObjectName)
        Me.GroupControl2.Location = New System.Drawing.Point(387, 7)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(375, 92)
        Me.GroupControl2.TabIndex = 5
        Me.GroupControl2.Text = "Object Name"
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(9, 62)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(70, 15)
        Me.CareLabel2.TabIndex = 2
        Me.CareLabel2.Text = "Object Name"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(9, 34)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(53, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Task Type"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxType
        '
        Me.cbxType.AllowBlank = False
        Me.cbxType.DataSource = Nothing
        Me.cbxType.DisplayMember = Nothing
        Me.cbxType.EnterMoveNextControl = True
        Me.cbxType.Location = New System.Drawing.Point(110, 31)
        Me.cbxType.Name = "cbxType"
        Me.cbxType.Properties.AccessibleName = "Task Type"
        Me.cbxType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxType.Properties.Appearance.Options.UseFont = True
        Me.cbxType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxType.ReadOnly = False
        Me.cbxType.SelectedValue = Nothing
        Me.cbxType.Size = New System.Drawing.Size(251, 22)
        Me.cbxType.TabIndex = 1
        Me.cbxType.Tag = "AEM"
        Me.cbxType.ValueMember = Nothing
        '
        'txtObjectName
        '
        Me.txtObjectName.CharacterCasing = CharacterCasing.Normal
        Me.txtObjectName.EnterMoveNextControl = True
        Me.txtObjectName.Location = New System.Drawing.Point(110, 59)
        Me.txtObjectName.MaxLength = 40
        Me.txtObjectName.Name = "txtObjectName"
        Me.txtObjectName.NumericAllowNegatives = False
        Me.txtObjectName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtObjectName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtObjectName.Properties.AccessibleName = ""
        Me.txtObjectName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtObjectName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtObjectName.Properties.Appearance.Options.UseFont = True
        Me.txtObjectName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtObjectName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtObjectName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtObjectName.Properties.MaxLength = 40
        Me.txtObjectName.ReadOnly = False
        Me.txtObjectName.Size = New System.Drawing.Size(251, 22)
        Me.txtObjectName.TabIndex = 3
        Me.txtObjectName.Tag = "AEB"
        Me.txtObjectName.TextAlign = HorizontalAlignment.Left
        Me.txtObjectName.ToolTipText = ""
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtTime)
        Me.GroupBox1.Controls.Add(Me.CareLabel4)
        Me.GroupBox1.Controls.Add(Me.cbxDay)
        Me.GroupBox1.Controls.Add(Me.txtDescription)
        Me.GroupBox1.Controls.Add(Me.CareLabel3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtName)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 7)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(375, 236)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.Text = "Task Details"
        '
        'txtTime
        '
        Me.txtTime.CharacterCasing = CharacterCasing.Normal
        Me.txtTime.EnterMoveNextControl = True
        Me.txtTime.Location = New System.Drawing.Point(111, 206)
        Me.txtTime.MaxLength = 5
        Me.txtTime.Name = "txtTime"
        Me.txtTime.NumericAllowNegatives = False
        Me.txtTime.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.Time
        Me.txtTime.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTime.Properties.AccessibleName = "Execution Time"
        Me.txtTime.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtTime.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtTime.Properties.Appearance.Options.UseFont = True
        Me.txtTime.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTime.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtTime.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic
        Me.txtTime.Properties.Mask.EditMask = "(0?\d|1\d|2[0-3])\:[0-5]\d"
        Me.txtTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTime.Properties.MaxLength = 5
        Me.txtTime.ReadOnly = False
        Me.txtTime.Size = New System.Drawing.Size(60, 22)
        Me.txtTime.TabIndex = 7
        Me.txtTime.Tag = "AEBM"
        Me.txtTime.TextAlign = HorizontalAlignment.Left
        Me.txtTime.ToolTipText = ""
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(9, 209)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(81, 15)
        Me.CareLabel4.TabIndex = 6
        Me.CareLabel4.Text = "Execution Time"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxDay
        '
        Me.cbxDay.AllowBlank = False
        Me.cbxDay.DataSource = Nothing
        Me.cbxDay.DisplayMember = Nothing
        Me.cbxDay.EnterMoveNextControl = True
        Me.cbxDay.Location = New System.Drawing.Point(111, 178)
        Me.cbxDay.Name = "cbxDay"
        Me.cbxDay.Properties.AccessibleName = "Execution Day"
        Me.cbxDay.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxDay.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxDay.Properties.Appearance.Options.UseFont = True
        Me.cbxDay.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxDay.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxDay.ReadOnly = False
        Me.cbxDay.SelectedValue = Nothing
        Me.cbxDay.Size = New System.Drawing.Size(251, 22)
        Me.cbxDay.TabIndex = 5
        Me.cbxDay.Tag = "AEM"
        Me.cbxDay.ValueMember = Nothing
        '
        'txtDescription
        '
        Me.txtDescription.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtDescription.Location = New System.Drawing.Point(111, 59)
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.Properties.Appearance.Options.UseFont = True
        Me.txtDescription.Properties.MaxLength = 1024
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtDescription, True)
        Me.txtDescription.Size = New System.Drawing.Size(251, 113)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtDescription, OptionsSpelling6)
        Me.txtDescription.TabIndex = 3
        Me.txtDescription.Tag = "AE"
        Me.txtDescription.UseOptimizedRendering = True
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(9, 181)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(74, 15)
        Me.CareLabel3.TabIndex = 4
        Me.CareLabel3.Text = "Execution Day"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label2.Location = New System.Drawing.Point(9, 62)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 15)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Description"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.CharacterCasing = CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(111, 31)
        Me.txtName.MaxLength = 40
        Me.txtName.Name = "txtName"
        Me.txtName.NumericAllowNegatives = False
        Me.txtName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AccessibleName = "Task Name"
        Me.txtName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtName.Properties.MaxLength = 40
        Me.txtName.ReadOnly = False
        Me.txtName.Size = New System.Drawing.Size(251, 22)
        Me.txtName.TabIndex = 1
        Me.txtName.Tag = "AEBM"
        Me.txtName.TextAlign = HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'Label1
        '
        Me.Label1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label1.Location = New System.Drawing.Point(9, 34)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Task Name"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabHistory
        '
        Me.tabHistory.Controls.Add(Me.cgHistory)
        Me.tabHistory.Controls.Add(Me.GroupControl1)
        Me.tabHistory.Name = "tabHistory"
        Me.tabHistory.Size = New System.Drawing.Size(769, 455)
        Me.tabHistory.Text = "History"
        '
        'cgHistory
        '
        Me.cgHistory.AllowBuildColumns = True
        Me.cgHistory.AllowEdit = False
        Me.cgHistory.AllowHorizontalScroll = False
        Me.cgHistory.AllowMultiSelect = False
        Me.cgHistory.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgHistory.Appearance.Options.UseFont = True
        Me.cgHistory.AutoSizeByData = True
        Me.cgHistory.DisableAutoSize = False
        Me.cgHistory.DisableDataFormatting = False
        Me.cgHistory.FocusedRowHandle = -2147483648
        Me.cgHistory.HideFirstColumn = False
        Me.cgHistory.Location = New System.Drawing.Point(8, 7)
        Me.cgHistory.Name = "cgHistory"
        Me.cgHistory.QueryID = Nothing
        Me.cgHistory.RowAutoHeight = False
        Me.cgHistory.SearchAsYouType = True
        Me.cgHistory.ShowAutoFilterRow = False
        Me.cgHistory.ShowFindPanel = False
        Me.cgHistory.ShowGroupByBox = False
        Me.cgHistory.ShowLoadingPanel = False
        Me.cgHistory.ShowNavigator = True
        Me.cgHistory.Size = New System.Drawing.Size(318, 441)
        Me.cgHistory.TabIndex = 9
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.txtErrors)
        Me.GroupControl1.Location = New System.Drawing.Point(332, 7)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(430, 441)
        Me.GroupControl1.TabIndex = 8
        Me.GroupControl1.Text = "Errors"
        '
        'txtErrors
        '
        Me.txtErrors.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtErrors.Location = New System.Drawing.Point(9, 27)
        Me.txtErrors.Name = "txtErrors"
        Me.txtErrors.Properties.AccessibleName = "Command Text"
        Me.txtErrors.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 11.25!)
        Me.txtErrors.Properties.Appearance.Options.UseFont = True
        Me.txtErrors.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtErrors, True)
        Me.txtErrors.Size = New System.Drawing.Size(411, 405)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtErrors, OptionsSpelling7)
        Me.txtErrors.TabIndex = 0
        Me.txtErrors.Tag = ""
        Me.txtErrors.UseOptimizedRendering = True
        '
        'txtGreen
        '
        Me.txtGreen.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.txtGreen.Location = New System.Drawing.Point(316, 7)
        Me.txtGreen.Name = "txtGreen"
        Me.txtGreen.Properties.Appearance.BackColor = System.Drawing.Color.LightGreen
        Me.txtGreen.Properties.Appearance.Options.UseBackColor = True
        Me.txtGreen.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtGreen, True)
        Me.txtGreen.Size = New System.Drawing.Size(50, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtGreen, OptionsSpelling1)
        Me.txtGreen.TabIndex = 16
        Me.txtGreen.TabStop = False
        Me.txtGreen.Visible = False
        '
        'txtRed
        '
        Me.txtRed.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.txtRed.Location = New System.Drawing.Point(428, 7)
        Me.txtRed.Name = "txtRed"
        Me.txtRed.Properties.Appearance.BackColor = System.Drawing.Color.LightCoral
        Me.txtRed.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRed.Properties.Appearance.Options.UseBackColor = True
        Me.txtRed.Properties.Appearance.Options.UseFont = True
        Me.txtRed.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtRed, True)
        Me.txtRed.Size = New System.Drawing.Size(50, 22)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtRed, OptionsSpelling2)
        Me.txtRed.TabIndex = 17
        Me.txtRed.TabStop = False
        Me.txtRed.Visible = False
        '
        'txtYellow
        '
        Me.txtYellow.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.txtYellow.Location = New System.Drawing.Point(372, 7)
        Me.txtYellow.Name = "txtYellow"
        Me.txtYellow.Properties.Appearance.BackColor = System.Drawing.Color.Khaki
        Me.txtYellow.Properties.Appearance.Options.UseBackColor = True
        Me.txtYellow.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtYellow, True)
        Me.txtYellow.Size = New System.Drawing.Size(50, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtYellow, OptionsSpelling3)
        Me.txtYellow.TabIndex = 18
        Me.txtYellow.TabStop = False
        Me.txtYellow.Visible = False
        '
        'frmTasks
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(795, 572)
        Me.Controls.Add(Me.CareTab1)
        Me.Name = "frmTasks"
        Me.Text = "frmTasks"
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.CareTab1, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.CareTab1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CareTab1.ResumeLayout(False)
        Me.tabDef.ResumeLayout(False)
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        CType(Me.txtCommand.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.txtArgs.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.cbxType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtObjectName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.txtTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxDay.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescription.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabHistory.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.txtErrors.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CareTab1 As Care.Controls.CareTab
    Friend WithEvents tabDef As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtCommand As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtArgs As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents cbxType As Care.Controls.CareComboBox
    Friend WithEvents txtObjectName As Care.Controls.CareTextBox
    Friend WithEvents GroupBox1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtTime As Care.Controls.CareTextBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents cbxDay As Care.Controls.CareComboBox
    Friend WithEvents txtDescription As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents Label2 As Care.Controls.CareLabel
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents Label1 As Care.Controls.CareLabel
    Friend WithEvents tabHistory As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cgHistory As Care.Controls.CareGrid
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtErrors As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtGreen As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRed As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtYellow As DevExpress.XtraEditors.TextEdit
End Class
