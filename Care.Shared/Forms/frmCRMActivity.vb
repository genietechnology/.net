﻿Imports System.Windows.Forms
Imports Care.Global

Public Class frmCRMActivity

    Private m_ActivityID As Guid? = Nothing
    Private m_LinkID As Guid = Nothing
    Private m_LinkType As CRM.EnumLinkType
    Private m_ContactID As Guid? = Nothing
    Private m_ActivityType As CRM.EnumActivityType

#Region "Constructors"

    Public Sub New(ByVal LinkID As Guid, ByVal LinkType As CRM.EnumLinkType, ByVal ActivityType As CRM.EnumActivityType, _
               ByVal ContactID As Guid?, ByVal ContactName As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_LinkID = LinkID

        m_LinkType = LinkType
        txtLinkType.Text = LinkType.ToString

        m_ActivityType = ActivityType
        txtActivityType.Text = ActivityType.ToString

        m_ContactID = ContactID
        txtContact.Tag = ContactID
        txtContact.Text = ContactName

        Me.Text = "Create New Activity"

    End Sub

    Public Sub New(ByVal ActivityID As Guid)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_ActivityID = ActivityID

        Me.Text = "Amend Activity Detail"

    End Sub

#End Region

#Region "Properties"

    Public ReadOnly Property ActivityID As Guid?
        Get
            Return m_ActivityID
        End Get
    End Property

#End Region

    Private Sub frmCRMActivity_Load(sender As Object, e As EventArgs) Handles Me.Load

        cdtFollowUp.Value = Nothing
        cdtFollowUp.Enabled = False

        If m_ActivityID.HasValue Then
            DisplayRecord()
        Else
            cdtActionDate.Value = Now
            DefaultSubject()
        End If

    End Sub

    Private Sub DisplayRecord()

        Dim _Activity As Business.ActivityCRM = Business.ActivityCRM.RetreiveByID(m_ActivityID.Value)
        With _Activity

            m_LinkType = CType([Enum].Parse(GetType(CRM.EnumLinkType), ._LinkType), CRM.EnumLinkType)
            txtLinkType.Text = ._LinkType

            m_ContactID = ._ContactId
            txtContact.Tag = ._ContactId
            txtContact.Text = ._ContactName

            cdtActionDate.Value = ._ActionDate

            m_ActivityType = CType([Enum].Parse(GetType(CRM.EnumActivityType), ._ActionType), CRM.EnumActivityType)
            txtActivityType.Text = ._ActionType

            If ._Direction = "In" Then
                radIn.Checked = True
            Else
                radOut.Checked = True
            End If

            txtSubject.Text = ._Subject
            memNotes.HtmlText = ._Notes

            If ._Fup Then
                chkFollowUp.Checked = True
                cdtFollowUp.Value = ._FupDate
            Else
                chkFollowUp.Checked = False
                cdtFollowUp.Value = Nothing
            End If

        End With

        _Activity = Nothing

    End Sub

    Private Sub DefaultSubject()

        Select Case m_ActivityType

            Case CRM.EnumActivityType.Note
                txtSubject.Text = "RE: " + txtContact.Text

            Case CRM.EnumActivityType.Appointment
                txtSubject.Text = "Appointment RE: " + txtContact.Text

            Case CRM.EnumActivityType.Feedback
                txtSubject.Text = "Feedback RE: " + txtContact.Text

            Case CRM.EnumActivityType.PhoneCall, CRM.EnumActivityType.Email, CRM.EnumActivityType.SMS
                If radOut.Checked Then
                    txtSubject.Text = txtActivityType.Text + " to " + txtContact.Text
                Else
                    txtSubject.Text = txtActivityType.Text + " from " + txtContact.Text
                End If

        End Select

    End Sub

    Private Sub frmCRMActivity_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        txtSubject.Focus()
    End Sub

    Private Sub radOut_CheckedChanged(sender As Object, e As EventArgs) Handles radOut.CheckedChanged
        DefaultSubject()
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click

        Dim _Activity As New Business.ActivityCRM
        If m_ActivityID.HasValue Then _Activity = Business.ActivityCRM.RetreiveByID(m_ActivityID.Value)

        With _Activity

            If Not m_ActivityID.HasValue Then
                ._LinkId = m_LinkID
                ._LinkType = m_LinkType.ToString
                ._ContactId = m_ContactID
                ._ContactName = txtContact.Text
                ._ActionType = m_ActivityType.ToString
                ._System = False
            End If

            ._ActionDate = cdtActionDate.Value
            ._Subject = txtSubject.Text
            ._Notes = memNotes.Text

            If radIn.Checked Then
                ._Direction = "In"
            Else
                ._Direction = "Out"
            End If

            If chkFollowUp.Checked Then
                ._Fup = True
                ._FupDate = cdtFollowUp.Value
            Else
                ._Fup = False
                ._FupDate = Nothing
            End If

            .Store()

            m_ActivityID = ._ID

        End With

        _Activity = Nothing

    End Sub

    Private Sub chkFollowUp_CheckedChanged(sender As Object, e As EventArgs) Handles chkFollowUp.CheckedChanged
        If chkFollowUp.Checked Then
            cdtFollowUp.Enabled = True
            cdtFollowUp.Focus()
        Else
            cdtFollowUp.Enabled = False
            cdtFollowUp.Text = ""
        End If
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If CareMessage("Are you sure you wish to delete this Activity?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Delete") = DialogResult.Yes Then
            If InputBox("Please enter DELETE to continue.", "Delete Protection").ToUpper() = "DELETE" Then
                If m_ActivityID.HasValue Then
                    Business.ActivityCRM.DeleteRecord(m_ActivityID.Value)
                    Me.DialogResult = DialogResult.OK
                    Me.Close()
                End If
            End If
        End If
    End Sub
End Class
