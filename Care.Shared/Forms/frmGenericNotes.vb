﻿Option Strict On

Imports Care.Global
Imports Care.Data

Public Class frmGenericNotes

    Private m_Mode As String = ""
    Private m_RecordID As Guid = Nothing
    Private m_NoteID As Guid? = Nothing

    Public Sub New(ByVal RecordID As Guid)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_RecordID = RecordID

    End Sub

    Private Sub frmGenericNotes_KeyUp(sender As Object, e As KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Escape Then
            Cancel()
        End If
    End Sub

    Private Sub frmGenericNotes_Load(sender As Object, e As EventArgs) Handles Me.Load

        Session.CursorWaiting()

        Dim _SQL As String = ""
        _SQL += "select n.ID, n.subject as 'Subject', n.body, u.fullname as 'User', n.stamp as 'Stamp' from AppNotes n"
        _SQL += " left join AppUsers u on u.ID = n.user_id"
        _SQL += " where n.record_id = '" + m_RecordID.ToString + "'"
        _SQL += " order by n.stamp desc"

        cgNotes.HideFirstColumn = True
        cgNotes.Populate(Session.ConnectionString, _SQL)

        cgNotes.Columns("body").Visible = False

        SetControls(False)

        Session.CursorDefault()

    End Sub

    Private Sub cgNotes_AddClick(sender As Object, e As EventArgs) Handles cgNotes.AddClick
        m_Mode = "ADD"
        Blank()
        SetControls(True)
    End Sub

    Private Sub cgNotes_EditClick(sender As Object, e As EventArgs) Handles cgNotes.EditClick
        m_Mode = "EDIT"
        DisplayNote()
        SetControls(True)
    End Sub

    Private Sub cgNotes_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles cgNotes.FocusedRowChanged
        If e Is Nothing Then Exit Sub
        If e.FocusedRowHandle < 0 Then Exit Sub
        DisplayNote()
    End Sub

    Private Sub cgNotes_RemoveClick(sender As Object, e As EventArgs) Handles cgNotes.RemoveClick
        If m_NoteID.HasValue Then
            If CareMessage("Are you sure you want to delete this note?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Delete") = DialogResult.Yes Then

                Dim _SQL As String = "delete from AppNotes where ID = '" + m_NoteID.ToString + "'"
                DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                Blank()
                cgNotes.RePopulate()
                cgNotes.Columns("body").Visible = False

            End If
        End If
    End Sub

    Private Sub SetControls(ByVal Enabled As Boolean)

        txtSubject.Enabled = Enabled
        txtBody.Enabled = Enabled

        btnOK.Enabled = Enabled
        btnCancel.Enabled = True

        cgNotes.ButtonsEnabled = Not Enabled

        If Enabled Then txtSubject.Focus()

    End Sub

    Private Sub DisplayNote()
        m_NoteID = New Guid(cgNotes.CurrentRow("ID").ToString)
        txtSubject.Text = cgNotes.CurrentRow("subject").ToString
        txtBody.HTMLText = cgNotes.CurrentRow("body").ToString
    End Sub

    Private Sub Blank()
        m_NoteID = Nothing
        txtSubject.Text = ""
        txtBody.Text = ""
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click

        btnOK.Enabled = False
        btnCancel.Enabled = False

        Session.CursorWaiting()

        Dim _Note As New Business.AppNote

        Select Case m_Mode

            Case "ADD"
                _Note._RecordId = m_RecordID

            Case "EDIT"
                _Note = Business.AppNote.RetreiveByID(m_NoteID.Value)

        End Select

        _Note._Subject = txtSubject.Text
        _Note._Body = txtBody.HTMLText
        _Note._UserId = Session.CurrentUser.ID
        _Note._Stamp = Now

        _Note.Store()

        m_Mode = ""
        m_NoteID = Nothing

        cgNotes.RePopulate()
        cgNotes.Columns("body").Visible = False

        SetControls(False)

        Session.CursorDefault()

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Cancel()
    End Sub

    Private Sub Cancel()

        Select Case m_Mode

            Case "ADD"
                If CareMessage("Are you sure you want to cancel creating this note?", MessageBoxIcon.Question, MessageBoxButtons.YesNo, "Confirm Cancel") = DialogResult.Yes Then
                    m_Mode = ""
                    Blank()
                    SetControls(False)
                End If

            Case "EDIT"
                If CareMessage("Are you sure you want to cancel amending this note?", MessageBoxIcon.Question, MessageBoxButtons.YesNo, "Confirm Cancel") = DialogResult.Yes Then
                    m_Mode = ""
                    DisplayNote()
                    SetControls(False)
                End If

            Case Else
                Me.Close()

        End Select

    End Sub

End Class
