﻿'Imports Microsoft.Office.Interop
Imports Care.Global
Imports Care.Data
Imports System.Windows.Forms

Public Class frmDocumentManagement

    Private m_KeyID As Guid

    Public Sub New(ByVal KeyID As Guid)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        m_KeyID = KeyID

    End Sub

    Private Sub frmDocuments_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        cbxCategory.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Document Category"))
        txtTags.BackColor = Session.ChangeColour

        DisplayGrid()
        gbxEdit.Hide()

    End Sub

    Private Sub DisplayGrid()

        Dim _SQL As String = "select ID, subject, category, tags, notes," & _
                             " file_type, file_ext, file_size / 1024 as 'file_size', type, private, user_name, stamp" & _
                             " from AppDocs" & _
                             " where key_id = '" & m_KeyID.ToString & "'"

        If radAll.Checked Then
            _SQL += " and private = 0"
        Else
            _SQL += " and user_id = '" & Session.CurrentUser.ID.ToString & "'"
        End If

        _SQL += " and deleted <> 1"

        _SQL += " order by stamp desc, subject"

        gexDocs.Populate(Session.ConnectionString, _SQL)
        gexDocs.Columns("ID").Visible = False
        gexDocs.Columns("subject").Caption = "Subject"
        gexDocs.Columns("category").Caption = "Category"
        gexDocs.Columns("tags").Caption = "Tags"
        gexDocs.Columns("notes").Visible = False
        gexDocs.Columns("file_type").Caption = "File Type"
        gexDocs.Columns("file_size").Caption = "Size (KB)"
        gexDocs.Columns("file_ext").Visible = False
        gexDocs.Columns("type").Caption = "Type"
        gexDocs.Columns("private").Caption = "Private"
        gexDocs.Columns("user_name").Caption = "User"
        gexDocs.Columns("stamp").Caption = "Stamp"
        gexDocs.Columns("stamp").DisplayFormat.FormatString = "dd/MM/yyyy HH:mm:ss"

        gexDocs.PreviewColumn = "notes"

        gexDocs.AutoSizeColumns()
        SetButtons()

    End Sub

    Private Sub SetButtons()

        btnNew.Enabled = True
        btnClose.Enabled = True
        If gexDocs.RecordCount > 0 Then
            btnEdit.Enabled = True
            btnDelete.Enabled = True
            btnOpen.Enabled = True
        Else
            btnEdit.Enabled = False
            btnDelete.Enabled = False
            btnOpen.Enabled = False
        End If

    End Sub

    Private Sub OpenFile()
        DocumentHandler.ViewFile(ReturnSelectedDocumentID)
    End Sub

    Private Function ReturnSelectedDocumentID() As String
        If gexDocs.CurrentRow IsNot Nothing Then
            If gexDocs.CurrentRow.Item("ID") IsNot Nothing Then
                Return gexDocs.CurrentRow.Item("ID").ToString
            End If
        End If
        Return ""
    End Function

    Private Sub EditDocument(ByVal DocumentID As String)

        If DocumentID = "" Then Exit Sub
        Dim _Doc As Business.Document = Business.Document.RetreiveByID(New Guid(DocumentID))
        If _Doc IsNot Nothing Then

            MyControls.SetControls(ControlHandler.Mode.Add, Me.Controls)

            btnOK.Tag = DocumentID
            txtSubject.Text = _Doc._Subject
            txtType.Text = _Doc._Type
            txtFileName.Text = _Doc._FileName
            txtFileType.Text = _Doc._FileType
            txtFileSize.Text = FileHandler.ReturnFriendlyFileSize(_Doc._FileSize)
            chkPrivate.Checked = _Doc._Private
            cbxCategory.Text = _Doc._Category
            txtTags.EditValue = _Doc._Tags
            txtNotes.Text = _Doc._Notes

            _Doc = Nothing

            gbxEdit.Show()
            txtSubject.Focus()

        End If

    End Sub

    Private Sub CommitUpdate()

        Dim _Doc As Business.Document = Business.Document.RetreiveByID(New Guid(btnOK.Tag.ToString))
        If _Doc IsNot Nothing Then

            With _Doc
                ._Subject = txtSubject.Text
                ._Private = chkPrivate.Checked
                ._Category = cbxCategory.Text
                ._Tags = txtTags.EditText
                ._Notes = txtNotes.Text
            End With

            Business.Document.SaveRecord(_Doc)
            _Doc = Nothing

            DisplayGrid()
            gbxEdit.Hide()

        End If

    End Sub

#Region "Drag & Drop Handlers"

    Private Sub frmDocuments_DragDrop(ByVal sender As Object, ByVal e As DragEventArgs) Handles Me.DragDrop

        If e.Data.GetDataPresent(DataFormats.FileDrop) Then

            Dim _Files() As String = CType(e.Data.GetData(DataFormats.FileDrop), String())
            For Each _File As String In _Files

                If IO.File.Exists(_File) Then
                    Dim _fi As IO.FileInfo = New IO.FileInfo(_File)
                    DocumentHandler.InsertDocument(m_KeyID, DocumentHandler.DocumentType.User, _File, _fi.Name, False, "Files", "General")
                    _fi = Nothing
                Else

                    If IO.Directory.Exists(_File) Then
                        For Each _F As String In IO.Directory.GetFiles(_File)
                            Dim _fi As IO.FileInfo = New IO.FileInfo(_F)
                            DocumentHandler.InsertDocument(m_KeyID, DocumentHandler.DocumentType.User, _File, _fi.Name, False, "Files", "General")
                            _fi = Nothing
                        Next
                    End If

                End If

            Next

            DisplayGrid()

        Else

            If e.Data.GetDataPresent(DataFormats.UnicodeText) Then

                'Dim _drop As String = e.Data.GetData(DataFormats.UnicodeText).ToString
                'Dim _maildata As Hashtable = ParseOutlookDropData(_drop)

                'If SaveOutlookItem(_maildata.Item("Subject").ToString) Then
                '    DisplayGrid()
                'End If

                '_drop = Nothing
                '_maildata = Nothing

            End If

        End If

    End Sub

    Private Sub frmDocuments_DragEnter(ByVal sender As Object, ByVal e As DragEventArgs) Handles Me.DragEnter

        e.Effect = DragDropEffects.None

        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            e.Effect = DragDropEffects.All
        Else
            If e.Data.GetDataPresent(DataFormats.UnicodeText) Then
                e.Effect = DragDropEffects.All
            End If
        End If

    End Sub

#End Region

#Region "Outlook"

    'Private Function SaveOutlookItem(ByVal Subject As String) As Boolean

    'Dim _Return As Boolean = False
    'Dim _Mail As Outlook.MailItem = Nothing
    'Dim _Outlook As Outlook.Application

    '    Try

    ''get the active mailitem from outlook
    '        _Outlook = CType(Microsoft.VisualBasic.Interaction.GetObject("", "Outlook.Application"), Outlook.Application)
    'Dim _Explorer As Outlook.Explorer = _Outlook.ActiveExplorer
    '        _Mail = CType(_Explorer.Selection.Item(1), Outlook.MailItem)

    ''check the current active item matches the subject of the message that was dragged in
    '        If Subject = _Mail.Subject Then

    ''save the item out as a .msg
    'Dim _File As String = FileHandler.ReturnTemporaryFile(".msg")
    '            _Mail.SaveAs(_File, Outlook.OlSaveAsType.olMSG)

    ''insert the file into document management
    '            DocumentHandler.InsertDocument(m_KeyID, DocumentHandler.DocumentType.User, _File, _Mail.SenderName & " - " & _Mail.Subject, , "Emails", "General")

    '            _Return = True
    '            _Mail = Nothing

    '        End If

    '    Catch ex As System.Exception
    '        MsgBox(ex)
    '    End Try

    '    _Outlook = Nothing

    '    Return _Return

    'End Function

    'Private Function ParseOutlookDropData(ByVal DropData As String) As Hashtable

    '    Dim _infoLines As String() = DropData.Split(Microsoft.VisualBasic.Chr(10))
    '    Dim _headers As String() = _infoLines(0).Split(Microsoft.VisualBasic.Chr(9))
    '    Dim _record As String() = _infoLines(1).Split(Microsoft.VisualBasic.Chr(9))
    '    Dim _mailData As Hashtable = New Hashtable

    '    Dim i As Integer = 0
    '    While i < _headers.Length
    '        _mailData.Add(_headers(i), _record(i))
    '        System.Math.Min(System.Threading.Interlocked.Increment(i), i - 1)
    '    End While

    '    Return _mailData

    'End Function

#End Region

#Region "Controls"

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpen.Click
        OpenFile()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        DocumentHandler.DeleteDocument(ReturnSelectedDocumentID)
        DisplayGrid()
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click

        If FileDialog.ShowDialog = DialogResult.OK Then

            Dim _fi As IO.FileInfo = New IO.FileInfo(FileDialog.FileName)
            Dim _id As Guid? = DocumentHandler.InsertDocument(m_KeyID, DocumentHandler.DocumentType.User, _fi.FullName, _fi.Name, False, "Files", "General")
            If _id.HasValue Then
                EditDocument(_id.Value.ToString)
                DisplayGrid()
                _fi = Nothing
            End If

        End If

    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        EditDocument(ReturnSelectedDocumentID)
        DisplayGrid()
    End Sub

    Private Sub radAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radAll.CheckedChanged
        If radAll.Checked Then DisplayGrid()
    End Sub

    Private Sub radMy_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radMy.CheckedChanged
        If radMy.Checked Then DisplayGrid()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        gbxEdit.Hide()
        SetButtons()
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        CommitUpdate()
    End Sub

    Private Sub gexDocs_GridDoubleClick(sender As Object, e As System.EventArgs) Handles gexDocs.GridDoubleClick
        OpenFile()
    End Sub

#End Region

    Private Sub frmDocumentManagement_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        gexDocs.Focus()
    End Sub

    Private Sub txtTags_ValidateToken(sender As Object, e As DevExpress.XtraEditors.TokenEditValidateTokenEventArgs) Handles txtTags.ValidateToken
        e.IsValid = True
    End Sub
End Class