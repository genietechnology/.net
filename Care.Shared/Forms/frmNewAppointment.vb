#Region "Note"
'
'{**************************************************************************************************************}
'{  This file is automatically created when you open the Scheduler Control smart tag                            }
'{  *and click Create Customizable Appointment Dialog.                                                          }
'{  It contains a a descendant of the default appointment editing form created by visual inheritance.           }
'{  In Visual Studio Designer add an editor that is required to edit your appointment custom field.             }
'{  Modify the LoadFormData method to get data from a custom field and fill your editor with data.              }
'{  Modify the SaveFormData method to retrieve data from the editor and set the appointment custom field value. }
'{  The code that displays this form is automatically inserted                                                  }
'{  *in the EditAppointmentFormShowing event handler of the SchedulerControl.                                   }
'{                                                                                                              }
'{**************************************************************************************************************}
'
#End Region ' Note
Imports Care.Global
Imports Care.Data

Partial Public Class frmNewAppointment

    Inherits DevExpress.XtraScheduler.UI.AppointmentForm

    Private m_ID As String = ""
    Private m_LinkForm As String = ""
    Private m_LinkID As String = ""
    Private m_NewAppointmentFromLinkedForm As Boolean

#Region "Constructors"

    Public Sub New(ByVal control As DevExpress.XtraScheduler.SchedulerControl, ByVal apt As DevExpress.XtraScheduler.Appointment)

        MyBase.New(control, apt)

        If Not m_NewAppointmentFromLinkedForm Then
            m_ID = apt.CustomFields("AppointmentID").ToString
            m_LinkForm = apt.CustomFields("LinkForm").ToString
            m_LinkID = apt.CustomFields("LinkID").ToString
        End If

        InitializeComponent()

    End Sub

    Public Sub New(ByVal control As DevExpress.XtraScheduler.SchedulerControl, ByVal apt As DevExpress.XtraScheduler.Appointment, ByVal openRecurrenceForm As Boolean)

        MyBase.New(control, apt, openRecurrenceForm)

        If Not m_NewAppointmentFromLinkedForm Then
            m_ID = CleanData.ReturnString(apt.CustomFields("AppointmentID"))
            m_LinkForm = CleanData.ReturnString(apt.CustomFields("LinkForm"))
            m_LinkID = CleanData.ReturnString(apt.CustomFields("LinkID"))
        End If

        InitializeComponent()

    End Sub

    Public Sub New()
        InitializeComponent()
        m_NewAppointmentFromLinkedForm = True
    End Sub

#End Region

    Private Sub frmNewAppointment_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        If m_NewAppointmentFromLinkedForm Then
            btnRecurrence.Visible = False
            btnDelete.Visible = False
            btnDrillDown.Visible = False
        Else
            If m_LinkForm <> "" AndAlso m_LinkID <> "" Then
                btnDrillDown.Visible = True
            Else
                btnDrillDown.Visible = False
            End If
        End If

    End Sub

#Region "Properties"

    Public Property AppointmentDescription As String
        Get
            Return tbDescription.Text
        End Get
        Set(value As String)
            tbDescription.Text = value
        End Set
    End Property

    Public Property AppointmentSubject As String
        Get
            Return tbSubject.Text
        End Get
        Set(value As String)
            tbSubject.Text = value
        End Set
    End Property

    Public Property AppointmentLocation As String
        Get
            Return tbLocation.Text
        End Get
        Set(value As String)
            tbLocation.Text = value
        End Set
    End Property

    Public Property AppointmentStartDate As Date
        Get
            Return edtStartDate.DateTime
        End Get
        Set(value As Date)
            edtStartDate.DateTime = value
        End Set
    End Property

    Public Property AppointmentStartTime As Date
        Get
            Return edtStartTime.Time
        End Get
        Set(value As Date)
            edtStartTime.Time = value
        End Set
    End Property

    Public Property AppointmentEndDate As Date
        Get
            Return edtEndDate.DateTime
        End Get
        Set(value As Date)
            edtEndDate.DateTime = value
        End Set
    End Property

    Public Property AppointmentEndTime As Date
        Get
            Return edtEndTime.Time
        End Get
        Set(value As Date)
            edtEndTime.Time = value
        End Set
    End Property

    Public Property AppointmentLabel As Integer
        Get
            Return edtLabel.SelectedIndex + 1
        End Get
        Set(value As Integer)
            edtLabel.SelectedIndex = AppointmentLabel - 1
        End Set
    End Property

#End Region

#Region "Public Methods"

    Public Sub DefaultDatesAndInterval()

        edtStartDate.DateTime = Today
        edtEndDate.DateTime = Today

        edtStartTime.Time = GetCurrentTime()
        edtEndTime.Time = edtStartTime.Time.AddMinutes(15)

    End Sub

#End Region

#Region "Overrides"

    ''' <summary>
    ''' Add your code to obtain a custom field value and fill the editor with data.
    ''' </summary>
    Public Overrides Sub LoadFormData(ByVal appointment As DevExpress.XtraScheduler.Appointment)
        MyBase.LoadFormData(appointment)
    End Sub

    ''' <summary>
    ''' Add your code to retrieve a value from the editor and set the custom appointment field.
    ''' </summary>
    Public Overrides Function SaveFormData(ByVal appointment As DevExpress.XtraScheduler.Appointment) As Boolean
        Return MyBase.SaveFormData(appointment)
    End Function

    ''' <summary>
    ''' Add your code to notify that any custom field is changed. Return true if a custom field is changed, otherwise false.
    ''' </summary>
    Public Overrides Function IsAppointmentChanged(ByVal appointment As DevExpress.XtraScheduler.Appointment) As Boolean
        Return False
    End Function

    Protected Overrides Sub OnOkButton()
        If m_NewAppointmentFromLinkedForm Then
            If ValidateEntry Then
                Me.DialogResult = DialogResult.OK
                Me.Close()
            End If
        Else
            If ValidateEntry Then MyBase.OnOkButton()
        End If
    End Sub

#End Region

    Private Function GetCurrentTime() As Date
        Dim _Time As Date = New Date(Now.Year, Now.Month, Now.Day, Now.Hour, Now.Minute, 0)
        Do Until _Time.Minute Mod 15 = 0
            _Time = _Time.AddMinutes(1)
        Loop
        Return _Time
    End Function

    Private Function ValidateEntry() As Boolean

        Return True

    End Function

    Private Sub btnDrillDown_Click(sender As Object, e As EventArgs) Handles btnDrillDown.Click

        If m_LinkForm = "" Then Exit Sub

        Dim _FormToLoadSplit As String() = m_LinkForm.Split(CChar("."))
        Dim _Ass As String = _FormToLoadSplit(0)
        Dim _FormName As String = _FormToLoadSplit(1)

        Dim _Ex As Exception = Nothing
        Dim _frm As IAppointment = FormHandler.ReturnInstance(Of IAppointment)(_Ass, _FormName, _ex)
        _frm.DrillFromCalendar(New Guid(m_ID), New Guid(m_LinkID))

    End Sub

    Private Sub frmNewAppointment_Shown(sender As Object, e As EventArgs) Handles Me.Shown

    End Sub
End Class
