﻿Imports Care.Global
Imports Care.Shared
Imports Care.Data
Imports System.Windows.Forms

Public Class frmListMaintenance

    Private m_ListID As String = ""
    Private m_DrillDownListName As String = ""
    Private m_DrillDown As Boolean = False
    Private m_DrillDownChanges As Boolean = False

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub New(ByVal ListName As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_DrillDown = True
        m_DrillDownListName = ListName

    End Sub

    Private Sub frmListMaintenance_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If m_DrillDown Then
            If m_DrillDownChanges Then
                Me.DialogResult = DialogResult.OK
                ListHandler.CacheLists()
            Else
                Me.DialogResult = DialogResult.Cancel
            End If
        End If
    End Sub

    Private Sub frmListMaintenance_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        cbxListType.AddItem("System", "S")
        cbxListType.AddItem("Application", "A")

        txtListName.BackColor = Session.ChangeColour
        cbxListType.BackColor = Session.ChangeColour

        txtItemText.BackColor = Session.ChangeColour
        txtRef1.BackColor = Session.ChangeColour
        txtRef2.BackColor = Session.ChangeColour
        txtRef3.BackColor = Session.ChangeColour
        chkHide.BackColor = Session.ChangeColour
        txtItemSeq.BackColor = Session.ChangeColour

        If m_DrillDown Then

            'if the list has not been created already, we make it now
            If Not ListHandler.ListExists(m_DrillDownListName) Then
                ListHandler.CreateList(m_DrillDownListName, 1)
            End If

            grdLists.ButtonsEnabled = False

        End If

        DisplayLists()

        gbxList.Hide()
        gbxItem.Hide()

    End Sub

#Region "Lists"

    Private Sub DisplayLists()

        Dim _SQL As String = "select id, name, type from AppLists"

        If m_DrillDownListName <> "" Then
            _SQL += " where name = '" + m_DrillDownListName + "'"
        End If

        _SQL += " order by name"

        grdLists.HideFirstColumn = True
        grdLists.Populate(Session.ConnectionString, _SQL)

        If grdLists.RecordCount > 0 Then
            grdLists.Columns("name").Caption = "List Name"
            grdLists.Columns("type").Caption = "List Type"
            DisplayItems()
        End If

    End Sub

    Private Sub EditList()
        Dim _ID As String = ReturnSelectedID(grdLists)
        If _ID <> "" Then ShowList(_ID)
    End Sub

    Private Sub ShowList(ByVal ListID As String)

        If ListID = "" Then
            gbxList.Tag = Nothing
            txtListName.Text = ""
            cbxListType.SelectedValue = "A"
        Else
            gbxList.Tag = ListID
            txtListName.Text = grdLists.CurrentRow("name").ToString
            cbxListType.SelectedValue = grdLists.CurrentRow("type").ToString
        End If

        gbxList.Show()
        txtListName.Focus()

    End Sub

    Private Sub grdLists_AddClick(sender As Object, e As EventArgs) Handles grdLists.AddClick
        ShowList("")
    End Sub

    Private Sub grdLists_EditClick(sender As Object, e As EventArgs) Handles grdLists.EditClick
        EditList()
    End Sub

    Private Sub grdLists_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles grdLists.FocusedRowChanged
        If e IsNot Nothing AndAlso e.FocusedRowHandle >= 0 Then
            m_ListID = ReturnSelectedID(grdLists)
            DisplayItems()
        Else
            grdItems.Clear()
        End If
    End Sub

    Private Sub grdLists_RemoveClick(sender As Object, e As EventArgs) Handles grdLists.RemoveClick


        If Session.CurrentUser.IsSuperAdministrator Then

            m_ListID = ReturnSelectedID(grdLists)
            If m_ListID <> "" Then

                If InputBox("Type DELETE to remove this List (and items)", "Confirm Delete").ToUpper = "DELETE" Then

                    Dim _SQL As String = ""

                    _SQL = "delete from AppListItems where list_id = '" + m_ListID.ToString + "'"
                    DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                    _SQL = "delete from AppLists where ID = '" + m_ListID.ToString + "'"
                    DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                    DisplayLists()

                End If

            End If

        Else
            CareMessage("Only Super Administrators can Delete Lists", MessageBoxIcon.Exclamation, "Delete List")
        End If

    End Sub

    Private Sub btnListCancel_Click(sender As Object, e As EventArgs) Handles btnListCancel.Click
        gbxList.Hide()
    End Sub

    Private Sub btnListOK_Click(sender As Object, e As EventArgs) Handles btnListOK.Click

        If Not CheckListMandatory() Then Exit Sub

        grdLists.PersistRowPosition()

        CommitList()
        gbxList.Hide()
        DisplayLists()

        grdLists.RestoreRowPosition()

    End Sub

    Private Sub CommitList()

        Dim _List As New Business.AppList
        If gbxList.Tag Is Nothing Then
            _List._ID = Guid.NewGuid
        Else
            _List = Business.AppList.RetreiveByID(New Guid(gbxList.Tag.ToString))
        End If

        With _List
            ._Name = txtListName.Text
            ._Type = cbxListType.SelectedValue.ToString
            .Save()
        End With

        _List = Nothing

    End Sub

    Private Function CheckListMandatory() As Boolean

        If txtListName.Text = "" Then
            CareMessage("Please enter the List Name.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        Return True

    End Function

#End Region

#Region "Items"

    Private Sub DisplayItems()

        Dim _SQL As String = "select id, name, ref_1, ref_2, ref_3, hide, seq from AppListItems" & _
                             " where list_id = '" & m_ListID.ToString & "'" & _
                             " order by seq"

        grdItems.HideFirstColumn = True
        grdItems.Populate(Session.ConnectionString, _SQL)
        grdItems.RePopulate()

        If grdItems.RecordCount > 0 Then
            grdItems.Columns("name").Caption = "Item Text"
            grdItems.Columns("ref_1").Caption = "Ref 1"
            grdItems.Columns("ref_2").Caption = "Ref 2"
            grdItems.Columns("ref_3").Caption = "Ref 3"
            grdItems.Columns("hide").Caption = "Hide"
            grdItems.Columns("seq").Caption = "Seq"
        Else
            grdItems.Clear()
        End If

    End Sub

    Private Sub EditItem()
        Dim _ID As String = ReturnSelectedID(grdItems)
        If _ID <> "" Then ShowItem(_ID)
    End Sub

    Private Sub ShowItem(ByVal ItemID As String)

        If ItemID = "" Then
            ClearItem()
        Else

            gbxItem.Tag = ItemID
            txtItemText.Text = grdItems.CurrentRow("name").ToString

            txtRef1.Text = grdItems.CurrentRow("ref_1").ToString
            txtRef2.Text = grdItems.CurrentRow("ref_2").ToString
            txtRef3.Text = grdItems.CurrentRow("ref_3").ToString

            chkHide.Checked = ValueHandler.ConvertBoolean(grdItems.CurrentRow("hide"))

            txtItemSeq.Text = grdItems.CurrentRow("seq").ToString

        End If

        gbxItem.Show()
        txtItemText.Focus()

    End Sub

    Private Sub btnItemCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnItemCancel.Click
        HideItem()
    End Sub

    Private Function CheckMandatory() As Boolean

        If txtItemText.Text = "" Then
            CareMessage("Please enter the Item Text.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        Return True

    End Function

    Private Sub CommitItem()

        Dim _Item As New Business.AppListItem
        If gbxItem.Tag Is Nothing Then
            _Item._ListId = New Guid(m_ListID)
        Else
            _Item = Business.AppListItem.RetreiveByID(New Guid(gbxItem.Tag.ToString))
        End If

        With _Item

            ._Name = txtItemText.Text

            ._Ref1 = txtRef1.Text
            ._Ref2 = txtRef2.Text
            ._Ref3 = txtRef3.Text

            ._Hide = chkHide.Checked


            If txtItemSeq.Text.Trim = "" Then
                ._Seq = 0
            Else
                ._Seq = Integer.Parse(txtItemSeq.Text)
            End If

            .Store()

        End With

        _Item = Nothing

    End Sub

    Private Sub btnItemOK_Click(sender As System.Object, e As System.EventArgs) Handles btnItemOK.Click

        If Not CheckMandatory() Then Exit Sub

        grdItems.PersistRowPosition()

        CommitItem()
        HideItem()
        DisplayItems()

        m_DrillDownChanges = True

        grdItems.RestoreRowPosition()

    End Sub

    Private Sub grdItems_AddClick(sender As Object, e As EventArgs) Handles grdItems.AddClick
        ShowItem("")
    End Sub

    Private Sub grdItems_EditClick(sender As Object, e As EventArgs) Handles grdItems.EditClick
        EditItem()
    End Sub

    Private Sub grdItems_RemoveClick(sender As Object, e As EventArgs) Handles grdItems.RemoveClick

        If CareMessage("Consider using the Hide option - rather than Deleting this item. If you want to continue anyway - Click OK.", MessageBoxIcon.Exclamation, MessageBoxButtons.OKCancel, "Delete List Item") = DialogResult.OK Then

            Dim _ID As String = ReturnSelectedID(grdItems)
            If _ID <> "" Then
                If CareMessage("Are you sure you want to Delete this record?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Delete") = DialogResult.Yes Then
                    Dim _SQL As String = "delete from AppListItems where ID = '" & _ID & "'"
                    Care.Data.DAL.ExecuteSQL(Session.ConnectionString, _SQL)
                    m_DrillDownChanges = True
                    DisplayItems()
                End If
            End If

        End If

    End Sub

    Private Sub HideItem()
        gbxItem.Tag = Nothing
        gbxItem.Hide()
    End Sub

#End Region

    Private Function ReturnSelectedID(ByRef GridControl As Care.Controls.CareGridWithButtons) As String

        If GridControl.RecordCount < 1 Then Return ""
        If GridControl.CurrentRow Is Nothing Then Return ""
        If GridControl.CurrentRow("id").ToString = "" Then Return ""

        Return GridControl.CurrentRow("id").ToString

    End Function

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnItemSaveAdd_Click(sender As Object, e As EventArgs) Handles btnItemSaveAdd.Click

        If Not CheckMandatory() Then Exit Sub

        grdItems.PersistRowPosition()

        CommitItem()
        DisplayItems()
        ClearItem()

        txtItemText.Focus()

        m_DrillDownChanges = True

        grdItems.RestoreRowPosition()

    End Sub

    Private Sub ClearItem()
        gbxItem.Tag = Nothing
        txtItemText.Text = ""
        txtRef1.Text = ""
        txtRef2.Text = ""
        txtRef3.Text = ""
        chkHide.Checked = False
        txtItemSeq.Text = ""
    End Sub

End Class
