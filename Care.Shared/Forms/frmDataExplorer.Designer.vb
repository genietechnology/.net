﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDataExplorer
    Inherits frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.grpHeader = New Care.Controls.CareFrame()
        Me.btnTextBox = New Care.Controls.CareButton(Me.components)
        Me.btnCSV = New Care.Controls.CareButton(Me.components)
        Me.btnMailMerge = New Care.Controls.CareButton(Me.components)
        Me.btnEdit = New Care.Controls.CareButton(Me.components)
        Me.btnNew = New Care.Controls.CareButton(Me.components)
        Me.btnRun = New Care.Controls.CareButton(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.cbxQuery = New Care.Controls.CareComboBox(Me.components)
        Me.cgData = New Care.Controls.CareGrid()
        Me.txtOutput = New DevExpress.XtraEditors.MemoEdit()
        CType(Me.grpHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpHeader.SuspendLayout()
        CType(Me.cbxQuery.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOutput.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'grpHeader
        '
        Me.grpHeader.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.grpHeader.Controls.Add(Me.btnTextBox)
        Me.grpHeader.Controls.Add(Me.btnCSV)
        Me.grpHeader.Controls.Add(Me.btnMailMerge)
        Me.grpHeader.Controls.Add(Me.btnEdit)
        Me.grpHeader.Controls.Add(Me.btnNew)
        Me.grpHeader.Controls.Add(Me.btnRun)
        Me.grpHeader.Controls.Add(Me.CareLabel1)
        Me.grpHeader.Controls.Add(Me.cbxQuery)
        Me.grpHeader.Location = New System.Drawing.Point(12, 12)
        Me.grpHeader.Name = "grpHeader"
        Me.grpHeader.ShowCaption = False
        Me.grpHeader.Size = New System.Drawing.Size(912, 48)
        Me.grpHeader.TabIndex = 0
        '
        'btnTextBox
        '
        Me.btnTextBox.Location = New System.Drawing.Point(584, 12)
        Me.btnTextBox.Name = "btnTextBox"
        Me.btnTextBox.Size = New System.Drawing.Size(75, 23)
        Me.btnTextBox.TabIndex = 3
        Me.btnTextBox.Text = "To Textbox"
        '
        'btnCSV
        '
        Me.btnCSV.Location = New System.Drawing.Point(503, 12)
        Me.btnCSV.Name = "btnCSV"
        Me.btnCSV.Size = New System.Drawing.Size(75, 23)
        Me.btnCSV.TabIndex = 2
        Me.btnCSV.Text = "Run to CSV"
        '
        'btnMailMerge
        '
        Me.btnMailMerge.Location = New System.Drawing.Point(665, 12)
        Me.btnMailMerge.Name = "btnMailMerge"
        Me.btnMailMerge.Size = New System.Drawing.Size(75, 23)
        Me.btnMailMerge.TabIndex = 4
        Me.btnMailMerge.Text = "Mail Merge"
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnEdit.Location = New System.Drawing.Point(827, 12)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(75, 23)
        Me.btnEdit.TabIndex = 6
        Me.btnEdit.Text = "Edit Query"
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnNew.Location = New System.Drawing.Point(746, 12)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(75, 23)
        Me.btnNew.TabIndex = 5
        Me.btnNew.Text = "Create New"
        '
        'btnRun
        '
        Me.btnRun.Location = New System.Drawing.Point(422, 12)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(75, 23)
        Me.btnRun.TabIndex = 1
        Me.btnRun.Text = "Run"
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(12, 16)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(59, 15)
        Me.CareLabel1.TabIndex = 1
        Me.CareLabel1.Text = "Base Query"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxQuery
        '
        Me.cbxQuery.AllowBlank = False
        Me.cbxQuery.DataSource = Nothing
        Me.cbxQuery.DisplayMember = Nothing
        Me.cbxQuery.EnterMoveNextControl = True
        Me.cbxQuery.Location = New System.Drawing.Point(85, 13)
        Me.cbxQuery.Name = "cbxQuery"
        Me.cbxQuery.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxQuery.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxQuery.Properties.Appearance.Options.UseFont = True
        Me.cbxQuery.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxQuery.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxQuery.ReadOnly = False
        Me.cbxQuery.SelectedValue = Nothing
        Me.cbxQuery.Size = New System.Drawing.Size(331, 22)
        Me.cbxQuery.TabIndex = 0
        Me.cbxQuery.ValueMember = Nothing
        '
        'cgData
        '
        Me.cgData.AllowBuildColumns = True
        Me.cgData.AllowHorizontalScroll = False
        Me.cgData.AllowMultiSelect = False
        Me.cgData.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgData.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgData.Appearance.Options.UseFont = True
        Me.cgData.AutoSizeByData = True
        Me.cgData.HideFirstColumn = False
        Me.cgData.Location = New System.Drawing.Point(12, 66)
        Me.cgData.Name = "cgData"
        Me.cgData.QueryID = Nothing
        Me.cgData.SearchAsYouType = True
        Me.cgData.ShowFindPanel = False
        Me.cgData.ShowGroupByBox = True
        Me.cgData.ShowNavigator = True
        Me.cgData.Size = New System.Drawing.Size(912, 193)
        Me.cgData.TabIndex = 1
        '
        'txtOutput
        '
        Me.txtOutput.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtOutput.Location = New System.Drawing.Point(12, 66)
        Me.txtOutput.Name = "txtOutput"
        Me.txtOutput.Properties.Appearance.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOutput.Properties.Appearance.Options.UseFont = True
        Me.txtOutput.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtOutput, True)
        Me.txtOutput.Size = New System.Drawing.Size(912, 193)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtOutput, OptionsSpelling1)
        Me.txtOutput.TabIndex = 2
        Me.txtOutput.UseOptimizedRendering = True
        '
        'frmDataExplorer
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(936, 271)
        Me.Controls.Add(Me.txtOutput)
        Me.Controls.Add(Me.cgData)
        Me.Controls.Add(Me.grpHeader)
        Me.Name = "frmDataExplorer"
        Me.Text = "Data Explorer"
        Me.WindowState = FormWindowState.Maximized
        CType(Me.grpHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpHeader.ResumeLayout(False)
        Me.grpHeader.PerformLayout()
        CType(Me.cbxQuery.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOutput.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grpHeader As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnRun As Care.Controls.CareButton
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents cbxQuery As Care.Controls.CareComboBox
    Friend WithEvents cgData As Care.Controls.CareGrid
    Friend WithEvents btnEdit As Care.Controls.CareButton
    Friend WithEvents btnNew As Care.Controls.CareButton
    Friend WithEvents btnMailMerge As Care.Controls.CareButton
    Friend WithEvents btnTextBox As Care.Controls.CareButton
    Friend WithEvents btnCSV As Care.Controls.CareButton
    Friend WithEvents txtOutput As DevExpress.XtraEditors.MemoEdit
End Class
