﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSetup
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupBox1 = New GroupBox()
        Me.Label4 = New Label()
        Me.Label3 = New Label()
        Me.Label2 = New Label()
        Me.txtPassword = New Care.Controls.CareTextBox(Me.components)
        Me.txtUserName = New Care.Controls.CareTextBox(Me.components)
        Me.radSQL = New RadioButton()
        Me.radWindows = New RadioButton()
        Me.txtDB = New Care.Controls.CareTextBox(Me.components)
        Me.txtServer = New Care.Controls.CareTextBox(Me.components)
        Me.Label1 = New Label()
        Me.btnOK = New Care.Controls.CareButton(Me.components)
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtPassword.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUserName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtServer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtPassword)
        Me.GroupBox1.Controls.Add(Me.txtUserName)
        Me.GroupBox1.Controls.Add(Me.radSQL)
        Me.GroupBox1.Controls.Add(Me.radWindows)
        Me.GroupBox1.Controls.Add(Me.txtDB)
        Me.GroupBox1.Controls.Add(Me.txtServer)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(14, 7)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(457, 190)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 150)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(81, 15)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "SQL Password"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 117)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(84, 15)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "SQL Username"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 57)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(90, 15)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Database Name"
        '
        'txtPassword
        '
        Me.txtPassword.CharacterCasing = CharacterCasing.Normal
        Me.txtPassword.EnterMoveNextControl = True
        Me.txtPassword.Location = New System.Drawing.Point(118, 148)
        Me.txtPassword.MaxLength = 32767
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPassword.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPassword.Properties.Appearance.Options.UseFont = True
        Me.txtPassword.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPassword.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPassword.Properties.MaxLength = 32767
        Me.txtPassword.ReadOnly = False
        Me.txtPassword.Size = New System.Drawing.Size(328, 20)
        Me.txtPassword.TabIndex = 6
        Me.txtPassword.TextAlign = HorizontalAlignment.Left
        Me.txtPassword.ToolTipText = ""
        '
        'txtUserName
        '
        Me.txtUserName.CharacterCasing = CharacterCasing.Normal
        Me.txtUserName.EnterMoveNextControl = True
        Me.txtUserName.Location = New System.Drawing.Point(118, 115)
        Me.txtUserName.MaxLength = 32767
        Me.txtUserName.Name = "txtUserName"
        Me.txtUserName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtUserName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUserName.Properties.Appearance.Options.UseFont = True
        Me.txtUserName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtUserName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtUserName.Properties.MaxLength = 32767
        Me.txtUserName.ReadOnly = False
        Me.txtUserName.Size = New System.Drawing.Size(328, 20)
        Me.txtUserName.TabIndex = 5
        Me.txtUserName.TextAlign = HorizontalAlignment.Left
        Me.txtUserName.ToolTipText = ""
        '
        'radSQL
        '
        Me.radSQL.AutoSize = True
        Me.radSQL.Location = New System.Drawing.Point(309, 89)
        Me.radSQL.Name = "radSQL"
        Me.radSQL.Size = New System.Drawing.Size(128, 19)
        Me.radSQL.TabIndex = 4
        Me.radSQL.TabStop = True
        Me.radSQL.Text = "SQL Authentication"
        '
        'radWindows
        '
        Me.radWindows.AutoSize = True
        Me.radWindows.Checked = True
        Me.radWindows.Location = New System.Drawing.Point(118, 89)
        Me.radWindows.Name = "radWindows"
        Me.radWindows.Size = New System.Drawing.Size(156, 19)
        Me.radWindows.TabIndex = 3
        Me.radWindows.TabStop = True
        Me.radWindows.Text = "Windows Authentication"
        '
        'txtDB
        '
        Me.txtDB.CharacterCasing = CharacterCasing.Normal
        Me.txtDB.EnterMoveNextControl = True
        Me.txtDB.Location = New System.Drawing.Point(118, 55)
        Me.txtDB.MaxLength = 32767
        Me.txtDB.Name = "txtDB"
        Me.txtDB.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDB.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDB.Properties.Appearance.Options.UseFont = True
        Me.txtDB.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDB.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtDB.Properties.MaxLength = 32767
        Me.txtDB.ReadOnly = False
        Me.txtDB.Size = New System.Drawing.Size(328, 20)
        Me.txtDB.TabIndex = 2
        Me.txtDB.TextAlign = HorizontalAlignment.Left
        Me.txtDB.ToolTipText = ""
        '
        'txtServer
        '
        Me.txtServer.CharacterCasing = CharacterCasing.Normal
        Me.txtServer.EnterMoveNextControl = True
        Me.txtServer.Location = New System.Drawing.Point(118, 23)
        Me.txtServer.MaxLength = 32767
        Me.txtServer.Name = "txtServer"
        Me.txtServer.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtServer.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtServer.Properties.Appearance.Options.UseFont = True
        Me.txtServer.Properties.Appearance.Options.UseTextOptions = True
        Me.txtServer.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtServer.Properties.MaxLength = 32767
        Me.txtServer.ReadOnly = False
        Me.txtServer.Size = New System.Drawing.Size(328, 20)
        Me.txtServer.TabIndex = 1
        Me.txtServer.TextAlign = HorizontalAlignment.Left
        Me.txtServer.ToolTipText = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(74, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Server Name"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(295, 203)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(85, 25)
        Me.btnOK.TabIndex = 1
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(386, 203)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 25)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "Cancel"
        '
        'frmSetup
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(485, 238)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmSetup"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "Create Connection to KickStart Database"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.txtPassword.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUserName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtServer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txtPassword As Care.Controls.CareTextBox
    Friend WithEvents txtUserName As Care.Controls.CareTextBox
    Friend WithEvents radSQL As RadioButton
    Friend WithEvents radWindows As RadioButton
    Friend WithEvents txtDB As Care.Controls.CareTextBox
    Friend WithEvents txtServer As Care.Controls.CareTextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents btnCancel As Care.Controls.CareButton
End Class
