﻿Imports Care.Global
Imports System.Windows.Forms
Imports System.Text

Public Class frmDataExplorer

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click

        If cbxQuery.SelectedIndex < 0 Then Exit Sub
        txtOutput.Hide()

        Dim _ID As Guid = New Guid(cbxQuery.SelectedValue.ToString)

        SetCMDs(False)
        Session.CursorWaiting()

        Dim _DT As DataTable = QueryHandler.GetQueryData(_ID)
        cgData.QueryID = _ID
        cgData.Populate(_DT)

        SetCMDs(True)
        Session.CursorDefault()

    End Sub

    Private Sub SetCMDs(ByVal Enabled As Boolean)

        If Enabled Then
            btnNew.Enabled = Session.CurrentUser.IsAdministrator
            btnEdit.Enabled = Session.CurrentUser.IsAdministrator
        End If

        btnRun.Enabled = Enabled
        btnCSV.Enabled = Enabled
        btnTextBox.Enabled = Enabled
        btnMailMerge.Enabled = Enabled

    End Sub

    Private Sub frmDataExplorer_Load(sender As Object, e As EventArgs) Handles Me.Load
        PopulateQueries()
        txtOutput.Hide()
    End Sub

    Private Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        Dim _f As New frmEditQuery(Nothing)
        _f.ShowDialog()
        _f.Dispose()
        _f = Nothing
        PopulateQueries()
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click

        Dim _ID As String = cbxQuery.SelectedValue.ToString

        Dim _f As New frmEditQuery(New Guid(_ID))
        _f.ShowDialog()
        _f.Dispose()
        _f = Nothing

        PopulateQueries()

        For Each _cbi As Care.Controls.CareComboBox.ComboItem In cbxQuery.Items
            If _cbi.Value = _ID Then
                cbxQuery.SelectedItem = _cbi
                Exit For
            End If
        Next

    End Sub

    Private Sub PopulateQueries()

        cbxQuery.Clear()

        Dim _SQL As String = "select id, title from AppQueries order by title"
        cbxQuery.PopulateWithSQL(Session.ConnectionString, _SQL)

        If cbxQuery.ItemCount > 0 Then
            SetCMDs(True)
            cbxQuery.SelectedIndex = 0
        Else
            SetCMDs(False)
            btnNew.Enabled = Session.CurrentUser.IsAdministrator
            cbxQuery.SelectedIndex = -1
        End If

    End Sub

    Private Sub btnMailMerge_Click(sender As Object, e As EventArgs) Handles btnMailMerge.Click

        If cgData.RecordCount <= 0 Then Exit Sub

        Dim _DV As DataView = cgData.GetFilteredRows
        If _DV.Count <= 0 Then Exit Sub

        If _DV.Count > 500 Then
            If CareMessage("Record count is over 500 records - Are you sure you wish to continue?", MessageBoxIcon.Warning, MessageBoxButtons.YesNo, "Confirm Mailmerge") = DialogResult.No Then
                Exit Sub
            End If
        End If

        Dim _ID As Guid = New Guid(cbxQuery.SelectedValue.ToString)
        Dim _LetterCode As String = WordProcessing.SelectLetter(_ID)

        If _LetterCode = "" Then Exit Sub

        SetCMDs(False)
        Session.CursorWaiting()

        WordProcessing.OpenFile(_LetterCode, _DV)

        SetCMDs(True)
        Session.CursorDefault()

    End Sub

    Private Sub btnCSV_Click(sender As Object, e As EventArgs) Handles btnCSV.Click
        CareMessage("CSV Export has not been enabled.", MessageBoxIcon.Exclamation, "CSV Export")
    End Sub

    Private Sub btnTextBox_Click(sender As Object, e As EventArgs) Handles btnTextBox.Click

        If cgData.RecordCount <= 0 Then Exit Sub

        Dim _DV As DataView = cgData.GetFilteredRows
        If _DV.Count <= 0 Then Exit Sub

        SetCMDs(False)
        Session.CursorWaiting()

        txtOutput.Text = ""
        Dim _sb As New StringBuilder

        For Each _Row As DataRow In _DV.ToTable.Rows
            _sb.AppendLine(WriteFields(_Row))
        Next

        txtOutput.Text = _sb.ToString
        txtOutput.Show()

        SetCMDs(True)
        Session.CursorDefault()

    End Sub

    Private Function WriteFields(ByRef DR As DataRow) As String

        Dim _Return As String = ""
        For Each _f In DR.ItemArray
            If _Return = "" Then
                _Return = _f.ToString
            Else
                _Return += "," + _f.ToString
            End If
        Next

        Return _Return

    End Function

End Class