﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDocumentManagement
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.Panel1 = New Panel()
        Me.btnOpen = New Care.Controls.CareButton()
        Me.btnClose = New Care.Controls.CareButton()
        Me.btnEdit = New Care.Controls.CareButton()
        Me.btnNew = New Care.Controls.CareButton()
        Me.btnDelete = New Care.Controls.CareButton()
        Me.FileDialog = New OpenFileDialog()
        Me.cbxCategory = New Care.Controls.CareComboBox()
        Me.btnOK = New Care.Controls.CareButton()
        Me.btnCancel = New Care.Controls.CareButton()
        Me.Label12 = New Label()
        Me.Label11 = New Label()
        Me.Label9 = New Label()
        Me.Label8 = New Label()
        Me.Label7 = New Label()
        Me.Label6 = New Label()
        Me.Label5 = New Label()
        Me.Label4 = New Label()
        Me.Label3 = New Label()
        Me.txtFileSize = New Care.Controls.CareTextBox()
        Me.txtFileType = New Care.Controls.CareTextBox()
        Me.txtFileName = New Care.Controls.CareTextBox()
        Me.txtType = New Care.Controls.CareTextBox()
        Me.txtSubject = New Care.Controls.CareTextBox()
        Me.gexDocs = New Care.Controls.CareGrid()
        Me.chkPrivate = New Care.Controls.CareCheckBox()
        Me.GroupControl2 = New Care.Controls.CareFrame()
        Me.radMy = New Care.Controls.CareRadioButton()
        Me.radAll = New Care.Controls.CareRadioButton()
        Me.gbxEdit = New Care.Controls.CareFrame()
        Me.txtTags = New DevExpress.XtraEditors.TokenEdit()
        Me.txtNotes = New DevExpress.XtraEditors.MemoEdit()
        Me.Panel1.SuspendLayout()
        CType(Me.cbxCategory.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFileSize.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFileType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFileName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSubject.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkPrivate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.radMy.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radAll.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxEdit, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxEdit.SuspendLayout()
        CType(Me.txtTags.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNotes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnOpen)
        Me.Panel1.Controls.Add(Me.btnClose)
        Me.Panel1.Controls.Add(Me.btnEdit)
        Me.Panel1.Controls.Add(Me.btnNew)
        Me.Panel1.Controls.Add(Me.btnDelete)
        Me.Panel1.Dock = DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 576)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(984, 35)
        Me.Panel1.TabIndex = 3
        '
        'btnOpen
        '
        Me.btnOpen.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOpen.Appearance.Options.UseFont = True
        Me.btnOpen.Location = New System.Drawing.Point(283, 5)
        Me.btnOpen.Name = "btnOpen"
        Me.btnOpen.Size = New System.Drawing.Size(85, 25)
        Me.btnOpen.TabIndex = 3
        Me.btnOpen.Tag = "I"
        Me.btnOpen.Text = "Open"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.DialogResult = DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(890, 5)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(85, 25)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Tag = "I"
        Me.btnClose.Text = "Close"
        '
        'btnEdit
        '
        Me.btnEdit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.Appearance.Options.UseFont = True
        Me.btnEdit.Location = New System.Drawing.Point(101, 5)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(85, 25)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Tag = "I"
        Me.btnEdit.Text = "Edit"
        '
        'btnNew
        '
        Me.btnNew.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.Appearance.Options.UseFont = True
        Me.btnNew.Location = New System.Drawing.Point(10, 5)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(85, 25)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Tag = "I"
        Me.btnNew.Text = "New"
        '
        'btnDelete
        '
        Me.btnDelete.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Appearance.Options.UseFont = True
        Me.btnDelete.Location = New System.Drawing.Point(192, 5)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(85, 25)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Tag = "I"
        Me.btnDelete.Text = "Delete"
        '
        'cbxCategory
        '
        Me.cbxCategory.AllowBlank = False
        Me.cbxCategory.DataSource = Nothing
        Me.cbxCategory.DisplayMember = Nothing
        Me.cbxCategory.EnterMoveNextControl = True
        Me.cbxCategory.Location = New System.Drawing.Point(97, 185)
        Me.cbxCategory.Name = "cbxCategory"
        Me.cbxCategory.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxCategory.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxCategory.Properties.Appearance.Options.UseFont = True
        Me.cbxCategory.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxCategory.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxCategory.SelectedValue = Nothing
        Me.cbxCategory.Size = New System.Drawing.Size(400, 22)
        Me.cbxCategory.TabIndex = 13
        Me.cbxCategory.Tag = "AE"
        Me.cbxCategory.ValueMember = Nothing
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(321, 360)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(85, 25)
        Me.btnOK.TabIndex = 18
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.CausesValidation = False
        Me.btnCancel.Location = New System.Drawing.Point(412, 360)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 25)
        Me.btnCancel.TabIndex = 19
        Me.btnCancel.Text = "Cancel"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(8, 241)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(35, 13)
        Me.Label12.TabIndex = 16
        Me.Label12.Text = "Notes"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(8, 214)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(30, 13)
        Me.Label11.TabIndex = 14
        Me.Label11.Text = "Tags"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(8, 188)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(52, 13)
        Me.Label9.TabIndex = 12
        Me.Label9.Text = "Category"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(8, 159)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(41, 13)
        Me.Label8.TabIndex = 10
        Me.Label8.Text = "Private"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(8, 72)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(53, 13)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "File Name"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 130)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(45, 13)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "File Size"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(8, 101)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(50, 13)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "File Type"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 14)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Subject"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 43)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(31, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Type"
        '
        'txtFileSize
        '
        Me.txtFileSize.CharacterCasing = CharacterCasing.Normal
        Me.txtFileSize.EnterMoveNextControl = True
        Me.txtFileSize.Location = New System.Drawing.Point(97, 127)
        Me.txtFileSize.MaxLength = 0
        Me.txtFileSize.Name = "txtFileSize"
        Me.txtFileSize.NumericAllowNegatives = False
        Me.txtFileSize.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtFileSize.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFileSize.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFileSize.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFileSize.Properties.Appearance.Options.UseFont = True
        Me.txtFileSize.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFileSize.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFileSize.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFileSize.Size = New System.Drawing.Size(118, 22)
        Me.txtFileSize.TabIndex = 9
        Me.txtFileSize.Tag = "R"
        Me.txtFileSize.TextAlign = HorizontalAlignment.Left
        Me.txtFileSize.ToolTipText = ""
        '
        'txtFileType
        '
        Me.txtFileType.CharacterCasing = CharacterCasing.Normal
        Me.txtFileType.EnterMoveNextControl = True
        Me.txtFileType.Location = New System.Drawing.Point(97, 98)
        Me.txtFileType.MaxLength = 0
        Me.txtFileType.Name = "txtFileType"
        Me.txtFileType.NumericAllowNegatives = False
        Me.txtFileType.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtFileType.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFileType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFileType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFileType.Properties.Appearance.Options.UseFont = True
        Me.txtFileType.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFileType.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFileType.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFileType.Size = New System.Drawing.Size(400, 22)
        Me.txtFileType.TabIndex = 7
        Me.txtFileType.Tag = "R"
        Me.txtFileType.TextAlign = HorizontalAlignment.Left
        Me.txtFileType.ToolTipText = ""
        '
        'txtFileName
        '
        Me.txtFileName.CharacterCasing = CharacterCasing.Normal
        Me.txtFileName.EnterMoveNextControl = True
        Me.txtFileName.Location = New System.Drawing.Point(97, 69)
        Me.txtFileName.MaxLength = 0
        Me.txtFileName.Name = "txtFileName"
        Me.txtFileName.NumericAllowNegatives = False
        Me.txtFileName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtFileName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFileName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFileName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFileName.Properties.Appearance.Options.UseFont = True
        Me.txtFileName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFileName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFileName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFileName.Size = New System.Drawing.Size(400, 22)
        Me.txtFileName.TabIndex = 5
        Me.txtFileName.Tag = "R"
        Me.txtFileName.TextAlign = HorizontalAlignment.Left
        Me.txtFileName.ToolTipText = ""
        '
        'txtType
        '
        Me.txtType.CharacterCasing = CharacterCasing.Normal
        Me.txtType.EnterMoveNextControl = True
        Me.txtType.Location = New System.Drawing.Point(97, 40)
        Me.txtType.MaxLength = 0
        Me.txtType.Name = "txtType"
        Me.txtType.NumericAllowNegatives = False
        Me.txtType.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtType.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtType.Properties.Appearance.Options.UseFont = True
        Me.txtType.Properties.Appearance.Options.UseTextOptions = True
        Me.txtType.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtType.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtType.Size = New System.Drawing.Size(118, 22)
        Me.txtType.TabIndex = 3
        Me.txtType.Tag = "R"
        Me.txtType.TextAlign = HorizontalAlignment.Left
        Me.txtType.ToolTipText = ""
        '
        'txtSubject
        '
        Me.txtSubject.CharacterCasing = CharacterCasing.Normal
        Me.txtSubject.EnterMoveNextControl = True
        Me.txtSubject.Location = New System.Drawing.Point(97, 11)
        Me.txtSubject.MaxLength = 1024
        Me.txtSubject.Name = "txtSubject"
        Me.txtSubject.NumericAllowNegatives = False
        Me.txtSubject.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSubject.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSubject.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSubject.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSubject.Properties.Appearance.Options.UseFont = True
        Me.txtSubject.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSubject.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSubject.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSubject.Properties.MaxLength = 1024
        Me.txtSubject.Size = New System.Drawing.Size(400, 22)
        Me.txtSubject.TabIndex = 1
        Me.txtSubject.Tag = "AM"
        Me.txtSubject.TextAlign = HorizontalAlignment.Left
        Me.txtSubject.ToolTipText = ""
        '
        'gexDocs
        '
        Me.gexDocs.AllowBuildColumns = True
        Me.gexDocs.AllowEdit = False
        Me.gexDocs.AllowHorizontalScroll = False
        Me.gexDocs.AllowMultiSelect = False
        Me.gexDocs.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.gexDocs.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gexDocs.Appearance.Options.UseFont = True
        Me.gexDocs.AutoSizeByData = True
        Me.gexDocs.DisableAutoSize = False
        Me.gexDocs.DisableDataFormatting = False
        Me.gexDocs.FocusedRowHandle = -2147483648
        Me.gexDocs.HideFirstColumn = False
        Me.gexDocs.Location = New System.Drawing.Point(10, 50)
        Me.gexDocs.Name = "gexDocs"
        Me.gexDocs.PreviewColumn = ""
        Me.gexDocs.QueryID = Nothing
        Me.gexDocs.RowAutoHeight = False
        Me.gexDocs.SearchAsYouType = True
        Me.gexDocs.ShowAutoFilterRow = False
        Me.gexDocs.ShowFindPanel = True
        Me.gexDocs.ShowGroupByBox = False
        Me.gexDocs.ShowLoadingPanel = True
        Me.gexDocs.ShowNavigator = True
        Me.gexDocs.Size = New System.Drawing.Size(965, 520)
        Me.gexDocs.TabIndex = 1
        '
        'chkPrivate
        '
        Me.chkPrivate.EnterMoveNextControl = True
        Me.chkPrivate.Location = New System.Drawing.Point(95, 157)
        Me.chkPrivate.Name = "chkPrivate"
        Me.chkPrivate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPrivate.Properties.Appearance.Options.UseFont = True
        Me.chkPrivate.Properties.Caption = "chkPrivate"
        Me.chkPrivate.Size = New System.Drawing.Size(20, 19)
        Me.chkPrivate.TabIndex = 11
        Me.chkPrivate.Tag = "AE"
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.radMy)
        Me.GroupControl2.Controls.Add(Me.radAll)
        Me.GroupControl2.Location = New System.Drawing.Point(10, 10)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.ShowCaption = False
        Me.GroupControl2.Size = New System.Drawing.Size(965, 34)
        Me.GroupControl2.TabIndex = 0
        '
        'radMy
        '
        Me.radMy.CausesValidation = False
        Me.radMy.Location = New System.Drawing.Point(115, 8)
        Me.radMy.Name = "radMy"
        Me.radMy.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.radMy.Properties.Appearance.Options.UseFont = True
        Me.radMy.Properties.Appearance.Options.UseTextOptions = True
        Me.radMy.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radMy.Properties.AutoWidth = True
        Me.radMy.Properties.Caption = "My Documents Only"
        Me.radMy.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radMy.Properties.RadioGroupIndex = 1
        Me.radMy.Size = New System.Drawing.Size(131, 19)
        Me.radMy.TabIndex = 6
        Me.radMy.TabStop = False
        '
        'radAll
        '
        Me.radAll.CausesValidation = False
        Me.radAll.EditValue = True
        Me.radAll.Location = New System.Drawing.Point(9, 8)
        Me.radAll.Name = "radAll"
        Me.radAll.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.radAll.Properties.Appearance.Options.UseFont = True
        Me.radAll.Properties.Appearance.Options.UseTextOptions = True
        Me.radAll.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radAll.Properties.AutoWidth = True
        Me.radAll.Properties.Caption = "All Documents"
        Me.radAll.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radAll.Properties.RadioGroupIndex = 1
        Me.radAll.Size = New System.Drawing.Size(100, 19)
        Me.radAll.TabIndex = 5
        '
        'gbxEdit
        '
        Me.gbxEdit.Anchor = AnchorStyles.Top
        Me.gbxEdit.Controls.Add(Me.txtTags)
        Me.gbxEdit.Controls.Add(Me.txtNotes)
        Me.gbxEdit.Controls.Add(Me.chkPrivate)
        Me.gbxEdit.Controls.Add(Me.Label4)
        Me.gbxEdit.Controls.Add(Me.txtSubject)
        Me.gbxEdit.Controls.Add(Me.cbxCategory)
        Me.gbxEdit.Controls.Add(Me.txtType)
        Me.gbxEdit.Controls.Add(Me.btnOK)
        Me.gbxEdit.Controls.Add(Me.txtFileName)
        Me.gbxEdit.Controls.Add(Me.btnCancel)
        Me.gbxEdit.Controls.Add(Me.txtFileType)
        Me.gbxEdit.Controls.Add(Me.Label12)
        Me.gbxEdit.Controls.Add(Me.txtFileSize)
        Me.gbxEdit.Controls.Add(Me.Label11)
        Me.gbxEdit.Controls.Add(Me.Label9)
        Me.gbxEdit.Controls.Add(Me.Label3)
        Me.gbxEdit.Controls.Add(Me.Label8)
        Me.gbxEdit.Controls.Add(Me.Label5)
        Me.gbxEdit.Controls.Add(Me.Label7)
        Me.gbxEdit.Controls.Add(Me.Label6)
        Me.gbxEdit.Location = New System.Drawing.Point(237, 132)
        Me.gbxEdit.Name = "gbxEdit"
        Me.gbxEdit.ShowCaption = False
        Me.gbxEdit.Size = New System.Drawing.Size(510, 395)
        Me.gbxEdit.TabIndex = 2
        '
        'txtTags
        '
        Me.txtTags.Location = New System.Drawing.Point(97, 213)
        Me.txtTags.Name = "txtTags"
        Me.txtTags.Properties.EditMode = DevExpress.XtraEditors.TokenEditMode.Manual
        Me.txtTags.Properties.Separators.AddRange(New String() {",", ";"})
        Me.txtTags.Properties.ShowDropDown = False
        Me.txtTags.Properties.ShowRemoveTokenButtons = True
        Me.txtTags.Size = New System.Drawing.Size(400, 20)
        Me.txtTags.TabIndex = 20
        '
        'txtNotes
        '
        Me.txtNotes.Location = New System.Drawing.Point(97, 239)
        Me.txtNotes.Name = "txtNotes"
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtNotes, True)
        Me.txtNotes.Size = New System.Drawing.Size(400, 115)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtNotes, OptionsSpelling1)
        Me.txtNotes.TabIndex = 17
        Me.txtNotes.Tag = "AE"
        '
        'frmDocumentManagement
        '
        Me.AllowDrop = True
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(984, 611)
        Me.Controls.Add(Me.gbxEdit)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.gexDocs)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = FormBorderStyle.Sizable
        Me.KeyPreview = False
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(750, 600)
        Me.Name = "frmDocumentManagement"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Panel1.ResumeLayout(False)
        CType(Me.cbxCategory.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFileSize.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFileType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFileName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSubject.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkPrivate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.radMy.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radAll.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxEdit, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxEdit.ResumeLayout(False)
        Me.gbxEdit.PerformLayout()
        CType(Me.txtTags.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNotes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Protected WithEvents Panel1 As Panel
    Protected WithEvents btnClose As Care.Controls.CareButton
    Protected WithEvents btnEdit As Care.Controls.CareButton
    Protected WithEvents btnNew As Care.Controls.CareButton
    Protected WithEvents btnDelete As Care.Controls.CareButton
    Friend WithEvents gexDocs As Care.Controls.CareGrid
    Protected WithEvents btnOpen As Care.Controls.CareButton
    Friend WithEvents FileDialog As OpenFileDialog
    Friend WithEvents Label12 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txtFileSize As Care.Controls.CareTextBox
    Friend WithEvents txtFileType As Care.Controls.CareTextBox
    Friend WithEvents txtFileName As Care.Controls.CareTextBox
    Friend WithEvents txtType As Care.Controls.CareTextBox
    Friend WithEvents txtSubject As Care.Controls.CareTextBox
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents cbxCategory As Care.Controls.CareComboBox
    Friend WithEvents chkPrivate As Care.Controls.CareCheckBox
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents radMy As Care.Controls.CareRadioButton
    Friend WithEvents radAll As Care.Controls.CareRadioButton
    Friend WithEvents gbxEdit As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtNotes As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtTags As DevExpress.XtraEditors.TokenEdit

End Class
