﻿Imports Care.Global

Public Class frmAuditForm

    Private m_FormName As String = ""
    Private m_FormCaption As String = ""
    Private m_FormRecordID As Guid? = Nothing

#Region "Constuctors"

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub New(ByVal FormName As String, ByVal FormCaption As String, ByVal RecordID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_FormName = FormName
        m_FormCaption = FormCaption
        m_FormRecordID = RecordID

    End Sub

#End Region

    Private Sub frmAuditForm_Load(sender As Object, e As EventArgs) Handles Me.Load

        Me.Text = "Form Audit - " + m_FormCaption

        PopulateFormAccess()
        radRecord.Visible = m_FormRecordID.HasValue
        radData.Visible = m_FormRecordID.HasValue

    End Sub

    Private Sub radData_CheckedChanged(sender As Object, e As EventArgs) Handles radData.CheckedChanged
        If radData.Checked Then PopulateDataChanges()
    End Sub

    Private Sub radForm_CheckedChanged(sender As Object, e As EventArgs) Handles radForm.CheckedChanged
        If radForm.Checked Then PopulateFormAccess()
    End Sub

    Private Sub radRecord_CheckedChanged(sender As Object, e As EventArgs) Handles radRecord.CheckedChanged
        If radRecord.Checked Then PopulateRecordAccess()
    End Sub

    Private Sub PopulateFormAccess()

        Dim _SQL As String = ""
        _SQL += "select user_name as 'User', user_type as 'User Type', computer_name as 'Computer', login_name as 'Login', stamp as 'Stamp' from AppAuditObjects"
        _SQL += " where scope = 'Form'"
        _SQL += " and object_name = '" + m_FormName + "'"
        _SQL += " order by stamp desc"

        cgAudit.Populate(Session.ConnectionString, _SQL)
        If cgAudit.RecordCount > 0 Then
            cgAudit.Columns("Stamp").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            cgAudit.Columns("Stamp").DisplayFormat.FormatString = "dd/MM/yyyy HH:mm:ss"
        End If

    End Sub

    Private Sub PopulateRecordAccess()

        Dim _SQL As String = ""
        _SQL += "select user_name as 'User', user_type as 'User Type', computer_name as 'Computer', login_name as 'Login', stamp as 'Stamp' from AppAuditObjects"
        _SQL += " where scope = 'Record'"
        _SQL += " and object_id = '" + m_FormRecordID.ToString + "'"
        _SQL += " order by stamp desc"

        cgAudit.Populate(Session.ConnectionString, _SQL)
        If cgAudit.RecordCount > 0 Then
            cgAudit.Columns("Stamp").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            cgAudit.Columns("Stamp").DisplayFormat.FormatString = "dd/MM/yyyy HH:mm:ss"
        End If

    End Sub

    Private Sub PopulateDataChanges()

        Dim _SQL As String = ""

        _SQL += ""
        _SQL += "select object_operation as 'Operation', proc_name as 'Proc', property_name as 'Property', value_old as 'Old Value', value_new as 'New Value',"
        _SQL += " u.fullname as 'User', computer_name as 'Computer Name', login_name as 'Login', stamp as 'Stamp' from AppAuditData a"
        _SQL += " left join AppUsers u on u.ID = a.user_id"
        _SQL += " where record_id = '" + m_FormRecordID.ToString + "'"
        _SQL += " order by stamp desc, property_name"

        cgAudit.Populate(Session.ConnectionString, _SQL)
        If cgAudit.RecordCount > 0 Then
            cgAudit.Columns("Stamp").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            cgAudit.Columns("Stamp").DisplayFormat.FormatString = "dd/MM/yyyy HH:mm:ss"
        End If

    End Sub

End Class