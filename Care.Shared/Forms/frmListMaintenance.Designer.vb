﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListMaintenance
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxList = New Care.Controls.CareFrame(Me.components)
        Me.btnListCancel = New Care.Controls.CareButton(Me.components)
        Me.btnListOK = New Care.Controls.CareButton(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.cbxListType = New Care.Controls.CareComboBox(Me.components)
        Me.txtListName = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.gbxItem = New Care.Controls.CareFrame(Me.components)
        Me.chkHide = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.txtRef3 = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.txtRef2 = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.txtRef1 = New Care.Controls.CareTextBox(Me.components)
        Me.btnItemCancel = New Care.Controls.CareButton(Me.components)
        Me.btnItemOK = New Care.Controls.CareButton(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.txtItemSeq = New Care.Controls.CareTextBox(Me.components)
        Me.txtItemText = New Care.Controls.CareTextBox(Me.components)
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.grdLists = New Care.Controls.CareGridWithButtons()
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.grdItems = New Care.Controls.CareGridWithButtons()
        Me.btnItemSaveAdd = New Care.Controls.CareButton(Me.components)
        CType(Me.gbxList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxList.SuspendLayout()
        CType(Me.cbxListType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtListName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxItem, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxItem.SuspendLayout()
        CType(Me.chkHide.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRef3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRef2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRef1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtItemSeq.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtItemText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'gbxList
        '
        Me.gbxList.Anchor = AnchorStyles.None
        Me.gbxList.Controls.Add(Me.btnListCancel)
        Me.gbxList.Controls.Add(Me.btnListOK)
        Me.gbxList.Controls.Add(Me.CareLabel2)
        Me.gbxList.Controls.Add(Me.cbxListType)
        Me.gbxList.Controls.Add(Me.txtListName)
        Me.gbxList.Controls.Add(Me.CareLabel1)
        Me.gbxList.Location = New System.Drawing.Point(15, 175)
        Me.gbxList.Name = "gbxList"
        Me.gbxList.Size = New System.Drawing.Size(295, 135)
        Me.gbxList.TabIndex = 1
        '
        'btnListCancel
        '
        Me.btnListCancel.Location = New System.Drawing.Point(206, 101)
        Me.btnListCancel.Name = "btnListCancel"
        Me.btnListCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnListCancel.TabIndex = 5
        Me.btnListCancel.Text = "Cancel"
        '
        'btnListOK
        '
        Me.btnListOK.Location = New System.Drawing.Point(125, 101)
        Me.btnListOK.Name = "btnListOK"
        Me.btnListOK.Size = New System.Drawing.Size(75, 23)
        Me.btnListOK.TabIndex = 4
        Me.btnListOK.Text = "OK"
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(10, 66)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(26, 15)
        Me.CareLabel2.TabIndex = 2
        Me.CareLabel2.Text = "Type"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxListType
        '
        Me.cbxListType.AllowBlank = False
        Me.cbxListType.DataSource = Nothing
        Me.cbxListType.DisplayMember = Nothing
        Me.cbxListType.EnterMoveNextControl = True
        Me.cbxListType.Location = New System.Drawing.Point(81, 63)
        Me.cbxListType.Name = "cbxListType"
        Me.cbxListType.Properties.AccessibleName = "List Type"
        Me.cbxListType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxListType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxListType.Properties.Appearance.Options.UseFont = True
        Me.cbxListType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxListType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxListType.SelectedValue = Nothing
        Me.cbxListType.Size = New System.Drawing.Size(200, 22)
        Me.cbxListType.TabIndex = 3
        Me.cbxListType.Tag = ""
        Me.cbxListType.ValueMember = Nothing
        '
        'txtListName
        '
        Me.txtListName.CharacterCasing = CharacterCasing.Normal
        Me.txtListName.EnterMoveNextControl = True
        Me.txtListName.Location = New System.Drawing.Point(81, 32)
        Me.txtListName.MaxLength = 30
        Me.txtListName.Name = "txtListName"
        Me.txtListName.NumericAllowNegatives = False
        Me.txtListName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtListName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtListName.Properties.AccessibleName = "List Name"
        Me.txtListName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtListName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtListName.Properties.Appearance.Options.UseFont = True
        Me.txtListName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtListName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtListName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtListName.Properties.MaxLength = 30
        Me.txtListName.Size = New System.Drawing.Size(200, 22)
        Me.txtListName.TabIndex = 1
        Me.txtListName.Tag = ""
        Me.txtListName.TextAlign = HorizontalAlignment.Left
        Me.txtListName.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(10, 35)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(53, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "List Name"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbxItem
        '
        Me.gbxItem.Anchor = AnchorStyles.None
        Me.gbxItem.Controls.Add(Me.btnItemSaveAdd)
        Me.gbxItem.Controls.Add(Me.chkHide)
        Me.gbxItem.Controls.Add(Me.CareLabel8)
        Me.gbxItem.Controls.Add(Me.CareLabel7)
        Me.gbxItem.Controls.Add(Me.txtRef3)
        Me.gbxItem.Controls.Add(Me.CareLabel6)
        Me.gbxItem.Controls.Add(Me.txtRef2)
        Me.gbxItem.Controls.Add(Me.CareLabel5)
        Me.gbxItem.Controls.Add(Me.txtRef1)
        Me.gbxItem.Controls.Add(Me.btnItemCancel)
        Me.gbxItem.Controls.Add(Me.btnItemOK)
        Me.gbxItem.Controls.Add(Me.CareLabel4)
        Me.gbxItem.Controls.Add(Me.CareLabel3)
        Me.gbxItem.Controls.Add(Me.txtItemSeq)
        Me.gbxItem.Controls.Add(Me.txtItemText)
        Me.gbxItem.Location = New System.Drawing.Point(28, 126)
        Me.gbxItem.Name = "gbxItem"
        Me.gbxItem.Size = New System.Drawing.Size(432, 219)
        Me.gbxItem.TabIndex = 1
        '
        'chkHide
        '
        Me.chkHide.EnterMoveNextControl = True
        Me.chkHide.Location = New System.Drawing.Point(118, 135)
        Me.chkHide.Name = "chkHide"
        Me.chkHide.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkHide.Properties.Appearance.Options.UseFont = True
        Me.chkHide.Size = New System.Drawing.Size(20, 19)
        Me.chkHide.TabIndex = 9
        Me.chkHide.Tag = ""
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(12, 137)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(80, 15)
        Me.CareLabel8.TabIndex = 8
        Me.CareLabel8.Text = "Hide from Lists"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(12, 111)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(61, 15)
        Me.CareLabel7.TabIndex = 6
        Me.CareLabel7.Text = "Reference 3"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRef3
        '
        Me.txtRef3.CharacterCasing = CharacterCasing.Normal
        Me.txtRef3.EnterMoveNextControl = True
        Me.txtRef3.Location = New System.Drawing.Point(120, 108)
        Me.txtRef3.MaxLength = 30
        Me.txtRef3.Name = "txtRef3"
        Me.txtRef3.NumericAllowNegatives = False
        Me.txtRef3.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRef3.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRef3.Properties.AccessibleName = "Item Text"
        Me.txtRef3.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRef3.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRef3.Properties.Appearance.Options.UseFont = True
        Me.txtRef3.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRef3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRef3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRef3.Properties.MaxLength = 30
        Me.txtRef3.Size = New System.Drawing.Size(299, 22)
        Me.txtRef3.TabIndex = 7
        Me.txtRef3.Tag = ""
        Me.txtRef3.TextAlign = HorizontalAlignment.Left
        Me.txtRef3.ToolTipText = ""
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(12, 85)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(61, 15)
        Me.CareLabel6.TabIndex = 4
        Me.CareLabel6.Text = "Reference 2"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRef2
        '
        Me.txtRef2.CharacterCasing = CharacterCasing.Normal
        Me.txtRef2.EnterMoveNextControl = True
        Me.txtRef2.Location = New System.Drawing.Point(120, 82)
        Me.txtRef2.MaxLength = 30
        Me.txtRef2.Name = "txtRef2"
        Me.txtRef2.NumericAllowNegatives = False
        Me.txtRef2.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRef2.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRef2.Properties.AccessibleName = "Item Text"
        Me.txtRef2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRef2.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRef2.Properties.Appearance.Options.UseFont = True
        Me.txtRef2.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRef2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRef2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRef2.Properties.MaxLength = 30
        Me.txtRef2.Size = New System.Drawing.Size(299, 22)
        Me.txtRef2.TabIndex = 5
        Me.txtRef2.Tag = ""
        Me.txtRef2.TextAlign = HorizontalAlignment.Left
        Me.txtRef2.ToolTipText = ""
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(12, 59)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(61, 15)
        Me.CareLabel5.TabIndex = 2
        Me.CareLabel5.Text = "Reference 1"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRef1
        '
        Me.txtRef1.CharacterCasing = CharacterCasing.Normal
        Me.txtRef1.EnterMoveNextControl = True
        Me.txtRef1.Location = New System.Drawing.Point(120, 56)
        Me.txtRef1.MaxLength = 30
        Me.txtRef1.Name = "txtRef1"
        Me.txtRef1.NumericAllowNegatives = False
        Me.txtRef1.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRef1.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRef1.Properties.AccessibleName = "Item Text"
        Me.txtRef1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRef1.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRef1.Properties.Appearance.Options.UseFont = True
        Me.txtRef1.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRef1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRef1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRef1.Properties.MaxLength = 30
        Me.txtRef1.Size = New System.Drawing.Size(299, 22)
        Me.txtRef1.TabIndex = 3
        Me.txtRef1.Tag = ""
        Me.txtRef1.TextAlign = HorizontalAlignment.Left
        Me.txtRef1.ToolTipText = ""
        '
        'btnItemCancel
        '
        Me.btnItemCancel.Location = New System.Drawing.Point(344, 188)
        Me.btnItemCancel.Name = "btnItemCancel"
        Me.btnItemCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnItemCancel.TabIndex = 13
        Me.btnItemCancel.Text = "Cancel"
        '
        'btnItemOK
        '
        Me.btnItemOK.Location = New System.Drawing.Point(263, 188)
        Me.btnItemOK.Name = "btnItemOK"
        Me.btnItemOK.Size = New System.Drawing.Size(75, 23)
        Me.btnItemOK.TabIndex = 12
        Me.btnItemOK.Text = "Save"
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(12, 163)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(92, 15)
        Me.CareLabel4.TabIndex = 10
        Me.CareLabel4.Text = "Display Sequence"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(12, 33)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(49, 15)
        Me.CareLabel3.TabIndex = 0
        Me.CareLabel3.Text = "Item Text"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtItemSeq
        '
        Me.txtItemSeq.CharacterCasing = CharacterCasing.Normal
        Me.txtItemSeq.EnterMoveNextControl = True
        Me.txtItemSeq.Location = New System.Drawing.Point(120, 160)
        Me.txtItemSeq.MaxLength = 4
        Me.txtItemSeq.Name = "txtItemSeq"
        Me.txtItemSeq.NumericAllowNegatives = False
        Me.txtItemSeq.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtItemSeq.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtItemSeq.Properties.AccessibleName = "Display Sequence"
        Me.txtItemSeq.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtItemSeq.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtItemSeq.Properties.Appearance.Options.UseFont = True
        Me.txtItemSeq.Properties.Appearance.Options.UseTextOptions = True
        Me.txtItemSeq.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtItemSeq.Properties.Mask.EditMask = "##########;"
        Me.txtItemSeq.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtItemSeq.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtItemSeq.Properties.MaxLength = 4
        Me.txtItemSeq.Size = New System.Drawing.Size(60, 22)
        Me.txtItemSeq.TabIndex = 11
        Me.txtItemSeq.Tag = ""
        Me.txtItemSeq.TextAlign = HorizontalAlignment.Left
        Me.txtItemSeq.ToolTipText = ""
        '
        'txtItemText
        '
        Me.txtItemText.CharacterCasing = CharacterCasing.Normal
        Me.txtItemText.EnterMoveNextControl = True
        Me.txtItemText.Location = New System.Drawing.Point(120, 30)
        Me.txtItemText.MaxLength = 30
        Me.txtItemText.Name = "txtItemText"
        Me.txtItemText.NumericAllowNegatives = False
        Me.txtItemText.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtItemText.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtItemText.Properties.AccessibleName = "Item Text"
        Me.txtItemText.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtItemText.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtItemText.Properties.Appearance.Options.UseFont = True
        Me.txtItemText.Properties.Appearance.Options.UseTextOptions = True
        Me.txtItemText.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtItemText.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtItemText.Properties.MaxLength = 30
        Me.txtItemText.Size = New System.Drawing.Size(299, 22)
        Me.txtItemText.TabIndex = 1
        Me.txtItemText.Tag = ""
        Me.txtItemText.TextAlign = HorizontalAlignment.Left
        Me.txtItemText.ToolTipText = ""
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.SplitContainerControl1.Location = New System.Drawing.Point(12, 12)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.gbxList)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.grdLists)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.btnClose)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.gbxItem)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.grdItems)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(815, 492)
        Me.SplitContainerControl1.SplitterPosition = 323
        Me.SplitContainerControl1.TabIndex = 8
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'grdLists
        '
        Me.grdLists.ButtonsEnabled = True
        Me.grdLists.Dock = DockStyle.Fill
        Me.grdLists.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdLists.HideFirstColumn = False
        Me.grdLists.Location = New System.Drawing.Point(0, 0)
        Me.grdLists.Name = "grdLists"
        Me.grdLists.PreviewColumn = ""
        Me.grdLists.Size = New System.Drawing.Size(323, 492)
        Me.grdLists.TabIndex = 0
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnClose.DialogResult = DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(412, 469)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "Close"
        Me.btnClose.Visible = False
        '
        'grdItems
        '
        Me.grdItems.ButtonsEnabled = True
        Me.grdItems.Dock = DockStyle.Fill
        Me.grdItems.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdItems.HideFirstColumn = False
        Me.grdItems.Location = New System.Drawing.Point(0, 0)
        Me.grdItems.Name = "grdItems"
        Me.grdItems.PreviewColumn = ""
        Me.grdItems.Size = New System.Drawing.Size(487, 492)
        Me.grdItems.TabIndex = 0
        '
        'btnItemSaveAdd
        '
        Me.btnItemSaveAdd.Location = New System.Drawing.Point(182, 188)
        Me.btnItemSaveAdd.Name = "btnItemSaveAdd"
        Me.btnItemSaveAdd.Size = New System.Drawing.Size(75, 23)
        Me.btnItemSaveAdd.TabIndex = 14
        Me.btnItemSaveAdd.Text = "Save && Add"
        '
        'frmListMaintenance
        '
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(839, 516)
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.FormBorderStyle = FormBorderStyle.Sizable
        Me.KeyPreview = False
        Me.LoadMaximised = True
        Me.MinimumSize = New System.Drawing.Size(855, 555)
        Me.Name = "frmListMaintenance"
        Me.Text = "List Manager"
        Me.WindowState = FormWindowState.Maximized
        CType(Me.gbxList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxList.ResumeLayout(False)
        Me.gbxList.PerformLayout()
        CType(Me.cbxListType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtListName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxItem, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxItem.ResumeLayout(False)
        Me.gbxItem.PerformLayout()
        CType(Me.chkHide.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRef3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRef2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRef1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtItemSeq.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtItemText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents cbxListType As Care.Controls.CareComboBox
    Friend WithEvents txtListName As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents btnItemCancel As Care.Controls.CareButton
    Friend WithEvents btnItemOK As Care.Controls.CareButton
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents txtItemSeq As Care.Controls.CareTextBox
    Friend WithEvents txtItemText As Care.Controls.CareTextBox
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents grdLists As Care.Controls.CareGridWithButtons
    Friend WithEvents grdItems As Care.Controls.CareGridWithButtons
    Friend WithEvents btnListCancel As Care.Controls.CareButton
    Friend WithEvents btnListOK As Care.Controls.CareButton
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents txtRef3 As Care.Controls.CareTextBox
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents txtRef2 As Care.Controls.CareTextBox
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents txtRef1 As Care.Controls.CareTextBox
    Friend WithEvents chkHide As Care.Controls.CareCheckBox
    Friend WithEvents btnItemSaveAdd As Controls.CareButton
    Friend WithEvents gbxList As Controls.CareFrame
    Friend WithEvents gbxItem As Controls.CareFrame
End Class
