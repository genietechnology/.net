﻿Imports Care.Global
Imports System.Windows.Forms

Public Class frmSetup

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        Application.Exit()
    End Sub

    Private Sub btnOK_Click(sender As System.Object, e As System.EventArgs) Handles btnOK.Click
        If Save() Then
            Application.Restart()
        End If
    End Sub

    Private Function Save() As Boolean

        If txtServer.Text = "" Then
            Msgbox("Please enter the SQL Server name.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If txtDB.Text = "" Then
            Msgbox("Please enter the Database name.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If txtUserName.Text = "" Then
            Msgbox("Please enter the SQL Server Username.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        Dim _Key As Microsoft.Win32.RegistryKey = My.Computer.Registry.CurrentUser.CreateSubKey(Session.RegistrySystemPath)
        _Key.SetValue("SQLServer", txtServer.Text)
        _Key.SetValue("SQLDB", txtDB.Text)
        _Key.SetValue("SQLUser", txtUserName.Text)
        _Key.SetValue("SQLPassword", txtPassword.Text)

        If radWindows.Checked Then
            _Key.SetValue("IntegratedSecurity", "Y")
        Else
            _Key.SetValue("IntegratedSecurity", "N")
        End If

        _Key.Close()
        _Key = Nothing

        Return True

    End Function

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub


End Class