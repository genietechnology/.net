﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSendEmail
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSendEmail))
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.CareLabel3 = New Care.Controls.CareLabel()
        Me.lblAttachmentPath = New Care.Controls.CareLabel()
        Me.txtBody = New Care.Controls.CareRichText()
        Me.txtStatus = New Care.Controls.CareTextBox()
        Me.CareLabel4 = New Care.Controls.CareLabel()
        Me.CareLabel2 = New Care.Controls.CareLabel()
        Me.txtSubject = New Care.Controls.CareTextBox()
        Me.txtTo = New Care.Controls.CareTextBox()
        Me.CareLabel1 = New Care.Controls.CareLabel()
        Me.btnTest = New Care.Controls.CareButton()
        Me.btnClose = New Care.Controls.CareButton()
        Me.btnSend = New Care.Controls.CareButton()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSubject.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Controls.Add(Me.lblAttachmentPath)
        Me.GroupControl1.Controls.Add(Me.txtBody)
        Me.GroupControl1.Controls.Add(Me.txtStatus)
        Me.GroupControl1.Controls.Add(Me.CareLabel4)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.txtSubject)
        Me.GroupControl1.Controls.Add(Me.txtTo)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(749, 415)
        Me.GroupControl1.TabIndex = 0
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(10, 68)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(68, 15)
        Me.CareLabel3.TabIndex = 4
        Me.CareLabel3.Text = "Attachments"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAttachmentPath
        '
        Me.lblAttachmentPath.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblAttachmentPath.Appearance.Image = CType(resources.GetObject("lblAttachmentPath.Appearance.Image"), System.Drawing.Image)
        Me.lblAttachmentPath.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblAttachmentPath.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblAttachmentPath.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter
        Me.lblAttachmentPath.Location = New System.Drawing.Point(92, 66)
        Me.lblAttachmentPath.Name = "lblAttachmentPath"
        Me.lblAttachmentPath.Size = New System.Drawing.Size(61, 20)
        Me.lblAttachmentPath.TabIndex = 5
        Me.lblAttachmentPath.Text = "<Path>"
        Me.lblAttachmentPath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblAttachmentPath.Visible = False
        '
        'txtBody
        '
        Me.txtBody.HTMLText = resources.GetString("txtBody.HTMLText")
        Me.txtBody.Location = New System.Drawing.Point(92, 92)
        Me.txtBody.Name = "txtBody"
        Me.txtBody.Size = New System.Drawing.Size(646, 286)
        Me.txtBody.TabIndex = 6
        '
        'txtStatus
        '
        Me.txtStatus.Anchor = CType(((AnchorStyles.Bottom Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtStatus.CharacterCasing = CharacterCasing.Normal
        Me.txtStatus.EnterMoveNextControl = True
        Me.txtStatus.Location = New System.Drawing.Point(92, 384)
        Me.txtStatus.MaxLength = 0
        Me.txtStatus.Name = "txtStatus"
        Me.txtStatus.NumericAllowNegatives = False
        Me.txtStatus.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtStatus.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStatus.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtStatus.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtStatus.Properties.Appearance.Options.UseFont = True
        Me.txtStatus.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStatus.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtStatus.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtStatus.Properties.ReadOnly = True
        Me.txtStatus.ReadOnly = True
        Me.txtStatus.Size = New System.Drawing.Size(646, 22)
        Me.txtStatus.TabIndex = 8
        Me.txtStatus.TextAlign = HorizontalAlignment.Left
        Me.txtStatus.ToolTipText = ""
        '
        'CareLabel4
        '
        Me.CareLabel4.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(10, 387)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel4.TabIndex = 7
        Me.CareLabel4.Text = "Status"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(10, 41)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(39, 15)
        Me.CareLabel2.TabIndex = 2
        Me.CareLabel2.Text = "Subject"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSubject
        '
        Me.txtSubject.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtSubject.CharacterCasing = CharacterCasing.Normal
        Me.txtSubject.EnterMoveNextControl = True
        Me.txtSubject.Location = New System.Drawing.Point(92, 38)
        Me.txtSubject.MaxLength = 0
        Me.txtSubject.Name = "txtSubject"
        Me.txtSubject.NumericAllowNegatives = False
        Me.txtSubject.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSubject.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSubject.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSubject.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSubject.Properties.Appearance.Options.UseFont = True
        Me.txtSubject.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSubject.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSubject.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSubject.ReadOnly = False
        Me.txtSubject.Size = New System.Drawing.Size(646, 22)
        Me.txtSubject.TabIndex = 3
        Me.txtSubject.TextAlign = HorizontalAlignment.Left
        Me.txtSubject.ToolTipText = ""
        '
        'txtTo
        '
        Me.txtTo.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtTo.CharacterCasing = CharacterCasing.Normal
        Me.txtTo.EnterMoveNextControl = True
        Me.txtTo.Location = New System.Drawing.Point(92, 10)
        Me.txtTo.MaxLength = 0
        Me.txtTo.Name = "txtTo"
        Me.txtTo.NumericAllowNegatives = False
        Me.txtTo.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtTo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtTo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtTo.Properties.Appearance.Options.UseFont = True
        Me.txtTo.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtTo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTo.ReadOnly = False
        Me.txtTo.Size = New System.Drawing.Size(646, 22)
        Me.txtTo.TabIndex = 1
        Me.txtTo.TextAlign = HorizontalAlignment.Left
        Me.txtTo.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(10, 13)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(14, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "To"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnTest
        '
        Me.btnTest.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnTest.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTest.Appearance.Options.UseFont = True
        Me.btnTest.Location = New System.Drawing.Point(12, 433)
        Me.btnTest.Name = "btnTest"
        Me.btnTest.Size = New System.Drawing.Size(75, 23)
        Me.btnTest.TabIndex = 1
        Me.btnTest.Text = "Test Email"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.DialogResult = DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(686, 433)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 5
        Me.btnClose.Text = "Close"
        '
        'btnSend
        '
        Me.btnSend.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnSend.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSend.Appearance.Options.UseFont = True
        Me.btnSend.Location = New System.Drawing.Point(605, 433)
        Me.btnSend.Name = "btnSend"
        Me.btnSend.Size = New System.Drawing.Size(75, 23)
        Me.btnSend.TabIndex = 4
        Me.btnSend.Text = "Send"
        '
        'frmSendEmail
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(773, 464)
        Me.Controls.Add(Me.btnTest)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSend)
        Me.Controls.Add(Me.GroupControl1)
        Me.KeyPreview = False
        Me.Margin = New Padding(3, 5, 3, 5)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSendEmail"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "Send an Email"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSubject.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents txtSubject As Care.Controls.CareTextBox
    Friend WithEvents txtTo As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents btnSend As Care.Controls.CareButton
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents btnTest As Care.Controls.CareButton
    Friend WithEvents txtStatus As Care.Controls.CareTextBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents txtBody As Care.Controls.CareRichText
    Friend WithEvents lblAttachmentPath As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
End Class
