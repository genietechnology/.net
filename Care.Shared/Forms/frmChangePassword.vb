﻿Imports Care.Global

Public Class frmChangePassword

    Private m_IsReset As Boolean
    Private m_PasswordMinimum As Integer
    Private m_PasswordUpper As Integer
    Private m_PasswordLower As Integer
    Private m_PasswordNumber As Integer
    Private m_PasswordSpecial As Integer
    Private m_PasswordHistory As Integer

    Public Property IsReset As Boolean
        Get
            Return m_IsReset
        End Get
        Set(value As Boolean)
            m_IsReset = value
        End Set
    End Property

    Private Sub frmChangePassword_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    Private Sub frmChangePassword_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        If m_IsReset Then
            txtOld.Enabled = False
        Else
            txtOld.Enabled = True
            txtOld.BackColor = Session.ChangeColour
        End If

        txtNew.BackColor = Session.ChangeColour
        txtVerify.BackColor = Session.ChangeColour

        m_PasswordMinimum = ParameterHandler.ReturnInteger("PWDMINIMUM", False)
        m_PasswordUpper = ParameterHandler.ReturnInteger("PWDUPPER", False)
        m_PasswordLower = ParameterHandler.ReturnInteger("PWDLOWER", False)
        m_PasswordLower = ParameterHandler.ReturnInteger("PWDNUMBER", False)
        m_PasswordSpecial = ParameterHandler.ReturnInteger("PWDSPECIAL", False)
        m_PasswordHistory = ParameterHandler.ReturnInteger("PWDHISTORY", False)

    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnChange_Click(sender As System.Object, e As System.EventArgs) Handles btnChange.Click

        If txtNew.Text = "" Then
            CareMessage("Please enter your new password.", MessageBoxIcon.Exclamation, "Change Password")
            txtNew.Focus()
            Exit Sub
        End If

        If txtVerify.Text = "" Then
            CareMessage("Please verify your new password.", MessageBoxIcon.Exclamation, "Change Password")
            txtVerify.Focus()
            Exit Sub
        End If

        If txtNew.Text <> txtVerify.Text Then
            CareMessage("Passwords do not match.", MessageBoxIcon.Exclamation, "Change Password")
            txtNew.Text = ""
            txtVerify.Text = ""
            txtNew.Focus()
            Exit Sub
        End If

        If txtNew.Text = txtOld.Text Then
            CareMessage("New password must be different to the old password.", MessageBoxIcon.Exclamation, "Change Password")
            txtNew.SelectAll()
            txtNew.Focus()
            Exit Sub
        End If

        If Not PasswordHandler.ValidatePassword(txtNew.Text, m_PasswordMinimum, m_PasswordUpper, m_PasswordLower, m_PasswordNumber, m_PasswordSpecial) Then

            Dim _messageText As String = "Password does not meet minimum complexity requirements. Your password must:" & vbNewLine & vbNewLine

            If m_PasswordMinimum > 0 Then _messageText &= "Contain at least " & m_PasswordMinimum & " character(s)." & vbNewLine
            If m_PasswordUpper > 0 Then _messageText &= "Contain at least " & m_PasswordUpper & " upper case character(s)." & vbNewLine
            If m_PasswordLower > 0 Then _messageText &= "Contain at least " & m_PasswordLower & " lower case character(s)." & vbNewLine
            If m_PasswordNumber > 0 Then _messageText &= "Contain at least " & m_PasswordNumber & " numeric character(s)." & vbNewLine
            If m_PasswordSpecial > 0 Then _messageText &= "Contain at least " & m_PasswordSpecial & " special character(s)."

            CareMessage(_messageText, MessageBoxIcon.Exclamation, "Change Password")
            txtNew.SelectAll()
            txtNew.Focus()
            Exit Sub
        End If

        'check password history
        If Not PasswordHandler.CheckHistory(Session.CurrentUser.ID, m_PasswordHistory, EncyptionHandler.EncryptString(txtNew.Text)) Then
            CareMessage("Password was used in the last " & m_PasswordHistory & " days.", MessageBoxIcon.Exclamation, "Change Password")
            txtNew.SelectAll()
            txtNew.Focus()
            Exit Sub
        End If

        'get the current user record
        Dim _User As Business.User = Business.User.RetreiveByID(Session.CurrentUser.ID)
        If _User Is Nothing Then

        Else

            If txtOld.Text = EncyptionHandler.DecryptString(_User._Password) OrElse m_IsReset Then

                'if performing a password reset then the new password needs to be checked 
                'against the existing password in the database
                If m_IsReset Then
                    If txtNew.Text = EncyptionHandler.DecryptString(_User._Password) Then
                        CareMessage("New password must be different to the old password.", MessageBoxIcon.Exclamation, "Change Password")
                        txtNew.SelectAll()
                        txtNew.Focus()
                        Exit Sub
                    End If
                End If

                'save old password to history
                Dim _passwordHistory As New Business.AppUsersPasswordHistory
                With _passwordHistory
                    ._UserId = Session.CurrentUser.ID
                    ._Password = _User._Password
                    ._LastUsed = Today
                    .Store()
                End With

                _User._Password = EncyptionHandler.EncryptString(txtNew.Text)
                Business.User.SaveRecord(_User)

                CareMessage("Password changed successfully.", MessageBoxIcon.Information, "Change Password")

            Else
                CareMessage("The old password entered is invalid.", MessageBoxIcon.Exclamation, "Change Password")
                txtOld.SelectAll()
                txtOld.Focus()
                Exit Sub
            End If

        End If

        Me.DialogResult = DialogResult.OK
        Me.Close()

    End Sub
End Class
