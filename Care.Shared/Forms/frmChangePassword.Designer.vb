﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChangePassword
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.Label3 = New Label()
        Me.Label2 = New Label()
        Me.txtVerify = New Care.Controls.CareTextBox(Me.components)
        Me.txtNew = New Care.Controls.CareTextBox(Me.components)
        Me.txtOld = New Care.Controls.CareTextBox(Me.components)
        Me.Label1 = New Label()
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.btnChange = New Care.Controls.CareButton(Me.components)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtVerify.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNew.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOld.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.Label3)
        Me.GroupControl1.Controls.Add(Me.Label2)
        Me.GroupControl1.Controls.Add(Me.txtVerify)
        Me.GroupControl1.Controls.Add(Me.txtNew)
        Me.GroupControl1.Controls.Add(Me.txtOld)
        Me.GroupControl1.Controls.Add(Me.Label1)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(323, 111)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "GroupControl1"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(13, 75)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(117, 15)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "Verify New Password"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 46)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 15)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "New Password"
        '
        'txtVerify
        '
        Me.txtVerify.CharacterCasing = CharacterCasing.Normal
        Me.txtVerify.EnterMoveNextControl = True
        Me.txtVerify.Location = New System.Drawing.Point(136, 72)
        Me.txtVerify.MaxLength = 20
        Me.txtVerify.Name = "txtVerify"
        Me.txtVerify.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtVerify.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVerify.Properties.Appearance.Options.UseFont = True
        Me.txtVerify.Properties.Appearance.Options.UseTextOptions = True
        Me.txtVerify.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtVerify.Properties.MaxLength = 20
        Me.txtVerify.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtVerify.ReadOnly = False
        Me.txtVerify.Size = New System.Drawing.Size(172, 22)
        Me.txtVerify.TabIndex = 2
        Me.txtVerify.TextAlign = HorizontalAlignment.Left
        Me.txtVerify.ToolTipText = ""
        '
        'txtNew
        '
        Me.txtNew.CharacterCasing = CharacterCasing.Normal
        Me.txtNew.EnterMoveNextControl = True
        Me.txtNew.Location = New System.Drawing.Point(136, 43)
        Me.txtNew.MaxLength = 20
        Me.txtNew.Name = "txtNew"
        Me.txtNew.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtNew.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNew.Properties.Appearance.Options.UseFont = True
        Me.txtNew.Properties.Appearance.Options.UseTextOptions = True
        Me.txtNew.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtNew.Properties.MaxLength = 20
        Me.txtNew.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtNew.ReadOnly = False
        Me.txtNew.Size = New System.Drawing.Size(172, 22)
        Me.txtNew.TabIndex = 1
        Me.txtNew.TextAlign = HorizontalAlignment.Left
        Me.txtNew.ToolTipText = ""
        '
        'txtOld
        '
        Me.txtOld.CharacterCasing = CharacterCasing.Normal
        Me.txtOld.EnterMoveNextControl = True
        Me.txtOld.Location = New System.Drawing.Point(136, 14)
        Me.txtOld.MaxLength = 20
        Me.txtOld.Name = "txtOld"
        Me.txtOld.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtOld.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOld.Properties.Appearance.Options.UseFont = True
        Me.txtOld.Properties.Appearance.Options.UseTextOptions = True
        Me.txtOld.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtOld.Properties.MaxLength = 20
        Me.txtOld.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtOld.ReadOnly = False
        Me.txtOld.Size = New System.Drawing.Size(172, 22)
        Me.txtOld.TabIndex = 0
        Me.txtOld.TextAlign = HorizontalAlignment.Left
        Me.txtOld.ToolTipText = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(79, 15)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Old Password"
        '
        'btnClose
        '
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(220, 133)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(115, 25)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "Cancel"
        '
        'btnChange
        '
        Me.btnChange.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnChange.Appearance.Options.UseFont = True
        Me.btnChange.Location = New System.Drawing.Point(99, 133)
        Me.btnChange.Name = "btnChange"
        Me.btnChange.Size = New System.Drawing.Size(115, 25)
        Me.btnChange.TabIndex = 1
        Me.btnChange.Text = "Change Password"
        '
        'frmChangePassword
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.ClientSize = New System.Drawing.Size(348, 167)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnChange)
        Me.FormBorderStyle = FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmChangePassword"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "Change Password"
        Me.TopMost = True
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtVerify.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNew.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOld.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnChange As Care.Controls.CareButton
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txtVerify As Care.Controls.CareTextBox
    Friend WithEvents txtNew As Care.Controls.CareTextBox
    Friend WithEvents txtOld As Care.Controls.CareTextBox
    Friend WithEvents Label1 As Label

End Class
