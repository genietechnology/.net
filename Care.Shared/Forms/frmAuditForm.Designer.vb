﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAuditForm
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl1 = New Care.Controls.CareFrame(Me.components)
        Me.radData = New Care.Controls.CareRadioButton(Me.components)
        Me.radRecord = New Care.Controls.CareRadioButton(Me.components)
        Me.radForm = New Care.Controls.CareRadioButton(Me.components)
        Me.cgAudit = New Care.Controls.CareGrid()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.radData.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radRecord.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radForm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.radData)
        Me.GroupControl1.Controls.Add(Me.radRecord)
        Me.GroupControl1.Controls.Add(Me.radForm)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 9)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(960, 43)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "GroupControl1"
        '
        'radData
        '
        Me.radData.CausesValidation = False
        Me.radData.Location = New System.Drawing.Point(261, 13)
        Me.radData.Name = "radData"
        Me.radData.Properties.Appearance.Options.UseTextOptions = True
        Me.radData.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radData.Properties.AutoWidth = True
        Me.radData.Properties.Caption = "Changed this Record"
        Me.radData.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radData.Properties.RadioGroupIndex = 0
        Me.radData.Size = New System.Drawing.Size(122, 19)
        Me.radData.TabIndex = 4
        Me.radData.TabStop = False
        '
        'radRecord
        '
        Me.radRecord.CausesValidation = False
        Me.radRecord.Location = New System.Drawing.Point(131, 13)
        Me.radRecord.Name = "radRecord"
        Me.radRecord.Properties.Appearance.Options.UseTextOptions = True
        Me.radRecord.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radRecord.Properties.AutoWidth = True
        Me.radRecord.Properties.Caption = "Accessed this Record"
        Me.radRecord.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radRecord.Properties.RadioGroupIndex = 0
        Me.radRecord.Size = New System.Drawing.Size(124, 19)
        Me.radRecord.TabIndex = 1
        Me.radRecord.TabStop = False
        '
        'radForm
        '
        Me.radForm.CausesValidation = False
        Me.radForm.EditValue = True
        Me.radForm.Location = New System.Drawing.Point(11, 13)
        Me.radForm.Name = "radForm"
        Me.radForm.Properties.Appearance.Options.UseTextOptions = True
        Me.radForm.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radForm.Properties.AutoWidth = True
        Me.radForm.Properties.Caption = "Accessed this Form"
        Me.radForm.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radForm.Properties.RadioGroupIndex = 0
        Me.radForm.Size = New System.Drawing.Size(114, 19)
        Me.radForm.TabIndex = 0
        '
        'cgAudit
        '
        Me.cgAudit.AllowBuildColumns = True
        Me.cgAudit.AllowEdit = False
        Me.cgAudit.AllowHorizontalScroll = False
        Me.cgAudit.AllowMultiSelect = False
        Me.cgAudit.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgAudit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgAudit.Appearance.Options.UseFont = True
        Me.cgAudit.AutoSizeByData = True
        Me.cgAudit.DisableAutoSize = False
        Me.cgAudit.DisableDataFormatting = False
        Me.cgAudit.FocusedRowHandle = -2147483648
        Me.cgAudit.HideFirstColumn = False
        Me.cgAudit.Location = New System.Drawing.Point(12, 61)
        Me.cgAudit.Name = "cgAudit"
        Me.cgAudit.PreviewColumn = ""
        Me.cgAudit.QueryID = Nothing
        Me.cgAudit.RowAutoHeight = False
        Me.cgAudit.SearchAsYouType = True
        Me.cgAudit.ShowAutoFilterRow = False
        Me.cgAudit.ShowFindPanel = True
        Me.cgAudit.ShowGroupByBox = False
        Me.cgAudit.ShowLoadingPanel = False
        Me.cgAudit.ShowNavigator = True
        Me.cgAudit.Size = New System.Drawing.Size(960, 488)
        Me.cgAudit.TabIndex = 1
        '
        'frmAuditForm
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(984, 561)
        Me.Controls.Add(Me.cgAudit)
        Me.Controls.Add(Me.GroupControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.MinimumSize = New System.Drawing.Size(800, 400)
        Me.Name = "frmAuditForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form Audit"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.radData.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radRecord.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radForm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupControl1 As Controls.CareFrame
    Friend WithEvents cgAudit As Controls.CareGrid
    Friend WithEvents radRecord As Controls.CareRadioButton
    Friend WithEvents radForm As Controls.CareRadioButton
    Friend WithEvents radData As Controls.CareRadioButton
End Class
