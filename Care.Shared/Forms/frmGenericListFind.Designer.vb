﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGenericListFind
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.grdRecords = New Care.Controls.CareGrid()
        Me.Panel1 = New Panel()
        Me.btnClose = New Care.Controls.CareButton()
        Me.btnSelect = New Care.Controls.CareButton()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'grdRecords
        '
        Me.grdRecords.AllowBuildColumns = True
        Me.grdRecords.AllowEdit = False
        Me.grdRecords.AllowHorizontalScroll = False
        Me.grdRecords.AllowMultiSelect = False
        Me.grdRecords.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.grdRecords.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdRecords.Appearance.Options.UseFont = True
        Me.grdRecords.AutoSizeByData = True
        Me.grdRecords.DisableAutoSize = False
        Me.grdRecords.DisableDataFormatting = False
        Me.grdRecords.FocusedRowHandle = -2147483648
        Me.grdRecords.HideFirstColumn = False
        Me.grdRecords.Location = New System.Drawing.Point(12, 12)
        Me.grdRecords.Name = "grdRecords"
        Me.grdRecords.QueryID = Nothing
        Me.grdRecords.RowAutoHeight = False
        Me.grdRecords.SearchAsYouType = True
        Me.grdRecords.ShowAutoFilterRow = False
        Me.grdRecords.ShowFindPanel = True
        Me.grdRecords.ShowGroupByBox = True
        Me.grdRecords.ShowLoadingPanel = False
        Me.grdRecords.ShowNavigator = True
        Me.grdRecords.Size = New System.Drawing.Size(460, 358)
        Me.grdRecords.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnClose)
        Me.Panel1.Controls.Add(Me.btnSelect)
        Me.Panel1.Dock = DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 376)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(484, 35)
        Me.Panel1.TabIndex = 2
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnClose.DialogResult = DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(397, 3)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "Close"
        '
        'btnSelect
        '
        Me.btnSelect.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnSelect.Location = New System.Drawing.Point(316, 3)
        Me.btnSelect.Name = "btnSelect"
        Me.btnSelect.Size = New System.Drawing.Size(75, 23)
        Me.btnSelect.TabIndex = 0
        Me.btnSelect.Text = "Select"
        '
        'frmGenericListFind
        '
        Me.AcceptButton = Me.btnSelect
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(484, 411)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.grdRecords)
        Me.FormBorderStyle = FormBorderStyle.Sizable
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(500, 450)
        Me.Name = "frmGenericListFind"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grdRecords As Care.Controls.CareGrid
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents btnSelect As Care.Controls.CareButton

End Class
