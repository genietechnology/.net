﻿Imports System.Windows.Forms
Imports Care.Global
Imports DevExpress.XtraScheduler.UI

Public Class frmReportClause

    Private m_New As Boolean = True
    Private m_ReportID As Guid
    Private m_ClauseID As Guid?
    Private m_Clause As New Business.AppReportClause

    Public Sub New(ByVal ReportID As Guid, ByVal ClauseID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_ReportID = ReportID
        m_ClauseID = ClauseID

    End Sub

    Private Sub frmReportClause_Load(sender As Object, e As EventArgs) Handles Me.Load

        cbxClauseType.AddItem("Where Clause")
        cbxClauseType.AddItem("Order by Clause")

        cbxClauseDataType.AddItem("String")
        cbxClauseDataType.AddItem("Numeric")
        cbxClauseDataType.AddItem("Date")
        cbxClauseDataType.AddItem("Date (00:00 to 23:59)")
        cbxClauseDataType.AddItem("Boolean")

        cbxClauseOperator.AddItem("=")
        cbxClauseOperator.AddItem("<>")
        cbxClauseOperator.AddItem(">")
        cbxClauseOperator.AddItem(">=")
        cbxClauseOperator.AddItem("<")
        cbxClauseOperator.AddItem("<=")
        cbxClauseOperator.AddItem("between")
        cbxClauseOperator.AddItem("not between")
        cbxClauseOperator.AddItem("like")
        cbxClauseOperator.AddItem("not like")

        If m_ClauseID.HasValue Then
            m_New = False
            m_Clause = Business.AppReportClause.RetreiveByID(m_ClauseID.Value)
        Else
            m_Clause._ID = Guid.NewGuid()
            m_Clause._ReportId = m_ReportID
        End If

        MyControls.SetControls(ControlHandler.Mode.Add, Me.Controls, ControlHandler.DebugMode.None)
        DisplayRecord()

    End Sub

    Private Sub DisplayRecord()

        With m_Clause
            txtClauseLabel.Text = ._ClauseLabel
            chkClauseMandatory.Checked = ._ClauseMandatory
            cbxClauseType.Text = ._ClauseType
            txtClauseField1.Text = ._ClauseField1
            txtClauseField2.Text = ._ClauseField2
            cbxClauseDataType.Text = ._ClauseDatatype
            cbxClauseOperator.Text = ._ClauseOperator
            txtClauseLookup.Text = ._ClauseLookup
            txtClauseSeq.Text = ._ClauseSeq.ToString()
        End With

    End Sub

    Private Function Save() As Boolean

        If Not MyControls.Validate(Me.Controls) Then Return False

        With m_Clause
            ._ClauseLabel = txtClauseLabel.Text
            ._ClauseTooltip = ""
            ._ClauseMandatory = chkClauseMandatory.Checked
            ._ClauseType = cbxClauseType.Text
            ._ClauseField1 = txtClauseField1.Text
            ._ClauseField2 = txtClauseField2.Text
            ._ClauseDatatype = cbxClauseDataType.Text
            ._ClauseOperator = cbxClauseOperator.Text
            ._ClauseLookup = txtClauseLookup.Text
            ._ClauseSeq = ValueHandler.ConvertInteger(txtClauseSeq.Text)
            .Store()
        End With

        Return True

    End Function

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        If Save() Then
            Me.DialogResult = DialogResult.OK
            Me.Close()
        End If
    End Sub
End Class