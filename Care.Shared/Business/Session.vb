﻿Imports System.Windows.Forms
Imports Care.Data
Public Class SessionObject

#Region "Variables"

    Private m_Connection As String
    Private m_User As Business.User
    Private m_PathIcons As String
    Private m_PathReports As String
    Private m_PathTemp As String

    Private m_SMTP As Boolean
    Private m_SMTPServer As String
    Private m_SMTPPort As Integer
    Private m_SMTPAuth As Boolean
    Private m_SMTPUser As String
    Private m_SMTPPassword As String
    Private m_SMTPSender As String
    Private m_SMTPSenderName As String
    Private m_SMTPBCC As String

    Private m_ClearTemp As Boolean
    Private m_ApplicationCode As String
    Private m_ApplicationName As String
    Private m_MDIForm As IMDIForm

    Private m_SpellCheck As Boolean

    Private m_CTI As Boolean
    Private m_CTIExt As String
    Private m_CTIServer As String

    Private m_SMS As Boolean
    Private m_SMSSender As String

#End Region

#Region "Properties"

    Public ReadOnly Property RegistrySystemPath As String
        Get
            Return "Software\Care Software\System"
        End Get
    End Property

    Public ReadOnly Property ApplicationCode() As String
        Get
            Return m_ApplicationCode
        End Get
    End Property

    Public ReadOnly Property ApplicationName() As String
        Get
            Return m_ApplicationName
        End Get
    End Property

    Public ReadOnly Property ConnectionString() As String
        Get
            Return m_Connection
        End Get
    End Property

    Public Property CurrentUser() As Business.User
        Get
            Return m_User
        End Get
        Set(ByVal value As Business.User)
            m_User = value
        End Set
    End Property

    Public ReadOnly Property IconFolder() As String
        Get
            Return m_PathIcons
        End Get
    End Property

    Public ReadOnly Property ReportFolder() As String
        Get
            Return m_PathReports
        End Get
    End Property

    Public ReadOnly Property TempFolder() As String
        Get
            Return m_PathTemp
        End Get
    End Property

    Public ReadOnly Property SMTPEnabled() As Boolean
        Get
            Return m_SMTP
        End Get
    End Property

    Public ReadOnly Property SMTPServer() As String
        Get
            Return m_SMTPServer
        End Get
    End Property

    Public ReadOnly Property SMTPPort() As Integer
        Get
            Return m_SMTPPort
        End Get
    End Property

    Public ReadOnly Property SMTPAuth() As Boolean
        Get
            Return m_SMTPAuth
        End Get
    End Property

    Public ReadOnly Property SMTPSenderEmail As String
        Get
            Return m_SMTPSender
        End Get
    End Property

    Public ReadOnly Property SMTPSenderName As String
        Get
            Return m_SMTPSenderName
        End Get
    End Property

    Public ReadOnly Property SMTPUser As String
        Get
            Return m_SMTPUser
        End Get
    End Property

    Public ReadOnly Property SMTPPassword As String
        Get
            Return m_SMTPPassword
        End Get
    End Property

    Public Property ChangeColour() As System.Drawing.Color
        Get
            Return Drawing.Color.Yellow
        End Get
        Set(ByVal value As System.Drawing.Color)

        End Set
    End Property

    Public Property FindColour() As System.Drawing.Color
        Get
            Return Drawing.Color.LightGreen
        End Get
        Set(ByVal value As System.Drawing.Color)

        End Set
    End Property

    Public Property MDIForm As Care.Shared.IMDIForm
        Get
            Return m_MDIForm
        End Get
        Set(ByVal value As Care.Shared.IMDIForm)
            m_MDIForm = value
        End Set
    End Property

    Public Property SpellCheck As Boolean
        Get
            Return m_SpellCheck
        End Get
        Set(value As Boolean)
            m_SpellCheck = value
        End Set
    End Property

    Public ReadOnly Property CTIEnabled As Boolean
        Get
            Return m_CTI
        End Get
    End Property

    Public ReadOnly Property CTIServer As String
        Get
            Return m_CTIServer
        End Get
    End Property

    Public ReadOnly Property CTIExt As String
        Get
            Return m_CTIExt
        End Get
    End Property

    Public ReadOnly Property SMSEnabled As Boolean
        Get
            Return m_SMS
        End Get
    End Property

    Public ReadOnly Property SMSSender As String
        Get
            Return m_SMSSender
        End Get
    End Property

#End Region

    Public Function Setup(ByVal ConnectionDetails As ConnectionBuilder, ByVal ApplicationCode As String, ByVal ApplicationName As String) As Boolean

        m_ApplicationCode = ApplicationCode
        m_ApplicationName = ApplicationName

        'return connection details from the system database (kickstart)
        If ConnectionDetails IsNot Nothing Then

            m_Connection = ConnectionDetails.ConnectionString

            'check the connection works
            ConnectionDetails.TestConnection()

            If ConnectionDetails.ConnectionOK Then

                'create admin account if it doesnt already exist
                CreateAdminUser()

                'create any mandatory parameters if they dont exist
                CreateParameters()

                'set parameters
                SetParameters()

            Else
                'application connection failed
                CareMessage("Connection Failed:" + m_Connection, MessageBoxIcon.Exclamation, MessageBoxButtons.OK, "Session.Setup")
                Return False
            End If

        Else
            Return False
        End If

        Return True

    End Function

    Public Sub ClearTempFolders()
        If m_ClearTemp Then
            FileHandler.ClearTemporaryFolder(Session.TempFolder)
        End If
    End Sub

    Private Sub CreateAdminUser()
        Business.User.CreateAdmin()
    End Sub

    Private Sub CreateParameters()

        If Not ParameterHandler.CheckExists("ICONPATH") Then ParameterHandler.Create("ICONPATH", ParameterHandler.EnumParameterType.FilePathType, "System Paths", "Icons Folder", "C:\App\Icons")
        If Not ParameterHandler.CheckExists("REPORTPATH") Then ParameterHandler.Create("REPORTPATH", ParameterHandler.EnumParameterType.FilePathType, "System Paths", "Reports Folder", "C:\App\Reports")
        If Not ParameterHandler.CheckExists("TEMPPATH") Then ParameterHandler.Create("TEMPPATH", ParameterHandler.EnumParameterType.FilePathType, "System Paths", "Temporary Folder", "C:\App\Temp")

        If Not ParameterHandler.CheckExists("SMTP") Then ParameterHandler.Create("SMTP", ParameterHandler.EnumParameterType.BooleanType, "SMTP Email", "SMTP Email Function", "False")
        If Not ParameterHandler.CheckExists("SMTPSERVER") Then ParameterHandler.Create("SMTPSERVER", ParameterHandler.EnumParameterType.StringType, "SMTP Email", "SMTP Server", "")
        If Not ParameterHandler.CheckExists("SMTPPORT") Then ParameterHandler.Create("SMTPPORT", ParameterHandler.EnumParameterType.IntegerType, "SMTP Email", "SMTP Port", "25")
        If Not ParameterHandler.CheckExists("SMTPAUTH") Then ParameterHandler.Create("SMTPAUTH", ParameterHandler.EnumParameterType.BooleanType, "SMTP Email", "SMTP Authentication", "False")
        If Not ParameterHandler.CheckExists("SMTPUSER") Then ParameterHandler.Create("SMTPUSER", ParameterHandler.EnumParameterType.StringType, "SMTP Email", "SMTP Username", "")
        If Not ParameterHandler.CheckExists("SMTPPASSWORD") Then ParameterHandler.Create("SMTPPASSWORD", ParameterHandler.EnumParameterType.StringType, "SMTP Email", "SMTP Password", "")
        If Not ParameterHandler.CheckExists("SMTPSENDER") Then ParameterHandler.Create("SMTPSENDER", ParameterHandler.EnumParameterType.StringType, "SMTP Email", "SMTP Sender Email", "")
        If Not ParameterHandler.CheckExists("SMTPSENDERNAME") Then ParameterHandler.Create("SMTPSENDERNAME", ParameterHandler.EnumParameterType.StringType, "SMTP Email", "SMTP Sender Name", "")
        If Not ParameterHandler.CheckExists("SMTPBCC") Then ParameterHandler.Create("SMTPBCC", ParameterHandler.EnumParameterType.StringType, "SMTP Email", "SMTP BCC Email", "")

        If Not ParameterHandler.CheckExists("BACKUPCHECK") Then ParameterHandler.Create("BACKUPCHECK", ParameterHandler.EnumParameterType.BooleanType, "Backup", "Backup Reminders", "True")
        If Not ParameterHandler.CheckExists("BACKUPPATH") Then ParameterHandler.Create("BACKUPPATH", ParameterHandler.EnumParameterType.FilePathType, "Backup", "Backup Path", "C:\App\Backup")
        If Not ParameterHandler.CheckExists("BACKUPLAST") Then ParameterHandler.Create("BACKUPLAST", ParameterHandler.EnumParameterType.DateType, "Backup", "Last Backup", "")
        If Not ParameterHandler.CheckExists("BACKUPDAYS") Then ParameterHandler.Create("BACKUPDAYS", ParameterHandler.EnumParameterType.IntegerType, "Backup", "Backup Warning (days)", "7")

        If Not ParameterHandler.CheckExists("SPELL") Then ParameterHandler.Create("SPELL", ParameterHandler.EnumParameterType.BooleanType, "Spell Checker", "Spell Checking Enabled", "False")
        If Not ParameterHandler.CheckExists("SPELLDICT") Then ParameterHandler.Create("SPELLDICT", ParameterHandler.EnumParameterType.StringType, "Spell Checker", "Spell Dictionary Path", "")
        If Not ParameterHandler.CheckExists("SPELLGRAM") Then ParameterHandler.Create("SPELLGRAM", ParameterHandler.EnumParameterType.StringType, "Spell Checker", "Grammar Dictionary Path", "")

        If Not ParameterHandler.CheckExists("CTI") Then ParameterHandler.Create("CTI", ParameterHandler.EnumParameterType.BooleanType, "CTI", "CTI Enabled", "False")
        If Not ParameterHandler.CheckExists("CTISERVER") Then ParameterHandler.Create("CTISERVER", ParameterHandler.EnumParameterType.StringType, "CTI", "Server IP", "DEMOSERVER")
        If Not ParameterHandler.CheckExists("CTIEXT") Then ParameterHandler.Create("CTIEXT", ParameterHandler.EnumParameterType.StringType, "CTI", "CTI Extension", "101")

        If Not ParameterHandler.CheckExists("SMS") Then ParameterHandler.Create("SMS", ParameterHandler.EnumParameterType.BooleanType, "SMS", "SMS Enabled", "False")
        If Not ParameterHandler.CheckExists("SMSSENDER") Then ParameterHandler.Create("SMSSENDER", ParameterHandler.EnumParameterType.StringType, "SMS", "SMS Sender Email", "sms@caresoftware.co.uk")

    End Sub

    Private Sub SetParameters()

        m_PathIcons = ParameterHandler.ReturnPath("ICONPATH")
        m_PathReports = ParameterHandler.ReturnPath("REPORTPATH")
        m_PathTemp = ParameterHandler.ReturnPath("TEMPPATH")

        m_SMTP = ParameterHandler.ReturnBoolean("SMTP")
        m_SMTPServer = ParameterHandler.ReturnString("SMTPSERVER")
        m_SMTPPort = ParameterHandler.ReturnInteger("SMTPPORT", False)
        m_SMTPAuth = ParameterHandler.ReturnBoolean("SMTPAUTH")
        m_SMTPUser = ParameterHandler.ReturnString("SMTPUSER")
        m_SMTPPassword = ParameterHandler.ReturnString("SMTPPASSWORD")
        m_SMTPSender = ParameterHandler.ReturnString("SMTPSENDER")
        m_SMTPSenderName = ParameterHandler.ReturnString("SMTPSENDERNAME")
        m_SMTPBCC = ParameterHandler.ReturnString("SMTPBCC")

        m_CTI = ParameterHandler.ReturnBoolean("CTI")
        m_CTIServer = ParameterHandler.ReturnString("CTISERVER")
        m_CTIExt = ParameterHandler.ReturnString("CTIEXT")

        m_SMS = ParameterHandler.ReturnBoolean("SMS")
        m_SMSSENDER = ParameterHandler.ReturnString("SMSSENDER")

        m_ClearTemp = True

    End Sub

    Public Sub CursorDefault()
        Cursor.Current = Cursors.Default
        Cursor.Show()
    End Sub

    Public Sub CursorWaiting()
        Cursor.Current = Cursors.WaitCursor
        Cursor.Show()
    End Sub

    Public Sub CursorApplicationStarting()
        Cursor.Current = Cursors.AppStarting
        Cursor.Show()
    End Sub

End Class
