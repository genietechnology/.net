﻿Public Class CRM

    Public Enum EnumLinkType
        Family
        Lead
        Account
        Staff
    End Enum

    Public Enum EnumActivityType
        PhoneCall
        Email
        SMS
        Appointment
        Feedback
        Note
    End Enum

    Public Shared Function CreateActivityRecord(ByVal CRMLinkID As Guid, ByVal LinkType As EnumLinkType, ActivityType As EnumActivityType, _
                                                ByVal ContactID As Guid?, ByVal ContactName As String, _
                                                ByVal Subject As String, ByVal Notes As String, Optional ByVal ViaSystem As Boolean = False) As Guid?

        Dim _Return As Guid? = Nothing
        Dim _Activity As New Business.ActivityCRM
        With _Activity

            ._LinkId = CRMLinkID
            ._LinkType = LinkType.ToString
            ._ContactId = ContactID
            ._ContactName = ContactName
            ._ActionType = ActivityType.ToString
            ._System = ViaSystem

            ._ActionDate = Now
            ._Subject = Subject
            ._Notes = Notes
            ._Direction = "Out"

            ._Fup = False
            ._FupDate = Nothing

            .Store()

            _Return = ._ID

        End With

        _Activity = Nothing

        Return _Return

    End Function

    Public Shared Function CreateActivity(ByVal CRMLinkID As Guid, ByVal LinkType As EnumLinkType, ActivityType As EnumActivityType, _
                                          ByVal ContactID As Guid?, ByVal ContactName As String) As Guid?

        Dim _Return As Guid? = Nothing
        Dim _frmCRMActivity As New frmCRMActivity(CRMLinkID, LinkType, ActivityType, ContactID, ContactName)
        _frmCRMActivity.ShowDialog()

        If _frmCRMActivity.DialogResult = DialogResult.OK Then
            _Return = _frmCRMActivity.ActivityID
        Else
            _Return = Nothing
        End If

        _frmCRMActivity.Dispose()
        _frmCRMActivity = Nothing

        Return _Return

    End Function

    Public Shared Function AmendActivity(ByVal ActivityID As Guid) As Boolean

        Dim _Return As Boolean = False
        Dim _frmCRMActivity As New frmCRMActivity(ActivityID)
        _frmCRMActivity.ShowDialog()

        If _frmCRMActivity.DialogResult = DialogResult.OK Then
            _Return = True
        Else
            _Return = False
        End If

        _frmCRMActivity.Dispose()
        _frmCRMActivity = Nothing

        Return _Return

    End Function

End Class
