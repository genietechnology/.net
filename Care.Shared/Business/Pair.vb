﻿Imports Care.Global

Public Class Pair

    Private m_Code As String
    Private m_Description As String

    Public Sub New(ByVal Code As String, ByVal Description As String)
        m_Code = Code
        m_Description = Description
    End Sub

    Public Property Code() As String
        Get
            Return m_Code
        End Get
        Set(ByVal value As String)
            m_Code = value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return m_Description
        End Get
        Set(ByVal value As String)
            m_Description = value
        End Set
    End Property

End Class