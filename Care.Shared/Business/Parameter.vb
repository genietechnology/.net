﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Partial Class Parameter

        Public Shared Function RetrieveByName(ByVal ParameterName As String) As Business.Parameter

            Dim _Parameter As Parameter
            _Parameter = PropertiesFromData(DAL.GetRowfromSPwithParam(Session.ConnectionString, "getParameterbyName", "@Name", SqlDbType.VarChar, ParameterName))
            Return _Parameter

        End Function

    End Class

End Namespace

