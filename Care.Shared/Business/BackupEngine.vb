﻿Imports Care.Global
Imports Care.Data

Public Class BackupEngine

#Region "Properties"

    Public Property Result As Integer = 0
    Public Property Errors As String = ""
    Public Property Status As String = ""

    Public ReadOnly Property HasErrors As Boolean
        Get
            If Errors = "" Then
                Return False
            Else
                Return True
            End If
        End Get
    End Property

    Private m_BackupPath As String = ""
    Private m_BackupFile As String = ""
    Private m_BackupDB As String = ""
    Private m_BackupKeep As Integer = 0
    Private m_BackupPassword As String = ""

#End Region

    Public Sub Backup()

        m_BackupPath = ParameterHandler.ReturnPath("BACKUPPATH")

        m_BackupKeep = ParameterHandler.ReturnInteger("BACKUPKEEP", False)
        If m_BackupKeep = 0 Then m_BackupKeep = 14

        m_BackupPassword = ParameterHandler.ReturnString("BACKUPPASSWORD")
        If m_BackupPassword = "" Then m_BackupPassword = "-WhyDoesItAlwaysRainOnMe122126!"

        Dim _cn As SqlClient.SqlConnection = DAL.ReturnConnection(Session.ConnectionString)
        If _cn IsNot Nothing Then

            m_BackupDB = _cn.Database.ToLower
            m_BackupFile = m_BackupDB + "_" + Format(Date.Now, "ddMMyyyy_HHmmss.bak")

            Session.SetProgressMessage("Performing Cleanup...")
            PerformCleanup()

            Threading.Thread.Sleep(3000)

            Session.SetProgressMessage("Performing Backup...")
            If PerformBackup() Then

                Threading.Thread.Sleep(3000)

                Session.SetProgressMessage("Zip Backup...")
                If ZipBackup() Then

                    Status = "Backup and Zip Completed Successfully."

                    Threading.Thread.Sleep(3000)

                    Try
                        IO.File.Delete(m_BackupPath + m_BackupFile)

                    Catch ex As Exception
                        Errors += ex.Message + vbCrLf + vbCrLf
                    End Try

                    ParameterHandler.SetDate("BACKUPLAST", Date.Today)

                Else
                    Status = "Backup Completed Successfully - Zip Failed."
                    Result = -2
                End If

            Else
                Status = "Backup Failed."
                Result = -1
            End If

        Else
            Status = "DB Connection Failed."
            Result = -3
        End If

    End Sub

    Private Function ZipBackup() As Boolean

        Dim _OK As Boolean = True

        Try
            Dim _Path As String = m_BackupPath + m_BackupFile.Replace(".bak", ".zip")
            Dim z As New Ionic.Zip.ZipFile
            z.Encryption = Ionic.Zip.EncryptionAlgorithm.WinZipAes256
            z.Password = m_BackupPassword
            z.AddFile(m_BackupPath + m_BackupFile)
            z.Save(_Path)

        Catch ex As Exception
            Errors += ex.Message + vbCrLf + vbCrLf
            _OK = False
        End Try

        Return _OK

    End Function

    Private Sub PerformCleanup()

        Try

            Dim _d As New IO.DirectoryInfo(m_BackupPath)

            For Each _f As IO.FileInfo In _d.GetFiles
                If (Now - _f.CreationTime).Days > m_BackupKeep Then _f.Delete()
            Next

        Catch ex As Exception
            Errors += ex.Message + vbCrLf + vbCrLf
        End Try

    End Sub

    Private Function PerformBackup() As Boolean

        Dim _OK As Boolean = True

        Dim _SQL As String = "BACKUP DATABASE " + m_BackupDB &
                             " TO DISK = '" + m_BackupPath + m_BackupFile + "'" &
                             " WITH FORMAT"

        Dim _cmd As SqlClient.SqlCommand = DAL.ReturnTextCommand(Session.ConnectionString, _SQL)

        Try
            _cmd.ExecuteNonQuery()

        Catch ex As Exception
            Errors += ex.Message + vbCrLf + vbCrLf
            _OK = False
        End Try

        DAL.DisposeofCommand(_cmd)

        Return _OK

    End Function

End Class
