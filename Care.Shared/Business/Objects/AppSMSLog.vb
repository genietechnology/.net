﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class AppSMSLog
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_OutboxId As Guid?
        Dim m_SmsNumber As String
        Dim m_SmsMessage As String
        Dim m_UserId As Guid?
        Dim m_Stamp As DateTime?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._OutboxId = Nothing
                ._SmsNumber = ""
                ._SmsMessage = ""
                ._UserId = Nothing
                ._Stamp = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._OutboxId = Nothing
                ._SmsNumber = ""
                ._SmsMessage = ""
                ._UserId = Nothing
                ._Stamp = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@OutboxId")> _
        Public Property _OutboxId() As Guid?
            Get
                Return m_OutboxId
            End Get
            Set(ByVal value As Guid?)
                m_OutboxId = value
            End Set
        End Property

        <StoredProcParameter("@SmsNumber")> _
        Public Property _SmsNumber() As String
            Get
                Return m_SmsNumber
            End Get
            Set(ByVal value As String)
                m_SmsNumber = value
            End Set
        End Property

        <StoredProcParameter("@SmsMessage")> _
        Public Property _SmsMessage() As String
            Get
                Return m_SmsMessage
            End Get
            Set(ByVal value As String)
                m_SmsMessage = value
            End Set
        End Property

        <StoredProcParameter("@UserId")> _
        Public Property _UserId() As Guid?
            Get
                Return m_UserId
            End Get
            Set(ByVal value As Guid?)
                m_UserId = value
            End Set
        End Property

        <StoredProcParameter("@Stamp")> _
        Public Property _Stamp() As DateTime?
            Get
                Return m_Stamp
            End Get
            Set(ByVal value As DateTime?)
                m_Stamp = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As AppSMSLog

            Dim _AppSMSLog As AppSMSLog
            _AppSMSLog = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getAppSMSLogbyID", ID))
            Return _AppSMSLog

        End Function

        Public Shared Function RetreiveAll() As List(Of AppSMSLog)

            Dim _AppSMSLogList As List(Of AppSMSLog)
            _AppSMSLogList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getAppSMSLogTable"))
            Return _AppSMSLogList

        End Function

        Public Shared Sub SaveAll(ByVal AppSMSLogList As List(Of AppSMSLog))

            For Each _AppSMSLog As AppSMSLog In AppSMSLogList
                SaveRecord(_AppSMSLog)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal AppSMSLog As AppSMSLog) As Guid
            DAL.SaveRecord(Session.ConnectionString, AppSMSLog, "upsertAppSMSLog")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteAppSMSLogbyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertAppSMSLog")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As AppSMSLog

            Dim _A As New AppSMSLog()

            If DR IsNot Nothing Then
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("id"))
                    _A._OutboxId = GetGUID(DR("outbox_id"))
                    _A._SmsNumber = DR("sms_number").ToString.Trim
                    _A._SmsMessage = DR("sms_message").ToString.Trim
                    _A._UserId = GetGUID(DR("user_id"))
                    _A._Stamp = GetDateTime(DR("stamp"))

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of AppSMSLog)

            Dim _AppSMSLogList As New List(Of AppSMSLog)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _AppSMSLogList.Add(PropertiesFromData(_DR))
            Next

            Return _AppSMSLogList

        End Function


#End Region

    End Class

End Namespace
