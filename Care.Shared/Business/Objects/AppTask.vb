﻿'*****************************************************
'Generated 26/03/2016 12:19:36 using Version 1.15.8.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class AppTask
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Name As String
        Dim m_Description As String
        Dim m_Day As Byte
        Dim m_Time As TimeSpan?
        Dim m_TaskType As String
        Dim m_ObjectName As String
        Dim m_Command As String
        Dim m_Args As String
        Dim m_LastRun As DateTime?
        Dim m_LastResult As Integer

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Name = ""
                ._Description = ""
                ._Day = Nothing
                ._Time = Nothing
                ._TaskType = ""
                ._ObjectName = ""
                ._Command = ""
                ._Args = ""
                ._LastRun = Nothing
                ._LastResult = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Name = ""
                ._Description = ""
                ._Day = Nothing
                ._Time = Nothing
                ._TaskType = ""
                ._ObjectName = ""
                ._Command = ""
                ._Args = ""
                ._LastRun = Nothing
                ._LastResult = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Name")> _
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@Description")> _
        Public Property _Description() As String
            Get
                Return m_Description
            End Get
            Set(ByVal value As String)
                m_Description = value
            End Set
        End Property

        <StoredProcParameter("@Day")> _
        Public Property _Day() As Byte
            Get
                Return m_Day
            End Get
            Set(ByVal value As Byte)
                m_Day = value
            End Set
        End Property

        <StoredProcParameter("@Time")> _
        Public Property _Time() As TimeSpan?
            Get
                Return m_Time
            End Get
            Set(ByVal value As TimeSpan?)
                m_Time = value
            End Set
        End Property

        <StoredProcParameter("@TaskType")> _
        Public Property _TaskType() As String
            Get
                Return m_TaskType
            End Get
            Set(ByVal value As String)
                m_TaskType = value
            End Set
        End Property

        <StoredProcParameter("@ObjectName")> _
        Public Property _ObjectName() As String
            Get
                Return m_ObjectName
            End Get
            Set(ByVal value As String)
                m_ObjectName = value
            End Set
        End Property

        <StoredProcParameter("@Command")> _
        Public Property _Command() As String
            Get
                Return m_Command
            End Get
            Set(ByVal value As String)
                m_Command = value
            End Set
        End Property

        <StoredProcParameter("@Args")> _
        Public Property _Args() As String
            Get
                Return m_Args
            End Get
            Set(ByVal value As String)
                m_Args = value
            End Set
        End Property

        <StoredProcParameter("@LastRun")> _
        Public Property _LastRun() As DateTime?
            Get
                Return m_LastRun
            End Get
            Set(ByVal value As DateTime?)
                m_LastRun = value
            End Set
        End Property

        <StoredProcParameter("@LastResult")> _
        Public Property _LastResult() As Integer
            Get
                Return m_LastResult
            End Get
            Set(ByVal value As Integer)
                m_LastResult = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As AppTask

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getAppTaskbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of AppTask)

            Dim _AppTaskList As List(Of AppTask)
            _AppTaskList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getAppTaskTable"))
            Return _AppTaskList

        End Function

        Public Shared Sub SaveAll(ByVal AppTaskList As List(Of AppTask))

            For Each _AppTask As AppTask In AppTaskList
                SaveRecord(_AppTask)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal AppTask As AppTask) As Guid
            AppTask.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, AppTask, "upsertAppTask")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteAppTaskbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertAppTask")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As AppTask

            Dim _A As AppTask = Nothing

            If DR IsNot Nothing Then
                _A = New AppTask()
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("ID"))
                    _A._Name = DR("name").ToString.Trim
                    _A._Description = DR("description").ToString.Trim
                    _A._Day = GetByte(DR("day"))
                    _A._Time = GetTimeSpan(DR("time"))
                    _A._TaskType = DR("task_type").ToString.Trim
                    _A._ObjectName = DR("object_name").ToString.Trim
                    _A._Command = DR("command").ToString.Trim
                    _A._Args = DR("args").ToString.Trim
                    _A._LastRun = GetDateTime(DR("last_run"))
                    _A._LastResult = GetInteger(DR("last_result"))

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of AppTask)

            Dim _AppTaskList As New List(Of AppTask)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _AppTaskList.Add(PropertiesFromData(_DR))
            Next

            Return _AppTaskList

        End Function


#End Region

    End Class

End Namespace
