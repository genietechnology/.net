﻿'*****************************************************
'Generated 01/05/2018 10:49:58 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class AppUsersPasswordHistory
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_UserId As Guid?
        Dim m_Password As String
        Dim m_LastUsed As Date?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._UserId = Nothing
                ._Password = ""
                ._LastUsed = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._UserId = Nothing
                ._Password = ""
                ._LastUsed = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@UserId")>
        Public Property _UserId() As Guid?
            Get
                Return m_UserId
            End Get
            Set(ByVal value As Guid?)
                m_UserId = value
            End Set
        End Property

        <StoredProcParameter("@Password")>
        Public Property _Password() As String
            Get
                Return m_Password
            End Get
            Set(ByVal value As String)
                m_Password = value
            End Set
        End Property

        <StoredProcParameter("@LastUsed")>
        Public Property _LastUsed() As Date?
            Get
                Return m_LastUsed
            End Get
            Set(ByVal value As Date?)
                m_LastUsed = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As AppUsersPasswordHistory

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getAppUsersPasswordHistorybyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As AppUsersPasswordHistory

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getAppUsersPasswordHistorybyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of AppUsersPasswordHistory)

            Dim _AppUsersPasswordHistoryList As List(Of AppUsersPasswordHistory)
            _AppUsersPasswordHistoryList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getAppUsersPasswordHistoryTable"))
            Return _AppUsersPasswordHistoryList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of AppUsersPasswordHistory)

            Dim _AppUsersPasswordHistoryList As List(Of AppUsersPasswordHistory)
            _AppUsersPasswordHistoryList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getAppUsersPasswordHistoryTable"))
            Return _AppUsersPasswordHistoryList

        End Function

        Public Shared Sub SaveAll(ByVal AppUsersPasswordHistoryList As List(Of AppUsersPasswordHistory))

            For Each _AppUsersPasswordHistory As AppUsersPasswordHistory In AppUsersPasswordHistoryList
                SaveRecord(_AppUsersPasswordHistory)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal AppUsersPasswordHistoryList As List(Of AppUsersPasswordHistory))

            For Each _AppUsersPasswordHistory As AppUsersPasswordHistory In AppUsersPasswordHistoryList
                SaveRecord(ConnectionString, _AppUsersPasswordHistory)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal AppUsersPasswordHistory As AppUsersPasswordHistory) As Guid
            AppUsersPasswordHistory.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, AppUsersPasswordHistory, "upsertAppUsersPasswordHistory")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal AppUsersPasswordHistory As AppUsersPasswordHistory) As Guid
            AppUsersPasswordHistory.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, AppUsersPasswordHistory, "upsertAppUsersPasswordHistory")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteAppUsersPasswordHistorybyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteAppUsersPasswordHistorybyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertAppUsersPasswordHistory")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertAppUsersPasswordHistory")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As AppUsersPasswordHistory

            Dim _A As AppUsersPasswordHistory = Nothing

            If DR IsNot Nothing Then
                _A = New AppUsersPasswordHistory()
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("ID"))
                    _A._UserId = GetGUID(DR("user_id"))
                    _A._Password = DR("password").ToString.Trim
                    _A._LastUsed = GetDate(DR("last_used"))

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of AppUsersPasswordHistory)

            Dim _AppUsersPasswordHistoryList As New List(Of AppUsersPasswordHistory)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _AppUsersPasswordHistoryList.Add(PropertiesFromData(_DR))
            Next

            Return _AppUsersPasswordHistoryList

        End Function


#End Region

    End Class

End Namespace
