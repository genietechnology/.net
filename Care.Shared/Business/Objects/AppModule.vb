﻿'*****************************************************
'Generated 03/05/2015 14:23:48 using Version 1.15.5.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class AppModule
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Caption As String
        Dim m_Tooltip As String
        Dim m_Icon As String
        Dim m_DisplaySeq As Long

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Caption = ""
                ._Tooltip = ""
                ._Icon = ""
                ._DisplaySeq = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Caption = ""
                ._Tooltip = ""
                ._Icon = ""
                ._DisplaySeq = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Caption")> _
        Public Property _Caption() As String
            Get
                Return m_Caption
            End Get
            Set(ByVal value As String)
                m_Caption = value
            End Set
        End Property

        <StoredProcParameter("@Tooltip")> _
        Public Property _Tooltip() As String
            Get
                Return m_Tooltip
            End Get
            Set(ByVal value As String)
                m_Tooltip = value
            End Set
        End Property

        <StoredProcParameter("@Icon")> _
        Public Property _Icon() As String
            Get
                Return m_Icon
            End Get
            Set(ByVal value As String)
                m_Icon = value
            End Set
        End Property

        <StoredProcParameter("@DisplaySeq")> _
        Public Property _DisplaySeq() As Long
            Get
                Return m_DisplaySeq
            End Get
            Set(ByVal value As Long)
                m_DisplaySeq = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As AppModule

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getAppModulebyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of AppModule)

            Dim _AppModuleList As List(Of AppModule)
            _AppModuleList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getAppModuleTable"))
            Return _AppModuleList

        End Function

        Public Shared Sub SaveAll(ByVal AppModuleList As List(Of AppModule))

            For Each _AppModule As AppModule In AppModuleList
                SaveRecord(_AppModule)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal AppModule As AppModule) As Guid
            DAL.SaveRecord(Session.ConnectionString, AppModule, "upsertAppModule")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteAppModulebyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertAppModule")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As AppModule

            Dim _A As AppModule = Nothing

            If DR IsNot Nothing Then
                _A = New AppModule()
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("ID"))
                    _A._Caption = DR("caption").ToString.Trim
                    _A._Tooltip = DR("tooltip").ToString.Trim
                    _A._Icon = DR("icon").ToString.Trim
                    _A._DisplaySeq = GetLong(DR("display_seq"))

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of AppModule)

            Dim _AppModuleList As New List(Of AppModule)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _AppModuleList.Add(PropertiesFromData(_DR))
            Next

            Return _AppModuleList

        End Function


#End Region

    End Class

End Namespace
