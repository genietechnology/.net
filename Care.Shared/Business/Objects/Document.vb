﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Document
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_KeyId As Guid?
        Dim m_Type As String
        Dim m_Subject As String
        Dim m_FileType As String
        Dim m_FileExt As String
        Dim m_FileSize As Long
        Dim m_FileName As String
        Dim m_FilePath As String
        Dim m_Private As Boolean
        Dim m_Deleted As Boolean
        Dim m_Category As String
        Dim m_SubCategory As String
        Dim m_Tags As String
        Dim m_Notes As String
        Dim m_UserId As Guid?
        Dim m_UserName As String
        Dim m_Stamp As Date?
        Dim m_Data As Byte()

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._KeyId = Nothing
                ._Type = ""
                ._Subject = ""
                ._FileType = ""
                ._FileExt = ""
                ._FileSize = 0
                ._FileName = ""
                ._FilePath = ""
                ._Private = False
                ._Deleted = False
                ._Category = ""
                ._SubCategory = ""
                ._Tags = ""
                ._Notes = ""
                ._UserId = Nothing
                ._UserName = ""
                ._Stamp = Nothing
                ._Data = Nothing

            End With

        End Sub

        Public Sub New(ByVal ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._KeyId = Nothing
                ._Type = ""
                ._Subject = ""
                ._FileType = ""
                ._FileExt = ""
                ._FileSize = 0
                ._FileName = ""
                ._FilePath = ""
                ._Private = False
                ._Deleted = False
                ._Category = ""
                ._SubCategory = ""
                ._Tags = ""
                ._Notes = ""
                ._UserId = Nothing
                ._UserName = ""
                ._Stamp = Nothing
                ._Data = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@KeyId")> _
        Public Property _KeyId() As Guid?
            Get
                Return m_KeyId
            End Get
            Set(ByVal value As Guid?)
                m_KeyId = value
            End Set
        End Property

        <StoredProcParameter("@Type")> _
        Public Property _Type() As String
            Get
                Return m_Type
            End Get
            Set(ByVal value As String)
                m_Type = value
            End Set
        End Property

        <StoredProcParameter("@Subject")> _
        Public Property _Subject() As String
            Get
                Return m_Subject
            End Get
            Set(ByVal value As String)
                m_Subject = value
            End Set
        End Property

        <StoredProcParameter("@FileType")> _
        Public Property _FileType() As String
            Get
                Return m_FileType
            End Get
            Set(ByVal value As String)
                m_FileType = value
            End Set
        End Property

        <StoredProcParameter("@FileExt")> _
        Public Property _FileExt() As String
            Get
                Return m_FileExt
            End Get
            Set(ByVal value As String)
                m_FileExt = value
            End Set
        End Property

        <StoredProcParameter("@FileSize")> _
        Public Property _FileSize() As Long
            Get
                Return m_FileSize
            End Get
            Set(ByVal value As Long)
                m_FileSize = value
            End Set
        End Property

        <StoredProcParameter("@FileName")> _
        Public Property _FileName() As String
            Get
                Return m_FileName
            End Get
            Set(ByVal value As String)
                m_FileName = value
            End Set
        End Property

        <StoredProcParameter("@FilePath")> _
        Public Property _FilePath() As String
            Get
                Return m_FilePath
            End Get
            Set(ByVal value As String)
                m_FilePath = value
            End Set
        End Property

        <StoredProcParameter("@Private")> _
        Public Property _Private() As Boolean
            Get
                Return m_Private
            End Get
            Set(ByVal value As Boolean)
                m_Private = value
            End Set
        End Property

        <StoredProcParameter("@Deleted")> _
        Public Property _Deleted() As Boolean
            Get
                Return m_Deleted
            End Get
            Set(ByVal value As Boolean)
                m_Deleted = value
            End Set
        End Property

        <StoredProcParameter("@Category")> _
        Public Property _Category() As String
            Get
                Return m_Category
            End Get
            Set(ByVal value As String)
                m_Category = value
            End Set
        End Property

        <StoredProcParameter("@SubCategory")> _
        Public Property _SubCategory() As String
            Get
                Return m_SubCategory
            End Get
            Set(ByVal value As String)
                m_SubCategory = value
            End Set
        End Property

        <StoredProcParameter("@Tags")> _
        Public Property _Tags() As String
            Get
                Return m_Tags
            End Get
            Set(ByVal value As String)
                m_Tags = value
            End Set
        End Property

        <StoredProcParameter("@Notes")> _
        Public Property _Notes() As String
            Get
                Return m_Notes
            End Get
            Set(ByVal value As String)
                m_Notes = value
            End Set
        End Property

        <StoredProcParameter("@UserId")> _
        Public Property _UserId() As Guid?
            Get
                Return m_UserId
            End Get
            Set(ByVal value As Guid?)
                m_UserId = value
            End Set
        End Property

        <StoredProcParameter("@UserName")> _
        Public Property _UserName() As String
            Get
                Return m_UserName
            End Get
            Set(ByVal value As String)
                m_UserName = value
            End Set
        End Property

        <StoredProcParameter("@Stamp")> _
        Public Property _Stamp() As Date?
            Get
                Return m_Stamp
            End Get
            Set(ByVal value As Date?)
                m_Stamp = value
            End Set
        End Property

        <StoredProcParameter("@Data")> _
        Public Property _Data() As Byte()
            Get
                Return m_Data
            End Get
            Set(ByVal value As Byte())
                m_Data = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Document

            Dim _Document As Document
            _Document = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getDocumentbyID", ID))
            Return _Document

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Document

            Dim _Document As Document
            _Document = PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getDocumentbyID", ID))
            Return _Document

        End Function

        Public Shared Function RetreiveAll() As List(Of Document)

            Dim _DocumentList As List(Of Document)
            _DocumentList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getDocumentTable"))
            Return _DocumentList

        End Function

        Public Shared Sub SaveAll(ByVal DocumentList As List(Of Document))

            For Each _Document As Document In DocumentList
                SaveRecord(_Document)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Document As Document) As Guid
            DAL.SaveRecord(Session.ConnectionString, Document, "upsertDocument")
        End Function

        Public Shared Function SaveRecord(ByVal Document As Document) As Guid
            DAL.SaveRecord(Session.ConnectionString, Document, "upsertDocument")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteDocumentbyID", ID)
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Document

            Dim _A As New Document()

            If DR IsNot Nothing Then
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("ID"))
                    _A._KeyId = GetGUID(DR("key_id"))
                    _A._Type = DR("type").ToString.Trim
                    _A._Subject = DR("subject").ToString.Trim
                    _A._FileType = DR("file_type").ToString.Trim
                    _A._FileExt = DR("file_ext").ToString.Trim
                    _A._FileSize = GetLong(DR("file_size"))
                    _A._FileName = DR("file_name").ToString.Trim
                    _A._FilePath = DR("file_path").ToString.Trim
                    _A._Private = GetBoolean(DR("private"))
                    _A._Deleted = GetBoolean(DR("deleted"))
                    _A._Category = DR("category").ToString.Trim
                    _A._SubCategory = DR("sub_category").ToString.Trim
                    _A._Tags = DR("tags").ToString.Trim
                    _A._Notes = DR("notes").ToString.Trim
                    _A._UserId = GetGUID(DR("user_id"))
                    _A._UserName = DR("user_name").ToString.Trim
                    _A._Stamp = GetDate(DR("stamp"))
                    _A._Data = GetImage(DR("data"))

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Document)

            Dim _DocumentList As New List(Of Document)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _DocumentList.Add(PropertiesFromData(_DR))
            Next

            Return _DocumentList

        End Function


#End Region

    End Class

End Namespace
