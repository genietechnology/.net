﻿'*****************************************************
'Generated 07/06/2015 19:27:46 using Version 1.15.5.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class ActivityCRM
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_LinkId As Guid?
        Dim m_LinkType As String
        Dim m_ContactId As Guid?
        Dim m_ContactName As String
        Dim m_ActionDate As DateTime?
        Dim m_ActionType As String
        Dim m_Direction As String
        Dim m_Subject As String
        Dim m_Notes As String
        Dim m_Fup As Boolean
        Dim m_FupDate As DateTime?
        Dim m_System As Boolean
        Dim m_AppointmentId As Guid?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._LinkId = Nothing
                ._LinkType = ""
                ._ContactId = Nothing
                ._ContactName = ""
                ._ActionDate = Nothing
                ._ActionType = ""
                ._Direction = ""
                ._Subject = ""
                ._Notes = ""
                ._Fup = False
                ._FupDate = Nothing
                ._System = False
                ._AppointmentId = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._LinkId = Nothing
                ._LinkType = ""
                ._ContactId = Nothing
                ._ContactName = ""
                ._ActionDate = Nothing
                ._ActionType = ""
                ._Direction = ""
                ._Subject = ""
                ._Notes = ""
                ._Fup = False
                ._FupDate = Nothing
                ._System = False
                ._AppointmentId = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@LinkId")> _
        Public Property _LinkId() As Guid?
            Get
                Return m_LinkId
            End Get
            Set(ByVal value As Guid?)
                m_LinkId = value
            End Set
        End Property

        <StoredProcParameter("@LinkType")> _
        Public Property _LinkType() As String
            Get
                Return m_LinkType
            End Get
            Set(ByVal value As String)
                m_LinkType = value
            End Set
        End Property

        <StoredProcParameter("@ContactId")> _
        Public Property _ContactId() As Guid?
            Get
                Return m_ContactId
            End Get
            Set(ByVal value As Guid?)
                m_ContactId = value
            End Set
        End Property

        <StoredProcParameter("@ContactName")> _
        Public Property _ContactName() As String
            Get
                Return m_ContactName
            End Get
            Set(ByVal value As String)
                m_ContactName = value
            End Set
        End Property

        <StoredProcParameter("@ActionDate")> _
        Public Property _ActionDate() As DateTime?
            Get
                Return m_ActionDate
            End Get
            Set(ByVal value As DateTime?)
                m_ActionDate = value
            End Set
        End Property

        <StoredProcParameter("@ActionType")> _
        Public Property _ActionType() As String
            Get
                Return m_ActionType
            End Get
            Set(ByVal value As String)
                m_ActionType = value
            End Set
        End Property

        <StoredProcParameter("@Direction")> _
        Public Property _Direction() As String
            Get
                Return m_Direction
            End Get
            Set(ByVal value As String)
                m_Direction = value
            End Set
        End Property

        <StoredProcParameter("@Subject")> _
        Public Property _Subject() As String
            Get
                Return m_Subject
            End Get
            Set(ByVal value As String)
                m_Subject = value
            End Set
        End Property

        <StoredProcParameter("@Notes")> _
        Public Property _Notes() As String
            Get
                Return m_Notes
            End Get
            Set(ByVal value As String)
                m_Notes = value
            End Set
        End Property

        <StoredProcParameter("@Fup")> _
        Public Property _Fup() As Boolean
            Get
                Return m_Fup
            End Get
            Set(ByVal value As Boolean)
                m_Fup = value
            End Set
        End Property

        <StoredProcParameter("@FupDate")> _
        Public Property _FupDate() As DateTime?
            Get
                Return m_FupDate
            End Get
            Set(ByVal value As DateTime?)
                m_FupDate = value
            End Set
        End Property

        <StoredProcParameter("@System")> _
        Public Property _System() As Boolean
            Get
                Return m_System
            End Get
            Set(ByVal value As Boolean)
                m_System = value
            End Set
        End Property

        <StoredProcParameter("@AppointmentId")> _
        Public Property _AppointmentId() As Guid?
            Get
                Return m_AppointmentId
            End Get
            Set(ByVal value As Guid?)
                m_AppointmentId = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As ActivityCRM

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getActivityCRMbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of ActivityCRM)

            Dim _ActivityCRMList As List(Of ActivityCRM)
            _ActivityCRMList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getActivityCRMTable"))
            Return _ActivityCRMList

        End Function

        Public Shared Sub SaveAll(ByVal ActivityCRMList As List(Of ActivityCRM))

            For Each _ActivityCRM As ActivityCRM In ActivityCRMList
                SaveRecord(_ActivityCRM)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal ActivityCRM As ActivityCRM) As Guid
            DAL.SaveRecord(Session.ConnectionString, ActivityCRM, "upsertActivityCRM")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteActivityCRMbyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertActivityCRM")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As ActivityCRM

            Dim _A As ActivityCRM = Nothing

            If DR IsNot Nothing Then
                _A = New ActivityCRM()
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("ID"))
                    _A._LinkId = GetGUID(DR("link_id"))
                    _A._LinkType = DR("link_type").ToString.Trim
                    _A._ContactId = GetGUID(DR("contact_id"))
                    _A._ContactName = DR("contact_name").ToString.Trim
                    _A._ActionDate = GetDateTime(DR("action_date"))
                    _A._ActionType = DR("action_type").ToString.Trim
                    _A._Direction = DR("direction").ToString.Trim
                    _A._Subject = DR("subject").ToString.Trim
                    _A._Notes = DR("notes").ToString.Trim
                    _A._Fup = GetBoolean(DR("fup"))
                    _A._FupDate = GetDateTime(DR("fup_date"))
                    _A._System = GetBoolean(DR("system"))
                    _A._AppointmentId = GetGUID(DR("appointment_id"))

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of ActivityCRM)

            Dim _ActivityCRMList As New List(Of ActivityCRM)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _ActivityCRMList.Add(PropertiesFromData(_DR))
            Next

            Return _ActivityCRMList

        End Function


#End Region

    End Class

End Namespace
