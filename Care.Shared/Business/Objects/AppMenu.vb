﻿'*****************************************************
'Generated 03/05/2015 14:25:15 using Version 1.15.5.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class AppMenu
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_ModuleId As Guid?
        Dim m_Caption As String
        Dim m_Tooltip As String
        Dim m_DisplaySeq As Long

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._ModuleId = Nothing
                ._Caption = ""
                ._Tooltip = ""
                ._DisplaySeq = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._ModuleId = Nothing
                ._Caption = ""
                ._Tooltip = ""
                ._DisplaySeq = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@ModuleId")> _
        Public Property _ModuleId() As Guid?
            Get
                Return m_ModuleId
            End Get
            Set(ByVal value As Guid?)
                m_ModuleId = value
            End Set
        End Property

        <StoredProcParameter("@Caption")> _
        Public Property _Caption() As String
            Get
                Return m_Caption
            End Get
            Set(ByVal value As String)
                m_Caption = value
            End Set
        End Property

        <StoredProcParameter("@Tooltip")> _
        Public Property _Tooltip() As String
            Get
                Return m_Tooltip
            End Get
            Set(ByVal value As String)
                m_Tooltip = value
            End Set
        End Property

        <StoredProcParameter("@DisplaySeq")> _
        Public Property _DisplaySeq() As Long
            Get
                Return m_DisplaySeq
            End Get
            Set(ByVal value As Long)
                m_DisplaySeq = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As AppMenu

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getAppMenubyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of AppMenu)

            Dim _AppMenuList As List(Of AppMenu)
            _AppMenuList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getAppMenuTable"))
            Return _AppMenuList

        End Function

        Public Shared Sub SaveAll(ByVal AppMenuList As List(Of AppMenu))

            For Each _AppMenu As AppMenu In AppMenuList
                SaveRecord(_AppMenu)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal AppMenu As AppMenu) As Guid
            DAL.SaveRecord(Session.ConnectionString, AppMenu, "upsertAppMenu")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteAppMenubyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertAppMenu")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As AppMenu

            Dim _A As AppMenu = Nothing

            If DR IsNot Nothing Then
                _A = New AppMenu()
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("ID"))
                    _A._ModuleId = GetGUID(DR("module_id"))
                    _A._Caption = DR("caption").ToString.Trim
                    _A._Tooltip = DR("tooltip").ToString.Trim
                    _A._DisplaySeq = GetLong(DR("display_seq"))

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of AppMenu)

            Dim _AppMenuList As New List(Of AppMenu)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _AppMenuList.Add(PropertiesFromData(_DR))
            Next

            Return _AppMenuList

        End Function


#End Region

    End Class

End Namespace
