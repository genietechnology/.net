﻿'*****************************************************
'Generated 10/06/2016 17:59:28 using Version 1.16.6.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class AppReportClause
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_ReportId As Guid?
        Dim m_ClauseLabel As String
        Dim m_ClauseTooltip As String
        Dim m_ClauseMandatory As Boolean
        Dim m_ClauseType As String
        Dim m_ClauseField1 As String
        Dim m_ClauseField2 As String
        Dim m_ClauseDatatype As String
        Dim m_ClauseOperator As String
        Dim m_ClauseLookup As String
        Dim m_ClauseSeq As Integer

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._ReportId = Nothing
                ._ClauseLabel = ""
                ._ClauseTooltip = ""
                ._ClauseMandatory = False
                ._ClauseType = ""
                ._ClauseField1 = ""
                ._ClauseField2 = ""
                ._ClauseDatatype = ""
                ._ClauseOperator = ""
                ._ClauseLookup = ""
                ._ClauseSeq = 0

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._ReportId = Nothing
                ._ClauseLabel = ""
                ._ClauseTooltip = ""
                ._ClauseMandatory = False
                ._ClauseType = ""
                ._ClauseField1 = ""
                ._ClauseField2 = ""
                ._ClauseDatatype = ""
                ._ClauseOperator = ""
                ._ClauseLookup = ""
                ._ClauseSeq = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@ReportId")> _
        Public Property _ReportId() As Guid?
            Get
                Return m_ReportId
            End Get
            Set(ByVal value As Guid?)
                m_ReportId = value
            End Set
        End Property

        <StoredProcParameter("@ClauseLabel")> _
        Public Property _ClauseLabel() As String
            Get
                Return m_ClauseLabel
            End Get
            Set(ByVal value As String)
                m_ClauseLabel = value
            End Set
        End Property

        <StoredProcParameter("@ClauseTooltip")> _
        Public Property _ClauseTooltip() As String
            Get
                Return m_ClauseTooltip
            End Get
            Set(ByVal value As String)
                m_ClauseTooltip = value
            End Set
        End Property

        <StoredProcParameter("@ClauseMandatory")> _
        Public Property _ClauseMandatory() As Boolean
            Get
                Return m_ClauseMandatory
            End Get
            Set(ByVal value As Boolean)
                m_ClauseMandatory = value
            End Set
        End Property

        <StoredProcParameter("@ClauseType")> _
        Public Property _ClauseType() As String
            Get
                Return m_ClauseType
            End Get
            Set(ByVal value As String)
                m_ClauseType = value
            End Set
        End Property

        <StoredProcParameter("@ClauseField1")> _
        Public Property _ClauseField1() As String
            Get
                Return m_ClauseField1
            End Get
            Set(ByVal value As String)
                m_ClauseField1 = value
            End Set
        End Property

        <StoredProcParameter("@ClauseField2")> _
        Public Property _ClauseField2() As String
            Get
                Return m_ClauseField2
            End Get
            Set(ByVal value As String)
                m_ClauseField2 = value
            End Set
        End Property

        <StoredProcParameter("@ClauseDatatype")> _
        Public Property _ClauseDatatype() As String
            Get
                Return m_ClauseDatatype
            End Get
            Set(ByVal value As String)
                m_ClauseDatatype = value
            End Set
        End Property

        <StoredProcParameter("@ClauseOperator")> _
        Public Property _ClauseOperator() As String
            Get
                Return m_ClauseOperator
            End Get
            Set(ByVal value As String)
                m_ClauseOperator = value
            End Set
        End Property

        <StoredProcParameter("@ClauseLookup")> _
        Public Property _ClauseLookup() As String
            Get
                Return m_ClauseLookup
            End Get
            Set(ByVal value As String)
                m_ClauseLookup = value
            End Set
        End Property

        <StoredProcParameter("@ClauseSeq")> _
        Public Property _ClauseSeq() As Integer
            Get
                Return m_ClauseSeq
            End Get
            Set(ByVal value As Integer)
                m_ClauseSeq = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As AppReportClause

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getAppReportClausebyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of AppReportClause)

            Dim _AppReportClauseList As List(Of AppReportClause)
            _AppReportClauseList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getAppReportClauseTable"))
            Return _AppReportClauseList

        End Function

        Public Shared Sub SaveAll(ByVal AppReportClauseList As List(Of AppReportClause))

            For Each _AppReportClause As AppReportClause In AppReportClauseList
                SaveRecord(_AppReportClause)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal AppReportClause As AppReportClause) As Guid
            AppReportClause.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, AppReportClause, "upsertAppReportClause")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteAppReportClausebyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertAppReportClause")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As AppReportClause

            Dim _A As AppReportClause = Nothing

            If DR IsNot Nothing Then
                _A = New AppReportClause()
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("ID"))
                    _A._ReportId = GetGUID(DR("report_id"))
                    _A._ClauseLabel = DR("clause_label").ToString.Trim
                    _A._ClauseTooltip = DR("clause_tooltip").ToString.Trim
                    _A._ClauseMandatory = GetBoolean(DR("clause_mandatory"))
                    _A._ClauseType = DR("clause_type").ToString.Trim
                    _A._ClauseField1 = DR("clause_field1").ToString.Trim
                    _A._ClauseField2 = DR("clause_field2").ToString.Trim
                    _A._ClauseDatatype = DR("clause_datatype").ToString.Trim
                    _A._ClauseOperator = DR("clause_operator").ToString.Trim
                    _A._ClauseLookup = DR("clause_lookup").ToString.Trim
                    _A._ClauseSeq = GetInteger(DR("clause_seq"))

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of AppReportClause)

            Dim _AppReportClauseList As New List(Of AppReportClause)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _AppReportClauseList.Add(PropertiesFromData(_DR))
            Next

            Return _AppReportClauseList

        End Function


#End Region

    End Class

End Namespace
