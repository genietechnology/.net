﻿'*****************************************************
'Generated 25/09/2018 23:13:11 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Parameter
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Name = ""
                ._Description = ""
                ._Type = ""
                ._Value = ""
                ._Parent = ""
                ._Readonly = False

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Name = ""
                ._Description = ""
                ._Type = ""
                ._Value = ""
                ._Parent = ""
                ._Readonly = False

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@Name")>
        Public Property _Name() As String

        <StoredProcParameter("@Description")>
        Public Property _Description() As String

        <StoredProcParameter("@Type")>
        Public Property _Type() As String

        <StoredProcParameter("@Value")>
        Public Property _Value() As String

        <StoredProcParameter("@Parent")>
        Public Property _Parent() As String

        <StoredProcParameter("@Readonly")>
        Public Property _Readonly() As Boolean


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Parameter
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getParameterbyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As Parameter
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getParameterbyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of Parameter)
            Dim _ParameterList As List(Of Parameter)
            _ParameterList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getParameterTable"))
            Return _ParameterList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of Parameter)
            Dim _ParameterList As List(Of Parameter)
            _ParameterList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getParameterTable"))
            Return _ParameterList
        End Function

        Public Shared Sub SaveAll(ByVal ParameterList As List(Of Parameter))
            For Each _Parameter As Parameter In ParameterList
                SaveRecord(_Parameter)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal ParameterList As List(Of Parameter))
            For Each _Parameter As Parameter In ParameterList
                SaveRecord(ConnectionString, _Parameter)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal Parameter As Parameter) As Guid
            Dim _Current As Parameter = RetreiveByID(Parameter._ID.Value)
            Parameter.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Parameter, _Current, "upsertParameter")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal Parameter As Parameter) As Guid
            Dim _Current As Parameter = RetreiveByID(ConnectionString, Parameter._ID.Value)
            Parameter.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Parameter, _Current, "upsertParameter")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As Parameter = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteParameterbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As Parameter = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteParameterbyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As Parameter = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertParameter")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As Parameter = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertParameter")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Parameter

            Dim _A As Parameter = Nothing

            If DR IsNot Nothing Then
                _A = New Parameter()
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("ID"))
                    _A._Name = DR("name").ToString.Trim
                    _A._Description = DR("description").ToString.Trim
                    _A._Type = DR("type").ToString.Trim
                    _A._Value = DR("value").ToString.Trim
                    _A._Parent = DR("parent").ToString.Trim
                    _A._Readonly = GetBoolean(DR("readonly"))

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Parameter)

            Dim _ParameterList As New List(Of Parameter)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _ParameterList.Add(PropertiesFromData(_DR))
            Next

            Return _ParameterList

        End Function


#End Region

    End Class

End Namespace
