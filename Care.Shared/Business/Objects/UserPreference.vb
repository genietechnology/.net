﻿'*****************************************************
'Generated 03/05/2015 12:51:53 using Version 1.15.5.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class UserPreference
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_UserId As Guid?
        Dim m_Name As String
        Dim m_Type As String
        Dim m_Value As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._UserId = Nothing
                ._Name = ""
                ._Type = ""
                ._Value = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._UserId = Nothing
                ._Name = ""
                ._Type = ""
                ._Value = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@UserId")> _
        Public Property _UserId() As Guid?
            Get
                Return m_UserId
            End Get
            Set(ByVal value As Guid?)
                m_UserId = value
            End Set
        End Property

        <StoredProcParameter("@Name")> _
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@Type")> _
        Public Property _Type() As String
            Get
                Return m_Type
            End Get
            Set(ByVal value As String)
                m_Type = value
            End Set
        End Property

        <StoredProcParameter("@Value")> _
        Public Property _Value() As String
            Get
                Return m_Value
            End Get
            Set(ByVal value As String)
                m_Value = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As UserPreference

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getUserPreferencebyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of UserPreference)

            Dim _UserPreferenceList As List(Of UserPreference)
            _UserPreferenceList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getUserPreferenceTable"))
            Return _UserPreferenceList

        End Function

        Public Shared Sub SaveAll(ByVal UserPreferenceList As List(Of UserPreference))

            For Each _UserPreference As UserPreference In UserPreferenceList
                SaveRecord(_UserPreference)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal UserPreference As UserPreference) As Guid
            DAL.SaveRecord(Session.ConnectionString, UserPreference, "upsertUserPreference")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteUserPreferencebyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertUserPreference")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As UserPreference

            Dim _A As UserPreference = Nothing

            If DR IsNot Nothing Then
                _A = New UserPreference()
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("ID"))
                    _A._UserId = GetGUID(DR("user_id"))
                    _A._Name = DR("name").ToString.Trim
                    _A._Type = DR("type").ToString.Trim
                    _A._Value = DR("value").ToString.Trim

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of UserPreference)

            Dim _UserPreferenceList As New List(Of UserPreference)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _UserPreferenceList.Add(PropertiesFromData(_DR))
            Next

            Return _UserPreferenceList

        End Function


#End Region

    End Class

End Namespace
