﻿'*****************************************************
'Generated 04/04/2017 21:07:53 using Version 1.17.2.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class AppNote
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_RecordId As Guid?
        Dim m_Subject As String
        Dim m_Body As String
        Dim m_UserId As Guid?
        Dim m_Stamp As DateTime?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._RecordId = Nothing
                ._Subject = ""
                ._Body = ""
                ._UserId = Nothing
                ._Stamp = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._RecordId = Nothing
                ._Subject = ""
                ._Body = ""
                ._UserId = Nothing
                ._Stamp = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@RecordId")> _
        Public Property _RecordId() As Guid?
            Get
                Return m_RecordId
            End Get
            Set(ByVal value As Guid?)
                m_RecordId = value
            End Set
        End Property

        <StoredProcParameter("@Subject")> _
        Public Property _Subject() As String
            Get
                Return m_Subject
            End Get
            Set(ByVal value As String)
                m_Subject = value
            End Set
        End Property

        <StoredProcParameter("@Body")> _
        Public Property _Body() As String
            Get
                Return m_Body
            End Get
            Set(ByVal value As String)
                m_Body = value
            End Set
        End Property

        <StoredProcParameter("@UserId")> _
        Public Property _UserId() As Guid?
            Get
                Return m_UserId
            End Get
            Set(ByVal value As Guid?)
                m_UserId = value
            End Set
        End Property

        <StoredProcParameter("@Stamp")> _
        Public Property _Stamp() As DateTime?
            Get
                Return m_Stamp
            End Get
            Set(ByVal value As DateTime?)
                m_Stamp = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As AppNote

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getAppNotebyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As AppNote

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getAppNotebyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of AppNote)

            Dim _AppNoteList As List(Of AppNote)
            _AppNoteList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getAppNoteTable"))
            Return _AppNoteList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of AppNote)

            Dim _AppNoteList As List(Of AppNote)
            _AppNoteList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getAppNoteTable"))
            Return _AppNoteList

        End Function

        Public Shared Sub SaveAll(ByVal AppNoteList As List(Of AppNote))

            For Each _AppNote As AppNote In AppNoteList
                SaveRecord(_AppNote)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal AppNoteList As List(Of AppNote))

            For Each _AppNote As AppNote In AppNoteList
                SaveRecord(ConnectionString, _AppNote)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal AppNote As AppNote) As Guid
            AppNote.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, AppNote, "upsertAppNote")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal AppNote As AppNote) As Guid
            AppNote.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, AppNote, "upsertAppNote")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteAppNotebyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteAppNotebyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertAppNote")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertAppNote")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As AppNote

            Dim _A As AppNote = Nothing

            If DR IsNot Nothing Then
                _A = New AppNote()
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("ID"))
                    _A._RecordId = GetGUID(DR("record_id"))
                    _A._Subject = DR("subject").ToString.Trim
                    _A._Body = DR("body").ToString.Trim
                    _A._UserId = GetGUID(DR("user_id"))
                    _A._Stamp = GetDateTime(DR("stamp"))

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of AppNote)

            Dim _AppNoteList As New List(Of AppNote)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _AppNoteList.Add(PropertiesFromData(_DR))
            Next

            Return _AppNoteList

        End Function


#End Region

    End Class

End Namespace
