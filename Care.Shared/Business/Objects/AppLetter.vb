﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class AppLetter
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Code As String
        Dim m_Name As String
        Dim m_SourceFile As String
        Dim m_OutputFile As String
        Dim m_OutputType As String
        Dim m_Multi As Boolean
        Dim m_MultiFilenameAppend As String
        Dim m_SqlSelect As String
        Dim m_SqlWhere As String
        Dim m_SqlOrder As String
        Dim m_Invu As Boolean
        Dim m_InvuCsvPath As String
        Dim m_InvuDocPath As String
        Dim m_InvuFields As String
        Dim m_Adhoc As Boolean
        Dim m_Type As String
        Dim m_QueryId As Guid?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Code = ""
                ._Name = ""
                ._SourceFile = ""
                ._OutputFile = ""
                ._OutputType = ""
                ._Multi = False
                ._MultiFilenameAppend = ""
                ._SqlSelect = ""
                ._SqlWhere = ""
                ._SqlOrder = ""
                ._Invu = False
                ._InvuCsvPath = ""
                ._InvuDocPath = ""
                ._InvuFields = ""
                ._Adhoc = False
                ._Type = ""
                ._QueryId = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Code = ""
                ._Name = ""
                ._SourceFile = ""
                ._OutputFile = ""
                ._OutputType = ""
                ._Multi = False
                ._MultiFilenameAppend = ""
                ._SqlSelect = ""
                ._SqlWhere = ""
                ._SqlOrder = ""
                ._Invu = False
                ._InvuCsvPath = ""
                ._InvuDocPath = ""
                ._InvuFields = ""
                ._Adhoc = False
                ._Type = ""
                ._QueryId = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Code")> _
        Public Property _Code() As String
            Get
                Return m_Code
            End Get
            Set(ByVal value As String)
                m_Code = value
            End Set
        End Property

        <StoredProcParameter("@Name")> _
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@SourceFile")> _
        Public Property _SourceFile() As String
            Get
                Return m_SourceFile
            End Get
            Set(ByVal value As String)
                m_SourceFile = value
            End Set
        End Property

        <StoredProcParameter("@OutputFile")> _
        Public Property _OutputFile() As String
            Get
                Return m_OutputFile
            End Get
            Set(ByVal value As String)
                m_OutputFile = value
            End Set
        End Property

        <StoredProcParameter("@OutputType")> _
        Public Property _OutputType() As String
            Get
                Return m_OutputType
            End Get
            Set(ByVal value As String)
                m_OutputType = value
            End Set
        End Property

        <StoredProcParameter("@Multi")> _
        Public Property _Multi() As Boolean
            Get
                Return m_Multi
            End Get
            Set(ByVal value As Boolean)
                m_Multi = value
            End Set
        End Property

        <StoredProcParameter("@MultiFilenameAppend")> _
        Public Property _MultiFilenameAppend() As String
            Get
                Return m_MultiFilenameAppend
            End Get
            Set(ByVal value As String)
                m_MultiFilenameAppend = value
            End Set
        End Property

        <StoredProcParameter("@SqlSelect")> _
        Public Property _SqlSelect() As String
            Get
                Return m_SqlSelect
            End Get
            Set(ByVal value As String)
                m_SqlSelect = value
            End Set
        End Property

        <StoredProcParameter("@SqlWhere")> _
        Public Property _SqlWhere() As String
            Get
                Return m_SqlWhere
            End Get
            Set(ByVal value As String)
                m_SqlWhere = value
            End Set
        End Property

        <StoredProcParameter("@SqlOrder")> _
        Public Property _SqlOrder() As String
            Get
                Return m_SqlOrder
            End Get
            Set(ByVal value As String)
                m_SqlOrder = value
            End Set
        End Property

        <StoredProcParameter("@Invu")> _
        Public Property _Invu() As Boolean
            Get
                Return m_Invu
            End Get
            Set(ByVal value As Boolean)
                m_Invu = value
            End Set
        End Property

        <StoredProcParameter("@InvuCsvPath")> _
        Public Property _InvuCsvPath() As String
            Get
                Return m_InvuCsvPath
            End Get
            Set(ByVal value As String)
                m_InvuCsvPath = value
            End Set
        End Property

        <StoredProcParameter("@InvuDocPath")> _
        Public Property _InvuDocPath() As String
            Get
                Return m_InvuDocPath
            End Get
            Set(ByVal value As String)
                m_InvuDocPath = value
            End Set
        End Property

        <StoredProcParameter("@InvuFields")> _
        Public Property _InvuFields() As String
            Get
                Return m_InvuFields
            End Get
            Set(ByVal value As String)
                m_InvuFields = value
            End Set
        End Property

        <StoredProcParameter("@Adhoc")> _
        Public Property _Adhoc() As Boolean
            Get
                Return m_Adhoc
            End Get
            Set(ByVal value As Boolean)
                m_Adhoc = value
            End Set
        End Property

        <StoredProcParameter("@Type")> _
        Public Property _Type() As String
            Get
                Return m_Type
            End Get
            Set(ByVal value As String)
                m_Type = value
            End Set
        End Property

        <StoredProcParameter("@QueryId")> _
        Public Property _QueryId() As Guid?
            Get
                Return m_QueryId
            End Get
            Set(ByVal value As Guid?)
                m_QueryId = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As AppLetter

            Dim _AppLetter As AppLetter
            _AppLetter = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getAppLetterbyID", ID))
            Return _AppLetter

        End Function

        Public Shared Function RetreiveAll() As List(Of AppLetter)

            Dim _AppLetterList As List(Of AppLetter)
            _AppLetterList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getAppLetterTable"))
            Return _AppLetterList

        End Function

        Public Shared Sub SaveAll(ByVal AppLetterList As List(Of AppLetter))

            For Each _AppLetter As AppLetter In AppLetterList
                SaveRecord(_AppLetter)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal AppLetter As AppLetter) As Guid
            DAL.SaveRecord(Session.ConnectionString, AppLetter, "upsertAppLetter")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteAppLetterbyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertAppLetter")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As AppLetter

            If DR Is Nothing Then Return Nothing

            Dim _A As New AppLetter()
            With DR
                _A.IsNew = False
                _A.IsDeleted = False
                _A._ID = GetGUID(DR("ID"))
                _A._Code = DR("code").ToString.Trim
                _A._Name = DR("name").ToString.Trim
                _A._SourceFile = DR("source_file").ToString.Trim
                _A._OutputFile = DR("output_file").ToString.Trim
                _A._OutputType = DR("output_type").ToString.Trim
                _A._Multi = GetBoolean(DR("multi"))
                _A._MultiFilenameAppend = DR("multi_filename_append").ToString.Trim
                _A._SqlSelect = DR("sql_select").ToString.Trim
                _A._SqlWhere = DR("sql_where").ToString.Trim
                _A._SqlOrder = DR("sql_order").ToString.Trim
                _A._Invu = GetBoolean(DR("invu"))
                _A._InvuCsvPath = DR("invu_csv_path").ToString.Trim
                _A._InvuDocPath = DR("invu_doc_path").ToString.Trim
                _A._InvuFields = DR("invu_fields").ToString.Trim
                _A._Adhoc = GetBoolean(DR("adhoc"))
                _A._Type = DR("type").ToString.Trim
                _A._QueryId = GetGUID(DR("query_id"))

            End With

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of AppLetter)

            Dim _AppLetterList As New List(Of AppLetter)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _AppLetterList.Add(PropertiesFromData(_DR))
            Next

            Return _AppLetterList

        End Function


#End Region

    End Class

End Namespace
