﻿'*****************************************************
'Generated 08/10/2017 18:40:51 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class AppControlCaption
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_FormName As String
        Dim m_ControlName As String
        Dim m_Caption As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._FormName = ""
                ._ControlName = ""
                ._Caption = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._FormName = ""
                ._ControlName = ""
                ._Caption = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@FormName")>
        Public Property _FormName() As String
            Get
                Return m_FormName
            End Get
            Set(ByVal value As String)
                m_FormName = value
            End Set
        End Property

        <StoredProcParameter("@ControlName")>
        Public Property _ControlName() As String
            Get
                Return m_ControlName
            End Get
            Set(ByVal value As String)
                m_ControlName = value
            End Set
        End Property

        <StoredProcParameter("@Caption")>
        Public Property _Caption() As String
            Get
                Return m_Caption
            End Get
            Set(ByVal value As String)
                m_Caption = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As AppControlCaption

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getAppControlCaptionbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As AppControlCaption

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getAppControlCaptionbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of AppControlCaption)

            Dim _AppControlCaptionList As List(Of AppControlCaption)
            _AppControlCaptionList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getAppControlCaptionTable"))
            Return _AppControlCaptionList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of AppControlCaption)

            Dim _AppControlCaptionList As List(Of AppControlCaption)
            _AppControlCaptionList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getAppControlCaptionTable"))
            Return _AppControlCaptionList

        End Function

        Public Shared Sub SaveAll(ByVal AppControlCaptionList As List(Of AppControlCaption))

            For Each _AppControlCaption As AppControlCaption In AppControlCaptionList
                SaveRecord(_AppControlCaption)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal AppControlCaptionList As List(Of AppControlCaption))

            For Each _AppControlCaption As AppControlCaption In AppControlCaptionList
                SaveRecord(ConnectionString, _AppControlCaption)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal AppControlCaption As AppControlCaption) As Guid
            AppControlCaption.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, AppControlCaption, "upsertAppControlCaption")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal AppControlCaption As AppControlCaption) As Guid
            AppControlCaption.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, AppControlCaption, "upsertAppControlCaption")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteAppControlCaptionbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteAppControlCaptionbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertAppControlCaption")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertAppControlCaption")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As AppControlCaption

            Dim _A As AppControlCaption = Nothing

            If DR IsNot Nothing Then
                _A = New AppControlCaption()
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("ID"))
                    _A._FormName = DR("form_name").ToString.Trim
                    _A._ControlName = DR("control_name").ToString.Trim
                    _A._Caption = DR("caption").ToString.Trim

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of AppControlCaption)

            Dim _AppControlCaptionList As New List(Of AppControlCaption)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _AppControlCaptionList.Add(PropertiesFromData(_DR))
            Next

            Return _AppControlCaptionList

        End Function


#End Region

    End Class

End Namespace
