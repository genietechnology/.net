﻿'*****************************************************
'Generated 03/05/2015 18:07:23 using Version 1.15.5.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class AppPerm
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_ParentId As Guid?
        Dim m_ItemId As Guid?
        Dim m_Noaccess As Boolean
        Dim m_Readonly As Boolean

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._ParentId = Nothing
                ._ItemId = Nothing
                ._Noaccess = False
                ._Readonly = False

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._ParentId = Nothing
                ._ItemId = Nothing
                ._Noaccess = False
                ._Readonly = False

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@ParentId")> _
        Public Property _ParentId() As Guid?
            Get
                Return m_ParentId
            End Get
            Set(ByVal value As Guid?)
                m_ParentId = value
            End Set
        End Property

        <StoredProcParameter("@ItemId")> _
        Public Property _ItemId() As Guid?
            Get
                Return m_ItemId
            End Get
            Set(ByVal value As Guid?)
                m_ItemId = value
            End Set
        End Property

        <StoredProcParameter("@Noaccess")> _
        Public Property _Noaccess() As Boolean
            Get
                Return m_Noaccess
            End Get
            Set(ByVal value As Boolean)
                m_Noaccess = value
            End Set
        End Property

        <StoredProcParameter("@Readonly")> _
        Public Property _Readonly() As Boolean
            Get
                Return m_Readonly
            End Get
            Set(ByVal value As Boolean)
                m_Readonly = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As AppPerm

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getAppPermbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of AppPerm)

            Dim _AppPermList As List(Of AppPerm)
            _AppPermList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getAppPermTable"))
            Return _AppPermList

        End Function

        Public Shared Sub SaveAll(ByVal AppPermList As List(Of AppPerm))

            For Each _AppPerm As AppPerm In AppPermList
                SaveRecord(_AppPerm)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal AppPerm As AppPerm) As Guid
            DAL.SaveRecord(Session.ConnectionString, AppPerm, "upsertAppPerm")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteAppPermbyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertAppPerm")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As AppPerm

            Dim _A As AppPerm = Nothing

            If DR IsNot Nothing Then
                _A = New AppPerm()
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("ID"))
                    _A._ParentId = GetGUID(DR("parent_id"))
                    _A._ItemId = GetGUID(DR("item_id"))
                    _A._Noaccess = GetBoolean(DR("noaccess"))
                    _A._Readonly = GetBoolean(DR("readonly"))

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of AppPerm)

            Dim _AppPermList As New List(Of AppPerm)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _AppPermList.Add(PropertiesFromData(_DR))
            Next

            Return _AppPermList

        End Function


#End Region

    End Class

End Namespace
