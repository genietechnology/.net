﻿'*****************************************************
'Generated 26/03/2016 12:20:44 using Version 1.15.8.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class AppTaskHistory
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_TaskId As Guid?
        Dim m_Result As Integer
        Dim m_Errors As String
        Dim m_Stamp As DateTime?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._TaskId = Nothing
                ._Result = 0
                ._Errors = ""
                ._Stamp = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._TaskId = Nothing
                ._Result = 0
                ._Errors = ""
                ._Stamp = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@TaskId")> _
        Public Property _TaskId() As Guid?
            Get
                Return m_TaskId
            End Get
            Set(ByVal value As Guid?)
                m_TaskId = value
            End Set
        End Property

        <StoredProcParameter("@Result")> _
        Public Property _Result() As Integer
            Get
                Return m_Result
            End Get
            Set(ByVal value As Integer)
                m_Result = value
            End Set
        End Property

        <StoredProcParameter("@Errors")> _
        Public Property _Errors() As String
            Get
                Return m_Errors
            End Get
            Set(ByVal value As String)
                m_Errors = value
            End Set
        End Property

        <StoredProcParameter("@Stamp")> _
        Public Property _Stamp() As DateTime?
            Get
                Return m_Stamp
            End Get
            Set(ByVal value As DateTime?)
                m_Stamp = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As AppTaskHistory

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getAppTaskHistorybyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of AppTaskHistory)

            Dim _AppTaskHistoryList As List(Of AppTaskHistory)
            _AppTaskHistoryList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getAppTaskHistoryTable"))
            Return _AppTaskHistoryList

        End Function

        Public Shared Sub SaveAll(ByVal AppTaskHistoryList As List(Of AppTaskHistory))

            For Each _AppTaskHistory As AppTaskHistory In AppTaskHistoryList
                SaveRecord(_AppTaskHistory)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal AppTaskHistory As AppTaskHistory) As Guid
            AppTaskHistory.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, AppTaskHistory, "upsertAppTaskHistory")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteAppTaskHistorybyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertAppTaskHistory")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As AppTaskHistory

            Dim _A As AppTaskHistory = Nothing

            If DR IsNot Nothing Then
                _A = New AppTaskHistory()
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("ID"))
                    _A._TaskId = GetGUID(DR("task_id"))
                    _A._Result = GetInteger(DR("result"))
                    _A._Errors = DR("errors").ToString.Trim
                    _A._Stamp = GetDateTime(DR("stamp"))

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of AppTaskHistory)

            Dim _AppTaskHistoryList As New List(Of AppTaskHistory)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _AppTaskHistoryList.Add(PropertiesFromData(_DR))
            Next

            Return _AppTaskHistoryList

        End Function


#End Region

    End Class

End Namespace
