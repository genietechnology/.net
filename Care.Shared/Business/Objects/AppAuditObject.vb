﻿'*****************************************************
'Generated 25/04/2018 21:28:52 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class AppAuditObject
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Scope As String
        Dim m_ObjectName As String
        Dim m_ObjectId As Guid?
        Dim m_ObjectDesc As String
        Dim m_StatusCode As Byte
        Dim m_Status As String
        Dim m_UserId As Guid?
        Dim m_UserName As String
        Dim m_UserType As String
        Dim m_ComputerName As String
        Dim m_LoginName As String
        Dim m_Stamp As DateTime?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Scope = ""
                ._ObjectName = ""
                ._ObjectId = Nothing
                ._ObjectDesc = ""
                ._StatusCode = Nothing
                ._Status = ""
                ._UserId = Nothing
                ._UserName = ""
                ._UserType = ""
                ._ComputerName = ""
                ._LoginName = ""
                ._Stamp = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Scope = ""
                ._ObjectName = ""
                ._ObjectId = Nothing
                ._ObjectDesc = ""
                ._StatusCode = Nothing
                ._Status = ""
                ._UserId = Nothing
                ._UserName = ""
                ._UserType = ""
                ._ComputerName = ""
                ._LoginName = ""
                ._Stamp = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Scope")>
        Public Property _Scope() As String
            Get
                Return m_Scope
            End Get
            Set(ByVal value As String)
                m_Scope = value
            End Set
        End Property

        <StoredProcParameter("@ObjectName")>
        Public Property _ObjectName() As String
            Get
                Return m_ObjectName
            End Get
            Set(ByVal value As String)
                m_ObjectName = value
            End Set
        End Property

        <StoredProcParameter("@ObjectId")>
        Public Property _ObjectId() As Guid?
            Get
                Return m_ObjectId
            End Get
            Set(ByVal value As Guid?)
                m_ObjectId = value
            End Set
        End Property

        <StoredProcParameter("@ObjectDesc")>
        Public Property _ObjectDesc() As String
            Get
                Return m_ObjectDesc
            End Get
            Set(ByVal value As String)
                m_ObjectDesc = value
            End Set
        End Property

        <StoredProcParameter("@StatusCode")>
        Public Property _StatusCode() As Byte
            Get
                Return m_StatusCode
            End Get
            Set(ByVal value As Byte)
                m_StatusCode = value
            End Set
        End Property

        <StoredProcParameter("@Status")>
        Public Property _Status() As String
            Get
                Return m_Status
            End Get
            Set(ByVal value As String)
                m_Status = value
            End Set
        End Property

        <StoredProcParameter("@UserId")>
        Public Property _UserId() As Guid?
            Get
                Return m_UserId
            End Get
            Set(ByVal value As Guid?)
                m_UserId = value
            End Set
        End Property

        <StoredProcParameter("@UserName")>
        Public Property _UserName() As String
            Get
                Return m_UserName
            End Get
            Set(ByVal value As String)
                m_UserName = value
            End Set
        End Property

        <StoredProcParameter("@UserType")>
        Public Property _UserType() As String
            Get
                Return m_UserType
            End Get
            Set(ByVal value As String)
                m_UserType = value
            End Set
        End Property

        <StoredProcParameter("@ComputerName")>
        Public Property _ComputerName() As String
            Get
                Return m_ComputerName
            End Get
            Set(ByVal value As String)
                m_ComputerName = value
            End Set
        End Property

        <StoredProcParameter("@LoginName")>
        Public Property _LoginName() As String
            Get
                Return m_LoginName
            End Get
            Set(ByVal value As String)
                m_LoginName = value
            End Set
        End Property

        <StoredProcParameter("@Stamp")>
        Public Property _Stamp() As DateTime?
            Get
                Return m_Stamp
            End Get
            Set(ByVal value As DateTime?)
                m_Stamp = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As AppAuditObject

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getAppAuditObjectbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As AppAuditObject

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getAppAuditObjectbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of AppAuditObject)

            Dim _AppAuditObjectList As List(Of AppAuditObject)
            _AppAuditObjectList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getAppAuditObjectTable"))
            Return _AppAuditObjectList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of AppAuditObject)

            Dim _AppAuditObjectList As List(Of AppAuditObject)
            _AppAuditObjectList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getAppAuditObjectTable"))
            Return _AppAuditObjectList

        End Function

        Public Shared Sub SaveAll(ByVal AppAuditObjectList As List(Of AppAuditObject))

            For Each _AppAuditObject As AppAuditObject In AppAuditObjectList
                SaveRecord(_AppAuditObject)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal AppAuditObjectList As List(Of AppAuditObject))

            For Each _AppAuditObject As AppAuditObject In AppAuditObjectList
                SaveRecord(ConnectionString, _AppAuditObject)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal AppAuditObject As AppAuditObject) As Guid
            AppAuditObject.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, AppAuditObject, "upsertAppAuditObject")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal AppAuditObject As AppAuditObject) As Guid
            AppAuditObject.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, AppAuditObject, "upsertAppAuditObject")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteAppAuditObjectbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteAppAuditObjectbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertAppAuditObject")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertAppAuditObject")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As AppAuditObject

            Dim _A As AppAuditObject = Nothing

            If DR IsNot Nothing Then
                _A = New AppAuditObject()
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("ID"))
                    _A._Scope = DR("scope").ToString.Trim
                    _A._ObjectName = DR("object_name").ToString.Trim
                    _A._ObjectId = GetGUID(DR("object_id"))
                    _A._ObjectDesc = DR("object_desc").ToString.Trim
                    _A._StatusCode = GetByte(DR("status_code"))
                    _A._Status = DR("status").ToString.Trim
                    _A._UserId = GetGUID(DR("user_id"))
                    _A._UserName = DR("user_name").ToString.Trim
                    _A._UserType = DR("user_type").ToString.Trim
                    _A._ComputerName = DR("computer_name").ToString.Trim
                    _A._LoginName = DR("login_name").ToString.Trim
                    _A._Stamp = GetDateTime(DR("stamp"))

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of AppAuditObject)

            Dim _AppAuditObjectList As New List(Of AppAuditObject)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _AppAuditObjectList.Add(PropertiesFromData(_DR))
            Next

            Return _AppAuditObjectList

        End Function


#End Region

    End Class

End Namespace
