﻿'*****************************************************
'Generated 25/09/2018 23:14:26 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class AppListItem
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._ListId = Nothing
                ._Name = ""
                ._Seq = 0
                ._Ref1 = ""
                ._Ref2 = ""
                ._Ref3 = ""
                ._Hide = False

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._ListId = Nothing
                ._Name = ""
                ._Seq = 0
                ._Ref1 = ""
                ._Ref2 = ""
                ._Ref3 = ""
                ._Hide = False

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@ListId")>
        Public Property _ListId() As Guid?

        <StoredProcParameter("@Name")>
        Public Property _Name() As String

        <StoredProcParameter("@Seq")>
        Public Property _Seq() As Integer

        <StoredProcParameter("@Ref1")>
        Public Property _Ref1() As String

        <StoredProcParameter("@Ref2")>
        Public Property _Ref2() As String

        <StoredProcParameter("@Ref3")>
        Public Property _Ref3() As String

        <StoredProcParameter("@Hide")>
        Public Property _Hide() As Boolean


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As AppListItem
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getAppListItembyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As AppListItem
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getAppListItembyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of AppListItem)
            Dim _AppListItemList As List(Of AppListItem)
            _AppListItemList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getAppListItemTable"))
            Return _AppListItemList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of AppListItem)
            Dim _AppListItemList As List(Of AppListItem)
            _AppListItemList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getAppListItemTable"))
            Return _AppListItemList
        End Function

        Public Shared Sub SaveAll(ByVal AppListItemList As List(Of AppListItem))
            For Each _AppListItem As AppListItem In AppListItemList
                SaveRecord(_AppListItem)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal AppListItemList As List(Of AppListItem))
            For Each _AppListItem As AppListItem In AppListItemList
                SaveRecord(ConnectionString, _AppListItem)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal AppListItem As AppListItem) As Guid
            Dim _Current As AppListItem = RetreiveByID(AppListItem._ID.Value)
            AppListItem.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, AppListItem, _Current, "upsertAppListItem")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal AppListItem As AppListItem) As Guid
            Dim _Current As AppListItem = RetreiveByID(ConnectionString, AppListItem._ID.Value)
            AppListItem.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, AppListItem, _Current, "upsertAppListItem")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As AppListItem = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteAppListItembyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As AppListItem = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteAppListItembyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As AppListItem = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertAppListItem")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As AppListItem = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertAppListItem")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As AppListItem

            Dim _A As AppListItem = Nothing

            If DR IsNot Nothing Then
                _A = New AppListItem()
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("ID"))
                    _A._ListId = GetGUID(DR("list_id"))
                    _A._Name = DR("name").ToString.Trim
                    _A._Seq = GetInteger(DR("seq"))
                    _A._Ref1 = DR("ref_1").ToString.Trim
                    _A._Ref2 = DR("ref_2").ToString.Trim
                    _A._Ref3 = DR("ref_3").ToString.Trim
                    _A._Hide = GetBoolean(DR("hide"))

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of AppListItem)

            Dim _AppListItemList As New List(Of AppListItem)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _AppListItemList.Add(PropertiesFromData(_DR))
            Next

            Return _AppListItemList

        End Function


#End Region

    End Class

End Namespace
