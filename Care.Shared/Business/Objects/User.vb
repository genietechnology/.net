﻿'*****************************************************
'Generated 25/09/2018 23:13:45 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class User
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Username = ""
                ._Type = ""
                ._Password = ""
                ._Disabled = False
                ._Forename = ""
                ._Surname = ""
                ._Fullname = ""
                ._JobTitle = ""
                ._Department = ""
                ._Tel = ""
                ._Email = ""
                ._GroupId = Nothing
                ._Sso = False
                ._SsoUsername = ""
                ._Signature = ""
                ._SignatureImg = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Username = ""
                ._Type = ""
                ._Password = ""
                ._Disabled = False
                ._Forename = ""
                ._Surname = ""
                ._Fullname = ""
                ._JobTitle = ""
                ._Department = ""
                ._Tel = ""
                ._Email = ""
                ._GroupId = Nothing
                ._Sso = False
                ._SsoUsername = ""
                ._Signature = ""
                ._SignatureImg = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@Username")>
        Public Property _Username() As String

        <StoredProcParameter("@Type")>
        Public Property _Type() As String

        <StoredProcParameter("@Password")>
        Public Property _Password() As String

        <StoredProcParameter("@Disabled")>
        Public Property _Disabled() As Boolean

        <StoredProcParameter("@Forename")>
        Public Property _Forename() As String

        <StoredProcParameter("@Surname")>
        Public Property _Surname() As String

        <StoredProcParameter("@Fullname")>
        Public Property _Fullname() As String

        <StoredProcParameter("@JobTitle")>
        Public Property _JobTitle() As String

        <StoredProcParameter("@Department")>
        Public Property _Department() As String

        <StoredProcParameter("@Tel")>
        Public Property _Tel() As String

        <StoredProcParameter("@Email")>
        Public Property _Email() As String

        <StoredProcParameter("@GroupId")>
        Public Property _GroupId() As Guid?

        <StoredProcParameter("@Sso")>
        Public Property _Sso() As Boolean

        <StoredProcParameter("@SsoUsername")>
        Public Property _SsoUsername() As String

        <StoredProcParameter("@Signature")>
        Public Property _Signature() As String

        <StoredProcParameter("@SignatureImg")>
        Public Property _SignatureImg() As Byte()


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As User
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getUserbyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As User
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getUserbyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of User)
            Dim _UserList As List(Of User)
            _UserList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getUserTable"))
            Return _UserList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of User)
            Dim _UserList As List(Of User)
            _UserList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getUserTable"))
            Return _UserList
        End Function

        Public Shared Sub SaveAll(ByVal UserList As List(Of User))
            For Each _User As User In UserList
                SaveRecord(_User)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal UserList As List(Of User))
            For Each _User As User In UserList
                SaveRecord(ConnectionString, _User)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal User As User) As Guid
            Dim _Current As User = RetreiveByID(User._ID.Value)
            User.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, User, _Current, "upsertUser")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal User As User) As Guid
            Dim _Current As User = RetreiveByID(ConnectionString, User._ID.Value)
            User.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, User, _Current, "upsertUser")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As User = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteUserbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As User = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteUserbyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As User = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertUser")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As User = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertUser")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As User

            Dim _A As User = Nothing

            If DR IsNot Nothing Then
                _A = New User()
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("ID"))
                    _A._Username = DR("username").ToString.Trim
                    _A._Type = DR("type").ToString.Trim
                    _A._Password = DR("password").ToString.Trim
                    _A._Disabled = GetBoolean(DR("disabled"))
                    _A._Forename = DR("forename").ToString.Trim
                    _A._Surname = DR("surname").ToString.Trim
                    _A._Fullname = DR("fullname").ToString.Trim
                    _A._JobTitle = DR("job_title").ToString.Trim
                    _A._Department = DR("department").ToString.Trim
                    _A._Tel = DR("tel").ToString.Trim
                    _A._Email = DR("email").ToString.Trim
                    _A._GroupId = GetGUID(DR("group_id"))
                    _A._Sso = GetBoolean(DR("sso"))
                    _A._SsoUsername = DR("sso_username").ToString.Trim
                    _A._Signature = DR("signature").ToString.Trim
                    _A._SignatureImg = GetImage(DR("signature_img"))

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of User)

            Dim _UserList As New List(Of User)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _UserList.Add(PropertiesFromData(_DR))
            Next

            Return _UserList

        End Function


#End Region

    End Class

End Namespace
