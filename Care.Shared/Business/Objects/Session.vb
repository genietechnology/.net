﻿Imports Care.Data

Public Class SessionObject

#Region "Variables"
    Private m_Connection As String
    Private m_ConnectionSystem As String
    Private m_User As Business.User
    Private m_PathIcons As String
    Private m_PathReports As String
    Private m_PathTemp As String
    Private m_SMTP As Boolean
    Private m_SMTPServer As String
    Private m_SMTPPort As Integer
    Private m_ClearTemp As Boolean
    Private m_ApplicationName As String
    Private m_MDIForm As System.Windows.Forms.Form
#End Region

#Region "Properties"

    Public ReadOnly Property RegistrySystemPath As String
        Get
            Return "Software\Care Software\System"
        End Get
    End Property

    Public ReadOnly Property ApplicationName() As String
        Get
            Return m_ApplicationName
        End Get
    End Property

    Public ReadOnly Property ConnectionString() As String
        Get
            Return m_Connection
        End Get
    End Property

    Public ReadOnly Property ConnectionStringSystem() As String
        Get
            Return m_ConnectionSystem
        End Get
    End Property

    Public Property CurrentUser() As Business.User
        Get
            Return m_User
        End Get
        Set(ByVal value As Business.User)
            m_User = value
        End Set
    End Property

    Public ReadOnly Property IconFolder() As String
        Get
            Return m_PathIcons
        End Get
    End Property

    Public ReadOnly Property ReportFolder() As String
        Get
            Return m_PathReports
        End Get
    End Property

    Public ReadOnly Property TempFolder() As String
        Get
            Return m_PathTemp
        End Get
    End Property

    Public ReadOnly Property SMTPEnabled() As Boolean
        Get
            Return m_SMTP
        End Get
    End Property

    Public ReadOnly Property SMTPServer() As String
        Get
            Return m_SMTPServer
        End Get
    End Property

    Public ReadOnly Property SMTPPort() As Integer
        Get
            Return m_SMTPPort
        End Get
    End Property

    Public Property ChangeColour() As System.Drawing.Color
        Get
            Return Drawing.Color.Yellow
        End Get
        Set(ByVal value As System.Drawing.Color)

        End Set
    End Property

    Public Property FindColour() As System.Drawing.Color
        Get
            Return Drawing.Color.LightGreen
        End Get
        Set(ByVal value As System.Drawing.Color)

        End Set
    End Property

    Public Property MDIForm As System.Windows.Forms.Form
        Get
            Return m_MDIForm
        End Get
        Set(ByVal value As System.Windows.Forms.Form)
            m_MDIForm = value
        End Set
    End Property

#End Region

    Public Function Setup(ByVal CmdArguments As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

        'return connection details from the system database (kickstart)
        Dim _CnBuilder As ConnectionBuilder = RegistryHandler.ReturnConnectionfromRegistryKey(Session.RegistrySystemPath)
        If _CnBuilder IsNot Nothing Then

            m_ConnectionSystem = _CnBuilder.ConnectionString

            'check the connection works
            _CnBuilder.TestConnection()

            If _CnBuilder.ConnectionOK Then

                'get the application code from commandline arguments
                Dim _Arg As String = CmdArguments(0)

                'get the application parameters from the system database
                Dim _SQL As String = "select * from AppKickStart" & _
                                     " where system_key = '" & _Arg & "'"

                Dim _dr As DataRow = DAL.GetRowfromSQL(_CnBuilder.ConnectionString, _SQL)
                If _dr IsNot Nothing Then

                    m_ApplicationName = _dr.Item("title_1").ToString + " " + _dr.Item("title_2").ToString

                    Dim _cn As New ConnectionBuilder
                    With _cn
                        .Server = _dr.Item("conn_server").ToString
                        .DB = _dr.Item("conn_db").ToString
                        .IntegratedSecurity = CBool(_dr.Item("conn_integrated"))
                        .User = _dr.Item("conn_user").ToString
                        .Password = _dr.Item("conn_password").ToString
                    End With

                    m_Connection = _cn.ConnectionString
                    _cn.TestConnection()

                    'check connection works
                    If _cn.ConnectionOK Then

                        'create admin account if it doesnt already exist
                        CreateAdminUser()

                        'create any mandatory parameters if they dont exist
                        CreateParameters()

                        'set parameters
                        SetParameters()

                    Else
                        'application connection failed
                        Return False
                    End If

                Else
                    'AppKickStart record not found
                    MsgBox("KickStart record " + _Arg + " could not be found. The application will now exit.", MsgBoxStyle.Critical, "Session.Setup")
                    Return False
                End If

                _dr = Nothing

            Else
                'connection to KickStart DB failed
                MsgBox("Connection to KickStart Database failed." + vbCrLf + vbCrLf + _
                       m_ConnectionSystem + vbCrLf + vbCrLf + "The application will now exit.", MsgBoxStyle.Critical, "Session.Setup")
                Return False
            End If

        Else
            'unable to get kickstart details from registry
            'Dim _frm As New frmSetup()
            '_frm.ShowDialog()
        End If

        Return True

    End Function

    Public Sub ClearTempFolders()
        If m_ClearTemp Then
            FileHandler.ClearTemporaryFolder(Session.TempFolder)
        End If
    End Sub

    Private Sub CreateAdminUser()
        Business.User.CreateAdmin()
    End Sub

    Private Sub CreateParameters()

        If Not ParameterHandler.CheckExists("ICONPATH") Then ParameterHandler.Create("ICONPATH", ParameterHandler.EnumParameterType.FilePathType, "System Paths", "Icons Folder", "C:\App\Icons")
        If Not ParameterHandler.CheckExists("REPORTPATH") Then ParameterHandler.Create("REPORTPATH", ParameterHandler.EnumParameterType.FilePathType, "System Paths", "Reports Folder", "C:\App\Reports")
        If Not ParameterHandler.CheckExists("TEMPPATH") Then ParameterHandler.Create("TEMPPATH", ParameterHandler.EnumParameterType.FilePathType, "System Paths", "Temporary Folder", "C:\App\Temp")

        If Not ParameterHandler.CheckExists("SMTP") Then ParameterHandler.Create("SMTP", ParameterHandler.EnumParameterType.BooleanType, "SMTP Email", "SMTP Email Function", "False")
        If Not ParameterHandler.CheckExists("SMTPSERVER") Then ParameterHandler.Create("SMTPSERVER", ParameterHandler.EnumParameterType.StringType, "SMTP Email", "SMTP Server", "")
        If Not ParameterHandler.CheckExists("SMTPPORT") Then ParameterHandler.Create("SMTPPORT", ParameterHandler.EnumParameterType.IntegerType, "SMTP Email", "SMTP Port", "25")

        If Not ParameterHandler.CheckExists("BACKUPCHECK") Then ParameterHandler.Create("BACKUPCHECK", ParameterHandler.EnumParameterType.BooleanType, "Backup", "Backup Reminders", "True")
        If Not ParameterHandler.CheckExists("BACKUPPATH") Then ParameterHandler.Create("BACKUPPATH", ParameterHandler.EnumParameterType.FilePathType, "Backup", "Backup Path", "C:\App\Backup")
        If Not ParameterHandler.CheckExists("BACKUPLAST") Then ParameterHandler.Create("BACKUPLAST", ParameterHandler.EnumParameterType.DateType, "Backup", "Last Backup", "")

    End Sub

    Private Sub SetParameters()

        m_PathIcons = ParameterHandler.ReturnPath("ICONPATH")
        m_PathReports = ParameterHandler.ReturnPath("REPORTPATH")
        m_PathTemp = ParameterHandler.ReturnPath("TEMPPATH")

        m_SMTP = ParameterHandler.ReturnBoolean("SMTP")
        m_SMTPServer = ParameterHandler.ReturnString("SMTPSERVER")
        m_SMTPPort = ParameterHandler.ReturnInteger("SMTPPORT", False)

        m_ClearTemp = True

    End Sub

    Public Sub CursorDefault()
        Windows.Forms.Cursor.Current = Windows.Forms.Cursors.Default
        Windows.Forms.Application.DoEvents()
    End Sub

    Public Sub CursorWaiting()
        Windows.Forms.Cursor.Current = Windows.Forms.Cursors.WaitCursor
        Windows.Forms.Application.DoEvents()
    End Sub

    Public Sub CursorApplicationStarting()
        Windows.Forms.Cursor.Current = Windows.Forms.Cursors.AppStarting
        Windows.Forms.Application.DoEvents()
    End Sub

End Class
