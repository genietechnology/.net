﻿'*****************************************************
'Generated 10/06/2016 12:59:26 using Version 1.16.6.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class AppReport
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Parent As String
        Dim m_Name As String
        Dim m_ReportFile As String
        Dim m_QueryId As Guid?
        Dim m_AdHoc As Boolean
        Dim m_SqlQuery As String
        Dim m_SqlOrder As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Parent = ""
                ._Name = ""
                ._ReportFile = ""
                ._QueryId = Nothing
                ._AdHoc = False
                ._SqlQuery = ""
                ._SqlOrder = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Parent = ""
                ._Name = ""
                ._ReportFile = ""
                ._QueryId = Nothing
                ._AdHoc = False
                ._SqlQuery = ""
                ._SqlOrder = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Parent")> _
        Public Property _Parent() As String
            Get
                Return m_Parent
            End Get
            Set(ByVal value As String)
                m_Parent = value
            End Set
        End Property

        <StoredProcParameter("@Name")> _
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@ReportFile")> _
        Public Property _ReportFile() As String
            Get
                Return m_ReportFile
            End Get
            Set(ByVal value As String)
                m_ReportFile = value
            End Set
        End Property

        <StoredProcParameter("@QueryId")> _
        Public Property _QueryId() As Guid?
            Get
                Return m_QueryId
            End Get
            Set(ByVal value As Guid?)
                m_QueryId = value
            End Set
        End Property

        <StoredProcParameter("@AdHoc")> _
        Public Property _AdHoc() As Boolean
            Get
                Return m_AdHoc
            End Get
            Set(ByVal value As Boolean)
                m_AdHoc = value
            End Set
        End Property

        <StoredProcParameter("@SqlQuery")> _
        Public Property _SqlQuery() As String
            Get
                Return m_SqlQuery
            End Get
            Set(ByVal value As String)
                m_SqlQuery = value
            End Set
        End Property

        <StoredProcParameter("@SqlOrder")> _
        Public Property _SqlOrder() As String
            Get
                Return m_SqlOrder
            End Get
            Set(ByVal value As String)
                m_SqlOrder = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As AppReport

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getAppReportbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of AppReport)

            Dim _AppReportList As List(Of AppReport)
            _AppReportList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getAppReportTable"))
            Return _AppReportList

        End Function

        Public Shared Sub SaveAll(ByVal AppReportList As List(Of AppReport))

            For Each _AppReport As AppReport In AppReportList
                SaveRecord(_AppReport)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal AppReport As AppReport) As Guid
            AppReport.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, AppReport, "upsertAppReport")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteAppReportbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertAppReport")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As AppReport

            Dim _A As AppReport = Nothing

            If DR IsNot Nothing Then
                _A = New AppReport()
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("ID"))
                    _A._Parent = DR("parent").ToString.Trim
                    _A._Name = DR("name").ToString.Trim
                    _A._ReportFile = DR("report_file").ToString.Trim
                    _A._QueryId = GetGUID(DR("query_id"))
                    _A._AdHoc = GetBoolean(DR("ad_hoc"))
                    _A._SqlQuery = DR("sql_query").ToString.Trim
                    _A._SqlOrder = DR("sql_order").ToString.Trim

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of AppReport)

            Dim _AppReportList As New List(Of AppReport)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _AppReportList.Add(PropertiesFromData(_DR))
            Next

            Return _AppReportList

        End Function


#End Region

    End Class

End Namespace
