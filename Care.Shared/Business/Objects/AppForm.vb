﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class AppForm
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_ModuleId As Guid?
        Dim m_MenuId As Guid?
        Dim m_ClassName As String
        Dim m_FormName As String
        Dim m_Caption As String
        Dim m_Tooltip As String
        Dim m_Icon As String
        Dim m_DisplaySeq As Long

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._ModuleId = Nothing
                ._MenuId = Nothing
                ._ClassName = ""
                ._FormName = ""
                ._Caption = ""
                ._Tooltip = ""
                ._Icon = ""
                ._DisplaySeq = 0

            End With

        End Sub

        Public Sub New(ByVal ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._ModuleId = Nothing
                ._MenuId = Nothing
                ._ClassName = ""
                ._FormName = ""
                ._Caption = ""
                ._Tooltip = ""
                ._Icon = ""
                ._DisplaySeq = 0

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@ModuleId")> _
        Public Property _ModuleId() As Guid?
            Get
                Return m_ModuleId
            End Get
            Set(ByVal value As Guid?)
                m_ModuleId = value
            End Set
        End Property

        <StoredProcParameter("@MenuId")> _
        Public Property _MenuId() As Guid?
            Get
                Return m_MenuId
            End Get
            Set(ByVal value As Guid?)
                m_MenuId = value
            End Set
        End Property

        <StoredProcParameter("@ClassName")> _
        Public Property _ClassName() As String
            Get
                Return m_ClassName
            End Get
            Set(ByVal value As String)
                m_ClassName = value
            End Set
        End Property

        <StoredProcParameter("@FormName")> _
        Public Property _FormName() As String
            Get
                Return m_FormName
            End Get
            Set(ByVal value As String)
                m_FormName = value
            End Set
        End Property

        <StoredProcParameter("@Caption")> _
        Public Property _Caption() As String
            Get
                Return m_Caption
            End Get
            Set(ByVal value As String)
                m_Caption = value
            End Set
        End Property

        <StoredProcParameter("@Tooltip")> _
        Public Property _Tooltip() As String
            Get
                Return m_Tooltip
            End Get
            Set(ByVal value As String)
                m_Tooltip = value
            End Set
        End Property

        <StoredProcParameter("@Icon")> _
        Public Property _Icon() As String
            Get
                Return m_Icon
            End Get
            Set(ByVal value As String)
                m_Icon = value
            End Set
        End Property

        <StoredProcParameter("@DisplaySeq")> _
        Public Property _DisplaySeq() As Long
            Get
                Return m_DisplaySeq
            End Get
            Set(ByVal value As Long)
                m_DisplaySeq = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As AppForm

            Dim _AppForm As AppForm
            _AppForm = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getAppFormbyID", ID))
            Return _AppForm

        End Function

        Public Shared Function RetreiveAll() As List(Of AppForm)

            Dim _AppFormList As List(Of AppForm)
            _AppFormList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getAppFormTable"))
            Return _AppFormList

        End Function

        Public Shared Sub SaveAll(ByVal AppFormList As List(Of AppForm))

            For Each _AppForm As AppForm In AppFormList
                SaveRecord(_AppForm)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal AppForm As AppForm) As Guid
            DAL.SaveRecord(Session.ConnectionString, AppForm, "upsertAppForm")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteAppFormbyID", ID)
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As AppForm

            Dim _A As New AppForm()

            If DR IsNot Nothing Then
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("ID"))
                    _A._ModuleId = GetGUID(DR("module_id"))
                    _A._MenuId = GetGUID(DR("menu_id"))
                    _A._ClassName = DR("class_name").ToString.Trim
                    _A._FormName = DR("form_name").ToString.Trim
                    _A._Caption = DR("caption").ToString.Trim
                    _A._Tooltip = DR("tooltip").ToString.Trim
                    _A._Icon = DR("icon").ToString.Trim
                    _A._DisplaySeq = GetLong(DR("display_seq"))

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of AppForm)

            Dim _AppFormList As New List(Of AppForm)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _AppFormList.Add(PropertiesFromData(_DR))
            Next

            Return _AppFormList

        End Function


#End Region

    End Class

End Namespace
