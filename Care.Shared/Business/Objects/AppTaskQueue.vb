﻿'*****************************************************
'Generated 03/10/2018 17:16:57 using Version 1.18.9.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class AppTaskQueue
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._TaskId = Nothing
                ._Status = Nothing
                ._Name = ""
                ._Description = ""
                ._TaskType = ""
                ._ObjectName = ""
                ._Command = ""
                ._Args = ""
                ._EnteredStamp = Nothing
                ._StartedStamp = Nothing
                ._FinishedStamp = Nothing
                ._Result = 0
                ._ErrorText = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._TaskId = Nothing
                ._Status = Nothing
                ._Name = ""
                ._Description = ""
                ._TaskType = ""
                ._ObjectName = ""
                ._Command = ""
                ._Args = ""
                ._EnteredStamp = Nothing
                ._StartedStamp = Nothing
                ._FinishedStamp = Nothing
                ._Result = 0
                ._ErrorText = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?

        <StoredProcParameter("@TaskId")>
        Public Property _TaskId() As Guid?

        <StoredProcParameter("@Status")>
        Public Property _Status() As Byte

        <StoredProcParameter("@Name")>
        Public Property _Name() As String

        <StoredProcParameter("@Description")>
        Public Property _Description() As String

        <StoredProcParameter("@TaskType")>
        Public Property _TaskType() As String

        <StoredProcParameter("@ObjectName")>
        Public Property _ObjectName() As String

        <StoredProcParameter("@Command")>
        Public Property _Command() As String

        <StoredProcParameter("@Args")>
        Public Property _Args() As String

        <StoredProcParameter("@EnteredStamp")>
        Public Property _EnteredStamp() As DateTime?

        <StoredProcParameter("@StartedStamp")>
        Public Property _StartedStamp() As DateTime?

        <StoredProcParameter("@FinishedStamp")>
        Public Property _FinishedStamp() As DateTime?

        <StoredProcParameter("@Result")>
        Public Property _Result() As Integer

        <StoredProcParameter("@ErrorText")>
        Public Property _ErrorText() As String


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As AppTaskQueue
            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getAppTaskQueuebyID", ID))
        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As AppTaskQueue
            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getAppTaskQueuebyID", ID))
        End Function

        Public Shared Function RetreiveAll() As List(Of AppTaskQueue)
            Dim _AppTaskQueueList As List(Of AppTaskQueue)
            _AppTaskQueueList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getAppTaskQueueTable"))
            Return _AppTaskQueueList
        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of AppTaskQueue)
            Dim _AppTaskQueueList As List(Of AppTaskQueue)
            _AppTaskQueueList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getAppTaskQueueTable"))
            Return _AppTaskQueueList
        End Function

        Public Shared Sub SaveAll(ByVal AppTaskQueueList As List(Of AppTaskQueue))
            For Each _AppTaskQueue As AppTaskQueue In AppTaskQueueList
                SaveRecord(_AppTaskQueue)
            Next
        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal AppTaskQueueList As List(Of AppTaskQueue))
            For Each _AppTaskQueue As AppTaskQueue In AppTaskQueueList
                SaveRecord(ConnectionString, _AppTaskQueue)
            Next
        End Sub

        Public Shared Function SaveRecord(ByVal AppTaskQueue As AppTaskQueue) As Guid
            Dim _Current As AppTaskQueue = RetreiveByID(AppTaskQueue._ID.Value)
            AppTaskQueue.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, AppTaskQueue, _Current, "upsertAppTaskQueue")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal AppTaskQueue As AppTaskQueue) As Guid
            Dim _Current As AppTaskQueue = RetreiveByID(ConnectionString, AppTaskQueue._ID.Value)
            AppTaskQueue.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, AppTaskQueue, _Current, "upsertAppTaskQueue")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            Dim _Current As AppTaskQueue = RetreiveByID(ID)
            DAL.DeleteRecordbyID(Session.ConnectionString, Session.CurrentUser.ID, _Current, "deleteAppTaskQueuebyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            Dim _Current As AppTaskQueue = RetreiveByID(ID)
            DAL.DeleteRecordbyID(ConnectionString, Session.CurrentUser.ID, _Current, "deleteAppTaskQueuebyID", ID)
        End Sub

        Public Sub Store()
            Dim _Current As AppTaskQueue = RetreiveByID(Session.ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertAppTaskQueue")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            Dim _Current As AppTaskQueue = RetreiveByID(ConnectionString, _ID.Value)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionManager, Session.CurrentUser.ID, Me, _Current, "upsertAppTaskQueue")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As AppTaskQueue

            Dim _A As AppTaskQueue = Nothing

            If DR IsNot Nothing Then
                _A = New AppTaskQueue()
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("ID"))
                    _A._TaskId = GetGUID(DR("task_id"))
                    _A._Status = GetByte(DR("status"))
                    _A._Name = DR("name").ToString.Trim
                    _A._Description = DR("description").ToString.Trim
                    _A._TaskType = DR("task_type").ToString.Trim
                    _A._ObjectName = DR("object_name").ToString.Trim
                    _A._Command = DR("command").ToString.Trim
                    _A._Args = DR("args").ToString.Trim
                    _A._EnteredStamp = GetDateTime(DR("entered_stamp"))
                    _A._StartedStamp = GetDateTime(DR("started_stamp"))
                    _A._FinishedStamp = GetDateTime(DR("finished_stamp"))
                    _A._Result = GetInteger(DR("result"))
                    _A._ErrorText = DR("error_text").ToString.Trim

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of AppTaskQueue)

            Dim _AppTaskQueueList As New List(Of AppTaskQueue)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _AppTaskQueueList.Add(PropertiesFromData(_DR))
            Next

            Return _AppTaskQueueList

        End Function


#End Region

    End Class

End Namespace
