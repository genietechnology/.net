﻿'*****************************************************
'Generated 08/10/2017 16:27:56 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class AppControlSecurity
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_FormName As String
        Dim m_ControlName As String
        Dim m_ControlType As String
        Dim m_ControlTabName As String
        Dim m_ControlSecurityType As String
        Dim m_ControlSecurityId As Guid?
        Dim m_ControlSecurityName As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._FormName = ""
                ._ControlName = ""
                ._ControlType = ""
                ._ControlTabName = ""
                ._ControlSecurityType = ""
                ._ControlSecurityId = Nothing
                ._ControlSecurityName = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._FormName = ""
                ._ControlName = ""
                ._ControlType = ""
                ._ControlTabName = ""
                ._ControlSecurityType = ""
                ._ControlSecurityId = Nothing
                ._ControlSecurityName = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@FormName")>
        Public Property _FormName() As String
            Get
                Return m_FormName
            End Get
            Set(ByVal value As String)
                m_FormName = value
            End Set
        End Property

        <StoredProcParameter("@ControlName")>
        Public Property _ControlName() As String
            Get
                Return m_ControlName
            End Get
            Set(ByVal value As String)
                m_ControlName = value
            End Set
        End Property

        <StoredProcParameter("@ControlType")>
        Public Property _ControlType() As String
            Get
                Return m_ControlType
            End Get
            Set(ByVal value As String)
                m_ControlType = value
            End Set
        End Property

        <StoredProcParameter("@ControlTabName")>
        Public Property _ControlTabName() As String
            Get
                Return m_ControlTabName
            End Get
            Set(ByVal value As String)
                m_ControlTabName = value
            End Set
        End Property

        <StoredProcParameter("@ControlSecurityType")>
        Public Property _ControlSecurityType() As String
            Get
                Return m_ControlSecurityType
            End Get
            Set(ByVal value As String)
                m_ControlSecurityType = value
            End Set
        End Property

        <StoredProcParameter("@ControlSecurityId")>
        Public Property _ControlSecurityId() As Guid?
            Get
                Return m_ControlSecurityId
            End Get
            Set(ByVal value As Guid?)
                m_ControlSecurityId = value
            End Set
        End Property

        <StoredProcParameter("@ControlSecurityName")>
        Public Property _ControlSecurityName() As String
            Get
                Return m_ControlSecurityName
            End Get
            Set(ByVal value As String)
                m_ControlSecurityName = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As AppControlSecurity

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getAppControlSecuritybyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As AppControlSecurity

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getAppControlSecuritybyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of AppControlSecurity)

            Dim _AppControlSecurityList As List(Of AppControlSecurity)
            _AppControlSecurityList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getAppControlSecurityTable"))
            Return _AppControlSecurityList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of AppControlSecurity)

            Dim _AppControlSecurityList As List(Of AppControlSecurity)
            _AppControlSecurityList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getAppControlSecurityTable"))
            Return _AppControlSecurityList

        End Function

        Public Shared Sub SaveAll(ByVal AppControlSecurityList As List(Of AppControlSecurity))

            For Each _AppControlSecurity As AppControlSecurity In AppControlSecurityList
                SaveRecord(_AppControlSecurity)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal AppControlSecurityList As List(Of AppControlSecurity))

            For Each _AppControlSecurity As AppControlSecurity In AppControlSecurityList
                SaveRecord(ConnectionString, _AppControlSecurity)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal AppControlSecurity As AppControlSecurity) As Guid
            AppControlSecurity.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, AppControlSecurity, "upsertAppControlSecurity")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal AppControlSecurity As AppControlSecurity) As Guid
            AppControlSecurity.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, AppControlSecurity, "upsertAppControlSecurity")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteAppControlSecuritybyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteAppControlSecuritybyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertAppControlSecurity")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertAppControlSecurity")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As AppControlSecurity

            Dim _A As AppControlSecurity = Nothing

            If DR IsNot Nothing Then
                _A = New AppControlSecurity()
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("ID"))
                    _A._FormName = DR("form_name").ToString.Trim
                    _A._ControlName = DR("control_name").ToString.Trim
                    _A._ControlType = DR("control_type").ToString.Trim
                    _A._ControlTabName = DR("control_tab_name").ToString.Trim
                    _A._ControlSecurityType = DR("control_security_type").ToString.Trim
                    _A._ControlSecurityId = GetGUID(DR("control_security_id"))
                    _A._ControlSecurityName = DR("control_security_name").ToString.Trim

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of AppControlSecurity)

            Dim _AppControlSecurityList As New List(Of AppControlSecurity)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _AppControlSecurityList.Add(PropertiesFromData(_DR))
            Next

            Return _AppControlSecurityList

        End Function


#End Region

    End Class

End Namespace
