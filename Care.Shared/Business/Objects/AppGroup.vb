﻿'*****************************************************
'Generated 03/05/2015 16:46:50 using Version 1.15.5.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class AppGroup
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Name As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Name = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Name = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Name")> _
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As AppGroup

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getAppGroupbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of AppGroup)

            Dim _AppGroupList As List(Of AppGroup)
            _AppGroupList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getAppGroupTable"))
            Return _AppGroupList

        End Function

        Public Shared Sub SaveAll(ByVal AppGroupList As List(Of AppGroup))

            For Each _AppGroup As AppGroup In AppGroupList
                SaveRecord(_AppGroup)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal AppGroup As AppGroup) As Guid
            DAL.SaveRecord(Session.ConnectionString, AppGroup, "upsertAppGroup")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteAppGroupbyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertAppGroup")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As AppGroup

            Dim _A As AppGroup = Nothing

            If DR IsNot Nothing Then
                _A = New AppGroup()
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("ID"))
                    _A._Name = DR("name").ToString.Trim

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of AppGroup)

            Dim _AppGroupList As New List(Of AppGroup)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _AppGroupList.Add(PropertiesFromData(_DR))
            Next

            Return _AppGroupList

        End Function


#End Region

    End Class

End Namespace
