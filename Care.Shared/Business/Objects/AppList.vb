﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class AppList
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Name As String
        Dim m_Type As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Name = ""
                ._Type = ""

            End With

        End Sub

        Public Sub New(ByVal ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Name = ""
                ._Type = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Name")> _
        Public Property _Name() As String
            Get
                Return m_Name
            End Get
            Set(ByVal value As String)
                m_Name = value
            End Set
        End Property

        <StoredProcParameter("@Type")> _
        Public Property _Type() As String
            Get
                Return m_Type
            End Get
            Set(ByVal value As String)
                m_Type = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Sub Save()
            SaveAppList(Me)
        End Sub

        Public Shared Function RetreiveByID(ByVal ID As Guid) As AppList

            Dim _AppList As AppList
            _AppList = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getAppListbyID", ID))
            Return _AppList

        End Function

        Public Shared Function RetreiveAll() As List(Of AppList)

            Dim _AppListList As List(Of AppList)
            _AppListList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getAppListTable"))
            Return _AppListList

        End Function

        Public Shared Sub SaveAll(ByVal AppListList As List(Of AppList))

            For Each _AppList As AppList In AppListList
                SaveAppList(_AppList)
            Next

        End Sub

        Public Shared Function SaveAppList(ByVal AppList As AppList) As Guid
            DAL.SaveRecord(Session.ConnectionString, AppList, "upsertAppList")
        End Function

#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As AppList

            Dim _A As New AppList()

            If DR IsNot Nothing Then
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("ID"))
                    _A._Name = DR("name").ToString.Trim
                    _A._Type = DR("type").ToString.Trim

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of AppList)

            Dim _AppListList As New List(Of AppList)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _AppListList.Add(PropertiesFromData(_DR))
            Next

            Return _AppListList

        End Function


#End Region

    End Class

End Namespace
