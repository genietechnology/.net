﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class AppQueries
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Title As String
        Dim m_Connection As String
        Dim m_Type As String
        Dim m_Query As String

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Title = ""
                ._Connection = ""
                ._Type = ""
                ._Query = ""

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Title = ""
                ._Connection = ""
                ._Type = ""
                ._Query = ""

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Title")> _
        Public Property _Title() As String
            Get
                Return m_Title
            End Get
            Set(ByVal value As String)
                m_Title = value
            End Set
        End Property

        <StoredProcParameter("@Connection")> _
        Public Property _Connection() As String
            Get
                Return m_Connection
            End Get
            Set(ByVal value As String)
                m_Connection = value
            End Set
        End Property

        <StoredProcParameter("@Type")> _
        Public Property _Type() As String
            Get
                Return m_Type
            End Get
            Set(ByVal value As String)
                m_Type = value
            End Set
        End Property

        <StoredProcParameter("@Query")> _
        Public Property _Query() As String
            Get
                Return m_Query
            End Get
            Set(ByVal value As String)
                m_Query = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As AppQueries

            Dim _AppQueries As AppQueries
            _AppQueries = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getAppQueriesbyID", ID))
            Return _AppQueries

        End Function

        Public Shared Function RetreiveAll() As List(Of AppQueries)

            Dim _AppQueriesList As List(Of AppQueries)
            _AppQueriesList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getAppQueriesTable"))
            Return _AppQueriesList

        End Function

        Public Shared Sub SaveAll(ByVal AppQueriesList As List(Of AppQueries))

            For Each _AppQueries As AppQueries In AppQueriesList
                SaveRecord(_AppQueries)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal AppQueries As AppQueries) As Guid
            DAL.SaveRecord(Session.ConnectionString, AppQueries, "upsertAppQueries")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteAppQueriesbyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertAppQueries")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As AppQueries

            If DR Is Nothing Then Return Nothing

            Dim _A As New AppQueries()
            With DR
                _A.IsNew = False
                _A.IsDeleted = False
                _A._ID = GetGUID(DR("ID"))
                _A._Title = DR("title").ToString.Trim
                _A._Connection = DR("connection").ToString.Trim
                _A._Type = DR("type").ToString.Trim
                _A._Query = DR("query").ToString.Trim

            End With

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of AppQueries)

            Dim _AppQueriesList As New List(Of AppQueries)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _AppQueriesList.Add(PropertiesFromData(_DR))
            Next

            Return _AppQueriesList

        End Function


#End Region

    End Class

End Namespace
