﻿'*****************************************************
'Generated 15/07/2018 10:38:19 using Version 1.17.4.0
'*****************************************************

Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class AppError
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_Stamp As DateTime?
        Dim m_Hostname As String
        Dim m_UserId As Guid?
        Dim m_UserName As String
        Dim m_WindowsUser As String
        Dim m_Source As String
        Dim m_SourceVersion As String
        Dim m_Exception As String
        Dim m_Stack As String
        Dim m_Unhandled As Boolean

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._Stamp = Nothing
                ._Hostname = ""
                ._UserId = Nothing
                ._UserName = ""
                ._WindowsUser = ""
                ._Source = ""
                ._SourceVersion = ""
                ._Exception = ""
                ._Stack = ""
                ._Unhandled = False

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._Stamp = Nothing
                ._Hostname = ""
                ._UserId = Nothing
                ._UserName = ""
                ._WindowsUser = ""
                ._Source = ""
                ._SourceVersion = ""
                ._Exception = ""
                ._Stack = ""
                ._Unhandled = False

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")>
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@Stamp")>
        Public Property _Stamp() As DateTime?
            Get
                Return m_Stamp
            End Get
            Set(ByVal value As DateTime?)
                m_Stamp = value
            End Set
        End Property

        <StoredProcParameter("@Hostname")>
        Public Property _Hostname() As String
            Get
                Return m_Hostname
            End Get
            Set(ByVal value As String)
                m_Hostname = value
            End Set
        End Property

        <StoredProcParameter("@UserId")>
        Public Property _UserId() As Guid?
            Get
                Return m_UserId
            End Get
            Set(ByVal value As Guid?)
                m_UserId = value
            End Set
        End Property

        <StoredProcParameter("@UserName")>
        Public Property _UserName() As String
            Get
                Return m_UserName
            End Get
            Set(ByVal value As String)
                m_UserName = value
            End Set
        End Property

        <StoredProcParameter("@WindowsUser")>
        Public Property _WindowsUser() As String
            Get
                Return m_WindowsUser
            End Get
            Set(ByVal value As String)
                m_WindowsUser = value
            End Set
        End Property

        <StoredProcParameter("@Source")>
        Public Property _Source() As String
            Get
                Return m_Source
            End Get
            Set(ByVal value As String)
                m_Source = value
            End Set
        End Property

        <StoredProcParameter("@SourceVersion")>
        Public Property _SourceVersion() As String
            Get
                Return m_SourceVersion
            End Get
            Set(ByVal value As String)
                m_SourceVersion = value
            End Set
        End Property

        <StoredProcParameter("@Exception")>
        Public Property _Exception() As String
            Get
                Return m_Exception
            End Get
            Set(ByVal value As String)
                m_Exception = value
            End Set
        End Property

        <StoredProcParameter("@Stack")>
        Public Property _Stack() As String
            Get
                Return m_Stack
            End Get
            Set(ByVal value As String)
                m_Stack = value
            End Set
        End Property

        <StoredProcParameter("@Unhandled")>
        Public Property _Unhandled() As Boolean
            Get
                Return m_Unhandled
            End Get
            Set(ByVal value As Boolean)
                m_Unhandled = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As AppError

            Return PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getAppErrorbyID", ID))

        End Function

        Public Shared Function RetreiveByID(ByVal ConnectionString As String, ByVal ID As Guid) As AppError

            Return PropertiesFromData(DAL.GetRowbyID(ConnectionString, "getAppErrorbyID", ID))

        End Function

        Public Shared Function RetreiveAll() As List(Of AppError)

            Dim _AppErrorList As List(Of AppError)
            _AppErrorList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getAppErrorTable"))
            Return _AppErrorList

        End Function

        Public Shared Function RetreiveAll(ByVal ConnectionString As String) As List(Of AppError)

            Dim _AppErrorList As List(Of AppError)
            _AppErrorList = PopulateList(DAL.GetDataTablefromSP(ConnectionString, "getAppErrorTable"))
            Return _AppErrorList

        End Function

        Public Shared Sub SaveAll(ByVal AppErrorList As List(Of AppError))

            For Each _AppError As AppError In AppErrorList
                SaveRecord(_AppError)
            Next

        End Sub

        Public Shared Sub SaveAll(ByVal ConnectionString As String, ByVal AppErrorList As List(Of AppError))

            For Each _AppError As AppError In AppErrorList
                SaveRecord(ConnectionString, _AppError)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal AppError As AppError) As Guid
            AppError.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, AppError, "upsertAppError")
        End Function

        Public Shared Function SaveRecord(ByVal ConnectionString As String, ByVal AppError As AppError) As Guid
            AppError.SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, AppError, "upsertAppError")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteAppErrorbyID", ID)
        End Sub

        Public Shared Sub DeleteRecord(ByVal ConnectionString As String, ByVal ID As Guid)
            DAL.DeleteRecordbyID(ConnectionString, "deleteAppErrorbyID", ID)
        End Sub

        Public Sub Store()
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertAppError")
        End Sub

        Public Sub Store(ByVal ConnectionString As String)
            SetDefaultValues()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertAppError")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As AppError

            Dim _A As AppError = Nothing

            If DR IsNot Nothing Then
                _A = New AppError()
                With DR
                    _A.IsNew = False
                    _A.IsDeleted = False
                    _A._ID = GetGUID(DR("ID"))
                    _A._Stamp = GetDateTime(DR("stamp"))
                    _A._Hostname = DR("hostname").ToString.Trim
                    _A._UserId = GetGUID(DR("user_id"))
                    _A._UserName = DR("user_name").ToString.Trim
                    _A._WindowsUser = DR("windows_user").ToString.Trim
                    _A._Source = DR("source").ToString.Trim
                    _A._SourceVersion = DR("source_version").ToString.Trim
                    _A._Exception = DR("exception").ToString.Trim
                    _A._Stack = DR("stack").ToString.Trim
                    _A._Unhandled = GetBoolean(DR("unhandled"))

                End With
            End If

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of AppError)

            Dim _AppErrorList As New List(Of AppError)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _AppErrorList.Add(PropertiesFromData(_DR))
            Next

            Return _AppErrorList

        End Function


#End Region

    End Class

End Namespace
