﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Public Class Appointment
        Inherits DataObjectBase
        Implements IBusinessEntity

#Region "Variables"

        Dim m_ID As Guid?
        Dim m_CalendarType As String
        Dim m_CalendarId As Guid?
        Dim m_StartDate As DateTime?
        Dim m_EndDate As DateTime?
        Dim m_Subject As String
        Dim m_Type As Integer
        Dim m_Status As Integer
        Dim m_Label As Integer
        Dim m_Allday As Boolean
        Dim m_Location As String
        Dim m_Description As String
        Dim m_ResourceId As Integer
        Dim m_ResourceDetails As String
        Dim m_ReminderDetails As String
        Dim m_RecurrenceDetails As String
        Dim m_LinkForm As String
        Dim m_LinkId As Guid?
        Dim m_UserId As Guid?
        Dim m_Stamp As DateTime?

#End Region

#Region "Constructors"

        Public Sub New()

            With Me
                .IsNew = True
                .IsDeleted = False
                ._ID = Guid.NewGuid
                ._CalendarType = ""
                ._CalendarId = Nothing
                ._StartDate = Nothing
                ._EndDate = Nothing
                ._Subject = ""
                ._Type = 0
                ._Status = 0
                ._Label = 0
                ._Allday = False
                ._Location = ""
                ._Description = ""
                ._ResourceId = 0
                ._ResourceDetails = ""
                ._ReminderDetails = ""
                ._RecurrenceDetails = ""
                ._LinkForm = ""
                ._LinkId = Nothing
                ._UserId = Nothing
                ._Stamp = Nothing

            End With

        End Sub

        Public Sub New(ID As Guid)

            With Me
                .IsNew = False
                .IsDeleted = False
                ._ID = ID
                ._CalendarType = ""
                ._CalendarId = Nothing
                ._StartDate = Nothing
                ._EndDate = Nothing
                ._Subject = ""
                ._Type = 0
                ._Status = 0
                ._Label = 0
                ._Allday = False
                ._Location = ""
                ._Description = ""
                ._ResourceId = 0
                ._ResourceDetails = ""
                ._ReminderDetails = ""
                ._RecurrenceDetails = ""
                ._LinkForm = ""
                ._LinkId = Nothing
                ._UserId = Nothing
                ._Stamp = Nothing

            End With

        End Sub

#End Region

#Region "Properties"

        <StoredProcParameter("@ID")> _
        Public Property _ID() As Guid?
            Get
                Return m_ID
            End Get
            Set(ByVal value As Guid?)
                m_ID = value
            End Set
        End Property

        <StoredProcParameter("@CalendarType")> _
        Public Property _CalendarType() As String
            Get
                Return m_CalendarType
            End Get
            Set(ByVal value As String)
                m_CalendarType = value
            End Set
        End Property

        <StoredProcParameter("@CalendarId")> _
        Public Property _CalendarId() As Guid?
            Get
                Return m_CalendarId
            End Get
            Set(ByVal value As Guid?)
                m_CalendarId = value
            End Set
        End Property

        <StoredProcParameter("@StartDate")> _
        Public Property _StartDate() As DateTime?
            Get
                Return m_StartDate
            End Get
            Set(ByVal value As DateTime?)
                m_StartDate = value
            End Set
        End Property

        <StoredProcParameter("@EndDate")> _
        Public Property _EndDate() As DateTime?
            Get
                Return m_EndDate
            End Get
            Set(ByVal value As DateTime?)
                m_EndDate = value
            End Set
        End Property

        <StoredProcParameter("@Subject")> _
        Public Property _Subject() As String
            Get
                Return m_Subject
            End Get
            Set(ByVal value As String)
                m_Subject = value
            End Set
        End Property

        <StoredProcParameter("@Type")> _
        Public Property _Type() As Integer
            Get
                Return m_Type
            End Get
            Set(ByVal value As Integer)
                m_Type = value
            End Set
        End Property

        <StoredProcParameter("@Status")> _
        Public Property _Status() As Integer
            Get
                Return m_Status
            End Get
            Set(ByVal value As Integer)
                m_Status = value
            End Set
        End Property

        <StoredProcParameter("@Label")> _
        Public Property _Label() As Integer
            Get
                Return m_Label
            End Get
            Set(ByVal value As Integer)
                m_Label = value
            End Set
        End Property

        <StoredProcParameter("@Allday")> _
        Public Property _Allday() As Boolean
            Get
                Return m_Allday
            End Get
            Set(ByVal value As Boolean)
                m_Allday = value
            End Set
        End Property

        <StoredProcParameter("@Location")> _
        Public Property _Location() As String
            Get
                Return m_Location
            End Get
            Set(ByVal value As String)
                m_Location = value
            End Set
        End Property

        <StoredProcParameter("@Description")> _
        Public Property _Description() As String
            Get
                Return m_Description
            End Get
            Set(ByVal value As String)
                m_Description = value
            End Set
        End Property

        <StoredProcParameter("@ResourceId")> _
        Public Property _ResourceId() As Integer
            Get
                Return m_ResourceId
            End Get
            Set(ByVal value As Integer)
                m_ResourceId = value
            End Set
        End Property

        <StoredProcParameter("@ResourceDetails")> _
        Public Property _ResourceDetails() As String
            Get
                Return m_ResourceDetails
            End Get
            Set(ByVal value As String)
                m_ResourceDetails = value
            End Set
        End Property

        <StoredProcParameter("@ReminderDetails")> _
        Public Property _ReminderDetails() As String
            Get
                Return m_ReminderDetails
            End Get
            Set(ByVal value As String)
                m_ReminderDetails = value
            End Set
        End Property

        <StoredProcParameter("@RecurrenceDetails")> _
        Public Property _RecurrenceDetails() As String
            Get
                Return m_RecurrenceDetails
            End Get
            Set(ByVal value As String)
                m_RecurrenceDetails = value
            End Set
        End Property

        <StoredProcParameter("@LinkForm")> _
        Public Property _LinkForm() As String
            Get
                Return m_LinkForm
            End Get
            Set(ByVal value As String)
                m_LinkForm = value
            End Set
        End Property

        <StoredProcParameter("@LinkId")> _
        Public Property _LinkId() As Guid?
            Get
                Return m_LinkId
            End Get
            Set(ByVal value As Guid?)
                m_LinkId = value
            End Set
        End Property

        <StoredProcParameter("@UserId")> _
        Public Property _UserId() As Guid?
            Get
                Return m_UserId
            End Get
            Set(ByVal value As Guid?)
                m_UserId = value
            End Set
        End Property

        <StoredProcParameter("@Stamp")> _
        Public Property _Stamp() As DateTime?
            Get
                Return m_Stamp
            End Get
            Set(ByVal value As DateTime?)
                m_Stamp = value
            End Set
        End Property


#End Region

#Region "Methods"

        Public Shared Function RetreiveByID(ByVal ID As Guid) As Appointment

            Dim _Appointment As Appointment
            _Appointment = PropertiesFromData(DAL.GetRowbyID(Session.ConnectionString, "getAppointmentbyID", ID))
            Return _Appointment

        End Function

        Public Shared Function RetreiveAll() As List(Of Appointment)

            Dim _AppointmentList As List(Of Appointment)
            _AppointmentList = PopulateList(DAL.GetDataTablefromSP(Session.ConnectionString, "getAppointmentTable"))
            Return _AppointmentList

        End Function

        Public Shared Sub SaveAll(ByVal AppointmentList As List(Of Appointment))

            For Each _Appointment As Appointment In AppointmentList
                SaveRecord(_Appointment)
            Next

        End Sub

        Public Shared Function SaveRecord(ByVal Appointment As Appointment) As Guid
            DAL.SaveRecord(Session.ConnectionString, Appointment, "upsertAppointment")
        End Function

        Public Shared Sub DeleteRecord(ByVal ID As Guid)
            DAL.DeleteRecordbyID(Session.ConnectionString, "deleteAppointmentbyID", ID)
        End Sub

        Public Sub Store()
            DAL.SaveRecord(Session.ConnectionString, Me, "upsertAppointment")
        End Sub


#End Region

#Region "Helper Methods"

        Private Shared Function PropertiesFromData(ByVal DR As DataRow) As Appointment

            If DR Is Nothing Then Return Nothing

            Dim _A As New Appointment()
            With DR
                _A.IsNew = False
                _A.IsDeleted = False
                _A._ID = GetGUID(DR("ID"))
                _A._CalendarType = DR("calendar_type").ToString.Trim
                _A._CalendarId = GetGUID(DR("calendar_id"))
                _A._StartDate = GetDateTime(DR("start_date"))
                _A._EndDate = GetDateTime(DR("end_date"))
                _A._Subject = DR("subject").ToString.Trim
                _A._Type = GetInteger(DR("type"))
                _A._Status = GetInteger(DR("status"))
                _A._Label = GetInteger(DR("label"))
                _A._Allday = GetBoolean(DR("allday"))
                _A._Location = DR("location").ToString.Trim
                _A._Description = DR("description").ToString.Trim
                _A._ResourceId = GetInteger(DR("resource_id"))
                _A._ResourceDetails = DR("resource_details").ToString.Trim
                _A._ReminderDetails = DR("reminder_details").ToString.Trim
                _A._RecurrenceDetails = DR("recurrence_details").ToString.Trim
                _A._LinkForm = DR("link_form").ToString.Trim
                _A._LinkId = GetGUID(DR("link_id"))
                _A._UserId = GetGUID(DR("user_id"))
                _A._Stamp = GetDateTime(DR("stamp"))

            End With

            Return _A

        End Function

        Private Shared Function PopulateList(ByVal Table As DataTable) As List(Of Appointment)

            Dim _AppointmentList As New List(Of Appointment)
            Dim _DR As DataRow

            For Each _DR In Table.Rows
                _AppointmentList.Add(PropertiesFromData(_DR))
            Next

            Return _AppointmentList

        End Function


#End Region

    End Class

End Namespace
