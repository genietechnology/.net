﻿
Public MustInherit Class BusinessObjectBase

    Private m_IsNew As Boolean = False
    Private m_IsDeleted As Boolean = False

    Public Property IsNew() As Boolean
        Get
            Return m_IsNew
        End Get
        Set(ByVal value As Boolean)
            m_IsNew = value
        End Set
    End Property

    Public Property IsDeleted() As Boolean
        Get
            Return m_IsDeleted
        End Get
        Set(ByVal value As Boolean)
            m_IsDeleted = value
        End Set
    End Property

#Region "Getters"

    Public Shared Function GetGUID(ByVal ObjectIn As Object) As Guid?

        If ObjectIn Is DBNull.Value Then
            Return Nothing
        Else
            Return CType(ObjectIn, Guid)
        End If

    End Function

    Public Shared Function GetDate(ByVal ObjectIn As Object) As Date?

        If ObjectIn Is DBNull.Value Then
            Return Nothing
        Else
            If CType(ObjectIn, Date) = Date.MinValue Then
                Return Nothing
            Else
                Return CType(ObjectIn, Date)
            End If
        End If

    End Function

    Public Shared Function GetDateTime(ByVal ObjectIn As Object) As Date?

        If ObjectIn Is DBNull.Value Then
            Return Nothing
        Else
            If CType(ObjectIn, Date) = Date.MinValue Then
                Return Nothing
            Else
                Return CType(ObjectIn, Date)
            End If
        End If

    End Function

    Public Shared Function GetBoolean(ByVal ObjectIn As Object) As Boolean

        If ObjectIn Is DBNull.Value Then
            Return False
        Else
            Return CType(ObjectIn, Boolean)
        End If

    End Function

    Public Shared Function GetDecimal(ByVal ObjectIn As Object) As Decimal

        If ObjectIn Is DBNull.Value Then
            Return 0
        Else
            Return CType(ObjectIn, Decimal)
        End If

    End Function

    Public Shared Function GetInteger(ByVal ObjectIn As Object) As Integer

        If ObjectIn Is DBNull.Value Then
            Return 0
        Else
            Return CType(ObjectIn, Integer)
        End If

    End Function

    Public Shared Function GetLong(ByVal ObjectIn As Object) As Long

        If ObjectIn Is DBNull.Value Then
            Return 0
        Else
            Return CType(ObjectIn, Long)
        End If

    End Function

    Public Shared Function GetImage(ByVal ObjectIn As Object) As Byte()

        If ObjectIn Is DBNull.Value Then
            Return Nothing
        Else
            Return CType(ObjectIn, Byte())
        End If

    End Function

    Public Shared Function GetByte(ByVal ObjectIn As Object) As Byte

        If ObjectIn Is DBNull.Value Then
            Return Nothing
        Else
            Return CType(ObjectIn, Byte)
        End If

    End Function


#End Region

End Class

