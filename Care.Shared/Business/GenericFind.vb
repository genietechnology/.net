﻿Imports Care.Global
Imports System.Windows.Forms
Imports System.ComponentModel

Public Class GenericFind

    Public Enum EnumSearchMode
        SearchAsYouType
        SearchOnEnter
        RadioButtons
    End Enum

    Public Sub New()
        m_SearchMode = EnumSearchMode.SearchAsYouType
        m_HideFirstColumn = False
        m_HasReturnValue = False
        m_ReturnValue = ""
        m_Caption = ""
        m_ParentForm = Nothing
        m_PopulateOnLoad = False
        m_GridSQL = ""
        m_GridWhereClause = ""
        m_GridOrderBy = ""
        m_ReturnField = ""
        m_Option1Field = ""
        m_Option1Caption = ""
        m_Option2Field = ""
        m_Option2Caption = ""
        m_Option3Field = ""
        m_Option3Caption = ""
        m_Option4Field = ""
        m_Option4Caption = ""
        m_LookupSQL = ""
        m_LookupReturn = ""
        m_LookupCode = ""
        m_LookupDescription = ""
        m_ConnectionString = ""
        m_UseSquareBrackets = True
        m_PreviewField = ""
    End Sub

#Region "Variables"

    Dim m_HideFirstColumn As Boolean
    Dim m_HasReturnValue As Boolean
    Dim m_ReturnValue As String
    Dim m_Caption As String
    Dim m_ParentForm As Form
    Dim m_PopulateOnLoad As Boolean
    Dim m_GridSQL As String
    Dim m_GridWhereClause As String
    Dim m_GridOrderBy As String
    Dim m_ReturnField As String
    Dim m_Option1Field As String
    Dim m_Option1Caption As String
    Dim m_Option2Field As String
    Dim m_Option2Caption As String
    Dim m_Option3Field As String
    Dim m_Option3Caption As String
    Dim m_Option4Field As String
    Dim m_Option4Caption As String
    Dim m_LookupSQL As String
    Dim m_LookupReturn As String
    Dim m_LookupCode As String
    Dim m_LookupDescription As String
    Dim m_ConnectionString As String
    Dim m_FormHeight As Integer
    Dim m_FormWidth As Integer
    Dim m_SearchMode As EnumSearchMode
    Dim m_UseSquareBrackets As Boolean
    Dim m_PreviewField As String

#End Region

#Region "Properties"

    Public ReadOnly Property ReturnValue() As String
        Get
            Return m_ReturnValue
        End Get
    End Property

    Public Property Caption() As String
        Get
            Return m_Caption
        End Get
        Set(ByVal value As String)
            m_Caption = value
        End Set
    End Property

    Public Property PopulateOnLoad() As Boolean
        Get
            Return m_PopulateOnLoad
        End Get
        Set(ByVal value As Boolean)
            m_PopulateOnLoad = value
        End Set
    End Property

    Public Property ParentForm() As Form
        Get
            Return m_ParentForm
        End Get
        Set(ByVal value As Form)
            m_ParentForm = value
        End Set
    End Property

    Public Property GridSQL() As String
        Get
            Return m_GridSQL
        End Get
        Set(ByVal value As String)
            m_GridSQL = value
        End Set
    End Property

    ''' <summary>
    ''' Ensure the clause begins with 'and' not 'where'
    ''' </summary>
    ''' <returns></returns>
    Public Property GridWhereClause As String
        Get
            Return m_GridWhereClause
        End Get
        Set(ByVal value As String)
            m_GridWhereClause = value
        End Set
    End Property

    Public Property GridOrderBy() As String
        Get
            Return m_GridOrderBy
        End Get
        Set(ByVal value As String)
            m_GridOrderBy = value
        End Set
    End Property

    Public Property ReturnField() As String
        Get
            Return m_ReturnField
        End Get
        Set(ByVal value As String)
            m_ReturnField = value
        End Set
    End Property

    Public Property Option1Field() As String
        Get
            Return m_Option1Field
        End Get
        Set(ByVal value As String)
            m_Option1Field = value
        End Set
    End Property

    Public Property Option1Caption() As String
        Get
            Return m_Option1Caption
        End Get
        Set(ByVal value As String)
            m_Option1Caption = value
        End Set
    End Property

    Public Property Option2Field() As String
        Get
            Return m_Option2Field
        End Get
        Set(ByVal value As String)
            m_Option2Field = value
        End Set
    End Property

    Public Property Option2Caption() As String
        Get
            Return m_Option2Caption
        End Get
        Set(ByVal value As String)
            m_Option2Caption = value
        End Set
    End Property

    Public Property Option3Field() As String
        Get
            Return m_Option3Field
        End Get
        Set(ByVal value As String)
            m_Option3Field = value
        End Set
    End Property

    Public Property Option3Caption() As String
        Get
            Return m_Option3Caption
        End Get
        Set(ByVal value As String)
            m_Option3Caption = value
        End Set
    End Property

    Public Property Option4Field() As String
        Get
            Return m_Option4Field
        End Get
        Set(ByVal value As String)
            m_Option4Field = value
        End Set
    End Property

    Public Property Option4Caption() As String
        Get
            Return m_Option4Caption
        End Get
        Set(ByVal value As String)
            m_Option4Caption = value
        End Set
    End Property

    Public Property LookupSQL() As String
        Get
            Return m_LookupSQL
        End Get
        Set(ByVal value As String)
            m_LookupSQL = value
        End Set
    End Property

    Public Property LookupReturn() As String
        Get
            Return m_LookupReturn
        End Get
        Set(ByVal value As String)
            m_LookupReturn = value
        End Set
    End Property

    Public Property LookupCode() As String
        Get
            Return m_LookupCode
        End Get
        Set(ByVal value As String)
            m_LookupCode = value
        End Set
    End Property

    Public Property LookupDescription() As String
        Get
            Return m_LookupDescription
        End Get
        Set(ByVal value As String)
            m_LookupDescription = value
        End Set
    End Property

    Public Property ConnectionString() As String
        Get
            Return m_ConnectionString
        End Get
        Set(ByVal value As String)
            m_ConnectionString = value
        End Set
    End Property

    Public Property HideFirstColumn() As Boolean
        Get
            Return m_HideFirstColumn
        End Get
        Set(ByVal value As Boolean)
            m_HideFirstColumn = value
        End Set
    End Property

    Public Property FormHeight As Integer
        Get
            Return m_FormHeight
        End Get
        Set(value As Integer)
            m_FormHeight = value
        End Set
    End Property

    Public Property FormWidth As Integer
        Get
            Return m_FormWidth
        End Get
        Set(value As Integer)
            m_FormWidth = value
        End Set
    End Property

    Public Property SearchMode As EnumSearchMode
        Get
            Return m_SearchMode
        End Get
        Set(value As EnumSearchMode)
            m_SearchMode = value
        End Set
    End Property

    Public Property UseSquareBrackets() As Boolean
        Get
            Return m_UseSquareBrackets
        End Get
        Set(ByVal value As Boolean)
            m_UseSquareBrackets = value
        End Set
    End Property

    Public Property PreviewField() As String
        Get
            Return m_PreviewField
        End Get
        Set(ByVal value As String)
            m_PreviewField = value
        End Set
    End Property

    Public Property ShowAutoFilterRow As Boolean

#End Region

    Public Sub Show()

        Dim frmFind As New frmGenericFind(m_Caption, m_PopulateOnLoad, m_ReturnField, m_ConnectionString,
                                          m_GridSQL, m_GridWhereClause, m_GridOrderBy,
                                          m_HideFirstColumn, m_SearchMode,
                                          m_Option1Field, m_Option1Caption, m_Option2Field, m_Option2Caption,
                                          m_Option3Field, m_Option3Caption, m_Option4Field, m_Option4Caption, FormWidth, FormHeight, m_UseSquareBrackets, m_PreviewField, ShowAutoFilterRow)

        If m_ConnectionString = "" Then
            Msgbox("Error: Connection String not set.", MessageBoxIcon.Exclamation, "GenericFind.Show")
            Exit Sub
        End If

        m_ReturnValue = ""
        Cursor.Current = Cursors.WaitCursor

        frmFind.Owner = m_ParentForm
        frmFind.ShowDialog()

        If Not frmFind.Tag Is Nothing Then
            m_ReturnValue = frmFind.Tag.ToString
        End If

        Cursor.Current = Cursors.Default

    End Sub

End Class
