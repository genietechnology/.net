﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Partial Public Class User

        Private m_Authenticated As Boolean = False
        Private m_AuthenticationError As String = ""

#Region "Properties"

        Public ReadOnly Property Authenticated() As Boolean
            Get
                Return m_Authenticated
            End Get
        End Property

        Public ReadOnly Property AuthenticationError() As String
            Get
                Return m_AuthenticationError
            End Get
        End Property


#End Region

        Public Shared Function Find() As Business.User

            Dim _Find As New GenericFind

            Dim _SQL As String = "select username as 'Username', fullname as 'Name', type as 'Type' from AppUsers"
            Dim _Where As String = ""

            With _Find
                .Caption = "Select User"
                .PopulateOnLoad = True
                .ConnectionString = Session.ConnectionString
                .GridSQL = _SQL
                .GridWhereClause = _Where
                .GridOrderBy = "order by fullname"
                .ReturnField = "ID"
                .Show()
            End With

            If _Find.ReturnValue = "" Then
                Return Nothing
            Else
                Return Business.User.RetreiveByID(New Guid(_Find.ReturnValue))
            End If

        End Function

        Public Shared Sub CreateAdmin()

            Dim _User As User = RetrieveByName("admin")
            If _User Is Nothing Then
                _User = New User
                With _User
                    ._ID = Guid.NewGuid
                    ._Username = "admin"
                    ._Password = EncyptionHandler.EncryptString("admin")
                    ._Type = "Super Administrator"
                    ._Forename = ""
                    ._Surname = ""
                    ._Fullname = "Super Administrator"
                    ._JobTitle = ""
                    ._Email = ""
                    ._Department = ""
                    ._Tel = ""
                End With
                User.SaveRecord(_User)
            End If

        End Sub

        Public Shared Function GetUserNamebyID(ByVal UserID As Guid) As String

            Dim _Return As String = ""
            Dim _User As User = Business.User.RetreiveByID(UserID)

            If _User IsNot Nothing Then _Return = _User._Fullname

            _User = Nothing
            Return _Return

        End Function

        Public Shared Function RetrieveByName(ByVal UserName As String) As Business.User

            Dim _User As User = Nothing
            Dim _DR As DataRow = DAL.GetRowfromSPwithParam(Session.ConnectionString, "getUserbyName", "@UserName", SqlDbType.VarChar, UserName)
            If _DR IsNot Nothing Then
                _User = PropertiesFromData(_DR)
            End If

            Return _User

        End Function

        Public Sub Authenticate(ByVal UserName As String, ByVal Password As String)

            Dim _Pwd As String = EncyptionHandler.DecryptString(Me._Password)
            If _Pwd <> "" Then
                If _Pwd = Password Then
                    If _Disabled = True Then
                        m_Authenticated = False
                        m_AuthenticationError = "Account is disabled."
                    Else

                        Dim _passwordExpiry As Integer = ParameterHandler.ReturnInteger("PWDEXPIRY", False)
                        If Not PasswordHandler.PasswordExpired(CType(Me._ID, Guid), _passwordExpiry) Then
                            CareMessage("You password has expired. Please enter a new password.", MessageBoxIcon.Exclamation, "Password Expired")
                            ChangePassword(True)
                        End If

                        m_Authenticated = True
                        m_AuthenticationError = ""
                    End If
                Else
                    m_Authenticated = False
                    m_AuthenticationError = "Invalid password."
                End If
            Else
                m_Authenticated = False
                m_AuthenticationError = "Password could not be decrypted."
            End If

        End Sub

        Public Function ChangePassword(IsReset As Boolean) As Boolean

            Dim _frm As New Care.Shared.frmChangePassword
            _frm.IsReset = IsReset
            _frm.ShowDialog()

            If _frm.DialogResult = DialogResult.OK Then
                Return True
            Else
                Return False
            End If

            _frm = Nothing

        End Function

    End Class

End Namespace