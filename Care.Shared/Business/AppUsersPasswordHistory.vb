﻿Imports Care.Global
Imports Care.Data

Namespace Business

    Partial Public Class AppUsersPasswordHistory

        Public Shared Function RetrieveByUserID(ByVal UserID As Guid) As List(Of AppUsersPasswordHistory)

            Dim _sql As String = "select * from AppUsersPasswordHistory where user_id = '" & UserID.ToString & "'"
            Dim _passwordHistory As List(Of AppUsersPasswordHistory)
            _passwordHistory = PopulateList(DAL.GetDataTablefromSQL(Session.ConnectionString, _sql))
            Return _passwordHistory

        End Function

        Public Shared Function RetrieveLastPassword(ByVal UserID As Guid) As AppUsersPasswordHistory

            Dim _sql As String = "select top 1 * from AppUsersPasswordHistory where user_id = '" & UserID.ToString & "'" &
                                    " order by last_used desc"

            Dim _row As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _sql)
            If _row IsNot Nothing Then
                Dim _Password As AppUsersPasswordHistory = PropertiesFromData(_row)
                Return _Password
            End If

            Return Nothing

        End Function

    End Class

End Namespace