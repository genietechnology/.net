﻿Imports Care.Global
Imports Care.Data

Public Class DBMaintenance

    Public Sub RebuildIndexes()

        Dim _SQL As String = ""

        _SQL += "SELECT OBJECT_NAME(ddips.object_id) AS TableName, avg(ddips.avg_fragmentation_in_percent)"
        _SQL += " FROM sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL, NULL, NULL) AS ddips"
        _SQL += " INNER JOIN sys.indexes As i On ddips.index_id = i.index_id And ddips.object_id = i.object_id"
        _SQL += " WHERE i.is_primary_key = 0"
        _SQL += " AND i.object_id > 255"
        _SQL += " AND I.name like '%IX_%'"
        _SQL += " group by OBJECT_NAME(ddips.object_id)"
        _SQL += " having avg(ddips.avg_fragmentation_in_percent) > 20"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows
                Rebuild(_DR.Item("TableName").ToString)
            Next

            _DT.Dispose()
            _DT = Nothing

        End If

    End Sub

    Private Sub Rebuild(ByVal TableName As String)
        Dim _SQL As String = "ALTER INDEX ALL ON " + TableName + " REBUILD"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)
    End Sub

End Class
