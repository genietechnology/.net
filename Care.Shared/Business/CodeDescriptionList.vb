﻿Imports Care.Global

Public Class CodeDescriptionList

    Inherits List(Of Pair)

    Public Overloads Sub Add(ByVal Code As String, ByVal Description As String)
        MyBase.Add(New Pair(Code, Description))
    End Sub

End Class
