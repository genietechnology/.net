﻿Imports Care.Global
Imports DevExpress.XtraBars

Public Class frmBaseForm

    Private m_Maximised As Boolean = False
    Private m_Captions As New List(Of Dictionary.ControlCaption)
    Public Property ReadOnlyPermissions As Boolean

    Public Property LoadMaximised As Boolean
        Get
            Return m_Maximised
        End Get
        Set(value As Boolean)
            m_Maximised = value
        End Set
    End Property

    Private Sub frmBaseForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        KernelLog(Me.Name + " - Load", DebugLogItem.EnumDebugType.LineBreakFirst)

        KernelLog("AuditFormLoad", DebugLogItem.EnumDebugType.BeginProcess)
        AuditFormLoad()

        KernelLog("ExtractTags")
        ExtractTags()

        KernelLog("SetSpellChecking")
        SetSpellChecking()

        KernelLog("CacheCaptions")
        CacheCaptions()

        KernelLog("SetCaptions")
        SetCaptions(Me)

        KernelLog("SetPopupMenus")
        'SetPopupMenu(Me)

        KernelLog("AuditFormLoad", DebugLogItem.EnumDebugType.EndProcess)

    End Sub

    Private Sub AuditFormLoad()
        Try
            AuditHandler.LogAudit(AuditHandler.EnumAuditScope.Form, Me.Name, Nothing, Me.Text, AuditHandler.EnumAuditStatus.Success)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub CacheCaptions()

        If Session.DictionaryControlCaptions.Count = 0 Then Exit Sub

        Dim _Q As IEnumerable(Of Dictionary.ControlCaption)

        _Q = From _c As Dictionary.ControlCaption In Session.DictionaryControlCaptions
             Where _c.FormName = Me.Name

        If _Q IsNot Nothing Then
            For Each _C In _Q
                m_Captions.Add(_C)
            Next
        End If

    End Sub

    Private Sub SetCaptions(ByVal ControlIn As Control)

        If m_Captions.Count = 0 Then Exit Sub

        For Each _c As Control In ControlIn.Controls

            If _c.Name <> "" Then

                If TypeOf (_c) Is Care.Controls.CareLabel Then
                    SetCaptionText(_c)
                Else
                    If TypeOf (_c) Is Care.Controls.CareFrame Then
                        SetCaptionText(_c)
                        SetCaptions(_c)
                    Else
                        If _c.HasChildren Then
                            SetCaptions(_c)
                        End If
                    End If
                End If

            End If

        Next

    End Sub

    Private Sub SetCaptionText(ByRef ControlIn As Control)
        For Each _dc In m_Captions
            If _dc.ControlName = ControlIn.Name Then
                If _dc.ControlCaption <> ControlIn.Text Then
                    ControlIn.AccessibleName = ControlIn.Text
                    ControlIn.Text = _dc.ControlCaption
                End If
            End If
        Next
    End Sub

    Private Sub SetSpellChecking()
        If Session IsNot Nothing AndAlso Session.SpellCheck Then
            SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.AsYouType
        Else
            SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        End If
    End Sub

    Protected Overridable Sub KeyHandler(ByVal e As KeyEventArgs)

    End Sub

    Private Sub frmBaseForm_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        If Not m_Maximised AndAlso Me.WindowState = FormWindowState.Maximized Then
            Me.WindowState = FormWindowState.Normal
        End If
    End Sub

    Private Sub frmBaseForm_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles Me.KeyDown

        If ActiveForm Is Nothing Then Exit Sub

        'no modifiers
        '**************************************************************************************************************************
        If e.Shift = False AndAlso e.Control = False Then

            Select Case e.KeyCode

                Case Keys.Enter, Keys.Return

                    Dim _TabNext As Boolean = True

                    If ActiveControl Is Nothing Then Exit Sub

                    If TypeOf ActiveControl Is Care.Address.CareAddress Then Exit Sub
                    If TypeOf ActiveControl Is Care.Controls.CareRichText Then Exit Sub
                    If TypeOf ActiveControl Is DevExpress.XtraEditors.MemoEdit Then Exit Sub

                    If TypeOf ActiveControl Is TextBox Then
                        Dim _txt As TextBox = DirectCast(ActiveControl, TextBox)
                        If _txt.Multiline Then Exit Sub
                    End If

                    If _TabNext Then
                        e.Handled = True
                        e.SuppressKeyPress = True
                        Me.SelectNextControl(Me.ActiveControl, True, True, True, True)
                    End If

            End Select

        Else

            'shift modifier
            '**************************************************************************************************************************
            If e.Shift = True AndAlso e.Control = False Then

                Select Case e.KeyCode

                    'form identifier
                    Case Keys.F1
                        Dim _Mess As String = Me.Name + vbNewLine + Me.Width.ToString + " x " + Me.Height.ToString
                        Msgbox(_Mess, MessageBoxIcon.Information, "Form Information")
                        Exit Sub

                    'field identifier
                    Case Keys.F2

                        If ActiveControl IsNot Nothing Then

                            Msgbox("Name: " + ActiveControl.Name + vbCrLf +
                                   "Tag: " + IIf(ActiveControl.Tag Is Nothing, "", ActiveControl.Tag).ToString + vbCrLf +
                                   "AccessibleName: " + IIf(ActiveControl.AccessibleName Is Nothing, "", ActiveControl.AccessibleName).ToString, MessageBoxIcon.Information, "Field Information")

                            Exit Sub

                        End If

                    'list maintenance
                    Case Keys.F3

                        If TypeOf (ActiveControl) Is Care.Controls.CareComboBox Then
                            Dim _c As Care.Controls.CareComboBox = CType(ActiveControl, Care.Controls.CareComboBox)
                            If _c.Enabled = True Then
                                If _c.ListName <> "" Then
                                    If ListHandler.DrillDown(_c.ListName) Then
                                        _c.PopulateFromListMaintenance(ListHandler.ReturnListFromCache(_c.ListName))
                                        _c.SelectedIndex = -1
                                    End If
                                End If
                            End If

                            Exit Sub

                        End If

                        If TypeOf (ActiveControl) Is Care.Controls.CareCheckedComboBox Then
                            Dim _c As Care.Controls.CareCheckedComboBox = CType(ActiveControl, Care.Controls.CareCheckedComboBox)
                            If _c.Enabled = True Then
                                If _c.ListName <> "" Then
                                    If ListHandler.DrillDown(_c.ListName) Then
                                        _c.PopulateFromListMaintenance(ListHandler.ReturnListFromCache(_c.ListName))
                                    End If
                                End If
                            End If

                            Exit Sub

                        End If

                End Select

            Else

                'control & shift modifier
                '**************************************************************************************************************************
                If e.Control = True AndAlso e.Shift = True Then

                    Select Case e.KeyCode

                        Case Keys.F11
                            KernelShow()

                        'control debugger
                        Case Keys.F12

                            MyControls.DebugPopup(Me)

                            Dim _Mode As String = InputBox("N = Name" & vbCrLf &
                                                           "T = Tag" & vbCrLf &
                                                           "M = Maxlength", "Select Debug Mode", "")

                            If _Mode = "N" Or _Mode = "n" Then MyControls.SetControls(ControlHandler.Mode.Locked, Me.Controls, ControlHandler.DebugMode.ControlName)
                            If _Mode = "T" Or _Mode = "t" Then MyControls.SetControls(ControlHandler.Mode.Locked, Me.Controls, ControlHandler.DebugMode.Tag)
                            If _Mode = "M" Or _Mode = "m" Then MyControls.SetControls(ControlHandler.Mode.Locked, Me.Controls, ControlHandler.DebugMode.MaxLength)

                            Exit Sub

                    End Select

                End If

                '**************************************************************************************************************************

            End If

        End If

        KeyHandler(e)

    End Sub

    Private Sub ExtractTags()
        If Me.Tag IsNot Nothing Then
            Dim _Tag As String = Me.Tag.ToString
            If _Tag.Contains("R") Then Me.ReadOnlyPermissions = True
            Me.Tag = Nothing
        End If
    End Sub

End Class