﻿Imports Care.Global

Public Class frmGridMaintenance

    Private m_GridSQL As String

    Public Property GridSQL() As String
        Get
            Return m_GridSQL
        End Get
        Set(ByVal value As String)
            m_GridSQL = value
            SetGrid()
        End Set
    End Property

#Region "Overridable Methods"

    Protected Overridable Sub FormatGrid()

    End Sub

    Protected Overridable Sub CommitUpdate()

    End Sub

    Protected Overridable Sub CommitDelete(ByVal ID As Guid)

    End Sub

    Protected Overridable Sub BindToID(ByVal ID As Guid, ByVal IsNew As Boolean)

    End Sub

    Protected Overridable Sub SetBindings()

    End Sub

    Protected Overridable Function BeforeAdd() As Boolean
        Return True
    End Function

    Protected Overridable Function BeforeEdit() As Boolean
        Return True
    End Function

    Protected Overridable Function BeforeDelete() As Boolean
        Return True
    End Function

    Protected Overridable Function BeforeCommitUpdate() As Boolean
        Return True
    End Function

    Protected Overridable Sub AfterAdd()

    End Sub

    Protected Overridable Sub AfterEdit()

    End Sub

#End Region

    Private Sub frmGridMaintenance_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        SetBindings()
        MyControls.SetControls(ControlHandler.Mode.Locked, Me.Controls)
        SetButtons()

    End Sub

    Protected Sub SetGrid()
        Grid.HideFirstColumn = True
        Grid.Populate(Session.ConnectionString, m_GridSQL)
        FormatGrid()
        SetButtons()
    End Sub

    Private Sub SetButtons()

        If Grid.RecordCount > 0 Then
            btnEdit.Enabled = True
            btnDelete.Enabled = True
        Else
            btnEdit.Enabled = False
            btnDelete.Enabled = False
        End If

    End Sub

    Private Sub AddRecord()
        If Not BeforeAdd() Then Exit Sub
        gbx.Show()
        MyControls.SetControls(ControlHandler.Mode.Add, Me.Controls)
        Me.SelectNextControl(Me, True, True, True, False)
        BindToID(New Guid, True)
        AfterAdd()
    End Sub

    Private Sub EditRecord()

        If Not BeforeEdit() Then Exit Sub

        Dim _GUID As Guid? = GetSelectedRecordID()
        If _GUID.HasValue Then

            Grid.PersistRowPosition()

            BindToID(_GUID.Value, False)
            gbx.Show()
            MyControls.SetControls(ControlHandler.Mode.Edit, Me.Controls)
            Me.SelectNextControl(Me, True, True, True, False)

            AfterEdit()

        End If

    End Sub

    Private Function GetSelectedRecordID() As Guid?

        If Grid.RecordCount = 0 Then Return Nothing

        Dim _Return As Guid? = Nothing

        Try
            If Grid.CurrentRow.Item(0).ToString <> "" Then
                _Return = New Guid(Grid.CurrentRow.Item(0).ToString)
            End If

        Catch ex As Exception

        End Try

        Return _Return

    End Function

    Private Sub DeleteRecord()

        If Not BeforeDelete() Then Exit Sub

        If CareMessage("Are you sure you want to Delete this record?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Delete Record") = MsgBoxResult.Yes Then
            Dim _GUID As Guid? = GetSelectedRecordID()
            If _GUID.HasValue Then
                CommitDelete(_GUID.Value)
                SetGrid()
                SetButtons()
            End If
        End If

    End Sub

#Region "Control Events"

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        AddRecord()
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        EditRecord()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        DeleteRecord()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Cancel()
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        OK()
    End Sub

    Protected Sub OK()

        If Not BeforeCommitUpdate() Then Exit Sub

        If MyControls.Validate(Me.Controls) Then

            bs.EndEdit()
            CommitUpdate()

            MyControls.SetControls(ControlHandler.Mode.Locked, Me.Controls)

            SetGrid()

            Grid.RestoreRowPosition()

            SetButtons()
            gbx.Hide()

        End If

    End Sub

    Protected Sub Cancel()
        MyControls.SetControls(ControlHandler.Mode.Locked, Me.Controls)
        SetButtons()
        gbx.Hide()
    End Sub

    Private Sub Grid_GridDoubleClick(sender As Object, e As System.EventArgs) Handles Grid.GridDoubleClick
        If Grid.RecordCount > 0 Then EditRecord()
    End Sub

    Private Sub ug_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs)
        'If e.Button = MouseButtons.Right Then
        '    Dim _GH As New GridHandler
        '    _GH.Popup(ug, e.Location)
        'End If
    End Sub

#End Region

End Class