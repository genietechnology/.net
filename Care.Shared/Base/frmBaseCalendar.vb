﻿Imports Care.Global
Imports System.Data.SqlClient
Imports DevExpress.XtraScheduler

Public Class frmBaseCalendar

        ' Modify this string if required to connect to your database.
    Private SchedulerDBConnection As String = ""

    Private DXSchedulerDataset As New DataTable
    Private WithEvents AppointmentDataAdapter As SqlDataAdapter
    Private DXSchedulerConn As SqlConnection

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        If Session Is Nothing Then Exit Sub
        If Session.ConnectionString = "" Then Exit Sub

        'Me.SchedulerStorage1.Appointments.CommitIdToDataSource = False
        SchedulerDBConnection = Session.ConnectionString
        Me.SchedulerControl1.Start = DateTime.Today

        Dim selectAppointments As String = "SELECT * FROM Appointments"
        DXSchedulerConn = New SqlConnection(SchedulerDBConnection)
        DXSchedulerConn.Open()

        AppointmentDataAdapter = New SqlDataAdapter(selectAppointments, DXSchedulerConn)
        AppointmentDataAdapter.Fill(DXSchedulerDataset)

        ' Specify mappings.
        MapAppointmentData()

        ' Generate commands using CommandBuilder.  
        Dim cmdBuilder As New SqlCommandBuilder(AppointmentDataAdapter)
        AppointmentDataAdapter.InsertCommand = cmdBuilder.GetInsertCommand()
        AppointmentDataAdapter.DeleteCommand = cmdBuilder.GetDeleteCommand()
        AppointmentDataAdapter.UpdateCommand = cmdBuilder.GetUpdateCommand()

        DXSchedulerConn.Close()

        Me.SchedulerStorage1.Appointments.DataSource = DXSchedulerDataset
        'Me.schedulerStorage1.Appointments.DataMember = "Appointments"

    End Sub

    Private Sub MapAppointmentData()

        Me.SchedulerStorage1.Appointments.Mappings.Start = "start_date"
        Me.SchedulerStorage1.Appointments.Mappings.End = "end_date"
        Me.SchedulerStorage1.Appointments.Mappings.Subject = "subject"

        Me.SchedulerStorage1.Appointments.Mappings.Status = "status"
        Me.SchedulerStorage1.Appointments.Mappings.Type = "type"
        Me.SchedulerStorage1.Appointments.Mappings.Label = "label"
        Me.SchedulerStorage1.Appointments.Mappings.Location = "location"
        Me.SchedulerStorage1.Appointments.Mappings.AllDay = "allday"

        Me.SchedulerStorage1.Appointments.Mappings.Description = "description"
        Me.SchedulerStorage1.Appointments.Mappings.RecurrenceInfo = "recurrence_details"
        Me.SchedulerStorage1.Appointments.Mappings.ReminderInfo = "reminder_details"

        'Me.schedulerStorage1.Appointments.Mappings.ResourceId = "ResourceIDs"
        Me.SchedulerStorage1.Appointments.CustomFieldMappings.Add(New AppointmentCustomFieldMapping("AppointmentID", "ID"))
        Me.SchedulerStorage1.Appointments.CustomFieldMappings.Add(New AppointmentCustomFieldMapping("LinkForm", "link_form"))
        Me.SchedulerStorage1.Appointments.CustomFieldMappings.Add(New AppointmentCustomFieldMapping("LinkID", "link_id"))

    End Sub

    ' Store modified data in the database
    Private Sub AppointmentUpdate(ByVal sender As Object, ByVal e As PersistentObjectsEventArgs)
        AppointmentDataAdapter.Update(DXSchedulerDataset)
        DXSchedulerDataset.AcceptChanges()
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        ' Subscribe to Storage events required for updating the data source. 
        AddHandler Me.SchedulerStorage1.AppointmentsInserted, AddressOf AppointmentUpdate
        AddHandler Me.SchedulerStorage1.AppointmentsChanged, AddressOf AppointmentUpdate
        AddHandler Me.SchedulerStorage1.AppointmentsDeleted, AddressOf AppointmentUpdate

    End Sub

    Private Sub AppointmentDataAdapter_RowUpdating(sender As Object, e As SqlRowUpdatingEventArgs) Handles AppointmentDataAdapter.RowUpdating
        If e.StatementType <> StatementType.Delete Then
            If e.Row("ID").ToString = "" Then e.Row("ID") = Guid.NewGuid
        End If
    End Sub

    Private Sub SchedulerControl1_EditAppointmentFormShowing(sender As Object, e As AppointmentFormEventArgs) Handles SchedulerControl1.EditAppointmentFormShowing

        Dim scheduler As DevExpress.XtraScheduler.SchedulerControl = CType(sender, DevExpress.XtraScheduler.SchedulerControl)
        Dim form As Care.[Shared].frmNewAppointment = New Care.[Shared].frmNewAppointment(scheduler, e.Appointment, e.OpenRecurrenceForm)
        Try
            e.DialogResult = form.ShowDialog
            e.Handled = True
        Finally
            form.Dispose()
        End Try

    End Sub

End Class