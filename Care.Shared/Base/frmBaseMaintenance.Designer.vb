﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBaseMaintenance
    Inherits frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBaseMaintenance))
        Me.bs = New System.Windows.Forms.BindingSource()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager()
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.tlFind = New DevExpress.XtraBars.BarButtonItem()
        Me.tlAdd = New DevExpress.XtraBars.BarButtonItem()
        Me.tlEdit = New DevExpress.XtraBars.BarButtonItem()
        Me.tlDelete = New DevExpress.XtraBars.BarButtonItem()
        Me.tlAccept = New DevExpress.XtraBars.BarButtonItem()
        Me.tlCancel = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiAudit = New DevExpress.XtraBars.BarButtonItem()
        Me.tlPrint = New DevExpress.XtraBars.BarButtonItem()
        Me.tlNotes = New DevExpress.XtraBars.BarButtonItem()
        Me.tlAttach = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.Panel1 = New DevExpress.XtraEditors.PanelControl()
        Me.btnCancel = New Care.Controls.CareButton()
        Me.btnOK = New Care.Controls.CareButton()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.PrintPreviewDialog1 = New System.Windows.Forms.PrintPreviewDialog()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.tlNotesAvailable = New DevExpress.XtraBars.BarButtonItem()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'BarManager1
        '
        Me.BarManager1.AllowCustomization = False
        Me.BarManager1.AllowMoveBarOnToolbar = False
        Me.BarManager1.AllowQuickCustomization = False
        Me.BarManager1.AllowShowToolbarsPopup = False
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.tlFind, Me.tlAdd, Me.tlEdit, Me.tlDelete, Me.tlAccept, Me.tlCancel, Me.tlPrint, Me.tlAttach, Me.tlNotes, Me.bbiAudit, Me.tlNotesAvailable})
        Me.BarManager1.MaxItemId = 12
        Me.BarManager1.UseF10KeyForMenu = False
        '
        'Bar1
        '
        Me.Bar1.BarName = "Tools"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.tlFind), New DevExpress.XtraBars.LinkPersistInfo(Me.tlAdd, True), New DevExpress.XtraBars.LinkPersistInfo(Me.tlEdit), New DevExpress.XtraBars.LinkPersistInfo(Me.tlDelete), New DevExpress.XtraBars.LinkPersistInfo(Me.tlAccept, True), New DevExpress.XtraBars.LinkPersistInfo(Me.tlCancel), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.bbiAudit, DevExpress.XtraBars.BarItemPaintStyle.Standard), New DevExpress.XtraBars.LinkPersistInfo(Me.tlPrint), New DevExpress.XtraBars.LinkPersistInfo(Me.tlNotes), New DevExpress.XtraBars.LinkPersistInfo(Me.tlAttach)})
        Me.Bar1.OptionsBar.AllowQuickCustomization = False
        Me.Bar1.OptionsBar.DisableCustomization = True
        Me.Bar1.OptionsBar.DrawDragBorder = False
        Me.Bar1.OptionsBar.UseWholeRow = True
        Me.Bar1.Text = "Tools"
        '
        'tlFind
        '
        Me.tlFind.Caption = "Find Record"
        Me.tlFind.Id = 0
        Me.tlFind.ImageOptions.Image = Global.Care.[Shared].My.Resources.Resources.find
        Me.tlFind.Name = "tlFind"
        '
        'tlAdd
        '
        Me.tlAdd.Caption = "Add Record"
        Me.tlAdd.Id = 1
        Me.tlAdd.ImageOptions.Image = Global.Care.[Shared].My.Resources.Resources.add
        Me.tlAdd.Name = "tlAdd"
        '
        'tlEdit
        '
        Me.tlEdit.Caption = "Edit Record"
        Me.tlEdit.Id = 2
        Me.tlEdit.ImageOptions.Image = Global.Care.[Shared].My.Resources.Resources.edit
        Me.tlEdit.Name = "tlEdit"
        '
        'tlDelete
        '
        Me.tlDelete.Caption = "Delete Record"
        Me.tlDelete.Id = 3
        Me.tlDelete.ImageOptions.Image = Global.Care.[Shared].My.Resources.Resources.delete
        Me.tlDelete.Name = "tlDelete"
        '
        'tlAccept
        '
        Me.tlAccept.Caption = "Accept Changes"
        Me.tlAccept.Id = 4
        Me.tlAccept.ImageOptions.Image = Global.Care.[Shared].My.Resources.Resources.accept
        Me.tlAccept.Name = "tlAccept"
        '
        'tlCancel
        '
        Me.tlCancel.Caption = "Cancel Changes"
        Me.tlCancel.Id = 5
        Me.tlCancel.ImageOptions.Image = Global.Care.[Shared].My.Resources.Resources.cancel
        Me.tlCancel.Name = "tlCancel"
        '
        'bbiAudit
        '
        Me.bbiAudit.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.bbiAudit.Caption = "Audit Trail"
        Me.bbiAudit.Id = 9
        Me.bbiAudit.ImageOptions.Image = Global.Care.[Shared].My.Resources.Resources.info_32
        Me.bbiAudit.Name = "bbiAudit"
        '
        'tlPrint
        '
        Me.tlPrint.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.tlPrint.Caption = "Print Screen"
        Me.tlPrint.Id = 6
        Me.tlPrint.ImageOptions.Image = Global.Care.[Shared].My.Resources.Resources.print
        Me.tlPrint.Name = "tlPrint"
        '
        'tlNotes
        '
        Me.tlNotes.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.tlNotes.Caption = "Notes"
        Me.tlNotes.Id = 8
        Me.tlNotes.ImageOptions.Image = Global.Care.[Shared].My.Resources.Resources.notes
        Me.tlNotes.Name = "tlNotes"
        '
        'tlAttach
        '
        Me.tlAttach.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.tlAttach.Caption = "Document Management"
        Me.tlAttach.Id = 7
        Me.tlAttach.ImageOptions.Image = Global.Care.[Shared].My.Resources.Resources.attachment
        Me.tlAttach.Name = "tlAttach"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Manager = Me.BarManager1
        Me.barDockControlTop.Size = New System.Drawing.Size(832, 47)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 261)
        Me.barDockControlBottom.Manager = Me.BarManager1
        Me.barDockControlBottom.Size = New System.Drawing.Size(832, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 47)
        Me.barDockControlLeft.Manager = Me.BarManager1
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 214)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(832, 47)
        Me.barDockControlRight.Manager = Me.BarManager1
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 214)
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.Panel1.Controls.Add(Me.btnCancel)
        Me.Panel1.Controls.Add(Me.btnOK)
        Me.Panel1.Location = New System.Drawing.Point(0, 225)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(832, 36)
        Me.Panel1.TabIndex = 0
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.CausesValidation = False
        Me.btnCancel.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(735, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 25)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Tag = "Y"
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(644, 3)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(85, 25)
        Me.btnOK.TabIndex = 0
        Me.btnOK.Text = "OK"
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.BarButtonItem1.Caption = "Print Screen"
        Me.BarButtonItem1.Id = 6
        Me.BarButtonItem1.ImageOptions.Image = Global.Care.[Shared].My.Resources.Resources.print
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'PrintPreviewDialog1
        '
        Me.PrintPreviewDialog1.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.ClientSize = New System.Drawing.Size(400, 300)
        Me.PrintPreviewDialog1.Enabled = True
        Me.PrintPreviewDialog1.Icon = CType(resources.GetObject("PrintPreviewDialog1.Icon"), System.Drawing.Icon)
        Me.PrintPreviewDialog1.Name = "PrintPreviewDialog1"
        Me.PrintPreviewDialog1.Visible = False
        '
        'PrintDocument1
        '
        '
        'tlNotesAvailable
        '
        Me.tlNotesAvailable.Caption = "Notes Available"
        Me.tlNotesAvailable.Id = 11
        Me.tlNotesAvailable.Name = "tlNotesAvailable"
        '
        'frmBaseMaintenance
        '
        Me.AcceptButton = Me.btnOK
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(832, 261)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.MaximizeBox = False
        Me.Name = "frmBaseMaintenance"
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Protected WithEvents bs As BindingSource

    Public Sub New()

        Me.SetStyle(ControlStyles.SupportsTransparentBackColor, True)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents tlFind As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents tlAdd As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents tlEdit As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents tlDelete As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents tlAccept As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents tlCancel As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents tlPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents tlAttach As DevExpress.XtraBars.BarButtonItem
    Protected WithEvents btnCancel As Care.Controls.CareButton
    Protected WithEvents btnOK As Care.Controls.CareButton
    Protected WithEvents Panel1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents tlNotes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PrintPreviewDialog1 As PrintPreviewDialog
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents bbiAudit As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents tlNotesAvailable As DevExpress.XtraBars.BarButtonItem
End Class
