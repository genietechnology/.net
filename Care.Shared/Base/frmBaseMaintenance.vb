﻿Imports System.Drawing
Imports Care.Global
Imports Care.Data

Public Class frmBaseMaintenance

    Public Enum ToolbarEnum
        Standard
        NewOnly
        EditOnly
        FindOnly
        FindandNew
        FindandEdit
        None
    End Enum

    Private m_PrintScreen As Bitmap

    Private m_Mode As String = ""
    Private m_RecordID As Guid? = Nothing
    Private m_RecordPopulated As Boolean = False
    Private m_ToolbarMode As ToolbarEnum = ToolbarEnum.Standard

    Private m_NotesAvailable As Boolean = False
    Private m_DocumentsAvailable As Boolean = False

#Region "Properties"

    Public Property DeleteMessage As String

    Public Property Mode() As String
        Get
            Return m_Mode
        End Get
        Set(ByVal value As String)
            m_Mode = value
        End Set
    End Property

    Public Property RecordPopulated() As Boolean
        Get
            Return m_RecordPopulated
        End Get
        Set(ByVal value As Boolean)
            m_RecordPopulated = value
        End Set
    End Property

    Public Property RecordID() As Guid?
        Get
            Return m_RecordID
        End Get
        Set(ByVal value As Guid?)
            m_RecordID = value
        End Set
    End Property

    Public Property ToolbarMode() As ToolbarEnum
        Get
            Return m_ToolbarMode
        End Get
        Set(ByVal value As ToolbarEnum)
            m_ToolbarMode = value
            SetToolbar()
        End Set
    End Property

#End Region

    Public Sub DrillDown(ByVal ID As Guid)
        SetBindingsByID(ID)
        SetToolbar()
    End Sub

#Region "Overrideable Methods"

    Protected Overridable Sub AfterAdd()

    End Sub

    Protected Overridable Sub AfterEdit()

    End Sub

    Protected Overridable Sub AfterCommitUpdate()

    End Sub

    Protected Overridable Sub AfterCancelChanges()

    End Sub

    Protected Overridable Sub AfterAcceptChanges()

    End Sub

    Protected Overridable Function BeforeDelete() As Boolean
        Return True
    End Function

    Protected Overridable Function BeforeCommitUpdate() As Boolean
        Return True
    End Function

    Protected Overridable Function BeforeCancelChanges() As Boolean
        Return True
    End Function

    Protected Overridable Sub AddRecord()
        CareMessage("AddRecord must be Overridden in the Inherited Form...", MessageBoxIcon.Asterisk, "Override Required")
    End Sub

    Protected Overridable Sub FindRecord()
        CareMessage("FindRecord must be Overridden in the Inherited Form...", MessageBoxIcon.Asterisk, "Override Required")
    End Sub

    Protected Overridable Sub SetBindings()
        If Not Me.DesignMode Then CareMessage("SetBindings must be Overridden in the Inherited Form...", MessageBoxIcon.Asterisk, "Override Required")
    End Sub

    Protected Overridable Sub SetBindingsByID(ByVal ID As Guid)
        If Not Me.DesignMode Then CareMessage("SetBindingsByID must be Overridden in the Inherited Form...", MessageBoxIcon.Asterisk, "Override Required")
    End Sub

    Protected Overridable Sub CommitUpdate()
        CareMessage("CommitUpdate must be Overridden in the Inherited Form...", MessageBoxIcon.Asterisk, "Override Required")
    End Sub

    Protected Overridable Sub CommitDelete()
        CareMessage("CommitDelete must be Overridden in the Inherited Form...", MessageBoxIcon.Asterisk, "Override Required")
    End Sub

    Protected Overridable Function BeforeAdd() As Boolean
        Return True
    End Function

    Protected Overridable Function BeforeEdit() As Boolean
        Return True
    End Function

#End Region

#Region "Standard Methods"

    Protected Overrides Sub KeyHandler(ByVal e As KeyEventArgs)

        If e.Control And e.KeyCode = Keys.F Then
            ToolFind()
            Exit Sub
        End If

        If e.Control And e.KeyCode = Keys.N Then
            ToolAdd()
            Exit Sub
        End If

        If e.Control And e.KeyCode = Keys.E Then
            ToolEdit()
            Exit Sub
        End If

        If e.Control And e.KeyCode = Keys.D Then
            ToolDelete()
            Exit Sub
        End If

        If e.Control And e.KeyCode = Keys.Enter Then
            ToolAccept()
            Exit Sub
        End If

        If e.KeyCode = Keys.Escape Then
            ToolCancel()
            Exit Sub
        End If

        If e.KeyCode = Keys.F12 Then
            ToolAttach()
            Exit Sub
        End If

        If e.Control And e.KeyCode = Keys.F1 Then
            Dim _Mess As String = ""
            _Mess += "RecordPopulated: " + m_RecordPopulated.ToString
            _Mess += vbCrLf
            _Mess += "RecordID: " + m_RecordID.ToString
            _Mess += vbCrLf
            _Mess += vbCrLf
            _Mess += "Copy RecordID to Clipboard?"
            If CareMessage(_Mess, MessageBoxIcon.Information, MessageBoxButtons.YesNo, "Control-F1") = DialogResult.Yes Then
                Clipboard.Clear()
                Clipboard.SetText(m_RecordID.ToString)
            End If
            Exit Sub
        End If

        If e.Control And e.KeyCode = Keys.P Then
            ToolPrint()
            Exit Sub
        End If

    End Sub

    Private Sub frmBaseMaintenance_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs) Handles Me.FormClosing
        If e.CloseReason = CloseReason.UserClosing Then
            If m_Mode = "ADD" Or m_Mode = "EDIT" Then
                m_Mode += "EXIT"
                CancelChanges()
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub frmBaseMaintenance_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        SetBindings()
        SetToolbar()
        MyControls.SetControls(ControlHandler.Mode.Locked, Me.Controls)
        btnOK.Enabled = False

        If Not DesignMode Then
            bbiAudit.Enabled = Session.CurrentUser.IsSuperAdministrator
        End If

        Cursor.Current = Cursors.Default

    End Sub

    Private Sub DeleteRecord()

        If Not BeforeDelete() Then Exit Sub

        Dim _Message As String = "Are you sure you want to Delete this record?"
        If Me.DeleteMessage IsNot Nothing AndAlso Me.DeleteMessage.ToString.Length > 0 Then
            _Message = Me.DeleteMessage
        End If

        If CareMessage(_Message, MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Delete Record") = MsgBoxResult.Yes Then
            CommitDelete()
            RecordID = Nothing
            RecordPopulated = False
            SetToolbar()
            MyControls.SetControls(ControlHandler.Mode.Blank, Me.Controls)
        End If

    End Sub

    Private Sub AcceptChanges()

        If MyControls.Validate(Me.Controls) Then

            If BeforeCommitUpdate() Then

                Cursor.Current = Cursors.WaitCursor
                btnOK.Enabled = False
                btnCancel.Enabled = False

                bs.EndEdit()

                CommitUpdate()
                AfterCommitUpdate()

                m_Mode = ""
                MyControls.SetControls(ControlHandler.Mode.Locked, Me.Controls)
                m_RecordPopulated = True
                SetToolbar()

                Cursor.Current = Cursors.Default
                AfterAcceptChanges()

            End If

        End If

    End Sub

    Private Sub CancelChanges()

        If BeforeCancelChanges() Then

            Dim _msg As String = ""
            Select Case m_Mode

                Case "ADD"
                    _msg = "Are you sure you want to cancel creating this record?"

                Case "EDIT"
                    _msg = "Are you sure you want to cancel amending this record?"

                Case "ADDEXIT"
                    _msg = "Are you sure you want to cancel creating this record and Exit?"

                Case "EDITEXIT"
                    _msg = "Are you sure you want to cancel amending this record and Exit?"

                Case Else
                    Me.Close()
                    Exit Sub

            End Select

            If CareMessage(_msg, MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Exit Form") = MsgBoxResult.Yes Then
                If m_Mode.ToString.Contains("EXIT") Then
                    m_Mode = ""
                    Me.Close()
                Else
                    m_Mode = ""
                    MyControls.SetControls(ControlHandler.Mode.Locked, Me.Controls)
                    SetToolbar()
                    bs.ResetCurrentItem()
                    AfterCancelChanges()
                End If
            End If

        End If

    End Sub

#End Region

#Region "Tool Methods"

    Protected Sub ToolFind()
        FindRecord()
        CheckForDocumentsAndNotes
        SetToolbar()
    End Sub

    Protected Sub ToolAdd()
        If Not BeforeAdd() Then Exit Sub
        m_Mode = "ADD"
        bs.AddNew()
        MyControls.SetControls(ControlHandler.Mode.Add, Me.Controls)
        Me.SelectNextControl(Me, True, True, True, False)
        SetAcceptOnly()
        AfterAdd()
    End Sub

    Protected Sub ToolEdit()
        If Not BeforeEdit() Then Exit Sub
        m_Mode = "EDIT"
        MyControls.SetControls(ControlHandler.Mode.Edit, Me.Controls)
        Me.SelectNextControl(Me, True, True, True, False)
        SetAcceptOnly()
        AfterEdit()
    End Sub

    Protected Sub ToolDelete()
        DeleteRecord()
    End Sub

    Protected Sub ToolAccept()
        AcceptChanges()
    End Sub

    Protected Sub ToolCancel()
        CancelChanges()
    End Sub

    Protected Sub ToolAttach()
        If RecordID.HasValue Then
            DocumentHandler.OpenDocumentManagement(RecordID.Value, Me.Text)
        End If
    End Sub

    Protected Sub ToolPrint()
        PrintScreen()
    End Sub

    Protected Sub ToolNotes()
        If RecordID.HasValue Then
            NotesHandler.OpenNotes(RecordID.Value, Me.Text)
        End If
    End Sub

#End Region

#Region "Toolbars"

    Private Sub SetAcceptOnly()

        tlFind.Enabled = False

        tlAdd.Enabled = False
        tlEdit.Enabled = False
        tlDelete.Enabled = False

        tlAccept.Enabled = True
        tlCancel.Enabled = True

        tlAttach.Enabled = False

        btnOK.Enabled = True

    End Sub

    Private Sub SetToolbar()

        Select Case m_ToolbarMode

            Case ToolbarEnum.NewOnly
                tlFind.Enabled = False
                tlAdd.Enabled = ReadOnlyCheck(True)
                tlEdit.Enabled = False
                tlDelete.Enabled = False
                tlAttach.Enabled = False
                tlNotes.Enabled = False

            Case ToolbarEnum.EditOnly
                tlFind.Enabled = False
                tlAdd.Enabled = False
                tlEdit.Enabled = ReadOnlyCheck(True)
                tlDelete.Enabled = False
                tlAttach.Enabled = False
                tlNotes.Enabled = False

            Case ToolbarEnum.FindOnly
                tlFind.Enabled = True
                tlAdd.Enabled = False
                tlEdit.Enabled = False
                tlDelete.Enabled = False
                tlAttach.Enabled = False
                tlNotes.Enabled = False

            Case ToolbarEnum.FindandNew
                tlFind.Enabled = True
                tlAdd.Enabled = ReadOnlyCheck(True)
                tlEdit.Enabled = False
                tlDelete.Enabled = False
                tlAttach.Enabled = False
                tlNotes.Enabled = False

            Case ToolbarEnum.FindandEdit
                tlFind.Enabled = True
                tlAdd.Enabled = False
                tlEdit.Enabled = ReadOnlyCheck(True)
                tlDelete.Enabled = False
                tlAttach.Enabled = False
                tlNotes.Enabled = False

            Case ToolbarEnum.None
                tlFind.Enabled = False
                tlAdd.Enabled = False
                tlEdit.Enabled = False
                tlDelete.Enabled = False
                tlAttach.Enabled = False
                tlNotes.Enabled = False

            Case ToolbarEnum.Standard

                tlFind.Enabled = True
                tlAdd.Enabled = ReadOnlyCheck(True)
                tlAttach.Enabled = False
                tlNotes.Enabled = False

                If m_RecordPopulated Then
                    tlEdit.Enabled = ReadOnlyCheck(True)
                    tlDelete.Enabled = ReadOnlyCheck(True)
                    tlAttach.Enabled = ReadOnlyCheck(True)
                    tlNotes.Enabled = ReadOnlyCheck(True)
                Else
                    tlEdit.Enabled = False
                    tlDelete.Enabled = False
                    tlAttach.Enabled = False
                    tlNotes.Enabled = False
                End If

        End Select

        tlAccept.Enabled = False
        tlCancel.Enabled = False
        btnOK.Enabled = False

        If m_RecordPopulated Then
            If m_RecordID.HasValue Then
                If m_NotesAvailable Then
                    tlNotes.ImageOptions.Image = Global.Care.[Shared].My.Resources.Resources.notesY
                Else
                    tlNotes.ImageOptions.Image = Global.Care.[Shared].My.Resources.Resources.notes
                End If
                If m_DocumentsAvailable Then
                    tlAttach.ImageOptions.Image = Global.Care.[Shared].My.Resources.Resources.attachmentY
                Else
                    tlAttach.ImageOptions.Image = Global.Care.[Shared].My.Resources.Resources.attachment
                End If
            Else
                tlNotes.ImageOptions.Image = Global.Care.[Shared].My.Resources.Resources.notes
                tlAttach.ImageOptions.Image = Global.Care.[Shared].My.Resources.Resources.attachment
            End If
        Else
            tlNotes.ImageOptions.Image = Global.Care.[Shared].My.Resources.Resources.notes
            tlAttach.ImageOptions.Image = Global.Care.[Shared].My.Resources.Resources.attachment
        End If

    End Sub

    Private Sub CheckForDocumentsAndNotes()

        m_NotesAvailable = False
        m_DocumentsAvailable = False

        If m_RecordPopulated AndAlso m_RecordID.HasValue Then

            Try

                Dim _SQL As String = ""
                Dim _Count As Integer = 0

                _SQL = "select count(*) from AppNotes where record_id = '" + m_RecordID.ToString + "'"
                _Count = ValueHandler.ConvertInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))
                If _Count > 0 Then m_NotesAvailable = True

                _SQL = "select count(*) from AppDocs where key_id = '" + m_RecordID.ToString + "' and type = 'User'"
                _Count = ValueHandler.ConvertInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))
                If _Count > 0 Then m_DocumentsAvailable = True

            Catch ex As Exception

            End Try

        End If

    End Sub

    Private Function ReadOnlyCheck(ByVal EnableTool As Boolean) As Boolean
        If Me.ReadOnlyPermissions Then
            Return False
        Else
            Return True
        End If
    End Function

#End Region

#Region "Buttons"

    Private Sub btnOK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnOK.Click
        AcceptChanges()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        CancelChanges()
    End Sub

#End Region

    Private Sub tlFind_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles tlFind.ItemClick
        ToolFind()
    End Sub

    Private Sub tlAdd_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles tlAdd.ItemClick
        ToolAdd()
    End Sub

    Private Sub tlEdit_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles tlEdit.ItemClick
        ToolEdit()
    End Sub

    Private Sub tlDelete_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles tlDelete.ItemClick
        ToolDelete()
    End Sub

    Private Sub tlAccept_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles tlAccept.ItemClick
        ToolAccept()
    End Sub

    Private Sub tlCancel_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles tlCancel.ItemClick
        ToolCancel()
    End Sub

    Private Sub tlPrint_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles tlPrint.ItemClick
        ToolPrint()
    End Sub

    Private Sub tlAttach_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles tlAttach.ItemClick
        ToolAttach()
    End Sub

    Private Sub tlNotes_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles tlNotes.ItemClick
        ToolNotes()
    End Sub

    Private Sub PrintScreen()

        Dim _p As New Panel()
        Me.Controls.Add(_p)

        'Create a Bitmap of size same as that of the Form.
        Dim _g As Graphics = _p.CreateGraphics()
        m_PrintScreen = New Bitmap(Me.ClientSize.Width, Me.ClientSize.Height, _g)
        _g = Graphics.FromImage(m_PrintScreen)

        'Copy screen area that that the Panel covers.
        Dim _pl As Point = PointToScreen(_p.Location)
        _g.CopyFromScreen(_pl.X, _pl.Y, 0, 0, Me.ClientSize)

        PrintDocument1.OriginAtMargins = True
        PrintDocument1.DefaultPageSettings.Landscape = True
        PrintDocument1.DefaultPageSettings.Margins.Top = 50
        PrintDocument1.DefaultPageSettings.Margins.Bottom = 50
        PrintDocument1.DefaultPageSettings.Margins.Left = 50
        PrintDocument1.DefaultPageSettings.Margins.Right = 50
        PrintDocument1.DocumentName = "Quick Print - " + Me.Text

        'Show the Print Preview Dialog.
        PrintPreviewDialog1.Document = PrintDocument1
        PrintPreviewDialog1.PrintPreviewControl.Zoom = 1
        PrintPreviewDialog1.ShowDialog()

    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        e.Graphics.DrawImage(m_PrintScreen, 0, 0)
    End Sub

    Private Sub bbiAudit_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiAudit.ItemClick
        AuditHandler.ViewFormAudit(Me.Name, Me.Text, Me.RecordID)
    End Sub
End Class