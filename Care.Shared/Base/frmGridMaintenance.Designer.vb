﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGridMaintenance
    Inherits frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New Panel()
        Me.btnEdit = New Care.Controls.CareButton()
        Me.btnAdd = New Care.Controls.CareButton()
        Me.btnDelete = New Care.Controls.CareButton()
        Me.gbx = New GroupBox()
        Me.Panel2 = New Panel()
        Me.btnOK = New Care.Controls.CareButton()
        Me.btnCancel = New Care.Controls.CareButton()
        Me.Grid = New Care.Controls.CareGrid()
        Me.bs = New BindingSource()
        Me.btnClose = New Care.Controls.CareButton()
        Me.Panel1.SuspendLayout()
        Me.gbx.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnClose)
        Me.Panel1.Controls.Add(Me.btnEdit)
        Me.Panel1.Controls.Add(Me.btnAdd)
        Me.Panel1.Controls.Add(Me.btnDelete)
        Me.Panel1.Dock = DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 367)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(424, 35)
        Me.Panel1.TabIndex = 2
        '
        'btnEdit
        '
        Me.btnEdit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.Appearance.Options.UseFont = True
        Me.btnEdit.Location = New System.Drawing.Point(101, 5)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(85, 25)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Tag = "I"
        Me.btnEdit.Text = "Edit"
        '
        'btnAdd
        '
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.Appearance.Options.UseFont = True
        Me.btnAdd.Location = New System.Drawing.Point(10, 5)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(85, 25)
        Me.btnAdd.TabIndex = 0
        Me.btnAdd.Tag = "I"
        Me.btnAdd.Text = "Add"
        '
        'btnDelete
        '
        Me.btnDelete.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Appearance.Options.UseFont = True
        Me.btnDelete.Location = New System.Drawing.Point(192, 5)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(85, 25)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Tag = "I"
        Me.btnDelete.Text = "Delete"
        '
        'gbx
        '
        Me.gbx.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.gbx.Controls.Add(Me.Panel2)
        Me.gbx.Location = New System.Drawing.Point(28, 80)
        Me.gbx.Name = "gbx"
        Me.gbx.Size = New System.Drawing.Size(369, 96)
        Me.gbx.TabIndex = 0
        Me.gbx.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnOK)
        Me.Panel2.Controls.Add(Me.btnCancel)
        Me.Panel2.Dock = DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(3, 65)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(363, 28)
        Me.Panel2.TabIndex = 0
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(178, 0)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(85, 25)
        Me.btnOK.TabIndex = 0
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.CausesValidation = False
        Me.btnCancel.Location = New System.Drawing.Point(269, 0)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 25)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "Cancel"
        '
        'Grid
        '
        Me.Grid.AllowBuildColumns = True
        Me.Grid.AllowHorizontalScroll = False
        Me.Grid.AllowMultiSelect = False
        Me.Grid.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.Grid.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Grid.Appearance.Options.UseFont = True
        Me.Grid.AutoSizeByData = True
        Me.Grid.HideFirstColumn = False
        Me.Grid.Location = New System.Drawing.Point(10, 12)
        Me.Grid.Name = "Grid"
        Me.Grid.QueryID = Nothing
        Me.Grid.SearchAsYouType = True
        Me.Grid.ShowFindPanel = False
        Me.Grid.ShowGroupByBox = False
        Me.Grid.ShowNavigator = False
        Me.Grid.Size = New System.Drawing.Size(405, 349)
        Me.Grid.TabIndex = 1
        Me.Grid.TabStop = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(330, 5)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(85, 25)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Tag = "I"
        Me.btnClose.Text = "Close"
        Me.btnClose.Visible = False
        '
        'frmGridMaintenance
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(424, 402)
        Me.Controls.Add(Me.gbx)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Grid)
        Me.MaximizeBox = False
        Me.MinimumSize = New System.Drawing.Size(440, 440)
        Me.Name = "frmGridMaintenance"
        Me.Panel1.ResumeLayout(False)
        Me.gbx.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Protected WithEvents gbx As GroupBox
    Protected WithEvents Panel2 As Panel
    Protected WithEvents btnOK As Care.Controls.CareButton
    Protected WithEvents btnCancel As Care.Controls.CareButton
    Protected WithEvents bs As BindingSource
    Protected WithEvents Grid As Care.Controls.CareGrid
    Protected WithEvents Panel1 As Panel
    Protected WithEvents btnDelete As Care.Controls.CareButton
    Protected WithEvents btnEdit As Care.Controls.CareButton
    Protected WithEvents btnAdd As Care.Controls.CareButton

    Public Sub New()

        Me.SetStyle(ControlStyles.SupportsTransparentBackColor, True)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Protected WithEvents btnClose As Care.Controls.CareButton
End Class
