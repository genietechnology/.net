﻿Imports Care.Global
Imports System.ComponentModel

Public Class CRMActivity

#Region "Properties"

    <Category("_CareAttributes")>
    Public Property CRMLinkType As CRM.EnumLinkType

    <Browsable(False)>
    Public Property CRMLinkID As Guid?

    <Browsable(False)>
    Public Property CRMContactID As Guid?

    <Browsable(False)>
    Public Property CRMContactName As String

    <Browsable(False)>
    Public Property CRMContactMobile As String

    <Browsable(False)>
    Public Property CRMContactEmail As String

#End Region

    Public Sub Populate()

        If Not CRMLinkID.HasValue Then Exit Sub

        Dim _SQL As String = ""
        _SQL += "select ID, action_date as 'Action Date', action_type as 'Type', contact_name as 'Name', subject as 'Subject', fup_date as 'Follow-Up Date'"
        _SQL += " from ActivityCRM"
        _SQL += " where link_id = '" + CRMLinkID.Value.ToString + "'"
        _SQL += " order by action_date desc"

        cgActivity.HideFirstColumn = True
        cgActivity.Populate(Session.ConnectionString, _SQL)

        cgActivity.Columns("Action Date").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        cgActivity.Columns("Action Date").DisplayFormat.FormatString = "dd/MM/yyyy HH:mm"

        SetCMDs()

    End Sub

    Private Sub SetCMDs()

        btnSMS.Enabled = False
        If Session.SMSEnabled AndAlso CRMContactMobile <> "" Then btnSMS.Enabled = True

        btnEmail.Enabled = False
        If Session.SMTPEnabled AndAlso CRMContactEmail <> "" Then btnEmail.Enabled = True

    End Sub

    Private Sub cgActivity_GridDoubleClick(sender As Object, e As EventArgs) Handles cgActivity.GridDoubleClick
        EditActivity()
    End Sub

    Private Sub EditActivity()
        If cgActivity.RecordCount <= 0 Then Exit Sub
        Dim _ID As Guid = New Guid(cgActivity.CurrentRow.Item("ID").ToString)
        If CRM.AmendActivity(_ID) Then
            Populate()
        End If
    End Sub

    Private Sub PopupActivity(ByVal ActivityType As CRM.EnumActivityType)
        If CRM.CreateActivity(CRMLinkID.Value, CRMLinkType, ActivityType, Nothing, CRMContactName).HasValue Then
            Populate()
        End If
    End Sub

    Private Sub btnCall_Click(sender As Object, e As EventArgs) Handles btnCall.Click
        PopupActivity(CRM.EnumActivityType.PhoneCall)
    End Sub

    Private Sub btnSMS_Click(sender As Object, e As EventArgs) Handles btnSMS.Click

        Dim _frm As New frmSendSMS(CRMContactMobile)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            LogActivity(CRM.EnumActivityType.SMS, "SMS Sent", _frm.Message)
        End If

        _frm.Dispose()
        _frm = Nothing

    End Sub

    Private Sub btnEmail_Click(sender As Object, e As EventArgs) Handles btnEmail.Click

        btnEmail.Enabled = False
        Session.CursorApplicationStarting()

        Dim _frm As New frmSendEmail(CRMContactEmail, True)
        _frm.ShowDialog()

        btnEmail.Enabled = True

        If _frm.DialogResult = DialogResult.OK Then
            LogActivity(CRM.EnumActivityType.Email, _frm.Subject, _frm.Body)
        End If

        _frm.Dispose()
        _frm = Nothing

    End Sub

    Private Sub btnEvent_Click(sender As Object, e As EventArgs) Handles btnEvent.Click
        PopupActivity(CRM.EnumActivityType.Appointment)
    End Sub

    Private Sub btnFeedback_Click(sender As Object, e As EventArgs) Handles btnFeedback.Click
        PopupActivity(CRM.EnumActivityType.Feedback)
    End Sub

    Private Sub btnNote_Click(sender As Object, e As EventArgs) Handles btnNote.Click
        PopupActivity(CRM.EnumActivityType.Note)
    End Sub

    Public Sub LogActivity(ByVal ActivityType As CRM.EnumActivityType, ByVal ActivitySubject As String, ByVal ActivityNotes As String)

        Dim _A As New Business.ActivityCRM
        With _A
            ._LinkId = CRMLinkID
            ._LinkType = CRMLinkType.ToString
            ._ContactId = CRMContactID
            ._ContactName = CRMContactName
            ._ActionDate = Now
            ._ActionType = ActivityType.ToString
            ._Subject = ActivitySubject
            ._Notes = ActivityNotes
            .Store()
        End With

        _A = Nothing

        Populate()

    End Sub

End Class
