﻿Imports System.ComponentModel

Public Class CareIcon

    Public Enum EnumIcon
        Blank
        Tick
        Warning
        Cross
    End Enum

    Private m_Icon As EnumIcon

    <Category("_CareAttributes")>
    Public Property Icon As EnumIcon
        Get
            Return m_Icon
        End Get
        Set(value As EnumIcon)

            m_Icon = value

            If value = EnumIcon.Blank Then
                pic.Hide()
            Else
                pic.Show()
                If value = EnumIcon.Tick Then pic.Image = My.Resources.Accept_32
                If value = EnumIcon.Warning Then pic.Image = My.Resources.alert_32
                If value = EnumIcon.Cross Then pic.Image = My.Resources.close_32
            End If

        End Set
    End Property

End Class
