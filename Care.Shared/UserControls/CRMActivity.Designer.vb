﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CRMActivity
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CRMActivity))
        Me.cgActivity = New Care.Controls.CareGrid()
        Me.btnCall = New Care.Controls.CareButton()
        Me.btnSMS = New Care.Controls.CareButton()
        Me.btnEmail = New Care.Controls.CareButton()
        Me.btnFeedback = New Care.Controls.CareButton()
        Me.btnNote = New Care.Controls.CareButton()
        Me.btnEvent = New Care.Controls.CareButton()
        Me.SuspendLayout()
        '
        'cgActivity
        '
        Me.cgActivity.AllowBuildColumns = True
        Me.cgActivity.AllowHorizontalScroll = False
        Me.cgActivity.AllowMultiSelect = False
        Me.cgActivity.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgActivity.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgActivity.Appearance.Options.UseFont = True
        Me.cgActivity.AutoSizeByData = True
        Me.cgActivity.FocusedRowHandle = -2147483648
        Me.cgActivity.HideFirstColumn = False
        Me.cgActivity.Location = New System.Drawing.Point(0, 0)
        Me.cgActivity.Name = "cgActivity"
        Me.cgActivity.QueryID = Nothing
        Me.cgActivity.RowAutoHeight = False
        Me.cgActivity.SearchAsYouType = True
        Me.cgActivity.ShowAutoFilterRow = False
        Me.cgActivity.ShowFindPanel = False
        Me.cgActivity.ShowGroupByBox = True
        Me.cgActivity.ShowNavigator = False
        Me.cgActivity.Size = New System.Drawing.Size(690, 315)
        Me.cgActivity.TabIndex = 0
        '
        'btnCall
        '
        Me.btnCall.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnCall.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCall.Appearance.Options.UseFont = True
        Me.btnCall.Image = CType(resources.GetObject("btnCall.Image"), System.Drawing.Image)
        Me.btnCall.Location = New System.Drawing.Point(0, 321)
        Me.btnCall.Name = "btnCall"
        Me.btnCall.Size = New System.Drawing.Size(110, 32)
        Me.btnCall.TabIndex = 5
        Me.btnCall.Text = "Log Call"
        '
        'btnSMS
        '
        Me.btnSMS.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnSMS.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSMS.Appearance.Options.UseFont = True
        Me.btnSMS.Image = CType(resources.GetObject("btnSMS.Image"), System.Drawing.Image)
        Me.btnSMS.Location = New System.Drawing.Point(116, 321)
        Me.btnSMS.Name = "btnSMS"
        Me.btnSMS.Size = New System.Drawing.Size(110, 32)
        Me.btnSMS.TabIndex = 6
        Me.btnSMS.Text = "Send SMS"
        '
        'btnEmail
        '
        Me.btnEmail.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnEmail.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnEmail.Appearance.Options.UseFont = True
        Me.btnEmail.Image = CType(resources.GetObject("btnEmail.Image"), System.Drawing.Image)
        Me.btnEmail.Location = New System.Drawing.Point(232, 321)
        Me.btnEmail.Name = "btnEmail"
        Me.btnEmail.Size = New System.Drawing.Size(110, 32)
        Me.btnEmail.TabIndex = 7
        Me.btnEmail.Text = "Send Email"
        '
        'btnFeedback
        '
        Me.btnFeedback.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnFeedback.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnFeedback.Appearance.Options.UseFont = True
        Me.btnFeedback.Image = CType(resources.GetObject("btnFeedback.Image"), System.Drawing.Image)
        Me.btnFeedback.Location = New System.Drawing.Point(464, 321)
        Me.btnFeedback.Name = "btnFeedback"
        Me.btnFeedback.Size = New System.Drawing.Size(110, 32)
        Me.btnFeedback.TabIndex = 8
        Me.btnFeedback.Text = "Log Feedback"
        '
        'btnNote
        '
        Me.btnNote.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnNote.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnNote.Appearance.Options.UseFont = True
        Me.btnNote.Image = CType(resources.GetObject("btnNote.Image"), System.Drawing.Image)
        Me.btnNote.Location = New System.Drawing.Point(580, 321)
        Me.btnNote.Name = "btnNote"
        Me.btnNote.Size = New System.Drawing.Size(110, 32)
        Me.btnNote.TabIndex = 9
        Me.btnNote.Text = "Log Note"
        '
        'btnEvent
        '
        Me.btnEvent.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnEvent.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnEvent.Appearance.Options.UseFont = True
        Me.btnEvent.Image = CType(resources.GetObject("btnEvent.Image"), System.Drawing.Image)
        Me.btnEvent.Location = New System.Drawing.Point(348, 321)
        Me.btnEvent.Name = "btnEvent"
        Me.btnEvent.Size = New System.Drawing.Size(110, 32)
        Me.btnEvent.TabIndex = 10
        Me.btnEvent.Text = "Create Event"
        '
        'CRMActivity
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.Controls.Add(Me.btnEvent)
        Me.Controls.Add(Me.btnNote)
        Me.Controls.Add(Me.btnFeedback)
        Me.Controls.Add(Me.btnEmail)
        Me.Controls.Add(Me.btnSMS)
        Me.Controls.Add(Me.btnCall)
        Me.Controls.Add(Me.cgActivity)
        Me.Name = "CRMActivity"
        Me.Size = New System.Drawing.Size(692, 353)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cgActivity As Care.Controls.CareGrid
    Friend WithEvents btnCall As Care.Controls.CareButton
    Friend WithEvents btnSMS As Care.Controls.CareButton
    Friend WithEvents btnEmail As Care.Controls.CareButton
    Friend WithEvents btnFeedback As Care.Controls.CareButton
    Friend WithEvents btnNote As Care.Controls.CareButton
    Friend WithEvents btnEvent As Care.Controls.CareButton

End Class
