﻿Imports Care.Global
Imports System.ComponentModel

Public Class CareEmailAddress

    Private m_NoColours As Boolean = False
    Private m_NoButton As Boolean = False
    Private m_NoValidate As Boolean = False

    <Category("_CareAttributes")>
    Public Property [ReadOnly] As Boolean
        Get
            Return be.Properties.ReadOnly
        End Get
        Set(value As Boolean)
            be.Properties.ReadOnly = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property MaxLength As Integer
        Get
            Return be.Properties.MaxLength
        End Get
        Set(value As Integer)
            be.Properties.MaxLength = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Overrides Property BackColor As System.Drawing.Color
        Get
            Return be.BackColor
        End Get
        Set(value As System.Drawing.Color)
            be.BackColor = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Overrides Property Text As String
        Get
            Return be.Text
        End Get
        Set(value As String)
            be.Text = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property NoColours As Boolean
        Get
            Return m_NoColours
        End Get
        Set(value As Boolean)
            m_NoColours = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property NoButton As Boolean
        Get
            Return m_NoButton
        End Get
        Set(value As Boolean)
            m_NoButton = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property NoValidate As Boolean
        Get
            Return m_NoValidate
        End Get
        Set(value As Boolean)
            m_NoValidate = value
        End Set
    End Property

    Private Sub SetLayout()
        If m_NoButton Then
            be.Properties.Buttons(0).Visible = False
        Else
            be.Properties.Buttons(0).Visible = True
            be.Properties.Buttons(0).ToolTip = "Email this address"
        End If
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub CareEmailAddress_Load(sender As Object, e As EventArgs) Handles Me.Load
        SetLayout()
    End Sub

    Private Sub be_ButtonClick(sender As Object, e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles be.ButtonClick
        If be.Text = "" Then Exit Sub
        Process.Start("mailto:" + be.Text)
    End Sub

    Private Sub be_Validating(sender As Object, e As CancelEventArgs) Handles be.Validating

        If be.Enabled = False Then Exit Sub
        If be.Properties.ReadOnly = True Then Exit Sub
        If m_NoValidate Then Exit Sub

        If be.Text <> "" Then
            If Not EmailHandler.AddressIsValid(be.Text) Then
                If CareMessage("This email address appears to be invalid. Are you sure you wish to continue?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Email Validation") = DialogResult.Yes Then
                    'yes
                    be.ErrorText = ""
                    If Not m_NoColours Then be.BackColor = Session.ChangeColour
                Else
                    e.Cancel = True
                    be.ErrorText = "Email Address is invalid"
                    If Not m_NoColours Then be.BackColor = Drawing.Color.LightCoral
                End If
            Else
                be.ErrorText = ""
                If Not m_NoColours Then be.BackColor = Session.ChangeColour
            End If
        Else
            be.ErrorText = ""
            If Not m_NoColours Then be.BackColor = Session.ChangeColour
        End If

    End Sub
End Class
