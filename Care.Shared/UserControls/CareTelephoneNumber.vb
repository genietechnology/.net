﻿Imports Care.Global
Imports System.ComponentModel

Public Class CareTelephoneNumber

    Private m_IsMobile As Boolean = True

    <Category("_CareAttributes")>
    Public Property IsMobile As Boolean
        Get
            Return m_IsMobile
        End Get
        Set(value As Boolean)
            m_IsMobile = value
            be.Properties.Buttons(1).Visible = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property [ReadOnly] As Boolean
        Get
            Return be.Properties.ReadOnly
        End Get
        Set(value As Boolean)
            be.Properties.ReadOnly = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Property MaxLength As Integer
        Get
            Return be.Properties.MaxLength
        End Get
        Set(value As Integer)
            be.Properties.MaxLength = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Overrides Property BackColor As System.Drawing.Color
        Get
            Return be.BackColor
        End Get
        Set(value As System.Drawing.Color)
            be.BackColor = value
        End Set
    End Property

    <Category("_CareAttributes")>
    Public Overrides Property Text As String
        Get
            Return be.Text
        End Get
        Set(value As String)
            be.Text = value
        End Set
    End Property

    Private Sub SetLayout()

        If Session Is Nothing Then Exit Sub

        If Session.CTIEnabled Then
            be.Properties.Buttons(0).Enabled = True
            be.Properties.Buttons(0).ToolTip = "Dial this number"
        Else
            be.Properties.Buttons(0).Enabled = False
            be.Properties.Buttons(0).ToolTip = "Telephone System Integration disabled - unable to Dial"
        End If

        If m_IsMobile Then
            be.Properties.Buttons(1).Visible = True
            If Session.SMSEnabled Then
                be.Properties.Buttons(1).Enabled = True
                be.Properties.Buttons(1).ToolTip = "Send Text Message to this number"
            Else
                be.Properties.Buttons(1).Enabled = False
                be.Properties.Buttons(1).ToolTip = "Text Messaging disabled - unable to text"
            End If
        Else
            be.Properties.Buttons(1).Visible = False
        End If

    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub CareTelephoneNumber_Load(sender As Object, e As EventArgs) Handles Me.Load
        SetLayout()
    End Sub

    Private Sub be_ButtonClick(sender As Object, e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles be.ButtonClick

        If be.Text = "" Then Exit Sub

        If e.Button.Index = 0 Then
            CTIHandler.DialNumber(be.Text)
        Else
            SMSHandler.Show(be.Text)
        End If

    End Sub

End Class
