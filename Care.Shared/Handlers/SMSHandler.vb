﻿
Imports Care.Global

Public Class SMSHandler

    Private m_OutBoxID As Guid?
    Private m_MobileNo As String
    Private m_Message As String

    Public Property OutBoxID() As Guid?
        Get
            Return m_OutBoxID
        End Get
        Set(ByVal value As Guid?)
            m_OutBoxID = value
        End Set
    End Property

    Public Property MobileNumber() As String
        Get
            Return m_MobileNo
        End Get
        Set(ByVal value As String)
            m_MobileNo = value
        End Set
    End Property

    Public Property Message() As String
        Get
            Return m_Message
        End Get
        Set(ByVal value As String)
            m_Message = value
        End Set
    End Property

    Public Shared Sub Show(ByVal MobileNumber As String)

        Dim _frm As New frmSendSMS(MobileNumber)
        _frm.ShowDialog()

        _frm.Dispose()
        _frm = Nothing

    End Sub

    Public Shared Function SendSMS(ByVal MobileNumber As String, ByVal Message As String, Optional OutBoxID As Guid? = Nothing) As Boolean

        If CleanNumber(MobileNumber) Then

            Dim _Handler As New SMSHandler
            With _Handler
                .OutBoxID = OutBoxID
                .MobileNumber = MobileNumber
                .Message = Message
                .Send()
            End With

            Return True

        Else
            Return False
        End If

    End Function

    Private Shared Function CleanNumber(ByRef MobileNumber As String) As Boolean

        If MobileNumber = "" Then Return False

        'remove all spaces from the number
        MobileNumber = MobileNumber.Replace(" ", "")

        Select Case MobileNumber.Length

            '7596320027
            Case 10
                If MobileNumber.StartsWith("7") Then
                    MobileNumber = "0" + MobileNumber
                    Return True
                End If

            '07596320027
            Case 11
                If MobileNumber.StartsWith("07") Then Return True

            '447596320027
            Case 12
                If MobileNumber.StartsWith("44") Then
                    MobileNumber = MobileNumber.Replace("44", "0")
                    Return True
                End If

            '+447596320027
            Case 13
                If MobileNumber.StartsWith("+44") Then
                    MobileNumber = MobileNumber.Replace("+44", "0")
                    Return True
                End If

        End Select

        Return False

    End Function

    Public Sub Send()

        If m_MobileNo = "" Then Exit Sub
        m_MobileNo = m_MobileNo.Replace(" ", "")

        Dim _E As New EmailMessage
        _E.Recipient = m_MobileNo + "@" + Session.SMSDomain
        _E.SenderEmail = Session.SMSSender
        _E.SenderName = Session.SMSSender
        _E.Body = m_Message
        _E.Send()

        _E = Nothing

        'LogSMS(m_OutBoxID, m_MobileNo, m_Message)

    End Sub

    Private Sub LogSMS(OutBoxID As Guid?, MobileNumber As String, Message As String)

        Dim _Log As New Business.AppSMSLog
        With _Log
            ._OutboxId = OutBoxID
            ._SmsNumber = MobileNumber
            ._SmsMessage = Message
            ._UserId = Session.CurrentUser.ID
            ._Stamp = Now
            .Store()
        End With

        _Log = Nothing

    End Sub

End Class
