﻿
Imports DevExpress.XtraEditors
Public Module MessageHandler

    Public Function CareMessage(ByVal Message As String, _
                                Optional ByVal MessageIcon As Windows.Forms.MessageBoxIcon = Windows.Forms.MessageBoxIcon.Information, _
                                Optional ByVal MessageButtons As Windows.Forms.MessageBoxButtons = Windows.Forms.MessageBoxButtons.OK, _
                                Optional ByVal Title As String = "") As Windows.Forms.DialogResult

        Dim _Title = Title
        If _Title = "" Then _Title = Session.ApplicationName

        Return XtraMessageBox.Show(Message, _Title, MessageButtons, MessageIcon)

    End Function

    Public Function CareMessage(ByVal Message As String, _
                                ByVal MessageIcon As Windows.Forms.MessageBoxIcon, _
                                ByVal Title As String) As Windows.Forms.DialogResult

        Dim _Title = Title
        If _Title = "" Then _Title = Session.ApplicationName

        Return XtraMessageBox.Show(Message, _Title, Windows.Forms.MessageBoxButtons.OK, MessageIcon)

    End Function

End Module
