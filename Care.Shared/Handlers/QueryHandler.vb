﻿Imports Care.Global
Imports Care.Data

Public Class QueryHandler

    Public Shared Function SelectQuery() As Guid?

        Dim _Find As New GenericFind

        Dim _SQL As String = "select type, title from AppQueries"

        With _Find
            .Caption = "Select Query"
            .PopulateOnLoad = True
            .ConnectionString = Session.ConnectionString
            .GridSQL = _SQL
            .GridOrderBy = "order by title"
            .ReturnField = "ID"
            .Show()
        End With

        If _Find.ReturnValue = "" Then
            Return Nothing
        Else
            Return New Guid(_Find.ReturnValue)
        End If

    End Function

    Public Shared Function GetQueryObject(ByVal QueryID As Guid) As Business.AppQueries
        Return Business.AppQueries.RetreiveByID(QueryID)
    End Function

    Public Shared Function GetQueryConnection(ByVal QueryID As Guid) As String

        Dim _Connection As String = ""

        Dim _Q As Business.AppQueries = Business.AppQueries.RetreiveByID(QueryID)
        If _Q IsNot Nothing Then
            _Connection = _Q._Connection
        End If

        If _Connection = "" Then _Connection = Session.ConnectionString

        Return _Connection

    End Function

    Public Shared Function GetQueryText(ByVal QueryID As Guid, Optional Top1 As Boolean = False, Optional WhereClause As String = "") As String

        Dim _SQL As String = ""
        Dim _Q As Business.AppQueries = Business.AppQueries.RetreiveByID(QueryID)
        If _Q IsNot Nothing Then

            Dim _Connection As String = Session.ConnectionString
            If _Q._Connection <> "" Then _Connection = _Q._Connection

            Select Case _Q._Type

                Case "SQL Query"
                    _SQL = _Q._Query

                Case "SQL View"
                    _SQL = _Q._Query

                Case "Stored Procedure"
                    _SQL = _Q._Query

            End Select

            If Top1 Then
                _SQL = Replace(_SQL.ToLower, "select ", "select top 1 ")
            End If

            If WhereClause <> "" Then
                _SQL += " " + WhereClause
            End If

        End If

        Return _SQL

    End Function

    Public Shared Function GetQueryData(ByVal QueryID As Guid, Optional Top1 As Boolean = False, Optional WhereClause As String = "") As DataTable

        Dim _DT As DataTable = Nothing

        Try

            Session.CursorWaiting()

            Dim _SQL As String = GetQueryText(QueryID, Top1, WhereClause)
            _DT = DAL.GetDataTablefromSQL(GetQueryConnection(QueryID), _SQL)

        Catch ex As Exception
            _DT = Nothing
        End Try

        Return _DT

    End Function

End Class
