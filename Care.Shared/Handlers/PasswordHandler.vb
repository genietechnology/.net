﻿Imports Care.Global
Imports Care.Data
Imports Care.Shared

Public Class PasswordHandler

    Public Shared Function ValidatePassword(Password As String,
        Optional ByVal minLength As Integer = 6,
        Optional ByVal minUpper As Integer = 1,
        Optional ByVal minLower As Integer = 1,
        Optional ByVal minNumbers As Integer = 1,
        Optional ByVal minSpecial As Integer = 0) As Boolean

        ' Replace [A-Z] with \p{Lu}, to allow for Unicode uppercase letters.
        Dim _upper As New Text.RegularExpressions.Regex("[A-Z]")
        Dim _lower As New Text.RegularExpressions.Regex("[a-z]")
        Dim _number As New Text.RegularExpressions.Regex("[0-9]")
        ' Special is "none of the above".
        Dim _special As New Text.RegularExpressions.Regex("[^a-zA-Z0-9]")

        ' Check the length.
        If Len(Password) < minLength Then Return False
        ' Check for minimum number of occurrences.
        If _upper.Matches(Password).Count < minUpper Then Return False
        If _lower.Matches(Password).Count < minLower Then Return False
        If _number.Matches(Password).Count < minNumbers Then Return False
        If _special.Matches(Password).Count < minSpecial Then Return False

        ' Passed all checks.
        Return True

    End Function

    Public Shared Function CheckHistory(UserID As Guid, History As Integer, NewPassword As String) As Boolean

        Dim _passwordHistory As List(Of Business.AppUsersPasswordHistory) = Business.AppUsersPasswordHistory.RetrieveByUserID(UserID)
        Dim _cutoffDate As Date = DateAdd(DateInterval.Day, History * -1, Today)

        'check if new password was used in the last X days
        For Each password In _passwordHistory
            If password._Password = NewPassword AndAlso password._LastUsed >= _cutoffDate Then Return False
        Next
        Return True

    End Function

    ''' <summary>
    ''' This function returns True if the Password is OK and False if the password has expired.
    ''' </summary>
    ''' <param name="UserID"></param>
    ''' <param name="Expiry"></param>
    ''' <returns></returns>
    Public Shared Function PasswordExpired(UserID As Guid, Expiry As Integer) As Boolean

        'password expiry not setup, this is the system default
        If Expiry = 0 Then Return True

        Dim _passwordHistory As Business.AppUsersPasswordHistory = Business.AppUsersPasswordHistory.RetrieveLastPassword(UserID)
        If IsNothing(_passwordHistory) Then

            'if there is no password history then add a blank password to history
            Dim _newPasswordHistory As New Business.AppUsersPasswordHistory
            Dim _user As Business.User = Business.User.RetreiveByID(Session.CurrentUser.ID)

            With _newPasswordHistory
                ._UserId = _user._ID
                ._Password = ""
                ._LastUsed = Today
                .Store()
            End With

        Else
            'check if password has expired by checking the date of the most recent password
            'in history against today's date - the expiry parameter
            If _passwordHistory._LastUsed < DateAdd(DateInterval.Day, Expiry * -1, Today) Then Return False
        End If

        Return True

    End Function

End Class
