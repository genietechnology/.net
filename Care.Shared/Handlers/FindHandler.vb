﻿Imports Care.Global

Public Class FindHandler

    Public Shared Function ReturnLikeClause(ByVal FieldName As String, ByVal Criteria As String) As String

        Dim _SQL As String = ""
        Criteria = Criteria.Trim

        If Criteria.Length > 0 Then

            Dim _Words As String() = Criteria.Split(Chr(32))
            For Each _Word As String In _Words
                _SQL += " and " + FieldName + " like '%" + _Word + "%'"
            Next

        End If

        Return _SQL

    End Function

End Class
