﻿Option Strict On

Imports Care.Global
Imports System.Net.Mail
Imports System.Text.RegularExpressions

Public Class EmailHandler

    Public Shared Function AddressIsValid(ByVal EmailAddress As String) As Boolean
        Static _Exp As New Regex("^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$")
        Return _Exp.IsMatch(EmailAddress)
    End Function

    Public Shared Sub PreviewEmail(ByVal SenderEmail As MailAddress, ByVal Recipient As String, ByVal Subject As String, ByVal Body As String, ByVal AttachmentPath As String)

        Dim _frm As New frmSendEmail()
        With _frm

            If SenderEmail IsNot Nothing Then
                .SenderName = SenderEmail.DisplayName
                .SenderEmail = SenderEmail.Address
            End If

            .Recipient = Recipient
            .Subject = Subject
            .Body = Body
            .AttachmentPath = AttachmentPath

            .HideTestButton = True
            .CloseOnSend = True

            .ShowDialog()
            .Dispose()

        End With

        _frm = Nothing

    End Sub

#Region "Overloaded Method Signatures"

    Public Shared Function SendHTMLEmail(ByVal SenderEmail As MailAddress, ByVal RecipientEmail As String, ByVal Subject As String, ByVal Body As String) As Integer
        Return SendHTMLEmailWithAttachment(SenderEmail, RecipientEmail, Subject, Body, "", Nothing, Nothing)
    End Function

    Public Shared Function SendHTMLEmail(ByVal SenderEmail As MailAddress, ByVal RecipientEmail As String, ByVal Subject As String, ByVal Body As String, ByVal CC As List(Of String), ByVal BCC As List(Of String)) As Integer
        Return SendHTMLEmailWithAttachment(SenderEmail, RecipientEmail, Subject, Body, "", CC, BCC)
    End Function

    Public Shared Function SendEmail(ByVal SenderEmail As MailAddress, ByVal RecipientEmail As String, ByVal Subject As String, ByVal Body As String) As Integer
        Return SendEmailWithAttachment(SenderEmail, RecipientEmail, Subject, Body, "", Nothing, Nothing)
    End Function

    Public Shared Function SendEmail(ByVal SenderEmail As MailAddress, ByVal RecipientEmail As String, ByVal Subject As String, ByVal Body As String, ByVal CC As List(Of String), ByVal BCC As List(Of String)) As Integer
        Return SendEmailWithAttachment(SenderEmail, RecipientEmail, Subject, Body, "", CC, BCC)
    End Function

    Public Shared Function SendEmailWithAttachment(ByVal SenderEmail As MailAddress, ByVal RecipientEmail As String, ByVal Subject As String, ByVal Body As String, ByVal AttachmentPath As String) As Integer
        Return SendEmailWithAttachment(SenderEmail, RecipientEmail, Subject, Body, AttachmentPath, Nothing, Nothing)
    End Function

    Public Shared Function SendEmailReturnStatus(ByVal SenderEmail As MailAddress, ByVal RecipientEmail As String, ByVal Subject As String, ByVal Body As String, ByRef Errors As String) As Integer
        Return SendEmailWithAttachmentReturnStatus(SenderEmail, RecipientEmail, Subject, Body, "", Errors)
    End Function

#End Region

    Public Shared Function SendHTMLEmailWithAttachment(ByVal SenderEmail As MailAddress, ByVal RecipientEmail As String, ByVal Subject As String, ByVal Body As String,
                                                       ByVal AttachmentPath As String, ByVal CC As List(Of String), ByVal BCC As List(Of String)) As Integer

        Dim _Result As Integer = 0
        Dim _Email As New EmailMessage
        With _Email
            If SenderEmail IsNot Nothing Then
                .SenderName = SenderEmail.DisplayName
                .SenderEmail = SenderEmail.Address
            End If
            .Recipient = RecipientEmail
            .Subject = Subject
            .BodyFormat = EmailMessage.EnumBodyFormat.HTMLText
            .Body = Body
            .AttachmentPath = AttachmentPath
            .CC = CC
            .BCC = BCC
            .Send()
        End With

        _Result = _Email.SendStatus
        _Email = Nothing

        Return _Result

    End Function

    Public Shared Function SendEmailWithAttachment(ByVal SenderEmail As MailAddress, ByVal RecipientEmail As String, ByVal Subject As String, ByVal Body As String,
                                                   ByVal AttachmentPath As String, ByVal CC As List(Of String), ByVal BCC As List(Of String)) As Integer

        Dim _Result As Integer = 0
        Dim _Email As New EmailMessage
        With _Email
            If SenderEmail IsNot Nothing Then
                .SenderName = SenderEmail.DisplayName
                .SenderEmail = SenderEmail.Address
            End If
            .Recipient = RecipientEmail
            .Subject = Subject
            .Body = Body
            .AttachmentPath = AttachmentPath
            .CC = CC
            .BCC = BCC
            .Send()
        End With

        _Result = _Email.SendStatus
        _Email = Nothing

        Return _Result

    End Function

    Public Shared Function SendEmailWithAttachmentReturnStatus(ByVal SenderEmail As MailAddress, ByVal RecipientEmail As String, ByVal Subject As String, ByVal Body As String,
                                                               ByVal AttachmentPath As String, ByRef Errors As String) As Integer

        Dim _Email As New EmailMessage
        With _Email
            If SenderEmail IsNot Nothing Then
                .SenderName = SenderEmail.DisplayName
                .SenderEmail = SenderEmail.Address
            End If
            .Recipient = RecipientEmail
            .Subject = Subject
            .Body = Body
            .AttachmentPath = AttachmentPath
        End With

        Dim _Result As Integer = _Email.Send
        Errors = _Email.SendErrors

        'if we have a -4 error, it could be the server is busy or something
        If _Result = -4 Then

            'lets try 3 more times before we give up
            Dim _Retry As Integer = 1
            While _Retry < 4

                'lets try again...
                _Result = _Email.Send
                Errors = _Email.SendErrors

                Select Case _Result

                    Case 0
                        'trying again has worked as the email has gone :-)
                        _Retry = 4

                    Case -4
                        'still having same error
                        _Retry += 1

                    Case Else
                        'different error now :-(
                        _Retry = 4

                End Select

            End While

        End If

        _Email = Nothing

        Return _Result

    End Function

End Class
