﻿Imports System.Windows.Forms

Public Class FormHandler

    Private Shared Function GetAssembly(ByVal AssemblyName As String) As System.Reflection.Assembly

        Dim _asbpath As String = Application.StartupPath + "\" + AssemblyName + ".dll"

        If IO.File.Exists(_asbpath) Then

            Dim _asb As System.Reflection.Assembly = Nothing
            Try
                _asb = System.Reflection.Assembly.LoadFrom(_asbpath)

            Catch ex As Exception
                CareMessage("Could not load Assembly: " + ex.Message, MessageBoxIcon.Exclamation, "GetAssembly")
                _asb = Nothing
            End Try

            Return _asb

        Else
            CareMessage("Could not find Assembly: " + _asbpath, MessageBoxIcon.Exclamation, "GetAssembly")
            Return Nothing
        End If

    End Function

    Public Shared Function ReturnInstance(Of T)(ByVal AssemblyName As String, ByVal FormName As String, ByRef ErrorHit As Exception) As T

        Dim _asb As System.Reflection.Assembly = GetAssembly(AssemblyName)
        If _asb IsNot Nothing Then

            Try
                Dim _Name As String = AssemblyName & "." & FormName
                Dim _Return As T = DirectCast(_asb.CreateInstance(_Name), T)
                Return _Return

            Catch ex As Exception
                ErrorHit = ex
                Return Nothing
            End Try

        Else
            Return Nothing
        End If

    End Function

    Public Shared Sub LoadFormUsingReflection(ByVal AssemblyName As String, ByVal FormName As String, ByVal FormCaption As String, ByVal ShowDialog As Boolean)

        'check if the form is already open, if it is just give it focus
        If FormAlreadyOpen(FormName) Then

        Else

            Cursor.Current = Cursors.AppStarting
            Application.DoEvents()

            Dim _Ex As Exception = Nothing
            Dim _Form As Form = ReturnInstance(Of Form)(AssemblyName, FormName, _Ex)
            If _Form IsNot Nothing Then
                ShowMDIForm(_Form, FormCaption, False)
            Else
                Cursor.Current = Cursors.Default
                Application.DoEvents()
                Dim _Mess As String = "Error Loading Form."
                If _Ex IsNot Nothing Then _Mess += vbCrLf + vbCrLf + "Message: " + _Ex.Message.ToString
                CareMessage(_Mess, MessageBoxIcon.Error, "LoadFormUsingReflection: " & AssemblyName & "." & FormName)
            End If

        End If

    End Sub

    Private Shared Function FormAlreadyOpen(ByVal FormName As String) As Boolean

        Dim _found As Boolean = False
        Dim _frm As Form
        For Each _frm In Session.MDIForm.MDIChildrenArray
            If _frm.Name.ToLower = FormName.ToLower Then
                _found = True
                _frm.WindowState = FormWindowState.Normal
                _frm.BringToFront()
                Exit For
            End If
        Next

        Return _found

    End Function

    Public Shared Sub ShowMDIForm(ByVal FormObject As Form, ByVal FormCaption As String, ByVal Modal As Boolean)

        If FormObject IsNot Nothing Then

            FormObject.Text = FormCaption

            If Modal Then
                With FormObject
                    .StartPosition = FormStartPosition.CenterScreen
                    .ShowDialog()
                End With
            Else
                With FormObject
                    .MdiParent = CType(Session.MDIForm, Form)
                    .StartPosition = FormStartPosition.Manual
                    .Left = 0
                    .Top = 0
                    .Show()
                End With
            End If

        End If

    End Sub

End Class
