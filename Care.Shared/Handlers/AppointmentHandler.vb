﻿Imports Care.Global

Public Class AppointmentHandler

    Public Shared Function CreateAppointment(ByVal LinkForm As String, LinkID As Guid, ByRef AppointmentID As Guid?, _
                                             Optional ByVal Subject As String = "", _
                                             Optional ByVal Location As String = "", _
                                             Optional ByVal Description As String = "", _
                                             Optional ByVal Label As Integer = 0 _
                                             ) As Boolean

        Dim _frm As New frmNewAppointment
        With _frm
            .AppointmentSubject = Subject
            .AppointmentLocation = Location
            .AppointmentDescription = Description
            .AppointmentLabel = Label
            .DefaultDatesAndInterval()
        End With

        If _frm.ShowDialog = DialogResult.OK Then

            Dim _Start As Date = ValueHandler.BuildDateTime(_frm.AppointmentStartDate, _frm.AppointmentStartTime)
            Dim _End As Date = ValueHandler.BuildDateTime(_frm.AppointmentEndDate, _frm.AppointmentEndTime)

            AppointmentID = Guid.NewGuid
            Dim _App As New Business.Appointment
            With _App

                ._ID = AppointmentID

                ._StartDate = _Start
                ._EndDate = _End

                ._Subject = _frm.AppointmentSubject
                ._Location = _frm.AppointmentLocation
                ._Description = _frm.AppointmentDescription

                ._LinkForm = LinkForm
                ._LinkId = LinkID

                ._UserId = Session.CurrentUser.ID
                ._Stamp = Now

                .Store()

            End With

            Return True

        End If

        Return False

    End Function

    Public Shared Function CreateAppointment(ByVal LinkForm As String, ByVal LinkID As Guid, _
                                             ByVal StartDate As Date, ByVal Duration As Integer, ByVal Subject As String, _
                                             ByRef AppointmentID As Guid?) As Boolean

        Dim _A As New Business.Appointment
        With _A

            ._LinkForm = LinkForm
            ._LinkId = LinkID

            ._StartDate = StartDate
            ._EndDate = StartDate.AddMinutes(Duration)

            ._Subject = Subject
            ._Type = 0
            ._Status = 0
            ._Label = 0
            ._Allday = False

            ._UserId = Session.CurrentUser.ID
            ._Stamp = Now

            .Store()

        End With

        Return True

    End Function

End Class
