﻿Option Strict Off

Imports Care.Global
Imports System.Drawing

Public Class ControlHandler

    Public Enum Mode
        Add = 1
        Edit = 2
        Blank = 3
        Locked = 0
    End Enum

    Public Enum DebugMode
        None = 0
        MaxLength = 1
        ControlName = 2
        Tag = 3
    End Enum

#Region "Control Event Handlers"

    Public Sub CreateEvents(ByVal ControlCollection As Control.ControlCollection)

        'Dim Ctrl As New Control
        'For Each Ctrl In ControlCollection

        '    If Ctrl.CompanyName = "Care.Controls" Then
        '        CreateEvent(Ctrl)
        '    Else
        '        If Ctrl.Controls.Count > 0 Then
        '            CreateEvents(Ctrl.Controls)
        '        Else
        '            CreateEvent(Ctrl)
        '        End If

        '    End If

        'Next

    End Sub

    Private Sub CreateEvent(ByVal ControlItem As Control, Optional ByVal ShowMaxLengths As Boolean = False)

        'If ControlItem.Tag Is Nothing Then Exit Sub

        'If ControlItem.Tag.ToString.ToUpper.Contains("M") Then
        '    AddHandler ControlItem.Validating, AddressOf CheckMandatory
        'End If

    End Sub

#End Region

    Public Sub SetControls(ByVal SetMode As Mode, ByRef ControlCollection As Control.ControlCollection, Optional ByVal Debugging As DebugMode = DebugMode.None)

        Dim Ctrl As New Control
        For Each Ctrl In ControlCollection

            If Ctrl.CompanyName = "Care Software Ltd" Then

                Select Case Ctrl.GetType.Name.ToLower

                    Case "caretab"

                        Dim _Tab As Care.Controls.CareTab = Ctrl
                        If SetMode = Mode.Locked Then
                            _Tab.EnableTabs()
                        Else
                            _Tab.DisableTabs()
                        End If

                        SetControls(SetMode, Ctrl.Controls, Debugging)

                    Case "careframe"
                        If Debugging = DebugMode.ControlName Then Ctrl.Text = Ctrl.Name
                        If Debugging = DebugMode.Tag Then Ctrl.Text = Ctrl.Tag.ToString
                        SetControls(SetMode, Ctrl.Controls, Debugging)

                    Case Else
                        SetControl(SetMode, Ctrl, Debugging)

                End Select

            Else

                Select Case Ctrl.GetType.Name.ToLower

                    Case "memoedit"
                        SetControl(SetMode, Ctrl, Debugging)

                    Case Else

                        If Ctrl.Controls.Count > 0 Then
                            SetControls(SetMode, Ctrl.Controls, Debugging)
                            If Ctrl.GetType.Name.ToLower = "groupbox" Then
                                If Debugging = DebugMode.ControlName Then Ctrl.Text = Ctrl.Name
                                If Debugging = DebugMode.Tag Then Ctrl.Text = Ctrl.Tag.ToString
                            End If
                        Else
                            SetControl(SetMode, Ctrl, Debugging)
                        End If

                End Select

            End If

        Next

    End Sub

    Private Sub SetButton(ByVal SetMode As Mode, ByVal ControlItem As Control)

        If SetMode = Mode.Blank Then Exit Sub
        If ControlItem.Tag Is Nothing Then Exit Sub
        If ControlItem.Tag.ToString = "" Then Exit Sub

        Dim _ButtonEnabled As Boolean = False
        If SetMode = Mode.Locked Then
            'defaults are effectively locked, unless the tag contains (I)nverted or (Y) - Always Enabled
            If ControlItem.Tag.ToString.ToUpper.Contains("I") Or ControlItem.Tag.ToString.ToUpper.Contains("Y") Then
                _ButtonEnabled = True
            End If
        Else
            'buttons are enabled, unless setup as inverted
            If ControlItem.Tag.ToString.ToUpper.Contains("I") Then
                _ButtonEnabled = False
            Else
                _ButtonEnabled = True
            End If
        End If

        ControlItem.Enabled = _ButtonEnabled

    End Sub

    Private Sub SetControl(ByVal SetMode As Mode, ByVal ControlItem As Control, Optional ByVal Debugging As DebugMode = DebugMode.None)

        'certain controls are ignored by this routine
        If Debugging = DebugMode.None Then
            If ControlItem.GetType.Name.ToLower = "carelabel" Then Exit Sub
            If ControlItem.GetType.Name.ToLower = "picturebox" Then Exit Sub
        End If

        'TAG SETTINGS
        '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        'A = Control supports Add Mode 
        'E = Control supports Edit Mode
        'B = Control supports Blanking
        'R = Make ReadOnly (Textbox Only)
        'I = Inverted (Button Only)
        'Y = Enabled Constantly (Button Only)
        '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        'ignore controls unless they have a tag set
        'the only control that can be amended that doesn't have a tag set is the button or tab
        Select Case ControlItem.GetType.Name.ToLower

            Case "carebutton"
                SetButton(SetMode, ControlItem)
                Exit Sub

            Case Else
                If Debugging = DebugMode.None Then If ControlItem.Tag Is Nothing Then Exit Sub

        End Select

        Dim _ChangeColour As System.Drawing.Color = Session.ChangeColour
        Dim _Blank As Boolean = False
        Dim _Enabled As Boolean = False
        Dim _CanChange As Boolean = False
        Dim _Tabstop As Boolean = False

        If ControlItem.Tag IsNot Nothing Then

            If ControlItem.Tag.ToString.ToLower.Contains("R") Then
                If SetMode = Mode.Add And ControlItem.Tag.ToString.ToUpper.Contains("B") Then _Blank = True
                _Enabled = True
                _CanChange = False
                _Tabstop = False
            Else

                'add mode
                If Not ControlItem.Tag Is Nothing Then
                    If SetMode = Mode.Add And ControlItem.Tag.ToString.ToUpper.Contains("A") Then
                        _Blank = True
                        _Enabled = True
                        _CanChange = True
                        _Tabstop = True
                    End If
                End If

                'edit mode
                If Not ControlItem.Tag Is Nothing Then
                    If SetMode = Mode.Edit And ControlItem.Tag.ToString.ToUpper.Contains("E") Then
                        _Blank = False
                        _Enabled = True
                        _CanChange = True
                        _Tabstop = True
                    End If
                End If

                'blank mode
                If SetMode = Mode.Blank Then
                    _Blank = True
                End If

            End If

        End If

        Select Case ControlItem.GetType.Name.ToLower

            Case "carelabel"
                'debugging
                If Debugging = DebugMode.ControlName Then ControlItem.Text = ControlItem.Name.ToString
                If Debugging = DebugMode.Tag Then ControlItem.Text = ControlItem.Tag.ToString

            Case "caretextbox"

                Dim _textbox As Care.Controls.CareTextBox
                _textbox = CType(ControlItem, Care.Controls.CareTextBox)

                If Debugging <> DebugMode.None Then
                    If Debugging = DebugMode.ControlName Then _textbox.Text = _textbox.Name.ToString
                    If Debugging = DebugMode.Tag Then _textbox.Text = _textbox.Tag.ToString
                    If Debugging = DebugMode.MaxLength Then _textbox.Text = _textbox.MaxLength.ToString
                Else
                    If _Blank Then _textbox.Text = ""
                    _textbox.Enabled = True
                    _textbox.ReadOnly = Not _Enabled
                    _textbox.TabStop = _Tabstop

                    If _CanChange Then
                        _textbox.BackColor = _ChangeColour
                    Else
                        _textbox.ResetBackColor()
                    End If

                End If

            Case "carecheckbox"

                Dim _check As Care.Controls.CareCheckBox
                _check = CType(ControlItem, Care.Controls.CareCheckBox)

                If _Blank Then _check.Checked = False
                _check.Enabled = _Enabled
                _check.TabStop = _Tabstop

                If _CanChange Then
                    _check.BackColor = _ChangeColour
                Else
                    _check.ResetBackColor()
                End If

            Case "carecombobox"

                Dim _combo As Care.Controls.CareComboBox
                _combo = CType(ControlItem, Care.Controls.CareComboBox)

                If _Blank Then _combo.SelectedIndex = -1
                _combo.Enabled = True
                _combo.ReadOnly = Not _Enabled
                _combo.TabStop = _Tabstop
                _combo.UseWaitCursor = False

                If _CanChange Then
                    _combo.BackColor = _ChangeColour
                Else
                    _combo.ResetBackColor()
                End If

            Case "carecheckedcombobox"

                Dim checkedcombo As Care.Controls.CareCheckedComboBox
                checkedcombo = CType(ControlItem, Care.Controls.CareCheckedComboBox)

                If _Blank Then checkedcombo.SelectedItems = Nothing
                checkedcombo.Enabled = True
                checkedcombo.ReadOnly = Not _Enabled
                checkedcombo.TabStop = _Tabstop
                checkedcombo.UseWaitCursor = False

                If _CanChange Then
                    checkedcombo.BackColor = _ChangeColour
                Else
                    checkedcombo.ResetBackColor()
                End If

            Case "caredatetime"

                Dim _cc As Care.Controls.CareDateTime
                _cc = CType(ControlItem, Care.Controls.CareDateTime)

                If _Blank Then _cc.Text = ""
                _cc.Enabled = _Enabled
                _cc.TabStop = _Tabstop

                If _CanChange Then
                    _cc.BackColor = _ChangeColour
                Else
                    _cc.ResetBackColor()
                End If

            Case "memoedit"

                Dim _memobox As DevExpress.XtraEditors.MemoEdit
                _memobox = CType(ControlItem, DevExpress.XtraEditors.MemoEdit)

                If Debugging <> DebugMode.None Then
                    If Debugging = DebugMode.ControlName Then _memobox.Text = _memobox.Name.ToString
                    If Debugging = DebugMode.Tag Then _memobox.Text = _memobox.Tag.ToString
                    If Debugging = DebugMode.MaxLength Then _memobox.Text = _memobox.Properties.MaxLength.ToString
                Else

                    If _Blank Then _memobox.Text = ""
                    _memobox.Enabled = True
                    _memobox.Properties.ReadOnly = Not _Enabled
                    _memobox.TabStop = _Tabstop

                    If _CanChange Then
                        _memobox.BackColor = _ChangeColour
                    Else
                        _memobox.ResetBackColor()
                    End If

                End If

            Case "careaddress"

                Dim _add As Care.Address.CareAddress
                _add = CType(ControlItem, Care.Address.CareAddress)

                If Debugging <> DebugMode.None Then
                    If Debugging = DebugMode.ControlName Then _add.Text = _add.Name.ToString
                    If Debugging = DebugMode.Tag Then _add.Text = _add.Tag.ToString
                    If Debugging = DebugMode.MaxLength Then _add.Text = _add.MaxLength.ToString
                Else

                    If _Blank Then _add.Text = ""

                    _add.Enabled = True
                    _add.ReadOnly = Not _Enabled
                    _add.TabStop = _Tabstop

                    If _CanChange Then
                        _add.BackColor = _ChangeColour
                    Else
                        _add.ResetBackColor()
                    End If

                End If

            Case "caretelephonenumber"

                Dim _tn As Care.Shared.CareTelephoneNumber
                _tn = CType(ControlItem, Care.Shared.CareTelephoneNumber)

                If Debugging <> DebugMode.None Then
                    If Debugging = DebugMode.ControlName Then _tn.Text = _tn.Name.ToString
                    If Debugging = DebugMode.Tag Then _tn.Text = _tn.Tag.ToString
                    If Debugging = DebugMode.MaxLength Then _tn.Text = _tn.MaxLength.ToString
                Else

                    If _Blank Then _tn.Text = ""

                    _tn.Enabled = True
                    _tn.ReadOnly = Not _Enabled
                    _tn.TabStop = _Tabstop

                    If _CanChange Then
                        _tn.BackColor = _ChangeColour
                    Else
                        _tn.ResetBackColor()
                    End If

                End If

            Case "careemailaddress"

                Dim _ea As Care.Shared.CareEmailAddress
                _ea = CType(ControlItem, Care.Shared.CareEmailAddress)

                If Debugging <> DebugMode.None Then
                    If Debugging = DebugMode.ControlName Then _ea.Text = _ea.Name.ToString
                    If Debugging = DebugMode.Tag Then _ea.Text = _ea.Tag.ToString
                    If Debugging = DebugMode.MaxLength Then _ea.Text = _ea.MaxLength.ToString
                Else

                    If _Blank Then _ea.Text = ""

                    _ea.Enabled = True
                    _ea.ReadOnly = Not _Enabled
                    _ea.TabStop = _Tabstop

                    If _CanChange Then
                        _ea.BackColor = _ChangeColour
                    Else
                        _ea.ResetBackColor()
                    End If

                End If

            Case Else
                If Debugging = DebugMode.None Then CareMessage("Unhandled Control - " & ControlItem.GetType.Name, MessageBoxIcon.Exclamation, "ControlHandler.SetControl")

        End Select

    End Sub

    Public Sub DebugPopup(ByRef ActiveForm As Form)

        Dim _FormName As String = ""
        Dim _Base As String = ""
        Dim _ControlName As String = ""
        Dim _ControlType As String = ""
        Dim _ControlTag As String = ""
        Dim _ControlMaxLength As String = ""
        Dim _ControlBinding As String = ""
        Dim _ControlParent As String = ""
        Dim _ControlParentType As String = ""
        Dim _ControlProductName As String = ""
        Dim _ControlProductVersion As String = ""
        Dim _Msg As String

        _FormName = "Form Name = " & ActiveForm.GetType.FullName
        ActiveForm.Text = _FormName

        _Base = "Base Hierarchy:" & vbCrLf & ErrorHandler.ReturnBaseHierarchy(ActiveForm.GetType.BaseType)

        If Not ActiveForm.ActiveControl Is Nothing Then

            _ControlName = "Control Name = " & ActiveForm.ActiveControl.Name
            _ControlType = "Control Type = " & ActiveForm.ActiveControl.GetType.Name

            If ActiveForm.ActiveControl.GetType.Name = "TextBox" Then
                Dim _txt As TextBox = CType(ActiveForm.ActiveControl, TextBox)
                _ControlMaxLength = "Control Max Length = " & _txt.MaxLength.ToString
            Else
                _ControlMaxLength = "Control Max Length = Nothing"
            End If

            If ActiveForm.ActiveControl.Tag Is Nothing Then
                _ControlTag = "Control Tag = Nothing"
            Else
                _ControlTag = "Control Tag = " & ActiveForm.ActiveControl.Tag.ToString
            End If

            If ActiveForm.ActiveControl.DataBindings.Count > 0 Then
                Dim _Object As String = ActiveForm.ActiveControl.DataBindings.Item(0).BindingManagerBase.Current.ToString
                Dim _Field As String = ActiveForm.ActiveControl.DataBindings.Item(0).BindingMemberInfo.BindingField
                Dim _Property As String = ActiveForm.ActiveControl.DataBindings.Item(0).PropertyName
                _ControlBinding = "Control Binding = " & _Object & "." & _Field & ", " & _Property
            Else
                _ControlBinding = "Control Binding = Nothing"
            End If

            _ControlParent = "Control Parent = " & ActiveForm.ActiveControl.Parent.Name
            _ControlParentType = "Control Parent Type = " & ActiveForm.ActiveControl.Parent.GetType.Name
            _ControlProductName = "Control Product Name = " & ActiveForm.ActiveControl.ProductName
            _ControlProductVersion = "Control Product Version = " & ActiveForm.ActiveControl.ProductVersion

        End If

        _Msg = _FormName & vbCrLf & vbCrLf &
               _Base & vbCrLf &
               _ControlName & vbCrLf &
               _ControlType & vbCrLf &
               _ControlTag & vbCrLf &
               _ControlMaxLength & vbCrLf &
               _ControlBinding & vbCrLf & vbCrLf &
               _ControlParent & vbCrLf &
               _ControlParentType & vbCrLf &
               _ControlProductName & vbCrLf &
               _ControlProductVersion

        CareMessage(_Msg, MessageBoxIcon.Information, "Debug Information")


    End Sub

    ''' <summary>
    ''' Mandatory fields are activated by entering "M" into the Tag. The AccessibleName field provides a "friendly name" for each field.
    ''' </summary>
    ''' <param name="ControlsCollection"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Validate(ByVal ControlsCollection As Control.ControlCollection) As Boolean

        Dim _OK As Boolean = True
        Dim _List As String = ValidateControlCollection(ControlsCollection)

        If _List.Length > 0 Then
            CareMessage("The following fields are mandatory:" & vbCrLf & vbCrLf & _List, MessageBoxIcon.Exclamation, "Field Validation")
            _OK = False
        End If

        Return _OK

    End Function

    Private Function ValidateControl(ByRef ControlIn As Control) As String

        Dim _Return As String = ""

        If ControlIn.Visible Then
            If ControlIn.Tag IsNot Nothing AndAlso ControlIn.Tag.ToString.ToUpper.Contains("M") Then
                If ControlIn.Text = "" Then
                    ControlIn.BackColor = Color.LightCoral
                    If ControlIn.AccessibleName = "" Then
                        _Return += "> " + ControlIn.Name + vbCrLf
                    Else
                        _Return += "> " + ControlIn.AccessibleName + vbCrLf
                    End If
                Else
                    If ControlIn.BackColor = Color.LightCoral Then ControlIn.BackColor = Session.ChangeColour
                End If
            End If
        End If

        Return _Return

    End Function

    Private Function ValidateControlCollection(ByVal ControlsCollection As Control.ControlCollection) As String

        Dim _List As String = ""
        For Each _ctrl As Control In ControlsCollection

            If _ctrl.CompanyName = "Care Software Ltd" Then

                If _ctrl.GetType.Name.ToLower = "caretab" OrElse _ctrl.GetType.Name.ToLower = "careframe" Then
                    _List += ValidateControlCollection(_ctrl.Controls)
                Else
                    _List += ValidateControl(_ctrl)
                End If

            Else

                If _ctrl.Controls.Count > 0 Then
                    _List += ValidateControlCollection(_ctrl.Controls)
                Else
                    _List += ValidateControl(_ctrl)
                End If

            End If

        Next

        Return _List

    End Function

    Public Shared Sub PopulateCheckedListBox(ByVal ListBoxIn As DevExpress.XtraEditors.CheckedComboBoxEdit, ByVal ConnectionString As String, ByVal SQL As String)

        ListBoxIn.Properties.Items.Clear()

        Dim _dt As DataTable = Care.Data.DAL.GetDataTablefromSQL(Session.ConnectionString, SQL)

        If _dt IsNot Nothing Then
            For Each _dr As DataRow In _dt.Rows
                If _dt.Columns.Count = 1 Then
                    ListBoxIn.Properties.Items.Add(_dr.Item(0))
                Else
                    ListBoxIn.Properties.Items.Add(_dr.Item(0), _dr.Item(1).ToString)
                End If
            Next
        End If

    End Sub

    Public Shared Sub PopulateCheckedListBox(ByVal ListBoxIn As DevExpress.XtraEditors.CheckedComboBoxEdit, ByVal ListName As String)

        Dim _SQL As String = "select AppListItems.ID, AppListItems.name from AppListItems" & _
                             " left join AppLists on AppLists.ID = AppListItems.list_id" & _
                             " where AppLists.name = '" & ListName & "'" & _
                             " order by AppListItems.seq, AppListItems.name"

        PopulateCheckedListBox(ListBoxIn, Session.ConnectionString, _SQL)

    End Sub

    Public Shared Sub PopulateCheckedListBox(ByVal ListBoxIn As DevExpress.XtraEditors.CheckedComboBoxEdit, ByRef Tags As List(Of String))

        ListBoxIn.Properties.Items.Clear()

        If Tags IsNot Nothing Then
            For Each _Tag In Tags
                ListBoxIn.Properties.Items.Add(_Tag)
            Next
        End If

    End Sub

End Class
