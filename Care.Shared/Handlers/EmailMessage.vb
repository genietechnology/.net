﻿Option Strict On

Imports Care.Global
Imports Care.Shared
Imports System.Net.Mail

Public Class EmailMessage

    Private m_SendStatus As Integer = -1 'assume something goes wrong
    Private m_SendErrors As String = "Unhandled Exception"

    Public Enum EnumBodyFormat
        PlainText
        HTMLText
    End Enum

    Public Property BodyFormat As EnumBodyFormat = EnumBodyFormat.PlainText
    Public Property Recipient() As String
    Public Property SenderEmail() As String
    Public Property SenderName() As String
    Public Property Subject() As String
    Public Property Body() As String
    Public Property AttachmentPath() As String
    Public Property CC As New List(Of String)
    Public Property BCC As New List(Of String)

    Public ReadOnly Property SendStatus As Integer
        Get
            Return m_SendStatus
        End Get
    End Property

    Public ReadOnly Property SendErrors As String
        Get
            Return m_SendErrors
        End Get
    End Property

    Public Function Send() As Integer

        m_SendErrors = ""

        If Session.MandrillEnabled Then
            m_SendStatus = SendViaMandrill()
        Else
            m_SendStatus = SendViaSMTP()
        End If

        Return m_SendStatus

    End Function

    Private Function SendViaMandrill() As Integer
        Return -9999
    End Function

    Private Function SendViaSMTP() As Integer

        If Not Session.SMTPEnabled Then
            m_SendErrors = "SMTP not enabled"
            Return -1
        End If

        Dim _SMTPSenderEmail As String = SenderEmail
        Dim _SMTPSenderName As String = SenderName
        Dim _SMTPServer As String = Session.SMTPServer
        Dim _SMTPPort As Integer = Session.SMTPPort
        Dim _SMTPSSL As Boolean = Session.SMTPSSL

        'override the actual recipient if we are in test mode...
        If Session.SMTPTestMode Then
            Recipient = "emailtest@caresoftware.co.uk"
        End If

        'default the sender details if not supplied
        If _SMTPSenderEmail = "" Then _SMTPSenderEmail = Session.SMTPSenderEmail
        If _SMTPSenderName = "" Then _SMTPSenderName = Session.SMTPSenderName

        Dim _SMTPAuthentication As Boolean = Session.SMTPAuth
        Dim _SMTPUser As String = Session.SMTPUser
        Dim _SMTPPassword As String = Session.SMTPPassword

        Dim _SenderAddress As MailAddress = SetEmailAddress(_SMTPSenderEmail, _SMTPSenderName)
        If _SenderAddress IsNot Nothing Then

            Dim _RecipientAddress As MailAddress = SetEmailAddress(Recipient)
            If _RecipientAddress IsNot Nothing Then

                Dim _msg As New MailMessage(_SenderAddress, _RecipientAddress)
                With _msg
                    .Subject = Subject
                    .Body = Body
                End With

                If Me.BodyFormat = EnumBodyFormat.HTMLText Then
                    _msg.IsBodyHtml = True
                Else
                    _msg.IsBodyHtml = False
                End If

                'CC
                If Not Session.SMTPTestMode Then
                    If Me.CC IsNot Nothing AndAlso Me.CC.Any Then
                        For Each item In Me.CC
                            If Not String.IsNullOrWhiteSpace(item.ToString) Then _msg.CC.Add(item.ToString)
                        Next
                    End If
                End If

                'BCC
                If Not Session.SMTPTestMode Then
                    If Session.SMTPBCC <> "" Then
                        If Me.BCC Is Nothing Then Me.BCC = New List(Of String)
                        Me.BCC.Add(Session.SMTPBCC)
                    End If
                    If Me.BCC IsNot Nothing AndAlso Me.BCC.Any Then
                        For Each item In Me.BCC
                            If Not String.IsNullOrWhiteSpace(item.ToString) Then _msg.Bcc.Add(item.ToString)
                        Next
                    End If
                End If

                If AttachmentPath <> "" Then

                    'check if there is a | in the path (multiple attachments)
                    If AttachmentPath.Contains("|") Then
                        Dim _Files As String() = Split(AttachmentPath, "|")
                        For Each _f In _Files
                            If IO.File.Exists(_f) Then
                                _msg.Attachments.Add(New Attachment(_f))
                            End If
                        Next
                    Else
                        If IO.File.Exists(AttachmentPath) Then
                            _msg.Attachments.Add(New Attachment(AttachmentPath))
                        End If
                    End If

                End If

                Dim _client As New SmtpClient
                _client.Host = _SMTPServer
                _client.Port = _SMTPPort
                _client.EnableSsl = _SMTPSSL

                If _SMTPAuthentication Then

                    Dim _credentials As New Net.NetworkCredential
                    _credentials.UserName = _SMTPUser
                    _credentials.Password = _SMTPPassword

                    _client.UseDefaultCredentials = False
                    _client.Credentials = _credentials

                End If

                Try
                    _client.Send(_msg)
                    m_SendStatus = 0
                    m_SendErrors = ""

                Catch ex As SmtpException
                    m_SendStatus = -4
                    m_SendErrors = "SMTP Exception (" + ex.StatusCode.ToString + ") sending to: " + Recipient + vbCrLf + ex.Message
                End Try

                _msg.Dispose()
                _msg = Nothing

                _client = Nothing

            Else
                'receipient address problem
                m_SendStatus = -3
                m_SendErrors = "Invalid Recipient Address: " + Recipient
            End If

        Else
            'sender address problem
            m_SendStatus = -2
            m_SendErrors = "Invalid Sender Address: " + _SMTPSenderEmail
        End If

        Return m_SendStatus

    End Function

    Private Function SetEmailAddress(ByVal EmailAddress As String, Optional ByVal DisplayName As String = "") As MailAddress

        Dim _Address As MailAddress = Nothing

        Try
            If DisplayName = "" Then
                _Address = New MailAddress(EmailAddress)
            Else
                _Address = New MailAddress(EmailAddress, DisplayName)
            End If

        Catch ex As Exception
            _Address = Nothing
        End Try

        Return _Address

    End Function

End Class
