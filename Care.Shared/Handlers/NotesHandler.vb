﻿Imports Care.Global
Imports Care.Data

Public Class NotesHandler

    Public Shared Sub OpenNotes(ByVal RecordID As Guid, ByVal Caption As String)

        Session.CursorWaiting()

        Dim _frm As New frmGenericNotes(RecordID)
        _frm.Text = Caption
        _frm.ShowDialog()

        _frm.Dispose()
        _frm = Nothing

    End Sub

End Class
