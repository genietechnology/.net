﻿Imports Care.Shared.Business
Imports Care.Global
Imports Care.Data

Public Class ListHandler

    Public Class ListItem
        Public Property Name As String
        Public Property Ref1 As String
        Public Property Ref2 As String
        Public Property Ref3 As String
        Public Property Hide As Boolean
    End Class

    Public Shared Function ReturnListFromCache(ByVal ListName As String) As Dictionary.ApplicationList

        For Each _al As Care.Global.Dictionary.ApplicationList In Session.DictionaryLists
            If _al.Name.ToUpper = ListName.ToUpper Then
                Return _al
            End If
        Next

        Return Nothing

    End Function

    Public Shared Sub CacheLists()

        Session.DictionaryLists.Clear()

        Dim _SQL As String = "select ID, name from AppLists order by name"
        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _AppList As New Dictionary.ApplicationList
                _AppList.ID = New Guid(_DR.Item("ID").ToString)
                _AppList.Name = _DR.Item("name").ToString

                'AppListItems.ID, AppListItems.name, AppListItems.ref_1, AppListItems.ref_2, AppListItems.ref_3, AppListItems.hide, AppListItems.seq from AppListItems" & _

                _AppList.ListItems = New List(Of Dictionary.ApplicationList.ApplicationListItem)
                Dim _Rows As DataRowCollection = ListHandler.ReturnItemsWithReferences(_AppList.Name)
                For Each _r As DataRow In _Rows

                    Dim _i As New Dictionary.ApplicationList.ApplicationListItem
                    _i.ItemID = New Guid(_r.Item("ID").ToString)
                    _i.Name = _r.Item("name").ToString
                    _i.Ref1 = _r.Item("ref_1").ToString
                    _i.Ref2 = _r.Item("ref_2").ToString
                    _i.Ref3 = _r.Item("ref_3").ToString
                    _i.Hide = ValueHandler.ConvertBoolean(_r.Item("hide"))
                    _i.Sequence = ValueHandler.ConvertInteger(_r.Item("seq"))

                    _AppList.ListItems.Add(_i)

                Next

                Session.DictionaryLists.Add(_AppList)

            Next _DR

        End If

    End Sub

    Public Shared Function ReturnItems(ByVal ListName As String) As DataRowCollection

        Dim _SQL As String = "select AppListItems.ID, AppListItems.name from AppListItems" &
                             " left join AppLists on AppLists.ID = AppListItems.list_id" &
                             " where AppLists.name = '" & ListName & "' and hide <> 1" &
                             " order by AppListItems.seq, AppListItems.name"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        Dim _Return As DataRowCollection = _DT.Rows

        _DT.Dispose()
        _DT = Nothing

        Return _Return

    End Function

    Public Shared Function ReturnItemsWithReferences(ByVal ListName As String) As DataRowCollection

        Dim _SQL As String = "select AppListItems.ID, AppListItems.name, AppListItems.ref_1, AppListItems.ref_2, AppListItems.ref_3, AppListItems.hide, AppListItems.seq from AppListItems" &
                             " left join AppLists on AppLists.ID = AppListItems.list_id" &
                             " where AppLists.name = '" & ListName & "' and hide <> 1" &
                             " order by AppListItems.seq, AppListItems.name"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        Dim _Return As DataRowCollection = _DT.Rows

        _DT.Dispose()
        _DT = Nothing

        Return _Return

    End Function

    Public Shared Function ReturnItem(ByVal ListName As String, ByVal ItemName As String) As ListItem

        Dim _Item As ListItem = Nothing

        Dim _SQL As String = "select AppListItems.name, AppListItems.ref_1, AppListItems.ref_2, AppListItems.ref_3, AppListItems.hide from AppListItems" & _
                             " left join AppLists on AppLists.ID = AppListItems.list_id" & _
                             " where AppLists.name = '" & ListName & "'" & _
                             " and AppListItems.name = '" & ItemName & "'"

        Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DR IsNot Nothing Then

            _Item = New ListItem
            _Item.Name = _DR.Item("name").ToString
            _Item.Ref1 = _DR.Item("ref_1").ToString
            _Item.Ref2 = _DR.Item("ref_2").ToString
            _Item.Ref3 = _DR.Item("ref_3").ToString
            _Item.Hide = ValueHandler.ConvertBoolean(_DR.Item("hide"))

            _DR = Nothing

        End If

        Return _Item

    End Function

    Public Shared Function ReturnItemUsingRef1(ByVal ListName As String, ByVal Ref1 As String) As ListItem

        Dim _Item As ListItem = Nothing

        Dim _SQL As String = "select AppListItems.name, AppListItems.ref_1, AppListItems.ref_2, AppListItems.ref_3, AppListItems.hide from AppListItems" & _
                             " left join AppLists on AppLists.ID = AppListItems.list_id" & _
                             " where AppLists.name = '" & ListName & "'" & _
                             " and AppListItems.ref_1 = '" & Ref1 & "'"

        Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DR IsNot Nothing Then

            _Item = New ListItem
            _Item.Name = _DR.Item("name").ToString
            _Item.Ref1 = _DR.Item("ref_1").ToString
            _Item.Ref2 = _DR.Item("ref_2").ToString
            _Item.Ref3 = _DR.Item("ref_3").ToString
            _Item.Hide = ValueHandler.ConvertBoolean(_DR.Item("hide"))

            _DR = Nothing

        End If

        Return _Item

    End Function

    Public Shared Function ListExists(ByVal ListName As String) As Boolean

        Dim _Return As Boolean = False
        Dim _SQL As String = "select * from AppLists where name = '" + ListName + "'"
        Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DR IsNot Nothing Then
            _Return = True
            _DR = Nothing
        End If

        Return _Return

    End Function

    Public Shared Function ItemExists(ByVal ListName As String, ByVal ItemName As String) As Boolean

        Dim _Return As Boolean = False

        Dim _SQL As String = "select AppListItems.ID, AppListItems.name from AppListItems" & _
                             " left join AppLists on AppLists.ID = AppListItems.list_id" & _
                             " where AppLists.name = '" & ListName & "'" & _
                             " and AppListItems.name = '" & ItemName & "'"

        Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DR IsNot Nothing Then
            _Return = True
            _DR = Nothing
        End If

        Return _Return

    End Function

    Public Shared Sub CreateList(ByVal ListName As String, ByVal ListType As Integer)

        Dim _Type As String = "A"
        If ListType = 1 Then _Type = "S"

        Dim _List As New AppList
        With _List
            ._Name = ListName
            ._Type = _Type
            .Save()
        End With

        _List = Nothing

    End Sub

    Public Shared Sub EditList(ByVal ListID As String, ByVal ListName As String, ByVal ListType As Integer)

        Dim _Type As String = "A"
        If ListType = 1 Then _Type = "S"

        Dim _List As New AppList(New Guid(ListID))
        With _List
            ._Name = ListName
            ._Type = _Type
            .Save()
        End With

        _List = Nothing

    End Sub

    Public Shared Sub DeleteList(ByVal ListID As String)

        Dim _SQL As String = "delete from AppLists" & _
                             " where ID = '" & ListID & "'"

        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from AppListItems" & _
               " where list_id = '" & ListID & "'"

        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

    End Sub

    Public Shared Sub CreateItem(ByVal ListID As String, ByVal ItemName As String, ByVal DisplaySeq As Object, Optional ByVal Ref1 As String = "")

        Dim _Seq As Integer = 0

        Try
            _Seq = CInt(DisplaySeq)

        Catch ex As Exception

        End Try

        Dim _Item As New Business.AppListItem
        With _Item
            ._ListId = New Guid(ListID)
            ._Name = ItemName
            ._Seq = _Seq
            ._Ref1 = Ref1
            .Store()
        End With

        _Item = Nothing

    End Sub

    Public Shared Sub EditItem(ByVal ItemID As String, ByVal ItemName As String, ByVal DisplaySeq As Object)

        Dim _Seq As Integer = 0
        If DisplaySeq.ToString <> "" Then _Seq = CInt(DisplaySeq)

        Dim _List As New Business.AppListItem(New Guid(ItemID))
        With _List
            ._Name = ItemName
            ._Seq = _Seq
            .Store()
        End With

        _List = Nothing

    End Sub

    Public Shared Sub DeleteItem(ByVal ItemID As String)

        Dim _SQL As String = "delete from AppListItems" & _
                             " where ID = '" & ItemID & "'"

        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

    End Sub

    Public Shared Function DrillDown(ByVal ListName As String) As Boolean

        If Session.CurrentUser.IsStandardUser Then
            Session.SetProgressMessage("You do not have permission to Access the List Manager.", 5)
        Else

            Dim _frm As New frmListMaintenance(ListName)
            _frm.ShowDialog()

            If _frm.DialogResult = DialogResult.OK Then
                Return True
            End If

        End If

        Return False

    End Function

End Class




