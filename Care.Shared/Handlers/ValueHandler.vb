﻿Public Class ValueHandler

    Public Shared Function SQLDate(ByVal DateIn As Date) As String
        Return Format(DateIn, "yyyy-MM-dd")
    End Function

    Public Shared Function BuildDateTime(ByVal DateIn As Date, ByVal TimeIn As Date) As Date
        Dim _Return As New Date(DateIn.Year, DateIn.Month, DateIn.Day, TimeIn.Hour, TimeIn.Minute, TimeIn.Second)
        Return _Return
    End Function

    Public Shared Function DateDifferenceAsText(ByVal DateFrom As Date, ByVal DateTo As Date, _
                                     Optional ByVal IncludeYears As Boolean = True, _
                                     Optional ByVal IncludeMonths As Boolean = True, _
                                     Optional ByVal IncludeDays As Boolean = True) As String

        Dim _y As Integer, _m As Integer, _d As Integer
        Dim _yt As String = "", _mt As String = "", _dt As String = "", _return As String = ""

        If Not IsDate(DateFrom) Then Return ""
        If Not IsDate(DateTo) Then Return ""
        If DateFrom > DateTo Then Return ""

        _y = DatePart(DateInterval.Year, DateTo) - DatePart(DateInterval.Year, DateFrom)
        _m = DatePart(DateInterval.Month, DateTo) - DatePart(DateInterval.Month, DateFrom)
        _d = DatePart(DateInterval.Day, DateTo) - DatePart(DateInterval.Day, DateFrom)

        'current month is less than birthday month
        If _m < 0 Then
            _y -= 1
            _m = 12 + _m
        Else
            If _m = 0 Then
                If _d < 0 Then
                    _y -= 1
                    _m = 11
                    _d = DateFrom.Day + _d
                End If
            End If
        End If

        If _y > 0 Then
            If _y = 1 Then
                _yt = _y.ToString & " year"
            Else
                _yt = _y.ToString & " years"
            End If
        End If

        If _m > 0 Then
            If _m = 1 Then
                _mt = _m.ToString & " month"
            Else
                _mt = _m.ToString & " months"
            End If
        End If

        If _d > 0 Then
            If _d = 1 Then
                _dt = _d.ToString & " day"
            Else
                _dt = _d.ToString & " days"
            End If
        End If

        If IncludeYears And _yt.Length > 0 Then _return = _yt
        If IncludeMonths And _mt.Length > 0 Then _return += ", " & _mt
        If IncludeDays And _dt.Length > 0 Then _return += ", " & _dt

        Return _return

    End Function

    Public Shared Function ReturnValueColour(ByVal ValueIn As Decimal, Optional ByVal NegativeIsCredit As Boolean = True) As Drawing.Color

        If ValueIn = 0 Then Return Drawing.Color.Black

        If NegativeIsCredit Then
            If ValueIn < 0 Then
                Return Drawing.Color.Blue
            Else
                Return Drawing.Color.Red
            End If
        Else
            If ValueIn < 0 Then
                Return Drawing.Color.Red
            Else
                Return Drawing.Color.Blue
            End If
        End If

    End Function

    Public Shared Function ConvertInteger(ByVal ValueIn As Object) As Integer

        If ValueIn Is Nothing Then Return 0
        If ValueIn.ToString = "" Then Return 0

        Dim _Return As Integer = 0

        Try
            _Return = Integer.Parse(ValueIn.ToString)

        Catch ex As Exception
            'error oDecimal.Parsered
        End Try

        Return _Return

    End Function

    Public Shared Function ConvertDecimal(ByVal ValueIn As Object) As Decimal

        If ValueIn Is Nothing Then Return 0
        If ValueIn.ToString = "" Then Return 0

        Dim _Return As Decimal = 0

        Try
            _Return = Decimal.Parse(ValueIn.ToString)

        Catch ex As Exception
            'error oDecimal.Parsered
        End Try

        Return _Return

    End Function

    Public Shared Function ConvertDate(ByVal ValueIn As Object) As Date

        If ValueIn Is Nothing Then Return Nothing
        If ValueIn.ToString = "" Then Return Nothing

        Dim _Return As Date = Nothing

        Try
            _Return = Date.Parse(ValueIn.ToString)

        Catch ex As Exception
            'error oDecimal.Parsered
        End Try

        Return _Return

    End Function

    Public Shared Function ConvertGUID(ByVal ValueIn As Object) As Guid

        If ValueIn Is Nothing Then Return Nothing
        If ValueIn.ToString = "" Then Return Nothing

        Dim _Return As Guid = Nothing

        Try
            _Return = New Guid(ValueIn.ToString)

        Catch ex As Exception
            'error oDecimal.Parsered
        End Try

        Return _Return

    End Function

End Class
