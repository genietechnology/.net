﻿Imports System.Net.NetworkInformation

Public Class NetworkHandler

    ''' <summary>
    ''' Ping a host or IP address
    ''' </summary>
    ''' <param name="Address"></param>
    ''' <returns>TTL, if there was an error, we return a negative value</returns>
    ''' <remarks></remarks>
    Public Shared Function Ping(ByVal Address As String) As Long

        Dim _Return As Long = 0
        Dim _Ping As New System.Net.NetworkInformation.Ping

        Try

            Dim _Reply As PingReply = _Ping.Send(Address)
            Select Case _Reply.Status

                Case IPStatus.Success
                    _Return = _Reply.RoundtripTime

                Case IPStatus.TimedOut
                    _Return = -1

                Case Else
                    _Return = -2

            End Select

            _Reply = Nothing

        Catch ex As Exception
            _Return = -99
        End Try

        _Ping.Dispose()

        Return _Return

    End Function



End Class
