﻿Imports Care.Global
Imports Care.Data

Public Class UserPreferenceHandler

    Public Enum EnumUserPreferenceType
        StringType
        BooleanType
        DateType
        IntegerType
        LongType
        FilePathType
    End Enum

    Public Shared Sub Create(ByVal Name As String, ByVal DataType As EnumUserPreferenceType, ByVal Value As String)

        Dim _Prm As New Business.UserPreference
        With _Prm
            ._ID = Guid.NewGuid
            ._UserId = Session.CurrentUser.ID
            ._Name = Name
            ._Type = ReturnUserPreferenceType(DataType)
            ._Value = Value
        End With

        Business.UserPreference.SaveRecord(_Prm)

    End Sub

    Private Shared Function ReturnUserPreferenceType(ByVal DataType As EnumUserPreferenceType) As String
        Dim _Return As String = "String"
        If DataType = EnumUserPreferenceType.BooleanType Then _Return = "Boolean"
        If DataType = EnumUserPreferenceType.DateType Then _Return = "Date"
        If DataType = EnumUserPreferenceType.IntegerType Then _Return = "Integer"
        If DataType = EnumUserPreferenceType.LongType Then _Return = "Long"
        If DataType = EnumUserPreferenceType.FilePathType Then _Return = "Path"
        Return _Return
    End Function

    Public Shared Function CheckExists(ByVal UserPreferenceName As String) As Boolean

        Dim _Exists As Boolean = False

        Dim _Prm As Business.UserPreference = Business.UserPreference.RetrieveByName(UserPreferenceName)
        If _Prm IsNot Nothing Then _Exists = True

        _Prm = Nothing
        Return _Exists

    End Function

#Region "Returns"

    Public Shared Function ReturnString(ByVal UserPreferenceName As String) As String

        Dim _Return As String = ""
        Dim _Prm As Business.UserPreference = Business.UserPreference.RetrieveByName(UserPreferenceName)
        If _Prm IsNot Nothing Then
            _Return = _Prm._Value
            _Prm = Nothing
        End If

        Return _Return

    End Function

    Public Shared Function ReturnBoolean(ByVal UserPreferenceName As String) As Boolean

        Dim _Return As Boolean = False
        Dim _Prm As Business.UserPreference = Business.UserPreference.RetrieveByName(UserPreferenceName)
        If _Prm IsNot Nothing Then
            If _Prm._Value.ToUpper = "TRUE" Then
                _Return = True
            End If
            _Prm = Nothing
        End If

        Return _Return

    End Function

    Public Shared Function ReturnInteger(ByVal UserPreferenceName As String) As Integer

        Dim _Return As Integer = 0
        Dim _Prm As Business.UserPreference = Business.UserPreference.RetrieveByName(UserPreferenceName)
        If _Prm IsNot Nothing Then
            If Not Integer.TryParse(_Prm._Value, _Return) Then
                _Return = 0
            End If
            _Prm = Nothing
        End If

        Return _Return

    End Function

    Public Shared Sub SetString(ByVal UserPreferenceName As String, ByVal Value As String)

        Dim _Prm As Business.UserPreference = Business.UserPreference.RetrieveByName(UserPreferenceName)
        If _Prm IsNot Nothing Then
            _Prm._Value = Value
        End If

        Business.UserPreference.SaveRecord(_Prm)
        _Prm = Nothing

    End Sub

#End Region

End Class
