﻿Imports Care.Global
Imports Care.Data
Imports DevExpress.XtraReports.UI
Imports System.Windows.Forms

Public Class ReportHandler

    Public Enum OutputMode
        Preview
        Print
        ExportReportToPDF
        ExportToPDF
    End Enum

    Private m_Blank As Boolean
    Private m_ReportName As String = ""
    Private m_ReportFile As String = ""
    Private m_ReportOutput As ReportHandler.OutputMode = OutputMode.Preview
    Private m_ExportFile As String = ""
    Private m_Param1 As String = ""
    Private m_Param2 As String = ""
    Private m_Param3 As String = ""

    Private m_GenerateSchemaFile As Boolean = False
    Private m_SchemaFile As String = ""
    Private m_ReportSQL As String = ""
    Private m_BusinessObject As Object = Nothing
    Private m_BusinessObjectList As IEnumerable
    Private m_SubReports As New List(Of SubReport)
    Private m_ReportDataTable As DataTable = Nothing

    Private m_UseHourGlass As Boolean = True
    Private m_ShowMDIMessages As Boolean = True

    Public Property DXReport As Boolean

    Public Property ReportFile() As String
        Get
            Return m_ReportFile
        End Get
        Set(ByVal value As String)
            m_ReportFile = Session.ReportFolder & value
        End Set
    End Property

    Public Property ReportName() As String
        Get
            Return m_ReportName
        End Get
        Set(ByVal value As String)
            m_ReportName = value
        End Set
    End Property

    Public Property ReportOutput() As ReportHandler.OutputMode
        Get
            Return m_ReportOutput
        End Get
        Set(ByVal value As ReportHandler.OutputMode)
            m_ReportOutput = value
        End Set
    End Property

    Public Property ExportFile() As String
        Get
            Return m_ExportFile
        End Get
        Set(ByVal value As String)
            m_ExportFile = value
        End Set
    End Property

    Public Property BusinessObject() As Object
        Get
            Return m_BusinessObject
        End Get
        Set(ByVal value As Object)

            m_BusinessObject = value

            Dim _List As New List(Of Object)
            _List.Add(m_BusinessObject)

            m_BusinessObjectList = _List
            _List = Nothing

        End Set

    End Property

    Public Property BusinessObjectList() As IEnumerable
        Get
            Return m_BusinessObjectList
        End Get
        Set(ByVal value As IEnumerable)
            m_BusinessObjectList = value
        End Set
    End Property

    Public Property ReportDataTable() As DataTable
        Get
            Return m_ReportDataTable
        End Get
        Set(ByVal value As DataTable)
            m_ReportDataTable = value
        End Set
    End Property

    Public Property ReportSQL() As String
        Get
            Return m_ReportSQL
        End Get
        Set(ByVal value As String)
            m_ReportSQL = value
        End Set
    End Property

    Public Property Param1() As String
        Get
            Return m_Param1
        End Get
        Set(ByVal value As String)
            m_Param1 = value
        End Set
    End Property

    Public Property Param2() As String
        Get
            Return m_Param2
        End Get
        Set(ByVal value As String)
            m_Param2 = value
        End Set
    End Property

    Public Property Param3() As String
        Get
            Return m_Param3
        End Get
        Set(ByVal value As String)
            m_Param3 = value
        End Set
    End Property

    Public Property BlankReport As Boolean
        Get
            Return m_Blank
        End Get
        Set(value As Boolean)
            m_Blank = value
        End Set
    End Property

    Private Function PopulateReportData() As Boolean

        If m_ReportSQL = "" Then Return False

        m_ReportDataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, m_ReportSQL)
        If m_ReportDataTable IsNot Nothing Then

            m_ReportDataTable.TableName = ReturnTableName(m_ReportFile)
            If WriteSchema() Then
                Return True
            Else
                Return False
            End If

        Else
            Return False
        End If

    End Function

    Private Function WriteSchema() As Boolean

        Try
            Dim _XSD As String = ReturnXSDName(m_ReportFile)
            m_ReportDataTable.WriteXmlSchema(_XSD)

        Catch ex As Exception
            Return False
        End Try

        Return True

    End Function

    Public Property SubReportList As List(Of SubReport)
        Get
            Return m_SubReports
        End Get
        Set(ByVal value As List(Of SubReport))
            m_SubReports = value
        End Set
    End Property

    Private Function SetupReport() As Boolean

        Dim _DataSourceCount As Integer = 0

        If m_BusinessObjectList IsNot Nothing Then _DataSourceCount += 1
        If m_ReportSQL <> "" Then _DataSourceCount += 1
        If m_ReportDataTable IsNot Nothing Then _DataSourceCount += 1

        If _DataSourceCount = 0 Then
            CareMessage("There is no data to report upon.", MessageBoxIcon.Exclamation, "Report Error")
            Return False
        End If

        If _DataSourceCount > 1 Then
            CareMessage("Setup Report Failed - Too many data sources!.", MessageBoxIcon.Exclamation, "Report Error")
            Return False
        End If

        If m_ReportSQL <> "" Then
            PopulateReportData()
            If m_ReportDataTable Is Nothing Then
                CareMessage("There is no data to report upon.", MessageBoxIcon.Exclamation, "Report Error")
                Return False
            End If
        End If

        If IO.File.Exists(m_ReportFile) = False Then
            CareMessage("Specified Report File [" + m_ReportFile + "] not found.", MessageBoxIcon.Exclamation, "Report Error")
            Return False
        End If

        Return True

    End Function

    Public Sub RunReport(Optional ByVal GenerateSchema As Boolean = False, Optional ShowMDIMessages As Boolean = True, Optional UseHourGlass As Boolean = True)

        m_GenerateSchemaFile = GenerateSchema
        m_ShowMDIMessages = ShowMDIMessages
        m_UseHourGlass = UseHourGlass

        Session.SetProgressMessage("Setup Report...")
        If Not SetupReport() Then Return

        If m_UseHourGlass Then Session.CursorWaiting()

        Session.SetProgressMessage("Creating Report...")
        RunDXReport()

        Session.HideProgressBar()
        If m_UseHourGlass Then Session.CursorDefault()

    End Sub

    Private Function ReturnTableName(ReportFile As String) As String
        If DXReport Then
            Return Replace(ReportFile, ".repx", "") + "_table"
        Else
            Return Replace(ReportFile, ".rpt", "") + "_table"
        End If
    End Function

    Private Function ReturnXSDName(ReportFile As String) As String
        If DXReport Then
            Return Replace(ReportFile, ".repx", ".xsd")
        Else
            Return Replace(ReportFile, ".rpt", ".xsd")
        End If
    End Function

    Private Sub RunDXReport()

        Dim _R As New DevExpress.XtraReports.UI.XtraReport
        _R.LoadLayout(m_ReportFile)

        _R.DataSource = m_ReportDataTable

        Select Case m_ReportOutput

            Case OutputMode.Preview
                PrintPreview(_R)

            Case OutputMode.Print
                _R.PrintDialog()

        End Select

        _R.Dispose()
        _R = Nothing

    End Sub

    Private Shared Sub PrintPreview(ByRef ReportIn As XtraReport)
        Dim _Preview As New frmReportPreview(ReportIn)
        _Preview.ShowDialog()
    End Sub

    Public Shared Function SelectReport(ByVal AdHocOnly As Boolean) As Guid?

        Dim _Find As New GenericFind

        Dim _SQL As String = ""
        _SQL += "select r.parent as 'Parent', r.name as 'Report', q.title as 'Query', r.ad_hoc as 'AdHoc' from AppReports r"
        _SQL += " left join AppQueries q on q.id = r.query_id"

        Dim _Where As String = ""
        If AdHocOnly Then _Where = " and ad_hoc = 1"

        With _Find
            .Caption = "Select Report"
            .PopulateOnLoad = True
            .ConnectionString = Session.ConnectionString
            .GridSQL = _SQL
            .GridWhereClause = _Where
            .GridOrderBy = "order by r.name"
            .ReturnField = "r.ID"
            .Show()
        End With

        If _Find.ReturnValue = "" Then
            Return Nothing
        Else
            Return New Guid(_Find.ReturnValue)
        End If

    End Function

    Public Shared Function RepositoryReportPDF(ByVal ReportName As String, ByVal WhereClause As String, ByVal AttachmentFileName As String, Optional ReportFolder As String = "") As Boolean

        Dim _Return As Boolean = False
        Dim _R As Business.AppReport = Business.AppReport.RetreiveByName(ReportName)
        If _R IsNot Nothing Then

            Dim _ReportPath As String = ""

            'if we have the report folder specified, we need to replace the path on report definitions
            'this is because this could be called from a windows service using a different path to the main application
            'i.e. some sites will have \\server\runtimes\reports - which is not supported in a windows service
            'the service will pickup a replacement path from config files and override the normal session path for reports

            If ReportFolder = "" Then
                'use the report file and path from the Report Definitions (default)
                _ReportPath = _R._ReportFile
            Else
                Dim _FI As New IO.FileInfo(_R._ReportFile)
                Dim _ReportFileOnly As String = _FI.Name
                _ReportPath = Session.ReportFolder + _ReportFileOnly
            End If

            Dim _Rpt As New DevExpress.XtraReports.UI.XtraReport
            _Rpt.LoadLayout(_ReportPath)

            Dim _DT As DataTable
            If _R._QueryId.HasValue Then
                _DT = QueryHandler.GetQueryData(_R._QueryId.Value, False, WhereClause)
            Else
                _DT = ReportHandler.GetReportCentreData(_R._ID.Value, WhereClause)
            End If

            _Rpt.DataSource = _DT

            Dim _TempFile As String = Session.TempFolder + AttachmentFileName
            _Rpt.ExportToPdf(_TempFile)

            If IO.File.Exists(_TempFile) Then
                _Return = True
            End If

            _Rpt.Dispose()
            _Rpt = Nothing

        End If

        Return _Return

    End Function

    Public Shared Sub RepositoryReportPrintDirect(ByVal ReportName As String, ByVal WhereClause As String)

        Dim _R As Business.AppReport = Business.AppReport.RetreiveByName(ReportName)
        If _R IsNot Nothing Then

            Dim _Rpt As New DevExpress.XtraReports.UI.XtraReport
            _Rpt.LoadLayout(_R._ReportFile)

            Dim _DT As DataTable
            If _R._QueryId.HasValue Then
                _DT = QueryHandler.GetQueryData(_R._QueryId.Value, False, WhereClause)
            Else
                _DT = ReportHandler.GetReportCentreData(_R._ID.Value, WhereClause)
            End If

            _Rpt.DataSource = _DT

            _Rpt.Print()

            _Rpt.Dispose()
            _Rpt = Nothing

        Else

        End If

    End Sub

    Public Shared Sub RepositoryReportPreview(ByVal ReportName As String, ByVal WhereClause As String)

        Dim _R As Business.AppReport = Business.AppReport.RetreiveByName(ReportName)
        If _R IsNot Nothing Then

            Dim _Rpt As New DevExpress.XtraReports.UI.XtraReport
            _Rpt.LoadLayout(_R._ReportFile)

            Dim _DT As DataTable
            If _R._QueryId.HasValue Then
                _DT = QueryHandler.GetQueryData(_R._QueryId.Value, False, WhereClause)
            Else
                _DT = ReportHandler.GetReportCentreData(_R._ID.Value, WhereClause)
            End If

            _Rpt.DataSource = _DT

            PrintPreview(_Rpt)

            _Rpt.Dispose()
            _Rpt = Nothing

        Else

        End If

    End Sub

    Public Shared Sub RepositoryReportPreview()

        Dim _ID As Guid? = SelectReport(True)
        If _ID IsNot Nothing Then

            Dim _R As Business.AppReport = Business.AppReport.RetreiveByID(_ID.Value)

            Dim _Rpt As New DevExpress.XtraReports.UI.XtraReport
            _Rpt.LoadLayout(_R._ReportFile)

            Dim _DT As DataTable
            If _R._QueryId.HasValue Then
                _DT = QueryHandler.GetQueryData(_R._QueryId.Value, False)
            Else
                _DT = ReportHandler.GetReportCentreData(_R._ID.Value)
            End If

            _Rpt.DataSource = _DT

            PrintPreview(_Rpt)

        End If

    End Sub

    Public Shared Sub LoadReportCentre()

        Dim _frmReportCentre As New frmReportCentre
        _frmReportCentre.Show()

    End Sub

    Public Shared Sub RepositoryDesignReport(ByVal ReportName As String, ByVal WhereClause As String)

        If Not Session.CurrentUser.IsSuperAdministrator Then Exit Sub

        Dim _R As Business.AppReport = Business.AppReport.RetreiveByName(ReportName)
        If _R IsNot Nothing Then

            Dim _FilePath As String = _R._ReportFile

            If IO.File.Exists(_FilePath) Then

                If _R._QueryId.HasValue Then

                    Dim _Q As Business.AppQueries = Business.AppQueries.RetreiveByID(_R._QueryId.Value)
                    If _Q IsNot Nothing Then

                        Dim _Connection As String = _Q._Connection
                        If _Connection = "" Then _Connection = Session.ConnectionString

                        Dim _FileName As String = New IO.FileInfo(_R._ReportFile).Name

                        Dim _frmDesigner As New frmDXReportDesigner
                        _frmDesigner.LoadReport(_FileName, _Connection, _Q._Query)
                        _frmDesigner.Show()

                    End If

                End If

            Else
                CareMessage(_R._ReportFile + " was not found in " + Session.ReportFolder, MessageBoxIcon.Exclamation, "ReportHandler.DesignReport")
            End If

        End If

    End Sub

    Public Shared Sub DesignReport(ByVal ReportID As Guid)
        Dim _frmDesigner As New frmDXReportDesigner(ReportID)
        _frmDesigner.Show()
    End Sub

    Public Shared Sub DesignReport(ByVal ReportFile As String, ByVal ConnectionString As String, ByVal SQL As String)

        If Not Session.CurrentUser.IsSuperAdministrator Then Exit Sub
        Dim _FilePath As String = Session.ReportFolder + ReportFile

        If IO.File.Exists(_FilePath) Then

            Dim _frmDesigner As New frmDXReportDesigner
            _frmDesigner.LoadReport(ReportFile, ConnectionString, SQL)
            _frmDesigner.Show()

        Else
            CareMessage(ReportFile + " was not found in " + Session.ReportFolder, MessageBoxIcon.Exclamation, "ReportHandler.DesignReport")
        End If

    End Sub

    Public Shared Sub DesignReport(Of T)(ByVal ReportFile As String, ByVal ReportData As List(Of T))

        If Not Session.CurrentUser.IsAdministrator Then
            Session.SetProgressMessage("Only Administrators can access the Report Designer.", 3)
            Exit Sub
        End If

        Dim _FilePath As String = Session.ReportFolder + ReportFile

        If IO.File.Exists(_FilePath) Then

            Session.CursorWaiting()

            Dim _frmDesigner As New frmDXReportDesigner
            _frmDesigner.LoadReportWithCustomDataSource(Of T)(ReportFile, ReportData)
            _frmDesigner.Show()

        Else
            CareMessage(ReportFile + " was not found in " + Session.ReportFolder, MessageBoxIcon.Exclamation, "ReportHandler.DesignReport")
        End If

    End Sub

    Public Shared Sub RunReport(Of T)(ByVal ReportFile As String, ByVal ReportData As List(Of T))

        Dim _FilePath As String = Session.ReportFolder + ReportFile

        If IO.File.Exists(_FilePath) Then

            Session.CursorWaiting()

            Dim _Rpt As New DevExpress.XtraReports.UI.XtraReport
            _Rpt.LoadLayout(_FilePath)
            _Rpt.DataSource = ReportData

            PrintPreview(_Rpt)

        Else
            CareMessage(ReportFile + " was not found in " + Session.ReportFolder, MessageBoxIcon.Exclamation, "ReportHandler.RunReport")
        End If

    End Sub

    Public Shared Function PDFReport(Of T)(ByVal ReportFile As String, ByVal PDFFile As String, ByVal ReportData As List(Of T)) As Integer

        Dim _Return As Integer = 0

        If Session.ReportFolder = "" Then
            _Return = -1
        Else

            Dim _ReportFilePath As String = Session.ReportFolder + ReportFile
            If IO.File.Exists(_ReportFilePath) Then

                Try

                    Dim _Rpt As New DevExpress.XtraReports.UI.XtraReport
                    _Rpt.LoadLayout(_ReportFilePath)
                    _Rpt.DataSource = ReportData

                    _Rpt.ExportToPdf(PDFFile)

                    _Rpt.Dispose()
                    _Rpt = Nothing

                Catch ex As Exception
                    _Return = -3
                End Try

            Else
                CareMessage(ReportFile + " was not found in " + Session.ReportFolder, MessageBoxIcon.Exclamation, "ReportHandler.RunReport")
                _Return = -2
            End If

        End If

        Return _Return

    End Function

    Public Shared Function PDFReport(Of T)(ByVal ReportFile As String, ByVal ReportData As List(Of T)) As String
        Dim _PDFFile As String = Session.TempFolder + Guid.NewGuid.ToString + ".pdf"
        If PDFReport(Of T)(ReportFile, _PDFFile, ReportData) = 0 Then
            Return _PDFFile
        Else
            Return ""
        End If
    End Function

    Public Shared Sub RunReport(ByVal ReportFile As String, ByVal ConnectionString As String, ByVal SQL As String)

        Dim _FilePath As String = Session.ReportFolder + ReportFile

        If IO.File.Exists(_FilePath) Then

            Dim _DT As DataTable = DAL.GetDataTablefromSQL(ConnectionString, SQL)

            If _DT IsNot Nothing Then

                Dim _Rpt As New DevExpress.XtraReports.UI.XtraReport
                _Rpt.LoadLayout(_FilePath)
                _Rpt.DataSource = _DT

                PrintPreview(_Rpt)

            End If

        Else
            CareMessage(ReportFile + " was not found in " + Session.ReportFolder, MessageBoxIcon.Exclamation, "ReportHandler.RunReport")
        End If

    End Sub

    Public Shared Function GetReportCentreData(ByVal ReportID As Guid, Optional WhereClause As String = "") As DataTable

        Dim _DT As DataTable = Nothing
        Dim _Cancel As Boolean = False

        Dim _ReportRecord As Business.AppReport = Business.AppReport.RetreiveByID(ReportID)
        Dim _Clauses As List(Of Business.AppReportClause) = Business.AppReportClause.RetreiveByReportID(ReportID)

        Dim _SQL As String = ""

        _SQL += _ReportRecord._SqlQuery

        If WhereClause = "" Then
            If _Clauses.Count > 0 Then

                Dim _frm As New frmReportPrompt(_Clauses)
                _frm.Text = "Filter data for " + _ReportRecord._Name
                _frm.ShowDialog()

                If _frm.DialogResult = DialogResult.OK Then
                    _SQL += _frm.GeneratedClauses
                Else
                    _Cancel = True
                End If

                _frm.Dispose()

            End If
        End If

        If Not _Cancel Then

            If WhereClause <> "" Then _SQL += " " + WhereClause
            _SQL += " " + _ReportRecord._SqlOrder

            Session.CursorWaiting()

            Try
                _DT = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)

            Catch ex As Exception
                _DT = Nothing
            End Try

        End If

        Return _DT

    End Function

End Class

Public Class SubReport

    Private m_ReportFile As String
    Private m_ReportData As DataTable

    Public Property ReportFile() As String
        Get
            Return m_ReportFile
        End Get
        Set(ByVal value As String)
            m_ReportFile = value
        End Set
    End Property

    Public Property ReportData() As DataTable
        Get
            Return m_ReportData
        End Get
        Set(ByVal value As DataTable)
            m_ReportData = value
        End Set
    End Property

End Class