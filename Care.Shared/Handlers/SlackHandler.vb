﻿Imports System.Collections.Specialized
Imports System.Net
Imports System.Text
Imports Care.Global
Imports Newtonsoft.Json

Public Class SlackHandler

    Public Shared Function MessageChannel(MessageText As String) As Integer

        Dim _return As Integer = -1
        Dim _uri As New Uri(Session.SlackURL)
        Dim _encoding As Encoding = New UTF8Encoding()

        Dim _p As New Payload
        '_p.Channel = Channel
        '_p.UserName = "aj"
        _p.Text = MessageText

        Dim _pj As String = JsonConvert.SerializeObject(_p)

        Using _wc As New WebClient

            Dim _data As New NameValueCollection()
            _data("payload") = _pj

            Dim _r = _wc.UploadValues(_uri, "POST", _data)
            Dim _rs As String = _encoding.GetString(_r)

            If _rs = "ok" Then _return = 0

        End Using

        Return _return

    End Function

    Private Class Payload

        <JsonProperty("username")>
        Public Property UserName As String

        <JsonProperty("channel")>
        Public Property Channel As String

        <JsonProperty("text")>
        Public Property Text As String

    End Class

End Class
