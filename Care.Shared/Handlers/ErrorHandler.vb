﻿Imports Care.Global
Imports System.Windows.Forms

Public Class ErrorHandler

    Public Shared Sub Activate()

        Try
            If Debugger.IsAttached Then Exit Sub
            AddHandler Application.ThreadException, AddressOf ThreadException
            AddHandler System.AppDomain.CurrentDomain.UnhandledException, AddressOf UnhandledExeception

        Catch ex As Exception

        End Try

    End Sub

    ''' <summary>
    ''' Allows us to Log an Exception we have handled to the DB. We assume its UnHandled...
    ''' </summary>
    Public Shared Sub LogExceptionToDatabase(ByRef Exception As System.Exception, Optional ByVal UnHandled As Boolean = True)
        LogToDB(Exception, UnHandled)
    End Sub

    ''' <summary>
    ''' Allows us to Log an Exception we have handled to the DB. We assume its UnHandled...
    ''' </summary>
    Public Shared Sub LogCustomErrorToDatabase(ByVal ErrorText As String, Optional ByVal UnHandled As Boolean = True)
        LogToDB(ErrorText, UnHandled)
    End Sub

    Public Shared Sub DeActivate()

    End Sub

    Private Shared Sub UnhandledExeception(sender As Object, e As UnhandledExceptionEventArgs)
        Try
            ManageException(CType(e.ExceptionObject, Exception))
        Catch ex As Exception

        End Try
    End Sub

    Private Shared Sub ThreadException(sender As Object, e As Threading.ThreadExceptionEventArgs)
        Try
            ManageException(e.Exception)
        Catch ex As Exception

        End Try
    End Sub

    Private Shared Sub ManageException(ByRef Exception As System.Exception)

        LogToDB(Exception, True)

        If Session.InteractiveSession Then

            Dim _frm As New frmError()
            _frm.ExceptionObject = Exception
            _frm.ShowDialog()

            Application.Exit()

        End If

    End Sub

    Private Shared Sub LogToDB(ByVal ErrorText As String, ByVal UnHandled As Boolean)

        Try

            Dim _e As New Business.AppError
            _e._Stamp = Now
            _e._Hostname = My.Computer.Name
            _e._UserId = Session.CurrentUser.ID
            _e._UserName = Session.CurrentUser.FullName
            _e._WindowsUser = Environment.UserName
            _e._Source = ErrorText
            _e._SourceVersion = My.Application.Info.Version.ToString
            _e._Exception = ""
            _e._Stack = ""
            _e._Unhandled = UnHandled
            _e.Store(Session.ConnectionString)

        Catch ex As Exception

        End Try

    End Sub

    Private Shared Sub LogToDB(ByRef Exception As System.Exception, ByVal UnHandled As Boolean)

        Try

            Dim _e As New Business.AppError
            _e._Stamp = Now
            _e._Hostname = My.Computer.Name
            _e._UserId = Session.CurrentUser.ID
            _e._UserName = Session.CurrentUser.FullName
            _e._WindowsUser = Environment.UserName
            _e._Source = Exception.Source
            _e._SourceVersion = My.Application.Info.Version.ToString


            If Exception.InnerException IsNot Nothing Then
                _e._Exception = Exception.Message + vbCrLf + "InnerException: " + Exception.InnerException.Message
                _e._Stack = Exception.StackTrace + vbCrLf + vbCrLf + "InnerStackTrace:" + vbCrLf + Exception.InnerException.StackTrace
            Else
                _e._Exception = Exception.Message
                _e._Stack = Exception.StackTrace
            End If

            _e._Unhandled = UnHandled
            _e.Store(Session.ConnectionString)

        Catch ex As Exception

        End Try

    End Sub

    Public Shared Function ReturnBaseHierarchy(ByVal Base As System.Type) As String
        Dim _BaseChain As String = GetBase(Base, "")
        Return _BaseChain
    End Function

    Private Shared Function GetBase(ByVal Base As System.Type, ByRef BaseChain As String) As String

        If Not Base.BaseType Is Nothing Then
            If Base.FullName <> "Form" Then
                BaseChain += ">> " & Base.FullName & vbCrLf
                GetBase(Base.BaseType, BaseChain)
            End If
        End If

        Return BaseChain

    End Function

End Class
