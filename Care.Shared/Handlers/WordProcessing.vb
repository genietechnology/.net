﻿Imports Care.Global
Imports Care.Data

Public Class WordProcessing

    Public Enum EnumBehaviour
        Normal
        HoldMerge
        SaveToINVU
    End Enum

    Public Shared Sub CreateDocument()

        Session.SetProgressMessage("Loading Word Processor. Please wait...")
        Session.CursorApplicationStarting()

        Dim _WP As New frmDocument
        _WP.NewDocument()

        _WP.Show()
        _WP.BringToFront()

    End Sub

    Public Shared Function OpenFile(ByVal FileName As String) As Boolean

        Session.SetProgressMessage("Loading Word Processor. Please wait...")
        Session.CursorApplicationStarting()

        Dim _WP As New frmDocument
        _WP.OpenDocument(FileName)

        _WP.Show()
        _WP.BringToFront()

        Return True

    End Function

    Public Shared Function OpenFile(ByVal LetterCode As String, ByVal FilteredRows As DataView) As Boolean

        Dim _Return As Boolean = False

        Session.SetProgressMessage("Loading Word Processor. Please wait...")
        Session.CursorApplicationStarting()

        Dim _WP As New frmDocument
        _Return = _WP.OpenDocument(LetterCode, FilteredRows)

        If _Return Then
            _WP.Show()
            _WP.BringToFront()
        Else

            Session.SetProgressMessage("Unable to load letter [" + LetterCode + "]", 5)

            _WP.Dispose()
            _WP = Nothing

        End If

        Return _Return

    End Function

    Public Shared Function OpenFile(ByVal LetterCode As String, ByVal AdditionalSelect As String, ByVal AdditionalWhereClause As String, ByVal Behaviour As EnumBehaviour, Optional ByVal RecordLimit As Integer = 0, Optional ByRef DocumentForm As frmDocument = Nothing) As Boolean

        Dim _Return As Boolean = False

        Session.SetProgressMessage("Loading Word Processor. Please wait...")
        Session.CursorApplicationStarting()

        Dim _WP As New frmDocument
        _Return = _WP.OpenDocument(LetterCode, AdditionalSelect, AdditionalWhereClause, Behaviour, RecordLimit)

        If _Return Then
            _WP.Show()
            _WP.BringToFront()
        Else

            Session.SetProgressMessage("Unable to load letter [" + LetterCode + "]", 5)

            _WP.Dispose()
            _WP = Nothing

        End If

        DocumentForm = _WP
        Return _Return

    End Function

    Public Shared Sub CreateINVUCSV(ByRef Letter As Business.AppLetter, ByVal DocumentPath As String, ByRef DR As DataRow)

        'GR Lease Reference, GR Property Address, GR Standard Document Type, GR Date, GR Name of Correspondent, full UNC path to document

        If Letter._Invu Then

            Dim _CSVFile As String = Letter._InvuCsvPath
            Dim _DocFile As String = Letter._InvuDocPath + "\" + FileHandler.ReturnFileName(DocumentPath)

            'copy the output document to the invu doc folder
            FileCopy(DocumentPath, _DocFile)

            If Letter._OutputType.Contains("(.doc)") Then _CSVFile += "\" + Replace(FileHandler.ReturnFileName(DocumentPath), ".doc", ".csv")
            If Letter._OutputType.Contains("(.docx)") Then _CSVFile += "\" + Replace(FileHandler.ReturnFileName(DocumentPath), ".docx", ".csv")

            Dim _Writer As New System.IO.StreamWriter(_CSVFile, False)

            Dim _Delimeter As String = "|"
            Dim _Data As String = ""
            Dim _Fields As List(Of String) = Letter._InvuFields.Split(CChar(_Delimeter)).ToList

            For Each _Field As String In _Fields

                Dim _FieldContent As String = ""

                If _Field <> "" Then

                    If _Field.First = "<" AndAlso _Field.Last = ">" Then
                        If _Field = "<PATH>" Then
                            _FieldContent = _DocFile
                        Else
                            _FieldContent = DR.Item(StripBrackets(_Field)).ToString
                        End If
                    Else
                        If _Field.First = Chr(34) AndAlso _Field.Last = Chr(34) Then
                            _FieldContent = Replace(_Field, Chr(34), "")
                        End If
                    End If

                    If _Data = "" Then
                        _Data += _FieldContent
                    Else
                        _Data += _Delimeter + _FieldContent
                    End If

                End If

            Next

            _Writer.WriteLine(_Data)

            _Writer.Flush()
            _Writer.Close()
            _Writer.Dispose()
            _Writer = Nothing

        End If

    End Sub

    Private Shared Function StripBrackets(ByVal FieldIn As String) As String
        Dim _Return As String = FieldIn
        _Return = Replace(_Return, "<", "")
        _Return = Replace(_Return, ">", "")
        Return _Return
    End Function

    Public Shared Function SelectLetter(ByVal QueryID As Guid) As String
        Return SelectLetter(" and query_id = '" + QueryID.ToString + "'")
    End Function

    Public Shared Function SelectLetter(ByVal AdHocOnly As Boolean) As String
        Return SelectLetter(" and adhoc = 1")
    End Function

    Private Shared Function SelectLetter(ByVal WhereClause As String) As String

        Dim _Return As String = ""

        Dim _SQL As String = "select code as 'Letter Code', name as 'Letter Description'" & _
                             " from AppLetters"

        Dim _Find As New Care.Shared.GenericFind
        With _Find

            .ConnectionString = Session.ConnectionString
            .Caption = "Select Letter"
            .ReturnField = "code"
            .GridSQL = _SQL

            If WhereClause <> "" Then .GridWhereClause = WhereClause

            .GridOrderBy = "order by code"
            .PopulateOnLoad = True
            .HideFirstColumn = True
            .Show()

        End With

        _Return = _Find.ReturnValue
        _Find = Nothing

        Return _Return

    End Function





End Class
