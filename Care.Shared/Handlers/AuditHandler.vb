﻿Imports Care.Global

Public Class AuditHandler

    Private Shared m_ComputerName As String
    Private Shared m_LoginName As String

#Region "Properties"

    Public Shared ReadOnly Property ComputerName As String
        Get
            Try
                If m_ComputerName = "" Then m_ComputerName = My.Computer.Name
            Catch ex As Exception

            End Try
            Return m_ComputerName
        End Get
    End Property

    Public Shared ReadOnly Property LoginName As String
        Get
            Try
                If m_LoginName = "" Then m_LoginName = Environment.UserDomainName + "\" + Environment.UserName
            Catch ex As Exception

            End Try
            Return m_LoginName
        End Get
    End Property

#End Region

#Region "Enums"

    Public Enum EnumAuditScope
        Login
        Form
        Record
        Custom
    End Enum

    Public Enum EnumAuditStatus As Byte
        Success = 1
        Failed = 0
        Unknown = 99
    End Enum

#End Region

    Public Shared Sub LogAudit(ByVal Scope As EnumAuditScope, ByVal ObjectName As String, ByVal ObjectID As Guid?, ByVal Description As String, ByVal Status As EnumAuditStatus)

        Try

            Dim _a As New Business.AppAuditObject
            With _a
                ._Scope = Scope.ToString
                ._ObjectName = ObjectName
                ._ObjectId = ObjectID
                ._ObjectDesc = Description
                ._StatusCode = Status
                ._Status = Status.ToString
                ._UserId = Session.CurrentUser.ID
                ._UserName = Session.CurrentUser.FullName
                ._UserType = Session.CurrentUser.Type
                ._ComputerName = ComputerName
                ._LoginName = LoginName
                ._Stamp = Now
                .Store()
            End With

            _a = Nothing

        Catch ex As Exception

        End Try

    End Sub

    Public Shared Sub ViewFormAudit(ByVal FormName As String, ByVal FormCaption As String, ByVal RecordID As Guid?)
        Dim _frm As New frmAuditForm(FormName, FormCaption, RecordID)
        _frm.ShowDialog()
        _frm.Dispose()
        _frm = Nothing
    End Sub



End Class
