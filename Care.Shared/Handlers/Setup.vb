﻿Imports Care.Data
Imports Care.Global

Public Class Setup

    Public Shared Function Execute() As Boolean

        'create admin account if it doesnt already exist
        CreateAdminUser()

        'create any mandatory parameters if they dont exist
        CreateParameters()

        'set parameters
        SetParameters()

        Return True

    End Function

    Public Shared Function LoginViaWindowsCredentials() As Boolean

        Dim _DomainAndUserName As String = Environment.UserDomainName + "\" + Environment.UserName
        Dim _SQL As String = "select * from AppUsers where sso = 1 and sso_username = '" + _DomainAndUserName + "'"

        Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DR IsNot Nothing Then
            Session.CurrentUser = New User()
            Session.CurrentUser.ID = New Guid(_DR.Item("ID").ToString)
            Session.CurrentUser.UserCode = _DR.Item("username").ToString
            Session.CurrentUser.FullName = _DR.Item("fullname").ToString
            Session.CurrentUser.Type = _DR.Item("type").ToString
            Session.CurrentUser.GroupID = _DR.Item("group_id").ToString
            Session.CurrentUser.Email = _DR.Item("email").ToString
            Return True
        End If

        Return False

    End Function

    Public Shared Function LoginWithUserCode(ByVal UserCode As String) As Boolean

        Dim _u As Business.User = Business.User.RetrieveByName(UserCode)
        If _u IsNot Nothing Then
            Session.CurrentUser = New User
            Session.CurrentUser.ID = _u._ID.Value
            Session.CurrentUser.UserCode = _u._Username
            Session.CurrentUser.FullName = _u._Fullname
            Session.CurrentUser.Type = _u._Type
            Session.CurrentUser.GroupID = _u._GroupId.ToString
            Session.CurrentUser.Email = _u._Email
            Return True
        End If

        Return False

    End Function

    Public Shared Function CheckAuthDatabase() As Boolean

        Dim _DomainAndUserName As String = Environment.UserDomainName + "\" + Environment.UserName
        Dim _DR As DataRow = DAL.GetRowfromSPwithParam(Session.ConnectionString, "getUserbyUsername", "@Username", SqlDbType.VarChar, _DomainAndUserName)
        If _DR IsNot Nothing Then

            Dim _ConnectionDetails As New ConnectionBuilder
            With _ConnectionDetails
                .Server = _DR.Item("sql_server").ToString
                .DB = _DR.Item("sql_database").ToString
                .User = ""
                .Password = ""
                .IntegratedSecurity = True
                .TestConnection()
            End With

            If _ConnectionDetails.ConnectionOK Then
                Session.ConnectionString = _ConnectionDetails.ConnectionString
                Return True
            End If

        End If

        Return False

    End Function

    Public Shared Function InitialiseParameters() As Boolean
        CreateParameters()
        EncryptPasswordParameters()
        SetParameters()
        Return True
    End Function

    Public Shared Function CheckAdminUser() As Boolean
        CreateAdminUser()
        Return True
    End Function

    Private Shared Sub CreateAdminUser()
        Business.User.CreateAdmin()
    End Sub

    Private Shared Sub CreateParameters()

        CheckParameter("AUDIT", ParameterHandler.EnumParameterType.BooleanType, "Audit", "Record Auditting", "True")
        CheckParameter("AUDITEXCLUPSERTS", ParameterHandler.EnumParameterType.StringType, "Audit", "Excluded Upserts", "")

        CheckParameter("ICONPATH", ParameterHandler.EnumParameterType.FilePathType, "System Paths", "Icons Folder", "C:\App\Icons")
        CheckParameter("REPORTPATH", ParameterHandler.EnumParameterType.FilePathType, "System Paths", "Reports Folder", "C:\App\Reports")
        CheckParameter("LETTERPATH", ParameterHandler.EnumParameterType.FilePathType, "System Paths", "Letter Folder", "C:\App\Letters")
        CheckParameter("TEMPPATH", ParameterHandler.EnumParameterType.FilePathType, "System Paths", "Temporary Folder", "C:\App\Temp")

        CheckParameter("SMTP", ParameterHandler.EnumParameterType.BooleanType, "Email", "SMTP Email Function", "False")
        CheckParameter("SMTPSERVER", ParameterHandler.EnumParameterType.StringType, "Email", "SMTP Server", "")
        CheckParameter("SMTPPORT", ParameterHandler.EnumParameterType.IntegerType, "Email", "SMTP Port", "25")
        CheckParameter("SMTPSSL", ParameterHandler.EnumParameterType.BooleanType, "Email", "SMTP SSL Enabled", "False")
        CheckParameter("SMTPAUTH", ParameterHandler.EnumParameterType.BooleanType, "Email", "SMTP Authentication Enabled", "False")
        CheckParameter("SMTPUSER", ParameterHandler.EnumParameterType.StringType, "Email", "SMTP Username", "")
        CheckParameter("SMTPPASSWORD", ParameterHandler.EnumParameterType.PasswordType, "Email", "SMTP Password", "")
        CheckParameter("SMTPSENDER", ParameterHandler.EnumParameterType.StringType, "Email", "SMTP Sender Email Address", "")
        CheckParameter("SMTPSENDERNAME", ParameterHandler.EnumParameterType.StringType, "Email", "SMTP Sender Name", "")
        CheckParameter("SMTPBCC", ParameterHandler.EnumParameterType.StringType, "Email", "SMTP BCC Email", "")
        CheckParameter("SMTPTEST", ParameterHandler.EnumParameterType.BooleanType, "Email", "SMTP Test Mode", "False")

        CheckParameter("CTI", ParameterHandler.EnumParameterType.BooleanType, "CTI", "CTI Enabled", "False")
        CheckParameter("CTISERVER", ParameterHandler.EnumParameterType.StringType, "CTI", "CTI Server", "DEMOSERVER")
        CheckParameter("CTIEXT", ParameterHandler.EnumParameterType.StringType, "CTI", "CTI Extension", "301")

        CheckParameter("SMS", ParameterHandler.EnumParameterType.BooleanType, "SMS", "SMS Enabled", "False")
        CheckParameter("SMSSENDER", ParameterHandler.EnumParameterType.StringType, "SMS", "SMS Sender Email", "sms@caresoftware.co.uk")
        CheckParameter("SMSDOMAIN", ParameterHandler.EnumParameterType.StringType, "SMS", "SMS Domain", "txtlocal.co.uk")

        CheckParameter("BACKUPCHECK", ParameterHandler.EnumParameterType.BooleanType, "Backup", "Backup Reminders", "True")
        CheckParameter("BACKUPPATH", ParameterHandler.EnumParameterType.FilePathType, "Backup", "Backup Path", "C:\App\Backup")
        CheckParameter("BACKUPLAST", ParameterHandler.EnumParameterType.DateType, "Backup", "Last Backup", "")
        CheckParameter("BACKUPPASSWORD", ParameterHandler.EnumParameterType.PasswordType, "Backup", "Backup Password", "")
        CheckParameter("BACKUPKEEP", ParameterHandler.EnumParameterType.IntegerType, "Backup", "Backups Days to Keep", "31")

        CheckParameter("FINSYSTEM", ParameterHandler.EnumParameterType.StringType, "Financials", "Financial System", "None")
        CheckParameter("FINAPIID", ParameterHandler.EnumParameterType.StringType, "Financials", "API ID", "")
        CheckParameter("FINAPIKEY", ParameterHandler.EnumParameterType.StringType, "Financials", "API Key", "")
        CheckParameter("FINAPISECRET", ParameterHandler.EnumParameterType.StringType, "Financials", "API Secret", "")
        CheckParameter("FINPFXPATH", ParameterHandler.EnumParameterType.StringType, "Financials", "PFX Path", "")
        CheckParameter("FINPFXPWD", ParameterHandler.EnumParameterType.PasswordType, "Financials", "PFX Password", "")

        CheckParameter("FINSAGEPATH", ParameterHandler.EnumParameterType.StringType, "Financials", "Sage Company Path", "")
        CheckParameter("FINSAGEUSER", ParameterHandler.EnumParameterType.StringType, "Financials", "Sage Username", "")
        CheckParameter("FINSAGEPASSWORD", ParameterHandler.EnumParameterType.PasswordType, "Financials", "Sage Password", "")
        CheckParameter("FINSAGEVERSION", ParameterHandler.EnumParameterType.StringType, "Financials", "Sage Version", "")

        CheckParameter("SPELL", ParameterHandler.EnumParameterType.BooleanType, "Spell Checker", "Spell Check Enabled", "False")
        CheckParameter("SPELLDICT", ParameterHandler.EnumParameterType.StringType, "Spell Checker", "Dictionary Path", "")
        CheckParameter("SPELLGRAM", ParameterHandler.EnumParameterType.StringType, "Spell Checker", "Grammar Path", "")

        CheckParameter("MANDRILL", ParameterHandler.EnumParameterType.BooleanType, "Email - Mandrill", "Mandrill Enabled", "False")
        CheckParameter("MANDRILLAPI", ParameterHandler.EnumParameterType.StringType, "Email - Mandrill", "Mandrill API Key", "")

        CheckParameter("CUSTOMERID", ParameterHandler.EnumParameterType.StringType, "System", "CustomerID", "")
        CheckParameter("LASTVERSION", ParameterHandler.EnumParameterType.StringType, "System", "Last Version", "")
        CheckParameter("DISABLEWAITFORM", ParameterHandler.EnumParameterType.BooleanType, "System", "Disable Wait Form", "False")
        CheckParameter("MANAGERPASSWORD", ParameterHandler.EnumParameterType.PasswordType, "System", "Manager Password", "password")

        CheckParameter("SLACK", ParameterHandler.EnumParameterType.BooleanType, "Slack", "Slack Enabled", "False")
        CheckParameter("SLACKURL", ParameterHandler.EnumParameterType.StringType, "Slack", "Slack URL", "")
        CheckParameter("SLACKUSER", ParameterHandler.EnumParameterType.StringType, "Slack", "Slack User", "")

    End Sub

    Private Shared Sub CheckParameter(ByVal ParameterName As String, ByVal ParameterType As ParameterHandler.EnumParameterType, ByVal ParameterParent As String, ByVal ParameterDescription As String, ByVal ParameterValue As String)
        If Not ParameterHandler.CheckExists(ParameterName) Then
            ParameterHandler.Create(ParameterName, ParameterType, ParameterParent, ParameterDescription, ParameterValue)
        End If
    End Sub

    Private Shared Sub EncryptPasswordParameters()
        ParameterHandler.ConvertToPasswordParameter("FINPFXPWD")
        ParameterHandler.ConvertToPasswordParameter("FINSAGEPASSWORD")
        ParameterHandler.ConvertToPasswordParameter("MANAGERPASSWORD")
        ParameterHandler.ConvertToPasswordParameter("PAXTONPASSWORD")
        ParameterHandler.ConvertToPasswordParameter("WUFOOPASSWORD")
        ParameterHandler.ConvertToPasswordParameter("SMTPPASSWORD")
    End Sub

    Public Shared Sub SetParameters()

        Session.ConnectionManager.PassParameters(ParameterHandler.ReturnBoolean("AUDIT"), ParameterHandler.ReturnString("AUDITEXCLUPSERTS"))

        Session.IconFolder = ParameterHandler.ReturnPath("ICONPATH")
        Session.ReportFolder = ParameterHandler.ReturnPath("REPORTPATH")
        Session.LetterFolder = ParameterHandler.ReturnPath("LETTERPATH")
        Session.TempFolder = ParameterHandler.ReturnPath("TEMPPATH")

        Session.SMTPEnabled = ParameterHandler.ReturnBoolean("SMTP")
        Session.SMTPServer = ParameterHandler.ReturnString("SMTPSERVER")
        Session.SMTPPort = ParameterHandler.ReturnInteger("SMTPPORT", False)
        Session.SMTPSSL = ParameterHandler.ReturnBoolean("SMTPSSL")
        Session.SMTPAuth = ParameterHandler.ReturnBoolean("SMTPAUTH")
        Session.SMTPUser = ParameterHandler.ReturnString("SMTPUSER")
        Session.SMTPPassword = ParameterHandler.ReturnString("SMTPPASSWORD")
        Session.SMTPSenderEmail = ParameterHandler.ReturnString("SMTPSENDER")
        Session.SMTPSenderName = ParameterHandler.ReturnString("SMTPSENDERNAME")
        Session.SMTPBCC = ParameterHandler.ReturnString("SMTPBCC")
        Session.SMTPTestMode = ParameterHandler.ReturnBoolean("SMTPTEST")

        Session.CTIEnabled = ParameterHandler.ReturnBoolean("CTI")

        Session.SMSEnabled = ParameterHandler.ReturnBoolean("SMS")
        Session.SMSSender = ParameterHandler.ReturnString("SMSSENDER")
        Session.SMSDomain = ParameterHandler.ReturnString("SMSDOMAIN")

        Session.FinancialSystem = ParameterHandler.ReturnString("FINSYSTEM")
        Session.FinancialsAPIID = ParameterHandler.ReturnString("FINAPIID")
        Session.FinancialsAPIKey = ParameterHandler.ReturnString("FINAPIKEY")
        Session.FinancialsAPISecret = ParameterHandler.ReturnString("FINAPISECRET")
        Session.FinancialsPFXPath = ParameterHandler.ReturnString("FINPFXPATH")
        Session.FinancialsPFXPassword = ParameterHandler.ReturnString("FINPFXPWD")
        Session.FinancialsSagePath = ParameterHandler.ReturnString("FINSAGEPATH")
        Session.FinancialsSageUser = ParameterHandler.ReturnString("FINSAGEUSER")
        Session.FinancialsSagePassword = ParameterHandler.ReturnString("FINSAGEPASSWORD")
        Session.FinancialsSageVersion = ParameterHandler.ReturnString("FINSAGEVERSION")

        Session.CustomerID = ParameterHandler.ReturnString("CUSTOMERID")

        Session.SlackEnabled = ParameterHandler.ReturnBoolean("SLACK")
        Session.SlackURL = ParameterHandler.ReturnString("SLACKURL")
        Session.SlackUser = ParameterHandler.ReturnString("SLACKUSER")

    End Sub

    Public Sub CursorDefault()
        Cursor.Current = Cursors.Default
        Application.DoEvents()
    End Sub

    Public Sub CursorWaiting()
        Cursor.Current = Cursors.WaitCursor
        Application.DoEvents()
    End Sub

    Public Sub CursorApplicationStarting()
        Cursor.Current = Cursors.AppStarting
        Application.DoEvents()
    End Sub

End Class
