﻿
Imports Care.Global
Imports System.IO
Imports Care.Data

Public Class DocumentHandler

    Public Enum DocumentType
        User
        Application
    End Enum

    Public Shared Function ReturnThumbnail(ByVal SourceImageData As Byte(), ThumbnailSize As Integer) As Byte()

        If SourceImageData Is Nothing Then Return Nothing

        Dim _Return As Byte() = Nothing
        Dim _SourceImage As System.Drawing.Image = GetImagefromByteArray(SourceImageData)
        If _SourceImage IsNot Nothing Then

            If _SourceImage.Size.Height > ThumbnailSize OrElse _SourceImage.Size.Width > ThumbnailSize Then

                Dim _NewHeight As Double = ThumbnailSize
                Dim _NewWidth As Double = _NewHeight / _SourceImage.Size.Height * _SourceImage.Size.Width

                If _NewWidth > ThumbnailSize Then
                    _NewWidth = ThumbnailSize
                    _NewHeight = _NewWidth / _SourceImage.Size.Width * _SourceImage.Size.Height
                End If

                Dim _ThumbImage As System.Drawing.Image = _SourceImage.GetThumbnailImage(CInt(_NewWidth), CInt(_NewHeight), Nothing, New IntPtr)
                If _ThumbImage IsNot Nothing Then
                    _Return = ImageToByteArray(_ThumbImage)
                    _ThumbImage.Dispose()
                    _ThumbImage = Nothing
                End If

            End If

            _SourceImage.Dispose()
            _SourceImage = Nothing

        End If

        Return _Return

    End Function

    Public Shared Sub OpenDocumentManagement(ByVal ID As Guid, ByVal FormCaption As String)

        Dim _frm As New frmDocumentManagement(ID)
        _frm.Text = FormCaption & " - Document Management"
        _frm.Show()

    End Sub

    Public Shared Function GetByteArraybyID(ByVal ID As Guid) As Byte()

        Dim _doc As Business.Document = Business.Document.RetreiveByID(ID)
        Dim _data As Byte() = _doc._Data
        _doc = Nothing

        Return _data

    End Function

    Public Shared Function GetImageByID(ByVal ID As Guid?) As Drawing.Image
        If ID.HasValue Then
            Return GetImagefromByteArray(GetByteArraybyID(New Guid(ID.ToString)))
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetImagefromByteArray(ByVal Data As Byte()) As Drawing.Image

        If Data Is Nothing Then Return Nothing

        Dim _ms As MemoryStream = New MemoryStream(Data, 0, Data.Length)
        Dim _im As Drawing.Image

        Try
            _im = Drawing.Image.FromStream(_ms)
        Catch ex As Exception
            Return Nothing
        End Try

        _ms.Close()
        _ms.Dispose()

        Return _im

    End Function

    Public Shared Sub ViewFile(ByVal DocID As String)

        If DocID = "" Then Exit Sub
        Cursor.Current = Cursors.WaitCursor

        Dim _Doc As Business.Document = Business.Document.RetreiveByID(New Guid(DocID))
        If _Doc IsNot Nothing Then

            Dim _FileName As String = FileHandler.ReturnTemporaryFile(Session.TempFolder, _Doc._FileExt)
            If ByteArraytoFile(_Doc._Data, _FileName) Then
                FileHandler.OpenFile(_FileName)
            End If

            _Doc = Nothing

        End If

        Cursor.Current = Cursors.Default

    End Sub

    Public Shared Function FileToByteArray(ByVal FilePath As String) As Byte()

        If File.Exists(FilePath) Then

            Try

                Dim _fs As FileStream = Nothing
                _fs = New FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read)

                Dim _fi As FileInfo = New FileInfo(FilePath)
                If Not _fi Is Nothing Then

                    Dim _size As Long = _fi.Length

                    Dim _int32 As Integer = CType(_size, Int32)
                    Dim _data As Byte() = New Byte(_int32 - 1) {}

                    _fs.Read(_data, 0, _int32)

                    _fs.Close()
                    _fs = Nothing

                    Return _data

                Else
                    Return Nothing
                End If

            Catch ex As Exception
                Return Nothing
            End Try

        Else
            Return Nothing
        End If

    End Function

    Private Shared Function ByteArraytoFile(ByVal Data As Byte(), ByVal FilePath As String) As Boolean

        If Data IsNot Nothing Then

            Try

                Dim _fs As FileStream = New IO.FileStream(FilePath, FileMode.OpenOrCreate, FileAccess.Write)
                Dim _bw As BinaryWriter = New IO.BinaryWriter(_fs)

                _bw.Write(Data)
                _bw.Flush()
                _bw.Close()
                _bw = Nothing

                _fs.Close()
                _fs = Nothing

                Return True

            Catch ex As Exception
                Return False
            End Try

        Else
            Return False
        End If

    End Function

    Public Shared Function ImageToByteArray(ByRef ImageObject As Drawing.Image) As Byte()

        Dim _bytes As Byte() = Nothing

        If ImageObject IsNot Nothing Then

            Try
                Dim _ms As IO.MemoryStream = New IO.MemoryStream()
                ImageObject.Save(_ms, System.Drawing.Imaging.ImageFormat.Png)
                _bytes = _ms.GetBuffer

                _ms.Close()
                _ms.Dispose()

            Catch ex As Exception

            End Try

        End If

        Return _bytes

    End Function

    Private Shared Function CopyToTempFolder(ByVal FilePath As String) As String

        Dim _TempFile As String = Session.TempFolder + Guid.NewGuid.ToString + IO.Path.GetExtension(FilePath)

        Try
            IO.File.Copy(FilePath, _TempFile)

        Catch ex As Exception
            _TempFile = ""
        End Try

        Return _TempFile

    End Function

    Public Shared Function InsertDocument(ByVal KeyID As Guid?, ByVal DocumentType As DocumentType, ByVal FilePath As String, ByVal Subject As String,
                                 Optional ByVal Priv As Boolean = False, Optional ByVal Category As String = "", Optional ByVal SubCat As String = "",
                                 Optional ByVal Tags As String = "", Optional ByVal Notes As String = "") As Guid?

        Return InsertDocument(Session.ConnectionString, KeyID, DocumentType, FilePath, Subject, Priv, Category, SubCat, Tags, Notes)

    End Function

    Public Shared Function InsertDocument(ByVal ConnectionString As String, ByVal KeyID As Guid?, ByVal DocumentType As DocumentType, ByVal FilePath As String, ByVal Subject As String,
                                 Optional ByVal Priv As Boolean = False, Optional ByVal Category As String = "", Optional ByVal SubCat As String = "",
                                 Optional ByVal Tags As String = "", Optional ByVal Notes As String = "") As Guid?

        Dim _Return As Guid? = Nothing

        'sometimes, the file is in use - so we copy the file into a temp folder and attach from there...
        Dim _TempFile As String = CopyToTempFolder(FilePath)
        If _TempFile = "" Then Return Nothing

        FilePath = _TempFile

        Dim _Data As Byte() = FileToByteArray(FilePath)
        If _Data IsNot Nothing Then

            Dim _fi As FileInfo = New FileInfo(FilePath)
            If Not _fi Is Nothing Then

                Dim _doc As New Business.Document
                _Return = _doc._ID

                With _doc
                    ._KeyId = KeyID
                    ._Type = GetDocumentType(DocumentType)
                    ._Subject = Subject
                    ._FileType = RegistryHandler.GetMIMEType(_fi.Extension)
                    ._FileExt = _fi.Extension
                    ._FileSize = _fi.Length
                    ._FileName = _fi.Name
                    ._FilePath = _fi.FullName
                    ._Private = Priv
                    ._Deleted = False
                    ._Category = Category
                    ._SubCategory = SubCat
                    ._Tags = Tags
                    ._Notes = Notes
                    ._UserId = Session.CurrentUser.ID
                    ._UserName = Session.CurrentUser.FullName
                    ._Stamp = Now
                    ._Data = _Data
                End With

                Business.Document.SaveRecord(ConnectionString, _doc)
                _doc = Nothing

            End If

        End If

        Return _Return

    End Function

    Public Shared Function InsertDocument(ByVal KeyID As Guid?, ByVal DocumentType As DocumentType, ByVal FileData As Byte(), ByVal Subject As String, ByVal Tags As String) As Guid?
        Return InsertDocument(Session.ConnectionString, KeyID, DocumentType, FileData, Subject, Tags, "")
    End Function

    Public Shared Function InsertDocument(ByVal KeyID As Guid?, ByVal DocumentType As DocumentType, ByVal FileData As Byte(), ByVal Subject As String, ByVal Tags As String, ByVal FileExtension As String) As Guid?
        Return InsertDocument(Session.ConnectionString, KeyID, DocumentType, FileData, Subject, Tags, FileExtension)
    End Function

    Public Shared Function InsertDocument(ByVal ConnectionString As String, ByVal KeyID As Guid?, ByVal DocumentType As DocumentType, ByVal FileData As Byte(), ByVal Subject As String, ByVal Tags As String, ByVal FileExtension As String) As Guid?

        Dim _Return As Guid? = Nothing

        Dim _doc As New Business.Document
        _Return = _doc._ID

        With _doc
            ._KeyId = KeyID
            ._Type = GetDocumentType(DocumentType)
            ._Subject = Subject
            ._FileType = ""
            ._FileExt = FileExtension
            ._FileSize = FileData.Length
            ._FileName = ""
            ._FilePath = ""
            ._Private = False
            ._Deleted = False
            ._Category = ""
            ._SubCategory = ""
            ._Tags = Tags
            ._Notes = ""
            ._UserId = Session.CurrentUser.ID
            ._UserName = Session.CurrentUser.FullName
            ._Stamp = Now
            ._Data = FileData
        End With

        Business.Document.SaveRecord(ConnectionString, _doc)
        _doc = Nothing

        Return _Return

    End Function

    Public Shared Sub UpdateDocument(ByVal DocumentID As Guid, ByVal DocumentType As DocumentType, ByVal FileData As Byte(), ByVal Subject As String)

        Dim _doc As Business.Document = Business.Document.RetreiveByID(DocumentID)
        With _doc
            ._UserId = Session.CurrentUser.ID
            ._UserName = Session.CurrentUser.FullName
            ._Subject = Subject
            ._Stamp = Now
            ._Data = FileData
            ._FileSize = FileData.Length
        End With

        Business.Document.SaveRecord(_doc)
        _doc = Nothing

    End Sub

    Public Shared Sub UpdateDocument(ByVal DocumentID As Guid, ByVal DocumentType As DocumentType, ByVal FileData As Byte())

        Dim _doc As Business.Document = Business.Document.RetreiveByID(DocumentID)
        With _doc
            ._UserId = Session.CurrentUser.ID
            ._UserName = Session.CurrentUser.FullName
            ._Stamp = Now
            ._Data = FileData
            ._FileSize = FileData.Length
        End With

        Business.Document.SaveRecord(_doc)
        _doc = Nothing

    End Sub

    Public Shared Sub UpdateDocument(ByVal DocumentID As Guid, ByVal DocumentType As DocumentType, ByVal FilePath As String, ByVal Subject As String, _
                                    Optional ByVal Priv As Boolean = False, Optional ByVal Category As String = "", Optional ByVal SubCat As String = "", _
                                    Optional ByVal Tags As String = "", Optional ByVal Notes As String = "")

        Dim _Data As Byte() = FileToByteArray(FilePath)
        If _Data IsNot Nothing Then

            Dim _fi As FileInfo = New FileInfo(FilePath)
            If Not _fi Is Nothing Then

                Dim _doc As Business.Document = Business.Document.RetreiveByID(DocumentID)
                With _doc
                    ._Type = GetDocumentType(DocumentType)
                    ._Subject = Subject
                    ._FileType = RegistryHandler.GetMIMEType(_fi.Extension)
                    ._FileExt = _fi.Extension
                    ._FileSize = _fi.Length
                    ._FileName = _fi.Name
                    ._FilePath = _fi.FullName
                    ._Private = Priv
                    ._Deleted = False
                    ._Category = Category
                    ._SubCategory = SubCat
                    ._Tags = Tags
                    ._Notes = Notes
                    ._UserId = Session.CurrentUser.ID
                    ._UserName = Session.CurrentUser.FullName
                    ._Stamp = Now
                    ._Data = _Data
                End With

                Business.Document.SaveRecord(_doc)
                _doc = Nothing

            End If

        End If

    End Sub

    Public Shared Sub DeleteDocument(ByVal DocumentID As String, Optional ByVal Prompt As Boolean = True)

        If DocumentID = "" Then Exit Sub

        Dim _doc As Business.Document = Business.Document.RetreiveByID(New Guid(DocumentID))
        If _doc IsNot Nothing Then

            Dim _Delete As Boolean = True

            If Prompt Then
                If CareMessage("Are you sure you want to Delete this document?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Delete") = MsgBoxResult.No Then
                    _Delete = False
                End If
            End If

            If _Delete Then
                _doc._Deleted = True
                Business.Document.SaveRecord(_doc)
            End If

            _doc = Nothing

        End If

    End Sub

    Private Shared Function GetDocumentType(ByVal DocType As DocumentType) As String
        If DocType = DocumentType.User Then Return "User"
        If DocType = DocumentType.Application Then Return "Application"
        Return ""
    End Function

    Public Shared Function UpdatePhoto(ByRef DocumentID As Guid?, ByRef KeyID As Guid?, ByRef PhotoControl As Care.Controls.PhotoControl, ByVal FriendlyName As String) As Boolean

        Select Case PhotoControl.Status

            Case Controls.PhotoControl.EnumStatus.Created
                DocumentID = DocumentHandler.InsertDocument(KeyID, DocumentHandler.DocumentType.Application, ImageToByteArray(PhotoControl.Image), "Original Photo of " + FriendlyName, "original", PhotoControl.FileExtension)
                If DocumentID IsNot Nothing AndAlso DocumentID.HasValue Then
                    'now we create a thumbnail version of the original photo
                    Dim _ThumbnailData As Byte() = DocumentHandler.ReturnThumbnail(DocumentHandler.ImageToByteArray(PhotoControl.Image), 325)
                    If _ThumbnailData IsNot Nothing Then
                        Dim _ThumbnailID As Guid? = DocumentHandler.InsertDocument(KeyID, DocumentHandler.DocumentType.Application, _ThumbnailData, "Thumbnail Photo of " + FriendlyName, "thumbnail", PhotoControl.FileExtension)
                        If _ThumbnailID IsNot Nothing And _ThumbnailID.HasValue Then
                            DocumentID = _ThumbnailID
                        End If
                    End If
                End If

            Case Controls.PhotoControl.EnumStatus.Rotated
                If DocumentID.HasValue Then
                    DocumentHandler.UpdateDocument(DocumentID.Value, DocumentHandler.DocumentType.User, DocumentHandler.ImageToByteArray(PhotoControl.Image))
                End If

            Case Controls.PhotoControl.EnumStatus.Deleted
                DocumentID = Nothing

        End Select

        Return True

    End Function

End Class

