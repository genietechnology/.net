﻿Imports Care.Global
Imports Care.Data

Public Class ParameterHandler

    Public Enum EnumParameterType
        StringType
        BooleanType
        DateType
        IntegerType
        LongType
        DecimalType
        FilePathType
        PasswordType
    End Enum

    Public Shared Sub CacheParameters()

        Session.DictionaryParameters.Clear()

        For Each _p In Business.Parameter.RetreiveAll

            'we don't cache integer parameters
            If _p._Type <> "Integer" Then

                Dim _dp As New Dictionary.Parameter
                _dp._ID = _p._ID.Value
                _dp._Name = _p._Name
                _dp._Description = _p._Description
                _dp._Type = _p._Type

                If _p._Type = "Password" Then
                    _dp._Value = EncyptionHandler.DecryptString(_p._Value)
                Else
                    _dp._Value = _p._Value
                End If

                Session.DictionaryParameters.Add(_dp)

            End If

        Next

    End Sub

    Public Shared Function ManagerAuthorised() As Boolean

        Dim _Return As Boolean = False
        Dim _Password As String = ReturnString("MANAGERPASSWORD")

        Dim _frmPassword As New frmPassword(_Password)
        _frmPassword.ShowDialog()

        If _frmPassword.DialogResult = DialogResult.OK Then
            _Return = True
        End If

        Return _Return

    End Function

    Public Shared Sub Create(ByVal Name As String, ByVal DataType As EnumParameterType, ByVal Parent As String, ByVal Description As String, ByVal Value As String)

        Dim _Prm As New Business.Parameter
        With _Prm

            ._ID = Guid.NewGuid
            ._Name = Name
            ._Description = Description
            ._Parent = Parent
            ._Type = ReturnParameterType(DataType)

            If DataType = EnumParameterType.PasswordType Then
                ._Value = EncyptionHandler.EncryptString(Value)
            Else
                ._Value = Value
            End If

        End With

        Business.Parameter.SaveRecord(_Prm)

    End Sub

    Private Shared Function ReturnParameterType(ByVal DataType As EnumParameterType) As String
        Dim _Return As String = "String"
        If DataType = EnumParameterType.BooleanType Then _Return = "Boolean"
        If DataType = EnumParameterType.DateType Then _Return = "Date"
        If DataType = EnumParameterType.IntegerType Then _Return = "Integer"
        If DataType = EnumParameterType.LongType Then _Return = "Long"
        If DataType = EnumParameterType.DecimalType Then _Return = "Decimal"
        If DataType = EnumParameterType.FilePathType Then _Return = "Path"
        If DataType = EnumParameterType.PasswordType Then _Return = "Password"
        Return _Return
    End Function

    Public Shared Function CheckExists(ByVal ParameterName As String) As Boolean

        Dim _Exists As Boolean = False

        Dim _Prm As Business.Parameter = Business.Parameter.RetrieveByName(ParameterName)
        If _Prm IsNot Nothing Then _Exists = True

        _Prm = Nothing
        Return _Exists

    End Function

    Public Shared Sub ConvertToPasswordParameter(ByVal ParameterName As String)
        Dim _Prm As Business.Parameter = Business.Parameter.RetrieveByName(ParameterName)
        If _Prm IsNot Nothing Then
            If _Prm._Type <> "Password" Then
                _Prm._Type = "Password"
                _Prm._Value = EncyptionHandler.EncryptString(_Prm._Value)
                Business.Parameter.SaveRecord(_Prm)
                _Prm = Nothing
            End If
        End If
    End Sub

    Private Shared Sub IncrementNumber(ByVal ParameterName As String)
        Dim _SQL As String = "update AppParams set value = value + 1 where name = '" & ParameterName & "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)
    End Sub

#Region "Returns"

    Public Shared Function ReturnString(ByVal ParameterName As String) As String

        Dim _Return As String = ""
        Dim _DP As Dictionary.Parameter = ReturnParameterFromCache(ParameterName)
        If _DP IsNot Nothing Then
            _Return = _DP._Value
        Else
            Dim _Prm As Business.Parameter = Business.Parameter.RetrieveByName(ParameterName)
            If _Prm IsNot Nothing Then
                If _Prm._Type = "Password" Then
                    _Return = EncyptionHandler.DecryptString(_Prm._Value)
                Else
                    _Return = _Prm._Value
                End If
                _Prm = Nothing
            End If
        End If

        Return _Return

    End Function

    Public Shared Function ReturnBoolean(ByVal ParameterName As String) As Boolean

        Dim _Return As Boolean = False

        Dim _DP As Dictionary.Parameter = ReturnParameterFromCache(ParameterName)
        If _DP IsNot Nothing Then
            If _DP._Value.ToUpper = "TRUE" Then
                _Return = True
            Else
                _Return = False
            End If
        Else
            Dim _Prm As Business.Parameter = Business.Parameter.RetrieveByName(ParameterName)
            If _Prm IsNot Nothing Then
                If _Prm._Value.ToUpper = "TRUE" Then
                    _Return = True
                End If
                _Prm = Nothing
            End If
        End If

        Return _Return

    End Function

    Public Shared Function ReturnDate(ByVal ParameterName As String) As Date?

        Dim _Return As Date? = Nothing
        Dim _Prm As Business.Parameter = Business.Parameter.RetrieveByName(ParameterName)
        If _Prm IsNot Nothing Then
            If _Prm._Value.ToString = "" Then
                Return Nothing
            Else
                Try
                    _Return = CType(_Prm._Value.ToString, Date?)
                Catch ex As Exception
                    ErrorHandler.LogExceptionToDatabase(ex, False)
                    _Return = Nothing
                End Try
            End If
            _Prm = Nothing
        End If

        Return _Return

    End Function

    Public Shared Function ReturnLong(ByVal ParameterName As String, ByVal Increment As Boolean) As Long

        Dim _Return As Long = 0
        Dim _Prm As Business.Parameter = Business.Parameter.RetrieveByName(ParameterName)

        If _Prm IsNot Nothing Then
            Dim _String As String = _Prm._Value
            If IsNumeric(_String) Then
                _Return = Long.Parse(_String)
                If Increment Then IncrementNumber(ParameterName)
            End If
            _Prm = Nothing
        End If

        Return _Return

    End Function

    Public Shared Function ReturnInteger(ByVal ParameterName As String, ByVal Increment As Boolean) As Integer

        Dim _Return As Integer = 0
        Dim _Prm As Business.Parameter = Business.Parameter.RetrieveByName(ParameterName)

        If _Prm IsNot Nothing Then
            Dim _String As String = _Prm._Value
            If IsNumeric(_String) Then
                _Return = Integer.Parse(_String)
                If Increment Then IncrementNumber(ParameterName)
            End If
            _Prm = Nothing
        End If

        Return _Return

    End Function

    Public Shared Function ReturnDecimal(ByVal ParameterName As String) As Decimal

        Dim _Return As Decimal = 0
        Dim _Prm As Business.Parameter = Business.Parameter.RetrieveByName(ParameterName)

        If _Prm IsNot Nothing Then
            Dim _String As String = _Prm._Value
            If IsNumeric(_String) Then
                _Return = Decimal.Parse(_String)
            End If
            _Prm = Nothing
        End If

        Return _Return

    End Function

    Public Shared Function ReturnPath(ByVal ParameterName As String) As String

        Dim _Return As String = ReturnString(ParameterName)
        If _Return <> "" Then

            'check if there is a slash at the end of the path, and remove it
            If _Return.EndsWith("\") Then
                _Return = _Return.Substring(0, _Return.Length - 1)
            End If

            'check the file path exists, if it doesn't create it
            If Not IO.Directory.Exists(_Return) Then

                Try
                    IO.Directory.CreateDirectory(_Return)

                Catch ex As Exception
                    CareMessage("Create Folder [" + _Return + "] failed.", MessageBoxIcon.Exclamation, "ParameterHandler.ReturnPath")
                    Return ""
                End Try

            End If

            'ensure the slash is put back at the end
            _Return += "\"

        End If

        Return _Return

    End Function

#End Region

#Region "Sets"

    Public Shared Sub SetDate(ByVal ParameterName As String, ByVal DateIn As Date?)

        Dim _Prm As Business.Parameter = Business.Parameter.RetrieveByName(ParameterName)
        If _Prm IsNot Nothing Then

            If DateIn Is Nothing Then
                _Prm._Value = ""
            Else
                _Prm._Value = DateIn.ToString
            End If

            Business.Parameter.SaveRecord(_Prm)
            _Prm = Nothing

        End If

    End Sub

    Public Shared Sub SetString(ByVal ParameterName As String, ByVal Value As String)

        Dim _Prm As Business.Parameter = Business.Parameter.RetrieveByName(ParameterName)
        If _Prm IsNot Nothing Then
            _Prm._Value = Value
        End If

        Business.Parameter.SaveRecord(_Prm)
        _Prm = Nothing

    End Sub

    Private Shared Function ReturnParameterFromCache(ByVal ParameterName As String) As Dictionary.Parameter
        If Session.DictionaryParameters IsNot Nothing AndAlso Session.DictionaryParameters.Count > 0 Then
            For Each _dp In Session.DictionaryParameters
                If _dp._Name = ParameterName Then
                    Return _dp
                End If
            Next
        End If
        Return Nothing
    End Function

#End Region

End Class
