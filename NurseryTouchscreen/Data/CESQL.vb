﻿Imports System.Data.SqlServerCe
Imports System.Reflection

Public Class CESQL

#Region "STANDARD CE DATA FUNCTIONS"

    Public Shared Sub subDisplayDataError(ByVal ErrorTitle As String, ByVal ex As SqlCeException, ByVal strSQL As String)

        Dim _Mess As String

        _Mess = "Exception: " & ex.Message & vbCrLf & vbCrLf & _
                "SQL: " & strSQL

        Msgbox(_Mess, MessageBoxIcon.Error, ErrorTitle)

        'Dim frm As New frmELLDataError
        'frm.subPassArguments(ErrorTitle, ex, ConnectionString, strSQL)
        'frm.ShowDialog()
        'frm.BringToFront()

        'frm.Dispose()

    End Sub

    Public Shared Function ReturnDataReader(ByVal strSQL As String) As SqlCeDataReader

        Cursor.Current = Cursors.WaitCursor

        Dim cmd As SqlCeCommand = Nothing
        Dim dr As SqlCeDataReader = Nothing

        cmd = ReturnCommand(strSQL)

        Try
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)

        Catch ex As SqlCeException
            Call subDisplayDataError("ReturnDataReader", ex, strSQL)
            dr = Nothing
        End Try

        cmd.Dispose()

        Cursor.Current = Cursors.Default
        Return dr

    End Function

    Public Shared Function ReturnDataAdapter(ByVal strSQL As String) As SqlCeDataAdapter

        Cursor.Current = Cursors.WaitCursor

        Dim da As SqlCeDataAdapter = Nothing

        Try
            da = New SqlCeDataAdapter(strSQL, SharedModule.ConnectionString)
            With da
                .MissingSchemaAction = MissingSchemaAction.AddWithKey
                .AcceptChangesDuringFill = True
                .AcceptChangesDuringUpdate = True
                .ContinueUpdateOnError = False
            End With

        Catch ex As SqlCeException
            Call subDisplayDataError("ReturnDataAdapter", ex, strSQL)
            da = Nothing
        End Try

        Cursor.Current = Cursors.Default
        Return da

    End Function

    Public Shared Function ReturnConnection() As SqlCeConnection

        Cursor.Current = Cursors.WaitCursor

        Dim cn As SqlCeConnection = Nothing

        Try
            cn = New SqlCeConnection
            cn.ConnectionString = SharedModule.ConnectionString
            cn.Open()

        Catch ex As SqlCeException
            Call subDisplayDataError("ReturnConnection", ex, "")
            cn = Nothing
        End Try

        Cursor.Current = Cursors.Default
        Return cn

    End Function

    Public Shared Function ReturnCommand(ByVal strSQL As String) As SqlCeCommand

        Cursor.Current = Cursors.WaitCursor

        Dim cn As New SqlCeConnection
        Dim cmd As New SqlCeCommand

        cn = ReturnConnection()
        If Not cn Is Nothing Then
            cmd.CommandType = CommandType.Text
            cmd.CommandText = strSQL
            cmd.Connection = cn
        End If

        Cursor.Current = Cursors.Default
        Return cmd

    End Function

    Public Shared Function ExecuteCommand(ByVal strSQL As String) As Boolean

        Dim blnOK As Boolean = True
        Dim cmd As New SqlCeCommand
        cmd = ReturnCommand(strSQL)

        If Not cmd Is Nothing Then

            Try
                cmd.ExecuteNonQuery()

            Catch ex As SqlCeException
                blnOK = False
                Call subDisplayDataError("ExecuteCommand", ex, strSQL)

            End Try

        End If

        cmd.Dispose()
        cmd = Nothing

        Return blnOK

    End Function

    Private Shared Function SortQuotes(ByVal StringIn As String) As String
        If StringIn = "NULL" Then
            Return StringIn
        Else
            StringIn = Replace(StringIn, "'", "''")
            Return "'" + StringIn + "'"
        End If
    End Function

    Public Shared Function UpdateDB(ByVal UpdateDataAdapter As IDataAdapter, ByVal UpdateDataSet As DataSet) As Boolean

        Dim blnOK As Boolean = True
        Cursor.Current = Cursors.WaitCursor

        Try

            Dim cmdBld As New SqlCeCommandBuilder(UpdateDataAdapter)
            UpdateDataAdapter = cmdBld.DataAdapter
            UpdateDataAdapter.Update(UpdateDataSet)
            cmdBld.Dispose()

        Catch ex As Exception
            Cursor.Current = Cursors.Default
            blnOK = False
        End Try

        Cursor.Current = Cursors.Default
        Return blnOK

    End Function

#End Region

End Class
