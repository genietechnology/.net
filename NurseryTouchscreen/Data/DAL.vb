﻿Imports System.Data
Imports System.Reflection
Imports System.IO

Public Class DAL

    Public Enum EnumDALMode
        Local
        Direct
    End Enum

    Public Shared Function ReturnConnectionString(Optional ByVal ConnectionMode As EnumDALMode = Nothing) As String

        Dim _CN As String = ""

        If ConnectionMode = Nothing Then
            If Parameters.UseDirectSQL Then
                ConnectionMode = EnumDALMode.Direct
            Else
                ConnectionMode = EnumDALMode.Local
            End If
        End If

        If ConnectionMode = EnumDALMode.Local Then
            _CN = "Data Source = " + IO.Path.GetDirectoryName(Application.ExecutablePath) + "\DB.sdf;"
        Else

            If Parameters.DirectConnectionString = "" Then
                _CN = "" & _
                      "Data Source=" & Parameters.ServerIP & ";" & _
                      "Database=nursery;" & _
                      "User ID=tablet;" & _
                      "Password=c@r3s0ftw@r3" & ";"
            Else
                _CN = Parameters.DirectConnectionString
            End If

        End If

        Return _CN

    End Function

    Public Shared Sub ExecuteCommand(ByVal SQL As String)

        Dim _Current As Cursor = Cursor.Current
        Cursor.Current = Cursors.WaitCursor

        If Parameters.UseDirectSQL Then
            DirectSQL.ExecuteCommand(SQL)
        Else
            'CESQL.ExecuteCommand(SQL)
        End If

        If _Current = Cursors.Default Then Cursor.Current = Cursors.Default

    End Sub

    Public Shared Function ReturnField(ByVal TableName As String, _
                                      ByVal FilterField As String, ByVal FilterValue As String, _
                                      ByVal TargetField As String) As String

        Dim strSQL As String
        Dim strReturn As String = ""
        Dim dr As DataRow = Nothing

        strSQL = "select " & TargetField & " from " & TableName & _
                 " where " & FilterField & " = '" & FilterValue & "'"

        dr = ReturnDataRow(strSQL)

        If Not dr Is Nothing Then
            strReturn = dr.Item(0).ToString
        End If

        dr = Nothing

        Return strReturn

    End Function

    Public Shared Function ReturnParam(ByVal ParameterField As String) As String

        Dim strSQL As String
        Dim strReturn As String = ""
        Dim dr As DataRow = Nothing

        strSQL = "select " & ParameterField & " from mbprmfle"
        dr = ReturnDataRow(strSQL)

        If Not dr Is Nothing Then
            strReturn = dr.Item(0).ToString
        End If

        dr = Nothing

        Return strReturn

    End Function

    Public Shared Function ReturnDataAdapter(ByVal strSQL As String) As IDataAdapter

        Dim _Current As Cursor = Cursor.Current
        Cursor.Current = Cursors.WaitCursor

        Dim _Return As IDataAdapter = Nothing

        If Parameters.UseDirectSQL Then
            _Return = DirectSQL.ReturnDataAdapter(strSQL)
        Else
            '_Return = CESQL.ReturnDataAdapter(strSQL)
        End If

        If _Current = Cursors.Default Then Cursor.Current = Cursors.Default
        Return _Return

    End Function

    Public Shared Function ReturnDataTable(ByVal strSQL As String) As DataTable

        Dim _Current As Cursor = Cursor.Current
        Cursor.Current = Cursors.WaitCursor

        Dim da As IDataAdapter = Nothing
        Dim ds As New DataSet
        Dim dt As DataTable = Nothing

        da = ReturnDataAdapter(strSQL)
        If Not da Is Nothing Then

            Try
                da.Fill(ds)
                dt = (ds.Tables(0))

            Catch ex As Exception
                Msgbox(ex.Message + vbCrLf + vbCrLf + "SQL: " + strSQL + vbCrLf + vbCrLf + "ConnectionString: " + ReturnConnectionString(), MessageBoxIcon.Exclamation, "ReturnDataTable")
                dt = Nothing
            End Try

            da = Nothing

        End If

        If _Current = Cursors.Default Then Cursor.Current = Cursors.Default
        Return dt

    End Function

    Public Shared Function ReturnDataRow(ByVal strSQL As String) As DataRow

        Dim _Current As Cursor = Cursor.Current
        Cursor.Current = Cursors.WaitCursor

        Dim dr As DataRow = Nothing

        Dim dt As DataTable = ReturnDataTable(strSQL)
        If Not dt Is Nothing Then

            If dt.Rows.Count = 1 Then dr = dt.Rows(0)

            dt.Dispose()
            dt = Nothing

        End If

        If _Current = Cursors.Default Then Cursor.Current = Cursors.Default
        Return dr

    End Function

    Public Shared Function UpdateDB(ByVal UpdateDataAdapter As IDataAdapter, ByVal UpdateDataSet As DataSet, Optional Interact As Boolean = True) As Boolean
        If Parameters.UseDirectSQL Then
            Return DirectSQL.UpdateDB(UpdateDataAdapter, UpdateDataSet, Interact)
        Else
            'Return CESQL.UpdateDB(UpdateDataAdapter, UpdateDataSet)
        End If
    End Function

    Public Shared Function GetImagefromByteArray(ByVal ObjectIn As Object) As Image

        If ObjectIn Is Nothing Then Return Nothing

        Dim _Bytes As Byte() = GetImage(ObjectIn)
        If _Bytes IsNot Nothing Then

            Dim _ms As MemoryStream = New MemoryStream(_Bytes, 0, _Bytes.Length)
            Dim _im As Image = Image.FromStream(_ms)

            _ms.Close()
            _ms.Dispose()

            Return _im

        Else
            Return Nothing
        End If

    End Function

    Private Shared Function GetImage(ByVal ObjectIn As Object) As Byte()

        If ObjectIn Is DBNull.Value Then
            Return Nothing
        Else
            Return CType(ObjectIn, Byte())
        End If

    End Function

    Public Shared Function ReturnRecordCount(ByVal SQL As String) As Integer
        Dim _DT As DataTable = ReturnDataTable(SQL)
        If _DT IsNot Nothing Then
            Return _DT.Rows.Count
        Else
            Return 0
        End If
    End Function

End Class
