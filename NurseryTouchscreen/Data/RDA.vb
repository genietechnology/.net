﻿Imports System.Data
Imports System.Data.SqlServerCe

Public Class RDA

    Private Shared m_RDAConnectionString As String = ""
    Private Shared m_IISURL As String = ""

    Public Sub New()

        m_IISURL = "http://" + Parameters.ServerIP + "/rda/sqlcesa35.dll"

        m_RDAConnectionString = "Provider=SQLOLEDB;" & _
                                "Data Source=" & Parameters.ServerIP & ";" & _
                                "Database=nursery;" & _
                                "User ID=tablet;" & _
                                "Password=c@r3s0ftw@r3" & ";"

    End Sub

#Region "Push and Pull Functions"

    Private Function Pull(ByVal LocalTable As String, _
                                   ByVal ServerSQL As String, Optional ByVal TrackingMode As RdaTrackOption = RdaTrackOption.TrackingOnWithIndexes) As Boolean

        Dim blnOK As Boolean = True
        Dim strErrorTable As String

        strErrorTable = LocalTable & "_Errors"

        Cursor.Current = Cursors.WaitCursor

        Try
            CheckDBExists()
            CheckTableExists(LocalTable, True)

            Dim rda As New SqlCeRemoteDataAccess()
            With rda
                .InternetUrl = m_IISURL
                .ConnectionRetryTimeout = 3 'secs
                .ConnectTimeout = 6000 'millisecs
                .ReceiveTimeout = 6000 'millisecs
                .SendTimeout = 6000 'millisecs
                .LocalConnectionString = DAL.ReturnConnectionString(DAL.EnumDALMode.Local)
                .ConnectionManager = True
                .Pull(LocalTable, ServerSQL, m_RDAConnectionString, TrackingMode, strErrorTable)
                .Dispose()
            End With

        Catch ex As SqlCeException
            blnOK = False
            Cursor.Current = Cursors.Default
        End Try

        Cursor.Current = Cursors.Default
        Return blnOK

    End Function

    Private Shared Function Push(ByVal LocalTable As String, Optional ByVal ForcePush As Boolean = False) As Boolean

        Dim blnOK As Boolean = True
        Cursor.Current = Cursors.WaitCursor

        'check the local table exists before we send it back...
        If CheckLocalTableExists(LocalTable, False) Then

            Dim rda As New SqlCeRemoteDataAccess

            Try

                With rda
                    .InternetUrl = m_IISURL
                    .ConnectionRetryTimeout = 3 'secs
                    .ConnectTimeout = 3000 'millisecs
                    .ReceiveTimeout = 3000 'millisecs
                    .SendTimeout = 3000 'millisecs
                    .LocalConnectionString = DAL.ReturnConnectionString(DAL.EnumDALMode.Local)
                    .ConnectionManager = True
                    .Push(LocalTable, m_RDAConnectionString, RdaBatchOption.BatchingOff)
                    .Dispose()
                End With

            Catch ex As SqlCeException

                blnOK = False
                Cursor.Current = Cursors.Default
                rda.Dispose()

            End Try

            Cursor.Current = Cursors.Default
            Return blnOK

        Else
            'nothing to upload
            Return True
        End If

    End Function

#End Region

#Region "Private Helpers"

    Public Shared Function CheckLocalTableExists(ByVal TableName As String, ByVal DropTable As Boolean) As Boolean

        Dim tableFound As Boolean = False

        Using conn As New SqlCeConnection

            Dim cmd As New SqlCeCommand()
            conn.ConnectionString = DAL.ReturnConnectionString(DAL.EnumDALMode.Local)
            cmd.Connection = conn
            cmd.CommandText = "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = @tableName"
            cmd.Parameters.AddWithValue("@tableName", TableName)
            conn.Open()
            If cmd.ExecuteScalar() = 1 Then
                tableFound = True

                If DropTable Then
                    cmd.Parameters.Clear()
                    cmd.CommandText = String.Format("DROP TABLE {0}", TableName)
                    cmd.ExecuteNonQuery()
                End If
            End If

        End Using

        Return tableFound

    End Function

    Public Function CheckTableExists(ByVal TableName As String, ByVal DropTable As Boolean) As Boolean

        Dim tableFound As Boolean = False

        Using conn As New SqlCeConnection

            Dim cmd As New SqlCeCommand()
            conn.ConnectionString = DAL.ReturnConnectionString(DAL.EnumDALMode.Local)
            cmd.Connection = conn
            cmd.CommandText = "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = @tableName"
            cmd.Parameters.AddWithValue("@tableName", TableName)
            conn.Open()
            If cmd.ExecuteScalar() = 1 Then
                tableFound = True

                If DropTable Then
                    cmd.Parameters.Clear()
                    cmd.CommandText = String.Format("DROP TABLE {0}", TableName)
                    cmd.ExecuteNonQuery()
                End If
            End If

        End Using

        Return tableFound

    End Function

    Private Sub HandleTables(ByVal TableName As String, ByVal Initialise As Boolean, ByVal PushedBack As Boolean, ByVal SQLQuery As String)

        If Initialise Then
            CheckTableExists(TableName, True)
            If PushedBack Then
                Pull(TableName, SQLQuery, SqlServerCe.RdaTrackOption.TrackingOnWithIndexes)
            Else
                Pull(TableName, SQLQuery, SqlServerCe.RdaTrackOption.TrackingOffWithIndexes)
            End If
        Else
            If CheckTableExists(TableName, False) = False Then
                If PushedBack Then
                    Pull(TableName, SQLQuery, SqlServerCe.RdaTrackOption.TrackingOnWithIndexes)
                Else
                    Pull(TableName, SQLQuery, SqlServerCe.RdaTrackOption.TrackingOffWithIndexes)
                End If
            End If
        End If

    End Sub

    Public Function CheckDBExists() As Boolean

        Dim _Exists As Boolean = False
        Dim _cn As String = DAL.ReturnConnectionString(DAL.EnumDALMode.Local)
        Using conn As New SqlCeConnection
            conn.ConnectionString = _cn
            If Not System.IO.File.Exists(conn.Database) Then
                Using engine As New SqlCeEngine(_cn)
                    engine.CreateDatabase()
                    _Exists = True
                End Using
            Else
                _Exists = True
            End If
        End Using
        Return _Exists
    End Function

#End Region

#Region "PULL - Main Tables"

    Public Function PullDay() As Boolean

        Dim _Return As Boolean = False
        Dim _SQL As String = "select id, date, breakfast_id, breakfast_name, snack_id, snack_name," & _
                               " lunch_id, lunch_name, lunch_des_id, lunch_des_name, tea_id, tea_name, tea_des_id, tea_des_name" & _
                               " from day" & _
                               " where date >= '" & Parameters.TodaySQLDate & "'"

        HandleTables("day", True, False, _SQL)

        Return SharedModule.DayExists

    End Function

    Public Function PullRegister() As Boolean
        Dim _SQL As String = "select * from register where date = '" & Parameters.TodaySQLDate & "'"
        HandleTables("register", True, True, _SQL)
        Return True
    End Function

    Public Function PullContacts() As Boolean

        Dim _SQL As String = "select ID, Family_ID, forename, fullname, relationship, primary_cont, emer_cont," & _
                             " sms, sms_name, tel_home, tel_mobile, job_tel, password" & _
                             " from contacts"

        HandleTables("contacts", True, False, _SQL)
        Return True

    End Function

    Public Function PullChildren() As Boolean

        Dim _SQL As String = "select ID, fullname, surname, forename, status, dob, gender, family_id, family_name," & _
                               " group_id, group_name, keyworker_name, nappies, milk, baby_food, off_menu," & _
                               " med_allergies, med_medication, med_notes, allergy_rating," & _
                               " cons_offsite, cons_outdoor, cons_photo, cons_plasters, cons_suncream," & _
                               " monday, tuesday, wednesday, thursday, friday, saturday, sunday," & _
                               " sms_milk, sms_food, sms_toilet, sms_sleep" & _
                               " from children"

        HandleTables("children", True, False, _SQL)
        Return True

    End Function

    Public Function PullActivity() As Boolean

        Dim _SQL As String = "select * from activity" & _
                               " where day_id = '" & Parameters.TodayID & "'"

        HandleTables("activity", True, True, _SQL)
        Return True

    End Function

    Public Function PullFoodRegister()

        Dim _SQL As String = "select * from foodregister where day_id = '" & Parameters.TodayID & "'"
        HandleTables("foodregister", True, True, _SQL)
        Return True

    End Function

    Public Function PullMedicalAdmin()

        Dim _SQL As String = "select * from MedicineAuth where day_id = '" & Parameters.TodayID & "'"
        HandleTables("MedicineAuth", True, True, _SQL)
        Return True

    End Function

    Public Function PullMedicalLog()

        Dim _SQL As String = "select * from MedicineLog where day_id = '" & Parameters.TodayID & "'"
        HandleTables("MedicineLog", True, True, _SQL)
        Return True

    End Function

    Public Function PullAccidentLog()

        Dim _SQL As String = "select * from AccidentLog where day_id = '" & Parameters.TodayID & "'"
        HandleTables("AccidentLog", True, True, _SQL)
        Return True

    End Function

    Public Function PullParameters()

        Dim _SQL As String = "select ID, name, value from AppParams" & _
                             " where name = 'SMS' or name = 'SMSSENDER'"

        HandleTables("appparams", True, False, _SQL)
        Return True

    End Function

#End Region

#Region "PULL - Supporting Tables"

    Public Function PullSupportingTables()

        If PullStaff() Then
            If PullGroups() Then
                If PullMeals() Then
                    If PullRequests Then
                        If PullQuickTexts() Then
                            Return True
                        Else
                            Return False
                        End If
                    Else
                        Return False
                    End If
                Else
                    Return False
                End If
            Else
                Return False
            End If
        Else
            Return False
        End If

    End Function

    Public Function PullQuickTexts() As Boolean

        Dim _SQL As String = "select AppLists.name as 'ListName', AppListItems.name as 'ItemName', " & _
                             " AppListItems.seq as 'ItemSeq'" & _
                             " from AppLists" & _
                             " left join AppListItems on AppListItems.list_id = AppLists.ID" & _
                             " where AppLists.name = 'Nature of Illness'" & _
                             " OR AppLists.name = 'Medicine'" & _
                             " OR AppLists.name = 'Dosage'"

        HandleTables("QuickTexts", True, False, _SQL)
        Return True

    End Function

    Public Function PullRequests() As Boolean
        Dim _SQL As String = "select id, description from Requests"
        HandleTables("requests", True, False, _SQL)
        Return True
    End Function

    Public Function PullStaff() As Boolean
        Dim _SQL As String = "select id, fullname, date_left, group_id from staff"
        HandleTables("staff", True, False, _SQL)
        Return True
    End Function

    Public Function PullGroups() As Boolean
        Dim _SQL As String = "select id, name, sequence from groups"
        HandleTables("groups", True, False, _SQL)
        Return True
    End Function

    Public Function PullMeals()

        Dim _SQL As String = ""

        _SQL = "select id, name from meals"
        HandleTables("meals", True, False, _SQL)

        _SQL = "select * from mealcomponents"
        HandleTables("mealcomponents", True, False, _SQL)

        _SQL = "select id, name, group_id, group_name, baby_food from Food"
        HandleTables("food", True, False, _SQL)

        Return True

    End Function

#End Region

#Region "PUSH Tables"

    Public Function PushFoodRegister() As Boolean
        Return Push("foodregister")
    End Function

    Public Function PushRegister() As Boolean
        Return Push("register")
    End Function

    Public Function PushActivity() As Boolean
        Return Push("activity")
    End Function

    Public Function PushMedicalAdmin() As Boolean
        Return Push("MedicineAuth")
    End Function

    Public Function PushMedicalLog() As Boolean
        Return Push("MedicineLog")
    End Function

    Public Function PushAccidentLog() As Boolean
        Return Push("AccidentLog")
    End Function

#End Region

End Class
