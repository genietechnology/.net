﻿Imports System.Data.SqlClient

Public Class DirectSQL

    Public Shared Function ExecuteCommand(ByVal SQLStatement As String) As Boolean

        Dim _Return As Boolean = True
        Dim _cmd As SqlCommand = ReturnTextCommand(DAL.ReturnConnectionString, SQLStatement)

        Try
            _cmd.ExecuteNonQuery()

        Catch ex As Exception
            _Return = False
            Msgbox(ex.Message, MessageBoxIcon.Error, "DAL.ExecuteCommand")
        End Try

        DisposeofCommand(_cmd)
        Return _Return

    End Function

    Private Shared Function ReturnTextCommand(ByVal ConnectionString As String, ByVal CommandText As String) As SqlCommand

        Dim _cmd As New SqlCommand
        With _cmd
            .Connection = ReturnConnection(ConnectionString)
            .CommandType = CommandType.Text
            .CommandText = CommandText
            .Parameters.Clear()
        End With

        Return _cmd

    End Function

    Private Shared Function ReturnConnection(ByVal ConnectionString As String) As SqlConnection

        Dim _cn As New SqlConnection
        _cn.ConnectionString = ConnectionString

        Try
            _cn.Open()

        Catch ex As Exception
            MsgBox(ex.Message, MessageBoxIcon.Error, "DAL.ReturnConnection")
            Return Nothing
        End Try

        Return _cn

    End Function

    Public Shared Function ReturnDataAdapter(ByVal strSQL As String) As SqlDataAdapter

        Dim da As SqlDataAdapter = Nothing

        Try
            da = New SqlDataAdapter(strSQL, SharedModule.ConnectionString)
            With da
                .MissingSchemaAction = MissingSchemaAction.AddWithKey
                .AcceptChangesDuringFill = True
                .AcceptChangesDuringUpdate = True
                .ContinueUpdateOnError = False
            End With

        Catch ex As SqlException
            Msgbox(ex.Message, MessageBoxIcon.Error, "DAL.ReturnDataAdapter")
            da = Nothing
        End Try

        Return da

    End Function

    Public Shared Function ReturnDataTable(ByVal SQL As String) As DataTable

        Dim _Return As DataTable = Nothing

        Dim _DT As New DataTable
        Dim _DA As SqlDataAdapter = ReturnDataAdapter(SQL)

        Try

            _DA.Fill(_DT)
            _Return = _DT

            _DT.Dispose()
            _DT = Nothing

        Catch ex As SqlException
            Msgbox(ex.Message, MessageBoxIcon.Error, "DAL.ReturnDataTable")
        Finally

        End Try

        Return _Return

    End Function

    Public Shared Function ReturnDataRow(ByVal SQL As String) As DataRow

        Dim _Return As DataRow = Nothing
        Dim _DT As DataTable = ReturnDataTable(SQL)
        If _DT IsNot Nothing Then

            _Return = GetRowFromTable(_DT)

            _DT.Dispose()
            _DT = Nothing

        End If

        Return _Return

    End Function

    Private Shared Function GetRowFromTable(ByVal Table As DataTable) As DataRow

        If Table Is Nothing OrElse Table.Rows.Count = 0 Then
            Return Nothing
        Else
            Return Table.Rows(0)
        End If

    End Function

    Public Shared Sub DisposeofCommand(ByRef cmd As SqlCommand)

        If cmd Is Nothing Then Exit Sub

        With cmd
            .Parameters.Clear()
            .Connection.Close()
            .Connection.Dispose()
            .Dispose()
        End With

        cmd = Nothing

    End Sub

    Public Shared Function ReturnTextCommand(ByVal CommandText As String) As SqlCommand

        Dim _cmd As New SqlCommand
        With _cmd
            .Connection = New SqlConnection(DAL.ReturnConnectionString)
            .CommandType = CommandType.Text
            .CommandText = CommandText
            .Parameters.Clear()
        End With

        Return _cmd

    End Function

    Public Shared Function UpdateDB(ByVal UpdateDataAdapter As IDataAdapter, ByVal UpdateDataSet As DataSet, Optional Interact As Boolean = True) As Boolean

        Dim blnOK As Boolean = True

        Try

            Dim cmdBld As New SqlCommandBuilder(UpdateDataAdapter)
            UpdateDataAdapter = cmdBld.DataAdapter
            UpdateDataAdapter.Update(UpdateDataSet)
            cmdBld.Dispose()

        Catch ex As Exception
            If Interact Then Msgbox(ex.Message, MessageBoxIcon.Error, "DAL.UpdateDB")
            blnOK = False
        End Try

        Return blnOK

    End Function

    Public Shared Function ReturnScalar(ByVal ConnectionString As String, ByVal SQLStatement As String) As Object

        Dim _Return As Object = Nothing
        If SQLStatement = "" Then Return Nothing

        Dim _cmd As SqlCommand = ReturnTextCommand(ConnectionString, SQLStatement)

        Try
            _Return = _cmd.ExecuteScalar

        Catch ex As Exception
            Msgbox(ex.Message, MessageBoxIcon.Error, "DAL.ReturnScalar")
        End Try

        DisposeofCommand(_cmd)

        Return _Return

    End Function

End Class
