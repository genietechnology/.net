﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLDMain
    Inherits frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLDMain))
        Me.Button7 = New DevExpress.XtraEditors.SimpleButton()
        Me.Button8 = New DevExpress.XtraEditors.SimpleButton()
        Me.Button4 = New DevExpress.XtraEditors.SimpleButton()
        Me.Button5 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEasy = New DevExpress.XtraEditors.SimpleButton()
        Me.btnQuickObs = New DevExpress.XtraEditors.SimpleButton()
        Me.btnColours = New DevExpress.XtraEditors.SimpleButton()
        Me.btnShapes = New DevExpress.XtraEditors.SimpleButton()
        Me.btnLetters = New DevExpress.XtraEditors.SimpleButton()
        Me.btnExit = New DevExpress.XtraEditors.SimpleButton()
        Me.btnNumbers = New DevExpress.XtraEditors.SimpleButton()
        Me.btnFlashcards = New DevExpress.XtraEditors.SimpleButton()
        Me.btnVideos = New DevExpress.XtraEditors.SimpleButton()
        Me.Button1 = New DevExpress.XtraEditors.SimpleButton()
        Me.Button2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SuspendLayout()
        '
        'Button7
        '
        Me.Button7.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.Location = New System.Drawing.Point(689, 436)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(320, 90)
        Me.Button7.TabIndex = 30
        '
        'Button8
        '
        Me.Button8.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.Location = New System.Drawing.Point(351, 436)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(320, 90)
        Me.Button8.TabIndex = 31
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(351, 120)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(320, 90)
        Me.Button4.TabIndex = 35
        '
        'Button5
        '
        Me.Button5.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(689, 120)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(320, 90)
        Me.Button5.TabIndex = 36
        '
        'btnEasy
        '
        Me.btnEasy.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEasy.Image = Global.NurseryTouchscreen.My.Resources.Resources.spark64
        Me.btnEasy.Location = New System.Drawing.Point(689, 12)
        Me.btnEasy.Name = "btnEasy"
        Me.btnEasy.Padding = New System.Windows.Forms.Padding(7, 0, 0, 0)
        Me.btnEasy.Size = New System.Drawing.Size(320, 90)
        Me.btnEasy.TabIndex = 34
        Me.btnEasy.Text = "sparkPRO"
        '
        'btnQuickObs
        '
        Me.btnQuickObs.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnQuickObs.Image = Global.NurseryTouchscreen.My.Resources.Resources.Eye_64
        Me.btnQuickObs.Location = New System.Drawing.Point(14, 12)
        Me.btnQuickObs.Name = "btnQuickObs"
        Me.btnQuickObs.Size = New System.Drawing.Size(320, 90)
        Me.btnQuickObs.TabIndex = 32
        Me.btnQuickObs.Text = "Quick Observation"
        '
        'btnColours
        '
        Me.btnColours.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnColours.Image = CType(resources.GetObject("btnColours.Image"), System.Drawing.Image)
        Me.btnColours.Location = New System.Drawing.Point(689, 225)
        Me.btnColours.Name = "btnColours"
        Me.btnColours.Size = New System.Drawing.Size(320, 90)
        Me.btnColours.TabIndex = 20
        Me.btnColours.Text = "Colours"
        '
        'btnShapes
        '
        Me.btnShapes.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShapes.Image = CType(resources.GetObject("btnShapes.Image"), System.Drawing.Image)
        Me.btnShapes.Location = New System.Drawing.Point(351, 330)
        Me.btnShapes.Name = "btnShapes"
        Me.btnShapes.Size = New System.Drawing.Size(320, 90)
        Me.btnShapes.TabIndex = 15
        Me.btnShapes.Text = "Shapes"
        '
        'btnLetters
        '
        Me.btnLetters.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLetters.Image = CType(resources.GetObject("btnLetters.Image"), System.Drawing.Image)
        Me.btnLetters.Location = New System.Drawing.Point(351, 225)
        Me.btnLetters.Name = "btnLetters"
        Me.btnLetters.Size = New System.Drawing.Size(320, 90)
        Me.btnLetters.TabIndex = 13
        Me.btnLetters.Text = "Letters"
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Image = CType(resources.GetObject("btnExit.Image"), System.Drawing.Image)
        Me.btnExit.Location = New System.Drawing.Point(14, 436)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(320, 90)
        Me.btnExit.TabIndex = 23
        Me.btnExit.Text = "Back"
        '
        'btnNumbers
        '
        Me.btnNumbers.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNumbers.Image = CType(resources.GetObject("btnNumbers.Image"), System.Drawing.Image)
        Me.btnNumbers.Location = New System.Drawing.Point(14, 225)
        Me.btnNumbers.Name = "btnNumbers"
        Me.btnNumbers.Size = New System.Drawing.Size(320, 90)
        Me.btnNumbers.TabIndex = 12
        Me.btnNumbers.Text = "Numbers"
        '
        'btnFlashcards
        '
        Me.btnFlashcards.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFlashcards.Image = CType(resources.GetObject("btnFlashcards.Image"), System.Drawing.Image)
        Me.btnFlashcards.Location = New System.Drawing.Point(14, 330)
        Me.btnFlashcards.Name = "btnFlashcards"
        Me.btnFlashcards.Size = New System.Drawing.Size(320, 90)
        Me.btnFlashcards.TabIndex = 14
        Me.btnFlashcards.Text = "Flashcards"
        '
        'btnVideos
        '
        Me.btnVideos.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVideos.Image = CType(resources.GetObject("btnVideos.Image"), System.Drawing.Image)
        Me.btnVideos.Location = New System.Drawing.Point(689, 330)
        Me.btnVideos.Name = "btnVideos"
        Me.btnVideos.Size = New System.Drawing.Size(320, 90)
        Me.btnVideos.TabIndex = 21
        Me.btnVideos.Text = "Videos"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(12, 120)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(320, 90)
        Me.Button1.TabIndex = 37
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(351, 12)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(320, 90)
        Me.Button2.TabIndex = 38
        '
        'frmLDMain
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1024, 538)
        Me.ControlBox = False
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.btnEasy)
        Me.Controls.Add(Me.btnQuickObs)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.btnColours)
        Me.Controls.Add(Me.btnShapes)
        Me.Controls.Add(Me.btnLetters)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnNumbers)
        Me.Controls.Add(Me.btnFlashcards)
        Me.Controls.Add(Me.btnVideos)
        Me.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmLDMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Nursery Management"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnColours As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnShapes As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnLetters As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnExit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnFlashcards As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnVideos As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnNumbers As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Button7 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Button8 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnQuickObs As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEasy As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Button4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Button5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Button1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Button2 As DevExpress.XtraEditors.SimpleButton
End Class
