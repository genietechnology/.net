﻿Public Class frmSleep

    Private m_StartHours As String
    Private m_StartMinutes As String
    Private m_EndHours As String
    Private m_EndMinutes As String
    Private m_ButtonNumber As Integer

    Private Sub frmSleep_AcceptChanged(sender As Object, e As AcceptArgs) Handles Me.AcceptChanged
        btnAdd.Enabled = e.Enabled
    End Sub

    Private Sub frmSleep_ChildChanged(sender As Object, e As EventArgs) Handles Me.ChildChanged
        Clear()
    End Sub

    Private Sub Clear()
        ClearSleepButtons()
        ClearButtons()
        SetButtons(False)
    End Sub

    Private Sub ClearSleepButtons()

        m_ButtonNumber = 0

        btnSleep1.Enabled = False
        btnSleep1.Appearance.Reset()
        btnSleep1.Text = ""
        btnSleep2.Enabled = False
        btnSleep2.Appearance.Reset()
        btnSleep2.Text = ""
        btnSleep3.Enabled = False
        btnSleep3.Appearance.Reset()
        btnSleep3.Text = ""
        btnSleep4.Enabled = False
        btnSleep4.Appearance.Reset()
        btnSleep4.Text = ""
        btnSleep5.Enabled = False
        btnSleep5.Appearance.Reset()
        btnSleep5.Text = ""
        btnSleep6.Enabled = False
        btnSleep6.Appearance.Reset()
        btnSleep6.Text = ""

        btnRemoveSleep.Enabled = False

    End Sub

    Protected Overrides Sub DisplayRecord()

        ClearSleepButtons()

        Dim _SQL As String = "select * from Activity" & _
                             " where day_id = '" & Parameters.TodayID & "'" & _
                             " and key_id = '" & ChildID & "'" & _
                             " and type = 'SLEEP'" & _
                             " order by value_2"

        Dim _dt As DataTable = DAL.ReturnDataTable(_SQL)
        If _dt IsNot Nothing Then

            If _dt.Rows.Count > 0 Then

                btnRemoveSleep.Enabled = True

                Dim _i As Integer = 0
                For Each _dr As DataRow In _dt.Rows

                    _i += 1

                    If _i = 1 Then
                        btnSleep1.Enabled = True
                        btnSleep1.Text = _dr("value_1").ToString
                        m_ButtonNumber = 1
                        DisplayTimes(1)
                    End If

                    If _i = 2 Then
                        btnSleep2.Enabled = True
                        btnSleep2.Text = _dr("value_1").ToString
                        m_ButtonNumber = 2
                    End If

                    If _i = 3 Then
                        btnSleep3.Enabled = True
                        btnSleep3.Text = _dr("value_1").ToString
                        m_ButtonNumber = 3
                    End If

                    If _i = 4 Then
                        btnSleep4.Enabled = True
                        btnSleep4.Text = _dr("value_1").ToString
                        m_ButtonNumber = 4
                    End If

                    If _i = 5 Then
                        btnSleep5.Enabled = True
                        btnSleep5.Text = _dr("value_1").ToString
                        m_ButtonNumber = 5
                    End If

                    If _i = 6 Then
                        btnSleep6.Enabled = True
                        btnSleep6.Text = _dr("value_1").ToString
                        m_ButtonNumber = 6
                    End If

                Next

                SetButtons(True)

            Else
                ClearButtons()
                SetButtons(False)
            End If

            _dt.Dispose()
            _dt = Nothing

        End If


    End Sub

    Protected Overrides Sub CommitUpdate()

        SharedModule.DeleteSleep(Parameters.TodayID, txtChild.Tag.ToString)

        If btnSleep1.Text <> "" Then SharedModule.LogSleep(Parameters.TodayID, txtChild.Tag.ToString, txtChild.Text, btnSleep1.Text, txtStaff.Tag.ToString, txtStaff.Text)
        If btnSleep2.Text <> "" Then SharedModule.LogSleep(Parameters.TodayID, txtChild.Tag.ToString, txtChild.Text, btnSleep2.Text, txtStaff.Tag.ToString, txtStaff.Text)
        If btnSleep3.Text <> "" Then SharedModule.LogSleep(Parameters.TodayID, txtChild.Tag.ToString, txtChild.Text, btnSleep3.Text, txtStaff.Tag.ToString, txtStaff.Text)
        If btnSleep4.Text <> "" Then SharedModule.LogSleep(Parameters.TodayID, txtChild.Tag.ToString, txtChild.Text, btnSleep4.Text, txtStaff.Tag.ToString, txtStaff.Text)
        If btnSleep5.Text <> "" Then SharedModule.LogSleep(Parameters.TodayID, txtChild.Tag.ToString, txtChild.Text, btnSleep5.Text, txtStaff.Tag.ToString, txtStaff.Text)
        If btnSleep6.Text <> "" Then SharedModule.LogSleep(Parameters.TodayID, txtChild.Tag.ToString, txtChild.Text, btnSleep6.Text, txtStaff.Tag.ToString, txtStaff.Text)

    End Sub

    Private Sub DisplayTimes(ButtonNumber As Integer)

        btnSleep1.Appearance.Reset()
        btnSleep2.Appearance.Reset()
        btnSleep3.Appearance.Reset()
        btnSleep4.Appearance.Reset()
        btnSleep5.Appearance.Reset()
        btnSleep6.Appearance.Reset()

        m_ButtonNumber = ButtonNumber

        Dim _btn As DevExpress.XtraEditors.SimpleButton = Nothing
        If ButtonNumber = 1 Then _btn = btnSleep1
        If ButtonNumber = 2 Then _btn = btnSleep2
        If ButtonNumber = 3 Then _btn = btnSleep3
        If ButtonNumber = 4 Then _btn = btnSleep4
        If ButtonNumber = 5 Then _btn = btnSleep5
        If ButtonNumber = 6 Then _btn = btnSleep6

        _btn.Appearance.BackColor = txtYellow.BackColor
        _btn.Enabled = True

        Dim _Start As String = Mid(_btn.Text, 1, 5)
        Dim _End As String = Mid(_btn.Text, 7)

        ClearButtons()
        SetTime(_Start, True)
        SetTime(_End, False)

    End Sub

    Private Sub SetTime(Time As String, StartTime As Boolean)

        Dim _Hours As String = Mid(Time, 1, 2)
        Dim _Mins As String = Mid(Time, 4, 2)

        SetHours(_Hours, StartTime)
        SetMinutes(_Mins, StartTime)

        If StartTime Then
            m_StartHours = _Hours
            m_StartMinutes = _Mins
        Else
            m_EndHours = _Hours
            m_EndMinutes = _Mins
        End If

    End Sub

    Private Sub BuildTime()

        Dim _Time As String = m_StartHours + ":" + m_StartMinutes + "-" + m_EndHours + ":" + m_EndMinutes

        If m_ButtonNumber = 1 Then btnSleep1.Text = _Time
        If m_ButtonNumber = 2 Then btnSleep2.Text = _Time
        If m_ButtonNumber = 3 Then btnSleep3.Text = _Time
        If m_ButtonNumber = 4 Then btnSleep4.Text = _Time
        If m_ButtonNumber = 5 Then btnSleep5.Text = _Time
        If m_ButtonNumber = 6 Then btnSleep6.Text = _Time

    End Sub

    Private Sub SetButtons(ByVal Enabled As Boolean)
        SetStartButtons(Enabled)
        SetEndButtons(Enabled)
    End Sub

    Private Sub SetStartButtons(ByVal Enabled As Boolean)
        btnStartH8.Enabled = Enabled
        btnStartH9.Enabled = Enabled
        btnStartH10.Enabled = Enabled
        btnStartH11.Enabled = Enabled
        btnStartH12.Enabled = Enabled
        btnStartH13.Enabled = Enabled
        btnStartH14.Enabled = Enabled
        btnStartH15.Enabled = Enabled
        btnStartH16.Enabled = Enabled
        btnStartH17.Enabled = Enabled
        btnStartH18.Enabled = Enabled
        btnStartH19.Enabled = Enabled
        btnStartM0.Enabled = Enabled
        btnStartM5.Enabled = Enabled
        btnStartM10.Enabled = Enabled
        btnStartM15.Enabled = Enabled
        btnStartM20.Enabled = Enabled
        btnStartM25.Enabled = Enabled
        btnStartM30.Enabled = Enabled
        btnStartM35.Enabled = Enabled
        btnStartM40.Enabled = Enabled
        btnStartM45.Enabled = Enabled
        btnStartM50.Enabled = Enabled
        btnStartM55.Enabled = Enabled
    End Sub

    Private Sub SetEndButtons(ByVal Enabled As Boolean)
        btnEndH8.Enabled = Enabled
        btnEndH9.Enabled = Enabled
        btnEndH10.Enabled = Enabled
        btnEndH11.Enabled = Enabled
        btnEndH12.Enabled = Enabled
        btnEndH13.Enabled = Enabled
        btnEndH14.Enabled = Enabled
        btnEndH15.Enabled = Enabled
        btnEndH16.Enabled = Enabled
        btnEndH17.Enabled = Enabled
        btnEndH18.Enabled = Enabled
        btnEndH19.Enabled = Enabled
        btnEndM0.Enabled = Enabled
        btnEndM5.Enabled = Enabled
        btnEndM10.Enabled = Enabled
        btnEndM15.Enabled = Enabled
        btnEndM20.Enabled = Enabled
        btnEndM25.Enabled = Enabled
        btnEndM30.Enabled = Enabled
        btnEndM35.Enabled = Enabled
        btnEndM40.Enabled = Enabled
        btnEndM45.Enabled = Enabled
        btnEndM50.Enabled = Enabled
        btnEndM55.Enabled = Enabled
    End Sub

    Private Sub ClearStartHours()
        btnStartH8.Appearance.Reset()
        btnStartH9.Appearance.Reset()
        btnStartH10.Appearance.Reset()
        btnStartH11.Appearance.Reset()
        btnStartH12.Appearance.Reset()
        btnStartH13.Appearance.Reset()
        btnStartH14.Appearance.Reset()
        btnStartH15.Appearance.Reset()
        btnStartH16.Appearance.Reset()
        btnStartH17.Appearance.Reset()
        btnStartH18.Appearance.Reset()
        btnStartH19.Appearance.Reset()
    End Sub

    Private Sub ClearStartMinutes()
        btnStartM0.Appearance.Reset()
        btnStartM5.Appearance.Reset()
        btnStartM10.Appearance.Reset()
        btnStartM15.Appearance.Reset()
        btnStartM20.Appearance.Reset()
        btnStartM25.Appearance.Reset()
        btnStartM30.Appearance.Reset()
        btnStartM35.Appearance.Reset()
        btnStartM40.Appearance.Reset()
        btnStartM45.Appearance.Reset()
        btnStartM50.Appearance.Reset()
        btnStartM55.Appearance.Reset()
    End Sub

    Private Sub ClearEndHours()
        btnEndH8.Appearance.Reset()
        btnEndH9.Appearance.Reset()
        btnEndH10.Appearance.Reset()
        btnEndH11.Appearance.Reset()
        btnEndH12.Appearance.Reset()
        btnEndH13.Appearance.Reset()
        btnEndH14.Appearance.Reset()
        btnEndH15.Appearance.Reset()
        btnEndH16.Appearance.Reset()
        btnEndH17.Appearance.Reset()
        btnEndH18.Appearance.Reset()
        btnEndH19.Appearance.Reset()
    End Sub

    Private Sub ClearEndMinutes()
        btnEndM0.Appearance.Reset()
        btnEndM5.Appearance.Reset()
        btnEndM10.Appearance.Reset()
        btnEndM15.Appearance.Reset()
        btnEndM20.Appearance.Reset()
        btnEndM25.Appearance.Reset()
        btnEndM30.Appearance.Reset()
        btnEndM35.Appearance.Reset()
        btnEndM40.Appearance.Reset()
        btnEndM45.Appearance.Reset()
        btnEndM50.Appearance.Reset()
        btnEndM55.Appearance.Reset()
    End Sub

    Private Sub ClearButtons()
        ClearStartHours()
        ClearStartMinutes()
        ClearEndHours()
        ClearEndMinutes()
    End Sub

    Private Sub SetHours(Hours As String, StartTime As Boolean)

        If Hours = "08" Then
            If StartTime Then
                btnStartH8.Appearance.BackColor = txtYellow.BackColor
            Else
                btnEndH8.Appearance.BackColor = txtYellow.BackColor
            End If
        End If

        If Hours = "09" Then
            If StartTime Then
                btnStartH9.Appearance.BackColor = txtYellow.BackColor
            Else
                btnEndH9.Appearance.BackColor = txtYellow.BackColor
            End If
        End If

        If Hours = "10" Then
            If StartTime Then
                btnStartH10.Appearance.BackColor = txtYellow.BackColor
            Else
                btnEndH10.Appearance.BackColor = txtYellow.BackColor
            End If
        End If

        If Hours = "11" Then
            If StartTime Then
                btnStartH11.Appearance.BackColor = txtYellow.BackColor
            Else
                btnEndH11.Appearance.BackColor = txtYellow.BackColor
            End If
        End If

        If Hours = "12" Then
            If StartTime Then
                btnStartH12.Appearance.BackColor = txtYellow.BackColor
            Else
                btnEndH12.Appearance.BackColor = txtYellow.BackColor
            End If
        End If

        If Hours = "13" Then
            If StartTime Then
                btnStartH13.Appearance.BackColor = txtYellow.BackColor
            Else
                btnEndH13.Appearance.BackColor = txtYellow.BackColor
            End If
        End If

        If Hours = "14" Then
            If StartTime Then
                btnStartH14.Appearance.BackColor = txtYellow.BackColor
            Else
                btnEndH14.Appearance.BackColor = txtYellow.BackColor
            End If
        End If

        If Hours = "15" Then
            If StartTime Then
                btnStartH15.Appearance.BackColor = txtYellow.BackColor
            Else
                btnEndH15.Appearance.BackColor = txtYellow.BackColor
            End If
        End If

        If Hours = "16" Then
            If StartTime Then
                btnStartH16.Appearance.BackColor = txtYellow.BackColor
            Else
                btnEndH16.Appearance.BackColor = txtYellow.BackColor
            End If
        End If

        If Hours = "17" Then
            If StartTime Then
                btnStartH17.Appearance.BackColor = txtYellow.BackColor
            Else
                btnEndH17.Appearance.BackColor = txtYellow.BackColor
            End If
        End If

        If Hours = "18" Then
            If StartTime Then
                btnStartH18.Appearance.BackColor = txtYellow.BackColor
            Else
                btnEndH18.Appearance.BackColor = txtYellow.BackColor
            End If
        End If

        If Hours = "19" Then
            If StartTime Then
                btnStartH19.Appearance.BackColor = txtYellow.BackColor
            Else
                btnEndH19.Appearance.BackColor = txtYellow.BackColor
            End If
        End If

    End Sub

    Private Sub SetMinutes(Minutes As String, StartTime As Boolean)

        If Minutes = "00" Then
            If StartTime Then
                btnStartM0.Appearance.BackColor = txtYellow.BackColor
            Else
                btnEndM0.Appearance.BackColor = txtYellow.BackColor
            End If
        End If

        If Minutes = "05" Then
            If StartTime Then
                btnStartM5.Appearance.BackColor = txtYellow.BackColor
            Else
                btnEndM5.Appearance.BackColor = txtYellow.BackColor
            End If
        End If

        If Minutes = "10" Then
            If StartTime Then
                btnStartM10.Appearance.BackColor = txtYellow.BackColor
            Else
                btnEndM10.Appearance.BackColor = txtYellow.BackColor
            End If
        End If

        If Minutes = "15" Then
            If StartTime Then
                btnStartM15.Appearance.BackColor = txtYellow.BackColor
            Else
                btnEndM15.Appearance.BackColor = txtYellow.BackColor
            End If
        End If

        If Minutes = "20" Then
            If StartTime Then
                btnStartM20.Appearance.BackColor = txtYellow.BackColor
            Else
                btnEndM20.Appearance.BackColor = txtYellow.BackColor
            End If
        End If

        If Minutes = "25" Then
            If StartTime Then
                btnStartM25.Appearance.BackColor = txtYellow.BackColor
            Else
                btnEndM25.Appearance.BackColor = txtYellow.BackColor
            End If
        End If

        If Minutes = "30" Then
            If StartTime Then
                btnStartM30.Appearance.BackColor = txtYellow.BackColor
            Else
                btnEndM30.Appearance.BackColor = txtYellow.BackColor
            End If
        End If

        If Minutes = "35" Then
            If StartTime Then
                btnStartM35.Appearance.BackColor = txtYellow.BackColor
            Else
                btnEndM35.Appearance.BackColor = txtYellow.BackColor
            End If
        End If

        If Minutes = "40" Then
            If StartTime Then
                btnStartM40.Appearance.BackColor = txtYellow.BackColor
            Else
                btnEndM40.Appearance.BackColor = txtYellow.BackColor
            End If
        End If

        If Minutes = "45" Then
            If StartTime Then
                btnStartM45.Appearance.BackColor = txtYellow.BackColor
            Else
                btnEndM45.Appearance.BackColor = txtYellow.BackColor
            End If
        End If

        If Minutes = "50" Then
            If StartTime Then
                btnStartM50.Appearance.BackColor = txtYellow.BackColor
            Else
                btnEndM50.Appearance.BackColor = txtYellow.BackColor
            End If
        End If

        If Minutes = "55" Then
            If StartTime Then
                btnStartM55.Appearance.BackColor = txtYellow.BackColor
            Else
                btnEndM55.Appearance.BackColor = txtYellow.BackColor
            End If
        End If

    End Sub

#Region "Button Clicks"

    Private Sub btnStartH9_Click(sender As System.Object, e As System.EventArgs) Handles btnStartH9.Click
        ClearStartHours()
        btnStartH9.Appearance.BackColor = txtYellow.BackColor
        m_StartHours = "09"
        BuildTime()
    End Sub

    Private Sub btnStartH10_Click(sender As System.Object, e As System.EventArgs) Handles btnStartH10.Click
        ClearStartHours()
        btnStartH10.Appearance.BackColor = txtYellow.BackColor
        m_StartHours = "10"
        BuildTime()
    End Sub

    Private Sub btnStartH11_Click(sender As System.Object, e As System.EventArgs) Handles btnStartH11.Click
        ClearStartHours()
        btnStartH11.Appearance.BackColor = txtYellow.BackColor
        m_StartHours = "11"
        BuildTime()
    End Sub

    Private Sub btnStartH12_Click(sender As System.Object, e As System.EventArgs) Handles btnStartH12.Click
        ClearStartHours()
        btnStartH12.Appearance.BackColor = txtYellow.BackColor
        m_StartHours = "12"
        BuildTime()
    End Sub

    Private Sub btnStartH13_Click(sender As System.Object, e As System.EventArgs) Handles btnStartH13.Click
        ClearStartHours()
        btnStartH13.Appearance.BackColor = txtYellow.BackColor
        m_StartHours = "13"
        BuildTime()
    End Sub

    Private Sub btnStartH14_Click(sender As System.Object, e As System.EventArgs) Handles btnStartH14.Click
        ClearStartHours()
        btnStartH14.Appearance.BackColor = txtYellow.BackColor
        m_StartHours = "14"
        BuildTime()
    End Sub

    Private Sub btnStartH15_Click(sender As System.Object, e As System.EventArgs) Handles btnStartH15.Click
        ClearStartHours()
        btnStartH15.Appearance.BackColor = txtYellow.BackColor
        m_StartHours = "15"
        BuildTime()
    End Sub

    Private Sub btnStartH16_Click(sender As System.Object, e As System.EventArgs) Handles btnStartH16.Click
        ClearStartHours()
        btnStartH16.Appearance.BackColor = txtYellow.BackColor
        m_StartHours = "16"
        BuildTime()
    End Sub

    Private Sub btnStartM0_Click(sender As System.Object, e As System.EventArgs) Handles btnStartM0.Click
        ClearStartMinutes()
        btnStartM0.Appearance.BackColor = txtYellow.BackColor
        m_StartMinutes = "00"
        BuildTime()
    End Sub

    Private Sub btnStartM5_Click(sender As System.Object, e As System.EventArgs) Handles btnStartM5.Click
        ClearStartMinutes()
        btnStartM5.Appearance.BackColor = txtYellow.BackColor
        m_StartMinutes = "05"
        BuildTime()
    End Sub

    Private Sub btnStartM10_Click(sender As System.Object, e As System.EventArgs) Handles btnStartM10.Click
        ClearStartMinutes()
        btnStartM10.Appearance.BackColor = txtYellow.BackColor
        m_StartMinutes = "10"
        BuildTime()
    End Sub

    Private Sub btnStartM15_Click(sender As System.Object, e As System.EventArgs) Handles btnStartM15.Click
        ClearStartMinutes()
        btnStartM15.Appearance.BackColor = txtYellow.BackColor
        m_StartMinutes = "15"
        BuildTime()
    End Sub

    Private Sub btnStartM20_Click(sender As System.Object, e As System.EventArgs) Handles btnStartM20.Click
        ClearStartMinutes()
        btnStartM20.Appearance.BackColor = txtYellow.BackColor
        m_StartMinutes = "20"
        BuildTime()
    End Sub

    Private Sub btnStartM25_Click(sender As System.Object, e As System.EventArgs) Handles btnStartM25.Click
        ClearStartMinutes()
        btnStartM25.Appearance.BackColor = txtYellow.BackColor
        m_StartMinutes = "25"
        BuildTime()
    End Sub

    Private Sub btnStartM30_Click(sender As System.Object, e As System.EventArgs) Handles btnStartM30.Click
        ClearStartMinutes()
        btnStartM30.Appearance.BackColor = txtYellow.BackColor
        m_StartMinutes = "30"
        BuildTime()
    End Sub

    Private Sub btnStartM35_Click(sender As System.Object, e As System.EventArgs) Handles btnStartM35.Click
        ClearStartMinutes()
        btnStartM35.Appearance.BackColor = txtYellow.BackColor
        m_StartMinutes = "35"
        BuildTime()
    End Sub

    Private Sub btnStartM40_Click(sender As System.Object, e As System.EventArgs) Handles btnStartM40.Click
        ClearStartMinutes()
        btnStartM40.Appearance.BackColor = txtYellow.BackColor
        m_StartMinutes = "40"
        BuildTime()
    End Sub

    Private Sub btnStartM45_Click(sender As System.Object, e As System.EventArgs) Handles btnStartM45.Click
        ClearStartMinutes()
        btnStartM45.Appearance.BackColor = txtYellow.BackColor
        m_StartMinutes = "45"
        BuildTime()
    End Sub

    Private Sub btnStartM50_Click(sender As System.Object, e As System.EventArgs) Handles btnStartM50.Click
        ClearStartMinutes()
        btnStartM50.Appearance.BackColor = txtYellow.BackColor
        m_StartMinutes = "50"
        BuildTime()
    End Sub

    Private Sub btnStartM55_Click(sender As System.Object, e As System.EventArgs) Handles btnStartM55.Click
        ClearStartMinutes()
        btnStartM55.Appearance.BackColor = txtYellow.BackColor
        m_StartMinutes = "55"
        BuildTime()
    End Sub

    Private Sub btnEndH9_Click(sender As System.Object, e As System.EventArgs) Handles btnEndH9.Click
        ClearEndHours()
        btnEndH9.Appearance.BackColor = txtYellow.BackColor
        m_EndHours = "09"
        BuildTime()
    End Sub

    Private Sub btnEndH10_Click(sender As System.Object, e As System.EventArgs) Handles btnEndH10.Click
        ClearEndHours()
        btnEndH10.Appearance.BackColor = txtYellow.BackColor
        m_EndHours = "10"
        BuildTime()
    End Sub

    Private Sub btnEndH11_Click(sender As System.Object, e As System.EventArgs) Handles btnEndH11.Click
        ClearEndHours()
        btnEndH11.Appearance.BackColor = txtYellow.BackColor
        m_EndHours = "11"
        BuildTime()
    End Sub

    Private Sub btnEndH12_Click(sender As System.Object, e As System.EventArgs) Handles btnEndH12.Click
        ClearEndHours()
        btnEndH12.Appearance.BackColor = txtYellow.BackColor
        m_EndHours = "12"
        BuildTime()
    End Sub

    Private Sub btnEndH13_Click(sender As System.Object, e As System.EventArgs) Handles btnEndH13.Click
        ClearEndHours()
        btnEndH13.Appearance.BackColor = txtYellow.BackColor
        m_EndHours = "13"
        BuildTime()
    End Sub

    Private Sub btnEndH14_Click(sender As System.Object, e As System.EventArgs) Handles btnEndH14.Click
        ClearEndHours()
        btnEndH14.Appearance.BackColor = txtYellow.BackColor
        m_EndHours = "14"
        BuildTime()
    End Sub

    Private Sub btnEndH15_Click(sender As System.Object, e As System.EventArgs) Handles btnEndH15.Click
        ClearEndHours()
        btnEndH15.Appearance.BackColor = txtYellow.BackColor
        m_EndHours = "15"
        BuildTime()
    End Sub

    Private Sub btnEndH16_Click(sender As System.Object, e As System.EventArgs) Handles btnEndH16.Click
        ClearEndHours()
        btnEndH16.Appearance.BackColor = txtYellow.BackColor
        m_EndHours = "16"
        BuildTime()
    End Sub

    Private Sub btnEndM0_Click(sender As System.Object, e As System.EventArgs) Handles btnEndM0.Click
        ClearEndMinutes()
        btnEndM0.Appearance.BackColor = txtYellow.BackColor
        m_EndMinutes = "00"
        BuildTime()
    End Sub

    Private Sub btnEndM5_Click(sender As System.Object, e As System.EventArgs) Handles btnEndM5.Click
        ClearEndMinutes()
        btnEndM5.Appearance.BackColor = txtYellow.BackColor
        m_EndMinutes = "05"
        BuildTime()
    End Sub

    Private Sub btnEndM10_Click(sender As System.Object, e As System.EventArgs) Handles btnEndM10.Click
        ClearEndMinutes()
        btnEndM10.Appearance.BackColor = txtYellow.BackColor
        m_EndMinutes = "10"
        BuildTime()
    End Sub

    Private Sub btnEndM15_Click(sender As System.Object, e As System.EventArgs) Handles btnEndM15.Click
        ClearEndMinutes()
        btnEndM15.Appearance.BackColor = txtYellow.BackColor
        m_EndMinutes = "15"
        BuildTime()
    End Sub

    Private Sub btnEndM20_Click(sender As System.Object, e As System.EventArgs) Handles btnEndM20.Click
        ClearEndMinutes()
        btnEndM20.Appearance.BackColor = txtYellow.BackColor
        m_EndMinutes = "20"
        BuildTime()
    End Sub

    Private Sub btnEndM25_Click(sender As System.Object, e As System.EventArgs) Handles btnEndM25.Click
        ClearEndMinutes()
        btnEndM25.Appearance.BackColor = txtYellow.BackColor
        m_EndMinutes = "25"
        BuildTime()
    End Sub

    Private Sub btnEndM30_Click(sender As System.Object, e As System.EventArgs) Handles btnEndM30.Click
        ClearEndMinutes()
        btnEndM30.Appearance.BackColor = txtYellow.BackColor
        m_EndMinutes = "30"
        BuildTime()
    End Sub

    Private Sub btnEndM35_Click(sender As System.Object, e As System.EventArgs) Handles btnEndM35.Click
        ClearEndMinutes()
        btnEndM35.Appearance.BackColor = txtYellow.BackColor
        m_EndMinutes = "35"
        BuildTime()
    End Sub

    Private Sub btnEndM40_Click(sender As System.Object, e As System.EventArgs) Handles btnEndM40.Click
        ClearEndMinutes()
        btnEndM40.Appearance.BackColor = txtYellow.BackColor
        m_EndMinutes = "40"
        BuildTime()
    End Sub

    Private Sub btnEndM45_Click(sender As System.Object, e As System.EventArgs) Handles btnEndM45.Click
        ClearEndMinutes()
        btnEndM45.Appearance.BackColor = txtYellow.BackColor
        m_EndMinutes = "45"
        BuildTime()
    End Sub

    Private Sub btnEndM50_Click(sender As System.Object, e As System.EventArgs) Handles btnEndM50.Click
        ClearEndMinutes()
        btnEndM50.Appearance.BackColor = txtYellow.BackColor
        m_EndMinutes = "50"
        BuildTime()
    End Sub

    Private Sub btnEndM55_Click(sender As System.Object, e As System.EventArgs) Handles btnEndM55.Click
        ClearEndMinutes()
        btnEndM55.Appearance.BackColor = txtYellow.BackColor
        m_EndMinutes = "55"
        BuildTime()
    End Sub

    Private Sub btnStartH8_Click(sender As Object, e As EventArgs) Handles btnStartH8.Click
        ClearStartHours()
        btnStartH8.Appearance.BackColor = txtYellow.BackColor
        m_StartHours = "08"
        BuildTime()
    End Sub

    Private Sub btnStartH17_Click(sender As Object, e As EventArgs) Handles btnStartH17.Click
        ClearStartHours()
        btnStartH17.Appearance.BackColor = txtYellow.BackColor
        m_StartHours = "17"
        BuildTime()
    End Sub

    Private Sub btnStartH18_Click(sender As Object, e As EventArgs) Handles btnStartH18.Click
        ClearStartHours()
        btnStartH18.Appearance.BackColor = txtYellow.BackColor
        m_StartHours = "18"
        BuildTime()
    End Sub

    Private Sub btnStartH19_Click(sender As Object, e As EventArgs) Handles btnStartH19.Click
        ClearStartHours()
        btnStartH19.Appearance.BackColor = txtYellow.BackColor
        m_StartHours = "19"
        BuildTime()
    End Sub

    Private Sub btnEndH8_Click(sender As Object, e As EventArgs) Handles btnEndH8.Click
        ClearEndHours()
        btnEndH8.Appearance.BackColor = txtYellow.BackColor
        m_EndHours = "08"
        BuildTime()
    End Sub

    Private Sub btnEndH17_Click(sender As Object, e As EventArgs) Handles btnEndH17.Click
        ClearEndHours()
        btnEndH17.Appearance.BackColor = txtYellow.BackColor
        m_EndHours = "17"
        BuildTime()
    End Sub

    Private Sub btnEndH18_Click(sender As Object, e As EventArgs) Handles btnEndH18.Click
        ClearEndHours()
        btnEndH18.Appearance.BackColor = txtYellow.BackColor
        m_EndHours = "18"
        BuildTime()
    End Sub

    Private Sub btnEndH19_Click(sender As Object, e As EventArgs) Handles btnEndH19.Click
        ClearEndHours()
        btnEndH19.Appearance.BackColor = txtYellow.BackColor
        m_EndHours = "19"
        BuildTime()
    End Sub

#End Region

    Private Sub btnRemoveSleep_Click(sender As System.Object, e As System.EventArgs) Handles btnRemoveSleep.Click

        If Msgbox("Are you sure you want to delete this sleep record?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Confirm Delete") = MsgBoxResult.Yes Then

            Dim _btn As DevExpress.XtraEditors.SimpleButton = Nothing
            If m_ButtonNumber = 1 Then _btn = btnSleep1
            If m_ButtonNumber = 2 Then _btn = btnSleep2
            If m_ButtonNumber = 3 Then _btn = btnSleep3
            If m_ButtonNumber = 4 Then _btn = btnSleep4
            If m_ButtonNumber = 5 Then _btn = btnSleep5
            If m_ButtonNumber = 6 Then _btn = btnSleep6

            _btn.Text = ""
            _btn.Appearance.Reset()
            _btn.Enabled = False

            m_StartHours = ""
            m_StartMinutes = ""
            m_EndHours = ""
            m_EndMinutes = ""

            m_ButtonNumber -= 1

            CommitUpdate()
            DisplayRecord()

        End If

    End Sub

    Private Sub btnAdd_Click(sender As System.Object, e As System.EventArgs) Handles btnAdd.Click

        If m_ButtonNumber = 6 Then
            MsgBox("You cannot add more than 6 sleep records per day.", MessageBoxIcon.Exclamation, "Maximum Sleep Records")
            Exit Sub
        End If

        btnRemoveSleep.Enabled = True

        m_ButtonNumber += 1

        Dim _btn As DevExpress.XtraEditors.SimpleButton = Nothing
        If m_ButtonNumber = 1 Then _btn = btnSleep1
        If m_ButtonNumber = 2 Then _btn = btnSleep2
        If m_ButtonNumber = 3 Then _btn = btnSleep3
        If m_ButtonNumber = 4 Then _btn = btnSleep4
        If m_ButtonNumber = 5 Then _btn = btnSleep5
        If m_ButtonNumber = 6 Then _btn = btnSleep6

        _btn.Text = "09:00-10:00"
        DisplayTimes(m_ButtonNumber)
        SetButtons(True)

    End Sub

    Private Sub btnSleep1_Click(sender As System.Object, e As System.EventArgs) Handles btnSleep1.Click
        DisplayTimes(1)
    End Sub

    Private Sub btnSleep2_Click(sender As System.Object, e As System.EventArgs) Handles btnSleep2.Click
        DisplayTimes(2)
    End Sub

    Private Sub btnSleep3_Click(sender As System.Object, e As System.EventArgs) Handles btnSleep3.Click
        DisplayTimes(3)
    End Sub

    Private Sub btnSleep4_Click(sender As System.Object, e As System.EventArgs) Handles btnSleep4.Click
        DisplayTimes(4)
    End Sub

    Private Sub btnSleep5_Click(sender As System.Object, e As System.EventArgs) Handles btnSleep5.Click
        DisplayTimes(5)
    End Sub

    Private Sub btnSleep6_Click(sender As System.Object, e As System.EventArgs) Handles btnSleep6.Click
        DisplayTimes(6)
    End Sub

End Class
