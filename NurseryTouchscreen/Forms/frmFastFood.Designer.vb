﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFastFood
    Inherits frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFastFood))
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.tbRooms = New DevExpress.XtraBars.Navigation.TileBar()
        Me.tbRoomGroup = New DevExpress.XtraBars.Navigation.TileBarGroup()
        Me.timSleepCheck = New System.Windows.Forms.Timer(Me.components)
        Me.sc = New DevExpress.XtraEditors.XtraScrollableControl()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAccept = New DevExpress.XtraEditors.SimpleButton()
        Me.scTop = New DevExpress.XtraEditors.XtraScrollableControl()
        Me.lblMealDetails = New DevExpress.XtraEditors.LabelControl()
        Me.btnBreakfast = New DevExpress.XtraEditors.SimpleButton()
        Me.btnTeaDessert = New DevExpress.XtraEditors.SimpleButton()
        Me.btnLunchDessert = New DevExpress.XtraEditors.SimpleButton()
        Me.btnTea = New DevExpress.XtraEditors.SimpleButton()
        Me.btnLunch = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAM = New DevExpress.XtraEditors.SimpleButton()
        Me.btnPM = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        Me.scTop.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupControl2.Controls.Add(Me.tbRooms)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.ShowCaption = False
        Me.GroupControl2.Size = New System.Drawing.Size(200, 437)
        Me.GroupControl2.TabIndex = 87
        Me.GroupControl2.Text = "GroupControl2"
        '
        'tbRooms
        '
        Me.tbRooms.AllowDrag = False
        Me.tbRooms.AllowItemHover = False
        Me.tbRooms.AllowSelectedItem = True
        Me.tbRooms.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbRooms.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        Me.tbRooms.Groups.Add(Me.tbRoomGroup)
        Me.tbRooms.ItemSize = 80
        Me.tbRooms.Location = New System.Drawing.Point(2, 2)
        Me.tbRooms.Name = "tbRooms"
        Me.tbRooms.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.tbRooms.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollButtons
        Me.tbRooms.Size = New System.Drawing.Size(196, 433)
        Me.tbRooms.TabIndex = 88
        Me.tbRooms.VerticalContentAlignment = DevExpress.Utils.VertAlignment.Top
        '
        'tbRoomGroup
        '
        Me.tbRoomGroup.Name = "tbRoomGroup"
        '
        'sc
        '
        Me.sc.AllowTouchScroll = True
        Me.sc.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.sc.Location = New System.Drawing.Point(218, 99)
        Me.sc.Name = "sc"
        Me.sc.Size = New System.Drawing.Size(949, 299)
        Me.sc.TabIndex = 88
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Image = CType(resources.GetObject("btnCancel.Image"), System.Drawing.Image)
        Me.btnCancel.Location = New System.Drawing.Point(1022, 405)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(145, 44)
        Me.btnCancel.TabIndex = 90
        Me.btnCancel.Text = "Cancel"
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAccept.Image = Global.NurseryTouchscreen.My.Resources.Resources.success_32
        Me.btnAccept.Location = New System.Drawing.Point(871, 405)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(145, 44)
        Me.btnAccept.TabIndex = 89
        Me.btnAccept.Text = "Accept"
        '
        'scTop
        '
        Me.scTop.AllowTouchScroll = True
        Me.scTop.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.scTop.Controls.Add(Me.lblMealDetails)
        Me.scTop.Controls.Add(Me.btnBreakfast)
        Me.scTop.Controls.Add(Me.btnTeaDessert)
        Me.scTop.Controls.Add(Me.btnLunchDessert)
        Me.scTop.Controls.Add(Me.btnTea)
        Me.scTop.Controls.Add(Me.btnLunch)
        Me.scTop.Controls.Add(Me.btnAM)
        Me.scTop.Controls.Add(Me.btnPM)
        Me.scTop.Location = New System.Drawing.Point(216, 12)
        Me.scTop.Name = "scTop"
        Me.scTop.Size = New System.Drawing.Size(951, 85)
        Me.scTop.TabIndex = 91
        '
        'lblMealDetails
        '
        Me.lblMealDetails.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMealDetails.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMealDetails.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblMealDetails.Location = New System.Drawing.Point(6, 45)
        Me.lblMealDetails.Name = "lblMealDetails"
        Me.lblMealDetails.Size = New System.Drawing.Size(942, 36)
        Me.lblMealDetails.TabIndex = 7
        Me.lblMealDetails.Text = "<Selected Meal Details>"
        '
        'btnBreakfast
        '
        Me.btnBreakfast.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnBreakfast.Location = New System.Drawing.Point(3, 3)
        Me.btnBreakfast.Name = "btnBreakfast"
        Me.btnBreakfast.Size = New System.Drawing.Size(130, 36)
        Me.btnBreakfast.TabIndex = 0
        Me.btnBreakfast.Text = "Breakfast"
        '
        'btnTeaDessert
        '
        Me.btnTeaDessert.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnTeaDessert.Location = New System.Drawing.Point(819, 3)
        Me.btnTeaDessert.Name = "btnTeaDessert"
        Me.btnTeaDessert.Size = New System.Drawing.Size(130, 36)
        Me.btnTeaDessert.TabIndex = 6
        Me.btnTeaDessert.Text = "Tea Dessert"
        '
        'btnLunchDessert
        '
        Me.btnLunchDessert.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnLunchDessert.Location = New System.Drawing.Point(411, 3)
        Me.btnLunchDessert.Name = "btnLunchDessert"
        Me.btnLunchDessert.Size = New System.Drawing.Size(130, 36)
        Me.btnLunchDessert.TabIndex = 3
        Me.btnLunchDessert.Text = "Lunch Dessert"
        '
        'btnTea
        '
        Me.btnTea.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnTea.Location = New System.Drawing.Point(683, 3)
        Me.btnTea.Name = "btnTea"
        Me.btnTea.Size = New System.Drawing.Size(130, 36)
        Me.btnTea.TabIndex = 5
        Me.btnTea.Text = "Tea"
        '
        'btnLunch
        '
        Me.btnLunch.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnLunch.Location = New System.Drawing.Point(275, 3)
        Me.btnLunch.Name = "btnLunch"
        Me.btnLunch.Size = New System.Drawing.Size(130, 36)
        Me.btnLunch.TabIndex = 2
        Me.btnLunch.Text = "Lunch"
        '
        'btnAM
        '
        Me.btnAM.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnAM.Location = New System.Drawing.Point(139, 3)
        Me.btnAM.Name = "btnAM"
        Me.btnAM.Size = New System.Drawing.Size(130, 36)
        Me.btnAM.TabIndex = 1
        Me.btnAM.Text = "AM Snack"
        '
        'btnPM
        '
        Me.btnPM.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnPM.Location = New System.Drawing.Point(547, 3)
        Me.btnPM.Name = "btnPM"
        Me.btnPM.Size = New System.Drawing.Size(130, 36)
        Me.btnPM.TabIndex = 4
        Me.btnPM.Text = "PM Snack"
        '
        'frmFastFood
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1179, 461)
        Me.Controls.Add(Me.scTop)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnAccept)
        Me.Controls.Add(Me.sc)
        Me.Controls.Add(Me.GroupControl2)
        Me.Name = "frmFastFood"
        Me.Text = "Food Entry"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Controls.SetChildIndex(Me.GroupControl2, 0)
        Me.Controls.SetChildIndex(Me.sc, 0)
        Me.Controls.SetChildIndex(Me.btnAccept, 0)
        Me.Controls.SetChildIndex(Me.btnCancel, 0)
        Me.Controls.SetChildIndex(Me.scTop, 0)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.scTop.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents tbRooms As DevExpress.XtraBars.Navigation.TileBar
    Friend WithEvents tbRoomGroup As DevExpress.XtraBars.Navigation.TileBarGroup
    Friend WithEvents timSleepCheck As System.Windows.Forms.Timer
    Friend WithEvents sc As DevExpress.XtraEditors.XtraScrollableControl
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAccept As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents scTop As DevExpress.XtraEditors.XtraScrollableControl
    Friend WithEvents btnBreakfast As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnTeaDessert As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnLunchDessert As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnTea As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnLunch As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAM As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnPM As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblMealDetails As DevExpress.XtraEditors.LabelControl
End Class
