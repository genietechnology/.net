﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPhoto
    Inherits frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnMediaDelete = New DevExpress.XtraEditors.SimpleButton()
        Me.btnPhotoL = New DevExpress.XtraEditors.SimpleButton()
        Me.btnPhotoR = New DevExpress.XtraEditors.SimpleButton()
        Me.btnClose = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSwap = New DevExpress.XtraEditors.SimpleButton()
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.cam = New DevExpress.XtraEditors.Camera.CameraControl()
        Me.iSlider = New DevExpress.XtraEditors.Controls.ImageSlider()
        Me.btnSave = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.iSlider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'btnMediaDelete
        '
        Me.btnMediaDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMediaDelete.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMediaDelete.Appearance.Options.UseFont = True
        Me.btnMediaDelete.Image = Global.NurseryTouchscreen.My.Resources.Resources.delete_32
        Me.btnMediaDelete.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnMediaDelete.Location = New System.Drawing.Point(873, 12)
        Me.btnMediaDelete.Name = "btnMediaDelete"
        Me.btnMediaDelete.Size = New System.Drawing.Size(45, 45)
        Me.btnMediaDelete.TabIndex = 107
        '
        'btnPhotoL
        '
        Me.btnPhotoL.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.btnPhotoL.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPhotoL.Appearance.Options.UseFont = True
        Me.btnPhotoL.Image = Global.NurseryTouchscreen.My.Resources.Resources.camera_64
        Me.btnPhotoL.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnPhotoL.Location = New System.Drawing.Point(12, 182)
        Me.btnPhotoL.Name = "btnPhotoL"
        Me.btnPhotoL.Size = New System.Drawing.Size(90, 90)
        Me.btnPhotoL.TabIndex = 106
        '
        'btnPhotoR
        '
        Me.btnPhotoR.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btnPhotoR.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPhotoR.Appearance.Options.UseFont = True
        Me.btnPhotoR.Image = Global.NurseryTouchscreen.My.Resources.Resources.camera_64
        Me.btnPhotoR.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnPhotoR.Location = New System.Drawing.Point(873, 182)
        Me.btnPhotoR.Name = "btnPhotoR"
        Me.btnPhotoR.Size = New System.Drawing.Size(90, 90)
        Me.btnPhotoR.TabIndex = 110
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnClose.Location = New System.Drawing.Point(873, 400)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(90, 45)
        Me.btnClose.TabIndex = 111
        Me.btnClose.Text = "Exit"
        '
        'btnSwap
        '
        Me.btnSwap.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSwap.Appearance.Options.UseFont = True
        Me.btnSwap.Image = Global.NurseryTouchscreen.My.Resources.Resources.swap_32
        Me.btnSwap.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnSwap.Location = New System.Drawing.Point(57, 12)
        Me.btnSwap.Name = "btnSwap"
        Me.btnSwap.Size = New System.Drawing.Size(45, 45)
        Me.btnSwap.TabIndex = 112
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainerControl1.Location = New System.Drawing.Point(108, 12)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.cam)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.iSlider)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(759, 433)
        Me.SplitContainerControl1.SplitterPosition = 387
        Me.SplitContainerControl1.TabIndex = 114
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'cam
        '
        Me.cam.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cam.Location = New System.Drawing.Point(0, 0)
        Me.cam.Name = "cam"
        Me.cam.ShowSettingsButton = False
        Me.cam.Size = New System.Drawing.Size(387, 433)
        Me.cam.TabIndex = 1
        '
        'iSlider
        '
        Me.iSlider.CurrentImageIndex = -1
        Me.iSlider.Dock = System.Windows.Forms.DockStyle.Fill
        Me.iSlider.LayoutMode = DevExpress.Utils.Drawing.ImageLayoutMode.ZoomInside
        Me.iSlider.Location = New System.Drawing.Point(0, 0)
        Me.iSlider.Name = "iSlider"
        Me.iSlider.Size = New System.Drawing.Size(367, 433)
        Me.iSlider.TabIndex = 91
        Me.iSlider.Text = "ImageSlider1"
        Me.iSlider.UseDisabledStatePainter = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Appearance.Options.UseFont = True
        Me.btnSave.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnSave.Location = New System.Drawing.Point(873, 349)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(90, 45)
        Me.btnSave.TabIndex = 115
        Me.btnSave.Text = "Save"
        '
        'frmPhoto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(975, 455)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Controls.Add(Me.btnSwap)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnPhotoR)
        Me.Controls.Add(Me.btnMediaDelete)
        Me.Controls.Add(Me.btnPhotoL)
        Me.Name = "frmPhoto"
        Me.Text = "Take a Photo"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Controls.SetChildIndex(Me.btnPhotoL, 0)
        Me.Controls.SetChildIndex(Me.btnMediaDelete, 0)
        Me.Controls.SetChildIndex(Me.btnPhotoR, 0)
        Me.Controls.SetChildIndex(Me.btnClose, 0)
        Me.Controls.SetChildIndex(Me.btnSwap, 0)
        Me.Controls.SetChildIndex(Me.SplitContainerControl1, 0)
        Me.Controls.SetChildIndex(Me.btnSave, 0)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.iSlider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnMediaDelete As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnPhotoL As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnPhotoR As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSwap As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents cam As DevExpress.XtraEditors.Camera.CameraControl
    Friend WithEvents btnSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents iSlider As DevExpress.XtraEditors.Controls.ImageSlider
End Class
