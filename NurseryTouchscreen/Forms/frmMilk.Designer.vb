﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMilk
    Inherits NurseryTouchscreen.frmBaseChild

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMilk))
        Me.gbxMilk = New System.Windows.Forms.GroupBox()
        Me.lblMilk12 = New System.Windows.Forms.Label()
        Me.lblMilk11 = New System.Windows.Forms.Label()
        Me.btnMilk7 = New DevExpress.XtraEditors.SimpleButton()
        Me.lblMilk10 = New System.Windows.Forms.Label()
        Me.btnMilk8 = New DevExpress.XtraEditors.SimpleButton()
        Me.lblMilk9 = New System.Windows.Forms.Label()
        Me.btnMilk9 = New DevExpress.XtraEditors.SimpleButton()
        Me.lblMilk8 = New System.Windows.Forms.Label()
        Me.btnMilk10 = New DevExpress.XtraEditors.SimpleButton()
        Me.lblMilk7 = New System.Windows.Forms.Label()
        Me.btnMilk11 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMilk12 = New DevExpress.XtraEditors.SimpleButton()
        Me.lblMilk6 = New System.Windows.Forms.Label()
        Me.lblMilk5 = New System.Windows.Forms.Label()
        Me.lblMilk4 = New System.Windows.Forms.Label()
        Me.lblMilk3 = New System.Windows.Forms.Label()
        Me.lblMilk2 = New System.Windows.Forms.Label()
        Me.lblMilk1 = New System.Windows.Forms.Label()
        Me.btnMilk6 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMilk5 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMilk4 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMilk3 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMilk2 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAdd = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMilk1 = New DevExpress.XtraEditors.SimpleButton()
        Me.gbxMilk.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxMilk
        '
        Me.gbxMilk.Controls.Add(Me.lblMilk12)
        Me.gbxMilk.Controls.Add(Me.lblMilk11)
        Me.gbxMilk.Controls.Add(Me.btnMilk7)
        Me.gbxMilk.Controls.Add(Me.lblMilk10)
        Me.gbxMilk.Controls.Add(Me.btnMilk8)
        Me.gbxMilk.Controls.Add(Me.lblMilk9)
        Me.gbxMilk.Controls.Add(Me.btnMilk9)
        Me.gbxMilk.Controls.Add(Me.lblMilk8)
        Me.gbxMilk.Controls.Add(Me.btnMilk10)
        Me.gbxMilk.Controls.Add(Me.lblMilk7)
        Me.gbxMilk.Controls.Add(Me.btnMilk11)
        Me.gbxMilk.Controls.Add(Me.btnMilk12)
        Me.gbxMilk.Controls.Add(Me.lblMilk6)
        Me.gbxMilk.Controls.Add(Me.lblMilk5)
        Me.gbxMilk.Controls.Add(Me.lblMilk4)
        Me.gbxMilk.Controls.Add(Me.lblMilk3)
        Me.gbxMilk.Controls.Add(Me.lblMilk2)
        Me.gbxMilk.Controls.Add(Me.lblMilk1)
        Me.gbxMilk.Controls.Add(Me.btnMilk6)
        Me.gbxMilk.Controls.Add(Me.btnMilk5)
        Me.gbxMilk.Controls.Add(Me.btnMilk4)
        Me.gbxMilk.Controls.Add(Me.btnMilk3)
        Me.gbxMilk.Controls.Add(Me.btnMilk2)
        Me.gbxMilk.Controls.Add(Me.btnAdd)
        Me.gbxMilk.Controls.Add(Me.btnMilk1)
        Me.gbxMilk.Location = New System.Drawing.Point(17, 90)
        Me.gbxMilk.Name = "gbxMilk"
        Me.gbxMilk.Size = New System.Drawing.Size(995, 382)
        Me.gbxMilk.TabIndex = 10
        Me.gbxMilk.TabStop = False
        '
        'lblMilk12
        '
        Me.lblMilk12.AutoSize = True
        Me.lblMilk12.Location = New System.Drawing.Point(511, 287)
        Me.lblMilk12.Name = "lblMilk12"
        Me.lblMilk12.Size = New System.Drawing.Size(327, 25)
        Me.lblMilk12.TabIndex = 36
        Me.lblMilk12.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblMilk11
        '
        Me.lblMilk11.AutoSize = True
        Me.lblMilk11.Location = New System.Drawing.Point(511, 236)
        Me.lblMilk11.Name = "lblMilk11"
        Me.lblMilk11.Size = New System.Drawing.Size(327, 25)
        Me.lblMilk11.TabIndex = 35
        Me.lblMilk11.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'btnMilk7
        '
        Me.btnMilk7.Image = CType(resources.GetObject("btnMilk7.Image"), System.Drawing.Image)
        Me.btnMilk7.Location = New System.Drawing.Point(859, 22)
        Me.btnMilk7.Name = "btnMilk7"
        Me.btnMilk7.Size = New System.Drawing.Size(130, 45)
        Me.btnMilk7.TabIndex = 25
        Me.btnMilk7.Text = "Remove"
        '
        'lblMilk10
        '
        Me.lblMilk10.AutoSize = True
        Me.lblMilk10.Location = New System.Drawing.Point(511, 185)
        Me.lblMilk10.Name = "lblMilk10"
        Me.lblMilk10.Size = New System.Drawing.Size(327, 25)
        Me.lblMilk10.TabIndex = 34
        Me.lblMilk10.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'btnMilk8
        '
        Me.btnMilk8.Image = CType(resources.GetObject("btnMilk8.Image"), System.Drawing.Image)
        Me.btnMilk8.Location = New System.Drawing.Point(859, 73)
        Me.btnMilk8.Name = "btnMilk8"
        Me.btnMilk8.Size = New System.Drawing.Size(130, 45)
        Me.btnMilk8.TabIndex = 26
        Me.btnMilk8.Text = "Remove"
        '
        'lblMilk9
        '
        Me.lblMilk9.AutoSize = True
        Me.lblMilk9.Location = New System.Drawing.Point(511, 134)
        Me.lblMilk9.Name = "lblMilk9"
        Me.lblMilk9.Size = New System.Drawing.Size(327, 25)
        Me.lblMilk9.TabIndex = 33
        Me.lblMilk9.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'btnMilk9
        '
        Me.btnMilk9.Image = CType(resources.GetObject("btnMilk9.Image"), System.Drawing.Image)
        Me.btnMilk9.Location = New System.Drawing.Point(859, 124)
        Me.btnMilk9.Name = "btnMilk9"
        Me.btnMilk9.Size = New System.Drawing.Size(130, 45)
        Me.btnMilk9.TabIndex = 27
        Me.btnMilk9.Text = "Remove"
        '
        'lblMilk8
        '
        Me.lblMilk8.AutoSize = True
        Me.lblMilk8.Location = New System.Drawing.Point(511, 83)
        Me.lblMilk8.Name = "lblMilk8"
        Me.lblMilk8.Size = New System.Drawing.Size(327, 25)
        Me.lblMilk8.TabIndex = 32
        Me.lblMilk8.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'btnMilk10
        '
        Me.btnMilk10.Image = CType(resources.GetObject("btnMilk10.Image"), System.Drawing.Image)
        Me.btnMilk10.Location = New System.Drawing.Point(859, 175)
        Me.btnMilk10.Name = "btnMilk10"
        Me.btnMilk10.Size = New System.Drawing.Size(130, 45)
        Me.btnMilk10.TabIndex = 28
        Me.btnMilk10.Text = "Remove"
        '
        'lblMilk7
        '
        Me.lblMilk7.AutoSize = True
        Me.lblMilk7.Location = New System.Drawing.Point(511, 32)
        Me.lblMilk7.Name = "lblMilk7"
        Me.lblMilk7.Size = New System.Drawing.Size(327, 25)
        Me.lblMilk7.TabIndex = 31
        Me.lblMilk7.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'btnMilk11
        '
        Me.btnMilk11.Image = CType(resources.GetObject("btnMilk11.Image"), System.Drawing.Image)
        Me.btnMilk11.Location = New System.Drawing.Point(859, 226)
        Me.btnMilk11.Name = "btnMilk11"
        Me.btnMilk11.Size = New System.Drawing.Size(130, 45)
        Me.btnMilk11.TabIndex = 29
        Me.btnMilk11.Text = "Remove"
        '
        'btnMilk12
        '
        Me.btnMilk12.Image = CType(resources.GetObject("btnMilk12.Image"), System.Drawing.Image)
        Me.btnMilk12.Location = New System.Drawing.Point(859, 277)
        Me.btnMilk12.Name = "btnMilk12"
        Me.btnMilk12.Size = New System.Drawing.Size(130, 45)
        Me.btnMilk12.TabIndex = 30
        Me.btnMilk12.Text = "Remove"
        '
        'lblMilk6
        '
        Me.lblMilk6.AutoSize = True
        Me.lblMilk6.Location = New System.Drawing.Point(6, 287)
        Me.lblMilk6.Name = "lblMilk6"
        Me.lblMilk6.Size = New System.Drawing.Size(327, 25)
        Me.lblMilk6.TabIndex = 12
        Me.lblMilk6.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblMilk5
        '
        Me.lblMilk5.AutoSize = True
        Me.lblMilk5.Location = New System.Drawing.Point(6, 236)
        Me.lblMilk5.Name = "lblMilk5"
        Me.lblMilk5.Size = New System.Drawing.Size(327, 25)
        Me.lblMilk5.TabIndex = 11
        Me.lblMilk5.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblMilk4
        '
        Me.lblMilk4.AutoSize = True
        Me.lblMilk4.Location = New System.Drawing.Point(6, 185)
        Me.lblMilk4.Name = "lblMilk4"
        Me.lblMilk4.Size = New System.Drawing.Size(327, 25)
        Me.lblMilk4.TabIndex = 10
        Me.lblMilk4.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblMilk3
        '
        Me.lblMilk3.AutoSize = True
        Me.lblMilk3.Location = New System.Drawing.Point(6, 134)
        Me.lblMilk3.Name = "lblMilk3"
        Me.lblMilk3.Size = New System.Drawing.Size(327, 25)
        Me.lblMilk3.TabIndex = 9
        Me.lblMilk3.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblMilk2
        '
        Me.lblMilk2.AutoSize = True
        Me.lblMilk2.Location = New System.Drawing.Point(6, 83)
        Me.lblMilk2.Name = "lblMilk2"
        Me.lblMilk2.Size = New System.Drawing.Size(327, 25)
        Me.lblMilk2.TabIndex = 8
        Me.lblMilk2.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'lblMilk1
        '
        Me.lblMilk1.AutoSize = True
        Me.lblMilk1.Location = New System.Drawing.Point(6, 32)
        Me.lblMilk1.Name = "lblMilk1"
        Me.lblMilk1.Size = New System.Drawing.Size(327, 25)
        Me.lblMilk1.TabIndex = 7
        Me.lblMilk1.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        '
        'btnMilk6
        '
        Me.btnMilk6.Image = CType(resources.GetObject("btnMilk6.Image"), System.Drawing.Image)
        Me.btnMilk6.Location = New System.Drawing.Point(354, 277)
        Me.btnMilk6.Name = "btnMilk6"
        Me.btnMilk6.Size = New System.Drawing.Size(130, 45)
        Me.btnMilk6.TabIndex = 6
        Me.btnMilk6.Text = "Remove"
        '
        'btnMilk5
        '
        Me.btnMilk5.Image = CType(resources.GetObject("btnMilk5.Image"), System.Drawing.Image)
        Me.btnMilk5.Location = New System.Drawing.Point(354, 226)
        Me.btnMilk5.Name = "btnMilk5"
        Me.btnMilk5.Size = New System.Drawing.Size(130, 45)
        Me.btnMilk5.TabIndex = 5
        Me.btnMilk5.Text = "Remove"
        '
        'btnMilk4
        '
        Me.btnMilk4.Image = CType(resources.GetObject("btnMilk4.Image"), System.Drawing.Image)
        Me.btnMilk4.Location = New System.Drawing.Point(354, 175)
        Me.btnMilk4.Name = "btnMilk4"
        Me.btnMilk4.Size = New System.Drawing.Size(130, 45)
        Me.btnMilk4.TabIndex = 4
        Me.btnMilk4.Text = "Remove"
        '
        'btnMilk3
        '
        Me.btnMilk3.Image = CType(resources.GetObject("btnMilk3.Image"), System.Drawing.Image)
        Me.btnMilk3.Location = New System.Drawing.Point(354, 124)
        Me.btnMilk3.Name = "btnMilk3"
        Me.btnMilk3.Size = New System.Drawing.Size(130, 45)
        Me.btnMilk3.TabIndex = 3
        Me.btnMilk3.Text = "Remove"
        '
        'btnMilk2
        '
        Me.btnMilk2.Image = CType(resources.GetObject("btnMilk2.Image"), System.Drawing.Image)
        Me.btnMilk2.Location = New System.Drawing.Point(354, 73)
        Me.btnMilk2.Name = "btnMilk2"
        Me.btnMilk2.Size = New System.Drawing.Size(130, 45)
        Me.btnMilk2.TabIndex = 2
        Me.btnMilk2.Text = "Remove"
        '
        'btnAdd
        '
        Me.btnAdd.Image = CType(resources.GetObject("btnAdd.Image"), System.Drawing.Image)
        Me.btnAdd.Location = New System.Drawing.Point(6, 328)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(983, 45)
        Me.btnAdd.TabIndex = 1
        Me.btnAdd.Text = "Add"
        '
        'btnMilk1
        '
        Me.btnMilk1.Image = CType(resources.GetObject("btnMilk1.Image"), System.Drawing.Image)
        Me.btnMilk1.Location = New System.Drawing.Point(354, 22)
        Me.btnMilk1.Name = "btnMilk1"
        Me.btnMilk1.Size = New System.Drawing.Size(130, 45)
        Me.btnMilk1.TabIndex = 0
        Me.btnMilk1.Text = "Remove"
        '
        'frmMilk
        '
        Me.ClientSize = New System.Drawing.Size(1024, 538)
        Me.Controls.Add(Me.gbxMilk)
        Me.Name = "frmMilk"
        Me.Text = "Milk Records"
        Me.Controls.SetChildIndex(Me.gbxMilk, 0)
        Me.gbxMilk.ResumeLayout(False)
        Me.gbxMilk.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbxMilk As System.Windows.Forms.GroupBox
    Friend WithEvents lblMilk6 As System.Windows.Forms.Label
    Friend WithEvents lblMilk5 As System.Windows.Forms.Label
    Friend WithEvents lblMilk4 As System.Windows.Forms.Label
    Friend WithEvents lblMilk3 As System.Windows.Forms.Label
    Friend WithEvents lblMilk2 As System.Windows.Forms.Label
    Friend WithEvents lblMilk1 As System.Windows.Forms.Label
    Friend WithEvents btnMilk6 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMilk5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMilk4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMilk3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMilk2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMilk1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblMilk12 As System.Windows.Forms.Label
    Friend WithEvents lblMilk11 As System.Windows.Forms.Label
    Friend WithEvents btnMilk7 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblMilk10 As System.Windows.Forms.Label
    Friend WithEvents btnMilk8 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblMilk9 As System.Windows.Forms.Label
    Friend WithEvents btnMilk9 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblMilk8 As System.Windows.Forms.Label
    Friend WithEvents btnMilk10 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblMilk7 As System.Windows.Forms.Label
    Friend WithEvents btnMilk11 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMilk12 As DevExpress.XtraEditors.SimpleButton

End Class
