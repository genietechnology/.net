﻿Option Strict On
Imports DevExpress.Data.Camera
Imports DevExpress.XtraEditors.Camera
Imports DevExpress.XtraGrid.Views.Base

Public Class frmPhoto

    Private m_Image As Image = Nothing
    Private m_Devices As New Dictionary(Of Integer, CameraDeviceInfo)
    Private m_DevicesKey As Integer = 0

    Public ReadOnly Property CapturedImage As Image
        Get
            Return m_Image
        End Get
    End Property

    Private Sub frmPhoto_Load(sender As Object, e As EventArgs) Handles Me.Load

        btnSave.Enabled = False

        Dim _i As Integer = 1
        For Each _d In CameraControl.GetDevices
            m_Devices.Add(_i, _d)
            _i += 1
        Next

        btnSwap.Enabled = _i > 1
        m_DevicesKey = 1

    End Sub

    Private Sub CapturePhoto()

        Try
            Dim _bmp As Bitmap = cam.TakeSnapshot
            iSlider.Images.Insert(0, _bmp)
            btnSave.Enabled = True

        Catch ex As Exception

        End Try

    End Sub

    Private Sub Save()

        If iSlider.Images.Count = 0 Then
            Msgbox("Please ensure you have taken a Photo...", MessageBoxIcon.Exclamation, "Save Photo")
        Else
            If iSlider.Images.Count = 1 Then
                m_Image = iSlider.Images(0)
                Me.DialogResult = DialogResult.OK
                Me.Close()
            Else
                Msgbox("You have multiple photos, please ensure only one Photo remains by removing the photos you don't want...", MessageBoxIcon.Exclamation, "Save Photo")
            End If
        End If

    End Sub

    Private Sub frmPhoto_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        cam.Stop()
    End Sub

    Private Sub frmPhoto_SizeChanged(sender As Object, e As EventArgs) Handles Me.SizeChanged
        SplitContainerControl1.SplitterPosition = CInt(SplitContainerControl1.Width / 2)
    End Sub

#Region "Buttons"

    Private Sub btnMediaDelete_Click(sender As Object, e As EventArgs) Handles btnMediaDelete.Click
        If Msgbox("Are you sure you want to Delete this photo?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Delete Photo") = DialogResult.Yes Then
            iSlider.Images.Remove(iSlider.CurrentImage)
            btnSave.Enabled = iSlider.Images.Count = 1
        End If
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnSwap_Click(sender As Object, e As EventArgs) Handles btnSwap.Click

        If m_DevicesKey < m_Devices.Count Then
            m_DevicesKey += 1
        Else
            m_DevicesKey = 1
        End If

        cam.Stop()
        cam.Start(CameraControl.GetDevice(m_Devices(m_DevicesKey)))

    End Sub

    Private Sub btnPhotoL_Click(sender As Object, e As EventArgs) Handles btnPhotoL.Click
        CapturePhoto()
    End Sub

    Private Sub btnPhotoR_Click(sender As Object, e As EventArgs) Handles btnPhotoR.Click
        CapturePhoto()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Save()
    End Sub


#End Region

End Class