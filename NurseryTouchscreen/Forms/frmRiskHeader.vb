﻿Imports NurseryTouchscreen.SharedModule

Public Class frmRiskHeader

    Private m_RiskID As Guid? = Nothing
    Private m_New As Boolean = False

    Public Sub New(ByVal RiskID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        If RiskID.HasValue Then
            m_New = False
            m_RiskID = RiskID
        Else
            m_New = True
        End If

    End Sub
    Private Sub frmRiskHeader_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not m_New Then
            DisplayRecord
        End If
    End Sub

    Private Sub DisplayRecord()

        Dim _SQL As String = "select * from Risks where ID = '" + m_RiskID.ToString + "'"

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If _DT IsNot Nothing Then

            Dim _DR As DataRow = _DT.Rows(0)

            fldTitle.ValueText = _DR.Item("title").ToString
            fldArea.ValueText = _DR.Item("area").ToString
            fldStatus.ValueText = _DR.Item("status").ToString
            fldRevision.ValueText = _DR.Item("rev").ToString
            fldFrequency.ValueText = _DR.Item("ass_freq").ToString

            _DT.Dispose()

        End If

    End Sub
    Private Function ValidateEntry() As Boolean

        If fldTitle.ValueText = "" Then
            Msgbox("Please enter the risk assessment title.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If fldArea.ValueText = "" Then
            Msgbox("Please enter the area.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If fldStatus.ValueText = "" Then
            Msgbox("Please select the status.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If fldRevision.ValueText = "" Then
            Msgbox("Please enter the revision number.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If fldFrequency.ValueText = "" Then
            Msgbox("Please select the assessment frequency.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        Return True

    End Function

    Private Function Store() As Boolean

        If Not ValidateEntry() Then Return False

        Dim _ID As String = ""

        If m_New Then
            _ID = Guid.NewGuid.ToString
        Else
            _ID = m_RiskID.ToString
        End If

        Dim _SQL As String = "select * from Risks where ID = '" + _ID + "'"

        Dim _DA As IDataAdapter = DAL.ReturnDataAdapter(_SQL)
        If Not _DA Is Nothing Then

            Dim _DR As DataRow

            Dim _DS As New DataSet
            _DA.Fill(_DS)

            If m_New Then
                _DR = _DS.Tables(0).NewRow
                _DR("id") = New Guid(_ID)
                _DR("site_id") = Parameters.SiteID
                _DR("site_name") = Parameters.Site
                _DR("created") = Now
            Else
                _DR = _DS.Tables(0).Rows(0)
            End If

            _DR("title") = fldTitle.ValueText
            _DR("area") = fldArea.ValueText
            _DR("status") = fldStatus.ValueText
            _DR("rev") = fldRevision.ValueText
            _DR("ass_freq") = fldFrequency.ValueText

            If m_New Then _DS.Tables(0).Rows.Add(_DR)
            DAL.UpdateDB(_DA, _DS)

        End If

        Return True

    End Function

#Region "Controls"
    Private Sub fldStatus_ButtonClick(sender As Object, e As EventArgs) Handles fldStatus.ButtonClick
        Dim _P As Pair = ReturnButtonSQL("Current|Archived", "Please select a Status")
        If _P IsNot Nothing Then
            fldStatus.ValueText = _P.Text
        End If
    End Sub

    Private Sub fldFrequency_ButtonClick(sender As Object, e As EventArgs) Handles fldFrequency.ButtonClick
        Dim _P As Pair = ReturnButtonSQL("Every week|Every fornight|Every month|Every 6 weeks|Every quarter|Every 6 months|Every year", "Please select a Frequency")
        If _P IsNot Nothing Then
            fldFrequency.ValueText = _P.Text
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click
        If Store() Then
            Me.DialogResult = DialogResult.OK
            Me.Close()
        End If
    End Sub

#End Region

End Class
