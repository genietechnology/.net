﻿Public Class frmRiskItem

    Private m_RiskID As Guid
    Private m_ItemID As Guid? = Nothing
    Private m_New As Boolean = False

    Public Sub New(ByVal RiskAssessmentID As Guid, ByVal ItemID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        m_RiskID = RiskAssessmentID

        If ItemID.HasValue Then
            m_New = False
            m_ItemID = ItemID
        Else
            m_New = True
        End If

    End Sub

    Private Sub frmRiskItem_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Not m_New Then
            DisplayRecord
        End If
    End Sub

    Private Sub DisplayRecord()

        Dim _SQL As String = "select * from RiskActivities where ID = '" + m_ItemID.ToString + "'"

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If _DT IsNot Nothing Then

            Dim _DR As DataRow = _DT.Rows(0)

            fldActivity.ValueText = _DR.Item("activity").ToString
            fldHazard.ValueText = _DR.Item("hazard_desc").ToString
            fldRisk.ValueText = _DR.Item("risk_desc").ToString
            fldPeople.ValueText = _DR.Item("people_desc").ToString

            fldControl.ValueText = _DR.Item("controls").ToString
            txtControlMeasures.Text = _DR.Item("controls").ToString

            rcUncontrolled.Likelihood = _DR.Item("uc_likely").ToString
            rcUncontrolled.Severity = _DR.Item("uc_severity").ToString

            rcControlled.Likelihood = _DR.Item("c_likely").ToString
            rcControlled.Severity = _DR.Item("c_severity").ToString

            _DT.Dispose()

        End If

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click
        If Store() Then
            Me.DialogResult = DialogResult.OK
            Me.Close()
        End If
    End Sub

    Private Function ValidateEntry() As Boolean

        If fldActivity.ValueText = "" Then
            Msgbox("Please enter the activity details.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If fldHazard.ValueText = "" Then
            Msgbox("Please enter the hazards associated with the Activity.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If fldRisk.ValueText = "" Then
            Msgbox("Please enter the risks associated with the hazard.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If fldPeople.ValueText = "" Then
            Msgbox("Please enter the people exposed to the hazard.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If fldControl.ValueText = "" Then
            Msgbox("Please enter the control measures implemented.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        Return True

    End Function

    Private Function Store() As Boolean

        If Not ValidateEntry Then Return False

        Dim _ID As String = ""

        If m_New Then
            _ID = Guid.NewGuid.ToString
        Else
            _ID = m_ItemID.ToString
        End If

        Dim _SQL As String = "select * from RiskActivities where ID = '" + _ID + "'"

        Dim _DA As IDataAdapter = DAL.ReturnDataAdapter(_SQL)
        If Not _DA Is Nothing Then

            Dim _DR As DataRow

            Dim _DS As New DataSet
            _DA.Fill(_DS)

            If m_New Then
                _DR = _DS.Tables(0).NewRow
                _DR("id") = New Guid(_ID)
                _DR("risk_id") = m_RiskID
            Else
                _DR = _DS.Tables(0).Rows(0)
            End If

            _DR("activity") = fldActivity.ValueText
            _DR("hazard_codes") = ""
            _DR("hazard_desc") = fldHazard.ValueText
            _DR("risk_codes") = ""
            _DR("risk_desc") = fldRisk.ValueText
            _DR("people_codes") = ""
            _DR("people_desc") = fldPeople.ValueText
            _DR("controls") = fldControl.ValueText

            _DR("uc_likely") = rcUncontrolled.Likelihood
            _DR("uc_severity") = rcUncontrolled.Severity
            _DR("uc_rating") = rcUncontrolled.Rating

            _DR("c_likely") = rcControlled.Likelihood
            _DR("c_severity") = rcControlled.Severity
            _DR("c_rating") = rcControlled.Rating

            If m_New Then _DS.Tables(0).Rows.Add(_DR)
            DAL.UpdateDB(_DA, _DS)

        End If

        Return True

    End Function

    Private Sub fldControl_AfterButtonClick(sender As Object, e As EventArgs) Handles fldControl.AfterButtonClick
        txtControlMeasures.Text = fldControl.ValueText
    End Sub
End Class
