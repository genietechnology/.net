﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHostingForm
    Inherits frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmHostingForm))
        Me.gcHost = New DevExpress.XtraEditors.GroupControl()
        Me.panHost = New DevExpress.XtraEditors.PanelControl()
        Me.panBottom = New DevExpress.XtraEditors.PanelControl()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAccept = New DevExpress.XtraEditors.SimpleButton()
        Me.tbGroups = New DevExpress.XtraBars.Navigation.TileBar()
        Me.tbGroupGroup = New DevExpress.XtraBars.Navigation.TileBarGroup()
        Me.tbStaff = New DevExpress.XtraBars.Navigation.TileBar()
        Me.tbStaffGroup = New DevExpress.XtraBars.Navigation.TileBarGroup()
        Me.tbChildren = New DevExpress.XtraBars.Navigation.TileBar()
        Me.tbChildGroup = New DevExpress.XtraBars.Navigation.TileBarGroup()
        CType(Me.gcHost, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcHost.SuspendLayout()
        CType(Me.panHost, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panBottom.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'gcHost
        '
        Me.gcHost.Controls.Add(Me.panHost)
        Me.gcHost.Controls.Add(Me.panBottom)
        Me.gcHost.Controls.Add(Me.tbGroups)
        Me.gcHost.Controls.Add(Me.tbStaff)
        Me.gcHost.Controls.Add(Me.tbChildren)
        Me.gcHost.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcHost.Location = New System.Drawing.Point(0, 0)
        Me.gcHost.Name = "gcHost"
        Me.gcHost.ShowCaption = False
        Me.gcHost.Size = New System.Drawing.Size(1184, 611)
        Me.gcHost.TabIndex = 62
        Me.gcHost.Text = "GroupControl1"
        '
        'panHost
        '
        Me.panHost.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.panHost.Dock = System.Windows.Forms.DockStyle.Fill
        Me.panHost.Location = New System.Drawing.Point(333, 122)
        Me.panHost.Name = "panHost"
        Me.panHost.Size = New System.Drawing.Size(624, 431)
        Me.panHost.TabIndex = 18
        '
        'panBottom
        '
        Me.panBottom.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panBottom.Appearance.Options.UseFont = True
        Me.panBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.panBottom.Controls.Add(Me.btnCancel)
        Me.panBottom.Controls.Add(Me.btnAccept)
        Me.panBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panBottom.Location = New System.Drawing.Point(333, 553)
        Me.panBottom.Name = "panBottom"
        Me.panBottom.Size = New System.Drawing.Size(624, 56)
        Me.panBottom.TabIndex = 5
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Image = CType(resources.GetObject("btnCancel.Image"), System.Drawing.Image)
        Me.btnCancel.Location = New System.Drawing.Point(473, 6)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(145, 44)
        Me.btnCancel.TabIndex = 16
        Me.btnCancel.Text = "Cancel"
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAccept.Image = Global.NurseryTouchscreen.My.Resources.Resources.success_32
        Me.btnAccept.Location = New System.Drawing.Point(322, 6)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(145, 44)
        Me.btnAccept.TabIndex = 0
        Me.btnAccept.Text = "Accept"
        '
        'tbGroups
        '
        Me.tbGroups.AllowDrag = False
        Me.tbGroups.AllowItemHover = False
        Me.tbGroups.AllowSelectedItem = True
        Me.tbGroups.AppearanceText.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbGroups.AppearanceText.Options.UseFont = True
        Me.tbGroups.Dock = System.Windows.Forms.DockStyle.Top
        Me.tbGroups.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        Me.tbGroups.Groups.Add(Me.tbGroupGroup)
        Me.tbGroups.HorizontalContentAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.tbGroups.ItemSize = 80
        Me.tbGroups.Location = New System.Drawing.Point(333, 2)
        Me.tbGroups.MaxId = 9
        Me.tbGroups.Name = "tbGroups"
        Me.tbGroups.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.None
        Me.tbGroups.SelectionBorderWidth = 5
        Me.tbGroups.SelectionColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.tbGroups.ShowGroupText = False
        Me.tbGroups.Size = New System.Drawing.Size(624, 120)
        Me.tbGroups.TabIndex = 16
        Me.tbGroups.Text = "TileBar1"
        Me.tbGroups.WideTileWidth = 200
        '
        'tbGroupGroup
        '
        Me.tbGroupGroup.Name = "tbGroupGroup"
        '
        'tbStaff
        '
        Me.tbStaff.AllowDrag = False
        Me.tbStaff.AllowItemHover = False
        Me.tbStaff.AllowSelectedItem = True
        Me.tbStaff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.tbStaff.Dock = System.Windows.Forms.DockStyle.Right
        Me.tbStaff.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        Me.tbStaff.Groups.Add(Me.tbStaffGroup)
        Me.tbStaff.ItemSize = 80
        Me.tbStaff.Location = New System.Drawing.Point(957, 2)
        Me.tbStaff.Name = "tbStaff"
        Me.tbStaff.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.tbStaff.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollButtons
        Me.tbStaff.SelectionBorderWidth = 5
        Me.tbStaff.SelectionColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tbStaff.ShowText = True
        Me.tbStaff.Size = New System.Drawing.Size(225, 607)
        Me.tbStaff.TabIndex = 1
        Me.tbStaff.Text = "Staff"
        Me.tbStaff.VerticalContentAlignment = DevExpress.Utils.VertAlignment.Top
        '
        'tbStaffGroup
        '
        Me.tbStaffGroup.Name = "tbStaffGroup"
        '
        'tbChildren
        '
        Me.tbChildren.AllowDrag = False
        Me.tbChildren.AllowItemHover = False
        Me.tbChildren.AllowSelectedItem = True
        Me.tbChildren.Dock = System.Windows.Forms.DockStyle.Left
        Me.tbChildren.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        Me.tbChildren.Groups.Add(Me.tbChildGroup)
        Me.tbChildren.ItemSize = 140
        Me.tbChildren.Location = New System.Drawing.Point(2, 2)
        Me.tbChildren.MaxId = 1
        Me.tbChildren.Name = "tbChildren"
        Me.tbChildren.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.tbChildren.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollButtons
        Me.tbChildren.SelectionBorderWidth = 5
        Me.tbChildren.SelectionColor = System.Drawing.Color.Gray
        Me.tbChildren.ShowText = True
        Me.tbChildren.Size = New System.Drawing.Size(331, 607)
        Me.tbChildren.TabIndex = 0
        Me.tbChildren.Text = "Children"
        Me.tbChildren.VerticalContentAlignment = DevExpress.Utils.VertAlignment.Top
        '
        'tbChildGroup
        '
        Me.tbChildGroup.Name = "tbChildGroup"
        '
        'frmHostingForm
        '
        Me.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.Appearance.Options.UseBackColor = True
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
        Me.ClientSize = New System.Drawing.Size(1184, 611)
        Me.Controls.Add(Me.gcHost)
        Me.Name = "frmHostingForm"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Controls.SetChildIndex(Me.gcHost, 0)
        CType(Me.gcHost, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcHost.ResumeLayout(False)
        CType(Me.panHost, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panBottom.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gcHost As DevExpress.XtraEditors.GroupControl
    Friend WithEvents tbGroups As DevExpress.XtraBars.Navigation.TileBar
    Friend WithEvents tbGroupGroup As DevExpress.XtraBars.Navigation.TileBarGroup
    Friend WithEvents tbChildren As DevExpress.XtraBars.Navigation.TileBar
    Friend WithEvents tbChildGroup As DevExpress.XtraBars.Navigation.TileBarGroup
    Protected Friend WithEvents panBottom As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAccept As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents panHost As DevExpress.XtraEditors.PanelControl
    Friend WithEvents tbStaff As DevExpress.XtraBars.Navigation.TileBar
    Friend WithEvents tbStaffGroup As DevExpress.XtraBars.Navigation.TileBarGroup
End Class
