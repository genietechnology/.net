﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBodyMap
    Inherits NurseryTouchscreen.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.picMap = New System.Windows.Forms.PictureBox()
        Me.btnBump = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCut = New DevExpress.XtraEditors.SimpleButton()
        Me.btnRedMark = New DevExpress.XtraEditors.SimpleButton()
        Me.btnBruise = New DevExpress.XtraEditors.SimpleButton()
        Me.btnGraze = New DevExpress.XtraEditors.SimpleButton()
        Me.panBottom = New DevExpress.XtraEditors.PanelControl()
        Me.btnClear = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSelect = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAccept = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSwelling = New DevExpress.XtraEditors.SimpleButton()
        Me.btnBite = New DevExpress.XtraEditors.SimpleButton()
        Me.btnScratch = New DevExpress.XtraEditors.SimpleButton()
        Me.btnBurn = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.picMap, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panBottom.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'picMap
        '
        Me.picMap.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picMap.Location = New System.Drawing.Point(12, 12)
        Me.picMap.Name = "picMap"
        Me.picMap.Size = New System.Drawing.Size(679, 402)
        Me.picMap.TabIndex = 46
        Me.picMap.TabStop = False
        '
        'btnBump
        '
        Me.btnBump.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBump.Appearance.BackColor = System.Drawing.Color.Black
        Me.btnBump.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBump.Appearance.ForeColor = System.Drawing.Color.White
        Me.btnBump.Appearance.Options.UseBackColor = True
        Me.btnBump.Appearance.Options.UseFont = True
        Me.btnBump.Appearance.Options.UseForeColor = True
        Me.btnBump.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnBump.Location = New System.Drawing.Point(697, 12)
        Me.btnBump.Name = "btnBump"
        Me.btnBump.Size = New System.Drawing.Size(150, 62)
        Me.btnBump.TabIndex = 0
        Me.btnBump.Text = "Bump"
        '
        'btnCut
        '
        Me.btnCut.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCut.Appearance.BackColor = System.Drawing.Color.LimeGreen
        Me.btnCut.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCut.Appearance.ForeColor = System.Drawing.Color.White
        Me.btnCut.Appearance.Options.UseBackColor = True
        Me.btnCut.Appearance.Options.UseFont = True
        Me.btnCut.Appearance.Options.UseForeColor = True
        Me.btnCut.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnCut.Location = New System.Drawing.Point(697, 352)
        Me.btnCut.Name = "btnCut"
        Me.btnCut.Size = New System.Drawing.Size(150, 62)
        Me.btnCut.TabIndex = 5
        Me.btnCut.Text = "Cut"
        '
        'btnRedMark
        '
        Me.btnRedMark.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRedMark.Appearance.BackColor = System.Drawing.Color.Red
        Me.btnRedMark.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRedMark.Appearance.ForeColor = System.Drawing.Color.White
        Me.btnRedMark.Appearance.Options.UseBackColor = True
        Me.btnRedMark.Appearance.Options.UseFont = True
        Me.btnRedMark.Appearance.Options.UseForeColor = True
        Me.btnRedMark.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnRedMark.Location = New System.Drawing.Point(697, 284)
        Me.btnRedMark.Name = "btnRedMark"
        Me.btnRedMark.Size = New System.Drawing.Size(150, 62)
        Me.btnRedMark.TabIndex = 4
        Me.btnRedMark.Text = "Red Mark"
        '
        'btnBruise
        '
        Me.btnBruise.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBruise.Appearance.BackColor = System.Drawing.Color.Purple
        Me.btnBruise.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBruise.Appearance.ForeColor = System.Drawing.Color.White
        Me.btnBruise.Appearance.Options.UseBackColor = True
        Me.btnBruise.Appearance.Options.UseFont = True
        Me.btnBruise.Appearance.Options.UseForeColor = True
        Me.btnBruise.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnBruise.Location = New System.Drawing.Point(697, 216)
        Me.btnBruise.Name = "btnBruise"
        Me.btnBruise.Size = New System.Drawing.Size(150, 62)
        Me.btnBruise.TabIndex = 3
        Me.btnBruise.Text = "Bruise"
        '
        'btnGraze
        '
        Me.btnGraze.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnGraze.Appearance.BackColor = System.Drawing.Color.Orange
        Me.btnGraze.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGraze.Appearance.ForeColor = System.Drawing.Color.White
        Me.btnGraze.Appearance.Options.UseBackColor = True
        Me.btnGraze.Appearance.Options.UseFont = True
        Me.btnGraze.Appearance.Options.UseForeColor = True
        Me.btnGraze.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnGraze.Location = New System.Drawing.Point(697, 80)
        Me.btnGraze.Name = "btnGraze"
        Me.btnGraze.Size = New System.Drawing.Size(150, 62)
        Me.btnGraze.TabIndex = 1
        Me.btnGraze.Text = "Graze"
        '
        'panBottom
        '
        Me.panBottom.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panBottom.Appearance.Options.UseFont = True
        Me.panBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.panBottom.Controls.Add(Me.btnClear)
        Me.panBottom.Controls.Add(Me.btnSelect)
        Me.panBottom.Controls.Add(Me.btnAccept)
        Me.panBottom.Controls.Add(Me.btnCancel)
        Me.panBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panBottom.Location = New System.Drawing.Point(0, 422)
        Me.panBottom.Name = "panBottom"
        Me.panBottom.Size = New System.Drawing.Size(1010, 60)
        Me.panBottom.TabIndex = 9
        '
        'btnClear
        '
        Me.btnClear.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.Appearance.Options.UseFont = True
        Me.btnClear.Image = Global.NurseryTouchscreen.My.Resources.Resources.delete_32
        Me.btnClear.Location = New System.Drawing.Point(237, 3)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(219, 45)
        Me.btnClear.TabIndex = 1
        Me.btnClear.Text = "Clear Body Map"
        '
        'btnSelect
        '
        Me.btnSelect.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSelect.Appearance.Options.UseFont = True
        Me.btnSelect.Image = Global.NurseryTouchscreen.My.Resources.Resources.glossary_32
        Me.btnSelect.Location = New System.Drawing.Point(12, 3)
        Me.btnSelect.Name = "btnSelect"
        Me.btnSelect.Size = New System.Drawing.Size(219, 45)
        Me.btnSelect.TabIndex = 0
        Me.btnSelect.Text = "Select Body Map"
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAccept.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAccept.Appearance.Options.UseFont = True
        Me.btnAccept.Image = Global.NurseryTouchscreen.My.Resources.Resources.success_32
        Me.btnAccept.Location = New System.Drawing.Point(697, 3)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(150, 45)
        Me.btnAccept.TabIndex = 2
        Me.btnAccept.Text = "Accept"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Image = Global.NurseryTouchscreen.My.Resources.Resources.cancel_32
        Me.btnCancel.Location = New System.Drawing.Point(853, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(150, 45)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        '
        'btnSwelling
        '
        Me.btnSwelling.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSwelling.Appearance.BackColor = System.Drawing.Color.SteelBlue
        Me.btnSwelling.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSwelling.Appearance.ForeColor = System.Drawing.Color.White
        Me.btnSwelling.Appearance.Options.UseBackColor = True
        Me.btnSwelling.Appearance.Options.UseFont = True
        Me.btnSwelling.Appearance.Options.UseForeColor = True
        Me.btnSwelling.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnSwelling.Location = New System.Drawing.Point(853, 80)
        Me.btnSwelling.Name = "btnSwelling"
        Me.btnSwelling.Size = New System.Drawing.Size(150, 62)
        Me.btnSwelling.TabIndex = 8
        Me.btnSwelling.Text = "Swelling"
        '
        'btnBite
        '
        Me.btnBite.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBite.Appearance.BackColor = System.Drawing.Color.Green
        Me.btnBite.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBite.Appearance.ForeColor = System.Drawing.Color.White
        Me.btnBite.Appearance.Options.UseBackColor = True
        Me.btnBite.Appearance.Options.UseFont = True
        Me.btnBite.Appearance.Options.UseForeColor = True
        Me.btnBite.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnBite.Location = New System.Drawing.Point(853, 12)
        Me.btnBite.Name = "btnBite"
        Me.btnBite.Size = New System.Drawing.Size(150, 62)
        Me.btnBite.TabIndex = 6
        Me.btnBite.Text = "Bite"
        '
        'btnScratch
        '
        Me.btnScratch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnScratch.Appearance.BackColor = System.Drawing.Color.Yellow
        Me.btnScratch.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnScratch.Appearance.ForeColor = System.Drawing.Color.Black
        Me.btnScratch.Appearance.Options.UseBackColor = True
        Me.btnScratch.Appearance.Options.UseFont = True
        Me.btnScratch.Appearance.Options.UseForeColor = True
        Me.btnScratch.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnScratch.Location = New System.Drawing.Point(697, 148)
        Me.btnScratch.Name = "btnScratch"
        Me.btnScratch.Size = New System.Drawing.Size(150, 62)
        Me.btnScratch.TabIndex = 2
        Me.btnScratch.Text = "Scratch"
        '
        'btnBurn
        '
        Me.btnBurn.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBurn.Appearance.BackColor = System.Drawing.Color.Brown
        Me.btnBurn.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBurn.Appearance.ForeColor = System.Drawing.Color.White
        Me.btnBurn.Appearance.Options.UseBackColor = True
        Me.btnBurn.Appearance.Options.UseFont = True
        Me.btnBurn.Appearance.Options.UseForeColor = True
        Me.btnBurn.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnBurn.Location = New System.Drawing.Point(853, 148)
        Me.btnBurn.Name = "btnBurn"
        Me.btnBurn.Size = New System.Drawing.Size(150, 62)
        Me.btnBurn.TabIndex = 9
        Me.btnBurn.Text = "Burn"
        '
        'frmBodyMap
        '
        Me.ClientSize = New System.Drawing.Size(1010, 482)
        Me.Controls.Add(Me.btnBurn)
        Me.Controls.Add(Me.btnScratch)
        Me.Controls.Add(Me.btnSwelling)
        Me.Controls.Add(Me.btnBite)
        Me.Controls.Add(Me.picMap)
        Me.Controls.Add(Me.btnBump)
        Me.Controls.Add(Me.btnCut)
        Me.Controls.Add(Me.btnRedMark)
        Me.Controls.Add(Me.btnBruise)
        Me.Controls.Add(Me.btnGraze)
        Me.Controls.Add(Me.panBottom)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBodyMap"
        Me.Text = "Body Map"
        Me.Controls.SetChildIndex(Me.panBottom, 0)
        Me.Controls.SetChildIndex(Me.btnGraze, 0)
        Me.Controls.SetChildIndex(Me.btnBruise, 0)
        Me.Controls.SetChildIndex(Me.btnRedMark, 0)
        Me.Controls.SetChildIndex(Me.btnCut, 0)
        Me.Controls.SetChildIndex(Me.btnBump, 0)
        Me.Controls.SetChildIndex(Me.picMap, 0)
        Me.Controls.SetChildIndex(Me.btnBite, 0)
        Me.Controls.SetChildIndex(Me.btnSwelling, 0)
        Me.Controls.SetChildIndex(Me.btnScratch, 0)
        Me.Controls.SetChildIndex(Me.btnBurn, 0)
        CType(Me.picMap, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panBottom.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Protected Friend WithEvents panBottom As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnAccept As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnGraze As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnBruise As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRedMark As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCut As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnBump As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents picMap As System.Windows.Forms.PictureBox
    Friend WithEvents btnSelect As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnClear As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSwelling As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnBite As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnScratch As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnBurn As DevExpress.XtraEditors.SimpleButton
End Class
