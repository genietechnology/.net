﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmRiskItem
    Inherits NurseryTouchscreen.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRiskItem))
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.rcUncontrolled = New NurseryTouchscreen.RiskControl()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.rcControlled = New NurseryTouchscreen.RiskControl()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAccept = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.txtControlMeasures = New DevExpress.XtraEditors.MemoEdit()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.fldActivity = New NurseryTouchscreen.Field()
        Me.GroupControl6 = New DevExpress.XtraEditors.GroupControl()
        Me.fldHazard = New NurseryTouchscreen.Field()
        Me.GroupControl7 = New DevExpress.XtraEditors.GroupControl()
        Me.fldRisk = New NurseryTouchscreen.Field()
        Me.GroupControl8 = New DevExpress.XtraEditors.GroupControl()
        Me.fldPeople = New NurseryTouchscreen.Field()
        Me.GroupControl9 = New DevExpress.XtraEditors.GroupControl()
        Me.fldControl = New NurseryTouchscreen.Field()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.txtControlMeasures.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl6.SuspendLayout()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl7.SuspendLayout()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl8.SuspendLayout()
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl9.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'rcUncontrolled
        '
        Me.rcUncontrolled.Likelihood = 0
        Me.rcUncontrolled.Location = New System.Drawing.Point(4, 42)
        Me.rcUncontrolled.Name = "rcUncontrolled"
        Me.rcUncontrolled.Severity = 0
        Me.rcUncontrolled.Size = New System.Drawing.Size(270, 226)
        Me.rcUncontrolled.TabIndex = 82
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.rcUncontrolled)
        Me.GroupControl1.Location = New System.Drawing.Point(10, 497)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(279, 277)
        Me.GroupControl1.TabIndex = 83
        Me.GroupControl1.Text = "Uncontrolled Risk Rating"
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl2.Controls.Add(Me.rcControlled)
        Me.GroupControl2.Location = New System.Drawing.Point(940, 497)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(279, 277)
        Me.GroupControl2.TabIndex = 84
        Me.GroupControl2.Text = "Residual Risk Rating"
        '
        'rcControlled
        '
        Me.rcControlled.Likelihood = 0
        Me.rcControlled.Location = New System.Drawing.Point(5, 42)
        Me.rcControlled.Name = "rcControlled"
        Me.rcControlled.Severity = 0
        Me.rcControlled.Size = New System.Drawing.Size(270, 226)
        Me.rcControlled.TabIndex = 82
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Image = CType(resources.GetObject("btnCancel.Image"), System.Drawing.Image)
        Me.btnCancel.Location = New System.Drawing.Point(1074, 780)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(145, 57)
        Me.btnCancel.TabIndex = 86
        Me.btnCancel.Text = "Cancel"
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAccept.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAccept.Appearance.Options.UseFont = True
        Me.btnAccept.Image = CType(resources.GetObject("btnAccept.Image"), System.Drawing.Image)
        Me.btnAccept.Location = New System.Drawing.Point(923, 780)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(145, 57)
        Me.btnAccept.TabIndex = 85
        Me.btnAccept.Text = "Accept"
        '
        'GroupControl3
        '
        Me.GroupControl3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl3.Controls.Add(Me.txtControlMeasures)
        Me.GroupControl3.Location = New System.Drawing.Point(295, 497)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(639, 277)
        Me.GroupControl3.TabIndex = 87
        Me.GroupControl3.Text = "Control Measures"
        '
        'txtControlMeasures
        '
        Me.txtControlMeasures.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtControlMeasures.Location = New System.Drawing.Point(2, 20)
        Me.txtControlMeasures.Name = "txtControlMeasures"
        Me.txtControlMeasures.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtControlMeasures.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtControlMeasures, True)
        Me.txtControlMeasures.Size = New System.Drawing.Size(635, 255)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtControlMeasures, OptionsSpelling1)
        Me.txtControlMeasures.TabIndex = 69
        '
        'GroupControl4
        '
        Me.GroupControl4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl4.Controls.Add(Me.fldActivity)
        Me.GroupControl4.Location = New System.Drawing.Point(10, 12)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(1209, 88)
        Me.GroupControl4.TabIndex = 88
        Me.GroupControl4.Text = "Activity"
        '
        'fldActivity
        '
        Me.fldActivity.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fldActivity.ButtonCustom = False
        Me.fldActivity.ButtonSQL = Nothing
        Me.fldActivity.ButtonText = ""
        Me.fldActivity.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Enter
        Me.fldActivity.ButtonWidth = 48.0!
        Me.fldActivity.FamilyID = Nothing
        Me.fldActivity.HideText = False
        Me.fldActivity.LabelText = "Label"
        Me.fldActivity.LabelWidth = 0!
        Me.fldActivity.Location = New System.Drawing.Point(5, 34)
        Me.fldActivity.MinimumSize = New System.Drawing.Size(210, 51)
        Me.fldActivity.Name = "fldActivity"
        Me.fldActivity.QuickTextList = Nothing
        Me.fldActivity.Size = New System.Drawing.Size(1199, 51)
        Me.fldActivity.TabIndex = 0
        Me.fldActivity.ValueDateTime = Nothing
        Me.fldActivity.ValueID = Nothing
        Me.fldActivity.ValueMaxLength = 0
        Me.fldActivity.ValueText = ""
        '
        'GroupControl6
        '
        Me.GroupControl6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl6.Controls.Add(Me.fldHazard)
        Me.GroupControl6.Location = New System.Drawing.Point(10, 106)
        Me.GroupControl6.Name = "GroupControl6"
        Me.GroupControl6.Size = New System.Drawing.Size(1209, 88)
        Me.GroupControl6.TabIndex = 89
        Me.GroupControl6.Text = "Hazard"
        '
        'fldHazard
        '
        Me.fldHazard.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fldHazard.ButtonCustom = False
        Me.fldHazard.ButtonSQL = Nothing
        Me.fldHazard.ButtonText = ""
        Me.fldHazard.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Enter
        Me.fldHazard.ButtonWidth = 48.0!
        Me.fldHazard.FamilyID = Nothing
        Me.fldHazard.HideText = False
        Me.fldHazard.LabelText = "Label"
        Me.fldHazard.LabelWidth = 0!
        Me.fldHazard.Location = New System.Drawing.Point(5, 34)
        Me.fldHazard.MinimumSize = New System.Drawing.Size(210, 51)
        Me.fldHazard.Name = "fldHazard"
        Me.fldHazard.QuickTextList = "Risk Assessment Hazard"
        Me.fldHazard.Size = New System.Drawing.Size(1199, 51)
        Me.fldHazard.TabIndex = 0
        Me.fldHazard.ValueDateTime = Nothing
        Me.fldHazard.ValueID = Nothing
        Me.fldHazard.ValueMaxLength = 0
        Me.fldHazard.ValueText = ""
        '
        'GroupControl7
        '
        Me.GroupControl7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl7.Controls.Add(Me.fldRisk)
        Me.GroupControl7.Location = New System.Drawing.Point(10, 200)
        Me.GroupControl7.Name = "GroupControl7"
        Me.GroupControl7.Size = New System.Drawing.Size(1209, 88)
        Me.GroupControl7.TabIndex = 89
        Me.GroupControl7.Text = "Risk"
        '
        'fldRisk
        '
        Me.fldRisk.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fldRisk.ButtonCustom = False
        Me.fldRisk.ButtonSQL = Nothing
        Me.fldRisk.ButtonText = ""
        Me.fldRisk.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Enter
        Me.fldRisk.ButtonWidth = 48.0!
        Me.fldRisk.FamilyID = Nothing
        Me.fldRisk.HideText = False
        Me.fldRisk.LabelText = "Label"
        Me.fldRisk.LabelWidth = 0!
        Me.fldRisk.Location = New System.Drawing.Point(5, 34)
        Me.fldRisk.MinimumSize = New System.Drawing.Size(210, 51)
        Me.fldRisk.Name = "fldRisk"
        Me.fldRisk.QuickTextList = Nothing
        Me.fldRisk.Size = New System.Drawing.Size(1199, 51)
        Me.fldRisk.TabIndex = 0
        Me.fldRisk.ValueDateTime = Nothing
        Me.fldRisk.ValueID = Nothing
        Me.fldRisk.ValueMaxLength = 0
        Me.fldRisk.ValueText = ""
        '
        'GroupControl8
        '
        Me.GroupControl8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl8.Controls.Add(Me.fldPeople)
        Me.GroupControl8.Location = New System.Drawing.Point(10, 294)
        Me.GroupControl8.Name = "GroupControl8"
        Me.GroupControl8.Size = New System.Drawing.Size(1209, 88)
        Me.GroupControl8.TabIndex = 89
        Me.GroupControl8.Text = "People Affected"
        '
        'fldPeople
        '
        Me.fldPeople.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fldPeople.ButtonCustom = False
        Me.fldPeople.ButtonSQL = Nothing
        Me.fldPeople.ButtonText = ""
        Me.fldPeople.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Enter
        Me.fldPeople.ButtonWidth = 48.0!
        Me.fldPeople.FamilyID = Nothing
        Me.fldPeople.HideText = False
        Me.fldPeople.LabelText = "Label"
        Me.fldPeople.LabelWidth = 0!
        Me.fldPeople.Location = New System.Drawing.Point(5, 34)
        Me.fldPeople.MinimumSize = New System.Drawing.Size(210, 51)
        Me.fldPeople.Name = "fldPeople"
        Me.fldPeople.QuickTextList = "Risk Assessment Who"
        Me.fldPeople.Size = New System.Drawing.Size(1199, 51)
        Me.fldPeople.TabIndex = 0
        Me.fldPeople.ValueDateTime = Nothing
        Me.fldPeople.ValueID = Nothing
        Me.fldPeople.ValueMaxLength = 0
        Me.fldPeople.ValueText = ""
        '
        'GroupControl9
        '
        Me.GroupControl9.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl9.Controls.Add(Me.fldControl)
        Me.GroupControl9.Location = New System.Drawing.Point(10, 389)
        Me.GroupControl9.Name = "GroupControl9"
        Me.GroupControl9.Size = New System.Drawing.Size(1209, 102)
        Me.GroupControl9.TabIndex = 90
        Me.GroupControl9.Text = "Control Measures"
        '
        'fldControl
        '
        Me.fldControl.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fldControl.ButtonCustom = False
        Me.fldControl.ButtonSQL = Nothing
        Me.fldControl.ButtonText = ""
        Me.fldControl.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Enter
        Me.fldControl.ButtonWidth = 48.0!
        Me.fldControl.FamilyID = Nothing
        Me.fldControl.HideText = False
        Me.fldControl.LabelText = "Label"
        Me.fldControl.LabelWidth = 0!
        Me.fldControl.Location = New System.Drawing.Point(5, 34)
        Me.fldControl.MinimumSize = New System.Drawing.Size(210, 51)
        Me.fldControl.Name = "fldControl"
        Me.fldControl.QuickTextList = Nothing
        Me.fldControl.Size = New System.Drawing.Size(1199, 65)
        Me.fldControl.TabIndex = 0
        Me.fldControl.ValueDateTime = Nothing
        Me.fldControl.ValueID = Nothing
        Me.fldControl.ValueMaxLength = 0
        Me.fldControl.ValueText = ""
        '
        'frmRiskItem
        '
        Me.ClientSize = New System.Drawing.Size(1231, 844)
        Me.Controls.Add(Me.GroupControl9)
        Me.Controls.Add(Me.GroupControl8)
        Me.Controls.Add(Me.GroupControl7)
        Me.Controls.Add(Me.GroupControl6)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnAccept)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.Name = "frmRiskItem"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        Me.Controls.SetChildIndex(Me.GroupControl2, 0)
        Me.Controls.SetChildIndex(Me.btnAccept, 0)
        Me.Controls.SetChildIndex(Me.btnCancel, 0)
        Me.Controls.SetChildIndex(Me.GroupControl3, 0)
        Me.Controls.SetChildIndex(Me.GroupControl4, 0)
        Me.Controls.SetChildIndex(Me.GroupControl6, 0)
        Me.Controls.SetChildIndex(Me.GroupControl7, 0)
        Me.Controls.SetChildIndex(Me.GroupControl8, 0)
        Me.Controls.SetChildIndex(Me.GroupControl9, 0)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.txtControlMeasures.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl6.ResumeLayout(False)
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl7.ResumeLayout(False)
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl8.ResumeLayout(False)
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl9.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents rcUncontrolled As RiskControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents rcControlled As RiskControl
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAccept As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents fldActivity As Field
    Friend WithEvents GroupControl6 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents fldHazard As Field
    Friend WithEvents GroupControl7 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents fldRisk As Field
    Friend WithEvents GroupControl8 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents fldPeople As Field
    Friend WithEvents GroupControl9 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents fldControl As Field
    Friend WithEvents txtControlMeasures As DevExpress.XtraEditors.MemoEdit
End Class
