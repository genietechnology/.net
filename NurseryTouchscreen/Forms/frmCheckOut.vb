﻿Option Strict On

Imports DevExpress.XtraGrid.Views.Grid

Public Class frmCheckOut

    Private m_ChildID As String = ""
    Private m_ChildName As String = ""
    Private m_FamilyID As String = ""

    Private m_CrossChecked As Boolean = False
    Private m_ChildIDQuotes As String = ""
    Private m_Siblings As New List(Of Sibling)

    Public Sub New(ByVal ChildID As String, ByVal ChildName As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_ChildID = ChildID
        m_ChildName = ChildName
        m_ChildIDQuotes = "'" + ChildID + "'"

    End Sub

    Private Sub frmCheckOut_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        txtChild.Text = m_ChildName
        m_FamilyID = Business.Child.ReturnFamilyID(m_ChildID)

        txtCheckedBy.Text = ""

        'have we cross-checked already?
        Dim _DR As DataRow = Nothing
        If SharedModule.ReturnActivityRow(Parameters.TodayID, m_ChildID, SharedModule.EnumActivityType.CrossCheck, _DR) Then
            m_CrossChecked = True
            txtCheckedBy.Text = _DR.Item("staff_name").ToString
            txtCheckedBy.BackColor = SharedModule.ColourGreen
        Else
            txtCheckedBy.Text = "Not Checked"
            txtCheckedBy.BackColor = SharedModule.ColourRed
        End If

        btnReport.Enabled = Parameters.UseWebServices

        txtChild.Text = m_ChildName
        SharedModule.ReturnFeedback(Parameters.TodayID, m_ChildID, txtFeedback.Text)

        PopulateSiblings()

        DisplayActivity()

    End Sub

    Private Sub PopulateSiblings()

        m_Siblings.Clear()

        Dim sqlfilter As String = ""
        Dim hideChildName As Boolean = Parameters.HideChildName

        If hideChildName Then
            sqlfilter = "select c.ID, c.knownas As 'Name',"
        Else
            sqlfilter = "Select c.ID, c.fullname As 'Name',"
        End If

        sqlfilter &= String.Concat(" r.location as 'Location',",
                                   " (select COUNT(*) from Incidents i where i.day_id = r.day_id And i.child_id = c.ID) as 'Incidents',",
                                   " (select COUNT(*) from MedicineLog m where m.day_id = r.day_id And m.child_id = c.ID) as 'Medication'",
                                   " from Children c",
                                   " left Join RegisterSummary r on r.person_id = c.ID",
                                   " where c.family_id = '", m_FamilyID, "'",
                                   " and c.ID <> '", m_ChildID, "'",
                                   " and r.day_id = '", Parameters.TodayID, "'")

        Dim table As DataTable = DAL.ReturnDataTable(sqlfilter)
        If table IsNot Nothing Then

            For Each row As DataRow In table.Rows

                Dim id As String = row.Item("ID").ToString
                Dim name As String = row.Item("Name").ToString
                Dim location As String = row.Item("Location").ToString
                Dim incidents As Integer = CInt(row.Item("Incidents"))
                Dim medication As Integer = CInt(row.Item("Medication"))

                m_Siblings.Add(New Sibling(id, name, location, incidents, medication))

            Next

        End If

        tgSibblings.HideFirstColumn = True
        tgSibblings.Populate(sqlfilter)

    End Sub

    Private Sub DisplayActivity()

        Me.Cursor = Cursors.WaitCursor
        Dim _SQL As String = ""

        '*******************************************************************************

        gbxToilet.Text = "Nappy Changes And Toilet Records"

        _SQL = ""
        _SQL += "Select description As ' ' from Activity"
        _SQL += " where day_id = '" + Parameters.TodayID + "'"
        _SQL += " and key_id = " + m_ChildIDQuotes
        _SQL += " and type = 'TOILET'"
        _SQL += " order by stamp desc"

        tgToilet.Clear()
        tgToilet.Populate(_SQL)

        '*******************************************************************************

        gbxSleep.Text = "Sleep Records"

        _SQL = ""
        _SQL += "select value_1 as ' ' from Activity"
        _SQL += " where day_id = '" + Parameters.TodayID + "'"
        _SQL += " and key_id = " + m_ChildIDQuotes
        _SQL += " and type = 'SLEEP'"
        _SQL += " order by stamp desc"

        tgSleep.Clear()
        tgSleep.Populate(_SQL)

        '*******************************************************************************

        gbxMilk.Text = "Milk Records"

        _SQL = ""
        _SQL += "select description as ' ' from Activity"
        _SQL += " where day_id = '" + Parameters.TodayID + "'"
        _SQL += " and key_id = " + m_ChildIDQuotes
        _SQL += " and type = 'MILK'"
        _SQL += " order by stamp desc"

        tgMilk.Clear()
        tgMilk.Populate(_SQL)

        '*******************************************************************************

        gbxIncidents.Text = "Incident Log"

        _SQL = ""
        _SQL += "select id as 'IncidentID', incident_type as 'Type', injury as 'Injury', details as 'Details' from Incidents"
        _SQL += " where day_id = '" + Parameters.TodayID + "'"
        _SQL += " and child_id = " + m_ChildIDQuotes
        _SQL += " order by stamp desc"

        tgIncidents.Clear()
        tgIncidents.HideFirstColumn = True
        tgIncidents.Populate(_SQL)

        '*******************************************************************************

        gbxMedication.Text = "Medication Log"

        _SQL = ""
        _SQL += "select id, illness as 'Illness', medicine as 'Medicine', dosage as 'Dosage', actual as 'Given' from MedicineLog"
        _SQL += " where day_id = '" + Parameters.TodayID + "'"
        _SQL += " and child_id = " + m_ChildIDQuotes
        _SQL += " and actual is not null"
        _SQL += " order by actual desc"

        tgMedication.Clear()
        tgMedication.HideFirstColumn = True
        tgMedication.Populate(_SQL)

        tgMedication.Columns("Given").DisplayFormat.FormatString = "HH:mm"

        '*******************************************************************************

        gbxRequests.Show()
        gbxRequests.Text = "Requests"

        _SQL = ""
        _SQL += "select description as ' ' from Activity"
        _SQL += " where day_id = '" + Parameters.TodayID + "'"
        _SQL += " and key_id = " + m_ChildIDQuotes
        _SQL += " and type = 'REQ'"
        _SQL += " order by stamp desc"

        tgReq.Clear()
        tgReq.Populate(_SQL)

        '*******************************************************************************

        gbxSuncream.Show()
        gbxSuncream.Text = "Suncream"

        _SQL = ""
        _SQL += "select description as ' ' from Activity"
        _SQL += " where day_id = '" + Parameters.TodayID + "'"
        _SQL += " and key_id = " + m_ChildIDQuotes
        _SQL += " and type = 'SUNCREAM'"
        _SQL += " order by stamp desc"

        tgSuncream.Clear()
        tgSuncream.Populate(_SQL)

        '*******************************************************************************

        Me.Cursor = Cursors.Default

    End Sub

    Private Sub FoodGrid(ByVal MealType As String, ByRef GridControl As TouchGrid)

        Dim _SQL As String = ""

        _SQL = ""
        _SQL += "select food_name as 'Food', food_status as 'Eaten' from FoodRegister"
        _SQL += " where day_id = '" + Parameters.TodayID + "'"
        _SQL += " and child_id = " + m_ChildIDQuotes
        _SQL += " and meal_type = '" + MealType + "'"
        _SQL += " order by food_name"

        GridControl.Clear()
        GridControl.HideFirstColumn = False
        GridControl.Populate(_SQL)

    End Sub

    Private Sub DisplayFood()

        Me.Cursor = Cursors.WaitCursor

        gbxToilet.Text = "Breakfast"
        FoodGrid("B", tgToilet)

        '*******************************************************************************

        gbxIncidents.Text = "Morning Snack"
        FoodGrid("S", tgIncidents)

        '*******************************************************************************

        gbxSleep.Text = "Lunch"
        FoodGrid("L", tgSleep)

        '*******************************************************************************

        gbxMilk.Text = "Lunch Dessert"
        FoodGrid("LD", tgMilk)

        '*******************************************************************************

        gbxMedication.Text = "Afternoon Snack"
        FoodGrid("SP", tgMedication)

        '*******************************************************************************

        gbxSuncream.Text = "Tea"
        FoodGrid("T", tgSuncream)

        '*******************************************************************************

        gbxRequests.Text = "Tea Dessert"
        FoodGrid("TD", tgReq)

        Me.Cursor = Cursors.Default

    End Sub

    Private Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnReport.Click

        If Parameters.UseWebServices Then

            SetCMDs(False)
            Me.Cursor = Cursors.AppStarting

            ServiceHandler.ViewReport(m_ChildID)

            SetCMDs(True)
            Me.Cursor = Cursors.Default

        End If

    End Sub

    Private Sub SetCMDs(ByVal Enabled As Boolean)

        If Enabled Then
            btnReport.Enabled = Parameters.UseWebServices
        Else
            btnReport.Enabled = False
        End If

        btnFood.Enabled = Enabled
        btnAccept.Enabled = Enabled
        btnCancel.Enabled = Enabled
        Application.DoEvents()

    End Sub

    Private Sub btnFood_Click(sender As Object, e As EventArgs) Handles btnFood.Click

        SetCMDs(False)

        If btnFood.Text = "Show Food" Then
            DisplayFood()
            btnFood.Text = "Show Activity"
        Else
            DisplayActivity()
            btnFood.Text = "Show Food"
        End If

        SetCMDs(True)

    End Sub

    Private Sub SubstituteStatus(sender As Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs)
        If e.Column.FieldName = "Eaten" Then
            If e.Value.ToString = "-2" Then e.DisplayText = "Not given"
            If e.Value.ToString = "0" Then e.DisplayText = "None"
            If e.Value.ToString = "1" Then e.DisplayText = "Some"
            If e.Value.ToString = "2" Then e.DisplayText = "Most"
            If e.Value.ToString = "3" Then e.DisplayText = "All"
            If e.Value.ToString = "4" Then e.DisplayText = "Half"
        End If
    End Sub

    Private Sub DrillDown(sender As Object, e As EventArgs)

        Dim _gv As GridView = CType(sender, GridView)
        If _gv IsNot Nothing Then

            If _gv.RowCount > 0 Then

                If _gv.Columns(0).FieldName = "IncidentID" Then

                    Dim _frmInc As New frmIncidentLog()
                    _frmInc.DrillDown(m_ChildID, _gv.GetRowCellValue(_gv.FocusedRowHandle, "IncidentID").ToString)
                    _frmInc.ShowDialog()

                    _frmInc.Dispose()
                    _frmInc = Nothing

                End If

            End If

        End If

    End Sub

#Region "Grid Events"

    Private Sub tgToilet_CustomColumnDisplayText(sender As Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles tgToilet.CustomColumnDisplayText
        SubstituteStatus(sender, e)
    End Sub

    Private Sub tgSleep_CustomColumnDisplayText(sender As Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles tgSleep.CustomColumnDisplayText
        SubstituteStatus(sender, e)
    End Sub

    Private Sub tgSleep_GridDoubleClick(sender As Object, e As EventArgs) Handles tgSleep.GridDoubleClick
        DrillDown(sender, e)
    End Sub

    Private Sub tgMilk_CustomColumnDisplayText(sender As Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles tgMilk.CustomColumnDisplayText
        SubstituteStatus(sender, e)
    End Sub

    Private Sub tgMilk_GridDoubleClick(sender As Object, e As EventArgs) Handles tgMilk.GridDoubleClick
        DrillDown(sender, e)
    End Sub

    Private Sub tgIncidents_CustomColumnDisplayText(sender As Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles tgIncidents.CustomColumnDisplayText
        SubstituteStatus(sender, e)
    End Sub

    Private Sub tgIncidents_GridDoubleClick(sender As Object, e As EventArgs) Handles tgIncidents.GridDoubleClick
        DrillDown(sender, e)
    End Sub

    Private Sub tgMedication_CustomColumnDisplayText(sender As Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles tgMedication.CustomColumnDisplayText
        SubstituteStatus(sender, e)
    End Sub

    Private Sub tgMedication_GridDoubleClick(sender As Object, e As EventArgs) Handles tgMedication.GridDoubleClick
        DrillDown(sender, e)
    End Sub

    Private Sub tgSuncream_CustomColumnDisplayText(sender As Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles tgSuncream.CustomColumnDisplayText
        SubstituteStatus(sender, e)
    End Sub

    Private Sub tgReq_CustomColumnDisplayText(sender As Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles tgReq.CustomColumnDisplayText
        SubstituteStatus(sender, e)
    End Sub

    Private Sub tgReq_GridDoubleClick(sender As Object, e As EventArgs) Handles tgReq.GridDoubleClick
        DrillDown(sender, e)
    End Sub

    Private Sub tgToilet_GridDoubleClick(sender As Object, e As EventArgs) Handles tgToilet.GridDoubleClick
        DrillDown(sender, e)
    End Sub

#End Region

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click
        SetCMDs(False)
        ProcessCheckOut()
        SetCMDs(True)
    End Sub

    Private Function AreWeCheckingOutSiblings() As Boolean

        If m_Siblings.Count > 0 Then

            Dim _Children As String = ""
            For Each _s In m_Siblings
                _Children += _s.ChildName + vbCrLf
            Next

            Dim _Mess As String = "Do you wish to Check Out the following siblings?" + vbCrLf + vbCrLf + _Children

            Dim _Result As DialogResult = Msgbox(_Mess, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Check Out")
            If _Result = DialogResult.Yes Then
                Return True
            End If

        End If

        Return False

    End Function

    Private Sub ProcessCheckOut()

        'ensure we have completed the cross checks if they are mandatory
        If Parameters.CrossCheckMandatory Then
            If Not m_CrossChecked Then
                Msgbox("Cross Check not completed - Check Out Cancelled.", MessageBoxIcon.Error, "Check Out")
                Exit Sub
            End If
        End If

        Dim _DoCheckOut As Boolean = False

        '***************************************************************************************************************************************************************

        Dim _CheckOutSiblings As Boolean = AreWeCheckingOutSiblings()
        Dim _SiblingIncidents As Integer = 0
        Dim _SiblingMedication As Integer = 0

        For Each _s In m_Siblings
            _SiblingIncidents += _s.IncidentCount
            _SiblingMedication += _s.MedicineCount
        Next

        '***************************************************************************************************************************************************************

        Dim _SignatureRequired As Boolean = False
        Dim _SignatureMessage As String = ""
        Dim _Signature As New Signature

        'check if this child was involved in an incident or accident
        Dim _MedicineGiven As Boolean = False
        _MedicineGiven = ChildHadMedicine(m_ChildID)
        If Not _MedicineGiven Then _MedicineGiven = ChildHadAdHocMedicine(m_ChildID)

        Dim _IncidentLogged As Boolean = ChildHadIncident(m_ChildID)

        If _MedicineGiven = True OrElse _IncidentLogged = True OrElse Parameters.ContactSignatureOut Then
            _SignatureRequired = True
        End If

        '***************************************************************************************************************************************************************

        If _CheckOutSiblings Then

            'if there are any incidents or accidents for any of the children checked-in, 
            'we prompt and warn the user that the signature captured will be used for all the children
            If _SiblingIncidents > 0 Or _SiblingMedication > 0 Then

                _SignatureRequired = True

                'medication only
                If _SiblingMedication > 0 AndAlso _SiblingIncidents = 0 Then
                    _SignatureMessage = "One or more of your Children was given medication today."
                End If

                'incident only
                If _SiblingMedication = 0 AndAlso _SiblingIncidents > 0 Then
                    _SignatureMessage = "One or more of your Children was involved in an incident today."
                End If

                'both
                If _SiblingMedication > 0 AndAlso _SiblingIncidents > 0 Then
                    _SignatureMessage = "One or more of your Children was involved in an incident or given medication today."
                End If

            End If

        End If

        '***************************************************************************************************************************************************************

        If _SignatureRequired AndAlso _SignatureMessage = "" Then

            If _MedicineGiven Or _IncidentLogged Then

                _SignatureRequired = True

                'medication only
                If _MedicineGiven = True AndAlso _IncidentLogged = False Then
                    _SignatureMessage = "Your child was given medication today."
                End If

                'incident only
                If _MedicineGiven = False AndAlso _IncidentLogged = True Then
                    _SignatureMessage = "Your child was involved in an incident today."
                End If

                'both
                If _MedicineGiven = True AndAlso _IncidentLogged = True Then
                    _SignatureMessage = "Your child was involved in an incident today and was also given medication."
                End If

            End If

        End If

        '***************************************************************************************************************************************************************

        If _SignatureRequired Then

            If _SignatureMessage <> "" Then _SignatureMessage += vbCrLf + vbCrLf
            _SignatureMessage += "Please tap OK so we can capture your signature..."

            Msgbox(_SignatureMessage, MessageBoxIcon.Exclamation, "Signature Required")

            _Signature.CaptureParentSignature(True, m_FamilyID)

            If _Signature.SignatureCaptured Then
                _DoCheckOut = True
            Else
                If Msgbox("Parent Signature declined. Do you want to specify another person collecting?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, "Signature Declined") = DialogResult.Yes Then
                    Dim _Name As String = SharedModule.OSK("Please enter the name of the person collecting")
                    If _Name <> "" Then
                        _Signature.CaptureVisitorSignature(True, Nothing, _Name)
                        If _Signature.SignatureCaptured Then
                            _DoCheckOut = True
                        End If
                    End If
                End If
            End If

        Else
            'so we don't need any signatures, we can just crack on
            _DoCheckOut = True
        End If

        '***************************************************************************************************************************************************************

        If _DoCheckOut Then

            If _CheckOutSiblings Then

                For Each _s In m_Siblings

                    Dim _Incidents As Boolean = False
                    Dim _Medication As Boolean = False

                    If _s.IncidentCount > 0 Then _Incidents = True
                    If _s.MedicineCount > 0 Then _Medication = True

                    CheckOutChild(_s.ChildID, _s.ChildName, _Signature, _Incidents, _Medication)

                Next

            End If

            'now check out the selected child
            CheckOutChild(m_ChildID, m_ChildName, _Signature, _IncidentLogged, _MedicineGiven)

            Me.DialogResult = DialogResult.OK

        Else
            Me.DialogResult = DialogResult.Cancel
        End If

        '***************************************************************************************************************************************************************

        Me.Close()

    End Sub

    Private Sub CheckOutChild(ByVal ChildID As String, ByVal ChildName As String, ByVal Signature As Signature, ByVal Incident As Boolean, ByVal Medicine As Boolean)

        'create a MEDSIG activity
        If Medicine Then
            UpdateMedicationAuth(ChildID, Signature)
            SharedModule.LogActivity(Parameters.TodayID, ChildID, ChildName, SharedModule.EnumActivityType.CheckOutMedication, "Contact Signature for Medication", Signature.SignatureID.ToString)
        End If

        'create a INCSIG activity
        If Incident Then
            UpdateIncident(ChildID, Signature)
            SharedModule.LogActivity(Parameters.TodayID, ChildID, ChildName, SharedModule.EnumActivityType.CheckOutIncident, "Contact Signature for Incident", Signature.SignatureID.ToString)
        End If

        SharedModule.RegisterTransaction(New Guid(ChildID), SharedModule.Enums.PersonType.Child, ChildName, SharedModule.Enums.InOut.CheckOut, SharedModule.Enums.RegisterVia.CheckOut)

        SendReports(ChildID)

    End Sub

    Private Function ChildCrossChecked(ByVal ChildID As String) As Boolean
        Return SharedModule.HasActivity(Parameters.TodayID, ChildID, SharedModule.EnumActivityType.CrossCheck)
    End Function

    Private Function ChildHadMedicine(ByVal ChildID As String) As Boolean
        Dim _Medicine As Integer = SharedModule.ReturnMedicineCount(Parameters.TodayID, ChildID)
        If _Medicine > 0 Then Return True
        Return False
    End Function

    Private Function ChildHadAdHocMedicine(ByVal ChildID As String) As Boolean
        Dim _Medicine As Integer = SharedModule.ReturnMedicineCount(Parameters.TodayID, ChildID, True)
        If _Medicine > 0 Then Return True
        Return False
    End Function

    Private Function ChildHadIncident(ByVal ChildID As String) As Boolean
        Dim _Incident As Integer = SharedModule.ReturnIncidentCount(Parameters.TodayID, ChildID)
        If _Incident > 0 Then Return True
        Return False
    End Function

    Private Sub SendReports(ByVal ChildID As String)
        If Parameters.UseWebServices Then
            ServiceHandler.SendReport(ChildID)
        End If
    End Sub

    Private Sub UpdateMedicationAuth(ByVal ChildID As String, ByRef SignatureCapture As Signature)

        Dim _SQL As String = ""
        _SQL += "select * from MedicineAuth"
        _SQL += " where day_id = '" + Parameters.TodayID + "'"
        _SQL += " and child_id = '" + ChildID + "'"

        Dim _DA As IDataAdapter = DAL.ReturnDataAdapter(_SQL)
        If Not _DA Is Nothing Then

            Dim _DS As New DataSet
            _DA.Fill(_DS)

            If _DS.Tables(0).Rows.Count > 0 Then

                For Each _DR As DataRow In _DS.Tables(0).Rows
                    _DR("collect_id") = SignatureCapture.PersonID
                    _DR("collect_name") = SignatureCapture.PersonName
                    _DR("collect_sig") = SignatureCapture.SecureSignatureData
                Next

                DAL.UpdateDB(_DA, _DS)

            End If

            _DS.Dispose()
            _DS = Nothing

            _DA = Nothing

        End If

    End Sub

    Private Sub UpdateIncident(ByVal ChildID As String, ByRef SignatureCapture As Signature)

        Dim _SQL As String = ""
        _SQL += "select * from Incidents"
        _SQL += " where day_id = '" + Parameters.TodayID + "'"
        _SQL += " and child_id = '" + ChildID + "'"

        Dim _DA As IDataAdapter = DAL.ReturnDataAdapter(_SQL)
        If Not _DA Is Nothing Then

            Dim _DS As New DataSet
            _DA.Fill(_DS)

            If _DS.Tables(0).Rows.Count > 0 Then

                For Each _DR As DataRow In _DS.Tables(0).Rows
                    _DR("sig_contact") = SignatureCapture.SignatureID
                Next

                DAL.UpdateDB(_DA, _DS)

            End If

            _DS.Dispose()
            _DS = Nothing

            _DA = Nothing

        End If

    End Sub

    Private Class Sibling

        Public Sub New(ByVal ChildIDIn As String, ByVal NameIn As String, ByVal LocationIn As String, ByVal IncidentsIn As Integer, ByVal MedicationIn As Integer)
            Me.ChildID = ChildIDIn
            Me.ChildName = NameIn
            Me.Location = LocationIn
            Me.IncidentCount = IncidentsIn
            Me.MedicineCount = MedicationIn
        End Sub

        Property ChildID As String
        Property ChildName As String
        Property Location As String
        Property IncidentCount As Integer
        Property MedicineCount As Integer
    End Class

End Class
