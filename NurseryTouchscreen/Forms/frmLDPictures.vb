﻿Imports System.IO
Imports NurseryTouchscreen.SharedModule

Public Class frmLDPictures

    Private m_ImagePath As String = ""

    Public Sub New(ByVal ImageFolder As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_ImagePath = ImageFolder

    End Sub

    Private Sub frmLDPictures_Load(sender As Object, e As EventArgs) Handles Me.Load
        For Each _f As String In IO.Directory.GetFiles(m_ImagePath)
            sldImage.Images.Add(Image.FromFile(_f))
        Next
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

End Class