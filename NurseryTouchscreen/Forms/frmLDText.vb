﻿Public Class frmLDText

    Private m_TextString As String
    Private m_TextArray As String()
    Private m_Max As Integer
    Private m_Position As Integer
    Private m_Colours As Boolean = False

    Private m_Font300 As Font = New Font("Tahoma", 250)
    Private m_Font200 As Font = New Font("Tahoma", 200)

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Public Sub Setup(ByVal TextString As String)

        m_TextString = TextString
        If m_TextString = "Colours" Then
            m_Colours = True
            TextString = "Red|Blue|Green|Yellow|Orange|Purple|Pink|Brown|Grey|Black|White"
            lblText.Font = m_Font200
        Else
            lblText.Font = m_Font300
        End If

        m_TextArray = TextString.Split("|")
        m_Max = m_TextArray.Length - 1
        m_Position = 0
        DisplayText()

    End Sub

    Private Sub DisplayText()

        lblText.Text = m_TextArray(m_Position)
        SetColours()
        SetButtons()

    End Sub

    Private Sub SetColours()

        lblText.BackColor = Color.White

        If m_Colours Then

            If lblText.Text = "Red" Then lblText.ForeColor = Color.Red
            If lblText.Text = "Blue" Then lblText.ForeColor = Color.Blue
            If lblText.Text = "Green" Then lblText.ForeColor = Color.Green
            If lblText.Text = "Orange" Then lblText.ForeColor = Color.Orange
            If lblText.Text = "Purple" Then lblText.ForeColor = Color.Purple
            If lblText.Text = "Pink" Then lblText.ForeColor = Color.Violet
            If lblText.Text = "Brown" Then lblText.ForeColor = Color.SaddleBrown
            If lblText.Text = "Grey" Then lblText.ForeColor = Color.LightSlateGray
            If lblText.Text = "Black" Then lblText.ForeColor = Color.Black

            If lblText.Text = "Yellow" Or lblText.Text = "White" Then
                If lblText.Text = "Yellow" Then lblText.ForeColor = Color.Yellow
                If lblText.Text = "White" Then lblText.ForeColor = Color.White
                lblText.BackColor = Color.Black
            End If

        Else
            lblText.ForeColor = Color.Black
        End If

    End Sub

    Private Sub SetButtons()
        If m_Position = 0 Then
            btnPrev.Enabled = False
            btnNext.Enabled = True
        Else
            If m_Position = m_Max Then
                btnPrev.Enabled = True
                btnNext.Enabled = False
            Else
                btnPrev.Enabled = True
                btnNext.Enabled = True
            End If
        End If
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub btnPrev_Click(sender As System.Object, e As System.EventArgs) Handles btnPrev.Click
        m_Position -= 1
        DisplayText()
    End Sub

    Private Sub btnNext_Click(sender As System.Object, e As System.EventArgs) Handles btnNext.Click
        m_Position += 1
        DisplayText()
    End Sub
End Class
