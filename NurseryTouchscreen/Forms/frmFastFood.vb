﻿Option Strict On

Imports DevExpress.XtraBars.Navigation
Imports DevExpress.XtraEditors
Imports NurseryTouchscreen.SharedModule

Public Class frmFastFood

    Private m_Loading As Boolean = False
    Private m_SelectedMealID As String = ""
    Private m_SelectedMealType As String = ""
    Private m_SelectedMealName As String = ""
    Private m_SelectedLocation As String = ""

    Private Sub SelectFirstMeal()
        If btnBreakfast.Tag.ToString <> "" Then SetMeal("B") : Exit Sub
        If btnAM.Tag.ToString <> "" Then SetMeal("S") : Exit Sub
        If btnLunch.Tag.ToString <> "" Then SetMeal("L") : Exit Sub
        If btnLunchDessert.Tag.ToString <> "" Then SetMeal("LD") : Exit Sub
        If btnPM.Tag.ToString <> "" Then SetMeal("SP") : Exit Sub
        If btnTea.Tag.ToString <> "" Then SetMeal("T") : Exit Sub
        If btnTeaDessert.Tag.ToString <> "" Then SetMeal("TD") : Exit Sub
    End Sub

    Private Sub frmFastFood_Load(sender As Object, e As EventArgs) Handles Me.Load

        m_Loading = True

        SetupMeals()

        SelectFirstMeal()

        PopulateGroups()

        m_Loading = False

        'populate children after loading flag to stop it loading twice!
        PopulateChildren()

    End Sub

    Private Sub frmFastFood_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        tbRooms.WideTileWidth = tbRooms.Width - 40
    End Sub

    Private Sub PopulateChildren()

        If m_Loading Then Exit Sub

        Me.Cursor = Cursors.WaitCursor
        Application.DoEvents()

        sc.SuspendLayout()

        sc.Controls.Clear()

        Dim _SQL As String = Business.Child.ReturnCheckedInSQL(m_SelectedLocation, Parameters.ShowPhotos)

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _ID As String = _DR.Item("ID").ToString
                Dim _Name As String = _DR.Item("name").ToString

                Dim _AllergyRating As String = _DR.Item("allergy_rating").ToString
                Dim _Allergies As String = _DR.Item("med_allergies").ToString
                Dim _Medication As String = _DR.Item("med_medication").ToString
                Dim _MedicalNotes As String = _DR.Item("med_notes").ToString
                Dim _MealStatus As String = ReturnMealStatus(_ID)

                AddChild(_ID, _Name, _MealStatus, _AllergyRating, _Allergies, _Medication, _MedicalNotes)

            Next

        End If

        sc.ResumeLayout()

        Me.Cursor = Cursors.Default
        Application.DoEvents()

    End Sub

    Private Sub AddChild(ByVal ChildID As String, ByVal ChildName As String, ByVal MealStatus As String, ByVal AllergyRating As String, ByVal MedAllergies As String, ByVal MedMedication As String, ByVal MedNotes As String)
        Dim _Item As New FoodControl(ChildID, ChildName, MealStatus)
        _Item.Dock = DockStyle.Top
        _Item.BringToFront()
        sc.Controls.Add(_Item)
    End Sub

    Private Function ReturnMealStatus(ByVal ChildID As String) As String

        Dim _Return As String = "0"

        If m_SelectedMealType <> "" AndAlso m_SelectedMealID <> "" Then

            Dim _SQL As String = ""
            _SQL += "select food_status from FoodRegister"
            _SQL += " where day_id = '" + TodayID + "'"
            _SQL += " and child_id = '" + ChildID + "'"
            _SQL += " and meal_type = '" + m_SelectedMealType + "'"
            _SQL += " and meal_id = '" + m_SelectedMealID + "'"

            Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
            If _DR IsNot Nothing Then
                _Return = _DR.Item("food_status").ToString
                _DR = Nothing
            End If

        End If

        Return _Return

    End Function

    Private Sub PopulateGroups()

        Dim _SQL As String = ""

        _SQL += "SELECT ID, room_name from SiteRooms"
        _SQL += " where site_id = '" + Parameters.SiteID + "'"
        _SQL += " and room_check_children = 1"
        _SQL += " order by room_sequence"

        PopulateTileBar(_SQL)

        If Parameters.DefaultRoom <> "" Then
            For Each _i As TileBarItem In tbRoomGroup.Items
                Dim _Text As String = _i.Elements(0).Text
                If _Text = Parameters.DefaultRoom Then
                    tbRooms.SelectedItem = _i
                    _i.PerformItemClick()
                    Exit Sub
                End If
            Next
        Else
            For Each _i As TileBarItem In tbRoomGroup.Items
                tbRooms.SelectedItem = _i
                _i.PerformItemClick()
                Exit Sub
            Next
        End If

    End Sub

    Private Sub PopulateTileBar(ByVal _SQL As String)

        tbRoomGroup.Items.Clear()

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _i As New TileBarItem
                With _i

                    .Name = "t_" + _DR.Item("ID").ToString

                    AddHandler _i.ItemClick, AddressOf GroupItemClick

                    .TextAlignment = TileItemContentAlignment.TopLeft
                    .AppearanceItem.Normal.ForeColor = Color.Black
                    .AppearanceItem.Normal.BackColor = txtLightYellow.BackColor
                    .AppearanceItem.Selected.BackColor = txtYellow.BackColor
                    .Text = _DR.Item("room_name").ToString

                End With

                tbRoomGroup.Items.Add(_i)

            Next

        End If

    End Sub

    Private Sub GroupItemClick(sender As Object, e As TileItemEventArgs)
        m_SelectedLocation = e.Item.Text
        Save()
        PopulateChildren()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click

        If ValidateEntry() Then

            Dim _StaffID As String = ""
            Dim _StaffName As String = ""

            If Parameters.CurrentStaffID <> "" Then
                _StaffID = Parameters.CurrentStaffID
                _StaffName = Parameters.CurrentStaffName
            Else
                Dim _Staff As Pair = Business.Staff.FindStaff(Enums.PersonMode.OnlyCheckedIn, False, Parameters.DefaultRoom)
                If _Staff IsNot Nothing Then
                    _StaffID = _Staff.Code
                    _StaffName = _Staff.Text
                End If
            End If

            If _StaffID <> "" Then
                Save()
                Me.DialogResult = DialogResult.OK
                Me.Close()
            End If

        End If

    End Sub

    Private Function ValidateEntry() As Boolean

        'For Each _T As ToiletControl In sc.Controls
        '    If _T.ToiletValue = "" Then
        '        Return False
        '    End If
        'Next

        Return True

    End Function

    Private Sub Save()

        If m_SelectedMealID = "" Then Exit Sub
        If m_SelectedMealType = "" Then Exit Sub
        If m_SelectedMealName = "" Then Exit Sub

        For Each _fc As FoodControl In sc.Controls

            LogFood(Parameters.TodayID, _fc.FoodID, _fc.FoodName,
                     m_SelectedMealID, m_SelectedMealType, m_SelectedMealName,
                     m_SelectedMealID, m_SelectedMealName, _fc.FoodStatus)

        Next

    End Sub

    Private Sub SetupMeals()

        Dim _SQL As String = "select * from Day where id = '" & TodayID & "'"

        Dim _dr As DataRow = DAL.ReturnDataRow(_SQL)
        If _dr IsNot Nothing Then

            Dim _BreakfastID As String = _dr.Item("breakfast_id").ToString
            If _BreakfastID <> "" Then
                btnBreakfast.Enabled = True
                btnBreakfast.Tag = _BreakfastID
                btnBreakfast.AccessibleDescription = _dr.Item("breakfast_name").ToString
            Else
                btnBreakfast.Enabled = False
                btnBreakfast.Tag = ""
                btnBreakfast.AccessibleDescription = ""
            End If

            Dim _SnackID As String = _dr.Item("snack_id").ToString
            If _SnackID <> "" Then
                btnAM.Enabled = True
                btnAM.Tag = _SnackID
                btnAM.AccessibleDescription = _dr.Item("snack_name").ToString
            Else
                btnAM.Enabled = False
                btnAM.Tag = ""
                btnAM.AccessibleDescription = ""
            End If

            Dim _LunchID As String = _dr.Item("lunch_id").ToString
            If _LunchID <> "" Then
                btnLunch.Enabled = True
                btnLunch.Tag = _LunchID
                btnLunch.AccessibleDescription = _dr.Item("lunch_name").ToString
            Else
                btnLunch.Enabled = False
                btnLunch.Tag = ""
                btnLunch.AccessibleDescription = ""
            End If

            Dim _LunchDessertID As String = _dr.Item("lunch_des_id").ToString
            If _LunchDessertID <> "" Then
                btnLunchDessert.Enabled = True
                btnLunchDessert.Tag = _LunchDessertID
                btnLunchDessert.AccessibleDescription = _dr.Item("lunch_des_name").ToString
            Else
                btnLunchDessert.Enabled = False
                btnLunchDessert.Tag = ""
                btnLunchDessert.AccessibleDescription = ""
            End If

            Dim _SnackPMID As String = _dr.Item("snack_pm_id").ToString
            If _SnackPMID <> "" Then
                btnPM.Enabled = True
                btnPM.Tag = _SnackPMID
                btnPM.AccessibleDescription = _dr.Item("snack_pm_name").ToString
            Else
                btnPM.Enabled = False
                btnPM.Tag = ""
                btnPM.AccessibleDescription = ""
            End If

            Dim _TeaID As String = _dr.Item("tea_id").ToString
            If _TeaID <> "" Then
                btnTea.Enabled = True
                btnTea.Tag = _TeaID
                btnTea.AccessibleDescription = _dr.Item("tea_name").ToString
            Else
                btnTea.Enabled = False
                btnTea.Tag = ""
                btnTea.AccessibleDescription = ""
            End If

            Dim _TeaDessertID As String = _dr.Item("tea_des_id").ToString
            If _TeaDessertID <> "" Then
                btnTeaDessert.Enabled = True
                btnTeaDessert.Tag = _TeaDessertID
                btnTeaDessert.AccessibleDescription = _dr.Item("tea_des_name").ToString
            Else
                btnTeaDessert.Enabled = False
                btnTeaDessert.Tag = ""
                btnTeaDessert.AccessibleDescription = ""
            End If

        End If

        _dr = Nothing

    End Sub

    Private Sub btnBreakfast_Click(sender As Object, e As EventArgs) Handles btnBreakfast.Click
        SetMeal("B")
    End Sub

    Private Sub btnAM_Click(sender As Object, e As EventArgs) Handles btnAM.Click
        SetMeal("S")
    End Sub

    Private Sub btnLunch_Click(sender As Object, e As EventArgs) Handles btnLunch.Click
        SetMeal("L")
    End Sub

    Private Sub btnLunchDessert_Click(sender As Object, e As EventArgs) Handles btnLunchDessert.Click
        SetMeal("LD")
    End Sub

    Private Sub btnPM_Click(sender As Object, e As EventArgs) Handles btnPM.Click
        SetMeal("SP")
    End Sub

    Private Sub btnTea_Click(sender As Object, e As EventArgs) Handles btnTea.Click
        SetMeal("T")
    End Sub

    Private Sub btnTeaDessert_Click(sender As Object, e As EventArgs) Handles btnTeaDessert.Click
        SetMeal("TD")
    End Sub

    Private Sub SetMeal(ByVal MealType As String)

        SetMealButtons(False)

        Me.Cursor = Cursors.WaitCursor
        Application.DoEvents()

        Save()

        m_SelectedMealType = MealType
        ResetMealButtons()

        Select Case MealType

            Case "B"
                m_SelectedMealID = btnBreakfast.Tag.ToString
                m_SelectedMealName = btnBreakfast.AccessibleDescription
                btnBreakfast.Appearance.BackColor = txtYellow.BackColor

            Case "S"
                m_SelectedMealID = btnAM.Tag.ToString
                m_SelectedMealName = btnAM.AccessibleDescription
                btnAM.Appearance.BackColor = txtYellow.BackColor

            Case "L"
                m_SelectedMealID = btnLunch.Tag.ToString
                m_SelectedMealName = btnLunch.AccessibleDescription
                btnLunch.Appearance.BackColor = txtYellow.BackColor

            Case "LD"
                m_SelectedMealID = btnLunchDessert.Tag.ToString
                m_SelectedMealName = btnLunchDessert.AccessibleDescription
                btnLunchDessert.Appearance.BackColor = txtYellow.BackColor

            Case "SP"
                m_SelectedMealID = btnPM.Tag.ToString
                m_SelectedMealName = btnPM.AccessibleDescription
                btnPM.Appearance.BackColor = txtYellow.BackColor

            Case "T"
                m_SelectedMealID = btnTea.Tag.ToString
                m_SelectedMealName = btnTea.AccessibleDescription
                btnTea.Appearance.BackColor = txtYellow.BackColor

            Case "TD"
                m_SelectedMealID = btnTeaDessert.Tag.ToString
                m_SelectedMealName = btnTeaDessert.AccessibleDescription
                btnTeaDessert.Appearance.BackColor = txtYellow.BackColor

        End Select

        lblMealDetails.Text = m_SelectedMealName
        PopulateChildren()

        SetMealButtons(True)

        Me.Cursor = Cursors.Default
        Application.DoEvents()

    End Sub

    Private Sub ResetMealButtons()
        ResetButton(btnBreakfast)
        ResetButton(btnAM)
        ResetButton(btnLunch)
        ResetButton(btnLunchDessert)
        ResetButton(btnPM)
        ResetButton(btnTea)
        ResetButton(btnTeaDessert)
    End Sub

    Private Sub ResetButton(ByRef Button As SimpleButton)
        Button.Appearance.Reset()
    End Sub

    Private Sub SetMealButtons(ByVal Enabled As Boolean)

        If Enabled Then
            If btnBreakfast.Tag.ToString <> "" Then btnBreakfast.Enabled = True
            If btnAM.Tag.ToString <> "" Then btnAM.Enabled = True
            If btnLunch.Tag.ToString <> "" Then btnLunch.Enabled = True
            If btnLunchDessert.Tag.ToString <> "" Then btnLunchDessert.Enabled = True
            If btnPM.Tag.ToString <> "" Then btnPM.Enabled = True
            If btnTea.Tag.ToString <> "" Then btnTea.Enabled = True
            If btnTeaDessert.Tag.ToString <> "" Then btnTeaDessert.Enabled = True
        Else
            btnBreakfast.Enabled = False
            btnAM.Enabled = False
            btnLunch.Enabled = False
            btnLunchDessert.Enabled = False
            btnPM.Enabled = False
            btnTea.Enabled = False
            btnTeaDessert.Enabled = False
        End If

    End Sub

End Class