﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.timCountdown = New System.Windows.Forms.Timer(Me.components)
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.btnPolicies = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCompliance = New DevExpress.XtraEditors.SimpleButton()
        Me.btnActivities = New DevExpress.XtraEditors.SimpleButton()
        Me.btnRegister = New DevExpress.XtraEditors.SimpleButton()
        Me.btnDoor1 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMedicine = New DevExpress.XtraEditors.SimpleButton()
        Me.btnLookupChild = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCheckOutStaff = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCheckOutChild = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCheckInChild = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCheckInStaff = New DevExpress.XtraEditors.SimpleButton()
        Me.btnToilet = New DevExpress.XtraEditors.SimpleButton()
        Me.btnFood = New DevExpress.XtraEditors.SimpleButton()
        Me.SplashScreenManager1 = New DevExpress.XtraSplashScreen.SplashScreenManager(Me, GetType(Global.NurseryTouchscreen.frmWait), True, True)
        Me.tlp = New System.Windows.Forms.TableLayoutPanel()
        Me.btnPhoto = New DevExpress.XtraEditors.SimpleButton()
        Me.btnHelp = New DevExpress.XtraEditors.SimpleButton()
        Me.btnOptions = New DevExpress.XtraEditors.SimpleButton()
        Me.btnLearn = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStaffCentre = New DevExpress.XtraEditors.SimpleButton()
        Me.btnExit = New DevExpress.XtraEditors.SimpleButton()
        Me.btnToiletTrip = New DevExpress.XtraEditors.SimpleButton()
        Me.btnPayments = New DevExpress.XtraEditors.SimpleButton()
        Me.btnRequests = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMilk = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSleep = New DevExpress.XtraEditors.SimpleButton()
        Me.btnBack = New DevExpress.XtraEditors.SimpleButton()
        Me.tlp.SuspendLayout()
        Me.SuspendLayout()
        '
        'timCountdown
        '
        Me.timCountdown.Interval = 60000
        '
        'BackgroundWorker1
        '
        '
        'btnPolicies
        '
        Me.btnPolicies.Appearance.Options.UseTextOptions = True
        Me.btnPolicies.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnPolicies.AutoSize = True
        Me.btnPolicies.AutoWidthInLayoutControl = True
        Me.btnPolicies.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnPolicies.Image = CType(resources.GetObject("btnPolicies.Image"), System.Drawing.Image)
        Me.btnPolicies.Location = New System.Drawing.Point(747, 414)
        Me.btnPolicies.Name = "btnPolicies"
        Me.btnPolicies.Size = New System.Drawing.Size(242, 131)
        Me.btnPolicies.TabIndex = 24
        Me.btnPolicies.Tag = ""
        Me.btnPolicies.Text = "Policies &&" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Procedures"
        '
        'btnCompliance
        '
        Me.btnCompliance.Appearance.Options.UseTextOptions = True
        Me.btnCompliance.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnCompliance.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnCompliance.Image = Global.NurseryTouchscreen.My.Resources.Resources.Test_paper_64
        Me.btnCompliance.Location = New System.Drawing.Point(747, 140)
        Me.btnCompliance.Name = "btnCompliance"
        Me.btnCompliance.Size = New System.Drawing.Size(242, 131)
        Me.btnCompliance.TabIndex = 7
        Me.btnCompliance.Tag = "SC"
        Me.btnCompliance.Text = "Compliance"
        '
        'btnActivities
        '
        Me.btnActivities.Appearance.Options.UseTextOptions = True
        Me.btnActivities.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnActivities.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnActivities.Image = CType(resources.GetObject("btnActivities.Image"), System.Drawing.Image)
        Me.btnActivities.Location = New System.Drawing.Point(747, 3)
        Me.btnActivities.Name = "btnActivities"
        Me.btnActivities.Size = New System.Drawing.Size(242, 131)
        Me.btnActivities.TabIndex = 3
        Me.btnActivities.Tag = "S"
        Me.btnActivities.Text = "Today's" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Activities"
        '
        'btnRegister
        '
        Me.btnRegister.Appearance.Options.UseTextOptions = True
        Me.btnRegister.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnRegister.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnRegister.Image = CType(resources.GetObject("btnRegister.Image"), System.Drawing.Image)
        Me.btnRegister.Location = New System.Drawing.Point(499, 3)
        Me.btnRegister.Name = "btnRegister"
        Me.btnRegister.Size = New System.Drawing.Size(242, 131)
        Me.btnRegister.TabIndex = 2
        Me.btnRegister.Tag = "SCD"
        Me.btnRegister.Text = "Register"
        '
        'btnDoor1
        '
        Me.btnDoor1.Appearance.Options.UseTextOptions = True
        Me.btnDoor1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnDoor1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnDoor1.Image = CType(resources.GetObject("btnDoor1.Image"), System.Drawing.Image)
        Me.btnDoor1.Location = New System.Drawing.Point(995, 3)
        Me.btnDoor1.Name = "btnDoor1"
        Me.btnDoor1.Size = New System.Drawing.Size(242, 131)
        Me.btnDoor1.TabIndex = 4
        Me.btnDoor1.Tag = ""
        Me.btnDoor1.Text = "Front Door"
        '
        'btnMedicine
        '
        Me.btnMedicine.Appearance.Options.UseTextOptions = True
        Me.btnMedicine.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnMedicine.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnMedicine.Image = CType(resources.GetObject("btnMedicine.Image"), System.Drawing.Image)
        Me.btnMedicine.Location = New System.Drawing.Point(747, 277)
        Me.btnMedicine.Name = "btnMedicine"
        Me.btnMedicine.Size = New System.Drawing.Size(242, 131)
        Me.btnMedicine.TabIndex = 18
        Me.btnMedicine.Tag = "SCD"
        Me.btnMedicine.Text = "Medicine &&" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Incidents"
        '
        'btnLookupChild
        '
        Me.btnLookupChild.Appearance.Options.UseTextOptions = True
        Me.btnLookupChild.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnLookupChild.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnLookupChild.Image = CType(resources.GetObject("btnLookupChild.Image"), System.Drawing.Image)
        Me.btnLookupChild.Location = New System.Drawing.Point(995, 140)
        Me.btnLookupChild.Name = "btnLookupChild"
        Me.btnLookupChild.Size = New System.Drawing.Size(242, 131)
        Me.btnLookupChild.TabIndex = 8
        Me.btnLookupChild.Tag = "SC"
        Me.btnLookupChild.Text = "Lookup Child"
        '
        'btnCheckOutStaff
        '
        Me.btnCheckOutStaff.Appearance.Options.UseTextOptions = True
        Me.btnCheckOutStaff.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnCheckOutStaff.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnCheckOutStaff.Image = CType(resources.GetObject("btnCheckOutStaff.Image"), System.Drawing.Image)
        Me.btnCheckOutStaff.Location = New System.Drawing.Point(251, 140)
        Me.btnCheckOutStaff.Name = "btnCheckOutStaff"
        Me.btnCheckOutStaff.Size = New System.Drawing.Size(242, 131)
        Me.btnCheckOutStaff.TabIndex = 6
        Me.btnCheckOutStaff.Tag = "SC"
        Me.btnCheckOutStaff.Text = "Check Out" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Staff"
        '
        'btnCheckOutChild
        '
        Me.btnCheckOutChild.Appearance.Options.UseTextOptions = True
        Me.btnCheckOutChild.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnCheckOutChild.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnCheckOutChild.Image = CType(resources.GetObject("btnCheckOutChild.Image"), System.Drawing.Image)
        Me.btnCheckOutChild.Location = New System.Drawing.Point(251, 3)
        Me.btnCheckOutChild.Name = "btnCheckOutChild"
        Me.btnCheckOutChild.Size = New System.Drawing.Size(242, 131)
        Me.btnCheckOutChild.TabIndex = 1
        Me.btnCheckOutChild.Tag = "SC"
        Me.btnCheckOutChild.Text = "Check Out" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Child"
        '
        'btnCheckInChild
        '
        Me.btnCheckInChild.Appearance.Options.UseTextOptions = True
        Me.btnCheckInChild.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnCheckInChild.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnCheckInChild.Image = CType(resources.GetObject("btnCheckInChild.Image"), System.Drawing.Image)
        Me.btnCheckInChild.Location = New System.Drawing.Point(3, 3)
        Me.btnCheckInChild.Name = "btnCheckInChild"
        Me.btnCheckInChild.Size = New System.Drawing.Size(242, 131)
        Me.btnCheckInChild.TabIndex = 0
        Me.btnCheckInChild.Tag = "SC"
        Me.btnCheckInChild.Text = "Check In" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Child"
        '
        'btnCheckInStaff
        '
        Me.btnCheckInStaff.Appearance.Options.UseTextOptions = True
        Me.btnCheckInStaff.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnCheckInStaff.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnCheckInStaff.Image = CType(resources.GetObject("btnCheckInStaff.Image"), System.Drawing.Image)
        Me.btnCheckInStaff.Location = New System.Drawing.Point(3, 140)
        Me.btnCheckInStaff.Name = "btnCheckInStaff"
        Me.btnCheckInStaff.Size = New System.Drawing.Size(242, 131)
        Me.btnCheckInStaff.TabIndex = 5
        Me.btnCheckInStaff.Tag = ""
        Me.btnCheckInStaff.Text = "Check In" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Staff"
        '
        'btnToilet
        '
        Me.btnToilet.Appearance.Options.UseTextOptions = True
        Me.btnToilet.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnToilet.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnToilet.Image = CType(resources.GetObject("btnToilet.Image"), System.Drawing.Image)
        Me.btnToilet.Location = New System.Drawing.Point(3, 414)
        Me.btnToilet.Name = "btnToilet"
        Me.btnToilet.Size = New System.Drawing.Size(242, 131)
        Me.btnToilet.TabIndex = 10
        Me.btnToilet.Tag = "S"
        Me.btnToilet.Text = "Toilet Records"
        '
        'btnFood
        '
        Me.btnFood.Appearance.Options.UseTextOptions = True
        Me.btnFood.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnFood.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnFood.Image = CType(resources.GetObject("btnFood.Image"), System.Drawing.Image)
        Me.btnFood.Location = New System.Drawing.Point(251, 414)
        Me.btnFood.Name = "btnFood"
        Me.btnFood.Size = New System.Drawing.Size(242, 131)
        Me.btnFood.TabIndex = 11
        Me.btnFood.Tag = "S"
        Me.btnFood.Text = "Food"
        '
        'tlp
        '
        Me.tlp.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tlp.BackColor = System.Drawing.Color.Transparent
        Me.tlp.ColumnCount = 5
        Me.tlp.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.tlp.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.tlp.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.tlp.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.tlp.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.tlp.Controls.Add(Me.btnPhoto, 4, 2)
        Me.tlp.Controls.Add(Me.btnHelp, 2, 4)
        Me.tlp.Controls.Add(Me.btnOptions, 3, 4)
        Me.tlp.Controls.Add(Me.btnLearn, 1, 4)
        Me.tlp.Controls.Add(Me.btnStaffCentre, 2, 1)
        Me.tlp.Controls.Add(Me.btnExit, 4, 3)
        Me.tlp.Controls.Add(Me.btnToiletTrip, 0, 4)
        Me.tlp.Controls.Add(Me.btnPayments, 2, 2)
        Me.tlp.Controls.Add(Me.btnRequests, 2, 3)
        Me.tlp.Controls.Add(Me.btnMilk, 1, 2)
        Me.tlp.Controls.Add(Me.btnSleep, 0, 2)
        Me.tlp.Controls.Add(Me.btnCheckInChild, 0, 0)
        Me.tlp.Controls.Add(Me.btnBack, 4, 4)
        Me.tlp.Controls.Add(Me.btnPolicies, 3, 3)
        Me.tlp.Controls.Add(Me.btnCheckOutChild, 1, 0)
        Me.tlp.Controls.Add(Me.btnCompliance, 3, 1)
        Me.tlp.Controls.Add(Me.btnRegister, 2, 0)
        Me.tlp.Controls.Add(Me.btnFood, 1, 3)
        Me.tlp.Controls.Add(Me.btnActivities, 3, 0)
        Me.tlp.Controls.Add(Me.btnToilet, 0, 3)
        Me.tlp.Controls.Add(Me.btnMedicine, 3, 2)
        Me.tlp.Controls.Add(Me.btnDoor1, 4, 0)
        Me.tlp.Controls.Add(Me.btnCheckInStaff, 0, 1)
        Me.tlp.Controls.Add(Me.btnCheckOutStaff, 1, 1)
        Me.tlp.Controls.Add(Me.btnLookupChild, 4, 1)
        Me.tlp.Location = New System.Drawing.Point(12, 12)
        Me.tlp.Name = "tlp"
        Me.tlp.RowCount = 5
        Me.tlp.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.tlp.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.tlp.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.tlp.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.tlp.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.tlp.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlp.Size = New System.Drawing.Size(1240, 687)
        Me.tlp.TabIndex = 25
        '
        'btnPhoto
        '
        Me.btnPhoto.Appearance.Options.UseTextOptions = True
        Me.btnPhoto.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnPhoto.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnPhoto.Image = Global.NurseryTouchscreen.My.Resources.Resources.camera_64
        Me.btnPhoto.Location = New System.Drawing.Point(995, 277)
        Me.btnPhoto.Name = "btnPhoto"
        Me.btnPhoto.Size = New System.Drawing.Size(242, 131)
        Me.btnPhoto.TabIndex = 39
        Me.btnPhoto.Tag = ""
        Me.btnPhoto.Text = "Take" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Photo"
        '
        'btnHelp
        '
        Me.btnHelp.Appearance.Options.UseTextOptions = True
        Me.btnHelp.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnHelp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnHelp.Image = CType(resources.GetObject("btnHelp.Image"), System.Drawing.Image)
        Me.btnHelp.Location = New System.Drawing.Point(499, 551)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(242, 133)
        Me.btnHelp.TabIndex = 38
        Me.btnHelp.Tag = ""
        Me.btnHelp.Text = "Help!"
        '
        'btnOptions
        '
        Me.btnOptions.Appearance.Options.UseTextOptions = True
        Me.btnOptions.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnOptions.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnOptions.Image = CType(resources.GetObject("btnOptions.Image"), System.Drawing.Image)
        Me.btnOptions.Location = New System.Drawing.Point(747, 551)
        Me.btnOptions.Name = "btnOptions"
        Me.btnOptions.Size = New System.Drawing.Size(242, 133)
        Me.btnOptions.TabIndex = 37
        Me.btnOptions.Tag = "SC"
        Me.btnOptions.Text = "Settings"
        '
        'btnLearn
        '
        Me.btnLearn.Appearance.Options.UseTextOptions = True
        Me.btnLearn.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnLearn.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnLearn.Image = CType(resources.GetObject("btnLearn.Image"), System.Drawing.Image)
        Me.btnLearn.Location = New System.Drawing.Point(251, 551)
        Me.btnLearn.Name = "btnLearn"
        Me.btnLearn.Size = New System.Drawing.Size(242, 133)
        Me.btnLearn.TabIndex = 36
        Me.btnLearn.Tag = "S"
        Me.btnLearn.Text = "Learning"
        '
        'btnStaffCentre
        '
        Me.btnStaffCentre.Appearance.Options.UseTextOptions = True
        Me.btnStaffCentre.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnStaffCentre.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnStaffCentre.Image = Global.NurseryTouchscreen.My.Resources.Resources.female_user_info_64
        Me.btnStaffCentre.Location = New System.Drawing.Point(499, 140)
        Me.btnStaffCentre.Name = "btnStaffCentre"
        Me.btnStaffCentre.Size = New System.Drawing.Size(242, 131)
        Me.btnStaffCentre.TabIndex = 35
        Me.btnStaffCentre.Tag = ""
        Me.btnStaffCentre.Text = "Staff Centre"
        '
        'btnExit
        '
        Me.btnExit.Appearance.Options.UseTextOptions = True
        Me.btnExit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnExit.DialogResult = DialogResult.Cancel
        Me.btnExit.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnExit.Image = Global.NurseryTouchscreen.My.Resources.Resources.Right_64
        Me.btnExit.Location = New System.Drawing.Point(995, 414)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(242, 131)
        Me.btnExit.TabIndex = 34
        Me.btnExit.Tag = ""
        Me.btnExit.Text = "Exit" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Application"
        '
        'btnToiletTrip
        '
        Me.btnToiletTrip.Appearance.Options.UseTextOptions = True
        Me.btnToiletTrip.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnToiletTrip.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnToiletTrip.Image = CType(resources.GetObject("btnToiletTrip.Image"), System.Drawing.Image)
        Me.btnToiletTrip.Location = New System.Drawing.Point(3, 551)
        Me.btnToiletTrip.Name = "btnToiletTrip"
        Me.btnToiletTrip.Size = New System.Drawing.Size(242, 133)
        Me.btnToiletTrip.TabIndex = 32
        Me.btnToiletTrip.Tag = "S"
        Me.btnToiletTrip.Text = "Toilet Trip"
        '
        'btnPayments
        '
        Me.btnPayments.Appearance.Options.UseTextOptions = True
        Me.btnPayments.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnPayments.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnPayments.Image = Global.NurseryTouchscreen.My.Resources.Resources.Pound_64
        Me.btnPayments.Location = New System.Drawing.Point(499, 277)
        Me.btnPayments.Name = "btnPayments"
        Me.btnPayments.Size = New System.Drawing.Size(242, 131)
        Me.btnPayments.TabIndex = 31
        Me.btnPayments.Tag = "SC"
        Me.btnPayments.Text = "Payments"
        '
        'btnRequests
        '
        Me.btnRequests.Appearance.Options.UseTextOptions = True
        Me.btnRequests.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnRequests.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnRequests.Image = CType(resources.GetObject("btnRequests.Image"), System.Drawing.Image)
        Me.btnRequests.Location = New System.Drawing.Point(499, 414)
        Me.btnRequests.Name = "btnRequests"
        Me.btnRequests.Size = New System.Drawing.Size(242, 131)
        Me.btnRequests.TabIndex = 27
        Me.btnRequests.Tag = "SC"
        Me.btnRequests.Text = "Request Centre"
        '
        'btnMilk
        '
        Me.btnMilk.Appearance.Options.UseTextOptions = True
        Me.btnMilk.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnMilk.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnMilk.Image = CType(resources.GetObject("btnMilk.Image"), System.Drawing.Image)
        Me.btnMilk.Location = New System.Drawing.Point(251, 277)
        Me.btnMilk.Name = "btnMilk"
        Me.btnMilk.Size = New System.Drawing.Size(242, 131)
        Me.btnMilk.TabIndex = 26
        Me.btnMilk.Tag = "S"
        Me.btnMilk.Text = "Milk Records"
        '
        'btnSleep
        '
        Me.btnSleep.Appearance.Options.UseTextOptions = True
        Me.btnSleep.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnSleep.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnSleep.Image = CType(resources.GetObject("btnSleep.Image"), System.Drawing.Image)
        Me.btnSleep.Location = New System.Drawing.Point(3, 277)
        Me.btnSleep.Name = "btnSleep"
        Me.btnSleep.Size = New System.Drawing.Size(242, 131)
        Me.btnSleep.TabIndex = 25
        Me.btnSleep.Tag = "S"
        Me.btnSleep.Text = "Sleep" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Management"
        '
        'btnBack
        '
        Me.btnBack.Appearance.Options.UseTextOptions = True
        Me.btnBack.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnBack.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnBack.Image = Global.NurseryTouchscreen.My.Resources.Resources.Undo_64
        Me.btnBack.Location = New System.Drawing.Point(995, 551)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(242, 133)
        Me.btnBack.TabIndex = 22
        Me.btnBack.Tag = ""
        Me.btnBack.Text = "Return"
        '
        'frmMain
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.CancelButton = Me.btnExit
        Me.ClientSize = New System.Drawing.Size(1264, 711)
        Me.Controls.Add(Me.tlp)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(1280, 750)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(1280, 750)
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Nursery Genie"
        Me.tlp.ResumeLayout(False)
        Me.tlp.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnLookupChild As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCheckOutStaff As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCheckOutChild As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCheckInStaff As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnToilet As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnFood As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCheckInChild As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMedicine As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents timCountdown As System.Windows.Forms.Timer
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents btnDoor1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRegister As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnActivities As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCompliance As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnPolicies As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SplashScreenManager1 As DevExpress.XtraSplashScreen.SplashScreenManager
    Friend WithEvents tlp As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnRequests As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMilk As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSleep As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnPayments As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnToiletTrip As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnBack As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnExit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStaffCentre As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnPhoto As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnHelp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnOptions As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnLearn As DevExpress.XtraEditors.SimpleButton
End Class
