﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRiskHeader
    Inherits NurseryTouchscreen.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRiskHeader))
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.fldTitle = New NurseryTouchscreen.Field()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.fldArea = New NurseryTouchscreen.Field()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.fldStatus = New NurseryTouchscreen.Field()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.fldRevision = New NurseryTouchscreen.Field()
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.fldFrequency = New NurseryTouchscreen.Field()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAccept = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl4
        '
        Me.GroupControl4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl4.Controls.Add(Me.fldTitle)
        Me.GroupControl4.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(993, 88)
        Me.GroupControl4.TabIndex = 89
        Me.GroupControl4.Text = "Title"
        '
        'fldTitle
        '
        Me.fldTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fldTitle.ButtonCustom = False
        Me.fldTitle.ButtonSQL = Nothing
        Me.fldTitle.ButtonText = ""
        Me.fldTitle.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Enter
        Me.fldTitle.ButtonWidth = 48.0!
        Me.fldTitle.FamilyID = Nothing
        Me.fldTitle.HideText = False
        Me.fldTitle.LabelText = "Label"
        Me.fldTitle.LabelWidth = 0!
        Me.fldTitle.Location = New System.Drawing.Point(5, 34)
        Me.fldTitle.MinimumSize = New System.Drawing.Size(210, 51)
        Me.fldTitle.Name = "fldTitle"
        Me.fldTitle.QuickTextList = Nothing
        Me.fldTitle.Size = New System.Drawing.Size(983, 51)
        Me.fldTitle.TabIndex = 0
        Me.fldTitle.ValueDateTime = Nothing
        Me.fldTitle.ValueID = Nothing
        Me.fldTitle.ValueMaxLength = 100
        Me.fldTitle.ValueText = ""
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.fldArea)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 106)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(993, 88)
        Me.GroupControl1.TabIndex = 90
        Me.GroupControl1.Text = "Area"
        '
        'fldArea
        '
        Me.fldArea.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fldArea.ButtonCustom = False
        Me.fldArea.ButtonSQL = Nothing
        Me.fldArea.ButtonText = ""
        Me.fldArea.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Enter
        Me.fldArea.ButtonWidth = 48.0!
        Me.fldArea.FamilyID = Nothing
        Me.fldArea.HideText = False
        Me.fldArea.LabelText = "Label"
        Me.fldArea.LabelWidth = 0!
        Me.fldArea.Location = New System.Drawing.Point(5, 34)
        Me.fldArea.MinimumSize = New System.Drawing.Size(210, 51)
        Me.fldArea.Name = "fldArea"
        Me.fldArea.QuickTextList = Nothing
        Me.fldArea.Size = New System.Drawing.Size(983, 51)
        Me.fldArea.TabIndex = 0
        Me.fldArea.ValueDateTime = Nothing
        Me.fldArea.ValueID = Nothing
        Me.fldArea.ValueMaxLength = 30
        Me.fldArea.ValueText = ""
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl2.Controls.Add(Me.fldStatus)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 200)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(993, 88)
        Me.GroupControl2.TabIndex = 90
        Me.GroupControl2.Text = "Status"
        '
        'fldStatus
        '
        Me.fldStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fldStatus.ButtonCustom = True
        Me.fldStatus.ButtonSQL = Nothing
        Me.fldStatus.ButtonText = ""
        Me.fldStatus.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Lookup
        Me.fldStatus.ButtonWidth = 48.0!
        Me.fldStatus.FamilyID = Nothing
        Me.fldStatus.HideText = False
        Me.fldStatus.LabelText = "Label"
        Me.fldStatus.LabelWidth = 0!
        Me.fldStatus.Location = New System.Drawing.Point(5, 34)
        Me.fldStatus.MinimumSize = New System.Drawing.Size(210, 51)
        Me.fldStatus.Name = "fldStatus"
        Me.fldStatus.QuickTextList = Nothing
        Me.fldStatus.Size = New System.Drawing.Size(983, 51)
        Me.fldStatus.TabIndex = 0
        Me.fldStatus.ValueDateTime = Nothing
        Me.fldStatus.ValueID = Nothing
        Me.fldStatus.ValueMaxLength = 0
        Me.fldStatus.ValueText = ""
        '
        'GroupControl3
        '
        Me.GroupControl3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl3.Controls.Add(Me.fldRevision)
        Me.GroupControl3.Location = New System.Drawing.Point(12, 294)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(993, 88)
        Me.GroupControl3.TabIndex = 90
        Me.GroupControl3.Text = "Revision"
        '
        'fldRevision
        '
        Me.fldRevision.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fldRevision.ButtonCustom = False
        Me.fldRevision.ButtonSQL = Nothing
        Me.fldRevision.ButtonText = ""
        Me.fldRevision.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Number
        Me.fldRevision.ButtonWidth = 48.0!
        Me.fldRevision.FamilyID = Nothing
        Me.fldRevision.HideText = False
        Me.fldRevision.LabelText = "Label"
        Me.fldRevision.LabelWidth = 0!
        Me.fldRevision.Location = New System.Drawing.Point(5, 34)
        Me.fldRevision.MinimumSize = New System.Drawing.Size(210, 51)
        Me.fldRevision.Name = "fldRevision"
        Me.fldRevision.QuickTextList = Nothing
        Me.fldRevision.Size = New System.Drawing.Size(983, 51)
        Me.fldRevision.TabIndex = 0
        Me.fldRevision.ValueDateTime = Nothing
        Me.fldRevision.ValueID = Nothing
        Me.fldRevision.ValueMaxLength = 0
        Me.fldRevision.ValueText = ""
        '
        'GroupControl5
        '
        Me.GroupControl5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl5.Controls.Add(Me.fldFrequency)
        Me.GroupControl5.Location = New System.Drawing.Point(12, 388)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(993, 88)
        Me.GroupControl5.TabIndex = 90
        Me.GroupControl5.Text = "Frequency"
        '
        'fldFrequency
        '
        Me.fldFrequency.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fldFrequency.ButtonCustom = True
        Me.fldFrequency.ButtonSQL = Nothing
        Me.fldFrequency.ButtonText = ""
        Me.fldFrequency.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Lookup
        Me.fldFrequency.ButtonWidth = 48.0!
        Me.fldFrequency.FamilyID = Nothing
        Me.fldFrequency.HideText = False
        Me.fldFrequency.LabelText = "Label"
        Me.fldFrequency.LabelWidth = 0!
        Me.fldFrequency.Location = New System.Drawing.Point(5, 34)
        Me.fldFrequency.MinimumSize = New System.Drawing.Size(210, 51)
        Me.fldFrequency.Name = "fldFrequency"
        Me.fldFrequency.QuickTextList = Nothing
        Me.fldFrequency.Size = New System.Drawing.Size(983, 51)
        Me.fldFrequency.TabIndex = 0
        Me.fldFrequency.ValueDateTime = Nothing
        Me.fldFrequency.ValueID = Nothing
        Me.fldFrequency.ValueMaxLength = 0
        Me.fldFrequency.ValueText = ""
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Image = CType(resources.GetObject("btnCancel.Image"), System.Drawing.Image)
        Me.btnCancel.Location = New System.Drawing.Point(860, 486)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(145, 57)
        Me.btnCancel.TabIndex = 92
        Me.btnCancel.Text = "Cancel"
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAccept.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAccept.Appearance.Options.UseFont = True
        Me.btnAccept.Image = CType(resources.GetObject("btnAccept.Image"), System.Drawing.Image)
        Me.btnAccept.Location = New System.Drawing.Point(709, 486)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(145, 57)
        Me.btnAccept.TabIndex = 91
        Me.btnAccept.Text = "Accept"
        '
        'frmRiskHeader
        '
        Me.ClientSize = New System.Drawing.Size(1017, 555)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnAccept)
        Me.Controls.Add(Me.GroupControl5)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.GroupControl4)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmRiskHeader"
        Me.Controls.SetChildIndex(Me.GroupControl4, 0)
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        Me.Controls.SetChildIndex(Me.GroupControl2, 0)
        Me.Controls.SetChildIndex(Me.GroupControl3, 0)
        Me.Controls.SetChildIndex(Me.GroupControl5, 0)
        Me.Controls.SetChildIndex(Me.btnAccept, 0)
        Me.Controls.SetChildIndex(Me.btnCancel, 0)
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents fldTitle As Field
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents fldArea As Field
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents fldStatus As Field
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents fldRevision As Field
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents fldFrequency As Field
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAccept As DevExpress.XtraEditors.SimpleButton
End Class
