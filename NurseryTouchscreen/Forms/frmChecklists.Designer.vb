﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmChecklists

    Inherits frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnClose = New DevExpress.XtraEditors.SimpleButton()
        Me.btnComplete = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.tbLists = New DevExpress.XtraBars.Navigation.TileBar()
        Me.tbListGroup = New DevExpress.XtraBars.Navigation.TileBarGroup()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.radComplete = New DevExpress.XtraEditors.CheckEdit()
        Me.radPending = New DevExpress.XtraEditors.CheckEdit()
        Me.timSleepCheck = New System.Windows.Forms.Timer(Me.components)
        Me.sc = New DevExpress.XtraEditors.XtraScrollableControl()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.radComplete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radPending.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(718, 404)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(200, 45)
        Me.btnClose.TabIndex = 84
        Me.btnClose.Text = "Close"
        '
        'btnComplete
        '
        Me.btnComplete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnComplete.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnComplete.Appearance.Options.UseFont = True
        Me.btnComplete.Image = Global.NurseryTouchscreen.My.Resources.Resources.success_32
        Me.btnComplete.Location = New System.Drawing.Point(351, 404)
        Me.btnComplete.Name = "btnComplete"
        Me.btnComplete.Size = New System.Drawing.Size(200, 45)
        Me.btnComplete.TabIndex = 80
        Me.btnComplete.Text = "Complete"
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupControl2.Controls.Add(Me.tbLists)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.ShowCaption = False
        Me.GroupControl2.Size = New System.Drawing.Size(333, 437)
        Me.GroupControl2.TabIndex = 87
        Me.GroupControl2.Text = "GroupControl2"
        '
        'tbLists
        '
        Me.tbLists.AllowDrag = False
        Me.tbLists.AllowItemHover = False
        Me.tbLists.AllowSelectedItem = True
        Me.tbLists.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbLists.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        Me.tbLists.Groups.Add(Me.tbListGroup)
        Me.tbLists.ItemSize = 80
        Me.tbLists.Location = New System.Drawing.Point(2, 2)
        Me.tbLists.Name = "tbLists"
        Me.tbLists.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.tbLists.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollButtons
        Me.tbLists.Size = New System.Drawing.Size(329, 433)
        Me.tbLists.TabIndex = 88
        Me.tbLists.VerticalContentAlignment = DevExpress.Utils.VertAlignment.Top
        '
        'tbListGroup
        '
        Me.tbListGroup.Name = "tbListGroup"
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.radComplete)
        Me.GroupControl1.Controls.Add(Me.radPending)
        Me.GroupControl1.Location = New System.Drawing.Point(351, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(567, 72)
        Me.GroupControl1.TabIndex = 88
        Me.GroupControl1.Text = "GroupControl1"
        '
        'radComplete
        '
        Me.radComplete.Location = New System.Drawing.Point(140, 5)
        Me.radComplete.Name = "radComplete"
        Me.radComplete.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radComplete.Properties.Appearance.Options.UseFont = True
        Me.radComplete.Properties.Appearance.Options.UseTextOptions = True
        Me.radComplete.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.radComplete.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.radComplete.Properties.AutoHeight = False
        Me.radComplete.Properties.Caption = "Completed"
        Me.radComplete.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radComplete.Properties.RadioGroupIndex = 2
        Me.radComplete.Size = New System.Drawing.Size(146, 61)
        Me.radComplete.TabIndex = 91
        Me.radComplete.TabStop = False
        '
        'radPending
        '
        Me.radPending.EditValue = True
        Me.radPending.Location = New System.Drawing.Point(11, 5)
        Me.radPending.Name = "radPending"
        Me.radPending.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radPending.Properties.Appearance.Options.UseFont = True
        Me.radPending.Properties.AutoHeight = False
        Me.radPending.Properties.Caption = "Pending"
        Me.radPending.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radPending.Properties.RadioGroupIndex = 2
        Me.radPending.Size = New System.Drawing.Size(123, 61)
        Me.radPending.TabIndex = 90
        '
        'sc
        '
        Me.sc.AllowTouchScroll = True
        Me.sc.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.sc.Location = New System.Drawing.Point(351, 90)
        Me.sc.Name = "sc"
        Me.sc.Size = New System.Drawing.Size(567, 308)
        Me.sc.TabIndex = 91
        '
        'frmChecklists
        '
        Me.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.Appearance.Options.UseBackColor = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(930, 461)
        Me.Controls.Add(Me.sc)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.btnComplete)
        Me.Controls.Add(Me.btnClose)
        Me.Name = "frmChecklists"
        Me.Text = "Checklists"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Controls.SetChildIndex(Me.btnClose, 0)
        Me.Controls.SetChildIndex(Me.btnComplete, 0)
        Me.Controls.SetChildIndex(Me.GroupControl2, 0)
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        Me.Controls.SetChildIndex(Me.sc, 0)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.radComplete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radPending.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnComplete As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents tbLists As DevExpress.XtraBars.Navigation.TileBar
    Friend WithEvents tbListGroup As DevExpress.XtraBars.Navigation.TileBarGroup
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents radComplete As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents radPending As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents timSleepCheck As System.Windows.Forms.Timer
    Friend WithEvents sc As DevExpress.XtraEditors.XtraScrollableControl
End Class
