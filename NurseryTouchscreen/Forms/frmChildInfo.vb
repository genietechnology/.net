﻿Imports NurseryTouchscreen.SharedModule

Public Class frmChildInfo

    Private m_ChildID As String
    Private m_ChildName As String
    Private m_ChildPair As New SharedModule.Pair
    Private m_ChildInOut As Enums.InOut
    Private m_FamilyID As String = ""
    Private m_Location As String = ""

    Private m_Feedback As Boolean = False
    Private m_FeedbackText As String = ""

    Private m_CrossChecked As Boolean = False

    Public Sub New(ByVal ChildID As String, ByVal ChildName As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_ChildID = ChildID
        m_ChildName = ChildName

    End Sub

    Private Sub frmChild_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        DisplayDetails()
    End Sub

    Private Sub DisplayDetails()

        m_ChildInOut = PersonInorOut(New Guid(m_ChildID))
        m_FamilyID = GetFamilyIDFromChildID(m_ChildID)

        Dim _SQL As String = ""

        _SQL = ""
        _SQL += "select forename as 'Forename', surname as 'Surname', dob as 'DOB', gender as 'Gender', started as 'Date Started',"
        _SQL += " keyworker_name as 'Keyworker', group_name as 'Group', religion as 'Religion', language as 'Language', nappies as 'Nappies?'"
        _SQL += " from Children"
        _SQL += " where ID = '" + m_ChildID + "'"

        vgBasic.DataSource = DAL.ReturnDataTable(_SQL)

        _SQL = ""
        _SQL += " select sen as 'SEN', allergy_rating as 'Allergy Rating', diet_restrict as 'Diet Restriction', diet_notes as 'Dietary Notes',"
        _SQL += " med_allergies as 'Allergies', med_medication as 'Medication', med_notes as 'Medical Notes',"
        _SQL += " surg_doc as 'Doctor', surg_name as 'Surgery', surg_tel as 'Surgery Tel',"
        _SQL += " hv_name as 'HV Name', hv_tel as 'HV Tel', hv_mobile as 'HV Mobile'"
        _SQL += " from Children"
        _SQL += " where ID = '" + m_ChildID + "'"

        vgMedical.DataSource = DAL.ReturnDataTable(_SQL)

        AllergyMatrix()

    End Sub

    Private Sub AllergyMatrix()

        Dim _SQL As String = ""
        _SQL += "select AppListItems.name as 'Allergy', cast(iif(ChildAttrib.attribute = AppListItems.name, 1, 0) as bit) as ' ' from AppListItems"
        _SQL += " left join AppLists on AppLists.ID = AppListItems.list_id"
        _SQL += " left join ChildAttrib on ChildAttrib.category = 'Allergies'"
        _SQL += " and ChildAttrib.child_id = '" + m_ChildID + "'"
        _SQL += " and ChildAttrib.attribute = AppListItems.name"
        _SQL += " where AppLists.name = 'Allergy Matrix'"
        _SQL += " order by AppListItems.seq, AppListItems.name"

        tgAllergiesConsent.Clear()
        tgAllergiesConsent.Populate(_SQL)

    End Sub

    Private Sub ConsentMatrix()

        Dim _SQL As String = ""

        _SQL += "select i.name as 'Consent', isnull(c.given, 0) as ' ' from AppListItems i"
        _SQL += " left join AppLists l on l.ID = i.list_id"
        _SQL += " left join ChildConsent c on c.consent = i.name and c.child_id = '" + m_ChildID + "'"
        _SQL += " where l.name = 'Consent'"
        _SQL += " order by i.ref_1, i.name"

        tgAllergiesConsent.Clear()
        tgAllergiesConsent.Populate(_SQL)

    End Sub

    Private Sub btnAllergyMatrix_Click(sender As Object, e As EventArgs) Handles btnAllergyMatrix.Click
        AllergyMatrix()
    End Sub

    Private Sub btnConsentMatrix_Click(sender As Object, e As EventArgs) Handles btnConsentMatrix.Click
        ConsentMatrix()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

End Class
