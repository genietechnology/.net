﻿Imports NurseryTouchscreen.SharedModule
Imports System.Drawing

Public Class frmIncidentLog

    Implements IAddRemoveChildForm

    Public Sub DrillDown(ChildID As String, RecordID As String) Implements IAddRemoveChildForm.DrillDown
        Me.ChildID = ChildID
        Me.RecordID = RecordID
        If Not Me.IsNew Then DisplayRecord()
    End Sub

    Private m_StaffSignature As New Signature
    Private m_ViewingBodyMap As Boolean = True
    Private m_Photo As Image = Nothing
    Private m_BodyMap As Image = Nothing
    Private m_IncidentType = "Accident"
    Private m_Time As Date? = Nothing

    Private Sub frmIncidentLog_Load(sender As Object, e As EventArgs) Handles Me.Load

        SetCMDs()

        If m_BodyMap Is Nothing Then
            m_BodyMap = My.Resources.ResourceManager.GetObject("Body")
            picMap.Image = m_BodyMap
        End If

    End Sub

    Private Sub DisplayRecord()

        If ChildID = "" Then Exit Sub

        Dim _SQL As String = ""

        _SQL += "select i.incident_type, i.location, i.details, i.injury, i.treatment, i.action,"
        _SQL += " i.equip_id, i.equip_name, i.risk_id, i.risk_title,"
        _SQL += " i.sig_staff, i.sig_witness, i.sig_contact, wit.person_name as 'witness_name', con.person_name as 'contact_name',"
        _SQL += " i.sig_manager, mgr.person_name As 'manager_name',"
        _SQL += " i.body_map, i.photo, i.stamp"
        _SQL += " from Incidents i"
        _SQL += " left join Signatures wit on wit.ID = i.sig_witness"
        _SQL += " left join Signatures mgr on mgr.ID = i.sig_manager"
        _SQL += " left join Signatures con on con.ID = i.sig_contact"
        _SQL += " where i.ID = '" & Me.RecordID & "'"
        _SQL += " order by i.stamp desc"

        Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
        If Not _DR Is Nothing Then

            m_IncidentType = _DR("incident_type").ToString

            m_Time = CDate(_DR("stamp"))
            fldTime.ValueDateTime = m_Time
            fldTime.ValueText = Format(CDate(_DR("stamp")), "ddd dd MMM yyyy HH:mm")

            fldEquipment.ValueID = _DR("equip_id").ToString
            fldEquipment.ValueText = _DR("equip_name").ToString

            fldRisk.ValueID = _DR("risk_id").ToString
            fldRisk.ValueText = _DR("risk_title").ToString

            fldLocation.ValueText = _DR("location").ToString
            fldDetails.ValueText = _DR("details").ToString
            fldInjury.ValueText = _DR("injury").ToString
            fldTreatment.ValueText = _DR("treatment").ToString
            fldAction.ValueText = _DR("action").ToString

            fldWitness.FamilyID = Me.FamilyID

            If m_IncidentType = "Prior Injury" Then
                fldWitness.ValueID = _DR("sig_contact").ToString
                fldWitness.ValueText = _DR("contact_name").ToString
            Else
                fldWitness.ValueID = _DR("sig_witness").ToString
                fldWitness.ValueText = _DR("witness_name").ToString
            End If

            fldManager.ValueID = _DR("sig_manager").ToString
            fldManager.ValueText = _DR("manager_name").ToString

            m_BodyMap = MediaHandler.ReturnImage(_DR("body_map"))
            m_Photo = MediaHandler.ReturnImage(_DR("photo"))

            m_ViewingBodyMap = False
            ToggleView()
            SetLayout()

            _DR = Nothing

        End If

    End Sub

    Protected Overrides Function BeforeCommitUpdate() As Boolean
        If HasActivity() Then
            Return CheckAnswers()
        Else
            Return False
        End If
    End Function

    Private Function HasActivity() As Boolean

        If fldTime.ValueText <> "" Then
            Return True
        End If

        If fldEquipment.ValueText <> "" Then
            Return True
        End If

        If fldRisk.ValueText <> "" Then
            Return True
        End If

        If fldLocation.ValueText <> "" Then
            Return True
        End If

        If fldDetails.ValueText <> "" Then
            Return True
        End If

        If fldInjury.ValueText <> "" Then
            Return True
        End If

        If fldTreatment.ValueText <> "" Then
            Return True
        End If

        If fldAction.ValueText <> "" Then
            Return True
        End If

        If m_StaffSignature.SignatureCaptured Then
            Return True
        End If

        If fldWitness.ValueID <> "" Then
            If fldWitness.SignatureCapture.SignatureCaptured Then
                Return True
            End If
        End If

        If fldManager.ValueID <> "" Then
            If fldManager.SignatureCapture.SignatureCaptured Then
                Return True
            End If
        End If

        Return False

    End Function

    Private Function CheckAnswers() As Boolean

        If fldTime.ValueText = "" Then
            Msgbox("Please enter the time.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If Parameters.IncidentsMandatoryEquipment Then
            If fldEquipment.ValueText = "" Then
                Msgbox("Please select an Equipment record to associate with this Incident.", MessageBoxIcon.Exclamation, "Mandatory Field")
                Return False
            End If
        End If

        If Parameters.IncidentsMandatoryRisk Then
            If fldRisk.ValueText = "" Then
                Msgbox("Please select a Risk Assessment to associate with this Incident.", MessageBoxIcon.Exclamation, "Mandatory Field")
                Return False
            End If
        End If

        If fldLocation.ValueText = "" Then
            Msgbox("Please enter the location.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If fldDetails.ValueText = "" Then
            Msgbox("Please enter what happened.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If fldInjury.ValueText = "" Then
            Msgbox("Please enter the injury suffered.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If fldTreatment.ValueText = "" Then
            Msgbox("Please enter the treatment given.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If fldAction.ValueText = "" Then
            Msgbox("Please enter the action taken.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If Not m_StaffSignature.SignatureCaptured Then
            Msgbox("Please ensure you sign this entry.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If Parameters.IncidentsMandatoryWitness Then
            If fldWitness.ValueID <> "" Then
                If Not fldWitness.SignatureCapture.SignatureCaptured Then
                    Msgbox("Please ensure the witness countersigns.", MessageBoxIcon.Exclamation, "Mandatory Field")
                    Return False
                End If
            End If
        End If

        If Parameters.IncidentsMandatoryManager Then
            If fldManager.ValueID <> "" Then
                If Not fldManager.SignatureCapture.SignatureCaptured Then
                    Msgbox("Please ensure the manager countersigns.", MessageBoxIcon.Exclamation, "Mandatory Field")
                    Return False
                End If
            End If
        End If

        Return True

    End Function

    Private Sub Clear()
        fldTime.Clear()
        fldEquipment.Clear()
        fldRisk.Clear()
        fldLocation.Clear()
        fldDetails.Clear()
        fldInjury.Clear()
        fldTreatment.Clear()
        fldAction.Clear()
        fldWitness.Clear()
        fldManager.Clear()
        picMap.Image = My.Resources.ResourceManager.GetObject("Body")
    End Sub

    Protected Overrides Sub CommitUpdate()

        Dim _SQL As String
        Dim IncidentID As Guid = Guid.NewGuid

        If Me.IsNew Then
            _SQL = "select * from Incidents where ID = '" & IncidentID.ToString & "'"
        Else
            _SQL = "select * from Incidents where ID = '" & Me.RecordID & "'"
        End If

        Dim _DA As IDataAdapter = DAL.ReturnDataAdapter(_SQL)
        If Not _DA Is Nothing Then

            Dim _DR As DataRow
            Dim _DS As New DataSet
            _DA.Fill(_DS)

            If _DS.Tables(0).Rows.Count = 0 Then
                _DR = _DS.Tables(0).NewRow
                _DR("id") = IncidentID
                _DR("day_id") = New Guid(TodayID)
                _DR("child_id") = New Guid(ChildID)
                _DR("child_name") = ChildName
            Else
                _DR = _DS.Tables(0).Rows(0)
            End If

            _DR("staff_id") = scrStaff.ValueID
            _DR("staff_name") = scrStaff.ValueText

            _DR("incident_type") = m_IncidentType

            If fldEquipment.ValueText = "" Then
                _DR("equip_id") = DBNull.Value
                _DR("equip_name") = ""
            Else
                _DR("equip_id") = fldEquipment.ValueID
                _DR("equip_name") = fldEquipment.ValueText
            End If

            If fldRisk.ValueText = "" Then
                _DR("risk_id") = DBNull.Value
                _DR("risk_title") = ""
            Else
                _DR("risk_id") = fldRisk.ValueID
                _DR("risk_title") = fldRisk.ValueText
            End If

            _DR("location") = fldLocation.ValueText
            _DR("details") = fldDetails.ValueText
            _DR("injury") = fldInjury.ValueText
            _DR("treatment") = fldTreatment.ValueText
            _DR("action") = fldAction.ValueText

            If m_StaffSignature.SignatureCaptured Then
                If m_StaffSignature.StoreSignature Then
                    _DR("sig_staff") = m_StaffSignature.SignatureID
                End If
            End If

            _DR("photo") = MediaHandler.ReturnByteArray(m_Photo)
            _DR("body_map") = MediaHandler.ReturnByteArray(m_BodyMap)

            If m_IncidentType = "Prior Injury" Then
                If _DR("sig_contact").ToString = "" AndAlso fldWitness.ValueID <> "" Then
                    If fldWitness.SignatureCapture.SignatureCaptured Then
                        If fldWitness.SignatureCapture.StoreSignature Then
                            _DR("sig_contact") = fldWitness.SignatureCapture.SignatureID
                        End If
                    End If
                End If
            Else
                If _DR("sig_witness").ToString = "" AndAlso fldWitness.ValueID <> "" Then
                    If fldWitness.SignatureCapture.SignatureCaptured Then
                        If fldWitness.SignatureCapture.StoreSignature Then
                            _DR("sig_witness") = fldWitness.SignatureCapture.SignatureID
                        End If
                    End If
                End If
            End If

            If _DR("sig_manager").ToString = "" AndAlso fldManager.ValueID <> "" Then
                If fldManager.SignatureCapture.SignatureCaptured Then
                    If fldManager.SignatureCapture.StoreSignature Then
                        _DR("sig_manager") = fldManager.SignatureCapture.SignatureID
                    End If
                End If
            End If

            If fldTime.ValueText <> "" Then
                _DR("stamp") = fldTime.ValueDateTime
            Else
                _DR("stamp") = DBNull.Value
            End If

            If _DS.Tables(0).Rows.Count = 0 Then _DS.Tables(0).Rows.Add(_DR)

            DAL.UpdateDB(_DA, _DS)

        End If

    End Sub

    Private Sub SetLayout()

        Select Case m_IncidentType

            Case "Accident"
                fldTime.LabelText = "Time"
                fldTime.ButtonType = Field.EnumButtonType.Time
                fldWitness.LabelText = "Witness"
                fldWitness.ButtonType = Field.EnumButtonType.SignatureStaff
                fldWitness.FamilyID = ""

            Case "Prior Injury"
                fldTime.LabelText = "Date && Time"
                fldTime.ButtonType = Field.EnumButtonType.DateTime
                fldWitness.LabelText = "Parent / Contact"
                fldWitness.ButtonType = Field.EnumButtonType.SignatureParent
                fldWitness.FamilyID = Me.FamilyID

            Case "Incident"
                fldTime.LabelText = "Time"
                fldTime.ButtonType = Field.EnumButtonType.Time
                fldWitness.LabelText = "Witness"
                fldWitness.ButtonType = Field.EnumButtonType.SignatureStaff
                fldWitness.FamilyID = ""

        End Select

    End Sub

    Private Sub SetCMDs()

        If m_ViewingBodyMap Then
            btnToggleView.Text = "View Photo"
            If m_Photo Is Nothing Then
                btnToggleView.Enabled = False
            Else
                btnToggleView.Enabled = True
            End If
        Else
            btnToggleView.Text = "View Body Map"
            btnToggleView.Enabled = True
        End If

    End Sub

    Private Sub btnMark_Click(sender As Object, e As EventArgs) Handles btnMark.Click

        Dim _frmBodyMap As New frmBodyMap
        _frmBodyMap.BodyImage = picMap.Image
        _frmBodyMap.ShowDialog()

        If _frmBodyMap.DialogResult = DialogResult.OK Then
            m_BodyMap = _frmBodyMap.BodyImage
            picMap.Image = _frmBodyMap.BodyImage
        End If

        _frmBodyMap.Dispose()
        _frmBodyMap = Nothing

    End Sub

    Private Sub btnPhoto_Click(sender As Object, e As EventArgs) Handles btnPhoto.Click
        'Dim _Path As String = MediaHandler.CaptureNow()
        'm_Photo = MediaHandler.ReturnImage(_Path)
        'If m_Photo IsNot Nothing Then
        '    m_ViewingBodyMap = True
        '    ToggleView()
        'End If
    End Sub

    Private Sub btnSig_Click(sender As Object, e As EventArgs) Handles btnSig.Click
        m_StaffSignature.CaptureStaffSignature(False)
    End Sub

    Private Sub btnToggleView_Click(sender As Object, e As EventArgs) Handles btnToggleView.Click
        ToggleView()
    End Sub

    Private Sub ToggleView()

        If m_ViewingBodyMap Then
            m_ViewingBodyMap = False
            picMap.Image = m_Photo
        Else
            m_ViewingBodyMap = True
            picMap.Image = m_BodyMap
        End If

        SetCMDs()

    End Sub

    Private Sub btnToggleMode_Click(sender As Object, e As EventArgs) Handles btnToggleMode.Click
        Dim _P As Pair = ReturnButtonSQL("Accident|Prior Injury|Incident", "Please select a Mode")
        If _P IsNot Nothing Then
            m_IncidentType = _P.Text
            Me.Text = "Log " + m_IncidentType
            SetLayout()
        End If
    End Sub

    Private Sub fldEquipment_ButtonClick(sender As Object, e As EventArgs) Handles fldEquipment.ButtonClick

        Dim _SQL As String = "select ID, name from Equipment where status = 'Active' order by name"

        Dim _P As Pair = ReturnButtonSQL(_SQL, "Select associated Equipment")
        If _P IsNot Nothing Then
            fldEquipment.ValueID = _P.Code
            fldEquipment.ValueText = _P.Text
            AutoSelectRisk(_P.Code)
        End If

    End Sub

    Private Sub AutoSelectRisk(ByVal EquipmentID As String)

        Dim _SQL As String = ""

        _SQL += "select r.ID, r.title from EquipmentRisks"
        _SQL += " left join Risks r on r.ID = risk_id"
        _SQL += " where equip_id = '" + EquipmentID + "'"

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)

        If _DT IsNot Nothing Then
            If _DT.Rows.Count > 0 Then
                If _DT.Rows.Count > 1 Then
                    Dim _P As Pair = ReturnButtonSQL(_SQL, "Select associated Risk Assessment")
                    If _P IsNot Nothing Then
                        fldRisk.ValueID = _P.Code
                        fldRisk.ValueText = _P.Text
                    End If
                Else
                    fldRisk.ValueID = _DT.Rows(0).Item("id").ToString
                    fldRisk.ValueText = _DT.Rows(0).Item("title").ToString
                End If
            End If
        End If

    End Sub

    Private Sub fldRisk_ButtonClick(sender As Object, e As EventArgs) Handles fldRisk.ButtonClick

        Dim _SQL As String = "select ID, title from Risks where status = 'Current' order by title"

        Dim _P As Pair = ReturnButtonSQL(_SQL, "Select associated Risk Assessment")
        If _P IsNot Nothing Then
            fldRisk.ValueID = _P.Code
            fldRisk.ValueText = _P.Text
        End If

    End Sub

End Class
