﻿
Public Class frmCrossCheck

    Private m_DayID As String = Parameters.TodayID
    Private m_Mode As EnumMode

    Private m_ChildID As String = ""
    Private m_ChildName As String = ""
    Private m_ChildGroupID As String

    Private m_Nappies As Boolean
    Private m_Milk As Boolean
    Private m_BabyFood As Boolean
    Private m_OffMenu As Boolean
    Private m_FamilyID As String

    Private m_Sibblings As New List(Of SharedModule.Pair)

    Private m_CrossChecked As Boolean = False
    Private m_MedicineOrIncident As Boolean = False

    Private Enum EnumColourMode
        None
        PositiveGreen
        PositiveRed
    End Enum

    Public Enum EnumMode
        CrossCheck
        CheckOut
    End Enum

    Public Sub New(ByVal ChildID As String, ByVal ChildName As String, ByVal Mode As EnumMode)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_ChildID = ChildID
        m_ChildName = ChildName
        m_Mode = Mode

    End Sub

    Private Sub frmCrossCheck_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        txtChild.Text = m_ChildName
        txtCheckedBy.Text = ""

        'have we cross-checked already?
        Dim _DR As DataRow = Nothing
        If SharedModule.ReturnActivityRow(Parameters.TodayID, m_ChildID, SharedModule.EnumActivityType.CrossCheck, _DR) Then
            m_CrossChecked = True
            txtCheckedBy.Text = _DR.Item("staff_name").ToString
            txtCheckedBy.BackColor = SharedModule.ColourGreen
        Else
            txtCheckedBy.Text = "Not Checked"
            txtCheckedBy.BackColor = SharedModule.ColourRed
        End If

        If m_Mode = EnumMode.CheckOut Then
            btnAccept.Text = "Check Out"
        Else
            btnAccept.Text = "Approve Cross-Check"
            btnAccept.Enabled = Not m_CrossChecked
        End If

        SetChildDetails()
        DisplayStats()

    End Sub

    Private Sub SetChildDetails()

        Dim _SQL As String = ""
        _SQL += "select family_id, group_id, nappies, milk, baby_food, off_menu,"
        _SQL += " med_allergies, med_medication, med_notes, allergy_rating"
        _SQL += " from children"
        _SQL += " where id = '" & m_ChildID & "'"

        Dim _dr As DataRow = DAL.ReturnDataRow(_SQL)
        If _dr IsNot Nothing Then

            'used for drilldown
            m_ChildGroupID = _dr.Item("group_id").ToString
            m_FamilyID = _dr.Item("family_id").ToString

            If Boolean.Parse(_dr.Item("nappies").ToString) Then
                m_Nappies = True
            Else
                m_Nappies = False
            End If

            If Boolean.Parse(_dr.Item("milk").ToString) Then
                m_Milk = True
            Else
                m_Milk = False
            End If

            If Boolean.Parse(_dr.Item("baby_food").ToString) Then
                m_BabyFood = True
            Else
                m_BabyFood = False
            End If

            If Boolean.Parse(_dr.Item("off_menu").ToString) Then
                m_OffMenu = True
            Else
                m_OffMenu = False
            End If

            _dr = Nothing

        End If

        _dr = Nothing

    End Sub

    Private Sub DisplayStats()

        '******************************************************************************************************************************

        SharedModule.ReturnFeedback(m_DayID, m_ChildID, txtFeedback.Text)

        '******************************************************************************************************************************

        Dim _Breakfast As Integer = SharedModule.ReturnFoodCount(m_DayID, m_ChildID, SharedModule.EnumMealType.Breakfast)
        SetTextBox(txtBreakfast, _Breakfast, EnumColourMode.PositiveGreen)

        Dim _Snack As Integer = SharedModule.ReturnFoodCount(m_DayID, m_ChildID, SharedModule.EnumMealType.Snack)
        SetTextBox(txtSnack, _Snack, EnumColourMode.PositiveGreen)

        Dim _Lunch As Integer = SharedModule.ReturnFoodCount(m_DayID, m_ChildID, SharedModule.EnumMealType.Lunch)
        SetTextBox(txtLunch, _Lunch, EnumColourMode.PositiveGreen)

        Dim _LunchDessert As Integer = SharedModule.ReturnFoodCount(m_DayID, m_ChildID, SharedModule.EnumMealType.LunchDessert)
        SetTextBox(txtLunchDessert, _LunchDessert, EnumColourMode.PositiveGreen)

        Dim _Tea As Integer = SharedModule.ReturnFoodCount(m_DayID, m_ChildID, SharedModule.EnumMealType.Tea)
        SetTextBox(txtTea, _Tea, EnumColourMode.PositiveGreen)

        Dim _TeaDessert As Integer = SharedModule.ReturnFoodCount(m_DayID, m_ChildID, SharedModule.EnumMealType.TeaDessert)
        SetTextBox(txtTeaDessert, _TeaDessert, EnumColourMode.PositiveGreen)

        '******************************************************************************************************************************

        Dim _Toilet As Integer = SharedModule.ReturnActivityCount(m_DayID, m_ChildID, SharedModule.EnumActivityType.Toilet)
        If m_Nappies Then
            SetTextBox(txtNappies, _Toilet, EnumColourMode.PositiveGreen)
        Else
            SetTextBox(txtNappies, _Toilet, EnumColourMode.None)
        End If

        Dim _Sleep As Integer = SharedModule.ReturnActivityCount(m_DayID, m_ChildID, SharedModule.EnumActivityType.Sleep)
        SetTextBox(txtSleep, _Sleep, EnumColourMode.None)

        Dim _Medication As Integer = SharedModule.ReturnMedicineCount(m_DayID, m_ChildID)
        SetTextBox(txtMedication, _Medication, EnumColourMode.PositiveRed)

        Dim _Incident As Integer = SharedModule.ReturnIncidentCount(m_DayID, m_ChildID)
        SetTextBox(txtIncidents, _Incident, EnumColourMode.PositiveRed)

        Dim _Suncream As Integer = SharedModule.ReturnActivityCount(m_DayID, m_ChildID, SharedModule.EnumActivityType.Suncream)
        SetTextBox(txtSuncream, _Suncream, EnumColourMode.None)

        Dim _Milk As Integer = SharedModule.ReturnActivityCount(m_DayID, m_ChildID, SharedModule.EnumActivityType.Milk)
        If m_Milk Then
            SetTextBox(txtMilk, _Milk, EnumColourMode.PositiveGreen)
        Else
            SetTextBox(txtMilk, _Milk, EnumColourMode.None)
        End If

        Dim _Obs As Integer = SharedModule.ReturnObservationCount(m_DayID, m_ChildID)
        SetTextBox(txtObservations, _Obs, EnumColourMode.None)

    End Sub

    Private Sub SetTextBox(ByRef TextBoxIn As DevExpress.XtraEditors.TextEdit, ByVal CountIn As Integer, ByVal ColourMode As EnumColourMode)

        Dim _Text As String = ""
        Dim _Backcolour As Drawing.Color

        Select Case CountIn

            Case 0
                _Text = "None"
                If ColourMode = EnumColourMode.PositiveGreen Then
                    _Backcolour = SharedModule.ColourRed
                Else
                    _Backcolour = SharedModule.ColourGreen
                End If

            Case 1
                _Text = "1 item"
                If ColourMode = EnumColourMode.PositiveGreen Then
                    _Backcolour = SharedModule.ColourGreen
                Else
                    _Backcolour = SharedModule.ColourRed
                End If

            Case Else
                _Text = CountIn.ToString + " items"
                If ColourMode = EnumColourMode.PositiveGreen Then
                    _Backcolour = SharedModule.ColourGreen
                Else
                    _Backcolour = SharedModule.ColourRed
                End If

        End Select

        If ColourMode = EnumColourMode.None Then
            TextBoxIn.ResetBackColor()
        Else
            TextBoxIn.BackColor = _Backcolour
        End If

        TextBoxIn.Text = _Text

    End Sub

    Private Sub ProcessCrossCheck()

        Dim _S As New Signature

        If _S.CaptureStaffSignature(True) Then

            SharedModule.LogActivity(Parameters.TodayID, m_ChildID, m_ChildName, SharedModule.EnumActivityType.CrossCheck, "Cross-Check Completed", _S.SignatureID.ToString, , , _S.PersonID.ToString, _S.PersonName)
            _S = Nothing

            Me.DialogResult = DialogResult.OK

        End If

        _S = Nothing

    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click

        btnAccept.Enabled = False
        btnCancel.Enabled = False

        Me.Cursor = Cursors.WaitCursor
        Application.DoEvents()

        ProcessCrossCheck()

        btnAccept.Enabled = True
        btnCancel.Enabled = True

        Me.Cursor = Cursors.Default
        Application.DoEvents()

    End Sub

    Private Sub txtNappies_DoubleClick(sender As Object, e As EventArgs) Handles txtNappies.DoubleClick
        SharedModule.ChildToilet(m_ChildID)
    End Sub

    Private Sub txtSleep_DoubleClick(sender As Object, e As EventArgs) Handles txtSleep.DoubleClick
        SharedModule.ChildSleep(m_ChildID)
    End Sub

    Private Sub txtMedication_DoubleClick(sender As Object, e As EventArgs) Handles txtMedication.DoubleClick
        SharedModule.ChildMedicationLog(m_ChildID)
    End Sub

    Private Sub txtIncidents_DoubleClick(sender As Object, e As EventArgs) Handles txtIncidents.DoubleClick
        SharedModule.ChildIncidents(m_ChildID)
    End Sub

    Private Sub txtSuncream_DoubleClick(sender As Object, e As EventArgs) Handles txtSuncream.DoubleClick
        SharedModule.ChildSuncream(m_ChildID)
    End Sub

    Private Sub txtMilk_DoubleClick(sender As Object, e As EventArgs) Handles txtMilk.DoubleClick
        SharedModule.ChildMilk(m_ChildID)
    End Sub

    Private Sub txtBreakfast_DoubleClick(sender As Object, e As EventArgs) Handles txtBreakfast.DoubleClick, txtSnack.DoubleClick, txtLunch.DoubleClick, txtLunchDessert.DoubleClick, txtTea.DoubleClick, txtTeaDessert.DoubleClick
        SharedModule.ChildFood(m_ChildID)
    End Sub

End Class