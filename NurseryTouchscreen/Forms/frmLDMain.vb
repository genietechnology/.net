﻿Imports NurseryTouchscreen.SharedModule
Imports System.IO

Public Class frmLDMain

    Private m_Colours As String = ""
    Private m_Shapes As String = ""
    Private m_Flashcards As String = ""

    Private Sub frmLDMain_Load(sender As Object, e As EventArgs) Handles Me.Load

        'check the flashcard path exists (this is now a general media path)
        SharedModule.FlashCardPath = Application.StartupPath + "\Media"

        If Not IO.Directory.Exists(SharedModule.FlashCardPath) Then
            IO.Directory.CreateDirectory(SharedModule.FlashCardPath)
        End If

        If IO.Directory.Exists(SharedModule.FlashCardPath) Then

            m_Colours = SharedModule.FlashCardPath + "\Splats"
            If Not IO.Directory.Exists(m_Colours) Then IO.Directory.CreateDirectory(m_Colours)

            m_Shapes = SharedModule.FlashCardPath + "\Shapes"
            If Not IO.Directory.Exists(m_Shapes) Then IO.Directory.CreateDirectory(m_Shapes)

            m_Flashcards = SharedModule.FlashCardPath + "\Flashcards"
            If Not IO.Directory.Exists(m_Flashcards) Then IO.Directory.CreateDirectory(m_Flashcards)

        Else
            btnColours.Enabled = False
            btnFlashcards.Enabled = False
            btnShapes.Enabled = False
        End If

    End Sub

    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub btnNumbers_Click(sender As System.Object, e As System.EventArgs) Handles btnNumbers.Click
        ShowLDText("1|2|3|4|5|6|7|8|9|10|11|12")
    End Sub

    Private Sub btnLetters_Click(sender As System.Object, e As System.EventArgs) Handles btnLetters.Click
        ShowLDText("Aa|Bb|Cc|Dd|Ee|Ff|Gg|Hh|Ii|Jj|Kk|Ll|Mm|Nn|Oo|Pp|Qq|Rr|Ss|Tt|Uu|Vv|Ww|Xx|Yy|Zz")
    End Sub

    Private Sub btnColours_Click(sender As System.Object, e As System.EventArgs) Handles btnColours.Click
        ShowMedia(m_Colours)
    End Sub

    Private Sub btnFlashcards_Click(sender As System.Object, e As System.EventArgs) Handles btnFlashcards.Click
        ShowMedia(m_FlashCards)
    End Sub

    Private Sub btnVideos_Click(sender As System.Object, e As System.EventArgs) Handles btnVideos.Click
        SharedModule.DisplayLinks("Videos", "Select Video")
    End Sub

    Private Sub btnEasy_Click(sender As Object, e As EventArgs) Handles btnEasy.Click
        Process.Start("https://secure.easy4nurseryeducation.co.uk/default.aspx")
    End Sub

    Private Sub ShowMedia(ByVal MediaFolder As String)

        'check the folder exists, and there are files in it
        If IO.Directory.Exists(MediaFolder) Then
            Dim _Files As String() = IO.Directory.GetFiles(MediaFolder)
            If _Files.Count > 0 Then
                Dim _frm As New frmLDPictures(MediaFolder)
                _frm.Show()
                _frm.BringToFront()
            Else
                Msgbox("There are no files in " + MediaFolder, MessageBoxIcon.Exclamation, "Load Media")
            End If
        Else
            Msgbox(MediaFolder + " does not exist.", MessageBoxIcon.Exclamation, "Load Media")
        End If

    End Sub

    Private Sub btnShapes_Click(sender As Object, e As EventArgs) Handles btnShapes.Click
        ShowMedia(m_Shapes)
    End Sub

    Private Sub btnQuickObs_Click(sender As Object, e As EventArgs) Handles btnQuickObs.Click
        QuickObservation()
    End Sub
End Class
