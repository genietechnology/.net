﻿Imports NurseryTouchscreen.SharedModule

Public Class frmPayment

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub frmPayment_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click

        If Not CheckAnswers Then Exit Sub

        btnAccept.Enabled = False
        Application.DoEvents()

        LogActivity(SharedModule.TodayID, fldFrom.ValueID, fldFrom.ValueText, EnumActivityType.Payment, "Payment from " + fldFrom.ValueText, _
                    fldValue.ValueText, fldMethod.ValueText, "", fldStaff.ValueID, fldStaff.ValueText, fldComments.ValueText)

        Me.DialogResult = DialogResult.OK
        Me.Close()

        btnAccept.Enabled = True
        Application.DoEvents()

    End Sub

    Private Function CheckAnswers() As Boolean

        If fldFrom.ValueText = "" Then
            Msgbox("Please enter the name of the person paying.", MessageBoxIcon.Exclamation, "Record Payment")
            Return False
        End If

        If fldValue.ValueText = "" Then
            Msgbox("Please enter the value of the payment.", MessageBoxIcon.Exclamation, "Record Payment")
            Return False
        End If

        If fldMethod.ValueText = "" Then
            Msgbox("Please select the payment method.", MessageBoxIcon.Exclamation, "Record Payment")
            Return False
        End If

        If fldStaff.ValueText = "" Then
            Msgbox("Please enter the staff accepting the payment.", MessageBoxIcon.Exclamation, "Record Payment")
            Return False
        End If

        Return True

    End Function

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

End Class