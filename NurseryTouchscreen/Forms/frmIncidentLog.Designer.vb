﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmIncidentLog
    Inherits NurseryTouchscreen.frmBaseChildAddRemove

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnMark = New DevExpress.XtraEditors.SimpleButton()
        Me.fldLocation = New NurseryTouchscreen.Field()
        Me.fldDetails = New NurseryTouchscreen.Field()
        Me.fldInjury = New NurseryTouchscreen.Field()
        Me.fldTreatment = New NurseryTouchscreen.Field()
        Me.fldAction = New NurseryTouchscreen.Field()
        Me.fldWitness = New NurseryTouchscreen.Field()
        Me.fldTime = New NurseryTouchscreen.Field()
        Me.picMap = New System.Windows.Forms.PictureBox()
        Me.btnPhoto = New DevExpress.XtraEditors.SimpleButton()
        Me.btnToggleView = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSig = New DevExpress.XtraEditors.SimpleButton()
        Me.btnToggleMode = New DevExpress.XtraEditors.SimpleButton()
        Me.fldEquipment = New NurseryTouchscreen.Field()
        Me.fldManager = New NurseryTouchscreen.Field()
        Me.fldRisk = New NurseryTouchscreen.Field()
        CType(Me.picMap, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnAccept
        '
        Me.btnAccept.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAccept.Appearance.Options.UseFont = True
        Me.btnAccept.Location = New System.Drawing.Point(866, 602)
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(1022, 602)
        '
        'btnMark
        '
        Me.btnMark.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMark.Appearance.Options.UseFont = True
        Me.btnMark.Image = Global.NurseryTouchscreen.My.Resources.Resources.Highlightmarker_red_32
        Me.btnMark.Location = New System.Drawing.Point(954, 494)
        Me.btnMark.Name = "btnMark"
        Me.btnMark.Size = New System.Drawing.Size(218, 45)
        Me.btnMark.TabIndex = 9
        Me.btnMark.Text = "Body Map"
        '
        'fldLocation
        '
        Me.fldLocation.ButtonCustom = False
        Me.fldLocation.ButtonSQL = Nothing
        Me.fldLocation.ButtonText = ""
        Me.fldLocation.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Enter
        Me.fldLocation.ButtonWidth = 50.0!
        Me.fldLocation.FamilyID = Nothing
        Me.fldLocation.HideText = False
        Me.fldLocation.LabelText = "Location"
        Me.fldLocation.LabelWidth = 160.0!
        Me.fldLocation.Location = New System.Drawing.Point(15, 264)
        Me.fldLocation.MaximumSize = New System.Drawing.Size(0, 51)
        Me.fldLocation.MinimumSize = New System.Drawing.Size(400, 51)
        Me.fldLocation.Name = "fldLocation"
        Me.fldLocation.QuickTextList = "Incident Location"
        Me.fldLocation.Size = New System.Drawing.Size(697, 51)
        Me.fldLocation.TabIndex = 2
        Me.fldLocation.ValueDateTime = Nothing
        Me.fldLocation.ValueID = Nothing
        Me.fldLocation.ValueMaxLength = 4000
        Me.fldLocation.ValueText = ""
        '
        'fldDetails
        '
        Me.fldDetails.ButtonCustom = False
        Me.fldDetails.ButtonSQL = Nothing
        Me.fldDetails.ButtonText = ""
        Me.fldDetails.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Enter
        Me.fldDetails.ButtonWidth = 50.0!
        Me.fldDetails.FamilyID = Nothing
        Me.fldDetails.HideText = False
        Me.fldDetails.LabelText = "What Happened?"
        Me.fldDetails.LabelWidth = 160.0!
        Me.fldDetails.Location = New System.Drawing.Point(15, 321)
        Me.fldDetails.MaximumSize = New System.Drawing.Size(0, 51)
        Me.fldDetails.MinimumSize = New System.Drawing.Size(400, 51)
        Me.fldDetails.Name = "fldDetails"
        Me.fldDetails.QuickTextList = "Incident Detail"
        Me.fldDetails.Size = New System.Drawing.Size(697, 51)
        Me.fldDetails.TabIndex = 3
        Me.fldDetails.ValueDateTime = Nothing
        Me.fldDetails.ValueID = Nothing
        Me.fldDetails.ValueMaxLength = 4000
        Me.fldDetails.ValueText = ""
        '
        'fldInjury
        '
        Me.fldInjury.ButtonCustom = False
        Me.fldInjury.ButtonSQL = Nothing
        Me.fldInjury.ButtonText = ""
        Me.fldInjury.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Enter
        Me.fldInjury.ButtonWidth = 50.0!
        Me.fldInjury.FamilyID = Nothing
        Me.fldInjury.HideText = False
        Me.fldInjury.LabelText = "Injury Suffered"
        Me.fldInjury.LabelWidth = 160.0!
        Me.fldInjury.Location = New System.Drawing.Point(15, 378)
        Me.fldInjury.MaximumSize = New System.Drawing.Size(0, 51)
        Me.fldInjury.MinimumSize = New System.Drawing.Size(400, 51)
        Me.fldInjury.Name = "fldInjury"
        Me.fldInjury.QuickTextList = "Incident Injury"
        Me.fldInjury.Size = New System.Drawing.Size(697, 51)
        Me.fldInjury.TabIndex = 4
        Me.fldInjury.ValueDateTime = Nothing
        Me.fldInjury.ValueID = Nothing
        Me.fldInjury.ValueMaxLength = 4000
        Me.fldInjury.ValueText = ""
        '
        'fldTreatment
        '
        Me.fldTreatment.ButtonCustom = False
        Me.fldTreatment.ButtonSQL = Nothing
        Me.fldTreatment.ButtonText = ""
        Me.fldTreatment.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Enter
        Me.fldTreatment.ButtonWidth = 50.0!
        Me.fldTreatment.FamilyID = Nothing
        Me.fldTreatment.HideText = False
        Me.fldTreatment.LabelText = "Treatment"
        Me.fldTreatment.LabelWidth = 160.0!
        Me.fldTreatment.Location = New System.Drawing.Point(15, 435)
        Me.fldTreatment.MaximumSize = New System.Drawing.Size(0, 51)
        Me.fldTreatment.MinimumSize = New System.Drawing.Size(400, 51)
        Me.fldTreatment.Name = "fldTreatment"
        Me.fldTreatment.QuickTextList = "Incident Treatment"
        Me.fldTreatment.Size = New System.Drawing.Size(697, 51)
        Me.fldTreatment.TabIndex = 5
        Me.fldTreatment.ValueDateTime = Nothing
        Me.fldTreatment.ValueID = Nothing
        Me.fldTreatment.ValueMaxLength = 4000
        Me.fldTreatment.ValueText = ""
        '
        'fldAction
        '
        Me.fldAction.ButtonCustom = False
        Me.fldAction.ButtonSQL = Nothing
        Me.fldAction.ButtonText = ""
        Me.fldAction.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Enter
        Me.fldAction.ButtonWidth = 50.0!
        Me.fldAction.FamilyID = Nothing
        Me.fldAction.HideText = False
        Me.fldAction.LabelText = "Further Action"
        Me.fldAction.LabelWidth = 160.0!
        Me.fldAction.Location = New System.Drawing.Point(15, 492)
        Me.fldAction.MaximumSize = New System.Drawing.Size(0, 51)
        Me.fldAction.MinimumSize = New System.Drawing.Size(400, 51)
        Me.fldAction.Name = "fldAction"
        Me.fldAction.QuickTextList = "Incident Action"
        Me.fldAction.Size = New System.Drawing.Size(697, 51)
        Me.fldAction.TabIndex = 6
        Me.fldAction.ValueDateTime = Nothing
        Me.fldAction.ValueID = Nothing
        Me.fldAction.ValueMaxLength = 4000
        Me.fldAction.ValueText = ""
        '
        'fldWitness
        '
        Me.fldWitness.ButtonCustom = False
        Me.fldWitness.ButtonSQL = Nothing
        Me.fldWitness.ButtonText = ""
        Me.fldWitness.ButtonType = NurseryTouchscreen.Field.EnumButtonType.SignatureStaff
        Me.fldWitness.ButtonWidth = 50.0!
        Me.fldWitness.FamilyID = Nothing
        Me.fldWitness.HideText = False
        Me.fldWitness.LabelText = "Witness"
        Me.fldWitness.LabelWidth = 160.0!
        Me.fldWitness.Location = New System.Drawing.Point(15, 545)
        Me.fldWitness.MaximumSize = New System.Drawing.Size(0, 51)
        Me.fldWitness.MinimumSize = New System.Drawing.Size(400, 51)
        Me.fldWitness.Name = "fldWitness"
        Me.fldWitness.QuickTextList = Nothing
        Me.fldWitness.Size = New System.Drawing.Size(697, 51)
        Me.fldWitness.TabIndex = 7
        Me.fldWitness.ValueDateTime = Nothing
        Me.fldWitness.ValueID = Nothing
        Me.fldWitness.ValueMaxLength = 0
        Me.fldWitness.ValueText = ""
        '
        'fldTime
        '
        Me.fldTime.ButtonCustom = False
        Me.fldTime.ButtonSQL = Nothing
        Me.fldTime.ButtonText = ""
        Me.fldTime.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Time
        Me.fldTime.ButtonWidth = 50.0!
        Me.fldTime.FamilyID = Nothing
        Me.fldTime.HideText = False
        Me.fldTime.LabelText = "Time"
        Me.fldTime.LabelWidth = 160.0!
        Me.fldTime.Location = New System.Drawing.Point(15, 93)
        Me.fldTime.MaximumSize = New System.Drawing.Size(0, 51)
        Me.fldTime.MinimumSize = New System.Drawing.Size(400, 51)
        Me.fldTime.Name = "fldTime"
        Me.fldTime.QuickTextList = Nothing
        Me.fldTime.Size = New System.Drawing.Size(697, 51)
        Me.fldTime.TabIndex = 1
        Me.fldTime.ValueDateTime = Nothing
        Me.fldTime.ValueID = Nothing
        Me.fldTime.ValueMaxLength = 0
        Me.fldTime.ValueText = ""
        '
        'picMap
        '
        Me.picMap.BackColor = System.Drawing.Color.White
        Me.picMap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picMap.Location = New System.Drawing.Point(727, 93)
        Me.picMap.Name = "picMap"
        Me.picMap.Size = New System.Drawing.Size(445, 389)
        Me.picMap.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picMap.TabIndex = 84
        Me.picMap.TabStop = False
        '
        'btnPhoto
        '
        Me.btnPhoto.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPhoto.Appearance.Options.UseFont = True
        Me.btnPhoto.Image = Global.NurseryTouchscreen.My.Resources.Resources.camera_32
        Me.btnPhoto.Location = New System.Drawing.Point(208, 602)
        Me.btnPhoto.Name = "btnPhoto"
        Me.btnPhoto.Size = New System.Drawing.Size(190, 45)
        Me.btnPhoto.TabIndex = 1
        Me.btnPhoto.Text = "Photo"
        '
        'btnToggleView
        '
        Me.btnToggleView.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnToggleView.Appearance.Options.UseFont = True
        Me.btnToggleView.Image = Global.NurseryTouchscreen.My.Resources.Resources.swap_32
        Me.btnToggleView.Location = New System.Drawing.Point(727, 494)
        Me.btnToggleView.Name = "btnToggleView"
        Me.btnToggleView.Size = New System.Drawing.Size(218, 45)
        Me.btnToggleView.TabIndex = 8
        Me.btnToggleView.Text = "View Photo"
        '
        'btnSig
        '
        Me.btnSig.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSig.Appearance.Options.UseFont = True
        Me.btnSig.Image = Global.NurseryTouchscreen.My.Resources.Resources.pen_32
        Me.btnSig.Location = New System.Drawing.Point(404, 602)
        Me.btnSig.Name = "btnSig"
        Me.btnSig.Size = New System.Drawing.Size(190, 45)
        Me.btnSig.TabIndex = 2
        Me.btnSig.Text = "Sign"
        '
        'btnToggleMode
        '
        Me.btnToggleMode.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnToggleMode.Appearance.Options.UseFont = True
        Me.btnToggleMode.Image = Global.NurseryTouchscreen.My.Resources.Resources.swap_32
        Me.btnToggleMode.Location = New System.Drawing.Point(12, 602)
        Me.btnToggleMode.Name = "btnToggleMode"
        Me.btnToggleMode.Size = New System.Drawing.Size(190, 45)
        Me.btnToggleMode.TabIndex = 0
        Me.btnToggleMode.Text = "Mode"
        '
        'fldEquipment
        '
        Me.fldEquipment.ButtonCustom = True
        Me.fldEquipment.ButtonSQL = Nothing
        Me.fldEquipment.ButtonText = ""
        Me.fldEquipment.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Lookup
        Me.fldEquipment.ButtonWidth = 50.0!
        Me.fldEquipment.FamilyID = Nothing
        Me.fldEquipment.HideText = False
        Me.fldEquipment.LabelText = "Equipment"
        Me.fldEquipment.LabelWidth = 160.0!
        Me.fldEquipment.Location = New System.Drawing.Point(15, 150)
        Me.fldEquipment.MaximumSize = New System.Drawing.Size(0, 51)
        Me.fldEquipment.MinimumSize = New System.Drawing.Size(400, 51)
        Me.fldEquipment.Name = "fldEquipment"
        Me.fldEquipment.QuickTextList = ""
        Me.fldEquipment.Size = New System.Drawing.Size(697, 51)
        Me.fldEquipment.TabIndex = 85
        Me.fldEquipment.ValueDateTime = Nothing
        Me.fldEquipment.ValueID = Nothing
        Me.fldEquipment.ValueMaxLength = 4000
        Me.fldEquipment.ValueText = ""
        '
        'fldManager
        '
        Me.fldManager.ButtonCustom = False
        Me.fldManager.ButtonSQL = Nothing
        Me.fldManager.ButtonText = ""
        Me.fldManager.ButtonType = NurseryTouchscreen.Field.EnumButtonType.SignatureStaff
        Me.fldManager.ButtonWidth = 50.0!
        Me.fldManager.FamilyID = Nothing
        Me.fldManager.HideText = False
        Me.fldManager.LabelText = "Manager"
        Me.fldManager.LabelWidth = 90.0!
        Me.fldManager.Location = New System.Drawing.Point(727, 545)
        Me.fldManager.MaximumSize = New System.Drawing.Size(0, 51)
        Me.fldManager.MinimumSize = New System.Drawing.Size(400, 51)
        Me.fldManager.Name = "fldManager"
        Me.fldManager.QuickTextList = Nothing
        Me.fldManager.Size = New System.Drawing.Size(445, 51)
        Me.fldManager.TabIndex = 86
        Me.fldManager.ValueDateTime = Nothing
        Me.fldManager.ValueID = Nothing
        Me.fldManager.ValueMaxLength = 0
        Me.fldManager.ValueText = ""
        '
        'fldRisk
        '
        Me.fldRisk.ButtonCustom = True
        Me.fldRisk.ButtonSQL = Nothing
        Me.fldRisk.ButtonText = ""
        Me.fldRisk.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Lookup
        Me.fldRisk.ButtonWidth = 50.0!
        Me.fldRisk.FamilyID = Nothing
        Me.fldRisk.HideText = False
        Me.fldRisk.LabelText = "Risk Assessment"
        Me.fldRisk.LabelWidth = 160.0!
        Me.fldRisk.Location = New System.Drawing.Point(15, 207)
        Me.fldRisk.MaximumSize = New System.Drawing.Size(0, 51)
        Me.fldRisk.MinimumSize = New System.Drawing.Size(400, 51)
        Me.fldRisk.Name = "fldRisk"
        Me.fldRisk.QuickTextList = ""
        Me.fldRisk.Size = New System.Drawing.Size(697, 51)
        Me.fldRisk.TabIndex = 87
        Me.fldRisk.ValueDateTime = Nothing
        Me.fldRisk.ValueID = Nothing
        Me.fldRisk.ValueMaxLength = 4000
        Me.fldRisk.ValueText = ""
        '
        'frmIncidentLog
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(1184, 652)
        Me.Controls.Add(Me.btnToggleMode)
        Me.Controls.Add(Me.btnSig)
        Me.Controls.Add(Me.fldRisk)
        Me.Controls.Add(Me.btnPhoto)
        Me.Controls.Add(Me.fldManager)
        Me.Controls.Add(Me.fldEquipment)
        Me.Controls.Add(Me.btnToggleView)
        Me.Controls.Add(Me.picMap)
        Me.Controls.Add(Me.fldTime)
        Me.Controls.Add(Me.fldWitness)
        Me.Controls.Add(Me.fldAction)
        Me.Controls.Add(Me.fldTreatment)
        Me.Controls.Add(Me.fldInjury)
        Me.Controls.Add(Me.fldDetails)
        Me.Controls.Add(Me.fldLocation)
        Me.Controls.Add(Me.btnMark)
        Me.Name = "frmIncidentLog"
        Me.Text = "Log Accident"
        Me.Controls.SetChildIndex(Me.btnCancel, 0)
        Me.Controls.SetChildIndex(Me.btnAccept, 0)
        Me.Controls.SetChildIndex(Me.btnMark, 0)
        Me.Controls.SetChildIndex(Me.fldLocation, 0)
        Me.Controls.SetChildIndex(Me.fldDetails, 0)
        Me.Controls.SetChildIndex(Me.fldInjury, 0)
        Me.Controls.SetChildIndex(Me.fldTreatment, 0)
        Me.Controls.SetChildIndex(Me.fldAction, 0)
        Me.Controls.SetChildIndex(Me.fldWitness, 0)
        Me.Controls.SetChildIndex(Me.fldTime, 0)
        Me.Controls.SetChildIndex(Me.picMap, 0)
        Me.Controls.SetChildIndex(Me.btnToggleView, 0)
        Me.Controls.SetChildIndex(Me.fldEquipment, 0)
        Me.Controls.SetChildIndex(Me.fldManager, 0)
        Me.Controls.SetChildIndex(Me.btnPhoto, 0)
        Me.Controls.SetChildIndex(Me.fldRisk, 0)
        Me.Controls.SetChildIndex(Me.btnSig, 0)
        Me.Controls.SetChildIndex(Me.btnToggleMode, 0)
        CType(Me.picMap, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnMark As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents fldWitness As NurseryTouchscreen.Field
    Friend WithEvents fldAction As NurseryTouchscreen.Field
    Friend WithEvents fldTreatment As NurseryTouchscreen.Field
    Friend WithEvents fldInjury As NurseryTouchscreen.Field
    Friend WithEvents fldDetails As NurseryTouchscreen.Field
    Friend WithEvents fldLocation As NurseryTouchscreen.Field
    Friend WithEvents fldTime As NurseryTouchscreen.Field
    Friend WithEvents picMap As System.Windows.Forms.PictureBox
    Friend WithEvents btnPhoto As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnToggleView As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSig As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnToggleMode As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents fldEquipment As Field
    Friend WithEvents fldManager As Field
    Friend WithEvents fldRisk As Field
End Class
