﻿
Public Class frmHelp

    Private Sub frmHelp_Load(sender As Object, e As EventArgs) Handles Me.Load

        btnPingService.Enabled = False
        If Parameters.ServiceIP <> "" Then
            btnPingService.Enabled = True
        End If

        Me.Text = "Help [" + My.Computer.Name + "]"

    End Sub

    Private Sub btnHelpCentre_Click(sender As Object, e As EventArgs) Handles btnHelpCentre.Click
        Process.Start("http://support.caresoftware.co.uk")
    End Sub

    Private Sub btnGotoAssist_Click(sender As Object, e As EventArgs) Handles btnGotoAssist.Click
        Process.Start("https://www.fastsupport.com")
    End Sub

    Private Sub btnScreenConnect_Click(sender As Object, e As EventArgs) Handles btnScreenConnect.Click
        Process.Start("http://connect.caresoftware.co.uk/guest")
    End Sub

    Private Sub btnPingServer_Click(sender As Object, e As EventArgs) Handles btnPingServer.Click
        Ping(Parameters.ServerIP)
    End Sub

    Private Sub btnPingService_Click(sender As Object, e As EventArgs) Handles btnPingService.Click
        Ping(Parameters.ServiceIP)
    End Sub

    Private Sub btnPingGoogle_Click(sender As Object, e As EventArgs) Handles btnPingGoogle.Click
        Ping("8.8.8.8")
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub Ping(ByVal IP As String)

        Dim pi As New ProcessStartInfo
        pi.WindowStyle = ProcessWindowStyle.Normal
        pi.Arguments = " " + IP + " -t"
        pi.FileName = "ping"

        Dim p As Process = New Process()
        p.StartInfo = pi
        p.Start()

        pi = Nothing
        p.Dispose()

    End Sub

    Private Sub CMD(ByVal Command As String)

        Dim pi As New ProcessStartInfo
        pi.Arguments = "/C " + Command + ""
        pi.WindowStyle = ProcessWindowStyle.Hidden
        pi.UseShellExecute = False
        pi.RedirectStandardOutput = True
        pi.FileName = "cmd"

        Dim p As Process = New Process()
        p.StartInfo = pi

        p.Start()
        p.WaitForExit()

        Dim _Result As String = p.StandardOutput.ReadToEnd
        Msgbox(_Result)

        pi = Nothing
        p.Dispose()

    End Sub

    Private Sub btnIPConfig_Click(sender As Object, e As EventArgs) Handles btnIPConfig.Click
        CMD("ipconfig")
    End Sub
End Class
