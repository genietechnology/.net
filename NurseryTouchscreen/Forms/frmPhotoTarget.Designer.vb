﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmPhotoTarget
    Inherits frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btnProfileChild = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMaintenance = New DevExpress.XtraEditors.SimpleButton()
        Me.btnReportChild = New DevExpress.XtraEditors.SimpleButton()
        Me.btnIncident = New DevExpress.XtraEditors.SimpleButton()
        Me.btnObs = New DevExpress.XtraEditors.SimpleButton()
        Me.btn2 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn1 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnReportSite = New DevExpress.XtraEditors.SimpleButton()
        Me.btnReportRoom = New DevExpress.XtraEditors.SimpleButton()
        Me.btnProfileStaff = New DevExpress.XtraEditors.SimpleButton()
        Me.btnExit = New DevExpress.XtraEditors.SimpleButton()
        Me.btnProfileContact = New DevExpress.XtraEditors.SimpleButton()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'btnProfileChild
        '
        Me.btnProfileChild.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProfileChild.Appearance.Options.UseFont = True
        Me.btnProfileChild.Image = Global.NurseryTouchscreen.My.Resources.Resources.baby_64
        Me.btnProfileChild.Location = New System.Drawing.Point(14, 12)
        Me.btnProfileChild.Name = "btnProfileChild"
        Me.btnProfileChild.Size = New System.Drawing.Size(350, 90)
        Me.btnProfileChild.TabIndex = 12
        Me.btnProfileChild.Text = "Child Profile Picture"
        '
        'btnMaintenance
        '
        Me.btnMaintenance.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMaintenance.Appearance.Options.UseFont = True
        Me.btnMaintenance.Image = Global.NurseryTouchscreen.My.Resources.Resources.maintenance_64
        Me.btnMaintenance.Location = New System.Drawing.Point(758, 228)
        Me.btnMaintenance.Name = "btnMaintenance"
        Me.btnMaintenance.Size = New System.Drawing.Size(350, 90)
        Me.btnMaintenance.TabIndex = 20
        Me.btnMaintenance.Text = "Maintenance"
        '
        'btnReportChild
        '
        Me.btnReportChild.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReportChild.Appearance.Options.UseFont = True
        Me.btnReportChild.Image = Global.NurseryTouchscreen.My.Resources.Resources.User_blue_64
        Me.btnReportChild.Location = New System.Drawing.Point(387, 12)
        Me.btnReportChild.Name = "btnReportChild"
        Me.btnReportChild.Size = New System.Drawing.Size(350, 90)
        Me.btnReportChild.TabIndex = 79
        Me.btnReportChild.Text = "Daily Report (for Child)"
        '
        'btnIncident
        '
        Me.btnIncident.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnIncident.Appearance.Options.UseFont = True
        Me.btnIncident.Image = Global.NurseryTouchscreen.My.Resources.Resources.nurse_64
        Me.btnIncident.Location = New System.Drawing.Point(758, 118)
        Me.btnIncident.Name = "btnIncident"
        Me.btnIncident.Size = New System.Drawing.Size(350, 90)
        Me.btnIncident.TabIndex = 83
        Me.btnIncident.Text = "Accident / Incident"
        '
        'btnObs
        '
        Me.btnObs.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnObs.Appearance.Options.UseFont = True
        Me.btnObs.Image = Global.NurseryTouchscreen.My.Resources.Resources.Monitoring_64
        Me.btnObs.Location = New System.Drawing.Point(758, 12)
        Me.btnObs.Name = "btnObs"
        Me.btnObs.Size = New System.Drawing.Size(350, 90)
        Me.btnObs.TabIndex = 81
        Me.btnObs.Text = "Observation"
        '
        'btn2
        '
        Me.btn2.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn2.Appearance.Options.UseFont = True
        Me.btn2.Enabled = False
        Me.btn2.Location = New System.Drawing.Point(387, 335)
        Me.btn2.Name = "btn2"
        Me.btn2.Size = New System.Drawing.Size(350, 90)
        Me.btn2.TabIndex = 84
        '
        'btn1
        '
        Me.btn1.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn1.Appearance.Options.UseFont = True
        Me.btn1.Enabled = False
        Me.btn1.Location = New System.Drawing.Point(14, 335)
        Me.btn1.Name = "btn1"
        Me.btn1.Size = New System.Drawing.Size(350, 90)
        Me.btn1.TabIndex = 85
        '
        'btnReportSite
        '
        Me.btnReportSite.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReportSite.Appearance.Options.UseFont = True
        Me.btnReportSite.Image = Global.NurseryTouchscreen.My.Resources.Resources.home_64
        Me.btnReportSite.Location = New System.Drawing.Point(387, 228)
        Me.btnReportSite.Name = "btnReportSite"
        Me.btnReportSite.Size = New System.Drawing.Size(350, 90)
        Me.btnReportSite.TabIndex = 80
        Me.btnReportSite.Text = "Daily Report (for Site)"
        '
        'btnReportRoom
        '
        Me.btnReportRoom.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReportRoom.Appearance.Options.UseFont = True
        Me.btnReportRoom.Image = Global.NurseryTouchscreen.My.Resources.Resources.Users_64
        Me.btnReportRoom.Location = New System.Drawing.Point(387, 118)
        Me.btnReportRoom.Name = "btnReportRoom"
        Me.btnReportRoom.Size = New System.Drawing.Size(350, 90)
        Me.btnReportRoom.TabIndex = 78
        Me.btnReportRoom.Text = "Daily Report (for Room)"
        '
        'btnProfileStaff
        '
        Me.btnProfileStaff.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProfileStaff.Appearance.Options.UseFont = True
        Me.btnProfileStaff.Image = Global.NurseryTouchscreen.My.Resources.Resources.woman_64
        Me.btnProfileStaff.Location = New System.Drawing.Point(14, 118)
        Me.btnProfileStaff.Name = "btnProfileStaff"
        Me.btnProfileStaff.Size = New System.Drawing.Size(350, 90)
        Me.btnProfileStaff.TabIndex = 13
        Me.btnProfileStaff.Text = "Staff Profile Picture"
        '
        'btnExit
        '
        Me.btnExit.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Appearance.Options.UseFont = True
        Me.btnExit.Image = Global.NurseryTouchscreen.My.Resources.Resources.Undo_64
        Me.btnExit.Location = New System.Drawing.Point(759, 335)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(350, 90)
        Me.btnExit.TabIndex = 23
        Me.btnExit.Text = "Back"
        '
        'btnProfileContact
        '
        Me.btnProfileContact.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProfileContact.Appearance.Options.UseFont = True
        Me.btnProfileContact.Image = Global.NurseryTouchscreen.My.Resources.Resources.family_64
        Me.btnProfileContact.Location = New System.Drawing.Point(14, 228)
        Me.btnProfileContact.Name = "btnProfileContact"
        Me.btnProfileContact.Size = New System.Drawing.Size(350, 90)
        Me.btnProfileContact.TabIndex = 14
        Me.btnProfileContact.Text = "Contact Profile Picture"
        '
        'frmPhotoTarget
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1121, 444)
        Me.ControlBox = False
        Me.Controls.Add(Me.btn1)
        Me.Controls.Add(Me.btnIncident)
        Me.Controls.Add(Me.btnObs)
        Me.Controls.Add(Me.btn2)
        Me.Controls.Add(Me.btnReportSite)
        Me.Controls.Add(Me.btnReportChild)
        Me.Controls.Add(Me.btnReportRoom)
        Me.Controls.Add(Me.btnMaintenance)
        Me.Controls.Add(Me.btnProfileStaff)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnProfileChild)
        Me.Controls.Add(Me.btnProfileContact)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPhotoTarget"
        Me.Text = "Nursery Management"
        Me.Controls.SetChildIndex(Me.btnProfileContact, 0)
        Me.Controls.SetChildIndex(Me.btnProfileChild, 0)
        Me.Controls.SetChildIndex(Me.btnExit, 0)
        Me.Controls.SetChildIndex(Me.btnProfileStaff, 0)
        Me.Controls.SetChildIndex(Me.btnMaintenance, 0)
        Me.Controls.SetChildIndex(Me.btnReportRoom, 0)
        Me.Controls.SetChildIndex(Me.btnReportChild, 0)
        Me.Controls.SetChildIndex(Me.btnReportSite, 0)
        Me.Controls.SetChildIndex(Me.btn2, 0)
        Me.Controls.SetChildIndex(Me.btnObs, 0)
        Me.Controls.SetChildIndex(Me.btnIncident, 0)
        Me.Controls.SetChildIndex(Me.btn1, 0)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnProfileStaff As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnExit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnProfileChild As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnProfileContact As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMaintenance As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnReportRoom As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnReportChild As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnReportSite As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnIncident As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnObs As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn1 As DevExpress.XtraEditors.SimpleButton
End Class
