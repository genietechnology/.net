﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChildObs
    Inherits NurseryTouchscreen.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tgObs = New NurseryTouchscreen.TouchGrid()
        Me.txtComments = New DevExpress.XtraEditors.MemoEdit()
        Me.btnObs = New DevExpress.XtraEditors.SimpleButton()
        Me.btnPhoto = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAss = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.txtComments.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tgObs
        '
        Me.tgObs.HideFirstColumn = False
        Me.tgObs.Location = New System.Drawing.Point(12, 12)
        Me.tgObs.Name = "tgObs"
        Me.tgObs.Size = New System.Drawing.Size(618, 463)
        Me.tgObs.TabIndex = 87
        '
        'txtComments
        '
        Me.txtComments.Location = New System.Drawing.Point(636, 12)
        Me.txtComments.Name = "txtComments"
        Me.txtComments.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtComments.Properties.Appearance.Options.UseFont = True
        Me.txtComments.Size = New System.Drawing.Size(376, 463)
        Me.txtComments.TabIndex = 92
        '
        'btnObs
        '
        Me.btnObs.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnObs.Appearance.Options.UseFont = True
        Me.btnObs.Image = Global.NurseryTouchscreen.My.Resources.Resources.search_32
        Me.btnObs.Location = New System.Drawing.Point(12, 481)
        Me.btnObs.Name = "btnObs"
        Me.btnObs.Size = New System.Drawing.Size(305, 45)
        Me.btnObs.TabIndex = 89
        Me.btnObs.Text = "Observation"
        '
        'btnPhoto
        '
        Me.btnPhoto.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPhoto.Appearance.Options.UseFont = True
        Me.btnPhoto.Image = Global.NurseryTouchscreen.My.Resources.Resources.camera_32
        Me.btnPhoto.Location = New System.Drawing.Point(3, 3)
        Me.btnPhoto.Name = "btnPhoto"
        Me.btnPhoto.Size = New System.Drawing.Size(190, 45)
        Me.btnPhoto.TabIndex = 1
        Me.btnPhoto.Text = "Take Photo"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton2.Appearance.Options.UseFont = True
        Me.SimpleButton2.Image = Global.NurseryTouchscreen.My.Resources.Resources.Highlightmarker_red_32
        Me.SimpleButton2.Location = New System.Drawing.Point(199, 3)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(362, 45)
        Me.SimpleButton2.TabIndex = 2
        Me.SimpleButton2.Text = "Start Assessment"
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Image = Global.NurseryTouchscreen.My.Resources.Resources.cancel_32
        Me.btnCancel.Location = New System.Drawing.Point(829, 481)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(183, 45)
        Me.btnCancel.TabIndex = 100
        Me.btnCancel.Text = "Close"
        '
        'btnAss
        '
        Me.btnAss.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAss.Appearance.Options.UseFont = True
        Me.btnAss.Image = Global.NurseryTouchscreen.My.Resources.Resources.Test_paper_32
        Me.btnAss.Location = New System.Drawing.Point(325, 481)
        Me.btnAss.Name = "btnAss"
        Me.btnAss.Size = New System.Drawing.Size(305, 45)
        Me.btnAss.TabIndex = 101
        Me.btnAss.Text = "Assessment"
        '
        'frmChildObs
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(1024, 538)
        Me.Controls.Add(Me.btnAss)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.txtComments)
        Me.Controls.Add(Me.btnObs)
        Me.Controls.Add(Me.tgObs)
        Me.Name = "frmChildObs"
        Me.Text = "Observations"
        CType(Me.txtComments.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnPhoto As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tgObs As NurseryTouchscreen.TouchGrid
    Friend WithEvents btnObs As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtComments As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAss As DevExpress.XtraEditors.SimpleButton

End Class
