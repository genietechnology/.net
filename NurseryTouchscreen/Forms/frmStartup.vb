﻿Imports Microsoft.Win32
Imports System.Data
Imports DevExpress.XtraSpellChecker
Imports System.IO
Imports System.ComponentModel

Public Class frmStartup

    Dim _Loading As Boolean

    Private Sub frmWWStartup_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ErrorHandler.Activate()

        txtStatus.Text = ""
        _Loading = True
        txtStatus.Text = ""

        Cursor.Current = Cursors.WaitCursor
        Application.DoEvents()

        DevExpress.Utils.AppearanceObject.DefaultFont = New Font("Segoe UI", 14)

        'DevExpress.LookAndFeel.UserLookAndFeel.Default.SkinName = "DevExpress Dark Style"

        DevExpress.XtraEditors.WindowsFormsSettings.TouchUIMode = DevExpress.LookAndFeel.TouchUIMode.True
        DevExpress.XtraEditors.WindowsFormsSettings.ScrollUIMode = DevExpress.XtraEditors.ScrollUIMode.Touch

        'check for photo upload every minute
        timUploadPhotos.Interval = 60000 * 1
        timUploadPhotos.Stop()

        'check for shutdown every 15 minutes
        timShutdown.Interval = 60000 * 15
        timShutdown.Start()

    End Sub

    Private Sub frmStartup_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        If _Loading Then

            _Loading = False

            UpdateText("Starting System Build " & SharedModule.Version, False)
            UpdateText("Loading Application Settings", False)
            LoadSettings()

            UpdateText("Setting Parameters", False)
            If SetParameters() Then

                'ensure we have a site parameter
                If Parameters.Site = "" Then
                    Msgbox("Site Parameter not found!", MessageBoxIcon.Exclamation, "SetParameters")
                    Application.Exit()
                Else

                    UpdateText("Setting Site ID", False)
                    If SetSiteID() Then

                        UpdateText("Checking Day", False)
                        If SharedModule.DayExists Then
                            SetupSpellChecker()
                            SetTempFolder()
                            ConnectToDoorController()
                            LoadMainForm(False)
                        Else
                            Application.Exit()
                        End If

                    Else
                        Msgbox("Specified Site (" + Parameters.Site + ") not found.", MessageBoxIcon.Exclamation, "SetSiteID")
                        Application.Exit()
                    End If
                End If

            Else
                Msgbox("There was an error loading parameters!", MessageBoxIcon.Exclamation, "SetParameters")
                Application.Exit()
            End If

        End If

    End Sub

    Private Function SetSiteID() As Boolean

        Dim _SQL As String = "select ID from Sites where name = '" + Parameters.Site + "'"
        Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
        If _DR IsNot Nothing Then
            Parameters.SiteID = _DR.Item("ID").ToString
            Return True
        Else
            Return False
        End If

    End Function

    Private Sub ConnectToDoorController()

        VLCLauncher(Parameters.CCTV.Video1IP)
        VLCLauncher(Parameters.CCTV.Video2IP)
        VLCLauncher(Parameters.CCTV.Video3IP)

        If Parameters.AccessControl.DoorControl Then

            Dim _IP As String = Parameters.AccessControl.DoorControlIP
            UpdateText("Connecting to Door Controller: " + _IP + "...", False)

            Dim _ErrorText As String = ""
            If DoorHandler.Connect(_ErrorText) Then
                UpdateText("Connection to Door Controller Successful", False)
            Else

                Clipboard.Clear()
                Clipboard.SetText(_ErrorText)

                Dim _Mess As String = "Connection to Door Controller Failed: " + _ErrorText
                UpdateText(_Mess, False)

                If Parameters.DebugMode Then Msgbox(_Mess, MessageBoxIcon.Error, "ConnectToDoorController")

            End If

        End If

    End Sub

    Private Function SetParameters() As Boolean

        If SetParameters("MASTER") Then
            Return SetParameters(My.Computer.Name)
        Else
            Return False
        End If

    End Function

    Private Function SetParameters(ByVal DeviceName As String) As Boolean

        Dim _Return As Boolean = True
        Dim _SQL As String = "select * from TabletParams where device = '" + DeviceName.ToUpper + "' order by parent, param_id"
        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)

        If Not _DT Is Nothing Then

            For Each _DR As DataRow In _DT.Rows
                _Return = SetParameter(_DR)
                If Not _Return Then
                    Exit For
                End If
            Next

            _DT.Dispose()
            _DT = Nothing

        End If

        Return _Return

    End Function

    Private Function SetParameter(ByRef DR As DataRow) As Boolean

        Dim _Name As String = DR.Item("param_id").ToString.ToUpper
        Dim _Type As String = DR.Item("param_type").ToString.ToUpper
        Dim _StringValue As String = DR.Item("param_value").ToString

        Select Case _Type

            Case "BOOLEAN"

                Dim _BooleanValue As Boolean = ReturnBooleanValue(_StringValue)
                Return SetBooleanParameter(_Name, _BooleanValue)

            Case "STRING"
                Return SetParameter(_Name, _StringValue)

            Case Else
                Return False

        End Select

    End Function

    Private Function SetBooleanParameter(ByVal ParameterName As String, ByVal ParameterValue As Boolean) As Boolean

        If ParameterName = "AC" Then Parameters.AccessControl.DoorControl = ParameterValue : Return True
        If ParameterName = "PAXTON" Then Parameters.AccessControl.Paxton = ParameterValue : Return True

        If ParameterName = "DISABLECAPTURE" Then Parameters.Capture.CaptureEnabled = Not ParameterValue : Return True
        If ParameterName = "DISABLECHECKIN" Then Parameters.DisableCheckIns = ParameterValue : Return True
        If ParameterName = "DISABLESCREENSAVER" Then Parameters.DisableScreenSaver = ParameterValue : Return True
        If ParameterName = "DISABLEPOTTY" Then Parameters.DisablePotty = ParameterValue : Return True

        If ParameterName = "HIDETELEPHONE" Then Parameters.HideTelephoneNumbers = ParameterValue : Return True

        If ParameterName = "BASICMEALS" Then Parameters.UseBasicMeals = ParameterValue : Return True
        If ParameterName = "CROSSCHECK" Then Parameters.CrossCheckMandatory = ParameterValue : Return True
        If ParameterName = "DEBUG" Then Parameters.DebugMode = ParameterValue : Return True
        If ParameterName = "MAXIMISE" Then Parameters.MaximiseForms = ParameterValue : Return True
        If ParameterName = "SPELLCHECK" Then Parameters.SpellChecking = ParameterValue : Return True
        If ParameterName = "WEBSERVICES" Then Parameters.UseWebServices = ParameterValue : Return True
        If ParameterName = "DISABLELOCATION" Then Parameters.DisableLocation = ParameterValue : Return True

        If ParameterName = "CUSTOMCCTV" Then Parameters.CCTV.CustomAddress = ParameterValue : Return True
        If ParameterName = "SORTSURNAME" Then Parameters.SortSurname = ParameterValue : Return True

        If ParameterName = "CHECKSIGCHILD" Then Parameters.CheckSignatureChildren = ParameterValue : Return True
        If ParameterName = "CHECKSIGSTAFF" Then Parameters.CheckSignatureStaff = ParameterValue : Return True

        If ParameterName = "SHOWPHOTOS" Then Parameters.ShowPhotos = ParameterValue : Return True
        If ParameterName = "TOPLEFT" Then Parameters.TopLeft = ParameterValue : Return True

        If ParameterName = "INCMANAGER" Then Parameters.IncidentsMandatoryManager = ParameterValue : Return True
        If ParameterName = "INCWITNESS" Then Parameters.IncidentsMandatoryWitness = ParameterValue : Return True
        If ParameterName = "INCEQUIPMENT" Then Parameters.IncidentsMandatoryEquipment = ParameterValue : Return True
        If ParameterName = "INCRISK" Then Parameters.IncidentsMandatoryRisk = ParameterValue : Return True

        If ParameterName = "CHECKCHILDPIN" Then Parameters.CheckChildPIN = ParameterValue : Return True

        If ParameterName = "HIDENAMES" Then Parameters.HideChildName = ParameterValue : Return True
        If ParameterName = "MEDICATIONSIGNATURE" Then Parameters.MedicationSignature = ParameterValue : Return True

        If ParameterName = "CONTACTSIGNATUREIN" Then Parameters.ContactSignatureIn = ParameterValue : Return True
        If ParameterName = "CONTACTSIGNATUREOUT" Then Parameters.ContactSignatureOut = ParameterValue : Return True

        Return False

    End Function

    Private Function SetParameter(ByVal ParameterName As String, ByVal ParameterValue As String) As Boolean

        If ParameterName = "SITE" Then Parameters.Site = ParameterValue : Return True

        If ParameterName = "ACIP" Then Parameters.AccessControl.DoorControlIP = ParameterValue : Return True
        If ParameterName = "DOOR1NAME" Then Parameters.AccessControl.Door1Name = ParameterValue : Return True
        If ParameterName = "DOOR1SERIAL" Then Parameters.AccessControl.Door1Serial = ParameterValue : Return True
        If ParameterName = "DOOR2NAME" Then Parameters.AccessControl.Door2Name = ParameterValue : Return True
        If ParameterName = "DOOR2SERIAL" Then Parameters.AccessControl.Door2Serial = ParameterValue : Return True
        If ParameterName = "PAXTONUSER" Then Parameters.AccessControl.PaxtonUser = ParameterValue : Return True
        If ParameterName = "PAXTONPASSWORD" Then Parameters.AccessControl.PaxtonPassword = ParameterValue : Return True
        If ParameterName = "MONITORSERIAL" Then Parameters.AccessControl.MonitorSerial = ParameterValue : Return True

        If ParameterName = "CAMERACOMMAND" Then Parameters.Capture.CameraCommand = ParameterValue : Return True
        If ParameterName = "CAMERADEVICE" Then Parameters.Capture.CameraDevice = ParameterValue : Return True

        If ParameterName = "CAMIP1" Then Parameters.CCTV.Video1IP = ParameterValue : Return True
        If ParameterName = "CAMIP2" Then Parameters.CCTV.Video2IP = ParameterValue : Return True
        If ParameterName = "CAMIP3" Then Parameters.CCTV.Video3IP = ParameterValue : Return True

        If ParameterName = "ROOM" Then Parameters.DefaultRoomParameter = ParameterValue : Return True

        If ParameterName = "PATHSHARED" Then Parameters.Paths.SharedFolderPath = ParameterValue : Return True
        If ParameterName = "PATHVLC" Then Parameters.Paths.VLCExePath = ParameterValue : Return True
        If ParameterName = "PATHPOLICIES" Then Parameters.Paths.Policies = ParameterValue : Return True

        If ParameterName = "LATEMEDICATIONREASON" Then Parameters.LateMedicationReason = ParameterValue : Return True

        If ParameterName = "UNLOCKMODE" Then
            Select Case ParameterValue.ToUpper
                Case "DISABLED"
                    Parameters.UnlockMode = SharedModule.EnumUnlockMode.Disabled
                    Return True
                Case "CODE"
                    Parameters.UnlockMode = SharedModule.EnumUnlockMode.Code
                    Return True
                Case "PIN"
                    Parameters.UnlockMode = SharedModule.EnumUnlockMode.PIN
                    Return True
                Case Else
                    Return False
            End Select
        End If

        If ParameterName = "UNLOCKCODE" Then Parameters.UnlockCode = ParameterValue : Return True

        If ParameterName = "USAGEMODE" Then
            Select Case ParameterValue.ToUpper
                Case "FOYER"
                    SharedModule.UsageMode = SharedModule.EnumUsage.Foyer
                    Return True
                Case "CLUB"
                    SharedModule.UsageMode = SharedModule.EnumUsage.ClubMode
                    Return True
                Case Else
                    SharedModule.UsageMode = SharedModule.EnumUsage.StandardTouch
                    Return True
            End Select
        End If

        Return False

    End Function

    Private Function ReturnBooleanValue(ByVal StringIn As String)
        Select Case StringIn.ToUpper
            Case "TRUE", "YES", "Y", "T"
                Return True
            Case Else
                Return False
        End Select
    End Function

    Private Sub LoadMainForm(ByVal SyncRequired As Boolean)

        Me.Hide()

        timUploadPhotos.Start()

        If Parameters.DisableScreenSaver Then
            Dim _frmMain As New frmMain(SyncRequired)
            _frmMain.Show()
        Else
            Dim _frm As New frmScreenSaver
            _frm.Show()
        End If

    End Sub

    Private Sub SetTempFolder()

        UpdateText("Checking Temp Folder", False)
        If Not IO.Directory.Exists(SharedModule.TempFolderPath) Then
            UpdateText("Creating Temp Folder", False)
            IO.Directory.CreateDirectory(SharedModule.TempFolderPath)
        End If

    End Sub

    Private Sub SetupSpellChecker()

        If Parameters.SpellChecking Then

            UpdateText("Loading Spell Check Dictionary", False)

            Dim _Dictionary As String = IO.Path.GetDirectoryName(Application.ExecutablePath) + "\dict\en_GB.dic"
            Dim _Grammar As String = IO.Path.GetDirectoryName(Application.ExecutablePath) + "\dict\en_GB.aff"

            If _Dictionary <> "" AndAlso _Grammar <> "" Then
                Parameters.SpellChecking = AddDictionary(_Dictionary, _Grammar)
            Else
                Parameters.SpellChecking = False
            End If

            If Parameters.SpellChecking Then
                UpdateText("Spell Checking Enabled", False)
            Else
                UpdateText("Spell Checking Disabled, Load Failed", False)
            End If

        Else
            UpdateText("Spell Checking Disabled", False)
            Parameters.SpellChecking = False
        End If

    End Sub

    Private Function AddDictionary(ByVal DictionaryPath As String, ByVal GrammarPath As String) As Boolean

        If Not File.Exists(DictionaryPath) Then Return False
        If Not File.Exists(GrammarPath) Then Return False

        Dim _Return As Boolean = False

        Try

            Dim _ood As New SpellCheckerOpenOfficeDictionary
            _ood.DictionaryPath = DictionaryPath
            _ood.GrammarPath = GrammarPath
            _ood.Load()

            If _ood.Loaded Then
                SharedDictionaryStorage1.Dictionaries.Add(_ood)
                _Return = True
            End If

            _ood = Nothing

        Catch ex As Exception

        End Try

        Return _Return

    End Function

    'Private Sub PullTables()

    '    Dim _RDA As New RDA

    '    UpdateText("Downloading Day")
    '    If _RDA.PullDay() Then

    '        UpdateText("Downloading Parameters")
    '        _RDA.PullParameters()

    '        UpdateText("Downloading Children")
    '        _RDA.PullChildren()

    '        UpdateText("Downloading Contacts")
    '        _RDA.PullContacts()

    '        UpdateText("Downloading Register")
    '        _RDA.PullRegister()

    '        UpdateText("Downloading Activity")
    '        _RDA.PullActivity()

    '        UpdateText("Downloading Food Register")
    '        _RDA.PullFoodRegister()

    '        UpdateText("Downloading Medication Requests")
    '        _RDA.PullMedicalAdmin()

    '        UpdateText("Downloading Medication Log")
    '        _RDA.PullMedicalLog()

    '        UpdateText("Downloading Accident Log")
    '        _RDA.PullAccidentLog()

    '        UpdateText("Downloading Staff")
    '        _RDA.PullStaff()

    '        UpdateText("Downloading Groups")
    '        _RDA.PullGroups()

    '        UpdateText("Downloading Meals")
    '        _RDA.PullMeals()

    '        UpdateText("Downloading Requests")
    '        _RDA.PullRequests()

    '        UpdateText("Downloading Quick Texts")
    '        _RDA.PullQuickTexts()

    '    End If

    '    _RDA = Nothing

    'End Sub

    'Private Function CheckSystemTables() As Boolean

    '    Dim _RDA As New RDA

    '    'check local db exists, if it doesnt then create it
    '    UpdateText("Connect to local database", False)
    '    If _RDA.CheckDBExists Then

    '        Dim _DayExists As Boolean = _RDA.CheckTableExists("day", False)
    '        Return _DayExists

    '    Else
    '        Return False
    '    End If

    '    _RDA = Nothing

    'End Function

    Private Sub UpdateText(ByVal TextIn As String, Optional ByVal IncrementProgressBar As Boolean = True)

        If txtStatus.Text = "" Then
            txtStatus.Text = TextIn & "..."
        Else
            txtStatus.Text = txtStatus.Text & vbCrLf & TextIn & "..."
        End If

        txtStatus.SelectionStart = txtStatus.Text.Length
        txtStatus.ScrollToCaret()

        Application.DoEvents()

    End Sub

    Private Sub LoadSettings()
        Parameters.ServerIP = My.Settings.ServerIP
        Parameters.DirectConnectionString = My.Settings.DirectConnectionString
        Parameters.ServiceIP = My.Settings.ServiceIP
    End Sub

    Private Sub timShutdown_Tick(sender As Object, e As EventArgs) Handles timShutdown.Tick
        If Now.Hour = 23 AndAlso Now.Minute >= 30 Then
            DoorHandler.Disconnect()
            End
        End If
    End Sub

    Private Sub VLCLauncher(ByVal IPAddress As String)

        If Parameters.Paths.VLCExePath = "" Then Exit Sub
        If IPAddress = "" Then Exit Sub

        Dim _Command As String = Parameters.Paths.VLCExePath
        Dim _Args As String = ""

        If Parameters.CCTV.CustomAddress Then
            _Args = IPAddress
        Else
            _Args += " --qt-minimal-view"
            _Args += " --no-video-deco"
            '_Args += " --width=" + WindowSize.Width.ToString
            '_Args += " --height=" + WindowSize.Height.ToString
            _Args += " rtsp://" + IPAddress + ":554/play1.sdp"
        End If

        Try
            Process.Start(_Command, _Args)

        Catch ex As Exception
            Msgbox(ex.Message, MessageBoxIcon.Exclamation, "VLC Launcher")
        End Try

    End Sub

    Private Sub timUploadPhotos_Tick(sender As Object, e As EventArgs) Handles timUploadPhotos.Tick
        'check if there are any photos to upload
        If MediaHandler.LocalMediaContent.Count > 0 Then
            timUploadPhotos.Stop()
            bgwUploadPhotos.RunWorkerAsync()
        End If
    End Sub

    Private Sub bgwUploadPhotos_DoWork(sender As Object, e As DoWorkEventArgs) Handles bgwUploadPhotos.DoWork
        MediaHandler.UploadMedia()
    End Sub

    Private Sub bgwUploadPhotos_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgwUploadPhotos.RunWorkerCompleted
        timUploadPhotos.Start()
    End Sub

End Class