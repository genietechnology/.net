﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEYFSAss
    Inherits frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling2 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling3 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling4 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling5 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling6 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEYFSAss))
        Dim OptionsSpelling7 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.panBottom = New DevExpress.XtraEditors.PanelControl()
        Me.btnDelete = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAdd = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAccept = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.txtCyan = New DevExpress.XtraEditors.TextEdit()
        Me.txtGreen = New DevExpress.XtraEditors.TextEdit()
        Me.txtYellow = New DevExpress.XtraEditors.TextEdit()
        Me.txtOrange = New DevExpress.XtraEditors.TextEdit()
        Me.txtRed = New DevExpress.XtraEditors.TextEdit()
        Me.tc = New DevExpress.XtraTab.XtraTabControl()
        Me.tabObs = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.txtComments = New DevExpress.XtraEditors.MemoEdit()
        Me.fldComments = New NurseryTouchscreen.Field()
        Me.fldChild = New NurseryTouchscreen.Field()
        Me.fldStatus = New NurseryTouchscreen.Field()
        Me.fldDateTime = New NurseryTouchscreen.Field()
        Me.fldAssessor = New NurseryTouchscreen.Field()
        Me.tabDM = New DevExpress.XtraTab.XtraTabPage()
        Me.tgDM = New NurseryTouchscreen.TouchGrid()
        Me.tabCOEL = New DevExpress.XtraTab.XtraTabPage()
        Me.tgCOEL = New NurseryTouchscreen.TouchGrid()
        Me.tabLS = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.radI5 = New DevExpress.XtraEditors.CheckEdit()
        Me.radI4 = New DevExpress.XtraEditors.CheckEdit()
        Me.radI3 = New DevExpress.XtraEditors.CheckEdit()
        Me.radI2 = New DevExpress.XtraEditors.CheckEdit()
        Me.radI1 = New DevExpress.XtraEditors.CheckEdit()
        Me.radWB2 = New DevExpress.XtraEditors.CheckEdit()
        Me.radWB5 = New DevExpress.XtraEditors.CheckEdit()
        Me.radWB4 = New DevExpress.XtraEditors.CheckEdit()
        Me.radWB3 = New DevExpress.XtraEditors.CheckEdit()
        Me.radWB1 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.tabNS = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.fldNextSteps = New NurseryTouchscreen.Field()
        Me.txtNextSteps = New DevExpress.XtraEditors.MemoEdit()
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panBottom.SuspendLayout()
        CType(Me.txtCyan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOrange.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tc, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tc.SuspendLayout()
        Me.tabObs.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.txtComments.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabDM.SuspendLayout()
        Me.tabCOEL.SuspendLayout()
        Me.tabLS.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.radI5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radI4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radI3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radI2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radI1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radWB2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radWB5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radWB4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radWB3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radWB1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabNS.SuspendLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.txtNextSteps.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'panBottom
        '
        Me.panBottom.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panBottom.Appearance.Options.UseFont = True
        Me.panBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.panBottom.Controls.Add(Me.btnDelete)
        Me.panBottom.Controls.Add(Me.btnAdd)
        Me.panBottom.Controls.Add(Me.btnAccept)
        Me.panBottom.Controls.Add(Me.btnCancel)
        Me.panBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panBottom.Location = New System.Drawing.Point(0, 482)
        Me.panBottom.Name = "panBottom"
        Me.panBottom.Size = New System.Drawing.Size(1024, 56)
        Me.panBottom.TabIndex = 10
        Me.panBottom.Controls.SetChildIndex(Me.btnCancel, 0)
        Me.panBottom.Controls.SetChildIndex(Me.btnAccept, 0)
        Me.panBottom.Controls.SetChildIndex(Me.btnAdd, 0)
        Me.panBottom.Controls.SetChildIndex(Me.btnDelete, 0)
        '
        'btnDelete
        '
        Me.btnDelete.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Appearance.Options.UseFont = True
        Me.btnDelete.Image = Global.NurseryTouchscreen.My.Resources.Resources.delete_32
        Me.btnDelete.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnDelete.Location = New System.Drawing.Point(63, 2)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(45, 45)
        Me.btnDelete.TabIndex = 98
        '
        'btnAdd
        '
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.Appearance.Options.UseFont = True
        Me.btnAdd.Image = Global.NurseryTouchscreen.My.Resources.Resources.add_32
        Me.btnAdd.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnAdd.Location = New System.Drawing.Point(12, 2)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(45, 45)
        Me.btnAdd.TabIndex = 97
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAccept.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAccept.Appearance.Options.UseFont = True
        Me.btnAccept.Image = Global.NurseryTouchscreen.My.Resources.Resources.success_32
        Me.btnAccept.Location = New System.Drawing.Point(706, 2)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(150, 45)
        Me.btnAccept.TabIndex = 0
        Me.btnAccept.Text = "Accept"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Image = Global.NurseryTouchscreen.My.Resources.Resources.cancel_32
        Me.btnCancel.Location = New System.Drawing.Point(862, 2)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(150, 45)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "Cancel"
        '
        'txtCyan
        '
        Me.txtCyan.Location = New System.Drawing.Point(677, 19)
        Me.txtCyan.Name = "txtCyan"
        Me.txtCyan.Properties.Appearance.BackColor = System.Drawing.Color.Blue
        Me.txtCyan.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtCyan, True)
        Me.txtCyan.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtCyan, OptionsSpelling1)
        Me.txtCyan.TabIndex = 15
        Me.txtCyan.Visible = False
        '
        'txtGreen
        '
        Me.txtGreen.Location = New System.Drawing.Point(648, 19)
        Me.txtGreen.Name = "txtGreen"
        Me.txtGreen.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtGreen.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtGreen, True)
        Me.txtGreen.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtGreen, OptionsSpelling2)
        Me.txtGreen.TabIndex = 14
        Me.txtGreen.Visible = False
        '
        'txtYellow
        '
        Me.txtYellow.Location = New System.Drawing.Point(619, 19)
        Me.txtYellow.Name = "txtYellow"
        Me.txtYellow.Properties.Appearance.BackColor = System.Drawing.Color.Gold
        Me.txtYellow.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtYellow, True)
        Me.txtYellow.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtYellow, OptionsSpelling3)
        Me.txtYellow.TabIndex = 13
        Me.txtYellow.Visible = False
        '
        'txtOrange
        '
        Me.txtOrange.Location = New System.Drawing.Point(590, 19)
        Me.txtOrange.Name = "txtOrange"
        Me.txtOrange.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtOrange.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtOrange, True)
        Me.txtOrange.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtOrange, OptionsSpelling4)
        Me.txtOrange.TabIndex = 12
        Me.txtOrange.Visible = False
        '
        'txtRed
        '
        Me.txtRed.Location = New System.Drawing.Point(561, 19)
        Me.txtRed.Name = "txtRed"
        Me.txtRed.Properties.Appearance.BackColor = System.Drawing.Color.Red
        Me.txtRed.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtRed, True)
        Me.txtRed.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtRed, OptionsSpelling5)
        Me.txtRed.TabIndex = 11
        Me.txtRed.Visible = False
        '
        'tc
        '
        Me.tc.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tc.Appearance.Options.UseFont = True
        Me.tc.Location = New System.Drawing.Point(12, 12)
        Me.tc.Name = "tc"
        Me.tc.SelectedTabPage = Me.tabObs
        Me.tc.Size = New System.Drawing.Size(1000, 466)
        Me.tc.TabIndex = 11
        Me.tc.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabObs, Me.tabDM, Me.tabCOEL, Me.tabLS, Me.tabNS})
        '
        'tabObs
        '
        Me.tabObs.Controls.Add(Me.GroupControl2)
        Me.tabObs.Name = "tabObs"
        Me.tabObs.Size = New System.Drawing.Size(994, 438)
        Me.tabObs.Text = "Assessment"
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl2.Controls.Add(Me.txtComments)
        Me.GroupControl2.Controls.Add(Me.fldComments)
        Me.GroupControl2.Controls.Add(Me.fldChild)
        Me.GroupControl2.Controls.Add(Me.fldStatus)
        Me.GroupControl2.Controls.Add(Me.fldDateTime)
        Me.GroupControl2.Controls.Add(Me.fldAssessor)
        Me.GroupControl2.Location = New System.Drawing.Point(3, 3)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.ShowCaption = False
        Me.GroupControl2.Size = New System.Drawing.Size(988, 432)
        Me.GroupControl2.TabIndex = 2
        Me.GroupControl2.Text = "GroupControl2"
        '
        'txtComments
        '
        Me.txtComments.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtComments.Location = New System.Drawing.Point(5, 290)
        Me.txtComments.Name = "txtComments"
        Me.txtComments.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtComments.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtComments, True)
        Me.txtComments.Size = New System.Drawing.Size(978, 137)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtComments, OptionsSpelling6)
        Me.txtComments.TabIndex = 101
        '
        'fldComments
        '
        Me.fldComments.ButtonCustom = False
        Me.fldComments.ButtonSQL = Nothing
        Me.fldComments.ButtonText = ""
        Me.fldComments.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Enter
        Me.fldComments.ButtonWidth = 50.0!
        Me.fldComments.FamilyID = Nothing
        Me.fldComments.HideText = True
        Me.fldComments.LabelText = "Comments"
        Me.fldComments.LabelWidth = 150.0!
        Me.fldComments.Location = New System.Drawing.Point(5, 233)
        Me.fldComments.MaximumSize = New System.Drawing.Size(1000, 51)
        Me.fldComments.MinimumSize = New System.Drawing.Size(200, 51)
        Me.fldComments.Name = "fldComments"
        Me.fldComments.QuickTextList = ""
        Me.fldComments.Size = New System.Drawing.Size(978, 51)
        Me.fldComments.TabIndex = 100
        Me.fldComments.ValueDateTime = Nothing
        Me.fldComments.ValueID = Nothing
        Me.fldComments.ValueMaxLength = 0
        Me.fldComments.ValueText = ""
        '
        'fldChild
        '
        Me.fldChild.ButtonCustom = False
        Me.fldChild.ButtonSQL = Nothing
        Me.fldChild.ButtonText = ""
        Me.fldChild.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Lookup
        Me.fldChild.ButtonWidth = 0!
        Me.fldChild.FamilyID = Nothing
        Me.fldChild.HideText = False
        Me.fldChild.LabelText = "Child"
        Me.fldChild.LabelWidth = 150.0!
        Me.fldChild.Location = New System.Drawing.Point(5, 5)
        Me.fldChild.MaximumSize = New System.Drawing.Size(1000, 51)
        Me.fldChild.MinimumSize = New System.Drawing.Size(200, 51)
        Me.fldChild.Name = "fldChild"
        Me.fldChild.QuickTextList = ""
        Me.fldChild.Size = New System.Drawing.Size(978, 51)
        Me.fldChild.TabIndex = 99
        Me.fldChild.ValueDateTime = Nothing
        Me.fldChild.ValueID = Nothing
        Me.fldChild.ValueMaxLength = 0
        Me.fldChild.ValueText = ""
        '
        'fldStatus
        '
        Me.fldStatus.ButtonCustom = True
        Me.fldStatus.ButtonSQL = Nothing
        Me.fldStatus.ButtonText = ""
        Me.fldStatus.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Lookup
        Me.fldStatus.ButtonWidth = 50.0!
        Me.fldStatus.FamilyID = Nothing
        Me.fldStatus.HideText = False
        Me.fldStatus.LabelText = "Status"
        Me.fldStatus.LabelWidth = 150.0!
        Me.fldStatus.Location = New System.Drawing.Point(5, 176)
        Me.fldStatus.MaximumSize = New System.Drawing.Size(1000, 51)
        Me.fldStatus.MinimumSize = New System.Drawing.Size(200, 51)
        Me.fldStatus.Name = "fldStatus"
        Me.fldStatus.QuickTextList = ""
        Me.fldStatus.Size = New System.Drawing.Size(978, 51)
        Me.fldStatus.TabIndex = 98
        Me.fldStatus.ValueDateTime = Nothing
        Me.fldStatus.ValueID = Nothing
        Me.fldStatus.ValueMaxLength = 0
        Me.fldStatus.ValueText = ""
        '
        'fldDateTime
        '
        Me.fldDateTime.ButtonCustom = False
        Me.fldDateTime.ButtonSQL = Nothing
        Me.fldDateTime.ButtonText = ""
        Me.fldDateTime.ButtonType = NurseryTouchscreen.Field.EnumButtonType.DateTime
        Me.fldDateTime.ButtonWidth = 50.0!
        Me.fldDateTime.FamilyID = Nothing
        Me.fldDateTime.HideText = False
        Me.fldDateTime.LabelText = "Assessment Date"
        Me.fldDateTime.LabelWidth = 150.0!
        Me.fldDateTime.Location = New System.Drawing.Point(5, 119)
        Me.fldDateTime.MaximumSize = New System.Drawing.Size(1000, 51)
        Me.fldDateTime.MinimumSize = New System.Drawing.Size(200, 51)
        Me.fldDateTime.Name = "fldDateTime"
        Me.fldDateTime.QuickTextList = ""
        Me.fldDateTime.Size = New System.Drawing.Size(978, 51)
        Me.fldDateTime.TabIndex = 97
        Me.fldDateTime.ValueDateTime = Nothing
        Me.fldDateTime.ValueID = Nothing
        Me.fldDateTime.ValueMaxLength = 0
        Me.fldDateTime.ValueText = ""
        '
        'fldAssessor
        '
        Me.fldAssessor.ButtonCustom = True
        Me.fldAssessor.ButtonSQL = Nothing
        Me.fldAssessor.ButtonText = ""
        Me.fldAssessor.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Lookup
        Me.fldAssessor.ButtonWidth = 50.0!
        Me.fldAssessor.FamilyID = Nothing
        Me.fldAssessor.HideText = False
        Me.fldAssessor.LabelText = "Assessed By"
        Me.fldAssessor.LabelWidth = 150.0!
        Me.fldAssessor.Location = New System.Drawing.Point(5, 62)
        Me.fldAssessor.MaximumSize = New System.Drawing.Size(1000, 51)
        Me.fldAssessor.MinimumSize = New System.Drawing.Size(200, 51)
        Me.fldAssessor.Name = "fldAssessor"
        Me.fldAssessor.QuickTextList = ""
        Me.fldAssessor.Size = New System.Drawing.Size(978, 51)
        Me.fldAssessor.TabIndex = 96
        Me.fldAssessor.ValueDateTime = Nothing
        Me.fldAssessor.ValueID = Nothing
        Me.fldAssessor.ValueMaxLength = 0
        Me.fldAssessor.ValueText = ""
        '
        'tabDM
        '
        Me.tabDM.Controls.Add(Me.tgDM)
        Me.tabDM.Name = "tabDM"
        Me.tabDM.Size = New System.Drawing.Size(994, 438)
        Me.tabDM.Text = "Development Matters Statements"
        '
        'tgDM
        '
        Me.tgDM.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tgDM.HideFirstColumn = False
        Me.tgDM.Location = New System.Drawing.Point(3, 3)
        Me.tgDM.Name = "tgDM"
        Me.tgDM.Size = New System.Drawing.Size(988, 432)
        Me.tgDM.TabIndex = 96
        '
        'tabCOEL
        '
        Me.tabCOEL.Controls.Add(Me.tgCOEL)
        Me.tabCOEL.Name = "tabCOEL"
        Me.tabCOEL.Size = New System.Drawing.Size(994, 438)
        Me.tabCOEL.Text = "Characteristics of Effective Learning"
        '
        'tgCOEL
        '
        Me.tgCOEL.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tgCOEL.HideFirstColumn = False
        Me.tgCOEL.Location = New System.Drawing.Point(3, 3)
        Me.tgCOEL.Name = "tgCOEL"
        Me.tgCOEL.Size = New System.Drawing.Size(988, 432)
        Me.tgCOEL.TabIndex = 97
        '
        'tabLS
        '
        Me.tabLS.Controls.Add(Me.GroupControl1)
        Me.tabLS.Name = "tabLS"
        Me.tabLS.Size = New System.Drawing.Size(994, 438)
        Me.tabLS.Text = "Leuven Scale"
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.TableLayoutPanel1)
        Me.GroupControl1.Location = New System.Drawing.Point(3, 3)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(988, 432)
        Me.GroupControl1.TabIndex = 1
        Me.GroupControl1.Text = "GroupControl1"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(496, 5)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(103, 23)
        Me.LabelControl2.TabIndex = 2
        Me.LabelControl2.Text = "Involvement"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(8, 5)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(89, 23)
        Me.LabelControl1.TabIndex = 1
        Me.LabelControl1.Text = "Well-being"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 4
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.radI5, 2, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.radI4, 2, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.radI3, 2, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.radI2, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.radI1, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.radWB2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.radWB5, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.radWB4, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.radWB3, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.radWB1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.LabelControl3, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.LabelControl4, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.LabelControl5, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.LabelControl6, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.LabelControl7, 1, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.LabelControl8, 3, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.LabelControl9, 3, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.LabelControl10, 3, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.LabelControl11, 3, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.LabelControl12, 3, 4)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(5, 34)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(978, 393)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'radI5
        '
        Me.radI5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radI5.Location = New System.Drawing.Point(491, 315)
        Me.radI5.Name = "radI5"
        Me.radI5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radI5.Properties.Appearance.Options.UseFont = True
        Me.radI5.Properties.AutoHeight = False
        Me.radI5.Properties.Caption = "Extremely High"
        Me.radI5.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radI5.Properties.RadioGroupIndex = 1
        Me.radI5.Size = New System.Drawing.Size(189, 75)
        Me.radI5.TabIndex = 9
        Me.radI5.TabStop = False
        '
        'radI4
        '
        Me.radI4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radI4.Location = New System.Drawing.Point(491, 237)
        Me.radI4.Name = "radI4"
        Me.radI4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radI4.Properties.Appearance.Options.UseFont = True
        Me.radI4.Properties.AutoHeight = False
        Me.radI4.Properties.Caption = "High"
        Me.radI4.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radI4.Properties.RadioGroupIndex = 1
        Me.radI4.Size = New System.Drawing.Size(189, 72)
        Me.radI4.TabIndex = 8
        Me.radI4.TabStop = False
        '
        'radI3
        '
        Me.radI3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radI3.Location = New System.Drawing.Point(491, 159)
        Me.radI3.Name = "radI3"
        Me.radI3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radI3.Properties.Appearance.Options.UseFont = True
        Me.radI3.Properties.AutoHeight = False
        Me.radI3.Properties.Caption = "Moderate"
        Me.radI3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radI3.Properties.RadioGroupIndex = 1
        Me.radI3.Size = New System.Drawing.Size(189, 72)
        Me.radI3.TabIndex = 7
        Me.radI3.TabStop = False
        '
        'radI2
        '
        Me.radI2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radI2.Location = New System.Drawing.Point(491, 81)
        Me.radI2.Name = "radI2"
        Me.radI2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radI2.Properties.Appearance.Options.UseFont = True
        Me.radI2.Properties.AutoHeight = False
        Me.radI2.Properties.Caption = "Low"
        Me.radI2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radI2.Properties.RadioGroupIndex = 1
        Me.radI2.Size = New System.Drawing.Size(189, 72)
        Me.radI2.TabIndex = 6
        Me.radI2.TabStop = False
        '
        'radI1
        '
        Me.radI1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radI1.Location = New System.Drawing.Point(491, 3)
        Me.radI1.Name = "radI1"
        Me.radI1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radI1.Properties.Appearance.Options.UseFont = True
        Me.radI1.Properties.AutoHeight = False
        Me.radI1.Properties.Caption = "Extremely Low"
        Me.radI1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radI1.Properties.RadioGroupIndex = 1
        Me.radI1.Size = New System.Drawing.Size(189, 72)
        Me.radI1.TabIndex = 5
        Me.radI1.TabStop = False
        '
        'radWB2
        '
        Me.radWB2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radWB2.Location = New System.Drawing.Point(3, 81)
        Me.radWB2.Name = "radWB2"
        Me.radWB2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radWB2.Properties.Appearance.Options.UseFont = True
        Me.radWB2.Properties.AutoHeight = False
        Me.radWB2.Properties.Caption = "Low"
        Me.radWB2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radWB2.Properties.RadioGroupIndex = 0
        Me.radWB2.Size = New System.Drawing.Size(189, 72)
        Me.radWB2.TabIndex = 4
        Me.radWB2.TabStop = False
        '
        'radWB5
        '
        Me.radWB5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radWB5.Location = New System.Drawing.Point(3, 315)
        Me.radWB5.Name = "radWB5"
        Me.radWB5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radWB5.Properties.Appearance.Options.UseFont = True
        Me.radWB5.Properties.AutoHeight = False
        Me.radWB5.Properties.Caption = "Extremely High"
        Me.radWB5.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radWB5.Properties.RadioGroupIndex = 0
        Me.radWB5.Size = New System.Drawing.Size(189, 75)
        Me.radWB5.TabIndex = 3
        Me.radWB5.TabStop = False
        '
        'radWB4
        '
        Me.radWB4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radWB4.Location = New System.Drawing.Point(3, 237)
        Me.radWB4.Name = "radWB4"
        Me.radWB4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radWB4.Properties.Appearance.Options.UseFont = True
        Me.radWB4.Properties.AutoHeight = False
        Me.radWB4.Properties.Caption = "High"
        Me.radWB4.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radWB4.Properties.RadioGroupIndex = 0
        Me.radWB4.Size = New System.Drawing.Size(189, 72)
        Me.radWB4.TabIndex = 2
        Me.radWB4.TabStop = False
        '
        'radWB3
        '
        Me.radWB3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radWB3.Location = New System.Drawing.Point(3, 159)
        Me.radWB3.Name = "radWB3"
        Me.radWB3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radWB3.Properties.Appearance.Options.UseFont = True
        Me.radWB3.Properties.AutoHeight = False
        Me.radWB3.Properties.Caption = "Moderate"
        Me.radWB3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radWB3.Properties.RadioGroupIndex = 0
        Me.radWB3.Size = New System.Drawing.Size(189, 72)
        Me.radWB3.TabIndex = 1
        Me.radWB3.TabStop = False
        '
        'radWB1
        '
        Me.radWB1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radWB1.Location = New System.Drawing.Point(3, 3)
        Me.radWB1.Name = "radWB1"
        Me.radWB1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radWB1.Properties.Appearance.Options.UseFont = True
        Me.radWB1.Properties.AutoHeight = False
        Me.radWB1.Properties.Caption = "Extremely Low"
        Me.radWB1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radWB1.Properties.RadioGroupIndex = 0
        Me.radWB1.Size = New System.Drawing.Size(189, 72)
        Me.radWB1.TabIndex = 0
        Me.radWB1.TabStop = False
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LabelControl3.Location = New System.Drawing.Point(198, 3)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(287, 72)
        Me.LabelControl3.TabIndex = 10
        Me.LabelControl3.Text = resources.GetString("LabelControl3.Text")
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LabelControl4.Location = New System.Drawing.Point(198, 81)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(287, 72)
        Me.LabelControl4.TabIndex = 11
        Me.LabelControl4.Text = resources.GetString("LabelControl4.Text")
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LabelControl5.Location = New System.Drawing.Point(198, 159)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(287, 72)
        Me.LabelControl5.TabIndex = 12
        Me.LabelControl5.Text = "The child has a neutral posture. Facial expression and posture" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "show little or no" &
    " emotion. There are no signs indicating sadness or" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "pleasure, comfort or discomf" &
    "ort."
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LabelControl6.Location = New System.Drawing.Point(198, 237)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(287, 72)
        Me.LabelControl6.TabIndex = 13
        Me.LabelControl6.Text = "The child shows obvious signs of satisfaction (as listed under level" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "5). However" &
    ", these signals are not constantly present with the" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "same intensity."
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LabelControl7.Location = New System.Drawing.Point(198, 315)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(287, 75)
        Me.LabelControl7.TabIndex = 14
        Me.LabelControl7.Text = resources.GetString("LabelControl7.Text")
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LabelControl8.Location = New System.Drawing.Point(686, 3)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(289, 72)
        Me.LabelControl8.TabIndex = 15
        Me.LabelControl8.Text = "Activity is simple, repetitive and passive. The child seems absent" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "and displays " &
    "no energy. They may stare into space or look around" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "to see what others are doin" &
    "g."
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LabelControl9.Location = New System.Drawing.Point(686, 81)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(289, 72)
        Me.LabelControl9.TabIndex = 16
        Me.LabelControl9.Text = resources.GetString("LabelControl9.Text")
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LabelControl10.Location = New System.Drawing.Point(686, 159)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(289, 72)
        Me.LabelControl10.TabIndex = 17
        Me.LabelControl10.Text = resources.GetString("LabelControl10.Text")
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LabelControl11.Location = New System.Drawing.Point(686, 237)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(289, 72)
        Me.LabelControl11.TabIndex = 18
        Me.LabelControl11.Text = "Continuous activity with intense moments. The child’ activity has" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "intense moment" &
    "s and at all times they seem involved. They are not" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "easily distracted."
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LabelControl12.Location = New System.Drawing.Point(686, 315)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(289, 75)
        Me.LabelControl12.TabIndex = 19
        Me.LabelControl12.Text = "The child shows continuous and intense activity revealing the" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "greatest involveme" &
    "nt. They are concentrated, creative, energetic" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "and persistent throughout nearly" &
    " all the observed period."
        '
        'tabNS
        '
        Me.tabNS.Controls.Add(Me.GroupControl3)
        Me.tabNS.Name = "tabNS"
        Me.tabNS.Size = New System.Drawing.Size(994, 438)
        Me.tabNS.Text = "Next Steps"
        '
        'GroupControl3
        '
        Me.GroupControl3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl3.Controls.Add(Me.fldNextSteps)
        Me.GroupControl3.Controls.Add(Me.txtNextSteps)
        Me.GroupControl3.Location = New System.Drawing.Point(3, 3)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.ShowCaption = False
        Me.GroupControl3.Size = New System.Drawing.Size(988, 432)
        Me.GroupControl3.TabIndex = 3
        Me.GroupControl3.Text = "GroupControl3"
        '
        'fldNextSteps
        '
        Me.fldNextSteps.ButtonCustom = False
        Me.fldNextSteps.ButtonSQL = Nothing
        Me.fldNextSteps.ButtonText = ""
        Me.fldNextSteps.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Enter
        Me.fldNextSteps.ButtonWidth = 50.0!
        Me.fldNextSteps.FamilyID = Nothing
        Me.fldNextSteps.HideText = True
        Me.fldNextSteps.LabelText = "Next Steps"
        Me.fldNextSteps.LabelWidth = 100.0!
        Me.fldNextSteps.Location = New System.Drawing.Point(5, 5)
        Me.fldNextSteps.MaximumSize = New System.Drawing.Size(1000, 51)
        Me.fldNextSteps.MinimumSize = New System.Drawing.Size(200, 51)
        Me.fldNextSteps.Name = "fldNextSteps"
        Me.fldNextSteps.QuickTextList = ""
        Me.fldNextSteps.Size = New System.Drawing.Size(978, 51)
        Me.fldNextSteps.TabIndex = 95
        Me.fldNextSteps.ValueDateTime = Nothing
        Me.fldNextSteps.ValueID = Nothing
        Me.fldNextSteps.ValueMaxLength = 0
        Me.fldNextSteps.ValueText = ""
        '
        'txtNextSteps
        '
        Me.txtNextSteps.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNextSteps.Location = New System.Drawing.Point(5, 62)
        Me.txtNextSteps.Name = "txtNextSteps"
        Me.txtNextSteps.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNextSteps.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtNextSteps, True)
        Me.txtNextSteps.Size = New System.Drawing.Size(978, 365)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtNextSteps, OptionsSpelling7)
        Me.txtNextSteps.TabIndex = 94
        '
        'frmEYFSAss
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1024, 538)
        Me.Controls.Add(Me.tc)
        Me.Controls.Add(Me.panBottom)
        Me.Name = "frmEYFSAss"
        Me.Text = "EYFS Assessment"
        Me.Controls.SetChildIndex(Me.panBottom, 0)
        Me.Controls.SetChildIndex(Me.tc, 0)
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panBottom.ResumeLayout(False)
        CType(Me.txtCyan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOrange.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tc, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tc.ResumeLayout(False)
        Me.tabObs.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.txtComments.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabDM.ResumeLayout(False)
        Me.tabCOEL.ResumeLayout(False)
        Me.tabLS.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.radI5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radI4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radI3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radI2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radI1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radWB2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radWB5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radWB4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radWB3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radWB1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabNS.ResumeLayout(False)
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.txtNextSteps.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Protected Friend WithEvents panBottom As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnAccept As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tc As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents tabObs As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabDM As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabCOEL As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabLS As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabNS As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents btnDelete As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tgDM As NurseryTouchscreen.TouchGrid
    Friend WithEvents tgCOEL As NurseryTouchscreen.TouchGrid
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents radWB2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents radWB5 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents radWB4 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents radWB3 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents radWB1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents radI5 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents radI4 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents radI3 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents radI2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents radI1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents fldNextSteps As NurseryTouchscreen.Field
    Friend WithEvents txtNextSteps As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents fldComments As NurseryTouchscreen.Field
    Friend WithEvents fldChild As NurseryTouchscreen.Field
    Friend WithEvents fldStatus As NurseryTouchscreen.Field
    Friend WithEvents fldDateTime As NurseryTouchscreen.Field
    Friend WithEvents fldAssessor As NurseryTouchscreen.Field
    Friend WithEvents txtComments As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtCyan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtGreen As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtYellow As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtOrange As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRed As DevExpress.XtraEditors.TextEdit
End Class
