﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFood
    Inherits NurseryTouchscreen.frmBaseChild

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFood))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnFood8 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnFood7 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnFood6 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnFood5 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnFood4 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnFood3 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnFood2 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnFood1 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnNone8 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSome8 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMost8 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAll8 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnNone7 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSome7 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMost7 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAll7 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnNone6 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSome6 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMost6 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAll6 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnNone5 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSome5 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMost5 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAll5 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnNone4 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSome4 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMost4 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAll4 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnNone3 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSome3 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMost3 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAll3 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnNone2 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSome2 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMost2 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAll2 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnNone1 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSome1 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMost1 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAll1 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnBreakfast = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSnack = New DevExpress.XtraEditors.SimpleButton()
        Me.btnLunch = New DevExpress.XtraEditors.SimpleButton()
        Me.btnTea = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAdd = New DevExpress.XtraEditors.SimpleButton()
        Me.btnLunchDessert = New DevExpress.XtraEditors.SimpleButton()
        Me.btnTeaDessert = New DevExpress.XtraEditors.SimpleButton()
        Me.btnRemoveAll = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnFood8)
        Me.GroupBox1.Controls.Add(Me.btnFood7)
        Me.GroupBox1.Controls.Add(Me.btnFood6)
        Me.GroupBox1.Controls.Add(Me.btnFood5)
        Me.GroupBox1.Controls.Add(Me.btnFood4)
        Me.GroupBox1.Controls.Add(Me.btnFood3)
        Me.GroupBox1.Controls.Add(Me.btnFood2)
        Me.GroupBox1.Controls.Add(Me.btnFood1)
        Me.GroupBox1.Controls.Add(Me.btnNone8)
        Me.GroupBox1.Controls.Add(Me.btnSome8)
        Me.GroupBox1.Controls.Add(Me.btnMost8)
        Me.GroupBox1.Controls.Add(Me.btnAll8)
        Me.GroupBox1.Controls.Add(Me.btnNone7)
        Me.GroupBox1.Controls.Add(Me.btnSome7)
        Me.GroupBox1.Controls.Add(Me.btnMost7)
        Me.GroupBox1.Controls.Add(Me.btnAll7)
        Me.GroupBox1.Controls.Add(Me.btnNone6)
        Me.GroupBox1.Controls.Add(Me.btnSome6)
        Me.GroupBox1.Controls.Add(Me.btnMost6)
        Me.GroupBox1.Controls.Add(Me.btnAll6)
        Me.GroupBox1.Controls.Add(Me.btnNone5)
        Me.GroupBox1.Controls.Add(Me.btnSome5)
        Me.GroupBox1.Controls.Add(Me.btnMost5)
        Me.GroupBox1.Controls.Add(Me.btnAll5)
        Me.GroupBox1.Controls.Add(Me.btnNone4)
        Me.GroupBox1.Controls.Add(Me.btnSome4)
        Me.GroupBox1.Controls.Add(Me.btnMost4)
        Me.GroupBox1.Controls.Add(Me.btnAll4)
        Me.GroupBox1.Controls.Add(Me.btnNone3)
        Me.GroupBox1.Controls.Add(Me.btnSome3)
        Me.GroupBox1.Controls.Add(Me.btnMost3)
        Me.GroupBox1.Controls.Add(Me.btnAll3)
        Me.GroupBox1.Controls.Add(Me.btnNone2)
        Me.GroupBox1.Controls.Add(Me.btnSome2)
        Me.GroupBox1.Controls.Add(Me.btnMost2)
        Me.GroupBox1.Controls.Add(Me.btnAll2)
        Me.GroupBox1.Controls.Add(Me.btnNone1)
        Me.GroupBox1.Controls.Add(Me.btnSome1)
        Me.GroupBox1.Controls.Add(Me.btnMost1)
        Me.GroupBox1.Controls.Add(Me.btnAll1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 138)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1000, 337)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Meal Detail"
        '
        'btnFood8
        '
        Me.btnFood8.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnFood8.Location = New System.Drawing.Point(11, 298)
        Me.btnFood8.Name = "btnFood8"
        Me.btnFood8.Size = New System.Drawing.Size(529, 33)
        Me.btnFood8.TabIndex = 35
        '
        'btnFood7
        '
        Me.btnFood7.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnFood7.Location = New System.Drawing.Point(11, 259)
        Me.btnFood7.Name = "btnFood7"
        Me.btnFood7.Size = New System.Drawing.Size(529, 33)
        Me.btnFood7.TabIndex = 30
        '
        'btnFood6
        '
        Me.btnFood6.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnFood6.Location = New System.Drawing.Point(11, 220)
        Me.btnFood6.Name = "btnFood6"
        Me.btnFood6.Size = New System.Drawing.Size(529, 33)
        Me.btnFood6.TabIndex = 25
        '
        'btnFood5
        '
        Me.btnFood5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnFood5.Location = New System.Drawing.Point(11, 181)
        Me.btnFood5.Name = "btnFood5"
        Me.btnFood5.Size = New System.Drawing.Size(529, 33)
        Me.btnFood5.TabIndex = 20
        '
        'btnFood4
        '
        Me.btnFood4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnFood4.Location = New System.Drawing.Point(11, 142)
        Me.btnFood4.Name = "btnFood4"
        Me.btnFood4.Size = New System.Drawing.Size(529, 33)
        Me.btnFood4.TabIndex = 15
        '
        'btnFood3
        '
        Me.btnFood3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnFood3.Location = New System.Drawing.Point(11, 103)
        Me.btnFood3.Name = "btnFood3"
        Me.btnFood3.Size = New System.Drawing.Size(529, 33)
        Me.btnFood3.TabIndex = 10
        '
        'btnFood2
        '
        Me.btnFood2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnFood2.Location = New System.Drawing.Point(11, 64)
        Me.btnFood2.Name = "btnFood2"
        Me.btnFood2.Size = New System.Drawing.Size(529, 33)
        Me.btnFood2.TabIndex = 5
        '
        'btnFood1
        '
        Me.btnFood1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnFood1.Location = New System.Drawing.Point(11, 25)
        Me.btnFood1.Name = "btnFood1"
        Me.btnFood1.Size = New System.Drawing.Size(529, 33)
        Me.btnFood1.TabIndex = 0
        '
        'btnNone8
        '
        Me.btnNone8.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnNone8.Location = New System.Drawing.Point(548, 298)
        Me.btnNone8.Name = "btnNone8"
        Me.btnNone8.Size = New System.Drawing.Size(107, 33)
        Me.btnNone8.TabIndex = 36
        Me.btnNone8.Text = "None"
        '
        'btnSome8
        '
        Me.btnSome8.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnSome8.Location = New System.Drawing.Point(661, 298)
        Me.btnSome8.Name = "btnSome8"
        Me.btnSome8.Size = New System.Drawing.Size(107, 33)
        Me.btnSome8.TabIndex = 37
        Me.btnSome8.Text = "Some"
        '
        'btnMost8
        '
        Me.btnMost8.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnMost8.Location = New System.Drawing.Point(774, 298)
        Me.btnMost8.Name = "btnMost8"
        Me.btnMost8.Size = New System.Drawing.Size(107, 33)
        Me.btnMost8.TabIndex = 38
        Me.btnMost8.Text = "Most"
        '
        'btnAll8
        '
        Me.btnAll8.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnAll8.Location = New System.Drawing.Point(887, 298)
        Me.btnAll8.Name = "btnAll8"
        Me.btnAll8.Size = New System.Drawing.Size(107, 33)
        Me.btnAll8.TabIndex = 39
        Me.btnAll8.Text = "All"
        '
        'btnNone7
        '
        Me.btnNone7.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnNone7.Location = New System.Drawing.Point(548, 259)
        Me.btnNone7.Name = "btnNone7"
        Me.btnNone7.Size = New System.Drawing.Size(107, 33)
        Me.btnNone7.TabIndex = 31
        Me.btnNone7.Text = "None"
        '
        'btnSome7
        '
        Me.btnSome7.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnSome7.Location = New System.Drawing.Point(661, 259)
        Me.btnSome7.Name = "btnSome7"
        Me.btnSome7.Size = New System.Drawing.Size(107, 33)
        Me.btnSome7.TabIndex = 32
        Me.btnSome7.Text = "Some"
        '
        'btnMost7
        '
        Me.btnMost7.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnMost7.Location = New System.Drawing.Point(774, 259)
        Me.btnMost7.Name = "btnMost7"
        Me.btnMost7.Size = New System.Drawing.Size(107, 33)
        Me.btnMost7.TabIndex = 33
        Me.btnMost7.Text = "Most"
        '
        'btnAll7
        '
        Me.btnAll7.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnAll7.Location = New System.Drawing.Point(887, 259)
        Me.btnAll7.Name = "btnAll7"
        Me.btnAll7.Size = New System.Drawing.Size(107, 33)
        Me.btnAll7.TabIndex = 34
        Me.btnAll7.Text = "All"
        '
        'btnNone6
        '
        Me.btnNone6.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnNone6.Location = New System.Drawing.Point(548, 220)
        Me.btnNone6.Name = "btnNone6"
        Me.btnNone6.Size = New System.Drawing.Size(107, 33)
        Me.btnNone6.TabIndex = 26
        Me.btnNone6.Text = "None"
        '
        'btnSome6
        '
        Me.btnSome6.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnSome6.Location = New System.Drawing.Point(661, 220)
        Me.btnSome6.Name = "btnSome6"
        Me.btnSome6.Size = New System.Drawing.Size(107, 33)
        Me.btnSome6.TabIndex = 27
        Me.btnSome6.Text = "Some"
        '
        'btnMost6
        '
        Me.btnMost6.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnMost6.Location = New System.Drawing.Point(774, 220)
        Me.btnMost6.Name = "btnMost6"
        Me.btnMost6.Size = New System.Drawing.Size(107, 33)
        Me.btnMost6.TabIndex = 28
        Me.btnMost6.Text = "Most"
        '
        'btnAll6
        '
        Me.btnAll6.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnAll6.Location = New System.Drawing.Point(887, 220)
        Me.btnAll6.Name = "btnAll6"
        Me.btnAll6.Size = New System.Drawing.Size(107, 33)
        Me.btnAll6.TabIndex = 29
        Me.btnAll6.Text = "All"
        '
        'btnNone5
        '
        Me.btnNone5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnNone5.Location = New System.Drawing.Point(548, 181)
        Me.btnNone5.Name = "btnNone5"
        Me.btnNone5.Size = New System.Drawing.Size(107, 33)
        Me.btnNone5.TabIndex = 21
        Me.btnNone5.Text = "None"
        '
        'btnSome5
        '
        Me.btnSome5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnSome5.Location = New System.Drawing.Point(661, 181)
        Me.btnSome5.Name = "btnSome5"
        Me.btnSome5.Size = New System.Drawing.Size(107, 33)
        Me.btnSome5.TabIndex = 22
        Me.btnSome5.Text = "Some"
        '
        'btnMost5
        '
        Me.btnMost5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnMost5.Location = New System.Drawing.Point(774, 181)
        Me.btnMost5.Name = "btnMost5"
        Me.btnMost5.Size = New System.Drawing.Size(107, 33)
        Me.btnMost5.TabIndex = 23
        Me.btnMost5.Text = "Most"
        '
        'btnAll5
        '
        Me.btnAll5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnAll5.Location = New System.Drawing.Point(887, 181)
        Me.btnAll5.Name = "btnAll5"
        Me.btnAll5.Size = New System.Drawing.Size(107, 33)
        Me.btnAll5.TabIndex = 24
        Me.btnAll5.Text = "All"
        '
        'btnNone4
        '
        Me.btnNone4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnNone4.Location = New System.Drawing.Point(548, 142)
        Me.btnNone4.Name = "btnNone4"
        Me.btnNone4.Size = New System.Drawing.Size(107, 33)
        Me.btnNone4.TabIndex = 16
        Me.btnNone4.Text = "None"
        '
        'btnSome4
        '
        Me.btnSome4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnSome4.Location = New System.Drawing.Point(661, 142)
        Me.btnSome4.Name = "btnSome4"
        Me.btnSome4.Size = New System.Drawing.Size(107, 33)
        Me.btnSome4.TabIndex = 17
        Me.btnSome4.Text = "Some"
        '
        'btnMost4
        '
        Me.btnMost4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnMost4.Location = New System.Drawing.Point(774, 142)
        Me.btnMost4.Name = "btnMost4"
        Me.btnMost4.Size = New System.Drawing.Size(107, 33)
        Me.btnMost4.TabIndex = 18
        Me.btnMost4.Text = "Most"
        '
        'btnAll4
        '
        Me.btnAll4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnAll4.Location = New System.Drawing.Point(887, 142)
        Me.btnAll4.Name = "btnAll4"
        Me.btnAll4.Size = New System.Drawing.Size(107, 33)
        Me.btnAll4.TabIndex = 19
        Me.btnAll4.Text = "All"
        '
        'btnNone3
        '
        Me.btnNone3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnNone3.Location = New System.Drawing.Point(548, 103)
        Me.btnNone3.Name = "btnNone3"
        Me.btnNone3.Size = New System.Drawing.Size(107, 33)
        Me.btnNone3.TabIndex = 11
        Me.btnNone3.Text = "None"
        '
        'btnSome3
        '
        Me.btnSome3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnSome3.Location = New System.Drawing.Point(661, 103)
        Me.btnSome3.Name = "btnSome3"
        Me.btnSome3.Size = New System.Drawing.Size(107, 33)
        Me.btnSome3.TabIndex = 12
        Me.btnSome3.Text = "Some"
        '
        'btnMost3
        '
        Me.btnMost3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnMost3.Location = New System.Drawing.Point(774, 103)
        Me.btnMost3.Name = "btnMost3"
        Me.btnMost3.Size = New System.Drawing.Size(107, 33)
        Me.btnMost3.TabIndex = 13
        Me.btnMost3.Text = "Most"
        '
        'btnAll3
        '
        Me.btnAll3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnAll3.Location = New System.Drawing.Point(887, 103)
        Me.btnAll3.Name = "btnAll3"
        Me.btnAll3.Size = New System.Drawing.Size(107, 33)
        Me.btnAll3.TabIndex = 14
        Me.btnAll3.Text = "All"
        '
        'btnNone2
        '
        Me.btnNone2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnNone2.Location = New System.Drawing.Point(548, 64)
        Me.btnNone2.Name = "btnNone2"
        Me.btnNone2.Size = New System.Drawing.Size(107, 33)
        Me.btnNone2.TabIndex = 6
        Me.btnNone2.Text = "None"
        '
        'btnSome2
        '
        Me.btnSome2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnSome2.Location = New System.Drawing.Point(661, 64)
        Me.btnSome2.Name = "btnSome2"
        Me.btnSome2.Size = New System.Drawing.Size(107, 33)
        Me.btnSome2.TabIndex = 7
        Me.btnSome2.Text = "Some"
        '
        'btnMost2
        '
        Me.btnMost2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnMost2.Location = New System.Drawing.Point(774, 64)
        Me.btnMost2.Name = "btnMost2"
        Me.btnMost2.Size = New System.Drawing.Size(107, 33)
        Me.btnMost2.TabIndex = 8
        Me.btnMost2.Text = "Most"
        '
        'btnAll2
        '
        Me.btnAll2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnAll2.Location = New System.Drawing.Point(887, 64)
        Me.btnAll2.Name = "btnAll2"
        Me.btnAll2.Size = New System.Drawing.Size(107, 33)
        Me.btnAll2.TabIndex = 9
        Me.btnAll2.Text = "All"
        '
        'btnNone1
        '
        Me.btnNone1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnNone1.Location = New System.Drawing.Point(548, 25)
        Me.btnNone1.Name = "btnNone1"
        Me.btnNone1.Size = New System.Drawing.Size(107, 33)
        Me.btnNone1.TabIndex = 1
        Me.btnNone1.Text = "None"
        '
        'btnSome1
        '
        Me.btnSome1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnSome1.Location = New System.Drawing.Point(661, 25)
        Me.btnSome1.Name = "btnSome1"
        Me.btnSome1.Size = New System.Drawing.Size(107, 33)
        Me.btnSome1.TabIndex = 2
        Me.btnSome1.Text = "Some"
        '
        'btnMost1
        '
        Me.btnMost1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnMost1.Location = New System.Drawing.Point(774, 25)
        Me.btnMost1.Name = "btnMost1"
        Me.btnMost1.Size = New System.Drawing.Size(107, 33)
        Me.btnMost1.TabIndex = 3
        Me.btnMost1.Text = "Most"
        '
        'btnAll1
        '
        Me.btnAll1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnAll1.Location = New System.Drawing.Point(887, 25)
        Me.btnAll1.Name = "btnAll1"
        Me.btnAll1.Size = New System.Drawing.Size(107, 33)
        Me.btnAll1.TabIndex = 4
        Me.btnAll1.Text = "All"
        '
        'btnBreakfast
        '
        Me.btnBreakfast.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnBreakfast.Location = New System.Drawing.Point(12, 99)
        Me.btnBreakfast.Name = "btnBreakfast"
        Me.btnBreakfast.Size = New System.Drawing.Size(160, 33)
        Me.btnBreakfast.TabIndex = 8
        Me.btnBreakfast.Text = "Breakfast"
        '
        'btnSnack
        '
        Me.btnSnack.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnSnack.Location = New System.Drawing.Point(178, 99)
        Me.btnSnack.Name = "btnSnack"
        Me.btnSnack.Size = New System.Drawing.Size(160, 33)
        Me.btnSnack.TabIndex = 9
        Me.btnSnack.Text = "Snack"
        '
        'btnLunch
        '
        Me.btnLunch.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnLunch.Location = New System.Drawing.Point(344, 99)
        Me.btnLunch.Name = "btnLunch"
        Me.btnLunch.Size = New System.Drawing.Size(160, 33)
        Me.btnLunch.TabIndex = 10
        Me.btnLunch.Text = "Lunch"
        '
        'btnTea
        '
        Me.btnTea.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnTea.Location = New System.Drawing.Point(676, 99)
        Me.btnTea.Name = "btnTea"
        Me.btnTea.Size = New System.Drawing.Size(160, 33)
        Me.btnTea.TabIndex = 12
        Me.btnTea.Text = "Tea"
        '
        'btnAdd
        '
        Me.btnAdd.Image = CType(resources.GetObject("btnAdd.Image"), System.Drawing.Image)
        Me.btnAdd.Location = New System.Drawing.Point(12, 481)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(266, 45)
        Me.btnAdd.TabIndex = 15
        Me.btnAdd.Text = "Add Item"
        '
        'btnLunchDessert
        '
        Me.btnLunchDessert.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnLunchDessert.Location = New System.Drawing.Point(510, 99)
        Me.btnLunchDessert.Name = "btnLunchDessert"
        Me.btnLunchDessert.Size = New System.Drawing.Size(160, 33)
        Me.btnLunchDessert.TabIndex = 11
        Me.btnLunchDessert.Text = "Lunch Dessert"
        '
        'btnTeaDessert
        '
        Me.btnTeaDessert.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnTeaDessert.Location = New System.Drawing.Point(842, 99)
        Me.btnTeaDessert.Name = "btnTeaDessert"
        Me.btnTeaDessert.Size = New System.Drawing.Size(160, 33)
        Me.btnTeaDessert.TabIndex = 13
        Me.btnTeaDessert.Text = "Tea Dessert"
        '
        'btnRemoveAll
        '
        Me.btnRemoveAll.Image = CType(resources.GetObject("btnRemoveAll.Image"), System.Drawing.Image)
        Me.btnRemoveAll.Location = New System.Drawing.Point(284, 481)
        Me.btnRemoveAll.Name = "btnRemoveAll"
        Me.btnRemoveAll.Size = New System.Drawing.Size(266, 45)
        Me.btnRemoveAll.TabIndex = 17
        Me.btnRemoveAll.Text = "Remove All"
        '
        'frmFood
        '
        Me.ClientSize = New System.Drawing.Size(1024, 538)
        Me.Controls.Add(Me.btnRemoveAll)
        Me.Controls.Add(Me.btnTeaDessert)
        Me.Controls.Add(Me.btnLunchDessert)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.btnTea)
        Me.Controls.Add(Me.btnLunch)
        Me.Controls.Add(Me.btnSnack)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnBreakfast)
        Me.Name = "frmFood"
        Me.Text = "Food Register"
        Me.Controls.SetChildIndex(Me.btnBreakfast, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.btnSnack, 0)
        Me.Controls.SetChildIndex(Me.btnLunch, 0)
        Me.Controls.SetChildIndex(Me.btnTea, 0)
        Me.Controls.SetChildIndex(Me.btnAdd, 0)
        Me.Controls.SetChildIndex(Me.btnLunchDessert, 0)
        Me.Controls.SetChildIndex(Me.btnTeaDessert, 0)
        Me.Controls.SetChildIndex(Me.btnRemoveAll, 0)
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnBreakfast As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSnack As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnLunch As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnTea As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnNone2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSome2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMost2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAll2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnNone1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSome1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMost1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAll1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnNone8 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSome8 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMost8 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAll8 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnNone7 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSome7 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMost7 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAll7 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnNone6 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSome6 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMost6 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAll6 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnNone5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSome5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMost5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAll5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnNone4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSome4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMost4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAll4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnNone3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSome3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMost3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAll3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnFood8 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnFood7 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnFood6 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnFood5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnFood4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnFood3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnFood2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnFood1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnLunchDessert As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnTeaDessert As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRemoveAll As DevExpress.XtraEditors.SimpleButton

End Class
