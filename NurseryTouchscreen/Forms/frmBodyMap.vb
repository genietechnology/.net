﻿Public Class frmBodyMap

    Private m_Circle As New Rectangle

    Private m_Regular As New Font("Segoe UI", 14.25, FontStyle.Regular)
    Private m_Bold As New Font("Segoe UI", 14.25, FontStyle.Bold)

    Private m_Map As String = "Body"
    Private m_Type As String = "Bump"
    Private m_Colour As Drawing.Color = Color.Black

    Public Property BodyImage As Drawing.Image
        Get
            Return picMap.Image
        End Get
        Set(value As Drawing.Image)
            picMap.Image = value
        End Set
    End Property

    Private Sub frmBodyMap_Load(sender As Object, e As EventArgs) Handles Me.Load
        If picMap.Image Is Nothing Then
            LoadBodyMap()
        End If
    End Sub

    Private Sub SetColour(ByRef ControlIn As Object)

        Dim _Button As DevExpress.XtraEditors.SimpleButton = CType(ControlIn, DevExpress.XtraEditors.SimpleButton)
        _Button.Font = m_Bold

        m_Type = _Button.Text
        m_Colour = _Button.Appearance.BackColor

    End Sub

    Private Sub LoadBodyMap()
        picMap.Image = My.Resources.ResourceManager.GetObject(m_Map)
    End Sub

    Private Sub ClearButtons()
        btnBump.Font = m_Regular
        btnGraze.Font = m_Regular
        btnScratch.Font = m_Regular
        btnBruise.Font = m_Regular
        btnRedMark.Font = m_Regular
        btnCut.Font = m_Regular
        btnBite.Font = m_Regular
        btnSwelling.Font = m_Regular
        btnBurn.Font = m_Regular
    End Sub

    Private Sub btnSelect_Click(sender As Object, e As EventArgs) Handles btnSelect.Click

        Dim _SQL As String = "Body|Head|Head Side|Hands|Palms|Feet Inner|Feet Outer|Soles"

        Dim _Map As SharedModule.Pair = SharedModule.ReturnButtonSQL(_SQL, "Select Body Map")
        If _Map IsNot Nothing Then
            m_Map = Replace(_Map.Code, " ", "")
            LoadBodyMap()
        End If

    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click
        Me.DialogResult = DialogResult.OK
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        picMap.Image = My.Resources.ResourceManager.GetObject(m_Map)
    End Sub

#Region "Drawing"

    Private Sub picMap_MouseDown(sender As Object, e As MouseEventArgs) Handles picMap.MouseDown
        m_Circle.Location = e.Location
    End Sub

    Private Sub picMap_MouseUp(sender As Object, e As MouseEventArgs) Handles picMap.MouseUp

        Dim _Pen As New Pen(m_Colour, 4)

        'the next line will set the size of the circle/eliipse based on
        'where the mouse was when the button was released
        m_Circle.Size = New Size(e.X - m_Circle.X, e.Y - m_Circle.Y)

        'next is where we are actually drawing onto the image:
        '1. Get a Graphics representation of the image
        Using gfx As Graphics = Graphics.FromImage(picMap.Image)

            '2. Draw an ellipse onto the image using the rectangle we created as the bounds
            gfx.DrawEllipse(_Pen, m_Circle)

            '3. Refresh the PictureBox so that it shows the circle/ellipse we just drew
            picMap.Refresh()

        End Using

    End Sub

#End Region

#Region "Types"

    Private Sub Incidents_Click(sender As Object, e As EventArgs) Handles btnBump.Click, btnGraze.Click, btnBruise.Click,
                                                                        btnRedMark.Click, btnCut.Click, btnSwelling.Click,
                                                                        btnScratch.Click, btnBite.Click, btnBurn.Click
        ClearButtons()
        SetColour(sender)
    End Sub

#End Region

End Class
