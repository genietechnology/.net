﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSleep
    Inherits NurseryTouchscreen.frmBaseChild

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSleep))
        Me.btnSleep4 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSleep3 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSleep2 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSleep1 = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnStartH19 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStartH18 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStartH17 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStartH8 = New DevExpress.XtraEditors.SimpleButton()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnStartM55 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStartM50 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStartM45 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStartM40 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStartM35 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStartM30 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStartM25 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStartM20 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStartM15 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStartH9 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStartH16 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStartH15 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStartH14 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStartH13 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStartH12 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStartM10 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStartM5 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStartM0 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStartH11 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStartH10 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnRemoveSleep = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnEndH19 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEndH8 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEndH18 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEndH17 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEndH9 = New DevExpress.XtraEditors.SimpleButton()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnEndH16 = New DevExpress.XtraEditors.SimpleButton()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnEndH15 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEndM55 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEndH14 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEndM50 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEndH13 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEndM45 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEndH12 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEndM40 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEndH11 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEndM35 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEndH10 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEndM30 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEndM25 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEndM20 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEndM15 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEndM10 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEndM5 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEndM0 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAdd = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSleep5 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSleep6 = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        '
        'btnSleep4
        '
        Me.btnSleep4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnSleep4.Location = New System.Drawing.Point(515, 102)
        Me.btnSleep4.Name = "btnSleep4"
        Me.btnSleep4.Size = New System.Drawing.Size(160, 33)
        Me.btnSleep4.TabIndex = 3
        Me.btnSleep4.Text = "Sleep 4"
        '
        'btnSleep3
        '
        Me.btnSleep3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnSleep3.Location = New System.Drawing.Point(349, 102)
        Me.btnSleep3.Name = "btnSleep3"
        Me.btnSleep3.Size = New System.Drawing.Size(160, 33)
        Me.btnSleep3.TabIndex = 2
        Me.btnSleep3.Text = "Sleep 3"
        '
        'btnSleep2
        '
        Me.btnSleep2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnSleep2.Location = New System.Drawing.Point(183, 102)
        Me.btnSleep2.Name = "btnSleep2"
        Me.btnSleep2.Size = New System.Drawing.Size(160, 33)
        Me.btnSleep2.TabIndex = 1
        Me.btnSleep2.Text = "Sleep 2"
        '
        'btnSleep1
        '
        Me.btnSleep1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnSleep1.Location = New System.Drawing.Point(17, 102)
        Me.btnSleep1.Name = "btnSleep1"
        Me.btnSleep1.Size = New System.Drawing.Size(160, 33)
        Me.btnSleep1.TabIndex = 0
        Me.btnSleep1.Text = "Sleep 1"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnStartH19)
        Me.GroupBox1.Controls.Add(Me.btnStartH18)
        Me.GroupBox1.Controls.Add(Me.btnStartH17)
        Me.GroupBox1.Controls.Add(Me.btnStartH8)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.btnStartM55)
        Me.GroupBox1.Controls.Add(Me.btnStartM50)
        Me.GroupBox1.Controls.Add(Me.btnStartM45)
        Me.GroupBox1.Controls.Add(Me.btnStartM40)
        Me.GroupBox1.Controls.Add(Me.btnStartM35)
        Me.GroupBox1.Controls.Add(Me.btnStartM30)
        Me.GroupBox1.Controls.Add(Me.btnStartM25)
        Me.GroupBox1.Controls.Add(Me.btnStartM20)
        Me.GroupBox1.Controls.Add(Me.btnStartM15)
        Me.GroupBox1.Controls.Add(Me.btnStartH9)
        Me.GroupBox1.Controls.Add(Me.btnStartH16)
        Me.GroupBox1.Controls.Add(Me.btnStartH15)
        Me.GroupBox1.Controls.Add(Me.btnStartH14)
        Me.GroupBox1.Controls.Add(Me.btnStartH13)
        Me.GroupBox1.Controls.Add(Me.btnStartH12)
        Me.GroupBox1.Controls.Add(Me.btnStartM10)
        Me.GroupBox1.Controls.Add(Me.btnStartM5)
        Me.GroupBox1.Controls.Add(Me.btnStartM0)
        Me.GroupBox1.Controls.Add(Me.btnStartH11)
        Me.GroupBox1.Controls.Add(Me.btnStartH10)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 141)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(497, 334)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Start Time"
        '
        'btnStartH19
        '
        Me.btnStartH19.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStartH19.Location = New System.Drawing.Point(167, 257)
        Me.btnStartH19.Name = "btnStartH19"
        Me.btnStartH19.Size = New System.Drawing.Size(72, 60)
        Me.btnStartH19.TabIndex = 12
        Me.btnStartH19.Text = "7 pm"
        '
        'btnStartH18
        '
        Me.btnStartH18.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStartH18.Location = New System.Drawing.Point(89, 257)
        Me.btnStartH18.Name = "btnStartH18"
        Me.btnStartH18.Size = New System.Drawing.Size(72, 60)
        Me.btnStartH18.TabIndex = 11
        Me.btnStartH18.Text = "6 pm"
        '
        'btnStartH17
        '
        Me.btnStartH17.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStartH17.Location = New System.Drawing.Point(11, 257)
        Me.btnStartH17.Name = "btnStartH17"
        Me.btnStartH17.Size = New System.Drawing.Size(72, 60)
        Me.btnStartH17.TabIndex = 10
        Me.btnStartH17.Text = "5 pm"
        '
        'btnStartH8
        '
        Me.btnStartH8.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStartH8.Location = New System.Drawing.Point(11, 59)
        Me.btnStartH8.Name = "btnStartH8"
        Me.btnStartH8.Size = New System.Drawing.Size(72, 60)
        Me.btnStartH8.TabIndex = 1
        Me.btnStartH8.Text = "8 am"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(255, 27)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(233, 24)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Minutes"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(6, 27)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(233, 24)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Hours"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnStartM55
        '
        Me.btnStartM55.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStartM55.Location = New System.Drawing.Point(416, 257)
        Me.btnStartM55.Name = "btnStartM55"
        Me.btnStartM55.Size = New System.Drawing.Size(72, 60)
        Me.btnStartM55.TabIndex = 25
        Me.btnStartM55.Text = ":55"
        '
        'btnStartM50
        '
        Me.btnStartM50.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStartM50.Location = New System.Drawing.Point(338, 257)
        Me.btnStartM50.Name = "btnStartM50"
        Me.btnStartM50.Size = New System.Drawing.Size(72, 60)
        Me.btnStartM50.TabIndex = 24
        Me.btnStartM50.Text = ":50"
        '
        'btnStartM45
        '
        Me.btnStartM45.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStartM45.Location = New System.Drawing.Point(260, 257)
        Me.btnStartM45.Name = "btnStartM45"
        Me.btnStartM45.Size = New System.Drawing.Size(72, 60)
        Me.btnStartM45.TabIndex = 23
        Me.btnStartM45.Text = ":45"
        '
        'btnStartM40
        '
        Me.btnStartM40.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStartM40.Location = New System.Drawing.Point(416, 191)
        Me.btnStartM40.Name = "btnStartM40"
        Me.btnStartM40.Size = New System.Drawing.Size(72, 60)
        Me.btnStartM40.TabIndex = 22
        Me.btnStartM40.Text = ":40"
        '
        'btnStartM35
        '
        Me.btnStartM35.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStartM35.Location = New System.Drawing.Point(338, 191)
        Me.btnStartM35.Name = "btnStartM35"
        Me.btnStartM35.Size = New System.Drawing.Size(72, 60)
        Me.btnStartM35.TabIndex = 21
        Me.btnStartM35.Text = ":35"
        '
        'btnStartM30
        '
        Me.btnStartM30.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStartM30.Location = New System.Drawing.Point(260, 191)
        Me.btnStartM30.Name = "btnStartM30"
        Me.btnStartM30.Size = New System.Drawing.Size(72, 60)
        Me.btnStartM30.TabIndex = 20
        Me.btnStartM30.Text = ":30"
        '
        'btnStartM25
        '
        Me.btnStartM25.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStartM25.Location = New System.Drawing.Point(416, 125)
        Me.btnStartM25.Name = "btnStartM25"
        Me.btnStartM25.Size = New System.Drawing.Size(72, 60)
        Me.btnStartM25.TabIndex = 19
        Me.btnStartM25.Text = ":25"
        '
        'btnStartM20
        '
        Me.btnStartM20.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStartM20.Location = New System.Drawing.Point(338, 125)
        Me.btnStartM20.Name = "btnStartM20"
        Me.btnStartM20.Size = New System.Drawing.Size(72, 60)
        Me.btnStartM20.TabIndex = 18
        Me.btnStartM20.Text = ":20"
        '
        'btnStartM15
        '
        Me.btnStartM15.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStartM15.Location = New System.Drawing.Point(260, 125)
        Me.btnStartM15.Name = "btnStartM15"
        Me.btnStartM15.Size = New System.Drawing.Size(72, 60)
        Me.btnStartM15.TabIndex = 17
        Me.btnStartM15.Text = ":15"
        '
        'btnStartH9
        '
        Me.btnStartH9.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStartH9.Location = New System.Drawing.Point(89, 59)
        Me.btnStartH9.Name = "btnStartH9"
        Me.btnStartH9.Size = New System.Drawing.Size(72, 60)
        Me.btnStartH9.TabIndex = 2
        Me.btnStartH9.Text = "9 am"
        '
        'btnStartH16
        '
        Me.btnStartH16.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStartH16.Location = New System.Drawing.Point(167, 191)
        Me.btnStartH16.Name = "btnStartH16"
        Me.btnStartH16.Size = New System.Drawing.Size(72, 60)
        Me.btnStartH16.TabIndex = 9
        Me.btnStartH16.Text = "4 pm"
        '
        'btnStartH15
        '
        Me.btnStartH15.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStartH15.Location = New System.Drawing.Point(89, 191)
        Me.btnStartH15.Name = "btnStartH15"
        Me.btnStartH15.Size = New System.Drawing.Size(72, 60)
        Me.btnStartH15.TabIndex = 8
        Me.btnStartH15.Text = "3 pm"
        '
        'btnStartH14
        '
        Me.btnStartH14.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStartH14.Location = New System.Drawing.Point(11, 191)
        Me.btnStartH14.Name = "btnStartH14"
        Me.btnStartH14.Size = New System.Drawing.Size(72, 60)
        Me.btnStartH14.TabIndex = 7
        Me.btnStartH14.Text = "2 pm"
        '
        'btnStartH13
        '
        Me.btnStartH13.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStartH13.Location = New System.Drawing.Point(167, 125)
        Me.btnStartH13.Name = "btnStartH13"
        Me.btnStartH13.Size = New System.Drawing.Size(72, 60)
        Me.btnStartH13.TabIndex = 6
        Me.btnStartH13.Text = "1 pm"
        '
        'btnStartH12
        '
        Me.btnStartH12.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStartH12.Location = New System.Drawing.Point(89, 125)
        Me.btnStartH12.Name = "btnStartH12"
        Me.btnStartH12.Size = New System.Drawing.Size(72, 60)
        Me.btnStartH12.TabIndex = 5
        Me.btnStartH12.Text = "12"
        '
        'btnStartM10
        '
        Me.btnStartM10.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStartM10.Location = New System.Drawing.Point(416, 59)
        Me.btnStartM10.Name = "btnStartM10"
        Me.btnStartM10.Size = New System.Drawing.Size(72, 60)
        Me.btnStartM10.TabIndex = 16
        Me.btnStartM10.Text = ":10"
        '
        'btnStartM5
        '
        Me.btnStartM5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStartM5.Location = New System.Drawing.Point(338, 59)
        Me.btnStartM5.Name = "btnStartM5"
        Me.btnStartM5.Size = New System.Drawing.Size(72, 60)
        Me.btnStartM5.TabIndex = 15
        Me.btnStartM5.Text = ":05"
        '
        'btnStartM0
        '
        Me.btnStartM0.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStartM0.Location = New System.Drawing.Point(260, 59)
        Me.btnStartM0.Name = "btnStartM0"
        Me.btnStartM0.Size = New System.Drawing.Size(72, 60)
        Me.btnStartM0.TabIndex = 14
        Me.btnStartM0.Text = ":00"
        '
        'btnStartH11
        '
        Me.btnStartH11.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStartH11.Location = New System.Drawing.Point(11, 125)
        Me.btnStartH11.Name = "btnStartH11"
        Me.btnStartH11.Size = New System.Drawing.Size(72, 60)
        Me.btnStartH11.TabIndex = 4
        Me.btnStartH11.Text = "11 am"
        '
        'btnStartH10
        '
        Me.btnStartH10.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStartH10.Location = New System.Drawing.Point(167, 59)
        Me.btnStartH10.Name = "btnStartH10"
        Me.btnStartH10.Size = New System.Drawing.Size(72, 60)
        Me.btnStartH10.TabIndex = 3
        Me.btnStartH10.Text = "10 am"
        '
        'btnRemoveSleep
        '
        Me.btnRemoveSleep.Image = CType(resources.GetObject("btnRemoveSleep.Image"), System.Drawing.Image)
        Me.btnRemoveSleep.Location = New System.Drawing.Point(263, 481)
        Me.btnRemoveSleep.Name = "btnRemoveSleep"
        Me.btnRemoveSleep.Size = New System.Drawing.Size(245, 45)
        Me.btnRemoveSleep.TabIndex = 4
        Me.btnRemoveSleep.Text = "Remove Sleep Record"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnEndH19)
        Me.GroupBox2.Controls.Add(Me.btnEndH8)
        Me.GroupBox2.Controls.Add(Me.btnEndH18)
        Me.GroupBox2.Controls.Add(Me.btnEndH17)
        Me.GroupBox2.Controls.Add(Me.btnEndH9)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.btnEndH16)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.btnEndH15)
        Me.GroupBox2.Controls.Add(Me.btnEndM55)
        Me.GroupBox2.Controls.Add(Me.btnEndH14)
        Me.GroupBox2.Controls.Add(Me.btnEndM50)
        Me.GroupBox2.Controls.Add(Me.btnEndH13)
        Me.GroupBox2.Controls.Add(Me.btnEndM45)
        Me.GroupBox2.Controls.Add(Me.btnEndH12)
        Me.GroupBox2.Controls.Add(Me.btnEndM40)
        Me.GroupBox2.Controls.Add(Me.btnEndH11)
        Me.GroupBox2.Controls.Add(Me.btnEndM35)
        Me.GroupBox2.Controls.Add(Me.btnEndH10)
        Me.GroupBox2.Controls.Add(Me.btnEndM30)
        Me.GroupBox2.Controls.Add(Me.btnEndM25)
        Me.GroupBox2.Controls.Add(Me.btnEndM20)
        Me.GroupBox2.Controls.Add(Me.btnEndM15)
        Me.GroupBox2.Controls.Add(Me.btnEndM10)
        Me.GroupBox2.Controls.Add(Me.btnEndM5)
        Me.GroupBox2.Controls.Add(Me.btnEndM0)
        Me.GroupBox2.Location = New System.Drawing.Point(516, 141)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(497, 334)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "End Time"
        '
        'btnEndH19
        '
        Me.btnEndH19.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnEndH19.Location = New System.Drawing.Point(165, 257)
        Me.btnEndH19.Name = "btnEndH19"
        Me.btnEndH19.Size = New System.Drawing.Size(72, 60)
        Me.btnEndH19.TabIndex = 34
        Me.btnEndH19.Text = "7 pm"
        '
        'btnEndH8
        '
        Me.btnEndH8.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnEndH8.Location = New System.Drawing.Point(9, 59)
        Me.btnEndH8.Name = "btnEndH8"
        Me.btnEndH8.Size = New System.Drawing.Size(72, 60)
        Me.btnEndH8.TabIndex = 1
        Me.btnEndH8.Text = "8 am"
        '
        'btnEndH18
        '
        Me.btnEndH18.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnEndH18.Location = New System.Drawing.Point(87, 257)
        Me.btnEndH18.Name = "btnEndH18"
        Me.btnEndH18.Size = New System.Drawing.Size(72, 60)
        Me.btnEndH18.TabIndex = 33
        Me.btnEndH18.Text = "6 pm"
        '
        'btnEndH17
        '
        Me.btnEndH17.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnEndH17.Location = New System.Drawing.Point(9, 257)
        Me.btnEndH17.Name = "btnEndH17"
        Me.btnEndH17.Size = New System.Drawing.Size(72, 60)
        Me.btnEndH17.TabIndex = 32
        Me.btnEndH17.Text = "5 pm"
        '
        'btnEndH9
        '
        Me.btnEndH9.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnEndH9.Location = New System.Drawing.Point(87, 59)
        Me.btnEndH9.Name = "btnEndH9"
        Me.btnEndH9.Size = New System.Drawing.Size(72, 60)
        Me.btnEndH9.TabIndex = 2
        Me.btnEndH9.Text = "9 am"
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(258, 27)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(233, 24)
        Me.Label5.TabIndex = 27
        Me.Label5.Text = "Minutes"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnEndH16
        '
        Me.btnEndH16.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnEndH16.Location = New System.Drawing.Point(165, 191)
        Me.btnEndH16.Name = "btnEndH16"
        Me.btnEndH16.Size = New System.Drawing.Size(72, 60)
        Me.btnEndH16.TabIndex = 9
        Me.btnEndH16.Text = "4 pm"
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(4, 27)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(233, 24)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Hours"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnEndH15
        '
        Me.btnEndH15.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnEndH15.Location = New System.Drawing.Point(87, 191)
        Me.btnEndH15.Name = "btnEndH15"
        Me.btnEndH15.Size = New System.Drawing.Size(72, 60)
        Me.btnEndH15.TabIndex = 8
        Me.btnEndH15.Text = "3 pm"
        '
        'btnEndM55
        '
        Me.btnEndM55.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnEndM55.Location = New System.Drawing.Point(419, 257)
        Me.btnEndM55.Name = "btnEndM55"
        Me.btnEndM55.Size = New System.Drawing.Size(72, 60)
        Me.btnEndM55.TabIndex = 25
        Me.btnEndM55.Text = ":55"
        '
        'btnEndH14
        '
        Me.btnEndH14.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnEndH14.Location = New System.Drawing.Point(9, 191)
        Me.btnEndH14.Name = "btnEndH14"
        Me.btnEndH14.Size = New System.Drawing.Size(72, 60)
        Me.btnEndH14.TabIndex = 7
        Me.btnEndH14.Text = "2 pm"
        '
        'btnEndM50
        '
        Me.btnEndM50.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnEndM50.Location = New System.Drawing.Point(341, 257)
        Me.btnEndM50.Name = "btnEndM50"
        Me.btnEndM50.Size = New System.Drawing.Size(72, 60)
        Me.btnEndM50.TabIndex = 24
        Me.btnEndM50.Text = ":50"
        '
        'btnEndH13
        '
        Me.btnEndH13.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnEndH13.Location = New System.Drawing.Point(165, 125)
        Me.btnEndH13.Name = "btnEndH13"
        Me.btnEndH13.Size = New System.Drawing.Size(72, 60)
        Me.btnEndH13.TabIndex = 6
        Me.btnEndH13.Text = "1 pm"
        '
        'btnEndM45
        '
        Me.btnEndM45.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnEndM45.Location = New System.Drawing.Point(263, 257)
        Me.btnEndM45.Name = "btnEndM45"
        Me.btnEndM45.Size = New System.Drawing.Size(72, 60)
        Me.btnEndM45.TabIndex = 23
        Me.btnEndM45.Text = ":45"
        '
        'btnEndH12
        '
        Me.btnEndH12.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnEndH12.Location = New System.Drawing.Point(87, 125)
        Me.btnEndH12.Name = "btnEndH12"
        Me.btnEndH12.Size = New System.Drawing.Size(72, 60)
        Me.btnEndH12.TabIndex = 5
        Me.btnEndH12.Text = "12"
        '
        'btnEndM40
        '
        Me.btnEndM40.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnEndM40.Location = New System.Drawing.Point(419, 191)
        Me.btnEndM40.Name = "btnEndM40"
        Me.btnEndM40.Size = New System.Drawing.Size(72, 60)
        Me.btnEndM40.TabIndex = 22
        Me.btnEndM40.Text = ":40"
        '
        'btnEndH11
        '
        Me.btnEndH11.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnEndH11.Location = New System.Drawing.Point(9, 125)
        Me.btnEndH11.Name = "btnEndH11"
        Me.btnEndH11.Size = New System.Drawing.Size(72, 60)
        Me.btnEndH11.TabIndex = 4
        Me.btnEndH11.Text = "11 am"
        '
        'btnEndM35
        '
        Me.btnEndM35.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnEndM35.Location = New System.Drawing.Point(341, 191)
        Me.btnEndM35.Name = "btnEndM35"
        Me.btnEndM35.Size = New System.Drawing.Size(72, 60)
        Me.btnEndM35.TabIndex = 21
        Me.btnEndM35.Text = ":35"
        '
        'btnEndH10
        '
        Me.btnEndH10.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnEndH10.Location = New System.Drawing.Point(165, 59)
        Me.btnEndH10.Name = "btnEndH10"
        Me.btnEndH10.Size = New System.Drawing.Size(72, 60)
        Me.btnEndH10.TabIndex = 3
        Me.btnEndH10.Text = "10 am"
        '
        'btnEndM30
        '
        Me.btnEndM30.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnEndM30.Location = New System.Drawing.Point(263, 191)
        Me.btnEndM30.Name = "btnEndM30"
        Me.btnEndM30.Size = New System.Drawing.Size(72, 60)
        Me.btnEndM30.TabIndex = 20
        Me.btnEndM30.Text = ":30"
        '
        'btnEndM25
        '
        Me.btnEndM25.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnEndM25.Location = New System.Drawing.Point(419, 125)
        Me.btnEndM25.Name = "btnEndM25"
        Me.btnEndM25.Size = New System.Drawing.Size(72, 60)
        Me.btnEndM25.TabIndex = 19
        Me.btnEndM25.Text = ":25"
        '
        'btnEndM20
        '
        Me.btnEndM20.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnEndM20.Location = New System.Drawing.Point(341, 125)
        Me.btnEndM20.Name = "btnEndM20"
        Me.btnEndM20.Size = New System.Drawing.Size(72, 60)
        Me.btnEndM20.TabIndex = 18
        Me.btnEndM20.Text = ":20"
        '
        'btnEndM15
        '
        Me.btnEndM15.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnEndM15.Location = New System.Drawing.Point(263, 125)
        Me.btnEndM15.Name = "btnEndM15"
        Me.btnEndM15.Size = New System.Drawing.Size(72, 60)
        Me.btnEndM15.TabIndex = 17
        Me.btnEndM15.Text = ":15"
        '
        'btnEndM10
        '
        Me.btnEndM10.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnEndM10.Location = New System.Drawing.Point(419, 59)
        Me.btnEndM10.Name = "btnEndM10"
        Me.btnEndM10.Size = New System.Drawing.Size(72, 60)
        Me.btnEndM10.TabIndex = 10
        Me.btnEndM10.Text = ":10"
        '
        'btnEndM5
        '
        Me.btnEndM5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnEndM5.Location = New System.Drawing.Point(341, 59)
        Me.btnEndM5.Name = "btnEndM5"
        Me.btnEndM5.Size = New System.Drawing.Size(72, 60)
        Me.btnEndM5.TabIndex = 9
        Me.btnEndM5.Text = ":05"
        '
        'btnEndM0
        '
        Me.btnEndM0.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnEndM0.Location = New System.Drawing.Point(263, 59)
        Me.btnEndM0.Name = "btnEndM0"
        Me.btnEndM0.Size = New System.Drawing.Size(72, 60)
        Me.btnEndM0.TabIndex = 8
        Me.btnEndM0.Text = ":00"
        '
        'btnAdd
        '
        Me.btnAdd.Image = CType(resources.GetObject("btnAdd.Image"), System.Drawing.Image)
        Me.btnAdd.Location = New System.Drawing.Point(12, 481)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(245, 45)
        Me.btnAdd.TabIndex = 3
        Me.btnAdd.Text = "Add Sleep Record"
        '
        'btnSleep5
        '
        Me.btnSleep5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnSleep5.Location = New System.Drawing.Point(681, 102)
        Me.btnSleep5.Name = "btnSleep5"
        Me.btnSleep5.Size = New System.Drawing.Size(160, 33)
        Me.btnSleep5.TabIndex = 4
        Me.btnSleep5.Text = "Sleep 5"
        '
        'btnSleep6
        '
        Me.btnSleep6.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnSleep6.Location = New System.Drawing.Point(847, 102)
        Me.btnSleep6.Name = "btnSleep6"
        Me.btnSleep6.Size = New System.Drawing.Size(160, 33)
        Me.btnSleep6.TabIndex = 5
        Me.btnSleep6.Text = "Sleep 6"
        '
        'frmSleep
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(1024, 538)
        Me.Controls.Add(Me.btnSleep6)
        Me.Controls.Add(Me.btnSleep5)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnRemoveSleep)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnSleep4)
        Me.Controls.Add(Me.btnSleep3)
        Me.Controls.Add(Me.btnSleep2)
        Me.Controls.Add(Me.btnSleep1)
        Me.Name = "frmSleep"
        Me.Text = "Sleep Records"
        Me.Controls.SetChildIndex(Me.btnSleep1, 0)
        Me.Controls.SetChildIndex(Me.btnSleep2, 0)
        Me.Controls.SetChildIndex(Me.btnSleep3, 0)
        Me.Controls.SetChildIndex(Me.btnSleep4, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.btnRemoveSleep, 0)
        Me.Controls.SetChildIndex(Me.GroupBox2, 0)
        Me.Controls.SetChildIndex(Me.btnAdd, 0)
        Me.Controls.SetChildIndex(Me.btnSleep5, 0)
        Me.Controls.SetChildIndex(Me.btnSleep6, 0)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSleep4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSleep3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSleep2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSleep1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnRemoveSleep As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStartH10 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnStartM55 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStartM50 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStartM45 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStartM40 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStartM35 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStartM30 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStartM25 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStartM20 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStartM15 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStartH9 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStartH16 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStartH15 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStartH14 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStartH13 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStartH12 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStartM10 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStartM5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStartM0 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStartH11 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btnEndM55 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEndM50 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEndM45 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEndM40 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEndM35 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEndM30 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEndM25 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEndM20 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEndM15 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEndM10 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEndM5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEndM0 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEndH9 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEndH16 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEndH15 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEndH14 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEndH13 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEndH12 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEndH11 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEndH10 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStartH19 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStartH18 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStartH17 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStartH8 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEndH19 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEndH8 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEndH18 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEndH17 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSleep5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSleep6 As DevExpress.XtraEditors.SimpleButton

End Class
