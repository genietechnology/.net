﻿Option Strict On

Imports NurseryTouchscreen.SharedModule

Public Class frmAbsent

    Private m_DayID As String = Parameters.TodayID

    Private m_ChildID As String = ""
    Private m_ChildName As String = ""
    Private m_ChildGroupID As String

    Private m_FamilyID As String

    Public Sub New(ByVal ChildID As String, ByVal ChildName As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_ChildID = ChildID
        m_ChildName = ChildName

    End Sub

    Private Sub frmAbsent_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtChild.Text = m_ChildName
        SetChildDetails()
    End Sub

    Private Sub SetChildDetails()

        Dim _SQL As String = ""
        _SQL += "select family_id, group_id, nappies, milk, baby_food, off_menu,"
        _SQL += " med_allergies, med_medication, med_notes, allergy_rating"
        _SQL += " from children"
        _SQL += " where id = '" & m_ChildID & "'"

        Dim _dr As DataRow = DAL.ReturnDataRow(_SQL)
        If _dr IsNot Nothing Then

            'used for drilldown
            m_ChildGroupID = _dr.Item("group_id").ToString
            m_FamilyID = _dr.Item("family_id").ToString

            _dr = Nothing

        End If

        _dr = Nothing

    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click

        If Not CheckAnswers Then Exit Sub

        btnAccept.Enabled = False
        Application.DoEvents()

        SharedModule.LogActivity(TodayID, m_ChildID, m_ChildName, EnumActivityType.Absence, fldReason.ValueText, _
                                 fldContact.ValueText, "", "", _
                                 fldStaff.ValueID, fldStaff.ValueText, fldComments.ValueText)

        Me.DialogResult = DialogResult.OK

        btnAccept.Enabled = True
        Application.DoEvents()

    End Sub

    Private Function CheckAnswers() As Boolean

        If fldContact.ValueText = "" Then
            Msgbox("Please select the contact reporting the absence.", MessageBoxIcon.Exclamation, "Log Absence")
            Return False
        End If

        If fldReason.ValueText = "" Then
            Msgbox("Please enter the reason for absence.", MessageBoxIcon.Exclamation, "Log Absence")
            Return False
        End If

        If fldStaff.ValueText = "" Then
            Msgbox("Please select the staff member recording the absence.", MessageBoxIcon.Exclamation, "Log Absence")
            Return False
        End If

        Return True

    End Function

    Private Sub fldStaff_ButtonClick(sender As Object, e As EventArgs) Handles fldStaff.ButtonClick

        Dim _P As Pair = Business.Staff.FindStaff(Enums.PersonMode.OnlyCheckedIn)
        If _P IsNot Nothing Then
            fldStaff.ValueID = _P.Code
            fldStaff.ValueText = _P.Text
        End If

    End Sub

    Private Sub fldContact_ButtonClick(sender As Object, e As EventArgs) Handles fldContact.ButtonClick

        Dim _SQL As String = "select id, fullname from Contacts" & _
                             " where Contacts.family_id = '" + m_FamilyID + "'"

        Dim _P As Pair = ReturnButtonSQL(_SQL, "Select Contact")
        If _P IsNot Nothing Then
            fldContact.ValueID = _P.Code
            fldContact.ValueText = _P.Text
        End If

    End Sub

    Private Sub fldComments_AfterButtonClick(sender As Object, e As EventArgs) Handles fldComments.AfterButtonClick
        txtComments.Text = fldComments.ValueText
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

End Class