﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOSK
    Inherits NurseryTouchscreen.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim VirtualKeyboardColorTable1 As DevComponents.DotNetBar.Keyboard.VirtualKeyboardColorTable = New DevComponents.DotNetBar.Keyboard.VirtualKeyboardColorTable()
        Dim FlatStyleRenderer1 As DevComponents.DotNetBar.Keyboard.FlatStyleRenderer = New DevComponents.DotNetBar.Keyboard.FlatStyleRenderer()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.tlp = New System.Windows.Forms.TableLayoutPanel()
        Me.tbQT = New DevExpress.XtraBars.Navigation.TileBar()
        Me.tbgQuickText = New DevExpress.XtraBars.Navigation.TileBarGroup()
        Me.kc = New DevComponents.DotNetBar.Keyboard.KeyboardControl()
        Me.txtText = New DevExpress.XtraEditors.MemoEdit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        Me.tlp.SuspendLayout()
        CType(Me.txtText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'PanelControl1
        '
        Me.PanelControl1.Appearance.BackColor = System.Drawing.Color.Black
        Me.PanelControl1.Appearance.Options.UseBackColor = True
        Me.PanelControl1.Controls.Add(Me.tlp)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(1500, 850)
        Me.PanelControl1.TabIndex = 78
        '
        'tlp
        '
        Me.tlp.ColumnCount = 2
        Me.tlp.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75.0!))
        Me.tlp.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.tlp.Controls.Add(Me.tbQT, 0, 0)
        Me.tlp.Controls.Add(Me.kc, 0, 1)
        Me.tlp.Controls.Add(Me.txtText, 0, 0)
        Me.tlp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp.Location = New System.Drawing.Point(2, 2)
        Me.tlp.Name = "tlp"
        Me.tlp.RowCount = 2
        Me.tlp.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 45.45454!))
        Me.tlp.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 54.54546!))
        Me.tlp.Size = New System.Drawing.Size(1496, 846)
        Me.tlp.TabIndex = 66
        '
        'tbQT
        '
        Me.tbQT.AllowDrag = False
        Me.tbQT.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbQT.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        Me.tbQT.Groups.Add(Me.tbgQuickText)
        Me.tbQT.Location = New System.Drawing.Point(1125, 3)
        Me.tbQT.MaxId = 2
        Me.tbQT.Name = "tbQT"
        Me.tbQT.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.tlp.SetRowSpan(Me.tbQT, 2)
        Me.tbQT.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollButtons
        Me.tbQT.Size = New System.Drawing.Size(368, 840)
        Me.tbQT.TabIndex = 69
        '
        'tbgQuickText
        '
        Me.tbgQuickText.Name = "tbgQuickText"
        '
        'kc
        '
        VirtualKeyboardColorTable1.BackgroundColor = System.Drawing.Color.Black
        VirtualKeyboardColorTable1.DarkKeysColor = System.Drawing.Color.FromArgb(CType(CType(29, Byte), Integer), CType(CType(28, Byte), Integer), CType(CType(33, Byte), Integer))
        VirtualKeyboardColorTable1.DownKeysColor = System.Drawing.Color.White
        VirtualKeyboardColorTable1.DownTextColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer))
        VirtualKeyboardColorTable1.KeysColor = System.Drawing.Color.FromArgb(CType(CType(48, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(55, Byte), Integer))
        VirtualKeyboardColorTable1.LightKeysColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(76, Byte), Integer))
        VirtualKeyboardColorTable1.PressedKeysColor = System.Drawing.Color.FromArgb(CType(CType(16, Byte), Integer), CType(CType(161, Byte), Integer), CType(CType(81, Byte), Integer))
        VirtualKeyboardColorTable1.TextColor = System.Drawing.Color.White
        VirtualKeyboardColorTable1.ToggleTextColor = System.Drawing.Color.Green
        VirtualKeyboardColorTable1.TopBarTextColor = System.Drawing.Color.White
        Me.kc.ColorTable = VirtualKeyboardColorTable1
        Me.kc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.kc.IsTopBarVisible = False
        Me.kc.Location = New System.Drawing.Point(3, 387)
        Me.kc.Name = "kc"
        FlatStyleRenderer1.ColorTable = VirtualKeyboardColorTable1
        FlatStyleRenderer1.ForceAntiAlias = False
        Me.kc.Renderer = FlatStyleRenderer1
        Me.kc.Size = New System.Drawing.Size(1116, 456)
        Me.kc.TabIndex = 67
        Me.kc.Text = "KeyboardControl1"
        '
        'txtText
        '
        Me.txtText.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtText.Location = New System.Drawing.Point(3, 3)
        Me.txtText.Name = "txtText"
        Me.txtText.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtText.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtText, True)
        Me.txtText.Size = New System.Drawing.Size(1116, 378)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtText, OptionsSpelling1)
        Me.txtText.TabIndex = 68
        '
        'frmOSK
        '
        Me.ClientSize = New System.Drawing.Size(1500, 850)
        Me.Controls.Add(Me.PanelControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmOSK"
        Me.Controls.SetChildIndex(Me.PanelControl1, 0)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.tlp.ResumeLayout(False)
        CType(Me.txtText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents tlp As TableLayoutPanel
    Friend WithEvents tbQT As DevExpress.XtraBars.Navigation.TileBar
    Friend WithEvents tbgQuickText As DevExpress.XtraBars.Navigation.TileBarGroup
    Friend WithEvents kc As DevComponents.DotNetBar.Keyboard.KeyboardControl
    Friend WithEvents txtText As DevExpress.XtraEditors.MemoEdit
End Class
