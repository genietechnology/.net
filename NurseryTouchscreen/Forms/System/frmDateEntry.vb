﻿Public Class frmDateEntry

    Public Property ReturnValue As String
        Get
            Return txtText.Text
        End Get
        Set(value As String)
            txtText.Text = value
        End Set
    End Property

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click
        Me.DialogResult = DialogResult.OK
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        txtText.Text = ""
        Me.DialogResult = DialogResult.Cancel
    End Sub

    Private Sub dn_EditDateModified(sender As Object, e As EventArgs) Handles dn.EditDateModified
        txtText.Text = Format(dn.DateTime, "dd/MM/yyyy")
    End Sub

    Private Sub btnToday_Click(sender As Object, e As EventArgs) Handles btnToday.Click
        txtText.Text = Format(Today, "dd/MM/yyyy")
    End Sub

    Private Sub frmDateEntry_Load(sender As Object, e As EventArgs) Handles Me.Load
        dn.DateTime = Today
    End Sub

End Class