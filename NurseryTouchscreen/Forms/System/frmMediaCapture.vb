﻿Option Strict On
Imports NurseryTouchscreen.SharedModule

Public Class frmMediaCapture

    Private m_MediaID As Guid? = Nothing
    Private m_SelectedFile As String = ""
    Private m_SelectedFileDetail As FileDetail = Nothing
    Private m_Capturing As Boolean = False
    Private m_Captures As New List(Of FileDetail)
    Private m_CaptureFolder As String = SharedModule.CameraRollPath

    Public Sub New(ByVal MultipleImages As Boolean)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public ReadOnly Property MediaID As Guid?
        Get
            Return m_MediaID
        End Get
    End Property

    Public ReadOnly Property SelectedFile As String
        Get
            Return m_SelectedFile
        End Get
    End Property

    Public ReadOnly Property SelectedFiles As List(Of FileDetail)
        Get
            Return m_Captures
        End Get
    End Property

    Private Sub frmVideoCapture_Load(sender As Object, e As EventArgs) Handles Me.Load
        SetupFolders()
        SetCMDs()
    End Sub

    Private Sub SetupFolders()
        If Not IO.Directory.Exists(m_CaptureFolder) Then IO.Directory.CreateDirectory(m_CaptureFolder)
    End Sub

    Private Sub PreviewFile(ByVal RowHandle As Integer)

        picPreview.Image = Nothing

        m_SelectedFileDetail = CType(tgFiles.GetRowObject(RowHandle), FileDetail)
        m_SelectedFile = m_SelectedFileDetail.FullPath

        Select Case m_SelectedFileDetail.Extension

            Case ".jpg"
                picPreview.Image = MediaHandler.ReturnImage(m_SelectedFile)

            Case Else
                picPreview.Image = Nothing

        End Select

    End Sub

    Private Sub StartCapture()

        btnPhoto.Text = "Finish Capture"
        m_Capturing = True

        'launch camera application...
        Shell(Parameters.Capture.CameraCommand, AppWinStyle.MaximizedFocus)

        SetCMDs()

    End Sub

    Private Sub StopCapture()
        btnPhoto.Text = "Start Capture"
        DisplayCaptures()
        m_Capturing = False
        SetCMDs()
    End Sub

    Private Sub SetCMDs()

        If m_Capturing Then
            btnAccept.Enabled = False
        Else
            If m_Captures.Count > 0 Then
                btnAccept.Enabled = True
            Else
                btnAccept.Enabled = False
            End If
        End If

    End Sub

    Private Sub DisplayCaptures()

        m_Captures.Clear()
        For Each _f As String In IO.Directory.GetFiles(m_CaptureFolder)
            If _f.Contains(".ini") Then Continue For
            If _f.Contains(".db") Then Continue For
            m_Captures.Add(New FileDetail(_f))
        Next _f

        tgFiles.Populate(m_Captures)
        tgFiles.Columns("ID").Visible = False
        tgFiles.Columns("Extension").Visible = False
        tgFiles.Columns("FullPath").Visible = False

    End Sub

#Region "Control Events"

    Private Sub btnPhoto_Click(sender As Object, e As EventArgs) Handles btnPhoto.Click
        If m_Capturing Then
            StopCapture()
        Else
            StartCapture()
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        ClearDown()
        Me.DialogResult = DialogResult.Cancel
    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click
        SaveMultiple()
        ClearDown()
    End Sub

    Private Sub ClearDown()
        picPreview.Image = Nothing
        picPreview.Dispose()
        picPreview = Nothing
    End Sub

    Private Sub SaveMultiple()

        If m_Captures.Count > 0 Then
            Me.DialogResult = DialogResult.OK
        Else
            Me.DialogResult = DialogResult.Cancel
        End If

        Me.Close()

    End Sub

    Private Sub tgFiles_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles tgFiles.FocusedRowChanged
        PreviewFile(e.FocusedRowHandle)
    End Sub

#End Region

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If Msgbox("Are you sure you want to delete this Media?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Confirm Delete") = DialogResult.Yes Then
            picPreview.Image = Nothing
            IO.File.Delete(m_SelectedFile)
            DisplayCaptures()
        End If
    End Sub
End Class