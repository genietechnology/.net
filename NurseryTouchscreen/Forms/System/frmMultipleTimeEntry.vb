﻿Imports DevExpress.XtraBars.Navigation
Imports DevExpress.XtraEditors
Public Class frmMultipleTimeEntry

    Private mTimes As New List(Of String)
    Private mDefaultValue As String

    Private mHours As String
    Private mMinutes As String

    Public Property ReturnValue() As List(Of String)
        Get
            Return mTimes
        End Get
        Set(value As List(Of String))
            mTimes = value
        End Set
    End Property

    Public Property DefaultValue As String
        Get
            Return mDefaultValue
        End Get
        Set(value As String)
            mDefaultValue = value
            txtText.Text = value
        End Set
    End Property

    Private Sub btnClear_Click(sender As System.Object, e As System.EventArgs) Handles btnClear.Click
        txtText.Text = ""
        mTimes.Clear()
        ClearButtons()
        PopulateTiles()
    End Sub

#Region "Button Clicks"

    Private Sub btnH0_Click(sender As Object, e As EventArgs) Handles btnH0.Click
        ClearHours()
        btnH0.Appearance.BackColor = Color.Yellow
        mHours = "00"
        BuildTime()
    End Sub

    Private Sub btnH1_Click(sender As Object, e As EventArgs) Handles btnH1.Click
        ClearHours()
        btnH1.Appearance.BackColor = Color.Yellow
        mHours = "01"
        BuildTime()
    End Sub

    Private Sub btnH2_Click(sender As Object, e As EventArgs) Handles btnH2.Click
        ClearHours()
        btnH2.Appearance.BackColor = Color.Yellow
        mHours = "02"
        BuildTime()
    End Sub

    Private Sub btnH3_Click(sender As Object, e As EventArgs) Handles btnH3.Click
        ClearHours()
        btnH3.Appearance.BackColor = Color.Yellow
        mHours = "03"
        BuildTime()
    End Sub

    Private Sub btnH4_Click(sender As Object, e As EventArgs) Handles btnH4.Click
        ClearHours()
        btnH4.Appearance.BackColor = Color.Yellow
        mHours = "04"
        BuildTime()
    End Sub

    Private Sub btnH5_Click(sender As Object, e As EventArgs) Handles btnH5.Click
        ClearHours()
        btnH5.Appearance.BackColor = Color.Yellow
        mHours = "05"
        BuildTime()
    End Sub

    Private Sub btnH6_Click(sender As Object, e As EventArgs) Handles btnH6.Click
        ClearHours()
        btnH6.Appearance.BackColor = Color.Yellow
        mHours = "06"
        BuildTime()
    End Sub

    Private Sub btnH7_Click(sender As Object, e As EventArgs) Handles btnH7.Click
        ClearHours()
        btnH7.Appearance.BackColor = Color.Yellow
        mHours = "07"
        BuildTime()
    End Sub

    Private Sub btnH8_Click(sender As Object, e As EventArgs) Handles btnH8.Click
        ClearHours()
        btnH8.Appearance.BackColor = Color.Yellow
        mHours = "08"
        BuildTime()
    End Sub

    Private Sub btnH9_Click(sender As System.Object, e As System.EventArgs) Handles btnH9.Click
        ClearHours()
        btnH9.Appearance.BackColor = Color.Yellow
        mHours = "09"
        BuildTime()
    End Sub

    Private Sub btnH10_Click(sender As System.Object, e As System.EventArgs) Handles btnH10.Click
        ClearHours()
        btnH10.Appearance.BackColor = Color.Yellow
        mHours = "10"
        BuildTime()
    End Sub

    Private Sub btnH11_Click(sender As System.Object, e As System.EventArgs) Handles btnH11.Click
        ClearHours()
        btnH11.Appearance.BackColor = Color.Yellow
        mHours = "11"
        BuildTime()
    End Sub

    Private Sub btnH12_Click(sender As System.Object, e As System.EventArgs) Handles btnH12.Click
        ClearHours()
        btnH12.Appearance.BackColor = Color.Yellow
        mHours = "12"
        BuildTime()
    End Sub

    Private Sub btnH13_Click(sender As System.Object, e As System.EventArgs) Handles btnH13.Click
        ClearHours()
        btnH13.Appearance.BackColor = Color.Yellow
        mHours = "13"
        BuildTime()
    End Sub

    Private Sub btnH14_Click(sender As System.Object, e As System.EventArgs) Handles btnH14.Click
        ClearHours()
        btnH14.Appearance.BackColor = Color.Yellow
        mHours = "14"
        BuildTime()
    End Sub

    Private Sub btnH15_Click(sender As System.Object, e As System.EventArgs) Handles btnH15.Click
        ClearHours()
        btnH15.Appearance.BackColor = Color.Yellow
        mHours = "15"
        BuildTime()
    End Sub

    Private Sub btnH16_Click(sender As System.Object, e As System.EventArgs) Handles btnH16.Click
        ClearHours()
        btnH16.Appearance.BackColor = Color.Yellow
        mHours = "16"
        BuildTime()
    End Sub

    Private Sub btnH17_Click(sender As Object, e As EventArgs) Handles btnH17.Click
        ClearHours()
        btnH17.Appearance.BackColor = Color.Yellow
        mHours = "17"
        BuildTime()
    End Sub

    Private Sub btnH18_Click(sender As Object, e As EventArgs) Handles btnH18.Click
        ClearHours()
        btnH18.Appearance.BackColor = Color.Yellow
        mHours = "18"
        BuildTime()
    End Sub

    Private Sub btnH19_Click(sender As Object, e As EventArgs) Handles btnH19.Click
        ClearHours()
        btnH19.Appearance.BackColor = Color.Yellow
        mHours = "19"
        BuildTime()
    End Sub

    Private Sub btnH20_Click(sender As Object, e As EventArgs) Handles btnH20.Click
        ClearHours()
        btnH20.Appearance.BackColor = Color.Yellow
        mHours = "20"
        BuildTime()
    End Sub

    Private Sub btnH21_Click(sender As Object, e As EventArgs) Handles btnH21.Click
        ClearHours()
        btnH21.Appearance.BackColor = Color.Yellow
        mHours = "21"
        BuildTime()
    End Sub

    Private Sub btnH22_Click(sender As Object, e As EventArgs) Handles btnH22.Click
        ClearHours()
        btnH22.Appearance.BackColor = Color.Yellow
        mHours = "22"
        BuildTime()
    End Sub

    Private Sub btnH23_Click(sender As Object, e As EventArgs) Handles btnH23.Click
        ClearHours()
        btnH23.Appearance.BackColor = Color.Yellow
        mHours = "23"
        BuildTime()
    End Sub

    Private Sub btnM0_Click(sender As System.Object, e As System.EventArgs) Handles btnM0.Click
        ClearMinutes()
        btnM0.Appearance.BackColor = Color.Yellow
        mMinutes = "00"
        BuildTime()
    End Sub

    Private Sub btnM5_Click(sender As System.Object, e As System.EventArgs) Handles btnM5.Click
        ClearMinutes()
        btnM5.Appearance.BackColor = Color.Yellow
        mMinutes = "05"
        BuildTime()
    End Sub

    Private Sub btnM10_Click(sender As System.Object, e As System.EventArgs) Handles btnM10.Click
        ClearMinutes()
        btnM10.Appearance.BackColor = Color.Yellow
        mMinutes = "10"
        BuildTime()
    End Sub

    Private Sub btnM15_Click(sender As System.Object, e As System.EventArgs) Handles btnM15.Click
        ClearMinutes()
        btnM15.Appearance.BackColor = Color.Yellow
        mMinutes = "15"
        BuildTime()
    End Sub

    Private Sub btnM20_Click(sender As System.Object, e As System.EventArgs) Handles btnM20.Click
        ClearMinutes()
        btnM20.Appearance.BackColor = Color.Yellow
        mMinutes = "20"
        BuildTime()
    End Sub

    Private Sub btnM25_Click(sender As System.Object, e As System.EventArgs) Handles btnM25.Click
        ClearMinutes()
        btnM25.Appearance.BackColor = Color.Yellow
        mMinutes = "25"
        BuildTime()
    End Sub

    Private Sub btnM30_Click(sender As System.Object, e As System.EventArgs) Handles btnM30.Click
        ClearMinutes()
        btnM30.Appearance.BackColor = Color.Yellow
        mMinutes = "30"
        BuildTime()
    End Sub

    Private Sub btnM35_Click(sender As System.Object, e As System.EventArgs) Handles btnM35.Click
        ClearMinutes()
        btnM35.Appearance.BackColor = Color.Yellow
        mMinutes = "35"
        BuildTime()
    End Sub

    Private Sub btnM40_Click(sender As System.Object, e As System.EventArgs) Handles btnM40.Click
        ClearMinutes()
        btnM40.Appearance.BackColor = Color.Yellow
        mMinutes = "40"
        BuildTime()
    End Sub

    Private Sub btnM45_Click(sender As System.Object, e As System.EventArgs) Handles btnM45.Click
        ClearMinutes()
        btnM45.Appearance.BackColor = Color.Yellow
        mMinutes = "45"
        BuildTime()
    End Sub

    Private Sub btnM50_Click(sender As System.Object, e As System.EventArgs) Handles btnM50.Click
        ClearMinutes()
        btnM50.Appearance.BackColor = Color.Yellow
        mMinutes = "50"
        BuildTime()
    End Sub

    Private Sub btnM55_Click(sender As System.Object, e As System.EventArgs) Handles btnM55.Click
        ClearMinutes()
        btnM55.Appearance.BackColor = Color.Yellow
        mMinutes = "55"
        BuildTime()
    End Sub

#End Region

    Private Sub SetTime(Time As String)

        Dim hours As String = Mid(Time, 1, 2)
        Dim mins As String = Mid(Time, 4, 2)

        SetHours(hours)
        SetMinutes(mins)

        mHours = hours
        mMinutes = mins

    End Sub

    Private Sub BuildTime()

        'default to o'clock
        If mMinutes = "" Then

            mMinutes = "00"

            ClearMinutes()
            btnM0.Appearance.BackColor = Color.Yellow

        End If

        Dim time As String = mHours + ":" + mMinutes
        txtText.Text = time

    End Sub

    Private Sub SetButtons(ByVal Enabled As Boolean)
        btnH9.Enabled = Enabled
        btnH10.Enabled = Enabled
        btnH11.Enabled = Enabled
        btnH12.Enabled = Enabled
        btnH13.Enabled = Enabled
        btnH14.Enabled = Enabled
        btnH15.Enabled = Enabled
        btnH16.Enabled = Enabled
        btnM0.Enabled = Enabled
        btnM5.Enabled = Enabled
        btnM10.Enabled = Enabled
        btnM15.Enabled = Enabled
        btnM20.Enabled = Enabled
        btnM25.Enabled = Enabled
        btnM30.Enabled = Enabled
        btnM35.Enabled = Enabled
        btnM40.Enabled = Enabled
        btnM45.Enabled = Enabled
        btnM50.Enabled = Enabled
        btnM55.Enabled = Enabled
    End Sub

    Private Sub ClearHours()
        btnH0.Appearance.Reset()
        btnH1.Appearance.Reset()
        btnH2.Appearance.Reset()
        btnH3.Appearance.Reset()
        btnH4.Appearance.Reset()
        btnH5.Appearance.Reset()
        btnH6.Appearance.Reset()
        btnH7.Appearance.Reset()
        btnH8.Appearance.Reset()
        btnH9.Appearance.Reset()
        btnH10.Appearance.Reset()
        btnH11.Appearance.Reset()
        btnH12.Appearance.Reset()
        btnH13.Appearance.Reset()
        btnH14.Appearance.Reset()
        btnH15.Appearance.Reset()
        btnH16.Appearance.Reset()
        btnH17.Appearance.Reset()
        btnH18.Appearance.Reset()
        btnH19.Appearance.Reset()
        btnH20.Appearance.Reset()
        btnH21.Appearance.Reset()
        btnH22.Appearance.Reset()
        btnH23.Appearance.Reset()
    End Sub

    Private Sub ClearMinutes()
        btnM0.Appearance.Reset()
        btnM5.Appearance.Reset()
        btnM10.Appearance.Reset()
        btnM15.Appearance.Reset()
        btnM20.Appearance.Reset()
        btnM25.Appearance.Reset()
        btnM30.Appearance.Reset()
        btnM35.Appearance.Reset()
        btnM40.Appearance.Reset()
        btnM45.Appearance.Reset()
        btnM50.Appearance.Reset()
        btnM55.Appearance.Reset()
    End Sub

    Private Sub ClearButtons()
        ClearHours()
        ClearMinutes()
    End Sub

    Private Sub SetHours(Hours As String)

        If Hours = "09" Then
            btnH9.Appearance.BackColor = Color.Yellow
        End If

        If Hours = "10" Then
            btnH10.Appearance.BackColor = Color.Yellow
        End If

        If Hours = "11" Then
            btnH11.Appearance.BackColor = Color.Yellow
        End If

        If Hours = "12" Then
            btnH12.Appearance.BackColor = Color.Yellow
        End If

        If Hours = "13" Then
            btnH13.Appearance.BackColor = Color.Yellow
        End If

        If Hours = "14" Then
            btnH14.Appearance.BackColor = Color.Yellow
        End If

        If Hours = "15" Then
            btnH15.Appearance.BackColor = Color.Yellow
        End If

        If Hours = "16" Then
            btnH16.Appearance.BackColor = Color.Yellow
        End If

    End Sub

    Private Sub SetMinutes(Minutes As String)

        If Minutes = "00" Then
            btnM0.Appearance.BackColor = Color.Yellow
        End If

        If Minutes = "05" Then
            btnM5.Appearance.BackColor = Color.Yellow
        End If

        If Minutes = "10" Then
            btnM10.Appearance.BackColor = Color.Yellow
        End If

        If Minutes = "15" Then
            btnM15.Appearance.BackColor = Color.Yellow
        End If

        If Minutes = "20" Then
            btnM20.Appearance.BackColor = Color.Yellow
        End If

        If Minutes = "25" Then
            btnM25.Appearance.BackColor = Color.Yellow
        End If

        If Minutes = "30" Then
            btnM30.Appearance.BackColor = Color.Yellow
        End If

        If Minutes = "35" Then
            btnM35.Appearance.BackColor = Color.Yellow
        End If

        If Minutes = "40" Then
            btnM40.Appearance.BackColor = Color.Yellow
        End If

        If Minutes = "45" Then
            btnM45.Appearance.BackColor = Color.Yellow
        End If

        If Minutes = "50" Then
            btnM50.Appearance.BackColor = Color.Yellow
        End If

        If Minutes = "55" Then
            btnM55.Appearance.BackColor = Color.Yellow
        End If

    End Sub

    Private Sub btnAccept_Click(sender As System.Object, e As System.EventArgs) Handles btnAccept.Click
        If Not String.IsNullOrWhiteSpace(txtText.Text) Then mTimes.Add(txtText.Text)
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        mTimes.Clear()
        Me.Close()
    End Sub

    Private Sub btnNow_Click(sender As Object, e As EventArgs) Handles btnNow.Click
        txtText.Text = Format(Now, "HH:mm")
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        If Not String.IsNullOrWhiteSpace(txtText.Text) Then
            mTimes.Add(txtText.Text)
            PopulateTiles()
            ClearButtons()
            txtText.Text = ""
        End If

    End Sub

    Private Sub PopulateTiles()

        tbgTime.Items.Clear()

        For Each time In mTimes

            Dim timeTile As New TileBarItem
            Dim timeTileElement As New TileItemElement

            With timeTile
                .Name = mTimes.IndexOf(time)
                .AppearanceItem.Normal.BackColor = Me.BackColor
                .ItemSize = TileBarItemSize.Wide
            End With

            With timeTileElement
                .Text = time.ToString
                .TextAlignment = TileItemContentAlignment.MiddleCenter
                .Appearance.Normal.FontSizeDelta = 10
                .Appearance.Normal.BackColor = Me.BackColor
                .Appearance.Normal.ForeColor = Me.ForeColor
            End With

            timeTile.Elements.Add(timeTileElement)
            tbgTime.Items.Add(timeTile)

        Next


    End Sub

    Private Sub frmMultipleTimeEntry_Load(sender As Object, e As EventArgs) Handles Me.Load
        PopulateTiles()
    End Sub
End Class
