﻿Imports DevExpress.XtraEditors.Repository

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDateEntry
    Inherits NurseryTouchscreen.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDateEntry))
        Me.btnToday = New DevExpress.XtraEditors.SimpleButton()
        Me.txtText = New System.Windows.Forms.TextBox()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAccept = New DevExpress.XtraEditors.SimpleButton()
        Me.dn = New DevExpress.XtraScheduler.DateNavigator()
        CType(Me.dn, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dn.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        '
        'btnToday
        '
        Me.btnToday.Image = Global.NurseryTouchscreen.My.Resources.Resources.alarm_32
        Me.btnToday.Location = New System.Drawing.Point(616, 12)
        Me.btnToday.Name = "btnToday"
        Me.btnToday.Size = New System.Drawing.Size(145, 61)
        Me.btnToday.TabIndex = 1
        Me.btnToday.Text = "Today"
        '
        'txtText
        '
        Me.txtText.Location = New System.Drawing.Point(14, 12)
        Me.txtText.Multiline = True
        Me.txtText.Name = "txtText"
        Me.txtText.Size = New System.Drawing.Size(594, 61)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtText, OptionsSpelling1)
        Me.txtText.TabIndex = 0
        '
        'btnCancel
        '
        Me.btnCancel.Image = CType(resources.GetObject("btnCancel.Image"), System.Drawing.Image)
        Me.btnCancel.Location = New System.Drawing.Point(616, 433)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(145, 57)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "Cancel"
        '
        'btnAccept
        '
        Me.btnAccept.Image = CType(resources.GetObject("btnAccept.Image"), System.Drawing.Image)
        Me.btnAccept.Location = New System.Drawing.Point(616, 370)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(145, 57)
        Me.btnAccept.TabIndex = 3
        Me.btnAccept.Text = "Accept"
        '
        'dn
        '
        Me.dn.Appearance.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dn.Appearance.Options.UseFont = True
        Me.dn.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dn.CellPadding = New System.Windows.Forms.Padding(2)
        Me.dn.DateTime = New Date(2015, 8, 16, 0, 0, 0, 0)
        Me.dn.EditValue = New Date(2015, 8, 16, 0, 0, 0, 0)
        Me.dn.FirstDayOfWeek = System.DayOfWeek.Monday
        Me.dn.Location = New System.Drawing.Point(14, 82)
        Me.dn.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.dn.Name = "dn"
        Me.dn.SelectionMode = DevExpress.XtraEditors.Repository.CalendarSelectionMode.[Single]
        Me.dn.ShowTodayButton = False
        Me.dn.ShowWeekNumbers = False
        Me.dn.Size = New System.Drawing.Size(594, 408)
        Me.dn.TabIndex = 2
        '
        'frmDateEntry
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(773, 505)
        Me.Controls.Add(Me.btnToday)
        Me.Controls.Add(Me.txtText)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnAccept)
        Me.Controls.Add(Me.dn)
        Me.Font = New System.Drawing.Font("Segoe UI", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(11, 12, 11, 12)
        Me.Name = "frmDateEntry"
        Me.Text = "Select Date"
        Me.Controls.SetChildIndex(Me.dn, 0)
        Me.Controls.SetChildIndex(Me.btnAccept, 0)
        Me.Controls.SetChildIndex(Me.btnCancel, 0)
        Me.Controls.SetChildIndex(Me.txtText, 0)
        Me.Controls.SetChildIndex(Me.btnToday, 0)
        CType(Me.dn.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dn, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dn As DevExpress.XtraScheduler.DateNavigator
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAccept As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtText As System.Windows.Forms.TextBox
    Friend WithEvents btnToday As DevExpress.XtraEditors.SimpleButton
End Class
