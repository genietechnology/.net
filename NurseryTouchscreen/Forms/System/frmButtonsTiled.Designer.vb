﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmButtonsTiled
    Inherits NurseryTouchscreen.frmBase

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmButtonsTiled))
        Me.ctc = New NurseryTouchscreen.CareTileControl(Me.components)
        Me.tg = New DevExpress.XtraEditors.TileGroup()
        Me.panBottom = New DevExpress.XtraEditors.PanelControl()
        Me.btnAccept = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panBottom.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'ctc
        '
        Me.ctc.AllowDrag = False
        Me.ctc.AllowDragTilesBetweenGroups = False
        Me.ctc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ctc.DragSize = New System.Drawing.Size(0, 0)
        Me.ctc.Groups.Add(Me.tg)
        Me.ctc.ItemTileHeight = 1500
        Me.ctc.ItemTileWidth = 4000
        Me.ctc.Location = New System.Drawing.Point(0, 0)
        Me.ctc.MaxId = 32
        Me.ctc.Name = "ctc"
        Me.ctc.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.ctc.Size = New System.Drawing.Size(779, 343)
        Me.ctc.TabIndex = 78
        '
        'tg
        '
        Me.tg.Name = "tg"
        '
        'panBottom
        '
        Me.panBottom.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panBottom.Appearance.Options.UseFont = True
        Me.panBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.panBottom.Controls.Add(Me.btnAccept)
        Me.panBottom.Controls.Add(Me.btnCancel)
        Me.panBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panBottom.Location = New System.Drawing.Point(0, 343)
        Me.panBottom.Name = "panBottom"
        Me.panBottom.Size = New System.Drawing.Size(779, 56)
        Me.panBottom.TabIndex = 79
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAccept.Image = Global.NurseryTouchscreen.My.Resources.Resources.success_32
        Me.btnAccept.Location = New System.Drawing.Point(474, 3)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(145, 44)
        Me.btnAccept.TabIndex = 16
        Me.btnAccept.Text = "Accept"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Image = CType(resources.GetObject("btnCancel.Image"), System.Drawing.Image)
        Me.btnCancel.Location = New System.Drawing.Point(625, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(145, 44)
        Me.btnCancel.TabIndex = 15
        Me.btnCancel.Text = "Cancel"
        '
        'frmButtonsTiled
        '
        Me.ClientSize = New System.Drawing.Size(779, 399)
        Me.Controls.Add(Me.ctc)
        Me.Controls.Add(Me.panBottom)
        Me.Name = "frmButtonsTiled"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Controls.SetChildIndex(Me.panBottom, 0)
        Me.Controls.SetChildIndex(Me.ctc, 0)
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panBottom.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ctc As NurseryTouchscreen.CareTileControl
    Friend WithEvents tg As DevExpress.XtraEditors.TileGroup
    Protected Friend WithEvents panBottom As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAccept As DevExpress.XtraEditors.SimpleButton

End Class
