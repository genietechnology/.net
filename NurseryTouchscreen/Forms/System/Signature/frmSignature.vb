﻿Public Class frmSignature

    Private m_SignatureData As String = ""
    Private m_SignatureBytes As Byte()

    Public Property SignatureData As String
        Get
            Return m_SignatureData
        End Get
        Set(value As String)
            m_SignatureData = value
        End Set
    End Property

    Public Property SignatureBytes As Byte()
        Get
            Return m_SignatureBytes
        End Get
        Set(value As Byte())
            m_SignatureBytes = value
        End Set
    End Property

    Private Sub frmSignature_Load(sender As Object, e As EventArgs) Handles Me.Load
        If m_SignatureData Is Nothing Then Exit Sub
        If m_SignatureData = "" Then Exit Sub
        SignatureControl1.Value = m_SignatureData
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        SignatureControl1.Clear()
        SignatureControl1.Reset()
    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click
        SignatureControl1.AcceptSignature()
    End Sub

    Private Sub SignatureControl1_Signed(sender As Object, e As EventArgs) Handles SignatureControl1.Signed
        m_SignatureData = SignatureControl1.Value
        m_SignatureBytes = SignatureControl1.SignatureBytes
        Me.DialogResult = DialogResult.OK
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

End Class