﻿Public Class frmGrid

    Private m_SQL As String = ""
    Private m_Headings As String = ""

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub frmGrid_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Cursor = Cursors.WaitCursor
        TouchGrid1.Populate(m_SQL)
        TouchGrid1.SetHeadings(m_Headings)
        Me.Cursor = Cursors.Default
    End Sub

    Public Sub New(ByVal SQL As String, ByVal ColumnHeadings As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_SQL = SQL
        m_Headings = ColumnHeadings

    End Sub
End Class
