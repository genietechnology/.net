﻿Option Strict On

Imports NurseryTouchscreen.SharedModule
Imports DevExpress.XtraBars.Navigation
Imports DevExpress.XtraEditors
Imports System.IO

Public Class frmButtonsTiled

    Private m_Buttons As List(Of Pair) = Nothing
    Private m_ReturnPair As Pair = Nothing
    Private m_ReturnPairs As List(Of Pair) = Nothing
    Private m_CurrentFolder As String = ""

    Public Enum EnumPopulateMode
        Custom
        SQL
        Children
        ChildrenDueIn
        ChildrenCheckedIn
        FileSystem
    End Enum

#Region "Properties"

    Public Property PopulateMode As EnumPopulateMode
    Public Property RoomFilter As String
    Public Property MultiSelect As Boolean

    Public Property SQL As String
    Public Property FieldID As String
    Public Property FieldName As String
    Public Property FieldBottomLeft As String
    Public Property FieldBottomRight As String
    Public Property FieldPhoto As String

    Public Property RootPath As String

    Public WriteOnly Property ButtonCollection As List(Of Pair)
        Set(value As List(Of Pair))
            m_Buttons = value
        End Set
    End Property

    Public ReadOnly Property SelectedPair As Pair
        Get
            Return m_ReturnPair
        End Get
    End Property

    Public ReadOnly Property SelectedPairs As List(Of Pair)
        Get
            Return m_ReturnPairs
        End Get
    End Property

#End Region

    Private Sub frmButtonsTiled_Load(sender As Object, e As EventArgs) Handles Me.Load

        PopulateButtons()
        btnAccept.Visible = MultiSelect

    End Sub

    Private Sub PopulateButtons()

        tg.Items.Clear()

        If PopulateMode = EnumPopulateMode.Custom Then PopulateCustom()
        If PopulateMode = EnumPopulateMode.ChildrenDueIn Then PopulateDueIn()
        If PopulateMode = EnumPopulateMode.ChildrenCheckedIn Then PopulateCheckedIn()
        If PopulateMode = EnumPopulateMode.Children Then PopulateChildren()
        If PopulateMode = EnumPopulateMode.FileSystem Then PopulateFilesandFolders()

        If PopulateMode = EnumPopulateMode.SQL Then
            If SQL.Contains("|") Then
                PopulateHardCoded()
            Else
                PopulateWithSQL()
            End If
        End If

    End Sub

    Private Sub PopulateFilesandFolders()
        If RootPath = "" Then Exit Sub
        m_CurrentFolder = RootPath
        PopulateFolder()
    End Sub

    Private Sub PopulateFolder()

        tg.Items.Clear()

        If m_CurrentFolder <> RootPath Then
            Dim _i As New TileBarItem
            _i.Name = "btnHome"
            _i.Text = "Back to Home Folder"
            _i.AppearanceItem.Normal.BackColor = Color.DarkKhaki
            AddHandler _i.ItemClick, AddressOf TileClick
            tg.Items.Add(_i)
        End If

        For Each _Folder As String In IO.Directory.EnumerateDirectories(m_CurrentFolder)

            Dim _di As New IO.DirectoryInfo(_Folder)
            Dim _i As New TileBarItem

            _i.Name = "dir_" + _Folder
            _i.Text = _di.Name
            _i.AppearanceItem.Normal.BackColor = Color.DarkKhaki
            AddHandler _i.ItemClick, AddressOf TileClick
            tg.Items.Add(_i)

        Next

        For Each _File As String In IO.Directory.EnumerateFiles(m_CurrentFolder)

            Dim _fi As New IO.FileInfo(_File)
            Dim _i As New TileBarItem

            _i.Name = "file_" + _File
            _i.Text = _fi.Name

            AddHandler _i.ItemClick, AddressOf TileClick
            tg.Items.Add(_i)

        Next

    End Sub

    Private Sub PopulateHardCoded()

        Dim _Items As String() = Split(SQL, "|")
        If _Items IsNot Nothing Then

            For Each _Item As String In _Items

                Dim _Values As String() = Split(_Item, "=")
                If _Values IsNot Nothing Then

                    Dim _i As New TileBarItem

                    If _Values.Count = 1 Then
                        _i.Name = "t_" + _Values(0)
                        _i.Text = _Values(0)
                    Else
                        _i.Name = "t_" + _Values(0)
                        _i.Text = _Values(1)
                    End If

                    AddHandler _i.ItemClick, AddressOf TileClick
                    tg.Items.Add(_i)

                End If
            Next

        End If

    End Sub

    Private Sub PopulateWithSQL()

        Dim _DT As DataTable = DAL.ReturnDataTable(SQL)
        If Not _DT Is Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _i As New TileBarItem

                With _i
                    .Name = "t_" + _DR.Item(FieldID).ToString
                    .Text = _DR.Item(FieldName).ToString
                    .AppearanceItem.Normal.ForeColor = Color.Black
                    .AppearanceItem.Normal.FontSizeDelta = 4
                    .AppearanceItem.Normal.FontStyleDelta = FontStyle.Bold
                    .TextAlignment = TileItemContentAlignment.TopLeft
                    AddHandler _i.ItemClick, AddressOf TileClick
                End With

                If FieldBottomLeft <> "" Then
                    Dim _e As New TileItemElement
                    With _e
                        .Text = _DR.Item(FieldBottomLeft).ToString
                        .Appearance.Normal.FontSizeDelta = -2
                        .TextAlignment = TileItemContentAlignment.BottomLeft
                    End With
                    _i.Elements.Add(_e)
                End If

                If FieldBottomRight <> "" Then
                    Dim _e As New TileItemElement
                    With _e
                        .Text = _DR.Item(FieldBottomRight).ToString
                        .Appearance.Normal.FontSizeDelta = -2
                        .TextAlignment = TileItemContentAlignment.BottomRight
                    End With
                    _i.Elements.Add(_e)
                End If

                If Parameters.ShowPhotos Then
                    If FieldPhoto <> "" Then
                        Dim _e As New TileItemElement
                        With _e
                            .Image = DAL.GetImagefromByteArray(_DR.Item(FieldPhoto))
                            .ImageAlignment = TileItemContentAlignment.TopRight
                            .ImageScaleMode = TileItemImageScaleMode.ZoomInside
                            .ImageSize = New Size(128, 128)
                        End With
                        _i.Elements.Add(_e)
                    End If
                End If

                tg.Items.Add(_i)

            Next

        End If

    End Sub

    Private Sub PopulateChildTiles(ByVal SQL As String, ByVal Bookings As Boolean, ByVal Photos As Boolean)

        Dim _DT As DataTable = DAL.ReturnDataTable(SQL)
        If Not _DT Is Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _i As New TileBarItem
                Dim _Colour As Color = Business.Child.ReturnColour(_DR.Item("allergy_rating").ToString, _DR.Item("diet_restrict").ToString)

                With _i
                    .Name = "t_" + _DR.Item("ID").ToString
                    .Text = _DR.Item("name").ToString
                    .AppearanceItem.Normal.ForeColor = Color.Black
                    .AppearanceItem.Normal.FontSizeDelta = 4
                    .AppearanceItem.Normal.FontStyleDelta = FontStyle.Bold
                    .TextAlignment = TileItemContentAlignment.TopLeft
                    AddHandler _i.ItemClick, AddressOf TileClick
                    AddHandler _i.ItemDoubleClick, AddressOf TileDoubleClick
                End With

                If _Colour <> Color.Black Then
                    _i.AppearanceItem.Normal.BackColor = _Colour
                End If

                If Bookings Then

                    Dim _ml As New TileItemElement
                    With _ml
                        .Text = Business.Child.BuildBookingText(_DR.Item("tariff_name").ToString, _DR.Item("timeslots").ToString)
                        .Appearance.Normal.FontSizeDelta = -2
                        .TextAlignment = TileItemContentAlignment.MiddleLeft
                    End With
                    _i.Elements.Add(_ml)

                    Dim _br As New TileItemElement
                    With _br
                        .Text = Business.Child.BuildBookingTimes(_DR.Item("booking_from"), _DR.Item("booking_to"))
                        .Appearance.Normal.FontSizeDelta = -2
                        .TextAlignment = TileItemContentAlignment.BottomRight
                    End With
                    _i.Elements.Add(_br)

                End If

                Dim _bl As New TileItemElement
                With _bl
                    .Text = Business.Child.BuildAllergyText(_DR.Item("allergy_rating").ToString, _DR.Item("diet_restrict").ToString, False)
                    .Appearance.Normal.FontSizeDelta = -2
                    .TextAlignment = TileItemContentAlignment.BottomLeft
                End With
                _i.Elements.Add(_bl)

                If Photos Then

                    Dim _tl As New TileItemElement
                    With _tl
                        .Image = DAL.GetImagefromByteArray(_DR.Item("photo"))
                        .ImageAlignment = TileItemContentAlignment.TopRight
                        .ImageScaleMode = TileItemImageScaleMode.ZoomInside
                        .ImageSize = New Size(128, 128)
                    End With
                    _i.Elements.Add(_tl)

                End If

                tg.Items.Add(_i)

            Next

        End If

    End Sub

    Private Sub PopulateCheckedIn()
        Dim _SQL As String = Business.Child.ReturnCheckedInSQL(Me.RoomFilter, Parameters.ShowPhotos)
        PopulateChildTiles(_SQL, True, Parameters.ShowPhotos)
    End Sub

    Private Sub PopulateChildren()
        Dim _SQL As String = Business.Child.ReturnAllChildren(Me.RoomFilter, Parameters.ShowPhotos)
        PopulateChildTiles(_SQL, False, Parameters.ShowPhotos)
    End Sub

    Private Sub PopulateDueIn()

        Dim sqlFilter As String = ""
        Dim hideChildName As Boolean = Parameters.HideChildName

        If hideChildName Then
            sqlFilter = "select c.id, c.knownas as 'name',"
        Else
            sqlFilter = "select c.id, c.fullname as 'name',"
        End If

        sqlFilter &= "c.surname_forename, c.dob, c.allergy_rating, c.diet_restrict,"

        If Parameters.ShowPhotos Then
            sqlFilter &= " d.data as 'photo',"
        End If

        sqlFilter &= String.Concat(" b.booking_from, b.booking_to, b.tariff_name, b.timeslots",
                                   " from Bookings b",
                                   " left join Children c on c.ID = b.child_id")

        If Parameters.ShowPhotos Then
            sqlFilter &= " left join AppDocs d on d.id = c.photo"
        End If

        sqlFilter &= String.Concat(" where b.site_id = ", Parameters.SiteIDQuotes,
                                   " and booking_date = '", Parameters.TodaySQLDate, "'",
                                   " and booking_status = 'Booked'",
                                   " and c.id not in (select r.person_id from RegisterSummary r where r.date = b.booking_date and r.in_out = 'I')")

        If RoomFilter <> "" Then
            sqlFilter &= " and c.group_name = '" & RoomFilter & "'"
        End If

        sqlFilter &= " order by name"

        PopulateChildTiles(sqlFilter, True, Parameters.ShowPhotos)

    End Sub

    Private Sub PopulateCustom()

        For Each _P As Pair In m_Buttons
            Dim _i As New TileBarItem
            With _i
                .Name = "t_" + _P.Code
                .Text = _P.Text
                If .Text = "" Then .Text = _P.Code
                AddHandler _i.ItemClick, AddressOf TileClick
            End With
            tg.Items.Add(_i)
        Next

    End Sub

    Private Sub TileClick(sender As Object, e As TileItemEventArgs)

        If PopulateMode = EnumPopulateMode.FileSystem Then

            If e.Item.Name = "btnHome" Then
                PopulateFilesandFolders()
            Else
                If e.Item.Name.StartsWith("dir_") Then
                    m_CurrentFolder = e.Item.Name.Substring(4)
                    PopulateFolder()
                Else
                    If e.Item.Name.StartsWith("file_") Then
                        m_ReturnPair = New Pair(e.Item.Name.Substring(5), e.Item.Text)
                        Me.DialogResult = DialogResult.OK
                        Me.Close()
                    End If
                End If
            End If

        Else

            If MultiSelect Then
                If e.Item.Checked Then
                    e.Item.Checked = False
                Else
                    e.Item.Checked = True
                End If
            Else
                m_ReturnPair = New Pair(e.Item.Name.Substring(2), e.Item.Text)
                Me.DialogResult = DialogResult.OK
                Me.Close()
            End If

        End If

    End Sub

    Private Sub TileDoubleClick(sender As Object, e As TileItemEventArgs)

        If PopulateMode = EnumPopulateMode.ChildrenDueIn Then
            Business.Child.UpdateTodayNotes(New Guid(e.Item.Name.Substring(2)))
        End If

    End Sub

    Private Sub Accept()

        Dim _SelectedCount As Integer = 0
        m_ReturnPairs = New List(Of Pair)

        For Each _i As TileBarItem In tg.Items
            If _i.Checked Then
                Dim _ID As String = _i.Name.Substring(2)
                Dim _Name As String = _i.Elements(0).Text
                Dim _P As New Pair(_ID, _Name)
                m_ReturnPairs.Add(_P)
                _SelectedCount += 1
            End If
        Next

        If _SelectedCount > 0 Then
            Me.DialogResult = DialogResult.OK
            Me.Close()
        Else
            m_ReturnPairs = Nothing
        End If

    End Sub

    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click
        Accept()
    End Sub
End Class
