﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGrid
    Inherits NurseryTouchscreen.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnClose = New DevExpress.XtraEditors.SimpleButton()
        Me.TouchGrid1 = New NurseryTouchscreen.TouchGrid()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Location = New System.Drawing.Point(230, 112)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(162, 57)
        Me.btnClose.TabIndex = 36
        Me.btnClose.Text = "Close"
        '
        'TouchGrid1
        '
        Me.TouchGrid1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TouchGrid1.HideFirstColumn = False
        Me.TouchGrid1.Location = New System.Drawing.Point(12, 12)
        Me.TouchGrid1.Name = "TouchGrid1"
        Me.TouchGrid1.Size = New System.Drawing.Size(380, 90)
        Me.TouchGrid1.TabIndex = 35
        '
        'frmGrid
        '
        Me.ClientSize = New System.Drawing.Size(404, 177)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.TouchGrid1)
        Me.Name = "frmGrid"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Controls.SetChildIndex(Me.TouchGrid1, 0)
        Me.Controls.SetChildIndex(Me.btnClose, 0)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TouchGrid1 As NurseryTouchscreen.TouchGrid

End Class
