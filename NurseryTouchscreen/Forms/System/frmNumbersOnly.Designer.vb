﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNumbersOnly
    Inherits NurseryTouchscreen.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmNumbersOnly))
        Me.txtText = New System.Windows.Forms.TextBox()
        Me.btnClear = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAccept = New DevExpress.XtraEditors.SimpleButton()
        Me.btn0 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn5 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn1 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn6 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn2 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn7 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn3 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn8 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn4 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn9 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnDOT = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMinus = New DevExpress.XtraEditors.SimpleButton()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'txtText
        '
        Me.txtText.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtText.Location = New System.Drawing.Point(12, 16)
        Me.txtText.Multiline = True
        Me.txtText.Name = "txtText"
        Me.txtText.Size = New System.Drawing.Size(1000, 58)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtText, OptionsSpelling1)
        Me.txtText.TabIndex = 0
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(565, 466)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(145, 57)
        Me.btnClear.TabIndex = 10
        Me.btnClear.Text = "Clear"
        '
        'btnCancel
        '
        Me.btnCancel.Image = CType(resources.GetObject("btnCancel.Image"), System.Drawing.Image)
        Me.btnCancel.Location = New System.Drawing.Point(867, 466)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(145, 57)
        Me.btnCancel.TabIndex = 14
        Me.btnCancel.Text = "Cancel"
        '
        'btnAccept
        '
        Me.btnAccept.Image = CType(resources.GetObject("btnAccept.Image"), System.Drawing.Image)
        Me.btnAccept.Location = New System.Drawing.Point(716, 466)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(145, 57)
        Me.btnAccept.TabIndex = 13
        Me.btnAccept.Text = "Accept"
        '
        'btn0
        '
        Me.btn0.Location = New System.Drawing.Point(421, 356)
        Me.btn0.Name = "btn0"
        Me.btn0.Size = New System.Drawing.Size(183, 75)
        Me.btn0.TabIndex = 11
        Me.btn0.Text = "0"
        '
        'btn5
        '
        Me.btn5.Location = New System.Drawing.Point(421, 194)
        Me.btn5.Name = "btn5"
        Me.btn5.Size = New System.Drawing.Size(183, 75)
        Me.btn5.TabIndex = 5
        Me.btn5.Text = "5"
        '
        'btn1
        '
        Me.btn1.Location = New System.Drawing.Point(232, 275)
        Me.btn1.Name = "btn1"
        Me.btn1.Size = New System.Drawing.Size(183, 75)
        Me.btn1.TabIndex = 7
        Me.btn1.Text = "1"
        '
        'btn6
        '
        Me.btn6.Location = New System.Drawing.Point(610, 194)
        Me.btn6.Name = "btn6"
        Me.btn6.Size = New System.Drawing.Size(183, 75)
        Me.btn6.TabIndex = 6
        Me.btn6.Text = "6"
        '
        'btn2
        '
        Me.btn2.Location = New System.Drawing.Point(421, 275)
        Me.btn2.Name = "btn2"
        Me.btn2.Size = New System.Drawing.Size(183, 75)
        Me.btn2.TabIndex = 8
        Me.btn2.Text = "2"
        '
        'btn7
        '
        Me.btn7.Location = New System.Drawing.Point(232, 113)
        Me.btn7.Name = "btn7"
        Me.btn7.Size = New System.Drawing.Size(183, 75)
        Me.btn7.TabIndex = 1
        Me.btn7.Text = "7"
        '
        'btn3
        '
        Me.btn3.Location = New System.Drawing.Point(610, 275)
        Me.btn3.Name = "btn3"
        Me.btn3.Size = New System.Drawing.Size(183, 75)
        Me.btn3.TabIndex = 9
        Me.btn3.Text = "3"
        '
        'btn8
        '
        Me.btn8.Location = New System.Drawing.Point(421, 113)
        Me.btn8.Name = "btn8"
        Me.btn8.Size = New System.Drawing.Size(183, 75)
        Me.btn8.TabIndex = 2
        Me.btn8.Text = "8"
        '
        'btn4
        '
        Me.btn4.Location = New System.Drawing.Point(232, 194)
        Me.btn4.Name = "btn4"
        Me.btn4.Size = New System.Drawing.Size(183, 75)
        Me.btn4.TabIndex = 4
        Me.btn4.Text = "4"
        '
        'btn9
        '
        Me.btn9.Location = New System.Drawing.Point(610, 113)
        Me.btn9.Name = "btn9"
        Me.btn9.Size = New System.Drawing.Size(183, 75)
        Me.btn9.TabIndex = 3
        Me.btn9.Text = "9"
        '
        'btnDOT
        '
        Me.btnDOT.Location = New System.Drawing.Point(610, 356)
        Me.btnDOT.Name = "btnDOT"
        Me.btnDOT.Size = New System.Drawing.Size(183, 75)
        Me.btnDOT.TabIndex = 12
        Me.btnDOT.Text = "."
        '
        'btnMinus
        '
        Me.btnMinus.Location = New System.Drawing.Point(232, 356)
        Me.btnMinus.Name = "btnMinus"
        Me.btnMinus.Size = New System.Drawing.Size(183, 75)
        Me.btnMinus.TabIndex = 78
        Me.btnMinus.Text = "-"
        '
        'frmNumbersOnly
        '
        Me.ClientSize = New System.Drawing.Size(1024, 538)
        Me.Controls.Add(Me.btnMinus)
        Me.Controls.Add(Me.btnDOT)
        Me.Controls.Add(Me.btn9)
        Me.Controls.Add(Me.btn4)
        Me.Controls.Add(Me.btn8)
        Me.Controls.Add(Me.btn3)
        Me.Controls.Add(Me.btn7)
        Me.Controls.Add(Me.btn2)
        Me.Controls.Add(Me.btn6)
        Me.Controls.Add(Me.btn1)
        Me.Controls.Add(Me.btn5)
        Me.Controls.Add(Me.btn0)
        Me.Controls.Add(Me.txtText)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnAccept)
        Me.Name = "frmNumbersOnly"
        Me.Controls.SetChildIndex(Me.btnAccept, 0)
        Me.Controls.SetChildIndex(Me.btnCancel, 0)
        Me.Controls.SetChildIndex(Me.btnClear, 0)
        Me.Controls.SetChildIndex(Me.txtText, 0)
        Me.Controls.SetChildIndex(Me.btn0, 0)
        Me.Controls.SetChildIndex(Me.btn5, 0)
        Me.Controls.SetChildIndex(Me.btn1, 0)
        Me.Controls.SetChildIndex(Me.btn6, 0)
        Me.Controls.SetChildIndex(Me.btn2, 0)
        Me.Controls.SetChildIndex(Me.btn7, 0)
        Me.Controls.SetChildIndex(Me.btn3, 0)
        Me.Controls.SetChildIndex(Me.btn8, 0)
        Me.Controls.SetChildIndex(Me.btn4, 0)
        Me.Controls.SetChildIndex(Me.btn9, 0)
        Me.Controls.SetChildIndex(Me.btnDOT, 0)
        Me.Controls.SetChildIndex(Me.btnMinus, 0)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtText As System.Windows.Forms.TextBox
    Friend WithEvents btnClear As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAccept As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn0 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn6 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn7 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn8 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn9 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnDOT As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMinus As DevExpress.XtraEditors.SimpleButton
End Class
