﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTimeEntry
    Inherits NurseryTouchscreen.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTimeEntry))
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.btnClear = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAccept = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnM55 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnM50 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnM45 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnM40 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnM35 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnM30 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnM25 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnM20 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnM15 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnM10 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnM5 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnM0 = New DevExpress.XtraEditors.SimpleButton()
        Me.txtText = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnH11 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnH8 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnH5 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnH2 = New DevExpress.XtraEditors.SimpleButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnH23 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnH22 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnH21 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnH20 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnH19 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnH18 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnH17 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnH16 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnH15 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnH0 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnH10 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnH9 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnH7 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnH6 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnH4 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnH14 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnH13 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnH12 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnH3 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnH1 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnNA = New DevExpress.XtraEditors.SimpleButton()
        Me.btnNow = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(12, 466)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(183, 57)
        Me.btnClear.TabIndex = 108
        Me.btnClear.Text = "Clear"
        '
        'btnCancel
        '
        Me.btnCancel.Image = CType(resources.GetObject("btnCancel.Image"), System.Drawing.Image)
        Me.btnCancel.Location = New System.Drawing.Point(1027, 466)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(145, 57)
        Me.btnCancel.TabIndex = 106
        Me.btnCancel.Text = "Cancel"
        '
        'btnAccept
        '
        Me.btnAccept.Image = CType(resources.GetObject("btnAccept.Image"), System.Drawing.Image)
        Me.btnAccept.Location = New System.Drawing.Point(876, 466)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(145, 57)
        Me.btnAccept.TabIndex = 105
        Me.btnAccept.Text = "Accept"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnM55)
        Me.GroupBox1.Controls.Add(Me.btnM50)
        Me.GroupBox1.Controls.Add(Me.btnM45)
        Me.GroupBox1.Controls.Add(Me.btnM40)
        Me.GroupBox1.Controls.Add(Me.btnM35)
        Me.GroupBox1.Controls.Add(Me.btnM30)
        Me.GroupBox1.Controls.Add(Me.btnM25)
        Me.GroupBox1.Controls.Add(Me.btnM20)
        Me.GroupBox1.Controls.Add(Me.btnM15)
        Me.GroupBox1.Controls.Add(Me.btnM10)
        Me.GroupBox1.Controls.Add(Me.btnM5)
        Me.GroupBox1.Controls.Add(Me.btnM0)
        Me.GroupBox1.Location = New System.Drawing.Point(802, 100)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(370, 334)
        Me.GroupBox1.TabIndex = 109
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Minutes"
        '
        'btnM55
        '
        Me.btnM55.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnM55.Location = New System.Drawing.Point(247, 257)
        Me.btnM55.Name = "btnM55"
        Me.btnM55.Size = New System.Drawing.Size(110, 60)
        Me.btnM55.TabIndex = 25
        Me.btnM55.Text = ":55"
        '
        'btnM50
        '
        Me.btnM50.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnM50.Location = New System.Drawing.Point(131, 257)
        Me.btnM50.Name = "btnM50"
        Me.btnM50.Size = New System.Drawing.Size(110, 60)
        Me.btnM50.TabIndex = 24
        Me.btnM50.Text = ":50"
        '
        'btnM45
        '
        Me.btnM45.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnM45.Location = New System.Drawing.Point(15, 257)
        Me.btnM45.Name = "btnM45"
        Me.btnM45.Size = New System.Drawing.Size(110, 60)
        Me.btnM45.TabIndex = 23
        Me.btnM45.Text = ":45"
        '
        'btnM40
        '
        Me.btnM40.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnM40.Location = New System.Drawing.Point(247, 191)
        Me.btnM40.Name = "btnM40"
        Me.btnM40.Size = New System.Drawing.Size(110, 60)
        Me.btnM40.TabIndex = 22
        Me.btnM40.Text = ":40"
        '
        'btnM35
        '
        Me.btnM35.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnM35.Location = New System.Drawing.Point(131, 191)
        Me.btnM35.Name = "btnM35"
        Me.btnM35.Size = New System.Drawing.Size(110, 60)
        Me.btnM35.TabIndex = 21
        Me.btnM35.Text = ":35"
        '
        'btnM30
        '
        Me.btnM30.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnM30.Location = New System.Drawing.Point(15, 191)
        Me.btnM30.Name = "btnM30"
        Me.btnM30.Size = New System.Drawing.Size(110, 60)
        Me.btnM30.TabIndex = 20
        Me.btnM30.Text = ":30"
        '
        'btnM25
        '
        Me.btnM25.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnM25.Location = New System.Drawing.Point(247, 125)
        Me.btnM25.Name = "btnM25"
        Me.btnM25.Size = New System.Drawing.Size(110, 60)
        Me.btnM25.TabIndex = 19
        Me.btnM25.Text = ":25"
        '
        'btnM20
        '
        Me.btnM20.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnM20.Location = New System.Drawing.Point(131, 125)
        Me.btnM20.Name = "btnM20"
        Me.btnM20.Size = New System.Drawing.Size(110, 60)
        Me.btnM20.TabIndex = 18
        Me.btnM20.Text = ":20"
        '
        'btnM15
        '
        Me.btnM15.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnM15.Location = New System.Drawing.Point(15, 125)
        Me.btnM15.Name = "btnM15"
        Me.btnM15.Size = New System.Drawing.Size(110, 60)
        Me.btnM15.TabIndex = 17
        Me.btnM15.Text = ":15"
        '
        'btnM10
        '
        Me.btnM10.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnM10.Location = New System.Drawing.Point(247, 59)
        Me.btnM10.Name = "btnM10"
        Me.btnM10.Size = New System.Drawing.Size(110, 60)
        Me.btnM10.TabIndex = 10
        Me.btnM10.Text = ":10"
        '
        'btnM5
        '
        Me.btnM5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnM5.Location = New System.Drawing.Point(131, 59)
        Me.btnM5.Name = "btnM5"
        Me.btnM5.Size = New System.Drawing.Size(110, 60)
        Me.btnM5.TabIndex = 9
        Me.btnM5.Text = ":05"
        '
        'btnM0
        '
        Me.btnM0.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnM0.Location = New System.Drawing.Point(15, 59)
        Me.btnM0.Name = "btnM0"
        Me.btnM0.Size = New System.Drawing.Size(110, 60)
        Me.btnM0.TabIndex = 8
        Me.btnM0.Text = ":00"
        '
        'txtText
        '
        Me.txtText.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtText.Location = New System.Drawing.Point(12, 12)
        Me.txtText.Multiline = True
        Me.txtText.Name = "txtText"
        Me.txtText.Size = New System.Drawing.Size(1160, 61)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtText, OptionsSpelling1)
        Me.txtText.TabIndex = 110
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnH11)
        Me.GroupBox2.Controls.Add(Me.btnH8)
        Me.GroupBox2.Controls.Add(Me.btnH5)
        Me.GroupBox2.Controls.Add(Me.btnH2)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.btnH23)
        Me.GroupBox2.Controls.Add(Me.btnH22)
        Me.GroupBox2.Controls.Add(Me.btnH21)
        Me.GroupBox2.Controls.Add(Me.btnH20)
        Me.GroupBox2.Controls.Add(Me.btnH19)
        Me.GroupBox2.Controls.Add(Me.btnH18)
        Me.GroupBox2.Controls.Add(Me.btnH17)
        Me.GroupBox2.Controls.Add(Me.btnH16)
        Me.GroupBox2.Controls.Add(Me.btnH15)
        Me.GroupBox2.Controls.Add(Me.btnH0)
        Me.GroupBox2.Controls.Add(Me.btnH10)
        Me.GroupBox2.Controls.Add(Me.btnH9)
        Me.GroupBox2.Controls.Add(Me.btnH7)
        Me.GroupBox2.Controls.Add(Me.btnH6)
        Me.GroupBox2.Controls.Add(Me.btnH4)
        Me.GroupBox2.Controls.Add(Me.btnH14)
        Me.GroupBox2.Controls.Add(Me.btnH13)
        Me.GroupBox2.Controls.Add(Me.btnH12)
        Me.GroupBox2.Controls.Add(Me.btnH3)
        Me.GroupBox2.Controls.Add(Me.btnH1)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 100)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(753, 334)
        Me.GroupBox2.TabIndex = 110
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Hour"
        '
        'btnH11
        '
        Me.btnH11.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnH11.Location = New System.Drawing.Point(246, 257)
        Me.btnH11.Name = "btnH11"
        Me.btnH11.Size = New System.Drawing.Size(110, 60)
        Me.btnH11.TabIndex = 31
        Me.btnH11.Text = "11 am"
        '
        'btnH8
        '
        Me.btnH8.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnH8.Location = New System.Drawing.Point(246, 191)
        Me.btnH8.Name = "btnH8"
        Me.btnH8.Size = New System.Drawing.Size(110, 60)
        Me.btnH8.TabIndex = 30
        Me.btnH8.Text = "8 am"
        '
        'btnH5
        '
        Me.btnH5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnH5.Location = New System.Drawing.Point(246, 125)
        Me.btnH5.Name = "btnH5"
        Me.btnH5.Size = New System.Drawing.Size(110, 60)
        Me.btnH5.TabIndex = 29
        Me.btnH5.Text = "5 am"
        '
        'btnH2
        '
        Me.btnH2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnH2.Location = New System.Drawing.Point(246, 59)
        Me.btnH2.Name = "btnH2"
        Me.btnH2.Size = New System.Drawing.Size(110, 60)
        Me.btnH2.TabIndex = 28
        Me.btnH2.Text = "2 am"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(395, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(345, 24)
        Me.Label1.TabIndex = 27
        Me.Label1.Text = "PM (Day / Evening)"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(14, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(342, 24)
        Me.Label2.TabIndex = 26
        Me.Label2.Text = "AM (Night / Morning)"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnH23
        '
        Me.btnH23.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnH23.Location = New System.Drawing.Point(630, 257)
        Me.btnH23.Name = "btnH23"
        Me.btnH23.Size = New System.Drawing.Size(110, 60)
        Me.btnH23.TabIndex = 25
        Me.btnH23.Text = "11 pm"
        '
        'btnH22
        '
        Me.btnH22.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnH22.Location = New System.Drawing.Point(514, 257)
        Me.btnH22.Name = "btnH22"
        Me.btnH22.Size = New System.Drawing.Size(110, 60)
        Me.btnH22.TabIndex = 24
        Me.btnH22.Text = "10 pm"
        '
        'btnH21
        '
        Me.btnH21.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnH21.Location = New System.Drawing.Point(398, 257)
        Me.btnH21.Name = "btnH21"
        Me.btnH21.Size = New System.Drawing.Size(110, 60)
        Me.btnH21.TabIndex = 23
        Me.btnH21.Text = "9 pm"
        '
        'btnH20
        '
        Me.btnH20.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnH20.Location = New System.Drawing.Point(630, 191)
        Me.btnH20.Name = "btnH20"
        Me.btnH20.Size = New System.Drawing.Size(110, 60)
        Me.btnH20.TabIndex = 22
        Me.btnH20.Text = "8 pm"
        '
        'btnH19
        '
        Me.btnH19.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnH19.Location = New System.Drawing.Point(514, 191)
        Me.btnH19.Name = "btnH19"
        Me.btnH19.Size = New System.Drawing.Size(110, 60)
        Me.btnH19.TabIndex = 21
        Me.btnH19.Text = "7 pm"
        '
        'btnH18
        '
        Me.btnH18.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnH18.Location = New System.Drawing.Point(398, 191)
        Me.btnH18.Name = "btnH18"
        Me.btnH18.Size = New System.Drawing.Size(110, 60)
        Me.btnH18.TabIndex = 20
        Me.btnH18.Text = "6 pm"
        '
        'btnH17
        '
        Me.btnH17.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnH17.Location = New System.Drawing.Point(630, 125)
        Me.btnH17.Name = "btnH17"
        Me.btnH17.Size = New System.Drawing.Size(110, 60)
        Me.btnH17.TabIndex = 19
        Me.btnH17.Text = "5 pm"
        '
        'btnH16
        '
        Me.btnH16.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnH16.Location = New System.Drawing.Point(514, 125)
        Me.btnH16.Name = "btnH16"
        Me.btnH16.Size = New System.Drawing.Size(110, 60)
        Me.btnH16.TabIndex = 18
        Me.btnH16.Text = "4 pm"
        '
        'btnH15
        '
        Me.btnH15.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnH15.Location = New System.Drawing.Point(398, 125)
        Me.btnH15.Name = "btnH15"
        Me.btnH15.Size = New System.Drawing.Size(110, 60)
        Me.btnH15.TabIndex = 17
        Me.btnH15.Text = "3 pm"
        '
        'btnH0
        '
        Me.btnH0.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnH0.Location = New System.Drawing.Point(14, 59)
        Me.btnH0.Name = "btnH0"
        Me.btnH0.Size = New System.Drawing.Size(110, 60)
        Me.btnH0.TabIndex = 16
        Me.btnH0.Text = "Midnight"
        '
        'btnH10
        '
        Me.btnH10.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnH10.Location = New System.Drawing.Point(130, 257)
        Me.btnH10.Name = "btnH10"
        Me.btnH10.Size = New System.Drawing.Size(110, 60)
        Me.btnH10.TabIndex = 15
        Me.btnH10.Text = "10 am"
        '
        'btnH9
        '
        Me.btnH9.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnH9.Location = New System.Drawing.Point(14, 257)
        Me.btnH9.Name = "btnH9"
        Me.btnH9.Size = New System.Drawing.Size(110, 60)
        Me.btnH9.TabIndex = 14
        Me.btnH9.Text = "9 am"
        '
        'btnH7
        '
        Me.btnH7.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnH7.Location = New System.Drawing.Point(130, 191)
        Me.btnH7.Name = "btnH7"
        Me.btnH7.Size = New System.Drawing.Size(110, 60)
        Me.btnH7.TabIndex = 13
        Me.btnH7.Text = "7 am"
        '
        'btnH6
        '
        Me.btnH6.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnH6.Location = New System.Drawing.Point(14, 191)
        Me.btnH6.Name = "btnH6"
        Me.btnH6.Size = New System.Drawing.Size(110, 60)
        Me.btnH6.TabIndex = 12
        Me.btnH6.Text = "6 am"
        '
        'btnH4
        '
        Me.btnH4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnH4.Location = New System.Drawing.Point(130, 125)
        Me.btnH4.Name = "btnH4"
        Me.btnH4.Size = New System.Drawing.Size(110, 60)
        Me.btnH4.TabIndex = 11
        Me.btnH4.Text = "4 am"
        '
        'btnH14
        '
        Me.btnH14.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnH14.Location = New System.Drawing.Point(630, 59)
        Me.btnH14.Name = "btnH14"
        Me.btnH14.Size = New System.Drawing.Size(110, 60)
        Me.btnH14.TabIndex = 10
        Me.btnH14.Text = "2 pm"
        '
        'btnH13
        '
        Me.btnH13.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnH13.Location = New System.Drawing.Point(514, 59)
        Me.btnH13.Name = "btnH13"
        Me.btnH13.Size = New System.Drawing.Size(110, 60)
        Me.btnH13.TabIndex = 9
        Me.btnH13.Text = "1 pm"
        '
        'btnH12
        '
        Me.btnH12.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnH12.Location = New System.Drawing.Point(398, 59)
        Me.btnH12.Name = "btnH12"
        Me.btnH12.Size = New System.Drawing.Size(110, 60)
        Me.btnH12.TabIndex = 8
        Me.btnH12.Text = "Midday"
        '
        'btnH3
        '
        Me.btnH3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnH3.Location = New System.Drawing.Point(14, 125)
        Me.btnH3.Name = "btnH3"
        Me.btnH3.Size = New System.Drawing.Size(110, 60)
        Me.btnH3.TabIndex = 7
        Me.btnH3.Text = "3 am"
        '
        'btnH1
        '
        Me.btnH1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnH1.Location = New System.Drawing.Point(130, 59)
        Me.btnH1.Name = "btnH1"
        Me.btnH1.Size = New System.Drawing.Size(110, 60)
        Me.btnH1.TabIndex = 0
        Me.btnH1.Text = "1 am"
        '
        'btnNA
        '
        Me.btnNA.Location = New System.Drawing.Point(201, 466)
        Me.btnNA.Name = "btnNA"
        Me.btnNA.Size = New System.Drawing.Size(183, 57)
        Me.btnNA.TabIndex = 111
        Me.btnNA.Text = "Not Applicable"
        '
        'btnNow
        '
        Me.btnNow.Image = Global.NurseryTouchscreen.My.Resources.Resources.alarm_32
        Me.btnNow.Location = New System.Drawing.Point(390, 466)
        Me.btnNow.Name = "btnNow"
        Me.btnNow.Size = New System.Drawing.Size(183, 57)
        Me.btnNow.TabIndex = 112
        Me.btnNow.Text = "Now"
        '
        'frmTimeEntry
        '
        Me.ClientSize = New System.Drawing.Size(1184, 538)
        Me.Controls.Add(Me.btnNow)
        Me.Controls.Add(Me.btnNA)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.txtText)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnAccept)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTimeEntry"
        Me.Controls.SetChildIndex(Me.btnAccept, 0)
        Me.Controls.SetChildIndex(Me.btnCancel, 0)
        Me.Controls.SetChildIndex(Me.btnClear, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.txtText, 0)
        Me.Controls.SetChildIndex(Me.GroupBox2, 0)
        Me.Controls.SetChildIndex(Me.btnNA, 0)
        Me.Controls.SetChildIndex(Me.btnNow, 0)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnClear As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAccept As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnM55 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnM50 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnM45 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnM40 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnM35 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnM30 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnM25 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnM20 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnM15 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnM10 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnM5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnM0 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtText As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnH11 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnH8 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnH5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnH2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnH23 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnH22 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnH21 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnH20 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnH19 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnH18 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnH17 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnH16 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnH15 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnH0 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnH10 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnH9 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnH7 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnH6 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnH4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnH14 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnH13 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnH12 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnH3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnH1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnNA As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnNow As DevExpress.XtraEditors.SimpleButton

End Class
