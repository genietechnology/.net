﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCompliance
    Inherits NurseryTouchscreen.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCompliance))
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnRiskAssessments = New DevExpress.XtraEditors.SimpleButton()
        Me.btnExit = New DevExpress.XtraEditors.SimpleButton()
        Me.btnChecklists = New DevExpress.XtraEditors.SimpleButton()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Image = Global.NurseryTouchscreen.My.Resources.Resources.projects_64
        Me.SimpleButton1.Location = New System.Drawing.Point(519, 119)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(490, 90)
        Me.SimpleButton1.TabIndex = 83
        Me.SimpleButton1.Text = "COSHH Register"
        '
        'btnRiskAssessments
        '
        Me.btnRiskAssessments.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRiskAssessments.Appearance.Options.UseFont = True
        Me.btnRiskAssessments.Image = Global.NurseryTouchscreen.My.Resources.Resources.warning_64
        Me.btnRiskAssessments.Location = New System.Drawing.Point(519, 12)
        Me.btnRiskAssessments.Name = "btnRiskAssessments"
        Me.btnRiskAssessments.Size = New System.Drawing.Size(490, 90)
        Me.btnRiskAssessments.TabIndex = 79
        Me.btnRiskAssessments.Text = "Risk Assessments"
        '
        'btnExit
        '
        Me.btnExit.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Appearance.Options.UseFont = True
        Me.btnExit.Image = CType(resources.GetObject("btnExit.Image"), System.Drawing.Image)
        Me.btnExit.Location = New System.Drawing.Point(12, 119)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(490, 90)
        Me.btnExit.TabIndex = 82
        Me.btnExit.Text = "Back"
        '
        'btnChecklists
        '
        Me.btnChecklists.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnChecklists.Appearance.Options.UseFont = True
        Me.btnChecklists.Image = Global.NurseryTouchscreen.My.Resources.Resources.Test_paper_64
        Me.btnChecklists.Location = New System.Drawing.Point(12, 12)
        Me.btnChecklists.Name = "btnChecklists"
        Me.btnChecklists.Size = New System.Drawing.Size(490, 90)
        Me.btnChecklists.TabIndex = 78
        Me.btnChecklists.Text = "CheckLists"
        '
        'frmCompliance
        '
        Me.ClientSize = New System.Drawing.Size(1023, 221)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.btnRiskAssessments)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnChecklists)
        Me.Name = "frmCompliance"
        Me.Text = "Compliance Menu"
        Me.Controls.SetChildIndex(Me.btnChecklists, 0)
        Me.Controls.SetChildIndex(Me.btnExit, 0)
        Me.Controls.SetChildIndex(Me.btnRiskAssessments, 0)
        Me.Controls.SetChildIndex(Me.SimpleButton1, 0)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnRiskAssessments As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnExit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnChecklists As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
End Class
