﻿Option Strict On

Imports DevExpress.XtraEditors
Imports DevExpress.XtraBars.Navigation

Public Class frmScreenSaver

    Private m_Capacity As Decimal = 0
    Private m_StaffRatio As Decimal = 0
    Private m_TodayID As String = ""
    Private m_SQLToday As String = ""
    Private m_SQLRoom As String = ""
    Private m_SQLThisWeek As String = ""

    Private m_NappyOverDue As Long = 240   '4 hours   
    Private m_NappyWarning As Long = 180   '3 hours

    Private Enum EnumDisplayMode
        ZeroGreenPositiveRed
        ZeroRedPositiveGreen
    End Enum

    Private Enum EnumBar
        ChildrenCheckedIn
        StaffCheckedIn
        Allergies
        Medication
    End Enum

    Private Sub frmScreenSaver_Load(sender As Object, e As EventArgs) Handles Me.Load

        timMinute.Interval = 60 * 1000
        btnExit.Hide()

        Me.Text = ""
        Me.Text += "Nursery Genie Version " + Application.ProductVersion
        Me.Text += " [" + My.Computer.Name + "]"
        Me.Text += WebServiceStatus()

        LoadLogo()

        dashChildren.LabelBottom = Parameters.DefaultRoom
        dashStaffRatio.LabelBottom = Parameters.DefaultRoom
        dashStaff.LabelBottom = Parameters.DefaultRoom
        dashStaffReqd.LabelBottom = Parameters.DefaultRoom
        dashDueIn.LabelBottom = Parameters.DefaultRoom
        dashSpaces.LabelBottom = Parameters.DefaultRoom
        dashAccidents.LabelBottom = Parameters.DefaultRoom
        dashLastChange.LabelBottom = Parameters.DefaultRoom
        dashMedRoom.LabelBottom = Parameters.DefaultRoom

        SetVariables()

        DisplayScreen()

        timMinute.Start()

    End Sub

    Private Sub LoadLogo()

        If Parameters.Appearance.ScreenSaverColour <> "" Then

            If Parameters.Appearance.ScreenSaverColour.ToUpper = "THEME" Then
                'leave the backcolour and use the theme
            Else

                Dim _rgb As String() = Parameters.Appearance.ScreenSaverColour.Replace(" ", "").Split(CChar(","))
                If _rgb.Count = 2 Then
                    Dim _r As Integer = CInt(_rgb(0))
                    Dim _g As Integer = CInt(_rgb(1))
                    Dim _b As Integer = CInt(_rgb(2))
                    Me.Appearance.BackColor = System.Drawing.Color.FromArgb(_r, _g, _b)
                End If

            End If

        Else
            Me.Appearance.BackColor = Color.White
        End If

        Dim _PNG As String = Application.StartupPath + "\logo.png"
        Dim _JPG As String = Application.StartupPath + "\logo.jpg"

        If IO.File.Exists(_PNG) Then
            picLogo.Load(_PNG)
        Else
            If IO.File.Exists(_JPG) Then
                picLogo.Load(_JPG)
            End If
        End If

    End Sub

    Private Sub SetVariables()

        m_TodayID = "'" + Parameters.TodayID + "'"
        m_SQLToday = "'" + Format(Today, "yyyy-MM-dd") + "'"

        Dim _WeekStarts As Date = SharedModule.NearestDate(Today, DayOfWeek.Monday, SharedModule.EnumDirection.Backwards)
        Dim _WeekEnds As Date = _WeekStarts.AddDays(6)
        m_SQLThisWeek = SharedModule.SQLDateWithQuotes(_WeekStarts) + " and " + SharedModule.SQLDateWithQuotes(_WeekEnds)

        Dim _SQL As String = ""

        If Parameters.DefaultRoom = "" Then
            m_SQLRoom = ""
        Else

            m_SQLRoom = "'" + Parameters.DefaultRoom + "'"

            _SQL += "select sum(capacity) as 'capacity', avg(ratio) as 'ratio' from SiteRooms r"
            _SQL += " left join SiteRoomRatios rr on rr.room_id = r.ID"
            _SQL += " where r.site_id = " + Parameters.SiteIDQuotes

            If m_SQLRoom <> "" Then _SQL += " and r.room_name = " + m_SQLRoom

            Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
            If _DR IsNot Nothing Then
                m_Capacity = CDec(_DR.Item("capacity"))
                m_StaffRatio = CDec(_DR.Item("ratio"))
                _DR = Nothing
            End If

        End If

    End Sub

    Private Function WebServiceStatus() As String
        If Parameters.UseWebServices Then Return " Using Service on " + Parameters.ServiceIP
        Return ""
    End Function

    Private Sub DisplayScreen()
        DisplayTiles()
        DisplayLists()
    End Sub

    Private Sub DisplayTiles()

        Me.Cursor = Cursors.WaitCursor
        Application.DoEvents()

        Dim _SQL As String = ""
        Dim _Value As Decimal = 0

        Dim _ChildHeadcount As Decimal = 0
        Dim _StaffHeadcount As Decimal = 0

        '*************************************************************************************************************************************************
        'child headcount

        _ChildHeadcount = SharedModule.ReturnHeadCount(SharedModule.EnumPersonType.Child, m_SQLRoom)
        DisplayLabel(dashChildren, EnumDisplayMode.ZeroRedPositiveGreen, _ChildHeadcount)

        '*************************************************************************************************************************************************
        'staff headcount
        _StaffHeadcount = SharedModule.ReturnHeadCount(SharedModule.EnumPersonType.Staff, m_SQLRoom)
        DisplayLabel(dashStaff, EnumDisplayMode.ZeroRedPositiveGreen, _StaffHeadcount)

        '*************************************************************************************************************************************************
        'staff required
        Dim _StaffRequired As Decimal = 0
        If m_StaffRatio > 0 Then

            Dim _StaffRequiredDecimal As Decimal = _ChildHeadcount / m_StaffRatio
            _StaffRequired = Math.Ceiling(_StaffRequiredDecimal)

            Dim _Diff As Decimal = _StaffHeadcount - _StaffRequired
            If _Diff < 0 Then
                dashStaffReqd.SetValue(_StaffRequired, DashboardLabel.EnumMiddleFormat.WholeNumber, DashboardLabel.EnumColourCode.Red)
            Else
                dashStaffReqd.SetValue(_StaffRequired, DashboardLabel.EnumMiddleFormat.WholeNumber, DashboardLabel.EnumColourCode.Green)
            End If

        Else
            dashStaffReqd.LabelMiddle = "N/A"
            dashStaffReqd.Colour = DashboardLabel.EnumColourCode.Green
        End If

        '*************************************************************************************************************************************************
        'staff utilisation
        If m_StaffRatio > 0 Then

            If _ChildHeadcount > 0 And _StaffHeadcount > 0 Then

                Dim _ChildMax As Decimal = _StaffHeadcount * m_StaffRatio
                Dim _Utilisation As Decimal = _ChildHeadcount / _ChildMax * 100
                Dim _ChildPcnt As Decimal = 1 / _ChildMax * 100
                Dim _SpacePcnt As Decimal = 100 - _Utilisation

                If _Utilisation > 100 Then
                    dashStaffRatio.SetValue(_Utilisation, DashboardLabel.EnumMiddleFormat.Percentage, DashboardLabel.EnumColourCode.Red)
                Else
                    If _SpacePcnt > _ChildPcnt Then
                        dashStaffRatio.SetValue(_Utilisation, DashboardLabel.EnumMiddleFormat.Percentage, DashboardLabel.EnumColourCode.Green)
                    Else
                        dashStaffRatio.SetValue(_Utilisation, DashboardLabel.EnumMiddleFormat.Percentage, DashboardLabel.EnumColourCode.Amber)
                    End If
                End If

            Else
                dashStaffRatio.SetValue(0, DashboardLabel.EnumMiddleFormat.Percentage, DashboardLabel.EnumColourCode.Green)
            End If

        Else
            dashStaffRatio.LabelMiddle = "N/A"
            dashStaffRatio.Colour = DashboardLabel.EnumColourCode.Green
        End If

        '*************************************************************************************************************************************************
        'due in

        _SQL = ""
        _SQL += "select count(*) from Bookings b"
        _SQL += " left join Children c on c.ID = b.child_id"
        _SQL += " where booking_date = " + m_SQLToday
        _SQL += " and c.site_id = " + Parameters.SiteIDQuotes
        _SQL += " and booking_status <> 'Holiday'"
        _SQL += " and b.child_id not in (select act.key_id from Activity act where act.day_id = " + m_TodayID + " and act.key_id = b.child_id and act.type = 'ABSENCE')"
        _SQL += " and b.child_id not in (select rs.person_id from RegisterSummary rs where rs.date = b.booking_date and rs.person_id = b.child_id)"

        If m_SQLRoom <> "" Then _SQL += " and c.group_name = " + m_SQLRoom

        _Value = SharedModule.ReturnSQLCount(_SQL)
        DisplayLabel(dashDueIn, EnumDisplayMode.ZeroGreenPositiveRed, _Value)

        '*************************************************************************************************************************************************
        'spaces
        Dim _Spaces As Decimal = m_Capacity - _ChildHeadcount
        DisplayLabel(dashSpaces, EnumDisplayMode.ZeroRedPositiveGreen, _Spaces)

        '*************************************************************************************************************************************************
        'birthdays

        _SQL = "SELECT count(*) "
        _SQL += " FROM Children"
        _SQL += " WHERE DatePart(Week, DateAdd(Year, DatePart(Year, GETDATE()) - DatePart(Year, DOB), DOB)) = DatePart(Week, GETDATE())"
        _SQL += " AND (DATE_LEFT IS NULL OR DATE_LEFT > getdate())"
        _SQL += " AND status = 'Current'"
        _SQL += " AND site_id = " + Parameters.SiteIDQuotes

        _Value = SharedModule.ReturnSQLCount(_SQL)
        If _Value > 0 Then
            dashBirthdays.SetValue(_Value, DashboardLabel.EnumMiddleFormat.WholeNumber, DashboardLabel.EnumColourCode.Amber)
        Else
            dashBirthdays.SetValue(_Value, DashboardLabel.EnumMiddleFormat.WholeNumber, DashboardLabel.EnumColourCode.Green)
        End If


        '*************************************************************************************************************************************************
        'viewings

        _SQL = "select count(*) from Leads where date_viewing = " + m_SQLToday
        _SQL += " and site_id = " + Parameters.SiteIDQuotes

        _Value = SharedModule.ReturnSQLCount(_SQL)
        DisplayLabel(dashViewings, EnumDisplayMode.ZeroRedPositiveGreen, _Value)

        '*************************************************************************************************************************************************
        'observations

        '_SQL = ""
        '_SQL += "select count(*) from Obs o"
        '_SQL += " left join ObsChildren oc on oc.obs_id = o.ID"
        '_SQL += " left join Children c on c.ID = oc.child_id"
        '_SQL += " where o.day_id = " + m_TodayID

        '_SQL += " and c.site_id = " + Parameters.SiteIDQuotes

        'If m_SQLRoom <> "" Then _SQL += " and c.group_name = " + m_SQLRoom

        '_Value = ReturnSQLCount(_SQL)
        'DisplayLabel(dashViewings, EnumDisplayMode.ZeroRedPositiveGreen, _Value)

        '*************************************************************************************************************************************************
        'accidents/incidents

        _SQL = ""
        _SQL += "select count(*) from Incidents i"
        _SQL += " left join Children c on c.ID = i.child_id"
        _SQL += " where i.day_id = " + m_TodayID

        _SQL += " and c.site_id = " + Parameters.SiteIDQuotes
        If m_SQLRoom <> "" Then _SQL += " and c.group_name = " + m_SQLRoom

        _Value = SharedModule.ReturnSQLCount(_SQL)
        DisplayLabel(dashAccidents, EnumDisplayMode.ZeroGreenPositiveRed, _Value)

        '*************************************************************************************************************************************************
        'last change

        _SQL = ""
        _SQL += "select stamp from Activity a"
        _SQL += " left join Children c on c.ID = a.key_id"
        _SQL += " where a.day_id = " + m_TodayID
        _SQL += " and a.type = 'TOILET'"

        _SQL += " and c.site_id = " + Parameters.SiteIDQuotes
        If m_SQLRoom <> "" Then _SQL += " and c.group_name = " + m_SQLRoom

        _SQL += " order by a.stamp desc"

        Dim _DR As DataRow = DirectSQL.ReturnDataRow(_SQL)
        If _DR Is Nothing Then
            dashLastChange.LabelMiddle = "None"
            dashLastChange.Colour = DashboardLabel.EnumColourCode.Green
        Else

            Dim _LastChange As Date = CDate(_DR.Item("stamp"))
            Dim _Mins As Long = DateDiff(DateInterval.Minute, _LastChange, Now)
            dashLastChange.LabelMiddle = _Mins.ToString

            If _Mins >= m_NappyOverDue Then
                dashLastChange.Colour = DashboardLabel.EnumColourCode.Red
            Else
                If _Mins >= m_NappyWarning Then
                    dashLastChange.Colour = DashboardLabel.EnumColourCode.Amber
                Else
                    dashLastChange.Colour = DashboardLabel.EnumColourCode.Green
                End If
            End If

        End If

        '*************************************************************************************************************************************************
        'next medicine due (room)
        Dim _RoomMeds As Date? = GetNextMeds(False)
        If _RoomMeds Is Nothing Then
            dashMedRoom.LabelMiddle = "OK"
            dashMedRoom.Colour = DashboardLabel.EnumColourCode.Green
        Else
            Dim _Mins As Long = DateDiff(DateInterval.Minute, Now, _RoomMeds.Value)
            DisplayMedLabel(dashMedRoom, _Mins)
        End If

        '*************************************************************************************************************************************************
        'next medicine due (building)
        Dim _NurseryMeds As Date? = GetNextMeds(False)
        If _NurseryMeds Is Nothing Then
            dashMedAll.LabelMiddle = "OK"
            dashMedAll.Colour = DashboardLabel.EnumColourCode.Green
        Else
            Dim _Mins As Long = DateDiff(DateInterval.Minute, Now, _NurseryMeds.Value)
            DisplayMedLabel(dashMedAll, _Mins)
        End If

        '*************************************************************************************************************************************************

        Me.Cursor = Cursors.Default
        Application.DoEvents()

    End Sub

    Private Function GetNextMeds(ByVal WholeNursery As Boolean) As Date?

        Dim _Return As Date? = Nothing

        Dim _SQL As String = SharedModule.MedicationDueQuery(WholeNursery)

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If _DT IsNot Nothing Then
            If _DT.Rows.Count > 0 Then
                _Return = CDate(_DT.Rows(0).Item("due"))
            End If
            _DT.Dispose()
            _DT = Nothing
        End If

        Return _Return

    End Function

    Private Sub DisplayMedLabel(ByRef DashLabel As DashboardLabel, ByVal Mins As Long)

        If Mins >= 15 Then
            DashLabel.SetValue(CDec(Mins), DashboardLabel.EnumMiddleFormat.WholeNumber, DashboardLabel.EnumColourCode.Green)
        Else
            If Mins > 5 Then
                DashLabel.SetValue(CDec(Mins), DashboardLabel.EnumMiddleFormat.WholeNumber, DashboardLabel.EnumColourCode.Amber)
            Else
                DashLabel.SetValue(CDec(Mins), DashboardLabel.EnumMiddleFormat.WholeNumber, DashboardLabel.EnumColourCode.Red)
            End If
        End If

    End Sub

    Private Sub DisplayChangeLabel(ByRef DashLabel As DashboardLabel, ByVal Mins As Long)

        If Mins >= 15 Then
            DashLabel.SetValue(CDec(Mins), DashboardLabel.EnumMiddleFormat.WholeNumber, DashboardLabel.EnumColourCode.Green)
        Else
            If Mins > 5 Then
                DashLabel.SetValue(CDec(Mins), DashboardLabel.EnumMiddleFormat.WholeNumber, DashboardLabel.EnumColourCode.Amber)
            Else
                DashLabel.SetValue(CDec(Mins), DashboardLabel.EnumMiddleFormat.WholeNumber, DashboardLabel.EnumColourCode.Red)
            End If
        End If

    End Sub

    Private Sub DisplayLabel(ByRef DashLabel As DashboardLabel, ByVal DisplayMode As EnumDisplayMode, ByVal Value As Decimal)

        If DisplayMode = EnumDisplayMode.ZeroGreenPositiveRed Then
            If Value <= 0 Then
                DashLabel.SetValue(Value, DashboardLabel.EnumMiddleFormat.WholeNumber, DashboardLabel.EnumColourCode.Green)
            Else
                DashLabel.SetValue(Value, DashboardLabel.EnumMiddleFormat.WholeNumber, DashboardLabel.EnumColourCode.Red)
            End If
        End If

        If DisplayMode = EnumDisplayMode.ZeroRedPositiveGreen Then
            If Value <= 0 Then
                DashLabel.SetValue(Value, DashboardLabel.EnumMiddleFormat.WholeNumber, DashboardLabel.EnumColourCode.Red)
            Else
                DashLabel.SetValue(Value, DashboardLabel.EnumMiddleFormat.WholeNumber, DashboardLabel.EnumColourCode.Green)
            End If
        End If

    End Sub

    Private Sub dashChildRoom_Click(sender As Object, e As DashboardLabel.DashboardEventArgs) Handles dashChildren.Click, dashStaffRatio.Click, dashStaff.Click, dashStaffReqd.Click,
                                                                                                      dashDueIn.Click, dashSpaces.Click,
                                                                                                      dashAccidents.Click, dashMedRoom.Click, dashLastChange.Click, dashMedAll.Click
        ScreenTapped()

    End Sub

    Private Sub ScreenTapped()

        If SharedModule.UnlockScreen Then

            timMinute.Stop()
            Me.Hide()

            Dim _frm As New frmMain(False)
            _frm.BringToFront()
            _frm.ShowDialog()

            If _frm.DialogResult = DialogResult.OK Then
                Me.Show()
                DisplayScreen()
                timMinute.Start()
            Else
                DoorHandler.Disconnect()
                End
            End If

        End If

    End Sub

    Private Sub timMinute_Tick(sender As Object, e As EventArgs) Handles timMinute.Tick
        DisplayScreen()
    End Sub

    Private Sub DisplayLists()

        ResizeTiles()
        DisplayChildTiles(False)
        DisplayStaffTiles()
        DisplayAllergyTiles()
        DisplayMedicineTiles()

    End Sub

    Private Sub DisplayAllergyTiles()
        DisplayChildTiles(True)
    End Sub

    Private Sub DisplayMedicineTiles()
        Dim _SQL As String = SharedModule.MedicationDueQuery
        PopulateTileBar(tbgMedication, _SQL, EnumBar.Medication)
    End Sub

    Private Sub DisplayChildTiles(ByVal AllergiesOnly As Boolean)

        Dim sql As String = ""

        sql = String.Concat("select r.person_id as 'ID',",
                            IIf(Parameters.HideChildName, " c.knownas as 'Name',", " c.forename as 'Name',"),
                            " r.location, c.allergy_rating, c.diet_restrict, b.timeslots, b.tariff_name,",
                            " b.booking_from, b.booking_to, c.today_notes",
                            " from RegisterSummary r",
                            " left join Children c on c.ID = r.person_id",
                            " left join Bookings b on b.booking_date = ", m_SQLToday, " and b.child_id = r.person_id",
                            " where r.day_id = ", m_TodayID,
                            " and r.person_type = 'C'",
                            " and r.in_out = 'I'")

        If m_SQLRoom <> "" Then sql &= " and r.location = " + m_SQLRoom
        If AllergiesOnly Then sql &= " and c.allergy_rating <> 'None'"

        sql &= " order by c.forename"

        If AllergiesOnly Then
            PopulateTileBar(tbgAllergy, sql, EnumBar.ChildrenCheckedIn)
        Else
            PopulateTileBar(tbgChildren, sql, EnumBar.ChildrenCheckedIn)
        End If

    End Sub

    Private Sub DisplayStaffTiles()

        Dim _SQL As String = ""
        _SQL += "select r.person_id as 'ID', s.forename as 'Name', r.location"
        _SQL += " from RegisterSummary r"
        _SQL += " left join Staff s on s.ID = r.person_id"
        _SQL += " where r.day_id = " + m_TodayID
        _SQL += " and r.person_type = 'S'"
        _SQL += " and r.in_out = 'I'"

        If m_SQLRoom <> "" Then _SQL += " and r.location = " + m_SQLRoom

        _SQL += " order by s.forename"

        PopulateTileBar(tbgStaff, _SQL, EnumBar.StaffCheckedIn)

    End Sub

    Private Sub PopulateTileBar(ByRef BarGroup As TileBarGroup, ByVal _SQL As String, ByVal Bar As EnumBar)

        BarGroup.Items.Clear()

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _i As New TileBarItem
                With _i

                    Dim _DefaultColour As Color = _i.AppearanceItem.Normal.BackColor

                    .ItemSize = TileBarItemSize.Wide

                    Select Case Bar

                        Case EnumBar.ChildrenCheckedIn

                            Dim _Colour As Color = Business.Child.ReturnColour(_DR.Item("allergy_rating").ToString, _DR.Item("diet_restrict").ToString)
                            If _Colour = Color.Black Then _Colour = _DefaultColour

                            .Name = "chi_" + _DR.Item("ID").ToString

                            AddHandler _i.ItemClick, AddressOf ChildItemClick

                            .TextAlignment = TileItemContentAlignment.TopLeft
                            .Text = _DR.Item("name").ToString

                            .AppearanceItem.Normal.BackColor = _Colour
                            If .AppearanceItem.Normal.BackColor <> _DefaultColour Then .AppearanceItem.Normal.ForeColor = Color.Black

                            Dim _te As New TileItemElement
                            With _te
                                .Text = Business.Child.BuildAllergyText(_DR.Item("allergy_rating").ToString, _DR.Item("diet_restrict").ToString, True)
                                .TextAlignment = TileItemContentAlignment.TopRight
                                .Appearance.Normal.BackColor = _Colour
                                If .Appearance.Normal.BackColor <> _DefaultColour Then .Appearance.Normal.ForeColor = Color.Black
                            End With

                            Dim _be As New TileItemElement
                            With _be
                                .Text = Business.Child.BuildBookingText(_DR.Item("tariff_name").ToString, _DR.Item("timeslots").ToString, True)
                                If Not String.IsNullOrWhiteSpace(_DR.Item("today_notes").ToString) Then .Text &= " | " & _DR.Item("today_notes").ToString
                                .TextAlignment = TileItemContentAlignment.BottomLeft
                                .Appearance.Normal.FontSizeDelta = -4
                                .Appearance.Normal.BackColor = _Colour
                                If .Appearance.Normal.BackColor <> _DefaultColour Then .Appearance.Normal.ForeColor = Color.Black
                            End With

                            _i.Elements.Add(_te)
                            _i.Elements.Add(_be)

                            Dim tileBar As TileBar = Nothing
                            If BarGroup.Name = "tbgChildren" Then tileBar = TryCast(tbChildren, TileBar)
                            If BarGroup.Name = "tbgAllergy" Then tileBar = TryCast(tbAllergy, TileBar)
                            tileBar.ItemSize += Convert.ToInt16(TileSizeIncrease(Len(_be.Text), tbChildren.ItemSize))

                        Case EnumBar.StaffCheckedIn

                            .Name = "sta_" + _DR.Item("ID").ToString

                            'AddHandler _i.ItemClick, AddressOf AgeItemClick

                            .TextAlignment = TileItemContentAlignment.TopLeft
                            .Text = _DR.Item("name").ToString

                            Dim _be As New TileItemElement
                            With _be
                                .Text = _DR.Item("location").ToString
                                .TextAlignment = TileItemContentAlignment.BottomLeft
                                .Appearance.Normal.FontSizeDelta = -4
                            End With

                            _i.Elements.Add(_be)

                        Case EnumBar.Medication

                            Dim _Colour As Color = ReturnMedicineColour(_DR, _DefaultColour)

                            .Name = "med_" + _DR.Item("ID").ToString

                            AddHandler _i.ItemClick, AddressOf MedicationItemClick

                            .TextAlignment = TileItemContentAlignment.TopLeft
                            .Text = _DR.Item("button").ToString

                            .AppearanceItem.Normal.BackColor = _Colour
                            If .AppearanceItem.Normal.BackColor <> _DefaultColour Then .AppearanceItem.Normal.ForeColor = Color.Black

                            Dim _be As New TileItemElement
                            With _be
                                .Text = _DR.Item("medicine").ToString
                                .TextAlignment = TileItemContentAlignment.BottomLeft
                                .Appearance.Normal.FontSizeDelta = -4
                                .Appearance.Normal.BackColor = _Colour
                                .Appearance.Normal.ForeColor = Color.Black
                            End With

                            _i.Elements.Add(_be)

                            tbMedication.ItemSize += Convert.ToInt16(TileSizeIncrease(Len(_be.Text), tbMedication.ItemSize))

                    End Select

                End With

                BarGroup.Items.Add(_i)

            Next

        End If


    End Sub

    Private Function TileSizeIncrease(textLength As Double, currentSize As Double) As Double

        Dim multiplier As Double = 0
        multiplier = Math.Floor(textLength / 50)

        If multiplier * 10 < (currentSize - 60) Then multiplier = 0

        Return multiplier * 10

    End Function

    Private Function ReturnMedicineColour(ByRef DR As DataRow, ByVal CurrentColour As Color) As Color

        Dim _Due As Date = CDate(DR.Item("due"))
        Dim _Diff As Long = DateDiff(DateInterval.Minute, Now, _Due)

        If _Diff > 15 Then
            Return txtGreen.BackColor
        Else
            If _Diff > 5 Then
                Return txtOrange.BackColor
            Else
                Return txtRed.BackColor
            End If
        End If

    End Function

    Private Sub ChildItemClick(sender As Object, e As TileItemEventArgs)

        If SharedModule.UnlockScreen Then

            timMinute.Stop()
            Me.Hide()

            Dim _id As String = e.Item.Name.Substring(4)
            Business.Child.DrillDownModal(_id, e.Item.Text)

            Me.Show()

            DisplayScreen()
            timMinute.Start()

        End If

    End Sub

    Private Sub MedicationItemClick(sender As Object, e As TileItemEventArgs)

        If SharedModule.UnlockScreen Then

            timMinute.Stop()
            Me.Hide()

            Dim _id As String = e.Item.Name.Substring(4)
            If SharedModule.LogMedicine(frmMedicalLog.EnumMode.EditLog, _id) Then
                DisplayScreen()
            End If

            Me.Show()
            timMinute.Start()

        End If

    End Sub

    Private Sub frmScreenSaver_ResizeEnd(sender As Object, e As EventArgs) Handles Me.ResizeEnd
        ResizeTiles()
    End Sub

    Private Sub ResizeTiles()
        tbChildren.WideTileWidth = tbChildren.Width - 50
        tbChildren.ItemSize = 60
        tbStaff.WideTileWidth = tbStaff.Width - 50
        tbStaff.ItemSize = 60
        tbAllergy.WideTileWidth = tbAllergy.Width - 50
        tbAllergy.ItemSize = 60
        tbMedication.WideTileWidth = tbMedication.Width - 50
        tbMedication.ItemSize = 60
    End Sub

    Private Sub picLogo_Click(sender As Object, e As EventArgs) Handles picLogo.Click
        ScreenTapped()
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        End
    End Sub

    Private Sub dashChildren_DoubleClick(sender As Object, e As DashboardLabel.DashboardEventArgs) Handles dashChildren.DoubleClick

    End Sub

    Private Sub dashViewings_DoubleClick(sender As Object, e As DashboardLabel.DashboardEventArgs) Handles dashViewings.DoubleClick, dashViewings.Click

        If SharedModule.UnlockScreen Then

            timMinute.Stop()

            Dim _SQL As String = ""

            _SQL += "select ID, date_viewing as 'Viewing Date', contact_fullname as 'Contact', contact_relationship as 'Relationship', contact_mobile as 'Mobile',"
            _SQL += " child_fullname as 'Child', child_dob as 'DOB' from Leads"
            _SQL += " where date_viewing = " + m_SQLToday
            _SQL += " AND site_id = " + Parameters.SiteIDQuotes

            DisplayGridForm(_SQL, "Viewings Today")

            timMinute.Start()

        End If

    End Sub

    Private Sub dashBirthdays_DoubleClick(sender As Object, e As DashboardLabel.DashboardEventArgs) Handles dashBirthdays.DoubleClick, dashBirthdays.Click

        If SharedModule.UnlockScreen Then

            timMinute.Stop()

            Dim _SQL As String = ""

            _SQL = "SELECT ID, fullname as 'Name', group_name as 'Room', keyworker_name as 'Keyworker', dob as 'DOB'"
            _SQL += " FROM Children"
            _SQL += " WHERE DatePart(Week, DateAdd(Year, DatePart(Year, GETDATE()) - DatePart(Year, DOB), DOB)) = DatePart(Week, GETDATE())"
            _SQL += " AND (DATE_LEFT IS NULL OR DATE_LEFT > GETDATE())"
            _SQL += " AND status = 'Current'"
            _SQL += " AND site_id = " + Parameters.SiteIDQuotes

            DisplayGridForm(_SQL, "Birthdays this week")

            timMinute.Start()

        End If

    End Sub

    Private Sub DisplayGridForm(ByVal SQL As String, ByVal FormCaption As String)
        Dim _frm As New frmAddRemove(SQL, FormCaption)
        _frm.DisableTimeFormatting = True
        _frm.ShowDialog()
        _frm.Dispose()
        _frm = Nothing
    End Sub

End Class
