﻿
Imports NurseryTouchscreen.SharedModule

Public Class frmPhotoTarget

    Private m_StaffIn As Boolean = False
    Private m_ChildrenIn As Boolean = False
    Private m_Target As String = ""
    Private m_TargetPair As Pair = Nothing

    Public ReadOnly Property Target As String
        Get
            Return m_Target
        End Get
    End Property

    Public ReadOnly Property TargetPair As Pair
        Get
            Return m_TargetPair
        End Get
    End Property

    Private Sub frmPhotoTarget_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        m_StaffIn = StaffCheckedIn()
        m_ChildrenIn = ChildrenCheckedIn()

        btnProfileStaff.Enabled = m_StaffIn

        btnProfileChild.Enabled = m_ChildrenIn
        btnProfileContact.Enabled = m_ChildrenIn
        btnReportChild.Enabled = m_ChildrenIn
        btnReportRoom.Enabled = m_ChildrenIn
        btnReportSite.Enabled = m_ChildrenIn
        btnObs.Enabled = m_ChildrenIn
        btnIncident.Enabled = m_ChildrenIn

    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub btnProfileChild_Click(sender As Object, e As EventArgs) Handles btnProfileChild.Click
        Dim _Child As Pair = Business.Child.FindChildByGroup(Enums.PersonMode.OnlyCheckedIn, True)
        If _Child IsNot Nothing Then
            SetAndExit("ProfileChild", _Child)
        End If
    End Sub

    Private Sub btnReportChild_Click(sender As Object, e As EventArgs) Handles btnReportChild.Click
        Dim _Child As Pair = Business.Child.FindChildByGroup(Enums.PersonMode.OnlyCheckedIn, True)
        If _Child IsNot Nothing Then
            SetAndExit("ReportChild", _Child)
        End If
    End Sub

    Private Sub btnObs_Click(sender As Object, e As EventArgs) Handles btnObs.Click
        Dim _Child As Pair = Business.Child.FindChildByGroup(Enums.PersonMode.OnlyCheckedIn, True)
        If _Child IsNot Nothing Then
            SetAndExit("Observation", _Child)
        End If
    End Sub

    Private Sub btnIncident_Click(sender As Object, e As EventArgs) Handles btnIncident.Click
        Dim _Child As Pair = Business.Child.FindChildByGroup(Enums.PersonMode.OnlyCheckedIn, True)
        If _Child IsNot Nothing Then
            SetAndExit("Incident", _Child)
        End If
    End Sub

    Private Sub btnProfileStaff_Click(sender As Object, e As EventArgs) Handles btnProfileStaff.Click
        Dim _Staff As Pair = Business.Staff.FindStaff(Enums.PersonMode.OnlyCheckedIn, False)
        If _Staff IsNot Nothing Then
            SetAndExit("ProfileStaff", _Staff)
        End If
    End Sub

    Private Sub SetAndExit(ByVal Target As String, ByVal TargetPair As Pair)
        m_Target = Target
        m_TargetPair = TargetPair
        Me.DialogResult = DialogResult.OK
        Me.Close()
    End Sub

End Class