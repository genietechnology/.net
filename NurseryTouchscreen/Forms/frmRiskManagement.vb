﻿Imports DevExpress.XtraGrid.Views.Base

Public Class frmRiskManagement

    Private m_SelectedRiskID As String = ""
    Private m_SelectedRiskTitle As String = ""

    Private Sub frmRiskManagement_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DisplayRisksGrid()
    End Sub

    Private Sub DisplayRisksGrid()

        Dim _SQL As String = ""

        _SQL += "select id, title as 'Assessment', ass_freq as 'Freq.'"
        _SQL += " from Risks"
        _SQL += " order by title"

        tgRisks.HideFirstColumn = True
        tgRisks.Populate(_SQL)

    End Sub

    Private Sub DisplayItemsGrid()

        m_SelectedRiskID = m_SelectedRiskID

        Dim _SQL As String = ""

        _SQL += "select id, activity as 'Activity', hazard_desc as 'Hazards'"
        _SQL += " from RiskActivities"
        _SQL += " where risk_id = '" + m_SelectedRiskID + "'"
        _SQL += " order by activity"

        tgItems.HideFirstColumn = True
        tgItems.Populate(_SQL)

    End Sub

    Private Function GetValue(ByRef GridControl As TouchGrid, ByVal RowHandle As Integer, ByVal ColumnName As String) As String
        If RowHandle < 0 Then Return ""
        If GridControl Is Nothing Then Return ""
        If GridControl.RecordCount <= 0 Then Return ""
        Return GridControl.GetRowCellValue(RowHandle, ColumnName).ToString
    End Function

    Private Sub EditItem()

        Dim _ItemID As String = GetValue(tgItems, tgItems.RowIndex, "id")
        If _ItemID <> "" Then

            Dim _ItemText As String = GetValue(tgItems, tgItems.RowIndex, "Activity")

            Dim _frm As New frmRiskItem(New Guid(m_SelectedRiskID), New Guid(_ItemID))
            _frm.Text = "Edit: " + _ItemText + " (" + m_SelectedRiskTitle + ")"
            _frm.ShowDialog()

            If _frm.DialogResult = DialogResult.OK Then
                DisplayItemsGrid()
            End If

            _frm.Dispose()

        End If

    End Sub

    Private Sub EditRisk()

        If m_SelectedRiskID <> "" Then

            Dim _frm As New frmRiskHeader(New Guid(m_SelectedRiskID))
            _frm.Text = "Edit " + m_SelectedRiskTitle
            _frm.ShowDialog()

            If _frm.DialogResult = DialogResult.OK Then
                DisplayRisksGrid()
            End If

            _frm.Dispose()

        End If

    End Sub

#Region "Controls"

    Private Sub tgRisks_GridDoubleClick(sender As Object, e As EventArgs) Handles tgRisks.GridDoubleClick
        EditRisk()
    End Sub

    Private Sub tgRisks_FocusedRowChanged(sender As Object, e As FocusedRowChangedEventArgs) Handles tgRisks.FocusedRowChanged

        Dim _RiskID As String = GetValue(tgRisks, e.FocusedRowHandle, "id")

        If _RiskID <> "" Then
            m_SelectedRiskID = _RiskID
            m_SelectedRiskTitle = GetValue(tgRisks, e.FocusedRowHandle, "Assessment")
            DisplayItemsGrid()
        End If

    End Sub

    Private Sub btnItemAdd_Click(sender As Object, e As EventArgs) Handles btnItemAdd.Click

        Dim _frm As New frmRiskItem(New Guid(m_SelectedRiskID), Nothing)
        _frm.Text = "Create New Item for " + m_SelectedRiskTitle
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            DisplayItemsGrid()
        End If

        _frm.Dispose()

    End Sub

    Private Sub tgItems_GridDoubleClick(sender As Object, e As EventArgs) Handles tgItems.GridDoubleClick
        EditItem()
    End Sub

    Private Sub btnItemEdit_Click(sender As Object, e As EventArgs) Handles btnItemEdit.Click
        EditItem()
    End Sub

    Private Sub btnRiskAdd_Click(sender As Object, e As EventArgs) Handles btnRiskAdd.Click

        Dim _frm As New frmRiskHeader(Nothing)
        _frm.Text = "Create Risk Assessment"
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            DisplayRisksGrid()
        End If

        _frm.Dispose()

    End Sub

    Private Sub btnRiskEdit_Click(sender As Object, e As EventArgs) Handles btnRiskEdit.Click
        EditRisk()
    End Sub

    Private Sub btnRiskDelete_Click(sender As Object, e As EventArgs) Handles btnRiskDelete.Click

    End Sub

    Private Sub btnItemDelete_Click(sender As Object, e As EventArgs) Handles btnItemDelete.Click

    End Sub

#End Region

End Class
