﻿Imports NurseryTouchscreen.SharedModule

Public Class frmChild

    Private m_ChildID As String
    Private m_ChildName As String
    Private m_ChildPair As New SharedModule.Pair
    Private m_ChildInOut As Enums.InOut
    Private m_FamilyID As String = ""
    Private m_Location As String = ""

    Private m_Feedback As Boolean = False
    Private m_FeedbackText As String = ""

    Private m_CrossChecked As Boolean = False

    Public Property ChildID() As String
        Get
            Return m_ChildID
        End Get
        Set(ByVal value As String)
            m_ChildID = value
        End Set
    End Property

    Public Property ChildName() As String
        Get
            Return m_ChildName
        End Get
        Set(ByVal value As String)
            m_ChildName = value
        End Set
    End Property

    Private Sub frmChild_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        DisplayDetails()
    End Sub

    Private Sub DisplayDetails()

        m_ChildPair.Code = m_ChildID
        m_ChildPair.Text = m_ChildName

        m_ChildInOut = PersonInorOut(New Guid(m_ChildID))
        m_FamilyID = GetFamilyIDFromChildID(m_ChildID)

        m_Feedback = ReturnFeedback(Parameters.TodayID, m_ChildID, m_FeedbackText)
        m_CrossChecked = HasActivity(Parameters.TodayID, m_ChildID, EnumActivityType.CrossCheck)

        m_Location = Business.Child.ReturnLocation(m_ChildID)
        picPhoto.Image = Business.Child.ReturnPhoto(m_ChildID)

        SetButtons()

        m_ChildInOut = PersonInorOut(New Guid(m_ChildID))
        m_FamilyID = GetFamilyIDFromChildID(m_ChildID)

        DisplayBasic()
        DisplayMedical()
        DisplayAllergies()
        DisplayConsent()
        DisplayDocuments()

    End Sub

    Private Sub DisplayBasic()

        Dim sqlFilter As String = ""
        Dim hideChildName As Boolean = Parameters.HideChildName

        If hideChildName Then
            sqlFilter = "select knownas as 'Name',"
        Else
            sqlFilter = "select forename as 'Forename', surname as 'Surname',"
        End If

        sqlFilter &= String.Concat(" dob as 'DOB', gender as 'Gender', started as 'Date Started',",
                                   " keyworker_name as 'Keyworker', group_name as 'Group', religion as 'Religion',",
                                   " language as 'Language', nappies as 'Nappies?', today_notes as 'Today Notes'",
                                   " from Children",
                                   " where ID = '", m_ChildID, "'")

        vgBasic.DataSource = DAL.ReturnDataTable(sqlFilter)

        Dim row As DevExpress.XtraVerticalGrid.Rows.BaseRow = vgBasic.GetRowByFieldName("Today Notes")
        If row IsNot Nothing Then
            Dim edit As New DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit()
            vgBasic.RepositoryItems.Add(edit)
            row.Properties.RowEdit = edit
        End If

    End Sub

    Private Sub DisplayMedical()

        Dim _SQL As String = ""
        _SQL += " select sen as 'SEN', allergy_rating as 'Allergy Rating', diet_restrict as 'Diet Restriction', diet_notes as 'Dietary Notes',"
        _SQL += " med_allergies as 'Allergies', med_medication as 'Medication', med_notes as 'Medical Notes',"
        _SQL += " surg_doc as 'Doctor', surg_name as 'Surgery', surg_tel as 'Surgery Tel',"
        _SQL += " hv_name as 'HV Name', hv_tel as 'HV Tel', hv_mobile as 'HV Mobile'"
        _SQL += " from Children"
        _SQL += " where ID = '" + m_ChildID + "'"

        vgMedical.DataSource = DAL.ReturnDataTable(_SQL)

    End Sub

    Private Sub DisplayAllergies()

        Dim _SQL As String = ""
        _SQL += "select AppListItems.name as 'Allergy', cast(iif(ChildAttrib.attribute = AppListItems.name, 1, 0) as bit) as ' ' from AppListItems"
        _SQL += " left join AppLists on AppLists.ID = AppListItems.list_id"
        _SQL += " left join ChildAttrib on ChildAttrib.category = 'Allergies'"
        _SQL += " and ChildAttrib.child_id = '" + m_ChildID + "'"
        _SQL += " and ChildAttrib.attribute = AppListItems.name"
        _SQL += " where AppLists.name = 'Allergy Matrix'"
        _SQL += " order by AppListItems.seq, AppListItems.name"

        tgAllergies.Clear()
        tgAllergies.Populate(_SQL)

    End Sub

    Private Sub DisplayConsent()

        Dim _SQL As String = ""

        _SQL += "select i.name as 'Consent', isnull(c.given, 0) as ' ' from AppListItems i"
        _SQL += " left join AppLists l on l.ID = i.list_id"
        _SQL += " left join ChildConsent c on c.consent = i.name and c.child_id = '" + m_ChildID + "'"
        _SQL += " where l.name = 'Consent'"
        _SQL += " order by i.ref_1, i.name"

        tgConsent.Clear()
        tgConsent.Populate(_SQL)

    End Sub

    Private Sub DisplayDocuments()

        Dim _SQL As String = ""

        _SQL += "select id, subject as 'Title' from AppDocs"
        _SQL += " where key_id = '" + m_ChildID + "'"
        _SQL += " and tags like '%touch%'"
        _SQL += " order by subject"

        tgDocs.Clear()
        tgDocs.HideFirstColumn = True
        tgDocs.Populate(_SQL)

    End Sub

    Private Sub SetButtons()

        Dim _CheckedIn As Boolean = False
        If m_ChildInOut = Enums.InOut.CheckIn Then _CheckedIn = True

        'If _CheckedIn Then
        '    If m_CrossChecked Then
        '        txtCrossChecked.Text = "Cross-Checked"
        '        txtCrossChecked.BackColor = SharedModule.ColourGreen
        '    Else
        '        txtCrossChecked.Text = "Not Cross-Checked"
        '        txtCrossChecked.BackColor = SharedModule.ColourRed
        '    End If
        'End If

        btnCheckInChild.Enabled = Not _CheckedIn
        btnCheckOutChild.Enabled = _CheckedIn
        btnMoveRoom.Enabled = Not Parameters.DisableLocation

        If Parameters.UseWebServices Then
            btnPhoto.Enabled = _CheckedIn
        Else
            btnPhoto.Enabled = False
        End If

        btnToilet.Enabled = _CheckedIn
        btnMilk.Enabled = _CheckedIn

        btnObs.Enabled = _CheckedIn
        btnFood.Enabled = _CheckedIn
        btnSleep.Enabled = _CheckedIn

        btnFeedback.Enabled = _CheckedIn
        btnIncidents.Enabled = _CheckedIn
        btnSuncream.Enabled = _CheckedIn
        btnRequests.Enabled = _CheckedIn

        If _CheckedIn Then
            btnReport.Enabled = Parameters.UseWebServices
        Else
            btnReport.Enabled = False
        End If

        btnContacts.Enabled = True

        btnLogMedicine.Enabled = _CheckedIn
        btnCrossCheck.Enabled = _CheckedIn
        btnMedAuth.Enabled = _CheckedIn

    End Sub

    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub btnToilet_Click(sender As System.Object, e As System.EventArgs) Handles btnToilet.Click
        btnToilet.Enabled = False
        ChildToilet(m_ChildID)
        btnToilet.Enabled = True
    End Sub

    Private Sub btnFood_Click(sender As System.Object, e As System.EventArgs) Handles btnFood.Click
        btnFood.Enabled = False
        ChildFood(m_ChildID)
        btnFood.Enabled = True
    End Sub

    Private Sub btnSleep_Click(sender As System.Object, e As System.EventArgs) Handles btnSleep.Click
        btnSleep.Enabled = False
        ChildSleep(m_ChildID)
        btnSleep.Enabled = True
    End Sub

    Private Sub btnRequests_Click(sender As System.Object, e As System.EventArgs) Handles btnRequests.Click
        btnRequests.Enabled = False
        ChildRequests(m_ChildID)
        btnRequests.Enabled = True
    End Sub

    Private Sub btnMilk_Click(sender As System.Object, e As System.EventArgs) Handles btnMilk.Click
        btnMilk.Enabled = False
        ChildMilk(m_ChildID)
        btnMilk.Enabled = True
    End Sub

    Private Sub btnCheckInChild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheckInChild.Click

        If Not SharedModule.CheckinsPermitted Then Exit Sub

        Dim signature As New Signature
        Dim signatureRequired As Boolean = Parameters.ContactSignatureIn

        If signatureRequired Then

            Msgbox("Please tap OK so we can capture your signature...", MessageBoxIcon.Exclamation, "Signature Required")
            signature.CaptureParentSignature(True, Business.Child.ReturnFamilyID(m_ChildID))

            If Signature.SignatureCaptured Then
                SharedModule.RegisterTransaction(New Guid(m_ChildID), Enums.PersonType.Child, m_ChildName, Enums.InOut.CheckIn, Enums.RegisterVia.Menu)
            Else
                If Msgbox("Parent Signature declined. Do you want to specify another person collecting?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, "Signature Declined") = DialogResult.Yes Then
                    Dim name As String = SharedModule.OSK("Please enter the name of the person collecting")
                    If name <> "" Then
                        Signature.CaptureVisitorSignature(True, Nothing, name)
                        If Signature.SignatureCaptured Then
                            SharedModule.RegisterTransaction(New Guid(m_ChildID), Enums.PersonType.Child, m_ChildName, Enums.InOut.CheckIn, Enums.RegisterVia.Menu)
                        End If
                    End If
                End If
            End If
        Else
            SharedModule.RegisterTransaction(New Guid(m_ChildID), Enums.PersonType.Child, m_ChildName, Enums.InOut.CheckIn, Enums.RegisterVia.Child)
        End If

        m_ChildInOut = PersonInorOut(New Guid(m_ChildID))
        SetButtons()
    End Sub

    Private Sub btnCheckOutChild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheckOutChild.Click
        If Not SharedModule.CheckinsPermitted Then Exit Sub
        If SharedModule.DoCheckOut(Me.ChildID, Me.ChildName) Then
            Me.Close()
        End If
    End Sub

    Private Sub btnLogMedicine_Click(sender As Object, e As EventArgs) Handles btnLogMedicine.Click
        SharedModule.LogMedicine(frmMedicalLog.EnumMode.CreateLog, m_ChildID)
    End Sub

    Private Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnReport.Click

        If Parameters.UseWebServices Then

            btnReport.Enabled = False
            Me.Cursor = Cursors.AppStarting

            ServiceHandler.ViewReport(m_ChildID)

            btnReport.Enabled = True
            Me.Cursor = Cursors.Default

        End If

    End Sub

    Private Sub btnContacts_Click(sender As Object, e As EventArgs) Handles btnContacts.Click

        Dim _SQL As String = ""

        If Parameters.UseDirectSQL Then

            _SQL += "select c.fullname as 'Name', c.relationship as 'Relationship',"

            If Not Parameters.HideTelephoneNumbers Then
                _SQL += " c.tel_home as 'Tel Home', c.tel_mobile as 'Tel Mobile', c.job_tel as 'Tel Work', c.email as 'Email',"
            End If

            _SQL += " c.primary_cont as 'Primary Contact', c.emer_cont as 'Emergency Contact',"
            _SQL += " password as 'Password'"
            _SQL += " from Children k"
            _SQL += " left join Contacts c on c.Family_ID = k.family_id"
            _SQL += " where k.ID = '" + m_ChildID + "'"

        Else

            _SQL += "select fullname, relationship,"

            If Not Parameters.HideTelephoneNumbers Then
                _SQL += " tel_home, tel_mobile, job_tel,"
            End If

            _SQL += " primary_cont, emer_cont, password"
            _SQL += " from Contacts"
            _SQL += " where family_id = '" + m_FamilyID + "'"

        End If

        Dim _Cols As String = ""

        If Parameters.HideTelephoneNumbers Then
            _Cols = "Name|Relationship|Primary|Emergency|Password"
        Else
            _Cols = "Name|Relationship|Tel Home|Tel Mobile|Tel Work|Email|Primary|Emergency|Password"
        End If

        SharedModule.DisplayGridData("Contacts for " + m_ChildName, _SQL, _Cols)

    End Sub

    Private Sub btnPhoto_Click(sender As Object, e As EventArgs) Handles btnPhoto.Click

        'Dim _Result As Integer = 0
        'Dim _File As String = MediaHandler.CaptureNow

        'If _File <> "" Then

        '    btnPhoto.Enabled = False
        '    Me.Cursor = Cursors.WaitCursor
        '    Application.DoEvents()

        '    Dim _F As New FileDetail(_File)
        '    Dim _M As New MediaContent(MediaHandler.EnumCaptureCategory.Media, m_ChildID, _F)

        '    Dim _ID As Guid? = MediaHandler.SaveMedia(_M)

        '    Dim _Service As New NurseryGenieService.NurseryGenieLocalClient

        '    Try
        '        _result = _Service.AttachChildPhoto(m_ChildID, _ID.Value.ToString)
        '        _Service.Close()

        '    Catch tex As TimeoutException
        '        If Parameters.DebugMode Then
        '            Msgbox(tex.Message, MessageBoxIcon.Error, "TimeOut Exception")
        '        Else
        '            Msgbox("Unable to attach photo.", MessageBoxIcon.Error, "Attach Photo")
        '        End If
        '        _Service.Abort()

        '    Catch cex As ServiceModel.CommunicationException
        '        If Parameters.DebugMode Then
        '            Msgbox(cex.Message, MessageBoxIcon.Error, "Communication Exception")
        '        Else
        '            Msgbox("Unable to attach photo.", MessageBoxIcon.Error, "Attach Photo")
        '        End If
        '        _Service.Abort()

        '    End Try

        '    _Service.Close()

        'End If

        btnPhoto.Enabled = True
        Me.Cursor = Cursors.Default
        Application.DoEvents()

        'If _Result = 1 Then
        '    Msgbox("Profile picture updated successfully.", MessageBoxIcon.Information, "Attach Photo")
        'End If

    End Sub

    Private Sub btnFeedback_Click(sender As Object, e As EventArgs) Handles btnFeedback.Click

        m_FeedbackText = SharedModule.OSK("Personal Feedback:", True, True, m_FeedbackText)

        If m_FeedbackText = "" Then
            m_Feedback = False
            DeleteActivity(Parameters.TodayID, Me.ChildID, EnumActivityType.Feedback)
        Else
            m_Feedback = True
            DeleteActivity(Parameters.TodayID, Me.ChildID, EnumActivityType.Feedback)
            LogActivity(Parameters.TodayID, Me.ChildID, Me.ChildName, EnumActivityType.Feedback, "", Notes:=m_FeedbackText)
        End If

    End Sub

    Private Sub btnCrossCheck_Click(sender As Object, e As EventArgs) Handles btnCrossCheck.Click

        Dim _frm As New frmCrossCheck(Me.ChildID, Me.ChildName, frmCrossCheck.EnumMode.CrossCheck)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            DisplayDetails()
        End If

        _frm.Dispose()
        _frm = Nothing

    End Sub

    Private Sub btnIncidents_Click(sender As Object, e As EventArgs) Handles btnIncidents.Click
        btnIncidents.Enabled = False
        SharedModule.ChildIncidents(m_ChildID)
        btnIncidents.Enabled = True
    End Sub

    Private Sub btnObs_Click(sender As Object, e As EventArgs) Handles btnObs.Click
        btnIncidents.Enabled = False
        SharedModule.ChildObservation(m_ChildID)
        btnIncidents.Enabled = True
    End Sub

    Private Sub btnMoveRoom_Click(sender As Object, e As EventArgs) Handles btnMoveRoom.Click

        If Parameters.DisableLocation Then Exit Sub
        Dim _Location As String = Business.Child.ReturnLocation(m_ChildID)

        If Msgbox("Do you want to move " + m_ChildName + " into another room?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Move Room") = MsgBoxResult.Yes Then
            If SharedModule.MoveRoom(Enums.PersonType.Child, New Guid(m_ChildID), m_ChildName, _Location) Then
                Msgbox("Move Completed", MessageBoxIcon.Information, "Move Room")
            End If
        End If

    End Sub

    Private Sub btnMedAuth_Click(sender As Object, e As EventArgs) Handles btnMedAuth.Click
        btnIncidents.Enabled = False
        SharedModule.ChildMedicalAuth(m_ChildID)
        btnIncidents.Enabled = True
    End Sub

    Private Sub tgDocs_GridDoubleClick(sender As Object, e As EventArgs) Handles tgDocs.GridDoubleClick

        If tgDocs.RecordCount = 0 Then Exit Sub

        Dim _ID As String = tgDocs.CurrentRow.Item("ID").ToString

        Dim _SQL As String = "select data, file_ext from AppDocs where ID = '" + _ID + "'"
        Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)

        If _DR IsNot Nothing Then
            Dim _Bytes As Byte() = _DR.Item("data")
            DocumentHandler.OpenFile(_Bytes, _DR.Item("file_ext").ToString)
            _DR = Nothing
        End If

    End Sub

    Private Sub btnSuncream_Click(sender As Object, e As EventArgs) Handles btnSuncream.Click
        btnSuncream.Enabled = False
        SharedModule.ChildSuncream(m_ChildID)
        btnSuncream.Enabled = True
    End Sub

    Private Sub vgBasic_DoubleClick(sender As Object, e As EventArgs) Handles vgBasic.DoubleClick

        Business.Child.UpdateTodayNotes(New Guid(m_ChildID))
        DisplayBasic()

    End Sub
End Class
