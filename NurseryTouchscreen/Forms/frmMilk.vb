﻿Public Class frmMilk

    Private m_MilkCount As Integer

    Private Sub frmMilk_AcceptChanged(sender As Object, e As AcceptArgs) Handles Me.AcceptChanged
        btnAdd.Enabled = e.Enabled
    End Sub

    Private Sub frmMilk_ChildChanged(sender As Object, e As EventArgs) Handles Me.ChildChanged
        DisplayRecord()
    End Sub

    Protected Overrides Sub CommitUpdate()

        SharedModule.DeleteMilk(Parameters.TodayID, txtChild.Tag.ToString)

        Dim _i As Integer
        For _i = 1 To 12
            Dim _ctrl As Control() = Me.Controls.Find("lblMilk" + _i.ToString, True)
            If _ctrl IsNot Nothing AndAlso _ctrl.Length = 1 Then
                If _ctrl(0).Visible AndAlso _ctrl(0).Tag.ToString <> "" AndAlso _ctrl(0).AccessibleDescription <> "" Then

                    Dim _Floz As Double = ReturnFloz(_ctrl(0).Tag.ToString)

                    SharedModule.LogMilk(Parameters.TodayID, txtChild.Tag.ToString, txtChild.Text, _
                                         _Floz, _ctrl(0).AccessibleDescription, _
                                         txtStaff.Tag.ToString, txtStaff.Text)

                End If
            End If
        Next

    End Sub

    Private Function ReturnFloz(ByVal Floz As String) As Double
        Dim _Return As Double = 0
        If Double.TryParse(Floz, _Return) Then
            Return _Return
        Else
            Return 0
        End If
    End Function

    Private Sub btnAdd_Click(sender As System.Object, e As System.EventArgs) Handles btnAdd.Click

        Dim _Return As String = SharedModule.NumberEntry("Enter Volume in Fluid Ounces", "", True)
        If _Return <> "" Then

            m_MilkCount += 1

            Dim _Floz As String = _Return
            Dim _Time As String = Format(Date.Now, "HH:mm:ss")
            Dim _Desc As String = _Floz + " floz @ " + _Time

            SetControl("lblMilk", m_MilkCount, _Floz, _Desc, _Time, True)
            SetControl("btnMilk", m_MilkCount, "", "Remove", "", True)

        End If

    End Sub

    Protected Overrides Sub DisplayRecord()

        Clear()

        If Not Me.ChildrenPopulated Then Exit Sub
        If Me.ChildID = "" Then Exit Sub

        Dim _SQL As String = "select ID, description, value_2, value_3 from Activity" & _
                             " where Activity.day_id = '" & Parameters.TodayID & "'" & _
                             " and Activity.key_id = '" & txtChild.Tag.ToString & "'" & _
                             " and type = 'MILK'" & _
                             " order by stamp"

        Dim _dt As DataTable = DAL.ReturnDataTable(_SQL)
        If _dt IsNot Nothing Then

            m_MilkCount = 0

            Dim _Desc As String
            Dim _Floz As String
            Dim _Time As String

            For Each Row As DataRow In _dt.Rows

                m_MilkCount += 1

                _Desc = Row.Item("description").ToString
                _Floz = Row.Item("value_2").ToString
                _Time = Row.Item("value_3").ToString

                SetControl("lblMilk", m_MilkCount, Row.Item("value_2"), Row.Item("description").ToString, Row.Item("value_3").ToString, True)
                SetControl("btnMilk", m_MilkCount, "", "Remove", "", True)

            Next

            _dt.Dispose()
            _dt = Nothing

        End If

    End Sub

    Private Sub Clear()

        Dim i As Integer = 1
        For i = 1 To 12
            SetControl("lblMilk", i, "", "", "", False)
            SetControl("btnMilk", i, "", "", "", False)
        Next

    End Sub

    Private Sub SetControl(ByVal ControlName As String, ByVal ControlIndex As Integer, ByVal Tag As String, ByVal Text As String, ByVal AccessibleDescription As String, ByVal Visible As Boolean)

        Dim _name As String = ControlName + ControlIndex.ToString
        Dim _ctrl As Control() = Me.Controls.Find(_name, True)
        If _ctrl.Length = 1 Then
            _ctrl(0).Visible = Visible
            _ctrl(0).Text = Text
            _ctrl(0).Tag = Tag
            _ctrl(0).AccessibleDescription = AccessibleDescription
        End If
    End Sub

    Private Sub btnMilk12_Click(sender As System.Object, e As System.EventArgs) Handles btnMilk12.Click
        SetControl("lblMilk", 12, "", "", "", False)
        SetControl("btnMilk", 12, "", "", "", False)
        SaveAndReDisplay()
    End Sub

    Private Sub btnMilk11_Click(sender As System.Object, e As System.EventArgs) Handles btnMilk11.Click
        SetControl("lblMilk", 11, "", "", "", False)
        SetControl("btnMilk", 11, "", "", "", False)
        SaveAndReDisplay()
    End Sub

    Private Sub btnMilk10_Click(sender As System.Object, e As System.EventArgs) Handles btnMilk10.Click
        SetControl("lblMilk", 10, "", "", "", False)
        SetControl("btnMilk", 10, "", "", "", False)
        SaveAndReDisplay()
    End Sub

    Private Sub btnMilk9_Click(sender As System.Object, e As System.EventArgs) Handles btnMilk9.Click
        SetControl("lblMilk", 9, "", "", "", False)
        SetControl("btnMilk", 9, "", "", "", False)
        SaveAndReDisplay()
    End Sub

    Private Sub btnMilk8_Click(sender As System.Object, e As System.EventArgs) Handles btnMilk8.Click
        SetControl("lblMilk", 8, "", "", "", False)
        SetControl("btnMilk", 8, "", "", "", False)
        SaveAndReDisplay()
    End Sub

    Private Sub btnMilk7_Click(sender As System.Object, e As System.EventArgs) Handles btnMilk7.Click
        SetControl("lblMilk", 7, "", "", "", False)
        SetControl("btnMilk", 7, "", "", "", False)
        SaveAndReDisplay()
    End Sub

    Private Sub btnMilk6_Click(sender As System.Object, e As System.EventArgs) Handles btnMilk6.Click
        SetControl("lblMilk", 6, "", "", "", False)
        SetControl("btnMilk", 6, "", "", "", False)
        SaveAndReDisplay()
    End Sub

    Private Sub btnMilk5_Click(sender As System.Object, e As System.EventArgs) Handles btnMilk5.Click
        SetControl("lblMilk", 5, "", "", "", False)
        SetControl("btnMilk", 5, "", "", "", False)
        SaveAndReDisplay()
    End Sub

    Private Sub btnMilk4_Click(sender As System.Object, e As System.EventArgs) Handles btnMilk4.Click
        SetControl("lblMilk", 4, "", "", "", False)
        SetControl("btnMilk", 4, "", "", "", False)
        SaveAndReDisplay()
    End Sub

    Private Sub btnMilk3_Click(sender As System.Object, e As System.EventArgs) Handles btnMilk3.Click
        SetControl("lblMilk", 3, "", "", "", False)
        SetControl("btnMilk", 3, "", "", "", False)
        SaveAndReDisplay()
    End Sub

    Private Sub btnMilk2_Click(sender As System.Object, e As System.EventArgs) Handles btnMilk2.Click
        SetControl("lblMilk", 2, "", "", "", False)
        SetControl("btnMilk", 2, "", "", "", False)
        SaveAndReDisplay()
    End Sub

    Private Sub btnMilk1_Click(sender As System.Object, e As System.EventArgs) Handles btnMilk1.Click
        SetControl("lblMilk", 1, "", "", "", False)
        SetControl("btnMilk", 1, "", "", "", False)
        SaveAndReDisplay()
    End Sub

    Private Sub SaveAndReDisplay()
        CommitUpdate()
        DisplayRecord()
    End Sub

End Class
