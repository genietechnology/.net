﻿Imports DevExpress.XtraEditors
Imports DevExpress.XtraBars.Navigation

Public Class frmHostingForm

    Private WithEvents m_HostedPanel As BaseHostedPanel
    Private m_FilterChildID As String = ""
    Private m_FilterChildName As String = ""

    Private Enum EnumBar
        Group
        Staff
        Children
    End Enum

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub New(ByVal HostedPanel As BaseHostedPanel, ByVal FilterChildID As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_HostedPanel = HostedPanel
        m_FilterChildID = FilterChildID

    End Sub

    Private Sub frmHostingForm_Load(sender As Object, e As EventArgs) Handles Me.Load

        If m_FilterChildID <> "" Then
            m_FilterChildName = Business.Child.ReturnName(m_FilterChildID)
            tbChildren.Visible = False
            tbGroups.Visible = False
        Else
            PopulateGroups()
        End If

        PopulateStaff()

        ResizeTiles()

        m_HostedPanel.Initialise()

        panHost.Controls.Add(m_HostedPanel)
        panHost.Controls(0).Dock = DockStyle.Fill

        If m_FilterChildID <> "" Then
            m_HostedPanel.OnChildChanged(m_FilterChildID, m_FilterChildName)
        End If

    End Sub

    Private Sub PopulateChildren(ByVal Location As String)

        tbChildGroup.Items.Clear()

        Dim _SQL As String = Business.Child.ReturnCheckedInSQL(Location, Parameters.ShowPhotos)

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If Not _DT Is Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _i As New TileBarItem
                Dim _Colour As Color = Business.Child.ReturnColour(_DR.Item("allergy_rating").ToString, _DR.Item("diet_restrict").ToString)

                With _i
                    .Name = "t_" + _DR.Item("ID").ToString
                    .Text = _DR.Item("name").ToString
                    .AppearanceItem.Normal.ForeColor = Color.Black
                    .AppearanceItem.Normal.FontSizeDelta = 4
                    .AppearanceItem.Normal.FontStyleDelta = FontStyle.Bold
                    .TextAlignment = TileItemContentAlignment.TopLeft
                    AddHandler _i.ItemClick, AddressOf ChildItemClick
                End With

                If _Colour <> Color.Black Then
                    _i.AppearanceItem.Normal.BackColor = _Colour
                End If

                Dim _ml As New TileItemElement
                With _ml
                    .Text = Business.Child.BuildBookingText(_DR.Item("tariff_name").ToString, _DR.Item("timeslots").ToString)
                    .Appearance.Normal.FontSizeDelta = -2
                    .TextAlignment = TileItemContentAlignment.MiddleLeft
                End With
                _i.Elements.Add(_ml)

                Dim _bl As New TileItemElement
                With _bl
                    .Text = Business.Child.BuildAllergyText(_DR.Item("allergy_rating").ToString, _DR.Item("diet_restrict").ToString, True)
                    .Appearance.Normal.FontSizeDelta = -2
                    .TextAlignment = TileItemContentAlignment.BottomLeft
                End With
                _i.Elements.Add(_bl)

                Dim _br As New TileItemElement
                With _br
                    .Text = Business.Child.BuildBookingTimes(_DR.Item("booking_from"), _DR.Item("booking_to"))
                    .Appearance.Normal.FontSizeDelta = -2
                    .TextAlignment = TileItemContentAlignment.BottomRight
                End With
                _i.Elements.Add(_br)

                If Parameters.ShowPhotos Then
                    Dim _tl As New TileItemElement
                    With _tl
                        .Image = DAL.GetImagefromByteArray(_DR.Item("photo"))
                        .ImageAlignment = TileItemContentAlignment.TopRight
                        .ImageScaleMode = TileItemImageScaleMode.ZoomInside
                        .ImageSize = New Size(96, 96)
                    End With
                    _i.Elements.Add(_tl)
                End If

                tbChildGroup.Items.Add(_i)

            Next

            SelectFirstTile(tbChildren, tbChildGroup)

        End If

    End Sub

    Private Sub PopulateStaff()

        Dim _SQL As String = ""

        _SQL += "select person_id as 'ID', person_name as 'name' from RegisterSummary"
        _SQL += " where day_id = '" + SharedModule.TodayID + "'"
        _SQL += " and person_type = 'S'"
        _SQL += " and in_out = 'I'"

        If Parameters.DefaultRoom <> "" Then
            _SQL += " and location = '" + Parameters.DefaultRoom + "'"
        End If

        _SQL += " order by person_name"

        PopulateTileBar(tbStaffGroup, _SQL, EnumBar.Staff)

        If Parameters.CurrentStaffName <> "" Then
            For Each _i As TileBarItem In tbStaffGroup.Items
                Dim _Name As String = _i.Elements(0).Text
                If _Name = Parameters.CurrentStaffName Then
                    tbStaff.SelectedItem = _i
                    _i.PerformItemClick()
                    Exit Sub
                End If
            Next
        Else
            SelectFirstTile(tbStaff, tbStaffGroup)
        End If

    End Sub

    Private Sub PopulateGroups()

        Dim _SQL As String = ""

        _SQL += "SELECT ID, room_name from SiteRooms"
        _SQL += " where site_id = '" + Parameters.SiteID + "'"
        _SQL += " and room_check_children = 1"
        _SQL += " order by room_sequence"

        PopulateTileBar(tbGroupGroup, _SQL, EnumBar.Group)

        If Parameters.DefaultRoom <> "" Then
            For Each _i As TileBarItem In tbGroupGroup.Items
                Dim _Text As String = _i.Elements(0).Text
                If _Text = Parameters.DefaultRoom Then
                    tbGroups.SelectedItem = _i
                    _i.PerformItemClick()
                    Exit Sub
                End If
            Next
        Else
            SelectFirstTile(tbGroups, tbGroupGroup)
        End If

    End Sub

    Private Sub SelectFirstTile(ByRef Bar As TileBar, ByRef BarGroup As TileBarGroup)
        For Each _i As TileBarItem In BarGroup.Items
            Bar.SelectedItem = _i
            _i.PerformItemClick()
            Exit Sub
        Next
    End Sub


    Private Sub PopulateTileBar(ByRef BarGroup As TileBarGroup, ByVal _SQL As String, ByVal Bar As EnumBar)

        BarGroup.Items.Clear()

        If Bar = EnumBar.Group Then m_HostedPanel.ClearSelectedGroup()
        If Bar = EnumBar.Staff Then m_HostedPanel.ClearSelectedStaff()
        If Bar = EnumBar.Children Then m_HostedPanel.ClearSelectedChild()

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _i As New TileBarItem
                With _i

                    .Name = "t_" + _DR.Item("ID").ToString

                    Select Case Bar

                        Case EnumBar.Group

                            AddHandler _i.ItemClick, AddressOf GroupItemClick

                            .TextAlignment = TileItemContentAlignment.TopLeft
                            .AppearanceItem.Normal.ForeColor = Color.Black
                            .AppearanceItem.Normal.BackColor = txtLightYellow.BackColor
                            .AppearanceItem.Selected.BackColor = txtYellow.BackColor
                            .Text = _DR.Item("room_name").ToString

                            '.Image = icAges.Images(_DR.Item("months_to").ToString)
                            '.ImageScaleMode = TileItemImageScaleMode.Squeeze
                            '.ImageToTextAlignment = TileControlImageToTextAlignment.Left

                        Case EnumBar.Staff

                            AddHandler _i.ItemClick, AddressOf StaffItemClick

                            .Text = _DR.Item("name").ToString
                            .AppearanceItem.Normal.BackColor = txtLightPink.BackColor
                            .AppearanceItem.Selected.BackColor = txtPink.BackColor

                            .Elements(0).Appearance.Normal.FontSizeDelta = 1

                        Case EnumBar.Children

                            AddHandler _i.ItemClick, AddressOf ChildItemClick

                            .TextAlignment = TileItemContentAlignment.TopLeft
                            .Text = _DR.Item("name").ToString

                            .AppearanceItem.Normal.BackColor = txtLightGrey.BackColor
                            .AppearanceItem.Selected.BackColor = txtGrey.BackColor

                            .Elements(0).Appearance.Normal.FontSizeDelta = 1

                            Dim _Times As New TileItemElement
                            With _Times
                                .TextAlignment = TileItemContentAlignment.MiddleLeft
                                .Appearance.Normal.FontSizeDelta = -2
                                .Text = "08:00-18:00"
                            End With

                            Dim _Status As New TileItemElement
                            With _Status
                                .TextAlignment = TileItemContentAlignment.BottomRight
                                .Appearance.Normal.FontSizeDelta = -2
                                .Text = "AG:Medium | DR"
                            End With

                            _i.Elements.Add(_Times)
                            _i.Elements.Add(_Status)

                    End Select

                End With

                BarGroup.Items.Add(_i)

            Next

        End If


    End Sub

    Private Sub GroupItemClick(sender As Object, e As TileItemEventArgs)
        'If Not m_HostedPanel.Initialised Then Exit Sub
        Dim _id As String = e.Item.Name.Substring(2)
        m_HostedPanel.OnGroupChanged(_id, e.Item.Text)
    End Sub

    Private Sub ChildItemClick(sender As Object, e As TileItemEventArgs)
        'If Not m_HostedPanel.Initialised Then Exit Sub
        Dim _id As String = e.Item.Name.Substring(2)
        m_HostedPanel.OnChildChanged(_id, e.Item.Text)
    End Sub

    Private Sub StaffItemClick(sender As Object, e As TileItemEventArgs)
        'If Not m_HostedPanel.Initialised Then Exit Sub
        Dim _id As String = e.Item.Name.Substring(2)
        m_HostedPanel.OnStaffChanged(_id, e.Item.Text)
    End Sub

    Private Sub frmBaseMax_ResizeEnd(sender As Object, e As EventArgs) Handles Me.ResizeEnd
        ResizeTiles()
    End Sub

    Private Sub ResizeTiles()
        tbChildren.WideTileWidth = tbChildren.Width - 50
        tbStaff.WideTileWidth = tbStaff.Width - 50
    End Sub

    Private Sub m_HostedPanel_Accepted(sender As Object, e As EventArgs) Handles m_HostedPanel.Accepted
        Me.DialogResult = DialogResult.OK
        Me.Close()
    End Sub

    Private Sub m_HostedPanel_GroupChanged(sender As Object, e As RecordChangedArgs) Handles m_HostedPanel.GroupChanged
        PopulateChildren(e.ValueText)
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click
        m_HostedPanel.OnAcceptClicked()
    End Sub

End Class

