﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCheckOut
    Inherits NurseryTouchscreen.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling2 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling3 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCheckOut))
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.txtCheckedBy = New DevExpress.XtraEditors.TextEdit()
        Me.txtChild = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.txtFeedback = New DevExpress.XtraEditors.MemoEdit()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.tgSibblings = New NurseryTouchscreen.TouchGrid()
        Me.gbxFeedback = New DevExpress.XtraEditors.GroupControl()
        Me.gbxRequests = New DevExpress.XtraEditors.GroupControl()
        Me.tgReq = New NurseryTouchscreen.TouchGrid()
        Me.gbxMedication = New DevExpress.XtraEditors.GroupControl()
        Me.tgMedication = New NurseryTouchscreen.TouchGrid()
        Me.gbxIncidents = New DevExpress.XtraEditors.GroupControl()
        Me.tgIncidents = New NurseryTouchscreen.TouchGrid()
        Me.gbxMilk = New DevExpress.XtraEditors.GroupControl()
        Me.tgMilk = New NurseryTouchscreen.TouchGrid()
        Me.gbxSleep = New DevExpress.XtraEditors.GroupControl()
        Me.tgSleep = New NurseryTouchscreen.TouchGrid()
        Me.gbxToilet = New DevExpress.XtraEditors.GroupControl()
        Me.tgToilet = New NurseryTouchscreen.TouchGrid()
        Me.panBottom = New DevExpress.XtraEditors.PanelControl()
        Me.btnFood = New DevExpress.XtraEditors.SimpleButton()
        Me.btnReport = New DevExpress.XtraEditors.SimpleButton()
        Me.Splitter2 = New System.Windows.Forms.Splitter()
        Me.btnAccept = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl7 = New DevExpress.XtraEditors.GroupControl()
        Me.gbxSuncream = New DevExpress.XtraEditors.GroupControl()
        Me.tgSuncream = New NurseryTouchscreen.TouchGrid()
        CType(Me.txtCheckedBy.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtChild.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFeedback.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.gbxFeedback, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxFeedback.SuspendLayout()
        CType(Me.gbxRequests, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxRequests.SuspendLayout()
        CType(Me.gbxMedication, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxMedication.SuspendLayout()
        CType(Me.gbxIncidents, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxIncidents.SuspendLayout()
        CType(Me.gbxMilk, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxMilk.SuspendLayout()
        CType(Me.gbxSleep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxSleep.SuspendLayout()
        CType(Me.gbxToilet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxToilet.SuspendLayout()
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panBottom.SuspendLayout()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl7.SuspendLayout()
        CType(Me.gbxSuncream, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxSuncream.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'LabelControl17
        '
        Me.LabelControl17.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl17.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl17.Location = New System.Drawing.Point(977, 13)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(108, 25)
        Me.LabelControl17.TabIndex = 91
        Me.LabelControl17.Text = "Checked By:"
        '
        'txtCheckedBy
        '
        Me.txtCheckedBy.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCheckedBy.Location = New System.Drawing.Point(1091, 10)
        Me.txtCheckedBy.Name = "txtCheckedBy"
        Me.txtCheckedBy.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCheckedBy.Properties.Appearance.Options.UseFont = True
        Me.txtCheckedBy.Properties.AutoHeight = False
        Me.txtCheckedBy.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtCheckedBy, True)
        Me.txtCheckedBy.Size = New System.Drawing.Size(276, 32)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtCheckedBy, OptionsSpelling1)
        Me.txtCheckedBy.TabIndex = 90
        '
        'txtChild
        '
        Me.txtChild.Location = New System.Drawing.Point(60, 10)
        Me.txtChild.Name = "txtChild"
        Me.txtChild.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtChild.Properties.Appearance.Options.UseFont = True
        Me.txtChild.Properties.AutoHeight = False
        Me.txtChild.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtChild, True)
        Me.txtChild.Size = New System.Drawing.Size(417, 32)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtChild, OptionsSpelling2)
        Me.txtChild.TabIndex = 79
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(10, 13)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(44, 25)
        Me.LabelControl2.TabIndex = 78
        Me.LabelControl2.Text = "Child"
        '
        'txtFeedback
        '
        Me.txtFeedback.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtFeedback.Location = New System.Drawing.Point(2, 20)
        Me.txtFeedback.Name = "txtFeedback"
        Me.txtFeedback.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFeedback.Properties.Appearance.Options.UseFont = True
        Me.txtFeedback.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtFeedback, True)
        Me.txtFeedback.Size = New System.Drawing.Size(448, 175)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtFeedback, OptionsSpelling3)
        Me.txtFeedback.TabIndex = 89
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334!))
        Me.TableLayoutPanel1.Controls.Add(Me.gbxSuncream, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.GroupControl1, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.gbxFeedback, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.gbxRequests, 2, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.gbxMedication, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.gbxIncidents, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.gbxMilk, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.gbxSleep, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.gbxToilet, 0, 1)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(8, 70)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1377, 677)
        Me.TableLayoutPanel1.TabIndex = 92
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.tgSibblings)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl1.Location = New System.Drawing.Point(920, 3)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(454, 197)
        Me.GroupControl1.TabIndex = 99
        Me.GroupControl1.Text = "Sibblings Checked-In"
        '
        'tgSibblings
        '
        Me.tgSibblings.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tgSibblings.HideFirstColumn = False
        Me.tgSibblings.Location = New System.Drawing.Point(2, 20)
        Me.tgSibblings.Name = "tgSibblings"
        Me.tgSibblings.Size = New System.Drawing.Size(450, 175)
        Me.tgSibblings.TabIndex = 1
        '
        'gbxFeedback
        '
        Me.gbxFeedback.Controls.Add(Me.txtFeedback)
        Me.gbxFeedback.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxFeedback.Location = New System.Drawing.Point(3, 3)
        Me.gbxFeedback.Name = "gbxFeedback"
        Me.gbxFeedback.Size = New System.Drawing.Size(452, 197)
        Me.gbxFeedback.TabIndex = 95
        Me.gbxFeedback.Text = "Personal Feedback"
        '
        'gbxRequests
        '
        Me.gbxRequests.Controls.Add(Me.tgReq)
        Me.gbxRequests.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxRequests.Location = New System.Drawing.Point(920, 442)
        Me.gbxRequests.Name = "gbxRequests"
        Me.gbxRequests.Size = New System.Drawing.Size(454, 232)
        Me.gbxRequests.TabIndex = 98
        '
        'tgReq
        '
        Me.tgReq.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tgReq.HideFirstColumn = False
        Me.tgReq.Location = New System.Drawing.Point(2, 20)
        Me.tgReq.Name = "tgReq"
        Me.tgReq.Size = New System.Drawing.Size(450, 210)
        Me.tgReq.TabIndex = 1
        '
        'gbxMedication
        '
        Me.gbxMedication.Controls.Add(Me.tgMedication)
        Me.gbxMedication.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxMedication.Location = New System.Drawing.Point(461, 442)
        Me.gbxMedication.Name = "gbxMedication"
        Me.gbxMedication.Size = New System.Drawing.Size(453, 232)
        Me.gbxMedication.TabIndex = 97
        '
        'tgMedication
        '
        Me.tgMedication.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tgMedication.HideFirstColumn = False
        Me.tgMedication.Location = New System.Drawing.Point(2, 20)
        Me.tgMedication.Name = "tgMedication"
        Me.tgMedication.Size = New System.Drawing.Size(449, 210)
        Me.tgMedication.TabIndex = 1
        '
        'gbxIncidents
        '
        Me.gbxIncidents.Controls.Add(Me.tgIncidents)
        Me.gbxIncidents.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxIncidents.Location = New System.Drawing.Point(3, 442)
        Me.gbxIncidents.Name = "gbxIncidents"
        Me.gbxIncidents.Size = New System.Drawing.Size(452, 232)
        Me.gbxIncidents.TabIndex = 96
        '
        'tgIncidents
        '
        Me.tgIncidents.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tgIncidents.HideFirstColumn = False
        Me.tgIncidents.Location = New System.Drawing.Point(2, 20)
        Me.tgIncidents.Name = "tgIncidents"
        Me.tgIncidents.Size = New System.Drawing.Size(448, 210)
        Me.tgIncidents.TabIndex = 1
        '
        'gbxMilk
        '
        Me.gbxMilk.Controls.Add(Me.tgMilk)
        Me.gbxMilk.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxMilk.Location = New System.Drawing.Point(461, 206)
        Me.gbxMilk.Name = "gbxMilk"
        Me.gbxMilk.Size = New System.Drawing.Size(453, 230)
        Me.gbxMilk.TabIndex = 95
        '
        'tgMilk
        '
        Me.tgMilk.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tgMilk.HideFirstColumn = False
        Me.tgMilk.Location = New System.Drawing.Point(2, 20)
        Me.tgMilk.Name = "tgMilk"
        Me.tgMilk.Size = New System.Drawing.Size(449, 208)
        Me.tgMilk.TabIndex = 1
        '
        'gbxSleep
        '
        Me.gbxSleep.Controls.Add(Me.tgSleep)
        Me.gbxSleep.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxSleep.Location = New System.Drawing.Point(461, 3)
        Me.gbxSleep.Name = "gbxSleep"
        Me.gbxSleep.Size = New System.Drawing.Size(453, 197)
        Me.gbxSleep.TabIndex = 94
        '
        'tgSleep
        '
        Me.tgSleep.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tgSleep.HideFirstColumn = False
        Me.tgSleep.Location = New System.Drawing.Point(2, 20)
        Me.tgSleep.Name = "tgSleep"
        Me.tgSleep.Size = New System.Drawing.Size(449, 175)
        Me.tgSleep.TabIndex = 1
        '
        'gbxToilet
        '
        Me.gbxToilet.Controls.Add(Me.tgToilet)
        Me.gbxToilet.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxToilet.Location = New System.Drawing.Point(3, 206)
        Me.gbxToilet.Name = "gbxToilet"
        Me.gbxToilet.Size = New System.Drawing.Size(452, 230)
        Me.gbxToilet.TabIndex = 93
        '
        'tgToilet
        '
        Me.tgToilet.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tgToilet.HideFirstColumn = False
        Me.tgToilet.Location = New System.Drawing.Point(2, 20)
        Me.tgToilet.Name = "tgToilet"
        Me.tgToilet.Size = New System.Drawing.Size(448, 208)
        Me.tgToilet.TabIndex = 1
        '
        'panBottom
        '
        Me.panBottom.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panBottom.Appearance.Options.UseFont = True
        Me.panBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.panBottom.Controls.Add(Me.btnFood)
        Me.panBottom.Controls.Add(Me.btnReport)
        Me.panBottom.Controls.Add(Me.Splitter2)
        Me.panBottom.Controls.Add(Me.btnAccept)
        Me.panBottom.Controls.Add(Me.btnCancel)
        Me.panBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panBottom.Location = New System.Drawing.Point(0, 750)
        Me.panBottom.Name = "panBottom"
        Me.panBottom.Size = New System.Drawing.Size(1395, 56)
        Me.panBottom.TabIndex = 93
        '
        'btnFood
        '
        Me.btnFood.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFood.Appearance.Options.UseFont = True
        Me.btnFood.Image = Global.NurseryTouchscreen.My.Resources.Resources.food_32
        Me.btnFood.Location = New System.Drawing.Point(10, 3)
        Me.btnFood.Name = "btnFood"
        Me.btnFood.Size = New System.Drawing.Size(192, 45)
        Me.btnFood.TabIndex = 3
        Me.btnFood.Text = "Show Food"
        '
        'btnReport
        '
        Me.btnReport.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReport.Appearance.Options.UseFont = True
        Me.btnReport.Image = Global.NurseryTouchscreen.My.Resources.Resources.report_32
        Me.btnReport.Location = New System.Drawing.Point(208, 3)
        Me.btnReport.Name = "btnReport"
        Me.btnReport.Size = New System.Drawing.Size(192, 45)
        Me.btnReport.TabIndex = 2
        Me.btnReport.Text = "View Report"
        '
        'Splitter2
        '
        Me.Splitter2.Location = New System.Drawing.Point(0, 0)
        Me.Splitter2.Name = "Splitter2"
        Me.Splitter2.Size = New System.Drawing.Size(3, 56)
        Me.Splitter2.TabIndex = 0
        Me.Splitter2.TabStop = False
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAccept.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAccept.Appearance.Options.UseFont = True
        Me.btnAccept.Image = CType(resources.GetObject("btnAccept.Image"), System.Drawing.Image)
        Me.btnAccept.Location = New System.Drawing.Point(1037, 3)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(170, 45)
        Me.btnAccept.TabIndex = 0
        Me.btnAccept.Text = "Check Out"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.DialogResult = DialogResult.Cancel
        Me.btnCancel.Image = CType(resources.GetObject("btnCancel.Image"), System.Drawing.Image)
        Me.btnCancel.Location = New System.Drawing.Point(1213, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(170, 45)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "Cancel"
        '
        'GroupControl7
        '
        Me.GroupControl7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl7.Controls.Add(Me.LabelControl2)
        Me.GroupControl7.Controls.Add(Me.txtChild)
        Me.GroupControl7.Controls.Add(Me.txtCheckedBy)
        Me.GroupControl7.Controls.Add(Me.LabelControl17)
        Me.GroupControl7.Location = New System.Drawing.Point(8, 12)
        Me.GroupControl7.Name = "GroupControl7"
        Me.GroupControl7.ShowCaption = False
        Me.GroupControl7.Size = New System.Drawing.Size(1377, 52)
        Me.GroupControl7.TabIndex = 94
        Me.GroupControl7.Text = "GroupControl7"
        '
        'gbxSuncream
        '
        Me.gbxSuncream.Controls.Add(Me.tgSuncream)
        Me.gbxSuncream.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxSuncream.Location = New System.Drawing.Point(920, 206)
        Me.gbxSuncream.Name = "gbxSuncream"
        Me.gbxSuncream.Size = New System.Drawing.Size(454, 230)
        Me.gbxSuncream.TabIndex = 100
        '
        'tgSuncream
        '
        Me.tgSuncream.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tgSuncream.HideFirstColumn = False
        Me.tgSuncream.Location = New System.Drawing.Point(2, 20)
        Me.tgSuncream.Name = "tgSuncream"
        Me.tgSuncream.Size = New System.Drawing.Size(450, 208)
        Me.tgSuncream.TabIndex = 1
        '
        'frmCheckOut
        '
        Me.AcceptButton = Me.btnAccept
        Me.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.Appearance.Options.UseBackColor = True
        Me.ClientSize = New System.Drawing.Size(1395, 806)
        Me.Controls.Add(Me.GroupControl7)
        Me.Controls.Add(Me.panBottom)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmCheckOut"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Controls.SetChildIndex(Me.TableLayoutPanel1, 0)
        Me.Controls.SetChildIndex(Me.panBottom, 0)
        Me.Controls.SetChildIndex(Me.GroupControl7, 0)
        CType(Me.txtCheckedBy.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtChild.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFeedback.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.gbxFeedback, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxFeedback.ResumeLayout(False)
        CType(Me.gbxRequests, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxRequests.ResumeLayout(False)
        CType(Me.gbxMedication, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxMedication.ResumeLayout(False)
        CType(Me.gbxIncidents, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxIncidents.ResumeLayout(False)
        CType(Me.gbxMilk, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxMilk.ResumeLayout(False)
        CType(Me.gbxSleep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxSleep.ResumeLayout(False)
        CType(Me.gbxToilet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxToilet.ResumeLayout(False)
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panBottom.ResumeLayout(False)
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl7.ResumeLayout(False)
        Me.GroupControl7.PerformLayout()
        CType(Me.gbxSuncream, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxSuncream.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtCheckedBy As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtChild As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtFeedback As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents gbxRequests As DevExpress.XtraEditors.GroupControl
    Friend WithEvents gbxMedication As DevExpress.XtraEditors.GroupControl
    Friend WithEvents gbxIncidents As DevExpress.XtraEditors.GroupControl
    Friend WithEvents gbxMilk As DevExpress.XtraEditors.GroupControl
    Protected Friend WithEvents panBottom As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnReport As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Splitter2 As System.Windows.Forms.Splitter
    Friend WithEvents btnAccept As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tgReq As NurseryTouchscreen.TouchGrid
    Friend WithEvents tgMedication As NurseryTouchscreen.TouchGrid
    Friend WithEvents tgIncidents As NurseryTouchscreen.TouchGrid
    Friend WithEvents tgMilk As NurseryTouchscreen.TouchGrid
    Friend WithEvents btnFood As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl7 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents gbxFeedback As DevExpress.XtraEditors.GroupControl
    Friend WithEvents gbxToilet As DevExpress.XtraEditors.GroupControl
    Friend WithEvents tgToilet As NurseryTouchscreen.TouchGrid
    Friend WithEvents gbxSleep As DevExpress.XtraEditors.GroupControl
    Friend WithEvents tgSleep As NurseryTouchscreen.TouchGrid
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents tgSibblings As TouchGrid
    Friend WithEvents gbxSuncream As DevExpress.XtraEditors.GroupControl
    Friend WithEvents tgSuncream As TouchGrid
End Class
