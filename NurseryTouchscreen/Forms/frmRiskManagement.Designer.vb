﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRiskManagement
    Inherits NurseryTouchscreen.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRiskManagement))
        Me.tgRisks = New NurseryTouchscreen.TouchGrid()
        Me.tgItems = New NurseryTouchscreen.TouchGrid()
        Me.btnClose = New DevExpress.XtraEditors.SimpleButton()
        Me.btnRiskAdd = New DevExpress.XtraEditors.SimpleButton()
        Me.btnRiskEdit = New DevExpress.XtraEditors.SimpleButton()
        Me.btnRiskDelete = New DevExpress.XtraEditors.SimpleButton()
        Me.btnItemDelete = New DevExpress.XtraEditors.SimpleButton()
        Me.btnItemEdit = New DevExpress.XtraEditors.SimpleButton()
        Me.btnItemAdd = New DevExpress.XtraEditors.SimpleButton()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'tgRisks
        '
        Me.tgRisks.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.tgRisks.HideFirstColumn = False
        Me.tgRisks.Location = New System.Drawing.Point(8, 12)
        Me.tgRisks.Name = "tgRisks"
        Me.tgRisks.Size = New System.Drawing.Size(513, 463)
        Me.tgRisks.TabIndex = 78
        '
        'tgItems
        '
        Me.tgItems.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tgItems.HideFirstColumn = False
        Me.tgItems.Location = New System.Drawing.Point(527, 12)
        Me.tgItems.Name = "tgItems"
        Me.tgItems.Size = New System.Drawing.Size(478, 463)
        Me.tgItems.TabIndex = 79
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.DialogResult = DialogResult.Cancel
        Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
        Me.btnClose.Location = New System.Drawing.Point(835, 481)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(170, 45)
        Me.btnClose.TabIndex = 83
        Me.btnClose.Text = "Close"
        '
        'btnRiskAdd
        '
        Me.btnRiskAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRiskAdd.Image = Global.NurseryTouchscreen.My.Resources.Resources.add_32
        Me.btnRiskAdd.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnRiskAdd.Location = New System.Drawing.Point(8, 481)
        Me.btnRiskAdd.Name = "btnRiskAdd"
        Me.btnRiskAdd.Size = New System.Drawing.Size(45, 45)
        Me.btnRiskAdd.TabIndex = 80
        '
        'btnRiskEdit
        '
        Me.btnRiskEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRiskEdit.Image = Global.NurseryTouchscreen.My.Resources.Resources.Pencil_32
        Me.btnRiskEdit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnRiskEdit.Location = New System.Drawing.Point(59, 481)
        Me.btnRiskEdit.Name = "btnRiskEdit"
        Me.btnRiskEdit.Size = New System.Drawing.Size(45, 45)
        Me.btnRiskEdit.TabIndex = 84
        '
        'btnRiskDelete
        '
        Me.btnRiskDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRiskDelete.Image = Global.NurseryTouchscreen.My.Resources.Resources.delete_32
        Me.btnRiskDelete.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnRiskDelete.Location = New System.Drawing.Point(110, 481)
        Me.btnRiskDelete.Name = "btnRiskDelete"
        Me.btnRiskDelete.Size = New System.Drawing.Size(45, 45)
        Me.btnRiskDelete.TabIndex = 85
        Me.btnRiskDelete.Visible = False
        '
        'btnItemDelete
        '
        Me.btnItemDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnItemDelete.Image = Global.NurseryTouchscreen.My.Resources.Resources.delete_32
        Me.btnItemDelete.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnItemDelete.Location = New System.Drawing.Point(629, 481)
        Me.btnItemDelete.Name = "btnItemDelete"
        Me.btnItemDelete.Size = New System.Drawing.Size(45, 45)
        Me.btnItemDelete.TabIndex = 88
        Me.btnItemDelete.Visible = False
        '
        'btnItemEdit
        '
        Me.btnItemEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnItemEdit.Image = Global.NurseryTouchscreen.My.Resources.Resources.Pencil_32
        Me.btnItemEdit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnItemEdit.Location = New System.Drawing.Point(578, 481)
        Me.btnItemEdit.Name = "btnItemEdit"
        Me.btnItemEdit.Size = New System.Drawing.Size(45, 45)
        Me.btnItemEdit.TabIndex = 87
        '
        'btnItemAdd
        '
        Me.btnItemAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnItemAdd.Image = Global.NurseryTouchscreen.My.Resources.Resources.add_32
        Me.btnItemAdd.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnItemAdd.Location = New System.Drawing.Point(527, 481)
        Me.btnItemAdd.Name = "btnItemAdd"
        Me.btnItemAdd.Size = New System.Drawing.Size(45, 45)
        Me.btnItemAdd.TabIndex = 86
        '
        'frmRiskManagement
        '
        Me.ClientSize = New System.Drawing.Size(1017, 538)
        Me.Controls.Add(Me.btnItemDelete)
        Me.Controls.Add(Me.btnItemEdit)
        Me.Controls.Add(Me.btnItemAdd)
        Me.Controls.Add(Me.btnRiskDelete)
        Me.Controls.Add(Me.btnRiskEdit)
        Me.Controls.Add(Me.btnRiskAdd)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.tgItems)
        Me.Controls.Add(Me.tgRisks)
        Me.Name = "frmRiskManagement"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Controls.SetChildIndex(Me.tgRisks, 0)
        Me.Controls.SetChildIndex(Me.tgItems, 0)
        Me.Controls.SetChildIndex(Me.btnClose, 0)
        Me.Controls.SetChildIndex(Me.btnRiskAdd, 0)
        Me.Controls.SetChildIndex(Me.btnRiskEdit, 0)
        Me.Controls.SetChildIndex(Me.btnRiskDelete, 0)
        Me.Controls.SetChildIndex(Me.btnItemAdd, 0)
        Me.Controls.SetChildIndex(Me.btnItemEdit, 0)
        Me.Controls.SetChildIndex(Me.btnItemDelete, 0)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents tgRisks As TouchGrid
    Friend WithEvents tgItems As TouchGrid
    Friend WithEvents btnClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRiskAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRiskEdit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRiskDelete As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnItemDelete As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnItemEdit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnItemAdd As DevExpress.XtraEditors.SimpleButton
End Class
