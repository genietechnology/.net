﻿Imports NurseryTouchscreen.SharedModule
Imports System.Drawing

Public Class frmChildObs

    Private m_ChildID As String = ""
    
    Public Sub New(ByVal ChildID As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_ChildID = ChildID

    End Sub

    Private Sub frmChildObs_Load(sender As Object, e As EventArgs) Handles Me.Load
        DisplayRecord()
    End Sub

    Private Sub DisplayRecord()

        Dim _SQL As String = ""

        _SQL += "select o.ID, o.title as 'Title', o.comments as 'Comments',"
        _SQL += " o.stamp as 'Observed', a.id as 'ass_id', a.stamp as 'Assessed'"
        _SQL += " from ObsChildren c"
        _SQL += " left join Obs o on o.ID = c.obs_id"
        _SQL += " left join Ass a on a.obs_id = o.ID"
        _SQL += " where o.day_id = '" + Parameters.TodayID + "'"
        _SQL += " and c.child_id = '" + m_ChildID + "'"

        tgObs.Populate(_SQL)

        tgObs.Columns("ID").Visible = False
        tgObs.Columns("Comments").Visible = False
        tgObs.Columns("ass_id").Visible = False

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnObs_Click(sender As Object, e As EventArgs) Handles btnObs.Click
        ShowObservation()
    End Sub

    Private Sub btnAss_Click(sender As Object, e As EventArgs) Handles btnAss.Click

        If tgObs.CurrentRow Is Nothing Then Exit Sub

        btnAss.Enabled = False
        Me.Cursor = Cursors.AppStarting

        Dim _KeyType As frmEYFSAss.EnumKeyType
        Dim _KeyID As String = ""

        Dim _Obs As String = tgObs.CurrentRow("ID").ToString
        Dim _Ass As String = tgObs.CurrentRow("ass_id").ToString

        If _Ass = "" Then
            _KeyType = frmEYFSAss.EnumKeyType.Observation
            _KeyID = _Obs
        Else
            _KeyType = frmEYFSAss.EnumKeyType.Assessment
            _KeyID = _Ass
        End If

        Dim _frm As New frmEYFSAss(m_ChildID, _KeyType, _KeyID)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then



        End If

        Me.Cursor = Cursors.Default
        btnAss.Enabled = True

    End Sub

    Private Sub tgObs_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles tgObs.FocusedRowChanged
        txtComments.Text = tgObs.Row(e.FocusedRowHandle).Item("Comments").ToString
    End Sub

    Private Sub tgObs_GridDoubleClick(sender As Object, e As EventArgs) Handles tgObs.GridDoubleClick
        ShowObservation()
    End Sub

    Private Sub ShowObservation()

        If tgObs.CurrentRow Is Nothing Then Exit Sub
        Dim _Obs As String = tgObs.CurrentRow("ID").ToString

        btnObs.Enabled = False
        Me.Cursor = Cursors.AppStarting

        Dim _frm As New frmObs(New Guid(_Obs))
        _frm.ShowDialog()

        Me.Cursor = Cursors.Default

        btnObs.Enabled = True

    End Sub

End Class
