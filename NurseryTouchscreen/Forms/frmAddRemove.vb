﻿Imports NurseryTouchscreen.SharedModule
Imports System.Drawing

Public Class frmAddRemove

    Private m_GridSQL As String = ""
    Private m_ReturnID As String = ""
    Private m_ReturnMode As Enums.AddRemoveReturnMode = Enums.AddRemoveReturnMode.NotSet

    Public Sub New(ByVal GridSQL As String, ByVal AddText As String, ByVal RemoveText As String, ByVal FormCaption As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_GridSQL = GridSQL
        btnAdd.Text = AddText
        btnRemove.Text = RemoveText
        Me.Text = FormCaption + " (Double-Tap to View Detail)"

    End Sub

    Public Sub New(ByVal GridSQL As String, ByVal FormCaption As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_GridSQL = GridSQL
        btnAdd.Visible = False
        btnRemove.Visible = False
        Me.Text = FormCaption

    End Sub

    Public Property DisableTimeFormatting As Boolean

    Public ReadOnly Property ReturnMode As Enums.AddRemoveReturnMode
        Get
            Return m_ReturnMode
        End Get
    End Property

    Public ReadOnly Property ReturnID As String
        Get
            Return m_ReturnID
        End Get
    End Property

    Private Sub frmAddRemove_Load(sender As Object, e As EventArgs) Handles Me.Load
        DisplayRecord()
    End Sub

    Private Sub DisplayRecord()
        If m_GridSQL = "" Then Exit Sub
        tgRecords.HideFirstColumn = True
        tgRecords.DisableTimeFormatting = Me.DisableTimeFormatting
        tgRecords.Populate(m_GridSQL)
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click

        m_ReturnMode = Enums.AddRemoveReturnMode.NotSet
        m_ReturnID = ""

        Me.DialogResult = DialogResult.Cancel
        Me.Close()

    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click

        m_ReturnMode = Enums.AddRemoveReturnMode.AddRecord
        m_ReturnID = ""

        Me.DialogResult = DialogResult.OK
        Me.Close()

    End Sub

    Private Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click

        If tgRecords.CurrentRow Is Nothing Then Exit Sub

        If Msgbox("Are you sure you want to Remove this Record?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Remove Record") = DialogResult.Yes Then

            m_ReturnMode = Enums.AddRemoveReturnMode.RemoveRecord
            m_ReturnID = tgRecords.CurrentRow("ID").ToString

            Me.DialogResult = DialogResult.OK
            Me.Close()

        End If

    End Sub

    Private Sub EditRecord()

        If tgRecords.CurrentRow Is Nothing Then Exit Sub

        m_ReturnMode = Enums.AddRemoveReturnMode.EditRecord
        m_ReturnID = tgRecords.CurrentRow("ID").ToString

        Me.DialogResult = DialogResult.OK
        Me.Close()

    End Sub

    Private Sub tgRecords_GridDoubleClick(sender As Object, e As EventArgs) Handles tgRecords.GridDoubleClick
        EditRecord()
    End Sub

End Class
