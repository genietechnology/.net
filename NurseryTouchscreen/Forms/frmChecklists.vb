﻿Option Strict On

Imports DevExpress.XtraBars.Navigation
Imports DevExpress.XtraEditors
Imports NurseryTouchscreen.SharedModule

Public Class frmChecklists

    Private m_SelectedListID As String = ""
    Private m_SelectedListName As String = ""

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub frmSleepManager_Load(sender As Object, e As EventArgs) Handles Me.Load
        PopulateCheckLists()
    End Sub

    Private Sub PopulateQuestions()

        Me.Cursor = Cursors.WaitCursor
        Application.DoEvents()

        sc.Controls.Clear()

        Dim _SQL As String = ""
        _SQL += "select id, description, item_type"
        _SQL += " from CheckListItems"
        _SQL += " where list_id = '" + m_SelectedListID + "'"
        _SQL += " order by sequence desc, description desc"

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If _DT IsNot Nothing Then

            'get response header





            For Each _DR As DataRow In _DT.Rows

                Dim _ID As String = _DR.Item("ID").ToString
                Dim _QuestionText As String = _DR.Item("description").ToString
                Dim _QuestionType As String = _DR.Item("item_type").ToString

                Dim _ResponseID As String = ""
                Dim _Answer As String = ""
                Dim _Comments As String = ""

                AddQuestion(_ID, _QuestionText, _QuestionType, _ResponseID, _Answer, _Comments)

            Next

        End If

        Me.Cursor = Cursors.Default
        Application.DoEvents()

    End Sub

    Private Sub AddQuestion(ByVal QuestionID As String, ByVal QuestionText As String, ByVal QuestionType As String, ByVal ResponseID As String, ByVal ResponseAnswer As String, ByVal ResponseComments As String)
        Dim _Item As New QuestionControl(QuestionID, QuestionText, QuestionType, ResponseID, ResponseAnswer, ResponseComments)
        _Item.Dock = DockStyle.Top
        _Item.BringToFront()
        sc.Controls.Add(_Item)
    End Sub

    Private Sub PopulateCheckLists()

        Cursor.Current = Cursors.WaitCursor
        Application.DoEvents()

        Dim _SQL As String = ""

        If radPending.Checked Then
            _SQL += "SELECT l.ID, chk_name from CheckLists l"
            _SQL += " where chk_status = 'Active'"
            _SQL += " and (l.chk_scope_site_name = '" & Parameters.Site & "' or l.chk_scope_site_name = 'All Sites')"
            _SQL += " and l.ID not in (select list_id from CheckListResp where day_id = '" + Parameters.TodayID + "' and list_id = l.ID)"
            _SQL += " order by chk_name"
        Else
            _SQL += "SELECT l.ID, chk_name from CheckLists l"
            _SQL += " where chk_status = 'Active'"
            _SQL += " and l.scope_site_name = '" & Parameters.Site & "' or l.scope_site_name = 'All Sites'"
            _SQL += " and l.ID in (select list_id from CheckListResp where day_id = '" + Parameters.TodayID + "' and list_id = l.ID)"
            _SQL += " order by chk_name"
        End If

        PopulateTileBar(_SQL)

        If tbListGroup.Items.Count > 0 Then
            For Each _i As TileBarItem In tbListGroup.Items
                tbLists.SelectedItem = _i
                _i.PerformItemClick()
                Exit Sub
            Next
        Else
            sc.Controls.Clear()
        End If

        Me.Cursor = Cursors.Default
        Application.DoEvents()

    End Sub

    Private Sub PopulateTileBar(ByVal _SQL As String)

        tbListGroup.Items.Clear()

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _i As New TileBarItem
                With _i

                    .Name = "t_" + _DR.Item("ID").ToString

                    AddHandler _i.ItemClick, AddressOf GroupItemClick

                    .TextAlignment = TileItemContentAlignment.TopLeft
                    .AppearanceItem.Normal.ForeColor = Color.Black
                    .AppearanceItem.Normal.BackColor = txtLightYellow.BackColor
                    .AppearanceItem.Selected.BackColor = txtYellow.BackColor
                    .Text = _DR.Item("chk_name").ToString

                End With

                tbListGroup.Items.Add(_i)

            Next

        End If

    End Sub

    Private Sub ChildClick(sender As Object, e As TileItemEventArgs)
        If e.Item.Checked Then
            e.Item.Checked = False
        Else
            e.Item.Checked = True
        End If
    End Sub

    Private Sub GroupItemClick(sender As Object, e As TileItemEventArgs)
        m_SelectedListID = e.Item.Name.Substring(2)
        m_SelectedListName = e.Item.Text
        PopulateQuestions()
    End Sub

    Private Sub frmSleepManager_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        tbLists.WideTileWidth = tbLists.Width - 40
    End Sub

    Private Sub radPending_CheckedChanged(sender As Object, e As EventArgs) Handles radPending.CheckedChanged
        If radPending.Checked Then PopulateCheckLists()
    End Sub

    Private Sub radComplete_CheckedChanged(sender As Object, e As EventArgs) Handles radComplete.CheckedChanged
        If radComplete.Checked Then PopulateCheckLists()
    End Sub

    Private Sub btnComplete_Click(sender As Object, e As EventArgs) Handles btnComplete.Click

        Dim _ResponseID As String = Guid.NewGuid.ToString

        Dim _Sig As New Signature
        _Sig.CaptureStaffSignature(False)

        If _Sig.SignatureCaptured Then
            StoreResponseHeader(_ResponseID, _Sig)
            StoreAnswers(_ResponseID)
        End If

        PopulateCheckLists()

    End Sub

    Private Sub StoreResponseHeader(ByVal ResponseID As String, ByVal Sig As Signature)

        Dim _SQL As String = "select * from CheckListResp where ID = '" & ResponseID & "'"

        Dim _DA As IDataAdapter = DAL.ReturnDataAdapter(_SQL)
        If Not _DA Is Nothing Then

            Dim _DR As DataRow
            Dim _DS As New DataSet
            _DA.Fill(_DS)

            _DR = _DS.Tables(0).NewRow
            _DR("id") = New Guid(ResponseID)
            _DR("day_id") = New Guid(TodayID)
            _DR("date") = Today
            _DR("list_id") = New Guid(m_SelectedListID)
            _DR("list_name") = m_SelectedListName

            If Sig.SignatureCaptured Then
                If Sig.StoreSignature Then
                    _DR("staff_signature") = Sig.SignatureID
                End If
            End If

            _DR("status") = "Complete"
            _DR("stamp") = Now

            _DS.Tables(0).Rows.Add(_DR)
            DAL.UpdateDB(_DA, _DS)

        End If

    End Sub

    Private Sub StoreAnswers(ByVal ResponseID As String)
        For Each _qc As QuestionControl In sc.Controls
            UpdateAnswer(ResponseID, _qc.QuestionID, _qc.ResponseAnswer, True, _qc.ResponseComments)
        Next
    End Sub

    Private Sub UpdateAnswer(ByVal ResponseID As String, ByVal ItemID As String, ByVal Answer As String, ByVal AnswerOK As Boolean, ByVal AnswerComments As String)

        Dim _SQL As String = "select * from CheckListRespItems where ID = '" & ResponseID & "'"

        Dim _DA As IDataAdapter = DAL.ReturnDataAdapter(_SQL)
        If Not _DA Is Nothing Then

            Dim _DR As DataRow
            Dim _DS As New DataSet
            _DA.Fill(_DS)

            If _DS.Tables(0).Rows.Count = 1 Then
                _DR = _DS.Tables(0).Rows(0)
            Else
                _DR = _DS.Tables(0).NewRow
            End If

            _DR("id") = Guid.NewGuid
            _DR("response_id") = New Guid(ResponseID)
            _DR("item_id") = New Guid(ItemID)
            _DR("response") = Answer
            _DR("response_ok") = AnswerOK
            _DR("response_comments") = AnswerComments

            If _DS.Tables(0).Rows.Count = 0 Then _DS.Tables(0).Rows.Add(_DR)

            DAL.UpdateDB(_DA, _DS)

        End If

    End Sub

End Class