﻿Option Strict On

Imports DevExpress.XtraBars.Navigation
Imports DevExpress.XtraEditors
Imports NurseryTouchscreen.SharedModule

Public Class frmSleepManager

    Private m_SelectedLocation As String = ""

    Private m_CheckPeriod As Integer = 10
    Private m_CheckWarn As Integer = 8

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub frmSleepManager_Load(sender As Object, e As EventArgs) Handles Me.Load

        PopulateGroups()
        PopulateDashboardTiles()

        timSleepCheck.Interval = 60000

    End Sub

    Private Sub PopulateDashboardTiles()

        Dim _SQL As String = ""

        'asleep count
        _SQL += "select count(*) as 'Count' from Activity a"
        _SQL += " left join Day on Day.ID = a.day_id"
        _SQL += " where a.day_id = '" + SharedModule.TodayID + "'"
        _SQL += " and a.type = 'ASLEEP'"
        _SQL += " and a.value_1 = '" + m_SelectedLocation + "'"

        Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
        If _DR IsNot Nothing Then
            dshAsleep.LabelMiddle = _DR.Item("Count").ToString
        Else
            dshAsleep.LabelMiddle = "0"
        End If

        DisplayLastCheck()

    End Sub

    Private Sub DisplayLastCheck()

        If dshAsleep.LabelMiddle = "0" Then

            timSleepCheck.Stop()

            dshCheck.LabelMiddle = "N/A"
            dshCheck.Colour = DashboardLabel.EnumColourCode.Green
            btnCheck.Enabled = False

        Else

            Dim _SQL As String = ""

            _SQL += "select top 1 stamp from Activity a"
            _SQL += " left join Day on Day.ID = a.day_id"
            _SQL += " where a.day_id = '" + SharedModule.TodayID + "'"
            _SQL += " and a.type = 'SLEEPCHECK'"
            _SQL += " and a.value_1 = '" + m_SelectedLocation + "'"
            _SQL += " order by a.stamp desc"

            Dim _LastCheck As Date? = Nothing
            Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
            If _DR IsNot Nothing Then

                _LastCheck = CDate(_DR.Item("stamp").ToString)

                If _LastCheck.HasValue Then

                    Dim _Diff As Long = DateDiff(DateInterval.Minute, _LastCheck.Value, Now)
                    dshCheck.LabelMiddle = _Diff.ToString

                    If _Diff >= m_CheckPeriod Then
                        dshCheck.Colour = DashboardLabel.EnumColourCode.Red
                    Else
                        If _Diff >= m_CheckWarn Then
                            dshCheck.Colour = DashboardLabel.EnumColourCode.Amber
                        Else
                            dshCheck.Colour = DashboardLabel.EnumColourCode.Green
                        End If
                    End If

                    btnCheck.Enabled = True
                    timSleepCheck.Start()

                Else
                    dshCheck.Colour = DashboardLabel.EnumColourCode.Green
                    dshCheck.LabelMiddle = "N/A"
                End If

            Else
                dshCheck.Colour = DashboardLabel.EnumColourCode.Green
                dshCheck.LabelMiddle = "N/A"
            End If

        End If

    End Sub

    Private Sub PopulateSleep()

        Dim sqlFilter As String = ""
        Dim hideChildName As Boolean = Parameters.HideChildName

        If radAsleep.Checked Then

            If hideChildName Then
                sqlFilter = "select c.id, c.knownas as 'name',"
            Else
                sqlFilter = "select c.id, c.fullname as 'name',"
            End If

            sqlFilter &= String.Concat(" c.dob, c.allergy_rating, c.diet_restrict,",
                                       " b.booking_from, b.booking_to, b.tariff_name, b.timeslots,",
                                       " a.stamp, a.id as 'activity_id'",
                                       " from Activity a",
                                       " left join Day on Day.ID = a.day_id",
                                       " left join Children c on c.ID = a.key_id",
                                       " left join Bookings b on b.site_id = c.site_id and b.booking_date = Day.date and b.child_id = c.ID",
                                       " where a.day_id = '", SharedModule.TodayID, "'",
                                       " and a.type = 'ASLEEP'",
                                       " and a.value_1 = '", m_SelectedLocation, "'",
                                       " order by name")

        Else

            Dim notIn As String = ""
            notIn = String.Concat("(select a.key_id from Activity a",
                                  " where a.day_id = '", TodayID, "'",
                                  " and a.type = 'ASLEEP'",
                                  " and a.key_id = r.person_id)")

            If hideChildName Then
                sqlFilter = "select c.id, c.knownas as 'name',"
            Else
                sqlFilter = "select c.id, c.fullname as 'name',"
            End If

            sqlFilter &= String.Concat(" c.dob, c.allergy_rating, c.diet_restrict,",
                                       " b.booking_from, b.booking_to, b.tariff_name, b.timeslots",
                                       " from RegisterSummary r",
                                       " left join Children c on c.ID = r.person_id",
                                       " left join Bookings b on b.site_id = c.site_id and b.booking_date = r.date and b.child_id = c.ID",
                                       " where day_id = '", SharedModule.TodayID, "'",
                                       " and person_type = 'C'",
                                       " and r.in_out = 'I'",
                                       " and r.location = '", m_SelectedLocation, "'",
                                       " and r.person_id NOT IN ", notIn,
                                       " order by name")

        End If

        tg.Items.Clear()
        btnAsleep.Enabled = False

        Dim table As DataTable = DAL.ReturnDataTable(sqlFilter)
        If table IsNot Nothing Then

            For Each row As DataRow In table.Rows

                Dim tileBarItem As New TileBarItem
                With tileBarItem

                    .Name = "t_" + row.Item("ID").ToString
                    .ItemSize = TileBarItemSize.Wide

                    .TextAlignment = TileItemContentAlignment.TopLeft
                    .AppearanceItem.Normal.ForeColor = Color.Black
                    .AppearanceItem.Normal.BackColor = txtLightYellow.BackColor
                    .AppearanceItem.Selected.BackColor = txtYellow.BackColor
                    .Text = row.Item("name").ToString

                    If radAsleep.Checked Then

                        Dim started As Date = CDate(row.Item("stamp"))
                        Dim timeDifference As Long = DateDiff(DateInterval.Minute, started, Now)
                        Dim id As String = row.Item("activity_id").ToString

                        .Tag = id

                        Dim sleepStart As New TileItemElement
                        With sleepStart
                            .TextAlignment = TileItemContentAlignment.BottomLeft
                            .Appearance.Normal.FontSizeDelta = -1
                            .Text = Format(started, "HH:mm").ToString
                        End With

                        Dim minutesAsleep As New TileItemElement
                        With minutesAsleep
                            .TextAlignment = TileItemContentAlignment.MiddleRight
                            .Appearance.Normal.FontSizeDelta = 28
                            .Text = timeDifference.ToString
                        End With

                        tileBarItem.Elements.Add(minutesAsleep)
                        tileBarItem.Elements.Add(sleepStart)

                    End If


                    AddHandler tileBarItem.ItemClick, AddressOf ChildClick

                End With

                tg.Items.Add(tileBarItem)

            Next

            If table.Rows.Count > 0 Then
                btnAsleep.Enabled = True
                If radAsleep.Checked Then
                    btnAsleep.Text = "Child Awake"
                Else
                    btnAsleep.Text = "Child Asleep"
                End If
            End If

        End If

    End Sub

    Private Sub PerformSleepCheck()

        Dim _StaffID As String = ""
        Dim _StaffName As String = ""

        If Parameters.CurrentStaffID <> "" Then
            _StaffID = Parameters.CurrentStaffID
            _StaffName = Parameters.CurrentStaffName
        Else
            Dim _Staff As Pair = Business.Staff.FindStaff(SharedModule.Enums.PersonMode.OnlyCheckedIn)
            If _Staff IsNot Nothing Then
                _StaffID = _Staff.Code
                _StaffName = _Staff.Text
            End If
        End If

        If _StaffID <> "" AndAlso _StaffName <> "" Then
            StoreSleepCheck(_StaffID, _StaffName)
        End If

    End Sub

    Private Sub StoreSleepCheck(ByVal StaffID As String, ByVal StaffName As String)

        Me.Cursor = Cursors.WaitCursor
        btnCheck.Enabled = False
        Application.DoEvents()

        Dim _SQL As String = ""
        _SQL += "select key_id, key_name from Activity"
        _SQL += " where day_id = '" + SharedModule.TodayID + "'"
        _SQL += " and type = 'ASLEEP'"
        _SQL += " and value_1 = '" + m_SelectedLocation + "'"

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows
                SharedModule.LogSleepCheck(Parameters.TodayID, _DR.Item("key_id").ToString, _DR.Item("key_name").ToString, m_SelectedLocation, StaffID, StaffName)
            Next

            _DT.Dispose()
            _DT = Nothing

        End If

        PopulateDashboardTiles()

        Me.Cursor = Cursors.Default
        Application.DoEvents()

    End Sub

    Private Sub PopulateGroups()

        Dim _SQL As String = ""

        _SQL += "SELECT ID, room_name from SiteRooms"
        _SQL += " where site_id = '" + Parameters.SiteID + "'"
        _SQL += " and room_check_children = 1"
        _SQL += " order by room_sequence"

        PopulateTileBar(_SQL)

        If Parameters.DefaultRoom <> "" Then
            For Each _i As TileBarItem In tbRoomGroup.Items
                Dim _Text As String = _i.Elements(0).Text
                If _Text = Parameters.DefaultRoom Then
                    tbRooms.SelectedItem = _i
                    _i.PerformItemClick()
                    Exit Sub
                End If
            Next
        Else
            For Each _i As TileBarItem In tbRoomGroup.Items
                tbRooms.SelectedItem = _i
                _i.PerformItemClick()
                Exit Sub
            Next
        End If

    End Sub

    Private Sub PopulateTileBar(ByVal _SQL As String)

        tbRoomGroup.Items.Clear()

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _i As New TileBarItem
                With _i

                    .Name = "t_" + _DR.Item("ID").ToString

                    AddHandler _i.ItemClick, AddressOf GroupItemClick

                    .TextAlignment = TileItemContentAlignment.TopLeft
                    .AppearanceItem.Normal.ForeColor = Color.Black
                    .AppearanceItem.Normal.BackColor = txtLightYellow.BackColor
                    .AppearanceItem.Selected.BackColor = txtYellow.BackColor
                    .Text = _DR.Item("room_name").ToString

                End With

                tbRoomGroup.Items.Add(_i)

            Next

        End If

    End Sub

    Private Sub ChildClick(sender As Object, e As TileItemEventArgs)
        If e.Item.Checked Then
            e.Item.Checked = False
        Else
            e.Item.Checked = True
        End If
    End Sub

    Private Sub GroupItemClick(sender As Object, e As TileItemEventArgs)
        m_SelectedLocation = e.Item.Text
        PopulateSleep()
        PopulateDashboardTiles()
    End Sub

    Private Sub frmSleepManager_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        tbRooms.WideTileWidth = tbRooms.Width - 40
    End Sub

    Private Sub dshCheck_DoubleClick(sender As Object, e As DashboardLabel.DashboardEventArgs) Handles dshCheck.DoubleClick
        PerformSleepCheck()
    End Sub

    Private Sub radAsleep_CheckedChanged(sender As Object, e As EventArgs) Handles radAsleep.CheckedChanged
        If radAsleep.Checked Then PopulateSleep()
    End Sub

    Private Sub radAwake_CheckedChanged(sender As Object, e As EventArgs) Handles radAwake.CheckedChanged
        If radAwake.Checked Then PopulateSleep()
    End Sub

    Private Sub timSleepCheck_Tick(sender As Object, e As EventArgs) Handles timSleepCheck.Tick

        Me.Cursor = Cursors.WaitCursor
        Application.DoEvents()

        PopulateDashboardTiles()
        PopulateSleep()

        Me.Cursor = Cursors.Default
        Application.DoEvents()

    End Sub

    Private Function ItemsSelected() As Boolean

        For Each _i As TileBarItem In tg.Items
            If _i.Checked Then Return True
        Next

        Return False

    End Function

    Private Sub btnAsleep_Click(sender As Object, e As EventArgs) Handles btnAsleep.Click

        If Not ItemsSelected() Then
            Msgbox("Please select at least one Child.", MessageBoxIcon.Exclamation, "Item Check")
            Exit Sub
        End If

        Dim _StaffID As String = ""
        Dim _StaffName As String = ""

        If Parameters.CurrentStaffID <> "" Then
            _StaffID = Parameters.CurrentStaffID
            _StaffName = Parameters.CurrentStaffName
        Else
            Dim _Staff As Pair = Business.Staff.FindStaff(SharedModule.Enums.PersonMode.OnlyCheckedIn)
            If _Staff IsNot Nothing Then
                _StaffID = _Staff.Code
                _StaffName = _Staff.Text
            End If
        End If

        If _StaffID <> "" AndAlso _StaffName <> "" Then

            btnAsleep.Enabled = False

            Me.Cursor = Cursors.WaitCursor
            Application.DoEvents()

            For Each _i As TileBarItem In tg.Items

                If _i.Checked Then

                    Dim _ChildID As String = _i.Name.Substring(2)
                    Dim _ChildName As String = _i.Elements(0).Text

                    If radAsleep.Checked Then
                        Dim _ID As String = _i.Tag.ToString
                        SharedModule.ConvertToSleep(_ID, _StaffID, _StaffName)
                    Else
                        SharedModule.LogAsleep(Parameters.TodayID, _ChildID, _ChildName, m_SelectedLocation, _StaffID, _StaffName)
                    End If

                End If

            Next

            StoreSleepCheck(_StaffID, _StaffName)

            PopulateSleep()
            PopulateDashboardTiles()

            Me.Cursor = Cursors.Default
            Application.DoEvents()

        End If

    End Sub

    Private Sub btnCheck_Click(sender As Object, e As EventArgs) Handles btnCheck.Click
        PerformSleepCheck()
    End Sub

End Class