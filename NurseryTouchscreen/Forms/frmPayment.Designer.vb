﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPayment
    Inherits frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPayment))
        Me.panBottom = New DevExpress.XtraEditors.PanelControl()
        Me.Splitter2 = New System.Windows.Forms.Splitter()
        Me.btnAccept = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.fldComments = New NurseryTouchscreen.Field()
        Me.fldFrom = New NurseryTouchscreen.Field()
        Me.fldMethod = New NurseryTouchscreen.Field()
        Me.fldValue = New NurseryTouchscreen.Field()
        Me.fldStaff = New NurseryTouchscreen.Field()
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panBottom.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        '
        'panBottom
        '
        Me.panBottom.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panBottom.Appearance.Options.UseFont = True
        Me.panBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.panBottom.Controls.Add(Me.Splitter2)
        Me.panBottom.Controls.Add(Me.btnAccept)
        Me.panBottom.Controls.Add(Me.btnCancel)
        Me.panBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panBottom.Location = New System.Drawing.Point(0, 299)
        Me.panBottom.Name = "panBottom"
        Me.panBottom.Size = New System.Drawing.Size(1019, 56)
        Me.panBottom.TabIndex = 5
        '
        'Splitter2
        '
        Me.Splitter2.Location = New System.Drawing.Point(0, 0)
        Me.Splitter2.Name = "Splitter2"
        Me.Splitter2.Size = New System.Drawing.Size(3, 56)
        Me.Splitter2.TabIndex = 0
        Me.Splitter2.TabStop = False
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAccept.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAccept.Appearance.Options.UseFont = True
        Me.btnAccept.Image = CType(resources.GetObject("btnAccept.Image"), System.Drawing.Image)
        Me.btnAccept.Location = New System.Drawing.Point(701, 3)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(150, 45)
        Me.btnAccept.TabIndex = 0
        Me.btnAccept.Text = "Accept"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.DialogResult = DialogResult.Cancel
        Me.btnCancel.Image = CType(resources.GetObject("btnCancel.Image"), System.Drawing.Image)
        Me.btnCancel.Location = New System.Drawing.Point(857, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(150, 45)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "Cancel"
        '
        'fldComments
        '
        Me.fldComments.ButtonCustom = False
        Me.fldComments.ButtonSQL = Nothing
        Me.fldComments.ButtonText = ""
        Me.fldComments.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Enter
        Me.fldComments.ButtonWidth = 50.0!
        Me.fldComments.FamilyID = Nothing
        Me.fldComments.HideText = False
        Me.fldComments.LabelText = "Comments"
        Me.fldComments.LabelWidth = 175.0!
        Me.fldComments.Location = New System.Drawing.Point(12, 183)
        Me.fldComments.MaximumSize = New System.Drawing.Size(1000, 51)
        Me.fldComments.MinimumSize = New System.Drawing.Size(210, 51)
        Me.fldComments.Name = "fldComments"
        Me.fldComments.QuickTextList = Nothing
        Me.fldComments.Size = New System.Drawing.Size(995, 51)
        Me.fldComments.TabIndex = 3
        Me.fldComments.ValueDateTime = Nothing
        Me.fldComments.ValueID = Nothing
        Me.fldComments.ValueMaxLength = 0
        Me.fldComments.ValueText = ""
        '
        'fldFrom
        '
        Me.fldFrom.ButtonCustom = False
        Me.fldFrom.ButtonSQL = Nothing
        Me.fldFrom.ButtonText = ""
        Me.fldFrom.ButtonType = NurseryTouchscreen.Field.EnumButtonType.SignatureParent
        Me.fldFrom.ButtonWidth = 50.0!
        Me.fldFrom.FamilyID = Nothing
        Me.fldFrom.HideText = False
        Me.fldFrom.LabelText = "Payment From"
        Me.fldFrom.LabelWidth = 175.0!
        Me.fldFrom.Location = New System.Drawing.Point(12, 12)
        Me.fldFrom.MaximumSize = New System.Drawing.Size(1000, 51)
        Me.fldFrom.MinimumSize = New System.Drawing.Size(210, 51)
        Me.fldFrom.Name = "fldFrom"
        Me.fldFrom.QuickTextList = Nothing
        Me.fldFrom.Size = New System.Drawing.Size(995, 51)
        Me.fldFrom.TabIndex = 0
        Me.fldFrom.ValueDateTime = Nothing
        Me.fldFrom.ValueID = Nothing
        Me.fldFrom.ValueMaxLength = 0
        Me.fldFrom.ValueText = ""
        '
        'fldMethod
        '
        Me.fldMethod.ButtonCustom = False
        Me.fldMethod.ButtonSQL = "Cash|Cheque|Card"
        Me.fldMethod.ButtonText = ""
        Me.fldMethod.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Lookup
        Me.fldMethod.ButtonWidth = 50.0!
        Me.fldMethod.FamilyID = Nothing
        Me.fldMethod.HideText = False
        Me.fldMethod.LabelText = "Payment Method"
        Me.fldMethod.LabelWidth = 175.0!
        Me.fldMethod.Location = New System.Drawing.Point(12, 126)
        Me.fldMethod.MaximumSize = New System.Drawing.Size(1000, 51)
        Me.fldMethod.MinimumSize = New System.Drawing.Size(210, 51)
        Me.fldMethod.Name = "fldMethod"
        Me.fldMethod.QuickTextList = Nothing
        Me.fldMethod.Size = New System.Drawing.Size(995, 51)
        Me.fldMethod.TabIndex = 2
        Me.fldMethod.ValueDateTime = Nothing
        Me.fldMethod.ValueID = Nothing
        Me.fldMethod.ValueMaxLength = 0
        Me.fldMethod.ValueText = ""
        '
        'fldValue
        '
        Me.fldValue.ButtonCustom = False
        Me.fldValue.ButtonSQL = Nothing
        Me.fldValue.ButtonText = ""
        Me.fldValue.ButtonType = NurseryTouchscreen.Field.EnumButtonType.DecimalNumber
        Me.fldValue.ButtonWidth = 50.0!
        Me.fldValue.FamilyID = Nothing
        Me.fldValue.HideText = False
        Me.fldValue.LabelText = "Payment Value"
        Me.fldValue.LabelWidth = 175.0!
        Me.fldValue.Location = New System.Drawing.Point(12, 69)
        Me.fldValue.MaximumSize = New System.Drawing.Size(1000, 51)
        Me.fldValue.MinimumSize = New System.Drawing.Size(210, 51)
        Me.fldValue.Name = "fldValue"
        Me.fldValue.QuickTextList = Nothing
        Me.fldValue.Size = New System.Drawing.Size(995, 51)
        Me.fldValue.TabIndex = 1
        Me.fldValue.ValueDateTime = Nothing
        Me.fldValue.ValueID = Nothing
        Me.fldValue.ValueMaxLength = 0
        Me.fldValue.ValueText = ""
        '
        'fldStaff
        '
        Me.fldStaff.ButtonCustom = False
        Me.fldStaff.ButtonSQL = Nothing
        Me.fldStaff.ButtonText = ""
        Me.fldStaff.ButtonType = NurseryTouchscreen.Field.EnumButtonType.SignatureStaff
        Me.fldStaff.ButtonWidth = 50.0!
        Me.fldStaff.FamilyID = Nothing
        Me.fldStaff.HideText = False
        Me.fldStaff.LabelText = "Taken by"
        Me.fldStaff.LabelWidth = 175.0!
        Me.fldStaff.Location = New System.Drawing.Point(12, 240)
        Me.fldStaff.MaximumSize = New System.Drawing.Size(1000, 51)
        Me.fldStaff.MinimumSize = New System.Drawing.Size(210, 51)
        Me.fldStaff.Name = "fldStaff"
        Me.fldStaff.QuickTextList = Nothing
        Me.fldStaff.Size = New System.Drawing.Size(995, 51)
        Me.fldStaff.TabIndex = 4
        Me.fldStaff.ValueDateTime = Nothing
        Me.fldStaff.ValueID = Nothing
        Me.fldStaff.ValueMaxLength = 0
        Me.fldStaff.ValueText = ""
        '
        'frmPayment
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1019, 355)
        Me.Controls.Add(Me.fldStaff)
        Me.Controls.Add(Me.fldComments)
        Me.Controls.Add(Me.fldFrom)
        Me.Controls.Add(Me.fldMethod)
        Me.Controls.Add(Me.fldValue)
        Me.Controls.Add(Me.panBottom)
        Me.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPayment"
        Me.Text = "Record Payment"
        Me.Controls.SetChildIndex(Me.panBottom, 0)
        Me.Controls.SetChildIndex(Me.fldValue, 0)
        Me.Controls.SetChildIndex(Me.fldMethod, 0)
        Me.Controls.SetChildIndex(Me.fldFrom, 0)
        Me.Controls.SetChildIndex(Me.fldComments, 0)
        Me.Controls.SetChildIndex(Me.fldStaff, 0)
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panBottom.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Protected Friend WithEvents panBottom As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnAccept As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Splitter2 As System.Windows.Forms.Splitter
    Friend WithEvents fldValue As NurseryTouchscreen.Field
    Friend WithEvents fldMethod As NurseryTouchscreen.Field
    Friend WithEvents fldFrom As NurseryTouchscreen.Field
    Friend WithEvents fldComments As NurseryTouchscreen.Field
    Friend WithEvents fldStaff As NurseryTouchscreen.Field
End Class
