﻿Imports NurseryTouchscreen.SharedModule

Public Class frmActivity

    Private m_LastRoomID As String = ""

    Private Sub frmActivity_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Me.DesignMode Then Exit Sub
        scrGroups.Populate()

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub GetActivityText()

        If scrGroups.ValueID = "" Then Exit Sub

        Dim _SQL As String = ""
        _SQL += "select id, activity_text from DayActivity"
        _SQL += " where day_id = '" + Parameters.TodayID + "'"
        _SQL += " and group_id = '" + scrGroups.ValueID + "'"

        Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
        If _DR IsNot Nothing Then
            m_LastRoomID = scrGroups.ValueID
            txtLargeText.Text = _DR.Item("activity_text").ToString
            _DR = Nothing
        Else
            txtLargeText.Text = ""
        End If

    End Sub

    Private Function SaveScreen() As Boolean
        CommitUpdate()
        Return True
    End Function

    Private Sub CommitUpdate()

        If m_LastRoomID = "" Then Exit Sub

        Dim _New As Boolean = True
        Dim _SQL As String = ""

        _SQL = "select * from DayActivity"
        _SQL += " where day_id = '" + Parameters.TodayID + "'"
        _SQL += " and group_id = '" + m_LastRoomID + "'"

        Dim _DA As IDataAdapter = DAL.ReturnDataAdapter(_SQL)
        If Not _DA Is Nothing Then

            Dim _DR As DataRow = Nothing
            Dim _DS As New DataSet
            _DA.Fill(_DS)

            If _DS.Tables.Count = 1 Then

                If _DS.Tables(0).Rows.Count = 1 Then
                    _New = False
                    _DR = _DS.Tables(0).Rows(0)
                Else
                    _DR = _DS.Tables(0).NewRow
                    _DR("ID") = Guid.NewGuid
                    _DR("day_id") = Parameters.TodayID
                    _DR("group_id") = scrGroups.ValueID
                End If

                _DR("activity_text") = txtLargeText.Text

                If _New Then
                    _DS.Tables(0).Rows.Add(_DR)
                End If

                DAL.UpdateDB(_DA, _DS)

            End If

        End If

    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click
        SaveScreen()
        Me.Close()
    End Sub

    Private Sub scrGroups_RecordChanged(sender As Object, e As RecordChangedArgs) Handles scrGroups.RecordChanged
        If m_LastRoomID <> "" Then SaveScreen()
        GetActivityText()
    End Sub
End Class
