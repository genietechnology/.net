﻿Option Strict On

Imports NurseryTouchscreen.SharedModule
Imports System.Diagnostics

Public Class frmMain

    Private m_SyncOnStartUp As Boolean = False

    Public Sub New(ByVal SyncRequired As Boolean)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_SyncOnStartUp = SyncRequired

    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles Me.Load

        btnDoor1.Enabled = True

        Dim _WideMode As Boolean

        Select Case UsageMode

            Case EnumUsage.StandardTouch
                _WideMode = True

            Case EnumUsage.Foyer, EnumUsage.StandardFromFoyerMode
                _WideMode = True

            Case EnumUsage.StandardTablet, EnumUsage.ClubMode
                _WideMode = False

        End Select

        If _WideMode Then
            Me.Width = 1366
        Else
            Me.Width = 1052
        End If

        If Parameters.TopLeft Then
            Me.Top = 0
            Me.Left = 0
        Else
            Me.CenterToScreen()
        End If

    End Sub

    Private Sub frmMain_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        Dim _OK As Boolean = False

        Me.Cursor = Cursors.AppStarting
        Application.DoEvents()

        If m_SyncOnStartUp Then
            _OK = SyncData()
        Else
            _OK = True
        End If

        If _OK Then

            Me.Text = "Nursery Genie Version " + Application.ProductVersion

            If DoorHandler.Enabled Then
                If DoorHandler.Connected Then
                    Me.Text = "Nursery Genie Version " + Application.ProductVersion + "  Connected to Door Controller: " + DoorHandler.IPAddress
                    btnDoor1.Enabled = True
                Else
                    Me.Text = "Nursery Genie Version " + Application.ProductVersion + "  Failed to connect to Door Controller: " + DoorHandler.IPAddress
                End If
            End If

            Me.Text += " [" + My.Computer.Name + "]"
            Me.Text += WebServiceStatus()

            LockButtons(False)

        End If

        Me.Cursor = Cursors.Default
        Application.DoEvents()

    End Sub

    Private Function WebServiceStatus() As String
        If Parameters.UseWebServices Then Return " Using Service on " + Parameters.ServiceIP
        Return ""
    End Function

    Private Function SyncData() As Boolean

        'LockButtons(True)
        'btnSync.Enabled = False
        'btnExit.Enabled = False
        'Cursor.Current = Cursors.WaitCursor

        'SplashScreenManager1.ShowWaitForm()

        Dim _Return As Boolean = False
        'Dim _rda As New RDA

        ''push back everything first...
        'SplashScreenManager1.SetWaitFormDescription("Uploading Register...")
        'If _rda.PushRegister() Then
        '    SplashScreenManager1.SetWaitFormDescription("Uploading Activity...")
        '    If _rda.PushActivity Then
        '        SplashScreenManager1.SetWaitFormDescription("Uploading Food Records...")
        '        If _rda.PushFoodRegister() Then
        '            SplashScreenManager1.SetWaitFormDescription("Uploading Medicine Authorisations...")
        '            If _rda.PushMedicalAdmin() Then
        '                SplashScreenManager1.SetWaitFormDescription("Uploading Medicine Log...")
        '                If _rda.PushMedicalLog() Then
        '                    SplashScreenManager1.SetWaitFormDescription("Uploading Accident Log...")
        '                    If _rda.PushAccidentLog() Then
        '                        _Return = True
        '                    End If
        '                End If
        '            End If
        '        End If
        '    End If
        'End If

        ''pull down other tables...
        'If _Return Then

        '    Cursor.Current = Cursors.Default

        '    SplashScreenManager1.SetWaitFormDescription("Downloading Parameters...")
        '    If _rda.PullParameters() Then
        '        SplashScreenManager1.SetWaitFormDescription("Downloading Children...")
        '        If _rda.PullChildren() Then
        '            SplashScreenManager1.SetWaitFormDescription("Downloading Contacts...")
        '            If _rda.PullContacts() Then
        '                SplashScreenManager1.SetWaitFormDescription("Downloading Day File...")
        '                If _rda.PullDay() Then
        '                    SplashScreenManager1.SetWaitFormDescription("Downloading Register...")
        '                    If _rda.PullRegister() Then
        '                        SplashScreenManager1.SetWaitFormDescription("Downloading Activity...")
        '                        If _rda.PullActivity() Then
        '                            SplashScreenManager1.SetWaitFormDescription("Downloading Food Records...")
        '                            If _rda.PullFoodRegister() Then
        '                                SplashScreenManager1.SetWaitFormDescription("Downloading Medical Authorisations...")
        '                                If _rda.PullMedicalAdmin() Then
        '                                    SplashScreenManager1.SetWaitFormDescription("Downloading Medicine Log...")
        '                                    If _rda.PullMedicalLog() Then
        '                                        SplashScreenManager1.SetWaitFormDescription("Downloading Accident Log...")
        '                                        If _rda.PullAccidentLog() Then
        '                                            SplashScreenManager1.SetWaitFormDescription("Downloading Staff...")
        '                                            If _rda.PullStaff() Then
        '                                                SplashScreenManager1.SetWaitFormDescription("Downloading Groups...")
        '                                                If _rda.PullGroups() Then
        '                                                    SplashScreenManager1.SetWaitFormDescription("Downloading Meals...")
        '                                                    If _rda.PullMeals() Then
        '                                                        SplashScreenManager1.SetWaitFormDescription("Downloading Requests...")
        '                                                        If _rda.PullRequests() Then
        '                                                            SplashScreenManager1.SetWaitFormDescription("Downloading Quick Texts...")
        '                                                            If _rda.PullQuickTexts() Then
        '                                                                SplashScreenManager1.CloseWaitForm()
        '                                                            Else
        '                                                                SplashScreenManager1.CloseWaitForm()
        '                                                                Msgbox("Server Updated Successfully - but could not refresh Tables.", MessageBoxIcon.Information, "Synchronise Data")
        '                                                            End If

        '                                                        End If
        '                                                    End If
        '                                                End If
        '                                            End If
        '                                        End If
        '                                    End If
        '                                End If
        '                            End If
        '                        End If
        '                    End If
        '                End If
        '            End If
        '        End If
        '    End If
        'Else
        '    Msgbox("Server Updated Failed.", MessageBoxIcon.Exclamation, "Synchronise Data")
        'End If

        '_rda = Nothing

        'LockButtons(False)
        'btnSync.Enabled = True
        'btnExit.Enabled = True

        Return _Return

    End Function

    Private Sub LockButtons(ByVal Lock As Boolean)

        Dim _StaffIn As Boolean = StaffCheckedIn()
        Dim _ChildrenIn As Boolean = ChildrenCheckedIn()

        For Each _c As Control In tlp.Controls

            If _c.Name.ToUpper.StartsWith("BTN") Then

                'assume we are disabling
                Dim _Enabled As Boolean = False

                If Not Lock Then

                    If _c.Name = "btnCheckInStaff" Then
                        If Parameters.UnlockMode <> EnumUnlockMode.PIN Then
                            _Enabled = True
                        End If
                    End If

                    If _c.Name = "btnStaffCentre" Then
                        If Parameters.UnlockMode = EnumUnlockMode.PIN Then
                            _Enabled = True
                        End If
                    End If

                    '***********************************************************************************************************************************

                    If _c.Name = "btnCheckInChild" Then
                        'we are checking in via PIN
                        If Parameters.CheckChildPIN Then
                            _Enabled = True
                        End If
                    End If

                    If _c.Name = "btnCheckOutChild" Then
                        'we are checking out via PIN
                        If _ChildrenIn AndAlso Parameters.CheckChildPIN Then
                            _Enabled = True
                        End If
                    End If

                    '***********************************************************************************************************************************

                    If _c.Name = "btnOptions" Then _Enabled = True
                    If _c.Name = "btnPolicies" Then _Enabled = True

                    'if we are in foyer mode, these options are always locked
                    If SharedModule.UsageMode = EnumUsage.Foyer Then



                    Else

                        'check if we have any staff checked in
                        If _StaffIn Then

                            If _c.Name = "btnRegister" Then _Enabled = True
                            If _c.Name = "btnLookupChild" Then _Enabled = True
                            If _c.Name = "btnCompliance" Then _Enabled = True
                            If _c.Name = "btnPhoto" Then _Enabled = True

                            If _c.Name = "btnCheckOutStaff" Then _Enabled = _StaffIn

                            If _c.Name = "btnCheckInChild" Then _Enabled = True
                            If _c.Name = "btnCheckOutChild" Then _Enabled = _ChildrenIn

                            'check if we have any children checked in
                            If _ChildrenIn Then

                                Dim _Tag As String = ""
                                If _Tag IsNot Nothing Then _Tag = _c.Tag.ToString.ToUpper

                                Select Case UsageMode

                                    Case EnumUsage.StandardTablet, EnumUsage.StandardTouch, EnumUsage.StandardFromFoyerMode
                                        If _Tag.Contains("S") Then _Enabled = IsDirectCheck(_Tag)

                                    Case EnumUsage.Foyer
                                        If _Tag.Contains("F") Then _Enabled = IsDirectCheck(_Tag)

                                    Case EnumUsage.ClubMode
                                        If _Tag.Contains("C") Then _Enabled = IsDirectCheck(_Tag)

                                End Select

                            Else
                                'no children checked in
                            End If

                        Else
                            'no staff checked in
                        End If

                    End If

                End If

                _c.Enabled = _Enabled

            End If

        Next

        '***********************************************************************************************************************************

        If Not Lock Then

            btnHelp.Enabled = True

            If Not Parameters.DisableScreenSaver Then
                btnBack.Enabled = True
            End If

            btnExit.Enabled = True

        End If

        Select Case UsageMode

            Case EnumUsage.StandardFromFoyerMode
                btnBack.Enabled = True
                btnBack.Text = "Back to Foyer"
                btnExit.Enabled = True

            Case EnumUsage.Foyer
                btnBack.Enabled = True
                btnBack.Text = "Exit" + vbCrLf + "Foyer Mode"
                btnExit.Enabled = False

        End Select

        'door control
        If DoorHandler.Connected Then
            btnDoor1.Enabled = True
        End If

    End Sub

    Private Function IsDirectCheck(ByVal TagString As String) As Boolean
        If TagString.Contains("D") Then
            'only enabled in direct mode
            If Parameters.UseDirectSQL Then
                Return True
            Else
                Return False
            End If
        Else
            Return True
        End If
    End Function

    Private Function GetNextMeds() As Date?

        Dim _Return As Date? = Nothing

        Dim _SQL As String = SharedModule.MedicationDueQuery

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If _DT IsNot Nothing Then
            If _DT.Rows.Count > 0 Then
                _Return = CDate(_DT.Rows(0).Item("due"))
            End If
            _DT.Dispose()
            _DT = Nothing
        End If

        Return _Return

    End Function

#Region "Buttons"

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click

        Select Case UsageMode

            Case EnumUsage.Foyer

                Dim _Entered As String = NumberEntry("Please enter your Staff PIN code", "", False, False)
                If _Entered <> "" Then

                    Dim _p As Pair = Business.Staff.ValidatePIN(_Entered)
                    If _p IsNot Nothing Then

                        'check in the staff member if they are not already
                        If SharedModule.PersonInorOut(New Guid(_p.Code)) = Enums.InOut.CheckOut Then
                            Dim _Location As String = GetLocation(Enums.PersonType.Staff, Enums.InOut.CheckIn)
                            SharedModule.RegisterTransaction(New Guid(_p.Code), Enums.PersonType.Staff, _p.Text, Enums.InOut.CheckIn, Enums.RegisterVia.Menu, _Location, "", "", "")
                        End If

                        UsageMode = EnumUsage.StandardFromFoyerMode
                        LockButtons(False)

                    End If

                End If

            Case EnumUsage.StandardFromFoyerMode
                UsageMode = EnumUsage.Foyer
                LockButtons(False)

            Case Else

                If Parameters.CurrentStaffID <> "" Then
                    LogActivity(TodayID, Parameters.CurrentStaffID, Parameters.CurrentStaffName, EnumActivityType.LogOff, Parameters.CurrentStaffName + " logged off " + My.Computer.Name, My.Computer.Name, "", "", Parameters.CurrentStaffID, Parameters.CurrentStaffName)
                    Parameters.CurrentStaffID = ""
                    Parameters.CurrentStaffName = ""
                End If

                Me.DialogResult = DialogResult.OK
                Me.Close()

        End Select

    End Sub

    Private Sub btnLookupChild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLookupChild.Click

        btnLookupChild.Enabled = False

        'show all current children (even if they are checked in or are not due in today)
        Dim _Return As Pair = Business.Child.FindChildByGroup(Enums.PersonMode.Everyone, True)
        If Not _Return Is Nothing AndAlso _Return.Code <> "" Then
            Business.Child.DrillDown(_Return.Code, _Return.Text)
        End If

        btnLookupChild.Enabled = True

    End Sub

    Private Sub btnCheckInChild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheckInChild.Click

        If Not CheckinsPermitted() Then Exit Sub

        btnCheckInChild.Enabled = False

        If Parameters.CheckChildPIN Then

            Dim pin As String = NumberEntry("Enter PIN", "", False, False)
            If pin <> "" Then

                Dim childPair As Pair = Nothing
                If Business.Child.ReturnChildFromPIN(childPair, pin) Then
                    'ensure the child is not already checked in...
                    If Not Business.Child.CheckedIn(childPair.Code) Then
                        If Msgbox("Check-In " + childPair.Text + "?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, "Confirm Check-In") = DialogResult.Yes Then
                            SharedModule.RegisterTransaction(New Guid(childPair.Code), Enums.PersonType.Child, childPair.Text, Enums.InOut.CheckIn, Enums.RegisterVia.Menu, "", "", "", "")
                        End If
                    Else
                        Msgbox("This child has already been checked in...", MessageBoxIcon.Exclamation, "Check-In Child")
                    End If
                End If

            Else
                'no PIN entered
            End If

        Else

            Dim childPairs As List(Of Pair) = Business.Child.FindChildrenByGroup(Enums.PersonMode.OnlyCheckedOut)
            If childPairs IsNot Nothing Then

                Dim _Location As String = GetLocation(Enums.PersonType.Child, Enums.InOut.CheckIn)

                For Each childPair In childPairs

                    Dim signature As New Signature
                    Dim signatureRequired As Boolean = Parameters.ContactSignatureIn

                    If signatureRequired Then

                        Msgbox("Please tap OK so we can capture your signature...", MessageBoxIcon.Exclamation, "Signature Required")
                        signature.CaptureParentSignature(True, Business.Child.ReturnFamilyID(childPair.Code))

                        If signature.SignatureCaptured Then
                            SharedModule.RegisterTransaction(New Guid(childPair.Code), Enums.PersonType.Child, childPair.Text, Enums.InOut.CheckIn, Enums.RegisterVia.Menu, _Location, "", "", "")
                        Else
                            If Msgbox("Parent Signature declined. Do you want to specify another person collecting?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, "Signature Declined") = DialogResult.Yes Then
                                Dim name As String = SharedModule.OSK("Please enter the name of the person collecting")
                                If name <> "" Then
                                    signature.CaptureVisitorSignature(True, Nothing, name)
                                    If signature.SignatureCaptured Then
                                        SharedModule.RegisterTransaction(New Guid(childPair.Code), Enums.PersonType.Child, childPair.Text, Enums.InOut.CheckIn, Enums.RegisterVia.Menu, _Location, "", "", "")
                                    End If
                                End If
                            End If
                        End If
                    Else
                        SharedModule.RegisterTransaction(New Guid(childPair.Code), Enums.PersonType.Child, childPair.Text, Enums.InOut.CheckIn, Enums.RegisterVia.Menu, _Location, "", "", "")
                    End If

                Next

                LockButtons(False)

            End If

        End If

        btnCheckInChild.Enabled = True

    End Sub

    Private Function GetLocation(ByVal PersonType As Enums.PersonType, ByVal InOut As Enums.InOut) As String

        Dim _Return As String = ""

        Dim _Type As String = "these children?"
        If PersonType = Enums.PersonType.Staff Then _Type = "these staff members?"

        Dim _InOut As String = "Check In"
        If InOut = Enums.InOut.CheckOut Then _InOut = "Check Out"

        If Parameters.DisableLocation Then
            'check-in without the prompt
        Else
            If Parameters.DefaultRoom = "" Then
                If Msgbox("Do you wish to specify the location for " + _Type, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, _InOut) = MsgBoxResult.Yes Then
                    Dim _p As Pair = ReturnRoom(Parameters.SiteID, PersonType)
                    If _p IsNot Nothing Then
                        _Return = _p.Text
                    End If
                End If
            Else
                'use the default room
                _Return = Parameters.DefaultRoom
            End If
        End If

        Return _Return

    End Function

    Private Sub btnCheckOutChild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheckOutChild.Click

        If Not CheckinsPermitted() Then Exit Sub

        btnCheckOutChild.Enabled = False

        If Parameters.CheckChildPIN Then

            Dim _PIN As String = NumberEntry("Enter PIN", "", False, False)
            If _PIN <> "" Then
                Dim _Child As Pair = Nothing
                If Business.Child.ReturnChildFromPIN(_Child, _PIN) Then
                    'ensure the child is checked in...
                    If Business.Child.CheckedIn(_Child.Code) Then
                        SharedModule.DoCheckOut(_Child.Code, _Child.Text)
                    Else
                        Msgbox("This child has not been checked in - so cannot be checked out...", MessageBoxIcon.Exclamation, "Check-Out Child")
                    End If
                End If
            Else
                'no PIN entered
            End If

        Else

            Dim _Return As Pair = Business.Child.FindChildByGroup(Enums.PersonMode.OnlyCheckedIn, True)
            If Not _Return Is Nothing AndAlso _Return.Code <> "" Then
                SharedModule.DoCheckOut(_Return.Code, _Return.Text)
                LockButtons(False)
                Exit Sub
            End If

        End If

        btnCheckOutChild.Enabled = True

    End Sub

    Private Sub btnFood_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFood.Click
        btnFood.Enabled = False
        ChildFood()
        btnFood.Enabled = True
    End Sub

    Private Sub btnSleep_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSleep.Click

        btnSleep.Enabled = False

        Dim _frm As New frmSleepManager
        _frm.ShowDialog()

        btnSleep.Enabled = True

    End Sub

    Private Sub btnToilet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnToilet.Click
        btnToilet.Enabled = False
        ChildToilet()
        btnToilet.Enabled = True
    End Sub

    Private Sub btnCheckInStaff_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheckInStaff.Click

        If Not CheckinsPermitted() Then Exit Sub

        btnCheckInStaff.Enabled = False

        Dim _Return As List(Of Pair) = Business.Staff.FindStaffMultiple(Enums.PersonMode.OnlyCheckedOut)
        If _Return IsNot Nothing Then

            Dim _Location As String = GetLocation(Enums.PersonType.Staff, Enums.InOut.CheckIn)

            For Each _P In _Return
                SharedModule.RegisterTransaction(New Guid(_P.Code), Enums.PersonType.Staff, _P.Text, Enums.InOut.CheckIn, Enums.RegisterVia.Menu, _Location, "", "", "")
            Next

            LockButtons(False)

        End If

        btnCheckInStaff.Enabled = True

    End Sub

    Private Sub btnOptions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOptions.Click
        If DeveloperPIN() Then
            ClearForDemo()
            LockButtons(False)
        End If
    End Sub

    Private Sub btnMilk_Click(sender As System.Object, e As System.EventArgs) Handles btnMilk.Click
        btnMilk.Enabled = False
        ChildMilk()
        btnMilk.Enabled = True
    End Sub

    Private Function SanityCheck() As Boolean

        'if we are the last person here, ensure all the children are also checked out!
        Dim _Staff As Decimal = SharedModule.ReturnHeadCount(EnumPersonType.Staff, "")

        If _Staff = 1 Then

            Dim _Children As Decimal = SharedModule.ReturnHeadCount(EnumPersonType.Child, "")
            Dim _Visitors As Decimal = SharedModule.ReturnHeadCount(EnumPersonType.Visitor, "")
            Dim _Message As String = ""

            If _Children > 0 Then

                If _Children = 1 Then
                    _Message = "One child still hasn't been checked out."
                Else
                    _Message = _Children.ToString + " children still haven't been checked out."
                End If

                If Msgbox(_Message + " Do you still want to Check Out?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Child Check") = DialogResult.No Then
                    Return False
                End If

            End If

            If _Visitors > 0 Then

                If _Visitors = 1 Then
                    _Message = "One visitor still hasn't been checked out."
                Else
                    _Message = _Children.ToString + " visitors still haven't been checked out."
                End If

                If Msgbox(_Message + " Do you still want to Check Out?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Visitor Check") = DialogResult.No Then
                    Return False
                End If

            End If

        End If

        Return True

    End Function

    Private Sub btnCheckOutStaff_Click(sender As System.Object, e As System.EventArgs) Handles btnCheckOutStaff.Click

        If Not CheckinsPermitted() Then Exit Sub
        If Not SanityCheck() Then Exit Sub

        btnCheckOutStaff.Enabled = False

        If Parameters.UnlockMode = EnumUnlockMode.PIN Then
            If Msgbox("Are you sure you want to check out " + Parameters.CurrentStaffName + "?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, "Confirm Check Out") = DialogResult.Yes Then
                SharedModule.RegisterTransaction(New Guid(Parameters.CurrentStaffID), Enums.PersonType.Staff, Parameters.CurrentStaffName, Enums.InOut.CheckOut, Enums.RegisterVia.Menu)
            End If
        Else
            Dim _Return As List(Of Pair) = Business.Staff.FindStaffMultiple(Enums.PersonMode.OnlyCheckedIn)
            If _Return IsNot Nothing Then
                For Each _P In _Return
                    SharedModule.RegisterTransaction(New Guid(_P.Code), Enums.PersonType.Staff, _P.Text, Enums.InOut.CheckOut, Enums.RegisterVia.Menu)
                Next
            End If
        End If

        LockButtons(False)

        btnCheckOutStaff.Enabled = True

    End Sub

    Private Sub btnRequests_Click(sender As System.Object, e As System.EventArgs) Handles btnRequests.Click
        btnRequests.Enabled = False
        ChildRequests()
        btnRequests.Enabled = True
    End Sub

    Private Sub btnMedicine_Click(sender As System.Object, e As System.EventArgs) Handles btnMedicine.Click

        Dim _frm As New frmMedicalMain
        _frm.Show()
        _frm.BringToFront()

    End Sub

    Private Sub btnLearn_Click(sender As System.Object, e As System.EventArgs) Handles btnLearn.Click

        Dim _frm As New frmLDMain
        _frm.Show()
        _frm.BringToFront()

    End Sub

#End Region

    Private Sub btnDoor1_Click(sender As Object, e As EventArgs) Handles btnDoor1.Click
        DoorHandler.Unlock(Parameters.AccessControl.Door1Serial)
    End Sub

    Private Sub btnActivities_Click(sender As Object, e As EventArgs) Handles btnActivities.Click
        Dim _A As New frmActivity
        _A.Show()
        _A.BringToFront()
    End Sub

    Private Sub btnRegister_Click(sender As Object, e As EventArgs) Handles btnRegister.Click

        Dim _frm As New frmRegister
        _frm.ShowDialog()

        LockButtons(False)

    End Sub

    Private Sub btnPolicies_Click(sender As Object, e As EventArgs) Handles btnPolicies.Click

        If Parameters.Paths.Policies <> "" Then

            Dim _frm As New frmButtonsTiled

            _frm.PopulateMode = frmButtonsTiled.EnumPopulateMode.FileSystem
            _frm.RootPath = Parameters.Paths.Policies
            _frm.MultiSelect = False

            _frm.ShowDialog()

            Dim _Return As Pair = Nothing

            If _frm.DialogResult = DialogResult.OK Then

                _Return = _frm.SelectedPair

                Dim _Proc As New Process
                _Proc.StartInfo.FileName = _Return.Code

                Try
                    _Proc.Start()

                Catch ex As Exception

                End Try

                _Proc.Dispose()
                _Proc = Nothing

            End If

            _frm.Dispose()
            _frm = Nothing

        Else
            SharedModule.DisplayLinks("Policies", "Select Policy")
        End If

    End Sub

    Private Sub btnPayments_Click(sender As Object, e As EventArgs) Handles btnPayments.Click
        Dim _frm As New frmPayment
        _frm.ShowDialog()
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click

        MediaHandler.UploadMedia()

        If Parameters.DisableScreenSaver Then
            Application.Exit()
        Else
            If Msgbox("Do you wish to Exit the Application?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Exit?") = DialogResult.Yes Then
                Application.Exit()
            Else
                Me.DialogResult = Nothing
            End If
        End If

        'Select Case UsageMode

        '    'Case EnumUsage.Foyer
        '    '    If StaffPIN() Then
        '    '        UsageMode = EnumUsage.StandardFromFoyerMode
        '    '        LockButtons(False)
        '    '    End If

        '    Case EnumUsage.StandardFromFoyerMode

        '        Select Case Msgbox("Return back to Foyer Mode?" + vbCrLf + vbCrLf + "Click Yes to Exit to Foyer Mode." + vbCrLf + "Click No to Exit the Application Completely." + vbCrLf + "Click Cancel to do nothing.", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, "Exit")

        '            Case DialogResult.Yes
        '                UsageMode = EnumUsage.Foyer
        '                LockButtons(False)

        '            Case DialogResult.No
        '                DoExit()

        '            Case DialogResult.Cancel

        '        End Select

        '    Case Else
        '        DoExit()

        'End Select

    End Sub

    Private Sub btnHelp_Click(sender As Object, e As EventArgs) Handles btnHelp.Click
        Dim _frm As New frmHelp
        _frm.ShowDialog()
    End Sub

    Private Sub btnToiletTrip_Click(sender As Object, e As EventArgs) Handles btnToiletTrip.Click
        Dim _frm As New frmFastToilet
        _frm.ShowDialog()
    End Sub

    Private Sub btnCompliance_Click(sender As Object, e As EventArgs) Handles btnCompliance.Click
        Dim _frm As New frmCompliance
        _frm.ShowDialog()
    End Sub

    Private Sub btnStaffCentre_Click(sender As Object, e As EventArgs) Handles btnStaffCentre.Click

    End Sub

    Private Sub BackgroundWorker1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork

    End Sub

    Private Sub btnPhoto_Click(sender As Object, e As EventArgs) Handles btnPhoto.Click
        MediaHandler.TakePhoto()
    End Sub

End Class