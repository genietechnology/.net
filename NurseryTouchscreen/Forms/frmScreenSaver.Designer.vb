﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmScreenSaver
    Inherits NurseryTouchscreen.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmScreenSaver))
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling2 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling3 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling4 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling5 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim TileItemElement1 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement2 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement3 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement4 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement5 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.dashLastChange = New NurseryTouchscreen.DashboardLabel()
        Me.dashMedAll = New NurseryTouchscreen.DashboardLabel()
        Me.picLogo = New System.Windows.Forms.PictureBox()
        Me.dashChildren = New NurseryTouchscreen.DashboardLabel()
        Me.dashSpaces = New NurseryTouchscreen.DashboardLabel()
        Me.dashViewings = New NurseryTouchscreen.DashboardLabel()
        Me.dashMedRoom = New NurseryTouchscreen.DashboardLabel()
        Me.tbChildren = New DevExpress.XtraBars.Navigation.TileBar()
        Me.tbgChildren = New DevExpress.XtraBars.Navigation.TileBarGroup()
        Me.tbAllergy = New DevExpress.XtraBars.Navigation.TileBar()
        Me.tbgAllergy = New DevExpress.XtraBars.Navigation.TileBarGroup()
        Me.tbStaff = New DevExpress.XtraBars.Navigation.TileBar()
        Me.tbgStaff = New DevExpress.XtraBars.Navigation.TileBarGroup()
        Me.tbMedication = New DevExpress.XtraBars.Navigation.TileBar()
        Me.tbgMedication = New DevExpress.XtraBars.Navigation.TileBarGroup()
        Me.dashStaffRatio = New NurseryTouchscreen.DashboardLabel()
        Me.dashStaffReqd = New NurseryTouchscreen.DashboardLabel()
        Me.dashStaff = New NurseryTouchscreen.DashboardLabel()
        Me.dashAccidents = New NurseryTouchscreen.DashboardLabel()
        Me.dashBirthdays = New NurseryTouchscreen.DashboardLabel()
        Me.dashDueIn = New NurseryTouchscreen.DashboardLabel()
        Me.txtGreen = New DevExpress.XtraEditors.TextEdit()
        Me.txtOrange = New DevExpress.XtraEditors.TextEdit()
        Me.txtRed = New DevExpress.XtraEditors.TextEdit()
        Me.txtYellow = New DevExpress.XtraEditors.TextEdit()
        Me.txtCyan = New DevExpress.XtraEditors.TextEdit()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.DashboardLabel5 = New NurseryTouchscreen.DashboardLabel()
        Me.DashboardLabel6 = New NurseryTouchscreen.DashboardLabel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.DashboardLabel7 = New NurseryTouchscreen.DashboardLabel()
        Me.DashboardLabel8 = New NurseryTouchscreen.DashboardLabel()
        Me.DashboardLabel9 = New NurseryTouchscreen.DashboardLabel()
        Me.DashboardLabel10 = New NurseryTouchscreen.DashboardLabel()
        Me.TileBar6 = New DevExpress.XtraBars.Navigation.TileBar()
        Me.TileBarGroup1 = New DevExpress.XtraBars.Navigation.TileBarGroup()
        Me.TileBarItem7 = New DevExpress.XtraBars.Navigation.TileBarItem()
        Me.TileBarItem8 = New DevExpress.XtraBars.Navigation.TileBarItem()
        Me.TileBar7 = New DevExpress.XtraBars.Navigation.TileBar()
        Me.TileBarGroup7 = New DevExpress.XtraBars.Navigation.TileBarGroup()
        Me.TileBarItem9 = New DevExpress.XtraBars.Navigation.TileBarItem()
        Me.TileBar8 = New DevExpress.XtraBars.Navigation.TileBar()
        Me.TileBarGroup8 = New DevExpress.XtraBars.Navigation.TileBarGroup()
        Me.TileBarItem10 = New DevExpress.XtraBars.Navigation.TileBarItem()
        Me.TileBar9 = New DevExpress.XtraBars.Navigation.TileBar()
        Me.TileBarGroup9 = New DevExpress.XtraBars.Navigation.TileBarGroup()
        Me.TileBarItem11 = New DevExpress.XtraBars.Navigation.TileBarItem()
        Me.TileBar10 = New DevExpress.XtraBars.Navigation.TileBar()
        Me.timMinute = New System.Windows.Forms.Timer(Me.components)
        Me.btnExit = New DevExpress.XtraEditors.SimpleButton()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.picLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOrange.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCyan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel2.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel1.ColumnCount = 8
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel1.Controls.Add(Me.dashLastChange, 6, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.dashMedAll, 7, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.picLogo, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.dashChildren, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.dashSpaces, 3, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.dashViewings, 5, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.dashMedRoom, 7, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.tbChildren, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.tbAllergy, 4, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.tbStaff, 2, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.tbMedication, 6, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.dashStaffRatio, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.dashStaffReqd, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.dashStaff, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.dashAccidents, 6, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.dashBirthdays, 4, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.dashDueIn, 2, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1475, 701)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'dashLastChange
        '
        Me.dashLastChange.Colour = NurseryTouchscreen.DashboardLabel.EnumColourCode.Neutral
        Me.dashLastChange.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dashLastChange.LabelBottom = "Minutes Ago"
        Me.dashLastChange.LabelMiddle = "55"
        Me.dashLastChange.LabelTop = "Last Change"
        Me.dashLastChange.Location = New System.Drawing.Point(1107, 143)
        Me.dashLastChange.Name = "dashLastChange"
        Me.dashLastChange.Size = New System.Drawing.Size(178, 134)
        Me.dashLastChange.TabIndex = 20
        '
        'dashMedAll
        '
        Me.dashMedAll.Colour = NurseryTouchscreen.DashboardLabel.EnumColourCode.Neutral
        Me.dashMedAll.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dashMedAll.LabelBottom = "Entire Building"
        Me.dashMedAll.LabelMiddle = "55"
        Me.dashMedAll.LabelTop = "Next Medication Due"
        Me.dashMedAll.Location = New System.Drawing.Point(1291, 143)
        Me.dashMedAll.Name = "dashMedAll"
        Me.dashMedAll.Size = New System.Drawing.Size(181, 134)
        Me.dashMedAll.TabIndex = 8
        '
        'picLogo
        '
        Me.picLogo.BackColor = System.Drawing.Color.White
        Me.TableLayoutPanel1.SetColumnSpan(Me.picLogo, 4)
        Me.picLogo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.picLogo.Image = CType(resources.GetObject("picLogo.Image"), System.Drawing.Image)
        Me.picLogo.Location = New System.Drawing.Point(371, 143)
        Me.picLogo.Name = "picLogo"
        Me.picLogo.Size = New System.Drawing.Size(730, 134)
        Me.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picLogo.TabIndex = 0
        Me.picLogo.TabStop = False
        '
        'dashChildren
        '
        Me.dashChildren.Colour = NurseryTouchscreen.DashboardLabel.EnumColourCode.Neutral
        Me.dashChildren.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dashChildren.LabelBottom = "<Room Name>"
        Me.dashChildren.LabelMiddle = "55"
        Me.dashChildren.LabelTop = "Child Headcount"
        Me.dashChildren.Location = New System.Drawing.Point(3, 3)
        Me.dashChildren.Name = "dashChildren"
        Me.dashChildren.Size = New System.Drawing.Size(178, 134)
        Me.dashChildren.TabIndex = 3
        '
        'dashSpaces
        '
        Me.dashSpaces.Colour = NurseryTouchscreen.DashboardLabel.EnumColourCode.Neutral
        Me.dashSpaces.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dashSpaces.LabelBottom = "<Room Name>"
        Me.dashSpaces.LabelMiddle = "55"
        Me.dashSpaces.LabelTop = "Spaces"
        Me.dashSpaces.Location = New System.Drawing.Point(555, 3)
        Me.dashSpaces.Name = "dashSpaces"
        Me.dashSpaces.Size = New System.Drawing.Size(178, 134)
        Me.dashSpaces.TabIndex = 4
        '
        'dashViewings
        '
        Me.dashViewings.Colour = NurseryTouchscreen.DashboardLabel.EnumColourCode.Neutral
        Me.dashViewings.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dashViewings.LabelBottom = "Today"
        Me.dashViewings.LabelMiddle = ""
        Me.dashViewings.LabelTop = "Viewings"
        Me.dashViewings.Location = New System.Drawing.Point(923, 3)
        Me.dashViewings.Name = "dashViewings"
        Me.dashViewings.Size = New System.Drawing.Size(178, 134)
        Me.dashViewings.TabIndex = 5
        '
        'dashMedRoom
        '
        Me.dashMedRoom.Colour = NurseryTouchscreen.DashboardLabel.EnumColourCode.Neutral
        Me.dashMedRoom.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dashMedRoom.LabelBottom = "<Room Name>"
        Me.dashMedRoom.LabelMiddle = "55"
        Me.dashMedRoom.LabelTop = "Next Medication Due"
        Me.dashMedRoom.Location = New System.Drawing.Point(1291, 3)
        Me.dashMedRoom.Name = "dashMedRoom"
        Me.dashMedRoom.Size = New System.Drawing.Size(181, 134)
        Me.dashMedRoom.TabIndex = 6
        '
        'tbChildren
        '
        Me.tbChildren.AllowDrag = False
        Me.TableLayoutPanel1.SetColumnSpan(Me.tbChildren, 2)
        Me.tbChildren.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbChildren.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        Me.tbChildren.Groups.Add(Me.tbgChildren)
        Me.tbChildren.Location = New System.Drawing.Point(3, 283)
        Me.tbChildren.MaxId = 2
        Me.tbChildren.Name = "tbChildren"
        Me.tbChildren.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.TableLayoutPanel1.SetRowSpan(Me.tbChildren, 3)
        Me.tbChildren.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollButtons
        Me.tbChildren.ShowText = True
        Me.tbChildren.Size = New System.Drawing.Size(362, 415)
        Me.tbChildren.TabIndex = 9
        Me.tbChildren.Text = "Children Checked In"
        '
        'tbgChildren
        '
        Me.tbgChildren.Name = "tbgChildren"
        '
        'tbAllergy
        '
        Me.tbAllergy.AllowDrag = False
        Me.TableLayoutPanel1.SetColumnSpan(Me.tbAllergy, 2)
        Me.tbAllergy.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbAllergy.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        Me.tbAllergy.Groups.Add(Me.tbgAllergy)
        Me.tbAllergy.Location = New System.Drawing.Point(739, 283)
        Me.tbAllergy.MaxId = 1
        Me.tbAllergy.Name = "tbAllergy"
        Me.tbAllergy.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.TableLayoutPanel1.SetRowSpan(Me.tbAllergy, 3)
        Me.tbAllergy.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollButtons
        Me.tbAllergy.ShowText = True
        Me.tbAllergy.Size = New System.Drawing.Size(362, 415)
        Me.tbAllergy.TabIndex = 10
        Me.tbAllergy.Text = "Allergy Board"
        '
        'tbgAllergy
        '
        Me.tbgAllergy.Name = "tbgAllergy"
        '
        'tbStaff
        '
        Me.tbStaff.AllowDrag = False
        Me.TableLayoutPanel1.SetColumnSpan(Me.tbStaff, 2)
        Me.tbStaff.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbStaff.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        Me.tbStaff.Groups.Add(Me.tbgStaff)
        Me.tbStaff.Location = New System.Drawing.Point(371, 283)
        Me.tbStaff.MaxId = 1
        Me.tbStaff.Name = "tbStaff"
        Me.tbStaff.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.TableLayoutPanel1.SetRowSpan(Me.tbStaff, 3)
        Me.tbStaff.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollButtons
        Me.tbStaff.ShowText = True
        Me.tbStaff.Size = New System.Drawing.Size(362, 415)
        Me.tbStaff.TabIndex = 11
        Me.tbStaff.Text = "Staff Checked In"
        '
        'tbgStaff
        '
        Me.tbgStaff.Name = "tbgStaff"
        '
        'tbMedication
        '
        Me.tbMedication.AllowDrag = False
        Me.TableLayoutPanel1.SetColumnSpan(Me.tbMedication, 2)
        Me.tbMedication.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbMedication.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        Me.tbMedication.Groups.Add(Me.tbgMedication)
        Me.tbMedication.Location = New System.Drawing.Point(1107, 283)
        Me.tbMedication.MaxId = 1
        Me.tbMedication.Name = "tbMedication"
        Me.tbMedication.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.TableLayoutPanel1.SetRowSpan(Me.tbMedication, 3)
        Me.tbMedication.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollButtons
        Me.tbMedication.ShowText = True
        Me.tbMedication.Size = New System.Drawing.Size(365, 415)
        Me.tbMedication.TabIndex = 12
        Me.tbMedication.Text = "Medication List"
        '
        'tbgMedication
        '
        Me.tbgMedication.Name = "tbgMedication"
        '
        'dashStaffRatio
        '
        Me.dashStaffRatio.Colour = NurseryTouchscreen.DashboardLabel.EnumColourCode.Neutral
        Me.dashStaffRatio.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dashStaffRatio.LabelBottom = "<Room Name>"
        Me.dashStaffRatio.LabelMiddle = "55"
        Me.dashStaffRatio.LabelTop = "Staff Utilisation"
        Me.dashStaffRatio.Location = New System.Drawing.Point(3, 143)
        Me.dashStaffRatio.Name = "dashStaffRatio"
        Me.dashStaffRatio.Size = New System.Drawing.Size(178, 134)
        Me.dashStaffRatio.TabIndex = 14
        '
        'dashStaffReqd
        '
        Me.dashStaffReqd.Colour = NurseryTouchscreen.DashboardLabel.EnumColourCode.Neutral
        Me.dashStaffReqd.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dashStaffReqd.LabelBottom = "<Room Name>"
        Me.dashStaffReqd.LabelMiddle = "55"
        Me.dashStaffReqd.LabelTop = "Staff Required"
        Me.dashStaffReqd.Location = New System.Drawing.Point(187, 143)
        Me.dashStaffReqd.Name = "dashStaffReqd"
        Me.dashStaffReqd.Size = New System.Drawing.Size(178, 134)
        Me.dashStaffReqd.TabIndex = 15
        '
        'dashStaff
        '
        Me.dashStaff.Colour = NurseryTouchscreen.DashboardLabel.EnumColourCode.Neutral
        Me.dashStaff.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dashStaff.LabelBottom = "<Room Name>"
        Me.dashStaff.LabelMiddle = "55"
        Me.dashStaff.LabelTop = "Staff Headcount"
        Me.dashStaff.Location = New System.Drawing.Point(187, 3)
        Me.dashStaff.Name = "dashStaff"
        Me.dashStaff.Size = New System.Drawing.Size(178, 134)
        Me.dashStaff.TabIndex = 16
        '
        'dashAccidents
        '
        Me.dashAccidents.Colour = NurseryTouchscreen.DashboardLabel.EnumColourCode.Neutral
        Me.dashAccidents.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dashAccidents.LabelBottom = "<Room Name>"
        Me.dashAccidents.LabelMiddle = "55"
        Me.dashAccidents.LabelTop = "Accidents / Incidents"
        Me.dashAccidents.Location = New System.Drawing.Point(1107, 3)
        Me.dashAccidents.Name = "dashAccidents"
        Me.dashAccidents.Size = New System.Drawing.Size(178, 134)
        Me.dashAccidents.TabIndex = 22
        '
        'dashBirthdays
        '
        Me.dashBirthdays.Colour = NurseryTouchscreen.DashboardLabel.EnumColourCode.Neutral
        Me.dashBirthdays.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dashBirthdays.LabelBottom = "This Week"
        Me.dashBirthdays.LabelMiddle = "55"
        Me.dashBirthdays.LabelTop = "Birthdays"
        Me.dashBirthdays.Location = New System.Drawing.Point(739, 3)
        Me.dashBirthdays.Name = "dashBirthdays"
        Me.dashBirthdays.Size = New System.Drawing.Size(178, 134)
        Me.dashBirthdays.TabIndex = 23
        '
        'dashDueIn
        '
        Me.dashDueIn.Colour = NurseryTouchscreen.DashboardLabel.EnumColourCode.Neutral
        Me.dashDueIn.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dashDueIn.LabelBottom = "<Room Name>"
        Me.dashDueIn.LabelMiddle = "55"
        Me.dashDueIn.LabelTop = "Due In"
        Me.dashDueIn.Location = New System.Drawing.Point(371, 3)
        Me.dashDueIn.Name = "dashDueIn"
        Me.dashDueIn.Size = New System.Drawing.Size(178, 134)
        Me.dashDueIn.TabIndex = 24
        '
        'txtGreen
        '
        Me.txtGreen.Location = New System.Drawing.Point(555, 683)
        Me.txtGreen.Name = "txtGreen"
        Me.txtGreen.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtGreen.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtGreen, True)
        Me.txtGreen.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtGreen, OptionsSpelling1)
        Me.txtGreen.TabIndex = 28
        Me.txtGreen.Visible = False
        '
        'txtOrange
        '
        Me.txtOrange.Location = New System.Drawing.Point(187, 683)
        Me.txtOrange.Name = "txtOrange"
        Me.txtOrange.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtOrange.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtOrange, True)
        Me.txtOrange.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtOrange, OptionsSpelling2)
        Me.txtOrange.TabIndex = 26
        Me.txtOrange.Visible = False
        '
        'txtRed
        '
        Me.txtRed.Location = New System.Drawing.Point(371, 683)
        Me.txtRed.Name = "txtRed"
        Me.txtRed.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtRed.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtRed, True)
        Me.txtRed.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtRed, OptionsSpelling3)
        Me.txtRed.TabIndex = 25
        Me.txtRed.Visible = False
        '
        'txtYellow
        '
        Me.txtYellow.Location = New System.Drawing.Point(739, 683)
        Me.txtYellow.Name = "txtYellow"
        Me.txtYellow.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtYellow.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtYellow, True)
        Me.txtYellow.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtYellow, OptionsSpelling4)
        Me.txtYellow.TabIndex = 27
        Me.txtYellow.Visible = False
        '
        'txtCyan
        '
        Me.txtCyan.Location = New System.Drawing.Point(3, 683)
        Me.txtCyan.Name = "txtCyan"
        Me.txtCyan.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtCyan.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtCyan, True)
        Me.txtCyan.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtCyan, OptionsSpelling5)
        Me.txtCyan.TabIndex = 29
        Me.txtCyan.Visible = False
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 8
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel2.Controls.Add(Me.DashboardLabel5, 6, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.DashboardLabel6, 7, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.PictureBox1, 2, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.DashboardLabel7, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.DashboardLabel8, 2, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.DashboardLabel9, 5, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.DashboardLabel10, 7, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.TileBar6, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.TileBar7, 1, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.TileBar8, 2, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.TileBar9, 3, 2)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 3
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(200, 100)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'DashboardLabel5
        '
        Me.DashboardLabel5.Colour = NurseryTouchscreen.DashboardLabel.EnumColourCode.Neutral
        Me.DashboardLabel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DashboardLabel5.LabelBottom = "<Room Name>"
        Me.DashboardLabel5.LabelMiddle = "55"
        Me.DashboardLabel5.LabelTop = "Last Change"
        Me.DashboardLabel5.Location = New System.Drawing.Point(153, 3)
        Me.DashboardLabel5.Name = "DashboardLabel5"
        Me.DashboardLabel5.Size = New System.Drawing.Size(19, 14)
        Me.DashboardLabel5.TabIndex = 20
        '
        'DashboardLabel6
        '
        Me.DashboardLabel6.Colour = NurseryTouchscreen.DashboardLabel.EnumColourCode.Neutral
        Me.DashboardLabel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DashboardLabel6.LabelBottom = "Entire Building"
        Me.DashboardLabel6.LabelMiddle = "55"
        Me.DashboardLabel6.LabelTop = "Next Medication Due"
        Me.DashboardLabel6.Location = New System.Drawing.Point(178, 23)
        Me.DashboardLabel6.Name = "DashboardLabel6"
        Me.DashboardLabel6.Size = New System.Drawing.Size(19, 14)
        Me.DashboardLabel6.TabIndex = 8
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.White
        Me.TableLayoutPanel2.SetColumnSpan(Me.PictureBox1, 4)
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(53, 23)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(94, 14)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'DashboardLabel7
        '
        Me.DashboardLabel7.Colour = NurseryTouchscreen.DashboardLabel.EnumColourCode.Neutral
        Me.DashboardLabel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DashboardLabel7.LabelBottom = "<Room Name>"
        Me.DashboardLabel7.LabelMiddle = "55"
        Me.DashboardLabel7.LabelTop = "Child Headcount"
        Me.DashboardLabel7.Location = New System.Drawing.Point(3, 3)
        Me.DashboardLabel7.Name = "DashboardLabel7"
        Me.DashboardLabel7.Size = New System.Drawing.Size(19, 14)
        Me.DashboardLabel7.TabIndex = 3
        '
        'DashboardLabel8
        '
        Me.DashboardLabel8.Colour = NurseryTouchscreen.DashboardLabel.EnumColourCode.Neutral
        Me.DashboardLabel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DashboardLabel8.LabelBottom = "<Room Name>"
        Me.DashboardLabel8.LabelMiddle = "55"
        Me.DashboardLabel8.LabelTop = "Spaces"
        Me.DashboardLabel8.Location = New System.Drawing.Point(53, 3)
        Me.DashboardLabel8.Name = "DashboardLabel8"
        Me.DashboardLabel8.Size = New System.Drawing.Size(19, 14)
        Me.DashboardLabel8.TabIndex = 4
        '
        'DashboardLabel9
        '
        Me.DashboardLabel9.Colour = NurseryTouchscreen.DashboardLabel.EnumColourCode.Neutral
        Me.DashboardLabel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DashboardLabel9.LabelBottom = "<Room Name>"
        Me.DashboardLabel9.LabelMiddle = ""
        Me.DashboardLabel9.LabelTop = "Observations"
        Me.DashboardLabel9.Location = New System.Drawing.Point(128, 3)
        Me.DashboardLabel9.Name = "DashboardLabel9"
        Me.DashboardLabel9.Size = New System.Drawing.Size(19, 14)
        Me.DashboardLabel9.TabIndex = 5
        '
        'DashboardLabel10
        '
        Me.DashboardLabel10.Colour = NurseryTouchscreen.DashboardLabel.EnumColourCode.Neutral
        Me.DashboardLabel10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DashboardLabel10.LabelBottom = "<Room Name>"
        Me.DashboardLabel10.LabelMiddle = "55"
        Me.DashboardLabel10.LabelTop = "Next Medication Due"
        Me.DashboardLabel10.Location = New System.Drawing.Point(178, 3)
        Me.DashboardLabel10.Name = "DashboardLabel10"
        Me.DashboardLabel10.Size = New System.Drawing.Size(19, 14)
        Me.DashboardLabel10.TabIndex = 6
        '
        'TileBar6
        '
        Me.TileBar6.AllowDrag = False
        Me.TileBar6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TileBar6.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        Me.TileBar6.Groups.Add(Me.TileBarGroup1)
        Me.TileBar6.Location = New System.Drawing.Point(3, 43)
        Me.TileBar6.MaxId = 2
        Me.TileBar6.Name = "TileBar6"
        Me.TileBar6.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.TableLayoutPanel2.SetRowSpan(Me.TileBar6, 2)
        Me.TileBar6.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollButtons
        Me.TileBar6.Size = New System.Drawing.Size(19, 54)
        Me.TileBar6.TabIndex = 9
        Me.TileBar6.Text = "TileBar6"
        '
        'TileBarGroup1
        '
        Me.TileBarGroup1.Items.Add(Me.TileBarItem7)
        Me.TileBarGroup1.Items.Add(Me.TileBarItem8)
        Me.TileBarGroup1.Name = "TileBarGroup1"
        '
        'TileBarItem7
        '
        Me.TileBarItem7.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        TileItemElement1.Text = "TileBarItem1"
        Me.TileBarItem7.Elements.Add(TileItemElement1)
        Me.TileBarItem7.Id = 0
        Me.TileBarItem7.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.Wide
        Me.TileBarItem7.Name = "TileBarItem7"
        '
        'TileBarItem8
        '
        Me.TileBarItem8.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        TileItemElement2.Text = "TileBarItem6"
        Me.TileBarItem8.Elements.Add(TileItemElement2)
        Me.TileBarItem8.Id = 1
        Me.TileBarItem8.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.Wide
        Me.TileBarItem8.Name = "TileBarItem8"
        '
        'TileBar7
        '
        Me.TileBar7.AllowDrag = False
        Me.TileBar7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TileBar7.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        Me.TileBar7.Groups.Add(Me.TileBarGroup7)
        Me.TileBar7.Location = New System.Drawing.Point(28, 43)
        Me.TileBar7.MaxId = 1
        Me.TileBar7.Name = "TileBar7"
        Me.TileBar7.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.TableLayoutPanel2.SetRowSpan(Me.TileBar7, 2)
        Me.TileBar7.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollButtons
        Me.TileBar7.Size = New System.Drawing.Size(19, 54)
        Me.TileBar7.TabIndex = 10
        Me.TileBar7.Text = "TileBar7"
        '
        'TileBarGroup7
        '
        Me.TileBarGroup7.Items.Add(Me.TileBarItem9)
        Me.TileBarGroup7.Name = "TileBarGroup7"
        '
        'TileBarItem9
        '
        Me.TileBarItem9.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        TileItemElement3.Text = "TileBarItem2"
        Me.TileBarItem9.Elements.Add(TileItemElement3)
        Me.TileBarItem9.Id = 0
        Me.TileBarItem9.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.Wide
        Me.TileBarItem9.Name = "TileBarItem9"
        '
        'TileBar8
        '
        Me.TileBar8.AllowDrag = False
        Me.TileBar8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TileBar8.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        Me.TileBar8.Groups.Add(Me.TileBarGroup8)
        Me.TileBar8.Location = New System.Drawing.Point(53, 43)
        Me.TileBar8.MaxId = 1
        Me.TileBar8.Name = "TileBar8"
        Me.TileBar8.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.TableLayoutPanel2.SetRowSpan(Me.TileBar8, 2)
        Me.TileBar8.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollButtons
        Me.TileBar8.Size = New System.Drawing.Size(19, 54)
        Me.TileBar8.TabIndex = 11
        Me.TileBar8.Text = "TileBar8"
        '
        'TileBarGroup8
        '
        Me.TileBarGroup8.Items.Add(Me.TileBarItem10)
        Me.TileBarGroup8.Name = "TileBarGroup8"
        '
        'TileBarItem10
        '
        Me.TileBarItem10.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        TileItemElement4.Text = "TileBarItem3"
        Me.TileBarItem10.Elements.Add(TileItemElement4)
        Me.TileBarItem10.Id = 0
        Me.TileBarItem10.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.Wide
        Me.TileBarItem10.Name = "TileBarItem10"
        '
        'TileBar9
        '
        Me.TileBar9.AllowDrag = False
        Me.TileBar9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TileBar9.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        Me.TileBar9.Groups.Add(Me.TileBarGroup9)
        Me.TileBar9.Location = New System.Drawing.Point(78, 43)
        Me.TileBar9.MaxId = 1
        Me.TileBar9.Name = "TileBar9"
        Me.TileBar9.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.TableLayoutPanel2.SetRowSpan(Me.TileBar9, 2)
        Me.TileBar9.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollButtons
        Me.TileBar9.Size = New System.Drawing.Size(19, 54)
        Me.TileBar9.TabIndex = 12
        Me.TileBar9.Text = "TileBar9"
        '
        'TileBarGroup9
        '
        Me.TileBarGroup9.Items.Add(Me.TileBarItem11)
        Me.TileBarGroup9.Name = "TileBarGroup9"
        '
        'TileBarItem11
        '
        Me.TileBarItem11.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        TileItemElement5.Text = "TileBarItem4"
        Me.TileBarItem11.Elements.Add(TileItemElement5)
        Me.TileBarItem11.Id = 0
        Me.TileBarItem11.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.Wide
        Me.TileBarItem11.Name = "TileBarItem11"
        '
        'TileBar10
        '
        Me.TileBar10.AllowDrag = False
        Me.TileBar10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TileBar10.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        Me.TileBar10.Location = New System.Drawing.Point(0, 0)
        Me.TileBar10.Name = "TileBar10"
        Me.TileBar10.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollButtons
        Me.TileBar10.Size = New System.Drawing.Size(240, 150)
        Me.TileBar10.TabIndex = 0
        '
        'timMinute
        '
        Me.timMinute.Interval = 60000
        '
        'btnExit
        '
        Me.btnExit.DialogResult = DialogResult.Cancel
        Me.btnExit.Location = New System.Drawing.Point(0, 0)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(120, 23)
        Me.btnExit.TabIndex = 78
        Me.btnExit.Text = "Exit Button (Hidden)"
        Me.btnExit.Visible = False
        '
        'frmScreenSaver
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(1475, 701)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.MaximizeBox = false
        Me.MinimizeBox = false
        Me.Name = "frmScreenSaver"
        Me.Text = "Nursery Genie Version x.x.x.x"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Controls.SetChildIndex(Me.TableLayoutPanel1, 0)
        Me.Controls.SetChildIndex(Me.btnExit, 0)
        Me.TableLayoutPanel1.ResumeLayout(false)
        CType(Me.picLogo,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.txtGreen.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.txtOrange.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.txtRed.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.txtYellow.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.txtCyan.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.TableLayoutPanel2.ResumeLayout(false)
        CType(Me.PictureBox1,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)

End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents picLogo As System.Windows.Forms.PictureBox
    Friend WithEvents dashMedAll As NurseryTouchscreen.DashboardLabel
    Friend WithEvents dashChildren As NurseryTouchscreen.DashboardLabel
    Friend WithEvents dashSpaces As NurseryTouchscreen.DashboardLabel
    Friend WithEvents dashViewings As NurseryTouchscreen.DashboardLabel
    Friend WithEvents dashMedRoom As NurseryTouchscreen.DashboardLabel
    Friend WithEvents tbChildren As DevExpress.XtraBars.Navigation.TileBar
    Friend WithEvents tbgChildren As DevExpress.XtraBars.Navigation.TileBarGroup
    Friend WithEvents tbAllergy As DevExpress.XtraBars.Navigation.TileBar
    Friend WithEvents tbgAllergy As DevExpress.XtraBars.Navigation.TileBarGroup
    Friend WithEvents tbStaff As DevExpress.XtraBars.Navigation.TileBar
    Friend WithEvents tbgStaff As DevExpress.XtraBars.Navigation.TileBarGroup
    Friend WithEvents tbMedication As DevExpress.XtraBars.Navigation.TileBar
    Friend WithEvents tbgMedication As DevExpress.XtraBars.Navigation.TileBarGroup
    Friend WithEvents dashStaffRatio As NurseryTouchscreen.DashboardLabel
    Friend WithEvents dashStaffReqd As NurseryTouchscreen.DashboardLabel
    Friend WithEvents dashLastChange As NurseryTouchscreen.DashboardLabel
    Friend WithEvents dashStaff As NurseryTouchscreen.DashboardLabel
    Friend WithEvents dashAccidents As NurseryTouchscreen.DashboardLabel
    Friend WithEvents dashBirthdays As NurseryTouchscreen.DashboardLabel
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents DashboardLabel5 As NurseryTouchscreen.DashboardLabel
    Friend WithEvents DashboardLabel6 As NurseryTouchscreen.DashboardLabel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents DashboardLabel7 As NurseryTouchscreen.DashboardLabel
    Friend WithEvents DashboardLabel8 As NurseryTouchscreen.DashboardLabel
    Friend WithEvents DashboardLabel9 As NurseryTouchscreen.DashboardLabel
    Friend WithEvents DashboardLabel10 As NurseryTouchscreen.DashboardLabel
    Friend WithEvents TileBar6 As DevExpress.XtraBars.Navigation.TileBar
    Friend WithEvents TileBarGroup1 As DevExpress.XtraBars.Navigation.TileBarGroup
    Friend WithEvents TileBarItem7 As DevExpress.XtraBars.Navigation.TileBarItem
    Friend WithEvents TileBarItem8 As DevExpress.XtraBars.Navigation.TileBarItem
    Friend WithEvents TileBar7 As DevExpress.XtraBars.Navigation.TileBar
    Friend WithEvents TileBarGroup7 As DevExpress.XtraBars.Navigation.TileBarGroup
    Friend WithEvents TileBarItem9 As DevExpress.XtraBars.Navigation.TileBarItem
    Friend WithEvents TileBar8 As DevExpress.XtraBars.Navigation.TileBar
    Friend WithEvents TileBarGroup8 As DevExpress.XtraBars.Navigation.TileBarGroup
    Friend WithEvents TileBarItem10 As DevExpress.XtraBars.Navigation.TileBarItem
    Friend WithEvents TileBar9 As DevExpress.XtraBars.Navigation.TileBar
    Friend WithEvents TileBarGroup9 As DevExpress.XtraBars.Navigation.TileBarGroup
    Friend WithEvents TileBarItem11 As DevExpress.XtraBars.Navigation.TileBarItem
    Friend WithEvents TileBar10 As DevExpress.XtraBars.Navigation.TileBar
    Friend WithEvents dashDueIn As NurseryTouchscreen.DashboardLabel
    Friend WithEvents timMinute As System.Windows.Forms.Timer
    Friend WithEvents txtGreen As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtOrange As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRed As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtYellow As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCyan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btnExit As DevExpress.XtraEditors.SimpleButton

End Class
