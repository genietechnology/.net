﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmObs
    Inherits NurseryTouchscreen.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnPhoto = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMediaDelete = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMediaCapture = New DevExpress.XtraEditors.SimpleButton()
        Me.btnChildDelete = New DevExpress.XtraEditors.SimpleButton()
        Me.fldStaff = New NurseryTouchscreen.Field()
        Me.fldComments = New NurseryTouchscreen.Field()
        Me.txtComments = New DevExpress.XtraEditors.MemoEdit()
        Me.btnChildAdd = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSave = New DevExpress.XtraEditors.SimpleButton()
        Me.fldTime = New NurseryTouchscreen.Field()
        Me.fldTitle = New NurseryTouchscreen.Field()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.btnMediaRight = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMediaLeft = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.picMedia = New System.Windows.Forms.PictureBox()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.tgChildren = New NurseryTouchscreen.TouchGrid()
        CType(Me.txtComments.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.picMedia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnPhoto
        '
        Me.btnPhoto.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPhoto.Appearance.Options.UseFont = True
        Me.btnPhoto.Image = Global.NurseryTouchscreen.My.Resources.Resources.camera_32
        Me.btnPhoto.Location = New System.Drawing.Point(3, 3)
        Me.btnPhoto.Name = "btnPhoto"
        Me.btnPhoto.Size = New System.Drawing.Size(190, 45)
        Me.btnPhoto.TabIndex = 1
        Me.btnPhoto.Text = "Take Photo"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton2.Appearance.Options.UseFont = True
        Me.SimpleButton2.Image = Global.NurseryTouchscreen.My.Resources.Resources.Highlightmarker_red_32
        Me.SimpleButton2.Location = New System.Drawing.Point(199, 3)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(362, 45)
        Me.SimpleButton2.TabIndex = 2
        Me.SimpleButton2.Text = "Start Assessment"
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Image = Global.NurseryTouchscreen.My.Resources.Resources.cancel_32
        Me.btnCancel.Location = New System.Drawing.Point(826, 481)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(183, 45)
        Me.btnCancel.TabIndex = 100
        Me.btnCancel.Text = "Cancel"
        '
        'btnMediaDelete
        '
        Me.btnMediaDelete.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMediaDelete.Appearance.Options.UseFont = True
        Me.btnMediaDelete.Image = Global.NurseryTouchscreen.My.Resources.Resources.delete_32
        Me.btnMediaDelete.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnMediaDelete.Location = New System.Drawing.Point(266, 122)
        Me.btnMediaDelete.Name = "btnMediaDelete"
        Me.btnMediaDelete.Size = New System.Drawing.Size(45, 45)
        Me.btnMediaDelete.TabIndex = 98
        '
        'btnMediaCapture
        '
        Me.btnMediaCapture.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMediaCapture.Appearance.Options.UseFont = True
        Me.btnMediaCapture.Image = Global.NurseryTouchscreen.My.Resources.Resources.camera_32
        Me.btnMediaCapture.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnMediaCapture.Location = New System.Drawing.Point(215, 122)
        Me.btnMediaCapture.Name = "btnMediaCapture"
        Me.btnMediaCapture.Size = New System.Drawing.Size(45, 45)
        Me.btnMediaCapture.TabIndex = 97
        '
        'btnChildDelete
        '
        Me.btnChildDelete.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnChildDelete.Appearance.Options.UseFont = True
        Me.btnChildDelete.Image = Global.NurseryTouchscreen.My.Resources.Resources.delete_32
        Me.btnChildDelete.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnChildDelete.Location = New System.Drawing.Point(582, 122)
        Me.btnChildDelete.Name = "btnChildDelete"
        Me.btnChildDelete.Size = New System.Drawing.Size(45, 45)
        Me.btnChildDelete.TabIndex = 95
        '
        'fldStaff
        '
        Me.fldStaff.ButtonCustom = True
        Me.fldStaff.ButtonSQL = ""
        Me.fldStaff.ButtonText = ""
        Me.fldStaff.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Lookup
        Me.fldStaff.ButtonWidth = 50.0!
        Me.fldStaff.FamilyID = Nothing
        Me.fldStaff.HideText = False
        Me.fldStaff.LabelText = "Staff"
        Me.fldStaff.LabelWidth = 50.0!
        Me.fldStaff.Location = New System.Drawing.Point(12, 65)
        Me.fldStaff.MaximumSize = New System.Drawing.Size(1000, 51)
        Me.fldStaff.MinimumSize = New System.Drawing.Size(200, 51)
        Me.fldStaff.Name = "fldStaff"
        Me.fldStaff.QuickTextList = Nothing
        Me.fldStaff.Size = New System.Drawing.Size(352, 51)
        Me.fldStaff.TabIndex = 94
        Me.fldStaff.ValueID = Nothing
        Me.fldStaff.ValueText = ""
        '
        'fldComments
        '
        Me.fldComments.ButtonCustom = False
        Me.fldComments.ButtonSQL = Nothing
        Me.fldComments.ButtonText = ""
        Me.fldComments.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Enter
        Me.fldComments.ButtonWidth = 50.0!
        Me.fldComments.FamilyID = Nothing
        Me.fldComments.HideText = True
        Me.fldComments.LabelText = "Comments"
        Me.fldComments.LabelWidth = 100.0!
        Me.fldComments.Location = New System.Drawing.Point(636, 65)
        Me.fldComments.MaximumSize = New System.Drawing.Size(0, 51)
        Me.fldComments.MinimumSize = New System.Drawing.Size(200, 51)
        Me.fldComments.Name = "fldComments"
        Me.fldComments.QuickTextList = ""
        Me.fldComments.Size = New System.Drawing.Size(376, 51)
        Me.fldComments.TabIndex = 93
        Me.fldComments.ValueID = Nothing
        Me.fldComments.ValueText = ""
        '
        'txtComments
        '
        Me.txtComments.Location = New System.Drawing.Point(636, 122)
        Me.txtComments.Name = "txtComments"
        Me.txtComments.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtComments.Properties.Appearance.Options.UseFont = True
        Me.txtComments.Size = New System.Drawing.Size(373, 353)
        Me.txtComments.TabIndex = 92
        '
        'btnChildAdd
        '
        Me.btnChildAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnChildAdd.Appearance.Options.UseFont = True
        Me.btnChildAdd.Image = Global.NurseryTouchscreen.My.Resources.Resources.add_32
        Me.btnChildAdd.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnChildAdd.Location = New System.Drawing.Point(531, 122)
        Me.btnChildAdd.Name = "btnChildAdd"
        Me.btnChildAdd.Size = New System.Drawing.Size(45, 45)
        Me.btnChildAdd.TabIndex = 90
        '
        'btnSave
        '
        Me.btnSave.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Appearance.Options.UseFont = True
        Me.btnSave.Image = Global.NurseryTouchscreen.My.Resources.Resources.success_32
        Me.btnSave.Location = New System.Drawing.Point(637, 481)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(183, 45)
        Me.btnSave.TabIndex = 88
        Me.btnSave.Text = "Accept"
        '
        'fldTime
        '
        Me.fldTime.ButtonCustom = False
        Me.fldTime.ButtonSQL = Nothing
        Me.fldTime.ButtonText = ""
        Me.fldTime.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Time
        Me.fldTime.ButtonWidth = 50.0!
        Me.fldTime.FamilyID = Nothing
        Me.fldTime.HideText = False
        Me.fldTime.LabelText = "Time"
        Me.fldTime.LabelWidth = 50.0!
        Me.fldTime.Location = New System.Drawing.Point(370, 65)
        Me.fldTime.MaximumSize = New System.Drawing.Size(0, 51)
        Me.fldTime.MinimumSize = New System.Drawing.Size(200, 51)
        Me.fldTime.Name = "fldTime"
        Me.fldTime.QuickTextList = Nothing
        Me.fldTime.Size = New System.Drawing.Size(260, 51)
        Me.fldTime.TabIndex = 1
        Me.fldTime.ValueID = Nothing
        Me.fldTime.ValueText = ""
        '
        'fldTitle
        '
        Me.fldTitle.ButtonCustom = False
        Me.fldTitle.ButtonSQL = Nothing
        Me.fldTitle.ButtonText = ""
        Me.fldTitle.ButtonType = NurseryTouchscreen.Field.EnumButtonType.Enter
        Me.fldTitle.ButtonWidth = 50.0!
        Me.fldTitle.FamilyID = Nothing
        Me.fldTitle.HideText = False
        Me.fldTitle.LabelText = "Title"
        Me.fldTitle.LabelWidth = 50.0!
        Me.fldTitle.Location = New System.Drawing.Point(12, 8)
        Me.fldTitle.MaximumSize = New System.Drawing.Size(0, 51)
        Me.fldTitle.MinimumSize = New System.Drawing.Size(400, 51)
        Me.fldTitle.Name = "fldTitle"
        Me.fldTitle.QuickTextList = ""
        Me.fldTitle.Size = New System.Drawing.Size(1000, 51)
        Me.fldTitle.TabIndex = 3
        Me.fldTitle.ValueID = Nothing
        Me.fldTitle.ValueText = ""
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(15, 133)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(67, 25)
        Me.LabelControl1.TabIndex = 102
        Me.LabelControl1.Text = "Pictures"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(373, 133)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(72, 25)
        Me.LabelControl2.TabIndex = 103
        Me.LabelControl2.Text = "Children"
        '
        'btnMediaRight
        '
        Me.btnMediaRight.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMediaRight.Appearance.Options.UseFont = True
        Me.btnMediaRight.Image = Global.NurseryTouchscreen.My.Resources.Resources.navigate_right_32
        Me.btnMediaRight.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnMediaRight.Location = New System.Drawing.Point(317, 122)
        Me.btnMediaRight.Name = "btnMediaRight"
        Me.btnMediaRight.Size = New System.Drawing.Size(45, 45)
        Me.btnMediaRight.TabIndex = 105
        '
        'btnMediaLeft
        '
        Me.btnMediaLeft.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMediaLeft.Appearance.Options.UseFont = True
        Me.btnMediaLeft.Image = Global.NurseryTouchscreen.My.Resources.Resources.navigate_left_32
        Me.btnMediaLeft.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnMediaLeft.Location = New System.Drawing.Point(164, 122)
        Me.btnMediaLeft.Name = "btnMediaLeft"
        Me.btnMediaLeft.Size = New System.Drawing.Size(45, 45)
        Me.btnMediaLeft.TabIndex = 104
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.picMedia)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 173)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(350, 353)
        Me.GroupControl1.TabIndex = 107
        Me.GroupControl1.Text = "Selected Media"
        '
        'picMedia
        '
        Me.picMedia.Dock = System.Windows.Forms.DockStyle.Fill
        Me.picMedia.Location = New System.Drawing.Point(2, 20)
        Me.picMedia.Name = "picMedia"
        Me.picMedia.Size = New System.Drawing.Size(346, 331)
        Me.picMedia.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picMedia.TabIndex = 107
        Me.picMedia.TabStop = False
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.tgChildren)
        Me.GroupControl2.Location = New System.Drawing.Point(368, 173)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(259, 353)
        Me.GroupControl2.TabIndex = 108
        Me.GroupControl2.Text = "Tagged Children"
        '
        'tgChildren
        '
        Me.tgChildren.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tgChildren.HideFirstColumn = False
        Me.tgChildren.Location = New System.Drawing.Point(2, 20)
        Me.tgChildren.Name = "tgChildren"
        Me.tgChildren.Size = New System.Drawing.Size(255, 331)
        Me.tgChildren.TabIndex = 88
        '
        'frmObs
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(1024, 538)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.btnMediaRight)
        Me.Controls.Add(Me.btnMediaLeft)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnMediaDelete)
        Me.Controls.Add(Me.btnMediaCapture)
        Me.Controls.Add(Me.btnChildDelete)
        Me.Controls.Add(Me.fldStaff)
        Me.Controls.Add(Me.fldComments)
        Me.Controls.Add(Me.txtComments)
        Me.Controls.Add(Me.btnChildAdd)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.fldTime)
        Me.Controls.Add(Me.fldTitle)
        Me.Name = "frmObs"
        Me.Text = "Quick Observation"
        CType(Me.txtComments.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.picMedia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents fldTitle As NurseryTouchscreen.Field
    Friend WithEvents fldTime As NurseryTouchscreen.Field
    Friend WithEvents btnPhoto As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnChildAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtComments As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents fldComments As NurseryTouchscreen.Field
    Friend WithEvents fldStaff As NurseryTouchscreen.Field
    Friend WithEvents btnChildDelete As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMediaCapture As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMediaDelete As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnMediaRight As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMediaLeft As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents picMedia As System.Windows.Forms.PictureBox
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents tgChildren As NurseryTouchscreen.TouchGrid

End Class
