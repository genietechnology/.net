﻿Public Class frmCompliance
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub btnChecklists_Click(sender As Object, e As EventArgs) Handles btnChecklists.Click
        Dim _frm As New frmChecklists
        _frm.ShowDialog()
    End Sub

    Private Sub btnRiskAssessments_Click(sender As Object, e As EventArgs) Handles btnRiskAssessments.Click
        Dim _frm As New frmRiskManagement
        _frm.ShowDialog()
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        Msgbox("COSHH Register under development..." + vbCrLf + vbCrLf + "We'll be in touch when the function is available.")
    End Sub
End Class
