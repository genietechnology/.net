﻿Imports NurseryTouchscreen.SharedModule

Public Class frmBaseChildAddRemove

    Private m_Nappies As Boolean = False
    Private m_Milk As Boolean = False
    Private m_BabyFood As Boolean = False
    Private m_OffMenu As Boolean = False
    Private m_FamilyID As String = ""

    Private m_Allergies As String = ""
    Private m_Medication As String = ""
    Private m_Notes As String = ""
    Private m_AllergyRating As String = ""

    Private m_DisableHeader As Boolean = False

    Private m_ButtonFont As New Font("Segoe UI", 14.25, FontStyle.Bold)

    Private m_IsNew As Boolean = True
    Private m_RecordID As String = ""
    Private m_ChildID As String = ""
    Private m_ChildName As String = ""
    Private m_ChildForename As String = ""

    Private Sub frmBaseChildNew_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Me.DesignMode Then Exit Sub

        If Parameters.MaximiseForms Then
            Me.WindowState = FormWindowState.Maximized
        End If

        If Debugger.IsAttached Then
            Me.TopMost = False
        End If

        scrStaff.Populate()

        SetAllergyButtonStatus(btnAllergies, m_Allergies, m_AllergyRating, m_OffMenu)
        SetButtonStatus(btnMedication, m_Medication)
        SetButtonStatus(btnNotes, m_Notes)

    End Sub

#Region "Properties"

    Public ReadOnly Property IsNew As Boolean
        Get
            Return m_IsNew
        End Get
    End Property

    Public Property RecordID As String
        Get
            Return m_RecordID
        End Get
        Set(value As String)
            m_RecordID = value
            If m_RecordID = "" Then
                m_IsNew = True
            Else
                m_IsNew = False
            End If
        End Set
    End Property

    Public Property ChildID() As String
        Get
            Return m_ChildID
        End Get
        Set(value As String)
            m_ChildID = value
            SetChildProperties()
        End Set
    End Property

    Public ReadOnly Property ChildName() As String
        Get
            Return m_ChildName
        End Get
    End Property

    Public ReadOnly Property ChildForename() As String
        Get
            Return m_ChildForename
        End Get
    End Property

    Public ReadOnly Property Nappies() As Boolean
        Get
            Return m_Nappies
        End Get
    End Property

    Public ReadOnly Property Milk() As Boolean
        Get
            Return m_Milk
        End Get
    End Property

    Public ReadOnly Property BabyFood() As Boolean
        Get
            Return m_BabyFood
        End Get
    End Property

    Public ReadOnly Property OffMenu() As Boolean
        Get
            Return m_OffMenu
        End Get
    End Property

    Public ReadOnly Property FamilyID As String
        Get
            Return m_FamilyID
        End Get
    End Property

#End Region

#Region "Overrideables"

    Protected Overridable Function BeforeCommitUpdate() As Boolean
        Return True
    End Function

    Protected Overridable Sub CommitUpdate()
        Msgbox("CommitUpdate must be overridden!")
    End Sub

#End Region

#Region "Child"

    Private Sub SetAllergyButtonStatus(ByRef ButtonIn As DevExpress.XtraEditors.SimpleButton, ByVal Allergies As String, ByVal AllergyRating As String, ByVal OffMenu As Boolean)

        Select Case AllergyRating

            Case "Moderate"
                ButtonIn.Enabled = True
                ButtonIn.Appearance.BackColor = Color.Gold

            Case "Serious"
                ButtonIn.Enabled = True
                ButtonIn.Appearance.BackColor = Color.LightCoral

            Case Else
                If OffMenu Then
                    ButtonIn.Enabled = True
                    ButtonIn.Appearance.BackColor = Color.LightGreen
                Else
                    ButtonIn.Appearance.Reset()
                    ButtonIn.Appearance.Font = m_ButtonFont
                    ButtonIn.Enabled = False
                End If

        End Select

    End Sub

    Private Sub SetButtonStatus(ByRef ButtonIn As DevExpress.XtraEditors.SimpleButton, ByVal Contents As String)
        If Contents.Trim = "" Then
            ButtonIn.Appearance.Reset()
            ButtonIn.Appearance.Font = m_ButtonFont
            ButtonIn.Enabled = False
        Else
            ButtonIn.Enabled = True
            ButtonIn.Appearance.BackColor = Color.Yellow
        End If
    End Sub

    Private Sub SetChildProperties()

        ResetProperties()

        If ChildID Is Nothing Then Exit Sub
        If ChildID = "" Then Exit Sub

        Dim sqlFilter As String = ""
        Dim hideChildName As Boolean = Parameters.HideChildName

        If hideChildName Then
            sqlFilter = "select knownas as 'name',"
        Else
            sqlFilter = "select fullname as 'name',"
        End If

        sqlFilter &= String.Concat(" forename, family_id, group_id, nappies, milk, baby_food, off_menu,",
                                   " med_allergies, med_medication, med_notes, allergy_rating",
                                   " from children",
                                   " where id = '", ChildID, "'")

        Dim row As DataRow = DAL.ReturnDataRow(sqlFilter)
        If row IsNot Nothing Then

            m_ChildName = row.Item("name").ToString
            m_ChildForename = row.Item("forename").ToString
            txtChild.Text = m_ChildName

            m_FamilyID = row.Item("family_id").ToString
            m_Allergies = row.Item("med_allergies").ToString.Trim
            m_Medication = row.Item("med_medication").ToString.Trim
            m_Notes = row.Item("med_notes").ToString.Trim
            m_AllergyRating = row.Item("allergy_rating").ToString.Trim

            If Boolean.Parse(row.Item("nappies").ToString) Then
                m_Nappies = True
            Else
                m_Nappies = False
            End If

            If Boolean.Parse(row.Item("milk").ToString) Then
                m_Milk = True
            Else
                m_Milk = False
            End If

            If Boolean.Parse(row.Item("baby_food").ToString) Then
                m_BabyFood = True
            Else
                m_BabyFood = False
            End If

            If Boolean.Parse(row.Item("off_menu").ToString) Then
                m_OffMenu = True
            Else
                m_OffMenu = False
            End If

            row = Nothing

        End If

        row = Nothing

        SetAllergyButtonStatus(btnAllergies, m_Allergies, m_AllergyRating, m_OffMenu)
        SetButtonStatus(btnMedication, m_Medication)
        SetButtonStatus(btnNotes, m_Notes)

    End Sub

#End Region

    Private Function SaveScreen() As Boolean

        If Not btnAccept.Enabled Then Return True

        If Not BeforeCommitUpdate() Then Return False

        CommitUpdate()

        Return True

    End Function

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnAccept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccept.Click

        If BeforeCommitUpdate() Then

            btnAccept.Enabled = False
            Application.DoEvents()

            CommitUpdate()

            btnAccept.Enabled = True
            Application.DoEvents()

            Me.Close()

        End If

    End Sub

    Private Sub btnAllergies_Click(sender As Object, e As EventArgs) Handles btnAllergies.Click
        ZoomText(m_Allergies)
    End Sub

    Private Sub btnMedication_Click(sender As Object, e As EventArgs) Handles btnMedication.Click
        ZoomText(m_Medication)
    End Sub

    Private Sub btnNotes_Click(sender As Object, e As EventArgs) Handles btnNotes.Click
        ZoomText(m_Notes)
    End Sub

    Private Sub ResetProperties()

        m_FamilyID = ""
        m_ChildName = ""
        m_Allergies = ""
        m_Medication = ""
        m_Notes = ""
        m_AllergyRating = ""

        m_Nappies = False
        m_Milk = False
        m_BabyFood = False
        m_OffMenu = False

    End Sub

End Class
