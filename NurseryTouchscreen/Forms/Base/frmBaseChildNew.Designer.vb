﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBaseChildNew
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.panTop = New DevExpress.XtraEditors.PanelControl()
        Me.scrChild = New NurseryTouchscreen.Scroller()
        Me.scrStaff = New NurseryTouchscreen.Scroller()
        Me.scrGroups = New NurseryTouchscreen.Scroller()
        Me.btnNotes = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMedication = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.btnAllergies = New DevExpress.XtraEditors.SimpleButton()
        Me.panBottom = New DevExpress.XtraEditors.PanelControl()
        Me.btnAccept = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.panTop, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panTop.SuspendLayout()
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panBottom.SuspendLayout()
        Me.SuspendLayout()
        '
        'panTop
        '
        Me.panTop.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panTop.Appearance.Options.UseFont = True
        Me.panTop.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.panTop.Controls.Add(Me.scrChild)
        Me.panTop.Controls.Add(Me.scrStaff)
        Me.panTop.Controls.Add(Me.scrGroups)
        Me.panTop.Controls.Add(Me.btnNotes)
        Me.panTop.Controls.Add(Me.btnMedication)
        Me.panTop.Controls.Add(Me.LabelControl3)
        Me.panTop.Controls.Add(Me.LabelControl2)
        Me.panTop.Controls.Add(Me.LabelControl1)
        Me.panTop.Controls.Add(Me.btnAllergies)
        Me.panTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.panTop.Location = New System.Drawing.Point(0, 0)
        Me.panTop.Name = "panTop"
        Me.panTop.Size = New System.Drawing.Size(1022, 98)
        Me.panTop.TabIndex = 0
        '
        'scrChild
        '
        Me.scrChild.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.scrChild.Location = New System.Drawing.Point(595, 9)
        Me.scrChild.Margin = New System.Windows.Forms.Padding(6)
        Me.scrChild.Name = "scrChild"
        Me.scrChild.ScrollerMode = NurseryTouchscreen.Scroller.EnumScrollerMode.Children
        Me.scrChild.SelectedIndex = 0
        Me.scrChild.Size = New System.Drawing.Size(417, 35)
        Me.scrChild.TabIndex = 3
        Me.scrChild.ValueID = Nothing
        Me.scrChild.ValueText = ""
        '
        'scrStaff
        '
        Me.scrStaff.Location = New System.Drawing.Point(88, 52)
        Me.scrStaff.Margin = New System.Windows.Forms.Padding(6)
        Me.scrStaff.Name = "scrStaff"
        Me.scrStaff.ScrollerMode = NurseryTouchscreen.Scroller.EnumScrollerMode.Staff
        Me.scrStaff.SelectedIndex = 0
        Me.scrStaff.Size = New System.Drawing.Size(417, 35)
        Me.scrStaff.TabIndex = 5
        Me.scrStaff.ValueID = Nothing
        Me.scrStaff.ValueText = ""
        '
        'scrGroups
        '
        Me.scrGroups.Location = New System.Drawing.Point(88, 9)
        Me.scrGroups.Margin = New System.Windows.Forms.Padding(6)
        Me.scrGroups.Name = "scrGroups"
        Me.scrGroups.ScrollerMode = NurseryTouchscreen.Scroller.EnumScrollerMode.Groups
        Me.scrGroups.SelectedIndex = 0
        Me.scrGroups.Size = New System.Drawing.Size(417, 35)
        Me.scrGroups.TabIndex = 1
        Me.scrGroups.ValueID = Nothing
        Me.scrGroups.ValueText = ""
        '
        'btnNotes
        '
        Me.btnNotes.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNotes.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNotes.Appearance.Options.UseFont = True
        Me.btnNotes.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnNotes.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnNotes.Location = New System.Drawing.Point(875, 53)
        Me.btnNotes.Name = "btnNotes"
        Me.btnNotes.Size = New System.Drawing.Size(134, 38)
        Me.btnNotes.TabIndex = 8
        Me.btnNotes.Text = "Notes"
        '
        'btnMedication
        '
        Me.btnMedication.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMedication.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMedication.Appearance.Options.UseFont = True
        Me.btnMedication.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnMedication.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnMedication.Location = New System.Drawing.Point(735, 53)
        Me.btnMedication.Name = "btnMedication"
        Me.btnMedication.Size = New System.Drawing.Size(134, 38)
        Me.btnMedication.TabIndex = 7
        Me.btnMedication.Text = "Medication"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.Location = New System.Drawing.Point(530, 18)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(44, 25)
        Me.LabelControl3.TabIndex = 2
        Me.LabelControl3.Text = "Child"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(12, 62)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(39, 25)
        Me.LabelControl2.TabIndex = 4
        Me.LabelControl2.Text = "Staff"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(12, 18)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(53, 25)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Group"
        '
        'btnAllergies
        '
        Me.btnAllergies.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAllergies.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAllergies.Appearance.Options.UseFont = True
        Me.btnAllergies.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnAllergies.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnAllergies.Location = New System.Drawing.Point(595, 53)
        Me.btnAllergies.Name = "btnAllergies"
        Me.btnAllergies.Size = New System.Drawing.Size(134, 38)
        Me.btnAllergies.TabIndex = 6
        Me.btnAllergies.Text = "Allergies"
        '
        'panBottom
        '
        Me.panBottom.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panBottom.Appearance.Options.UseFont = True
        Me.panBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.panBottom.Controls.Add(Me.btnAccept)
        Me.panBottom.Controls.Add(Me.btnCancel)
        Me.panBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panBottom.Location = New System.Drawing.Point(0, 482)
        Me.panBottom.Name = "panBottom"
        Me.panBottom.Size = New System.Drawing.Size(1022, 56)
        Me.panBottom.TabIndex = 1
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAccept.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAccept.Appearance.Options.UseFont = True
        Me.btnAccept.Image = Global.NurseryTouchscreen.My.Resources.Resources.success_32
        Me.btnAccept.Location = New System.Drawing.Point(704, 3)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(150, 45)
        Me.btnAccept.TabIndex = 0
        Me.btnAccept.Text = "Accept"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Image = Global.NurseryTouchscreen.My.Resources.Resources.cancel_32
        Me.btnCancel.Location = New System.Drawing.Point(860, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(150, 45)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "Cancel"
        '
        'frmBaseChildNew
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1022, 538)
        Me.ControlBox = False
        Me.Controls.Add(Me.panTop)
        Me.Controls.Add(Me.panBottom)
        Me.Font = New System.Drawing.Font("Segoe UI", 14.25!)
        Me.Margin = New System.Windows.Forms.Padding(6)
        Me.Name = "frmBaseChildNew"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmBaseChildNew"
        CType(Me.panTop, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panTop.ResumeLayout(False)
        Me.panTop.PerformLayout()
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panBottom.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents panTop As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnAccept As DevExpress.XtraEditors.SimpleButton
    Protected Friend WithEvents panBottom As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAllergies As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnNotes As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMedication As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents scrGroups As NurseryTouchscreen.Scroller
    Friend WithEvents scrChild As NurseryTouchscreen.Scroller
    Friend WithEvents scrStaff As NurseryTouchscreen.Scroller
End Class
