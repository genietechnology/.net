﻿Imports NurseryTouchscreen.SharedModule

Public Class frmBaseChildNew

    Public Event AcceptChanged(sender As Object, e As AcceptArgs)
    Public Event ChildChanged(sender As Object, e As RecordChangedArgs)
    Public Event StaffChanged(sender As Object, e As RecordChangedArgs)

    Private m_Nappies As Boolean
    Private m_Milk As Boolean
    Private m_BabyFood As Boolean
    Private m_OffMenu As Boolean
    Private m_FamilyID As String

    Private m_Allergies As String
    Private m_Medication As String
    Private m_Notes As String
    Private m_AllergyRating As String

    Private m_DisableHeader As Boolean = False

    Private m_DrillDownChild As String = ""
    Private m_GroupName As String = ""

    Private m_ButtonFont As New Font("Segoe UI", 14.25, FontStyle.Bold)

#Region "Properties"

    Public Property DisableHeader As Boolean
        Get
            Return m_DisableHeader
        End Get
        Set(value As Boolean)
            m_DisableHeader = value
            scrChild.Enabled = Not m_DisableHeader
            scrGroups.Enabled = Not m_DisableHeader
            scrStaff.Enabled = Not m_DisableHeader
        End Set
    End Property

    Public Property DisableChldren As Boolean
        Get
            Return Not scrChild.Enabled
        End Get
        Set(value As Boolean)
            scrGroups.Enabled = Not value
            scrChild.Enabled = Not value
        End Set
    End Property

    Public Property DisableStaff As Boolean
        Get
            Return Not scrStaff.Enabled
        End Get
        Set(value As Boolean)
            scrStaff.Enabled = Not value
        End Set
    End Property

    Public ReadOnly Property ChildrenPopulated As Boolean
        Get
            If scrChild.Count > 0 Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

    Public ReadOnly Property ChildID() As String
        Get
            Return scrChild.ValueID
        End Get
    End Property

    Public ReadOnly Property ChildName() As String
        Get
            Return scrChild.ValueText
        End Get
    End Property

    Public ReadOnly Property Nappies() As Boolean
        Get
            Return m_Nappies
        End Get
    End Property

    Public ReadOnly Property Milk() As Boolean
        Get
            Return m_Milk
        End Get
    End Property

    Public ReadOnly Property BabyFood() As Boolean
        Get
            Return m_BabyFood
        End Get
    End Property

    Public ReadOnly Property OffMenu() As Boolean
        Get
            Return m_OffMenu
        End Get
    End Property

    Public ReadOnly Property FamilyID As String
        Get
            Return m_FamilyID
        End Get
    End Property

#End Region

#Region "Overrideables"

    Protected Overridable Function BeforeCommitUpdate() As Boolean
        Return True
    End Function

    Protected Overridable Sub CommitUpdate()
        MsgBox("CommitUpdate must be overridden!")
    End Sub

#End Region

    Public Sub DrillDown(ByVal ChildID As String)

        m_DrillDownChild = ChildID
        SetChildProperties(ChildID)

        'now we have the group, we set the system to the same group
        scrGroups.SelectRecordByName(m_GroupName)
        scrChild.SelectRecord(m_DrillDownChild)

        Me.DisableChldren = True

    End Sub

    Private Sub frmBaseChildNew_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Me.DesignMode Then Exit Sub

        If Parameters.MaximiseForms Then
            Me.WindowState = FormWindowState.Maximized
        End If

        If Debugger.IsAttached Then
            Me.TopMost = False
        End If

        scrGroups.Populate()
        scrStaff.Populate()

    End Sub

#Region "Child"

    Private Sub SetAllergyButtonStatus(ByRef ButtonIn As DevExpress.XtraEditors.SimpleButton, ByVal Allergies As String, ByVal AllergyRating As String, ByVal OffMenu As Boolean)

        Select Case AllergyRating

            Case "Moderate"
                ButtonIn.Enabled = True
                ButtonIn.Appearance.BackColor = Color.Gold

            Case "Serious"
                ButtonIn.Enabled = True
                ButtonIn.Appearance.BackColor = Color.LightCoral

            Case Else
                If OffMenu Then
                    ButtonIn.Enabled = True
                    ButtonIn.Appearance.BackColor = Color.LightGreen
                Else
                    ButtonIn.Appearance.Reset()
                    ButtonIn.Appearance.Font = m_ButtonFont
                    ButtonIn.Enabled = False
                End If

        End Select

    End Sub

    Private Sub SetButtonStatus(ByRef ButtonIn As DevExpress.XtraEditors.SimpleButton, ByVal Contents As String)
        If Contents.Trim = "" Then
            ButtonIn.Appearance.Reset()
            ButtonIn.Appearance.Font = m_ButtonFont
            ButtonIn.Enabled = False
        Else
            ButtonIn.Enabled = True
            ButtonIn.Appearance.BackColor = Color.Yellow
        End If
    End Sub

    Private Sub SetChildProperties(ByVal ChildID As String)

        ResetProperties()

        If ChildID Is Nothing Then Exit Sub
        If ChildID = "" Then Exit Sub

        Dim _SQL As String = ""
        _SQL += "select family_id, group_id, nappies, milk, baby_food, off_menu,"
        _SQL += " med_allergies, med_medication, med_notes, allergy_rating"
        _SQL += " from children"
        _SQL += " where id = '" & ChildID & "'"

        Dim _dr As DataRow = DAL.ReturnDataRow(_SQL)
        If _dr IsNot Nothing Then

            m_FamilyID = _dr.Item("family_id").ToString
            m_GroupName = Business.Child.ReturnLocation(ChildID)
            m_Allergies = _dr.Item("med_allergies").ToString.Trim
            m_Medication = _dr.Item("med_medication").ToString.Trim
            m_Notes = _dr.Item("med_notes").ToString.Trim
            m_AllergyRating = _dr.Item("allergy_rating").ToString.Trim

            If Boolean.Parse(_dr.Item("nappies").ToString) Then
                m_Nappies = True
            Else
                m_Nappies = False
            End If

            If Boolean.Parse(_dr.Item("milk").ToString) Then
                m_Milk = True
            Else
                m_Milk = False
            End If

            If Boolean.Parse(_dr.Item("baby_food").ToString) Then
                m_BabyFood = True
            Else
                m_BabyFood = False
            End If

            If Boolean.Parse(_dr.Item("off_menu").ToString) Then
                m_OffMenu = True
            Else
                m_OffMenu = False
            End If

            _dr = Nothing

        End If

        _dr = Nothing

    End Sub

#End Region

#Region "Staff"

    Protected Function CheckStaff() As Boolean
        If scrStaff.ValueText = "" Then
            Msgbox("Please select a member of staff.", MessageBoxIcon.Exclamation, "Staff Check")
            Return False
        Else
            Return True
        End If
    End Function

#End Region

    Private Function SaveScreen() As Boolean

        If Not btnAccept.Enabled Then Return True

        If Not BeforeCommitUpdate() Then Return False

        CommitUpdate()

        Return True

    End Function

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnAccept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccept.Click

        If Not CheckStaff() Then Exit Sub
        If BeforeCommitUpdate() Then

            btnAccept.Enabled = False
            Application.DoEvents()

            CommitUpdate()

            btnAccept.Enabled = True
            Application.DoEvents()

            Me.Close()

        End If

    End Sub

    Private Sub btnAccept_EnabledChanged(sender As Object, e As EventArgs) Handles btnAccept.EnabledChanged
        RaiseEvent AcceptChanged(sender, New AcceptArgs(btnAccept.Enabled))
    End Sub

    Private Sub btnAllergies_Click(sender As Object, e As EventArgs) Handles btnAllergies.Click
        ZoomText(m_Allergies)
    End Sub

    Private Sub btnMedication_Click(sender As Object, e As EventArgs) Handles btnMedication.Click
        ZoomText(m_Medication)
    End Sub

    Private Sub btnNotes_Click(sender As Object, e As EventArgs) Handles btnNotes.Click
        ZoomText(m_Notes)
    End Sub

    Private Sub scrGroups_RecordChanged(sender As Object, e As RecordChangedArgs) Handles scrGroups.RecordChanged
        scrChild.PopulateChildren(e.ValueText)
    End Sub

    Private Sub scrChild_RecordChanged(sender As Object, e As RecordChangedArgs) Handles scrChild.RecordChanged

        SetChildProperties(scrChild.ValueID)
        SetAllergyButtonStatus(btnAllergies, m_Allergies, m_AllergyRating, m_OffMenu)
        SetButtonStatus(btnMedication, m_Medication)
        SetButtonStatus(btnNotes, m_Notes)

        RaiseEvent ChildChanged(sender, e)

    End Sub

    Private Sub ResetProperties()

        m_FamilyID = ""
        m_GroupName = ""
        m_Allergies = ""
        m_Medication = ""
        m_Notes = ""
        m_AllergyRating = ""

        m_Nappies = False
        m_Milk = False
        m_BabyFood = False
        m_OffMenu = False

    End Sub




















End Class
