﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBaseChild
    Inherits NurseryTouchscreen.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBaseChild))
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling2 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling3 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.btnMedication = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAllergies = New DevExpress.XtraEditors.SimpleButton()
        Me.btnNotes = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStaffSearch = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStaffNext = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStaffPrev = New DevExpress.XtraEditors.SimpleButton()
        Me.btnGroupSearch = New DevExpress.XtraEditors.SimpleButton()
        Me.btnGroupNext = New DevExpress.XtraEditors.SimpleButton()
        Me.btnGroupPrev = New DevExpress.XtraEditors.SimpleButton()
        Me.btnChildSearch = New DevExpress.XtraEditors.SimpleButton()
        Me.btnChildNext = New DevExpress.XtraEditors.SimpleButton()
        Me.btnChildPrev = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAccept = New DevExpress.XtraEditors.SimpleButton()
        Me.txtGroup = New DevExpress.XtraEditors.TextEdit()
        Me.txtStaff = New DevExpress.XtraEditors.TextEdit()
        Me.txtChild = New DevExpress.XtraEditors.TextEdit()
        Me.lblGroup = New DevExpress.XtraEditors.LabelControl()
        Me.lblChild = New DevExpress.XtraEditors.LabelControl()
        Me.lblStaff = New DevExpress.XtraEditors.LabelControl()
        CType(Me.txtGroup.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStaff.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtChild.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'btnMedication
        '
        Me.btnMedication.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnMedication.Location = New System.Drawing.Point(706, 51)
        Me.btnMedication.Name = "btnMedication"
        Me.btnMedication.Size = New System.Drawing.Size(150, 33)
        Me.btnMedication.TabIndex = 19
        Me.btnMedication.Text = "Medication"
        '
        'btnAllergies
        '
        Me.btnAllergies.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnAllergies.Location = New System.Drawing.Point(550, 51)
        Me.btnAllergies.Name = "btnAllergies"
        Me.btnAllergies.Size = New System.Drawing.Size(150, 33)
        Me.btnAllergies.TabIndex = 18
        Me.btnAllergies.Text = "Allergies"
        '
        'btnNotes
        '
        Me.btnNotes.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnNotes.Location = New System.Drawing.Point(862, 51)
        Me.btnNotes.Name = "btnNotes"
        Me.btnNotes.Size = New System.Drawing.Size(150, 33)
        Me.btnNotes.TabIndex = 17
        Me.btnNotes.Text = "Notes"
        '
        'btnStaffSearch
        '
        Me.btnStaffSearch.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStaffSearch.Image = CType(resources.GetObject("btnStaffSearch.Image"), System.Drawing.Image)
        Me.btnStaffSearch.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnStaffSearch.Location = New System.Drawing.Point(474, 51)
        Me.btnStaffSearch.Name = "btnStaffSearch"
        Me.btnStaffSearch.Size = New System.Drawing.Size(33, 33)
        Me.btnStaffSearch.TabIndex = 14
        '
        'btnStaffNext
        '
        Me.btnStaffNext.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStaffNext.Image = CType(resources.GetObject("btnStaffNext.Image"), System.Drawing.Image)
        Me.btnStaffNext.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnStaffNext.Location = New System.Drawing.Point(435, 51)
        Me.btnStaffNext.Name = "btnStaffNext"
        Me.btnStaffNext.Size = New System.Drawing.Size(33, 33)
        Me.btnStaffNext.TabIndex = 13
        '
        'btnStaffPrev
        '
        Me.btnStaffPrev.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnStaffPrev.Image = CType(resources.GetObject("btnStaffPrev.Image"), System.Drawing.Image)
        Me.btnStaffPrev.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnStaffPrev.Location = New System.Drawing.Point(90, 51)
        Me.btnStaffPrev.Name = "btnStaffPrev"
        Me.btnStaffPrev.Size = New System.Drawing.Size(33, 33)
        Me.btnStaffPrev.TabIndex = 11
        '
        'btnGroupSearch
        '
        Me.btnGroupSearch.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnGroupSearch.Image = CType(resources.GetObject("btnGroupSearch.Image"), System.Drawing.Image)
        Me.btnGroupSearch.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnGroupSearch.Location = New System.Drawing.Point(474, 12)
        Me.btnGroupSearch.Name = "btnGroupSearch"
        Me.btnGroupSearch.Size = New System.Drawing.Size(33, 33)
        Me.btnGroupSearch.TabIndex = 4
        '
        'btnGroupNext
        '
        Me.btnGroupNext.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnGroupNext.Image = CType(resources.GetObject("btnGroupNext.Image"), System.Drawing.Image)
        Me.btnGroupNext.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnGroupNext.Location = New System.Drawing.Point(435, 12)
        Me.btnGroupNext.Name = "btnGroupNext"
        Me.btnGroupNext.Size = New System.Drawing.Size(33, 33)
        Me.btnGroupNext.TabIndex = 3
        '
        'btnGroupPrev
        '
        Me.btnGroupPrev.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat
        Me.btnGroupPrev.Image = CType(resources.GetObject("btnGroupPrev.Image"), System.Drawing.Image)
        Me.btnGroupPrev.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnGroupPrev.Location = New System.Drawing.Point(90, 12)
        Me.btnGroupPrev.Name = "btnGroupPrev"
        Me.btnGroupPrev.Size = New System.Drawing.Size(33, 33)
        Me.btnGroupPrev.TabIndex = 1
        '
        'btnChildSearch
        '
        Me.btnChildSearch.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnChildSearch.Image = CType(resources.GetObject("btnChildSearch.Image"), System.Drawing.Image)
        Me.btnChildSearch.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnChildSearch.Location = New System.Drawing.Point(979, 12)
        Me.btnChildSearch.Name = "btnChildSearch"
        Me.btnChildSearch.Size = New System.Drawing.Size(33, 33)
        Me.btnChildSearch.TabIndex = 9
        '
        'btnChildNext
        '
        Me.btnChildNext.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnChildNext.Image = CType(resources.GetObject("btnChildNext.Image"), System.Drawing.Image)
        Me.btnChildNext.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnChildNext.Location = New System.Drawing.Point(940, 12)
        Me.btnChildNext.Name = "btnChildNext"
        Me.btnChildNext.Size = New System.Drawing.Size(33, 33)
        Me.btnChildNext.TabIndex = 8
        '
        'btnChildPrev
        '
        Me.btnChildPrev.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnChildPrev.Image = CType(resources.GetObject("btnChildPrev.Image"), System.Drawing.Image)
        Me.btnChildPrev.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnChildPrev.Location = New System.Drawing.Point(595, 12)
        Me.btnChildPrev.Name = "btnChildPrev"
        Me.btnChildPrev.Size = New System.Drawing.Size(33, 33)
        Me.btnChildPrev.TabIndex = 6
        '
        'btnCancel
        '
        Me.btnCancel.Image = CType(resources.GetObject("btnCancel.Image"), System.Drawing.Image)
        Me.btnCancel.Location = New System.Drawing.Point(862, 481)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(150, 45)
        Me.btnCancel.TabIndex = 16
        Me.btnCancel.Text = "Cancel"
        '
        'btnAccept
        '
        Me.btnAccept.Image = CType(resources.GetObject("btnAccept.Image"), System.Drawing.Image)
        Me.btnAccept.Location = New System.Drawing.Point(706, 481)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(150, 45)
        Me.btnAccept.TabIndex = 15
        Me.btnAccept.Text = "Accept"
        '
        'txtGroup
        '
        Me.txtGroup.Location = New System.Drawing.Point(129, 12)
        Me.txtGroup.Name = "txtGroup"
        Me.txtGroup.Properties.AutoHeight = False
        Me.txtGroup.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtGroup, True)
        Me.txtGroup.Size = New System.Drawing.Size(300, 33)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtGroup, OptionsSpelling1)
        Me.txtGroup.TabIndex = 78
        Me.txtGroup.TabStop = False
        '
        'txtStaff
        '
        Me.txtStaff.Location = New System.Drawing.Point(129, 51)
        Me.txtStaff.Name = "txtStaff"
        Me.txtStaff.Properties.AutoHeight = False
        Me.txtStaff.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtStaff, True)
        Me.txtStaff.Size = New System.Drawing.Size(300, 33)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtStaff, OptionsSpelling2)
        Me.txtStaff.TabIndex = 79
        Me.txtStaff.TabStop = False
        '
        'txtChild
        '
        Me.txtChild.Location = New System.Drawing.Point(634, 12)
        Me.txtChild.Name = "txtChild"
        Me.txtChild.Properties.AutoHeight = False
        Me.txtChild.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtChild, True)
        Me.txtChild.Size = New System.Drawing.Size(300, 33)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtChild, OptionsSpelling3)
        Me.txtChild.TabIndex = 80
        Me.txtChild.TabStop = False
        '
        'lblGroup
        '
        Me.lblGroup.Location = New System.Drawing.Point(10, 22)
        Me.lblGroup.Name = "lblGroup"
        Me.lblGroup.Size = New System.Drawing.Size(29, 13)
        Me.lblGroup.TabIndex = 81
        Me.lblGroup.Text = "Group"
        '
        'lblChild
        '
        Me.lblChild.Location = New System.Drawing.Point(523, 22)
        Me.lblChild.Name = "lblChild"
        Me.lblChild.Size = New System.Drawing.Size(23, 13)
        Me.lblChild.TabIndex = 83
        Me.lblChild.Text = "Child"
        '
        'lblStaff
        '
        Me.lblStaff.Location = New System.Drawing.Point(10, 61)
        Me.lblStaff.Name = "lblStaff"
        Me.lblStaff.Size = New System.Drawing.Size(25, 13)
        Me.lblStaff.TabIndex = 84
        Me.lblStaff.Text = "Staff"
        '
        'frmBaseChild
        '
        Me.ClientSize = New System.Drawing.Size(1024, 538)
        Me.Controls.Add(Me.lblStaff)
        Me.Controls.Add(Me.lblChild)
        Me.Controls.Add(Me.lblGroup)
        Me.Controls.Add(Me.txtChild)
        Me.Controls.Add(Me.txtStaff)
        Me.Controls.Add(Me.txtGroup)
        Me.Controls.Add(Me.btnMedication)
        Me.Controls.Add(Me.btnAllergies)
        Me.Controls.Add(Me.btnNotes)
        Me.Controls.Add(Me.btnStaffSearch)
        Me.Controls.Add(Me.btnStaffNext)
        Me.Controls.Add(Me.btnStaffPrev)
        Me.Controls.Add(Me.btnGroupSearch)
        Me.Controls.Add(Me.btnGroupNext)
        Me.Controls.Add(Me.btnGroupPrev)
        Me.Controls.Add(Me.btnChildSearch)
        Me.Controls.Add(Me.btnChildNext)
        Me.Controls.Add(Me.btnChildPrev)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnAccept)
        Me.Name = "frmBaseChild"
        Me.Controls.SetChildIndex(Me.btnAccept, 0)
        Me.Controls.SetChildIndex(Me.btnCancel, 0)
        Me.Controls.SetChildIndex(Me.btnChildPrev, 0)
        Me.Controls.SetChildIndex(Me.btnChildNext, 0)
        Me.Controls.SetChildIndex(Me.btnChildSearch, 0)
        Me.Controls.SetChildIndex(Me.btnGroupPrev, 0)
        Me.Controls.SetChildIndex(Me.btnGroupNext, 0)
        Me.Controls.SetChildIndex(Me.btnGroupSearch, 0)
        Me.Controls.SetChildIndex(Me.btnStaffPrev, 0)
        Me.Controls.SetChildIndex(Me.btnStaffNext, 0)
        Me.Controls.SetChildIndex(Me.btnStaffSearch, 0)
        Me.Controls.SetChildIndex(Me.btnNotes, 0)
        Me.Controls.SetChildIndex(Me.btnAllergies, 0)
        Me.Controls.SetChildIndex(Me.btnMedication, 0)
        Me.Controls.SetChildIndex(Me.txtGroup, 0)
        Me.Controls.SetChildIndex(Me.txtStaff, 0)
        Me.Controls.SetChildIndex(Me.txtChild, 0)
        Me.Controls.SetChildIndex(Me.lblGroup, 0)
        Me.Controls.SetChildIndex(Me.lblChild, 0)
        Me.Controls.SetChildIndex(Me.lblStaff, 0)
        CType(Me.txtGroup.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStaff.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtChild.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnAccept As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnChildPrev As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnChildNext As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnChildSearch As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnGroupSearch As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnGroupNext As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnGroupPrev As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStaffSearch As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStaffNext As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStaffPrev As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnNotes As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAllergies As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMedication As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtGroup As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtStaff As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtChild As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblGroup As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblChild As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblStaff As DevExpress.XtraEditors.LabelControl

End Class
