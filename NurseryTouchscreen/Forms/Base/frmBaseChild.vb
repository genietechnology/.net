﻿Imports NurseryTouchscreen.SharedModule

Public Class frmBaseChild

    Public Event AcceptChanged(sender As Object, e As AcceptArgs)
    Public Event ChildChanged(sender As Object, e As System.EventArgs)
    Public Event StaffChanged(sender As Object, e As System.EventArgs)

    Private m_DrillDownChild As String = ""

    Private m_ChildID As String
    Private m_ChildName As String
    Private m_GroupName As String

    Private m_Nappies As Boolean
    Private m_Milk As Boolean
    Private m_BabyFood As Boolean
    Private m_OffMenu As Boolean
    Private m_FamilyID As String

    Private m_Allergies As String
    Private m_Medication As String
    Private m_Notes As String
    Private m_AllergyRating As String

    Private m_StaffID As String
    Private m_StaffName As String

    Private m_Groups As New List(Of Pair)
    Private m_GroupIndex As Integer = 1

    Private m_Children As New List(Of Pair)
    Private m_ChildrenIndex As Integer = 1

    Private m_Staff As New List(Of Pair)
    Private m_StaffIndex As Integer = 1

    Private m_DisableHeader As Boolean = False
    Private m_DisableChildren As Boolean = False
    Private m_DisableStaff As Boolean = False

#Region "Properties"

    Public Property DisableHeader As Boolean
        Get
            Return m_DisableHeader
        End Get
        Set(value As Boolean)
            m_DisableHeader = value
            If m_DisableHeader Then
                DisableChldren = True
                DisableStaff = True
                SetChildButtons()
                SetGroupButtons()
                SetStaffButtons()
            End If
        End Set
    End Property

    Public Property DisableChldren As Boolean
        Get
            Return m_DisableChildren
        End Get
        Set(value As Boolean)
            m_DisableChildren = value
            SetChildButtons()
            SetGroupButtons()
        End Set
    End Property

    Public Property DisableStaff As Boolean
        Get
            Return m_DisableStaff
        End Get
        Set(value As Boolean)
            m_DisableStaff = value
            SetStaffButtons()
        End Set
    End Property

    Public ReadOnly Property ChildrenPopulated As Boolean
        Get
            If m_Children.Count > 0 Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

    Public ReadOnly Property ChildID() As String
        Get
            Return m_ChildID
        End Get
    End Property

    Public ReadOnly Property ChildName() As String
        Get
            Return m_ChildName
        End Get
    End Property

    Public ReadOnly Property Nappies() As Boolean
        Get
            Return m_Nappies
        End Get
    End Property

    Public ReadOnly Property Milk() As Boolean
        Get
            Return m_Milk
        End Get
    End Property

    Public ReadOnly Property BabyFood() As Boolean
        Get
            Return m_BabyFood
        End Get
    End Property

    Public ReadOnly Property OffMenu() As Boolean
        Get
            Return m_OffMenu
        End Get
    End Property

    Public ReadOnly Property FamilyID As String
        Get
            Return m_FamilyID
        End Get
    End Property

#End Region

#Region "Overrideables"

    Protected Overridable Function BeforeCommitUpdate() As Boolean
        Return True
    End Function

    Protected Overridable Sub DisplayRecord()
        MsgBox("DisplayRecord must be overridden!")
    End Sub

    Protected Overridable Sub CommitUpdate()
        MsgBox("CommitUpdate must be overridden!")
    End Sub

#End Region

    Private Sub frmBaseChild_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.DesignMode Then Exit Sub
        GetGroups()
        GetStaff()
    End Sub

    Public Sub DrillDown(ByVal ChildID As String)

        m_DrillDownChild = ChildID
        m_ChildID = ChildID
        SetChildProperties()

        'now we have the group, we set the system to the same group
        m_GroupIndex = 1
        For Each _p As Pair In m_Groups
            If _p.Text = m_GroupName Then
                SetGroup()
                Exit For
            End If
            m_GroupIndex += 1
        Next

        'now we set the child
        m_ChildrenIndex = 1
        For Each _p As Pair In m_Children
            If _p.Code = m_DrillDownChild Then
                SetChild()
                Exit For
            End If
            m_ChildrenIndex += 1
        Next

        Me.DisableChldren = True

    End Sub

#Region "Child"

    Private Sub ResetChild()

        m_ChildID = ""
        txtChild.Tag = ""

        m_ChildName = ""
        txtChild.Text = ""

        SetButtonStatus(btnAllergies, "")
        SetButtonStatus(btnMedication, "")
        SetButtonStatus(btnNotes, "")

        RaiseEvent ChildChanged(Nothing, Nothing)

    End Sub

    Private Sub SetChild()

        Dim _Child As Pair = m_Children(m_ChildrenIndex - 1)

        m_ChildID = _Child.Code
        txtChild.Tag = _Child.Code

        m_ChildName = _Child.Text
        txtChild.Text = _Child.Text

        SetChildProperties()
        SetChildButtons()

        RaiseEvent ChildChanged(Nothing, Nothing)
        DisplayRecord()

    End Sub

    Private Sub SetChildButtons()

        If DisableChldren Then
            btnChildPrev.Enabled = False
            btnChildNext.Enabled = False
            btnChildSearch.Enabled = False
        Else

            If m_ChildrenIndex = 1 Then
                btnChildPrev.Enabled = False
            Else
                btnChildPrev.Enabled = True
            End If

            If m_Children.Count > m_ChildrenIndex Then
                btnChildNext.Enabled = True
            Else
                btnChildNext.Enabled = False
            End If

        End If

        SetAllergyButtonStatus(btnAllergies, m_Allergies, m_AllergyRating, m_OffMenu)
        SetButtonStatus(btnMedication, m_Medication)
        SetButtonStatus(btnNotes, m_Notes)

    End Sub

    Private Sub SetAllergyButtonStatus(ByRef ButtonIn As DevExpress.XtraEditors.SimpleButton, ByVal Allergies As String, ByVal AllergyRating As String, ByVal OffMenu As Boolean)

        Select Case AllergyRating

            Case "Moderate"
                ButtonIn.Appearance.BackColor = Color.Gold
                ButtonIn.Enabled = True

            Case "Serious"
                ButtonIn.Appearance.BackColor = Color.LightCoral
                ButtonIn.Enabled = True

            Case Else
                If OffMenu Then
                    ButtonIn.Appearance.BackColor = Color.LightGreen
                    ButtonIn.Enabled = True
                Else
                    ButtonIn.ResetBackColor()
                    ButtonIn.Enabled = False
                End If

        End Select

    End Sub

    Private Sub SetButtonStatus(ByRef ButtonIn As DevExpress.XtraEditors.SimpleButton, ByVal Contents As String)
        If Contents.Trim = "" Then
            ButtonIn.ResetBackColor()
            ButtonIn.Enabled = False
        Else
            ButtonIn.Appearance.BackColor = Color.Yellow
            ButtonIn.Enabled = True
        End If
    End Sub

    Private Sub GetChildren()

        m_ChildrenIndex = 0
        m_Children.Clear()

        Dim _SQL As String = ""

        If Parameters.DisableLocation Then

            _SQL += "select person_id, person_name from RegisterSummary"
            _SQL += " left join Children c on c.id = person_id"
            _SQL += " where day_id = '" + TodayID + "'"
            _SQL += " and c.group_name = '" + txtGroup.Text + "'"
            _SQL += " and person_type = 'C'"
            _SQL += " and in_out = 'I'"
            _SQL += " order by person_name"

        Else

            'get all the children in the group, regardless of the day as we might have a child checked in
            'that doesn't normally attend on this day

            _SQL += "select person_id, person_name from RegisterSummary"
            _SQL += " where day_id = '" + TodayID + "'"
            _SQL += " and location = '" + txtGroup.Text + "'"
            _SQL += " and person_type = 'C'"
            _SQL += " and in_out = 'I'"
            _SQL += " order by person_name"

        End If

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If _DT IsNot Nothing Then
            For Each _DR As DataRow In _DT.Rows
                Dim _ID As Guid = New Guid(_DR.Item(0).ToString)
                m_Children.Add(New Pair(_DR.Item(0).ToString, _DR.Item(1).ToString))
            Next
        End If

        If m_Children.Count > 0 Then

            m_ChildrenIndex = 1
            SetChild()

            If m_Children.Count = 1 Then
                btnChildSearch.Enabled = False
            Else
                btnChildSearch.Enabled = True
            End If

            btnAccept.Enabled = True

        Else
            ResetChild()
            btnChildPrev.Enabled = False
            btnChildNext.Enabled = False
            btnChildSearch.Enabled = False
            btnAccept.Enabled = False
        End If

    End Sub

    Private Sub SetChildProperties()

        Dim _SQL As String = ""
        _SQL += "select family_id, group_id, nappies, milk, baby_food, off_menu,"
        _SQL += " med_allergies, med_medication, med_notes, allergy_rating"
        _SQL += " from children"
        _SQL += " where id = '" & m_ChildID & "'"

        Dim _dr As DataRow = DAL.ReturnDataRow(_SQL)
        If _dr IsNot Nothing Then

            'used for drilldown
            m_GroupName = Business.Child.ReturnLocation(m_ChildID)

            m_FamilyID = _dr.Item("family_id").ToString
            m_Allergies = _dr.Item("med_allergies").ToString.Trim
            m_Medication = _dr.Item("med_medication").ToString.Trim
            m_Notes = _dr.Item("med_notes").ToString.Trim
            m_AllergyRating = _dr.Item("allergy_rating").ToString.Trim

            If Boolean.Parse(_dr.Item("nappies").ToString) Then
                m_Nappies = True
            Else
                m_Nappies = False
            End If

            If Boolean.Parse(_dr.Item("milk").ToString) Then
                m_Milk = True
            Else
                m_Milk = False
            End If

            If Boolean.Parse(_dr.Item("baby_food").ToString) Then
                m_BabyFood = True
            Else
                m_BabyFood = False
            End If

            If Boolean.Parse(_dr.Item("off_menu").ToString) Then
                m_OffMenu = True
            Else
                m_OffMenu = False
            End If

            _dr = Nothing

        End If

        _dr = Nothing

    End Sub

#End Region

#Region "Groups"

    Private Sub GetGroups()

        'default to the first group in sequence
        m_GroupIndex = 1

        Dim _SQL As String = ""

        If Parameters.DisableLocation Then
            _SQL += "select id, room_name from SiteRooms"
            _SQL += " where site_id = " + Parameters.SiteIDQuotes + ""
            _SQL += " and room_check_children = 1 order by room_sequence"
        Else

            _SQL += "select distinct sr.id, sr.room_name, sr.room_sequence from RegisterSummary"
            _SQL += " left join SiteRooms sr on sr.room_name = RegisterSummary.location and sr.site_id = " + Parameters.SiteIDQuotes
            _SQL += " where day_id = '" + TodayID + "'"
            _SQL += " and RegisterSummary.location <> ''"
            _SQL += " and person_type = 'C'"
            _SQL += " and in_out = 'I'"
            _SQL += " order by sr.room_sequence"

        End If

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If _DT IsNot Nothing Then
            Dim _i As Integer = 1
            For Each _DR As DataRow In _DT.Rows
                Dim _ID As String = _DR.Item(0).ToString
                Dim _Name As String = _DR.Item(1).ToString
                m_Groups.Add(New Pair(_ID, _Name))
                If Parameters.DefaultRoom <> "" Then
                    If _Name = Parameters.DefaultRoom Then
                        m_GroupIndex = _i
                    End If
                End If
                _i += 1
            Next
        End If

        SetGroup()

    End Sub

    Private Sub SetGroup()

        Dim _Group As Pair = m_Groups(m_GroupIndex - 1)
        txtGroup.Tag = _Group.Code
        txtGroup.Text = _Group.Text
        GetChildren()
        SetGroupButtons()

    End Sub

    Private Sub SetGroupButtons()

        If DisableChldren Then
            btnGroupPrev.Enabled = False
            btnGroupNext.Enabled = False
            btnGroupSearch.Enabled = False
        Else
            If m_GroupIndex = 1 Then
                btnGroupPrev.Enabled = False
            Else
                btnGroupPrev.Enabled = True
            End If

            If m_Groups.Count > m_GroupIndex Then
                btnGroupNext.Enabled = True
            Else
                btnGroupNext.Enabled = False
            End If
        End If

    End Sub

#End Region

#Region "Staff"

    Private Sub GetStaff()

        Dim _SQL As String = "select id, fullname from Staff where status = 'C' order by fullname"

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If _DT IsNot Nothing Then
            For Each _DR As DataRow In _DT.Rows
                Dim _ID As Guid = New Guid(_DR.Item(0).ToString)
                If PersonInorOut(_ID) = Enums.InOut.CheckIn Then
                    m_Staff.Add(New Pair(_DR.Item(0).ToString, _DR.Item(1).ToString))
                End If
            Next
        End If

        If m_Staff.Count > 0 Then

            m_StaffIndex = 1
            SetStaff()

            If m_Staff.Count = 1 Then
                btnStaffSearch.Enabled = False
            Else
                btnStaffSearch.Enabled = True
            End If

        Else
            txtStaff.Tag = ""
            txtStaff.Text = ""
            btnStaffPrev.Enabled = False
            btnStaffNext.Enabled = False
            btnStaffSearch.Enabled = False
        End If

    End Sub

    Private Sub SetStaff()

        Dim _Staff As Pair = m_Staff(m_StaffIndex - 1)
        txtStaff.Tag = _Staff.Code
        txtStaff.Text = _Staff.Text
        SetStaffButtons()

        RaiseEvent StaffChanged(Nothing, Nothing)

    End Sub

    Private Sub SetStaffButtons()

        If DisableStaff Then
            btnStaffPrev.Enabled = False
            btnStaffNext.Enabled = False
            btnStaffSearch.Enabled = False
        Else

            If m_StaffIndex = 1 Then
                btnStaffPrev.Enabled = False
            Else
                btnStaffPrev.Enabled = True
            End If

            If m_Staff.Count > m_StaffIndex Then
                btnStaffNext.Enabled = True
            Else
                btnStaffNext.Enabled = False
            End If

        End If

    End Sub

    Protected Function CheckStaff() As Boolean
        If txtStaff.Text = "" Then
            Msgbox("Please select a member of staff.", MessageBoxIcon.Exclamation, "Staff Check")
            Return False
        Else
            Return True
        End If
    End Function

#End Region

#Region "Control Events"

    Private Sub btnGroupNext_Click(sender As Object, e As EventArgs) Handles btnGroupNext.Click
        If Not SaveScreen() Then Exit Sub
        m_GroupIndex += 1
        SetGroup()
    End Sub

    Private Sub btnGroupPrev_Click(sender As Object, e As EventArgs) Handles btnGroupPrev.Click
        If Not SaveScreen() Then Exit Sub
        m_GroupIndex -= 1
        SetGroup()
    End Sub

    Private Sub btnChildPrev_Click(sender As Object, e As EventArgs) Handles btnChildPrev.Click
        If Not SaveScreen() Then Exit Sub
        m_ChildrenIndex -= 1
        SetChild()
    End Sub

    Private Sub btnChildNext_Click(sender As Object, e As EventArgs) Handles btnChildNext.Click
        If Not SaveScreen() Then Exit Sub
        m_ChildrenIndex += 1
        SetChild()
    End Sub

    Private Sub btnChildSearch_Click(sender As Object, e As EventArgs) Handles btnChildSearch.Click

        Dim _Selected As Pair = ReturnButtonSQL(m_Children, "Select Child")
        If _Selected IsNot Nothing Then

            Dim _Index As Integer = 1
            For Each _P As Pair In m_Children
                If _P.Code = _Selected.Code Then
                    m_ChildrenIndex = _Index
                    SetChild()
                    Exit For
                End If
                _Index += 1
            Next

        End If

    End Sub

    Private Sub btnStaffPrev_Click(sender As Object, e As EventArgs) Handles btnStaffPrev.Click
        m_StaffIndex -= 1
        SetStaff()
    End Sub

    Private Sub btnStaffNext_Click(sender As Object, e As EventArgs) Handles btnStaffNext.Click
        m_StaffIndex += 1
        SetStaff()
    End Sub

    Private Function SaveScreen() As Boolean

        If Not btnAccept.Enabled Then Return True

        If Not BeforeCommitUpdate() Then Return False

        CommitUpdate()

        Return True

    End Function

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnAccept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccept.Click

        If Not CheckStaff() Then Exit Sub
        If BeforeCommitUpdate() Then

            btnAccept.Enabled = False
            Application.DoEvents()

            CommitUpdate()

            btnAccept.Enabled = True
            Application.DoEvents()

            Me.DialogResult = DialogResult.OK
            Me.Close()

        End If

    End Sub

    Private Sub btnAccept_EnabledChanged(sender As Object, e As EventArgs) Handles btnAccept.EnabledChanged
        RaiseEvent AcceptChanged(sender, New AcceptArgs(btnAccept.Enabled))
    End Sub

    Private Sub btnGroupSearch_Click(sender As Object, e As EventArgs) Handles btnGroupSearch.Click

        Dim _Selected As Pair = ReturnButtonSQL(m_Groups, "Select Group")
        If _Selected IsNot Nothing Then

            Dim _Index As Integer = 1
            For Each _P As Pair In m_Groups
                If _P.Code = _Selected.Code Then
                    m_GroupIndex = _Index
                    SetGroup()
                    Exit For
                End If
                _Index += 1
            Next

        End If

    End Sub

    Private Sub btnStaffSearch_Click(sender As Object, e As EventArgs) Handles btnStaffSearch.Click

        Dim _Selected As Pair = ReturnButtonSQL(m_Staff, "Select Staff")
        If _Selected IsNot Nothing Then

            Dim _Index As Integer = 1
            For Each _P As Pair In m_Staff
                If _P.Code = _Selected.Code Then
                    m_StaffIndex = _Index
                    SetStaff()
                    Exit For
                End If
                _Index += 1
            Next

        End If

    End Sub

    Private Sub btnStaff_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        btnStaffSearch.Enabled = False
        Dim _Return As Pair = Business.Staff.FindStaff(Enums.PersonMode.OnlyCheckedIn)

        If Not _Return Is Nothing AndAlso _Return.Code <> "" Then
            m_StaffID = _Return.Code
            m_StaffName = _Return.Text
            txtStaff.Tag = m_StaffID
            txtStaff.Text = m_StaffName
            RaiseEvent StaffChanged(sender, e)
        End If

        btnStaffSearch.Enabled = True

    End Sub

    Private Sub btnChild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        btnChildSearch.Enabled = False
        Dim _Return As Pair = Business.Child.FindChildByGroup(Enums.PersonMode.OnlyCheckedIn)

        If Not _Return Is Nothing AndAlso _Return.Code <> "" Then
            m_ChildID = _Return.Code
            SetChildProperties()
            m_ChildName = _Return.Text
            txtChild.Tag = m_ChildID
            txtChild.Text = m_ChildName
            RaiseEvent ChildChanged(sender, e)
        End If

        btnChildSearch.Enabled = True

    End Sub

#End Region

    Private Sub btnAllergies_Click(sender As Object, e As EventArgs) Handles btnAllergies.Click
        ZoomText(m_Allergies)
    End Sub

    Private Sub btnMedication_Click(sender As Object, e As EventArgs) Handles btnMedication.Click
        ZoomText(m_Medication)
    End Sub

    Private Sub btnNotes_Click(sender As Object, e As EventArgs) Handles btnNotes.Click
        ZoomText(m_Notes)
    End Sub

End Class
