﻿Public Class frmBase

    Private Sub frmBase_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If Debugger.IsAttached Then
            Me.TopMost = False
            Me.MaximizeBox = False
            Me.MinimizeBox = False
        End If

        If Parameters.MaximiseForms Then
            Me.WindowState = FormWindowState.Maximized
        Else
            If Parameters.TopLeft Then
                Me.Top = 0
                Me.Left = 0
            Else
                'Me.CenterToScreen()
            End If
        End If

        If Not Parameters.SpellChecking Then
            SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        End If

    End Sub
End Class