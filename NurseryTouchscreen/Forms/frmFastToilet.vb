﻿Option Strict On

Imports DevExpress.XtraBars.Navigation
Imports DevExpress.XtraEditors
Imports NurseryTouchscreen.SharedModule

Public Class frmFastToilet

    Private m_SelectedLocation As String = ""

    Private Sub frmToiletTrip_Load(sender As Object, e As EventArgs) Handles Me.Load
        PopulateGroups()
    End Sub

    Private Sub frmToiletTrip_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        tbRooms.WideTileWidth = tbRooms.Width - 40
    End Sub

    Private Sub PopulateChildren()

        Me.Cursor = Cursors.WaitCursor
        Application.DoEvents()

        sc.Controls.Clear()

        Dim _SQL As String = Business.Child.ReturnCheckedInSQL(m_SelectedLocation, Parameters.ShowPhotos)

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _ID As String = _DR.Item("ID").ToString
                Dim _Name As String = _DR.Item("name").ToString
                Dim _Nappies As Boolean = ReturnBoolean(_DR.Item("nappies"))
                Dim _AllergyRating As String = _DR.Item("allergy_rating").ToString
                Dim _Allergies As String = _DR.Item("med_allergies").ToString
                Dim _Medication As String = _DR.Item("med_medication").ToString
                Dim _MedicalNotes As String = _DR.Item("med_notes").ToString

                AddChild(_ID, _Name, _Nappies, _AllergyRating, _Allergies, _Medication, _MedicalNotes)

            Next

        End If

        Me.Cursor = Cursors.Default
        Application.DoEvents()

    End Sub

    Private Sub AddChild(ByVal ChildID As String, ByVal ChildName As String, ByVal Nappies As Boolean, ByVal AllergyRating As String, ByVal MedAllergies As String, ByVal MedMedication As String, ByVal MedNotes As String)
        Dim _Item As New ToiletControl(ChildID, ChildName, Nappies, AllergyRating, MedAllergies, MedMedication, MedNotes)
        _Item.Dock = DockStyle.Top
        _Item.BringToFront()
        sc.Controls.Add(_Item)
    End Sub

    Private Sub PopulateGroups()

        Dim _SQL As String = ""

        _SQL += "SELECT ID, room_name from SiteRooms"
        _SQL += " where site_id = '" + Parameters.SiteID + "'"
        _SQL += " and room_check_children = 1"
        _SQL += " order by room_sequence"

        PopulateTileBar(_SQL)

        If Parameters.DefaultRoom <> "" Then
            For Each _i As TileBarItem In tbRoomGroup.Items
                Dim _Text As String = _i.Elements(0).Text
                If _Text = Parameters.DefaultRoom Then
                    tbRooms.SelectedItem = _i
                    _i.PerformItemClick()
                    Exit Sub
                End If
            Next
        Else
            For Each _i As TileBarItem In tbRoomGroup.Items
                tbRooms.SelectedItem = _i
                _i.PerformItemClick()
                Exit Sub
            Next
        End If

    End Sub

    Private Sub PopulateTileBar(ByVal _SQL As String)

        tbRoomGroup.Items.Clear()

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _i As New TileBarItem
                With _i

                    .Name = "t_" + _DR.Item("ID").ToString

                    AddHandler _i.ItemClick, AddressOf GroupItemClick

                    .TextAlignment = TileItemContentAlignment.TopLeft
                    .AppearanceItem.Normal.ForeColor = Color.Black
                    .AppearanceItem.Normal.BackColor = txtLightYellow.BackColor
                    .AppearanceItem.Selected.BackColor = txtYellow.BackColor
                    .Text = _DR.Item("room_name").ToString

                End With

                tbRoomGroup.Items.Add(_i)

            Next

        End If

    End Sub

    Private Sub GroupItemClick(sender As Object, e As TileItemEventArgs)
        m_SelectedLocation = e.Item.Text
        PopulateChildren()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click

        If ValidateEntry() Then

            Dim _StaffID As String = ""
            Dim _StaffName As String = ""

            If Parameters.CurrentStaffID <> "" Then
                _StaffID = Parameters.CurrentStaffID
                _StaffName = Parameters.CurrentStaffName
            Else
                Dim _Staff As Pair = Business.Staff.FindStaff(Enums.PersonMode.OnlyCheckedIn, False, Parameters.DefaultRoom)
                If _Staff IsNot Nothing Then
                    _StaffID = _Staff.Code
                    _StaffName = _Staff.Text
                End If
            End If

            If _StaffID <> "" Then
                Save(_StaffID, _StaffName)
                Me.DialogResult = DialogResult.OK
                Me.Close()
            End If

        End If

    End Sub

    Private Function ValidateEntry() As Boolean

        For Each _T As ToiletControl In sc.Controls
            If _T.IsDeleted = False AndAlso _T.ToiletValue = "" Then
                Return False
            End If
        Next

        Return True

    End Function

    Private Sub Save(ByVal StaffID As String, ByVal StaffName As String)

        Dim _Now As Date = Now
        Dim _HHMM As String = Format(_Now, "HH:mm").ToString

        For Each _T As ToiletControl In sc.Controls

            If _T.IsDeleted = False Then

                Dim _CreamIndicator As String = ""
                Dim _CreamDesc As String = ""

                If _T.CreamApplied Then
                    _CreamDesc = ", Cream Applied"
                    _CreamIndicator = "CREAM"
                End If

                Dim _ToiletType As String = ReturnToiletType(_T.ToiletType)
                Dim _ToiletValue As String = ReturnToiletValue(_T.ToiletValue)

                Dim _Desc As String = _T.ToiletValue + " " + _T.ToiletType + _CreamDesc + " @ " + _HHMM + " by " + StaffName

                SharedModule.LogActivity(Parameters.TodayID, _T.ChildID, _T.ChildName, EnumActivityType.Toilet, _Desc, _ToiletValue, _ToiletType, _CreamIndicator, StaffID, StaffName, "TOILETTRIP")

            End If

        Next

    End Sub

    Private Function ReturnToiletType(ByVal TypeIn As String) As String
        If TypeIn = "Nappy" Then Return "NAP"
        If TypeIn = "Potty" Then Return "POTTY"
        If TypeIn = "Accident" Then Return "ACC"
        Return ""
    End Function

    Private Function ReturnToiletValue(ByVal ValueIn As String) As String
        If ValueIn = "Dry" Then Return "DRY"
        If ValueIn = "Wet" Then Return "WET"
        If ValueIn = "Soiled" Then Return "SOIL"
        If ValueIn = "Loose" Then Return "LOOSE"
        Return ""
    End Function

End Class