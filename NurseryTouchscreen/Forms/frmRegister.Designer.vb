﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRegister
    Inherits NurseryTouchscreen.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling2 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling3 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling4 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling5 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.gbxRoom = New DevExpress.XtraEditors.GroupControl()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.radRoom = New DevExpress.XtraEditors.CheckEdit()
        Me.radNursery = New DevExpress.XtraEditors.CheckEdit()
        Me.gbxPerson = New DevExpress.XtraEditors.GroupControl()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.radVisitors = New DevExpress.XtraEditors.CheckEdit()
        Me.radStaff = New DevExpress.XtraEditors.CheckEdit()
        Me.radChildren = New DevExpress.XtraEditors.CheckEdit()
        Me.btnMoveRoom = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCrossCheck = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCheckIn = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCheckOut = New DevExpress.XtraEditors.SimpleButton()
        Me.txtCyan = New DevExpress.XtraEditors.TextEdit()
        Me.txtGreen = New DevExpress.XtraEditors.TextEdit()
        Me.txtYellow = New DevExpress.XtraEditors.TextEdit()
        Me.txtOrange = New DevExpress.XtraEditors.TextEdit()
        Me.txtRed = New DevExpress.XtraEditors.TextEdit()
        Me.btnClose = New DevExpress.XtraEditors.SimpleButton()
        Me.gbxFilter = New DevExpress.XtraEditors.GroupControl()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.radAbsent = New DevExpress.XtraEditors.CheckEdit()
        Me.radDueIn = New DevExpress.XtraEditors.CheckEdit()
        Me.radAwaitingCrossCheck = New DevExpress.XtraEditors.CheckEdit()
        Me.radCrossChecked = New DevExpress.XtraEditors.CheckEdit()
        Me.radCheckedOut = New DevExpress.XtraEditors.CheckEdit()
        Me.radCheckedIn = New DevExpress.XtraEditors.CheckEdit()
        Me.tgRegister = New NurseryTouchscreen.TouchGrid()
        Me.btnMassMove = New DevExpress.XtraEditors.SimpleButton()
        Me.btnUndo = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.gbxRoom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxRoom.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        CType(Me.radRoom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radNursery.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxPerson, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxPerson.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        CType(Me.radVisitors.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radStaff.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radChildren.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCyan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOrange.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxFilter, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxFilter.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.radAbsent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radDueIn.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radAwaitingCrossCheck.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radCrossChecked.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radCheckedOut.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radCheckedIn.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'gbxRoom
        '
        Me.gbxRoom.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxRoom.Appearance.Options.UseFont = True
        Me.gbxRoom.Controls.Add(Me.TableLayoutPanel3)
        Me.gbxRoom.Location = New System.Drawing.Point(12, 12)
        Me.gbxRoom.Name = "gbxRoom"
        Me.gbxRoom.ShowCaption = False
        Me.gbxRoom.Size = New System.Drawing.Size(186, 140)
        Me.gbxRoom.TabIndex = 1
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 1
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.radRoom, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.radNursery, 0, 1)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(2, 2)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 2
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(182, 136)
        Me.TableLayoutPanel3.TabIndex = 0
        '
        'radRoom
        '
        Me.radRoom.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radRoom.EditValue = True
        Me.radRoom.Location = New System.Drawing.Point(3, 3)
        Me.radRoom.Name = "radRoom"
        Me.radRoom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radRoom.Properties.Appearance.Options.UseFont = True
        Me.radRoom.Properties.Appearance.Options.UseTextOptions = True
        Me.radRoom.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.radRoom.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.radRoom.Properties.AutoHeight = False
        Me.radRoom.Properties.Caption = "This Room"
        Me.radRoom.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radRoom.Properties.RadioGroupIndex = 0
        Me.radRoom.Size = New System.Drawing.Size(176, 62)
        Me.radRoom.TabIndex = 1
        '
        'radNursery
        '
        Me.radNursery.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radNursery.Location = New System.Drawing.Point(3, 71)
        Me.radNursery.Name = "radNursery"
        Me.radNursery.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radNursery.Properties.Appearance.Options.UseFont = True
        Me.radNursery.Properties.Appearance.Options.UseTextOptions = True
        Me.radNursery.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.radNursery.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.radNursery.Properties.AutoHeight = False
        Me.radNursery.Properties.Caption = "Everyone"
        Me.radNursery.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radNursery.Properties.RadioGroupIndex = 0
        Me.radNursery.Size = New System.Drawing.Size(176, 62)
        Me.radNursery.TabIndex = 0
        Me.radNursery.TabStop = False
        '
        'gbxPerson
        '
        Me.gbxPerson.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxPerson.Appearance.Options.UseFont = True
        Me.gbxPerson.Controls.Add(Me.TableLayoutPanel2)
        Me.gbxPerson.Location = New System.Drawing.Point(204, 12)
        Me.gbxPerson.Name = "gbxPerson"
        Me.gbxPerson.ShowCaption = False
        Me.gbxPerson.Size = New System.Drawing.Size(186, 140)
        Me.gbxPerson.TabIndex = 0
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.665!))
        Me.TableLayoutPanel2.Controls.Add(Me.radVisitors, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.radStaff, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.radChildren, 0, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(2, 2)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 3
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 34.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(182, 136)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'radVisitors
        '
        Me.radVisitors.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radVisitors.Location = New System.Drawing.Point(3, 91)
        Me.radVisitors.Name = "radVisitors"
        Me.radVisitors.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radVisitors.Properties.Appearance.Options.UseFont = True
        Me.radVisitors.Properties.Appearance.Options.UseTextOptions = True
        Me.radVisitors.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.radVisitors.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.radVisitors.Properties.AutoHeight = False
        Me.radVisitors.Properties.Caption = "Visitors"
        Me.radVisitors.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radVisitors.Properties.RadioGroupIndex = 1
        Me.radVisitors.Size = New System.Drawing.Size(176, 42)
        Me.radVisitors.TabIndex = 2
        Me.radVisitors.TabStop = False
        '
        'radStaff
        '
        Me.radStaff.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radStaff.Location = New System.Drawing.Point(3, 47)
        Me.radStaff.Name = "radStaff"
        Me.radStaff.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radStaff.Properties.Appearance.Options.UseFont = True
        Me.radStaff.Properties.Appearance.Options.UseTextOptions = True
        Me.radStaff.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.radStaff.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.radStaff.Properties.AutoHeight = False
        Me.radStaff.Properties.Caption = "Staff"
        Me.radStaff.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radStaff.Properties.RadioGroupIndex = 1
        Me.radStaff.Size = New System.Drawing.Size(176, 38)
        Me.radStaff.TabIndex = 1
        Me.radStaff.TabStop = False
        '
        'radChildren
        '
        Me.radChildren.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radChildren.EditValue = True
        Me.radChildren.Location = New System.Drawing.Point(3, 3)
        Me.radChildren.Name = "radChildren"
        Me.radChildren.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radChildren.Properties.Appearance.Options.UseFont = True
        Me.radChildren.Properties.Appearance.Options.UseTextOptions = True
        Me.radChildren.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.radChildren.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.radChildren.Properties.AutoHeight = False
        Me.radChildren.Properties.Caption = "Children"
        Me.radChildren.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radChildren.Properties.RadioGroupIndex = 1
        Me.radChildren.Size = New System.Drawing.Size(176, 38)
        Me.radChildren.TabIndex = 0
        '
        'btnMoveRoom
        '
        Me.btnMoveRoom.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnMoveRoom.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMoveRoom.Appearance.Options.UseFont = True
        Me.btnMoveRoom.Image = Global.NurseryTouchscreen.My.Resources.Resources.swap_32
        Me.btnMoveRoom.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.btnMoveRoom.Location = New System.Drawing.Point(630, 405)
        Me.btnMoveRoom.Name = "btnMoveRoom"
        Me.btnMoveRoom.Size = New System.Drawing.Size(200, 45)
        Me.btnMoveRoom.TabIndex = 10
        Me.btnMoveRoom.Text = "Change Room"
        '
        'btnCrossCheck
        '
        Me.btnCrossCheck.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCrossCheck.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCrossCheck.Appearance.Options.UseFont = True
        Me.btnCrossCheck.Location = New System.Drawing.Point(1042, 405)
        Me.btnCrossCheck.Name = "btnCrossCheck"
        Me.btnCrossCheck.Size = New System.Drawing.Size(200, 45)
        Me.btnCrossCheck.TabIndex = 11
        Me.btnCrossCheck.Text = "Cross-Check"
        '
        'btnCheckIn
        '
        Me.btnCheckIn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCheckIn.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCheckIn.Appearance.Options.UseFont = True
        Me.btnCheckIn.Image = Global.NurseryTouchscreen.My.Resources.Resources.success_32
        Me.btnCheckIn.Location = New System.Drawing.Point(12, 405)
        Me.btnCheckIn.Name = "btnCheckIn"
        Me.btnCheckIn.Size = New System.Drawing.Size(200, 45)
        Me.btnCheckIn.TabIndex = 8
        Me.btnCheckIn.Text = "Check In"
        '
        'btnCheckOut
        '
        Me.btnCheckOut.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCheckOut.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCheckOut.Appearance.Options.UseFont = True
        Me.btnCheckOut.Image = Global.NurseryTouchscreen.My.Resources.Resources.delete_32
        Me.btnCheckOut.Location = New System.Drawing.Point(424, 405)
        Me.btnCheckOut.Name = "btnCheckOut"
        Me.btnCheckOut.Size = New System.Drawing.Size(200, 45)
        Me.btnCheckOut.TabIndex = 9
        Me.btnCheckOut.Text = "Check Out"
        '
        'txtCyan
        '
        Me.txtCyan.Location = New System.Drawing.Point(128, 376)
        Me.txtCyan.Name = "txtCyan"
        Me.txtCyan.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtCyan.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtCyan, True)
        Me.txtCyan.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtCyan, OptionsSpelling1)
        Me.txtCyan.TabIndex = 7
        Me.txtCyan.Visible = False
        '
        'txtGreen
        '
        Me.txtGreen.Location = New System.Drawing.Point(99, 376)
        Me.txtGreen.Name = "txtGreen"
        Me.txtGreen.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtGreen.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtGreen, True)
        Me.txtGreen.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtGreen, OptionsSpelling2)
        Me.txtGreen.TabIndex = 6
        Me.txtGreen.Visible = False
        '
        'txtYellow
        '
        Me.txtYellow.Location = New System.Drawing.Point(70, 376)
        Me.txtYellow.Name = "txtYellow"
        Me.txtYellow.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtYellow.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtYellow, True)
        Me.txtYellow.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtYellow, OptionsSpelling3)
        Me.txtYellow.TabIndex = 5
        Me.txtYellow.Visible = False
        '
        'txtOrange
        '
        Me.txtOrange.Location = New System.Drawing.Point(41, 376)
        Me.txtOrange.Name = "txtOrange"
        Me.txtOrange.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtOrange.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtOrange, True)
        Me.txtOrange.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtOrange, OptionsSpelling4)
        Me.txtOrange.TabIndex = 4
        Me.txtOrange.Visible = False
        '
        'txtRed
        '
        Me.txtRed.Location = New System.Drawing.Point(12, 376)
        Me.txtRed.Name = "txtRed"
        Me.txtRed.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtRed.Properties.Appearance.Options.UseBackColor = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtRed, True)
        Me.txtRed.Size = New System.Drawing.Size(23, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtRed, OptionsSpelling5)
        Me.txtRed.TabIndex = 3
        Me.txtRed.Visible = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(1231, 405)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(200, 45)
        Me.btnClose.TabIndex = 12
        Me.btnClose.Text = "Close"
        '
        'gbxFilter
        '
        Me.gbxFilter.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxFilter.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxFilter.Appearance.Options.UseFont = True
        Me.gbxFilter.Controls.Add(Me.TableLayoutPanel1)
        Me.gbxFilter.Location = New System.Drawing.Point(396, 12)
        Me.gbxFilter.Name = "gbxFilter"
        Me.gbxFilter.ShowCaption = False
        Me.gbxFilter.Size = New System.Drawing.Size(1035, 140)
        Me.gbxFilter.TabIndex = 1
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 5
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.radAbsent, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.radDueIn, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.radAwaitingCrossCheck, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.radCrossChecked, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.radCheckedOut, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.radCheckedIn, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(2, 2)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1031, 136)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'radAbsent
        '
        Me.radAbsent.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radAbsent.Location = New System.Drawing.Point(209, 71)
        Me.radAbsent.Name = "radAbsent"
        Me.radAbsent.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAbsent.Properties.Appearance.Options.UseFont = True
        Me.radAbsent.Properties.Appearance.Options.UseTextOptions = True
        Me.radAbsent.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.radAbsent.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.radAbsent.Properties.AutoHeight = False
        Me.radAbsent.Properties.Caption = "Absent"
        Me.radAbsent.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radAbsent.Properties.RadioGroupIndex = 2
        Me.radAbsent.Size = New System.Drawing.Size(200, 62)
        Me.radAbsent.TabIndex = 5
        Me.radAbsent.TabStop = False
        '
        'radDueIn
        '
        Me.radDueIn.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radDueIn.Location = New System.Drawing.Point(209, 3)
        Me.radDueIn.Name = "radDueIn"
        Me.radDueIn.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radDueIn.Properties.Appearance.Options.UseFont = True
        Me.radDueIn.Properties.Appearance.Options.UseTextOptions = True
        Me.radDueIn.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.radDueIn.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.radDueIn.Properties.AutoHeight = False
        Me.radDueIn.Properties.Caption = "Due In"
        Me.radDueIn.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radDueIn.Properties.RadioGroupIndex = 2
        Me.radDueIn.Size = New System.Drawing.Size(200, 62)
        Me.radDueIn.TabIndex = 2
        Me.radDueIn.TabStop = False
        '
        'radAwaitingCrossCheck
        '
        Me.radAwaitingCrossCheck.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radAwaitingCrossCheck.Location = New System.Drawing.Point(415, 71)
        Me.radAwaitingCrossCheck.Name = "radAwaitingCrossCheck"
        Me.radAwaitingCrossCheck.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAwaitingCrossCheck.Properties.Appearance.Options.UseFont = True
        Me.radAwaitingCrossCheck.Properties.AutoHeight = False
        Me.radAwaitingCrossCheck.Properties.Caption = "Awaiting Cross Check"
        Me.radAwaitingCrossCheck.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radAwaitingCrossCheck.Properties.RadioGroupIndex = 2
        Me.radAwaitingCrossCheck.Size = New System.Drawing.Size(200, 62)
        Me.radAwaitingCrossCheck.TabIndex = 4
        Me.radAwaitingCrossCheck.TabStop = False
        '
        'radCrossChecked
        '
        Me.radCrossChecked.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radCrossChecked.Location = New System.Drawing.Point(415, 3)
        Me.radCrossChecked.Name = "radCrossChecked"
        Me.radCrossChecked.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radCrossChecked.Properties.Appearance.Options.UseFont = True
        Me.radCrossChecked.Properties.AutoHeight = False
        Me.radCrossChecked.Properties.Caption = "Cross Checked"
        Me.radCrossChecked.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radCrossChecked.Properties.RadioGroupIndex = 2
        Me.radCrossChecked.Size = New System.Drawing.Size(200, 62)
        Me.radCrossChecked.TabIndex = 3
        Me.radCrossChecked.TabStop = False
        '
        'radCheckedOut
        '
        Me.radCheckedOut.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radCheckedOut.Location = New System.Drawing.Point(3, 71)
        Me.radCheckedOut.Name = "radCheckedOut"
        Me.radCheckedOut.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radCheckedOut.Properties.Appearance.Options.UseFont = True
        Me.radCheckedOut.Properties.AutoHeight = False
        Me.radCheckedOut.Properties.Caption = "Checked Out"
        Me.radCheckedOut.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radCheckedOut.Properties.RadioGroupIndex = 2
        Me.radCheckedOut.Size = New System.Drawing.Size(200, 62)
        Me.radCheckedOut.TabIndex = 1
        Me.radCheckedOut.TabStop = False
        '
        'radCheckedIn
        '
        Me.radCheckedIn.Dock = System.Windows.Forms.DockStyle.Fill
        Me.radCheckedIn.EditValue = True
        Me.radCheckedIn.Location = New System.Drawing.Point(3, 3)
        Me.radCheckedIn.Name = "radCheckedIn"
        Me.radCheckedIn.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radCheckedIn.Properties.Appearance.Options.UseFont = True
        Me.radCheckedIn.Properties.AutoHeight = False
        Me.radCheckedIn.Properties.Caption = "Checked In"
        Me.radCheckedIn.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radCheckedIn.Properties.RadioGroupIndex = 2
        Me.radCheckedIn.Size = New System.Drawing.Size(200, 62)
        Me.radCheckedIn.TabIndex = 0
        '
        'tgRegister
        '
        Me.tgRegister.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tgRegister.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tgRegister.Appearance.Options.UseFont = True
        Me.tgRegister.HideFirstColumn = False
        Me.tgRegister.Location = New System.Drawing.Point(12, 161)
        Me.tgRegister.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.tgRegister.Name = "tgRegister"
        Me.tgRegister.Size = New System.Drawing.Size(1419, 235)
        Me.tgRegister.TabIndex = 2
        '
        'btnMassMove
        '
        Me.btnMassMove.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnMassMove.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMassMove.Appearance.Options.UseFont = True
        Me.btnMassMove.Image = Global.NurseryTouchscreen.My.Resources.Resources.swap_32
        Me.btnMassMove.Location = New System.Drawing.Point(836, 405)
        Me.btnMassMove.Name = "btnMassMove"
        Me.btnMassMove.Size = New System.Drawing.Size(200, 45)
        Me.btnMassMove.TabIndex = 78
        Me.btnMassMove.Text = "Mass Move"
        '
        'btnUndo
        '
        Me.btnUndo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnUndo.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUndo.Appearance.Options.UseFont = True
        Me.btnUndo.Image = Global.NurseryTouchscreen.My.Resources.Resources.delete_32
        Me.btnUndo.Location = New System.Drawing.Point(218, 405)
        Me.btnUndo.Name = "btnUndo"
        Me.btnUndo.Size = New System.Drawing.Size(200, 45)
        Me.btnUndo.TabIndex = 79
        Me.btnUndo.Text = "Undo"
        '
        'frmRegister
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
        Me.ClientSize = New System.Drawing.Size(1443, 462)
        Me.Controls.Add(Me.btnUndo)
        Me.Controls.Add(Me.btnMassMove)
        Me.Controls.Add(Me.gbxRoom)
        Me.Controls.Add(Me.gbxPerson)
        Me.Controls.Add(Me.btnMoveRoom)
        Me.Controls.Add(Me.btnCrossCheck)
        Me.Controls.Add(Me.btnCheckIn)
        Me.Controls.Add(Me.btnCheckOut)
        Me.Controls.Add(Me.txtCyan)
        Me.Controls.Add(Me.txtGreen)
        Me.Controls.Add(Me.txtYellow)
        Me.Controls.Add(Me.txtOrange)
        Me.Controls.Add(Me.txtRed)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.gbxFilter)
        Me.Controls.Add(Me.tgRegister)
        Me.Name = "frmRegister"
        Me.Text = "Register"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Controls.SetChildIndex(Me.tgRegister, 0)
        Me.Controls.SetChildIndex(Me.gbxFilter, 0)
        Me.Controls.SetChildIndex(Me.btnClose, 0)
        Me.Controls.SetChildIndex(Me.txtRed, 0)
        Me.Controls.SetChildIndex(Me.txtOrange, 0)
        Me.Controls.SetChildIndex(Me.txtYellow, 0)
        Me.Controls.SetChildIndex(Me.txtGreen, 0)
        Me.Controls.SetChildIndex(Me.txtCyan, 0)
        Me.Controls.SetChildIndex(Me.btnCheckOut, 0)
        Me.Controls.SetChildIndex(Me.btnCheckIn, 0)
        Me.Controls.SetChildIndex(Me.btnCrossCheck, 0)
        Me.Controls.SetChildIndex(Me.btnMoveRoom, 0)
        Me.Controls.SetChildIndex(Me.gbxPerson, 0)
        Me.Controls.SetChildIndex(Me.gbxRoom, 0)
        Me.Controls.SetChildIndex(Me.btnMassMove, 0)
        Me.Controls.SetChildIndex(Me.btnUndo, 0)
        CType(Me.gbxRoom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxRoom.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        CType(Me.radRoom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radNursery.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxPerson, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxPerson.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        CType(Me.radVisitors.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radStaff.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radChildren.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCyan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOrange.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxFilter, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxFilter.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.radAbsent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radDueIn.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radAwaitingCrossCheck.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radCrossChecked.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radCheckedOut.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radCheckedIn.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tgRegister As NurseryTouchscreen.TouchGrid
    Friend WithEvents gbxFilter As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents radAwaitingCrossCheck As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents radCrossChecked As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents radCheckedOut As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents radCheckedIn As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents btnCheckIn As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCheckOut As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCrossCheck As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMoveRoom As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents radDueIn As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents gbxPerson As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents radVisitors As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents radStaff As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents radChildren As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents gbxRoom As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents radRoom As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents radNursery As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents radAbsent As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txtCyan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtGreen As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtYellow As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtOrange As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRed As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btnMassMove As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnUndo As DevExpress.XtraEditors.SimpleButton
End Class
