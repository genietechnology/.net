﻿Imports NurseryTouchscreen.SharedModule

Public Class frmMedicalAuth

    Implements IAddRemoveChildForm

    Private mDosagesDue As New List(Of String)

    Public Sub DrillDown(ChildID As String, RecordID As String) Implements IAddRemoveChildForm.DrillDown

        Me.ChildID = ChildID
        Me.RecordID = RecordID

        Clear()

        If Not Me.IsNew Then DisplayRecord()

    End Sub

    Private Sub DisplayRecord()

        Clear()

        If Me.ChildID = "" Then Exit Sub

        Dim _SQL As String
        Dim _AuthID As Guid = Guid.NewGuid

        _SQL = "select * from MedicineAuth where ID = '" & Me.RecordID & "'"

        Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
        If Not _DR Is Nothing Then

            lblContact.Tag = _DR("contact_id").ToString
            lblContact.Text = _DR("contact_name").ToString
            btnSigContact.Tag = _DR("contact_sig").ToString

            scrStaff.ValueID = _DR("staff_id").ToString
            scrStaff.ValueText = _DR("staff_name").ToString
            btnSigStaff.Tag = _DR("staff_sig").ToString

            lblNature.Text = _DR("illness").ToString
            lblMedicine.Text = _DR("medicine").ToString
            lblDosage.Text = _DR("dosage").ToString

            If _DR("last_given") Is Nothing Then
                lblLastParent.Text = "Not Applicable"
            Else
                If IsDBNull(_DR("last_given")) Then
                    lblLastParent.Text = "Not Applicable"
                Else
                    lblLastParent.Text = Format(CDate(_DR("last_given")), "HH:mm")
                End If
            End If

            If _DR("frequency") Is Nothing Then
                lblFrequency.Text = "Not Applicable"
            Else
                If IsDBNull(_DR("frequency")) Then
                    lblFrequency.Text = "Not Applicable"
                Else
                    lblFrequency.Text = _DR("frequency").ToString
                End If
            End If

            If _DR("dosages_due") Is Nothing Then
                lblDosagesDue.Text = "As Required"
            Else
                If IsDBNull(_DR("dosages_due")) Then
                    lblDosagesDue.Text = "As Required"
                Else
                    lblDosagesDue.Text = _DR("dosages_due").ToString
                End If
            End If

            If _DR("frequency") Is Nothing OrElse IsDBNull(_DR("frequency")) AndAlso
                    _DR("dosages_due") IsNot Nothing Then

                mDosagesDue = Split(_DR("dosages_due"), ", ").ToList

            End If

            If _DR("pres_date") Is Nothing OrElse IsDBNull(_DR("pres_date")) Then
                lblDatePrescribed.Text = ""
            Else
                lblDatePrescribed.Text = Format(CDate(_DR("pres_date")), "dd/MM/yyyy")
            End If

            _DR = Nothing

        End If

    End Sub

    Protected Overrides Function BeforeCommitUpdate() As Boolean

        'check we have completed everything
        If lblContact.Text = "" Then
            Msgbox("Please select the contact.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If lblNature.Text = "" Then
            Msgbox("Please enter the nature of the illness.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If lblMedicine.Text = "" Then
            Msgbox("Please enter the name of the medicine to be given.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If lblDatePrescribed.Text = "" Then
            Msgbox("Please select the date in which the medicine was prescribed.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If lblDosage.Text = "" Then
            Msgbox("Please enter the dosage to be given.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If lblLastParent.Text = "" Then
            Msgbox("Please select the time the last dosage was given by the parent.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If lblFrequency.Text = "" Then
            Msgbox("Please select the frequency between dosages (in hours).", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        'check we have the signatures
        If Parameters.MedicationSignature Then
            If btnSigContact.Tag = "" Then
                Msgbox("Contact signature is mandatory.", MessageBoxIcon.Exclamation, "Mandatory Field")
                Return False
            End If
        End If

        If btnSigStaff.Tag = "" Then
            Msgbox("Staff signature is mandatory.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        Return True

    End Function

    Protected Overrides Sub CommitUpdate()

        If mDosagesDue.Count > 0 Then
            UpdateUsingManualDosages()
        Else
            UpdateUsingFrequency()
        End If

    End Sub

    Private Sub UpdateUsingFrequency()

        Dim _SQL As String
        Dim _AuthID As Guid = Guid.NewGuid

        If Me.IsNew Then
            _SQL = "select * from MedicineAuth where ID = '" & _AuthID.ToString & "'"
        Else
            _SQL = "select * from MedicineAuth where ID = '" & Me.RecordID & "'"
            _AuthID = New Guid(Me.RecordID)
        End If

        Dim _DA As IDataAdapter = DAL.ReturnDataAdapter(_SQL)
        If Not _DA Is Nothing Then

            Dim _DR As DataRow
            Dim _DS As New DataSet
            _DA.Fill(_DS)

            If _DS.Tables(0).Rows.Count = 0 Then
                _DR = _DS.Tables(0).NewRow
                _DR("id") = _AuthID
                _DR("day_id") = New Guid(TodayID)
                _DR("child_id") = New Guid(ChildID)
                _DR("child_name") = ChildName
            Else
                _DR = _DS.Tables(0).Rows(0)
            End If

            _DR("contact_id") = lblContact.Tag
            _DR("contact_name") = lblContact.Text
            _DR("contact_sig") = btnSigContact.Tag

            _DR("staff_id") = scrStaff.ValueID
            _DR("staff_name") = scrStaff.ValueText
            _DR("staff_sig") = btnSigStaff.Tag

            _DR("illness") = lblNature.Text
            _DR("medicine") = lblMedicine.Text
            _DR("dosage") = lblDosage.Text

            If lblLastParent.Text = "Not Applicable" Then
                _DR("last_given") = DBNull.Value
                _DR("frequency") = DBNull.Value
                _DR("dosages_due") = DBNull.Value
            Else
                _DR("last_given") = lblLastParent.Text
                _DR("frequency") = lblFrequency.Text
                _DR("dosages_due") = lblDosagesDue.Text
            End If

            If lblDatePrescribed.Text <> "" Then
                _DR("pres_date") = CDate(lblDatePrescribed.Text)
            Else
                _DR("pres_date") = DBNull.Value
            End If

            _DR("stamp") = Now

            If _DS.Tables(0).Rows.Count = 0 Then _DS.Tables(0).Rows.Add(_DR)
            DAL.UpdateDB(_DA, _DS)

            DeleteLogs(_AuthID.ToString)
            CalculateTimes(True, _AuthID)

        End If
    End Sub

    Private Sub UpdateUsingManualDosages()

        Dim sql As String
        Dim authID As Guid = Nothing

        Dim dataAdapter As IDataAdapter = Nothing
        Dim dataSet As DataSet = Nothing
        Dim dataRow As DataRow = Nothing

        authID = Guid.NewGuid

        If Me.IsNew Then
            sql = "select * from MedicineAuth where ID = '" & authID.ToString & "'"
        Else
            sql = "select * from MedicineAuth where ID = '" & Me.RecordID & "'"
            authID = New Guid(Me.RecordID)
        End If

        dataAdapter = DAL.ReturnDataAdapter(sql)
        If Not dataAdapter Is Nothing Then

            DeleteLogs(authID.ToString)

            dataSet = New DataSet
            dataAdapter.Fill(dataSet)

            If dataSet.Tables(0).Rows.Count = 0 Then
                dataRow = dataSet.Tables(0).NewRow
                dataRow("id") = authID
                dataRow("day_id") = New Guid(TodayID)
                dataRow("child_id") = New Guid(ChildID)
                dataRow("child_name") = ChildName
            Else
                dataRow = dataSet.Tables(0).Rows(0)
            End If

            dataRow("contact_id") = lblContact.Tag
            dataRow("contact_name") = lblContact.Text
            dataRow("contact_sig") = btnSigContact.Tag

            dataRow("staff_id") = scrStaff.ValueID
            dataRow("staff_name") = scrStaff.ValueText
            dataRow("staff_sig") = btnSigStaff.Tag

            dataRow("illness") = lblNature.Text
            dataRow("medicine") = lblMedicine.Text
            dataRow("dosage") = lblDosage.Text

            dataRow("last_given") = DBNull.Value
            dataRow("frequency") = DBNull.Value
            dataRow("dosages_due") = lblDosagesDue.Text

            dataRow("stamp") = Now

            For Each dose In mDosagesDue
                CreateMedicineLog(Today.Date & " " & dose.ToString, authID)
            Next

            If dataSet.Tables(0).Rows.Count = 0 Then dataSet.Tables(0).Rows.Add(dataRow)

        End If
        DAL.UpdateDB(dataAdapter, dataSet)

    End Sub

    Private Sub DeleteLogs(ByVal AuthID As String)
        Dim _SQL As String = "delete from MedicineLog where auth_id = '" + AuthID + "'"
        DAL.ExecuteCommand(_SQL)
    End Sub

    Private Sub Clear()
        lblContact.Text = ""
        lblNature.Text = ""
        lblMedicine.Text = ""
        lblDatePrescribed.Text = ""
        lblDosage.Text = ""
        lblLastParent.Text = ""
        lblFrequency.Text = ""
        btnSigStaff.Tag = ""
        btnSigContact.Tag = ""
        lblDosagesDue.Text = ""
    End Sub

    Private Sub btnContact_Click(sender As Object, e As EventArgs) Handles btnContact.Click

        Dim _SQL As String = "select id, fullname from Contacts" &
                             " where Contacts.family_id = '" + FamilyID + "'"

        Dim _P As Pair = SharedModule.ReturnButtonSQL(_SQL, "Select Contact")
        If _P IsNot Nothing Then
            lblContact.Tag = _P.Code
            lblContact.Text = _P.Text
        End If

    End Sub

    Private Sub btnNature_Click(sender As Object, e As EventArgs) Handles btnNature.Click
        lblNature.Text = OSK("Nature of Illness", False, False, lblNature.Text, "Nature of Illness", 100)
    End Sub

    Private Sub btnMedicine_Click(sender As Object, e As EventArgs) Handles btnMedicine.Click
        lblMedicine.Text = OSK("Medicine to be given", False, False, lblMedicine.Text, "Medicine", 100)
    End Sub

    Private Sub btnDosage_Click(sender As Object, e As EventArgs) Handles btnDosage.Click
        lblDosage.Text = OSK("Dosage", False, True, lblDosage.Text, "Dosage", 100)
    End Sub

    Private Sub btnLastParent_Click(sender As Object, e As EventArgs) Handles btnLastParent.Click
        lblLastParent.Text = TimeEntry(, True)
        CalculateTimes(False)
    End Sub

    Private Sub btnNextDue_Click(sender As Object, e As EventArgs)
        lblNextDue.Text = TimeEntry()
    End Sub

    Private Sub btnFrequency_Click(sender As Object, e As EventArgs) Handles btnFrequency.Click
        lblFrequency.Text = NumberEntry("Frequency")
        CalculateTimes(False)
    End Sub

    Private Sub btnSigContact_Click(sender As Object, e As EventArgs) Handles btnSigContact.Click
        LegacySignatureCapture(btnSigContact.Tag)
    End Sub

    Private Sub btnSigStaff_Click(sender As Object, e As EventArgs) Handles btnSigStaff.Click
        LegacySignatureCapture(btnSigStaff.Tag)
    End Sub

    Private Sub CalculateTimes(ByVal GenerateLogs As Boolean, Optional ByVal AuthID As Guid? = Nothing)

        If lblLastParent.Text = "" Then Exit Sub
        If lblLastParent.Text = "Not Applicable" Then Exit Sub
        If lblFrequency.Text = "" Then Exit Sub
        If lblFrequency.Text = "Not Applicable" Then Exit Sub

        Dim _Freq As Double = lblFrequency.Text
        Dim _Dosages As String = ""

        Dim _LastTime As Date = DateAndTime.Today + " 23:59:00"
        Dim _LastGiven As Date = DateAndTime.Today + " " + lblLastParent.Text

        Dim _Next As Date = _LastGiven
        _Next = DateAdd(DateInterval.Hour, _Freq, _Next)

        While _Next.Day = _LastTime.Day

            If GenerateLogs Then CreateMedicineLog(_Next, AuthID)

            If _Dosages = "" Then
                _Dosages += Format(_Next, "HH:mm").ToString
            Else
                _Dosages += ", " + Format(_Next, "HH:mm").ToString
            End If

            _Next = DateAdd(DateInterval.Hour, _Freq, _Next)

        End While

        lblDosagesDue.Text = _Dosages

    End Sub

    Private Sub CreateMedicineLog(ByVal NextDate As Date, ByVal AuthID As Guid)

        Dim _SQL As String

        _SQL = "select * from MedicineLog" &
               " where day_id = '" & TodayID & "'" &
               " and child_id = '" & ChildID & "'"

        Dim _DA As IDataAdapter = DAL.ReturnDataAdapter(_SQL)
        If Not _DA Is Nothing Then

            Dim _DR As DataRow
            Dim _DS As New DataSet
            _DA.Fill(_DS)

            _DR = _DS.Tables(0).NewRow

            _DR("id") = Guid.NewGuid
            _DR("day_id") = New Guid(TodayID)
            _DR("auth_id") = AuthID

            _DR("due") = NextDate

            _DR("child_id") = New Guid(ChildID)
            _DR("child_name") = ChildName

            _DR("illness") = lblNature.Text
            _DR("medicine") = lblMedicine.Text
            _DR("dosage") = lblDosage.Text

            _DR("button") = Me.ChildName + " @ " + Format(NextDate, "HH:mm").ToString

            _DS.Tables(0).Rows.Add(_DR)
            DAL.UpdateDB(_DA, _DS)

        End If

    End Sub

    Private Sub btnAsRequired_Click(sender As Object, e As EventArgs) Handles btnAsRequired.Click
        lblFrequency.Text = "Not Applicable"
        lblDosagesDue.Text = "As Required"
    End Sub

    Private Sub btnDosageDue_Click(sender As Object, e As EventArgs) Handles btnDosageDue.Click

        'we black the dosage label first, if there was anything in the list
        'this is to ensure that if they click the dosage button and clear the list, it clears down the label
        'but if they didn't previously have manual doses setup (i.e. they were using frequency) and the accidentally
        'click the button, it will not clear the frequency they had selected
        If mDosagesDue.Count > 0 Then
            lblDosagesDue.Text = ""
        End If

        mDosagesDue = MultipleTimeEntry(existingTimes:=mDosagesDue)
        For Each dose In mDosagesDue
            lblDosagesDue.Text &= IIf(lblDosagesDue.Text = "", dose, ", " & dose)
            lblFrequency.Text = "Not Applicable"
        Next

    End Sub

    Private Sub btnPrescribed_Click(sender As Object, e As EventArgs) Handles btnPrescribed.Click
        Dim _Date As String = SharedModule.DateEntry
        If _Date <> "" Then
            Dim _d As Date = CDate(_Date)
            If _d > Today Then
                Msgbox("The prescribed date cannot be later than today!", MessageBoxIcon.Exclamation, "Date Check")
                lblDatePrescribed.Text = ""
            Else
                lblDatePrescribed.Text = SharedModule.DateEntry
            End If
        Else
            lblDatePrescribed.Text = ""
        End If
    End Sub
End Class
