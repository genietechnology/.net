﻿Imports NurseryTouchscreen.SharedModule

Public Class frmRegister

    Private m_Loading As Boolean = True

    Private Sub frmRegister_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Parameters.DisableLocation Then
            radNursery.Checked = True
            radRoom.Enabled = False
        End If

        m_Loading = False

        RefreshGrid()

    End Sub

    Private Sub RefreshGrid()

        If m_Loading Then Exit Sub

        btnCheckIn.Enabled = False
        btnCheckOut.Enabled = False
        btnUndo.Enabled = False
        btnMoveRoom.Enabled = False
        btnCrossCheck.Enabled = False
        btnMassMove.Enabled = False

        btnCheckOut.Text = "Check Out"

        SetGroupBoxes(False)

        Cursor.Current = Cursors.WaitCursor
        Application.DoEvents()

        If radChildren.Checked Then
            If radDueIn.Checked Then
                RefreshDueIn()
            Else
                If radAbsent.Checked Then
                    RefreshAbsent()
                Else
                    RefreshChildren()
                End If
            End If
        End If

        If radStaff.Checked Then
            RefreshStaff()
        End If

        If radVisitors.Checked Then
            RefreshVisitors()
            If radCheckedIn.Checked Then btnCheckIn.Enabled = True
        End If

        If tgRegister.RecordCount > 0 Then

            If radCheckedIn.Checked Then

                btnUndo.Enabled = True

                If radChildren.Checked Then
                    If Parameters.CrossCheckMandatory Then
                        btnCrossCheck.Enabled = True
                    Else
                        btnCheckOut.Enabled = True
                    End If
                Else
                    btnCheckOut.Enabled = True
                End If

                If radVisitors.Checked Then
                    btnMoveRoom.Enabled = False
                Else
                    btnMoveRoom.Enabled = Not Parameters.DisableLocation
                End If

                If radRoom.Checked Then
                    If radChildren.Checked Then btnMassMove.Enabled = True
                    If radStaff.Checked Then btnMassMove.Enabled = True
                End If

            End If

            If radCheckedOut.Checked Then
                btnCheckIn.Enabled = True
                btnUndo.Enabled = True
            End If

            If radDueIn.Checked Then
                btnCheckIn.Enabled = True
                btnCheckOut.Enabled = True
                btnCheckOut.Text = "Absent"
            End If

            If radAbsent.Checked Then
                btnUndo.Enabled = True
            End If

            If radAwaitingCrossCheck.Checked Then
                btnCrossCheck.Enabled = True
                btnMoveRoom.Enabled = Not Parameters.DisableLocation
            End If

            If radCrossChecked.Checked Then
                btnCheckOut.Enabled = True
                btnMoveRoom.Enabled = Not Parameters.DisableLocation
            End If

        End If

        SetGroupBoxes(True)

        Cursor.Current = Cursors.Default
        Application.DoEvents()

    End Sub

    Private Sub SetGroupBoxes(ByVal Enabled As Boolean)
        gbxRoom.Enabled = Enabled
        gbxPerson.Enabled = Enabled
        gbxRoom.Enabled = Enabled
    End Sub

    Private Sub RefreshAbsent()

        Dim _SQL As String = ""

        _SQL += "select key_id as 'ID', key_name 'Name', description as 'Reason', value_1 as 'Reported By', staff_name as 'Logged By'"
        _SQL += " from Activity"
        _SQL += " where day_id = '" + TodayID + "'"
        _SQL += " and type = 'ABSENCE'"
        _SQL += " order by stamp desc"

        tgRegister.HideFirstColumn = True

        tgRegister.Clear()
        tgRegister.Populate(_SQL)

    End Sub

    Private Sub RefreshDueIn()

        Dim sqlFilter As String = ""
        Dim hideChildName As Boolean = Parameters.HideChildName

        If hideChildName Then
            sqlFilter = "select c.ID, sr.room_name as 'Room', c.knownas as 'Name',"
        Else
            sqlFilter = "select c.ID, sr.room_name as 'Room', c.fullname as 'Name',"
        End If

        sqlFilter &= String.Concat(" b.tariff_name as 'Session', b.booking_from as 'Booking From', b.booking_to as 'Booking To'",
                                   " from Bookings b",
                                   " left join Children c on c.ID = b.child_id",
                                   " left join SiteRooms sr on sr.ID = c.group_id",
                                   " where booking_date = '", Parameters.TodaySQLDate, "'",
                                   " and booking_status <> 'Holiday'",
                                   " and b.child_id not in (select act.key_id from Activity act where act.day_id = '", Parameters.TodayID, "' and act.key_id = b.child_id and act.type = 'ABSENCE')",
                                   " and b.child_id not in (select rs.person_id from RegisterSummary rs where rs.date = b.booking_date and rs.person_id = b.child_id)")

        sqlFilter &= " and c.site_id = " & Parameters.SiteIDQuotes

        If radRoom.Checked Then
            sqlFilter &= " and sr.room_name = '" & Parameters.DefaultRoom & "'"
        End If

        sqlFilter &= " order by sr.room_sequence, name"

        tgRegister.HideFirstColumn = True

        tgRegister.Clear()
        tgRegister.Populate(sqlFilter)

    End Sub

    Private Sub RefreshChildren()

        Dim sqlFilter As String = ""
        Dim hideChildName As Boolean = Parameters.HideChildName

        Dim crossCheckCount As String = "(select count(*) from Activity a where a.key_id = c.ID and a.type = 'CROSSCHECK' and a.day_id = r.day_id)"

        If hideChildName Then
            sqlFilter = "select c.ID, sr.room_name as 'Room', c.knownas as 'Name',"
        Else
            sqlFilter = "select c.ID, sr.room_name as 'Room', c.fullname as 'Name',"
        End If

        sqlFilter &= String.Concat(" c.nappies, c.milk, b.tariff_name as 'Session',",
                                   " location as 'Location', b.booking_from as 'Booking From', b.booking_to as 'Booking To', last_stamp,",
                                   " (select count(*) from FoodRegister f where f.day_id = r.day_ID and f.child_id = c.ID) as 'Food Items',",
                                   " (select top 1 a.stamp from Activity a where a.key_id = c.ID and a.type = 'TOILET' and a.day_id = r.day_id order by a.stamp desc) as 'Last Toilet',",
                                   " (select top 1 a.stamp from Activity a where a.key_id = c.ID and a.type = 'SLEEP' and a.day_id = r.day_id order by a.stamp desc) as 'Last Sleep',",
                                   " (select top 1 a.stamp from Activity a where a.key_id = c.ID and a.type = 'MILK' and a.day_id = r.day_id order by a.stamp desc) as 'Last Bottle',",
                                   " (select top 1 a.stamp from Activity a where a.key_id = c.ID and a.type = 'CROSSCHECK' and a.day_id = r.day_id order by a.stamp desc) as 'Cross-Checked'",
                                   " from RegisterSummary r",
                                   " left join Children c on c.ID = person_id",
                                   " left join SiteRooms sr on sr.ID = c.group_id",
                                   " left join Bookings b on b.booking_date = r.date and b.child_id = c.ID",
                                   " where day_id = '" + Parameters.TodayID + "'",
                                   " and person_type = 'C'",
                                   " and c.site_id = ", Parameters.SiteIDQuotes)

        If radCheckedIn.Checked Then
            sqlFilter &= " and r.in_out = 'I'"
        End If

        If radCheckedOut.Checked Then
            sqlFilter &= " and r.in_out = 'O'"
        End If

        If radAwaitingCrossCheck.Checked Then
            sqlFilter &= " and r.in_out = 'I'"
            sqlFilter &= " and " & crossCheckCount & " <= 0"
        End If

        If radCrossChecked.Checked Then
            sqlFilter &= " and r.in_out = 'I'"
            sqlFilter &= " and " & crossCheckCount & " > 0"
        End If

        If radRoom.Checked Then
            sqlFilter &= " and r.location = '" & Parameters.DefaultRoom + "'"
        End If

        sqlFilter &= " order by sr.room_sequence, name"

        tgRegister.HideFirstColumn = True

        tgRegister.Clear()
        tgRegister.Populate(sqlFilter)

        tgRegister.Columns("nappies").Visible = False
        tgRegister.Columns("milk").Visible = False

        tgRegister.Columns("last_stamp").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        tgRegister.Columns("last_stamp").DisplayFormat.FormatString = "HH:mm:ss"

        If radCheckedIn.Checked Then
            tgRegister.Columns("last_stamp").Caption = "Checked-In"
        Else
            tgRegister.Columns("last_stamp").Caption = "Checked-Out"
        End If

        tgRegister.Columns("Last Toilet").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        tgRegister.Columns("Last Toilet").DisplayFormat.FormatString = "HH:mm:ss"

        tgRegister.Columns("Last Sleep").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        tgRegister.Columns("Last Sleep").DisplayFormat.FormatString = "HH:mm:ss"

        tgRegister.Columns("Last Bottle").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        tgRegister.Columns("Last Bottle").DisplayFormat.FormatString = "HH:mm:ss"

        tgRegister.Columns("Cross-Checked").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        tgRegister.Columns("Cross-Checked").DisplayFormat.FormatString = "HH:mm:ss"

    End Sub

    Private Sub RefreshStaff()

        Dim _SQL As String = ""

        _SQL += "select person_id as 'ID', person_name as 'Name', s.job_title as 'Job Title',"
        _SQL += " location as 'Location', last_stamp"
        _SQL += " from RegisterSummary"
        _SQL += " left join Staff s on s.ID = person_id"
        _SQL += " where day_id = '" + Parameters.TodayID + "'"
        _SQL += " and person_type = 'S'"

        _SQL += " and s.site_id = " + Parameters.SiteIDQuotes

        If radCheckedIn.Checked Then
            _SQL += " and in_out = 'I'"
        End If

        If radCheckedOut.Checked Then
            _SQL += " and in_out = 'O'"
        End If

        If radRoom.Checked Then
            _SQL += " and location = '" + Parameters.DefaultRoom + "'"
        End If

        _SQL += " order by person_name"

        tgRegister.HideFirstColumn = True

        tgRegister.Clear()
        tgRegister.Populate(_SQL)

        tgRegister.Columns("last_stamp").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        tgRegister.Columns("last_stamp").DisplayFormat.FormatString = "HH:mm:ss"

        If radCheckedIn.Checked Then
            tgRegister.Columns("last_stamp").Caption = "Checked-In"
        Else
            tgRegister.Columns("last_stamp").Caption = "Checked-Out"
        End If

    End Sub

    Private Sub RefreshVisitors()

        Dim _Today As String = "'" + Parameters.TodaySQLDate + "'"
        Dim _SQL As String = ""

        Dim _CheckInCount As String = "(select count(*) from Register r where r.date = " + _Today + " and r.person_id = v.person_id and r.in_out = 'I')"
        Dim _CheckOutCount As String = "(select count(*) from Register r where r.date = " + _Today + " and r.person_id = v.person_id and r.in_out = 'O')"

        _SQL += "select v.person_id as 'ID', v.name as 'Name',"

        If radCheckedIn.Checked Then
            _SQL += " v.ref_1 as 'Company', v.location as 'Visiting', v.ref_2 as 'Car Reg',"
            _SQL += " stamp_in as 'Checked-In'"
        Else
            _SQL += " stamp_out as 'Checked-Out'"
        End If

        _SQL += " from Register v"
        _SQL += " where v.date = " + _Today
        _SQL += " and v.type = 'V'"

        If radCheckedIn.Checked Then
            _SQL += " and v.in_out = 'I'"
            _SQL += " and " + _CheckInCount + " > " + _CheckOutCount
            _SQL += " order by v.stamp_in"
        End If

        If radCheckedOut.Checked Then
            _SQL += " and v.in_out = 'O'"
            _SQL += " and " + _CheckInCount + " > 0"
            _SQL += " and " + _CheckInCount + " <= " + _CheckOutCount
            _SQL += " order by v.stamp_out desc"
        End If

        tgRegister.Clear()
        tgRegister.Populate(_SQL)

        If radCheckedIn.Checked Then
            tgRegister.Columns("Checked-In").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            tgRegister.Columns("Checked-In").DisplayFormat.FormatString = "HH:mm:ss"
        Else
            tgRegister.Columns("Checked-Out").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            tgRegister.Columns("Checked-Out").DisplayFormat.FormatString = "HH:mm:ss"
        End If

    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub tgRegister_GridDoubleClick(sender As Object, e As EventArgs) Handles tgRegister.GridDoubleClick

        If Not radChildren.Checked Then Exit Sub
        If tgRegister.CurrentRow Is Nothing Then Exit Sub

        Dim _ID As String = tgRegister.CurrentRow("ID").ToString
        Dim _Name As String = tgRegister.CurrentRow("Name").ToString

        Business.Child.DrillDown(_ID, _Name)

    End Sub

    Private Sub btnCrossCheck_Click(sender As Object, e As EventArgs) Handles btnCrossCheck.Click

        If tgRegister.CurrentRow Is Nothing Then Exit Sub
        Dim _ID As String = tgRegister.CurrentRow("ID").ToString
        Dim _Name As String = tgRegister.CurrentRow("Name").ToString

        Dim _frm As New frmCrossCheck(_ID, _Name, frmCrossCheck.EnumMode.CrossCheck)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            RefreshGrid()
        End If

        _frm.Dispose()
        _frm = Nothing

    End Sub

    Private Sub btnCheckIn_Click(sender As Object, e As EventArgs) Handles btnCheckIn.Click

        If Not SharedModule.CheckinsPermitted Then Exit Sub

        If radVisitors.Checked Then

            Dim _frm As New frmVisitor()
            _frm.ShowDialog()

            If _frm.DialogResult = DialogResult.OK Then
                RefreshGrid()
            End If

            _frm.Dispose()
            _frm = Nothing

        Else

            If tgRegister.CurrentRow Is Nothing Then Exit Sub

            Dim _ID As String = tgRegister.CurrentRow("ID").ToString
            Dim _Name As String = tgRegister.CurrentRow("Name").ToString

            Dim _Type As Enums.PersonType = Enums.PersonType.Child
            If radStaff.Checked Then _Type = Enums.PersonType.Staff

            If _Type = Enums.PersonType.Child Then

                Dim signature As New Signature
                Dim signatureRequired As Boolean = Parameters.ContactSignatureIn

                If signatureRequired Then

                    Msgbox("Please tap OK so we can capture your signature...", MessageBoxIcon.Exclamation, "Signature Required")
                    signature.CaptureParentSignature(True, Business.Child.ReturnFamilyID(_ID))

                    If signature.SignatureCaptured Then
                        SharedModule.RegisterTransaction(New Guid(_ID), _Type, _Name, Enums.InOut.CheckIn, Enums.RegisterVia.Register)
                    Else
                        If Msgbox("Parent Signature declined. Do you want to specify another person collecting?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, "Signature Declined") = DialogResult.Yes Then
                            Dim name As String = SharedModule.OSK("Please enter the name of the person collecting")
                            If name <> "" Then
                                signature.CaptureVisitorSignature(True, Nothing, name)
                                If signature.SignatureCaptured Then
                                    SharedModule.RegisterTransaction(New Guid(_ID), _Type, _Name, Enums.InOut.CheckIn, Enums.RegisterVia.Register)
                                End If
                            End If
                        End If
                    End If
                Else
                    SharedModule.RegisterTransaction(New Guid(_ID), _Type, _Name, Enums.InOut.CheckIn, Enums.RegisterVia.Register)
                End If
            Else
                SharedModule.RegisterTransaction(New Guid(_ID), _Type, _Name, Enums.InOut.CheckIn, Enums.RegisterVia.Register)
            End If

        End If

        RefreshGrid()

    End Sub

    Private Sub btnCheckOut_Click(sender As Object, e As EventArgs) Handles btnCheckOut.Click

        If Not SharedModule.CheckinsPermitted Then Exit Sub

        If tgRegister.CurrentRow Is Nothing Then Exit Sub
        Dim _ID As String = tgRegister.CurrentRow("ID").ToString
        Dim _Name As String = tgRegister.CurrentRow("Name").ToString

        If radChildren.Checked Then

            If radDueIn.Checked Then

                Dim _frm As New frmAbsent(_ID, _Name)
                _frm.ShowDialog()

                If _frm.DialogResult = DialogResult.OK Then
                    RefreshGrid()
                End If

                _frm.Dispose()
                _frm = Nothing

            Else
                If SharedModule.DoCheckOut(_ID, _Name) Then
                    RefreshGrid()
                End If
            End If

        End If

        If radStaff.Checked Then
            If Msgbox("Do you want to check " + _Name + " out?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Check Out") = MsgBoxResult.Yes Then
                SharedModule.RegisterTransaction(New Guid(_ID), Enums.PersonType.Staff, _Name, Enums.InOut.CheckOut, Enums.RegisterVia.Register)
            End If
        End If

        If radVisitors.Checked Then
            If Msgbox("Do you want to check " + _Name + " out?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Check Out") = MsgBoxResult.Yes Then
                SharedModule.RegisterTransaction(New Guid(_ID), Enums.PersonType.Visitor, _Name, Enums.InOut.CheckOut, Enums.RegisterVia.Register)
            End If
        End If

        RefreshGrid()

    End Sub

    Private Sub btnMoveRoom_Click(sender As Object, e As EventArgs) Handles btnMoveRoom.Click

        If tgRegister.CurrentRow Is Nothing Then Exit Sub
        Dim _ID As Guid = New Guid(tgRegister.CurrentRow("ID").ToString)
        Dim _Name As String = tgRegister.CurrentRow("Name").ToString
        Dim _Location As String = tgRegister.CurrentRow("Location").ToString

        Dim _Type As Enums.PersonType = Enums.PersonType.Child
        If radStaff.Checked Then _Type = Enums.PersonType.Staff
        If radVisitors.Checked Then _Type = Enums.PersonType.Visitor

        If Msgbox("Do you want to move " + _Name + " into another room?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Move Room") = MsgBoxResult.Yes Then
            If SharedModule.MoveRoom(_Type, _ID, _Name, _Location) Then
                RefreshGrid()
            End If
        End If

    End Sub

    Private Sub SetOptions()

        radDueIn.Enabled = radChildren.Checked

        If radChildren.Checked Then
            radCrossChecked.Enabled = Parameters.CrossCheckMandatory
            radAwaitingCrossCheck.Enabled = Parameters.CrossCheckMandatory
            radAbsent.Enabled = True
        Else
            radCrossChecked.Enabled = False
            radAwaitingCrossCheck.Enabled = False
            radAbsent.Enabled = False
        End If

        If radCheckedIn.Checked Then
            RefreshGrid()
        Else
            radCheckedIn.Checked = True
        End If

    End Sub

    Private Sub MassMove()

        Dim _frm As New frmButtonsTiled
        _frm.PopulateMode = frmButtonsTiled.EnumPopulateMode.ChildrenCheckedIn
        _frm.MultiSelect = True
        _frm.RoomFilter = Parameters.DefaultRoom
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then

            Dim _NewRoom As Pair = SharedModule.ReturnRoom(Parameters.SiteID, Enums.PersonType.Child, Parameters.DefaultRoom)

            Dim _Mess As String = ""
            Dim _Movers As List(Of Pair) = _frm.SelectedPairs

            If _Movers.Count = 1 Then
                _Mess = "Are you sure you want to move " + _Movers.First.Text + " to " + _NewRoom.Text + "?"
            Else
                _Mess = "Are you sure you want to move the " + _Movers.Count.ToString + " people you have selected to " + _NewRoom.Text + "?"
            End If

            If Msgbox(_Mess, MessageBoxButtons.YesNo, MessageBoxIcon.Warning, "Confirm Mass Move") = DialogResult.Yes Then

                For Each _C In _Movers

                    Dim _ID As Guid = New Guid(_C.Code)

                    Dim _PersonType As Enums.PersonType
                    If radChildren.Checked Then _PersonType = Enums.PersonType.Child
                    If radStaff.Checked Then _PersonType = Enums.PersonType.Staff

                    SharedModule.RegisterTransaction(_ID, _PersonType, _C.Text, Enums.InOut.CheckOut, Enums.RegisterVia.MoveLogic, "", "MO", "Move Out >", "")
                    SharedModule.RegisterTransaction(_ID, _PersonType, _C.Text, Enums.InOut.CheckIn, Enums.RegisterVia.MoveLogic, _NewRoom.Text, "MI", "< Move In", "")

                Next

            End If

            RefreshGrid()

        End If

        _frm.Dispose()
        _frm = Nothing

    End Sub

#Region "Radio Buttons"

    Private Sub radCheckedIn_CheckedChanged(sender As Object, e As EventArgs) Handles radCheckedIn.CheckedChanged
        If radCheckedIn.Checked Then RefreshGrid()
    End Sub

    Private Sub radCheckedOut_CheckedChanged(sender As Object, e As EventArgs) Handles radCheckedOut.CheckedChanged
        If radCheckedOut.Checked Then RefreshGrid()
    End Sub

    Private Sub radDueIn_CheckedChanged(sender As Object, e As EventArgs) Handles radDueIn.CheckedChanged
        If radDueIn.Checked Then RefreshGrid()
    End Sub

    Private Sub radCrossChecked_CheckedChanged(sender As Object, e As EventArgs) Handles radCrossChecked.CheckedChanged
        If radCrossChecked.Checked Then RefreshGrid()
    End Sub

    Private Sub chkRoom_CheckedChanged(sender As Object, e As EventArgs) Handles radRoom.CheckedChanged
        If radRoom.Checked Then RefreshGrid()
    End Sub

    Private Sub chkNursery_CheckedChanged(sender As Object, e As EventArgs) Handles radNursery.CheckedChanged
        If radNursery.Checked Then RefreshGrid()
    End Sub

    Private Sub radAwaitingCrossCheck_CheckedChanged(sender As Object, e As EventArgs) Handles radAwaitingCrossCheck.CheckedChanged
        If radAwaitingCrossCheck.Checked Then RefreshGrid()
    End Sub

    Private Sub radAbsent_CheckedChanged(sender As Object, e As EventArgs) Handles radAbsent.CheckedChanged
        If radAbsent.Checked Then RefreshGrid()
    End Sub

    Private Sub radChildren_CheckedChanged(sender As Object, e As EventArgs) Handles radChildren.CheckedChanged
        If radChildren.Checked Then SetOptions()
    End Sub

    Private Sub radStaff_CheckedChanged(sender As Object, e As EventArgs) Handles radStaff.CheckedChanged
        If radStaff.Checked Then SetOptions()
    End Sub

    Private Sub radVisitors_CheckedChanged(sender As Object, e As EventArgs) Handles radVisitors.CheckedChanged
        If radVisitors.Checked Then SetOptions()
    End Sub

#End Region

    Private Sub btnMassMove_Click(sender As Object, e As EventArgs) Handles btnMassMove.Click
        MassMove()
    End Sub

    Private Sub btnUndo_Click(sender As Object, e As EventArgs) Handles btnUndo.Click

        If tgRegister.CurrentRow Is Nothing Then Exit Sub

        btnUndo.Enabled = False
        Me.Cursor = Cursors.WaitCursor
        Application.DoEvents()

        Dim _ID As String = tgRegister.CurrentRow("ID").ToString
        Dim _Name As String = tgRegister.CurrentRow("Name").ToString

        If Msgbox("Are you sure you want to perform an Undo?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, "Confirm Undo") = DialogResult.Yes Then
            If radCheckedIn.Checked Then UndoCheckIn(_ID, _Name)
            If radCheckedOut.Checked Then UndoCheckOut(_ID, _Name)
            If radAbsent.Checked Then UndoAbsence(_ID, _Name)
        End If

        RefreshGrid()

        Me.Cursor = Cursors.Default
        Application.DoEvents()

    End Sub

    Private Sub UndoCheckIn(ByVal PersonID As String, ByVal PersonName As String)

        'we simply delete all the Register and RegisterSummary records for this person, this day

        Dim _SQL As String = ""

        _SQL += "delete from Register"
        _SQL += " where date = '" + Parameters.TodaySQLDate + "'"
        _SQL += " and person_id = '" + PersonID + "'"

        DAL.ExecuteCommand(_SQL)

        _SQL = ""
        _SQL += "delete from RegisterSummary"
        _SQL += " where day_id = '" + Parameters.TodayID + "'"
        _SQL += " and person_id = '" + PersonID + "'"

        DAL.ExecuteCommand(_SQL)

    End Sub

    Private Sub UndoCheckOut(ByVal PersonID As String, ByVal PersonName As String)

        Dim _PersonType As String = "C"
        If radStaff.Checked Then _PersonType = "S"
        If radVisitors.Checked Then _PersonType = "V"

        'fetch the original check-in from the register for this person (to get the location)
        Dim _SQL As String = ""

        _SQL += "select location, stamp_in from Register"
        _SQL += " where date = '" + Parameters.TodaySQLDate + "'"
        _SQL += " and person_id = '" + PersonID + "'"
        _SQL += " and in_out = 'I'"

        Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
        If _DR IsNot Nothing Then

            Dim _Location As String = _DR.Item("location").ToString
            Dim _Time As Date = _DR.Item("stamp_in")

            'now delete the Register Out transaction
            _SQL = ""
            _SQL += "delete from Register"
            _SQL += " where date = '" + Parameters.TodaySQLDate + "'"
            _SQL += " and person_id = '" + PersonID + "'"
            _SQL += " and in_out = 'O'"

            DAL.ExecuteCommand(_SQL)

            'update the RegisterSummary table with the location etc
            UpdateRegisterSummary(New Guid(PersonID), PersonName, _PersonType, "I", _Location, _Time, True)

        End If

    End Sub

    Private Sub UndoAbsence(ByVal PersonID As String, ByVal PersonName As String)

        Dim _SQL As String = ""

        _SQL += "delete from Activity"
        _SQL += " where day_id = '" + Parameters.TodayID + "'"
        _SQL += " and key_id = '" + PersonID + "'"
        _SQL += " and type = 'ABSENCE'"

        DAL.ExecuteCommand(_SQL)

    End Sub

End Class
