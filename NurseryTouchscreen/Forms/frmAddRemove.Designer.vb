﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAddRemove
    Inherits NurseryTouchscreen.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tgRecords = New NurseryTouchscreen.TouchGrid()
        Me.btnRemove = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAdd = New DevExpress.XtraEditors.SimpleButton()
        Me.btnPhoto = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'tgRecords
        '
        Me.tgRecords.DisableTimeFormatting = False
        Me.tgRecords.HideFirstColumn = False
        Me.tgRecords.Location = New System.Drawing.Point(12, 12)
        Me.tgRecords.Name = "tgRecords"
        Me.tgRecords.Size = New System.Drawing.Size(1000, 457)
        Me.tgRecords.TabIndex = 87
        '
        'btnRemove
        '
        Me.btnRemove.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRemove.Appearance.Options.UseFont = True
        Me.btnRemove.Image = Global.NurseryTouchscreen.My.Resources.Resources.delete_32
        Me.btnRemove.Location = New System.Drawing.Point(325, 481)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(305, 45)
        Me.btnRemove.TabIndex = 101
        Me.btnRemove.Text = "Remove"
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Image = Global.NurseryTouchscreen.My.Resources.Resources.cancel_32
        Me.btnCancel.Location = New System.Drawing.Point(829, 481)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(183, 45)
        Me.btnCancel.TabIndex = 100
        Me.btnCancel.Text = "Cancel"
        '
        'btnAdd
        '
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.Appearance.Options.UseFont = True
        Me.btnAdd.Image = Global.NurseryTouchscreen.My.Resources.Resources.add_32
        Me.btnAdd.Location = New System.Drawing.Point(12, 481)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(305, 45)
        Me.btnAdd.TabIndex = 89
        Me.btnAdd.Text = "Add"
        '
        'btnPhoto
        '
        Me.btnPhoto.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPhoto.Appearance.Options.UseFont = True
        Me.btnPhoto.Image = Global.NurseryTouchscreen.My.Resources.Resources.camera_32
        Me.btnPhoto.Location = New System.Drawing.Point(3, 3)
        Me.btnPhoto.Name = "btnPhoto"
        Me.btnPhoto.Size = New System.Drawing.Size(190, 45)
        Me.btnPhoto.TabIndex = 1
        Me.btnPhoto.Text = "Take Photo"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton2.Appearance.Options.UseFont = True
        Me.SimpleButton2.Image = Global.NurseryTouchscreen.My.Resources.Resources.Highlightmarker_red_32
        Me.SimpleButton2.Location = New System.Drawing.Point(199, 3)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(362, 45)
        Me.SimpleButton2.TabIndex = 2
        Me.SimpleButton2.Text = "Start Assessment"
        '
        'frmAddRemove
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(1024, 538)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnRemove)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.tgRecords)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmAddRemove"
        Me.Text = "Add / Remove"
        Me.Controls.SetChildIndex(Me.tgRecords, 0)
        Me.Controls.SetChildIndex(Me.btnAdd, 0)
        Me.Controls.SetChildIndex(Me.btnCancel, 0)
        Me.Controls.SetChildIndex(Me.btnRemove, 0)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnPhoto As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tgRecords As NurseryTouchscreen.TouchGrid
    Friend WithEvents btnAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRemove As DevExpress.XtraEditors.SimpleButton

End Class
