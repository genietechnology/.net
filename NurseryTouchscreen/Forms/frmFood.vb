﻿Option Strict On
Imports NurseryTouchscreen.SharedModule

Public Class frmFood

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        SetColours()

    End Sub

    Private m_None As Color
    Private m_Some As Color
    Private m_Most As Color
    Private m_All As Color

    Private Sub SetColours()
        m_None = btnNone1.BackColor
        m_Some = btnSome1.BackColor
        m_Most = btnMost1.BackColor
        m_All = btnAll1.BackColor
    End Sub

    Private Sub frmFood_ChildChanged(sender As Object, e As EventArgs) Handles Me.ChildChanged
        ClearButtons()
    End Sub

    Private Sub frmFood_AcceptChanged(sender As Object, e As AcceptArgs) Handles Me.AcceptChanged

        btnAdd.Enabled = e.Enabled
        btnRemoveAll.Enabled = e.Enabled

        'capture the meal button, before we disable it
        Dim _btn As DevExpress.XtraEditors.SimpleButton = Nothing
        If e.Enabled = False Then
            _btn = ReturnMealButton()
        End If

        ToggleMealButtons(e.Enabled)

        If e.Enabled = False Then
            If _btn IsNot Nothing Then
                _btn.Enabled = True
                _btn.Appearance.BackColor = Color.Yellow
                Application.DoEvents()
            End If
        End If

    End Sub

    Protected Overrides Function BeforeCommitUpdate() As Boolean
        If HasActivity() Then
            Return CheckAnswers()
        Else
            Return True
        End If
    End Function

    Private Function HasActivity() As Boolean

        Dim _btn As DevExpress.XtraEditors.SimpleButton = ReturnMealButton()
        If _btn Is Nothing Then
            Return False
        Else
            For i = 1 To 8
                Dim _name As String = "btnFood" + i.ToString
                Dim _ctrl As Control() = Me.Controls.Find(_name, True)
                If _ctrl.Length = 1 Then
                    If _ctrl(0).Enabled Then
                        If _ctrl(0).AccessibleDescription <> "-1" Then
                            Return True
                        End If
                    End If
                End If
            Next
            Return False
        End If

    End Function

    Protected Overrides Sub CommitUpdate()
        Save()
    End Sub

    Protected Overrides Sub DisplayRecord()

        If BabyFood Then
            btnAdd.Text = "Add Baby Food"
            ToggleMealButtons(False)
            DisplayMealDetails(EnumMealType.BabyFood, g_BabyFoodMealID)
        Else
            btnAdd.Text = "Add Food Item"
            SetDay()
            ToggleMealButtons(True)
        End If

    End Sub

    Private Sub EnableMealButton(ByRef ButtonIn As DevExpress.XtraEditors.SimpleButton, ByVal Enable As Boolean)
        If Enable Then
            If ButtonIn.AccessibleDescription = "" Then
                ButtonIn.Enabled = False
            Else
                ButtonIn.Enabled = True
            End If
        Else
            ButtonIn.Enabled = False
            ButtonIn.Appearance.Reset()
        End If
    End Sub

    Private Sub ToggleMealButtons(ByVal Enable As Boolean)
        EnableMealButton(btnBreakfast, Enable)
        EnableMealButton(btnSnack, Enable)
        EnableMealButton(btnLunch, Enable)
        EnableMealButton(btnLunchDessert, Enable)
        EnableMealButton(btnTea, Enable)
        EnableMealButton(btnTeaDessert, Enable)
    End Sub

    Private Sub SetDay()

        Dim _SQL As String = "select * from Day" & _
                             " where id = '" & TodayID & "'"

        Dim _dr As DataRow = DAL.ReturnDataRow(_SQL)
        If _dr IsNot Nothing Then
            btnBreakfast.Tag = _dr.Item("breakfast_id").ToString
            btnBreakfast.AccessibleDescription = _dr.Item("breakfast_name").ToString
            btnSnack.Tag = _dr.Item("snack_id").ToString
            btnSnack.AccessibleDescription = _dr.Item("snack_name").ToString
            btnLunch.Tag = _dr.Item("lunch_id").ToString
            btnLunch.AccessibleDescription = _dr.Item("lunch_name").ToString
            btnLunchDessert.Tag = _dr.Item("lunch_des_id").ToString
            btnLunchDessert.AccessibleDescription = _dr.Item("lunch_des_name").ToString
            btnTea.Tag = _dr.Item("tea_id").ToString
            btnTea.AccessibleDescription = _dr.Item("tea_name").ToString
            btnTeaDessert.Tag = _dr.Item("tea_des_id").ToString
            btnTeaDessert.AccessibleDescription = _dr.Item("tea_des_name").ToString
            SelectFirstMeal()
        End If

        _dr = Nothing

    End Sub

    Private Sub SelectFirstMeal()

        If btnBreakfast.AccessibleDescription <> "" Then
            Breakfast()
            Exit Sub
        End If

        If btnSnack.AccessibleDescription <> "" Then
            Snack()
            Exit Sub
        End If

        If btnLunch.AccessibleDescription <> "" Then
            Lunch()
            Exit Sub
        End If

        If btnLunchDessert.AccessibleDescription <> "" Then
            LunchDessert()
            Exit Sub
        End If

        If btnTea.AccessibleDescription <> "" Then
            Tea()
            Exit Sub
        End If

        If btnTeaDessert.AccessibleDescription <> "" Then
            TeaDessert()
            Exit Sub
        End If

    End Sub


    Private Sub DisplayMealDetails(ByVal MealType As EnumMealType, ByVal MealID As String)

        ClearButtons()

        If MealID = "" Then Exit Sub

        Dim _OffMenuMode As Boolean = OffMenu
        Dim _MealCode As String = ReturnMealCode(MealType)
        Dim _DetailRow As Integer = 1

        If Parameters.UseBasicMeals Then
            _OffMenuMode = True
            If Not OffMenu Then
                DisplayMeal(MealType, MealID)
                _DetailRow += 1
            End If
        End If

        Dim _SQL As String = ""

        If BabyFood Then

            _SQL = "select food_id, food_name from FoodRegister" & _
                   " where day_id = '" & TodayID & "'" & _
                   " and child_id = '" & txtChild.Tag.ToString & "'" & _
                   " and meal_id = '" & g_BabyFoodMealID & "'" & _
                   " and meal_type = '" & _MealCode & "'" & _
                   " order by food_name"

        Else

            If _OffMenuMode Then

                _SQL = "select food_id, food_name from FoodRegister" & _
                       " where day_id = '" & TodayID & "'" & _
                       " and child_id = '" & txtChild.Tag.ToString & "'" & _
                       " and meal_id = '" & MealID & "'" & _
                       " and meal_type = '" & _MealCode & "'" & _
                       " and meal_id <> food_id" & _
                       " order by food_name"

            Else

                _SQL = "select food_id, food_name from MealComponents" & _
                       " where meal_id = '" & MealID & "'" & _
                       " UNION" & _
                       " select food_id, food_name from FoodRegister" & _
                       " where day_id = '" & TodayID & "'" & _
                       " and child_id = '" & txtChild.Tag.ToString & "'" & _
                       " and meal_id = '" & MealID & "'" & _
                       " and meal_type = '" & _MealCode & "'" & _
                       " order by food_name"

            End If


        End If

        Dim _dt As DataTable = DAL.ReturnDataTable(_SQL)
        If _dt IsNot Nothing Then

            If _dt.Rows.Count > 0 Then

                btnRemoveAll.Enabled = True

                For Each Row As DataRow In _dt.Rows
                    Dim _Status As Integer = ReturnStatus(MealType, Row.Item("food_id").ToString)
                    SetButton("btnFood", _DetailRow, True, Row.Item("food_name").ToString, Row.Item("food_id").ToString, System.Drawing.SystemColors.Control, _Status)
                    SetAnswers(_DetailRow, MealType, Row.Item("food_id").ToString)
                    _DetailRow += 1
                Next

            End If

            _dt.Dispose()
            _dt = Nothing

        End If

        btnAdd.Enabled = True

    End Sub

    Private Sub DisplayMeal(ByVal MealType As EnumMealType, ByVal MealID As String)

        Dim _FoodID As String = ""
        Dim _FoodName As String = ""
        Dim _Status As Integer = -1

        '***********************************************************************************************************************

        Dim _btn As DevExpress.XtraEditors.SimpleButton = ReturnMealButton()
        Dim _MealType As EnumMealType = ReturnMealTypeEnum(ReturnMealCode(_btn.Text))

        _FoodID = _btn.Tag.ToString
        _FoodName = _btn.AccessibleDescription

        _Status = ReturnStatus(_MealType, _FoodID)
        SetButton("btnFood", 1, True, _FoodName, _FoodID, System.Drawing.SystemColors.Control, _Status)
        SetAnswers(1, _MealType, _FoodID)

        '***********************************************************************************************************************

    End Sub

    Private Sub SetButton(ByVal Name As String, ByVal ControlIndex As Integer, _
                          ByVal Enabled As Boolean, ByVal FoodName As String, ByVal FoodID As String, _
                          ByVal BackColour As System.Drawing.Color, _
                          ByVal Status As Integer)

        Dim _Name As String = Name + ControlIndex.ToString
        Dim _ctrl As Control() = Me.Controls.Find(_Name, True)
        If _ctrl.Length = 1 Then
            Dim _b As DevExpress.XtraEditors.SimpleButton
            _b = CType(_ctrl(0), DevExpress.XtraEditors.SimpleButton)
            _b.Enabled = Enabled
            _b.Tag = FoodID
            _b.Text = FoodName
            _b.Appearance.BackColor = BackColour
            _b.AccessibleDescription = Status.ToString
        End If

    End Sub

    Private Sub SetAnswers(ByVal ControlIndex As Integer, ByVal MealType As EnumMealType, ByVal FoodID As String)

        Dim _None As System.Drawing.Color = System.Drawing.SystemColors.Control
        Dim _Some As System.Drawing.Color = System.Drawing.SystemColors.Control
        Dim _Most As System.Drawing.Color = System.Drawing.SystemColors.Control
        Dim _All As System.Drawing.Color = System.Drawing.SystemColors.Control

        Dim _Status As Integer = ReturnStatus(MealType, FoodID)

        Select Case _Status

            Case 0
                _None = m_None
                _Some = System.Drawing.SystemColors.Control
                _Most = System.Drawing.SystemColors.Control
                _All = System.Drawing.SystemColors.Control

            Case 1
                _None = System.Drawing.SystemColors.Control
                _Some = m_Some
                _Most = System.Drawing.SystemColors.Control
                _All = System.Drawing.SystemColors.Control

            Case 2
                _None = System.Drawing.SystemColors.Control
                _Some = System.Drawing.SystemColors.Control
                _Most = m_Most
                _All = System.Drawing.SystemColors.Control

            Case 3
                _None = System.Drawing.SystemColors.Control
                _Some = System.Drawing.SystemColors.Control
                _Most = System.Drawing.SystemColors.Control
                _All = m_All

        End Select

        SetButton("btnNone", ControlIndex, True, "None", "", _None, _Status)
        SetButton("btnSome", ControlIndex, True, "Some", "", _Some, _Status)
        SetButton("btnMost", ControlIndex, True, "Most", "", _Most, _Status)
        SetButton("btnAll", ControlIndex, True, "All", "", _All, _Status)

    End Sub

    Private Function ReturnStatus(ByVal MealType As EnumMealType, ByVal FoodID As String) As Integer

        Dim _MealType As String = ReturnMealCode(MealType)

        Dim _Return As Integer = -1
        Dim _SQL As String = "select food_status from FoodRegister" & _
                     " where day_id = '" & TodayID & "'" & _
                     " and child_id = '" & txtChild.Tag.ToString & "'" & _
                     " and meal_type = '" & _MealType & "'" & _
                     " and food_id = '" & FoodID & "'"

        Dim _dr As DataRow = DAL.ReturnDataRow(_SQL)
        If _dr IsNot Nothing Then
            _Return = CInt(_dr("food_status"))
        End If

        _dr = Nothing

        Return _Return

    End Function

    Private Sub ClearButtons()

        Dim i As Integer = 1
        For i = 1 To 8
            ClearButton("btnFood", i)
            ClearButton("btnNone", i)
            ClearButton("btnSome", i)
            ClearButton("btnMost", i)
            ClearButton("btnAll", i)
        Next

        btnAdd.Enabled = False
        btnRemoveAll.Enabled = False

    End Sub

    Private Sub ClearButton(ByVal ButtonName As String, ByVal ControlIndex As Integer)

        Dim _name As String = ButtonName + ControlIndex.ToString
        Dim _ctrl As Control() = Me.Controls.Find(_name, True)
        If _ctrl.Length = 1 Then
            Dim _b As DevExpress.XtraEditors.SimpleButton = CType(_ctrl(0), DevExpress.XtraEditors.SimpleButton)
            _b.Enabled = False
            _b.Tag = ""
            _b.Text = ""
            _b.Appearance.Reset()
        End If
    End Sub

    Private Sub Breakfast()
        btnBreakfast.Appearance.BackColor = Color.Yellow
        btnSnack.Appearance.Reset()
        btnLunch.Appearance.Reset()
        btnLunchDessert.Appearance.Reset()
        btnTea.Appearance.Reset()
        btnTeaDessert.Appearance.Reset()
        DisplayMealDetails(EnumMealType.Breakfast, btnBreakfast.Tag.ToString)
    End Sub

    Private Function ReturnMealButton() As DevExpress.XtraEditors.SimpleButton
        If btnBreakfast.Appearance.BackColor = Color.Yellow Then Return btnBreakfast
        If btnSnack.Appearance.BackColor = Color.Yellow Then Return btnSnack
        If btnLunch.Appearance.BackColor = Color.Yellow Then Return btnLunch
        If btnLunchDessert.Appearance.BackColor = Color.Yellow Then Return btnLunchDessert
        If btnTea.Appearance.BackColor = Color.Yellow Then Return btnTea
        If btnTeaDessert.Appearance.BackColor = Color.Yellow Then Return btnTeaDessert
        Return Nothing
    End Function

    Private Sub Save()

        Dim _MealID As String = ""
        Dim _MealName As String = ""
        Dim _MealType As String = ""

        If BabyFood Then
            _MealID = g_BabyFoodMealID
            _MealName = "Baby Food"
            _MealType = "J"
        Else
            Dim _btnMeal As DevExpress.XtraEditors.SimpleButton = ReturnMealButton()
            If _btnMeal IsNot Nothing Then
                _MealID = _btnMeal.Tag.ToString
                _MealType = _btnMeal.Text
                _MealName = _btnMeal.AccessibleDescription
            End If
        End If

        If _MealID <> "" Then

            _MealType = ReturnMealCode(_MealType)

            For i = 1 To 8

                Dim _name As String = "btnFood" + i.ToString
                Dim _ctrl As Control() = Me.Controls.Find(_name, True)
                If _ctrl.Length = 1 Then
                    If _ctrl(0).Enabled Then
                        Dim _FoodID As String = _ctrl(0).Tag.ToString
                        Dim _FoodName As String = _ctrl(0).Text
                        Dim _FoodStatus As String = _ctrl(0).AccessibleDescription
                        LogFood(TodayID, txtChild.Tag.ToString, txtChild.Text, _MealID, _MealType, _MealName, _FoodID, _FoodName, _FoodStatus)
                    End If
                End If

            Next

        Else
            Msgbox("No Meal has been selected! Save Cancelled.", MessageBoxIcon.Error, "Save Food")
        End If

    End Sub

    Private Function CheckAnswers() As Boolean

        For i = 1 To 8

            Dim _name As String = "btnFood" + i.ToString
            Dim _ctrl As Control() = Me.Controls.Find(_name, True)
            If _ctrl.Length = 1 Then
                If _ctrl(0).Enabled Then
                    If _ctrl(0).AccessibleDescription = "-1" Then
                        Msgbox("Please select amount of food eaten for " & _ctrl(0).Text, MessageBoxIcon.Exclamation, "Missing Status")
                        Return False
                    End If
                End If
            End If

        Next

        Return True

    End Function

#Region "Food Status Buttons"

    Private Sub btnNone1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNone1.Click
        btnFood1.AccessibleDescription = "0"
        btnNone1.Appearance.BackColor = Color.Red
        btnSome1.Appearance.Reset()
        btnMost1.Appearance.Reset()
        btnAll1.Appearance.Reset()
    End Sub

    Private Sub btnSome1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSome1.Click
        btnFood1.AccessibleDescription = "1"
        btnNone1.Appearance.Reset()
        btnSome1.Appearance.BackColor = Color.Orange
        btnMost1.Appearance.Reset()
        btnAll1.Appearance.Reset()

    End Sub

    Private Sub btnMost1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMost1.Click
        btnFood1.AccessibleDescription = "2"
        btnNone1.Appearance.Reset()
        btnSome1.Appearance.Reset()
        btnMost1.Appearance.BackColor = Color.Yellow
        btnAll1.Appearance.Reset()

    End Sub

    Private Sub btnAll1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAll1.Click
        btnFood1.AccessibleDescription = "3"
        btnNone1.Appearance.Reset()
        btnSome1.Appearance.Reset()
        btnMost1.Appearance.Reset()
        btnAll1.Appearance.BackColor = Color.YellowGreen
    End Sub

    Private Sub btnNone2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNone2.Click
        btnFood2.AccessibleDescription = "0"
        btnNone2.Appearance.BackColor = Color.Red
        btnSome2.Appearance.Reset()
        btnMost2.Appearance.Reset()
        btnAll2.Appearance.Reset()
    End Sub

    Private Sub btnSome2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSome2.Click
        btnFood2.AccessibleDescription = "1"
        btnNone2.Appearance.Reset()
        btnSome2.Appearance.BackColor = Color.Orange
        btnMost2.Appearance.Reset()
        btnAll2.Appearance.Reset()

    End Sub

    Private Sub btnMost2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMost2.Click
        btnFood2.AccessibleDescription = "2"
        btnNone2.Appearance.Reset()
        btnSome2.Appearance.Reset()
        btnMost2.Appearance.BackColor = Color.Yellow
        btnAll2.Appearance.Reset()

    End Sub

    Private Sub btnAll2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAll2.Click
        btnFood2.AccessibleDescription = "3"
        btnNone2.Appearance.Reset()
        btnSome2.Appearance.Reset()
        btnMost2.Appearance.Reset()
        btnAll2.Appearance.BackColor = Color.YellowGreen
    End Sub

    Private Sub btnNone3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNone3.Click
        btnFood3.AccessibleDescription = "0"
        btnNone3.Appearance.BackColor = Color.Red
        btnSome3.Appearance.Reset()
        btnMost3.Appearance.Reset()
        btnAll3.Appearance.Reset()
    End Sub

    Private Sub btnSome3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSome3.Click
        btnFood3.AccessibleDescription = "1"
        btnNone3.Appearance.Reset()
        btnSome3.Appearance.BackColor = Color.Orange
        btnMost3.Appearance.Reset()
        btnAll3.Appearance.Reset()

    End Sub

    Private Sub btnMost3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMost3.Click
        btnFood3.AccessibleDescription = "2"
        btnNone3.Appearance.Reset()
        btnSome3.Appearance.Reset()
        btnMost3.Appearance.BackColor = Color.Yellow
        btnAll3.Appearance.Reset()

    End Sub

    Private Sub btnAll3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAll3.Click
        btnFood3.AccessibleDescription = "3"
        btnNone3.Appearance.Reset()
        btnSome3.Appearance.Reset()
        btnMost3.Appearance.Reset()
        btnAll3.Appearance.BackColor = Color.YellowGreen
    End Sub

    Private Sub btnNone4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNone4.Click
        btnFood4.AccessibleDescription = "0"
        btnNone4.Appearance.BackColor = Color.Red
        btnSome4.Appearance.Reset()
        btnMost4.Appearance.Reset()
        btnAll4.Appearance.Reset()
    End Sub

    Private Sub btnSome4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSome4.Click
        btnFood4.AccessibleDescription = "1"
        btnNone4.Appearance.Reset()
        btnSome4.Appearance.BackColor = Color.Orange
        btnMost4.Appearance.Reset()
        btnAll4.Appearance.Reset()

    End Sub

    Private Sub btnMost4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMost4.Click
        btnFood4.AccessibleDescription = "2"
        btnNone4.Appearance.Reset()
        btnSome4.Appearance.Reset()
        btnMost4.Appearance.BackColor = Color.Yellow
        btnAll4.Appearance.Reset()

    End Sub

    Private Sub btnAll4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAll4.Click
        btnFood4.AccessibleDescription = "3"
        btnNone4.Appearance.Reset()
        btnSome4.Appearance.Reset()
        btnMost4.Appearance.Reset()
        btnAll4.Appearance.BackColor = Color.YellowGreen
    End Sub

    Private Sub btnNone5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNone5.Click
        btnFood5.AccessibleDescription = "0"
        btnNone5.Appearance.BackColor = Color.Red
        btnSome5.Appearance.Reset()
        btnMost5.Appearance.Reset()
        btnAll5.Appearance.Reset()
    End Sub

    Private Sub btnSome5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSome5.Click
        btnFood5.AccessibleDescription = "1"
        btnNone5.Appearance.Reset()
        btnSome5.Appearance.BackColor = Color.Orange
        btnMost5.Appearance.Reset()
        btnAll5.Appearance.Reset()

    End Sub

    Private Sub btnMost5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMost5.Click
        btnFood5.AccessibleDescription = "2"
        btnNone5.Appearance.Reset()
        btnSome5.Appearance.Reset()
        btnMost5.Appearance.BackColor = Color.Yellow
        btnAll5.Appearance.Reset()

    End Sub

    Private Sub btnAll5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAll5.Click
        btnFood5.AccessibleDescription = "3"
        btnNone5.Appearance.Reset()
        btnSome5.Appearance.Reset()
        btnMost5.Appearance.Reset()
        btnAll5.Appearance.BackColor = Color.YellowGreen
    End Sub

    Private Sub btnNone6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNone6.Click
        btnFood6.AccessibleDescription = "0"
        btnNone6.Appearance.BackColor = Color.Red
        btnSome6.Appearance.Reset()
        btnMost6.Appearance.Reset()
        btnAll6.Appearance.Reset()
    End Sub

    Private Sub btnSome6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSome6.Click
        btnFood6.AccessibleDescription = "1"
        btnNone6.Appearance.Reset()
        btnSome6.Appearance.BackColor = Color.Orange
        btnMost6.Appearance.Reset()
        btnAll6.Appearance.Reset()

    End Sub

    Private Sub btnMost6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMost6.Click
        btnFood6.AccessibleDescription = "2"
        btnNone6.Appearance.Reset()
        btnSome6.Appearance.Reset()
        btnMost6.Appearance.BackColor = Color.Yellow
        btnAll6.Appearance.Reset()

    End Sub

    Private Sub btnAll6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAll6.Click
        btnFood6.AccessibleDescription = "3"
        btnNone6.Appearance.Reset()
        btnSome6.Appearance.Reset()
        btnMost6.Appearance.Reset()
        btnAll6.Appearance.BackColor = Color.YellowGreen
    End Sub

    Private Sub btnNone7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNone7.Click
        btnFood7.AccessibleDescription = "0"
        btnNone7.Appearance.BackColor = Color.Red
        btnSome7.Appearance.Reset()
        btnMost7.Appearance.Reset()
        btnAll7.Appearance.Reset()
    End Sub

    Private Sub btnSome7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSome7.Click
        btnFood7.AccessibleDescription = "1"
        btnNone7.Appearance.Reset()
        btnSome7.Appearance.BackColor = Color.Orange
        btnMost7.Appearance.Reset()
        btnAll7.Appearance.Reset()

    End Sub

    Private Sub btnMost7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMost7.Click
        btnFood7.AccessibleDescription = "2"
        btnNone7.Appearance.Reset()
        btnSome7.Appearance.Reset()
        btnMost7.Appearance.BackColor = Color.Yellow
        btnAll7.Appearance.Reset()

    End Sub

    Private Sub btnAll7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAll7.Click
        btnFood7.AccessibleDescription = "3"
        btnNone7.Appearance.Reset()
        btnSome7.Appearance.Reset()
        btnMost7.Appearance.Reset()
        btnAll7.Appearance.BackColor = Color.YellowGreen
    End Sub

    Private Sub btnNone8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNone8.Click
        btnFood8.AccessibleDescription = "0"
        btnNone8.Appearance.BackColor = Color.Red
        btnSome8.Appearance.Reset()
        btnMost8.Appearance.Reset()
        btnAll8.Appearance.Reset()
    End Sub

    Private Sub btnSome8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSome8.Click
        btnFood8.AccessibleDescription = "1"
        btnNone8.Appearance.Reset()
        btnSome8.Appearance.BackColor = Color.Orange
        btnMost8.Appearance.Reset()
        btnAll8.Appearance.Reset()

    End Sub

    Private Sub btnMost8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMost8.Click
        btnFood8.AccessibleDescription = "2"
        btnNone8.Appearance.Reset()
        btnSome8.Appearance.Reset()
        btnMost8.Appearance.BackColor = Color.Yellow
        btnAll8.Appearance.Reset()

    End Sub

    Private Sub btnAll8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAll8.Click
        btnFood8.AccessibleDescription = "3"
        btnNone8.Appearance.Reset()
        btnSome8.Appearance.Reset()
        btnMost8.Appearance.Reset()
        btnAll8.Appearance.BackColor = Color.YellowGreen
    End Sub

#End Region

#Region "Other Buttons"

    Private Sub btnAdd_Click(sender As System.Object, e As System.EventArgs) Handles btnAdd.Click

        Dim _MealID As String
        Dim _MealName As String
        Dim _MealType As String

        If BabyFood Then
            _MealID = g_BabyFoodMealID
            _MealType = "J"
            _MealName = "Baby Food"
        Else

            Dim _btn As DevExpress.XtraEditors.SimpleButton = ReturnMealButton()

            If _btn Is Nothing Then
                Msgbox("No meal has been selected. Ensure meals has been defined in the Today screen.", MessageBoxIcon.Exclamation, "Add Meal")
                Exit Sub
            End If

            _MealID = _btn.Tag.ToString
            _MealType = ReturnMealCode(_btn.Text)
            _MealName = _btn.AccessibleDescription

        End If

        Dim _SQL As String
        Dim _Caption As String
        Dim _Pair As Pair = Nothing

        Dim _GroupID As String = ""
        Dim _GroupName As String = ""

        'get food group
        _SQL = "select distinct group_id, group_name from Food order by group_name"
        _Caption = "Select Food Group"

        _Pair = ReturnButtonSQL(_SQL, _Caption)
        If _Pair Is Nothing Then
            Exit Sub
        Else
            _GroupID = _Pair.Code
            _GroupName = _Pair.Text
            _Pair = Nothing
        End If

        _SQL = "select id, name from Food where group_id = '" + _GroupID + "'"
        _Caption = "Select Food"

        If BabyFood Then
            _SQL += " and baby_food = 1"
            _Caption = "Select Baby Food"
        End If

        _SQL += " order by name"

        _Pair = ReturnButtonSQL(_SQL, _Caption)
        If _Pair IsNot Nothing Then

            Dim _FoodID As String = _Pair.Code
            Dim _FoodName As String = _Pair.Text
            Dim _FoodStatus As String = "3"

            LogFood(TodayID, txtChild.Tag.ToString, txtChild.Text, _MealID, _MealType, _MealName, _FoodID, _FoodName, _FoodStatus)
            DisplayMealDetails(ReturnMealTypeEnum(_MealType), _MealID)

        End If

    End Sub

    Private Sub btnBreakfast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBreakfast.Click
        Save()
        Breakfast()
    End Sub

    Private Sub btnSnack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSnack.Click
        Save()
        Snack()
    End Sub

    Private Sub btnLunch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLunch.Click
        Save()
        Lunch()
    End Sub

    Private Sub btnTea_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTea.Click
        Save()
        Tea()
    End Sub

    Private Sub btnLunchDessert_Click(sender As System.Object, e As System.EventArgs) Handles btnLunchDessert.Click
        Save()
        LunchDessert()
    End Sub

    Private Sub btnTeaDessert_Click(sender As System.Object, e As System.EventArgs) Handles btnTeaDessert.Click
        Save()
        TeaDessert()
    End Sub

    Private Sub Snack()
        btnBreakfast.Appearance.Reset()
        btnSnack.Appearance.BackColor = Color.Yellow
        btnLunch.Appearance.Reset()
        btnLunchDessert.Appearance.Reset()
        btnTea.Appearance.Reset()
        btnTeaDessert.Appearance.Reset()
        DisplayMealDetails(EnumMealType.Snack, btnSnack.Tag.ToString)
    End Sub

    Private Sub Lunch()
        btnBreakfast.Appearance.Reset()
        btnSnack.Appearance.Reset()
        btnLunch.Appearance.BackColor = Color.Yellow
        btnLunchDessert.Appearance.Reset()
        btnTea.Appearance.Reset()
        btnTeaDessert.Appearance.Reset()
        DisplayMealDetails(EnumMealType.Lunch, btnLunch.Tag.ToString)
    End Sub

    Private Sub Tea()
        btnBreakfast.Appearance.Reset()
        btnSnack.Appearance.Reset()
        btnLunch.Appearance.Reset()
        btnLunchDessert.Appearance.Reset()
        btnTea.Appearance.BackColor = Color.Yellow
        btnTeaDessert.Appearance.Reset()
        DisplayMealDetails(EnumMealType.Tea, btnTea.Tag.ToString)
    End Sub

    Private Sub LunchDessert()
        btnBreakfast.Appearance.Reset()
        btnSnack.Appearance.Reset()
        btnLunch.Appearance.Reset()
        btnLunchDessert.Appearance.BackColor = Color.Yellow
        btnTea.Appearance.Reset()
        DisplayMealDetails(EnumMealType.LunchDessert, btnLunchDessert.Tag.ToString)
    End Sub

    Private Sub TeaDessert()
        btnBreakfast.Appearance.Reset()
        btnSnack.Appearance.Reset()
        btnLunch.Appearance.Reset()
        btnTea.Appearance.Reset()
        btnTeaDessert.Appearance.BackColor = Color.Yellow
        DisplayMealDetails(EnumMealType.TeaDessert, btnTeaDessert.Tag.ToString)
    End Sub

    Private Sub btnRemoveAll_Click(sender As Object, e As EventArgs) Handles btnRemoveAll.Click
        RemoveAll()
        ClearButtons()
    End Sub

    Private Sub RemoveAll()

        Dim _BTN As DevExpress.XtraEditors.SimpleButton = ReturnMealButton()
        If _BTN Is Nothing Then Exit Sub

        If _BTN.Tag.ToString <> "" Then

            Dim _SQL As String = ""

            _SQL = "DELETE from FoodRegister" & _
                   " where day_id = '" & TodayID & "'" & _
                   " and child_id = '" & txtChild.Tag.ToString & "'" & _
                   " and meal_id = '" & _BTN.Tag.ToString & "'"

            DAL.ExecuteCommand(_SQL)

        End If

    End Sub

#End Region

End Class
