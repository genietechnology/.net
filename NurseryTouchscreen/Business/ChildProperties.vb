﻿Public Class ChildProperties

    Private m_Populated As Boolean = False
    Private mTodayNotes As String = ""

    Public ReadOnly Property Populated As Boolean
        Get
            Return m_Populated
        End Get
    End Property

    Public Property ID As String
    Public Property Name As String
    Public Property Forename As String
    Public Property BabyFood As Boolean
    Public Property OffMenu As Boolean
    Public Property Nappies As Boolean
    Public Property TodayNotes As String

    Public Sub Clear()
        m_Populated = False
        ID = ""
        Name = ""
        Forename = ""
        BabyFood = False
        OffMenu = False
        Nappies = True
        TodayNotes = ""
    End Sub

    Public Sub Retreive(ByVal ChildID As String)

        Dim sqlFilter As String = ""
        Dim hideChildName As Boolean = Parameters.HideChildName

        If hideChildName Then
            sqlFilter = "select id, knownas as 'name',"
        Else
            sqlFilter = "select id, fullname as 'name',"
        End If

        sqlFilter &= String.Concat(" forename, dob, baby_food, off_menu, nappies, milk, today_notes",
                                   " from Children",
                                   " where ID = '", ChildID, "'")

        Dim row As DataRow = DAL.ReturnDataRow(sqlFilter)
        If row IsNot Nothing Then

            ID = ChildID
            Name = row.Item("name").ToString
            Forename = row.Item("forename").ToString
            BabyFood = SharedModule.ReturnBoolean(row.Item("baby_food"))
            OffMenu = SharedModule.ReturnBoolean(row.Item("off_menu"))
            Nappies = SharedModule.ReturnBoolean(row.Item("nappies"))
            TodayNotes = row.Item("today_notes").ToString

            m_Populated = True

        Else
            m_Populated = False
        End If

    End Sub

End Class
