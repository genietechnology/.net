﻿Imports NurseryTouchscreen.SharedModule

Namespace Business

    Public Class Child

        Public Enum EnumQueryType
            AllChildren
            AllChildrenWithPhotos
            DueInWithPhotos
            CheckedIn
            CheckedInWithPhotos
            CheckedOutWithPhotos
        End Enum

        Public Shared Function CheckedIn(ByVal ChildID As String) As Boolean

            Dim _Return As Boolean = False
            Dim _SQL As String = ReturnCheckedInSQL("", False)

            Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
            If _DT IsNot Nothing Then
                For Each _DR As DataRow In _DT.Rows
                    If _DR.Item("ID").ToString = ChildID Then
                        _Return = True
                        Exit For
                    End If
                Next
                _DT.Dispose()
                _DT = Nothing
            End If

            Return _Return

        End Function

        Public Shared Function ReturnLocation(ByVal ChildID As String)

            Dim _Return As String = ""
            Dim _SQL As String = ""

            If Parameters.DisableLocation Then
                _SQL += "select group_name as 'location' from Children"
                _SQL += " where ID = '" + ChildID + "'"
            Else
                _SQL += "select location from RegisterSummary"
                _SQL += " where day_id = '" + TodayID + "'"
                _SQL += " and person_type = 'C'"
                _SQL += " and person_id = '" + ChildID + "'"
            End If

            Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
            If _DR IsNot Nothing Then
                _Return = _DR.Item("location").ToString
                _DR = Nothing
            End If

            Return _Return

        End Function

        Public Shared Function ReturnPhoto(ByVal ID As String) As Drawing.Image

            Dim _Return As Drawing.Image = Nothing

            Dim _SQL As String = ""
            _SQL += "select d.data from Children c"
            _SQL += " left join AppDocs d on d.ID = c.photo"
            _SQL += " where c.ID = '" + ID + "'"

            Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
            If _DR IsNot Nothing Then
                _Return = DAL.GetImagefromByteArray(_DR.Item("data"))
                _DR = Nothing
            End If

            Return _Return

        End Function

        Public Shared Function ReturnName(ByVal ID As String) As String

            Dim hideChildName As Boolean = Parameters.HideChildName
            Dim returnValue As String = ""
            Dim sqlFilter As String = ""

            If hideChildName Then
                sqlFilter = "select knownas as 'name' from Children where ID = '" & ID & "'"
            Else
                sqlFilter = "select fullname as 'name' from Children where ID = '" & ID & "'"
            End If

            Dim row As DataRow = DAL.ReturnDataRow(sqlFilter)
            If row IsNot Nothing Then
                returnValue = row.Item("name").ToString
                row = Nothing
            End If

            Return returnValue

        End Function

        Public Shared Function ReturnFamilyID(ByVal ChildID As String) As String

            Dim _Return As String = ""
            Dim _SQL As String = "select family_id from Children where ID = '" + ChildID + "'"

            Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
            If _DR IsNot Nothing Then
                _Return = _DR.Item("family_id").ToString
                _DR = Nothing
            End If

            Return _Return

        End Function

        Public Shared Sub DrillDown(ByVal ChildID As String, ByVal ChildName As String)

            Dim _frm As New frmChild
            _frm.ChildID = ChildID
            _frm.ChildName = ChildName

            _frm.Show()
            _frm.BringToFront()

        End Sub

        Public Shared Sub DrillDownModal(ByVal ChildID As String, ByVal ChildName As String)

            Dim _frm As New frmChild
            _frm.ChildID = ChildID
            _frm.ChildName = ChildName

            _frm.ShowDialog()

            _frm.Dispose()
            _frm = Nothing

        End Sub

        Public Shared Sub UpdateTodayNotes(ByVal childID As Guid)

            Dim child As New ChildProperties
            Dim todayNotes As String = ""

            child.Retreive(childID.ToString)
            todayNotes = OSK("Today Notes", True, True, child.TodayNotes, MaxLength:=1024)

            Dim sql As String = "select * from children where ID = '" & childID.ToString & "'"
            Dim dataAdapter As IDataAdapter = DAL.ReturnDataAdapter(sql)

            If Not dataAdapter Is Nothing Then
                Dim dataRow As DataRow
                Dim dataSet As New DataSet
                dataAdapter.Fill(dataSet)

                dataRow = dataSet.Tables(0).Rows(0)
                dataRow("today_notes") = todayNotes
                DAL.UpdateDB(dataAdapter, dataSet)

            End If

        End Sub

        Private Shared Function SetDOB(ByVal PIN As String) As String

            Dim _d As String = PIN.Substring(0, 2)
            Dim _m As String = PIN.Substring(2, 2)
            Dim _y As String = PIN.Substring(4)

            If _y.Length = 2 Then _y = "20" + _y

            Dim _DOB As String = _y + "-" + _m + "-" + _d

            If Date.TryParse(_DOB, Nothing) Then
                Return _DOB
            Else
                Return ""
            End If

        End Function

        Public Shared Function ReturnChildFromPIN(ByRef ReturnPair As Pair, ByVal PIN As String) As Boolean

            Dim _Return As Boolean = False
            ReturnPair = Nothing

            If PIN.Length = 4 OrElse PIN.Length = 6 OrElse PIN.Length = 8 Then

                Dim _Children As List(Of Pair) = Business.Child.ReturnChildrenByPin(PIN)
                If _Children IsNot Nothing Then

                    Select Case _Children.Count

                        Case < 1
                            Msgbox("Child not found using the PIN entered.", MessageBoxIcon.Exclamation, "PIN Entry")

                        Case 1
                            ReturnPair = _Children.First
                            _Return = True

                        Case > 1
                            Msgbox("This child shares a DOB with another Child, please setup a PIN.", MessageBoxIcon.Exclamation, "PIN Entry")

                    End Select

                End If

            Else
                Msgbox("Invalid PIN entered. A PIN should be 4 digits, or a 6 or 8 digit DOB.", MessageBoxIcon.Exclamation, "PIN Entry")
            End If

            Return _Return

        End Function

        Public Shared Function ReturnChildrenByPin(ByVal pin As String) As List(Of Pair)

            Dim _pairs As New List(Of Pair)
            Dim _hideChildName As Boolean = Parameters.HideChildName

            If pin.Length = 4 OrElse pin.Length = 6 OrElse pin.Length = 8 Then

                Dim _sqlFilter As String = ""

                If _hideChildName Then
                    _sqlFilter = "select ID, knownas as 'name' from Children"
                Else
                    _sqlFilter = "select ID, fullname as 'name' from Children"
                End If

                _sqlFilter &= String.Concat(" where site_id = ", Parameters.SiteIDQuotes,
                                            " and status = 'Current'")

                'check PIN field for supplied PIN
                Dim _table As DataTable = DAL.ReturnDataTable(_sqlFilter & " and pin = '" & pin & "'")
                If _table Is Nothing OrElse _table.Rows.Count = 0 Then

                    'if not found check the DOB field 
                    Dim _dob As String = SetDOB(pin)
                    If _dob <> "" Then _table = DAL.ReturnDataTable(_sqlFilter & " and dob = '" & _dob & "'")

                    If _table Is Nothing OrElse _table.Rows.Count = 0 Then Return Nothing

                End If

                For Each row As DataRow In _table.Rows
                    Dim _pair As New Pair(row.Item("ID").ToString, row.Item("name").ToString)
                    _pairs.Add(_pair)
                Next

                _table.Dispose()
                _table = Nothing

            End If

            Return _pairs

        End Function

        Public Shared Function FindChildByGroup(ByVal Mode As SharedModule.Enums.PersonMode, Optional ByVal AnyDay As Boolean = False) As Pair

            Dim _Room As Pair = ReturnRoom(Parameters.SiteID, Enums.PersonType.Child)
            If _Room IsNot Nothing Then

                Dim _frm As New frmButtonsTiled

                Select Case Mode

                    Case Enums.PersonMode.Everyone
                        _frm.PopulateMode = frmButtonsTiled.EnumPopulateMode.Children
                        _frm.RoomFilter = _Room.Text
                        _frm.MultiSelect = False

                    Case Enums.PersonMode.OnlyCheckedOut
                        _frm.PopulateMode = frmButtonsTiled.EnumPopulateMode.ChildrenDueIn
                        _frm.RoomFilter = _Room.Text
                        _frm.MultiSelect = False

                    Case Enums.PersonMode.OnlyCheckedIn
                        _frm.PopulateMode = frmButtonsTiled.EnumPopulateMode.ChildrenCheckedIn
                        _frm.RoomFilter = _Room.Text
                        _frm.MultiSelect = False

                End Select

                _frm.ShowDialog()

                Dim _Return As Pair = Nothing

                If _frm.DialogResult = DialogResult.OK Then
                    _Return = _frm.SelectedPair
                End If

                _frm.Dispose()
                _frm = Nothing

                Return _Return

            Else
                Return Nothing
            End If

        End Function

        Public Shared Function FindChildrenByGroup(ByVal Mode As SharedModule.Enums.PersonMode, Optional ByVal AnyDay As Boolean = False) As List(Of Pair)

            Dim _RoomID As String = ReturnRoomFilter(Enums.PersonType.Child)
            If _RoomID = "" Then Return Nothing

            Dim _Return As New List(Of Pair)

            Select Case Mode

                Case Enums.PersonMode.OnlyCheckedOut

                    Dim _frm As New frmButtonsTiled
                    _frm.PopulateMode = frmButtonsTiled.EnumPopulateMode.ChildrenDueIn
                    _frm.MultiSelect = True
                    _frm.RoomFilter = _RoomID
                    _frm.ShowDialog()

                    If _frm.DialogResult = DialogResult.OK Then
                        _Return = _frm.SelectedPairs
                    Else
                        _Return = Nothing
                    End If

                    _frm.Dispose()
                    _frm = Nothing

            End Select

            Return _Return

        End Function

        Public Shared Function ReturnColour(ByVal AllergyRating As String, ByVal DietRestrictions As String) As Color

            Dim _Allergy As String = AllergyRating.Trim
            If _Allergy = "None" Then _Allergy = ""

            If _Allergy <> "" Then
                If _Allergy = "Serious" Then Return frmBase.txtRed.BackColor
                If _Allergy = "Moderate" Then Return frmBase.txtOrange.BackColor
            Else

                Dim _Diet As String = DietRestrictions.Trim
                If _Diet = "None" Then _Diet = ""

                If _Diet <> "" Then
                    Return frmBase.txtYellow.BackColor
                Else
                    Return Color.Black
                End If

            End If

        End Function

        Public Shared Function BuildBookingTimes(ByVal BookingFrom As Object, ByVal BookingTo As Object) As String
            If BookingFrom.ToString <> "" AndAlso BookingTo.ToString <> "" Then
                Return Format(CDate(BookingFrom.ToString), "HH:mm").ToString + " - " + Format(CDate(BookingTo.ToString), "HH:mm").ToString
            Else
                Return ""
            End If
        End Function

        Public Shared Function BuildBookingText(ByVal Tariffs As String, ByVal TimeSlots As String, Optional ByVal OneLine As Boolean = False) As String

            If OneLine Then

                Dim _Line As String = ""
                Dim _Lines As String() = Tariffs.Split(vbCrLf)

                For Each _l As String In _Lines
                    If _Line <> "" Then _Line += ", "
                    _Line += _l.Trim
                Next

                Return _Line

            End If

            Return Tariffs

        End Function

        Public Shared Function BuildAllergyText(ByVal AllergyRating As String, ByVal DietRestriction As String, ByVal Abbreviate As Boolean) As String

            Dim _Allergy As String = AllergyRating.Trim
            If _Allergy = "None" Then _Allergy = ""

            Dim _Diet As String = DietRestriction.Trim
            If _Diet = "None" Then _Diet = ""

            If _Allergy + _Diet = "" Then Return ""

            Dim _Return As String = ""

            'If _Allergy <> "" Then
            '    If Abbreviate Then
            '        _Return += "AG: " + _Allergy
            '    Else
            '        _Return += _Allergy + " Allergy"
            '    End If
            'End If

            If _Diet <> "" Then

                If _Return <> "" Then _Return += ", "

                If Abbreviate Then
                    _Return += "DR: " + _Diet
                Else
                    _Return += "Diet Restriction: " + _Diet
                End If

            End If

            Return _Return

        End Function

        Public Shared Function ReturnCheckedInSql(ByVal Location As String, ByVal Photos As Boolean) As String

            Dim sqlFilter As String = ""
            Dim hideChildName As Boolean = Parameters.HideChildName

            If hideChildName Then
                sqlFilter = "select c.id, c.knownas as 'name',"
            Else
                sqlFilter = "select c.id, c.fullname as 'name',"
            End If

            sqlFilter &= String.Concat("c.dob, c.nappies, c.diet_restrict, c.allergy_rating, c.med_allergies, c.med_medication, c.med_notes,",
                                       "b.booking_from, b.booking_to, b.tariff_name, b.timeslots")

            If Photos Then sqlFilter &= ", d.data as 'photo'"

            sqlFilter &= String.Concat(" from RegisterSummary r",
                                       " left join Children c on c.ID = r.person_id",
                                       " left join Bookings b on b.site_id = c.site_id and b.booking_date = r.date and b.child_id = c.ID")

            If Photos Then sqlFilter &= " left join AppDocs d on d.ID = c.photo"

            sqlFilter &= String.Concat(" where day_id = '", SharedModule.TodayID, "'",
                                       " and person_type = 'C'",
                                       " and r.in_out = 'I'")

            If Location <> "" Then
                sqlFilter &= " and r.location = '" & Location & "'"
            End If

            sqlFilter &= " order by name"

            Return sqlFilter

        End Function

        Public Shared Function ReturnAllChildren(ByVal Location As String, ByVal Photos As Boolean) As String

            Dim sqlfilter As String = ""
            Dim hideChildName As Boolean = Parameters.HideChildName

            If hideChildName Then
                sqlfilter = "select c.id, c.knownas as 'name',"
            Else
                sqlfilter = "select c.id, c.fullname as 'name',"
            End If

            sqlfilter &= "c.dob, c.nappies, c.diet_restrict, c.allergy_rating, c.med_allergies, c.med_medication, c.med_notes"

            If Photos Then
                sqlfilter &= ", d.data as 'photo'"
            End If

            sqlfilter &= " from Children c"

            If Photos Then
                sqlfilter &= " left join AppDocs d on d.ID = c.photo"
            End If

            sqlfilter &= " where c.status = 'Current'"

            If Location <> "" Then
                sqlfilter &= " and c.group_name = '" + Location + "'"
            End If

            sqlfilter &= " order by name"

            Return sqlfilter

        End Function

    End Class

End Namespace

