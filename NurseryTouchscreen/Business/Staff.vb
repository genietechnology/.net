﻿Imports NurseryTouchscreen.SharedModule

Namespace Business

    Public Class Staff

        Public Shared Function ReturnCurrentLocation(ByVal StaffID As Guid) As String

            Dim _Return As String = ""
            Dim _SQL As String = ""

            _SQL += "select location from RegisterSummary"
            _SQL += " where day_id = '" + Parameters.TodayID + "'"
            _SQL += " and person_id = '" + StaffID.ToString + "'"
            _SQL += " and in_out = 'I'"

            Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
            If _DR IsNot Nothing Then
                _Return = _DR.Item("location").ToString
                _DR = Nothing
            Else
                'person is not in
                Return "OUT"
            End If

            Return _Return

        End Function

        Public Shared Function ReturnIDFromPaxtonID(ByVal PaxtonID As String) As Guid?

            Dim _Return As Guid? = Nothing
            Dim _SQL As String = "select ID from staff where clock_no = '" + PaxtonID + "'"

            Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
            If _DR IsNot Nothing Then
                _Return = New Guid(_DR.Item("ID").ToString)
                _DR = Nothing
            End If

            Return _Return

        End Function

        Public Shared Function ReturnName(ByVal ID As String) As String

            Dim _Return As String = ""
            Dim _SQL As String = "select fullname from staff where ID = '" + ID + "'"

            Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
            If _DR IsNot Nothing Then
                _Return = _DR.Item("fullname").ToString
                _DR = Nothing
            End If

            Return _Return

        End Function

        Public Shared Function ValidatePIN(ByVal PIN As String) As Pair

            Dim _Return As Pair = Nothing
            Dim _SQL As String = "select id, fullname from staff where touch_pin = '" + PIN + "'"

            Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
            If _DT IsNot Nothing Then

                If _DT.Rows.Count > 0 Then
                    If _DT.Rows.Count = 1 Then
                        _Return = New Pair(_DT.Rows(0).Item("id").ToString, _DT.Rows(0).Item("fullname").ToString)
                    Else
                        Msgbox("One or more Staff members are using this PIN. Please correct this.", MessageBoxIcon.Exclamation, "Staff PIN")
                    End If
                End If

                _DT.Dispose()
                _DT = Nothing

            End If

            Return _Return

        End Function

        Private Shared Function FindSQL(ByVal Mode As SharedModule.Enums.PersonMode, Optional ByVal RoomName As String = "") As String

            Dim _SQLSQ As String = ""

            _SQLSQ += " ("

            _SQLSQ += " select person_id from RegisterSummary r"
            _SQLSQ += " where r.day_id = '" + TodayID + "'"
            _SQLSQ += " and r.person_id = s.ID"
            _SQLSQ += " and r.in_out = 'I'"

            If RoomName <> "" Then _SQLSQ += " and r.location = '" + RoomName + "'"

            _SQLSQ += ")"

            Dim _SQL As String = ""

            _SQL += "select s.id, s.fullname, s.job_title, s.group_name"

            If Parameters.ShowPhotos Then _SQL += " ,d.data"

            _SQL += " from Staff s"

            If Parameters.ShowPhotos Then _SQL += " left join AppDocs d on d.id = s.photo"

            _SQL += " where site_id = " + Parameters.SiteIDQuotes

            If Mode = Enums.PersonMode.OnlyCheckedIn Then
                _SQL += " and s.ID in " + _SQLSQ
            Else
                If Mode = Enums.PersonMode.OnlyCheckedOut Then
                    _SQL += " and s.ID not in " + _SQLSQ
                End If
            End If

            _SQL += " and s.status = 'C'"
            _SQL += " order by s.fullname"

            Return _SQL

        End Function

        Public Shared Function FindStaff(ByVal Mode As SharedModule.Enums.PersonMode, Optional ByVal MultiSelect As Boolean = False, Optional ByVal RoomName As String = "") As Pair

            Dim _Return As Pair = Nothing
            Dim _frm As New frmButtonsTiled
            With _frm
                .Text = "Find Staff"
                .PopulateMode = frmButtonsTiled.EnumPopulateMode.SQL
                .SQL = FindSQL(Mode, RoomName)
                .FieldID = "id"
                .FieldName = "fullname"
                .FieldBottomLeft = "group_name"
                .FieldBottomRight = "job_title"
                .FieldPhoto = "data"
                .MultiSelect = MultiSelect
                .ShowDialog()
                .BringToFront()
            End With

            If _frm.DialogResult = DialogResult.OK Then
                _Return = _frm.SelectedPair
            Else
                _Return = Nothing
            End If

            _frm.Dispose()
            _frm = Nothing

            Return _Return

        End Function

        Public Shared Function FindStaffMultiple(ByVal Mode As SharedModule.Enums.PersonMode) As List(Of Pair)

            Dim _Return As New List(Of Pair)

            Dim _frm As New frmButtonsTiled
            With _frm
                .Text = "Find Staff"
                .PopulateMode = frmButtonsTiled.EnumPopulateMode.SQL
                .SQL = FindSQL(Mode)
                .FieldID = "id"
                .FieldName = "fullname"
                .FieldBottomLeft = "group_name"
                .FieldBottomRight = "job_title"
                .FieldPhoto = "data"
                .MultiSelect = True
                .ShowDialog()
                .BringToFront()
            End With

            If _frm.DialogResult = DialogResult.OK Then
                _Return = _frm.SelectedPairs
            Else
                _Return = Nothing
            End If

            _frm.Dispose()
            _frm = Nothing

            Return _Return

        End Function

    End Class


End Namespace
