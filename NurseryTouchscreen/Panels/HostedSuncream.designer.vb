﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class HostedSuncream
    Inherits NurseryTouchscreen.BaseHostedPanel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(HostedSuncream))
        Me.panTop = New DevExpress.XtraEditors.PanelControl()
        Me.btnAdd = New DevExpress.XtraEditors.SimpleButton()
        Me.sc = New DevExpress.XtraEditors.XtraScrollableControl()
        CType(Me.panTop, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panTop.SuspendLayout()
        Me.SuspendLayout()
        '
        'panTop
        '
        Me.panTop.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.panTop.Controls.Add(Me.btnAdd)
        Me.panTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.panTop.Location = New System.Drawing.Point(0, 0)
        Me.panTop.Name = "panTop"
        Me.panTop.Size = New System.Drawing.Size(432, 29)
        Me.panTop.TabIndex = 1
        '
        'btnAdd
        '
        Me.btnAdd.Appearance.Options.UseTextOptions = True
        Me.btnAdd.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnAdd.Image = CType(resources.GetObject("btnAdd.Image"), System.Drawing.Image)
        Me.btnAdd.Location = New System.Drawing.Point(3, 3)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(150, 23)
        Me.btnAdd.TabIndex = 22
        Me.btnAdd.Text = "Add Suncream"
        '
        'sc
        '
        Me.sc.AllowTouchScroll = True
        Me.sc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.sc.Location = New System.Drawing.Point(0, 29)
        Me.sc.Name = "sc"
        Me.sc.Size = New System.Drawing.Size(432, 98)
        Me.sc.TabIndex = 96
        '
        'HostedSuncream
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.Controls.Add(Me.sc)
        Me.Controls.Add(Me.panTop)
        Me.Name = "HostedSuncream"
        Me.Size = New System.Drawing.Size(432, 127)
        CType(Me.panTop, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panTop.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents panTop As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sc As DevExpress.XtraEditors.XtraScrollableControl

End Class
