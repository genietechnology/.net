﻿Public Class BaseHostedPanel

    Private m_Initialised As Boolean = False
    Private m_Group As New GroupProperties
    Private m_Child As New ChildProperties
    Private m_Staff As New StaffProperties

#Region "Properties"

    Public ReadOnly Property IsDirty As Boolean
        Get

        End Get
    End Property

    Public ReadOnly Property SelectedChild As ChildProperties
        Get
            Return m_Child
        End Get
    End Property

    Public ReadOnly Property SelectedStaff As StaffProperties
        Get
            Return m_Staff
        End Get
    End Property

    Public ReadOnly Property SelectedGroup As GroupProperties
        Get
            Return m_Group
        End Get
    End Property

    Public ReadOnly Property Initialised As Boolean
        Get
            Return m_Initialised
        End Get
    End Property

#End Region

#Region "Events"

    Public Event Initialising()

    Public Event GroupChanging As EventHandler(Of RecordChangingArgs)
    Public Event ChildChanging As EventHandler(Of RecordChangingArgs)
    Public Event StaffChanging As EventHandler(Of RecordChangingArgs)

    Public Event GroupChanged(ByVal sender As Object, ByVal e As RecordChangedArgs)
    Public Event ChildChanged(ByVal sender As Object, ByVal e As RecordChangedArgs)
    Public Event StaffChanged(ByVal sender As Object, ByVal e As RecordChangedArgs)

    Public Event Accepting(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
    Public Event Accepted(ByVal sender As Object, ByVal e As EventArgs)

#End Region

    Public Sub Initialise()
        RaiseEvent Initialising()
        m_Initialised = True
    End Sub

    Public Sub ClearSelectedChild()
        m_Child.Clear()
    End Sub

    Public Sub ClearSelectedGroup()
        m_Group.Clear()
        m_Child.Clear()
    End Sub

    Public Sub ClearSelectedStaff()
        m_Staff.Clear()
    End Sub

    Public Sub OnChildChanged(ByVal ChildID As String, ChildName As String)

        Dim e As New RecordChangingArgs

        RaiseEvent ChildChanging(Me, e)

        If Not e.Cancel Then
            m_Child.Retreive(ChildID)
            RaiseEvent ChildChanged(Me, New RecordChangedArgs(ChildID, ChildName))
        End If

    End Sub

    Public Sub OnStaffChanged(ByVal StaffID As String, StaffName As String)

        Dim e As New RecordChangingArgs

        RaiseEvent StaffChanging(Me, e)

        If Not e.Cancel Then
            m_Staff.StaffID = StaffID
            m_Staff.StaffName = StaffName
            RaiseEvent StaffChanged(Me, New RecordChangedArgs(StaffID, StaffName))
        End If

    End Sub

    Public Sub OnGroupChanged(ByVal GroupID As String, GroupName As String)

        Dim e As New RecordChangingArgs

        RaiseEvent GroupChanging(Me, e)

        If Not e.Cancel Then
            RaiseEvent GroupChanged(Me, New RecordChangedArgs(GroupID, GroupName))
        End If

    End Sub

    Public Sub OnAcceptClicked()

        Dim e As New System.ComponentModel.CancelEventArgs

        RaiseEvent Accepting(Me, e)

        If Not e.Cancel Then
            RaiseEvent Accepted(Me, New EventArgs())
        End If

    End Sub

End Class
