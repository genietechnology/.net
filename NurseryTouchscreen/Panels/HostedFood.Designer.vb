﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class HostedFood
    Inherits BaseHostedPanel

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TileItemElement1 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement2 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement3 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement4 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement5 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement6 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement7 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement8 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Me.TileGroup1 = New DevExpress.XtraEditors.TileGroup()
        Me.TileItem4 = New DevExpress.XtraEditors.TileItem()
        Me.TileItem5 = New DevExpress.XtraEditors.TileItem()
        Me.TileItem6 = New DevExpress.XtraEditors.TileItem()
        Me.txtLightPink = New DevExpress.XtraEditors.TextEdit()
        Me.txtPink = New DevExpress.XtraEditors.TextEdit()
        Me.txtLightBlue = New DevExpress.XtraEditors.TextEdit()
        Me.txtBlue = New DevExpress.XtraEditors.TextEdit()
        Me.txtLightGrey = New DevExpress.XtraEditors.TextEdit()
        Me.txtGrey = New DevExpress.XtraEditors.TextEdit()
        Me.txtLightGreen = New DevExpress.XtraEditors.TextEdit()
        Me.txtLightOrange = New DevExpress.XtraEditors.TextEdit()
        Me.txtLightRed = New DevExpress.XtraEditors.TextEdit()
        Me.txtLightYellow = New DevExpress.XtraEditors.TextEdit()
        Me.txtLightCyan = New DevExpress.XtraEditors.TextEdit()
        Me.txtGreen = New DevExpress.XtraEditors.TextEdit()
        Me.txtOrange = New DevExpress.XtraEditors.TextEdit()
        Me.txtRed = New DevExpress.XtraEditors.TextEdit()
        Me.txtYellow = New DevExpress.XtraEditors.TextEdit()
        Me.txtCyan = New DevExpress.XtraEditors.TextEdit()
        Me.panBottom = New DevExpress.XtraEditors.PanelControl()
        Me.btnAddFood = New DevExpress.XtraEditors.SimpleButton()
        Me.scTop = New DevExpress.XtraEditors.XtraScrollableControl()
        Me.btnBreakfast = New DevExpress.XtraEditors.SimpleButton()
        Me.btnTeaDessert = New DevExpress.XtraEditors.SimpleButton()
        Me.btnLunchDessert = New DevExpress.XtraEditors.SimpleButton()
        Me.btnTea = New DevExpress.XtraEditors.SimpleButton()
        Me.btnLunch = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAM = New DevExpress.XtraEditors.SimpleButton()
        Me.btnPM = New DevExpress.XtraEditors.SimpleButton()
        Me.sc = New DevExpress.XtraEditors.XtraScrollableControl()
        CType(Me.txtLightPink.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPink.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLightBlue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBlue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLightGrey.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGrey.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLightGreen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLightOrange.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLightRed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLightYellow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLightCyan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOrange.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCyan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panBottom.SuspendLayout()
        Me.scTop.SuspendLayout()
        Me.SuspendLayout()
        '
        'TileGroup1
        '
        Me.TileGroup1.Items.Add(Me.TileItem4)
        Me.TileGroup1.Items.Add(Me.TileItem5)
        Me.TileGroup1.Items.Add(Me.TileItem6)
        Me.TileGroup1.Name = "TileGroup1"
        Me.TileGroup1.Text = "TileGroup1"
        '
        'TileItem4
        '
        TileItemElement1.Appearance.Normal.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TileItemElement1.Appearance.Normal.Options.UseFont = True
        TileItemElement1.Text = "TileItem1"
        TileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopLeft
        TileItemElement2.Text = "All"
        TileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopRight
        TileItemElement3.Text = "Most"
        TileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleRight
        TileItemElement4.Text = "Half"
        TileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomRight
        TileItemElement5.Text = "None"
        TileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft
        TileItemElement6.Text = "Some"
        TileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter
        Me.TileItem4.Elements.Add(TileItemElement1)
        Me.TileItem4.Elements.Add(TileItemElement2)
        Me.TileItem4.Elements.Add(TileItemElement3)
        Me.TileItem4.Elements.Add(TileItemElement4)
        Me.TileItem4.Elements.Add(TileItemElement5)
        Me.TileItem4.Elements.Add(TileItemElement6)
        Me.TileItem4.Id = 0
        Me.TileItem4.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide
        Me.TileItem4.Name = "TileItem4"
        '
        'TileItem5
        '
        TileItemElement7.Text = "TileItem2"
        Me.TileItem5.Elements.Add(TileItemElement7)
        Me.TileItem5.Id = 1
        Me.TileItem5.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide
        Me.TileItem5.Name = "TileItem5"
        '
        'TileItem6
        '
        TileItemElement8.Text = "TileItem3"
        Me.TileItem6.Elements.Add(TileItemElement8)
        Me.TileItem6.Id = 2
        Me.TileItem6.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide
        Me.TileItem6.Name = "TileItem6"
        '
        'txtLightPink
        '
        Me.txtLightPink.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtLightPink.Location = New System.Drawing.Point(206, 73)
        Me.txtLightPink.Name = "txtLightPink"
        Me.txtLightPink.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtLightPink.Properties.Appearance.Options.UseBackColor = True
        Me.txtLightPink.Size = New System.Drawing.Size(23, 20)
        Me.txtLightPink.TabIndex = 93
        Me.txtLightPink.Visible = False
        '
        'txtPink
        '
        Me.txtPink.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtPink.Location = New System.Drawing.Point(206, 47)
        Me.txtPink.Name = "txtPink"
        Me.txtPink.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtPink.Properties.Appearance.ForeColor = System.Drawing.Color.White
        Me.txtPink.Properties.Appearance.Options.UseBackColor = True
        Me.txtPink.Properties.Appearance.Options.UseForeColor = True
        Me.txtPink.Size = New System.Drawing.Size(23, 20)
        Me.txtPink.TabIndex = 92
        Me.txtPink.Visible = False
        '
        'txtLightBlue
        '
        Me.txtLightBlue.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtLightBlue.Location = New System.Drawing.Point(177, 73)
        Me.txtLightBlue.Name = "txtLightBlue"
        Me.txtLightBlue.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtLightBlue.Properties.Appearance.Options.UseBackColor = True
        Me.txtLightBlue.Size = New System.Drawing.Size(23, 20)
        Me.txtLightBlue.TabIndex = 91
        Me.txtLightBlue.Visible = False
        '
        'txtBlue
        '
        Me.txtBlue.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtBlue.Location = New System.Drawing.Point(177, 47)
        Me.txtBlue.Name = "txtBlue"
        Me.txtBlue.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBlue.Properties.Appearance.ForeColor = System.Drawing.Color.White
        Me.txtBlue.Properties.Appearance.Options.UseBackColor = True
        Me.txtBlue.Properties.Appearance.Options.UseForeColor = True
        Me.txtBlue.Size = New System.Drawing.Size(23, 20)
        Me.txtBlue.TabIndex = 90
        Me.txtBlue.Visible = False
        '
        'txtLightGrey
        '
        Me.txtLightGrey.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtLightGrey.Location = New System.Drawing.Point(148, 73)
        Me.txtLightGrey.Name = "txtLightGrey"
        Me.txtLightGrey.Properties.Appearance.BackColor = System.Drawing.Color.Silver
        Me.txtLightGrey.Properties.Appearance.Options.UseBackColor = True
        Me.txtLightGrey.Size = New System.Drawing.Size(23, 20)
        Me.txtLightGrey.TabIndex = 89
        Me.txtLightGrey.Visible = False
        '
        'txtGrey
        '
        Me.txtGrey.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtGrey.Location = New System.Drawing.Point(148, 47)
        Me.txtGrey.Name = "txtGrey"
        Me.txtGrey.Properties.Appearance.BackColor = System.Drawing.Color.Gray
        Me.txtGrey.Properties.Appearance.Options.UseBackColor = True
        Me.txtGrey.Size = New System.Drawing.Size(23, 20)
        Me.txtGrey.TabIndex = 88
        Me.txtGrey.Visible = False
        '
        'txtLightGreen
        '
        Me.txtLightGreen.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtLightGreen.Location = New System.Drawing.Point(61, 73)
        Me.txtLightGreen.Name = "txtLightGreen"
        Me.txtLightGreen.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtLightGreen.Properties.Appearance.Options.UseBackColor = True
        Me.txtLightGreen.Size = New System.Drawing.Size(23, 20)
        Me.txtLightGreen.TabIndex = 86
        Me.txtLightGreen.Visible = False
        '
        'txtLightOrange
        '
        Me.txtLightOrange.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtLightOrange.Location = New System.Drawing.Point(3, 73)
        Me.txtLightOrange.Name = "txtLightOrange"
        Me.txtLightOrange.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtLightOrange.Properties.Appearance.Options.UseBackColor = True
        Me.txtLightOrange.Size = New System.Drawing.Size(23, 20)
        Me.txtLightOrange.TabIndex = 84
        Me.txtLightOrange.Visible = False
        '
        'txtLightRed
        '
        Me.txtLightRed.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtLightRed.Location = New System.Drawing.Point(32, 73)
        Me.txtLightRed.Name = "txtLightRed"
        Me.txtLightRed.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtLightRed.Properties.Appearance.Options.UseBackColor = True
        Me.txtLightRed.Size = New System.Drawing.Size(23, 20)
        Me.txtLightRed.TabIndex = 83
        Me.txtLightRed.Visible = False
        '
        'txtLightYellow
        '
        Me.txtLightYellow.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtLightYellow.Location = New System.Drawing.Point(119, 73)
        Me.txtLightYellow.Name = "txtLightYellow"
        Me.txtLightYellow.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtLightYellow.Properties.Appearance.Options.UseBackColor = True
        Me.txtLightYellow.Size = New System.Drawing.Size(23, 20)
        Me.txtLightYellow.TabIndex = 85
        Me.txtLightYellow.Visible = False
        '
        'txtLightCyan
        '
        Me.txtLightCyan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtLightCyan.Location = New System.Drawing.Point(90, 73)
        Me.txtLightCyan.Name = "txtLightCyan"
        Me.txtLightCyan.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtLightCyan.Properties.Appearance.Options.UseBackColor = True
        Me.txtLightCyan.Size = New System.Drawing.Size(23, 20)
        Me.txtLightCyan.TabIndex = 87
        Me.txtLightCyan.Visible = False
        '
        'txtGreen
        '
        Me.txtGreen.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtGreen.Location = New System.Drawing.Point(61, 47)
        Me.txtGreen.Name = "txtGreen"
        Me.txtGreen.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtGreen.Properties.Appearance.Options.UseBackColor = True
        Me.txtGreen.Size = New System.Drawing.Size(23, 20)
        Me.txtGreen.TabIndex = 81
        Me.txtGreen.Visible = False
        '
        'txtOrange
        '
        Me.txtOrange.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtOrange.Location = New System.Drawing.Point(3, 47)
        Me.txtOrange.Name = "txtOrange"
        Me.txtOrange.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtOrange.Properties.Appearance.Options.UseBackColor = True
        Me.txtOrange.Size = New System.Drawing.Size(23, 20)
        Me.txtOrange.TabIndex = 79
        Me.txtOrange.Visible = False
        '
        'txtRed
        '
        Me.txtRed.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtRed.Location = New System.Drawing.Point(32, 47)
        Me.txtRed.Name = "txtRed"
        Me.txtRed.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtRed.Properties.Appearance.Options.UseBackColor = True
        Me.txtRed.Size = New System.Drawing.Size(23, 20)
        Me.txtRed.TabIndex = 78
        Me.txtRed.Visible = False
        '
        'txtYellow
        '
        Me.txtYellow.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtYellow.Location = New System.Drawing.Point(119, 47)
        Me.txtYellow.Name = "txtYellow"
        Me.txtYellow.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtYellow.Properties.Appearance.Options.UseBackColor = True
        Me.txtYellow.Size = New System.Drawing.Size(23, 20)
        Me.txtYellow.TabIndex = 80
        Me.txtYellow.Visible = False
        '
        'txtCyan
        '
        Me.txtCyan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtCyan.Location = New System.Drawing.Point(90, 47)
        Me.txtCyan.Name = "txtCyan"
        Me.txtCyan.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtCyan.Properties.Appearance.Options.UseBackColor = True
        Me.txtCyan.Size = New System.Drawing.Size(23, 20)
        Me.txtCyan.TabIndex = 82
        Me.txtCyan.Visible = False
        '
        'panBottom
        '
        Me.panBottom.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.panBottom.Controls.Add(Me.btnAddFood)
        Me.panBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panBottom.Location = New System.Drawing.Point(0, 61)
        Me.panBottom.Name = "panBottom"
        Me.panBottom.Size = New System.Drawing.Size(598, 35)
        Me.panBottom.TabIndex = 97
        '
        'btnAddFood
        '
        Me.btnAddFood.Location = New System.Drawing.Point(5, 5)
        Me.btnAddFood.Name = "btnAddFood"
        Me.btnAddFood.Size = New System.Drawing.Size(125, 23)
        Me.btnAddFood.TabIndex = 0
        Me.btnAddFood.Text = "Add Food Item"
        '
        'scTop
        '
        Me.scTop.AllowTouchScroll = True
        Me.scTop.Controls.Add(Me.btnBreakfast)
        Me.scTop.Controls.Add(Me.btnTeaDessert)
        Me.scTop.Controls.Add(Me.btnLunchDessert)
        Me.scTop.Controls.Add(Me.btnTea)
        Me.scTop.Controls.Add(Me.btnLunch)
        Me.scTop.Controls.Add(Me.btnAM)
        Me.scTop.Controls.Add(Me.btnPM)
        Me.scTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.scTop.Location = New System.Drawing.Point(0, 0)
        Me.scTop.Name = "scTop"
        Me.scTop.Size = New System.Drawing.Size(598, 39)
        Me.scTop.TabIndex = 7
        '
        'btnBreakfast
        '
        Me.btnBreakfast.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnBreakfast.Location = New System.Drawing.Point(3, 3)
        Me.btnBreakfast.Name = "btnBreakfast"
        Me.btnBreakfast.Size = New System.Drawing.Size(75, 23)
        Me.btnBreakfast.TabIndex = 0
        Me.btnBreakfast.Text = "Breakfast"
        '
        'btnTeaDessert
        '
        Me.btnTeaDessert.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnTeaDessert.Location = New System.Drawing.Point(489, 3)
        Me.btnTeaDessert.Name = "btnTeaDessert"
        Me.btnTeaDessert.Size = New System.Drawing.Size(75, 23)
        Me.btnTeaDessert.TabIndex = 6
        Me.btnTeaDessert.Text = "Tea Dessert"
        '
        'btnLunchDessert
        '
        Me.btnLunchDessert.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnLunchDessert.Location = New System.Drawing.Point(246, 3)
        Me.btnLunchDessert.Name = "btnLunchDessert"
        Me.btnLunchDessert.Size = New System.Drawing.Size(75, 23)
        Me.btnLunchDessert.TabIndex = 3
        Me.btnLunchDessert.Text = "Lunch Dessert"
        '
        'btnTea
        '
        Me.btnTea.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnTea.Location = New System.Drawing.Point(408, 3)
        Me.btnTea.Name = "btnTea"
        Me.btnTea.Size = New System.Drawing.Size(75, 23)
        Me.btnTea.TabIndex = 5
        Me.btnTea.Text = "Tea"
        '
        'btnLunch
        '
        Me.btnLunch.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnLunch.Location = New System.Drawing.Point(165, 3)
        Me.btnLunch.Name = "btnLunch"
        Me.btnLunch.Size = New System.Drawing.Size(75, 23)
        Me.btnLunch.TabIndex = 2
        Me.btnLunch.Text = "Lunch"
        '
        'btnAM
        '
        Me.btnAM.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnAM.Location = New System.Drawing.Point(84, 3)
        Me.btnAM.Name = "btnAM"
        Me.btnAM.Size = New System.Drawing.Size(75, 23)
        Me.btnAM.TabIndex = 1
        Me.btnAM.Text = "AM Snack"
        '
        'btnPM
        '
        Me.btnPM.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnPM.Location = New System.Drawing.Point(327, 3)
        Me.btnPM.Name = "btnPM"
        Me.btnPM.Size = New System.Drawing.Size(75, 23)
        Me.btnPM.TabIndex = 4
        Me.btnPM.Text = "PM Snack"
        '
        'sc
        '
        Me.sc.AllowTouchScroll = True
        Me.sc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.sc.Location = New System.Drawing.Point(0, 39)
        Me.sc.Name = "sc"
        Me.sc.Size = New System.Drawing.Size(598, 22)
        Me.sc.TabIndex = 100
        '
        'HostedFood
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.sc)
        Me.Controls.Add(Me.scTop)
        Me.Controls.Add(Me.panBottom)
        Me.Controls.Add(Me.txtLightPink)
        Me.Controls.Add(Me.txtPink)
        Me.Controls.Add(Me.txtLightBlue)
        Me.Controls.Add(Me.txtBlue)
        Me.Controls.Add(Me.txtLightGrey)
        Me.Controls.Add(Me.txtGrey)
        Me.Controls.Add(Me.txtLightGreen)
        Me.Controls.Add(Me.txtLightOrange)
        Me.Controls.Add(Me.txtLightRed)
        Me.Controls.Add(Me.txtLightYellow)
        Me.Controls.Add(Me.txtLightCyan)
        Me.Controls.Add(Me.txtGreen)
        Me.Controls.Add(Me.txtOrange)
        Me.Controls.Add(Me.txtRed)
        Me.Controls.Add(Me.txtYellow)
        Me.Controls.Add(Me.txtCyan)
        Me.Name = "HostedFood"
        Me.Size = New System.Drawing.Size(598, 96)
        CType(Me.txtLightPink.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPink.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLightBlue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBlue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLightGrey.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGrey.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLightGreen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLightOrange.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLightRed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLightYellow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLightCyan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOrange.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCyan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.panBottom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panBottom.ResumeLayout(False)
        Me.scTop.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TileGroup1 As DevExpress.XtraEditors.TileGroup
    Friend WithEvents TileItem4 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItem5 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItem6 As DevExpress.XtraEditors.TileItem
    Friend WithEvents txtLightPink As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtPink As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLightBlue As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtBlue As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLightGrey As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtGrey As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLightGreen As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLightOrange As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLightRed As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLightYellow As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLightCyan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtGreen As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtOrange As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRed As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtYellow As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCyan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents panBottom As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnAddFood As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents scTop As DevExpress.XtraEditors.XtraScrollableControl
    Friend WithEvents btnBreakfast As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnTeaDessert As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnLunchDessert As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnTea As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnLunch As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAM As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnPM As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sc As DevExpress.XtraEditors.XtraScrollableControl

End Class
