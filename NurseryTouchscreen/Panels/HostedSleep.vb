﻿Public Class HostedSleep

    Private m_ChildID As String = ""

    Private Sub HostedSleep_ChildChanged(sender As Object, e As RecordChangedArgs) Handles Me.ChildChanged
        m_ChildID = e.ValueID
        DisplaySleep(e.ValueID)
    End Sub

    Private Sub DisplaySleep(ByVal ChildID As String)

        sc.Controls.Clear()

        Dim _SQL As String = ""

        _SQL += "select ID, value_1, value_2, value_3, stamp"
        _SQL += " from Activity"
        _SQL += " where day_id = '" + Parameters.TodayID + "'"
        _SQL += " and key_id = '" + ChildID + "'"
        _SQL += " and type = 'SLEEP'"
        _SQL += " order by stamp"

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _ID As String = _DR.Item("ID").ToString
                Dim _Value1 As String = _DR.Item("value_1").ToString
                Dim _Value2 As String = _DR.Item("value_2").ToString
                Dim _Value3 As String = _DR.Item("value_3").ToString
                Dim _Stamp As Date? = _DR.Item("stamp")

                Dim _Item As New ItemControl(_ID, _Value1, False, True)
                With _Item
                    .ChildID = ChildID
                    .Value1 = _Value1
                    .Value2 = _Value2
                    .Value3 = _Value3
                    .Stamp = _Stamp
                    .Dock = DockStyle.Top
                    .BringToFront()
                End With

                AddHandler _Item.DeleteClick, AddressOf DeleteClick

                sc.Controls.Add(_Item)

            Next

            _DT.Dispose()
            _DT = Nothing

        End If

    End Sub

    Private Sub DeleteClick(sender As Object, e As EventArgs)

        If Msgbox("Are you sure you want to delete this Sleep Record?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Confirm Delete") = DialogResult.Yes Then
            Dim _I As ItemControl = CType(sender, ItemControl)
            If _I IsNot Nothing Then
                If _I.ActivtyID <> "" Then
                    SharedModule.DeleteActivity(_I.ActivtyID)
                    DisplaySleep(_I.ChildID)
                End If
            End If
        End If

    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click

        Dim _Start As String = SharedModule.TimeEntry("Please enter the Sleep Start time")
        Dim _End As String = SharedModule.TimeEntry("Please enter the Sleep End time")

        If _Start <> "" AndAlso _End <> "" Then

            Dim _ST As Date = _Start
            Dim _ED As Date = _End

            If _ST >= _ED Then
                Msgbox("Please enter a valid Sleep Period.", MessageBoxIcon.Warning, "Create Sleep Record")
            Else

                Dim _SleepTimes As String = _Start + "-" + _End
                SharedModule.LogSleep(SharedModule.TodayID, SelectedChild.ID, SelectedChild.Name, _SleepTimes, SelectedStaff.StaffID, SelectedStaff.StaffName)

                DisplaySleep(m_ChildID)

            End If

        End If

    End Sub

End Class
