﻿Option Strict On

Imports DevExpress.XtraBars.Navigation
Imports DevExpress.XtraEditors
Imports NurseryTouchscreen.SharedModule
Imports System.Reflection

Public Class HostedFood

    Private m_SelectedMealID As String = ""
    Private m_SelectedMealType As String = ""
    Private m_SelectedMealName As String = ""

    Private Sub HostedFood_Accepting(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles Me.Accepting
        SaveFood()
    End Sub

    Private Sub HostedFood_Initialising() Handles Me.Initialising
        SetupMeals()
        SelectFirstMeal()
    End Sub

    Private Sub SelectFirstMeal()
        If btnBreakfast.Tag.ToString <> "" Then SetMeal("B") : Exit Sub
        If btnAM.Tag.ToString <> "" Then SetMeal("S") : Exit Sub
        If btnLunch.Tag.ToString <> "" Then SetMeal("L") : Exit Sub
        If btnLunchDessert.Tag.ToString <> "" Then SetMeal("LD") : Exit Sub
        If btnPM.Tag.ToString <> "" Then SetMeal("SP") : Exit Sub
        If btnTea.Tag.ToString <> "" Then SetMeal("T") : Exit Sub
        If btnTeaDessert.Tag.ToString <> "" Then SetMeal("TD") : Exit Sub
    End Sub

    Private Sub HostedFood_ChildChanged(sender As Object, e As RecordChangedArgs) Handles Me.ChildChanged
        PopulateItems()
    End Sub

    Private Sub HostedFood_ChildChanging(sender As Object, e As RecordChangingArgs) Handles Me.ChildChanging
        SaveFood()
    End Sub

    Private Sub HostedFood_GroupChanged(sender As Object, e As RecordChangedArgs) Handles Me.GroupChanged
        sc.Controls.Clear()
    End Sub

    Private Sub HostedFood_GroupChanging(sender As Object, e As RecordChangingArgs) Handles Me.GroupChanging
        SaveFood()
    End Sub

    Private Sub AddFoodItem(ByVal FoodID As String, ByVal FoodName As String, ByVal Status As String)
        Dim _Item As New FoodControl(FoodID, FoodName, Status)
        _Item.Dock = DockStyle.Top
        _Item.BringToFront()
        sc.Controls.Add(_Item)
    End Sub

    Private Sub SaveFood()

        If Not Me.SelectedChild.Populated Then Exit Sub

        If m_SelectedMealID = "" Then Exit Sub
        If m_SelectedMealType = "" Then Exit Sub
        If m_SelectedMealName = "" Then Exit Sub

        For Each _i As FoodControl In sc.Controls

            LogFood(Parameters.TodayID, SelectedChild.ID, SelectedChild.Name,
                    m_SelectedMealID, m_SelectedMealType, m_SelectedMealName,
                    _i.FoodID, _i.FoodName, _i.FoodStatus)

        Next

    End Sub

    Private Sub PopulateItems()

        sc.Controls.Clear()
        If m_SelectedMealID = "" Then Exit Sub

        'check if we have generated the FoodRegister yet
        If FoodRegisterGenerated() Then

            Dim _SQL As String = ""
            _SQL += "select food_id, food_name, food_status from FoodRegister"
            _SQL += " where day_id = '" & TodayID & "'"
            _SQL += " and child_id = '" & SelectedChild.ID & "'"
            _SQL += " and meal_id = '" & m_SelectedMealID & "'"
            _SQL += " and meal_type = '" & m_SelectedMealType & "'"
            _SQL += " and meal_id <> food_id"
            _SQL += " order by food_name desc"

            Dim _dt As DataTable = DAL.ReturnDataTable(_SQL)
            If _dt IsNot Nothing Then

                If _dt.Rows.Count > 0 Then
                    For Each _DR As DataRow In _dt.Rows
                        AddFoodItem(_DR.Item("food_id").ToString, _DR.Item("food_name").ToString, _DR.Item("food_status").ToString)
                    Next
                End If

                _dt.Dispose()
                _dt = Nothing

            End If

        Else
            GenerateFoodItems()
        End If

    End Sub

    Private Function FoodRegisterGenerated() As Boolean

        Dim _SQL As String = ""
        _SQL += "select count(*) as 'count' from FoodRegister"
        _SQL += " where day_id = '" & TodayID & "'"
        _SQL += " and child_id = '" & SelectedChild.ID & "'"
        _SQL += " and meal_id = '" & m_SelectedMealID & "'"
        _SQL += " and meal_type = '" & m_SelectedMealType & "'"

        Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
        If _DR IsNot Nothing Then
            If CInt(_DR.Item("count")) > 0 Then
                Return True
            End If
            _DR = Nothing
        End If

        Return False

    End Function

    Private Sub GenerateFoodItems()

        Dim _SQL As String = ""
        _SQL = "select food_id, food_name from MealComponents"
        _SQL += " where meal_id = '" & m_SelectedMealID & "'"
        _SQL += " order by food_name desc"

        Dim _dt As DataTable = DAL.ReturnDataTable(_SQL)
        If _dt IsNot Nothing Then

            If _dt.Rows.Count > 0 Then
                For Each _DR As DataRow In _dt.Rows
                    AddFoodItem(_DR.Item("food_id").ToString, _DR.Item("food_name").ToString, "X")
                Next
            End If

            _dt.Dispose()
            _dt = Nothing

        End If

    End Sub

    Private Sub SetupMeals()

        Dim _SQL As String = "select * from Day where id = '" & TodayID & "'"

        Dim _dr As DataRow = DAL.ReturnDataRow(_SQL)
        If _dr IsNot Nothing Then

            Dim _BreakfastID As String = _dr.Item("breakfast_id").ToString
            If _BreakfastID <> "" Then
                btnBreakfast.Enabled = True
                btnBreakfast.Tag = _BreakfastID
                btnBreakfast.AccessibleDescription = _dr.Item("breakfast_name").ToString
            Else
                btnBreakfast.Enabled = False
                btnBreakfast.Tag = ""
                btnBreakfast.AccessibleDescription = ""
            End If

            Dim _SnackID As String = _dr.Item("snack_id").ToString
            If _SnackID <> "" Then
                btnAM.Enabled = True
                btnAM.Tag = _SnackID
                btnAM.AccessibleDescription = _dr.Item("snack_name").ToString
            Else
                btnAM.Enabled = False
                btnAM.Tag = ""
                btnAM.AccessibleDescription = ""
            End If

            Dim _LunchID As String = _dr.Item("lunch_id").ToString
            If _LunchID <> "" Then
                btnLunch.Enabled = True
                btnLunch.Tag = _LunchID
                btnLunch.AccessibleDescription = _dr.Item("lunch_name").ToString
            Else
                btnLunch.Enabled = False
                btnLunch.Tag = ""
                btnLunch.AccessibleDescription = ""
            End If

            Dim _LunchDessertID As String = _dr.Item("lunch_des_id").ToString
            If _LunchDessertID <> "" Then
                btnLunchDessert.Enabled = True
                btnLunchDessert.Tag = _LunchDessertID
                btnLunchDessert.AccessibleDescription = _dr.Item("lunch_des_name").ToString
            Else
                btnLunchDessert.Enabled = False
                btnLunchDessert.Tag = ""
                btnLunchDessert.AccessibleDescription = ""
            End If

            Dim _SnackPMID As String = _dr.Item("snack_pm_id").ToString
            If _SnackPMID <> "" Then
                btnPM.Enabled = True
                btnPM.Tag = _SnackPMID
                btnPM.AccessibleDescription = _dr.Item("snack_pm_name").ToString
            Else
                btnPM.Enabled = False
                btnPM.Tag = ""
                btnPM.AccessibleDescription = ""
            End If

            Dim _TeaID As String = _dr.Item("tea_id").ToString
            If _TeaID <> "" Then
                btnTea.Enabled = True
                btnTea.Tag = _TeaID
                btnTea.AccessibleDescription = _dr.Item("tea_name").ToString
            Else
                btnTea.Enabled = False
                btnTea.Tag = ""
                btnTea.AccessibleDescription = ""
            End If

            Dim _TeaDessertID As String = _dr.Item("tea_des_id").ToString
            If _TeaDessertID <> "" Then
                btnTeaDessert.Enabled = True
                btnTeaDessert.Tag = _TeaDessertID
                btnTeaDessert.AccessibleDescription = _dr.Item("tea_des_name").ToString
            Else
                btnTeaDessert.Enabled = False
                btnTeaDessert.Tag = ""
                btnTeaDessert.AccessibleDescription = ""
            End If

        End If

        _dr = Nothing

    End Sub

    Private Sub btnBreakfast_Click(sender As Object, e As EventArgs) Handles btnBreakfast.Click
        SetMeal("B")
    End Sub

    Private Sub btnAM_Click(sender As Object, e As EventArgs) Handles btnAM.Click
        SetMeal("S")
    End Sub

    Private Sub btnLunch_Click(sender As Object, e As EventArgs) Handles btnLunch.Click
        SetMeal("L")
    End Sub

    Private Sub btnLunchDessert_Click(sender As Object, e As EventArgs) Handles btnLunchDessert.Click
        SetMeal("LD")
    End Sub

    Private Sub btnPM_Click(sender As Object, e As EventArgs) Handles btnPM.Click
        SetMeal("SP")
    End Sub

    Private Sub btnTea_Click(sender As Object, e As EventArgs) Handles btnTea.Click
        SetMeal("T")
    End Sub

    Private Sub btnTeaDessert_Click(sender As Object, e As EventArgs) Handles btnTeaDessert.Click
        SetMeal("TD")
    End Sub

    Private Sub SetMeal(ByVal MealType As String)

        If SelectedChild.Populated Then
            SaveFood()
        End If

        m_SelectedMealType = MealType
        ResetMealButtons()

        Select Case MealType

            Case "B"
                m_SelectedMealID = btnBreakfast.Tag.ToString
                m_SelectedMealName = btnBreakfast.AccessibleDescription
                btnBreakfast.Appearance.BackColor = txtYellow.BackColor

            Case "S"
                m_SelectedMealID = btnAM.Tag.ToString
                m_SelectedMealName = btnAM.AccessibleDescription
                btnAM.Appearance.BackColor = txtYellow.BackColor

            Case "L"
                m_SelectedMealID = btnLunch.Tag.ToString
                m_SelectedMealName = btnLunch.AccessibleDescription
                btnLunch.Appearance.BackColor = txtYellow.BackColor

            Case "LD"
                m_SelectedMealID = btnLunchDessert.Tag.ToString
                m_SelectedMealName = btnLunchDessert.AccessibleDescription
                btnLunchDessert.Appearance.BackColor = txtYellow.BackColor

            Case "SP"
                m_SelectedMealID = btnPM.Tag.ToString
                m_SelectedMealName = btnPM.AccessibleDescription
                btnPM.Appearance.BackColor = txtYellow.BackColor

            Case "T"
                m_SelectedMealID = btnTea.Tag.ToString
                m_SelectedMealName = btnTea.AccessibleDescription
                btnTea.Appearance.BackColor = txtYellow.BackColor

            Case "TD"
                m_SelectedMealID = btnTeaDessert.Tag.ToString
                m_SelectedMealName = btnTeaDessert.AccessibleDescription
                btnTeaDessert.Appearance.BackColor = txtYellow.BackColor

        End Select

        If SelectedChild.Populated Then
            PopulateItems()
        End If

    End Sub

    Private Sub ResetMealButtons()
        btnBreakfast.Appearance.Reset()
        btnAM.Appearance.Reset()
        btnLunch.Appearance.Reset()
        btnLunchDessert.Appearance.Reset()
        btnPM.Appearance.Reset()
        btnTea.Appearance.Reset()
        btnTeaDessert.Appearance.Reset()
    End Sub

    Private Sub btnAddFood_Click(sender As Object, e As EventArgs) Handles btnAddFood.Click
        Dim _P As Pair = ChooseFoodItem()
        If _P IsNot Nothing Then
            AddFoodItem(_P.Code, _P.Text, "3")
        End If
    End Sub
End Class
