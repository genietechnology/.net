﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class HostedToilet
    Inherits NurseryTouchscreen.BaseHostedPanel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(HostedToilet))
        Me.panTop = New DevExpress.XtraEditors.PanelControl()
        Me.btnAccident = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCream = New DevExpress.XtraEditors.SimpleButton()
        Me.btnToilet = New DevExpress.XtraEditors.SimpleButton()
        Me.btnPotty = New DevExpress.XtraEditors.SimpleButton()
        Me.btnNappy = New DevExpress.XtraEditors.SimpleButton()
        Me.btnLoose = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSoiled = New DevExpress.XtraEditors.SimpleButton()
        Me.btnWet = New DevExpress.XtraEditors.SimpleButton()
        Me.btnDry = New DevExpress.XtraEditors.SimpleButton()
        Me.sc = New DevExpress.XtraEditors.XtraScrollableControl()
        CType(Me.panTop, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panTop.SuspendLayout()
        Me.SuspendLayout()
        '
        'panTop
        '
        Me.panTop.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.panTop.Controls.Add(Me.btnAccident)
        Me.panTop.Controls.Add(Me.btnCream)
        Me.panTop.Controls.Add(Me.btnToilet)
        Me.panTop.Controls.Add(Me.btnPotty)
        Me.panTop.Controls.Add(Me.btnNappy)
        Me.panTop.Controls.Add(Me.btnLoose)
        Me.panTop.Controls.Add(Me.btnSoiled)
        Me.panTop.Controls.Add(Me.btnWet)
        Me.panTop.Controls.Add(Me.btnDry)
        Me.panTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.panTop.Location = New System.Drawing.Point(0, 0)
        Me.panTop.Name = "panTop"
        Me.panTop.Size = New System.Drawing.Size(432, 59)
        Me.panTop.TabIndex = 1
        '
        'btnAccident
        '
        Me.btnAccident.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnAccident.Location = New System.Drawing.Point(89, 32)
        Me.btnAccident.Name = "btnAccident"
        Me.btnAccident.Size = New System.Drawing.Size(80, 23)
        Me.btnAccident.TabIndex = 31
        Me.btnAccident.Text = "Accident"
        '
        'btnCream
        '
        Me.btnCream.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnCream.Location = New System.Drawing.Point(175, 17)
        Me.btnCream.Name = "btnCream"
        Me.btnCream.Size = New System.Drawing.Size(80, 23)
        Me.btnCream.TabIndex = 30
        Me.btnCream.Text = "Cream"
        '
        'btnToilet
        '
        Me.btnToilet.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnToilet.Location = New System.Drawing.Point(89, 3)
        Me.btnToilet.Name = "btnToilet"
        Me.btnToilet.Size = New System.Drawing.Size(80, 23)
        Me.btnToilet.TabIndex = 29
        Me.btnToilet.Text = "Toilet"
        '
        'btnPotty
        '
        Me.btnPotty.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnPotty.Location = New System.Drawing.Point(3, 32)
        Me.btnPotty.Name = "btnPotty"
        Me.btnPotty.Size = New System.Drawing.Size(80, 23)
        Me.btnPotty.TabIndex = 28
        Me.btnPotty.Text = "Potty"
        '
        'btnNappy
        '
        Me.btnNappy.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnNappy.Location = New System.Drawing.Point(3, 3)
        Me.btnNappy.Name = "btnNappy"
        Me.btnNappy.Size = New System.Drawing.Size(80, 23)
        Me.btnNappy.TabIndex = 26
        Me.btnNappy.Text = "Nappy"
        '
        'btnLoose
        '
        Me.btnLoose.Appearance.Options.UseTextOptions = True
        Me.btnLoose.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnLoose.Image = CType(resources.GetObject("btnLoose.Image"), System.Drawing.Image)
        Me.btnLoose.Location = New System.Drawing.Point(347, 32)
        Me.btnLoose.Name = "btnLoose"
        Me.btnLoose.Size = New System.Drawing.Size(80, 23)
        Me.btnLoose.TabIndex = 25
        Me.btnLoose.Text = "Loose"
        '
        'btnSoiled
        '
        Me.btnSoiled.Appearance.Options.UseTextOptions = True
        Me.btnSoiled.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnSoiled.Image = CType(resources.GetObject("btnSoiled.Image"), System.Drawing.Image)
        Me.btnSoiled.Location = New System.Drawing.Point(347, 3)
        Me.btnSoiled.Name = "btnSoiled"
        Me.btnSoiled.Size = New System.Drawing.Size(80, 23)
        Me.btnSoiled.TabIndex = 24
        Me.btnSoiled.Text = "Soiled"
        '
        'btnWet
        '
        Me.btnWet.Appearance.Options.UseTextOptions = True
        Me.btnWet.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnWet.Image = CType(resources.GetObject("btnWet.Image"), System.Drawing.Image)
        Me.btnWet.Location = New System.Drawing.Point(261, 32)
        Me.btnWet.Name = "btnWet"
        Me.btnWet.Size = New System.Drawing.Size(80, 23)
        Me.btnWet.TabIndex = 23
        Me.btnWet.Text = "Wet"
        '
        'btnDry
        '
        Me.btnDry.Appearance.Options.UseTextOptions = True
        Me.btnDry.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnDry.Image = CType(resources.GetObject("btnDry.Image"), System.Drawing.Image)
        Me.btnDry.Location = New System.Drawing.Point(261, 3)
        Me.btnDry.Name = "btnDry"
        Me.btnDry.Size = New System.Drawing.Size(80, 23)
        Me.btnDry.TabIndex = 22
        Me.btnDry.Text = "Dry"
        '
        'sc
        '
        Me.sc.AllowTouchScroll = True
        Me.sc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.sc.Location = New System.Drawing.Point(0, 59)
        Me.sc.Name = "sc"
        Me.sc.Size = New System.Drawing.Size(432, 68)
        Me.sc.TabIndex = 96
        '
        'HostedToilet
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.Controls.Add(Me.sc)
        Me.Controls.Add(Me.panTop)
        Me.Name = "HostedToilet"
        Me.Size = New System.Drawing.Size(432, 127)
        CType(Me.panTop, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panTop.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents panTop As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnToilet As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnPotty As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnNappy As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnLoose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSoiled As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnWet As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnDry As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sc As DevExpress.XtraEditors.XtraScrollableControl
    Friend WithEvents btnCream As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAccident As DevExpress.XtraEditors.SimpleButton
End Class
