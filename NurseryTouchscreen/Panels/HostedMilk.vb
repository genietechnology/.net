﻿
Public Class HostedMilk

    Private Sub HostedMilk_ChildChanged(sender As Object, e As RecordChangedArgs) Handles Me.ChildChanged
        DisplayMilk(e.ValueID)
    End Sub

    Private Sub DisplayMilk(ByVal ChildID As String)

        sc.Controls.Clear()

        Dim _SQL As String = ""

        _SQL += "select ID, description, value_1, value_2, value_3, stamp"
        _SQL += " from Activity"
        _SQL += " where day_id = '" + Parameters.TodayID + "'"
        _SQL += " and key_id = '" + ChildID + "'"
        _SQL += " and type = 'MILK'"
        _SQL += " order by stamp"

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _ID As String = _DR.Item("ID").ToString
                Dim _Desc As String = _DR.Item("description").ToString
                Dim _Value1 As String = _DR.Item("value_1").ToString
                Dim _Value2 As String = _DR.Item("value_2").ToString
                Dim _Value3 As String = _DR.Item("value_3").ToString
                Dim _Stamp As Date? = _DR.Item("stamp")

                Dim _Item As New ItemControl(_ID, _Desc, True, True)
                With _Item
                    .ChildID = ChildID
                    .Value1 = _Value1
                    .Value2 = _Value2
                    .Value3 = _Value3
                    .Stamp = _Stamp
                    .Dock = DockStyle.Top
                    .BringToFront()
                End With

                AddHandler _Item.TimeClick, AddressOf TimeClick
                AddHandler _Item.DeleteClick, AddressOf DeleteClick

                sc.Controls.Add(_Item)

            Next

            _DT.Dispose()
            _DT = Nothing

        End If

    End Sub

    Private Sub TimeClick(sender As Object, e As EventArgs)

        Dim _i As ItemControl = sender
        If _i IsNot Nothing Then

            'typical transaction is:
            '4 floz @ 13:23:44

            'so the time is always after @
            Dim _Desc As String = _i.ItemText
            Dim _Left As String = _Desc.Substring(0, _Desc.IndexOf("@") + 1)

            Dim _NewDesc As String = _Left + " " + _i.Time
            Dim _NewTime As Date = Today + " " + _i.Time

            SharedModule.UpdateActivity(_i.ActivtyID, _NewDesc, _NewTime, Value3:=_NewTime, Notes:="AMENDED")
            DisplayMilk(_i.ChildID)

        End If

    End Sub

    Private Sub DeleteClick(sender As Object, e As EventArgs)

        If Msgbox("Are you sure you want to delete this Milk Record?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Confirm Delete") = DialogResult.Yes Then
            Dim _I As ItemControl = CType(sender, ItemControl)
            If _I IsNot Nothing Then
                If _I.ActivtyID <> "" Then
                    SharedModule.DeleteActivity(_I.ActivtyID)
                    DisplayMilk(_I.ChildID)
                End If
            End If
        End If

    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click

        Dim _Time As String = SharedModule.TimeEntry("Please enter the time your started the feed.")
        Dim _Qty As String = SharedModule.NumberEntry("Enter the fl.oz")

        If _Time <> "" AndAlso _Qty <> "" Then
            Dim _DoubleQty As Double = CDbl(_Qty)
            SharedModule.LogMilk(SharedModule.TodayID, SelectedChild.ID, SelectedChild.Name, _DoubleQty, _Time, SelectedStaff.StaffID, SelectedStaff.StaffName)
            DisplayMilk(SelectedChild.ID)
        End If

    End Sub

End Class
