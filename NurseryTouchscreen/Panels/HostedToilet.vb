﻿Imports NurseryTouchscreen.SharedModule

Public Class HostedToilet

    Private m_Cream As Boolean = False
    Private m_Accident As Boolean = False
    Private m_Method As EnumMethod = EnumMethod.Nappy

    Private Sub HostedToilet_ChildChanged(sender As Object, e As RecordChangedArgs) Handles Me.ChildChanged
        DisplayToilet(e.ValueID)
    End Sub

    Private Enum EnumMethod
        Toilet
        Nappy
        Potty
        Accident
    End Enum

    Private Enum EnumValue
        Dry
        Wet
        Soiled
        Loose
    End Enum

    Private Sub SetMethod(Method As EnumMethod)

        btnNappy.Appearance.Reset()
        btnToilet.Appearance.Reset()
        btnPotty.Appearance.Reset()
        btnAccident.Appearance.Reset()

        If Method = EnumMethod.Nappy Then btnNappy.Appearance.BackColor = Color.Yellow
        If Method = EnumMethod.Toilet Then btnToilet.Appearance.BackColor = Color.Yellow
        If Method = EnumMethod.Potty Then btnPotty.Appearance.BackColor = Color.Yellow
        If Method = EnumMethod.Accident Then btnAccident.Appearance.BackColor = Color.Yellow

        m_Method = Method

    End Sub

    Private Sub SetCream()
        If m_Cream Then
            btnCream.Appearance.BackColor = Color.Yellow
        Else
            btnCream.Appearance.Reset()
        End If
    End Sub

    Private Sub SetValue(Value As EnumValue)

        Dim _Now As Date = Now
        Dim _HHMM As String = Format(_Now, "HH:mm").ToString

        Dim _CreamIndicator As String = ""
        Dim _CreamDesc As String = ""

        If m_Cream Then
            _CreamDesc = ", Cream Applied"
            _CreamIndicator = "CREAM"
        End If

        Dim _ToiletType As String = ReturnToiletType(m_Method.ToString)
        Dim _ToiletValue As String = ReturnToiletValue(Value.ToString)

        Dim _Desc As String = Value.ToString + " " + m_Method.ToString + _CreamDesc + " @ " + _HHMM + " by " + SelectedStaff.StaffName

        SharedModule.LogActivity(Parameters.TodayID, SelectedChild.ID, SelectedChild.Name, EnumActivityType.Toilet, _Desc, _ToiletValue, _ToiletType, _CreamIndicator, SelectedStaff.StaffID, SelectedStaff.StaffName)
        DisplayToilet(SelectedChild.ID)

    End Sub

    Private Function ReturnToiletType(ByVal TypeIn As String) As String
        If TypeIn = "Nappy" Then Return "NAP"
        If TypeIn = "Potty" Then Return "POTTY"
        If TypeIn = "Accident" Then Return "ACC"
        Return ""
    End Function

    Private Function ReturnToiletValue(ByVal ValueIn As String) As String
        If ValueIn = "Dry" Then Return "DRY"
        If ValueIn = "Wet" Then Return "WET"
        If ValueIn = "Soiled" Then Return "SOIL"
        If ValueIn = "Loose" Then Return "LOOSE"
        Return ""
    End Function

    Private Sub DisplayToilet(ByVal ChildID As String)

        sc.Controls.Clear()

        SetCream()
        SetMethod(EnumMethod.Toilet)

        Dim _Child As New ChildProperties
        _Child.Retreive(ChildID)

        If _Child.Populated Then

            If _Child.Nappies Then SetMethod(EnumMethod.Nappy)

            Dim _SQL As String = ""

            _SQL += "select ID, description, value_1, value_2, value_3, staff_name, stamp"
            _SQL += " from Activity"
            _SQL += " where day_id = '" + Parameters.TodayID + "'"
            _SQL += " and key_id = '" + ChildID + "'"
            _SQL += " and type = 'TOILET'"
            _SQL += " order by stamp"

            Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
            If _DT IsNot Nothing Then

                For Each _DR As DataRow In _DT.Rows

                    Dim _ID As String = _DR.Item("ID").ToString
                    Dim _Desc As String = _DR.Item("description").ToString
                    Dim _Value1 As String = _DR.Item("value_1").ToString
                    Dim _Value2 As String = _DR.Item("value_2").ToString
                    Dim _Value3 As String = _DR.Item("value_3").ToString
                    Dim _Stamp As Date? = _DR.Item("stamp")
                    Dim _StaffName As String = _DR.Item("staff_name").ToString

                    Dim _Item As New ItemControl(_ID, _Desc, True, True)
                    With _Item
                        .ChildID = ChildID
                        .Value1 = _Value1
                        .Value2 = _Value2
                        .Value3 = _Value3
                        .Stamp = _Stamp
                        .Dock = DockStyle.Top
                        .BringToFront()
                    End With

                    AddHandler _Item.TimeClick, AddressOf TimeClick
                    AddHandler _Item.DeleteClick, AddressOf DeleteClick

                    sc.Controls.Add(_Item)

                Next

                _DT.Dispose()
                _DT = Nothing

            End If

        End If

    End Sub

    Private Sub TimeClick(sender As Object, e As EventArgs)

        Dim _i As ItemControl = sender
        If _i IsNot Nothing Then

            'typical transaction is:
            'Soiled Nappy, Cream Applied @ 19:12 by George Clooney

            'so the time is always between @ and by
            Dim _Desc As String = _i.ItemText
            Dim _Left As String = _Desc.Substring(0, _Desc.IndexOf("@") + 1)
            Dim _Right As String = _Desc.Substring(_Desc.IndexOf(" by "))

            Dim _NewDesc As String = _Left + " " + _i.Time + _Right
            Dim _NewTime As Date = Today + " " + _i.Time

            UpdateActivity(_i.ActivtyID, _NewDesc, _NewTime, Notes:="AMENDED")
            DisplayToilet(_i.ChildID)

        End If

    End Sub

    Private Sub DeleteClick(sender As Object, e As EventArgs)

        If Msgbox("Are you sure you want to delete this Toilet Record?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Confirm Delete") = DialogResult.Yes Then
            Dim _I As ItemControl = CType(sender, ItemControl)
            If _I IsNot Nothing Then
                If _I.ActivtyID <> "" Then
                    SharedModule.DeleteActivity(_I.ActivtyID)
                    DisplayToilet(_I.ChildID)
                End If
            End If
        End If

    End Sub

    Private Sub btnDry_Click(sender As Object, e As EventArgs) Handles btnDry.Click
        SetValue(EnumValue.Dry)
    End Sub

    Private Sub btnWet_Click(sender As Object, e As EventArgs) Handles btnWet.Click
        SetValue(EnumValue.Wet)
    End Sub

    Private Sub btnLoose_Click(sender As Object, e As EventArgs) Handles btnLoose.Click
        SetValue(EnumValue.Loose)
    End Sub

    Private Sub btnSoiled_Click(sender As Object, e As EventArgs) Handles btnSoiled.Click
        SetValue(EnumValue.Soiled)
    End Sub

    Private Sub btnCream_Click(sender As Object, e As EventArgs) Handles btnCream.Click
        If m_Cream Then
            m_Cream = False
        Else
            m_Cream = True
        End If
        SetCream()
    End Sub

    Private Sub btnPotty_Click(sender As Object, e As EventArgs) Handles btnPotty.Click
        SetMethod(EnumMethod.Potty)
    End Sub

    Private Sub btnNappy_Click(sender As Object, e As EventArgs) Handles btnNappy.Click
        SetMethod(EnumMethod.Nappy)
    End Sub

    Private Sub btnToilet_Click(sender As Object, e As EventArgs) Handles btnToilet.Click
        SetMethod(EnumMethod.Toilet)
    End Sub

    Private Sub btnAccident_Click(sender As Object, e As EventArgs) Handles btnAccident.Click
        SetMethod(EnumMethod.Accident)
    End Sub
End Class
