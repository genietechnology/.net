﻿Imports NurseryTouchscreen.SharedModule

Public Class FoodControl

    Private m_FoodID As String
    Private m_FoodName As String
    Private m_FoodStatus As String

    Private m_Red As Color
    Private m_Orange As Color
    Private m_Yellow As Color
    Private m_LightGreen As Color
    Private m_Green As Color

    Private m_StrikeOut As Font

    Public Sub New(ByVal FoodID As String, ByVal FoodName As String, ByVal Status As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        m_StrikeOut = New Font(lblItem.Font.Name, lblItem.Font.Size, FontStyle.Strikeout)

        m_Red = btnNone.Appearance.BackColor
        m_Orange = btnSome.Appearance.BackColor
        m_Yellow = btnHalf.Appearance.BackColor
        m_LightGreen = btnMost.Appearance.BackColor
        m_Green = btnAll.Appearance.BackColor

        m_FoodID = FoodID
        m_FoodName = FoodName
        m_FoodStatus = Status

        If Parameters.UseBasicMeals Then
            btnDelete.Hide()
            btnSwap.Hide()
        End If

        lblItem.Text = FoodName
        SetColour()

    End Sub

    Public ReadOnly Property FoodID As String
        Get
            Return m_FoodID
        End Get
    End Property

    Public ReadOnly Property FoodName As String
        Get
            Return m_FoodName
        End Get
    End Property

    Public ReadOnly Property FoodStatus As String
        Get
            Return m_FoodStatus
        End Get
    End Property

    Private Sub SetColour()

        lblItem.Font = Nothing
        btnNone.Appearance.Reset()
        btnSome.Appearance.Reset()
        btnHalf.Appearance.Reset()
        btnMost.Appearance.Reset()
        btnAll.Appearance.Reset()

        If m_FoodStatus = "-2" Then lblItem.Font = m_StrikeOut
        If m_FoodStatus = "0" Then btnNone.Appearance.BackColor = m_Red
        If m_FoodStatus = "1" Then btnSome.Appearance.BackColor = m_Orange
        If m_FoodStatus = "2" Then btnMost.Appearance.BackColor = m_LightGreen
        If m_FoodStatus = "3" Then btnAll.Appearance.BackColor = m_Green
        If m_FoodStatus = "4" Then btnHalf.Appearance.BackColor = m_Yellow

    End Sub

    Private Sub btnNone_Click(sender As Object, e As EventArgs) Handles btnNone.Click
        m_FoodStatus = 0
        SetColour()
    End Sub

    Private Sub btnSome_Click(sender As Object, e As EventArgs) Handles btnSome.Click
        m_FoodStatus = 1
        SetColour()
    End Sub

    Private Sub btnHalf_Click(sender As Object, e As EventArgs) Handles btnHalf.Click
        m_FoodStatus = 4
        SetColour()
    End Sub

    Private Sub btnMost_Click(sender As Object, e As EventArgs) Handles btnMost.Click
        m_FoodStatus = 2
        SetColour()
    End Sub

    Private Sub btnAll_Click(sender As Object, e As EventArgs) Handles btnAll.Click
        m_FoodStatus = 3
        SetColour()
    End Sub

    Private Sub btnSwap_Click(sender As Object, e As EventArgs) Handles btnSwap.Click
        Dim _P As Pair = ChooseFoodItem()
        If _P IsNot Nothing Then
            m_FoodID = _P.Code
            m_FoodName = _P.Text
            lblItem.Text = _P.Text
            m_FoodStatus = 3
            SetColour()
        End If
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If Msgbox("Are you sure you want to Remove this Food Item?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, "Confirm Remove") = DialogResult.Yes Then
            lblItem.Font = m_StrikeOut
            m_FoodStatus = -2
            SetColour()
        End If
    End Sub
End Class
