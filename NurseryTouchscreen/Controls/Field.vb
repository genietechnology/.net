﻿Imports System.ComponentModel
Imports NurseryTouchscreen.SharedModule

<DefaultEvent("ButtonClick")>
Public Class Field

    Public Enum EnumButtonType
        Enter
        Lookup
        SignatureStaff
        SignatureParent
        SignatureVisitor
        Time
        DateTime
        Number
        DecimalNumber
    End Enum

    Private m_Signature As New Signature
    Private m_LabelWidth As Single
    Private m_ButtonWidth As Single
    Private m_ButtonType As EnumButtonType
    Private m_DateTime As Date? = Nothing

    Public Event ButtonClick(sender As Object, e As EventArgs)
    Public Event AfterButtonClick(sender As Object, e As EventArgs)

#Region "Properties"

    <Category("_Label")>
    Public Property LabelText As String
        Get
            Return lblLabel.Text
        End Get
        Set(value As String)
            lblLabel.Text = value
            SetLayout()
            If HideText Then lblText.Hide()
        End Set
    End Property

    <Category("_Label")>
    Public Property LabelWidth As Single
        Get
            Return tlp.ColumnStyles(0).Width
        End Get
        Set(value As Single)
            m_LabelWidth = value
            SetLayout()
        End Set
    End Property

    <Category("_Button")>
    Public Property ButtonCustom As Boolean

    <Category("_Button")>
    Public Property ButtonSQL As String

    <Category("_Button")>
    Public Property QuickTextList As String

    <Category("_Button")>
    Public Property ButtonText As String
        Get
            Return btnButton.Text
        End Get
        Set(value As String)
            'btnButton.Text = value
        End Set
    End Property

    <Category("_Button")>
    Public Property ButtonWidth As Single
        Get
            Return tlp.ColumnStyles(2).Width
        End Get
        Set(value As Single)
            m_ButtonWidth = value
            SetLayout()
        End Set
    End Property

    <Category("_Button")>
    Public Property ButtonType As EnumButtonType
        Get
            Return m_ButtonType
        End Get
        Set(value As EnumButtonType)
            m_ButtonType = value
            SetIcon()
        End Set
    End Property

    <Category("_Values")>
    Public Property HideText As Boolean

    <Category("_Values")>
    Public Property ValueID As String

    <Category("_Values")>
    Public Property ValueText As String
        Get
            Return lblText.Text
        End Get
        Set(value As String)
            lblText.Text = value
        End Set
    End Property

    <Category("_Values")>
    Public Property ValueDateTime As Date?
        Get
            Return m_DateTime
        End Get
        Set(value As Date?)
            m_DateTime = value
        End Set
    End Property

    <Category("_Values")>
    Public ReadOnly Property SignatureCapture As Signature
        Get
            Return m_Signature
        End Get
    End Property

    <Category("_Values")>
    Public Property ValueMaxLength As Integer

    <Category("_Parameters")>
    Public Property FamilyID As String

#End Region

    Public Sub Clear()
        Me.ValueID = ""
        Me.ValueText = ""
    End Sub

    Protected Overrides Sub OnEnabledChanged(e As EventArgs)
        MyBase.OnEnabledChanged(e)
        lblLabel.Enabled = True
        lblText.Enabled = True
        btnButton.Enabled = MyBase.Enabled
    End Sub

    Private Sub Field_Load(sender As Object, e As EventArgs) Handles Me.Load
        SetLayout()
    End Sub

    Private Sub SetLayout()
        Dim _TextWidth As Single = Me.Width - (m_LabelWidth + m_ButtonWidth)
        tlp.ColumnStyles(0).SizeType = SizeType.Absolute
        tlp.ColumnStyles(0).Width = m_LabelWidth
        tlp.ColumnStyles(1).SizeType = SizeType.Absolute
        tlp.ColumnStyles(1).Width = _TextWidth
        tlp.ColumnStyles(2).SizeType = SizeType.Absolute
        tlp.ColumnStyles(2).Width = m_ButtonWidth
        SetIcon()
    End Sub

    Private Sub SetIcon()

        btnButton.Image = Nothing
        Select Case m_ButtonType

            Case EnumButtonType.Enter
                btnButton.Image = My.Resources.ResourceManager.GetObject("Pencil_32")

            Case EnumButtonType.Lookup
                btnButton.Image = My.Resources.ResourceManager.GetObject("glossary_32")

            Case EnumButtonType.SignatureParent, EnumButtonType.SignatureStaff, EnumButtonType.SignatureVisitor
                btnButton.Image = My.Resources.ResourceManager.GetObject("pen_32")

            Case EnumButtonType.DateTime, EnumButtonType.Time
                btnButton.Image = My.Resources.ResourceManager.GetObject("alarm_32")

            Case EnumButtonType.Number, EnumButtonType.DecimalNumber
                btnButton.Image = My.Resources.ResourceManager.GetObject("calculator_32")

        End Select

    End Sub

    Private Sub btnButton_Click(sender As Object, e As EventArgs) Handles btnButton.Click

        If ButtonCustom Then
            RaiseEvent ButtonClick(sender, e)
        Else

            Select Case m_ButtonType

                Case EnumButtonType.Enter
                    lblText.Text = SharedModule.OSK(ApplicationListName:=QuickTextList, DefaultValue:=lblText.Text, MaxLength:=Me.ValueMaxLength)

                Case EnumButtonType.Lookup
                    Dim _P As SharedModule.Pair = SharedModule.ReturnButtonSQL(Me.ButtonSQL, "Select " + Me.LabelText)
                    If _P IsNot Nothing Then
                        Me.ValueID = _P.Code
                        Me.ValueText = _P.Text
                    End If

                Case EnumButtonType.SignatureParent

                    If Me.FamilyID = "" Then
                        Dim _P As Pair = Business.Child.FindChildByGroup(SharedModule.Enums.PersonMode.OnlyCheckedIn)
                        If _P IsNot Nothing Then
                            Me.FamilyID = Business.Child.ReturnFamilyID(_P.Code)
                        End If
                    End If

                    If Me.FamilyID <> "" Then

                        m_Signature.CaptureParentSignature(True, Me.FamilyID)

                        If m_Signature.SignatureCaptured Then
                            Me.ValueID = m_Signature.PersonID.ToString
                            Me.ValueText = m_Signature.PersonName
                        End If

                    End If

                Case EnumButtonType.SignatureStaff

                    m_Signature.CaptureStaffSignature(True)

                    If m_Signature.SignatureCaptured Then
                        Me.ValueID = m_Signature.PersonID.ToString
                        Me.ValueText = m_Signature.PersonName
                    End If

                Case EnumButtonType.SignatureVisitor

                Case EnumButtonType.Time
                    lblText.Text = SharedModule.TimeEntry
                    If lblText.Text = "" Then
                        m_DateTime = Nothing
                    Else
                        m_DateTime = Today + " " + Format(CDate(lblText.Text), "HH:mm")
                    End If

                Case EnumButtonType.DateTime
                    Dim _DateTime As String = SharedModule.DateEntry
                    If _DateTime = "" Then
                        m_DateTime = Nothing
                    Else
                        Dim _Time As String = SharedModule.TimeEntry
                        If _Time <> "" Then
                            m_DateTime = CDate(_DateTime) + " " + Format(CDate(_Time), "HH:mm")
                            _DateTime = Format(m_DateTime, "ddd dd MMM yyyy HH:mm")
                            lblText.Text = _DateTime
                        End If
                    End If

                Case EnumButtonType.Number
                    lblText.Text = NumberEntry("Enter Value")

                Case EnumButtonType.DecimalNumber
                    Dim _Text As String = NumberEntry("Enter Value", "", True)
                    If _Text = "" Then
                        lblText.Text = "0.00"
                    Else
                        lblText.Text = Format(CDec(_Text), "0.00").ToString
                    End If

            End Select

        End If

        If HideText Then lblText.Hide()
        RaiseEvent AfterButtonClick(sender, e)

    End Sub

    Private Sub Field_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        SetLayout()
    End Sub

End Class
