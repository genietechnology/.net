﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Field
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tlp = New System.Windows.Forms.TableLayoutPanel()
        Me.lblText = New DevExpress.XtraEditors.LabelControl()
        Me.lblLabel = New DevExpress.XtraEditors.LabelControl()
        Me.btnButton = New DevExpress.XtraEditors.SimpleButton()
        Me.tlp.SuspendLayout()
        Me.SuspendLayout()
        '
        'tlp
        '
        Me.tlp.ColumnCount = 3
        Me.tlp.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150.0!))
        Me.tlp.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200.0!))
        Me.tlp.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50.0!))
        Me.tlp.Controls.Add(Me.lblText, 0, 0)
        Me.tlp.Controls.Add(Me.lblLabel, 0, 0)
        Me.tlp.Controls.Add(Me.btnButton, 2, 0)
        Me.tlp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp.Location = New System.Drawing.Point(0, 0)
        Me.tlp.Name = "tlp"
        Me.tlp.RowCount = 1
        Me.tlp.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlp.Size = New System.Drawing.Size(400, 51)
        Me.tlp.TabIndex = 0
        '
        'lblText
        '
        Me.lblText.Appearance.Font = New System.Drawing.Font("Segoe UI Semibold", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblText.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblText.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblText.Location = New System.Drawing.Point(153, 3)
        Me.lblText.Name = "lblText"
        Me.lblText.Size = New System.Drawing.Size(194, 45)
        Me.lblText.TabIndex = 75
        '
        'lblLabel
        '
        Me.lblLabel.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblLabel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblLabel.Location = New System.Drawing.Point(3, 3)
        Me.lblLabel.Name = "lblLabel"
        Me.lblLabel.Size = New System.Drawing.Size(144, 45)
        Me.lblLabel.TabIndex = 2
        Me.lblLabel.Text = "Label"
        '
        'btnButton
        '
        Me.btnButton.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btnButton.Appearance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnButton.Appearance.Options.UseFont = True
        Me.btnButton.Appearance.Options.UseTextOptions = True
        Me.btnButton.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnButton.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.btnButton.Image = Global.NurseryTouchscreen.My.Resources.Resources.Pencil_32
        Me.btnButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnButton.Location = New System.Drawing.Point(353, 3)
        Me.btnButton.Name = "btnButton"
        Me.btnButton.Size = New System.Drawing.Size(44, 45)
        Me.btnButton.TabIndex = 74
        '
        'Field
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
        Me.Controls.Add(Me.tlp)
        Me.MinimumSize = New System.Drawing.Size(210, 51)
        Me.Name = "Field"
        Me.Size = New System.Drawing.Size(400, 51)
        Me.tlp.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tlp As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblLabel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblText As DevExpress.XtraEditors.LabelControl

End Class
