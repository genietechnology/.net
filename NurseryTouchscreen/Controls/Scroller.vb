﻿Imports NurseryTouchscreen.SharedModule
Imports System.ComponentModel

<DefaultEvent("RecordChanged")>
Public Class Scroller

    Public Enum EnumScrollerMode
        Children
        Groups
        Staff
    End Enum

    Public Event RecordChanged(sender As Object, e As RecordChangedArgs)

#Region "Properties"

    <Category("_Setup")>
    Public Property ScrollerMode As EnumScrollerMode

    Public Property ValueID As String

    Public ReadOnly Property Count As Integer
        Get
            Return m_Records.Count
        End Get
    End Property

    Public Property ValueText As String
        Get
            Return be.Text
        End Get
        Set(value As String)
            be.Text = value
        End Set
    End Property

    Public Property SelectedIndex As Integer
        Get
            Return m_SelectedIndex
        End Get
        Set(value As Integer)
            m_SelectedIndex = value
            SetValues()
            SetButtons()
        End Set
    End Property


#End Region

#Region "Variables"

    Private m_Records As New List(Of Pair)
    Private m_Record As Pair
    Private m_SelectedIndex As Integer

#End Region

#Region "Methods"

    Public Sub Clear()
        m_SelectedIndex = -1
        m_Record = Nothing
        ValueID = ""
        ValueText = ""
    End Sub

    Public Sub Populate()

        m_Records.Clear()
        Clear()

        If ScrollerMode = EnumScrollerMode.Groups Then PopulateGroups()
        If ScrollerMode = EnumScrollerMode.Staff Then PopulateStaff()

        If m_Records.Count > 0 Then m_SelectedIndex = 1

        SetButtons()
        SetValues()

    End Sub

    Public Sub SelectRecord(ByVal ID As String)
        If NavigateToRecordByID(ID) Then
            SetButtons()
            SetValues()
        Else
            Clear()
        End If
    End Sub

    Public Sub SelectRecordByName(ByVal Name As String)
        If NavigateToRecordByName(Name) Then
            SetButtons()
            SetValues()
        Else
            Clear()
        End If
    End Sub

    Public Sub PopulateChildren(ByVal Group As String)

        m_Records.Clear()
        Clear()

        If Group Is Nothing Then Exit Sub
        If Group = "" Then Exit Sub

        Dim _SQL As String = ""

        If Parameters.DisableLocation Then

            _SQL += "select person_id, person_name from RegisterSummary"
            _SQL += " left join Children c on c.id = person_id"
            _SQL += " where day_id = '" + TodayID + "'"
            _SQL += " and c.group_name = '" + Group + "'"
            _SQL += " and person_type = 'C'"
            _SQL += " and in_out = 'I'"
            _SQL += " order by person_name"

        Else

            'get all the children in the group, regardless of the day as we might have a child checked in
            'that doesn't normally attend on this day

            _SQL += "select person_id, person_name from RegisterSummary"
            _SQL += " where day_id = '" + TodayID + "'"
            _SQL += " and location = '" + Group + "'"
            _SQL += " and person_type = 'C'"
            _SQL += " and in_out = 'I'"
            _SQL += " order by person_name"

        End If

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If _DT IsNot Nothing Then
            For Each _DR As DataRow In _DT.Rows
                Dim _ID As Guid = New Guid(_DR.Item(0).ToString)
                m_Records.Add(New Pair(_DR.Item(0).ToString, _DR.Item(1).ToString))
            Next
        End If

        If m_Records.Count > 0 Then m_SelectedIndex = 1

        SetButtons()
        SetValues()

    End Sub

#End Region

    Private Sub PopulateGroups()

        Dim _SQL As String = ""

        If Parameters.DisableLocation Then
            _SQL += "select id, room_name from SiteRooms"
            _SQL += " where site_id = " + Parameters.SiteIDQuotes
            _SQL += " order by room_sequence"
        Else

            _SQL += "select distinct g.id, g.room_name, g.room_sequence from RegisterSummary"
            _SQL += " left join SiteRooms g on g.room_name = RegisterSummary.location"
            _SQL += " where day_id = '" + TodayID + "'"
            _SQL += " and site_id = " + Parameters.SiteIDQuotes
            _SQL += " and RegisterSummary.location <> ''"
            _SQL += " and person_type = 'C'"
            _SQL += " and in_out = 'I'"
            _SQL += " order by g.room_sequence"

        End If

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If _DT IsNot Nothing Then
            Dim _i As Integer = 1
            For Each _DR As DataRow In _DT.Rows
                Dim _ID As String = _DR.Item(0).ToString
                Dim _Name As String = _DR.Item(1).ToString
                m_Records.Add(New Pair(_ID, _Name))
                If Parameters.DefaultRoom <> "" Then
                    If _Name = Parameters.DefaultRoom Then
                        SelectedIndex = _i
                    End If
                End If
                _i += 1
            Next
        End If

    End Sub

    Private Sub PopulateStaff()

        Dim _SQL As String = "select id, fullname from Staff where status = 'C' order by fullname"

        Dim _DT As DataTable = DAL.ReturnDataTable(_SQL)
        If _DT IsNot Nothing Then
            For Each _DR As DataRow In _DT.Rows
                Dim _ID As Guid = New Guid(_DR.Item(0).ToString)
                If PersonInorOut(_ID) = Enums.InOut.CheckIn Then
                    m_Records.Add(New Pair(_DR.Item(0).ToString, _DR.Item(1).ToString))
                End If
            Next
        End If

    End Sub

    Private Sub SetButtons()

        be.Properties.Buttons(2).Enabled = False

        If m_Records.Count = 0 Then
            be.Properties.Buttons(0).Enabled = False
            be.Properties.Buttons(1).Enabled = False
            be.Properties.Buttons(3).Enabled = False
        Else

            If m_Records.Count = 1 Then
                be.Properties.Buttons(3).Enabled = False
            Else
                be.Properties.Buttons(3).Enabled = True
            End If

            If m_SelectedIndex = 1 Then
                be.Properties.Buttons(0).Enabled = False
            Else
                be.Properties.Buttons(0).Enabled = True
            End If

            If m_Records.Count > m_SelectedIndex Then
                be.Properties.Buttons(1).Enabled = True
            Else
                be.Properties.Buttons(1).Enabled = False
            End If

        End If

    End Sub

    Private Sub SetValues()

        If m_Records.Count > 0 Then
            m_Record = m_Records(m_SelectedIndex - 1)
            ValueID = m_Record.Code
            ValueText = m_Record.Text
        End If

        RaiseEvent RecordChanged(Me, New RecordChangedArgs(ValueID, ValueText))

    End Sub

    Private Sub be_ButtonClick(sender As Object, e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles be.ButtonClick

        Select Case e.Button.Caption

            Case "Left"
                m_SelectedIndex -= 1

            Case "Right"
                m_SelectedIndex += 1

            Case "Search"
                DoSearch()

            Case Else
                Exit Sub

        End Select

        SetButtons()
        SetValues()

    End Sub

    Private Sub DoSearch()

        Dim _Selected As Pair = ReturnButtonSQL(m_Records, "Select Item")
        If Not _Selected Is Nothing AndAlso _Selected.Code <> "" Then
            NavigateToRecordByID(_Selected.Code)
        End If

    End Sub

    Private Function NavigateToRecordByID(ByVal ID As String) As Boolean

        'set the current list index depending on what was selected
        Dim _Index As Integer = 1
        For Each _p As Pair In m_Records
            If _p.Code = ID Then
                m_SelectedIndex = _Index
                Return True
            End If
            _Index += 1
        Next

        m_SelectedIndex = -1
        Return False

    End Function

    Private Function NavigateToRecordByName(ByVal Name As String) As Boolean

        'set the current list index depending on what was selected
        Dim _Index As Integer = 1
        For Each _p As Pair In m_Records
            If _p.Text = Name Then
                m_SelectedIndex = _Index
                Return True
            End If
            _Index += 1
        Next

        m_SelectedIndex = -1
        Return False

    End Function

End Class
