﻿Public Class QuestionControl

    Public Sub New(ByVal QuestionID As String, ByVal QuestionText As String, ByVal QuestionType As String,
                   ByVal ResponseID As String, ByVal ResponseAnswer As String, ByVal ResponseComments As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_QuestionID = QuestionID
        m_QuestionText = QuestionText
        m_QuestionType = QuestionType

        m_ResponseID = ResponseID
        m_Answer = ResponseAnswer
        m_Comments = ResponseComments

        lblQuestion.Text = QuestionText
        lblAnswer.Text = ResponseAnswer

        Select Case QuestionType

            Case "Yes/No", "Reverse Yes/No"

                btnEdit.Enabled = False

                btnYes.Enabled = True
                btnYes.Appearance.BackColor = m_LightGreen
                btnNo.Enabled = True
                btnNo.Appearance.BackColor = m_LightRed

            Case Else

                btnEdit.Enabled = True

                btnYes.Enabled = False
                btnNo.Enabled = False

        End Select

    End Sub

#Region "Variables"

    Private m_QuestionID As String = ""
    Private m_QuestionText As String = ""
    Private m_QuestionType As String = ""
    Private m_ResponseID As String = ""
    Private m_Answer As String = ""
    Private m_Comments As String = ""

    Private m_LightGreen As Color = Drawing.Color.FromArgb(192, 255, 192)
    Private m_LightRed As Color = Drawing.Color.FromArgb(255, 192, 192)

    Private m_Green As Color = Drawing.Color.FromArgb(128, 255, 128)
    Private m_Red As Color = Drawing.Color.FromArgb(255, 128, 128)

#End Region

#Region "Properties"

    Public ReadOnly Property QuestionID As String
        Get
            Return m_QuestionID
        End Get
    End Property

    Public ReadOnly Property QuestionText As String
        Get
            Return m_QuestionText
        End Get
    End Property

    Public ReadOnly Property QuestionType As String
        Get
            Return m_QuestionType
        End Get
    End Property

    Public ReadOnly Property ResponseID As String
        Get
            Return m_ResponseID
        End Get
    End Property

    Public ReadOnly Property ResponseAnswer As String
        Get
            Return m_Answer
        End Get
    End Property

    Public ReadOnly Property ResponseComments As String
        Get
            Return m_Comments
        End Get
    End Property

#End Region

#Region "Button Click Implementation"

    Private Sub Yes()
        m_Answer = "Yes"
        lblAnswer.Text = "Yes"
        btnYes.Appearance.BackColor = m_Green
        btnNo.Appearance.BackColor = m_LightRed
    End Sub

    Private Sub No()
        m_Answer = "No"
        lblAnswer.Text = "No"
        btnYes.Appearance.BackColor = m_LightGreen
        btnNo.Appearance.BackColor = m_Red
    End Sub

#End Region

#Region "Button Clicks"

    Private Sub btnYes_Click(sender As Object, e As EventArgs) Handles btnYes.Click
        Yes()
    End Sub

    Private Sub btnNo_Click(sender As Object, e As EventArgs) Handles btnNo.Click
        No()
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click

        Dim _Value As String = ""

        Select Case m_QuestionType

            Case "Number"
                _Value = SharedModule.NumberEntry(m_QuestionText, "")

            Case "Decimal"
                _Value = SharedModule.NumberEntry(m_QuestionText, "", True, True, True)

            Case "Time"
                _Value = SharedModule.TimeEntry(m_QuestionText, False)

        End Select

        m_Answer = _Value
        lblAnswer.Text = _Value

    End Sub

    Private Sub btnNotes_Click(sender As Object, e As EventArgs) Handles btnNotes.Click
        m_Comments = SharedModule.OSK("Enter Comments", True, True, m_Comments)
    End Sub

#End Region

End Class
