﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RiskControl
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.btn31 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn21 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn11 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn32 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn22 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn12 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn33 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn23 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn13 = New DevExpress.XtraEditors.SimpleButton()
        Me.SuspendLayout()
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl2.Location = New System.Drawing.Point(75, 201)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(192, 24)
        Me.LabelControl2.TabIndex = 21
        Me.LabelControl2.Text = "Likelihood"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl1.Location = New System.Drawing.Point(5, 90)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(56, 19)
        Me.LabelControl1.TabIndex = 20
        Me.LabelControl1.Text = "Severity"
        '
        'btn31
        '
        Me.btn31.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btn31.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn31.Appearance.Options.UseBackColor = True
        Me.btn31.Appearance.Options.UseFont = True
        Me.btn31.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btn31.Location = New System.Drawing.Point(207, 135)
        Me.btn31.Name = "btn31"
        Me.btn31.Size = New System.Drawing.Size(60, 60)
        Me.btn31.TabIndex = 19
        Me.btn31.Text = "3"
        '
        'btn21
        '
        Me.btn21.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btn21.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn21.Appearance.Options.UseBackColor = True
        Me.btn21.Appearance.Options.UseFont = True
        Me.btn21.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btn21.Location = New System.Drawing.Point(141, 135)
        Me.btn21.Name = "btn21"
        Me.btn21.Size = New System.Drawing.Size(60, 60)
        Me.btn21.TabIndex = 18
        Me.btn21.Text = "2"
        '
        'btn11
        '
        Me.btn11.Appearance.BackColor = System.Drawing.Color.LawnGreen
        Me.btn11.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn11.Appearance.Options.UseBackColor = True
        Me.btn11.Appearance.Options.UseFont = True
        Me.btn11.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btn11.Location = New System.Drawing.Point(75, 135)
        Me.btn11.Name = "btn11"
        Me.btn11.Size = New System.Drawing.Size(60, 60)
        Me.btn11.TabIndex = 17
        Me.btn11.Text = "1"
        '
        'btn32
        '
        Me.btn32.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btn32.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn32.Appearance.Options.UseBackColor = True
        Me.btn32.Appearance.Options.UseFont = True
        Me.btn32.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btn32.Location = New System.Drawing.Point(207, 69)
        Me.btn32.Name = "btn32"
        Me.btn32.Size = New System.Drawing.Size(60, 60)
        Me.btn32.TabIndex = 16
        Me.btn32.Text = "6"
        '
        'btn22
        '
        Me.btn22.Appearance.BackColor = System.Drawing.Color.Orange
        Me.btn22.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn22.Appearance.Options.UseBackColor = True
        Me.btn22.Appearance.Options.UseFont = True
        Me.btn22.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btn22.Location = New System.Drawing.Point(141, 69)
        Me.btn22.Name = "btn22"
        Me.btn22.Size = New System.Drawing.Size(60, 60)
        Me.btn22.TabIndex = 15
        Me.btn22.Text = "4"
        '
        'btn12
        '
        Me.btn12.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btn12.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn12.Appearance.Options.UseBackColor = True
        Me.btn12.Appearance.Options.UseFont = True
        Me.btn12.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btn12.Location = New System.Drawing.Point(75, 69)
        Me.btn12.Name = "btn12"
        Me.btn12.Size = New System.Drawing.Size(60, 60)
        Me.btn12.TabIndex = 14
        Me.btn12.Text = "2"
        '
        'btn33
        '
        Me.btn33.Appearance.BackColor = System.Drawing.Color.Tomato
        Me.btn33.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn33.Appearance.Options.UseBackColor = True
        Me.btn33.Appearance.Options.UseFont = True
        Me.btn33.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btn33.Location = New System.Drawing.Point(207, 3)
        Me.btn33.Name = "btn33"
        Me.btn33.Size = New System.Drawing.Size(60, 60)
        Me.btn33.TabIndex = 13
        Me.btn33.Text = "9"
        '
        'btn23
        '
        Me.btn23.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btn23.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn23.Appearance.Options.UseBackColor = True
        Me.btn23.Appearance.Options.UseFont = True
        Me.btn23.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btn23.Location = New System.Drawing.Point(141, 3)
        Me.btn23.Name = "btn23"
        Me.btn23.Size = New System.Drawing.Size(60, 60)
        Me.btn23.TabIndex = 12
        Me.btn23.Text = "6"
        '
        'btn13
        '
        Me.btn13.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btn13.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn13.Appearance.Options.UseBackColor = True
        Me.btn13.Appearance.Options.UseFont = True
        Me.btn13.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btn13.Location = New System.Drawing.Point(75, 3)
        Me.btn13.Name = "btn13"
        Me.btn13.Size = New System.Drawing.Size(60, 60)
        Me.btn13.TabIndex = 11
        Me.btn13.Text = "3"
        '
        'RiskControl
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.btn31)
        Me.Controls.Add(Me.btn21)
        Me.Controls.Add(Me.btn11)
        Me.Controls.Add(Me.btn32)
        Me.Controls.Add(Me.btn22)
        Me.Controls.Add(Me.btn12)
        Me.Controls.Add(Me.btn33)
        Me.Controls.Add(Me.btn23)
        Me.Controls.Add(Me.btn13)
        Me.Name = "RiskControl"
        Me.Size = New System.Drawing.Size(270, 226)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btn31 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn21 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn11 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn32 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn22 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn12 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn33 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn23 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn13 As DevExpress.XtraEditors.SimpleButton
End Class
