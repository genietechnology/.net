﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ToiletControl
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnDelete = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCream = New DevExpress.XtraEditors.SimpleButton()
        Me.btnLoose = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSoiled = New DevExpress.XtraEditors.SimpleButton()
        Me.btnWet = New DevExpress.XtraEditors.SimpleButton()
        Me.btnDry = New DevExpress.XtraEditors.SimpleButton()
        Me.lblName = New DevExpress.XtraEditors.LabelControl()
        Me.btnPotty = New DevExpress.XtraEditors.SimpleButton()
        Me.btnToilet = New DevExpress.XtraEditors.SimpleButton()
        Me.btnNappy = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAccident = New DevExpress.XtraEditors.SimpleButton()
        Me.SuspendLayout()
        '
        'btnDelete
        '
        Me.btnDelete.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnDelete.Image = Global.NurseryTouchscreen.My.Resources.Resources.delete_32
        Me.btnDelete.Location = New System.Drawing.Point(312, 8)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(36, 36)
        Me.btnDelete.TabIndex = 1
        '
        'btnCream
        '
        Me.btnCream.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnCream.Appearance.Options.UseBackColor = True
        Me.btnCream.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnCream.Location = New System.Drawing.Point(1057, 8)
        Me.btnCream.Name = "btnCream"
        Me.btnCream.Size = New System.Drawing.Size(141, 36)
        Me.btnCream.TabIndex = 10
        Me.btnCream.Text = "Cream Applied"
        '
        'btnLoose
        '
        Me.btnLoose.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnLoose.Appearance.Options.UseBackColor = True
        Me.btnLoose.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnLoose.Location = New System.Drawing.Point(971, 8)
        Me.btnLoose.Name = "btnLoose"
        Me.btnLoose.Size = New System.Drawing.Size(80, 36)
        Me.btnLoose.TabIndex = 9
        Me.btnLoose.Text = "Loose"
        '
        'btnSoiled
        '
        Me.btnSoiled.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnSoiled.Appearance.Options.UseBackColor = True
        Me.btnSoiled.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnSoiled.Location = New System.Drawing.Point(885, 8)
        Me.btnSoiled.Name = "btnSoiled"
        Me.btnSoiled.Size = New System.Drawing.Size(80, 36)
        Me.btnSoiled.TabIndex = 8
        Me.btnSoiled.Text = "Soiled"
        '
        'btnWet
        '
        Me.btnWet.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnWet.Appearance.Options.UseBackColor = True
        Me.btnWet.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnWet.Location = New System.Drawing.Point(799, 8)
        Me.btnWet.Name = "btnWet"
        Me.btnWet.Size = New System.Drawing.Size(80, 36)
        Me.btnWet.TabIndex = 7
        Me.btnWet.Text = "Wet"
        '
        'btnDry
        '
        Me.btnDry.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnDry.Appearance.Options.UseBackColor = True
        Me.btnDry.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnDry.Location = New System.Drawing.Point(713, 8)
        Me.btnDry.Name = "btnDry"
        Me.btnDry.Size = New System.Drawing.Size(80, 36)
        Me.btnDry.TabIndex = 6
        Me.btnDry.Text = "Dry"
        '
        'lblName
        '
        Me.lblName.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblName.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.lblName.Location = New System.Drawing.Point(6, 8)
        Me.lblName.Name = "lblName"
        Me.lblName.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.lblName.Size = New System.Drawing.Size(300, 36)
        Me.lblName.TabIndex = 0
        Me.lblName.Text = "Child Name"
        '
        'btnPotty
        '
        Me.btnPotty.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnPotty.Appearance.Options.UseBackColor = True
        Me.btnPotty.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnPotty.Location = New System.Drawing.Point(440, 8)
        Me.btnPotty.Name = "btnPotty"
        Me.btnPotty.Size = New System.Drawing.Size(80, 36)
        Me.btnPotty.TabIndex = 3
        Me.btnPotty.Text = "Potty"
        '
        'btnToilet
        '
        Me.btnToilet.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnToilet.Appearance.Options.UseBackColor = True
        Me.btnToilet.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnToilet.Location = New System.Drawing.Point(526, 8)
        Me.btnToilet.Name = "btnToilet"
        Me.btnToilet.Size = New System.Drawing.Size(80, 36)
        Me.btnToilet.TabIndex = 4
        Me.btnToilet.Text = "Toilet"
        '
        'btnNappy
        '
        Me.btnNappy.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnNappy.Appearance.Options.UseBackColor = True
        Me.btnNappy.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnNappy.Location = New System.Drawing.Point(354, 8)
        Me.btnNappy.Name = "btnNappy"
        Me.btnNappy.Size = New System.Drawing.Size(80, 36)
        Me.btnNappy.TabIndex = 2
        Me.btnNappy.Text = "Nappy"
        '
        'btnAccident
        '
        Me.btnAccident.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnAccident.Appearance.Options.UseBackColor = True
        Me.btnAccident.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnAccident.Location = New System.Drawing.Point(612, 8)
        Me.btnAccident.Name = "btnAccident"
        Me.btnAccident.Size = New System.Drawing.Size(95, 36)
        Me.btnAccident.TabIndex = 5
        Me.btnAccident.Text = "Accident"
        '
        'ToiletControl
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.Controls.Add(Me.btnAccident)
        Me.Controls.Add(Me.btnPotty)
        Me.Controls.Add(Me.btnToilet)
        Me.Controls.Add(Me.btnNappy)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnCream)
        Me.Controls.Add(Me.btnLoose)
        Me.Controls.Add(Me.btnSoiled)
        Me.Controls.Add(Me.btnWet)
        Me.Controls.Add(Me.btnDry)
        Me.Controls.Add(Me.lblName)
        Me.Name = "ToiletControl"
        Me.Size = New System.Drawing.Size(1205, 53)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnDelete As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCream As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnLoose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSoiled As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnWet As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnDry As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnPotty As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnToilet As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnNappy As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAccident As DevExpress.XtraEditors.SimpleButton
End Class
