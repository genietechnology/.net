﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class QuestionControl
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btnEdit = New DevExpress.XtraEditors.SimpleButton()
        Me.lblQuestion = New DevExpress.XtraEditors.LabelControl()
        Me.btnYes = New DevExpress.XtraEditors.SimpleButton()
        Me.btnNo = New DevExpress.XtraEditors.SimpleButton()
        Me.lblAnswer = New DevExpress.XtraEditors.LabelControl()
        Me.btnNotes = New DevExpress.XtraEditors.SimpleButton()
        Me.SuspendLayout()
        '
        'btnEdit
        '
        Me.btnEdit.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnEdit.Image = Global.NurseryTouchscreen.My.Resources.Resources.Pencil_32
        Me.btnEdit.Location = New System.Drawing.Point(45, 3)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(36, 36)
        Me.btnEdit.TabIndex = 1
        '
        'lblQuestion
        '
        Me.lblQuestion.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblQuestion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblQuestion.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.lblQuestion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblQuestion.Location = New System.Drawing.Point(338, 3)
        Me.lblQuestion.Name = "lblQuestion"
        Me.lblQuestion.Size = New System.Drawing.Size(590, 36)
        Me.lblQuestion.TabIndex = 5
        Me.lblQuestion.Text = "Is the changing table secure?"
        '
        'btnYes
        '
        Me.btnYes.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnYes.Appearance.Options.UseBackColor = True
        Me.btnYes.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnYes.Location = New System.Drawing.Point(186, 3)
        Me.btnYes.Name = "btnYes"
        Me.btnYes.Size = New System.Drawing.Size(70, 36)
        Me.btnYes.TabIndex = 3
        Me.btnYes.Text = "Yes"
        '
        'btnNo
        '
        Me.btnNo.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnNo.Appearance.Options.UseBackColor = True
        Me.btnNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnNo.Location = New System.Drawing.Point(262, 3)
        Me.btnNo.Name = "btnNo"
        Me.btnNo.Size = New System.Drawing.Size(70, 36)
        Me.btnNo.TabIndex = 4
        Me.btnNo.Text = "No"
        '
        'lblAnswer
        '
        Me.lblAnswer.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblAnswer.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lblAnswer.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.lblAnswer.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblAnswer.Location = New System.Drawing.Point(87, 3)
        Me.lblAnswer.Name = "lblAnswer"
        Me.lblAnswer.Size = New System.Drawing.Size(93, 36)
        Me.lblAnswer.TabIndex = 2
        Me.lblAnswer.Text = "Yes"
        '
        'btnNotes
        '
        Me.btnNotes.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.btnNotes.Image = Global.NurseryTouchscreen.My.Resources.Resources.req_32
        Me.btnNotes.Location = New System.Drawing.Point(3, 3)
        Me.btnNotes.Name = "btnNotes"
        Me.btnNotes.Size = New System.Drawing.Size(36, 36)
        Me.btnNotes.TabIndex = 0
        '
        'QuestionControl
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.Controls.Add(Me.btnNotes)
        Me.Controls.Add(Me.lblAnswer)
        Me.Controls.Add(Me.btnYes)
        Me.Controls.Add(Me.btnNo)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.lblQuestion)
        Me.Name = "QuestionControl"
        Me.Size = New System.Drawing.Size(931, 53)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnEdit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblQuestion As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnYes As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnNo As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblAnswer As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnNotes As DevExpress.XtraEditors.SimpleButton
End Class
