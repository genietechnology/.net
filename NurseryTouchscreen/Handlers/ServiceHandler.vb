﻿Imports System.ServiceModel

Public Class ServiceHandler

    Public Shared Sub ViewReport(ByVal ChildID As String)

        Dim _Service As NurseryGenieService.NurseryGenieLocalClient = Nothing
        Dim _File As NurseryGenieService.GeneratedFile = Nothing

        Try
            _Service = New NurseryGenieService.NurseryGenieLocalClient
            _File = _Service.GetReportPDF("", "", My.Computer.Name, ChildID)
            _Service.Close()

        Catch tex As TimeoutException
            ErrorHandler.LogException(tex)
            Msgbox("Timeout - Unable to download Report File.", MessageBoxIcon.Error, "Download Report")
            _Service.Abort()

        Catch cex As CommunicationException
            ErrorHandler.LogException(cex)
            Msgbox("Communications Error - Unable to download Report File.", MessageBoxIcon.Error, "Download Report")
            _Service.Abort()
        End Try

        If _File IsNot Nothing Then

            Try

                Dim _PDF As String = SharedModule.TempFolderPath + "\" + _File.FileName
                IO.File.WriteAllBytes(_PDF, _File.FileData)

                If IO.File.Exists(_PDF) Then
                    Dim _frm As New frmPDFViewer(_PDF)
                    _frm.Show()
                End If

            Catch ex As Exception
                ErrorHandler.LogException(ex)
                Msgbox("Error writing PDF File.", MessageBoxIcon.Error, "Download Report")
            End Try

        Else
            Msgbox("No report was generated.", MessageBoxIcon.Error, "Download Report")
        End If

    End Sub

    Public Shared Sub SendReport(ByVal ChildID As String)

        Dim _Service = New NurseryGenieService.NurseryGenieLocalClient
        Dim _Result As Integer = 0

        Try
            _Result = _Service.EmailReports("", "", My.Computer.Name, ChildID)
            _Service.Close()

            If _Result < 0 Then
                Msgbox("Service Error: " + _Result.ToString + " - Unable to Email Report.", MessageBoxIcon.Error, "Email Report")
            End If

        Catch tex As TimeoutException
            Msgbox("Timeout Error - Unable to Email Report.", MessageBoxIcon.Error, "Email Report")
            _Service.Abort()

        Catch cex As CommunicationException
            Msgbox("Communucation Error - Unable to Email Report.", MessageBoxIcon.Error, "Email Report")
            _Service.Abort()
        End Try

    End Sub

    'Public Shared Function ReturnDayID() As Guid?

    '    Dim _Return As Guid? = Nothing
    '    Dim _Service = New NurseryGenieService.NurseryGenieLocalClient

    '    Try
    '        _Return = _Service.ReturnDayID(Today)
    '        _Service.Close()

    '    Catch tex As TimeoutException
    '        _Service.Abort()

    '    Catch cex As CommunicationException
    '        _Service.Abort()

    '    End Try

    '    Return _Return

    'End Function

End Class
