﻿Imports System.IO

Public Class DocumentHandler

    Public Shared Sub OpenFile(ByRef Data As Byte(), ByVal FileExt As String)

        Dim _TempFolder As String = ""
        Dim _FilePath As String = ReturnTemporaryFile(_TempFolder, FileExt)

        If ByteArraytoFile(Data, _FilePath) Then

            If File.Exists(_FilePath) Then

                Dim _Proc As New Process
                _Proc.StartInfo.FileName = _FilePath

                Try
                    _Proc.Start()

                Catch ex As Exception

                End Try

                _Proc.Dispose()
                _Proc = Nothing

            End If

        End If

    End Sub

    Private Shared Function ByteArraytoFile(ByVal Data As Byte(), ByVal FilePath As String) As Boolean

        If Data IsNot Nothing Then

            Try

                Dim _fs As FileStream = New IO.FileStream(FilePath, FileMode.OpenOrCreate, FileAccess.Write)
                Dim _bw As BinaryWriter = New IO.BinaryWriter(_fs)

                _bw.Write(Data)
                _bw.Flush()
                _bw.Close()
                _bw = Nothing

                _fs.Close()
                _fs = Nothing

                Return True

            Catch ex As Exception
                Return False
            End Try

        Else
            Return False
        End If

    End Function

    Private Shared Function ReturnTemporaryFile(ByVal TempFolder As String, ByVal Extension As String) As String
        Dim _Ext As String = Extension.Replace(".", "")
        Return TempFolder & Guid.NewGuid.ToString & "." & _Ext
    End Function

End Class

