﻿
Public Class SMSHandler

    Private m_OutBoxID As Guid?
    Private m_MobileNo As String
    Private m_Message As String

    Public Property OutBoxID() As Guid?
        Get
            Return m_OutBoxID
        End Get
        Set(ByVal value As Guid?)
            m_OutBoxID = value
        End Set
    End Property

    Public Property MobileNumber() As String
        Get
            Return m_MobileNo
        End Get
        Set(ByVal value As String)
            m_MobileNo = value
        End Set
    End Property

    Public Property Message() As String
        Get
            Return m_Message
        End Get
        Set(ByVal value As String)
            m_Message = value
        End Set
    End Property

    Public Shared Function SendSMS(ByVal MobileNumber As String, ByVal Message As String, Optional OutBoxID As Guid? = Nothing) As Boolean

        Dim _Handler As New SMSHandler
        With _Handler
            .OutBoxID = OutBoxID
            .MobileNumber = MobileNumber
            .Message = Message
            .Send()
        End With

        Return True

    End Function

    Public Sub Send()

        Dim _E As New EmailHandler
        _E.Recipient = m_MobileNo + "@txtlocal.co.uk"
        _E.SenderEmail = "sms@caresoftware.co.uk"
        _E.SenderName = "Care Software"
        _E.Body = m_Message
        _E.Send()

        _E = Nothing

    End Sub

End Class
