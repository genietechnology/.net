﻿Option Strict On

Imports System.IO

Public Class SharedModule

#Region "Enums"

    Public Enum EnumDirection
        Forwards
        Backwards
    End Enum

    Public Enum EnumMealType
        Breakfast
        Snack
        Lunch
        LunchDessert
        PMSnack
        Tea
        TeaDessert
        BabyFood
    End Enum

    Public Enum EnumUsage
        StandardTouch
        StandardTablet
        ClubMode
        Foyer
        StandardFromFoyerMode
    End Enum

    Public Enum EnumActivityType
        Toilet
        Milk
        Sleep
        Request
        Suncream
        Feedback
        CrossCheck
        CheckOutMedication
        CheckOutIncident
        Toothbrush
        Absence
        Asleep
        SleepCheck
        Payment
        Unlock
        LogOff
    End Enum

    Public Enum EnumSMSActivityType
        Toilet
        Milk
        Sleep
        Food
    End Enum

    Public Enum EnumUnlockMode
        Code
        PIN
        Disabled
    End Enum

    Public Enum EnumCheckMode
        SinglePerson
        MultiPerson
        SignPerson
    End Enum

    Public Enum EnumPersonType
        Child
        Staff
        Visitor
    End Enum

#End Region

    Public Const g_BabyFoodMealID As String = "3BF50230-A1CA-4652-B878-C4770F9A5BF0"

#Region "Properties"

    Public Shared Property UsageMode As EnumUsage
    Public Shared Property CameraRollPath As String = My.Computer.FileSystem.SpecialDirectories.MyPictures + "\" + "Camera Roll"
    Public Shared Property FlashCardPath As String
    Public Shared Property TempFolderPath As String = Environment.CurrentDirectory + "\temp"

    Public Shared ReadOnly Property TodayID As String
        Get
            Return Parameters.TodayID
        End Get
    End Property

    Public Shared ReadOnly Property SQLTodayWithQuotes As String
        Get
            Return "'" + Format(Date.Today, "yyyy-MM-dd").ToString + "'"
        End Get
    End Property

    Public Shared ReadOnly Property Version() As String
        Get
            Return Application.ProductVersion
        End Get
    End Property

    Public Shared ReadOnly Property ConnectionString As String

        Get

            If Parameters.UseDirectSQL Then

                If Parameters.DirectConnectionString = "" Then

                    Return "" & _
                          "Data Source=" & Parameters.ServerIP & ";" & _
                          "Database=nursery;" & _
                          "User ID=tablet;" & _
                          "Password=c@r3s0ftw@r3" & ";"

                Else
                    Return Parameters.DirectConnectionString
                End If

            Else
                Return "Data Source = " + IO.Path.GetDirectoryName(Application.ExecutablePath) + "\DB.sdf;"
            End If

        End Get

    End Property

    Public Shared Property ColourRed As Drawing.Color = Color.LightCoral
    Public Shared Property ColourYellow As Drawing.Color = Color.Yellow
    Public Shared Property ColourGreen As Drawing.Color = Color.LightGreen

#End Region

#Region "Methods"

    Public Shared Function SQLDateWithQuotes(ByVal DateIn As Date) As String
        Return "'" + Format(DateIn, "yyyy-MM-dd").ToString + "'"
    End Function

    Public Shared Function NearestDate(ByVal DateIn As Date, ByVal NearestWeekDay As DayOfWeek, ByVal SearchDirection As EnumDirection) As Date

        Dim _Date As Date = DateIn
        Dim _DayIncrement As Integer = 1

        If SearchDirection = EnumDirection.Backwards Then _DayIncrement = -1

        While _Date.DayOfWeek <> NearestWeekDay
            _Date = _Date.AddDays(_DayIncrement)
        End While

        Return _Date

    End Function

    Public Shared Function UnlockScreen() As Boolean

        Dim _Return As Boolean = False
        Dim _Entered As String = ""

        Select Case Parameters.UnlockMode

            Case EnumUnlockMode.Disabled
                _Return = True

            Case EnumUnlockMode.Code
                _Entered = NumberEntry("Please enter the Unlock Code", "", False, False)
                If _Entered = Parameters.UnlockCode Then
                    _Return = True
                End If

            Case EnumUnlockMode.PIN
                _Entered = NumberEntry("Please enter your Staff PIN code", "", False, False)
                If _Entered <> "" Then

                    Dim _Staff As Pair = Business.Staff.ValidatePIN(_Entered)
                    If _Staff IsNot Nothing Then

                        Parameters.CurrentStaffID = _Staff.Code
                        Parameters.CurrentStaffName = _Staff.Text

                        'check-in this member if not already
                        If SharedModule.PersonInorOut(New Guid(_Staff.Code)) = Enums.InOut.CheckOut Then
                            SharedModule.RegisterTransaction(New Guid(_Staff.Code), Enums.PersonType.Staff, _Staff.Text, Enums.InOut.CheckIn, Enums.RegisterVia.UnlockPIN)
                        End If

                        'create an unlocked activity
                        LogActivity(TodayID, _Staff.Code, _Staff.Text, EnumActivityType.Unlock, _Staff.Text + " unlocked " + My.Computer.Name, My.Computer.Name, "", "", _Staff.Code, _Staff.Text)

                        _Return = True

                    End If

                End If

        End Select

        Return _Return

    End Function

    Public Shared Sub ZoomText(ByVal TextIn As String)

        If TextIn.Trim = "" Then Exit Sub

        Dim frmText As New frmTextView(TextIn)
        frmText.ShowDialog()

        frmText.Dispose()
        frmText = Nothing

    End Sub

    Public Shared Sub DisplayGridData(ByVal Caption As String, ByVal SQL As String, Optional ColumnHeadings As String = "")

        Dim frmGrid As New frmGrid(SQL, ColumnHeadings)
        frmGrid.Text = Caption
        frmGrid.ShowDialog()

        frmGrid.Dispose()
        frmGrid = Nothing

    End Sub

    Public Shared Sub DisplayLinks(ByVal Area As String, ByVal Caption As String)

        Dim _SQL As String = ""
        _SQL += "select ID, name from TouchLinks"
        _SQL += " where area = '" + Area + "'"
        _SQL += " order by area, name"

        Dim _P As Pair = ReturnButtonSQL(_SQL, Caption)

        If _P IsNot Nothing Then

            _SQL = "select * from TouchLinks where ID = '" + _P.Code + "'"
            Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
            If _DR IsNot Nothing Then

                Dim _Type As String = _DR.Item("type").ToString
                Dim _Path As String = _DR.Item("path").ToString

                Select Case _Type

                    Case "URL"
                        Process.Start(_Path)

                    Case "File"
                        Dim _FilePath As String = Parameters.Paths.SharedFolderPath + "\" + _Path
                        If IO.File.Exists(_FilePath) Then
                            Process.Start(_FilePath)
                        Else
                            Msgbox("The specified file cannot be located." + vbCrLf + vbCrLf + _FilePath, MessageBoxIcon.Exclamation, "Display Link")
                        End If

                End Select

            End If

        End If

    End Sub

    Public Shared Function DeveloperPIN() As Boolean
        If NumberEntry("Enter PIN", "", False, False) = "1226" Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Shared Function ClearForDemo() As Boolean

        Dim _ID As String = DAL.ReturnField("day", "date", Parameters.TodaySQLDate, "ID")

        DAL.ExecuteCommand("delete from Register where date = '" + Parameters.TodaySQLDate + "'")

        ExecDelete("DayActivity", Parameters.TodayID)
        ExecDelete("Activity", Parameters.TodayID)
        ExecDelete("FoodRegister", Parameters.TodayID)
        ExecDelete("MedicineAuth", Parameters.TodayID)
        ExecDelete("MedicineLog", Parameters.TodayID)
        ExecDelete("Incidents", Parameters.TodayID)
        ExecDelete("Signatures", Parameters.TodayID)
        ExecDelete("Media", Parameters.TodayID)
        ExecDelete("Obs", Parameters.TodayID)
        ExecDelete("Ass", Parameters.TodayID)
        ExecDelete("RegisterSummary", Parameters.TodayID)

        Msgbox("Demo Reset", MessageBoxIcon.Information, "Reset Demo")

    End Function

    Private Shared Sub ExecDelete(ByVal TableName As String, ByVal ID As String)
        Dim _SQL As String = "delete from " + TableName + " where day_id = '" + ID + "'"
        DAL.ExecuteCommand(_SQL)
    End Sub

    Public Shared Function DayExists() As Boolean

        Dim _Return As Boolean = False
        Dim _ID As String = ""

        If Parameters.UseDirectSQL Then
            _ID = ReturnDayIDFromDB()
        Else
            'If Parameters.UseWebServices Then
            '    Dim _GUID As Guid? = ServiceHandler.ReturnDayID
            '    If _GUID.HasValue Then
            '        _ID = _GUID.Value.ToString
            '    Else
            '        _ID = ""
            '    End If
            'Else
            '    _ID = ReturnDayIDFromDB()
            'End If
        End If

        If _ID <> "" Then
            _Return = True
            Parameters.TodayID = _ID
        Else

            Parameters.TodayID = ""

            Dim _Mess As String = ""
            _Mess += "Today 's Menu has not been created." + vbCrLf
            _Mess += vbCrLf
            _Mess += "Please ensure you have selected the meals on the Workstation Today Screen."

            Msgbox(_Mess, MessageBoxIcon.Exclamation, "Menu Missing")

        End If

        Return _Return

    End Function

    Public Shared Function GetFamilyIDFromChildID(ByVal ChildID As String) As String

        Dim _SQL As String = "select family_id from Children where ID = '" & ChildID & "'"

        Dim _Day As DataRow = DAL.ReturnDataRow(_SQL)
        If _Day Is Nothing Then
            Return ""
        Else
            Return _Day.Item("family_id").ToString
        End If

    End Function

    Private Shared Function ReturnDayIDFromDB() As String

        Dim _SQL As String = "select * from day where date = '" & Parameters.TodaySQLDate & "' and site_id = " + Parameters.SiteIDQuotes

        Dim _Day As DataRow = DAL.ReturnDataRow(_SQL)
        If _Day Is Nothing Then
            Return ""
        Else
            Return _Day.Item("id").ToString
        End If

    End Function

    Public Shared Sub ShowLDText(ByVal TextString As String)
        Dim _frm As New frmLDText
        _frm.Setup(TextString)
        _frm.Show()
        _frm.BringToFront()
    End Sub

    Public Shared Function OSK(Optional ByVal FormCaption As String = "On Screen Keyboard", Optional ByVal AllowReturn As Boolean = False, _
                               Optional ByVal AllowPunctuation As Boolean = False, Optional ByVal DefaultValue As String = "", _
                               Optional ByVal ApplicationListName As String = "", Optional MaxLength As Integer = 0) As String

        Dim _Return As String = ""
        Dim _OSK As New frmOSK
        With _OSK
            .ApplicationListName = ApplicationListName
            .AllowPunc = AllowPunctuation
            .AllowReturn = AllowReturn
            .DefaultValue = DefaultValue
            .MaxLength = MaxLength
            .Text = FormCaption
            .ShowDialog()
        End With

        _Return = _OSK.ReturnValue

        _OSK.Dispose()
        _OSK = Nothing

        Return _Return

    End Function

    Public Shared Function NumberEntry(Optional ByVal FormCaption As String = "Number Entry",
                                       Optional ByVal DefaultValue As String = "",
                                       Optional ByVal AllowDecimals As Boolean = False,
                                       Optional ByVal ShowCharacters As Boolean = True,
                                       Optional ByVal AllowNegative As Boolean = False) As String

        Dim _Return As String = ""
        Dim _Keypad As New frmNumbersOnly
        With _Keypad
            .DefaultValue = DefaultValue
            .AllowDecimals = AllowDecimals
            .AllowNegative = AllowNegative
            .ShowCharacters = ShowCharacters
            .Text = FormCaption
            .ShowDialog()
        End With

        _Return = _Keypad.ReturnValue

        _Keypad.Dispose()
        _Keypad = Nothing

        Return _Return

    End Function

    Public Shared Function TimeEntry(Optional ByVal FormCaption As String = "TimeEntry", Optional ByVal NA As Boolean = False) As String

        Dim _Return As String = ""
        Dim _TE As New frmTimeEntry
        With _TE
            .Text = FormCaption
            .NA = NA
            .ShowDialog()
        End With

        _Return = _TE.ReturnValue

        _TE.Dispose()
        _TE = Nothing

        Return _Return

    End Function

    Public Shared Function MultipleTimeEntry(Optional ByVal FormCaption As String = "TimeEntry", Optional existingTimes As List(Of String) = Nothing) As List(Of String)

        Dim times As List(Of String)

        Dim form As New frmMultipleTimeEntry
        With form
            .ReturnValue = existingTimes
            .Text = FormCaption
            .ShowDialog()
        End With

        times = form.ReturnValue

        form.Dispose()
        form = Nothing

        Return times

    End Function

    Public Shared Function DateEntry(Optional ByVal FormCaption As String = "Please select a Date") As String

        Dim _Return As String = ""
        Dim _DE As New frmDateEntry
        With _DE
            .Text = FormCaption
            .ShowDialog()
        End With

        _Return = _DE.ReturnValue

        _DE.Dispose()
        _DE = Nothing

        Return _Return

    End Function

    Public Shared Sub Message(ByVal MessageText As String, Optional ByVal Warning As Boolean = False)
        If MessageText = "" Then Exit Sub
        If Warning Then
            Msgbox(MessageText, MessageBoxIcon.Exclamation, "")
        Else
            Msgbox(MessageText, MessageBoxIcon.Information, "")
        End If
    End Sub

    Public Shared Function ReturnButtonSQL(ByVal SQLQuery As String, ByVal FormCaption As String) As Pair

        Dim _Buttons As New List(Of Pair)

        If SQLQuery IsNot Nothing Then

            If SQLQuery.Contains("|") Then

                'split string by pipe
                Dim _Items As String() = Split(SQLQuery, "|")
                If _Items IsNot Nothing Then
                    For Each _Item As String In _Items
                        Dim _Values As String() = Split(_Item, "=")
                        If _Values IsNot Nothing Then
                            If _Values.Count = 1 Then
                                _Buttons.Add(New Pair(_Values(0), ""))
                            Else
                                _Buttons.Add(New Pair(_Values(0), _Values(1)))
                            End If
                        End If
                    Next
                End If

            Else

                Dim _DT As DataTable = DAL.ReturnDataTable(SQLQuery)
                If _DT IsNot Nothing Then
                    For Each _DR As DataRow In _DT.Rows
                        If _DR.ItemArray.Count = 1 Then
                            _Buttons.Add(New Pair(_DR.Item(0).ToString, ""))
                        Else
                            _Buttons.Add(New Pair(_DR.Item(0).ToString, _DR.Item(1).ToString))
                        End If
                    Next
                End If

            End If

            If _Buttons.Count > 0 Then
                Return SetupButtonForm(_Buttons, FormCaption)
            End If

        End If

        Return Nothing

    End Function

    Public Shared Function ReturnButtonSQL(ByVal Buttons As List(Of Pair), ByVal FormCaption As String) As Pair
        Return SetupButtonForm(Buttons, FormCaption)
    End Function

    Private Shared Function SetupButtonForm(ByVal ButtonList As List(Of Pair), ByVal FormCaption As String) As Pair

        Dim _Return As New Pair("", "")

        Dim _frm As New frmButtonsTiled
        With _frm
            .Text = FormCaption
            .ButtonCollection = ButtonList
            .ShowDialog()
            .BringToFront()
        End With

        If _frm.DialogResult = DialogResult.OK Then
            _Return = _frm.SelectedPair
        Else
            _Return = Nothing
        End If

        _frm.Dispose()
        _frm = Nothing

        Return _Return

    End Function

    Public Shared Sub QuickObservation(Optional ByVal ChildID As String = "")
        Dim _frm As New frmObs
        _frm.Show()
        _frm.BringToFront()
    End Sub

    Public Shared Sub ChildObservation(ByVal ChildID As String)
        Dim _frm As New frmChildObs(ChildID)
        _frm.Show()
        _frm.BringToFront()
    End Sub

    Public Shared Sub ChildCDAP(Optional ByVal ChildID As String = "")
        Dim _frm As New frmLDAssessment
        _frm.Show()
        If ChildID <> "" Then _frm.DrillDown(ChildID)
        _frm.BringToFront()
    End Sub

    Public Shared Sub ChildToilet(Optional ByVal ChildID As String = "")
        Dim _Toilet As New HostedToilet
        LoadHostedPanel(_Toilet, ChildID)
    End Sub

    Public Shared Sub ChildMilk(Optional ByVal ChildID As String = "")
        Dim _Milk As New HostedMilk
        LoadHostedPanel(_Milk, ChildID)
    End Sub

    Public Shared Sub ChildFood(Optional ByVal ChildID As String = "")
        If Parameters.UseBasicMeals Then
            Dim _frm As New frmFastFood
            _frm.ShowDialog()
        Else
            Dim _Food As New HostedFood
            LoadHostedPanel(_Food, ChildID)
        End If
    End Sub

    Public Shared Sub ChildSleep(ByVal ChildID As String)
        Dim _Sleep As New HostedSleep
        LoadHostedPanel(_Sleep, ChildID)
    End Sub

    Public Shared Sub ChildRequests(Optional ByVal ChildID As String = "")
        Dim _frm As New frmRequests
        _frm.Show()
        If ChildID <> "" Then _frm.DrillDown(ChildID)
        _frm.BringToFront()
    End Sub

    Public Shared Sub ChildMoreInfo(ByVal ChildID As String, ByVal ChildName As String)
        Dim _frm As New frmChildInfo(ChildID, ChildName)
        _frm.Show()
        _frm.BringToFront()
    End Sub

    Public Shared Sub ChildMedicationLog(ByVal ChildID As String)

    End Sub

    Public Shared Sub ChildIncidents(Optional ByVal ChildID As String = "")

        Dim _ChildID As String = ChildID
        If _ChildID = "" Then
            Dim _Return As Pair = Business.Child.FindChildByGroup(Enums.PersonMode.OnlyCheckedIn, True)
            If Not _Return Is Nothing AndAlso _Return.Code <> "" Then
                _ChildID = _Return.Code
            End If
        End If

        If _ChildID = "" Then Exit Sub

        Dim _SQL As String = ""

        _SQL += "select ID, incident_type as 'Type', location as 'Location', details as 'Details', injury as 'Injury Suffered', stamp as 'Date/Time'"
        _SQL += " from Incidents"
        _SQL += " where day_id = '" + SharedModule.TodayID + "'"
        _SQL += " and child_id = '" + _ChildID + "'"
        _SQL += " order by stamp desc"

        'if there are no incidents defined, then we can just skip the add/remove form and load the incident form
        If DAL.ReturnRecordCount(_SQL) = 0 Then

            Dim _frmInc As New frmIncidentLog()
            _frmInc.DrillDown(_ChildID, "")
            _frmInc.ShowDialog()

            _frmInc.Dispose()
            _frmInc = Nothing

        Else

            Dim _frmAR As New frmAddRemove(_SQL, "New Incident", "Remove Incident", "View Incidents")
            _frmAR.ShowDialog()

            If _frmAR.DialogResult = DialogResult.OK Then

                Select Case _frmAR.ReturnMode

                    Case Enums.AddRemoveReturnMode.AddRecord, Enums.AddRemoveReturnMode.EditRecord

                        Dim _frmInc As New frmIncidentLog()
                        _frmInc.DrillDown(_ChildID, _frmAR.ReturnID)
                        _frmInc.ShowDialog()

                        _frmInc.Dispose()
                        _frmInc = Nothing

                    Case Enums.AddRemoveReturnMode.RemoveRecord

                        _SQL = "delete from Incidents where ID = '" + _frmAR.ReturnID + "'"
                        DAL.ExecuteCommand(_SQL)

                End Select

            End If

            _frmAR.Dispose()
            _frmAR = Nothing

        End If

    End Sub

    Public Shared Sub ChildMedicalAuth(Optional ByVal ChildID As String = "")

        Dim _ChildID As String = ChildID
        If _ChildID = "" Then
            Dim _Return As Pair = Business.Child.FindChildByGroup(Enums.PersonMode.OnlyCheckedIn, True)
            If Not _Return Is Nothing AndAlso _Return.Code <> "" Then
                _ChildID = _Return.Code
            End If
        End If

        If _ChildID = "" Then Exit Sub

        Dim _SQL As String = ""

        _SQL += "select ID, illness as 'Illness', medicine as 'Medicine', dosage as 'Dosage', dosages_due as 'Due', staff_name as 'Staff'"
        _SQL += " from MedicineAuth"
        _SQL += " where day_id = '" + SharedModule.TodayID + "'"
        _SQL += " and child_id = '" + _ChildID + "'"
        _SQL += " order by last_given"

        'if there are no incidents defined, then we can just skip the add/remove form and load the incident form
        If DAL.ReturnRecordCount(_SQL) = 0 Then

            Dim _frmAuth As New frmMedicalAuth
            _frmAuth.DrillDown(_ChildID, "")
            _frmAuth.ShowDialog()

            _frmAuth.Dispose()
            _frmAuth = Nothing

        Else

            Dim _frmAR As New frmAddRemove(_SQL, "New Authorisation", "Remove Authorisation", "Medicine Authorisations")
            _frmAR.ShowDialog()

            If _frmAR.DialogResult = DialogResult.OK Then

                Select Case _frmAR.ReturnMode

                    Case Enums.AddRemoveReturnMode.AddRecord, Enums.AddRemoveReturnMode.EditRecord

                        Dim _frmAuth As New frmMedicalAuth
                        _frmAuth.DrillDown(_ChildID, _frmAR.ReturnID)
                        _frmAuth.ShowDialog()

                        _frmAuth.Dispose()
                        _frmAuth = Nothing

                    Case Enums.AddRemoveReturnMode.RemoveRecord

                        _SQL = "delete from MedicineAuth where ID = '" + _frmAR.ReturnID + "'"
                        DAL.ExecuteCommand(_SQL)

                        _SQL = "delete from MedicineLog where auth_id = '" + _frmAR.ReturnID + "'"
                        DAL.ExecuteCommand(_SQL)

                End Select

            End If

            _frmAR.Dispose()
            _frmAR = Nothing

        End If

    End Sub

    Public Shared Sub ChildToothbrushing(Optional ByVal ChildID As String = "")
        Dim _Suncream As New HostedToothbrushing
        LoadHostedPanel(_Suncream, ChildID)
    End Sub


    Public Shared Sub ChildSuncream(Optional ByVal ChildID As String = "")
        Dim _Suncream As New HostedSuncream
        LoadHostedPanel(_Suncream, ChildID)
    End Sub

    Public Shared Function MedicationDueQuery(Optional ByVal ForceNurseryWideCheck As Boolean = False) As String

        Dim _SQL As String = ""
        Dim _RoomFilter As String = ""

        If Not ForceNurseryWideCheck Then
            If Parameters.DefaultRoom <> "" Then _RoomFilter += " and r.location = '" + Parameters.DefaultRoom + "'"
        End If

        _SQL += "select m.ID, m.button, m.due, m.medicine"
        _SQL += " from MedicineLog m"
        _SQL += " left join Day d on d.ID = m.day_id"
        _SQL += " left Join RegisterSummary r on  r.day_id = m.day_id And r.person_id = m.child_id "
        _SQL += " where m.day_id = '" + Parameters.TodayID + "'"
        _SQL += " and m.staff_id IS NULL"

        If _RoomFilter <> "" Then _SQL += _RoomFilter

        _SQL += " and"
        _SQL += " (select count (*) from Register r where r.date = d.date and r.person_id = m.child_id and r.in_out = 'I') <>"
        _SQL += " (select count (*) from Register r where r.date = d.date and r.person_id = m.child_id and r.in_out = 'O')"
        _SQL += " order by m.due"

        Return _SQL

    End Function

    Public Shared Function LogMedicine(ByVal Mode As frmMedicalLog.EnumMode, ByVal ID As String) As Boolean

        'ViewLog = 0 'viewing from child record or list
        'EditLog = 1 'editting a log that has been generated by creating a request
        'CreateLog = 2 'create ad-hoc medicine i.e. giving calpol for temp

        Dim _Return As Boolean = False
        Dim _frm As New frmMedicalLog(Mode, ID)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            _Return = True
        End If

        _frm.Dispose()
        _frm = Nothing

        Return _Return

    End Function

    Public Shared Function MoveRoom(ByVal PersonType As Enums.PersonType, ByVal PersonID As Guid, ByVal PersonName As String, ByVal CurrentRoom As String) As Boolean

        Dim _Return As Boolean = False

        Dim _NewRoom As Pair = SharedModule.ReturnRoom(Parameters.SiteID, Enums.PersonType.Child, CurrentRoom)
        If _NewRoom IsNot Nothing Then

            If Msgbox("Move " + PersonName + " into " + _NewRoom.Text + "?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Confirm Move") = MsgBoxResult.Yes Then
                SharedModule.RegisterTransaction(PersonID, PersonType, PersonName, Enums.InOut.CheckOut, Enums.RegisterVia.MoveLogic, "", "MO", "Move Out >", "")
                SharedModule.RegisterTransaction(PersonID, PersonType, PersonName, Enums.InOut.CheckIn, Enums.RegisterVia.MoveLogic, _NewRoom.Text, "MI", "< Move In", "")
                _Return = True
            End If

        End If

        Return _Return

    End Function

#End Region

#Region "Classes"

    Public Class Enums

        Public Enum AddRemoveReturnMode
            NotSet
            AddRecord
            EditRecord
            RemoveRecord
        End Enum

        Public Enum PersonMode
            Everyone
            OnlyCheckedIn
            OnlyCheckedOut
        End Enum

        Public Enum InOut
            CheckIn
            CheckOut
        End Enum

        Public Enum PersonType
            Child
            Staff
            Visitor
            Contact
        End Enum

        Public Enum ToiletType
            WetNappy
            SoiledNappy
            Wet
            Soiled
        End Enum

        Public Enum RegisterVia
            Menu
            Child
            Register
            CheckOut
            MoveLogic
            Paxton
            UnlockPIN
        End Enum

    End Class

    Public Class Pair

        Private _Code As String
        Private _Text As String

        Public Sub New()
            _Code = ""
            _Text = ""
        End Sub

        Public Sub New(ByVal CodeValue As String, ByVal TextValue As String)
            _Code = CodeValue
            _Text = TextValue
        End Sub

        Public Property Code() As String
            Get
                Return _Code
            End Get
            Set(ByVal value As String)
                _Code = value
            End Set
        End Property

        Public Property Text() As String
            Get
                Return _Text
            End Get
            Set(ByVal value As String)
                _Text = value
            End Set
        End Property

    End Class

#End Region

#Region "Transactions"

    Public Shared Sub LogActivity(ByVal DayID As String, ByVal KeyID As String, ByVal KeyName As String, _
                                  ByVal ActivityType As EnumActivityType, ByVal Description As String, _
                                  Optional ByVal Value1 As String = "", Optional ByVal Value2 As String = "", Optional ByVal Value3 As String = "", _
                                  Optional ByVal StaffID As String = "", Optional ByVal StaffName As String = "", Optional ByVal Notes As String = "")

        Dim _SQL As String

        _SQL = "select * from Activity where ID is NULL"

        Dim _DA As IDataAdapter = DAL.ReturnDataAdapter(_SQL)
        If Not _DA Is Nothing Then

            Dim _DR As DataRow
            Dim _DS As New DataSet
            _DA.Fill(_DS)

            _DR = _DS.Tables(0).NewRow

            _DR("ID") = Guid.NewGuid
            _DR("day_id") = New Guid(DayID)

            _DR("key_id") = New Guid(KeyID)
            _DR("key_name") = KeyName

            _DR("type") = ReturnActivityTypeCode(ActivityType)
            _DR("description") = Description

            _DR("value_1") = Value1
            _DR("value_2") = Value2
            _DR("value_3") = Value3

            _DR("stamp") = Now

            If StaffID <> "" Then _DR("staff_id") = StaffID
            _DR("staff_name") = StaffName

            _DR("notes") = Notes

            _DS.Tables(0).Rows.Add(_DR)
            DAL.UpdateDB(_DA, _DS)

        End If

    End Sub

    Public Shared Sub UpdateActivity(ByVal ActivityID As String, Optional ByVal Description As String = "", Optional Stamp As Date? = Nothing, _
                                     Optional ByVal Value1 As String = "", Optional ByVal Value2 As String = "", Optional ByVal Value3 As String = "", _
                                     Optional ByVal StaffID As String = "", Optional ByVal StaffName As String = "", Optional ByVal Notes As String = "")

        Dim _SQL As String

        _SQL = "select * from Activity where ID = '" + ActivityID + "'"

        Dim _DA As IDataAdapter = DAL.ReturnDataAdapter(_SQL)
        If Not _DA Is Nothing Then

            Dim _DR As DataRow
            Dim _DS As New DataSet
            _DA.Fill(_DS)

            _DR = _DS.Tables(0).Rows(0)

            If Description <> "" Then _DR("description") = Description

            If Value1 <> "" Then _DR("value_1") = Value1
            If Value2 <> "" Then _DR("value_2") = Value2
            If Value3 <> "" Then _DR("value_3") = Value3

            If StaffID <> "" Then _DR("staff_id") = StaffID
            If StaffName <> "" Then _DR("staff_name") = StaffName

            If Notes <> "" Then _DR("notes") = Notes

            If Stamp.HasValue Then _DR("stamp") = Stamp

            DAL.UpdateDB(_DA, _DS)

        End If

    End Sub

    Public Shared Sub RegisterTransaction(ByVal PersonID As Guid, ByVal PersonType As Enums.PersonType, _
                                          ByVal PersonName As String, ByVal InOut As Enums.InOut, ByVal Via As Enums.RegisterVia)

        RegisterTransaction(PersonID, PersonType, PersonName, InOut, Via, "", "", "", "")

    End Sub

    Public Shared Sub RegisterTransaction(ByVal PersonID As Guid, ByVal PersonType As Enums.PersonType, _
                                          ByVal PersonName As String, ByVal InOut As Enums.InOut, ByVal Via As Enums.RegisterVia, _
                                          ByVal Location As String, ByVal Ref1 As String, ByVal Ref2 As String, ByVal Ref3 As String)

        Dim _Location As String = ""

        If InOut = Enums.InOut.CheckIn Then

            If Ref1 = "MI" OrElse Ref2 = "MO" Then
                _Location = Location
            Else

                If Ref1 = "" Then
                    If AlreadyRegisteredOnServer(PersonID) Then
                        If Msgbox(PersonName & " has already been checked in. Are you sure you want to check them in again?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Check In") = MsgBoxResult.Yes Then
                            Ref1 = "FORCED"
                        Else
                            Exit Sub
                        End If
                    End If
                End If

                If Location = "" Then

                    If Parameters.DisableLocation Then
                        'check-in without the prompt
                    Else
                        If Parameters.DefaultRoom = "" Then
                            If Msgbox("Do you want to check " + PersonName + " into a specific room?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Check In") = MsgBoxResult.Yes Then
                                Dim _p As Pair = ReturnRoom(Parameters.SiteID, PersonType)
                                If _p IsNot Nothing Then
                                    _Location = _p.Text
                                End If
                            End If
                        Else
                            'use the default room
                            _Location = Parameters.DefaultRoom
                        End If
                    End If

                Else
                    _Location = Location
                End If

                'ok, so if we get here, we have either disabled the locations or the user was prompted to choose a location and cancelled
                'we therefore need to default the location to be the location from their record
                If _Location = "" Then
                    If PersonType = Enums.PersonType.Child Then _Location = DAL.ReturnField("Children", "ID", PersonID.ToString, "group_name")
                    If PersonType = Enums.PersonType.Staff Then _Location = DAL.ReturnField("Staff", "ID", PersonID.ToString, "group_name")
                End If

            End If

        Else
            'location was passed in
            _Location = Location
        End If

        Dim _PersonType As String = ""
        Dim _InOut As String = ""
        Dim _Status As String = ""
        Dim _Time As Date = DateAndTime.Now

        If PersonType = Enums.PersonType.Child Then _PersonType = "C"
        If PersonType = Enums.PersonType.Staff Then _PersonType = "S"
        If PersonType = Enums.PersonType.Visitor Then _PersonType = "V"
        If InOut = Enums.InOut.CheckIn Then _InOut = "I"
        If InOut = Enums.InOut.CheckOut Then _InOut = "O"

        Dim _ID As Guid = Guid.NewGuid

        Dim _SQL As String = "select * from Register where ID = '" + _ID.ToString + "'"
        Dim _DA As IDataAdapter = DAL.ReturnDataAdapter(_SQL)

        If Not _DA Is Nothing Then

            Dim _DR As DataRow
            Dim _DS As New DataSet
            _DA.Fill(_DS)

            _DR = _DS.Tables(0).NewRow

            _DR.Item("ID") = _ID
            _DR.Item("date") = Date.Today
            _DR.Item("type") = _PersonType
            _DR.Item("in_out") = _InOut
            _DR.Item("person_id") = PersonID
            _DR.Item("name") = PersonName

            If InOut = Enums.InOut.CheckIn Then
                _DR.Item("stamp_in") = _Time
                _DR.Item("stamp_out") = DBNull.Value
            Else
                _DR.Item("stamp_in") = _Time
                _DR.Item("stamp_out") = Date.Now
            End If

            _DR.Item("location") = _Location
            _DR.Item("ref_1") = Ref1
            _DR.Item("ref_2") = Ref2
            _DR.Item("ref_3") = SharedModule.Version + " | " + My.Computer.Name + "|" + Via.ToString

            _DS.Tables(0).Rows.Add(_DR)
            DAL.UpdateDB(_DA, _DS)

            _DS.Dispose()
            _DS = Nothing

            _DA = Nothing

            UpdateRegisterSummary(PersonID, PersonName, _PersonType, _InOut, _Location, _Time, False)

        End If

    End Sub

    Public Shared Sub UpdateRegisterSummary(ByVal PersonID As Guid, ByVal PersonName As String, ByVal PersonType As String, ByVal InOut As String, ByVal Location As String, ByVal Time As Date, ByVal UndoCheckOut As Boolean)

        Dim _ID As Guid = Guid.NewGuid

        Dim _SQL As String = "select * from RegisterSummary where day_id = '" + TodayID + "' and person_id = '" + PersonID.ToString + "'"
        Dim _DA As IDataAdapter = DAL.ReturnDataAdapter(_SQL)

        If Not _DA Is Nothing Then

            Dim _DR As DataRow
            Dim _DS As New DataSet
            _DA.Fill(_DS)

            If _DS.Tables(0).Rows.Count = 0 Then

                _DR = _DS.Tables(0).NewRow

                _DR.Item("ID") = Guid.NewGuid
                _DR.Item("day_id") = New Guid(TodayID)
                _DR.Item("date") = Date.Today
                _DR.Item("person_id") = PersonID
                _DR.Item("person_type") = PersonType
                _DR.Item("person_name") = PersonName
                _DR.Item("first_stamp") = Time
                _DR.Item("last_stamp") = Time

            Else

                _DR = _DS.Tables(0).Rows(0)

                _DR.Item("last_stamp") = Time

                If UndoCheckOut Then
                    _DR.Item("duration") = DBNull.Value
                    _DR.Item("out_contact") = DBNull.Value
                    _DR.Item("out_signature") = DBNull.Value
                    _DR.Item("out_comments") = DBNull.Value
                Else
                    Dim _FirstStamp As Date = CDate(_DR.Item("first_stamp"))
                    Dim _Duration As Long = DateDiff(DateInterval.Minute, _FirstStamp, Time)
                    _DR.Item("duration") = _Duration
                End If

            End If

            _DR.Item("in_out") = InOut
            _DR.Item("location") = Location

            If _DS.Tables(0).Rows.Count = 0 Then _DS.Tables(0).Rows.Add(_DR)

            DAL.UpdateDB(_DA, _DS)

            _DS.Dispose()
            _DS = Nothing

            _DA = Nothing

        End If

    End Sub

    Private Shared Function AlreadyRegisteredOnServer(ByVal PersonID As Guid) As Boolean

        If Parameters.UseDirectSQL Then

            Dim _Return As Boolean = False

            Dim _SQL As String = "select in_out from RegisterSummary" & _
                                 " where person_id = '" & PersonID.ToString & "'" & _
                                 " and date = '" & Format(Date.Today, "yyyy-MM-dd").ToString & "'"

            Dim _DT As DataTable = DirectSQL.ReturnDataTable(_SQL)
            If Not _DT Is Nothing Then
                If _DT.Rows.Count = 1 Then _Return = True
                _DT.Dispose()
                _DT = Nothing
            End If

            Return _Return

        Else
            'do not do this check unless we are direct
            Return False
        End If

    End Function

    Public Shared Function PersonInorOut(ByVal PersonID As Guid) As Enums.InOut

        Dim _Return As Enums.InOut

        Dim _SQL As String = "select in_out from RegisterSummary" & _
                             " where day_id = '" & TodayID & "'" & _
                             " and person_id = '" & PersonID.ToString & "'"

        Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
        If _DR Is Nothing Then
            _Return = Enums.InOut.CheckOut
        Else
            If _DR.Item("in_out").ToString = "I" Then
                _Return = Enums.InOut.CheckIn
            Else
                _Return = Enums.InOut.CheckOut
            End If
        End If

        _DR = Nothing

        Return _Return

    End Function

    Public Shared Sub LogFood(ByVal DayID As String, ByVal ChildID As String, ByVal ChildName As String,
                              ByVal MealID As String, ByVal MealType As String, ByVal MealName As String,
                              ByVal FoodID As String, ByVal FoodName As String, ByVal FoodStatus As String)

        Dim _SQL As String

        _SQL = "select * from FoodRegister" &
               " where day_id = '" & DayID & "'" &
               " and child_id = '" & ChildID & "'" &
               " and meal_type = '" & MealType & "'" &
               " and meal_id = '" & MealID & "'" &
               " and food_id = '" & FoodID & "'"

        Dim _DA As IDataAdapter = DAL.ReturnDataAdapter(_SQL)
        If Not _DA Is Nothing Then

            Dim _DR As DataRow
            Dim _DS As New DataSet
            _DA.Fill(_DS)

            If _DS.Tables(0).Rows.Count = 0 Then
                _DR = _DS.Tables(0).NewRow
                _DR("ID") = Guid.NewGuid
            Else
                _DR = _DS.Tables(0).Rows(0)
            End If

            _DR("day_id") = New Guid(DayID)
            _DR("child_id") = New Guid(ChildID)
            _DR("child_name") = ChildName
            _DR("meal_id") = New Guid(MealID)
            _DR("meal_type") = MealType
            _DR("meal_name") = MealName
            _DR("food_id") = New Guid(FoodID)
            _DR("food_name") = FoodName
            _DR("food_status") = FoodStatus

            If _DS.Tables(0).Rows.Count = 0 Then _DS.Tables(0).Rows.Add(_DR)

            DAL.UpdateDB(_DA, _DS)

        End If

    End Sub

    Public Shared Sub DeleteSleep(ByVal DayID As String, ByVal ChildID As String)

        Dim _SQL As String = "delete from Activity" & _
                             " where day_id = '" & Parameters.TodayID & "'" & _
                             " and key_id = '" & ChildID & "'" & _
                             " and type = 'SLEEP'"

        DAL.ExecuteCommand(_SQL)

    End Sub

    Public Shared Sub DeleteMilk(ByVal DayID As String, ByVal ChildID As String)

        Dim _SQL As String = "delete from Activity" & _
                             " where day_id = '" & Parameters.TodayID & "'" & _
                             " and key_id = '" & ChildID & "'" & _
                             " and type = 'MILK'"

        DAL.ExecuteCommand(_SQL)

    End Sub

    Public Shared Sub DeleteRequests(ByVal DayID As String, ByVal ChildID As String)

        Dim _SQL As String = "delete from Activity" & _
                             " where day_id = '" & Parameters.TodayID & "'" & _
                             " and key_id = '" & ChildID & "'" & _
                             " and type = 'REQ'"

        DAL.ExecuteCommand(_SQL)

    End Sub

    Public Shared Sub DeleteActivity(ByVal DayID As String, ByVal ChildID As String, ByVal ActivityType As EnumActivityType)

        Dim _SQL As String = "delete from Activity" & _
                             " where day_id = '" & Parameters.TodayID & "'" & _
                             " and key_id = '" & ChildID & "'" & _
                             " and type = '" + ReturnActivityTypeCode(ActivityType) + "'"

        DAL.ExecuteCommand(_SQL)

    End Sub

    Public Shared Sub DeleteActivity(ByVal ActivityID As String)
        Dim _SQL As String = "delete from Activity where ID = '" + ActivityID + "'"
        DAL.ExecuteCommand(_SQL)
    End Sub

    Private Shared Function ReturnActivityTypeCode(ByVal ActivityType As EnumActivityType) As String
        If ActivityType = EnumActivityType.CrossCheck Then Return "CROSSCHECK"
        If ActivityType = EnumActivityType.Feedback Then Return "FEEDBACK"
        If ActivityType = EnumActivityType.Milk Then Return "MILK"
        If ActivityType = EnumActivityType.Request Then Return "REQ"
        If ActivityType = EnumActivityType.Sleep Then Return "SLEEP"
        If ActivityType = EnumActivityType.Suncream Then Return "SUNCREAM"
        If ActivityType = EnumActivityType.Toilet Then Return "TOILET"
        If ActivityType = EnumActivityType.Toothbrush Then Return "TOOTHBRUSH"
        If ActivityType = EnumActivityType.CheckOutMedication Then Return "COMEDICINE"
        If ActivityType = EnumActivityType.CheckOutIncident Then Return "COINCIDENT"
        If ActivityType = EnumActivityType.Absence Then Return "ABSENCE"
        If ActivityType = EnumActivityType.Asleep Then Return "ASLEEP"
        If ActivityType = EnumActivityType.SleepCheck Then Return "SLEEPCHECK"
        If ActivityType = EnumActivityType.Payment Then Return "PAYMENT"
        If ActivityType = EnumActivityType.Unlock Then Return "UNLOCK"
        If ActivityType = EnumActivityType.LogOff Then Return "LOGOFF"
        Return "!!ERROR!!"
    End Function

    Public Shared Sub LogSleep(ByVal DayID As String, ByVal ChildID As String, ByVal ChildName As String, ByVal Times As String, _
                               ByVal StaffID As String, ByVal StaffName As String)

        Dim _Start As String = Mid(Times, 1, 5)
        Dim _End As String = Mid(Times, Times.Length - 4)

        LogActivity(DayID, ChildID, ChildName, EnumActivityType.Sleep, "Sleep", Times, _Start, _End, StaffID, StaffName)

        Dim _Mess As String = "I've just had a lovely nap. I slept from " + _Start + " til " + _End + "."
        SendSMS(ChildID, EnumSMSActivityType.Sleep, _Mess)

    End Sub

    Public Shared Sub LogMilk(ByVal DayID As String, ByVal ChildID As String, ByVal ChildName As String, ByVal floz As Double, Stamp As String, _
                              ByVal StaffID As String, ByVal StaffName As String)

        Dim _Desc As String = floz.ToString + " floz @ " + Stamp
        LogActivity(DayID, ChildID, ChildName, EnumActivityType.Milk, _Desc, floz.ToString + " floz", floz.ToString, Stamp, StaffID, StaffName)

        Dim _Mess As String = "I've just had a bottle. I drank " + floz.ToString + " floz."
        SendSMS(ChildID, EnumSMSActivityType.Milk, _Mess)

    End Sub

    Public Shared Sub LogSuncream(ByVal DayID As String, ByVal ChildID As String, ByVal ChildName As String, Stamp As String,
                                  ByVal StaffID As String, ByVal StaffName As String)

        Dim _Desc As String = "Suncream applied @ " + Stamp
        LogActivity(DayID, ChildID, ChildName, EnumActivityType.Suncream, _Desc, Stamp, "", "", StaffID, StaffName)

    End Sub

    Public Shared Sub LogToothbrush(ByVal DayID As String, ByVal ChildID As String, ByVal ChildName As String, Stamp As String,
                                  ByVal StaffID As String, ByVal StaffName As String)

        Dim _Desc As String = "Teeth brushed @ " + Stamp
        LogActivity(DayID, ChildID, ChildName, EnumActivityType.Toothbrush, _Desc, Stamp, "", "", StaffID, StaffName)

    End Sub

    Public Shared Sub LogAsleep(ByVal DayID As String, ByVal ChildID As String, ByVal ChildName As String, ByVal Location As String, _
                                ByVal StaffID As String, ByVal StaffName As String)

        Dim _Desc As String = ChildName + " Asleep"
        LogActivity(DayID, ChildID, ChildName, EnumActivityType.Asleep, _Desc, Location, , , StaffID, StaffName)

    End Sub

    Public Shared Sub LogSleepCheck(ByVal DayID As String, ByVal ChildID As String, ByVal ChildName As String, ByVal Location As String, _
                                    ByVal StaffID As String, ByVal StaffName As String)

        Dim _Desc As String = ChildName + " - Sleep Check"
        LogActivity(DayID, ChildID, ChildName, EnumActivityType.SleepCheck, _Desc, Location, , , StaffID, StaffName)

    End Sub

    Public Shared Sub LogRequest(ByVal DayID As String, ByVal ChildID As String, ByVal ChildName As String, _
                                 ByVal RequestID As String, ByVal RequestDescription As String, _
                                 ByVal StaffID As String, ByVal StaffName As String)

        LogActivity(DayID, ChildID, ChildName, EnumActivityType.Request, RequestDescription, RequestID, , , StaffID, StaffName)

    End Sub

    Public Shared Sub LogFeedback(ByVal DayID As String, ByVal ChildID As String, ByVal ChildName As String, _
                                 ByVal Feedback As String)

        LogActivity(DayID, ChildID, ChildName, EnumActivityType.Feedback, "Personal Feedback", , , , , , Feedback)

    End Sub

    Public Shared Function ReturnFeedback(ByVal DayID As String, ByVal ChildID As String, ByRef Feedback As String) As Boolean

        Dim _DR As DataRow = Nothing
        If ReturnActivityRow(DayID, ChildID, EnumActivityType.Feedback, _DR) Then
            Feedback = _DR.Item("notes").ToString
        Else
            Return False
        End If

    End Function

    Public Shared Function ReturnActivityCount(ByVal DayID As String, ByVal ChildID As String, ByVal ActivityType As EnumActivityType) As Integer

        Dim _Return As Integer = 0
        Dim _SQL As String = "select count(*) from Activity" & _
                             " where day_id = '" & DayID & "'" & _
                             " and key_id = '" & ChildID & "'" & _
                             " and type = '" & ReturnActivityTypeCode(ActivityType) & "'"

        Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
        If Not _DR Is Nothing Then
            If _DR.Item(0).ToString <> "" Then
                _Return = CInt(_DR.Item(0))
            End If
        End If

        Return _Return

    End Function

    Public Shared Function ReturnFoodCount(ByVal DayID As String, ByVal ChildID As String, MealType As EnumMealType) As Integer

        Dim _Return As Integer = 0
        Dim _SQL As String = "select count(*) from FoodRegister" & _
                             " where day_id = '" & DayID & "'" & _
                             " and child_id = '" & ChildID & "'" & _
                             " and meal_type = '" & ReturnMealCode(MealType) & "'"

        Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
        If Not _DR Is Nothing Then
            If _DR.Item(0).ToString <> "" Then
                _Return = CInt(_DR.Item(0))
            End If
        End If

        Return _Return

    End Function

    Public Shared Function ReturnIncidentCount(ByVal DayID As String, ByVal ChildID As String) As Integer

        Dim _Return As Integer = 0
        Dim _SQL As String = "select count(*) from Incidents" & _
                             " where day_id = '" & DayID & "'" & _
                             " and child_id = '" & ChildID & "'"

        Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
        If Not _DR Is Nothing Then
            If _DR.Item(0).ToString <> "" Then
                _Return = CInt(_DR.Item(0))
            End If
        End If

        Return _Return

    End Function

    Public Shared Function ReturnMedicineCount(ByVal DayID As String, ByVal ChildID As String, Optional AdHocOnly As Boolean = False) As Integer

        Dim _Return As Integer = 0
        Dim _SQL As String = "select count(*) from MedicineLog" &
                             " where day_id = '" & DayID & "'" &
                             " and child_id = '" & ChildID & "'" &
                             " and staff_name is not null"

        If AdHocOnly Then _SQL &= " and auth_id is null"

        Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
        If Not _DR Is Nothing Then
            If _DR.Item(0).ToString <> "" Then
                _Return = CInt(_DR.Item(0))
            End If
        End If

        Return _Return

    End Function

    Public Shared Sub ConvertToSleep(ByVal ID As String, ByVal StaffID As String, ByVal StaffName As String)

        'we convert the existing asleep record to a sleep record...
        Dim _SQL As String = "select * from Activity where ID = '" + ID + "'"
        Dim _DA As IDataAdapter = DAL.ReturnDataAdapter(_SQL)

        If Not _DA Is Nothing Then

            Dim _DS As New DataSet
            _DA.Fill(_DS)

            Dim _DR As DataRow = _DS.Tables(0).Rows(0)

            Dim _Started As Date = CDate(_DR.Item("stamp"))
            Dim _Ended As Date = Now

            'store the location and staff details for the original asleep
            Dim _Notes As String = _DR.Item("staff_name").ToString + " - " + _DR.Item("value_1").ToString
            _DR.Item("notes") = _Notes

            'change to a sleep record
            _DR.Item("type") = "SLEEP"
            _DR.Item("description") = "Sleep"
            _DR.Item("value_1") = Format(_Started, "HH:mm").ToString + "-" + Format(_Ended, "HH:mm").ToString
            _DR.Item("value_2") = Format(_Started, "HH:mm").ToString
            _DR.Item("value_3") = Format(_Ended, "HH:mm").ToString
            _DR.Item("staff_id") = StaffID
            _DR.Item("staff_name") = StaffName
            _DR.Item("stamp") = Now

            DAL.UpdateDB(_DA, _DS)

            _DS.Dispose()
            _DS = Nothing

            _DA = Nothing

        End If

    End Sub

    Public Shared Function ReturnObservationCount(ByVal DayID As String, ByVal ChildID As String) As Integer
        Return 0
    End Function

    Public Shared Function HasActivity(ByVal DayID As String, ByVal ChildID As String, ByVal ActivityType As EnumActivityType) As Boolean
        If ReturnActivityCount(DayID, ChildID, ActivityType) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Shared Function ReturnActivityRow(ByVal DayID As String, ByVal ChildID As String, ByVal ActivityType As EnumActivityType, ByRef ReturnRow As DataRow) As Boolean

        Dim _Return As Boolean = False
        Dim _SQL As String = "select * from Activity" & _
                             " where day_id = '" & DayID & "'" & _
                             " and key_id = '" & ChildID & "'" & _
                             " and type = '" & ReturnActivityTypeCode(ActivityType) & "'"

        Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
        If Not _DR Is Nothing Then
            ReturnRow = _DR
            _Return = True
        End If

        Return _Return

    End Function

    Public Shared Sub SendSMS(ByVal ChildID As String, ByVal ActivityType As EnumSMSActivityType, Message As String)

        Dim _SMS As Boolean = ReturnBoolean(ReturnParameter("SMS"))
        If _SMS Then

            Dim _SQL As String = ""

            _SQL = "select family_id, forename, sms_milk, sms_food, sms_toilet, sms_sleep from Children" & _
                   " where ID = '" & ChildID & "'"

            Dim _Child As DataRow = DAL.ReturnDataRow(_SQL)
            If _Child IsNot Nothing Then

                Dim _SendSMS As Boolean = False
                Select Case ActivityType

                    Case EnumSMSActivityType.Food
                        Dim _Option As Boolean = ReturnBoolean(_Child.Item("sms_food"))
                        If _Option Then _SendSMS = True

                    Case EnumSMSActivityType.Milk
                        Dim _Option As Boolean = ReturnBoolean(_Child.Item("sms_milk"))
                        If _Option Then _SendSMS = True

                    Case EnumSMSActivityType.Sleep
                        Dim _Option As Boolean = ReturnBoolean(_Child.Item("sms_sleep"))
                        If _Option Then _SendSMS = True

                    Case EnumSMSActivityType.Toilet
                        Dim _Option As Boolean = ReturnBoolean(_Child.Item("sms_toilet"))
                        If _Option Then _SendSMS = True

                End Select

                If _SendSMS Then

                    Dim _SMSSender As String = ReturnParameter("SMSSENDER")
                    Dim _FamilyID As String = _Child.Item("family_id").ToString
                    Dim _ChildName As String = _Child.Item("forename").ToString
                    Dim _Mess As String = Message + vbCrLf + "Love " + _ChildName + " xxx"

                    _SQL = "select forename, tel_mobile from Contacts" & _
                           " where family_id = '" & _FamilyID & "'" & _
                           " and sms = 1"

                    Dim _Contacts As DataTable = DAL.ReturnDataTable(_SQL)
                    For Each _Contact As DataRow In _Contacts.Rows
                        Dim _Tel As String = _Contact.Item("tel_mobile").ToString
                        _Tel = _Tel.Replace(" ", "")
                        If _Tel <> "" Then
                            SMSHandler.SendSMS(_Tel, _Mess)
                        End If
                    Next

                End If

            End If

        End If

    End Sub

#End Region

    Public Shared Function ReturnBoolean(ByVal ObjectIn As Object) As Boolean

        If ObjectIn Is Nothing Then
            Return False
        Else

            If TypeOf ObjectIn Is String Then

                Select Case ObjectIn.ToString.ToUpper

                    Case "1", "Y", "T", "TRUE"
                        Return True

                    Case Else
                        Return False

                End Select

            Else

                If TypeOf ObjectIn Is Boolean Then
                    Return CBool(ObjectIn)
                End If

            End If

        End If

    End Function

    Public Shared Function ReturnParameter(ByVal ParameterName As String) As String

        Dim _Return As String = ""
        Dim _SQL As String = "select value from AppParams" & _
                             " where name = '" & ParameterName & "'"

        Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
        If Not _DR Is Nothing Then
            If _DR.Item(0).ToString <> "" Then
                _Return = _DR.Item(0).ToString
            End If
        End If

        Return _Return

    End Function

    Public Shared Function ShowWindowsKeyboard() As Boolean

    End Function

    Public Shared Sub HideWindowsKeyboard()

    End Sub

    Public Shared Function CheckinsPermitted() As Boolean
        If Parameters.DisableCheckIns Then
            Msgbox("Register functionality has been disable on this device." + vbCrLf + "Please ensure you are using the correct Device.", MessageBoxIcon.Exclamation, "Register Functionality")
        Else
            Return True
        End If
    End Function

    Public Shared Function DoCheckOut(ByVal ChildID As String, ByVal ChildName As String) As Boolean

        If Not CheckinsPermitted() Then Return False

        Dim _Return As Boolean = False

        Dim _frm As New frmCheckOut(ChildID, ChildName)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then _Return = True

        _frm.Dispose()
        _frm = Nothing

        Return _Return

    End Function

    Public Shared Function ReturnMealCode(ByVal MealName As String) As String

        'baby food
        If MealName = "J" Then Return "J"

        If MealName = "Breakfast" Then Return "B"
        If MealName = "Snack" Then Return "S"
        If MealName = "Lunch" Then Return "L"
        If MealName = "Lunch Dessert" Then Return "LD"
        If MealName = "Tea" Then Return "T"
        If MealName = "Tea Dessert" Then Return "TD"

        Return "X"

    End Function

    Public Shared Function ReturnMealCode(ByVal MealType As EnumMealType) As String

        'baby food
        If MealType = SharedModule.EnumMealType.BabyFood Then Return "J"

        If MealType = SharedModule.EnumMealType.Breakfast Then Return "B"
        If MealType = SharedModule.EnumMealType.Snack Then Return "S"
        If MealType = SharedModule.EnumMealType.Lunch Then Return "L"
        If MealType = SharedModule.EnumMealType.LunchDessert Then Return "LD"
        If MealType = SharedModule.EnumMealType.Tea Then Return "T"
        If MealType = SharedModule.EnumMealType.TeaDessert Then Return "TD"

        Return Nothing

    End Function

    Public Shared Function ReturnMealTypeEnum(ByVal MealCode As String) As EnumMealType

        'baby food
        If MealCode = "J" Then Return SharedModule.EnumMealType.BabyFood

        If MealCode = "B" Then Return SharedModule.EnumMealType.Breakfast
        If MealCode = "S" Then Return SharedModule.EnumMealType.Snack
        If MealCode = "L" Then Return SharedModule.EnumMealType.Lunch
        If MealCode = "LD" Then Return SharedModule.EnumMealType.LunchDessert
        If MealCode = "T" Then Return SharedModule.EnumMealType.Tea
        If MealCode = "TD" Then Return SharedModule.EnumMealType.TeaDessert

        Return Nothing

    End Function

    Public Shared Function LegacySignatureCapture(ByRef SignatureData As String) As Boolean

        Dim _Return As Boolean = False

        Dim _Signature As New frmSignature
        With _Signature
            .SignatureData = SignatureData
            .ShowDialog()
        End With

        If _Signature.DialogResult = DialogResult.OK Then
            SignatureData = _Signature.SignatureData
            _Return = True
        End If

        _Signature.Dispose()
        _Signature = Nothing

        Return _Return

    End Function

    Public Shared Function ChooseFoodItem() As Pair

        Dim _SQL As String
        Dim _Pair As Pair = Nothing

        Dim _GroupID As String = ""
        Dim _GroupName As String = ""

        'get food group
        _SQL = "select distinct group_id, group_name from Food order by group_name"

        _Pair = ReturnButtonSQL(_SQL, "Select Food Group")
        If _Pair Is Nothing Then
            Return Nothing
        Else
            _GroupID = _Pair.Code
            _GroupName = _Pair.Text
            _Pair = Nothing
        End If

        _SQL = "select id, name from Food where group_id = '" + _GroupID + "' order by name"

        Return ReturnButtonSQL(_SQL, "Select Food")

    End Function

    Public Shared Function ReturnRoomFilter(ByVal PersonType As Enums.PersonType) As String

        Dim _SQL As String = ""
        Dim _RoomName As String = ""

        If Parameters.DefaultRoom <> "" Then

            _SQL = ""
            _SQL += "SELECT ID, room_name from SiteRooms"
            _SQL += " where site_id = " + Parameters.SiteIDQuotes
            _SQL += " and room_name = '" + Parameters.DefaultRoom + "'"

            If PersonType = Enums.PersonType.Child Then _SQL += " and room_check_children = 1"
            If PersonType = Enums.PersonType.Staff Then _SQL += " and room_check_staff = 1"

            _SQL += " order by room_sequence"

            Dim _DR As DataRow = DAL.ReturnDataRow(_SQL)
            If _DR IsNot Nothing Then
                _RoomName = _DR.Item("room_name").ToString
            End If

        End If

        If _RoomName = "" Then

            Dim _Group As Pair = ReturnRoom(Parameters.SiteID, Enums.PersonType.Child)
            If _Group Is Nothing Then Return Nothing
            If _Group.Code = "" Then Return Nothing

            _RoomName = _Group.Text

        End If

        Return _RoomName

    End Function

    Public Shared Function ReturnRoom(ByVal SiteID As String, ByVal PersonType As Enums.PersonType, Optional ByVal ExcludedRoom As String = "") As Pair

        Dim _SQL As String = ""

        _SQL += "SELECT ID, room_name from SiteRooms"
        _SQL += " where site_id = '" + SiteID + "'"

        If PersonType = Enums.PersonType.Child Then _SQL += " and room_check_children = 1"
        If PersonType = Enums.PersonType.Staff Then _SQL += " and room_check_staff = 1"

        If ExcludedRoom <> "" Then
            _SQL += " and room_name <> '" + ExcludedRoom + "'"
        End If

        _SQL += " order by room_sequence"

        Return ReturnButtonSQL(_SQL, "Select Group")

    End Function

    Public Shared Function ReturnHeadCount(ByVal VisitorType As EnumPersonType, ByVal CurrentLocation As String) As Decimal

        Dim _Type As String = "'C'"
        If VisitorType = EnumPersonType.Staff Then _Type = "'S'"
        If VisitorType = EnumPersonType.Visitor Then _Type = "'V'"

        Dim _SQL As String = ""

        _SQL += "select count(*) from RegisterSummary r"

        If VisitorType = EnumPersonType.Child Then _SQL += " left join Children c on c.ID = r.person_id"
        If VisitorType = EnumPersonType.Staff Then _SQL += " left join Staff s on s.ID = r.person_id"

        _SQL += " where day_id = '" + Parameters.TodayID + "'"

        If VisitorType <> EnumPersonType.Visitor Then
            _SQL += " and site_id = " + Parameters.SiteIDQuotes
        End If

        _SQL += " and person_type = " + _Type
        _SQL += " and in_out = 'I'"

        If CurrentLocation <> "" Then _SQL += " and location = " + CurrentLocation

        Return ReturnSQLCount(_SQL)

    End Function

    Public Shared Function ReturnSQLCount(ByVal SQL As String) As Decimal

        Dim _o As Object = DirectSQL.ReturnScalar(SharedModule.ConnectionString, SQL)
        If _o IsNot Nothing Then
            Return ConvertDecimal(_o)
        Else
            Return 0
        End If

    End Function

    Private Shared Function ConvertDecimal(ByVal ValueIn As Object) As Decimal

        If ValueIn Is Nothing Then Return 0
        If ValueIn.ToString = "" Then Return 0

        Dim _Return As Decimal = 0

        Try
            _Return = Decimal.Parse(ValueIn.ToString)

        Catch ex As Exception
            'error oDecimal.Parsered
        End Try

        Return _Return

    End Function

    Public Shared Function StaffCheckedIn() As Boolean
        Dim _Count As Integer = RegisterCount("S")
        If _Count > 0 Then Return True
        Return False
    End Function

    Public Shared Function ChildrenCheckedIn() As Boolean
        Dim _Count As Integer = RegisterCount("C")
        If _Count > 0 Then Return True
        Return False
    End Function

    Private Shared Function RegisterCount(ByVal PersonType As String) As Integer

        Dim _SQL As String = ""

        _SQL += "select count(*) from RegisterSummary"
        _SQL += " where day_id = '" + TodayID + "'"
        _SQL += " and person_type = '" + PersonType + "'"
        _SQL += " and in_out = 'I'"

        Dim _o As Object = DirectSQL.ReturnScalar(ConnectionString, _SQL)
        If _o IsNot Nothing Then
            Return CInt(_o)
        Else
            Return 0
        End If

    End Function

End Class
