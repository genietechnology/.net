﻿
Public Class Parameters

    'LEAVE THESE AS VARIABLES, SO WE KNOW THEY COME FROM THE APP.CONFIG
    '*************************************************************************
    Public Shared DirectConnectionString As String = ""
    Public Shared ServerIP As String = ""
    Public Shared ServiceIP As String = ""
    Public Shared OfflineMode As Boolean = False
    '*************************************************************************

    Public Shared Property UseDirectSQL As Boolean = True
    Public Shared Property UseWebServices As Boolean = False

    Public Shared Property DebugMode As Boolean = False
    Public Shared Property SpellChecking As Boolean = False
    Public Shared Property UseBasicMeals As Boolean = False
    Public Shared Property CrossCheckMandatory As Boolean = False

    'Public Shared Property UsageMode As SharedModule.EnumUsage = SharedModule.EnumUsage.StandardTouch
    Public Shared Property DisableCheckIns As Boolean = False
    Public Shared Property DisableScreenSaver As Boolean = False
    Public Shared Property DisablePotty As Boolean = False

    Public Shared Property MaximiseForms As Boolean = False

    Public Shared Property TodayID As String = ""
    Public Shared Property TodaySQLDate As String = Format(Date.Today, "yyyy-MM-dd")
    Public Shared Property TodaySQLStart As String = TodaySQLDate & " 00:00:00.000"
    Public Shared Property TodaySQLEnd As String = TodaySQLDate & " 23:59:59.000"

    Public Shared Property DefaultRoomParameter As String = ""
    Public Shared Property HideTelephoneNumbers As Boolean = False
    Public Shared Property DisableLocation As Boolean = False
    Public Shared Property SortSurname As Boolean = False
    Public Shared Property Site As String = ""
    Public Shared Property SiteID As String = ""

    Public Shared Property UnlockMode As SharedModule.EnumUnlockMode = SharedModule.EnumUnlockMode.Disabled
    Public Shared Property UnlockCode As String = ""

    Public Shared Property CheckSignatureChildren As Boolean = False
    Public Shared Property CheckSignatureStaff As Boolean = False

    Public Shared Property CurrentStaffID As String = ""
    Public Shared Property CurrentStaffName As String = ""

    Public Shared Property ShowPhotos As Boolean = True
    Public Shared Property TopLeft As Boolean = False

    Public Shared Property IncidentsMandatoryManager As Boolean = False
    Public Shared Property IncidentsMandatoryWitness As Boolean = False
    Public Shared Property IncidentsMandatoryEquipment As Boolean = False
    Public Shared Property IncidentsMandatoryRisk As Boolean = False

    Public Shared Property CheckChildPIN As Boolean = False
    Public Shared Property HideChildName As Boolean = False

    Public Shared Property LateMedicationReason As String = "0"
    Public Shared Property MedicationSignature As Boolean = True

    Public Shared Property ContactSignatureIn As Boolean = False
    Public Shared Property ContactSignatureOut As Boolean = False

    Public Shared ReadOnly Property SiteIDQuotes As String
        Get
            Return "'" + SiteID + "'"
        End Get
    End Property

    Public Shared ReadOnly Property DefaultRoom As String
        Get
            If DisableLocation Then
                Return ""
            Else
                Return DefaultRoomParameter
            End If
        End Get
    End Property

    Public Class Appearance
        Public Shared Property ScreenSaverColour As String = ""
    End Class

    Public Class Paths
        Public Shared Property SharedFolderPath As String = ""
        Public Shared Property VLCExePath As String = ""
        Public Shared Property Policies As String = ""
    End Class

    Public Class Capture
        Public Shared Property CaptureEnabled As Boolean = False
        Public Shared Property CameraCommand As String = ""
        Public Shared Property CameraDevice As String = ""
    End Class

    Public Class AccessControl

        Public Shared Property DoorControl As Boolean = False
        Public Shared Property DoorControlIP As String = ""

        Public Shared Property Paxton As Boolean = False
        Public Shared Property PaxtonUser As String = ""
        Public Shared Property PaxtonPassword As String = ""

        Public Shared Property Door1Name As String = ""
        Public Shared Property Door1Serial As String = ""
        Public Shared Property Door2Name As String = ""
        Public Shared Property Door2Serial As String = ""

        Public Shared Property MonitorSerial As String = ""

    End Class

    Public Class CCTV
        Public Shared Property Video1IP As String = ""
        Public Shared Property Video2IP As String = ""
        Public Shared Property Video3IP As String = ""
        Public Shared Property CustomAddress As Boolean = False
    End Class

End Class
