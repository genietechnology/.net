﻿Option Strict On

Imports NurseryTouchscreen.SharedModule

Public Class DoorHandler

    Private Shared WithEvents m_DoorHandler As Care.TAAC.Controller
    Private Shared m_Monitoring As Boolean = False

    Public Shared ReadOnly Property Monitoring As Boolean
        Get
            Return m_Monitoring
        End Get
    End Property

    Public Shared ReadOnly Property Connected As Boolean
        Get
            If m_DoorHandler Is Nothing Then
                Return False
            Else
                Return m_DoorHandler.Connected
            End If
        End Get
    End Property

    Public Shared ReadOnly Property IPAddress As String
        Get
            Return Parameters.AccessControl.DoorControlIP
        End Get
    End Property

    Public Shared ReadOnly Property Enabled As Boolean
        Get
            Return Parameters.AccessControl.DoorControl
        End Get
    End Property

    Public Shared Sub Disconnect()
        If Connected Then m_DoorHandler.Disconnect()
        For Each P As Process In System.Diagnostics.Process.GetProcessesByName("vlc")
            P.Kill()
        Next
    End Sub

    Public Shared Function Connect(ByRef Errors As String) As Boolean

        Dim _Return As Boolean = False
        m_DoorHandler = New Care.TAAC.Controller(Parameters.AccessControl.Paxton, Parameters.AccessControl.DoorControlIP, Parameters.AccessControl.PaxtonUser, Parameters.AccessControl.PaxtonPassword)
        _Return = m_DoorHandler.Connect(Errors)

        If _Return Then
            Dim _MonitorSerial As String = Parameters.AccessControl.MonitorSerial
            If _MonitorSerial <> "" Then
                m_Monitoring = m_DoorHandler.MonitorDoor(CInt(_MonitorSerial))
            End If
        End If

        Return _Return

    End Function

    Public Shared Sub Unlock(ByVal DoorSerial As String)
        If Connected Then m_DoorHandler.Unlock(DoorSerial)
    End Sub

    Private Shared Sub m_DoorHandler_AccessEvent(sender As Object, e As Paxton.Net2.OemClientLibrary.IEventView) Handles m_DoorHandler.AccessEvent

        Select Case e.EventType

            'access permitted card only
            Case 20
                StaffEnteredRoom(e.UserId.ToString)

        End Select

    End Sub

    Private Shared Sub StaffEnteredRoom(ByVal PaxtonUserID As String)

        'get the staff record from the paxton user id
        Dim _StaffID As Guid? = Business.Staff.ReturnIDFromPaxtonID(PaxtonUserID)

        If _StaffID.HasValue Then

            Dim _Name As String = Business.Staff.ReturnName(_StaffID.ToString)

            'get the current location for this member of staff
            Dim _CurrentLocation As String = Business.Staff.ReturnCurrentLocation(_StaffID.Value)

            'if this person is already in another location, we will need to move them from said location
            Select Case _CurrentLocation

                'person is not in, so we check them in
                Case "OUT"
                    SharedModule.RegisterTransaction(_StaffID.Value, Enums.PersonType.Child, _Name, Enums.InOut.CheckIn, Enums.RegisterVia.Paxton)

                    'person is in - but we have no location
                Case ""
                    SharedModule.RegisterTransaction(_StaffID.Value, Enums.PersonType.Staff, _Name, Enums.InOut.CheckIn, Enums.RegisterVia.Paxton, Parameters.DefaultRoom, "MI", "< Move In", "")

                Case Else

                    If _CurrentLocation <> Parameters.DefaultRoom Then
                        SharedModule.RegisterTransaction(_StaffID.Value, Enums.PersonType.Staff, _Name, Enums.InOut.CheckOut, Enums.RegisterVia.Paxton, "", "MO", "Move Out >", "")
                        SharedModule.RegisterTransaction(_StaffID.Value, Enums.PersonType.Staff, _Name, Enums.InOut.CheckIn, Enums.RegisterVia.Paxton, Parameters.DefaultRoom, "MI", "< Move In", "")
                    End If

            End Select

        End If

    End Sub

End Class
