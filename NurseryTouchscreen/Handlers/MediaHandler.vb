﻿Public Class MediaHandler

    Private Shared m_LocalMedia As New List(Of MediaContent)

    Public Shared ReadOnly Property LocalMediaContent As List(Of MediaContent)
        Get
            Return m_LocalMedia
        End Get
    End Property

    Public Enum EnumCaptureMethod
        Photo
        Video
        Audio
    End Enum

    Public Shared Sub UploadMedia()

        Dim _ToRemove As New List(Of MediaContent)

        'whip through the photos in Local Media, if we successfully store to DB, mark it for removal
        For Each _m In m_LocalMedia
            If SaveToDB(_m) Then
                _ToRemove.Add(_m)
            End If
        Next

        For Each _m In _ToRemove
            m_LocalMedia.Remove(_m)
        Next

    End Sub

    Private Shared Function SaveToDB(ByVal Media As MediaContent) As Boolean

        Dim _Return As Boolean = True
        Dim _ID As Guid = Guid.NewGuid

        Try

            Dim _SQL As String
            _SQL = "select * from Media where ID = '" & _ID.ToString & "'"

            Dim _DA As IDataAdapter = DAL.ReturnDataAdapter(_SQL)
            If Not _DA Is Nothing Then

                Dim _DR As DataRow
                Dim _DS As New DataSet
                _DA.Fill(_DS)

                _DR = _DS.Tables(0).NewRow

                _DR("id") = _ID
                _DR("day_id") = Parameters.TodayID
                _DR("key_type") = Media.Target
                _DR("key_id") = Media.TargetPair.Code
                _DR("key_name") = Media.TargetPair.Text
                _DR("data") = Media.Data
                _DR("data_size") = Media.Data.LongLength
                _DR("device_name") = My.Computer.Name
                _DR("upload_stamp") = Now

                If _DS.Tables(0).Rows.Count = 0 Then _DS.Tables(0).Rows.Add(_DR)

                _Return = DAL.UpdateDB(_DA, _DS, False)

            End If

        Catch ex As Exception
            _Return = False
        End Try

        Return _Return

    End Function

    Public Shared Sub TakePhoto()

        Dim _frmPhoto As New frmPhoto
        _frmPhoto.ShowDialog()

        If _frmPhoto.DialogResult = DialogResult.OK Then

            Dim _frmTarget As New frmPhotoTarget
            _frmTarget.ShowDialog()

            If _frmTarget.DialogResult = DialogResult.OK Then
                Dim _m As New MediaContent(_frmTarget.Target, _frmTarget.TargetPair, _frmPhoto.CapturedImage)
                m_LocalMedia.Add(_m)
            End If

        End If

    End Sub

#Region "Helpers"

    Public Shared Function ReturnByteArray(ByVal FilePath As String) As Byte()

        Dim _bytes As Byte() = Nothing

        If FilePath <> "" Then
            If IO.File.Exists(FilePath) Then
                _bytes = IO.File.ReadAllBytes(FilePath)
            End If
        End If

        Return _bytes

    End Function

    Public Shared Function ReturnByteArray(ByRef ImageObject As Drawing.Image) As Byte()

        Dim _bytes As Byte() = Nothing

        If ImageObject IsNot Nothing Then

            Try
                Dim _ms As IO.MemoryStream = New IO.MemoryStream()
                ImageObject.Save(_ms, System.Drawing.Imaging.ImageFormat.Png)
                _bytes = _ms.GetBuffer

                _ms.Close()
                _ms.Dispose()

            Catch ex As Exception

            End Try

        End If

        Return _bytes

    End Function

    Public Shared Function ReturnImage(ByVal PhotoPath As String) As Image

        If PhotoPath = "" Then Return Nothing

        'we put the file in a byte array so the file is not in use (so we can delete it etc)
        Dim _bytes As Byte() = ReturnByteArray(PhotoPath)
        Dim _image As Image = ReturnImage(_bytes)

        _bytes = Nothing

        Return _image

    End Function

    Public Shared Function ReturnImage(ByVal ByteArrayIn As Object) As Image

        If ByteArrayIn Is Nothing Then Return Nothing
        If TypeOf ByteArrayIn Is Byte() Then

            Try

                Dim _Bytes As Byte() = CType(ByteArrayIn, Byte())
                Dim _ms As New IO.MemoryStream(_Bytes)
                Dim _Image As Image = Image.FromStream(_ms)

                _Bytes = Nothing
                _ms.Close()
                _ms.Dispose()

                Return _Image

            Catch ex As Exception
                Return Nothing
            End Try

        Else
            Return Nothing
        End If

    End Function

#End Region

End Class
