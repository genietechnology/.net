﻿Imports System.IO
Imports System.ServiceModel
Imports System.Windows.Forms

Public Class ErrorHandler

    Private Shared m_LogPath As String = ""

    Public Shared Sub Activate()

        Try

            If Not IO.Directory.Exists(Application.StartupPath + "\log") Then
                IO.Directory.CreateDirectory(Application.StartupPath + "\log")
            End If

            m_LogPath = Application.StartupPath + "\log\" + Replace(Application.ProductName, " ", "") + "_" + Format(Now, "yyyyMMdd") + ".log"

            'If Debugger.IsAttached Then Exit Sub

            AddHandler Application.ThreadException, AddressOf ThreadException
            AddHandler AppDomain.CurrentDomain.UnhandledException, AddressOf UnhandledExeception

        Catch ex As Exception

        End Try

    End Sub

    Public Shared Sub DeActivate()

    End Sub

    Public Shared Sub LogException(ex As TimeoutException)
        LogExceptionToFile(ex)
    End Sub

    Public Shared Sub LogException(ex As CommunicationException)
        LogExceptionToFile(ex)
    End Sub

    Public Shared Sub LogException(ex As Exception)
        LogExceptionToFile(ex)
    End Sub

    Private Shared Sub UnhandledExeception(sender As Object, e As UnhandledExceptionEventArgs)
        Try
            LogExceptionToFile(CType(e.ExceptionObject, Exception))
        Catch ex As Exception

        End Try
    End Sub

    Private Shared Sub ThreadException(sender As Object, e As Threading.ThreadExceptionEventArgs)
        Try
            LogExceptionToFile(e.Exception)
        Catch ex As Exception

        End Try
    End Sub

    Private Shared Sub LogExceptionToFile(ByRef Exception As System.Exception)

        Try

            'check the file
            Dim fs As FileStream = New FileStream(m_LogPath, FileMode.OpenOrCreate, FileAccess.ReadWrite)
            Dim s As StreamWriter = New StreamWriter(fs)
            s.Close()
            fs.Close()

            'log it
            Dim fs1 As FileStream = New FileStream(m_LogPath, FileMode.Append, FileAccess.Write)
            Dim s1 As StreamWriter = New StreamWriter(fs1)
            s1.WriteLine("Date/Time: " + Now.ToString)
            s1.WriteLine("Message: " + Exception.Message)
            s1.WriteLine("StackTrace: " + Exception.StackTrace)
            s1.WriteLine("================================================================================================")
            s1.Close()
            fs1.Close()

        Catch ex As Exception

        End Try

    End Sub

    Public Shared Function ReturnBaseHierarchy(ByVal Base As System.Type) As String
        Dim _BaseChain As String = GetBase(Base, "")
        Return _BaseChain
    End Function

    Private Shared Function GetBase(ByVal Base As System.Type, ByRef BaseChain As String) As String

        If Not Base.BaseType Is Nothing Then
            If Base.FullName <> "System.Windows.Forms.Form" Then
                BaseChain += ">> " & Base.FullName & vbCrLf
                GetBase(Base.BaseType, BaseChain)
            End If
        End If

        Return BaseChain

    End Function

End Class
