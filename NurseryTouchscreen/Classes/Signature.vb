﻿Imports NurseryTouchscreen.SharedModule

Public Class Signature

    Public Enum EnumPersonType
        Parent
        Staff
        Visitor
    End Enum

    Private m_SignatureID As Guid? = Nothing
    Private m_SecureSignatureData As String = ""
    Private m_SignatureImage As Byte()

    Private m_PersonID As Guid? = Nothing
    Private m_PersonType As EnumPersonType
    Private m_PersonName As String = ""

    Private m_FamilyID As String = ""

    'indicators
    Private m_SignatureCaptured As Boolean = False
    Private m_SignatureSaved As Boolean = False
    Private m_PersonSelected As Boolean = False

    Public Sub New()
        m_SignatureID = Guid.NewGuid
    End Sub

    Public Sub New(ByVal SignatureID As Guid)
        m_SignatureID = SignatureID
    End Sub

#Region "Properties"

    Public ReadOnly Property SecureSignatureData As String
        Get
            Return m_SecureSignatureData
        End Get
    End Property

    Public ReadOnly Property SignatureID As Guid
        Get
            Return m_SignatureID
        End Get
    End Property

    Public ReadOnly Property SignatureCaptured As Boolean
        Get
            Return m_SignatureCaptured
        End Get
    End Property

    Public ReadOnly Property SignatureSaved As Boolean
        Get
            Return m_SignatureSaved
        End Get
    End Property

    Public ReadOnly Property PersonID As Guid?
        Get
            Return m_PersonID
        End Get
    End Property

    Public ReadOnly Property PersonType As String
        Get
            Return m_PersonType.ToString
        End Get
    End Property

    Public ReadOnly Property PersonName As String
        Get
            Return m_PersonName
        End Get
    End Property

#End Region

#Region "Public Methods"

    Public Sub LoadSignature()


    End Sub

    Public Function StoreSignature() As Boolean
        Return Save()
    End Function

    Public Function CaptureParentSignature(ByVal SaveSignature As Boolean, ByVal FamilyID As String) As Boolean
        m_FamilyID = FamilyID
        Return CaptureNow(SaveSignature, EnumPersonType.Parent)
    End Function

    Public Function CaptureStaffSignature(ByVal SaveSignature As Boolean) As Boolean
        Return CaptureNow(SaveSignature, EnumPersonType.Staff)
    End Function

    Public Function CaptureVisitorSignature(ByVal SaveSignature As Boolean, ByVal ActivityID As Guid?, ByVal Name As String) As Boolean
        If CaptureNow(SaveSignature, EnumPersonType.Visitor) Then
            m_PersonType = EnumPersonType.Visitor
            m_PersonID = ActivityID
            m_PersonName = Name
            m_PersonSelected = True
            Return True
        Else
            ClearPerson()
            Return False
        End If
    End Function

#End Region

    Private Function CaptureNow(ByVal SaveSignature As Boolean, ByVal PersonType As EnumPersonType) As Boolean

        If SelectPerson(PersonType) Then
            If CaptureSignature() Then
                If SaveSignature Then
                    If StoreSignature() Then
                        Return True
                    End If
                Else
                    Return True
                End If
            End If
        End If

        Return False

    End Function

    Private Function SelectPerson(ByVal PersonType As EnumPersonType) As Boolean

        Dim _Return As Guid? = Nothing
        Dim _Person As New Pair

        Select Case PersonType

            Case EnumPersonType.Parent
                _Person = Business.family.FindContact(m_FamilyID)

            Case EnumPersonType.Staff
                _Person = Business.Staff.FindStaff(Enums.PersonMode.OnlyCheckedIn)

            Case EnumPersonType.Visitor
                Return True

        End Select

        If _Person IsNot Nothing Then
            If _Person.Code <> "" Then
                m_PersonID = New Guid(_Person.Code)
                m_PersonName = _Person.Text
                m_PersonType = PersonType
                m_PersonSelected = True
                Return True
            Else
                ClearPerson()
            End If
        Else
            ClearPerson()
        End If

        Return False

    End Function

    Private Sub ClearPerson()
        m_PersonID = Nothing
        m_PersonName = ""
        m_PersonType = Nothing
        m_PersonSelected = False
    End Sub

    Private Function CaptureSignature() As Boolean

        Dim _Return As Boolean = False
        m_SignatureCaptured = False

        Dim _Signature As New frmSignature
        _Signature.ShowDialog()

        If _Signature.DialogResult = DialogResult.OK Then
            m_SignatureCaptured = True
            m_SecureSignatureData = _Signature.SignatureData
            m_SignatureImage = _Signature.SignatureBytes
            _Return = True
        End If

        _Signature.Dispose()
        _Signature = Nothing

        Return _Return

    End Function

    Private Function Save()

        m_SignatureSaved = False
        Dim _OK As Boolean = True
        Dim _SQL As String

        _SQL = "select * from Signatures" & _
               " where ID = '" & SignatureID.ToString & "'"

        Dim _DA As IDataAdapter = DAL.ReturnDataAdapter(_SQL)
        If Not _DA Is Nothing Then

            Dim _DR As DataRow
            Dim _DS As New DataSet
            _DA.Fill(_DS)

            If _DS.Tables(0).Rows.Count = 0 Then
                _DR = _DS.Tables(0).NewRow
            Else
                _DR = _DS.Tables(0).Rows(0)
            End If

            _DR("id") = SignatureID
            _DR("day_id") = TodayID

            If m_PersonID.HasValue Then
                _DR("person_id") = m_PersonID
            Else
                _DR("person_id") = DBNull.Value
            End If

            _DR("person_type") = m_PersonType.ToString
            _DR("person_name") = m_PersonName
            _DR("signature") = m_SignatureImage
            _DR("stamp") = Now

            If _DS.Tables(0).Rows.Count = 0 Then _DS.Tables(0).Rows.Add(_DR)

            Try
                DAL.UpdateDB(_DA, _DS)
                m_SignatureSaved = True

            Catch ex As Exception
                _OK = False
            End Try

        End If

        Return _OK

    End Function

End Class
