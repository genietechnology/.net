﻿Public Class MediaContent

    Public Property Target As String = ""
    Public Property TargetPair As SharedModule.Pair = Nothing

    Private m_Image As Drawing.Image = Nothing
    Private m_Data As Byte() = Nothing

    Public Property Data As Byte()
        Get
            Return m_Data
        End Get
        Set(value As Byte())
            m_Data = value
            SetImage()
        End Set
    End Property

    Public ReadOnly Property Image As Drawing.Image
        Get
            Return m_Image
        End Get
    End Property

    Public Sub New(ByVal Target As String, ByVal TargetPair As SharedModule.Pair, ByVal Image As Drawing.Image)
        Me.Target = Target
        Me.TargetPair = TargetPair
        m_Image = Image
        m_Data = MediaHandler.ReturnByteArray(m_Image)
    End Sub

    Private Sub SetImage()
        If Data Is Nothing Then
            m_Image = Nothing
        Else
            m_Image = MediaHandler.ReturnImage(Data)
        End If
    End Sub

End Class
