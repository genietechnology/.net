﻿Public Class FileDetail

    Public Sub New(ByVal FullPath As String)

        Dim _File As New IO.FileInfo(FullPath)

        Me.ID = Guid.NewGuid
        Me.FileName = _File.Name
        Me.Extension = _File.Extension.ToLower
        Me.FullPath = FullPath
        Me.Size = _File.Length

        _File = Nothing

    End Sub

    Public Property ID As Guid
    Public Property FileName As String
    Public Property Extension As String
    Public Property FullPath As String
    Public Property Size As Long

End Class
