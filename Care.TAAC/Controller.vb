﻿Option Strict On

Imports Paxton.Net2.OemClientLibrary

Public Class Controller

    Private WithEvents m_Net2 As OemClient

#Region "Variables"

    Private m_ServerIP As String = ""

    Private m_Paxton As Boolean = False
    Private m_PaxtonUser As String = ""
    Private m_PaxtonPassword As String = ""
    Private m_Port As Integer = 0

    Private m_Connecting As Boolean = False
    Private m_Connected As Boolean = False

    Private m_Departments As Dictionary(Of Integer, String)
    Private m_AccessLevels As Dictionary(Of Integer, String)
    Private m_Doors As List(Of IDoorView)

#End Region

#Region "Properties"

    Public ReadOnly Property Connected As Boolean
        Get
            Return m_Connected
        End Get
    End Property

    Public ReadOnly Property Departments As Dictionary(Of Integer, String)
        Get
            Return m_Departments
        End Get
    End Property

    Public ReadOnly Property AccessLevels As Dictionary(Of Integer, String)
        Get
            Return m_AccessLevels
        End Get
    End Property

#End Region

    Public Event AccessEvent(ByVal sender As Object, ByVal e As IEventView)

    Public Sub New(ByVal Paxton As Boolean, ByVal IPAddress As String, ByVal PaxtonUser As String, ByVal PaxtonPassword As String)
        m_Paxton = Paxton
        m_ServerIP = IPAddress
        m_PaxtonUser = PaxtonUser
        m_PaxtonPassword = PaxtonPassword
    End Sub

    Public Function Connect(ByRef ErrorString As String) As Boolean

        If m_Connected Then Return True

        m_Connecting = True

        m_Port = 8025
        ConnectToPaxton(ErrorString)

        m_Connecting = False

        Return m_Connected

    End Function

    Public Sub Disconnect()
        If m_Connected Then
            m_Net2.Dispose()
        End If
    End Sub

    Public Function Unlock(ByVal DoorSerial As String) As Boolean
        If m_Connected Then
            Return m_Net2.OpenDoor(CInt(DoorSerial))
        Else
            Return False
        End If
    End Function

#Region "Paxton"

    Private Sub ConnectToPaxton(ByRef ErrorString As String)

        Try

            '************************************************************************************

            m_Net2 = New OemClient(m_ServerIP, m_Port)

            '************************************************************************************

            'we have to login and authenticate with Net2 before we can do anything

            Dim _Operators As IOperators

            _Operators = m_Net2.GetListOfOperators

            If _Operators IsNot Nothing Then

                Dim _Users As New Dictionary(Of Integer, String)
                _Users = _Operators.UsersDictionary

                If _Users IsNot Nothing Then

                    If _Users.Count > 0 Then

                        Dim _UserID As Integer = 0
                        For Each _u As KeyValuePair(Of Integer, String) In _Users
                            If _u.Value.ToLower = m_PaxtonUser.ToLower Then
                                _UserID = _u.Key
                                Exit For
                            End If
                        Next

                        If _UserID > 0 Then

                            Dim _Methods As New Dictionary(Of String, Integer)
                            _Methods = m_Net2.AuthenticateUser(_UserID, m_PaxtonPassword)

                            If _Methods.Count > 0 Then

                                m_Departments = m_Net2.ViewDepartments.DepartmentsDictionary
                                m_AccessLevels = m_Net2.ViewAccessLevels.AccessLevelsDictionary
                                m_Doors = m_Net2.ViewDoors.DoorsList

                                m_Connected = True

                                Exit Sub

                            Else
                                'authentication failed
                                ErrorString = "Authentication failed."
                            End If

                        Else
                            'user not found
                            ErrorString = "User [" + m_PaxtonUser + "] not found."
                        End If

                    Else
                        'user count = 0
                        ErrorString = "No users found"
                    End If

                Else
                    'users nothing
                    ErrorString = "No users found (object nothing)"
                End If

            Else
                'operators nothing
                ErrorString = "No operators found (object nothing)"
            End If


        Catch ex As Exception
            ErrorString = "Unhandled Exception: " + ex.Message
            m_Connected = False
        End Try

        m_Connected = False

    End Sub

    Public Function MonitorDoor(ByVal ACUNo As Integer) As Boolean
        Dim _Error As String = ""
        If Connect(_Error) Then
            Return m_Net2.MonitorAcu(ACUNo)
        Else
            Return False
        End If
    End Function

    Public Function GetUsers() As List(Of UserData)

        Dim _Error As String = ""
        If Connect(_Error) Then

            Dim _Users As New List(Of UserData)

            Dim _PaxU As IUsers = m_Net2.ViewUserRecords("active=1")
            If _PaxU IsNot Nothing Then

                Dim _PaxUsers As New Dictionary(Of Integer, IUserView)
                _PaxUsers = _PaxU.UsersList

                For Each _PaxUser As KeyValuePair(Of Integer, IUserView) In _PaxUsers

                    Dim _U As New UserData
                    With _U

                        .UserID = _PaxUser.Value.UserId
                        .Forename = _PaxUser.Value.FirstName
                        .Surname = _PaxUser.Value.Surname
                        .Telephone = _PaxUser.Value.Telephone
                        .PIN = _PaxUser.Value.PIN

                        .AccessLevelID = _PaxUser.Value.AccessLevelId
                        .AccessLevel = _PaxUser.Value.AccessLevel

                        .DepartmentID = _PaxUser.Value.DepartmentId
                        .Department = _PaxUser.Value.Department

                    End With

                    _Users.Add(_U)

                Next

                Return _Users

            Else
                'query failed
                Return Nothing
            End If

        Else
            'no connection
            Return Nothing
        End If

    End Function

    Public Function CreateUser(ByVal AccessLevel As Integer, ByVal Department As Integer, _
                           ByVal Forename As String, Surname As String, Telephone As String, PIN As String, Photo As Drawing.Image) As Integer

        Try
            If m_Net2.AddUserRecord(AccessLevel, Department, False, False, Forename, "", Surname, Telephone, "", PIN, "", Today, 0, 0, True, "", Nothing, Nothing) Then
                Return m_Net2.LastCreatedUserId
            Else
                Return -1
            End If

        Catch ex As Exception
            Return -2
        End Try

    End Function

    Public Function UpdateUser(ByVal UserID As Integer, ByVal AccessLevel As Integer, ByVal Department As Integer, _
                               ByVal Forename As String, Surname As String, Telephone As String, PIN As String, Photo As Drawing.Image) As Boolean

        Try
            Return m_Net2.UpdateUserRecord(UserID, AccessLevel, Department, False, False, Forename, "", Surname, Telephone, "", PIN, "", Today, True, "", Nothing, Nothing)

        Catch ex As Exception
            Return False
        End Try

    End Function

    Private Sub m_Net2_Net2AccessEvent(sender As Object, e As IEventView) Handles m_Net2.Net2AccessEvent
        RaiseEvent AccessEvent(sender, e)
    End Sub

    Private Sub m_Net2_Net2DoorStatusChangedEvent(sender As Object, e As Paxton.Net2.OemClientLibrary.IEventView) Handles m_Net2.Net2DoorStatusChangedEvent

    End Sub

    Private Sub m_Net2_Net2ServerReconnected(sender As Object, e As Paxton.Net2.OemClientLibrary.Net2OemReconnectedEventArgs) Handles m_Net2.Net2ServerReconnected

    End Sub

#End Region

End Class
