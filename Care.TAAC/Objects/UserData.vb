﻿Public Class UserData

    Public Property UserID As Integer
    Public Property Forename As String
    Public Property Surname As String
    Public Property Telephone As String
    Public Property PIN As String
    Public Property AccessLevelID As Integer
    Public Property AccessLevel As String
    Public Property DepartmentID As Integer
    Public Property Department As String

End Class
