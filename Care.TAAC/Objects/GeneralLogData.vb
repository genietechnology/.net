﻿Public Class GeneralLogData
    Public Property dwTMachineNumber As Integer
    Public Property dwEnrollNumber As Integer
    Public Property dwEMachineNumber As Integer
    Public Property dwVerifyMode As Integer
    Public Property dwInOutMode As Integer
    Public Property dwYear As Integer
    Public Property dwMonth As Integer
    Public Property dwDay As Integer
    Public Property dwHour As Integer
    Public Property dwMinute As Integer
    Public Property dwSecond As Integer
    Public Property dwWorkCode As Integer
End Class
