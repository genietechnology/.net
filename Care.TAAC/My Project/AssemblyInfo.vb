﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Care.TAAC")>
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyCompany("Care Software Ltd")>
<Assembly: AssemblyProduct("Care.TAAC")>
<Assembly: AssemblyCopyright("Copyright ©  2014")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(True)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("6c1f3639-c011-47a5-83d4-767c64732b04")>
<Assembly: AssemblyVersion("1.19.1.3")>
<Assembly: AssemblyFileVersion("1.19.1.3")>



' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 
