﻿Imports System.IO
Imports System.Text

Public Class License

    Public Property LicenseTerms() As String
    Public Property Signature() As String

    Public Sub Save(fileName As [String])
        Serialiser.Save(Of License)(Me, fileName)
    End Sub

    Public Sub Save(stream As Stream)
        Serialiser.Save(Of License)(Me, stream)
    End Sub

    Public Overrides Function ToString() As String
        Return Serialiser.Save(Of License)(Me, Encoding.ASCII)
    End Function

    Public Shared Function Load(fileName As [String]) As License
        Return Serialiser.Load(Of License)(New FileInfo(fileName))
    End Function

    Public Shared Function Load(data As Stream) As License
        Return Serialiser.Load(Of License)(data)
    End Function

End Class