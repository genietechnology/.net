﻿
Imports System.Security
Imports System.Security.Cryptography

Public Class LicenseValidation

    Public Shared Sub ValidateLicense(license As License, publicKey As [String])

        ' get the valid terms for the license: (this checks the digital signature on the license file)
        Dim terms As LicenseTerms = GetValidTerms(license, publicKey)

        ' ensure a valid license-terms object was returned:
        If terms IsNot Nothing Then
            ' validate the date-range of the license terms:
            If DateTime.Now.CompareTo(terms.ExpiryDate) <= 0 Then
                If DateTime.Now.CompareTo(terms.StartDate) >= 0 Then

                    ' date range is valid... check the product name against the current assembly

                    'check if we are dealing with a domain license
                    If terms.CompanyLocation = "Domain License" Then

                        If Environment.UserDomainName.ToUpper = terms.ComputerName.ToUpper Then
                            Return
                        Else
                            Throw New SecurityException("License is not valid for this domain: " & Environment.UserDomainName)
                        End If

                    Else

                        'normal device license - so check the computer name
                        If Environment.MachineName = terms.ComputerName Then
                            Return
                        Else
                            Throw New SecurityException("License is not valid for this computer: " & Environment.MachineName)
                        End If

                    End If

                Else
                    ' license terms not valid yet.
                    Throw New SecurityException("License Terms Not Valid Until: " & terms.StartDate.ToShortDateString())
                End If
            Else
                ' license terms have expired.
                Throw New SecurityException("License Terms Expired On: " & terms.ExpiryDate.ToShortDateString())
            End If
        Else
            ' the license file was not valid.
            Throw New SecurityException("Invalid License File!")
        End If

    End Sub

    Public Shared Function GetValidTerms(license As License, publicKey As String) As LicenseTerms

        ' create the crypto-service provider:
        Dim _dsa As New DSACryptoServiceProvider()

        Try

            ' setup the provider from the public key:
            _dsa.FromXmlString(publicKey)

            ' get the license terms data:
            Dim _terms As Byte() = Convert.FromBase64String(license.LicenseTerms)

            ' get the signature data:
            Dim _signature As Byte() = Convert.FromBase64String(license.Signature)

            ' verify that the license-terms match the signature data
            If _dsa.VerifyData(_terms, _signature) Then
                Return LicenseTerms.FromString(license.LicenseTerms)
            Else
                Throw New SecurityException("Signature Not Verified!")
            End If

        Catch ex As Exception
            Throw New SecurityException("Signature Verification Failure: " + ex.Message)
        End Try

    End Function
End Class
