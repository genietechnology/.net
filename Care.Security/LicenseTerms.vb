﻿Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary

<Serializable> _
Public Class LicenseTerms

    Public Property StartDate() As DateTime
    Public Property ExpiryDate() As DateTime
    Public Property CompanyName() As String
    Public Property CompanyLocation() As String
    Public Property ComputerName() As String

    Public Function GetLicenseString() As [String]
        Using ms As New MemoryStream()
            ' create a binary formatter:
            Dim bnfmt As New BinaryFormatter()

            ' serialize the data to the memory-steam;
            bnfmt.Serialize(ms, Me)

            ' return a base64 string representation of the binary data:
            Return Convert.ToBase64String(ms.GetBuffer())
        End Using
    End Function

    Public Function GetLicenseData() As Byte()
        Using ms As New MemoryStream()
            ' create a binary formatter:
            Dim bnfmt As New BinaryFormatter()

            ' serialize the data to the memory-steam;
            bnfmt.Serialize(ms, Me)

            ' return a base64 string representation of the binary data:
            Return ms.GetBuffer()
        End Using
    End Function

    Friend Shared Function FromString(licenseTerms As [String]) As LicenseTerms

        Using ms As New MemoryStream(Convert.FromBase64String(licenseTerms))
            ' create a binary formatter:
            Dim bnfmt As New BinaryFormatter()

            ' serialize the data to the memory-steam;
            Dim value As Object = bnfmt.Deserialize(ms)

            If TypeOf value Is LicenseTerms Then
                Return DirectCast(value, LicenseTerms)
            Else
                Throw New ApplicationException("Invalid Type!")
            End If
        End Using
    End Function

End Class
