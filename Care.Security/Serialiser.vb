﻿Imports System.IO
Imports System.Text
Imports System.Xml.Serialization

NotInheritable Class Serialiser

    Private Sub New()

    End Sub

    Private Shared Function getSerializer(Of T)() As XmlSerializer
        Return New XmlSerializer(GetType(T))
    End Function

    Public Shared Sub Save(Of T)(o As T, fileName As [String])
        Using fs As FileStream = File.Open(fileName, FileMode.Create)
            Save(Of T)(o, fs)
        End Using
    End Sub

    Public Shared Sub Save(Of T)(o As T, stream As Stream)
        getSerializer(Of T)().Serialize(stream, o)
    End Sub

    Public Shared Function Save(Of T)(o As T) As Byte()
        Using ms As New MemoryStream()
            ' write the data to the memory stream:
            Save(Of T)(o, ms)

            ' return the back-buffer:
            Return ms.GetBuffer()
        End Using
    End Function

    Public Shared Function Save(Of T)(o As T, encoding As Encoding) As [String]
        Return encoding.GetString(Save(Of T)(o))
    End Function

    Public Shared Function Load(Of T)(stream As Stream) As T
        Try
            Return DirectCast(getSerializer(Of T)().Deserialize(stream), T)

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function Load(Of T)(file As FileInfo) As T
        Using fs As FileStream = file.Open(FileMode.Open)
            Return Load(Of T)(fs)
        End Using
    End Function

    Public Shared Function Load(Of T)(data As Byte()) As T
        Using ms As New MemoryStream(data)
            Return Load(Of T)(ms)
        End Using
    End Function

    Public Shared Function Load(Of T)(data As [String]) As T
        Return Load(Of T)(Encoding.ASCII.GetBytes(data))
    End Function

End Class
