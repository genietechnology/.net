﻿<DataContract>
Public Class JobStep

    <DataMember>
    Public Property ID As String

    <DataMember>
    Public Property JobID As String

    <DataMember>
    Public Property Type As String

    <DataMember>
    Public Property Arg1 As String

    <DataMember>
    Public Property Arg2 As String

    <DataMember>
    Public Property Arg3 As String

    <DataMember>
    Public Property Memo As String

    <DataMember>
    Public Property Seq As Integer

End Class
