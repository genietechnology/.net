﻿Imports System.Collections.Generic

<DataContract>
Public Class Job

    <DataMember>
    Public Property ID As String

    <DataMember>
    Public Property JobID As String

    <DataMember>
    Public Property JobName As String

    <DataMember>
    Public Property AppName As String

    <DataMember>
    Public Property AppPath As String

    <DataMember>
    Public Property AppServer As String

    <DataMember>
    Public Property AppCS As String

    <DataMember>
    Public Property Steps As List(Of JobStep)

    <DataMember>
    Public Property Seq As Integer

    <DataMember>
    Public Property HasPack As Boolean

End Class
