﻿Imports System.Data
Imports Care.Data
Imports System.ServiceModel.Web
Imports System.IO
Imports System.Collections.Generic

' NOTE: You can use the "Rename" command on the context menu to change the class name "Update" in code, svc and config file together.
' NOTE: In order to launch WCF Test Client for testing this service, please select Update.svc or Update.svc.vb at the Solution Explorer and start debugging.
Public Class Update

    Implements IUpdate

    Public Function RegisterEndpoint(ByVal CustomerID As String, ByVal ComputerName As String) As String Implements IUpdate.RegisterEndpoint
        Return UpdateFunctions.RegisterEndPoint(CustomerID, ComputerName)
    End Function

    Public Function CheckForUpdates(ByVal EndPointID As String, ByVal ImmediateOnly As Boolean) As List(Of Job) Implements IUpdate.CheckForUpdates
        Return UpdateFunctions.CheckForUpdates(EndPointID, ImmediateOnly)
    End Function

    Public Function TestConnection() As String Implements IUpdate.TestConnection
        Return UpdateFunctions.TestConnection
    End Function

    Public Function LogUpdate(ID As String, Status As Integer, LogText As String) As Boolean Implements IUpdate.LogUpdate
        Return UpdateFunctions.LogUpdate(ID, Status, LogText)
    End Function
End Class
