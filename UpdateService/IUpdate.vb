﻿Imports System.ServiceModel
Imports System.ServiceModel.Web
Imports System.IO
Imports System.Collections.Generic

' NOTE: You can use the "Rename" command on the context menu to change the interface name "IUpdate" in both code and config file together.
<ServiceContract()>
Public Interface IUpdate

    <OperationContract()>
    Function TestConnection() As String

    <OperationContract()>
    Function CheckForUpdates(ByVal EndPointID As String, ByVal ImmediateOnly As Boolean) As List(Of Job)

    <OperationContract()>
    Function LogUpdate(ByVal ID As String, ByVal Status As Integer, ByVal LogText As String) As Boolean

    <OperationContract()>
    Function RegisterEndpoint(ByVal CustomerID As String, ByVal ComputerName As String) As String

End Interface
