﻿Option Strict On

Imports Care.Global
Imports Care.Data

Imports System.Data
Imports System.Collections.Generic
Imports System.Net.Mail
Imports UpdateShared

Friend Class UpdateFunctions

    Private Shared m_Connected As Boolean = False

    Friend Shared Function TestConnection() As String
        Return My.Settings.UpdateDB
    End Function

    Public Shared Function Connect() As Boolean
        If m_Connected Then
            Return True
        Else
            Return DoConnect()
        End If
    End Function

    Private Shared Function DoConnect() As Boolean

        Dim _Return As Boolean = False

        Session.ServiceMode()
        Session.ConnectionString = My.Settings.UpdateDB

        Session.CurrentUser = New User()
        Session.CurrentUser.ID = Guid.NewGuid
        Session.CurrentUser.FullName = "Update Service"

        m_Connected = True

        Return True

    End Function

    Friend Shared Function LogUpdate(ByVal ID As String, ByVal Status As Integer, ByVal LogText As String) As Boolean

        Dim _EJ As Business.EndpointJob = Business.EndpointJob.RetreiveByID(New Guid(ID))
        If _EJ IsNot Nothing Then

            _EJ._Status = CByte(Status)
            _EJ._LastRun = Now
            _EJ._LastLog = LogText
            _EJ.Store()

            'update endpoint version


            EmailReport(_EJ)

        End If

        Return True

    End Function

    Private Shared Sub EmailReport(ByVal EJ As Business.EndpointJob)

        Try

            Dim _SQL As String = ""
            _SQL += "select c.name as 'customer', e.computer_name as 'computer', a.app_name as 'app', j.name as 'job' from EndpointJobs ej"
            _SQL += " left join Endpoints e on e.ID = ej.endpoint_id"
            _SQL += " left join Jobs j on j.ID = ej.job_id"
            _SQL += " left join CustomerApps a on a.ID = ej.app_id"
            _SQL += " left join Customers c on c.ID = a.customer_id"
            _SQL += " where ej.ID = '" + EJ._ID.Value.ToString + "'"

            Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
            If _DR IsNot Nothing Then

                Dim _Subject As String = "Update Service - " + _DR.Item("job").ToString + " Job Completed on " + _DR.Item("computer").ToString + " (" + _DR.Item("customer").ToString + ")"
                Dim _Body As String = ""

                _Body += "Customer: " + _DR.Item("customer").ToString + vbCrLf
                _Body += "Computer: " + _DR.Item("computer").ToString + vbCrLf
                _Body += "Application: " + _DR.Item("app").ToString + vbCrLf
                _Body += "Job: " + _DR.Item("job").ToString + vbCrLf
                '_Body += "Last Run: " + Format(EJ._LastRun.Value, "dd/mm/yyyy HH:MM:SS").ToString + vbCrLf
                _Body += vbCrLf
                '_Body += "Status: " + EJ._Status.ToString + vbCrLf
                _Body += vbCrLf
                _Body += EJ._LastLog

                SendEmail("api@caresoftware.co.uk", _Subject, _Body)

            End If

        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try

    End Sub

    Private Shared Sub SendEmail(ByVal Recipient As String, ByVal Subject As String, ByVal Body As String)

        Dim _SenderAddress As MailAddress = SetEmailAddress("api@caresoftware.co.uk", "Care Software API")
        If _SenderAddress IsNot Nothing Then

            Dim _RecipientAddress As MailAddress = SetEmailAddress(Recipient)
            If _RecipientAddress IsNot Nothing Then

                Dim _msg As New MailMessage(_SenderAddress, _RecipientAddress)
                With _msg
                    .Subject = Subject
                    .Body = Body
                End With

                Dim _client As New SmtpClient
                _client.Host = "smtp.mandrillapp.com"
                _client.Port = 587
                _client.EnableSsl = True

                Dim _credentials As New Net.NetworkCredential
                _credentials.UserName = "james@caresoftware.co.uk"
                _credentials.Password = "Rd35yx2S5CwbbNkwLiu-Bw"

                _client.UseDefaultCredentials = False
                _client.Credentials = _credentials

                Try
                    _client.Send(_msg)

                Catch ex As SmtpException

                End Try

                _msg.Dispose()
                _msg = Nothing

                _client = Nothing

            Else
                'receipient address problem
            End If

        Else
            'sender address problem
        End If

    End Sub

    Private Shared Function SetEmailAddress(ByVal EmailAddress As String, Optional ByVal DisplayName As String = "") As MailAddress

        Dim _Address As MailAddress = Nothing

        Try
            If DisplayName = "" Then
                _Address = New MailAddress(EmailAddress)
            Else
                _Address = New MailAddress(EmailAddress, DisplayName)
            End If

        Catch ex As Exception
            _Address = Nothing
        End Try

        Return _Address

    End Function

    Friend Shared Function RegisterEndPoint(ByVal CustomerID As String, ByVal ComputerName As String) As String

        If Connect() Then

            If Not CustomerExists(CustomerID) Then
                Return "-CUSTOMERNOTFOUND"
            End If

            Dim _Return As String = ""
            Dim _SQL As String = ""

            _SQL += "select * from Endpoints"
            _SQL += " where customer_id = '" + CustomerID + "'"
            _SQL += " and computer_name = '" + ComputerName + "'"

            Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If _DT IsNot Nothing Then

                Select Case _DT.Rows.Count

                    Case 0
                        Try
                            _Return = CreateNewEndPoint(CustomerID, ComputerName)

                        Catch ex As Exception
                            _Return = "CS:" + My.Settings.UpdateDB + vbCrLf + "Err:" + ex.Message
                        End Try

                    Case 1
                        _Return = _DT.Rows(0).Item("ID").ToString
                        LogCheckIn(_Return)

                    Case Else
                        _Return = "-DUPLICATENAME"

                End Select

                _DT.Dispose()
                _DT = Nothing

            End If

            Return _Return

        Else
            Return "-CONNECTFAILED"
        End If

    End Function

    Private Shared Sub LogCheckIn(ByVal EndpointID As String)

        Dim _E As Business.Endpoint = Business.Endpoint.RetreiveByID(New Guid(EndpointID))
        If _E IsNot Nothing Then
            _E._LastCheckin = Now
            _E.Store()
        End If

    End Sub

    Friend Shared Function CheckForUpdates(ByVal EndPointID As String, ByVal ImmediateOnly As Boolean) As List(Of Job)

        Dim _Return As List(Of Job) = Nothing
        Dim _SQL As String = ""

        _SQL += "select ej.ID, j.ID as 'job_id', j.name, j.seq,"
        _SQL += " a.app_name, a.app_path, a.app_server, a.app_cs"
        _SQL += " from EndpointJobs ej"
        _SQL += " left join Jobs j on j.ID = ej.job_id"
        _SQL += " left join CustomerApps a on a.ID = ej.app_id"
        _SQL += " where ej.endpoint_id = '" + EndPointID + "'"
        _SQL += " and ej.status = 0"

        If ImmediateOnly Then _SQL += " and ej.immediate = 1"

        _SQL += " order by j.seq"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            _Return = New List(Of Job)

            For Each _DR As DataRow In _DT.Rows

                Dim _J As New Job
                With _J
                    .ID = _DR.Item("ID").ToString
                    .JobID = _DR.Item("job_id").ToString
                    .JobName = _DR.Item("name").ToString
                    .AppName = _DR.Item("app_name").ToString
                    .AppPath = _DR.Item("app_path").ToString
                    .AppServer = _DR.Item("app_server").ToString
                    .AppCS = _DR.Item("app_cs").ToString
                    .Steps = ReturnSteps(.JobID)
                    .Seq = CInt(_DR.Item("seq"))
                End With

                _Return.Add(_J)

            Next

        End If

        Return _Return

    End Function

    Private Shared Function CustomerExists(ByVal CustomerID As String) As Boolean

        Dim _Count As Integer = 0
        Dim _SQL As String = "select count (*) from Customers where ID = '" + CustomerID + "'"

        Dim _Scalar As Object = DAL.ReturnScalar(Session.ConnectionString, _SQL)
        If _Scalar IsNot Nothing Then
            _Count = CInt(_Scalar)
        End If

        If _Count = 1 Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Shared Function CreateNewEndPoint(ByVal CustomerID As String, ByVal ComputerName As String) As String

        Dim _e As New Business.Endpoint
        With _e
            ._CustomerId = New Guid(CustomerID)
            ._ComputerName = ComputerName
            ._LastCheckin = Now
            .Store()
        End With

        Return _e._ID.ToString

    End Function

    Private Shared Function ReturnSteps(ByVal JobID As String) As List(Of JobStep)

        Dim _Return As List(Of JobStep) = Nothing

        Try

            Dim _SQL As String = ""

            _SQL += "select * from JobSteps"
            _SQL += " where job_id = '" + JobID + "'"
            _SQL += " order by seq"

            Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If _DT IsNot Nothing Then

                _Return = New List(Of JobStep)

                For Each _DR As DataRow In _DT.Rows

                    Dim _S As New JobStep
                    With _S
                        .ID = _DR.Item("ID").ToString
                        .JobID = _DR.Item("job_id").ToString
                        .Type = _DR.Item("type").ToString
                        .Arg1 = _DR.Item("arg_1").ToString
                        .Arg2 = _DR.Item("arg_2").ToString
                        .Arg3 = _DR.Item("arg_3").ToString
                        .Memo = _DR.Item("memo").ToString
                        .Seq = CInt(_DR.Item("seq"))
                    End With

                    _Return.Add(_S)

                Next

            End If

        Catch ex As Exception

            _Return = New List(Of JobStep)

            Dim _S As New JobStep
            _S.ID = Guid.NewGuid.ToString
            _S.JobID = JobID
            _S.Memo = ex.Message

            _Return.Add(_S)

        End Try

        Return _Return

    End Function

End Class
