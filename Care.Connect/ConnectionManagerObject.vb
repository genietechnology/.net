﻿Public Class ConnectionManagerObject

    Private m_ConnectionString As String = ""
    Private m_AuditEnabled As Boolean = True
    Private m_ExcludedUpserts As String = ""
    Private m_AuditHalt As Boolean = False

#Region "Methods"

    ''' <summary>
    ''' Should be called as soon as we have a connection string
    ''' </summary>
    Public Sub Setup(ByVal ConnectionString As String)
        m_ConnectionString = ConnectionString
    End Sub

    ''' <summary>
    ''' Should be called after we have parameters
    ''' </summary>
    Public Sub PassParameters(ByVal AuditEnabled As Boolean, ByVal ExcludedUpserts As String)
        m_AuditEnabled = AuditEnabled
        m_ExcludedUpserts = ExcludedUpserts
    End Sub

    Public Sub HaltAuditting()
        If m_AuditEnabled Then m_AuditHalt = True
    End Sub

    Public Sub ResumeAuditting()
        If m_AuditEnabled Then m_AuditHalt = False
    End Sub

#End Region

#Region "Read-Only Properties"

    Public ReadOnly Property ConnectionString As String
        Get
            Return m_ConnectionString
        End Get
    End Property

    ''' <summary>
    ''' Auditing Enabled at an application level
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property AuditEnabled As Boolean
        Get
            Return m_AuditEnabled
        End Get
    End Property

    Public ReadOnly Property ExcludedUpserts As String
        Get
            Return m_ExcludedUpserts
        End Get
    End Property

    ''' <summary>
    ''' Halt auditting for a temporary period of time
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property AuditTemporaryHalt As Boolean
        Get
            Return m_AuditHalt
        End Get
    End Property

#End Region

End Class