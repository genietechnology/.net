﻿Imports Oak.Connect

Public Class Engine

    Private WithEvents m_Connector As SDK.IConnectSmart

    Public Property IPAddress As String
    Public Property PortNumber As String
    Public Property Extension As String

    Public Sub Connect(ByVal ServerIP As String, ByVal ServerPort As Integer, ByVal Extension As String)
        m_Connector = New SDK.SmartConnector
        m_Connector.Login(ServerIP, ServerPort, Extension)
    End Sub

    Private Sub m_Connector_Answered(sender As Object, e As EventArgs) Handles m_Connector.Answered

    End Sub

    Private Sub m_Connector_CallInitiated(sender As Object, e As SDK.CallInitiatedEventArgs) Handles m_Connector.CallInitiated

    End Sub

    Private Sub m_Connector_Connected(sender As Object, e As SDK.ConnectedEventArgs) Handles m_Connector.Connected

    End Sub

    Private Sub m_Connector_ErrorDetected(sender As Object, e As SDK.ErrorEventArgs) Handles m_Connector.ErrorDetected

    End Sub

    Private Sub m_Connector_HungUp(sender As Object, e As EventArgs) Handles m_Connector.HungUp

    End Sub

    Private Sub m_Connector_OffHook(sender As Object, e As SDK.OffHookEventArgs) Handles m_Connector.OffHook

    End Sub

    Private Sub m_Connector_OnHook(sender As Object, e As EventArgs) Handles m_Connector.OnHook

    End Sub

    Private Sub m_Connector_Ringing(sender As Object, e As SDK.RingingEventArgs) Handles m_Connector.Ringing

    End Sub

    Private Sub m_Connector_Trace(sender As Object, e As SDK.TraceEventArgs) Handles m_Connector.Trace

    End Sub
End Class
