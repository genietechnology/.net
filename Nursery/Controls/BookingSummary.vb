﻿
Public Class BookingSummary

    Public Property BookingStart As Date
    Public Property BookingEnd As Date
    Public Property Subject As String
    Public Property Location As String
    Public Property AllDay As Boolean
    Public Property Notes As String
    Public Property Label As String

End Class
