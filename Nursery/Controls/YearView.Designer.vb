﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class YearView
    Inherits UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim TimeRuler1 As DevExpress.XtraScheduler.TimeRuler = New DevExpress.XtraScheduler.TimeRuler()
        Dim TimeRuler2 As DevExpress.XtraScheduler.TimeRuler = New DevExpress.XtraScheduler.TimeRuler()
        Me.txtYellow = New DevExpress.XtraEditors.TextEdit()
        Me.txtRed = New DevExpress.XtraEditors.TextEdit()
        Me.txtBlue = New DevExpress.XtraEditors.TextEdit()
        Me.txtGreen = New DevExpress.XtraEditors.TextEdit()
        Me.MyScheduler = New DevExpress.XtraScheduler.SchedulerControl()
        Me.MyStorage = New DevExpress.XtraScheduler.SchedulerStorage(Me.components)
        Me.ToolTipController1 = New DevExpress.Utils.ToolTipController(Me.components)
        Me.gbxLegend = New Care.Controls.CareFrame(Me.components)
        Me.lblBlue = New Care.Controls.CareLabel(Me.components)
        Me.lblRed = New Care.Controls.CareLabel(Me.components)
        Me.lblYellow = New Care.Controls.CareLabel(Me.components)
        Me.lblGreen = New Care.Controls.CareLabel(Me.components)
        Me.MyDateNav = New DevExpress.XtraScheduler.DateNavigator()
        Me.gbxDensity = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel10 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.txt100 = New DevExpress.XtraEditors.TextEdit()
        Me.txt20 = New DevExpress.XtraEditors.TextEdit()
        Me.txt40 = New DevExpress.XtraEditors.TextEdit()
        Me.txt80 = New DevExpress.XtraEditors.TextEdit()
        Me.txt60 = New DevExpress.XtraEditors.TextEdit()
        Me.txtOrange = New DevExpress.XtraEditors.TextEdit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBlue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MyScheduler, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MyStorage, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxLegend, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxLegend.SuspendLayout()
        CType(Me.MyDateNav, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MyDateNav.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxDensity, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxDensity.SuspendLayout()
        CType(Me.txt100.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt20.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt40.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt80.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt60.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOrange.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtYellow
        '
        Me.txtYellow.Location = New System.Drawing.Point(287, 9)
        Me.txtYellow.Name = "txtYellow"
        Me.txtYellow.Properties.Appearance.BackColor = System.Drawing.Color.Khaki
        Me.txtYellow.Properties.Appearance.Options.UseBackColor = True
        Me.txtYellow.Properties.ReadOnly = True
        Me.txtYellow.Size = New System.Drawing.Size(20, 20)
        Me.txtYellow.TabIndex = 9
        Me.txtYellow.TabStop = False
        '
        'txtRed
        '
        Me.txtRed.Location = New System.Drawing.Point(9, 9)
        Me.txtRed.Name = "txtRed"
        Me.txtRed.Properties.Appearance.BackColor = System.Drawing.Color.LightCoral
        Me.txtRed.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRed.Properties.Appearance.Options.UseBackColor = True
        Me.txtRed.Properties.Appearance.Options.UseFont = True
        Me.txtRed.Properties.ReadOnly = True
        Me.txtRed.Size = New System.Drawing.Size(20, 20)
        Me.txtRed.TabIndex = 8
        Me.txtRed.TabStop = False
        '
        'txtBlue
        '
        Me.txtBlue.Location = New System.Drawing.Point(442, 9)
        Me.txtBlue.Name = "txtBlue"
        Me.txtBlue.Properties.Appearance.BackColor = System.Drawing.Color.LightSkyBlue
        Me.txtBlue.Properties.Appearance.Options.UseBackColor = True
        Me.txtBlue.Properties.ReadOnly = True
        Me.txtBlue.Size = New System.Drawing.Size(20, 20)
        Me.txtBlue.TabIndex = 6
        Me.txtBlue.TabStop = False
        '
        'txtGreen
        '
        Me.txtGreen.Location = New System.Drawing.Point(156, 9)
        Me.txtGreen.Name = "txtGreen"
        Me.txtGreen.Properties.Appearance.BackColor = System.Drawing.Color.LightGreen
        Me.txtGreen.Properties.Appearance.Options.UseBackColor = True
        Me.txtGreen.Properties.ReadOnly = True
        Me.txtGreen.Size = New System.Drawing.Size(20, 20)
        Me.txtGreen.TabIndex = 5
        Me.txtGreen.TabStop = False
        '
        'MyScheduler
        '
        Me.MyScheduler.Location = New System.Drawing.Point(400, 3)
        Me.MyScheduler.Name = "MyScheduler"
        Me.MyScheduler.Size = New System.Drawing.Size(286, 255)
        Me.MyScheduler.Start = New Date(2018, 1, 1, 0, 0, 0, 0)
        Me.MyScheduler.Storage = Me.MyStorage
        Me.MyScheduler.TabIndex = 5
        Me.MyScheduler.Text = "SchedulerControl1"
        Me.MyScheduler.ToolTipController = Me.ToolTipController1
        Me.MyScheduler.Views.DayView.TimeRulers.Add(TimeRuler1)
        Me.MyScheduler.Views.GanttView.Enabled = False
        Me.MyScheduler.Views.MonthView.Enabled = False
        Me.MyScheduler.Views.TimelineView.Enabled = False
        Me.MyScheduler.Views.WeekView.Enabled = False
        Me.MyScheduler.Views.WorkWeekView.Enabled = False
        Me.MyScheduler.Views.WorkWeekView.TimeRulers.Add(TimeRuler2)
        '
        'gbxLegend
        '
        Me.gbxLegend.Controls.Add(Me.txtOrange)
        Me.gbxLegend.Controls.Add(Me.lblBlue)
        Me.gbxLegend.Controls.Add(Me.lblRed)
        Me.gbxLegend.Controls.Add(Me.lblYellow)
        Me.gbxLegend.Controls.Add(Me.lblGreen)
        Me.gbxLegend.Controls.Add(Me.txtGreen)
        Me.gbxLegend.Controls.Add(Me.txtYellow)
        Me.gbxLegend.Controls.Add(Me.txtRed)
        Me.gbxLegend.Controls.Add(Me.txtBlue)
        Me.gbxLegend.Dock = DockStyle.Bottom
        Me.gbxLegend.Location = New System.Drawing.Point(0, 275)
        Me.gbxLegend.Name = "gbxLegend"
        Me.gbxLegend.ShowCaption = False
        Me.gbxLegend.Size = New System.Drawing.Size(689, 36)
        Me.gbxLegend.TabIndex = 10
        Me.gbxLegend.Text = "GroupControl1"
        '
        'lblBlue
        '
        Me.lblBlue.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblBlue.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblBlue.Location = New System.Drawing.Point(468, 11)
        Me.lblBlue.Name = "lblBlue"
        Me.lblBlue.Size = New System.Drawing.Size(79, 15)
        Me.lblBlue.TabIndex = 18
        Me.lblBlue.Text = "PM Spaces Full"
        Me.lblBlue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRed
        '
        Me.lblRed.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblRed.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblRed.Location = New System.Drawing.Point(35, 12)
        Me.lblRed.Name = "lblRed"
        Me.lblRed.Size = New System.Drawing.Size(106, 15)
        Me.lblRed.TabIndex = 17
        Me.lblRed.Text = "No Spaces Available"
        Me.lblRed.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblYellow
        '
        Me.lblYellow.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblYellow.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblYellow.Location = New System.Drawing.Point(313, 11)
        Me.lblYellow.Name = "lblYellow"
        Me.lblYellow.Size = New System.Drawing.Size(80, 15)
        Me.lblYellow.TabIndex = 16
        Me.lblYellow.Text = "AM Spaces Full"
        Me.lblYellow.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGreen
        '
        Me.lblGreen.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblGreen.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblGreen.Location = New System.Drawing.Point(182, 11)
        Me.lblGreen.Name = "lblGreen"
        Me.lblGreen.Size = New System.Drawing.Size(87, 15)
        Me.lblGreen.TabIndex = 15
        Me.lblGreen.Text = "Spaces Available"
        Me.lblGreen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'MyDateNav
        '
        Me.MyDateNav.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MyDateNav.Appearance.Options.UseFont = True
        Me.MyDateNav.BoldAppointmentDates = False
        Me.MyDateNav.CalendarAppearance.DayCellHighlighted.BackColor = System.Drawing.Color.Transparent
        Me.MyDateNav.CalendarAppearance.DayCellHighlighted.BackColor2 = System.Drawing.Color.Transparent
        Me.MyDateNav.CalendarAppearance.DayCellHighlighted.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.MyDateNav.CalendarAppearance.DayCellHighlighted.Options.UseBackColor = True
        Me.MyDateNav.CalendarAppearance.DayCellHighlighted.Options.UseFont = True
        Me.MyDateNav.CalendarAppearance.DayCellSpecial.FontStyleDelta = System.Drawing.FontStyle.Bold
        Me.MyDateNav.CalendarAppearance.DayCellSpecial.Options.UseFont = True
        Me.MyDateNav.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.MyDateNav.CellSize = New System.Drawing.Size(20, 20)
        Me.MyDateNav.Dock = DockStyle.Fill
        Me.MyDateNav.DrawCellLines = True
        Me.MyDateNav.FirstDayOfWeek = System.DayOfWeek.Monday
        Me.MyDateNav.HighlightSelection = False
        Me.MyDateNav.HighlightTodayCellWhenSelected = False
        Me.MyDateNav.Location = New System.Drawing.Point(0, 0)
        Me.MyDateNav.Name = "MyDateNav"
        Me.MyDateNav.NavigationMode = DevExpress.XtraScheduler.DateNavigationMode.ScrollCalendar
        Me.MyDateNav.SchedulerControl = Me.MyScheduler
        Me.MyDateNav.SelectionMode = DevExpress.XtraEditors.Repository.CalendarSelectionMode.[Single]
        Me.MyDateNav.ShowFooter = False
        Me.MyDateNav.ShowHeader = False
        Me.MyDateNav.ShowMonthNavigationButtons = DevExpress.Utils.DefaultBoolean.[False]
        Me.MyDateNav.ShowWeekNumbers = False
        Me.MyDateNav.ShowYearNavigationButtons = DevExpress.Utils.DefaultBoolean.[False]
        Me.MyDateNav.Size = New System.Drawing.Size(689, 275)
        Me.MyDateNav.TabIndex = 11
        '
        'gbxDensity
        '
        Me.gbxDensity.Controls.Add(Me.CareLabel10)
        Me.gbxDensity.Controls.Add(Me.CareLabel9)
        Me.gbxDensity.Controls.Add(Me.CareLabel8)
        Me.gbxDensity.Controls.Add(Me.CareLabel7)
        Me.gbxDensity.Controls.Add(Me.CareLabel6)
        Me.gbxDensity.Controls.Add(Me.CareLabel5)
        Me.gbxDensity.Controls.Add(Me.CareLabel4)
        Me.gbxDensity.Controls.Add(Me.txt100)
        Me.gbxDensity.Controls.Add(Me.txt20)
        Me.gbxDensity.Controls.Add(Me.txt40)
        Me.gbxDensity.Controls.Add(Me.txt80)
        Me.gbxDensity.Controls.Add(Me.txt60)
        Me.gbxDensity.Dock = DockStyle.Bottom
        Me.gbxDensity.Location = New System.Drawing.Point(0, 239)
        Me.gbxDensity.Name = "gbxDensity"
        Me.gbxDensity.ShowCaption = False
        Me.gbxDensity.Size = New System.Drawing.Size(689, 36)
        Me.gbxDensity.TabIndex = 12
        Me.gbxDensity.Text = "GroupControl1"
        '
        'CareLabel10
        '
        Me.CareLabel10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel10.Location = New System.Drawing.Point(477, 11)
        Me.CareLabel10.Name = "CareLabel10"
        Me.CareLabel10.Size = New System.Drawing.Size(0, 15)
        Me.CareLabel10.TabIndex = 23
        Me.CareLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(426, 11)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(45, 15)
        Me.CareLabel9.TabIndex = 22
        Me.CareLabel9.Text = "81-100%"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(350, 11)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(39, 15)
        Me.CareLabel8.TabIndex = 21
        Me.CareLabel8.Text = "61-80%"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(279, 11)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(39, 15)
        Me.CareLabel7.TabIndex = 19
        Me.CareLabel7.Text = "41-60%"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(208, 11)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(39, 15)
        Me.CareLabel6.TabIndex = 18
        Me.CareLabel6.Text = "21-40%"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(143, 11)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(33, 15)
        Me.CareLabel5.TabIndex = 17
        Me.CareLabel5.Text = "0-20%"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(9, 11)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(102, 15)
        Me.CareLabel4.TabIndex = 16
        Me.CareLabel4.Text = "Occupancy Density"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt100
        '
        Me.txt100.Location = New System.Drawing.Point(400, 9)
        Me.txt100.Name = "txt100"
        Me.txt100.Properties.Appearance.BackColor = System.Drawing.Color.Green
        Me.txt100.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt100.Properties.Appearance.Options.UseBackColor = True
        Me.txt100.Properties.Appearance.Options.UseFont = True
        Me.txt100.Properties.ReadOnly = True
        Me.txt100.Size = New System.Drawing.Size(20, 20)
        Me.txt100.TabIndex = 10
        Me.txt100.TabStop = False
        '
        'txt20
        '
        Me.txt20.Location = New System.Drawing.Point(117, 9)
        Me.txt20.Name = "txt20"
        Me.txt20.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txt20.Properties.Appearance.Options.UseBackColor = True
        Me.txt20.Properties.ReadOnly = True
        Me.txt20.Size = New System.Drawing.Size(20, 20)
        Me.txt20.TabIndex = 5
        Me.txt20.TabStop = False
        '
        'txt40
        '
        Me.txt40.Location = New System.Drawing.Point(182, 9)
        Me.txt40.Name = "txt40"
        Me.txt40.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txt40.Properties.Appearance.Options.UseBackColor = True
        Me.txt40.Properties.ReadOnly = True
        Me.txt40.Size = New System.Drawing.Size(20, 20)
        Me.txt40.TabIndex = 9
        Me.txt40.TabStop = False
        '
        'txt80
        '
        Me.txt80.Location = New System.Drawing.Point(324, 9)
        Me.txt80.Name = "txt80"
        Me.txt80.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txt80.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt80.Properties.Appearance.Options.UseBackColor = True
        Me.txt80.Properties.Appearance.Options.UseFont = True
        Me.txt80.Properties.ReadOnly = True
        Me.txt80.Size = New System.Drawing.Size(20, 20)
        Me.txt80.TabIndex = 8
        Me.txt80.TabStop = False
        '
        'txt60
        '
        Me.txt60.Location = New System.Drawing.Point(253, 9)
        Me.txt60.Name = "txt60"
        Me.txt60.Properties.Appearance.BackColor = System.Drawing.Color.Lime
        Me.txt60.Properties.Appearance.Options.UseBackColor = True
        Me.txt60.Properties.ReadOnly = True
        Me.txt60.Size = New System.Drawing.Size(20, 20)
        Me.txt60.TabIndex = 6
        Me.txt60.TabStop = False
        '
        'txtOrange
        '
        Me.txtOrange.Location = New System.Drawing.Point(664, 9)
        Me.txtOrange.Name = "txtOrange"
        Me.txtOrange.Properties.Appearance.BackColor = System.Drawing.Color.Orange
        Me.txtOrange.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrange.Properties.Appearance.Options.UseBackColor = True
        Me.txtOrange.Properties.Appearance.Options.UseFont = True
        Me.txtOrange.Properties.ReadOnly = True
        Me.txtOrange.Size = New System.Drawing.Size(20, 20)
        Me.txtOrange.TabIndex = 19
        Me.txtOrange.TabStop = False
        '
        'YearView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.Controls.Add(Me.gbxDensity)
        Me.Controls.Add(Me.MyDateNav)
        Me.Controls.Add(Me.gbxLegend)
        Me.Controls.Add(Me.MyScheduler)
        Me.Name = "YearView"
        Me.Size = New System.Drawing.Size(689, 311)
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBlue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MyScheduler, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MyStorage, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxLegend, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxLegend.ResumeLayout(False)
        Me.gbxLegend.PerformLayout()
        CType(Me.MyDateNav.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MyDateNav, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxDensity, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxDensity.ResumeLayout(False)
        Me.gbxDensity.PerformLayout()
        CType(Me.txt100.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt20.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt40.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt80.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt60.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOrange.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MyScheduler As DevExpress.XtraScheduler.SchedulerControl
    Friend WithEvents MyStorage As DevExpress.XtraScheduler.SchedulerStorage
    Friend WithEvents txtRed As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtBlue As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtGreen As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtYellow As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ToolTipController1 As DevExpress.Utils.ToolTipController
    Friend WithEvents MyDateNav As DevExpress.XtraScheduler.DateNavigator
    Friend WithEvents lblBlue As Care.Controls.CareLabel
    Friend WithEvents lblRed As Care.Controls.CareLabel
    Friend WithEvents lblYellow As Care.Controls.CareLabel
    Friend WithEvents lblGreen As Care.Controls.CareLabel
    Friend WithEvents CareLabel10 As Care.Controls.CareLabel
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents txt100 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt20 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt40 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt80 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt60 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents gbxLegend As Care.Controls.CareFrame
    Friend WithEvents gbxDensity As Care.Controls.CareFrame
    Friend WithEvents txtOrange As DevExpress.XtraEditors.TextEdit
End Class
