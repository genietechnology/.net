﻿

Imports Care.Global
Imports System.ComponentModel

<DefaultEvent("ManualCalculationValidated")>
Public Class ManualCalculation

    Public Event ManualCalculationValidated(sender As Object, e As EventArgs)
    Public Event Cleared(sender As Object, e As EventArgs)

    Private m_BackColour As Drawing.Color
    Private m_ForeColour As Drawing.Color
    Private m_Calculated As Boolean = False
    Private m_CalcDay As Decimal = 0
    Private m_CalcWeek As Decimal = 0
    Private m_CalcMonth As Decimal = 0
    Private m_CalcAnnum As Decimal = 0

    Private Sub ManualCalculation_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cbxMultiplier.AddItem("hours per week")
        cbxMultiplier.AddItem("days per week")
        cbxMultiplier.AddItem("days per annum")
        cbxMultiplier.SelectedIndex = 1

        cbxPer.AddItem("hour")
        cbxPer.AddItem("day")
        cbxPer.AddItem("week")
        cbxPer.SelectedIndex = 1

    End Sub

#Region "Properties"

    Public Overrides Property BackColor As Drawing.Color
        Get
            Return m_BackColour
        End Get
        Set(value As Drawing.Color)
            m_BackColour = value
            txtUnits.BackColor = m_BackColour
            cbxMultiplier.BackColor = m_BackColour
            txtRate.BackColor = m_BackColour
            cbxPer.BackColor = m_BackColour
            txtWeeks.BackColor = m_BackColour
        End Set
    End Property

    Public Property CalcForeColor As Drawing.Color
        Get
            Return m_ForeColour
        End Get
        Set(value As Drawing.Color)
            m_ForeColour = value
            txtCalcDay.ForeColor = m_ForeColour
            txtCalcWeek.ForeColor = m_ForeColour
            txtCalcMonth.ForeColor = m_ForeColour
            txtCalcAnnum.ForeColor = m_ForeColour
        End Set
    End Property

    Public Property Units As Decimal
        Get
            Return ValueHandler.ConvertDecimal(txtUnits.Text)
        End Get
        Set(value As Decimal)
            txtUnits.Text = ValueHandler.MoneyAsText(value)
        End Set
    End Property

    Public Property Multiplier As String
        Get
            Return cbxMultiplier.Text
        End Get
        Set(value As String)
            cbxMultiplier.Text = value
        End Set
    End Property

    Public Property Rate As Decimal
        Get
            Return ValueHandler.ConvertDecimal(txtRate.Text)
        End Get
        Set(value As Decimal)
            txtRate.Text = ValueHandler.MoneyAsText(value)
        End Set
    End Property

    Public Property Per As String
        Get
            Return cbxPer.Text
        End Get
        Set(value As String)
            cbxPer.Text = value
        End Set
    End Property

    Public Property Weeks As Decimal
        Get
            Return ValueHandler.ConvertDecimal(txtWeeks.Text)
        End Get
        Set(value As Decimal)
            txtWeeks.Text = ValueHandler.MoneyAsText(value)
        End Set
    End Property

    Public ReadOnly Property CalculatedDay As Decimal
        Get
            Return m_CalcDay
        End Get
    End Property

    Public ReadOnly Property CalculatedWeek As Decimal
        Get
            Return m_CalcWeek
        End Get
    End Property

    Public ReadOnly Property CalculatedMonth As Decimal
        Get
            Return m_CalcMonth
        End Get
    End Property

    Public ReadOnly Property CalculatedAnnum As Decimal
        Get
            Return m_CalcAnnum
        End Get
    End Property

    Public ReadOnly Property Calculated As Boolean
        Get
            Return m_Calculated
        End Get
    End Property

#End Region

    Public Sub Clear()

        m_Calculated = False

        m_CalcDay = 0
        m_CalcWeek = 0
        m_CalcMonth = 0
        m_CalcAnnum = 0

        txtUnits.Text = ""
        cbxMultiplier.SelectedIndex = 1
        txtRate.Text = ""
        cbxPer.SelectedIndex = 1
        txtWeeks.Text = ""

        txtCalcDay.Text = ""
        txtCalcWeek.Text = ""
        txtCalcMonth.Text = ""
        txtCalcAnnum.Text = ""

    End Sub

    Public Sub Recalculate()

        m_Calculated = False

        Select Case cbxMultiplier.Text

            Case "hours per week"
                cbxPer.SelectedIndex = 0
                cbxPer.ReadOnly = True
                txtWeeks.ReadOnly = False

            Case "days per annum"
                cbxPer.SelectedIndex = 1
                cbxPer.ReadOnly = True
                txtWeeks.Text = "1"
                txtWeeks.ReadOnly = True

            Case Else
                cbxPer.ReadOnly = False
                txtWeeks.ReadOnly = False

        End Select

        If txtUnits.Text = "" Then Exit Sub
        If txtRate.Text = "" Then Exit Sub
        If txtWeeks.Text = "" Then Exit Sub

        Dim _Units As Decimal = CDec(txtUnits.Text)
        Dim _Rate As Decimal = CDec(txtRate.Text)
        Dim _Weeks As Decimal = CDec(txtWeeks.Text)

        'if we have specified a daily amount, we need to establish a weekly amount
        Select Case cbxMultiplier.Text

            Case "hours per week"
                'essentially a hard coded calculation as many of the options are defaulted and disabled
                m_CalcWeek = _Units * _Rate
                m_CalcDay = m_CalcWeek / 5
                m_CalcAnnum = m_CalcWeek * _Weeks

            Case "days per week"

                Select Case cbxPer.Text

                    Case "hour (x 10 hrs)"
                        m_CalcDay = _Rate * 10
                        m_CalcWeek = m_CalcDay * _Units
                        m_CalcAnnum = m_CalcWeek * _Weeks

                    Case "day"
                        m_CalcDay = _Rate
                        m_CalcWeek = m_CalcDay * _Units
                        m_CalcAnnum = m_CalcWeek * _Weeks

                    Case "week"
                        m_CalcWeek = _Rate
                        m_CalcDay = m_CalcWeek / _Units
                        m_CalcAnnum = m_CalcWeek * _Weeks

                End Select

            Case "days per annum"
                'essentially a hard coded calculation as many of the options are defaulted and disabled
                m_CalcDay = _Rate
                m_CalcAnnum = _Units * m_CalcDay
                m_CalcWeek = m_CalcAnnum / 52

        End Select

        m_CalcMonth = m_CalcAnnum / 12

        txtCalcDay.Text = ValueHandler.MoneyAsText(m_CalcDay)
        txtCalcWeek.Text = ValueHandler.MoneyAsText(m_CalcWeek)
        txtCalcMonth.Text = ValueHandler.MoneyAsText(m_CalcMonth)
        txtCalcAnnum.Text = ValueHandler.MoneyAsText(m_CalcAnnum)

        m_Calculated = True

    End Sub

#Region "Control Events"

    Private Sub txtUnits_Validating(sender As Object, e As ComponentModel.CancelEventArgs) Handles txtUnits.Validating
        Recalculate()
    End Sub

    Private Sub cbxMultiplier_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxMultiplier.SelectedIndexChanged
        Recalculate()
    End Sub

    Private Sub txtRate_Validating(sender As Object, e As ComponentModel.CancelEventArgs) Handles txtRate.Validating
        Recalculate()
    End Sub

    Private Sub cbxPer_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxPer.SelectedIndexChanged
        Recalculate()
    End Sub

    Private Sub txtWeeks_Validating(sender As Object, e As ComponentModel.CancelEventArgs) Handles txtWeeks.Validating
        Recalculate()
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        Clear()
        RaiseEvent Cleared(Me, e)
    End Sub

#End Region

    Private Sub ManualCalculation_Validated(sender As Object, e As EventArgs) Handles Me.Validated
        RaiseEvent ManualCalculationValidated(Me, e)
    End Sub
End Class
