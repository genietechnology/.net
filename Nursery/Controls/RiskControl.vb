﻿Public Class RiskControl

    Private m_Severity As Integer = 0
    Private m_Likelihood As Integer = 0
    Private m_Rating As Integer = 0

    Private m_LightGreen As Drawing.Color
    Private m_LightOrange As Drawing.Color
    Private m_LightRed As Drawing.Color

    Private m_DarkGreen As Drawing.Color
    Private m_DarkOrange As Drawing.Color
    Private m_DarkRed As Drawing.Color

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_DarkGreen = btn11.Appearance.BackColor
        m_DarkOrange = btn22.Appearance.BackColor
        m_DarkRed = btn33.Appearance.BackColor

        m_LightGreen = btn21.Appearance.BackColor
        m_LightOrange = btn31.Appearance.BackColor
        m_LightRed = btn32.Appearance.BackColor

        SetRating(0, 0)

    End Sub

#Region "Properties"

    Public Property Severity As Integer
        Get
            Return m_Severity
        End Get
        Set(value As Integer)
            m_Severity = value
            SetRating(m_Likelihood, m_Severity)
        End Set
    End Property

    Public Property Likelihood As Integer
        Get
            Return m_Likelihood
        End Get
        Set(value As Integer)
            m_Likelihood = value
            SetRating(m_Likelihood, m_Severity)
        End Set
    End Property

    Public ReadOnly Property Rating As Integer
        Get
            Return m_Rating
        End Get
    End Property

#End Region

    Private Sub SetRating(ByVal Likelihood As Integer, ByVal Severity As Integer)

        m_Likelihood = Likelihood
        m_Severity = Severity
        m_Rating = m_Likelihood * m_Severity

        ResetColours()

        If Likelihood < 1 OrElse Severity < 1 Then
            'don't set the control colours
        Else

            Select Case m_Likelihood

                Case 1
                    If m_Severity = 1 Then btn11.Appearance.BackColor = m_DarkGreen
                    If m_Severity = 2 Then btn12.Appearance.BackColor = m_DarkGreen
                    If m_Severity = 3 Then btn13.Appearance.BackColor = m_DarkOrange

                Case 2
                    If m_Severity = 1 Then btn21.Appearance.BackColor = m_DarkGreen
                    If m_Severity = 2 Then btn22.Appearance.BackColor = m_DarkOrange
                    If m_Severity = 3 Then btn23.Appearance.BackColor = m_DarkRed

                Case 3
                    If m_Severity = 1 Then btn31.Appearance.BackColor = m_DarkOrange
                    If m_Severity = 2 Then btn32.Appearance.BackColor = m_DarkRed
                    If m_Severity = 3 Then btn33.Appearance.BackColor = m_DarkRed

            End Select

        End If

    End Sub

    Private Sub ResetColours()

        btn11.Appearance.BackColor = m_LightGreen
        btn12.Appearance.BackColor = m_LightGreen
        btn13.Appearance.BackColor = m_LightOrange

        btn21.Appearance.BackColor = m_LightGreen
        btn22.Appearance.BackColor = m_LightOrange
        btn23.Appearance.BackColor = m_LightRed

        btn31.Appearance.BackColor = m_LightOrange
        btn32.Appearance.BackColor = m_LightRed
        btn33.Appearance.BackColor = m_LightRed

    End Sub

#Region "Buttons"

    Private Sub btn11_Click(sender As Object, e As EventArgs) Handles btn11.Click
        SetRating(1, 1)
    End Sub

    Private Sub btn21_Click(sender As Object, e As EventArgs) Handles btn21.Click
        SetRating(2, 1)
    End Sub

    Private Sub btn31_Click(sender As Object, e As EventArgs) Handles btn31.Click
        SetRating(3, 1)
    End Sub

    Private Sub btn12_Click(sender As Object, e As EventArgs) Handles btn12.Click
        SetRating(1, 2)
    End Sub

    Private Sub btn22_Click(sender As Object, e As EventArgs) Handles btn22.Click
        SetRating(2, 2)
    End Sub

    Private Sub btn32_Click(sender As Object, e As EventArgs) Handles btn32.Click
        SetRating(3, 2)
    End Sub

    Private Sub btn13_Click(sender As Object, e As EventArgs) Handles btn13.Click
        SetRating(1, 3)
    End Sub

    Private Sub btn23_Click(sender As Object, e As EventArgs) Handles btn23.Click
        SetRating(2, 3)
    End Sub

    Private Sub btn33_Click(sender As Object, e As EventArgs) Handles btn33.Click
        SetRating(3, 3)
    End Sub

#End Region

End Class
