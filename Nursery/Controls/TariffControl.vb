﻿

Imports Care.Global
Imports Care.Data

Public Class TariffControl

    Private Enum EnumIDorText
        ID
        Text
    End Enum

    Public Enum EnumPopulateMode
        Daily
        Weekly
        Monthly
    End Enum

    Private m_IsPopulated As Boolean = False
    Private m_Enabled As Boolean = True
    Private m_PopulateMode As EnumPopulateMode
    Private m_TariffList As List(Of Business.Tariff) = Nothing
    Private m_OverrideAvailable As Boolean = False
    Private m_OverrideType As String = ""
    Private m_FEEE As Boolean = False
    Private m_AllowBoltOns As Boolean = False
    Private m_ValidateTimesAgainstDuration As Boolean = False
    Private m_TariffDuration As Decimal = 0
    Private m_BoltOnIDs As String = ""
    Private m_BoltOnNames As String = ""
    Private m_SiteID As Guid

    Private Sub TariffControl_Load(sender As Object, e As EventArgs) Handles Me.Load
        'population triggered by setting the siteID
    End Sub

#Region "Properties"

    Public ReadOnly Property OverrideAvailable As Boolean
        Get
            Return m_OverrideAvailable
        End Get
    End Property
    Public ReadOnly Property OverrideType As String
        Get
            Return m_OverrideType
        End Get
    End Property

    Public Property ComboValue As Guid?
        Get
            If m_IsPopulated And cbxCombo.SelectedItem IsNot Nothing Then
                If IsDBNull(cbxCombo.SelectedItem) Then
                    Return Nothing
                Else
                    Dim _item As ComboItem = CType(cbxCombo.SelectedItem, ComboItem)
                    Return New Guid(_item.Value)
                End If
            Else
                Return Nothing
            End If
        End Get
        Set(value As Guid?)

            cbxCombo.SelectedIndex = 0
            If value Is Nothing Then Exit Property
            If value.ToString = "" Then Exit Property
            If Not m_IsPopulated Then Exit Property

            For Each _item As Object In cbxCombo.Properties.Items

                If Not IsDBNull(_item) Then

                    Dim _ComboItem As ComboItem = DirectCast(_item, ComboItem)
                    If _ComboItem.Value = value.ToString Then
                        cbxCombo.SelectedItem = _item
                    End If

                    _ComboItem = Nothing

                End If

            Next

        End Set
    End Property

    Public ReadOnly Property Description As String
        Get
            Return BuildDescription()
        End Get
    End Property

    Public Property Value1 As Decimal
        Get
            Select Case m_OverrideType
                Case "Manual Times", "Time Matrix", "Age Matrix (Times)"
                    Return CleanData.ReturnDecimal(txtTextBox.Text.Replace(":", ""))
                Case Else
                    Return CleanData.ReturnDecimal(txtTextBox.Text)
            End Select
        End Get
        Set(value As Decimal)
            SetValue(txtTextBox, value)
        End Set
    End Property

    Public Property Value2 As Decimal
        Get
            Select Case m_OverrideType
                Case "Manual Times", "Time Matrix", "Age Matrix (Times)"
                    Return CleanData.ReturnDecimal(txtTextbox2.Text.Replace(":", ""))
                Case Else
                    Return CleanData.ReturnDecimal(txtTextbox2.Text)
            End Select
        End Get
        Set(value As Decimal)
            SetValue(txtTextbox2, value)
        End Set
    End Property

    Public Property ValueFEEE As Decimal
        Get
            If m_FEEE Then
                Return CleanData.ReturnDecimal(txtFEEE.Text)
            Else
                Return 0
            End If
        End Get
        Set(value As Decimal)
            SetValue(txtFEEE, value)
        End Set
    End Property

    Public Property SelectedIndex As Integer
        Get
            Return cbxCombo.SelectedIndex
        End Get
        Set(value As Integer)
            If value < 0 Then
                Clear()
                cbxCombo.SelectedIndex = value
            End If
        End Set
    End Property

    Public Property PopulateMode As EnumPopulateMode
        Get
            Return m_PopulateMode
        End Get
        Set(value As EnumPopulateMode)
            m_PopulateMode = value
            PopulateItems()
            Clear()
        End Set
    End Property

    Public Overloads Property Enabled As Boolean
        Get
            Return m_Enabled
        End Get
        Set(value As Boolean)
            m_Enabled = value
            SetControl()
        End Set
    End Property

    Public ReadOnly Property BoltOnNames As String
        Get
            Return ReturnSelectedItems(EnumIDorText.Text)
        End Get
    End Property

    Public Property BoltOns As String
        Get
            Return ReturnSelectedItems(EnumIDorText.ID)
        End Get
        Set(value As String)
            m_BoltOnIDs = value
            SetBoltOns()
        End Set
    End Property

    Public Property SiteID As Guid
        Get

        End Get
        Set(value As Guid)
            m_SiteID = value
            If Not Me.DesignMode Then
                PopulateItems()
            End If
        End Set
    End Property

#End Region

    Private Function ReturnSelectedItems(ByVal ReturnType As EnumIDorText) As String

        Dim _Return As String = ""
        Dim _Separator As String = ""

        For Each _Item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbxBoltOns.Properties.Items
            If _Item.CheckState = CheckState.Checked Then
                If _Return <> "" Then _Separator = ","
                If ReturnType = EnumIDorText.ID Then
                    _Return += _Separator + _Item.Value.ToString
                Else
                    _Return += _Separator + _Item.Description
                End If
            End If
        Next

        Return _Return

    End Function

    Private Sub SetBoltOns()

        ClearBoltOns()

        If m_BoltOnIDs <> "" Then
            For Each _ID As String In m_BoltOnIDs.Split(","c)
                For Each _Item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbxBoltOns.Properties.Items
                    If _Item.Value.ToString.ToUpper = _ID Then
                        _Item.CheckState = CheckState.Checked
                    End If
                Next
            Next
        End If

    End Sub

    Private Function BuildDescription() As String

        If m_OverrideAvailable Then

            Select Case m_OverrideType

                Case "Manual Times", "Time Matrix", "Age Matrix (Times)"
                    Return txtTextBox.Text + " to " + txtTextbox2.Text

                Case "Fixed Price"
                    Return "£ " + txtTextBox.Text

                Case Else
                    Return txtTextBox.Text + " hours"

            End Select

        Else
            Return cbxCombo.Text
        End If

    End Function

    Private Sub SetValue(ByRef TextBox As DevExpress.XtraEditors.TextEdit, ByVal ValueIn As Decimal)

        If TextBox.Name = "txtFEEE" Then
            TextBox.Text = Format(ValueIn, "0.00").ToString
        Else
            If ValueIn = 0 Then
                TextBox.Text = ""
            Else

                Select Case m_OverrideType

                    Case "Manual Times", "Time Matrix"
                        If CInt(ValueIn).ToString.Length = 3 Then
                            TextBox.Text = "0" + CInt(ValueIn).ToString.Substring(0, 1) + ":" + CInt(ValueIn).ToString.Substring(1)
                        Else
                            TextBox.Text = CInt(ValueIn).ToString.Substring(0, 2) + ":" + CInt(ValueIn).ToString.Substring(2)
                        End If

                    Case Else
                        TextBox.Text = Format(ValueIn, "0.00").ToString

                End Select

            End If
        End If

    End Sub

    Private Sub SetControl()

        If m_Enabled Then

            cbxCombo.Enabled = True
            cbxCombo.BackColor = Session.ChangeColour

            cbxBoltOns.Enabled = True
            cbxBoltOns.BackColor = Session.ChangeColour

        Else

            cbxCombo.Enabled = False
            cbxCombo.ResetBackColor()

            cbxBoltOns.Enabled = False
            cbxBoltOns.ResetBackColor()

        End If

    End Sub

    Public Sub HighlightError()
        If txtTextBox.Enabled Then txtTextBox.BackColor = Drawing.Color.LightCoral
        If txtTextbox2.Enabled Then txtTextbox2.BackColor = Drawing.Color.LightCoral
    End Sub

    Public Sub Clear()
        cbxCombo.SelectedIndex = 0
        txtTextBox.Text = ""
        txtTextbox2.Text = ""
        txtFEEE.Text = ""
        ClearBoltOns()
    End Sub

    Public Function TimesValid() As Boolean
        If m_OverrideAvailable Then
            Select Case m_OverrideType
                Case "Manual Times", "Time Matrix", "Age Matrix (Times)"
                    If txtTextBox.Text = "" Then Return False
                    If txtTextbox2.Text = "" Then Return False
                    Return Not ValidateTimes()
                Case Else
                    Return True
            End Select
        Else
            Return True
        End If
    End Function

    Private Sub PopulateItems()

        m_IsPopulated = False
        If Me.DesignMode Then Exit Sub

        '/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        If m_TariffList IsNot Nothing Then m_TariffList.Clear()
        cbxCombo.Properties.Items.Clear()

        If m_PopulateMode = EnumPopulateMode.Daily Then m_TariffList = Business.Tariff.RetreiveAllDailyTariffs(m_SiteID)
        If m_PopulateMode = EnumPopulateMode.Weekly Then m_TariffList = Business.Tariff.RetreiveAllWeeklyTariffs(m_SiteID)
        If m_PopulateMode = EnumPopulateMode.Monthly Then m_TariffList = Business.Tariff.RetreiveAllMonthlyTariffs(m_SiteID)

        cbxCombo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True
        cbxCombo.Properties.Items.Add(DBNull.Value)

        For Each _t As Business.Tariff In m_TariffList
            cbxCombo.Properties.Items.Add(New ComboItem(_t._ID.Value.ToString, _t._Name))
        Next

        '/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        cbxBoltOns.Properties.Items.Clear()

        Dim _BoltOns As List(Of Business.TariffBoltOn) = Business.TariffBoltOn.RetreiveAll
        If _BoltOns.Count > 0 Then

            Dim _Q As IEnumerable(Of Business.TariffBoltOn) = From _B As Business.TariffBoltOn In _BoltOns
                                                              Order By _B._Name

            For Each _b As Business.TariffBoltOn In _Q
                cbxBoltOns.Properties.Items.Add(New DevExpress.XtraEditors.Controls.CheckedListBoxItem(_b._ID.Value.ToString.ToUpper, _b._Name))
            Next

        End If

        '/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        m_IsPopulated = True

    End Sub

    Public Class ComboItem

        Private _Value As String
        Private _Text As String

        Public Sub New(Value As String, Text As String)
            _Value = Value
            _Text = Text
        End Sub

        Public ReadOnly Property Text As String
            Get
                Return _Text
            End Get
        End Property

        Public ReadOnly Property Value As String
            Get
                Return _Value
            End Get
        End Property

        Public Overrides Function ToString() As String
            Return _Text
        End Function

    End Class

    Private Sub cbxCombo_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbxCombo.SelectedValueChanged
        SetOverride()
    End Sub

    Private Sub SetOverride()

        m_OverrideAvailable = False
        m_OverrideType = ""
        m_AllowBoltOns = False
        m_TariffDuration = 0

        txtTextBox.Text = ""
        txtTextbox2.Text = ""
        txtFEEE.Text = ""

        If cbxCombo.SelectedIndex <= 0 Then
            SetControl(txtTextBox, False, "")
            SetControl(txtTextbox2, False, "")
        Else

            Dim _T As Business.Tariff = m_TariffList(cbxCombo.SelectedIndex - 1)
            If _T._Override Then

                m_OverrideAvailable = True
                m_OverrideType = _T._Type

                Select Case _T._Type

                    Case "Manual Times", "Time Matrix", "Age Matrix (Times)"
                        SetControl(txtTextBox, True, _T._Type)
                        SetControl(txtTextbox2, True, _T._Type)

                    Case "Fixed Price"
                        SetControl(txtTextBox, True, _T._Type)
                        SetControl(txtTextbox2, False, _T._Type)
                        txtTextBox.Text = Format(_T._Rate, "0.00")

                    Case "Duration (Hours)"
                        SetControl(txtTextBox, True, _T._Type)
                        SetControl(txtTextbox2, False, _T._Type)
                        txtTextBox.Text = Format(_T._Duration, "0.00")

                    Case Else
                        SetControl(txtTextBox, False, "")
                        SetControl(txtTextbox2, False, "")

                End Select

            Else
                SetControl(txtTextBox, False, "")
                SetControl(txtTextbox2, False, "")
            End If

            m_FEEE = _T._FundingHours
            If _T._FundingHours Then
                SetControl(txtFEEE, True, "")
            Else
                SetControl(txtFEEE, False, "")
            End If

            m_AllowBoltOns = _T._BoltOns
            If m_AllowBoltOns Then
                cbxBoltOns.Enabled = True
                cbxBoltOns.BackColor = Session.ChangeColour
            Else
                cbxBoltOns.Enabled = False
                cbxBoltOns.ResetBackColor()
                ClearBoltOns()
            End If

            m_TariffDuration = _T._Duration

        End If

    End Sub

    Private Function ValidateTimes() As Boolean

        'return is hooked to e.cancel, so false is actually good!

        Select Case m_OverrideType

            Case "Manual Times", "Time Matrix", "Age Matrix (Times)"

                Dim _from As DateTime? = ConverttoDateTime(txtTextBox.Text)
                Dim _to As DateTime? = ConverttoDateTime(txtTextbox2.Text)

                If _from.HasValue AndAlso _to.HasValue Then
                    If _from.Value >= _to.Value Then
                        txtTextBox.BackColor = Drawing.Color.LightCoral
                        txtTextbox2.BackColor = Drawing.Color.LightCoral
                        Return True
                    Else
                        Return False
                    End If
                Else
                    Return False
                End If

            Case Else
                Return False

        End Select

    End Function

    Private Function ConverttoDateTime(ByVal TextIn As String) As DateTime?

        If TextIn = "" Then Return Nothing

        Try
            Return DateTime.Parse(TextIn)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Private Sub SetControl(ByRef ControlIn As DevExpress.XtraEditors.TextEdit, ByVal Enabled As Boolean, ByVal TariffType As String)

        If Enabled Then

            ControlIn.Enabled = True
            ControlIn.BackColor = Session.ChangeColour
            ControlIn.Tag = TariffType

            ControlIn.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic
            ControlIn.Properties.Mask.UseMaskAsDisplayFormat = True

            Select Case TariffType

                Case "Manual Times", "Time Matrix", "Age Matrix (Times)"
                    ControlIn.ToolTipIconType = DevExpress.Utils.ToolTipIconType.None
                    ControlIn.ToolTip = ""
                    ControlIn.Properties.MaxLength = 5
                    ControlIn.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
                    ControlIn.Properties.Mask.EditMask = "(0?\d|1\d|2[0-3])\:[0-5]\d"

                Case "Fixed Price"
                    ControlIn.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
                    ControlIn.ToolTip = "Enter the price for this Session"
                    ControlIn.Properties.MaxLength = 6
                    ControlIn.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
                    ControlIn.Properties.Mask.EditMask = "##0.00"

                Case "Duration (Hours)"
                    ControlIn.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
                    ControlIn.ToolTip = "Enter the duration (in hours) of this Session"
                    ControlIn.Properties.MaxLength = 5
                    ControlIn.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
                    ControlIn.Properties.Mask.EditMask = "#0.00"

            End Select

            If m_FEEE Then
                txtFEEE.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
                txtFEEE.ToolTip = "Enter the FEEE Hours used for this session."
                txtFEEE.Properties.MaxLength = 5
                txtFEEE.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
                txtFEEE.Properties.Mask.EditMask = "#0.00"
            End If

        Else
            ControlIn.Text = ""
            ControlIn.ToolTipIconType = DevExpress.Utils.ToolTipIconType.None
            ControlIn.ToolTip = ""
            ControlIn.Tag = ""
            ControlIn.Enabled = False
            ControlIn.ResetBackColor()
        End If

    End Sub

    Private Sub txtTextbox2_InvalidValue(sender As Object, e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles txtTextbox2.InvalidValue
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub

    Private Sub txtTextbox2_Validated(sender As Object, e As EventArgs) Handles txtTextbox2.Validated
        If txtTextbox2.Enabled Then
            txtTextbox2.BackColor = Session.ChangeColour
        Else
            txtTextbox2.ResetBackColor()
        End If
    End Sub

    Private Sub txtTextbox2_Validating(sender As Object, e As ComponentModel.CancelEventArgs) Handles txtTextbox2.Validating
        e.Cancel = ValidateTimes()
    End Sub

    Private Sub txtTextBox_InvalidValue(sender As Object, e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles txtTextBox.InvalidValue
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub

    Private Sub txtTextBox_Validated(sender As Object, e As EventArgs) Handles txtTextBox.Validated
        If txtTextBox.Enabled Then
            txtTextBox.BackColor = Session.ChangeColour
        Else
            txtTextBox.ResetBackColor()
        End If
    End Sub

    Private Sub txtTextBox_Validating(sender As Object, e As ComponentModel.CancelEventArgs) Handles txtTextBox.Validating
        e.Cancel = ValidateTimes()
    End Sub

    Private Sub ClearBoltOns()
        For Each _Item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbxBoltOns.Properties.Items
            _Item.CheckState = CheckState.Unchecked
        Next
    End Sub

End Class
