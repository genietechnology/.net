﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TariffControl
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtTextBox = New DevExpress.XtraEditors.TextEdit()
        Me.lblTo = New DevExpress.XtraEditors.LabelControl()
        Me.txtTextbox2 = New DevExpress.XtraEditors.TextEdit()
        Me.cbxCombo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.lblFEEE = New DevExpress.XtraEditors.LabelControl()
        Me.txtFEEE = New DevExpress.XtraEditors.TextEdit()
        Me.TableLayoutPanel1 = New TableLayoutPanel()
        Me.cbxBoltOns = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        CType(Me.txtTextBox.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTextbox2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxCombo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFEEE.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.cbxBoltOns.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtTextBox
        '
        Me.txtTextBox.Dock = DockStyle.Fill
        Me.txtTextBox.EditValue = "23:59"
        Me.txtTextBox.Enabled = False
        Me.txtTextBox.Location = New System.Drawing.Point(180, 1)
        Me.txtTextBox.Margin = New Padding(1)
        Me.txtTextBox.Name = "txtTextBox"
        Me.txtTextBox.Properties.MaxLength = 6
        Me.txtTextBox.Size = New System.Drawing.Size(38, 20)
        Me.txtTextBox.TabIndex = 1
        '
        'lblTo
        '
        Me.lblTo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lblTo.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblTo.Dock = DockStyle.Fill
        Me.lblTo.Enabled = False
        Me.lblTo.Location = New System.Drawing.Point(220, 1)
        Me.lblTo.Margin = New Padding(1)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(18, 20)
        Me.lblTo.TabIndex = 2
        Me.lblTo.Text = "to"
        '
        'txtTextbox2
        '
        Me.txtTextbox2.Dock = DockStyle.Fill
        Me.txtTextbox2.EditValue = "23:59"
        Me.txtTextbox2.Enabled = False
        Me.txtTextbox2.Location = New System.Drawing.Point(240, 1)
        Me.txtTextbox2.Margin = New Padding(1)
        Me.txtTextbox2.Name = "txtTextbox2"
        Me.txtTextbox2.Properties.MaxLength = 6
        Me.txtTextbox2.Size = New System.Drawing.Size(38, 20)
        Me.txtTextbox2.TabIndex = 3
        '
        'cbxCombo
        '
        Me.cbxCombo.Dock = DockStyle.Fill
        Me.cbxCombo.Location = New System.Drawing.Point(1, 1)
        Me.cbxCombo.Margin = New Padding(1)
        Me.cbxCombo.Name = "cbxCombo"
        Me.cbxCombo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxCombo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxCombo.Size = New System.Drawing.Size(169, 20)
        Me.cbxCombo.TabIndex = 0
        '
        'lblFEEE
        '
        Me.lblFEEE.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lblFEEE.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblFEEE.Dock = DockStyle.Fill
        Me.lblFEEE.Enabled = False
        Me.lblFEEE.Location = New System.Drawing.Point(288, 1)
        Me.lblFEEE.Margin = New Padding(1)
        Me.lblFEEE.Name = "lblFEEE"
        Me.lblFEEE.Size = New System.Drawing.Size(48, 20)
        Me.lblFEEE.TabIndex = 4
        Me.lblFEEE.Text = "FEEE Hrs"
        '
        'txtFEEE
        '
        Me.txtFEEE.Dock = DockStyle.Fill
        Me.txtFEEE.EditValue = "23:59"
        Me.txtFEEE.Enabled = False
        Me.txtFEEE.Location = New System.Drawing.Point(338, 1)
        Me.txtFEEE.Margin = New Padding(1)
        Me.txtFEEE.Name = "txtFEEE"
        Me.txtFEEE.Properties.MaxLength = 6
        Me.txtFEEE.Size = New System.Drawing.Size(38, 20)
        Me.txtFEEE.TabIndex = 5
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 10
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Absolute, 8.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Absolute, 40.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Absolute, 40.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Absolute, 8.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Absolute, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Absolute, 40.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Absolute, 8.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.cbxCombo, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtFEEE, 7, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.cbxBoltOns, 9, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblFEEE, 6, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtTextBox, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtTextbox2, 4, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblTo, 3, 0)
        Me.TableLayoutPanel1.Dock = DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New RowStyle(SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(556, 22)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'cbxBoltOns
        '
        Me.cbxBoltOns.Dock = DockStyle.Fill
        Me.cbxBoltOns.Location = New System.Drawing.Point(386, 1)
        Me.cbxBoltOns.Margin = New Padding(1)
        Me.cbxBoltOns.Name = "cbxBoltOns"
        Me.cbxBoltOns.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxBoltOns.Properties.SelectAllItemVisible = False
        Me.cbxBoltOns.Size = New System.Drawing.Size(169, 20)
        Me.cbxBoltOns.TabIndex = 6
        '
        'TariffControl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "TariffControl"
        Me.Size = New System.Drawing.Size(556, 22)
        CType(Me.txtTextBox.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTextbox2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxCombo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFEEE.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.cbxBoltOns.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtTextBox As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblTo As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtTextbox2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cbxCombo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents lblFEEE As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtFEEE As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents cbxBoltOns As DevExpress.XtraEditors.CheckedComboBoxEdit

End Class
