﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ManualCalculation
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnClear = New Care.Controls.CareButton(Me.components)
        Me.CareLabel10 = New Care.Controls.CareLabel(Me.components)
        Me.cbxMultiplier = New Care.Controls.CareComboBox(Me.components)
        Me.txtWeeks = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.txtCalcWeek = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel13 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel12 = New Care.Controls.CareLabel(Me.components)
        Me.txtCalcAnnum = New Care.Controls.CareTextBox(Me.components)
        Me.txtCalcMonth = New Care.Controls.CareTextBox(Me.components)
        Me.txtCalcDay = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.txtRate = New Care.Controls.CareTextBox(Me.components)
        Me.cbxPer = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.txtUnits = New Care.Controls.CareTextBox(Me.components)
        CType(Me.cbxMultiplier.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtWeeks.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCalcWeek.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCalcAnnum.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCalcMonth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCalcDay.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxPer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUnits.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnClear
        '
        Me.btnClear.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClear.Appearance.Options.UseFont = True
        Me.btnClear.Location = New System.Drawing.Point(803, 22)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(50, 24)
        Me.btnClear.TabIndex = 10
        Me.btnClear.Text = "Clear"
        '
        'CareLabel10
        '
        Me.CareLabel10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.CareLabel10.Location = New System.Drawing.Point(403, 2)
        Me.CareLabel10.Name = "CareLabel10"
        Me.CareLabel10.Size = New System.Drawing.Size(48, 15)
        Me.CareLabel10.TabIndex = 11
        Me.CareLabel10.Text = "weeks"
        Me.CareLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cbxMultiplier
        '
        Me.cbxMultiplier.AllowBlank = False
        Me.cbxMultiplier.DataSource = Nothing
        Me.cbxMultiplier.DisplayMember = Nothing
        Me.cbxMultiplier.EnterMoveNextControl = True
        Me.cbxMultiplier.Location = New System.Drawing.Point(54, 23)
        Me.cbxMultiplier.Name = "cbxMultiplier"
        Me.cbxMultiplier.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxMultiplier.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxMultiplier.Properties.Appearance.Options.UseFont = True
        Me.cbxMultiplier.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxMultiplier.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxMultiplier.SelectedValue = Nothing
        Me.cbxMultiplier.Size = New System.Drawing.Size(118, 22)
        Me.cbxMultiplier.TabIndex = 2
        Me.cbxMultiplier.Tag = "AEM"
        Me.cbxMultiplier.ValueMember = Nothing
        '
        'txtWeeks
        '
        Me.txtWeeks.CharacterCasing = CharacterCasing.Normal
        Me.txtWeeks.EditValue = ""
        Me.txtWeeks.EnterMoveNextControl = True
        Me.txtWeeks.Location = New System.Drawing.Point(403, 23)
        Me.txtWeeks.MaxLength = 5
        Me.txtWeeks.Name = "txtWeeks"
        Me.txtWeeks.NumericAllowNegatives = False
        Me.txtWeeks.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtWeeks.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtWeeks.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtWeeks.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtWeeks.Properties.Appearance.Options.UseFont = True
        Me.txtWeeks.Properties.Appearance.Options.UseTextOptions = True
        Me.txtWeeks.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtWeeks.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtWeeks.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtWeeks.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtWeeks.Properties.MaxLength = 5
        Me.txtWeeks.Size = New System.Drawing.Size(48, 22)
        Me.txtWeeks.TabIndex = 5
        Me.txtWeeks.TextAlign = HorizontalAlignment.Right
        Me.txtWeeks.ToolTipText = ""
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(392, 26)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(5, 15)
        Me.CareLabel7.TabIndex = 18
        Me.CareLabel7.Text = "x"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.CareLabel2.Location = New System.Drawing.Point(554, 2)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(77, 15)
        Me.CareLabel2.TabIndex = 13
        Me.CareLabel2.Text = "per week"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtCalcWeek
        '
        Me.txtCalcWeek.CharacterCasing = CharacterCasing.Normal
        Me.txtCalcWeek.EnterMoveNextControl = True
        Me.txtCalcWeek.Location = New System.Drawing.Point(554, 23)
        Me.txtCalcWeek.MaxLength = 14
        Me.txtCalcWeek.Name = "txtCalcWeek"
        Me.txtCalcWeek.NumericAllowNegatives = False
        Me.txtCalcWeek.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtCalcWeek.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCalcWeek.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtCalcWeek.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtCalcWeek.Properties.Appearance.Options.UseFont = True
        Me.txtCalcWeek.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCalcWeek.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtCalcWeek.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtCalcWeek.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtCalcWeek.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtCalcWeek.Properties.MaxLength = 14
        Me.txtCalcWeek.Properties.ReadOnly = True
        Me.txtCalcWeek.Size = New System.Drawing.Size(77, 22)
        Me.txtCalcWeek.TabIndex = 7
        Me.txtCalcWeek.TabStop = False
        Me.txtCalcWeek.TextAlign = HorizontalAlignment.Right
        Me.txtCalcWeek.ToolTipText = ""
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel14.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.CareLabel14.Location = New System.Drawing.Point(720, 2)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(77, 15)
        Me.CareLabel14.TabIndex = 15
        Me.CareLabel14.Text = "per annum"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CareLabel13
        '
        Me.CareLabel13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.CareLabel13.Location = New System.Drawing.Point(637, 2)
        Me.CareLabel13.Name = "CareLabel13"
        Me.CareLabel13.Size = New System.Drawing.Size(77, 15)
        Me.CareLabel13.TabIndex = 14
        Me.CareLabel13.Text = "per month"
        Me.CareLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CareLabel12
        '
        Me.CareLabel12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.CareLabel12.Location = New System.Drawing.Point(471, 2)
        Me.CareLabel12.Name = "CareLabel12"
        Me.CareLabel12.Size = New System.Drawing.Size(77, 15)
        Me.CareLabel12.TabIndex = 12
        Me.CareLabel12.Text = "per day"
        Me.CareLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtCalcAnnum
        '
        Me.txtCalcAnnum.CharacterCasing = CharacterCasing.Normal
        Me.txtCalcAnnum.EnterMoveNextControl = True
        Me.txtCalcAnnum.Location = New System.Drawing.Point(720, 23)
        Me.txtCalcAnnum.MaxLength = 14
        Me.txtCalcAnnum.Name = "txtCalcAnnum"
        Me.txtCalcAnnum.NumericAllowNegatives = False
        Me.txtCalcAnnum.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtCalcAnnum.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCalcAnnum.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtCalcAnnum.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtCalcAnnum.Properties.Appearance.Options.UseFont = True
        Me.txtCalcAnnum.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCalcAnnum.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtCalcAnnum.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtCalcAnnum.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtCalcAnnum.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtCalcAnnum.Properties.MaxLength = 14
        Me.txtCalcAnnum.Properties.ReadOnly = True
        Me.txtCalcAnnum.Size = New System.Drawing.Size(77, 22)
        Me.txtCalcAnnum.TabIndex = 9
        Me.txtCalcAnnum.TabStop = False
        Me.txtCalcAnnum.TextAlign = HorizontalAlignment.Right
        Me.txtCalcAnnum.ToolTipText = ""
        '
        'txtCalcMonth
        '
        Me.txtCalcMonth.CharacterCasing = CharacterCasing.Normal
        Me.txtCalcMonth.EnterMoveNextControl = True
        Me.txtCalcMonth.Location = New System.Drawing.Point(637, 23)
        Me.txtCalcMonth.MaxLength = 14
        Me.txtCalcMonth.Name = "txtCalcMonth"
        Me.txtCalcMonth.NumericAllowNegatives = False
        Me.txtCalcMonth.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtCalcMonth.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCalcMonth.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtCalcMonth.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtCalcMonth.Properties.Appearance.Options.UseFont = True
        Me.txtCalcMonth.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCalcMonth.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtCalcMonth.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtCalcMonth.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtCalcMonth.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtCalcMonth.Properties.MaxLength = 14
        Me.txtCalcMonth.Properties.ReadOnly = True
        Me.txtCalcMonth.Size = New System.Drawing.Size(77, 22)
        Me.txtCalcMonth.TabIndex = 8
        Me.txtCalcMonth.TabStop = False
        Me.txtCalcMonth.TextAlign = HorizontalAlignment.Right
        Me.txtCalcMonth.ToolTipText = ""
        '
        'txtCalcDay
        '
        Me.txtCalcDay.CharacterCasing = CharacterCasing.Normal
        Me.txtCalcDay.EnterMoveNextControl = True
        Me.txtCalcDay.Location = New System.Drawing.Point(471, 23)
        Me.txtCalcDay.MaxLength = 14
        Me.txtCalcDay.Name = "txtCalcDay"
        Me.txtCalcDay.NumericAllowNegatives = False
        Me.txtCalcDay.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtCalcDay.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCalcDay.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtCalcDay.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtCalcDay.Properties.Appearance.Options.UseFont = True
        Me.txtCalcDay.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCalcDay.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtCalcDay.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtCalcDay.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtCalcDay.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtCalcDay.Properties.MaxLength = 14
        Me.txtCalcDay.Properties.ReadOnly = True
        Me.txtCalcDay.Size = New System.Drawing.Size(77, 22)
        Me.txtCalcDay.TabIndex = 6
        Me.txtCalcDay.TabStop = False
        Me.txtCalcDay.TextAlign = HorizontalAlignment.Right
        Me.txtCalcDay.ToolTipText = ""
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(457, 26)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(8, 15)
        Me.CareLabel9.TabIndex = 19
        Me.CareLabel9.Text = "="
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(255, 26)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(17, 15)
        Me.CareLabel8.TabIndex = 17
        Me.CareLabel8.Text = "per"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRate
        '
        Me.txtRate.CharacterCasing = CharacterCasing.Normal
        Me.txtRate.EnterMoveNextControl = True
        Me.txtRate.Location = New System.Drawing.Point(189, 23)
        Me.txtRate.MaxLength = 14
        Me.txtRate.Name = "txtRate"
        Me.txtRate.NumericAllowNegatives = False
        Me.txtRate.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtRate.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRate.Properties.Appearance.Options.UseFont = True
        Me.txtRate.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtRate.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtRate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtRate.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRate.Properties.MaxLength = 14
        Me.txtRate.Size = New System.Drawing.Size(60, 22)
        Me.txtRate.TabIndex = 3
        Me.txtRate.TextAlign = HorizontalAlignment.Right
        Me.txtRate.ToolTipText = ""
        '
        'cbxPer
        '
        Me.cbxPer.AllowBlank = False
        Me.cbxPer.DataSource = Nothing
        Me.cbxPer.DisplayMember = Nothing
        Me.cbxPer.EnterMoveNextControl = True
        Me.cbxPer.Location = New System.Drawing.Point(278, 23)
        Me.cbxPer.Name = "cbxPer"
        Me.cbxPer.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxPer.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxPer.Properties.Appearance.Options.UseFont = True
        Me.cbxPer.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxPer.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxPer.SelectedValue = Nothing
        Me.cbxPer.Size = New System.Drawing.Size(108, 22)
        Me.cbxPer.TabIndex = 4
        Me.cbxPer.Tag = "AEM"
        Me.cbxPer.ValueMember = Nothing
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.CareLabel6.Location = New System.Drawing.Point(189, 2)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(60, 15)
        Me.CareLabel6.TabIndex = 10
        Me.CareLabel6.Text = "rate"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(178, 26)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(5, 15)
        Me.CareLabel5.TabIndex = 16
        Me.CareLabel5.Text = "x"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtUnits
        '
        Me.txtUnits.CharacterCasing = CharacterCasing.Normal
        Me.txtUnits.EnterMoveNextControl = True
        Me.txtUnits.Location = New System.Drawing.Point(0, 23)
        Me.txtUnits.MaxLength = 3
        Me.txtUnits.Name = "txtUnits"
        Me.txtUnits.NumericAllowNegatives = False
        Me.txtUnits.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtUnits.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtUnits.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtUnits.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtUnits.Properties.Appearance.Options.UseFont = True
        Me.txtUnits.Properties.Appearance.Options.UseTextOptions = True
        Me.txtUnits.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtUnits.Properties.Mask.EditMask = "##########;"
        Me.txtUnits.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtUnits.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtUnits.Properties.MaxLength = 3
        Me.txtUnits.Size = New System.Drawing.Size(48, 22)
        Me.txtUnits.TabIndex = 1
        Me.txtUnits.TextAlign = HorizontalAlignment.Right
        Me.txtUnits.ToolTipText = ""
        '
        'ManualCalculation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.CareLabel10)
        Me.Controls.Add(Me.cbxMultiplier)
        Me.Controls.Add(Me.txtWeeks)
        Me.Controls.Add(Me.CareLabel7)
        Me.Controls.Add(Me.CareLabel2)
        Me.Controls.Add(Me.txtCalcWeek)
        Me.Controls.Add(Me.CareLabel14)
        Me.Controls.Add(Me.CareLabel13)
        Me.Controls.Add(Me.CareLabel12)
        Me.Controls.Add(Me.txtCalcAnnum)
        Me.Controls.Add(Me.txtCalcMonth)
        Me.Controls.Add(Me.txtCalcDay)
        Me.Controls.Add(Me.CareLabel9)
        Me.Controls.Add(Me.CareLabel8)
        Me.Controls.Add(Me.txtRate)
        Me.Controls.Add(Me.cbxPer)
        Me.Controls.Add(Me.CareLabel6)
        Me.Controls.Add(Me.CareLabel5)
        Me.Controls.Add(Me.txtUnits)
        Me.Name = "ManualCalculation"
        Me.Size = New System.Drawing.Size(854, 46)
        CType(Me.cbxMultiplier.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtWeeks.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCalcWeek.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCalcAnnum.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCalcMonth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCalcDay.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxPer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUnits.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents CareLabel10 As Care.Controls.CareLabel
    Private WithEvents cbxMultiplier As Care.Controls.CareComboBox
    Private WithEvents txtWeeks As Care.Controls.CareTextBox
    Private WithEvents CareLabel7 As Care.Controls.CareLabel
    Private WithEvents CareLabel2 As Care.Controls.CareLabel
    Private WithEvents txtCalcWeek As Care.Controls.CareTextBox
    Private WithEvents CareLabel14 As Care.Controls.CareLabel
    Private WithEvents CareLabel13 As Care.Controls.CareLabel
    Private WithEvents CareLabel12 As Care.Controls.CareLabel
    Private WithEvents txtCalcAnnum As Care.Controls.CareTextBox
    Private WithEvents txtCalcMonth As Care.Controls.CareTextBox
    Private WithEvents txtCalcDay As Care.Controls.CareTextBox
    Private WithEvents CareLabel9 As Care.Controls.CareLabel
    Private WithEvents CareLabel8 As Care.Controls.CareLabel
    Private WithEvents txtRate As Care.Controls.CareTextBox
    Private WithEvents cbxPer As Care.Controls.CareComboBox
    Private WithEvents CareLabel6 As Care.Controls.CareLabel
    Private WithEvents CareLabel5 As Care.Controls.CareLabel
    Private WithEvents txtUnits As Care.Controls.CareTextBox
    Private WithEvents btnClear As Care.Controls.CareButton

End Class
