﻿Imports System.Drawing
Imports System.IO

Public Class SignatureHandler

    Public Shared Function WriteToBMP(ByVal BMPPath As String, ByVal InkSignatureXMLString As String, ByVal BMPWidth As Integer, BMPHeight As Integer) As Boolean

        Dim _bmp As New Bitmap(BMPWidth, BMPHeight, Imaging.PixelFormat.Format32bppPArgb)
        _bmp.SetResolution(300, 300)

        Dim _g As Graphics = Graphics.FromImage(_bmp)

        Dim _Ink As New InkSecureSignature
        _Ink.Width = BMPWidth
        _Ink.Height = BMPHeight
        _Ink.Value = InkSignatureXMLString

        _Ink.Print(_g, New Point(1, 1))

        _bmp.Save(BMPPath, Imaging.ImageFormat.Png)

        Return True

    End Function

    Public Shared Function WriteToBytes(ByRef Bytes As Byte(), ByVal InkSignatureXMLString As String, ByVal BMPWidth As Integer, BMPHeight As Integer) As Boolean

        Try

            Dim _bmp As New Bitmap(BMPWidth, BMPHeight, Imaging.PixelFormat.Format32bppPArgb)
            _bmp.SetResolution(300, 300)

            Dim _g As Graphics = Graphics.FromImage(_bmp)

            Dim _Ink As New InkSecureSignature
            _Ink.Width = BMPWidth
            _Ink.Height = BMPHeight
            _Ink.Value = InkSignatureXMLString

            _Ink.Print(_g, New Point(1, 1))

            Dim _C As New ImageConverter
            Bytes = CType(_C.ConvertTo(_bmp, GetType(Byte())), Byte())

            _Ink.Dispose()
            _Ink = Nothing

            _g.Dispose()
            _g = Nothing

            _bmp.Dispose()
            _bmp = Nothing

            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function
End Class
