﻿'-------------------------------------------------------------------------- 
' 
'  Copyright (c) Microsoft Corporation.  All rights reserved. 
' 
'  File: InkSecureSignature.vb
'
'  Description: User control for safely collecting an ink signature.
'
'-------------------------------------------------------------------------- 

Imports Microsoft.Ink
Imports System.Drawing
Imports System.Drawing.Drawing2D

''' <summary>
''' User control for safely collecting an ink signature.
''' </summary>
Public Class InkSecureSignature

    Friend WithEvents signaturePanel As Panel



    ' InkOverlay to collect Ink.
    Private WithEvents signatureInkOverlay As InkOverlay
    ' Storage of all Ink signature data.
    Private InkSecureSignatureDataObject As New InkSecureSignatureData
    ' Indicates if this control is in design mode or runtime mode.
    Private inDesignMode As Boolean = True
    ' Locks the signature.
    Private signatureReadOnly As Boolean
    ' The caption for the signer's name.
    Private signersNameCaptionValue As String = "Signer's Name: "

    ''' <summary>
    ''' Occurs when the signature has been fully signed.
    ''' </summary>
    ''' <param name="sender">The source InkSecureSignature object for this event.</param>
    ''' <param name="e">The EventArgs object that contains the event data.</param>
    Public Event Signed(ByVal sender As Object, ByVal e As EventArgs)

    ''' <summary>
    ''' Gets or sets an XML document string containing the entire state of the control.
    ''' </summary>
    Public Property Value() As String

        ' Serialize this object and return it as a string.
        Get
            Dim Serializer As New Xml.Serialization.XmlSerializer(GetType(InkSecureSignatureData))
            ' Create an in-memory stream to hold the serialization.
            Dim Stream As New IO.MemoryStream
            ' Create a StringBuilder object to hold the serialized string.
            Dim SB As New System.Text.StringBuilder
            ' Serialize the object.
            Serializer.Serialize(Stream, InkSecureSignatureDataObject)
            ' Reset the position of the stream back to the start.
            Stream.Position = 0
            ' Create a reader to read the stream.
            Dim Reader As New System.IO.StreamReader(Stream)
            ' Read the stream into the StringBuilder object.
            SB.Append(Reader.ReadToEnd)
            ' Clean up.
            Stream.Close()
            ' Return the serialized string.
            Return SB.ToString
        End Get

        ' Deserialize this object and restore its data.
        Set(ByVal Value As String)
            Try
                ' Create an in-memory stream.
                Dim Stream As New IO.MemoryStream
                ' Create a new writer for the in-memory stream.
                Dim Writer As New IO.StreamWriter(Stream)
                ' Write the passed-in value to the stream.
                Writer.Write(Value)
                ' Ensure that all data has been written.
                Writer.Flush()
                ' Go to the start of the stream.
                Stream.Position = 0

                ' Create a serialization object and deserialize the stream.
                Dim Serializer As New Xml.Serialization.XmlSerializer(GetType(InkSecureSignatureData))
                InkSecureSignatureDataObject = CType(Serializer.Deserialize(Stream), InkSecureSignatureData)

                ' Check to see if we are loading a signed/accepted signature, or an unaccepted signature.
                If Not InkSecureSignatureDataObject.EncryptedBiometricData Is Nothing AndAlso _
                        InkSecureSignatureDataObject.EncryptedBiometricData.Length > 0 Then

                    ' Display the signature.
                    Call CreateNewInkOverlay()
                    Call SetDefaultDrawingAttributes(Color.Black)
                    signatureInkOverlay.Ink.Load(InkSecureSignatureDataObject.InkWashedSignature)
                    signaturePanel.Refresh()

                Else
                    ' This signature has NOT been accepted, so display the control in edit mode.
                    signatureReadOnly = False

                    ' Create an Ink region to capture a signature.
                    Call CreateNewInkOverlay()
                    Call SetDefaultDrawingAttributes(Color.Blue)

                    ' Do not collect Ink in design mode, but enable it at runtime.
                    signatureInkOverlay.Enabled = Not inDesignMode

                    ' If there is signature Ink, then load it up.
                    ' The control does not store Ink in the InkSecureSignature field
                    ' until the user clicks Accept, so there will probably
                    ' never be Ink in an unaccepted signature.

                    If Not InkSecureSignatureDataObject.InkSecureSignature Is Nothing Then
                        signatureInkOverlay.Ink.Load(InkSecureSignatureDataObject.InkSecureSignature)
                        ' If there is at least one Stroke, then enable buttons.
                    End If

                    ' Refresh the signaturePanel and set signer fields.
                    signaturePanel.Refresh()

                End If

            Catch ex As Exception
                Throw ex
            End Try

        End Set

    End Property

    '
    ' SignaturePanel_Paint
    '
    ' Occurs when the control is redrawn.
    '
    ' Parameters:
    '  sender - The source Panel object for this event.
    '  e - The PaintEventArgs object that contains the event data.
    '
    Private Sub SignaturePanel_Paint(ByVal sender As System.Object, ByVal e As PaintEventArgs) Handles signaturePanel.Paint

        ' Draw the lines and the label.
        Dim DashedLinePen As New Pen(New Drawing2D.HatchBrush(HatchStyle.DashedVertical, SystemColors.Control, SystemColors.ControlDark))
        Dim BottomLineY As Integer = signaturePanel.Height - 20
        Dim TopLineY As Integer = 10

        e.Graphics.DrawLine(DashedLinePen, 0, BottomLineY, signaturePanel.Width, BottomLineY)
        e.Graphics.DrawLine(DashedLinePen, 0, TopLineY, signaturePanel.Width, TopLineY)
        e.Graphics.DrawString("Signature", MyBase.Font, Brushes.Black, 3, BottomLineY + 4)

    End Sub

    '
    ' InkSecureSignature_Load
    '
    ' Occurs before the control becomes visible for the first time.
    '
    ' Parameters:
    '  sender - The source InkSecureSignature object for this event.
    '  e - The EventArgs object that contains the event data.
    '
    '
    ' SetDefaultDrawingAttributes
    '
    ' Set the default drawing attributes for ink collection.
    '
    ' Parameters:
    '  color - The desired ink color.
    '
    Private Sub SetDefaultDrawingAttributes(ByVal color As System.Drawing.Color)

        With signatureInkOverlay.DefaultDrawingAttributes
            .Color = color          ' Color.
            .AntiAliased = True     ' Smooth.
            .FitToCurve = False     ' Set to not round (modify) the Stroke.
            .PenTip = PenTip.Ball   ' Ball Point.
            .Width = 25             ' Size.
        End With

    End Sub

    '
    ' clearButton_Click
    '
    ' Occurs when the Button is clicked to clear the signature in process.
    '
    ' Parameters:
    '  sender - The source Button object for this event.
    '  e - The EventArgs object that contains the event data.
    ' 
    Public Sub Clear()
        ' Delete the Strokes collection.
        signatureInkOverlay.Ink.DeleteStrokes(signatureInkOverlay.Ink.Strokes)
        ' Invalidating the panel forces a redraw.
        signaturePanel.Invalidate()
    End Sub

    '
    ' CreateNewInkOverlay
    '
    ' Creates or resets the signature InkOverlay object.
    '
    Private Sub CreateNewInkOverlay()

        If Not signatureInkOverlay Is Nothing Then
            signatureInkOverlay.Enabled = False
            signatureInkOverlay.Ink.DeleteStrokes(signatureInkOverlay.Ink.Strokes)
            signatureInkOverlay.AttachedControl = Nothing
            signatureInkOverlay.Dispose()
            signatureInkOverlay = Nothing
        End If

        signatureInkOverlay = New InkOverlay
        signatureInkOverlay.AttachedControl = signaturePanel
        signatureInkOverlay.DefaultDrawingAttributes.AntiAliased = True
        signatureInkOverlay.DefaultDrawingAttributes.FitToCurve = True

    End Sub

    ''' <summary>
    ''' Prints the signature.
    ''' </summary>
    ''' <param name="graphics">The Graphics context to print to.</param>
    ''' <param name="topLeftPoint">The top left corner of the print area.</param>
    Public Sub Print(ByVal graphics As Graphics, ByVal topLeftPoint As Point)

        ' Starting locations.
        Dim Indentation As Integer = 5
        Dim BottomLineY As Integer = 17
        Dim VerticalLocation As Integer = topLeftPoint.Y

        ' Specify a bordered print area slightly smaller than the control.
        Dim ThisRect As New Rectangle(0, 0, Me.Width, Me.Height)
        Dim BorderColor As Color = Color.FromArgb(255, 0, 45, 150)
        Dim Renderer As New Microsoft.Ink.Renderer

        With graphics
            .FillRectangle(Brushes.White, ThisRect)
            '.DrawRectangle(New Pen(BorderColor), ThisRect)

            '' Draw the bottom line.
            '.DrawLine(Pens.Black, _
            '    Indentation, _
            '    ThisRect.Height - BottomLineY, _
            '    ThisRect.Width - (2 * Indentation), _
            '    ThisRect.Height - BottomLineY)

            '' Draw header text and washed Ink.
            '.DrawString("RSA Encrypted Digital Biometric Signature", _
            '    Me.Font, _
            '    New SolidBrush(Color.Blue), _
            '    ThisRect.Left + 3, _
            '    VerticalLocation + 3)

            graphics.SmoothingMode = SmoothingMode.AntiAlias
            graphics.CompositingMode = CompositingMode.SourceOver
            graphics.CompositingQuality = CompositingQuality.HighQuality
            graphics.InterpolationMode = InterpolationMode.HighQualityBilinear

            Dim da As New DrawingAttributes(Color.Black)
            da.AntiAliased = True
            da.FitToCurve = True
            da.RasterOperation = RasterOperation.Black
            For Each Stroke As Stroke In signatureInkOverlay.Ink.Strokes
                Renderer.Draw(graphics, Stroke, da)
            Next

            '.DrawString("Signed By: " & _
            '    InkSecureSignatureDataObject.SignersName & " on " & _
            '    InkSecureSignatureDataObject.SignerAcceptedOn.ToShortDateString & " at " & _
            '    InkSecureSignatureDataObject.SignerAcceptedOn.ToShortTimeString, _
            '    Me.Font, _
            '    New SolidBrush(Color.Blue), _
            '    ThisRect.Left + Indentation, _
            '    ThisRect.Height - BottomLineY + 1)
        End With

    End Sub

    ''' <summary>
    ''' Clears signature data to reset the control.
    ''' </summary>
    Public Sub Reset()

        Me.Value = "<InkSecureSignatureData/>"

    End Sub

    '
    ' acceptButton_Click
    '
    ' Occurs when the Button is clicked to lock the signature.
    '
    ' Parameters:
    '  sender - The source Button object for this event.
    '  e - The EventArgs object that contains the event data.
    ' 


    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.signaturePanel = New Panel()
        Me.signaturePanel.SuspendLayout()
    End Sub
End Class
