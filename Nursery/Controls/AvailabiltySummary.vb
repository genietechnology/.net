﻿Public Class AvailabilitySummary
    Public Property BookingDate As Date
    Public Property AM As Integer
    Public Property PM As Integer
    Public Property Capacity As Integer
    Public Property VacanciesAM As Integer
    Public Property VacanciesPM As Integer
End Class