﻿Imports DevExpress.XtraScheduler
Imports System.Drawing
Imports DevExpress.XtraPrinting
Imports System.Windows.Forms
Imports DevExpress.XtraEditors.Calendar
Imports System.ComponentModel
Imports DevExpress.Utils

<DefaultEvent("DoubleClick")>
Public Class YearView

    Public Class YearViewDoubleClickArgs
        Inherits EventArgs
        Property HitDate As Date?
    End Class

    Public Shadows Event DoubleClick(sender As Object, e As YearViewDoubleClickArgs)

    Public Enum EnumMode
        Availability
        OccupancyDensity
        Bookings
        Absence
        Equipment
        MedicineIncidents
    End Enum

    Private m_IsRuntime As Boolean = False
    Private m_IsPopulated As Boolean = False

    Private m_Tariffs As List(Of Business.Tariff)
    Private m_AvailabiltySummaryData As List(Of AvailabilitySummary) = Nothing
    Private m_BookingData As List(Of Business.Booking) = Nothing
    Private m_AbsenceData As List(Of Business.StaffAbsence) = Nothing
    Private m_EquipmentData As New List(Of EquipmentData)
    Private m_MedIncData As New List(Of Business.MedicineIncidents)

    Private m_DateFrom As Date = Nothing
    Private m_Mode As EnumMode = EnumMode.Availability

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        gbxLegend.Hide()
        gbxDensity.Hide()

    End Sub

#Region "Properties"

    <Category("_Appearance")>
    Public Property CalendarFont As Font
        Get
            Return MyDateNav.Font
        End Get
        Set(value As Font)
            MyDateNav.Font = value
        End Set
    End Property

#End Region

    Public Sub Clear()
        MyStorage.Appointments.Clear()
        MyDateNav.Refresh()
    End Sub

    Private Sub DisplayLegend()

        gbxDensity.Hide()
        gbxLegend.Hide()

        Select Case m_Mode

            Case EnumMode.Bookings
                'we do not want to display the legend

            Case EnumMode.MedicineIncidents
                'no legend (chops calendar to display 4, frmChildren is not big enough)

            Case EnumMode.Absence

                lblRed.Text = "Sickness"
                lblYellow.Text = "Holidays"
                lblGreen.Text = "Maternity"
                lblBlue.Text = "Other"

                gbxLegend.Show()

            Case EnumMode.Equipment

                lblRed.Text = "Risk Assessment"
                lblYellow.Text = "Test"
                lblBlue.Text = "Service"
                lblGreen.Text = "Warranty"

                gbxLegend.Show()

            Case EnumMode.Availability
                gbxLegend.Show()

            Case EnumMode.OccupancyDensity
                gbxDensity.Show()

        End Select

    End Sub

    Public Sub Populate(ByRef Equipment As Business.Equipment)

        m_Mode = EnumMode.Equipment

        m_DateFrom = DateSerial(Today.Year, Today.Month, 1)
        BuildEquipmentData(Equipment)

        If m_EquipmentData Is Nothing Then Exit Sub

        MyDateNav.SchedulerControl.Start = m_DateFrom

        MyStorage.Appointments.Mappings.End = "TheDate"
        MyStorage.Appointments.Mappings.Start = "TheDate"

        MyStorage.Appointments.DataSource = m_BookingData
        MyDateNav.Refresh()

        m_IsPopulated = True

        DisplayLegend()

    End Sub

    Public Sub Populate(ByRef MedIncData As List(Of Business.MedicineIncidents), ByVal DateFrom As Date)

        m_Mode = EnumMode.MedicineIncidents
        m_MedIncData = MedIncData
        m_DateFrom = DateFrom

        If m_MedIncData Is Nothing Then Exit Sub

        MyDateNav.SchedulerControl.Start = m_DateFrom

        MyStorage.Appointments.Mappings.Start = "ActionDate"
        MyStorage.Appointments.Mappings.End = "ActionDate"

        MyStorage.Appointments.DataSource = m_MedIncData
        MyDateNav.Refresh()

        m_IsPopulated = True

        DisplayLegend()

    End Sub

    Public Sub Populate(ByRef BookingData As List(Of Business.Booking), ByVal DateFrom As Date)

        m_Mode = EnumMode.Bookings
        m_BookingData = BookingData
        m_DateFrom = DateFrom

        If m_BookingData Is Nothing Then Exit Sub

        m_Tariffs = Business.Tariff.RetreiveAll

        MyDateNav.SchedulerControl.Start = m_DateFrom

        MyStorage.Appointments.Mappings.End = "_BookingDate"
        MyStorage.Appointments.Mappings.Start = "_BookingDate"

        MyStorage.Appointments.DataSource = m_BookingData
        MyDateNav.Refresh()

        m_IsPopulated = True

        DisplayLegend()

    End Sub

    Public Sub Populate(ByRef AbsenceData As List(Of Business.StaffAbsence), ByVal DateFrom As Date)

        m_Mode = EnumMode.Absence
        m_AbsenceData = AbsenceData
        m_DateFrom = DateFrom

        If m_AbsenceData Is Nothing Then Exit Sub

        MyDateNav.SchedulerControl.Start = m_DateFrom

        MyStorage.Appointments.Mappings.End = "_AbsTo"
        MyStorage.Appointments.Mappings.Start = "_AbsFrom"

        MyStorage.Appointments.DataSource = m_AbsenceData
        MyDateNav.Refresh()

        m_IsPopulated = True

        DisplayLegend()

    End Sub

    Public Sub Populate(ByRef AvailabilitySummaryData As List(Of AvailabilitySummary), ByVal DateFrom As Date, ByVal DensityMode As Boolean)

        If DensityMode Then
            m_Mode = EnumMode.OccupancyDensity
        Else
            m_Mode = EnumMode.Availability
        End If

        m_AvailabiltySummaryData = AvailabilitySummaryData
        m_DateFrom = DateFrom

        If m_AvailabiltySummaryData Is Nothing Then Exit Sub

        MyDateNav.SchedulerControl.Start = m_DateFrom

        MyStorage.Appointments.Mappings.End = "BookingDate"
        MyStorage.Appointments.Mappings.Start = "BookingDate"

        MyStorage.Appointments.DataSource = m_AvailabiltySummaryData
        MyDateNav.Refresh()

        m_IsPopulated = True

        DisplayLegend()

    End Sub

    Public Sub Print()
        Dim link As New PrintableComponentLink(New PrintingSystem(CType(MyDateNav, IContainer)))
        link.Landscape = True
        link.PrintingSystem.Document.AutoFitToPagesWidth = 1
        link.ShowPreviewDialog()
    End Sub

    Public Sub ExportToPDF(ByVal filePath As String, ByVal middleText As String)

        Me.MyScheduler.ActiveViewType = SchedulerViewType.Month
        Me.MyScheduler.Views.MonthView.AppointmentDisplayOptions.TimeDisplayType = AppointmentTimeDisplayType.Text
        Me.MyScheduler.Views.MonthView.AppointmentDisplayOptions.StartTimeVisibility = AppointmentTimeVisibility.Never
        Me.MyScheduler.Views.MonthView.AppointmentDisplayOptions.EndTimeVisibility = AppointmentTimeVisibility.Never

        SetPrintStyle()
        Dim pcl As New PrintableComponentLink(New PrintingSystem())
        Dim phf As PageHeaderFooter = CType(pcl.PageHeaderFooter, PageHeaderFooter)

        pcl.Landscape = True
        pcl.PrintingSystem.Document.AutoFitToPagesWidth = 1
        pcl.Component = Me.MyScheduler

        phf.Header.Content.Clear()
        phf.Header.Content.AddRange(New String() {"", middleText, ""})
        phf.Header.LineAlignment = BrickAlignment.Far

        pcl.CreateDocument()
        pcl.ExportToPdf(filePath)

    End Sub

    Private Sub SetPrintStyle()
        Dim pStyle As Printing.MonthlyPrintStyle = TryCast(Me.MyScheduler.ActivePrintStyle, Printing.MonthlyPrintStyle)

        pStyle.StartRangeDate = Me.m_DateFrom
        pStyle.EndRangeDate = m_DateFrom.AddMonths(6).Date

        ' Specify resources to print.
        pStyle.ResourceOptions.PrintCustomCollection = True

    End Sub

    Private Sub MyDateNav_CustomDrawDayNumberCell(sender As Object, e As DevExpress.XtraEditors.Calendar.CustomDrawDayNumberCellEventArgs) Handles MyDateNav.CustomDrawDayNumberCell

        If Me.DesignMode Then Exit Sub
        If Not m_IsPopulated Then Exit Sub

        Try

            Select Case m_Mode

                Case EnumMode.OccupancyDensity

                    Dim _Availability As AvailabilitySummary = ReturnAvailablilty(e.Date)
                    If _Availability IsNot Nothing Then

                        Dim _HeadCount As Integer = _Availability.AM
                        If _Availability.PM > _Availability.AM Then _HeadCount = _Availability.PM

                        Dim _Occupancy As Decimal = 0

                        If _Availability.Capacity > 0 Then
                            _Occupancy = CDec(_HeadCount / _Availability.Capacity * 100)
                        End If

                        Dim _20 As New SolidBrush(txt20.BackColor)
                        Dim _40 As New SolidBrush(txt40.BackColor)
                        Dim _60 As New SolidBrush(txt60.BackColor)
                        Dim _80 As New SolidBrush(txt80.BackColor)
                        Dim _100 As New SolidBrush(txt100.BackColor)

                        If _Occupancy > 80 Then
                            e.Graphics.FillRectangle(_100, e.Bounds)
                        Else
                            If _Occupancy > 60 Then
                                e.Graphics.FillRectangle(_80, e.Bounds)
                            Else
                                If _Occupancy > 40 Then
                                    e.Graphics.FillRectangle(_60, e.Bounds)
                                Else
                                    If _Occupancy > 20 Then
                                        e.Graphics.FillRectangle(_40, e.Bounds)
                                    Else
                                        e.Graphics.FillRectangle(_20, e.Bounds)
                                    End If
                                End If
                            End If
                        End If

                    End If

                Case EnumMode.Availability

                    Dim _Availability As AvailabilitySummary = ReturnAvailablilty(e.Date)
                    If _Availability IsNot Nothing Then

                        Dim _Full As New SolidBrush(txtRed.BackColor)
                        Dim _OK As New SolidBrush(txtGreen.BackColor)
                        Dim _AMOnly As New SolidBrush(txtBlue.BackColor)
                        Dim _PMOnly As New SolidBrush(txtYellow.BackColor)

                        If _Availability.VacanciesAM <= 0 AndAlso _Availability.VacanciesPM <= 0 Then
                            e.Graphics.FillRectangle(_Full, e.Bounds)
                        Else
                            If _Availability.VacanciesAM > 0 AndAlso _Availability.VacanciesPM > 0 Then
                                e.Graphics.FillRectangle(_OK, e.Bounds)
                            Else
                                If _Availability.VacanciesAM > 0 Then
                                    e.Graphics.FillRectangle(_AMOnly, e.Bounds)
                                Else
                                    e.Graphics.FillRectangle(_PMOnly, e.Bounds)
                                End If
                            End If
                        End If

                    End If

                Case EnumMode.Bookings

                    Dim _Booking As Business.Booking = ReturnBooking(e.Date)
                    If _Booking IsNot Nothing Then

                        If _Booking._SessionCount > 1 Then
                            e.Style.ForeColor = Color.Blue
                            e.Graphics.FillRectangle(Brushes.Gray, e.Bounds)
                        Else
                            If _Booking._BookingStatus = "Holiday" Then
                                e.Graphics.FillRectangle(Brushes.Silver, e.Bounds)
                            Else
                                Dim _Tariff As Business.Tariff = ReturnTariff(_Booking._TariffId)
                                If _Tariff IsNot Nothing Then
                                    e.Graphics.FillRectangle(New SolidBrush(Color.FromName(_Tariff._Colour)), e.Bounds)
                                Else
                                    e.Graphics.FillRectangle(Brushes.White, e.Bounds)
                                End If
                            End If
                        End If
                    End If

                Case EnumMode.Absence

                    Dim Absence As Business.StaffAbsence = ReturnAbsence(e.Date)
                    If Absence IsNot Nothing Then

                        Select Case Absence._AbsType

                            Case "Sickness"
                                e.Graphics.FillRectangle(New SolidBrush(txtRed.BackColor), e.Bounds)

                            Case "Annual Leave"
                                e.Graphics.FillRectangle(New SolidBrush(txtYellow.BackColor), e.Bounds)

                            Case "Maternity Leave"
                                e.Graphics.FillRectangle(New SolidBrush(txtGreen.BackColor), e.Bounds)

                            Case Else
                                e.Graphics.FillRectangle(New SolidBrush(txtBlue.BackColor), e.Bounds)

                        End Select

                    End If

                Case EnumMode.Equipment

                    Dim _E As EquipmentData = ReturnEquipment(e.Date)
                    If _E IsNot Nothing Then

                        Select Case _E.Indicator

                            Case EquipmentData.EnumIndicator.Warranty
                                e.Graphics.FillRectangle(New SolidBrush(txtGreen.BackColor), e.Bounds)

                            Case EquipmentData.EnumIndicator.Service
                                e.Graphics.FillRectangle(New SolidBrush(txtBlue.BackColor), e.Bounds)

                            Case EquipmentData.EnumIndicator.Test
                                e.Graphics.FillRectangle(New SolidBrush(txtYellow.BackColor), e.Bounds)

                            Case EquipmentData.EnumIndicator.RiskAssesment
                                e.Graphics.FillRectangle(New SolidBrush(txtRed.BackColor), e.Bounds)

                        End Select

                    End If

                Case EnumMode.MedicineIncidents

                    Dim _MI As Business.MedicineIncidents = ReturnMedicineIncident(e.Date)
                    If _MI IsNot Nothing Then

                        Select Case _MI.ItemType

                            Case "Prior Authorised Medication"
                                e.Graphics.FillRectangle(New SolidBrush(txtGreen.BackColor), e.Bounds)

                            Case "Ad-Hoc Medication"
                                e.Graphics.FillRectangle(New SolidBrush(txtBlue.BackColor), e.Bounds)

                            Case "Accident"
                                e.Graphics.FillRectangle(New SolidBrush(txtYellow.BackColor), e.Bounds)

                            Case "Prior Injury"
                                e.Graphics.FillRectangle(New SolidBrush(txtOrange.BackColor), e.Bounds)

                            Case "Absence"
                                e.Graphics.FillRectangle(New SolidBrush(txtRed.BackColor), e.Bounds)

                        End Select

                    End If

            End Select

        Catch ex As Exception

        End Try

    End Sub

    Private Function ReturnEquipment(ByVal DateIn As Date) As EquipmentData

        Dim _Q As IEnumerable(Of EquipmentData) = From _E As EquipmentData In m_EquipmentData Where _E.TheDate = DateIn
        If _Q IsNot Nothing Then
            If _Q.Count = 0 Then
                Return Nothing
            Else
                Return _Q.First
            End If
        End If

        Return Nothing

    End Function

    Private Function ReturnMedicineIncident(ByVal DateIn As Date) As Business.MedicineIncidents

        Dim _Q As IEnumerable(Of Business.MedicineIncidents) = From _MI As Business.MedicineIncidents In m_MedIncData Where _MI.ActionDate = DateIn
        If _Q IsNot Nothing Then
            If _Q.Count = 0 Then
                Return Nothing
            Else
                Return _Q.First
            End If
        End If

        Return Nothing

    End Function

    Private Function ReturnAvailablilty(ByVal DateIn As Date) As AvailabilitySummary

        Dim _Q As IEnumerable(Of AvailabilitySummary) = From _AS As AvailabilitySummary In m_AvailabiltySummaryData Where _AS.BookingDate = DateIn
        If _Q IsNot Nothing Then
            If _Q.Count = 0 Then
                Return Nothing
            Else
                Return _Q.First
            End If
        End If

        Return Nothing

    End Function

    Private Function ReturnTariff(ByVal TariffID As Guid?) As Business.Tariff

        Dim _Q As IEnumerable(Of Business.Tariff) = From _T As Business.Tariff In m_Tariffs Where _T._ID = TariffID
        If _Q IsNot Nothing Then
            If _Q.Count = 1 Then
                Return _Q.First
            Else
                Return Nothing
            End If
        End If

        Return Nothing

    End Function

    Private Function ReturnAbsence(ByVal DateIn As Date) As Business.StaffAbsence

        Dim _Q As IEnumerable(Of Business.StaffAbsence) = From _A As Business.StaffAbsence In m_AbsenceData Where DateIn >= _A._AbsFrom AndAlso DateIn <= _A._AbsTo
        If _Q IsNot Nothing Then
            If _Q.Count = 1 Then
                Return _Q.First
            Else
                Return Nothing
            End If
        End If

        Return Nothing

    End Function

    Private Function ReturnBooking(ByVal DateIn As Date) As Business.Booking

        Dim _Q As IEnumerable(Of Business.Booking) = From _B As Business.Booking In m_BookingData Where _B._BookingDate = DateIn
        If _Q IsNot Nothing Then
            If _Q.Count = 1 Then
                Return _Q.First
            Else
                Return Nothing
            End If
        End If

        Return Nothing

    End Function

    Private Sub MyDateNav_DoubleClick(sender As Object, e As EventArgs) Handles MyDateNav.DoubleClick

        Dim _a As MouseEventArgs = CType(e, MouseEventArgs)
        Dim _h As CalendarHitInfo = MyDateNav.GetHitInfo(_a)

        If _h.HitTest = CalendarHitInfoType.MonthNumber Then

            Dim _e As New YearViewDoubleClickArgs
            _e.HitDate = _h.HitDate

            RaiseEvent DoubleClick(sender, _e)

        End If

    End Sub

    Private Sub MyDateNav_MouseMove(sender As Object, e As MouseEventArgs) Handles MyDateNav.MouseMove

        If m_Mode = EnumMode.Availability Then Exit Sub

        Dim _chi As CalendarHitInfo = MyDateNav.GetHitInfo(e)
        If _chi.HitObject IsNot Nothing Then

            Dim _stt As SuperToolTip = GetToolTipInfo(_chi)
            If _stt IsNot Nothing Then

                Dim _ttci As New ToolTipControlInfo()

                _ttci.ToolTipType = ToolTipType.SuperTip
                _ttci.IconType = ToolTipIconType.Information
                _ttci.SuperTip = _stt
                _ttci.ToolTipPosition = MousePosition
                _ttci.Interval = 500
                _ttci.Object = _chi.HitDate

                ToolTipController1.ShowHint(_ttci)

            End If

        End If

    End Sub

    Private Function GetToolTipInfo(ByVal chi As CalendarHitInfo) As SuperToolTip

        Select Case m_Mode

            Case EnumMode.OccupancyDensity

                Dim _A As AvailabilitySummary = ReturnAvailablilty(chi.HitDate)
                If _A IsNot Nothing Then

                    Dim _stt As New SuperToolTip()

                    Dim _DateTitle As New ToolTipTitleItem()
                    _DateTitle.Text = Format(chi.HitDate, "dddd d MMMM yyyy")

                    Dim _TimeTitle As New ToolTipTitleItem()
                    Dim _HeadCount As Integer = _A.AM
                    If _A.PM > _A.AM Then _HeadCount = _A.PM

                    _stt.Items.Add(_DateTitle)

                    If _A.Capacity > 0 Then

                        Dim _Occ As Double = _HeadCount / _A.Capacity * 100
                        _TimeTitle.Text = "Occupancy: " + Format(_Occ, "0.00").ToString + "%"

                        Dim _i1 As New ToolTipItem()
                        '_i1.Text = _A._TariffName

                        _stt.Items.Add(_TimeTitle)
                        _stt.Items.Add(_i1)

                    End If

                    Return _stt

                End If

            Case EnumMode.Bookings

                Dim _B As Business.Booking = ReturnBooking(chi.HitDate)
                If _B IsNot Nothing Then

                    Dim _stt As New SuperToolTip()

                    Dim _DateTitle As New ToolTipTitleItem()
                    _DateTitle.Text = Format(chi.HitDate, "dddd d MMMM yyyy")

                    Dim _TimeTitle As New ToolTipTitleItem()
                    _TimeTitle.Text = _B._BookingFrom.ToString + " - " + _B._BookingTo.ToString

                    Dim _i1 As New ToolTipItem()

                    If _B._BookingStatus = "Holiday" Then
                        _i1.Text = "Holiday"
                    Else
                        _i1.Text = _B._TariffName
                    End If

                    _stt.Items.Add(_DateTitle)
                    _stt.Items.Add(_TimeTitle)
                    _stt.Items.Add(_i1)

                    Return _stt

                End If

            Case EnumMode.Absence

                Dim _A As Business.StaffAbsence = ReturnAbsence(chi.HitDate)
                If _A IsNot Nothing Then

                    Dim _stt As New SuperToolTip()

                    Dim _DateTitle As New ToolTipTitleItem()
                    _DateTitle.Text = _A._AbsType

                    Dim _TimeTitle As New ToolTipTitleItem()
                    _TimeTitle.Text = Format(_A._AbsFrom, "dd/MM/yyyy") + " - " + Format(_A._AbsTo, "dd/MM/yyyy")

                    _stt.Items.Add(_DateTitle)
                    _stt.Items.Add(_TimeTitle)

                    Return _stt

                End If

            Case EnumMode.MedicineIncidents

                Dim _A As Business.MedicineIncidents = ReturnMedicineIncident(chi.HitDate)
                If _A IsNot Nothing Then

                    Dim _stt As New SuperToolTip()

                    Dim _DateTitle As New ToolTipTitleItem()
                    _DateTitle.Text = _A.ItemType

                    Dim _TimeTitle As New ToolTipItem()
                    _TimeTitle.Text = _A.Tooltip

                    _stt.Items.Add(_DateTitle)
                    _stt.Items.Add(_TimeTitle)

                    Return _stt

                End If

            Case Else
                Return Nothing

        End Select

        Return Nothing

    End Function

    Private Sub MyDateNav_MouseWheel(sender As Object, e As MouseEventArgs) Handles MyDateNav.MouseWheel

        Me.Cursor = Cursors.WaitCursor
        MyDateNav.BeginUpdate()

        Dim _CurrentDate As Date = MyDateNav.GetStartDate

        If e.Delta > 0 Then
            'scroll up
            MyDateNav.DateTime = _CurrentDate.AddMonths(-1)
        Else
            'scroll down
            MyDateNav.DateTime = MyDateNav.GetEndDate.AddMonths(1)
            MyDateNav.DateTime = _CurrentDate.AddMonths(1)
        End If

        Me.Cursor = Cursors.Default
        MyDateNav.EndUpdate()

    End Sub

    Private Sub BuildEquipmentData(ByRef EquipmentRecord As Business.Equipment)

        m_EquipmentData.Clear()

        'put in the warranty first
        If EquipmentRecord._Warranty Then
            If EquipmentRecord._WarrantyFrom.HasValue AndAlso EquipmentRecord._WarrantyExpires.HasValue Then
                Dim _d As Date = EquipmentRecord._WarrantyFrom.Value
                While _d < EquipmentRecord._WarrantyExpires.Value
                    m_EquipmentData.Add(New EquipmentData(_d, EquipmentData.EnumIndicator.Warranty))
                    _d = _d.AddDays(1)
                End While
            End If
        End If

        'any other date will superseed the warranty...

        If EquipmentRecord._Serv AndAlso EquipmentRecord._ServLast.HasValue Then
            AddAnotherDate(EquipmentRecord._ServLast.Value, EquipmentData.EnumIndicator.Service)
        End If

        If EquipmentRecord._Serv AndAlso EquipmentRecord._ServNext.HasValue Then
            AddAnotherDate(EquipmentRecord._ServNext.Value, EquipmentData.EnumIndicator.Service)
        End If

        If EquipmentRecord._Risk AndAlso EquipmentRecord._RiskLast.HasValue Then
            AddAnotherDate(EquipmentRecord._RiskLast.Value, EquipmentData.EnumIndicator.RiskAssesment)
        End If

        If EquipmentRecord._Risk AndAlso EquipmentRecord._RiskNext.HasValue Then
            AddAnotherDate(EquipmentRecord._RiskNext.Value, EquipmentData.EnumIndicator.RiskAssesment)
        End If

        If EquipmentRecord._Test AndAlso EquipmentRecord._TestLast.HasValue Then
            AddAnotherDate(EquipmentRecord._TestLast.Value, EquipmentData.EnumIndicator.Test)
        End If

        If EquipmentRecord._Test AndAlso EquipmentRecord._TestNext.HasValue Then
            AddAnotherDate(EquipmentRecord._TestNext.Value, EquipmentData.EnumIndicator.Test)
        End If

    End Sub

    Private Sub AddAnotherDate(ByVal DateIn As Date, ByVal Indicator As EquipmentData.EnumIndicator)
        Dim _D As EquipmentData = ReturnEquipment(DateIn)
        If _D IsNot Nothing Then m_EquipmentData.Remove(_D)
        m_EquipmentData.Add(New EquipmentData(DateIn, Indicator))
    End Sub

    Private Sub MyDateNav_MouseLeave(sender As Object, e As EventArgs) Handles MyDateNav.MouseLeave
        ToolTipController1.HideHint()
    End Sub

    Private Sub MyScheduler_InitAppointmentDisplayText(sender As Object, e As AppointmentDisplayTextEventArgs) Handles MyScheduler.InitAppointmentDisplayText
        e.Text = "Available"
    End Sub

    Private Class EquipmentData

        Public Enum EnumIndicator
            Warranty
            Service
            RiskAssesment
            Test
        End Enum

        Public Sub New(ByVal DateIn As Date, ByVal Indicator As EnumIndicator)
            Me.TheDate = DateIn
            Me.Indicator = Indicator
        End Sub

        Property TheDate As Date
        Property Indicator As EnumIndicator

    End Class

End Class
