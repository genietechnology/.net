﻿

Imports Care.Global
Imports Care.Shared
Imports Care.Data

Namespace Reports

    Public Class InvoiceRun

#Region "Enums"

        Public Enum EnumOutputMode
            PrintedOnly
            EmailOnly
            ForcePrint
            ForceEmail
        End Enum

        Private Enum EnumSingleBatch
            SingleInvoice
            BatchOfInvoices
        End Enum

        Private Enum EnumSearchMode
            Tariffs
            BoltOns
            Funding
        End Enum

        Public Enum EnumGenerationMode
            StandardLines = 0
            SummaryColumns = 1
            KLN = 2
        End Enum

#End Region

        Private m_ReportData As New List(Of IInvoiceDocument)
        Private m_PrintData As New List(Of IInvoiceDocument)

        Public Sub RunInvoices(ByVal OutputMode As EnumOutputMode, ByVal InvoicesToSend As List(Of Guid), _
                               ByVal Narrative As String, ByRef Errors As String, ByVal EmailSubject As String, ByVal EmailBody As String)

            Process(OutputMode, InvoicesToSend, Narrative, Errors, EmailSubject, EmailBody)

        End Sub

        Private Sub Process(ByVal OutputMode As EnumOutputMode, ByVal InvoicesToSend As List(Of Guid), ByVal Narrative As String, ByRef Errors As String, ByVal EmailSubjectTemplate As String, ByVal EmailBodyTemplate As String)

            FileHandler.ClearTemporaryFolder(Session.TempFolder)

            Dim _InvoiceIDList As String = ""
            Dim _InvoiceCount As Integer = 0
            For Each _ID In InvoicesToSend
                If _InvoiceCount = 0 Then
                    _InvoiceIDList += "'" + _ID.ToString + "'"
                Else
                    _InvoiceIDList += ",'" + _ID.ToString + "'"
                End If
                _InvoiceCount += 1
            Next

            Dim _SQL As String = "select Invoices.id, invoice_status, child_id, family_id, e_invoicing, invoice_layout, generation_mode from Invoices" & _
                                 " left join Family on Family.ID = Invoices.family_id"

            _SQL += " where Invoices.ID in (" + _InvoiceIDList + ")"

            Select Case OutputMode

                Case EnumOutputMode.EmailOnly
                    _SQL += " and invoice_status <> 'Emailed'"

                Case EnumOutputMode.PrintedOnly
                    _SQL += " and e_invoicing <> 1"

            End Select

            _SQL += " order by family_name"

            Dim _dtInv As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If Not _dtInv Is Nothing Then

                If _dtInv.Rows.Count > 0 Then

                    Dim _LayoutFile As String = _dtInv.Rows(0).Item("invoice_layout").ToString
                    _LayoutFile = Invoicing.HandleInvoiceLayout(_LayoutFile)

                    Session.SetupProgressBar("Processing...", _dtInv.Rows.Count)
                    For Each _dr As DataRow In _dtInv.Rows

                        Dim _GenerationModeInt As Integer = ValueHandler.ConvertInteger(_dr.Item("generation_mode"))
                        Dim _GenerationMode As EnumGenerationMode = CType(_GenerationModeInt, EnumGenerationMode)

                        BuildReportData(_dr.Item(0).ToString, Narrative, _GenerationMode)

                        Dim _Count As Integer = m_ReportData.Count
                        If _Count > 0 Then

                            Select Case OutputMode

                                Case EnumOutputMode.EmailOnly, EnumOutputMode.ForceEmail
                                    EmailInvoice(_dr.Item("id").ToString, _dr.Item("family_id").ToString, _dr.Item("child_id").ToString, _LayoutFile, EmailSubjectTemplate, EmailBodyTemplate, Errors)

                                Case EnumOutputMode.PrintedOnly
                                    m_PrintData.AddRange(m_ReportData)
                                    Invoicing.UpdateInvoiceStatus(_dr.Item("id").ToString, "Printed")

                                Case EnumOutputMode.ForcePrint
                                    m_PrintData.AddRange(m_ReportData)

                            End Select

                        End If

                        Session.StepProgressBar()

                    Next

                    If m_PrintData.Count > 0 Then
                        ReportHandler.RunReport(Of IInvoiceDocument)(_LayoutFile, m_PrintData)
                    End If

                End If

                _dtInv.Dispose()
                _dtInv = Nothing

                Session.SetProgressMessage("Process Complete", 10)

            End If

            m_PrintData = Nothing

        End Sub

        Private Sub BuildReportData(ByVal InvoiceID As String, ByVal Narrative As String, ByVal GenerationMode As EnumGenerationMode)

            Select Case GenerationMode

                Case EnumGenerationMode.StandardLines
                    BuildDetailReportData(InvoiceID, Narrative)

                Case EnumGenerationMode.SummaryColumns
                    BuildSummaryColumnReportData(InvoiceID, Narrative)

                Case EnumGenerationMode.KLN
                    BuildKLNReportData(InvoiceID, Narrative)
            End Select

        End Sub

#Region "Standard Report Data (Lines)"

        Private Sub BuildDetailReportData(ByVal InvoiceID As String, ByVal Narrative As String)

            m_ReportData.Clear()

            Dim _DepositBalance As Decimal = 0
            Dim _DepositTaken As Decimal = 0
            Dim _DepositReturned As Decimal = 0

            Dim _SQL As String = "select Invoices.ID as invoice_id, invoice_no, invoice_date, Invoices.invoice_due_date, Invoices.invoice_due, invoice_period, invoice_period_end, invoice_layout," &
                                 " letter_name, Family.id as 'family_id', family_account_no, Family.e_invoicing, Invoices.child_id, Invoices.child_name," &
                                 " Invoices.address, Invoices.discount_name, Invoices.invoice_sub, Invoices.invoice_discount, Invoices.invoice_total," &
                                 " Invoices.voucher_mode, Invoices.voucher_proportion, Invoices.voucher_pool, Invoices.voucher_amount," &
                                 " InvoiceLines.action_date, InvoiceLines.description as 'line_text', InvoiceLines.value," &
                                 " InvoiceLines.section, InvoiceLines.hours" &
                                 " from Invoices" &
                                 " right join InvoiceLines on Invoices.ID = InvoiceLines.invoice_id" &
                                 " left join Family on Family.ID = Invoices.family_id" &
                                 " where Invoices.ID = '" & InvoiceID & "'" &
                                 " order by InvoiceLines.ref_2, InvoiceLines.line_no"

            Dim _dtInv As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If Not _dtInv Is Nothing Then

                If _dtInv.Rows.Count > 0 Then

                    For Each Record As DataRow In _dtInv.Rows

                        Dim _ReportRecord As New InvoiceData
                        With _ReportRecord

                            ._InvoiceID = New Guid(Record.Item("invoice_id").ToString)
                            ._AccountNo = Record.Item("family_account_no").ToString

                            ._InvoiceNo = Record.Item("invoice_no").ToString
                            ._InvoiceDate = CDate(Record.Item("invoice_date"))

                            ._InvoiceDueDate = CDate(Record.Item("invoice_due_date"))
                            ._InvoiceDueText = Record.Item("invoice_due").ToString

                            If Record.Item("invoice_period").ToString <> "" Then
                                ._InvoicePeriod = CDate(Record.Item("invoice_period"))
                            End If

                            If Record.Item("invoice_period_end").ToString <> "" Then
                                ._InvoicePeriodEnd = CDate(Record.Item("invoice_period_end"))
                            End If

                            ._InvoiceLayout = Record.Item("invoice_layout").ToString
                            ._Narrative = Narrative
                            ._InvoiceTo = Record.Item("letter_name").ToString
                            ._AddressBlock = Record.Item("address").ToString
                            ._ChildName = Record.Item("child_name").ToString

                            ._LineDate = CDate(Record.Item("action_date"))
                            ._LineText = Record.Item("line_text").ToString
                            ._LineRate = CDec(Record.Item("value"))

                            ._LineSection = Record.Item("section").ToString

                            ._LineHours = ValueHandler.ConvertDecimal(Record.Item("hours"))

                            ._VoucherMode = Record.Item("voucher_mode").ToString
                            ._VoucherProportion = ValueHandler.ConvertDecimal(Record.Item("voucher_proportion"))
                            ._VoucherPool = ValueHandler.ConvertDecimal(Record.Item("voucher_pool"))
                            ._VoucherAmount = ValueHandler.ConvertDecimal(Record.Item("voucher_amount"))

                            If m_ReportData.Count = 0 Then
                                SetDepositDetails(Record.Item("child_id").ToString, ._InvoicePeriod, ._InvoicePeriodEnd, _DepositBalance, _DepositTaken, _DepositReturned)
                            End If

                            ._DepositBalance = _DepositBalance
                            ._DepositThisTaken = _DepositTaken
                            ._DepositThisReturn = _DepositReturned

                            ._DiscountName = Record.Item("discount_name").ToString
                            ._InvoiceSub = Decimal.Parse(Record.Item("invoice_sub").ToString)
                            ._InvoiceDiscount = Decimal.Parse(Record.Item("invoice_discount").ToString)
                            ._InvoiceTotal = Decimal.Parse(Record.Item("invoice_total").ToString)

                        End With

                        m_ReportData.Add(_ReportRecord)

                    Next

                End If

            End If

            _dtInv.Dispose()
            _dtInv = Nothing

        End Sub

#End Region

#Region "Summary Column Data"

        Private Sub BuildSummaryColumnReportData(ByVal InvoiceID As String, ByVal Narrative As String)

            m_ReportData.Clear()

            Dim _DepositBalance As Decimal = 0
            Dim _DepositTaken As Decimal = 0
            Dim _DepositReturned As Decimal = 0

            Dim _SQL As String = ""

            _SQL += "select Invoices.ID as 'invoice_id', invoice_no, invoice_date, Invoices.invoice_due_date, Invoices.invoice_due, invoice_period, invoice_period_end, invoice_layout,"
            _SQL += " letter_name, Family.id as 'family_id', family_account_no, Family.e_invoicing, Invoices.child_id, Invoices.child_name,"
            _SQL += " Invoices.voucher_mode, Invoices.voucher_proportion, Invoices.voucher_pool, Invoices.voucher_amount,"
            _SQL += " Invoices.address, Invoices.discount_name, Invoices.invoice_sub, Invoices.invoice_discount, Invoices.invoice_total"
            _SQL += " from Invoices"
            _SQL += " left join Family On Family.ID = Invoices.family_id"
            _SQL += " where Invoices.ID = '" & InvoiceID & "'"

            Dim _dtInv As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If Not _dtInv Is Nothing Then

                If _dtInv.Rows.Count = 1 Then

                    Dim Record As DataRow = _dtInv.Rows(0)

                    Dim _SummaryColumnData As New SummaryColumnInvoiceData
                    With _SummaryColumnData

                        ._InvoiceID = New Guid(Record.Item("invoice_id").ToString)
                        ._AccountNo = Record.Item("family_account_no").ToString

                        ._InvoiceNo = Record.Item("invoice_no").ToString
                        ._InvoiceDate = CDate(Record.Item("invoice_date"))

                        ._InvoiceDueDate = CDate(Record.Item("invoice_due_date"))
                        ._InvoiceDueText = Record.Item("invoice_due").ToString

                        If Record.Item("invoice_period").ToString <> "" Then
                            ._InvoicePeriod = CDate(Record.Item("invoice_period"))
                        End If

                        If Record.Item("invoice_period_end").ToString <> "" Then
                            ._InvoicePeriodEnd = CDate(Record.Item("invoice_period_end"))
                        End If

                        ._InvoiceLayout = Record.Item("invoice_layout").ToString
                        ._Narrative = Narrative
                        ._InvoiceTo = Record.Item("letter_name").ToString
                        ._AddressBlock = Record.Item("address").ToString
                        ._ChildName = Record.Item("child_name").ToString

                        ._VoucherMode = Record.Item("voucher_mode").ToString
                        ._VoucherProportion = ValueHandler.ConvertDecimal(Record.Item("voucher_proportion"))
                        ._VoucherPool = ValueHandler.ConvertDecimal(Record.Item("voucher_pool"))
                        ._VoucherAmount = ValueHandler.ConvertDecimal(Record.Item("voucher_amount"))

                        ._DiscountName = Record.Item("discount_name").ToString
                        ._InvoiceSub = Decimal.Parse(Record.Item("invoice_sub").ToString)
                        ._InvoiceDiscount = Decimal.Parse(Record.Item("invoice_discount").ToString)
                        ._InvoiceTotal = Decimal.Parse(Record.Item("invoice_total").ToString)

                        If m_ReportData.Count = 0 Then
                            SetDepositDetails(Record.Item("child_id").ToString, ._InvoicePeriod, ._InvoicePeriodEnd, _DepositBalance, _DepositTaken, _DepositReturned)
                        End If

                        ._DepositBalance = _DepositBalance
                        ._DepositThisTaken = _DepositTaken
                        ._DepositThisReturn = _DepositReturned

                        '***************************************************************************************************************************************

                        'process columns
                        For _i = 0 To 9
                            SetSummaryColumn(InvoiceID, _i, _SummaryColumnData)
                        Next

                        'process day totals
                        For _i = 1 To 7
                            SetDayTotalColumn(InvoiceID, _i, _SummaryColumnData)
                        Next

                        '***************************************************************************************************************************************

                        'check if we have any itemised invoice lines (ones that don't go in the summary columns)
                        'we need to loop through all the invoice lines with a summary column of X
                        'these are lines that need to appear on the invoice as itemised
                        _SQL = ""
                        _SQL += "select action_date, description, hours, value from InvoiceLines"
                        _SQL += " where invoice_id = '" + InvoiceID + "'"
                        _SQL += " and summary_column = 'X'"
                        _SQL += " order by line_no"

                        Dim _dtLines As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
                        If _dtLines.Rows.Count > 0 Then
                            For Each _DR As DataRow In _dtLines.Rows
                                Dim _SCD As SummaryColumnInvoiceData = _SummaryColumnData.Clone
                                With _SCD
                                    ._LineDate = ValueHandler.ConvertDate(_DR.Item("action_date"))
                                    ._LineText = _DR.Item("description").ToString
                                    ._LineHours = ValueHandler.ConvertDecimal(_DR.Item("hours"))
                                    ._LineRate = ValueHandler.ConvertDecimal(_DR.Item("value"))
                                End With
                                m_ReportData.Add(_SCD)
                            Next
                        Else
                            'no itemised lines, so we just insert the one record
                            m_ReportData.Add(_SummaryColumnData)
                        End If

                        '***************************************************************************************************************************************

                    End With

                End If

            End If

        End Sub

        Private Sub SetDayTotalColumn(ByVal InvoiceID As String, ByVal DayNumber As Integer, ByRef SummaryColumnDataRecord As SummaryColumnInvoiceData)

            Dim _DayName As String = WeekdayName(DayNumber, False, FirstDayOfWeek.Monday)
            Dim _DayAbv As String = WeekdayName(DayNumber, True, FirstDayOfWeek.Monday)

            Dim _TotalCount As Integer = 0
            Dim _TotalHours As Decimal = 0
            Dim _TotalCost As Decimal = 0

            For _iColumnNo = 0 To 9

                Dim _CountField As String = "_" + _DayAbv + _iColumnNo.ToString + "Count"
                Dim _HoursField As String = "_" + _DayAbv + _iColumnNo.ToString + "Hours"
                Dim _CostField As String = "_" + _DayAbv + _iColumnNo.ToString + "Cost"

                Dim _CountValue As Integer = 0
                Dim _HoursValue As Decimal = 0
                Dim _CostValue As Decimal = 0

                _CountValue = ValueHandler.ConvertInteger(CallByName(SummaryColumnDataRecord, _CountField, CallType.Get))
                _HoursValue = ValueHandler.ConvertDecimal(CallByName(SummaryColumnDataRecord, _HoursField, CallType.Get))
                _CostValue = ValueHandler.ConvertDecimal(CallByName(SummaryColumnDataRecord, _CostField, CallType.Get))

                _TotalCount += _CountValue
                _TotalHours += _HoursValue
                _TotalCost += _CostValue

            Next

            Dim _TotalCountField As String = "_" + _DayAbv + "TotalCount"
            Dim _TotalHoursField As String = "_" + _DayAbv + "TotalHours"
            Dim _TotalCostField As String = "_" + _DayAbv + "TotalCost"

            CallByName(SummaryColumnDataRecord, _TotalCountField, CallType.Set, _TotalCount)
            CallByName(SummaryColumnDataRecord, _TotalHoursField, CallType.Set, _TotalHours)
            CallByName(SummaryColumnDataRecord, _TotalCostField, CallType.Set, _TotalCost)

        End Sub

        Private Sub SetSummaryColumn(ByVal InvoiceID As String, ByVal ColumnNumber As Integer, ByRef SummaryColumnDataRecord As SummaryColumnInvoiceData)

            For _i = 1 To 7

                Dim _DayName As String = WeekdayName(_i, False, FirstDayOfWeek.Monday)
                Dim _DayAbv As String = WeekdayName(_i, True, FirstDayOfWeek.Monday)

                Dim _SQL As String = ""

                _SQL += "select count(*) as 'Count', isnull(sum(hours),0) as 'Hours', isnull(sum(value),0) as 'Cost' from InvoiceLines"
                _SQL += " where invoice_id = '" + InvoiceID + "'"
                _SQL += " and datename(weekday,action_date) = '" + _DayName + "'"
                _SQL += " and summary_column <> 'X'"
                _SQL += " and summary_column = '" + ColumnNumber.ToString + "'"

                Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
                If _DR IsNot Nothing Then

                    Dim _CountField As String = "_" + _DayAbv + ColumnNumber.ToString + "Count"
                    Dim _HoursField As String = "_" + _DayAbv + ColumnNumber.ToString + "Hours"
                    Dim _CostField As String = "_" + _DayAbv + ColumnNumber.ToString + "Cost"

                    Dim _CountValue As Integer = ValueHandler.ConvertInteger(_DR.Item("Count"))
                    Dim _HoursValue As Decimal = ValueHandler.ConvertDecimal(_DR.Item("Hours"))
                    Dim _CostValue As Decimal = ValueHandler.ConvertDecimal(_DR.Item("Cost"))

                    CallByName(SummaryColumnDataRecord, _CountField, CallType.Set, _CountValue)
                    CallByName(SummaryColumnDataRecord, _HoursField, CallType.Set, _HoursValue)
                    CallByName(SummaryColumnDataRecord, _CostField, CallType.Set, _CostValue)

                End If

            Next

        End Sub

#End Region

#Region "Kids Love Nature (KLN)"

        Private Sub BuildKLNReportData(ByVal InvoiceID As String, ByVal Narrative As String)

            m_ReportData.Clear()

            Dim _SQL As String = ""

            _SQL += "select Invoices.ID as invoice_id, invoice_no, invoice_date, Invoices.invoice_due_date, Invoices.invoice_due, invoice_period, invoice_period_end, invoice_layout,"
            _SQL += " letter_name, Family.id as 'family_id', family_account_no, Family.e_invoicing, Invoices.child_id, Invoices.child_name,"
            _SQL += " Invoices.address, Invoices.discount_name, Invoices.invoice_sub, Invoices.invoice_discount, Invoices.invoice_total"
            _SQL += " from Invoices"
            _SQL += " left join Family on Family.ID = Invoices.family_id"
            _SQL += " where Invoices.ID = '" & InvoiceID & "'"

            Dim _dtInv As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If Not _dtInv Is Nothing Then

                'invoice exists
                If _dtInv.Rows.Count = 1 Then

                    Dim _ChargeBlock As String = ""
                    Dim _ChargesTotal As Decimal = 0

                    _SQL = ""
                    _SQL += "select action_date, description, value from InvoiceLines"
                    _SQL += " where invoice_id = '" + InvoiceID + "'"
                    _SQL += " and section = '3'"
                    _SQL += " order by action_date, value desc"

                    Dim _DTCharges As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
                    If _DTCharges IsNot Nothing Then

                        For Each _dr As DataRow In _DTCharges.Rows

                            Dim _ChargeDate As Date = ValueHandler.ConvertDate(_dr.Item("action_date")).Value
                            Dim _ChargeDesc As String = _dr.Item("description").ToString
                            Dim _ChargeValue As Decimal = ValueHandler.ConvertDecimal(_dr.Item("value"))

                            If _ChargeBlock <> "" Then _ChargeBlock += vbCrLf

                            Dim _Line As String = Format(_ChargeDate, "dd/MM/yyyy").ToString + " - " + _ChargeDesc + " = £" + Format(_ChargeValue, "0.00")
                            _ChargeBlock += _Line

                            _ChargesTotal += _ChargeValue

                        Next

                    End If

                    Dim _SummaryRecord As New KLNInvoiceData
                    SetKLNSummaryData(InvoiceID, Narrative, _ChargeBlock, _ChargesTotal, _dtInv.Rows(0), _SummaryRecord)

                    Dim _ChildID As String = _dtInv.Rows(0).Item("child_id").ToString
                    Dim _InvoiceFrom As Date? = ValueHandler.ConvertDate(_dtInv.Rows(0).Item("invoice_period"))
                    Dim _InvoiceTo As Date? = ValueHandler.ConvertDate(_dtInv.Rows(0).Item("invoice_period_end"))

                    SetDepositDetails(_ChildID, _InvoiceFrom, _InvoiceTo, _SummaryRecord._DepositBalance, _SummaryRecord._DepositThisTaken, _SummaryRecord._DepositThisReturn)

                    m_ReportData.Add(_SummaryRecord)

                End If

            End If

            _dtInv.Dispose()
            _dtInv = Nothing

        End Sub

        Private Sub SetKLNSummaryData(ByVal InvoiceID As String, ByVal Narrative As String, _
                                   ByVal ChargeBlock As String, ByVal ChargesTotal As Decimal, _
                                   ByRef Record As DataRow, ByRef KLNData As KLNInvoiceData)

            With KLNData

                ._InvoiceID = New Guid(InvoiceID)
                ._AccountNo = Record.Item("family_account_no").ToString

                ._InvoiceNo = Record.Item("invoice_no").ToString
                ._InvoiceDate = CDate(Record.Item("invoice_date"))

                ._InvoiceDueDate = CDate(Record.Item("invoice_due_date"))
                ._InvoiceDueText = Record.Item("invoice_due").ToString

                If Record.Item("invoice_period").ToString <> "" Then
                    ._InvoicePeriod = CDate(Record.Item("invoice_period"))
                End If

                If Record.Item("invoice_period_end").ToString <> "" Then
                    ._InvoicePeriodEnd = CDate(Record.Item("invoice_period_end"))
                End If

                ._InvoiceLayout = Record.Item("invoice_layout").ToString
                ._Narrative = Narrative
                ._InvoiceTo = Record.Item("letter_name").ToString
                ._AddressBlock = Record.Item("address").ToString
                ._ChildName = Record.Item("child_name").ToString

                ._DiscountName = Record.Item("discount_name").ToString
                ._InvoiceSub = Decimal.Parse(Record.Item("invoice_sub").ToString)
                ._InvoiceDiscount = Decimal.Parse(Record.Item("invoice_discount").ToString)
                ._InvoiceTotal = Decimal.Parse(Record.Item("invoice_total").ToString)

                GetKLNSummaryData(InvoiceID, DayOfWeek.Monday, .MonCount, .MonFunded, .MonNonFundedHours, .MonNonFundedCost, .MonLunchCount, .MonLunchCost, .MonCommCount, .MonCommCost, .MonAddlCount, .MonAddlCost, .MonTotalCost)
                GetKLNSummaryData(InvoiceID, DayOfWeek.Tuesday, .TueCount, .TueFunded, .TueNonFundedHours, .TueNonFundedCost, .TueLunchCount, .TueLunchCost, .TueCommCount, .TueCommCost, .TueAddlCount, .TueAddlCost, .TueTotalCost)
                GetKLNSummaryData(InvoiceID, DayOfWeek.Wednesday, .WedCount, .WedFunded, .WedNonFundedHours, .WedNonFundedCost, .WedLunchCount, .WedLunchCost, .WedCommCount, .WedCommCost, .WedAddlCount, .WedAddlCost, .WedTotalCost)
                GetKLNSummaryData(InvoiceID, DayOfWeek.Thursday, .ThuCount, .ThuFunded, .ThuNonFundedHours, .ThuNonFundedCost, .ThuLunchCount, .ThuLunchCost, .ThuCommCount, .ThuCommCost, .ThuAddlCount, .ThuAddlCost, .ThuTotalCost)
                GetKLNSummaryData(InvoiceID, DayOfWeek.Friday, .FriCount, .FriFunded, .FriNonFundedHours, .FriNonFundedCost, .FriLunchCount, .FriLunchCost, .FriCommCount, .FriCommCost, .FriAddlCount, .FriAddlCost, .FriTotalCost)

                SetVoucherDetails(Record.Item("family_id").ToString, True, ._VouchPriAmount, ._VouchPriName)
                SetVoucherDetails(Record.Item("family_id").ToString, False, ._VouchOtherAmount, ._VouchOtherName)

                ._ChargeBlock = ChargeBlock
                ._VouchTotal = ._VouchPriAmount + ._VouchOtherAmount

                .TotalCharges = ChargesTotal
                .TotalGrand = .MonTotalCost + .TueTotalCost + .WedTotalCost + .ThuTotalCost + .FriTotalCost + .TotalCharges

                If ._VouchTotal > .TotalGrand Then
                    'vouchers are more than invoice is worth, we therefore set the DD amount to zero
                    .TotalNetVouchers = 0
                Else
                    .TotalNetVouchers = .TotalGrand - ._VouchTotal
                End If

            End With

        End Sub

        Private Sub GetKLNSummaryData(ByVal InvoiceID As String, ByVal Day As DayOfWeek, ByRef SessionCount As Integer, ByRef FundedHours As Decimal,
                                      ByRef NonFundedHours As Decimal, ByRef NonFundedCost As Decimal, ByRef LunchCount As Integer, ByRef LunchCost As Decimal,
                                      ByRef CommuterCount As Integer, ByRef CommuterCost As Decimal,
                                      ByRef AdditionalCount As Integer, ByRef AdditionalCost As Decimal, ByRef TotalCost As Decimal)

            Dim _Days As Integer = Day + 1
            Dim _SQL As String = ""

            _SQL += "select count(*) as 'Count', sum(l.hours) as 'Hours', sum(l.value) as 'Total'"
            _SQL += " from InvoiceLines l"
            _SQL += " left join Tariffs t on t.id = l.ref_3"
            _SQL += " where l.invoice_id = '" + InvoiceID + "'"
            _SQL += " and l.section = 2"
            _SQL += " and l.description not like '%Commute%'"
            _SQL += " and DATEPART(WEEKDAY, l.action_date) = " + _Days.ToString

            SessionCount = 0
            NonFundedHours = 0
            NonFundedCost = 0

            Dim _DRNonFunded As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
            If _DRNonFunded IsNot Nothing Then
                SessionCount = ValueHandler.ConvertInteger(_DRNonFunded.Item("Count"))
                NonFundedHours = ValueHandler.ConvertDecimal(_DRNonFunded.Item("Hours"))
                NonFundedCost = ValueHandler.ConvertDecimal(_DRNonFunded.Item("Total"))
            End If

            '****************************************************************************************************************************

            _SQL = ""
            _SQL += "select count(*) as 'Count', sum(l.hours) as 'Hours', sum(l.value) as 'Total'"
            _SQL += " from InvoiceLines l"
            _SQL += " left join Tariffs t on t.id = l.ref_3"
            _SQL += " where l.invoice_id = '" + InvoiceID + "'"
            _SQL += " and l.section = 2"
            _SQL += " and l.description like '%Commute%'"
            _SQL += " and DATEPART(WEEKDAY, l.action_date) = " + _Days.ToString

            CommuterCount = 0
            CommuterCost = 0

            Dim _DRCommuter As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
            If _DRCommuter IsNot Nothing Then
                CommuterCount = ValueHandler.ConvertInteger(_DRCommuter.Item("Count"))
                CommuterCost = ValueHandler.ConvertDecimal(_DRCommuter.Item("Total"))
            End If

            '****************************************************************************************************************************

            _SQL = ""
            _SQL += "select sum(l.hours) as 'Hours'"
            _SQL += " from InvoiceLines l"
            _SQL += " where l.invoice_id = '" + InvoiceID + "'"
            _SQL += " and l.section = 1"
            _SQL += " and DATEPART(WEEKDAY, l.action_date) = " + _Days.ToString

            FundedHours = 0

            Dim _DRFunded As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
            If _DRFunded IsNot Nothing Then
                FundedHours = ValueHandler.ConvertDecimal(_DRFunded.Item("Hours"))
            End If

            '****************************************************************************************************************************

            'remove any funded hours
            NonFundedHours = NonFundedHours - FundedHours

            GetKLNBoltOnData(InvoiceID, _Days.ToString, "Lunch", LunchCount, LunchCost)
            GetKLNBoltOnData(InvoiceID, _Days.ToString, "ADS", AdditionalCount, AdditionalCost)

            TotalCost = NonFundedCost + LunchCost + CommuterCost + AdditionalCost

        End Sub

        Private Sub GetKLNBoltOnData(ByVal InvoiceID As String, ByVal Days As String, ByVal BoltOnSearch As String, ByRef Count As Integer, ByRef Value As Decimal)

            Dim _SQL As String = ""

            Count = 0
            Value = 0

            _SQL += "select count(*) as 'Count', sum(l.value) as 'Total'"
            _SQL += " from InvoiceLines l"
            _SQL += " left join TariffBoltOns b on b.id = l.ref_3"
            _SQL += " where l.invoice_id = '" + InvoiceID + "'"
            _SQL += " and l.section = 5"
            _SQL += " and b.name like '%" + BoltOnSearch + "%'"
            _SQL += " and DATEPART(WEEKDAY, l.action_date) = " + Days

            Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
            If _DR IsNot Nothing Then
                Count = ValueHandler.ConvertInteger(_DR.Item("Count"))
                Value = ValueHandler.ConvertDecimal(_DR.Item("Total"))
            End If

        End Sub

#End Region

#Region "Email Functionality"

        Private Sub EmailInvoice(ByVal InvoiceID As String, ByVal FamilyID As String, ByVal ChildID As String, ByVal LayoutFile As String, ByVal EmailSubjectTemplate As String, ByVal EmailBodyTemplate As String, ByRef Errors As String)

            Dim _Child As Business.Child = Nothing
            If ChildID <> "" Then
                _Child = Business.Child.RetreiveByID(New Guid(ChildID))
            End If

            'produce pdf
            Dim _PDFFile As String = ""
            Dim _PDFOK As Boolean = CreatePDF(LayoutFile, m_ReportData, _PDFFile)

            If _PDFOK Then

                Dim _FriendlyPDFFile As String = FriendlyPDFFile(m_ReportData(0)._ChildName, m_ReportData(0)._InvoicePeriod)

                Dim _pos As Integer = _PDFFile.LastIndexOf("\")
                Dim _path As String = _PDFFile.Trim.Substring(0, _pos)

                'check if the file to be renamed exists and delete
                If My.Computer.FileSystem.FileExists(_path & "\" & _FriendlyPDFFile) Then
                    My.Computer.FileSystem.DeleteFile(_path & "\" & _FriendlyPDFFile)
                End If

                My.Computer.FileSystem.RenameFile(_PDFFile, _FriendlyPDFFile)

                For Each _Contact In Business.Contact.RetrieveInvoiceContacts(New Guid(FamilyID))

                    Dim _Subject As String = MergePlaceHolders(EmailSubjectTemplate, _Contact, _Child)
                    Dim _Body As String = MergePlaceHolders(EmailBodyTemplate, _Contact, _Child)

                    Dim _EmailError As String = ""
                    Dim _Sender As Net.Mail.MailAddress = Business.Site.ReturnEmailAddress(_Child._SiteId.Value, Business.Site.EnumEmailAddressType.Finance)
                    Dim _EmailStatus As Integer = EmailHandler.SendEmailWithAttachmentReturnStatus(_Sender, _Contact._Email, _Subject, _Body, Session.TempFolder + _FriendlyPDFFile, _EmailError)

                    If _EmailStatus = 0 Then
                        Invoicing.UpdateInvoiceStatus(InvoiceID, "Emailed")
                    Else
                        Dim _NameAndEmail As String = _Contact._Fullname + " <" + _Contact._Email + ">"
                        LogError(Errors, _NameAndEmail + " : " + _EmailError)
                    End If

                Next

            Else
                'pdf error
                LogError(Errors, "Create PDF Failed: " + _PDFFile)
            End If

        End Sub

        Private Function FriendlyPDFFile(ByVal ChildName As String, ByVal InvoicePeriod As Date) As String
            Return ChildName + " - " + Format(InvoicePeriod, "ddMMyyyy") + ".pdf"
        End Function

        Public Function MergePlaceHolders(ByVal TemplateIn As String, ByRef ContactIn As Business.Contact, ByRef ChildIn As Business.Child) As String

            Dim _Return As String = TemplateIn

            'merge contact fields
            If ContactIn IsNot Nothing Then
                _Return = _Return.Replace("{ContactForename}", ContactIn._Forename)
                _Return = _Return.Replace("{ContactFullname}", ContactIn._Forename + " " + ContactIn._Surname)
            End If

            'merge child fields
            If ChildIn IsNot Nothing Then
                _Return = _Return.Replace("{ChildForename}", ChildIn._Forename)
                _Return = _Return.Replace("{ChildFullname}", ChildIn._Forename + " " + ChildIn._Surname)
            End If

            If m_ReportData IsNot Nothing Then

                _Return = _Return.Replace("{InvoiceNo}", m_ReportData(0)._InvoiceNo)
                _Return = _Return.Replace("{InvoiceDue}", Format(m_ReportData(0)._InvoiceDueDate, "dd/MM/yyyy"))
                _Return = _Return.Replace("{InvoiceTotal}", Format(m_ReportData(0)._InvoiceTotal, "0.00"))

                Dim _Period As String = Format(m_ReportData(0)._InvoicePeriod, "d MMMM yyyy") + " to " + Format(m_ReportData(0)._InvoicePeriodEnd, "d MMMM yyyy")
                _Return = _Return.Replace("{InvoicePeriod}", _Period)

                _Return = _Return.Replace("{InvoiceMonth}", MonthName(m_ReportData(0)._InvoicePeriod.Month))

            End If

            Return _Return

        End Function

        Public Function CreatePDF(Of T)(ByVal LayoutFile As String, ByVal InvoiceData As List(Of T), ByRef PDFPath As String) As Boolean

            Try
                PDFPath = ReportHandler.PDFReport(Of T)(LayoutFile, InvoiceData)

            Catch ex As Exception
                Return False
            End Try

            If IO.File.Exists(PDFPath) Then
                Return True
            Else
                Return False
            End If

        End Function

#End Region

#Region "Shared Functions"

        Private Sub SetVoucherDetails(ByVal FamilyID As String, ByVal Primary As Boolean, ByRef VoucherAmount As Decimal, ByRef VoucherProvider As String)

            Dim _SQL As String = ""

            _SQL += "select fullname, voucher_value from Contacts"
            _SQL += " where family_id = '" + FamilyID + "'"
            _SQL += " and voucher_value > 0"

            If Primary Then
                _SQL += " and primary_cont = 1"
            Else
                _SQL += " and primary_cont = 0"
            End If

            Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
            If _DR IsNot Nothing Then
                VoucherProvider = _DR.Item("fullname").ToString
                VoucherAmount = ValueHandler.ConvertDecimal(_DR.Item("voucher_value"))
            Else
                VoucherProvider = ""
                VoucherAmount = 0
            End If

        End Sub

        Private Sub SetDepositDetails(ByVal ChildID As String, ByVal InvoiceFrom As Date?, ByVal InvoiceTo As Date?, ByRef Balance As Decimal, ByRef ThisTaken As Decimal, ByRef ThisReturned As Decimal)

            Balance = 0
            ThisTaken = 0
            ThisReturned = 0

            If ChildID <> "" Then

                Balance = Business.Child.ReturnDepositBalance(New Guid(ChildID))

                If InvoiceFrom.HasValue AndAlso InvoiceTo.HasValue Then

                    Dim _SQL As String = ""
                    Dim _f As String = ValueHandler.SQLDate(InvoiceFrom.Value, True)
                    Dim _t As String = ValueHandler.SQLDate(InvoiceTo.Value, True)

                    _SQL += "select isnull(sum(t.dep_value),0) as 'taken', isnull(sum(r.dep_value),0) as 'returned' from Children c"
                    _SQL += " left join ChildDeposits t On t.child_id = c.id And t.dep_type = 'Take Deposit' and t.dep_date between " + _f + " and " + _t
                    _SQL += " left join ChildDeposits r On r.child_id = c.id And r.dep_type = 'Return Deposit' and r.dep_date between " + _f + " and " + _t
                    _SQL += " where c.id = '" + ChildID + "'"

                    Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
                    If _DR IsNot Nothing Then
                        ThisTaken = ValueHandler.ConvertDecimal(_DR.Item("taken"))
                        ThisReturned = ValueHandler.ConvertDecimal(_DR.Item("returned"))
                    End If

                End If

            End If

        End Sub

        Private Sub LogError(ByRef ErrorLog As String, ByVal ErrorText As String)
            If ErrorLog = "" Then
                ErrorLog = ErrorText
            Else
                ErrorLog += vbNewLine + ErrorText
            End If
        End Sub

#End Region

    End Class

End Namespace

