﻿

Imports Care.Global
Imports Care.Data
Imports Care.Shared

Namespace Reports

    Public Class DailyReport

        Private m_DailyReportsEmailBody As String = ""
        Private m_Mode As EnumRunMode
        Private m_ReportDate As Date
        Private m_SiteID As Guid
        Private m_FamilyID As String = ""
        Private m_ChildID As String = ""
        Private m_Interact As Boolean = False
        Private m_TaskID As Guid? = Nothing
        Private m_GroupID As String = ""

        Public Sub New()
            m_DailyReportsEmailBody = ParameterHandler.ReturnString("DAYBODY")
        End Sub

        Public Sub New(ByVal Mode As EnumRunMode, ByVal ReportDate As Date, ByVal SiteID As Guid,
                               Optional ByVal FamilyID As String = "",
                               Optional ByVal ChildID As String = "",
                               Optional ByVal Interact As Boolean = False,
                               Optional ByVal TaskID As Guid? = Nothing,
                               Optional ByVal GroupID As String = "")

            m_DailyReportsEmailBody = ParameterHandler.ReturnString("DAYBODY")
            m_Mode = Mode
            m_ReportDate = ReportDate
            m_SiteID = SiteID
            m_FamilyID = FamilyID
            m_ChildID = ChildID
            m_Interact = Interact
            m_TaskID = TaskID
            m_GroupID = GroupID

        End Sub

        Private Enum ReportDetail
            Basic
            Standard
            HighDetail
        End Enum

        Public Enum EnumRunMode
            Print = 0
            Email = 1
            Both = 2
            ForcedPrint = 3
            ForcedEmail = 4
            Task = 99
        End Enum

        Public Function EmailReportForToday(ByVal ChildID As String) As Integer

            Dim _Errors As String = ""
            Dim _SendResult As Integer = 0

            Dim _Child As Business.Child = Business.Child.RetreiveByID(New Guid(ChildID))
            If _Child IsNot Nothing Then

                Dim _Day As Business.Day = Business.Day.RetreiveByDateAndSite(Today, _Child._SiteId.Value)
                If _Day IsNot Nothing Then

                    Try
                        Dim _DailyReport As New DailyReport(EnumRunMode.ForcedEmail, Today, _Child._SiteId.Value, _Child._FamilyId.ToString, ChildID, False)
                        _SendResult = _DailyReport.Run
                        If _SendResult = 0 Then
                            'all OK
                        Else
                            _SendResult = _SendResult
                        End If

                    Catch ex As Exception
                        _SendResult = -99
                        _Errors = ex.Message
                    End Try

                Else
                    'no day record
                    _SendResult = -97
                    _Errors = "Unable to find Day Record"
                End If

            Else
                'cannot find child record
                _SendResult = -98
                _Errors = "Unable to find Child Record"
            End If

            Return _SendResult

        End Function

        Public Function Run() As Integer

            m_DailyReportsEmailBody = ParameterHandler.ReturnString("DAYBODY")

            Dim _Return As Integer = 0
            Dim _ReportList As New List(Of DailyReportData)

            Dim _TaskMode As Boolean = False
            Dim _UserWantToPrint As Boolean = False
            Dim _UserWantToEmail As Boolean = False

            Select Case m_Mode

                Case EnumRunMode.Print
                    _UserWantToPrint = True

                Case EnumRunMode.Email
                    _UserWantToEmail = True

                Case EnumRunMode.Both
                    _UserWantToPrint = True
                    _UserWantToEmail = True

                Case EnumRunMode.Task
                    _TaskMode = True

            End Select

            Dim _Today As String = Format(m_ReportDate, "yyyy-MM-dd")

            Dim _SQL As String = "select id, message from Day where date = '" + _Today + "' and site_id = '" + m_SiteID.ToString + "'"

            Dim _dtToday As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If Not _dtToday Is Nothing Then

                If _dtToday.Rows.Count = 1 Then

                    Dim _TodayID As String = _dtToday.Rows(0).Item("ID").ToString
                    Dim _Message As String = _dtToday.Rows(0).Item("message").ToString

                    _SQL = "select person_id, name from Register" &
                           " left join Children on Children.ID = Register.person_id" &
                           " where date = '" & _Today & "'" &
                           " and type = 'C' and in_out = 'I'"

                    If m_FamilyID <> "" Then _SQL += " and Children.family_id = '" & m_FamilyID & "'"
                    If m_ChildID <> "" Then _SQL += " and Children.ID = '" & m_ChildID & "'"
                    If m_GroupID <> "" Then _SQL += " and Children.group_id = '" & m_GroupID & "'"

                    Dim _dtReg As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
                    If Not _dtReg Is Nothing Then

                        If m_Interact AndAlso m_Mode = EnumRunMode.Email Then
                            Session.SetupProgressBar("Sending Reports via Email...", _dtReg.Rows.Count)
                        End If

                        For Each _dr As DataRow In _dtReg.Rows

                            Dim _ChildID As Guid = New Guid(_dr.Item("person_id").ToString)
                            Dim _ChildRecord As Business.Child = Business.Child.RetreiveByID(_ChildID)

                            Dim _PaperReportOnChildRecord As Boolean = False

                            Select Case m_Mode

                                Case EnumRunMode.ForcedPrint
                                    _UserWantToPrint = True
                                    _PaperReportOnChildRecord = True
                                    _UserWantToEmail = False

                                Case EnumRunMode.ForcedEmail
                                    _UserWantToPrint = False
                                    _PaperReportOnChildRecord = False
                                    _UserWantToEmail = True

                                Case Else
                                    _PaperReportOnChildRecord = _ChildRecord._PaperReport

                            End Select

                            'if this is printed report, or we are saving a PDF - we need to use the report detail from the child record
                            If (_UserWantToPrint AndAlso _PaperReportOnChildRecord) Or _TaskMode Then

                                Dim _ReportDetail As ReportDetail = ReportDetail.Standard

                                If _ChildRecord._ReportDetail = "B" Then _ReportDetail = ReportDetail.Basic
                                If _ChildRecord._ReportDetail = "S" Then _ReportDetail = ReportDetail.Standard
                                If _ChildRecord._ReportDetail = "H" Then _ReportDetail = ReportDetail.HighDetail

                                Dim _ReportRecord As DailyReportData = ReturnReportRecord(_TodayID, m_ReportDate, _ChildRecord, _ReportDetail, _Message)
                                If _ReportRecord IsNot Nothing Then

                                    'user wants to print reports - so we add to the list
                                    If _UserWantToPrint AndAlso _PaperReportOnChildRecord Then
                                        _ReportList.Add(_ReportRecord)
                                    End If

                                    'we need to create the pdf
                                    If _TaskMode Then
                                        If m_TaskID.HasValue Then
                                            Dim _FileName As String = Session.TempFolder & m_TaskID.Value.ToString & ".pdf"
                                            Dim _PDFResult As Integer = GeneratePDF(_FileName, _ReportRecord)
                                            If _PDFResult <> 0 Then
                                                'pdf error
                                                _Return = (_PDFResult * 100)
                                            End If
                                        Else
                                            'no task id
                                            _Return = -5
                                        End If
                                    End If

                                Else
                                    'no report data
                                    _Return = -4
                                End If

                            End If

                            If _UserWantToEmail Then
                                'we cannot pass in the generated report above as contacts can choose their own detail level
                                EmailContacts(_TodayID, m_ReportDate, _ChildRecord, _Message, "")
                            End If

                            If m_Interact AndAlso m_Mode = EnumRunMode.Email Then
                                Session.StepProgressBar()
                                Application.DoEvents()
                            End If

                        Next

                        _dtReg.Dispose()
                        _dtReg = Nothing

                        If m_Interact AndAlso m_Mode = EnumRunMode.Email Then
                            Session.SetProgressMessage("Process Complete.", 5)
                        End If

                    Else
                        'no register record
                        _Return = -3
                    End If

                    If _UserWantToPrint Then
                        ReportHandler.RunReport(Of DailyReportData)("XtraDailyReport.repx", _ReportList)
                    End If

                Else
                    'today rows <> 1
                    _Return = -2
                    If m_Interact Then CareMessage("There is no report data available for " + m_ReportDate.ToShortDateString + ".", MessageBoxIcon.Exclamation, "Parent Reports")
                End If

                _dtToday.Dispose()
                _dtToday = Nothing

            Else
                'no today file
                _Return = -1
            End If

            _ReportList = Nothing

            Return _Return

        End Function

        Private Function GeneratePDF(ByVal FileName As String, ByRef ReportData As DailyReportData) As Integer

            Dim _ReportData As New List(Of DailyReportData)
            _ReportData.Add(ReportData)

            Dim _Return As Integer = ReportHandler.PDFReport(Of DailyReportData)("XtraDailyReport.repx", FileName, _ReportData)

            _ReportData = Nothing

            Return _Return

        End Function

        ''' <summary>
        ''' Email Contacts
        ''' </summary>
        ''' <param name="TodayID"></param>
        ''' <param name="ReportDate"></param>
        ''' <param name="ChildRecord"></param>
        ''' <param name="Message"></param>
        ''' <returns>Return 0 if successful, negative for error, positive for not sent (but not really an error - like not setup for email etc)</returns>
        ''' <remarks></remarks>
        Private Function EmailContacts(ByVal TodayID As String, ByVal ReportDate As Date, ByRef ChildRecord As Business.Child, ByVal Message As String, ByRef Errors As String) As Integer

            Dim _Result As Integer = -1 'assume some sort of exception

            Dim _ReportPDF As String = ChildRecord._Fullname.Replace(" ", "_") & "-Report-" & Format(ReportDate, "ddMMyyyy") & ".pdf"
            Dim _ReportPDFPath As String = Session.TempFolder + _ReportPDF
            Dim _IncidentNameTemplate As String = ChildRecord._Fullname.Replace(" ", "_") & "-Incident-" & Format(ReportDate, "ddMMyyyy")
            Dim _Attachments As String = _ReportPDFPath

            'check if there were any incidents
            Dim _IncidentAttachments As String = ""
            _IncidentAttachments = ReturnIncidentPDFs(TodayID, ChildRecord._ID.Value.ToString, Session.TempFolder, _IncidentNameTemplate)
            If _IncidentAttachments <> "" Then
                _Attachments += _IncidentAttachments
            End If

            'get contacts who receive reports via email
            Dim _SQL As String = "select surname, forename, report_detail, email" &
                                 " from Contacts" &
                                 " where family_id = '" & ChildRecord._FamilyId.ToString & "'" &
                                 " and report = 1" &
                                 " and len(email) > 0"

            Dim _dtContact As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If Not _dtContact Is Nothing Then

                If _dtContact.Rows.Count > 0 Then

                    Dim _Record As DailyReportData
                    For Each Row As DataRow In _dtContact.Rows

                        Dim _ReportDetail As ReportDetail = ReportDetail.Standard
                        If Row.Item("report_detail").ToString = "B" Then _ReportDetail = ReportDetail.Basic
                        If Row.Item("report_detail").ToString = "S" Then _ReportDetail = ReportDetail.Standard
                        If Row.Item("report_detail").ToString = "H" Then _ReportDetail = ReportDetail.HighDetail

                        _Record = ReturnReportRecord(TodayID, ReportDate, ChildRecord, _ReportDetail, Message)

                        If _Record IsNot Nothing Then

                            Dim _PDFResult As Integer = GeneratePDF(_ReportPDFPath, _Record)
                            If _PDFResult = 0 Then

                                Dim _To As String = Row.Item("email").ToString
                                Dim _Forename As String = Row.Item("forename").ToString
                                Dim _Subject As String = ChildRecord._Fullname & " - " & Format(ReportDate, "dddd d MMMM yyyy")
                                Dim _Body As String = "Dear " & _Forename & "," & vbCrLf & vbCrLf & m_DailyReportsEmailBody

                                Dim _EmailResult As Integer = EmailHandler.SendEmailWithAttachmentReturnStatus(Business.Site.ReturnEmailAddress(ChildRecord._SiteId.Value, Business.Site.EnumEmailAddressType.DailyReport), _To, _Subject, _Body, _Attachments, Errors)
                                If _EmailResult = 0 Then
                                    'emailed OK
                                    _Result = 0
                                Else
                                    'email failed
                                    _Result = _EmailResult * 10
                                End If

                            Else
                                'pdf generation failed
                                _Result = _PDFResult * 100
                            End If

                        Else
                            'no report record, return a warning
                            _Result = 3
                        End If

                    Next

                Else
                    'no contacts with email addresses who want reports, return a warning
                    _Result = 2
                End If

                _dtContact.Dispose()
                _dtContact = Nothing

            Else
                'no contacts with email addresses who want reports, return a warning
                _Result = 1
            End If

            Return _Result

        End Function

        Private Function ReturnIncidentPDFs(ByVal TodayID As String, ByVal ChildID As String, ByVal TempFolder As String, ByVal FileNameTemplate As String) As String

            Dim _Return As String = ""
            Dim _IncidentID As String = ""

            Dim _SQL As String = ""
            _SQL += "select ID from Incidents"
            _SQL += " where day_id = '" + TodayID + "'"
            _SQL += " and child_id = '" + ChildID + "'"
            _SQL += " order by stamp"

            Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If _DT IsNot Nothing Then

                Dim _i As Integer = 1
                For Each _DR As DataRow In _DT.Rows

                    _IncidentID = _DR.Item("ID").ToString
                    Dim _PDFFileOnly As String = FileNameTemplate + "_" + _i.ToString + ".pdf"
                    Dim _PDFFilePath As String = TempFolder + _PDFFileOnly

                    If ReportHandler.RepositoryReportPDF("Incident Report", " and i.ID = '" + _IncidentID + "'", _PDFFileOnly, Session.ReportFolder) Then
                        _Return += "|" + _PDFFilePath
                    End If

                    _i += 1

                Next
            End If

            Return _Return

        End Function

        Private Function ReturnDailyReportObject(ByVal TodayID As String, ByVal ReportDate As Date, ByVal ReportDetailLevel As ReportDetail,
                                                        ByVal ChildID As String, ByVal ChildName As String, ByVal ChildGroup As String,
                                                        ByVal ChildNappies As Boolean, ByVal ChildMilk As Boolean, ByVal ChildBabyFood As Boolean, ByVal ChildOffMenu As Boolean,
                                                        ByVal ChildDietRestriction As String, ByVal Message As String) As DailyReportData

            Dim _Record As New DailyReportData
            With _Record
                ._ReportDate = ReportDate
                ._ChildName = ChildName
                ._Activity = ReturnActivities(TodayID, ChildGroup)
                ._Food = ReturnFood(TodayID, ChildID, ChildBabyFood, ChildOffMenu, ReportDetailLevel, ChildDietRestriction)
                ._Milk = ReturnMilk(TodayID, ChildID, ChildMilk)
                ._Nappies = ReturnToilet(TodayID, ChildID)
                ._ToiletDetailed = ReturnToiletDetailed(TodayID, ChildID)
                ._Sleep = ReturnSleep(TodayID, ChildID)
                ._Message = Message
                ._Requests = ReturnRequests(TodayID, ChildID)
                ._KeyWorker = ReturnKeyWorker(ChildID)
                ._Drinks = ReturnDrinks(TodayID, ChildID)
                ._Medication = ReturnMedication(TodayID, ChildID)
                ._Incident = ReturnIncident(TodayID, ChildID)
                ._PersonalComments = ReturnPersonalComments(TodayID, ChildID)
                ._Suncream = ReturnSuncream(TodayID, ChildID)
                ._Toothbrushing = ReturnToothbrushing(TodayID, ChildID)
                ._PicPersonal = ReturnImage(TodayID, ChildID, "ReportChild")
                ._PicRoom = ReturnImage(TodayID, ChildID, "ReportRoom")
                ._PicSite = ReturnImage(TodayID, ChildID, "ReportSite")
            End With

            Return _Record

        End Function

        Private Function ReturnImage(ByVal TodayID As String, ByVal ChildID As String, ByVal KeyType As String) As Byte()

            Dim _Return As Byte() = Nothing

            Dim _SQL As String = ""
            _SQL += "select data from Media"
            _SQL += " where day_id = '" + TodayID + "'"
            _SQL += " and key_id = '" + ChildID + "'"
            _SQL += " and key_type = '" + KeyType + "'"

            Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
            If _DR IsNot Nothing Then
                Try
                    _Return = CType(_DR.Item("data"), Byte())
                Catch ex As Exception

                End Try
            End If

            Return _Return

        End Function

        Private Function ReturnKeyWorker(ByVal ChildID As String) As String

            Dim _Return As String = ""
            Dim _Child As Business.Child = Business.Child.RetreiveByID(New Guid(ChildID))
            If _Child IsNot Nothing Then
                _Return = _Child._KeyworkerName
                _Child = Nothing
            End If

            Return _Return

        End Function

        Private Function ReturnDrinks(ByVal TodayID As String, ByVal ChildID As String) As String

            Dim _Return As String = ""
            Dim _dt As DataTable = ReturnActivity(TodayID, ChildID, "REQ")

            If Not _dt Is Nothing Then
                For Each Row As DataRow In _dt.Rows

                Next
            End If

            Return _Return

        End Function

        Private Function ReturnPersonalComments(ByVal TodayID As String, ByVal ChildID As String) As String

            Dim _Return As String = ""
            Dim _dt As DataTable = ReturnActivity(TodayID, ChildID, "FEEDBACK")

            If Not _dt Is Nothing Then
                For Each Row As DataRow In _dt.Rows
                    _Return += Row.Item("notes").ToString
                Next
            End If

            Return _Return

        End Function

        Private Function ReturnIncident(ByVal TodayID As String, ByVal ChildID As String) As String

            Dim _SQL As String = "select count(*) from Incidents" &
                                 " where day_id = '" & TodayID & "'" &
                                 " and child_id = '" & ChildID & "'"

            Dim _Count As Integer = 0
            Dim _Return As String = ""

            Dim _o As Object = DAL.ReturnScalar(Session.ConnectionString, _SQL)
            If _o IsNot Nothing Then

                Try
                    _Count = CInt(_o)

                Catch ex As Exception

                End Try

            End If

            Select Case _Count

                Case 0
                    _Return = ""

                Case 1
                    _Return = "An incident occurred today - please see the Incident Report for more information."

                Case Else
                    _Return = _Count.ToString + " incidents occurred today - please see the Incident Report for more information."

            End Select

            Return _Return

        End Function

        Private Function ReturnMedication(ByVal TodayID As String, ByVal ChildID As String) As String

            '5ML of Calpol @ 22:30 by Cameron Diaz

            Dim _SQL As String = "select dosage, medicine, staff_name, actual from MedicineLog" &
                                 " where day_id = '" & TodayID & "'" &
                                 " and child_id = '" & ChildID & "'" &
                                 " and actual is not null" &
                                 " order by actual"

            Dim _Return As String = ""
            Dim _dt As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)

            If Not _dt Is Nothing Then
                For Each Row As DataRow In _dt.Rows
                    _Return += Row.Item("dosage").ToString + " of " + Row.Item("medicine").ToString + " @ " + Format(Row.Item("actual"), "HH:mm") + " by " + Row.Item("staff_name").ToString + vbNewLine
                Next
            End If

            Return _Return

        End Function

        Private Function ReturnMilk(ByVal TodayID As String, ByVal ChildID As String, ByVal ChildMilk As Boolean) As String

            Dim _Milk As String = ""
            Dim _dt As DataTable = ReturnActivity(TodayID, ChildID, "MILK")

            If Not _dt Is Nothing Then
                For Each Row As DataRow In _dt.Rows
                    _Milk += Row.Item("description").ToString & vbCrLf
                Next
            End If

            Return _Milk

        End Function

        Private Function ReturnRequests(ByVal TodayID As String, ByVal ChildID As String) As String

            Dim _Req As String = ""
            Dim _dt As DataTable = ReturnActivity(TodayID, ChildID, "REQ")

            If Not _dt Is Nothing Then
                For Each Row As DataRow In _dt.Rows
                    _Req += ReturnRequestText(Row.Item("value_1").ToString) & vbCrLf
                Next
            End If

            Return _Req

        End Function

        Private Function ReturnSuncream(ByVal TodayID As String, ByVal ChildID As String) As String

            Dim _Suncream As String = ""
            Dim _dt As DataTable = ReturnActivity(TodayID, ChildID, "SUNCREAM")

            If Not _dt Is Nothing Then
                For Each Row As DataRow In _dt.Rows
                    _Suncream += Row.Item("description").ToString & vbCrLf
                Next
            End If

            Return _Suncream

        End Function

        Private Function ReturnToothbrushing(ByVal TodayID As String, ByVal ChildID As String) As String

            Dim _Toothbrushing As String = ""
            Dim _dt As DataTable = ReturnActivity(TodayID, ChildID, "TOOTHBRUSH")

            If Not _dt Is Nothing Then
                For Each Row As DataRow In _dt.Rows
                    _Toothbrushing += Row.Item("description").ToString & vbCrLf
                Next
            End If

            Return _Toothbrushing

        End Function

        Private Function ReturnReportRecord(ByVal TodayID As String, ByVal ReportDate As Date, ByRef ChildRecord As Business.Child,
                                                   ByVal ReportDetail As ReportDetail, ByVal Message As String) As DailyReportData

            If ChildRecord Is Nothing Then Return Nothing

            Return ReturnDailyReportObject(TodayID, ReportDate, ReportDetail,
                                              ChildRecord._ID.ToString, ChildRecord._Fullname.ToString, ChildRecord._GroupId.ToString,
                                              ChildRecord._Nappies, ChildRecord._Milk, ChildRecord._BabyFood, ChildRecord._OffMenu, ChildRecord._DietRestrict, Message)

        End Function

        Private Function ReturnActivity(ByVal TodayID As String, ByVal ChildID As String, ByVal ActivityType As String,
                                               Optional ByVal Value1 As String = "") As DataTable

            Dim _SQL As String = "select description, value_1, value_2, value_3, notes, stamp from Activity" &
                                 " where day_id = '" & TodayID & "'" &
                                 " and key_id = '" & ChildID & "'" &
                                 " and type = '" & ActivityType & "'"

            If Value1 <> "" Then
                _SQL += " and value_1 = '" + Value1 + "'"
            End If

            _SQL += " order by stamp"

            Return DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)

        End Function

        Private Function ReturnActivities(ByVal TodayID As String, ByVal GroupID As String) As String

            Dim _Return As String = ""
            Dim _SQL As String = "select activity_text from DayActivity" &
                                 " where day_id = '" & TodayID & "'" &
                                 " and group_id = '" & GroupID & "'"

            Dim _dr As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
            If Not _dr Is Nothing Then
                _Return = _dr.Item("activity_text").ToString
            End If

            _dr = Nothing
            Return _Return

        End Function

        Private Function ReturnRequestText(ByVal RequestID As String) As String

            Dim _Return As String = ""
            Dim _SQL As String = "select report_text from Requests" &
                                 " where id = '" & RequestID & "'"

            Dim _dr As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
            If Not _dr Is Nothing Then
                _Return = _dr.Item("report_text").ToString
            End If

            _dr = Nothing
            Return _Return

        End Function

        Private Function ReturnToilet(ByVal TodayID As String, ByVal ChildID As String) As String

            Dim _Return As String = ""

            Dim _Dry As Integer = 0
            Dim _Wet As Integer = 0
            Dim _Soiled As Integer = 0
            Dim _Loose As Integer = 0
            Dim _Potty As Integer = 0

            Dim _dt As DataTable

            _dt = ReturnActivity(TodayID, ChildID, "TOILET", "DRY")
            If Not _dt Is Nothing Then _Dry = _dt.Rows.Count

            _dt = ReturnActivity(TodayID, ChildID, "TOILET", "WET")
            If Not _dt Is Nothing Then _Wet = _dt.Rows.Count

            _dt = ReturnActivity(TodayID, ChildID, "TOILET", "SOIL")
            If Not _dt Is Nothing Then _Soiled = _dt.Rows.Count

            _dt = ReturnActivity(TodayID, ChildID, "TOILET", "LOOSE")
            If Not _dt Is Nothing Then _Loose = _dt.Rows.Count

            _dt = ReturnActivity(TodayID, ChildID, "TOILET", "POTTY")
            If Not _dt Is Nothing Then _Potty = _dt.Rows.Count

            _Return = _Dry.ToString & " x Dry" & vbCrLf &
                      _Wet.ToString & " x Wet" & vbCrLf &
                      _Soiled.ToString & " x Soiled" & vbCrLf &
                      _Loose.ToString & " x Loose" & vbCrLf &
                      _Potty.ToString & " x Potty"

            Return _Return

        End Function

        Private Function ReturnToiletDetailed(ByVal TodayID As String, ByVal ChildID As String) As String

            Dim _Return As String = ""
            Dim _dt As DataTable
            _dt = ReturnActivity(TodayID, ChildID, "TOILET")
            If Not _dt Is Nothing Then
                Dim _i As Integer = 0
                For Each _DR As DataRow In _dt.Rows
                    If _i = 0 Then
                        _Return = _DR.Item("description").ToString
                    Else
                        _Return = _Return + vbNewLine + _DR.Item("description").ToString
                    End If
                    _i += 1
                Next
            End If

            Return _Return

        End Function

        Private Function ReturnSleep(ByVal TodayID As String, ByVal ChildID As String) As String

            Dim _Sleep As String = ""
            Dim _dt As DataTable = ReturnActivity(TodayID, ChildID, "SLEEP")

            If Not _dt Is Nothing Then
                For Each Row As DataRow In _dt.Rows
                    _Sleep += Row.Item("value_1").ToString & vbCrLf
                Next
            End If

            Return _Sleep

        End Function

        Private Function ReturnFood(ByVal TodayID As String, ByVal ChildID As String,
                                           ByVal BabyFood As Boolean, ByVal OffMenu As Boolean, ByVal DetailLevel As ReportDetail, ByVal DietRestriction As String) As String

            If BabyFood Then
                Return ReturnBabyFood(TodayID, ChildID)
            Else

                Dim _Breakfast As String = ""
                Dim _Snack As String = ""
                Dim _Lunch As String = ""
                Dim _LunchDessert As String = ""
                Dim _SnackPM As String = ""
                Dim _Tea As String = ""
                Dim _TeaDessert As String = ""

                If DetailLevel = ReportDetail.Basic Then
                    _Breakfast = ReturnFoodSummary(TodayID, ChildID, "B", OffMenu, DietRestriction)
                    _Snack = ReturnFoodSummary(TodayID, ChildID, "S", OffMenu, DietRestriction)
                    _Lunch = ReturnFoodSummary(TodayID, ChildID, "L", OffMenu, DietRestriction)
                    _LunchDessert = ReturnFoodSummary(TodayID, ChildID, "LD", OffMenu, DietRestriction)
                    _SnackPM = ReturnFoodSummary(TodayID, ChildID, "SP", OffMenu, DietRestriction)
                    _Tea = ReturnFoodSummary(TodayID, ChildID, "T", OffMenu, DietRestriction)
                    _TeaDessert = ReturnFoodSummary(TodayID, ChildID, "TD", OffMenu, DietRestriction)
                Else
                    _Breakfast = ReturnFoodDetail(TodayID, ChildID, "B", DetailLevel, OffMenu, DietRestriction)
                    _Snack = ReturnFoodDetail(TodayID, ChildID, "S", DetailLevel, OffMenu, DietRestriction)
                    _Lunch = ReturnFoodDetail(TodayID, ChildID, "L", DetailLevel, OffMenu, DietRestriction)
                    _LunchDessert = ReturnFoodDetail(TodayID, ChildID, "LD", DetailLevel, OffMenu, DietRestriction)
                    _SnackPM = ReturnFoodDetail(TodayID, ChildID, "SP", DetailLevel, OffMenu, DietRestriction)
                    _Tea = ReturnFoodDetail(TodayID, ChildID, "T", DetailLevel, OffMenu, DietRestriction)
                    _TeaDessert = ReturnFoodDetail(TodayID, ChildID, "TD", DetailLevel, OffMenu, DietRestriction)
                End If

                Dim _Return As String = ""
                If _Breakfast <> "" Then _Return += _Breakfast + vbCrLf
                If _Snack <> "" Then _Return += _Snack + vbCrLf
                If _Lunch <> "" Then _Return += _Lunch + vbCrLf
                If _LunchDessert <> "" Then _Return += _LunchDessert + vbCrLf
                If _SnackPM <> "" Then _Return += _SnackPM + vbCrLf
                If _Tea <> "" Then _Return += _Tea + vbCrLf
                If _TeaDessert <> "" Then _Return += _TeaDessert

                Return _Return

            End If

        End Function

        Private Function ReturnBabyFood(ByVal TodayID As String, ByVal ChildID As String) As String

            Dim _Return As String = ""

            Dim _SQL As String = "select food_name, food_status" &
                                 " from FoodRegister" &
                                 " left join Food on Food.ID = FoodRegister.food_id" &
                                 " left join AppListItems on AppListItems.ID = Food.group_id" &
                                 " where day_id = '" & TodayID & "'" &
                                 " and child_id = '" & ChildID & "'" &
                                 " and meal_type = 'J'" &
                                 " order by AppListItems.seq, food_name"

            Dim _dt As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If Not _dt Is Nothing Then

                For Each Row As DataRow In _dt.Rows
                    _Return += Row("food_name").ToString & ": " & ReturnEaten(CInt(Row("food_status"))) & vbCrLf
                Next

                _dt.Dispose()
                _dt = Nothing

            End If

            Return _Return

        End Function

        Private Function ReturnFoodDetail(ByVal TodayID As String, ByVal ChildID As String,
                                                 ByVal MealType As String, ByVal DetailLevel As ReportDetail, ByVal OffMenu As Boolean, ByVal DietRestriction As String) As String

            Dim _Return As String = ""

            Dim _SQL As String = "select Food.name, FoodRegister.meal_name, nut_kj, nut_kcal, nut_prot, nut_carb, nut_fat, nut_fibre, nut_salt, ingredients, food_status" &
                     " from FoodRegister" &
                     " left join Food on Food.ID = FoodRegister.food_id" &
                     " left join AppListItems FoodGroups on FoodGroups.ID = Food.group_id" &
                     " where day_id = '" & TodayID & "'" &
                     " and child_id = '" & ChildID & "'" &
                     " and meal_type = '" & MealType & "'" &
                     " order by FoodGroups.seq, Food.name"

            Dim _dt As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If Not _dt Is Nothing Then

                If _dt.Rows.Count > 0 Then

                    Dim _Nutrition As String = ""
                    Dim _MealType As String = ReturnMealType(MealType, DietRestriction)
                    Dim _MealName As String = ReturnMealName(TodayID, MealType, OffMenu)

                    _Return = _MealType & ": " & _MealName & vbCrLf
                    If DetailLevel = ReportDetail.HighDetail Then _Return += vbCrLf

                    For Each Row As DataRow In _dt.Rows

                        If Row("food_status").ToString <> "X" Then

                            If CInt(Row("food_status")) >= 0 Then
                                _Return += Row("name").ToString & ": " & ReturnEaten(CInt(Row("food_status"))) & vbCrLf

                                If DetailLevel = ReportDetail.HighDetail Then

                                    _Nutrition = ReturnNutrition(ValueHandler.ConvertDecimal(Row("nut_kj")), ValueHandler.ConvertDecimal(Row("nut_kcal")), ValueHandler.ConvertDecimal(Row("nut_prot")),
                                        ValueHandler.ConvertDecimal(Row("nut_carb")), ValueHandler.ConvertDecimal(Row("nut_fat")), ValueHandler.ConvertDecimal(Row("nut_fibre")),
                                        ValueHandler.ConvertDecimal(Row("nut_salt")), Row("ingredients").ToString)

                                    If _Nutrition <> "" Then
                                        _Return += _Nutrition
                                        _Return += vbCrLf + vbCrLf
                                    End If

                                End If
                            End If
                        End If
                    Next

                End If

                _dt.Dispose()
                _dt = Nothing

            End If

            Return _Return

        End Function

        Private Function ReturnNutrition(ByVal kj As Decimal, ByVal kcal As Decimal, ByVal Protein As Decimal, ByVal Carbs As Decimal,
                                                ByVal Fat As Decimal, ByVal Fibre As Decimal, ByVal Salt As Decimal, ByVal Ingredients As String) As String

            Dim _Nutrition As String = ""

            'check there are nutritional values stored
            Dim _Total As Decimal = kj + kcal + Protein + Carbs + Fat + Fibre + Salt
            If _Total > 0 Then

                _Nutrition = "Nutrition Information per 100g: "
                If kcal > 0 Or kj > 0 Then
                    _Nutrition += "Energy "
                    If kj > 0 Then _Nutrition += kj & "kj"
                    If kcal > 0 Then
                        If kj > 0 Then _Nutrition += "/"
                        _Nutrition += kcal & "kcal"
                    End If
                End If

                If Protein > 0 Then _Nutrition += ", Protein " & Protein
                If Carbs > 0 Then _Nutrition += ", Carbohydrate " & Carbs
                If Fat > 0 Then _Nutrition += ", Fat " & Fat
                If Fibre > 0 Then _Nutrition += ", Fibre " & Fibre
                If Salt > 0 Then _Nutrition += ", Salt " & Salt

            End If

            If Ingredients <> "" Then
                If _Nutrition <> "" Then _Nutrition += vbCrLf
                _Nutrition += "Ingredients: " & Ingredients
            End If

            Return _Nutrition

        End Function

        Private Function ReturnFoodSummary(ByVal TodayID As String, ByVal ChildID As String, ByVal MealType As String, ByVal OffMenu As Boolean, ByVal DietRestriction As String) As String

            Dim _MealID As String = ""
            Dim _MealPercentage As Decimal = 0

            Dim _SQL As String = "select meal_id, cast(food_status as int) 'status'" &
                                 " from FoodRegister" &
                                 " where day_id = '" & TodayID & "'" &
                                 " and child_id = '" & ChildID & "'" &
                                 " and meal_type = '" & MealType & "'"

            Dim _dt As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If Not _dt Is Nothing Then

                If _dt.Rows.Count > 0 Then

                    Dim _Items As Integer = _dt.Rows.Count
                    Dim _ScoreAccumulated As Decimal = 0

                    For Each _DR As DataRow In _dt.Rows
                        _MealID = _DR("meal_id").ToString
                        _ScoreAccumulated += ReturnPercentageFromScore(CInt(_DR("status")))
                    Next

                    _MealPercentage = _ScoreAccumulated / _Items

                End If

                _dt.Dispose()
                _dt = Nothing

            End If

            Return ReturnMealType(MealType, DietRestriction) + ": " + ReturnMealReportDescription(_MealID, OffMenu) & vbCrLf & ReturnEaten(_MealPercentage) & vbCrLf

        End Function

        Private Function ReturnMealName(ByVal TodayID As String, ByVal MealType As String, ByVal OffMenu As Boolean) As String

            Dim _Return As String = ""

            'fetch the MEAL ID from the today file
            Dim _SQL As String = "select breakfast_id, snack_id, lunch_id, lunch_des_id, snack_pm_id, tea_id, tea_des_id " &
                                 " from Day" &
                                 " where id = '" & TodayID & "'"

            Dim _dr As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
            If _dr IsNot Nothing Then

                If MealType = "B" Then _Return = ReturnMealReportDescription(_dr.Item("breakfast_id").ToString, OffMenu)
                If MealType = "S" Then _Return = ReturnMealReportDescription(_dr.Item("snack_id").ToString, OffMenu)
                If MealType = "L" Then _Return = ReturnMealReportDescription(_dr.Item("lunch_id").ToString, OffMenu)
                If MealType = "LD" Then _Return = ReturnMealReportDescription(_dr.Item("lunch_des_id").ToString, OffMenu)
                If MealType = "SP" Then _Return = ReturnMealReportDescription(_dr.Item("snack_pm_id").ToString, OffMenu)
                If MealType = "T" Then _Return = ReturnMealReportDescription(_dr.Item("tea_id").ToString, OffMenu)
                If MealType = "TD" Then _Return = ReturnMealReportDescription(_dr.Item("tea_des_id").ToString, OffMenu)

                _dr = Nothing

            End If

            Return _Return

        End Function

        Private Function ReturnMealReportDescription(ByVal MealID As String, ByVal OffMenu As Boolean) As String

            If OffMenu Then Return ""
            If MealID = "" Then Return ""

            Dim _Return As String = ""
            Dim _SQL As String = "select description from Meals where ID = '" & MealID & "'"

            Dim _dr As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
            If _dr IsNot Nothing Then
                _Return = _dr.Item("description").ToString
                _dr = Nothing
            End If

            Return _Return

        End Function

        Private Function ReturnMealType(ByVal MealType As String, ByVal DietRestriction As String) As String

            Dim _Return As String = ""
            Dim _Name As String = ""

            If MealType = "B" Then _Name = "Breakfast"
            If MealType = "S" Then _Name = "Morning Snack"
            If MealType = "L" Then _Name = "Lunch"
            If MealType = "LD" Then _Name = "Lunch Dessert"
            If MealType = "SP" Then _Name = "Afternoon Snack"
            If MealType = "T" Then _Name = "Tea"
            If MealType = "TD" Then _Name = "Tea Dessert"

            Select Case DietRestriction

                Case "None", "Nothing", ""
                    _Return = _Name

                Case Else
                    _Return = DietRestriction + " " + _Name

            End Select

            Return _Return

        End Function

        Private Function ReturnEaten(ByVal MealStatus As Integer) As String
            If MealStatus = 0 Then Return "None of it"
            If MealStatus = 1 Then Return "Some of it"
            If MealStatus = 2 Then Return "Most of it"
            If MealStatus = 3 Then Return "All of it"
            If MealStatus = 4 Then Return "Half of it"
            Return ""
        End Function

        Private Function ReturnPercentageFromScore(ByVal MealStatus As Integer) As Decimal
            If MealStatus = 0 Then Return 0
            If MealStatus = 1 Then Return 25
            If MealStatus = 2 Then Return 75
            If MealStatus = 3 Then Return 100
            If MealStatus = 4 Then Return 50
            Return 0
        End Function

        Private Function ReturnEaten(ByVal Percentage As Decimal) As String
            If Percentage = 100 Then Return "All of it"
            If Percentage >= 75 Then Return "Most of it"
            If Percentage >= 50 Then Return "Half of it"
            If Percentage >= 25 Then Return "Some of it"
            Return "None of it"
        End Function

    End Class

End Namespace
