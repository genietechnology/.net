﻿
Imports Care.Global

Namespace Reports

    Public Interface IInvoiceDocument

        Property _InvoiceID As Guid
        Property _InvoiceNo As String
        Property _AccountNo As String
        Property _InvoiceLayout As String

        Property _InvoiceDate As Date

        Property _InvoiceDueDate As Date
        Property _InvoiceDueText As String

        Property _InvoicePeriod As Date
        Property _InvoicePeriodEnd As Date

        Property _InvoiceTo As String
        Property _AddressBlock As String
        Property _ChildName As String

        Property _Narrative As String

        Property _DiscountName As String

        Property _InvoiceSub As Decimal
        Property _InvoiceDiscount As Decimal
        Property _InvoiceTotal As Decimal

        Property _VoucherMode As String
        Property _VoucherProportion As Decimal
        Property _VoucherPool As Decimal
        Property _VoucherAmount As Decimal

        Property _DepositBalance As Decimal
        Property _DepositThisTaken As Decimal
        Property _DepositThisReturn As Decimal

        'these properties may seem a little out of place, but are required to send data to the sub-report
        'the sub-report is the summary section on the standard invoice
        Property _LineDate As Date?
        Property _LineText As String
        Property _LineRate As Decimal
        Property _LineSection As String
        Property _LineHours As Decimal

    End Interface

    Public Class InvoiceData

        Implements IInvoiceDocument

        Public Property _InvoiceID As Guid Implements IInvoiceDocument._InvoiceID
        Public Property _AccountNo As String Implements IInvoiceDocument._AccountNo
        Public Property _AddressBlock As String Implements IInvoiceDocument._AddressBlock
        Public Property _ChildName As String Implements IInvoiceDocument._ChildName
        Public Property _DiscountName As String Implements IInvoiceDocument._DiscountName
        Public Property _InvoiceDate As Date Implements IInvoiceDocument._InvoiceDate
        Public Property _InvoiceDiscount As Decimal Implements IInvoiceDocument._InvoiceDiscount
        Public Property _InvoiceDueDate As Date Implements IInvoiceDocument._InvoiceDueDate
        Public Property _InvoiceDueText As String Implements IInvoiceDocument._InvoiceDueText
        Public Property _InvoiceLayout As String Implements IInvoiceDocument._InvoiceLayout
        Public Property _InvoiceNo As String Implements IInvoiceDocument._InvoiceNo
        Public Property _InvoicePeriod As Date Implements IInvoiceDocument._InvoicePeriod
        Public Property _InvoicePeriodEnd As Date Implements IInvoiceDocument._InvoicePeriodEnd
        Public Property _InvoiceSub As Decimal Implements IInvoiceDocument._InvoiceSub
        Public Property _InvoiceTo As String Implements IInvoiceDocument._InvoiceTo
        Public Property _InvoiceTotal As Decimal Implements IInvoiceDocument._InvoiceTotal
        Public Property _Narrative As String Implements IInvoiceDocument._Narrative

        Public Property _VoucherMode As String Implements IInvoiceDocument._VoucherMode
        Public Property _VoucherProportion As Decimal Implements IInvoiceDocument._VoucherProportion
        Public Property _VoucherPool As Decimal Implements IInvoiceDocument._VoucherPool
        Public Property _VoucherAmount As Decimal Implements IInvoiceDocument._VoucherAmount

        Public Property _LineDate As Date? Implements IInvoiceDocument._LineDate
        Public Property _LineText As String Implements IInvoiceDocument._LineText
        Public Property _LineRate As Decimal Implements IInvoiceDocument._LineRate
        Public Property _LineSection As String Implements IInvoiceDocument._LineSection
        Public Property _LineHours As Decimal Implements IInvoiceDocument._LineHours

        Public Property _DepositBalance As Decimal Implements IInvoiceDocument._DepositBalance
        Public Property _DepositThisTaken As Decimal Implements IInvoiceDocument._DepositThisTaken
        Public Property _DepositThisReturn As Decimal Implements IInvoiceDocument._DepositThisReturn

    End Class

    Public Class KLNInvoiceData

        Implements IInvoiceDocument

        Public Property _InvoiceID As Guid Implements IInvoiceDocument._InvoiceID
        Public Property _AccountNo As String Implements IInvoiceDocument._AccountNo
        Public Property _AddressBlock As String Implements IInvoiceDocument._AddressBlock
        Public Property _ChildName As String Implements IInvoiceDocument._ChildName
        Public Property _DiscountName As String Implements IInvoiceDocument._DiscountName
        Public Property _InvoiceDate As Date Implements IInvoiceDocument._InvoiceDate
        Public Property _InvoiceDiscount As Decimal Implements IInvoiceDocument._InvoiceDiscount
        Public Property _InvoiceDueDate As Date Implements IInvoiceDocument._InvoiceDueDate
        Public Property _InvoiceDueText As String Implements IInvoiceDocument._InvoiceDueText
        Public Property _InvoiceLayout As String Implements IInvoiceDocument._InvoiceLayout
        Public Property _InvoiceNo As String Implements IInvoiceDocument._InvoiceNo
        Public Property _InvoicePeriod As Date Implements IInvoiceDocument._InvoicePeriod
        Public Property _InvoicePeriodEnd As Date Implements IInvoiceDocument._InvoicePeriodEnd
        Public Property _InvoiceSub As Decimal Implements IInvoiceDocument._InvoiceSub
        Public Property _InvoiceTo As String Implements IInvoiceDocument._InvoiceTo
        Public Property _InvoiceTotal As Decimal Implements IInvoiceDocument._InvoiceTotal
        Public Property _Narrative As String Implements IInvoiceDocument._Narrative

        Public Property _LineDate As Date? Implements IInvoiceDocument._LineDate
        Public Property _LineText As String Implements IInvoiceDocument._LineText
        Public Property _LineRate As Decimal Implements IInvoiceDocument._LineRate
        Public Property _LineSection As String Implements IInvoiceDocument._LineSection
        Public Property _LineHours As Decimal Implements IInvoiceDocument._LineHours

        Public Property _ChargeBlock As String

        Public Property _VouchPriName As String
        Public Property _VouchPriAmount As Decimal
        Public Property _VouchOtherName As String
        Public Property _VouchOtherAmount As Decimal
        Public Property _VouchTotal As Decimal

        Public Property MonCount As Integer
        Public Property MonFunded As Decimal
        Public Property MonNonFundedHours As Decimal
        Public Property MonNonFundedCost As Decimal
        Public Property MonLunchCount As Integer
        Public Property MonLunchCost As Decimal
        Public Property MonCommCount As Integer
        Public Property MonCommCost As Decimal
        Public Property MonAddlCount As Integer
        Public Property MonAddlCost As Decimal
        Public Property MonTotalCost As Decimal

        Public Property TueCount As Integer
        Public Property TueFunded As Decimal
        Public Property TueNonFundedHours As Decimal
        Public Property TueNonFundedCost As Decimal
        Public Property TueLunchCount As Integer
        Public Property TueLunchCost As Decimal
        Public Property TueCommCount As Integer
        Public Property TueCommCost As Decimal
        Public Property TueAddlCount As Integer
        Public Property TueAddlCost As Decimal
        Public Property TueTotalCost As Decimal

        Public Property WedCount As Integer
        Public Property WedFunded As Decimal
        Public Property WedNonFundedHours As Decimal
        Public Property WedNonFundedCost As Decimal
        Public Property WedLunchCount As Integer
        Public Property WedLunchCost As Decimal
        Public Property WedCommCount As Integer
        Public Property WedCommCost As Decimal
        Public Property WedAddlCount As Integer
        Public Property WedAddlCost As Decimal
        Public Property WedTotalCost As Decimal

        Public Property ThuCount As Integer
        Public Property ThuFunded As Decimal
        Public Property ThuNonFundedHours As Decimal
        Public Property ThuNonFundedCost As Decimal
        Public Property ThuLunchCount As Integer
        Public Property ThuLunchCost As Decimal
        Public Property ThuCommCount As Integer
        Public Property ThuCommCost As Decimal
        Public Property ThuAddlCount As Integer
        Public Property ThuAddlCost As Decimal
        Public Property ThuTotalCost As Decimal

        Public Property FriCount As Integer
        Public Property FriFunded As Decimal
        Public Property FriNonFundedHours As Decimal
        Public Property FriNonFundedCost As Decimal
        Public Property FriLunchCount As Integer
        Public Property FriLunchCost As Decimal
        Public Property FriCommCount As Integer
        Public Property FriCommCost As Decimal
        Public Property FriAddlCount As Integer
        Public Property FriAddlCost As Decimal
        Public Property FriTotalCost As Decimal

        Public Property TotalCharges As Decimal
        Public Property TotalGrand As Decimal
        Public Property TotalNetVouchers As Decimal

        Public Property _VoucherMode As String Implements IInvoiceDocument._VoucherMode
        Public Property _VoucherProportion As Decimal Implements IInvoiceDocument._VoucherProportion
        Public Property _VoucherPool As Decimal Implements IInvoiceDocument._VoucherPool
        Public Property _VoucherAmount As Decimal Implements IInvoiceDocument._VoucherAmount

        Public Property _DepositBalance As Decimal Implements IInvoiceDocument._DepositBalance
        Public Property _DepositThisTaken As Decimal Implements IInvoiceDocument._DepositThisTaken
        Public Property _DepositThisReturn As Decimal Implements IInvoiceDocument._DepositThisReturn

        '**********************************************************************************

    End Class

    Public Class SummaryColumnInvoiceData

        Implements IInvoiceDocument

        Public Function Clone() As SummaryColumnInvoiceData
            Return DirectCast(Me.MemberwiseClone, SummaryColumnInvoiceData)
        End Function

        Public Property _InvoiceID As Guid Implements IInvoiceDocument._InvoiceID
        Public Property _AccountNo As String Implements IInvoiceDocument._AccountNo
        Public Property _AddressBlock As String Implements IInvoiceDocument._AddressBlock
        Public Property _ChildName As String Implements IInvoiceDocument._ChildName
        Public Property _DiscountName As String Implements IInvoiceDocument._DiscountName
        Public Property _InvoiceDate As Date Implements IInvoiceDocument._InvoiceDate
        Public Property _InvoiceDiscount As Decimal Implements IInvoiceDocument._InvoiceDiscount
        Public Property _InvoiceDueDate As Date Implements IInvoiceDocument._InvoiceDueDate
        Public Property _InvoiceDueText As String Implements IInvoiceDocument._InvoiceDueText
        Public Property _InvoiceLayout As String Implements IInvoiceDocument._InvoiceLayout
        Public Property _InvoiceNo As String Implements IInvoiceDocument._InvoiceNo
        Public Property _InvoicePeriod As Date Implements IInvoiceDocument._InvoicePeriod
        Public Property _InvoicePeriodEnd As Date Implements IInvoiceDocument._InvoicePeriodEnd
        Public Property _InvoiceSub As Decimal Implements IInvoiceDocument._InvoiceSub
        Public Property _InvoiceTo As String Implements IInvoiceDocument._InvoiceTo
        Public Property _InvoiceTotal As Decimal Implements IInvoiceDocument._InvoiceTotal
        Public Property _Narrative As String Implements IInvoiceDocument._Narrative

        Public Property _VoucherMode As String Implements IInvoiceDocument._VoucherMode
        Public Property _VoucherProportion As Decimal Implements IInvoiceDocument._VoucherProportion
        Public Property _VoucherPool As Decimal Implements IInvoiceDocument._VoucherPool
        Public Property _VoucherAmount As Decimal Implements IInvoiceDocument._VoucherAmount

        Public Property _LineDate As Date? Implements IInvoiceDocument._LineDate
        Public Property _LineText As String Implements IInvoiceDocument._LineText
        Public Property _LineRate As Decimal Implements IInvoiceDocument._LineRate
        Public Property _LineSection As String Implements IInvoiceDocument._LineSection
        Public Property _LineHours As Decimal Implements IInvoiceDocument._LineHours

        '**********************************************************************************

        Public Property _Mon0Count As Integer
        Public Property _Mon0Hours As Decimal
        Public Property _Mon0Cost As Decimal

        Public Property _Mon1Count As Integer
        Public Property _Mon1Hours As Decimal
        Public Property _Mon1Cost As Decimal

        Public Property _Mon2Count As Integer
        Public Property _Mon2Hours As Decimal
        Public Property _Mon2Cost As Decimal

        Public Property _Mon3Count As Integer
        Public Property _Mon3Hours As Decimal
        Public Property _Mon3Cost As Decimal

        Public Property _Mon4Count As Integer
        Public Property _Mon4Hours As Decimal
        Public Property _Mon4Cost As Decimal

        Public Property _Mon5Count As Integer
        Public Property _Mon5Hours As Decimal
        Public Property _Mon5Cost As Decimal

        Public Property _Mon6Count As Integer
        Public Property _Mon6Hours As Decimal
        Public Property _Mon6Cost As Decimal

        Public Property _Mon7Count As Integer
        Public Property _Mon7Hours As Decimal
        Public Property _Mon7Cost As Decimal

        Public Property _Mon8Count As Integer
        Public Property _Mon8Hours As Decimal
        Public Property _Mon8Cost As Decimal

        Public Property _Mon9Count As Integer
        Public Property _Mon9Hours As Decimal
        Public Property _Mon9Cost As Decimal

        Public Property _MonTotalCount As Integer
        Public Property _MonTotalHours As Decimal
        Public Property _MonTotalCost As Decimal

        '**********************************************************************************

        Public Property _Tue0Count As Integer
        Public Property _Tue0Hours As Decimal
        Public Property _Tue0Cost As Decimal

        Public Property _Tue1Count As Integer
        Public Property _Tue1Hours As Decimal
        Public Property _Tue1Cost As Decimal

        Public Property _Tue2Count As Integer
        Public Property _Tue2Hours As Decimal
        Public Property _Tue2Cost As Decimal

        Public Property _Tue3Count As Integer
        Public Property _Tue3Hours As Decimal
        Public Property _Tue3Cost As Decimal

        Public Property _Tue4Count As Integer
        Public Property _Tue4Hours As Decimal
        Public Property _Tue4Cost As Decimal

        Public Property _Tue5Count As Integer
        Public Property _Tue5Hours As Decimal
        Public Property _Tue5Cost As Decimal

        Public Property _Tue6Count As Integer
        Public Property _Tue6Hours As Decimal
        Public Property _Tue6Cost As Decimal

        Public Property _Tue7Count As Integer
        Public Property _Tue7Hours As Decimal
        Public Property _Tue7Cost As Decimal

        Public Property _Tue8Count As Integer
        Public Property _Tue8Hours As Decimal
        Public Property _Tue8Cost As Decimal

        Public Property _Tue9Count As Integer
        Public Property _Tue9Hours As Decimal
        Public Property _Tue9Cost As Decimal

        Public Property _TueTotalCount As Integer
        Public Property _TueTotalHours As Decimal
        Public Property _TueTotalCost As Decimal

        '**********************************************************************************

        Public Property _Wed0Count As Integer
        Public Property _Wed0Hours As Decimal
        Public Property _Wed0Cost As Decimal

        Public Property _Wed1Count As Integer
        Public Property _Wed1Hours As Decimal
        Public Property _Wed1Cost As Decimal

        Public Property _Wed2Count As Integer
        Public Property _Wed2Hours As Decimal
        Public Property _Wed2Cost As Decimal

        Public Property _Wed3Count As Integer
        Public Property _Wed3Hours As Decimal
        Public Property _Wed3Cost As Decimal

        Public Property _Wed4Count As Integer
        Public Property _Wed4Hours As Decimal
        Public Property _Wed4Cost As Decimal

        Public Property _Wed5Count As Integer
        Public Property _Wed5Hours As Decimal
        Public Property _Wed5Cost As Decimal

        Public Property _Wed6Count As Integer
        Public Property _Wed6Hours As Decimal
        Public Property _Wed6Cost As Decimal

        Public Property _Wed7Count As Integer
        Public Property _Wed7Hours As Decimal
        Public Property _Wed7Cost As Decimal

        Public Property _Wed8Count As Integer
        Public Property _Wed8Hours As Decimal
        Public Property _Wed8Cost As Decimal

        Public Property _Wed9Count As Integer
        Public Property _Wed9Hours As Decimal
        Public Property _Wed9Cost As Decimal

        Public Property _WedTotalCount As Integer
        Public Property _WedTotalHours As Decimal
        Public Property _WedTotalCost As Decimal

        '**********************************************************************************

        Public Property _Thu0Count As Integer
        Public Property _Thu0Hours As Decimal
        Public Property _Thu0Cost As Decimal

        Public Property _Thu1Count As Integer
        Public Property _Thu1Hours As Decimal
        Public Property _Thu1Cost As Decimal

        Public Property _Thu2Count As Integer
        Public Property _Thu2Hours As Decimal
        Public Property _Thu2Cost As Decimal

        Public Property _Thu3Count As Integer
        Public Property _Thu3Hours As Decimal
        Public Property _Thu3Cost As Decimal

        Public Property _Thu4Count As Integer
        Public Property _Thu4Hours As Decimal
        Public Property _Thu4Cost As Decimal

        Public Property _Thu5Count As Integer
        Public Property _Thu5Hours As Decimal
        Public Property _Thu5Cost As Decimal

        Public Property _Thu6Count As Integer
        Public Property _Thu6Hours As Decimal
        Public Property _Thu6Cost As Decimal

        Public Property _Thu7Count As Integer
        Public Property _Thu7Hours As Decimal
        Public Property _Thu7Cost As Decimal

        Public Property _Thu8Count As Integer
        Public Property _Thu8Hours As Decimal
        Public Property _Thu8Cost As Decimal

        Public Property _Thu9Count As Integer
        Public Property _Thu9Hours As Decimal
        Public Property _Thu9Cost As Decimal

        Public Property _ThuTotalCount As Integer
        Public Property _ThuTotalHours As Decimal
        Public Property _ThuTotalCost As Decimal

        '**********************************************************************************

        Public Property _Fri0Count As Integer
        Public Property _Fri0Hours As Decimal
        Public Property _Fri0Cost As Decimal

        Public Property _Fri1Count As Integer
        Public Property _Fri1Hours As Decimal
        Public Property _Fri1Cost As Decimal

        Public Property _Fri2Count As Integer
        Public Property _Fri2Hours As Decimal
        Public Property _Fri2Cost As Decimal

        Public Property _Fri3Count As Integer
        Public Property _Fri3Hours As Decimal
        Public Property _Fri3Cost As Decimal

        Public Property _Fri4Count As Integer
        Public Property _Fri4Hours As Decimal
        Public Property _Fri4Cost As Decimal

        Public Property _Fri5Count As Integer
        Public Property _Fri5Hours As Decimal
        Public Property _Fri5Cost As Decimal

        Public Property _Fri6Count As Integer
        Public Property _Fri6Hours As Decimal
        Public Property _Fri6Cost As Decimal

        Public Property _Fri7Count As Integer
        Public Property _Fri7Hours As Decimal
        Public Property _Fri7Cost As Decimal

        Public Property _Fri8Count As Integer
        Public Property _Fri8Hours As Decimal
        Public Property _Fri8Cost As Decimal

        Public Property _Fri9Count As Integer
        Public Property _Fri9Hours As Decimal
        Public Property _Fri9Cost As Decimal

        Public Property _FriTotalCount As Integer
        Public Property _FriTotalHours As Decimal
        Public Property _FriTotalCost As Decimal

        '**********************************************************************************

        Public Property _Sat0Count As Integer
        Public Property _Sat0Hours As Decimal
        Public Property _Sat0Cost As Decimal

        Public Property _Sat1Count As Integer
        Public Property _Sat1Hours As Decimal
        Public Property _Sat1Cost As Decimal

        Public Property _Sat2Count As Integer
        Public Property _Sat2Hours As Decimal
        Public Property _Sat2Cost As Decimal

        Public Property _Sat3Count As Integer
        Public Property _Sat3Hours As Decimal
        Public Property _Sat3Cost As Decimal

        Public Property _Sat4Count As Integer
        Public Property _Sat4Hours As Decimal
        Public Property _Sat4Cost As Decimal

        Public Property _Sat5Count As Integer
        Public Property _Sat5Hours As Decimal
        Public Property _Sat5Cost As Decimal

        Public Property _Sat6Count As Integer
        Public Property _Sat6Hours As Decimal
        Public Property _Sat6Cost As Decimal

        Public Property _Sat7Count As Integer
        Public Property _Sat7Hours As Decimal
        Public Property _Sat7Cost As Decimal

        Public Property _Sat8Count As Integer
        Public Property _Sat8Hours As Decimal
        Public Property _Sat8Cost As Decimal

        Public Property _Sat9Count As Integer
        Public Property _Sat9Hours As Decimal
        Public Property _Sat9Cost As Decimal

        Public Property _SatTotalCount As Integer
        Public Property _SatTotalHours As Decimal
        Public Property _SatTotalCost As Decimal

        '**********************************************************************************

        Public Property _Sun0Count As Integer
        Public Property _Sun0Hours As Decimal
        Public Property _Sun0Cost As Decimal

        Public Property _Sun1Count As Integer
        Public Property _Sun1Hours As Decimal
        Public Property _Sun1Cost As Decimal

        Public Property _Sun2Count As Integer
        Public Property _Sun2Hours As Decimal
        Public Property _Sun2Cost As Decimal

        Public Property _Sun3Count As Integer
        Public Property _Sun3Hours As Decimal
        Public Property _Sun3Cost As Decimal

        Public Property _Sun4Count As Integer
        Public Property _Sun4Hours As Decimal
        Public Property _Sun4Cost As Decimal

        Public Property _Sun5Count As Integer
        Public Property _Sun5Hours As Decimal
        Public Property _Sun5Cost As Decimal

        Public Property _Sun6Count As Integer
        Public Property _Sun6Hours As Decimal
        Public Property _Sun6Cost As Decimal

        Public Property _Sun7Count As Integer
        Public Property _Sun7Hours As Decimal
        Public Property _Sun7Cost As Decimal

        Public Property _Sun8Count As Integer
        Public Property _Sun8Hours As Decimal
        Public Property _Sun8Cost As Decimal

        Public Property _Sun9Count As Integer
        Public Property _Sun9Hours As Decimal
        Public Property _Sun9Cost As Decimal

        Public Property _SunTotalCount As Integer
        Public Property _SunTotalHours As Decimal
        Public Property _SunTotalCost As Decimal

        '**********************************************************************************

        Public Property _DepositBalance As Decimal Implements IInvoiceDocument._DepositBalance
        Public Property _DepositThisTaken As Decimal Implements IInvoiceDocument._DepositThisTaken
        Public Property _DepositThisReturn As Decimal Implements IInvoiceDocument._DepositThisReturn

    End Class

End Namespace

