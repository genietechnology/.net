﻿
<Serializable()>
Public Class DailyReportData
    Public Property _ReportDate() As Date
    Public Property _ChildName() As String
    Public Property _Requests() As String
    Public Property _Sleep() As String
    Public Property _Nappies() As String
    Public Property _Food() As String
    Public Property _Activity() As String
    Public Property _Message() As String
    Public Property _Milk() As String
    Public Property _ToiletDetailed() As String
    Public Property _Drinks() As String
    Public Property _KeyWorker() As String
    Public Property _Medication() As String
    Public Property _PersonalComments() As String
    Public Property _Incident() As String
    Public Property _Suncream As String
    Public Property _Toothbrushing As String
    Public Property _PicPersonal As Byte()
    Public Property _PicRoom As Byte()
    Public Property _PicSite As Byte()
End Class
