﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFinancialsSync
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.btnRun = New Care.Controls.CareButton(Me.components)
        Me.GroupControl1 = New Care.Controls.CareFrame(Me.components)
        Me.btnRefresh = New Care.Controls.CareButton(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.cbxNameFormat = New Care.Controls.CareComboBox(Me.components)
        Me.cbxOption = New Care.Controls.CareComboBox(Me.components)
        Me.Label10 = New Care.Controls.CareLabel(Me.components)
        Me.cgRecords = New Care.Controls.CareGrid()
        Me.txtRed = New Care.Controls.CareTextBox(Me.components)
        Me.txtYellow = New Care.Controls.CareTextBox(Me.components)
        Me.txtBlue = New Care.Controls.CareTextBox(Me.components)
        Me.txtGreen = New Care.Controls.CareTextBox(Me.components)
        Me.txtOrange = New Care.Controls.CareTextBox(Me.components)
        Me.cbxSite = New Care.Controls.CareComboBox(Me.components)
        Me.radAll = New Care.Controls.CareRadioButton(Me.components)
        Me.radSpecific = New Care.Controls.CareRadioButton(Me.components)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.cbxNameFormat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxOption.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBlue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOrange.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radAll.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radSpecific.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(945, 480)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(115, 24)
        Me.btnClose.TabIndex = 8
        Me.btnClose.Tag = ""
        Me.btnClose.Text = "Close"
        '
        'btnRun
        '
        Me.btnRun.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRun.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRun.Appearance.Options.UseFont = True
        Me.btnRun.Location = New System.Drawing.Point(824, 480)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(115, 24)
        Me.btnRun.TabIndex = 7
        Me.btnRun.Tag = ""
        Me.btnRun.Text = "Run"
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.radAll)
        Me.GroupControl1.Controls.Add(Me.radSpecific)
        Me.GroupControl1.Controls.Add(Me.cbxSite)
        Me.GroupControl1.Controls.Add(Me.btnRefresh)
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Controls.Add(Me.cbxNameFormat)
        Me.GroupControl1.Controls.Add(Me.cbxOption)
        Me.GroupControl1.Controls.Add(Me.Label10)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 10)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(1048, 71)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Financials Integration"
        '
        'btnRefresh
        '
        Me.btnRefresh.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefresh.Appearance.Options.UseFont = True
        Me.btnRefresh.Location = New System.Drawing.Point(983, 37)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(60, 24)
        Me.btnRefresh.TabIndex = 7
        Me.btnRefresh.Tag = ""
        Me.btnRefresh.Text = "Refresh"
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel3.Appearance.Options.UseFont = True
        Me.CareLabel3.Appearance.Options.UseTextOptions = True
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(404, 42)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(91, 15)
        Me.CareLabel3.TabIndex = 5
        Me.CareLabel3.Text = "Account Naming"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxNameFormat
        '
        Me.cbxNameFormat.AllowBlank = False
        Me.cbxNameFormat.DataSource = Nothing
        Me.cbxNameFormat.DisplayMember = Nothing
        Me.cbxNameFormat.EnterMoveNextControl = True
        Me.cbxNameFormat.Location = New System.Drawing.Point(501, 39)
        Me.cbxNameFormat.Name = "cbxNameFormat"
        Me.cbxNameFormat.Properties.AccessibleName = "Status"
        Me.cbxNameFormat.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxNameFormat.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxNameFormat.Properties.Appearance.Options.UseFont = True
        Me.cbxNameFormat.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxNameFormat.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxNameFormat.SelectedValue = Nothing
        Me.cbxNameFormat.Size = New System.Drawing.Size(476, 22)
        Me.cbxNameFormat.TabIndex = 6
        Me.cbxNameFormat.Tag = ""
        Me.cbxNameFormat.ValueMember = Nothing
        '
        'cbxOption
        '
        Me.cbxOption.AllowBlank = False
        Me.cbxOption.DataSource = Nothing
        Me.cbxOption.DisplayMember = Nothing
        Me.cbxOption.EnterMoveNextControl = True
        Me.cbxOption.Location = New System.Drawing.Point(111, 39)
        Me.cbxOption.Name = "cbxOption"
        Me.cbxOption.Properties.AccessibleName = "Status"
        Me.cbxOption.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxOption.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxOption.Properties.Appearance.Options.UseFont = True
        Me.cbxOption.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxOption.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxOption.SelectedValue = Nothing
        Me.cbxOption.Size = New System.Drawing.Size(273, 22)
        Me.cbxOption.TabIndex = 3
        Me.cbxOption.Tag = ""
        Me.cbxOption.ValueMember = Nothing
        '
        'Label10
        '
        Me.Label10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Appearance.Options.UseFont = True
        Me.Label10.Appearance.Options.UseTextOptions = True
        Me.Label10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label10.Location = New System.Drawing.Point(81, 42)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(24, 15)
        Me.Label10.TabIndex = 4
        Me.Label10.Text = "Task"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cgRecords
        '
        Me.cgRecords.AllowBuildColumns = True
        Me.cgRecords.AllowEdit = False
        Me.cgRecords.AllowHorizontalScroll = False
        Me.cgRecords.AllowMultiSelect = False
        Me.cgRecords.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgRecords.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgRecords.Appearance.Options.UseFont = True
        Me.cgRecords.AutoSizeByData = True
        Me.cgRecords.DisableAutoSize = False
        Me.cgRecords.DisableDataFormatting = False
        Me.cgRecords.FocusedRowHandle = -2147483648
        Me.cgRecords.HideFirstColumn = False
        Me.cgRecords.Location = New System.Drawing.Point(12, 87)
        Me.cgRecords.Name = "cgRecords"
        Me.cgRecords.PreviewColumn = ""
        Me.cgRecords.QueryID = Nothing
        Me.cgRecords.RowAutoHeight = False
        Me.cgRecords.SearchAsYouType = True
        Me.cgRecords.ShowAutoFilterRow = False
        Me.cgRecords.ShowFindPanel = False
        Me.cgRecords.ShowGroupByBox = False
        Me.cgRecords.ShowLoadingPanel = False
        Me.cgRecords.ShowNavigator = False
        Me.cgRecords.Size = New System.Drawing.Size(1048, 384)
        Me.cgRecords.TabIndex = 1
        '
        'txtRed
        '
        Me.txtRed.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtRed.EnterMoveNextControl = True
        Me.txtRed.Location = New System.Drawing.Point(12, 479)
        Me.txtRed.MaxLength = 0
        Me.txtRed.Name = "txtRed"
        Me.txtRed.NumericAllowNegatives = False
        Me.txtRed.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRed.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRed.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRed.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtRed.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRed.Properties.Appearance.Options.UseBackColor = True
        Me.txtRed.Properties.Appearance.Options.UseFont = True
        Me.txtRed.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRed.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRed.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRed.Size = New System.Drawing.Size(25, 22)
        Me.txtRed.TabIndex = 2
        Me.txtRed.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRed.ToolTipText = ""
        Me.txtRed.Visible = False
        '
        'txtYellow
        '
        Me.txtYellow.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtYellow.EnterMoveNextControl = True
        Me.txtYellow.Location = New System.Drawing.Point(103, 479)
        Me.txtYellow.MaxLength = 0
        Me.txtYellow.Name = "txtYellow"
        Me.txtYellow.NumericAllowNegatives = False
        Me.txtYellow.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtYellow.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtYellow.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtYellow.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtYellow.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtYellow.Properties.Appearance.Options.UseBackColor = True
        Me.txtYellow.Properties.Appearance.Options.UseFont = True
        Me.txtYellow.Properties.Appearance.Options.UseTextOptions = True
        Me.txtYellow.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtYellow.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtYellow.Size = New System.Drawing.Size(25, 22)
        Me.txtYellow.TabIndex = 5
        Me.txtYellow.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtYellow.ToolTipText = ""
        Me.txtYellow.Visible = False
        '
        'txtBlue
        '
        Me.txtBlue.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtBlue.EnterMoveNextControl = True
        Me.txtBlue.Location = New System.Drawing.Point(72, 479)
        Me.txtBlue.MaxLength = 0
        Me.txtBlue.Name = "txtBlue"
        Me.txtBlue.NumericAllowNegatives = False
        Me.txtBlue.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtBlue.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBlue.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtBlue.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBlue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtBlue.Properties.Appearance.Options.UseBackColor = True
        Me.txtBlue.Properties.Appearance.Options.UseFont = True
        Me.txtBlue.Properties.Appearance.Options.UseTextOptions = True
        Me.txtBlue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtBlue.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtBlue.Size = New System.Drawing.Size(25, 22)
        Me.txtBlue.TabIndex = 4
        Me.txtBlue.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtBlue.ToolTipText = ""
        Me.txtBlue.Visible = False
        '
        'txtGreen
        '
        Me.txtGreen.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtGreen.EnterMoveNextControl = True
        Me.txtGreen.Location = New System.Drawing.Point(41, 479)
        Me.txtGreen.MaxLength = 0
        Me.txtGreen.Name = "txtGreen"
        Me.txtGreen.NumericAllowNegatives = False
        Me.txtGreen.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtGreen.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtGreen.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtGreen.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtGreen.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtGreen.Properties.Appearance.Options.UseBackColor = True
        Me.txtGreen.Properties.Appearance.Options.UseFont = True
        Me.txtGreen.Properties.Appearance.Options.UseTextOptions = True
        Me.txtGreen.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtGreen.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtGreen.Size = New System.Drawing.Size(25, 22)
        Me.txtGreen.TabIndex = 3
        Me.txtGreen.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtGreen.ToolTipText = ""
        Me.txtGreen.Visible = False
        '
        'txtOrange
        '
        Me.txtOrange.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtOrange.EnterMoveNextControl = True
        Me.txtOrange.Location = New System.Drawing.Point(134, 479)
        Me.txtOrange.MaxLength = 0
        Me.txtOrange.Name = "txtOrange"
        Me.txtOrange.NumericAllowNegatives = False
        Me.txtOrange.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtOrange.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtOrange.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtOrange.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtOrange.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtOrange.Properties.Appearance.Options.UseBackColor = True
        Me.txtOrange.Properties.Appearance.Options.UseFont = True
        Me.txtOrange.Properties.Appearance.Options.UseTextOptions = True
        Me.txtOrange.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtOrange.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtOrange.Size = New System.Drawing.Size(25, 22)
        Me.txtOrange.TabIndex = 6
        Me.txtOrange.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtOrange.ToolTipText = ""
        Me.txtOrange.Visible = False
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(111, 11)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AccessibleName = "Status"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(273, 22)
        Me.cbxSite.TabIndex = 2
        Me.cbxSite.Tag = ""
        Me.cbxSite.ValueMember = Nothing
        '
        'radAll
        '
        Me.radAll.EditValue = True
        Me.radAll.Location = New System.Drawing.Point(8, 12)
        Me.radAll.Name = "radAll"
        Me.radAll.Properties.AutoWidth = True
        Me.radAll.Properties.Caption = "All"
        Me.radAll.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radAll.Properties.RadioGroupIndex = 0
        Me.radAll.Size = New System.Drawing.Size(33, 19)
        Me.radAll.TabIndex = 0
        '
        'radSpecific
        '
        Me.radSpecific.Location = New System.Drawing.Point(47, 12)
        Me.radSpecific.Name = "radSpecific"
        Me.radSpecific.Properties.AutoWidth = True
        Me.radSpecific.Properties.Caption = "Specific"
        Me.radSpecific.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radSpecific.Properties.RadioGroupIndex = 0
        Me.radSpecific.Size = New System.Drawing.Size(58, 19)
        Me.radSpecific.TabIndex = 1
        Me.radSpecific.TabStop = False
        '
        'frmFinancialsSync
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1072, 513)
        Me.Controls.Add(Me.txtOrange)
        Me.Controls.Add(Me.txtRed)
        Me.Controls.Add(Me.txtYellow)
        Me.Controls.Add(Me.txtBlue)
        Me.Controls.Add(Me.txtGreen)
        Me.Controls.Add(Me.cgRecords)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnRun)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.LoadMaximised = True
        Me.Name = "frmFinancialsSync"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = ""
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.cbxNameFormat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxOption.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBlue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOrange.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radAll.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radSpecific.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnRun As Care.Controls.CareButton
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents cbxNameFormat As Care.Controls.CareComboBox
    Friend WithEvents Label10 As Care.Controls.CareLabel
    Friend WithEvents cbxOption As Care.Controls.CareComboBox
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents btnRefresh As Care.Controls.CareButton
    Friend WithEvents cgRecords As Care.Controls.CareGrid
    Friend WithEvents txtRed As Care.Controls.CareTextBox
    Friend WithEvents txtYellow As Care.Controls.CareTextBox
    Friend WithEvents txtBlue As Care.Controls.CareTextBox
    Friend WithEvents txtGreen As Care.Controls.CareTextBox
    Friend WithEvents txtOrange As Care.Controls.CareTextBox
    Friend WithEvents GroupControl1 As Care.Controls.CareFrame
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
    Friend WithEvents radAll As Care.Controls.CareRadioButton
    Friend WithEvents radSpecific As Care.Controls.CareRadioButton
End Class
