﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFamily
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox2 = New Care.Controls.CareFrame()
        Me.GridChildren = New Care.Controls.CareGrid()
        Me.GroupBox1 = New Care.Controls.CareFrame()
        Me.GridContacts = New Care.Controls.CareGrid()
        Me.Panel2 = New Panel()
        Me.btnEdit = New Care.Controls.CareButton()
        Me.btnDelete = New Care.Controls.CareButton()
        Me.lblWarning = New Care.Controls.CareLabel()
        Me.btnAdd = New Care.Controls.CareButton()
        Me.XtraTabControl1 = New Care.Controls.CareTab()
        Me.tabFamily = New DevExpress.XtraTab.XtraTabPage()
        Me.fraFinancials = New Care.Controls.CareFrame()
        Me.lblBalance = New Care.Controls.CareLabel()
        Me.lblSageAccount = New Care.Controls.CareLabel()
        Me.CareLabel2 = New Care.Controls.CareLabel()
        Me.chkExclFinancials = New Care.Controls.CareCheckBox()
        Me.txtOpeningBalance = New Care.Controls.CareTextBox()
        Me.CareLabel1 = New Care.Controls.CareLabel()
        Me.btnCustomerCreate = New Care.Controls.CareButton()
        Me.btnCustomerStatement = New Care.Controls.CareButton()
        Me.btnCustomerOpen = New Care.Controls.CareButton()
        Me.GroupControl3 = New Care.Controls.CareFrame()
        Me.CareLabel9 = New Care.Controls.CareLabel()
        Me.chkArchived = New Care.Controls.CareCheckBox()
        Me.CareLabel3 = New Care.Controls.CareLabel()
        Me.CareLabel6 = New Care.Controls.CareLabel()
        Me.CareLabel4 = New Care.Controls.CareLabel()
        Me.cbxSite = New Care.Controls.CareComboBox()
        Me.Label4 = New Care.Controls.CareLabel()
        Me.chkExclInvoicing = New Care.Controls.CareCheckBox()
        Me.txtAccountNo = New Care.Controls.CareTextBox()
        Me.chkEInvoicing = New Care.Controls.CareCheckBox()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.txtSurname = New Care.Controls.CareTextBox()
        Me.CareLabel8 = New Care.Controls.CareLabel()
        Me.CareLabel7 = New Care.Controls.CareLabel()
        Me.txtLetterName = New Care.Controls.CareTextBox()
        Me.cbxInvoiceDue = New Care.Controls.CareComboBox()
        Me.CareLabel5 = New Care.Controls.CareLabel()
        Me.txtAddressFull = New Care.Address.CareAddress()
        Me.cbxInvoiceFreq = New Care.Controls.CareComboBox()
        Me.Label7 = New Care.Controls.CareLabel()
        Me.Label2 = New Care.Controls.CareLabel()
        Me.tabInvoices = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl5 = New Care.Controls.CareFrame()
        Me.radPosted = New Care.Controls.CareRadioButton()
        Me.radNonPosted = New Care.Controls.CareRadioButton()
        Me.btnDeleteInvoice = New Care.Controls.CareButton()
        Me.btnInvoiceEdit = New Care.Controls.CareButton()
        Me.GridInvoices = New Care.Controls.CareGrid()
        Me.tabActivity = New DevExpress.XtraTab.XtraTabPage()
        Me.crmFamily = New Care.[Shared].CRMActivity()
        Me.tabStatement = New DevExpress.XtraTab.XtraTabPage()
        Me.GridStatement = New Care.Controls.CareGrid()
        Me.tabPayments = New DevExpress.XtraTab.XtraTabPage()
        Me.GridPayments = New Care.Controls.CareGrid()
        Me.btnPrintReport = New Care.Controls.CareButton()
        Me.btnEmailReport = New Care.Controls.CareButton()
        Me.btnMerge = New Care.Controls.CareButton()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.GroupBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.tabFamily.SuspendLayout()
        CType(Me.fraFinancials, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.fraFinancials.SuspendLayout()
        CType(Me.chkExclFinancials.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOpeningBalance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.chkArchived.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkExclInvoicing.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAccountNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkEInvoicing.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLetterName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxInvoiceDue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxInvoiceFreq.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabInvoices.SuspendLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.radPosted.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radNonPosted.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabActivity.SuspendLayout()
        Me.tabStatement.SuspendLayout()
        Me.tabPayments.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(750, 4)
        Me.btnCancel.TabIndex = 4
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(659, 4)
        Me.btnOK.TabIndex = 3
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnMerge)
        Me.Panel1.Controls.Add(Me.btnEmailReport)
        Me.Panel1.Controls.Add(Me.btnPrintReport)
        Me.Panel1.Location = New System.Drawing.Point(0, 565)
        Me.Panel1.Size = New System.Drawing.Size(845, 35)
        Me.Panel1.TabIndex = 1
        Me.Panel1.Controls.SetChildIndex(Me.btnPrintReport, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnOK, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnCancel, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnEmailReport, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnMerge, 0)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupBox2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Appearance.Options.UseFont = True
        Me.GroupBox2.AppearanceCaption.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.AppearanceCaption.Options.UseFont = True
        Me.GroupBox2.Controls.Add(Me.GridChildren)
        Me.GroupBox2.Location = New System.Drawing.Point(563, 9)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(252, 157)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.Text = "Children (Double-Click to View Child)"
        '
        'GridChildren
        '
        Me.GridChildren.AllowBuildColumns = True
        Me.GridChildren.AllowEdit = False
        Me.GridChildren.AllowHorizontalScroll = False
        Me.GridChildren.AllowMultiSelect = False
        Me.GridChildren.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GridChildren.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridChildren.Appearance.Options.UseFont = True
        Me.GridChildren.AutoSizeByData = True
        Me.GridChildren.DisableAutoSize = False
        Me.GridChildren.DisableDataFormatting = False
        Me.GridChildren.FocusedRowHandle = -2147483648
        Me.GridChildren.HideFirstColumn = False
        Me.GridChildren.Location = New System.Drawing.Point(10, 30)
        Me.GridChildren.Name = "GridChildren"
        Me.GridChildren.PreviewColumn = ""
        Me.GridChildren.QueryID = Nothing
        Me.GridChildren.RowAutoHeight = False
        Me.GridChildren.SearchAsYouType = True
        Me.GridChildren.ShowAutoFilterRow = False
        Me.GridChildren.ShowFindPanel = False
        Me.GridChildren.ShowGroupByBox = False
        Me.GridChildren.ShowLoadingPanel = False
        Me.GridChildren.ShowNavigator = False
        Me.GridChildren.Size = New System.Drawing.Size(234, 119)
        Me.GridChildren.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupBox1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Appearance.Options.UseFont = True
        Me.GroupBox1.AppearanceCaption.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.AppearanceCaption.Options.UseFont = True
        Me.GroupBox1.Controls.Add(Me.GridContacts)
        Me.GroupBox1.Controls.Add(Me.Panel2)
        Me.GroupBox1.Location = New System.Drawing.Point(9, 265)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(806, 210)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.Text = "Contacts"
        '
        'GridContacts
        '
        Me.GridContacts.AllowBuildColumns = True
        Me.GridContacts.AllowEdit = False
        Me.GridContacts.AllowHorizontalScroll = False
        Me.GridContacts.AllowMultiSelect = False
        Me.GridContacts.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GridContacts.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridContacts.Appearance.Options.UseFont = True
        Me.GridContacts.AutoSizeByData = True
        Me.GridContacts.DisableAutoSize = False
        Me.GridContacts.DisableDataFormatting = False
        Me.GridContacts.FocusedRowHandle = -2147483648
        Me.GridContacts.HideFirstColumn = False
        Me.GridContacts.Location = New System.Drawing.Point(12, 30)
        Me.GridContacts.Name = "GridContacts"
        Me.GridContacts.PreviewColumn = ""
        Me.GridContacts.QueryID = Nothing
        Me.GridContacts.RowAutoHeight = False
        Me.GridContacts.SearchAsYouType = True
        Me.GridContacts.ShowAutoFilterRow = False
        Me.GridContacts.ShowFindPanel = False
        Me.GridContacts.ShowGroupByBox = False
        Me.GridContacts.ShowLoadingPanel = False
        Me.GridContacts.ShowNavigator = False
        Me.GridContacts.Size = New System.Drawing.Size(784, 142)
        Me.GridContacts.TabIndex = 0
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnEdit)
        Me.Panel2.Controls.Add(Me.btnDelete)
        Me.Panel2.Controls.Add(Me.lblWarning)
        Me.Panel2.Controls.Add(Me.btnAdd)
        Me.Panel2.Dock = DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(2, 173)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(802, 35)
        Me.Panel2.TabIndex = 0
        '
        'btnEdit
        '
        Me.btnEdit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.Appearance.Options.UseFont = True
        Me.btnEdit.Location = New System.Drawing.Point(91, 6)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(75, 24)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Tag = ""
        Me.btnEdit.Text = "Edit"
        '
        'btnDelete
        '
        Me.btnDelete.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Appearance.Options.UseFont = True
        Me.btnDelete.Location = New System.Drawing.Point(172, 6)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(75, 24)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Tag = ""
        Me.btnDelete.Text = "Delete"
        '
        'lblWarning
        '
        Me.lblWarning.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWarning.Appearance.ForeColor = System.Drawing.Color.Red
        Me.lblWarning.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblWarning.Location = New System.Drawing.Point(255, 11)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(149, 15)
        Me.lblWarning.TabIndex = 3
        Me.lblWarning.Text = "Primary Contact not setup"
        Me.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnAdd
        '
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.Appearance.Options.UseFont = True
        Me.btnAdd.Location = New System.Drawing.Point(10, 5)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(75, 25)
        Me.btnAdd.TabIndex = 0
        Me.btnAdd.Tag = ""
        Me.btnAdd.Text = "Add"
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XtraTabControl1.Appearance.Options.UseFont = True
        Me.XtraTabControl1.AppearancePage.Header.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XtraTabControl1.AppearancePage.Header.Options.UseFont = True
        Me.XtraTabControl1.AppearancePage.HeaderActive.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.XtraTabControl1.AppearancePage.HeaderActive.Options.UseFont = True
        Me.XtraTabControl1.AppearancePage.HeaderDisabled.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.XtraTabControl1.AppearancePage.HeaderDisabled.Options.UseFont = True
        Me.XtraTabControl1.AppearancePage.HeaderHotTracked.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.XtraTabControl1.AppearancePage.HeaderHotTracked.Options.UseFont = True
        Me.XtraTabControl1.AppearancePage.PageClient.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.XtraTabControl1.AppearancePage.PageClient.Options.UseFont = True
        Me.XtraTabControl1.Location = New System.Drawing.Point(10, 53)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.tabFamily
        Me.XtraTabControl1.Size = New System.Drawing.Size(829, 511)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabFamily, Me.tabInvoices, Me.tabActivity, Me.tabStatement, Me.tabPayments})
        '
        'tabFamily
        '
        Me.tabFamily.Controls.Add(Me.fraFinancials)
        Me.tabFamily.Controls.Add(Me.GroupControl3)
        Me.tabFamily.Controls.Add(Me.GroupBox2)
        Me.tabFamily.Controls.Add(Me.GroupControl1)
        Me.tabFamily.Controls.Add(Me.GroupBox1)
        Me.tabFamily.Name = "tabFamily"
        Me.tabFamily.Size = New System.Drawing.Size(823, 481)
        Me.tabFamily.Text = "Family"
        '
        'fraFinancials
        '
        Me.fraFinancials.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.fraFinancials.Controls.Add(Me.lblBalance)
        Me.fraFinancials.Controls.Add(Me.lblSageAccount)
        Me.fraFinancials.Controls.Add(Me.CareLabel2)
        Me.fraFinancials.Controls.Add(Me.chkExclFinancials)
        Me.fraFinancials.Controls.Add(Me.txtOpeningBalance)
        Me.fraFinancials.Controls.Add(Me.CareLabel1)
        Me.fraFinancials.Controls.Add(Me.btnCustomerCreate)
        Me.fraFinancials.Controls.Add(Me.btnCustomerStatement)
        Me.fraFinancials.Controls.Add(Me.btnCustomerOpen)
        Me.fraFinancials.Location = New System.Drawing.Point(348, 172)
        Me.fraFinancials.Name = "fraFinancials"
        Me.fraFinancials.Size = New System.Drawing.Size(467, 87)
        Me.fraFinancials.TabIndex = 2
        Me.fraFinancials.Text = "Financials Integration"
        '
        'lblBalance
        '
        Me.lblBalance.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBalance.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblBalance.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblBalance.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblBalance.Location = New System.Drawing.Point(361, 30)
        Me.lblBalance.Name = "lblBalance"
        Me.lblBalance.Size = New System.Drawing.Size(98, 15)
        Me.lblBalance.TabIndex = 8
        Me.lblBalance.Text = "<Balance>"
        Me.lblBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSageAccount
        '
        Me.lblSageAccount.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSageAccount.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblSageAccount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblSageAccount.Location = New System.Drawing.Point(192, 30)
        Me.lblSageAccount.Name = "lblSageAccount"
        Me.lblSageAccount.Size = New System.Drawing.Size(141, 15)
        Me.lblSageAccount.TabIndex = 7
        Me.lblSageAccount.Text = "<Sage Account Number>"
        Me.lblSageAccount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(10, 30)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(124, 15)
        Me.CareLabel2.TabIndex = 0
        Me.CareLabel2.Text = "Exclude from Financials"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkExclFinancials
        '
        Me.chkExclFinancials.EnterMoveNextControl = True
        Me.chkExclFinancials.Location = New System.Drawing.Point(140, 28)
        Me.chkExclFinancials.Name = "chkExclFinancials"
        Me.chkExclFinancials.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkExclFinancials.Properties.Appearance.Options.UseFont = True
        Me.chkExclFinancials.Size = New System.Drawing.Size(20, 19)
        Me.chkExclFinancials.TabIndex = 1
        Me.chkExclFinancials.Tag = "E"
        '
        'txtOpeningBalance
        '
        Me.txtOpeningBalance.CharacterCasing = CharacterCasing.Normal
        Me.txtOpeningBalance.EnterMoveNextControl = True
        Me.txtOpeningBalance.Location = New System.Drawing.Point(106, 57)
        Me.txtOpeningBalance.MaxLength = 14
        Me.txtOpeningBalance.Name = "txtOpeningBalance"
        Me.txtOpeningBalance.NumericAllowNegatives = True
        Me.txtOpeningBalance.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtOpeningBalance.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtOpeningBalance.Properties.AccessibleName = "Surname"
        Me.txtOpeningBalance.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtOpeningBalance.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtOpeningBalance.Properties.Appearance.Options.UseFont = True
        Me.txtOpeningBalance.Properties.Appearance.Options.UseTextOptions = True
        Me.txtOpeningBalance.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtOpeningBalance.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txtOpeningBalance.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtOpeningBalance.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtOpeningBalance.Properties.MaxLength = 14
        Me.txtOpeningBalance.Size = New System.Drawing.Size(80, 22)
        Me.txtOpeningBalance.TabIndex = 3
        Me.txtOpeningBalance.Tag = "E"
        Me.txtOpeningBalance.TextAlign = HorizontalAlignment.Right
        Me.txtOpeningBalance.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(10, 60)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(90, 15)
        Me.CareLabel1.TabIndex = 2
        Me.CareLabel1.Text = "Opening Balance"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnCustomerCreate
        '
        Me.btnCustomerCreate.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCustomerCreate.Appearance.Options.UseFont = True
        Me.btnCustomerCreate.Location = New System.Drawing.Point(192, 55)
        Me.btnCustomerCreate.Name = "btnCustomerCreate"
        Me.btnCustomerCreate.Size = New System.Drawing.Size(85, 24)
        Me.btnCustomerCreate.TabIndex = 4
        Me.btnCustomerCreate.Tag = ""
        Me.btnCustomerCreate.Text = "Create / Link"
        '
        'btnCustomerStatement
        '
        Me.btnCustomerStatement.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCustomerStatement.Appearance.Options.UseFont = True
        Me.btnCustomerStatement.Location = New System.Drawing.Point(374, 55)
        Me.btnCustomerStatement.Name = "btnCustomerStatement"
        Me.btnCustomerStatement.Size = New System.Drawing.Size(85, 24)
        Me.btnCustomerStatement.TabIndex = 6
        Me.btnCustomerStatement.Tag = ""
        Me.btnCustomerStatement.Text = "Statement"
        '
        'btnCustomerOpen
        '
        Me.btnCustomerOpen.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCustomerOpen.Appearance.Options.UseFont = True
        Me.btnCustomerOpen.Location = New System.Drawing.Point(283, 55)
        Me.btnCustomerOpen.Name = "btnCustomerOpen"
        Me.btnCustomerOpen.Size = New System.Drawing.Size(85, 24)
        Me.btnCustomerOpen.TabIndex = 5
        Me.btnCustomerOpen.Tag = ""
        Me.btnCustomerOpen.Text = "Open"
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.CareLabel9)
        Me.GroupControl3.Controls.Add(Me.chkArchived)
        Me.GroupControl3.Controls.Add(Me.CareLabel3)
        Me.GroupControl3.Controls.Add(Me.CareLabel6)
        Me.GroupControl3.Controls.Add(Me.CareLabel4)
        Me.GroupControl3.Controls.Add(Me.cbxSite)
        Me.GroupControl3.Controls.Add(Me.Label4)
        Me.GroupControl3.Controls.Add(Me.chkExclInvoicing)
        Me.GroupControl3.Controls.Add(Me.txtAccountNo)
        Me.GroupControl3.Controls.Add(Me.chkEInvoicing)
        Me.GroupControl3.Location = New System.Drawing.Point(348, 8)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(209, 158)
        Me.GroupControl3.TabIndex = 1
        Me.GroupControl3.Text = "Site && Account Number"
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(10, 136)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(47, 15)
        Me.CareLabel9.TabIndex = 8
        Me.CareLabel9.Text = "Archived"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkArchived
        '
        Me.chkArchived.EnterMoveNextControl = True
        Me.chkArchived.Location = New System.Drawing.Point(182, 134)
        Me.chkArchived.Name = "chkArchived"
        Me.chkArchived.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkArchived.Properties.Appearance.Options.UseFont = True
        Me.chkArchived.Size = New System.Drawing.Size(20, 19)
        Me.chkArchived.TabIndex = 9
        Me.chkArchived.Tag = "E"
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(10, 111)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(121, 15)
        Me.CareLabel3.TabIndex = 6
        Me.CareLabel3.Text = "Exclude from Invoicing"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(10, 58)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(16, 15)
        Me.CareLabel6.TabIndex = 2
        Me.CareLabel6.Text = "No"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(10, 30)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(19, 15)
        Me.CareLabel4.TabIndex = 0
        Me.CareLabel4.Text = "Site"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(47, 27)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AccessibleName = "Site"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(154, 22)
        Me.cbxSite.TabIndex = 1
        Me.cbxSite.Tag = "AEM"
        Me.cbxSite.ValueMember = Nothing
        '
        'Label4
        '
        Me.Label4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label4.Location = New System.Drawing.Point(10, 86)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(83, 15)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Email Invoices ?"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkExclInvoicing
        '
        Me.chkExclInvoicing.EnterMoveNextControl = True
        Me.chkExclInvoicing.Location = New System.Drawing.Point(182, 109)
        Me.chkExclInvoicing.Name = "chkExclInvoicing"
        Me.chkExclInvoicing.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkExclInvoicing.Properties.Appearance.Options.UseFont = True
        Me.chkExclInvoicing.Size = New System.Drawing.Size(20, 19)
        Me.chkExclInvoicing.TabIndex = 7
        Me.chkExclInvoicing.Tag = "E"
        '
        'txtAccountNo
        '
        Me.txtAccountNo.CharacterCasing = CharacterCasing.Upper
        Me.txtAccountNo.EnterMoveNextControl = True
        Me.txtAccountNo.Location = New System.Drawing.Point(47, 55)
        Me.txtAccountNo.MaxLength = 20
        Me.txtAccountNo.Name = "txtAccountNo"
        Me.txtAccountNo.NumericAllowNegatives = False
        Me.txtAccountNo.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtAccountNo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtAccountNo.Properties.AccessibleName = "Addressee"
        Me.txtAccountNo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtAccountNo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtAccountNo.Properties.Appearance.Options.UseFont = True
        Me.txtAccountNo.Properties.Appearance.Options.UseTextOptions = True
        Me.txtAccountNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtAccountNo.Properties.CharacterCasing = CharacterCasing.Upper
        Me.txtAccountNo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtAccountNo.Properties.MaxLength = 20
        Me.txtAccountNo.Properties.ReadOnly = True
        Me.txtAccountNo.Size = New System.Drawing.Size(154, 22)
        Me.txtAccountNo.TabIndex = 3
        Me.txtAccountNo.Tag = ""
        Me.txtAccountNo.TextAlign = HorizontalAlignment.Left
        Me.txtAccountNo.ToolTipText = ""
        '
        'chkEInvoicing
        '
        Me.chkEInvoicing.EnterMoveNextControl = True
        Me.chkEInvoicing.Location = New System.Drawing.Point(182, 84)
        Me.chkEInvoicing.Name = "chkEInvoicing"
        Me.chkEInvoicing.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkEInvoicing.Properties.Appearance.Options.UseFont = True
        Me.chkEInvoicing.Size = New System.Drawing.Size(20, 19)
        Me.chkEInvoicing.TabIndex = 5
        Me.chkEInvoicing.Tag = "AE"
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.txtSurname)
        Me.GroupControl1.Controls.Add(Me.CareLabel8)
        Me.GroupControl1.Controls.Add(Me.CareLabel7)
        Me.GroupControl1.Controls.Add(Me.txtLetterName)
        Me.GroupControl1.Controls.Add(Me.cbxInvoiceDue)
        Me.GroupControl1.Controls.Add(Me.CareLabel5)
        Me.GroupControl1.Controls.Add(Me.txtAddressFull)
        Me.GroupControl1.Controls.Add(Me.cbxInvoiceFreq)
        Me.GroupControl1.Controls.Add(Me.Label7)
        Me.GroupControl1.Controls.Add(Me.Label2)
        Me.GroupControl1.Location = New System.Drawing.Point(9, 8)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(333, 251)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "GroupControl1"
        '
        'txtSurname
        '
        Me.txtSurname.CharacterCasing = CharacterCasing.Normal
        Me.txtSurname.EnterMoveNextControl = True
        Me.txtSurname.Location = New System.Drawing.Point(93, 11)
        Me.txtSurname.MaxLength = 30
        Me.txtSurname.Name = "txtSurname"
        Me.txtSurname.NumericAllowNegatives = False
        Me.txtSurname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSurname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSurname.Properties.AccessibleName = "Surname"
        Me.txtSurname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSurname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSurname.Properties.Appearance.Options.UseFont = True
        Me.txtSurname.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSurname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSurname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSurname.Properties.MaxLength = 30
        Me.txtSurname.Size = New System.Drawing.Size(230, 22)
        Me.txtSurname.TabIndex = 1
        Me.txtSurname.Tag = "AETM"
        Me.txtSurname.TextAlign = HorizontalAlignment.Left
        Me.txtSurname.ToolTipText = ""
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(10, 14)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(47, 15)
        Me.CareLabel8.TabIndex = 0
        Me.CareLabel8.Text = "Surname"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel8.ToolTip = "This is the name that will appear on Invoices or Letters from the nursery."
        Me.CareLabel8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(10, 194)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(67, 15)
        Me.CareLabel7.TabIndex = 6
        Me.CareLabel7.Text = "Invoices Due"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLetterName
        '
        Me.txtLetterName.CharacterCasing = CharacterCasing.Normal
        Me.txtLetterName.EnterMoveNextControl = True
        Me.txtLetterName.Location = New System.Drawing.Point(93, 39)
        Me.txtLetterName.MaxLength = 30
        Me.txtLetterName.Name = "txtLetterName"
        Me.txtLetterName.NumericAllowNegatives = False
        Me.txtLetterName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtLetterName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLetterName.Properties.AccessibleName = "Addressee"
        Me.txtLetterName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtLetterName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtLetterName.Properties.Appearance.Options.UseFont = True
        Me.txtLetterName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLetterName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtLetterName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtLetterName.Properties.MaxLength = 30
        Me.txtLetterName.Size = New System.Drawing.Size(230, 22)
        Me.txtLetterName.TabIndex = 3
        Me.txtLetterName.Tag = "AETM"
        Me.txtLetterName.TextAlign = HorizontalAlignment.Left
        Me.txtLetterName.ToolTipText = ""
        '
        'cbxInvoiceDue
        '
        Me.cbxInvoiceDue.AllowBlank = False
        Me.cbxInvoiceDue.DataSource = Nothing
        Me.cbxInvoiceDue.DisplayMember = Nothing
        Me.cbxInvoiceDue.EnterMoveNextControl = True
        Me.cbxInvoiceDue.Location = New System.Drawing.Point(93, 191)
        Me.cbxInvoiceDue.Name = "cbxInvoiceDue"
        Me.cbxInvoiceDue.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxInvoiceDue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxInvoiceDue.Properties.Appearance.Options.UseFont = True
        Me.cbxInvoiceDue.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxInvoiceDue.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxInvoiceDue.SelectedValue = Nothing
        Me.cbxInvoiceDue.Size = New System.Drawing.Size(230, 22)
        Me.cbxInvoiceDue.TabIndex = 7
        Me.cbxInvoiceDue.Tag = "AEB"
        Me.cbxInvoiceDue.ValueMember = Nothing
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(10, 222)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(55, 15)
        Me.CareLabel5.TabIndex = 8
        Me.CareLabel5.Text = "Frequency"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAddressFull
        '
        Me.txtAddressFull.AccessibleName = "Address"
        Me.txtAddressFull.Address = ""
        Me.txtAddressFull.AddressBlock = ""
        Me.txtAddressFull.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddressFull.Appearance.Options.UseFont = True
        Me.txtAddressFull.County = ""
        Me.txtAddressFull.DisplayMode = Care.Address.CareAddress.EnumDisplayMode.SingleControl
        Me.txtAddressFull.Location = New System.Drawing.Point(93, 67)
        Me.txtAddressFull.MaxLength = 500
        Me.txtAddressFull.Name = "txtAddressFull"
        Me.txtAddressFull.PostCode = ""
        Me.txtAddressFull.ReadOnly = False
        Me.txtAddressFull.Size = New System.Drawing.Size(230, 118)
        Me.txtAddressFull.TabIndex = 5
        Me.txtAddressFull.Tag = "AEM"
        Me.txtAddressFull.Town = ""
        '
        'cbxInvoiceFreq
        '
        Me.cbxInvoiceFreq.AllowBlank = False
        Me.cbxInvoiceFreq.DataSource = Nothing
        Me.cbxInvoiceFreq.DisplayMember = Nothing
        Me.cbxInvoiceFreq.EnterMoveNextControl = True
        Me.cbxInvoiceFreq.Location = New System.Drawing.Point(93, 219)
        Me.cbxInvoiceFreq.Name = "cbxInvoiceFreq"
        Me.cbxInvoiceFreq.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxInvoiceFreq.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxInvoiceFreq.Properties.Appearance.Options.UseFont = True
        Me.cbxInvoiceFreq.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxInvoiceFreq.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxInvoiceFreq.SelectedValue = Nothing
        Me.cbxInvoiceFreq.Size = New System.Drawing.Size(230, 22)
        Me.cbxInvoiceFreq.TabIndex = 9
        Me.cbxInvoiceFreq.Tag = "AEB"
        Me.cbxInvoiceFreq.ValueMember = Nothing
        '
        'Label7
        '
        Me.Label7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label7.Location = New System.Drawing.Point(10, 42)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(54, 15)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "Addressee"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label7.ToolTip = "This is the name that will appear on Invoices or Letters from the nursery."
        Me.Label7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'Label2
        '
        Me.Label2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label2.Location = New System.Drawing.Point(10, 71)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 15)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Address"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabInvoices
        '
        Me.tabInvoices.Controls.Add(Me.GroupControl5)
        Me.tabInvoices.Controls.Add(Me.btnDeleteInvoice)
        Me.tabInvoices.Controls.Add(Me.btnInvoiceEdit)
        Me.tabInvoices.Controls.Add(Me.GridInvoices)
        Me.tabInvoices.Name = "tabInvoices"
        Me.tabInvoices.Size = New System.Drawing.Size(823, 481)
        Me.tabInvoices.Text = "Invoices"
        '
        'GroupControl5
        '
        Me.GroupControl5.Controls.Add(Me.radPosted)
        Me.GroupControl5.Controls.Add(Me.radNonPosted)
        Me.GroupControl5.Location = New System.Drawing.Point(9, 8)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.ShowCaption = False
        Me.GroupControl5.Size = New System.Drawing.Size(805, 33)
        Me.GroupControl5.TabIndex = 5
        '
        'radPosted
        '
        Me.radPosted.EditValue = True
        Me.radPosted.Location = New System.Drawing.Point(14, 7)
        Me.radPosted.Name = "radPosted"
        Me.radPosted.Properties.AutoWidth = True
        Me.radPosted.Properties.Caption = "Posted Invoices"
        Me.radPosted.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radPosted.Properties.RadioGroupIndex = 0
        Me.radPosted.Size = New System.Drawing.Size(98, 19)
        Me.radPosted.TabIndex = 0
        '
        'radNonPosted
        '
        Me.radNonPosted.Location = New System.Drawing.Point(121, 7)
        Me.radNonPosted.Name = "radNonPosted"
        Me.radNonPosted.Properties.AutoWidth = True
        Me.radNonPosted.Properties.Caption = "Non-Posted Invoices"
        Me.radNonPosted.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radNonPosted.Properties.RadioGroupIndex = 0
        Me.radNonPosted.Size = New System.Drawing.Size(121, 19)
        Me.radNonPosted.TabIndex = 1
        Me.radNonPosted.TabStop = False
        '
        'btnDeleteInvoice
        '
        Me.btnDeleteInvoice.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnDeleteInvoice.Appearance.Options.UseFont = True
        Me.btnDeleteInvoice.Location = New System.Drawing.Point(110, 450)
        Me.btnDeleteInvoice.Name = "btnDeleteInvoice"
        Me.btnDeleteInvoice.Size = New System.Drawing.Size(95, 23)
        Me.btnDeleteInvoice.TabIndex = 4
        Me.btnDeleteInvoice.Text = "Delete Invoice"
        '
        'btnInvoiceEdit
        '
        Me.btnInvoiceEdit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnInvoiceEdit.Appearance.Options.UseFont = True
        Me.btnInvoiceEdit.Location = New System.Drawing.Point(9, 450)
        Me.btnInvoiceEdit.Name = "btnInvoiceEdit"
        Me.btnInvoiceEdit.Size = New System.Drawing.Size(95, 23)
        Me.btnInvoiceEdit.TabIndex = 3
        Me.btnInvoiceEdit.Text = "View Invoice"
        '
        'GridInvoices
        '
        Me.GridInvoices.AllowBuildColumns = True
        Me.GridInvoices.AllowEdit = False
        Me.GridInvoices.AllowHorizontalScroll = False
        Me.GridInvoices.AllowMultiSelect = False
        Me.GridInvoices.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GridInvoices.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridInvoices.Appearance.Options.UseFont = True
        Me.GridInvoices.AutoSizeByData = True
        Me.GridInvoices.DisableAutoSize = False
        Me.GridInvoices.DisableDataFormatting = False
        Me.GridInvoices.FocusedRowHandle = -2147483648
        Me.GridInvoices.HideFirstColumn = False
        Me.GridInvoices.Location = New System.Drawing.Point(9, 47)
        Me.GridInvoices.Name = "GridInvoices"
        Me.GridInvoices.PreviewColumn = ""
        Me.GridInvoices.QueryID = Nothing
        Me.GridInvoices.RowAutoHeight = False
        Me.GridInvoices.SearchAsYouType = True
        Me.GridInvoices.ShowAutoFilterRow = False
        Me.GridInvoices.ShowFindPanel = False
        Me.GridInvoices.ShowGroupByBox = False
        Me.GridInvoices.ShowLoadingPanel = False
        Me.GridInvoices.ShowNavigator = False
        Me.GridInvoices.Size = New System.Drawing.Size(805, 397)
        Me.GridInvoices.TabIndex = 1
        '
        'tabActivity
        '
        Me.tabActivity.Controls.Add(Me.crmFamily)
        Me.tabActivity.Name = "tabActivity"
        Me.tabActivity.Size = New System.Drawing.Size(823, 481)
        Me.tabActivity.Text = "Activity"
        '
        'crmFamily
        '
        Me.crmFamily.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.crmFamily.CRMContactEmail = Nothing
        Me.crmFamily.CRMContactID = Nothing
        Me.crmFamily.CRMContactMobile = Nothing
        Me.crmFamily.CRMContactName = Nothing
        Me.crmFamily.CRMLinkID = Nothing
        Me.crmFamily.CRMLinkType = Care.[Shared].CRM.EnumLinkType.Family
        Me.crmFamily.Location = New System.Drawing.Point(9, 8)
        Me.crmFamily.Name = "crmFamily"
        Me.crmFamily.Size = New System.Drawing.Size(808, 466)
        Me.crmFamily.TabIndex = 0
        '
        'tabStatement
        '
        Me.tabStatement.Controls.Add(Me.GridStatement)
        Me.tabStatement.Name = "tabStatement"
        Me.tabStatement.Size = New System.Drawing.Size(823, 481)
        Me.tabStatement.Text = "Statement"
        '
        'GridStatement
        '
        Me.GridStatement.AllowBuildColumns = True
        Me.GridStatement.AllowEdit = False
        Me.GridStatement.AllowHorizontalScroll = False
        Me.GridStatement.AllowMultiSelect = False
        Me.GridStatement.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GridStatement.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridStatement.Appearance.Options.UseFont = True
        Me.GridStatement.AutoSizeByData = True
        Me.GridStatement.DisableAutoSize = False
        Me.GridStatement.DisableDataFormatting = False
        Me.GridStatement.FocusedRowHandle = -2147483648
        Me.GridStatement.HideFirstColumn = False
        Me.GridStatement.Location = New System.Drawing.Point(9, 8)
        Me.GridStatement.Name = "GridStatement"
        Me.GridStatement.PreviewColumn = ""
        Me.GridStatement.QueryID = Nothing
        Me.GridStatement.RowAutoHeight = False
        Me.GridStatement.SearchAsYouType = True
        Me.GridStatement.ShowAutoFilterRow = False
        Me.GridStatement.ShowFindPanel = False
        Me.GridStatement.ShowGroupByBox = False
        Me.GridStatement.ShowLoadingPanel = False
        Me.GridStatement.ShowNavigator = False
        Me.GridStatement.Size = New System.Drawing.Size(806, 465)
        Me.GridStatement.TabIndex = 0
        '
        'tabPayments
        '
        Me.tabPayments.Controls.Add(Me.GridPayments)
        Me.tabPayments.Name = "tabPayments"
        Me.tabPayments.Size = New System.Drawing.Size(823, 481)
        Me.tabPayments.Text = "Payment History"
        '
        'GridPayments
        '
        Me.GridPayments.AllowBuildColumns = True
        Me.GridPayments.AllowEdit = False
        Me.GridPayments.AllowHorizontalScroll = False
        Me.GridPayments.AllowMultiSelect = False
        Me.GridPayments.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GridPayments.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridPayments.Appearance.Options.UseFont = True
        Me.GridPayments.AutoSizeByData = True
        Me.GridPayments.DisableAutoSize = False
        Me.GridPayments.DisableDataFormatting = False
        Me.GridPayments.FocusedRowHandle = -2147483648
        Me.GridPayments.HideFirstColumn = False
        Me.GridPayments.Location = New System.Drawing.Point(9, 8)
        Me.GridPayments.Name = "GridPayments"
        Me.GridPayments.PreviewColumn = ""
        Me.GridPayments.QueryID = Nothing
        Me.GridPayments.RowAutoHeight = False
        Me.GridPayments.SearchAsYouType = True
        Me.GridPayments.ShowAutoFilterRow = False
        Me.GridPayments.ShowFindPanel = False
        Me.GridPayments.ShowGroupByBox = False
        Me.GridPayments.ShowLoadingPanel = False
        Me.GridPayments.ShowNavigator = False
        Me.GridPayments.Size = New System.Drawing.Size(806, 465)
        Me.GridPayments.TabIndex = 1
        '
        'btnPrintReport
        '
        Me.btnPrintReport.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnPrintReport.Appearance.Options.UseFont = True
        Me.btnPrintReport.Location = New System.Drawing.Point(10, 5)
        Me.btnPrintReport.Name = "btnPrintReport"
        Me.btnPrintReport.Size = New System.Drawing.Size(125, 23)
        Me.btnPrintReport.TabIndex = 0
        Me.btnPrintReport.Text = "Print Todays Report"
        '
        'btnEmailReport
        '
        Me.btnEmailReport.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnEmailReport.Appearance.Options.UseFont = True
        Me.btnEmailReport.Location = New System.Drawing.Point(141, 5)
        Me.btnEmailReport.Name = "btnEmailReport"
        Me.btnEmailReport.Size = New System.Drawing.Size(125, 23)
        Me.btnEmailReport.TabIndex = 1
        Me.btnEmailReport.Text = "Email Todays Report"
        '
        'btnMerge
        '
        Me.btnMerge.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnMerge.Appearance.Options.UseFont = True
        Me.btnMerge.Location = New System.Drawing.Point(272, 5)
        Me.btnMerge.Name = "btnMerge"
        Me.btnMerge.Size = New System.Drawing.Size(125, 23)
        Me.btnMerge.TabIndex = 2
        Me.btnMerge.Text = "Merge Family"
        '
        'frmFamily
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(845, 600)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.MinimumSize = New System.Drawing.Size(544, 289)
        Me.Name = "frmFamily"
        Me.Text = "Family"
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.XtraTabControl1, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.GroupBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabControl1.PerformLayout()
        Me.tabFamily.ResumeLayout(False)
        CType(Me.fraFinancials, System.ComponentModel.ISupportInitialize).EndInit()
        Me.fraFinancials.ResumeLayout(False)
        Me.fraFinancials.PerformLayout()
        CType(Me.chkExclFinancials.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOpeningBalance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.chkArchived.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkExclInvoicing.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAccountNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkEInvoicing.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLetterName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxInvoiceDue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxInvoiceFreq.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabInvoices.ResumeLayout(False)
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.radPosted.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radNonPosted.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabActivity.ResumeLayout(False)
        Me.tabStatement.ResumeLayout(False)
        Me.tabPayments.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel2 As Panel
    Friend WithEvents btnEdit As Care.Controls.CareButton
    Friend WithEvents btnDelete As Care.Controls.CareButton
    Friend WithEvents btnAdd As Care.Controls.CareButton
    Friend WithEvents XtraTabControl1 As Care.Controls.CareTab
    Friend WithEvents tabFamily As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents chkEInvoicing As Care.Controls.CareCheckBox
    Friend WithEvents txtLetterName As Care.Controls.CareTextBox
    Friend WithEvents Label7 As Care.Controls.CareLabel
    Friend WithEvents Label4 As Care.Controls.CareLabel
    Friend WithEvents Label2 As Care.Controls.CareLabel
    Friend WithEvents tabStatement As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabInvoices As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabPayments As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GridChildren As Care.Controls.CareGrid
    Friend WithEvents GridContacts As Care.Controls.CareGrid
    Friend WithEvents GridStatement As Care.Controls.CareGrid
    Friend WithEvents GridInvoices As Care.Controls.CareGrid
    Friend WithEvents GridPayments As Care.Controls.CareGrid
    Friend WithEvents txtAddressFull As Care.Address.CareAddress
    Friend WithEvents btnInvoiceEdit As Care.Controls.CareButton
    Friend WithEvents btnPrintReport As Care.Controls.CareButton
    Friend WithEvents btnEmailReport As Care.Controls.CareButton
    Friend WithEvents btnCustomerCreate As Care.Controls.CareButton
    Friend WithEvents btnCustomerOpen As Care.Controls.CareButton
    Friend WithEvents btnCustomerStatement As Care.Controls.CareButton
    Friend WithEvents lblWarning As Care.Controls.CareLabel
    Friend WithEvents txtOpeningBalance As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents chkExclFinancials As Care.Controls.CareCheckBox
    Friend WithEvents chkExclInvoicing As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents cbxInvoiceDue As Care.Controls.CareComboBox
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents cbxInvoiceFreq As Care.Controls.CareComboBox
    Friend WithEvents tabActivity As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents crmFamily As Care.Shared.CRMActivity
    Friend WithEvents btnMerge As Care.Controls.CareButton
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
    Friend WithEvents txtAccountNo As Care.Controls.CareTextBox
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents btnDeleteInvoice As Care.Controls.CareButton
    Friend WithEvents txtSurname As Care.Controls.CareTextBox
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents lblSageAccount As Care.Controls.CareLabel
    Friend WithEvents lblBalance As Care.Controls.CareLabel
    Friend WithEvents radPosted As Care.Controls.CareRadioButton
    Friend WithEvents radNonPosted As Care.Controls.CareRadioButton
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents chkArchived As Care.Controls.CareCheckBox
    Friend WithEvents GroupBox1 As Care.Controls.CareFrame
    Friend WithEvents GroupBox2 As Care.Controls.CareFrame
    Friend WithEvents GroupControl1 As Care.Controls.CareFrame
    Friend WithEvents fraFinancials As Care.Controls.CareFrame
    Friend WithEvents GroupControl3 As Care.Controls.CareFrame
    Friend WithEvents GroupControl5 As Care.Controls.CareFrame
End Class
