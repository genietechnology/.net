﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLeadStages
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.GroupControl7 = New Care.Controls.CareFrame()
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.chkActionClose = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.chkActionQuote = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.chkActionList = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.chkActionViewing = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.chkActionContact = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.chkActionPack = New Care.Controls.CareCheckBox(Me.components)
        Me.GroupBox1 = New Care.Controls.CareFrame()
        Me.txtPcntComplete = New Care.Controls.CareTextBox(Me.components)
        Me.lblName = New Care.Controls.CareLabel(Me.components)
        Me.txtName = New Care.Controls.CareTextBox(Me.components)
        Me.Label2 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.cbxFields = New Care.Controls.CareComboBox(Me.components)
        Me.txtEmailBody = New DevExpress.XtraEditors.MemoEdit()
        Me.txtEmailSubject = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.chkEmail = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel10 = New Care.Controls.CareLabel(Me.components)
        Me.chkActionStart = New Care.Controls.CareCheckBox(Me.components)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl7.SuspendLayout()
        CType(Me.chkActionClose.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkActionQuote.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkActionList.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkActionViewing.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkActionContact.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkActionPack.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtPcntComplete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.cbxFields.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEmailBody.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEmailSubject.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkActionStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(404, 3)
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(313, 3)
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(0, 606)
        Me.Panel1.Size = New System.Drawing.Size(501, 36)
        Me.Panel1.TabIndex = 3
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'GroupControl7
        '
        Me.GroupControl7.Controls.Add(Me.CareLabel10)
        Me.GroupControl7.Controls.Add(Me.chkActionStart)
        Me.GroupControl7.Controls.Add(Me.CareLabel5)
        Me.GroupControl7.Controls.Add(Me.chkActionClose)
        Me.GroupControl7.Controls.Add(Me.CareLabel6)
        Me.GroupControl7.Controls.Add(Me.chkActionQuote)
        Me.GroupControl7.Controls.Add(Me.CareLabel7)
        Me.GroupControl7.Controls.Add(Me.chkActionList)
        Me.GroupControl7.Controls.Add(Me.CareLabel4)
        Me.GroupControl7.Controls.Add(Me.chkActionViewing)
        Me.GroupControl7.Controls.Add(Me.CareLabel11)
        Me.GroupControl7.Controls.Add(Me.chkActionContact)
        Me.GroupControl7.Controls.Add(Me.CareLabel9)
        Me.GroupControl7.Controls.Add(Me.chkActionPack)
        Me.GroupControl7.Location = New System.Drawing.Point(12, 101)
        Me.GroupControl7.Name = "GroupControl7"
        Me.GroupControl7.Size = New System.Drawing.Size(479, 128)
        Me.GroupControl7.TabIndex = 1
        Me.GroupControl7.Text = "Action Dates"
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(298, 79)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(70, 15)
        Me.CareLabel5.TabIndex = 12
        Me.CareLabel5.Text = "Update Close"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkActionClose
        '
        Me.chkActionClose.EnterMoveNextControl = True
        Me.chkActionClose.Location = New System.Drawing.Point(449, 77)
        Me.chkActionClose.Name = "chkActionClose"
        Me.chkActionClose.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkActionClose.Properties.Appearance.Options.UseFont = True
        Me.chkActionClose.Size = New System.Drawing.Size(20, 19)
        Me.chkActionClose.TabIndex = 13
        Me.chkActionClose.Tag = "AE"
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(298, 29)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(100, 15)
        Me.CareLabel6.TabIndex = 8
        Me.CareLabel6.Text = "Update Quote Sent"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkActionQuote
        '
        Me.chkActionQuote.EnterMoveNextControl = True
        Me.chkActionQuote.Location = New System.Drawing.Point(449, 27)
        Me.chkActionQuote.Name = "chkActionQuote"
        Me.chkActionQuote.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkActionQuote.Properties.Appearance.Options.UseFont = True
        Me.chkActionQuote.Size = New System.Drawing.Size(20, 19)
        Me.chkActionQuote.TabIndex = 9
        Me.chkActionQuote.Tag = "AE"
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(298, 54)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(140, 15)
        Me.CareLabel7.TabIndex = 10
        Me.CareLabel7.Text = "Update Joined Waiting List"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkActionList
        '
        Me.chkActionList.EnterMoveNextControl = True
        Me.chkActionList.Location = New System.Drawing.Point(449, 52)
        Me.chkActionList.Name = "chkActionList"
        Me.chkActionList.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkActionList.Properties.Appearance.Options.UseFont = True
        Me.chkActionList.Size = New System.Drawing.Size(20, 19)
        Me.chkActionList.TabIndex = 11
        Me.chkActionList.Tag = "AE"
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(11, 104)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(83, 15)
        Me.CareLabel4.TabIndex = 6
        Me.CareLabel4.Text = "Update Viewing"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkActionViewing
        '
        Me.chkActionViewing.EnterMoveNextControl = True
        Me.chkActionViewing.Location = New System.Drawing.Point(162, 102)
        Me.chkActionViewing.Name = "chkActionViewing"
        Me.chkActionViewing.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkActionViewing.Properties.Appearance.Options.UseFont = True
        Me.chkActionViewing.Size = New System.Drawing.Size(20, 19)
        Me.chkActionViewing.TabIndex = 7
        Me.chkActionViewing.Tag = "AE"
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(11, 54)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(96, 15)
        Me.CareLabel11.TabIndex = 2
        Me.CareLabel11.Text = "Update Contacted"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkActionContact
        '
        Me.chkActionContact.EnterMoveNextControl = True
        Me.chkActionContact.Location = New System.Drawing.Point(162, 52)
        Me.chkActionContact.Name = "chkActionContact"
        Me.chkActionContact.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkActionContact.Properties.Appearance.Options.UseFont = True
        Me.chkActionContact.Size = New System.Drawing.Size(20, 19)
        Me.chkActionContact.TabIndex = 3
        Me.chkActionContact.Tag = "AE"
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(11, 79)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(92, 15)
        Me.CareLabel9.TabIndex = 4
        Me.CareLabel9.Text = "Update Pack Sent"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkActionPack
        '
        Me.chkActionPack.EnterMoveNextControl = True
        Me.chkActionPack.Location = New System.Drawing.Point(162, 77)
        Me.chkActionPack.Name = "chkActionPack"
        Me.chkActionPack.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkActionPack.Properties.Appearance.Options.UseFont = True
        Me.chkActionPack.Size = New System.Drawing.Size(20, 19)
        Me.chkActionPack.TabIndex = 5
        Me.chkActionPack.Tag = "AE"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtPcntComplete)
        Me.GroupBox1.Controls.Add(Me.lblName)
        Me.GroupBox1.Controls.Add(Me.txtName)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 55)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.ShowCaption = False
        Me.GroupBox1.Size = New System.Drawing.Size(479, 40)
        Me.GroupBox1.TabIndex = 0
        '
        'txtPcntComplete
        '
        Me.txtPcntComplete.CharacterCasing = CharacterCasing.Normal
        Me.txtPcntComplete.EnterMoveNextControl = True
        Me.txtPcntComplete.Location = New System.Drawing.Point(432, 9)
        Me.txtPcntComplete.MaxLength = 3
        Me.txtPcntComplete.Name = "txtPcntComplete"
        Me.txtPcntComplete.NumericAllowNegatives = False
        Me.txtPcntComplete.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtPcntComplete.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPcntComplete.Properties.AccessibleName = "Surname"
        Me.txtPcntComplete.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPcntComplete.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtPcntComplete.Properties.Appearance.Options.UseFont = True
        Me.txtPcntComplete.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPcntComplete.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPcntComplete.Properties.Mask.EditMask = "##########;"
        Me.txtPcntComplete.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtPcntComplete.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPcntComplete.Properties.MaxLength = 3
        Me.txtPcntComplete.ReadOnly = False
        Me.txtPcntComplete.Size = New System.Drawing.Size(37, 20)
        Me.txtPcntComplete.TabIndex = 3
        Me.txtPcntComplete.Tag = "AE"
        Me.txtPcntComplete.TextAlign = HorizontalAlignment.Left
        Me.txtPcntComplete.ToolTipText = ""
        '
        'lblName
        '
        Me.lblName.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblName.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblName.Location = New System.Drawing.Point(11, 12)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(32, 15)
        Me.lblName.TabIndex = 0
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.CharacterCasing = CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(80, 9)
        Me.txtName.MaxLength = 40
        Me.txtName.Name = "txtName"
        Me.txtName.NumericAllowNegatives = False
        Me.txtName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AccessibleName = "Family"
        Me.txtName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtName.Properties.MaxLength = 40
        Me.txtName.ReadOnly = False
        Me.txtName.Size = New System.Drawing.Size(266, 20)
        Me.txtName.TabIndex = 1
        Me.txtName.Tag = "AEM"
        Me.txtName.TextAlign = HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'Label2
        '
        Me.Label2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label2.Location = New System.Drawing.Point(361, 12)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 15)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "% Complete"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.CareLabel8)
        Me.GroupControl1.Controls.Add(Me.cbxFields)
        Me.GroupControl1.Controls.Add(Me.txtEmailBody)
        Me.GroupControl1.Controls.Add(Me.txtEmailSubject)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Controls.Add(Me.chkEmail)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 235)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(479, 365)
        Me.GroupControl1.TabIndex = 2
        Me.GroupControl1.Text = "Email Configuration"
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(160, 29)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(94, 15)
        Me.CareLabel8.TabIndex = 13
        Me.CareLabel8.Text = "Insert Merge Field"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxFields
        '
        Me.cbxFields.AllowBlank = False
        Me.cbxFields.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxFields.DataSource = Nothing
        Me.cbxFields.DisplayMember = Nothing
        Me.cbxFields.EnterMoveNextControl = True
        Me.cbxFields.Location = New System.Drawing.Point(260, 26)
        Me.cbxFields.Name = "cbxFields"
        Me.cbxFields.Properties.AccessibleName = "Gender"
        Me.cbxFields.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxFields.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxFields.Properties.Appearance.Options.UseFont = True
        Me.cbxFields.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxFields.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxFields.ReadOnly = False
        Me.cbxFields.SelectedValue = Nothing
        Me.cbxFields.Size = New System.Drawing.Size(209, 20)
        Me.cbxFields.TabIndex = 5
        Me.cbxFields.Tag = ""
        Me.cbxFields.ValueMember = Nothing
        '
        'txtEmailBody
        '
        Me.txtEmailBody.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtEmailBody.Location = New System.Drawing.Point(88, 79)
        Me.txtEmailBody.Name = "txtEmailBody"
        Me.txtEmailBody.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmailBody.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtEmailBody, True)
        Me.txtEmailBody.Size = New System.Drawing.Size(381, 277)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtEmailBody, OptionsSpelling1)
        Me.txtEmailBody.TabIndex = 2
        Me.txtEmailBody.UseOptimizedRendering = True
        '
        'txtEmailSubject
        '
        Me.txtEmailSubject.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtEmailSubject.CharacterCasing = CharacterCasing.Normal
        Me.txtEmailSubject.EnterMoveNextControl = True
        Me.txtEmailSubject.Location = New System.Drawing.Point(88, 51)
        Me.txtEmailSubject.MaxLength = 1000
        Me.txtEmailSubject.Name = "txtEmailSubject"
        Me.txtEmailSubject.NumericAllowNegatives = False
        Me.txtEmailSubject.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtEmailSubject.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtEmailSubject.Properties.AccessibleName = "Family"
        Me.txtEmailSubject.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtEmailSubject.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtEmailSubject.Properties.Appearance.Options.UseFont = True
        Me.txtEmailSubject.Properties.Appearance.Options.UseTextOptions = True
        Me.txtEmailSubject.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtEmailSubject.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtEmailSubject.Properties.MaxLength = 1000
        Me.txtEmailSubject.ReadOnly = False
        Me.txtEmailSubject.Size = New System.Drawing.Size(381, 20)
        Me.txtEmailSubject.TabIndex = 1
        Me.txtEmailSubject.Tag = ""
        Me.txtEmailSubject.TextAlign = HorizontalAlignment.Left
        Me.txtEmailSubject.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(11, 29)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(67, 15)
        Me.CareLabel1.TabIndex = 2
        Me.CareLabel1.Text = "Email Popup"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkEmail
        '
        Me.chkEmail.EnterMoveNextControl = True
        Me.chkEmail.Location = New System.Drawing.Point(86, 27)
        Me.chkEmail.Name = "chkEmail"
        Me.chkEmail.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEmail.Properties.Appearance.Options.UseFont = True
        Me.chkEmail.Size = New System.Drawing.Size(20, 19)
        Me.chkEmail.TabIndex = 0
        Me.chkEmail.Tag = "AE"
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(11, 54)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(39, 15)
        Me.CareLabel2.TabIndex = 4
        Me.CareLabel2.Text = "Subject"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(11, 81)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(27, 15)
        Me.CareLabel3.TabIndex = 0
        Me.CareLabel3.Text = "Body"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel10
        '
        Me.CareLabel10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel10.Location = New System.Drawing.Point(11, 29)
        Me.CareLabel10.Name = "CareLabel10"
        Me.CareLabel10.Size = New System.Drawing.Size(122, 15)
        Me.CareLabel10.TabIndex = 0
        Me.CareLabel10.Text = "Update Start of Process"
        Me.CareLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkActionStart
        '
        Me.chkActionStart.EnterMoveNextControl = True
        Me.chkActionStart.Location = New System.Drawing.Point(162, 27)
        Me.chkActionStart.Name = "chkActionStart"
        Me.chkActionStart.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkActionStart.Properties.Appearance.Options.UseFont = True
        Me.chkActionStart.Size = New System.Drawing.Size(20, 19)
        Me.chkActionStart.TabIndex = 1
        Me.chkActionStart.Tag = "AE"
        '
        'frmLeadStages
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(501, 642)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.GroupControl7)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmLeadStages"
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.GroupControl7, 0)
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl7.ResumeLayout(False)
        Me.GroupControl7.PerformLayout()
        CType(Me.chkActionClose.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkActionQuote.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkActionList.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkActionViewing.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkActionContact.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkActionPack.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.txtPcntComplete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.cbxFields.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEmailBody.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEmailSubject.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkActionStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl7 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents chkActionClose As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents chkActionQuote As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents chkActionList As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents chkActionViewing As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents chkActionContact As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents chkActionPack As Care.Controls.CareCheckBox
    Friend WithEvents GroupBox1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtPcntComplete As Care.Controls.CareTextBox
    Friend WithEvents lblName As Care.Controls.CareLabel
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents Label2 As Care.Controls.CareLabel
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtEmailBody As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtEmailSubject As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents chkEmail As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents cbxFields As Care.Controls.CareComboBox
    Friend WithEvents CareLabel10 As Care.Controls.CareLabel
    Friend WithEvents chkActionStart As Care.Controls.CareCheckBox

End Class
