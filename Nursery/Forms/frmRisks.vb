﻿

Imports Care.Global
Imports Care.Data
Imports Care.Shared

Public Class frmRisks

    Private m_Risk As Business.Risk
    Private m_FormCaption As String = ""

    Private Sub frmSites_Load(sender As Object, e As EventArgs) Handles Me.Load

        m_FormCaption = Me.Text

        cbxSite.PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)
        cbxArea.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("RiskArea"))

        With cbxStatus
            .AddItem("Current", "C")
            .AddItem("Archived", "A")
        End With

        With cbxFreq
            .AddItem("Every week")
            .AddItem("Every fornight")
            .AddItem("Every month")
            .AddItem("Every 6 weeks")
            .AddItem("Every quarter")
            .AddItem("Every 6 months")
            .AddItem("Every year")
        End With

        cgHazards.ButtonsEnabled = False
        tabMain.DisableTabs()

    End Sub

#Region "Overrides"

    Protected Overrides Sub AfterAdd()
        cgHazards.Clear()
        txtTitle.Focus()
    End Sub

    Protected Overrides Sub AfterEdit()

        Select Case tabMain.SelectedTabPage.Name

            Case "tabRisk"
                cgHazards.ButtonsEnabled = True
                txtTitle.Focus()

            Case "tabHistory"
                cgHistory.ButtonsEnabled = True

        End Select

    End Sub

    Protected Overrides Sub SetBindings()

        m_Risk = New Business.Risk
        bs.DataSource = m_Risk

        txtTitle.DataBindings.Add("Text", bs, "_Title")
        cbxStatus.DataBindings.Add("Text", bs, "_Status")
        cbxArea.DataBindings.Add("Text", bs, "_Area")
        cbxFreq.DataBindings.Add("Text", bs, "_AssFreq")
        'txtRevision.DataBindings.Add("Text", bs, "_Revision")

    End Sub

    Protected Overrides Sub AfterAcceptChanges()
        cgHazards.ButtonsEnabled = False
    End Sub

    Protected Overrides Sub AfterCancelChanges()
        cgHazards.ButtonsEnabled = False
    End Sub

    Protected Overrides Sub CommitDelete()

        Dim sql As String = ""

        sql = "delete from risks where id = '" & m_Risk._ID.ToString & "'"
        DAL.ExecuteSQL(Session.ConnectionString, sql)

        sql = "delete from riskactivities where risk_id = '" & m_Risk._ID.ToString & "'"
        DAL.ExecuteSQL(Session.ConnectionString, sql)

        cgHazards.Clear()

    End Sub

    Protected Overrides Sub CommitUpdate()

        m_Risk = CType(bs.Item(bs.Position), Business.Risk)

        m_Risk._SiteId = New Guid(cbxSite.SelectedValue.ToString)
        m_Risk._SiteName = cbxSite.Text

        m_Risk.Store()

        If IsNothing(MyBase.RecordID) Then
            MyBase.RecordID = m_Risk._ID
            MyBase.RecordPopulated = True
        End If

    End Sub

    Protected Overrides Sub FindRecord()

        Dim _Find As New GenericFind

        Dim _SQL As String = "select title as 'Title', site_name as 'Site', area as 'Area', status as 'Status' from Risks"

        With _Find
            .Caption = "Find Risk Assessment"
            .ParentForm = ParentForm
            .PopulateOnLoad = True
            .ConnectionString = Session.ConnectionString
            .GridSQL = _SQL
            .GridWhereClause = " and site_id not in (select site_id from SiteExcludedUsers where user_id = '" + Session.CurrentUser.ID.ToString + "')"
            .GridOrderBy = "order by title"
            .ReturnField = "ID"
            .Show()
        End With

        If _Find.ReturnValue <> "" Then

            MyBase.RecordID = New Guid(_Find.ReturnValue)
            MyBase.RecordPopulated = True

            m_Risk = Business.Risk.RetreiveByID(New Guid(_Find.ReturnValue))
            bs.DataSource = m_Risk

            DisplayRecord()
            tabMain.EnableTabs()

        End If

    End Sub

#End Region

    Private Sub tabMain_SelectedPageChanging(sender As Object, e As DevExpress.XtraTab.TabPageChangedEventArgs) Handles tabMain.SelectedPageChanging

        Select Case e.Page.Name

            Case "tabRisk"
                Me.ToolbarMode = ToolbarEnum.Standard

            Case "tabHistory", "tabEquipment"
                Me.ToolbarMode = ToolbarEnum.FindandEdit

            Case "tabIncidents"
                Me.ToolbarMode = ToolbarEnum.FindOnly

        End Select

    End Sub

    Private Sub DisplayRecord()

        If Me.RecordPopulated = False Then Exit Sub

        Me.Text = m_FormCaption + " - " + m_Risk._Title

        Select Case tabMain.SelectedTabPage.Name

            Case "tabRisk"
                cbxSite.SelectedValue = New Guid(m_Risk._SiteId.ToString)
                cbxSite.Text = m_Risk._SiteName

                DisplayHazards()

            Case "tabHistory"
                DisplayHistory()

            Case "tabEquipment"
                DisplayEquipment()

            Case "tabIncidents"
                DisplayIncidents()

        End Select

    End Sub

    Private Sub tabMain_SelectedPageChanged(sender As Object, e As DevExpress.XtraTab.TabPageChangedEventArgs) Handles tabMain.SelectedPageChanged
        DisplayRecord()
    End Sub

    Private Sub DisplayEquipment()

    End Sub

    Private Sub DisplayHistory()

    End Sub

    Private Sub DisplayIncidents()

    End Sub

    Private Sub DisplayHazards()

        Dim _SQL As String = ""

        _SQL += "select ID, activity as 'Activity', hazard_desc as 'Hazard', risk_desc as 'Risk', people_desc as 'Who', uc_rating as 'Un-Controlled', c_rating as 'Residual'"
        _SQL += " from RiskActivities"
        _SQL += " where risk_id = '" + Me.RecordID.ToString + "'"
        _SQL += " order by activity"

        cgHazards.HideFirstColumn = True
        cgHazards.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub cgHazards_AddClick(sender As Object, e As EventArgs) Handles cgHazards.AddClick
        EditHazard(Nothing)
    End Sub

    Private Sub cgHazards_EditClick(sender As Object, e As EventArgs) Handles cgHazards.EditClick
        EditHazard(GetID(cgHazards))
    End Sub

    Private Sub cgHazards_RemoveClick(sender As Object, e As EventArgs) Handles cgHazards.RemoveClick

        Dim activityID As Guid? = GetID(cgHazards)
        If activityID.HasValue Then

            If CareMessage("Are you sure you want to Remove this Activity?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Remove Activity") = DialogResult.Yes Then

                Dim sql As String = ""
                sql = "delete from riskactivities where id = '" & activityID.ToString & "'"
                DAL.ExecuteSQL(Session.ConnectionString, sql)

                cgHazards.RePopulate()


            End If

        End If

    End Sub

    Private Sub EditHazard(ByVal RoomID As Guid?)

        Dim _frm As New frmRiskHazard(m_Risk._ID.Value, RoomID)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            DisplayHazards()
        End If

        _frm.Dispose()
        _frm = Nothing

    End Sub

    Private Function GetID(ByRef GridControl As Care.Controls.CareGridWithButtons) As Guid?

        If GridControl Is Nothing Then Return Nothing
        If GridControl.RecordCount < 0 Then Return Nothing
        If GridControl.CurrentRow Is Nothing Then Return Nothing

        Dim _ID As String = GridControl.CurrentRow.Item("ID").ToString

        Return New Guid(_ID)

    End Function

    Private Sub cgHazards_RowCellStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs) Handles cgHazards.RowCellStyle

        If e.RowHandle < 0 Then Exit Sub

        Select Case e.Column.FieldName

            Case "Un-Controlled"
                Dim _Rating As Integer = ValueHandler.ConvertInteger(cgHazards.UnderlyingGridView.GetRowCellValue(e.RowHandle, e.Column.FieldName))
                e.Appearance.BackColor = ReturnRatingColour(_Rating)

            Case "Residual"
                Dim _Rating As Integer = ValueHandler.ConvertInteger(cgHazards.UnderlyingGridView.GetRowCellValue(e.RowHandle, e.Column.FieldName))
                e.Appearance.BackColor = ReturnRatingColour(_Rating)


        End Select

    End Sub

    Private Function ReturnRatingColour(ByVal Rating As Integer) As Drawing.Color
        If Rating < 3 Then
            Return Drawing.Color.LawnGreen
        Else
            If Rating > 5 Then
                Return Drawing.Color.Tomato
            Else
                Return Drawing.Color.Orange
            End If
        End If
    End Function

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click

        Dim _SQL As String = ""
        _SQL += "select Risks.ID, title, site_name, area, status, rev, created, created_by, ass_freq,"
        _SQL += " activity, hazard_desc, risk_desc, people_desc, uc_likely, uc_severity, uc_rating,"
        _SQL += " controls, c_likely, c_severity, c_rating"
        _SQL += " from Risks"
        _SQL += " left join RiskActivities on risk_id = Risks.ID"
        _SQL += " where Risks.ID = '" + Me.RecordID.ToString + "'"
        _SQL += " order by activity"

        Session.CursorWaiting()

        If btnPrint.ShiftDown Then
            btnPrint.Enabled = False
            ReportHandler.DesignReport("RiskAssessment.repx", Session.ConnectionString, _SQL)
            btnPrint.Enabled = True
        Else
            btnPrint.Enabled = False
            ReportHandler.RunReport("RiskAssessment.repx", Session.ConnectionString, _SQL)
            btnPrint.Enabled = True
        End If

    End Sub
End Class
