﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDataImport
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDataImport))
        Dim OptionsSpelling2 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.ofd = New OpenFileDialog()
        Me.Wiz = New DevExpress.XtraWizard.WizardControl()
        Me.wpWelcome = New DevExpress.XtraWizard.WelcomeWizardPage()
        Me.wpComplete = New DevExpress.XtraWizard.CompletionWizardPage()
        Me.wpSpreadsheet = New DevExpress.XtraWizard.WizardPage()
        Me.sc = New DevExpress.XtraSpreadsheet.SpreadsheetControl()
        Me.wpPreview = New DevExpress.XtraWizard.WizardPage()
        Me.cgRecords = New Care.Controls.CareGrid()
        Me.wpDefaults = New DevExpress.XtraWizard.WizardPage()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.cbxKeyworker = New Care.Controls.CareComboBox()
        Me.cbxRoom = New Care.Controls.CareComboBox()
        Me.Label15 = New Care.Controls.CareLabel()
        Me.cdtStartDate = New Care.Controls.CareDateTime()
        Me.Label13 = New Care.Controls.CareLabel()
        Me.Label14 = New Care.Controls.CareLabel()
        Me.wpProgress = New DevExpress.XtraWizard.WizardPage()
        Me.memProgress = New DevExpress.XtraEditors.MemoEdit()
        Me.cbxSite = New Care.Controls.CareComboBox()
        Me.CareLabel1 = New Care.Controls.CareLabel()
        CType(Me.Wiz, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Wiz.SuspendLayout()
        Me.wpSpreadsheet.SuspendLayout()
        Me.wpPreview.SuspendLayout()
        Me.wpDefaults.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.cbxKeyworker.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxRoom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtStartDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtStartDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wpProgress.SuspendLayout()
        CType(Me.memProgress.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'Wiz
        '
        Me.Wiz.Appearance.ExteriorPage.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Wiz.Appearance.ExteriorPage.Options.UseFont = True
        Me.Wiz.Appearance.PageTitle.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Wiz.Appearance.PageTitle.Options.UseFont = True
        Me.Wiz.Controls.Add(Me.wpWelcome)
        Me.Wiz.Controls.Add(Me.wpComplete)
        Me.Wiz.Controls.Add(Me.wpSpreadsheet)
        Me.Wiz.Controls.Add(Me.wpPreview)
        Me.Wiz.Controls.Add(Me.wpDefaults)
        Me.Wiz.Controls.Add(Me.wpProgress)
        Me.Wiz.Dock = DockStyle.Fill
        Me.Wiz.Location = New System.Drawing.Point(0, 0)
        Me.Wiz.Name = "Wiz"
        Me.Wiz.Pages.AddRange(New DevExpress.XtraWizard.BaseWizardPage() {Me.wpWelcome, Me.wpSpreadsheet, Me.wpDefaults, Me.wpPreview, Me.wpProgress, Me.wpComplete})
        Me.Wiz.Size = New System.Drawing.Size(851, 450)
        '
        'wpWelcome
        '
        Me.wpWelcome.IntroductionText = resources.GetString("wpWelcome.IntroductionText")
        Me.wpWelcome.Name = "wpWelcome"
        Me.wpWelcome.ProceedText = "To continue and select your Spreadsheet, click Next"
        Me.wpWelcome.Size = New System.Drawing.Size(634, 317)
        Me.wpWelcome.Text = "Welcome to the Data Import Wizard"
        '
        'wpComplete
        '
        Me.wpComplete.FinishText = "Congratulations!" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "You have successfully completed the import wizard." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.wpComplete.Name = "wpComplete"
        Me.wpComplete.Size = New System.Drawing.Size(634, 317)
        Me.wpComplete.Text = "Import Complete"
        '
        'wpSpreadsheet
        '
        Me.wpSpreadsheet.Controls.Add(Me.sc)
        Me.wpSpreadsheet.DescriptionText = "Check this is correct spreadsheet, then click Next when ready..."
        Me.wpSpreadsheet.Name = "wpSpreadsheet"
        Me.wpSpreadsheet.Size = New System.Drawing.Size(819, 305)
        Me.wpSpreadsheet.Text = "Spreadsheet Preview"
        '
        'sc
        '
        Me.sc.Dock = DockStyle.Fill
        Me.sc.Location = New System.Drawing.Point(0, 0)
        Me.sc.Name = "sc"
        Me.sc.Size = New System.Drawing.Size(819, 305)
        Me.sc.TabIndex = 2
        Me.sc.Text = "SpreadsheetControl1"
        '
        'wpPreview
        '
        Me.wpPreview.Controls.Add(Me.cgRecords)
        Me.wpPreview.DescriptionText = "Providing all your data is valid, Click Next to import your data..."
        Me.wpPreview.Name = "wpPreview"
        Me.wpPreview.Size = New System.Drawing.Size(819, 305)
        Me.wpPreview.Text = "Import Preview"
        '
        'cgRecords
        '
        Me.cgRecords.AllowBuildColumns = True
        Me.cgRecords.AllowEdit = False
        Me.cgRecords.AllowHorizontalScroll = True
        Me.cgRecords.AllowMultiSelect = False
        Me.cgRecords.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgRecords.Appearance.Options.UseFont = True
        Me.cgRecords.AutoSizeByData = True
        Me.cgRecords.DisableAutoSize = False
        Me.cgRecords.DisableDataFormatting = False
        Me.cgRecords.Dock = DockStyle.Fill
        Me.cgRecords.FocusedRowHandle = -2147483648
        Me.cgRecords.HideFirstColumn = False
        Me.cgRecords.Location = New System.Drawing.Point(0, 0)
        Me.cgRecords.Name = "cgRecords"
        Me.cgRecords.PreviewColumn = ""
        Me.cgRecords.QueryID = Nothing
        Me.cgRecords.RowAutoHeight = False
        Me.cgRecords.SearchAsYouType = True
        Me.cgRecords.ShowAutoFilterRow = False
        Me.cgRecords.ShowFindPanel = False
        Me.cgRecords.ShowGroupByBox = True
        Me.cgRecords.ShowLoadingPanel = False
        Me.cgRecords.ShowNavigator = False
        Me.cgRecords.Size = New System.Drawing.Size(819, 305)
        Me.cgRecords.TabIndex = 6
        '
        'wpDefaults
        '
        Me.wpDefaults.Controls.Add(Me.GroupControl1)
        Me.wpDefaults.DescriptionText = "Override certain elements depending on your requirements. Click Next to preview y" & _
    "our import..."
        Me.wpDefaults.Name = "wpDefaults"
        Me.wpDefaults.Size = New System.Drawing.Size(819, 305)
        Me.wpDefaults.Text = "Override Data"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Controls.Add(Me.cbxSite)
        Me.GroupControl1.Controls.Add(Me.cbxKeyworker)
        Me.GroupControl1.Controls.Add(Me.cbxRoom)
        Me.GroupControl1.Controls.Add(Me.Label15)
        Me.GroupControl1.Controls.Add(Me.cdtStartDate)
        Me.GroupControl1.Controls.Add(Me.Label13)
        Me.GroupControl1.Controls.Add(Me.Label14)
        Me.GroupControl1.Dock = DockStyle.Fill
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(819, 305)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "GroupControl1"
        '
        'cbxKeyworker
        '
        Me.cbxKeyworker.AllowBlank = False
        Me.cbxKeyworker.DataSource = Nothing
        Me.cbxKeyworker.DisplayMember = Nothing
        Me.cbxKeyworker.EnterMoveNextControl = True
        Me.cbxKeyworker.Location = New System.Drawing.Point(115, 67)
        Me.cbxKeyworker.Name = "cbxKeyworker"
        Me.cbxKeyworker.Properties.AccessibleName = "Keyworker"
        Me.cbxKeyworker.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxKeyworker.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxKeyworker.Properties.Appearance.Options.UseFont = True
        Me.cbxKeyworker.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxKeyworker.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxKeyworker.SelectedValue = Nothing
        Me.cbxKeyworker.Size = New System.Drawing.Size(322, 22)
        Me.cbxKeyworker.TabIndex = 5
        Me.cbxKeyworker.Tag = "AEBM"
        Me.cbxKeyworker.ValueMember = Nothing
        '
        'cbxRoom
        '
        Me.cbxRoom.AllowBlank = False
        Me.cbxRoom.DataSource = Nothing
        Me.cbxRoom.DisplayMember = Nothing
        Me.cbxRoom.EnterMoveNextControl = True
        Me.cbxRoom.Location = New System.Drawing.Point(115, 39)
        Me.cbxRoom.Name = "cbxRoom"
        Me.cbxRoom.Properties.AccessibleName = "Group"
        Me.cbxRoom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxRoom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxRoom.Properties.Appearance.Options.UseFont = True
        Me.cbxRoom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxRoom.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxRoom.SelectedValue = Nothing
        Me.cbxRoom.Size = New System.Drawing.Size(322, 22)
        Me.cbxRoom.TabIndex = 3
        Me.cbxRoom.Tag = "AEBM"
        Me.cbxRoom.ValueMember = Nothing
        '
        'Label15
        '
        Me.Label15.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label15.Location = New System.Drawing.Point(12, 98)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(51, 15)
        Me.Label15.TabIndex = 6
        Me.Label15.Text = "Start Date"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label15.ToolTip = "The group start date indicates when the child started in the particular group."
        Me.Label15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'cdtStartDate
        '
        Me.cdtStartDate.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtStartDate.EnterMoveNextControl = True
        Me.cdtStartDate.Location = New System.Drawing.Point(115, 95)
        Me.cdtStartDate.Name = "cdtStartDate"
        Me.cdtStartDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtStartDate.Properties.Appearance.Options.UseFont = True
        Me.cdtStartDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtStartDate.Size = New System.Drawing.Size(104, 22)
        Me.cdtStartDate.TabIndex = 7
        Me.cdtStartDate.Tag = ""
        Me.cdtStartDate.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'Label13
        '
        Me.Label13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label13.Location = New System.Drawing.Point(12, 70)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(55, 15)
        Me.Label13.TabIndex = 4
        Me.Label13.Text = "Keyworker"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label14
        '
        Me.Label14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label14.Location = New System.Drawing.Point(12, 42)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(32, 15)
        Me.Label14.TabIndex = 2
        Me.Label14.Text = "Room"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'wpProgress
        '
        Me.wpProgress.Controls.Add(Me.memProgress)
        Me.wpProgress.DescriptionText = "View the progress of your import..."
        Me.wpProgress.Name = "wpProgress"
        Me.wpProgress.Size = New System.Drawing.Size(819, 305)
        Me.wpProgress.Text = "Import Progress"
        '
        'memProgress
        '
        Me.memProgress.Dock = DockStyle.Fill
        Me.memProgress.Location = New System.Drawing.Point(0, 0)
        Me.memProgress.Name = "memProgress"
        Me.memProgress.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.memProgress, True)
        Me.memProgress.Size = New System.Drawing.Size(819, 305)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.memProgress, OptionsSpelling2)
        Me.memProgress.TabIndex = 0
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(115, 11)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AccessibleName = "Keyworker"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(322, 22)
        Me.cbxSite.TabIndex = 1
        Me.cbxSite.Tag = "AEBM"
        Me.cbxSite.ValueMember = Nothing
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(12, 14)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(19, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Site"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmDataImport
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(851, 450)
        Me.Controls.Add(Me.Wiz)
        Me.Margin = New Padding(3, 5, 3, 5)
        Me.Name = "frmDataImport"
        Me.Text = "frmDataImport"
        Me.WindowState = FormWindowState.Maximized
        CType(Me.Wiz, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Wiz.ResumeLayout(False)
        Me.wpSpreadsheet.ResumeLayout(False)
        Me.wpPreview.ResumeLayout(False)
        Me.wpDefaults.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.cbxKeyworker.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxRoom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtStartDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtStartDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wpProgress.ResumeLayout(False)
        CType(Me.memProgress.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ofd As OpenFileDialog
    Friend WithEvents Wiz As DevExpress.XtraWizard.WizardControl
    Friend WithEvents wpWelcome As DevExpress.XtraWizard.WelcomeWizardPage
    Friend WithEvents wpComplete As DevExpress.XtraWizard.CompletionWizardPage
    Friend WithEvents wpSpreadsheet As DevExpress.XtraWizard.WizardPage
    Friend WithEvents sc As DevExpress.XtraSpreadsheet.SpreadsheetControl
    Friend WithEvents wpPreview As DevExpress.XtraWizard.WizardPage
    Friend WithEvents cgRecords As Care.Controls.CareGrid
    Friend WithEvents wpDefaults As DevExpress.XtraWizard.WizardPage
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents wpProgress As DevExpress.XtraWizard.WizardPage
    Friend WithEvents cbxKeyworker As Care.Controls.CareComboBox
    Friend WithEvents cbxRoom As Care.Controls.CareComboBox
    Friend WithEvents Label15 As Care.Controls.CareLabel
    Private WithEvents cdtStartDate As Care.Controls.CareDateTime
    Friend WithEvents Label13 As Care.Controls.CareLabel
    Friend WithEvents Label14 As Care.Controls.CareLabel
    Friend WithEvents memProgress As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
End Class
