﻿
Imports Care.Global

Public Class frmReportIntegrity

    Private m_SQL As String = ""

    Private Sub RefreshGrid()

        grdResults.Clear()

        If m_SQL = "" Then Exit Sub

        grdResults.Populate(Session.ConnectionString, m_SQL)

        If radAddressee.Checked Then
            'addressee does not use the TYPE and ID field
        Else
            If grdResults.Columns.Count > 0 Then
                grdResults.Columns(0).Visible = False
                grdResults.Columns(1).Visible = False
            End If
        End If

    End Sub

    Private Sub radNoContacts_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles radNoContacts.CheckedChanged

        If Not radNoContacts.Checked Then Exit Sub

        m_SQL = "select 'FAMILY' as 'type', family.id as 'family_id', family.surname as 'Surname'" & _
                " from Family" & _
                " where (select count (*) from Contacts where Contacts.family_id = Family.id) = 0" & _
                " and archived = 0" & _
                " order by family.surname"

        RefreshGrid()

    End Sub

    Private Sub radNoPrimary_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles radNoPrimary.CheckedChanged

        If Not radNoPrimary.Checked Then Exit Sub

        m_SQL = "select 'FAMILY' as 'type', family.id as 'family_id', family.surname as 'Surname'" & _
                " from Family" & _
                " where (select count (*) from Contacts where Contacts.family_id = Family.id and Contacts.primary_cont = 1) = 0" & _
                " and archived = 0" & _
                " order by family.surname"

        RefreshGrid()

    End Sub

    Private Sub radPrimaryNoEmail_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles radPrimaryNoEmail.CheckedChanged

        If Not radPrimaryNoEmail.Checked Then Exit Sub

        m_SQL = "select 'FAMILY' as 'type', family_id as 'family_id', fullname as 'Name'" & _
                " from contacts" & _
                " left join Family on Family.ID = Contacts.family_id" & _
                " where primary_cont = 1" & _
                " and len(email) = 0" & _
                " and archived = 0" & _
                " order by family.surname"

        RefreshGrid()

    End Sub

    Private Sub radElecInv_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles radElecInv.CheckedChanged

        If Not radElecInv.Checked Then Exit Sub

        m_SQL = "select 'FAMILY' as 'type', family_id as 'family_id', fullname as 'Name'" & _
                " from contacts" & _
                " left join Family on Family.ID = Contacts.family_id" & _
                " where primary_cont = 1" & _
                " and len(email) = 0" & _
                " and e_invoicing = 1" & _
                " and archived = 0" & _
                " order by family.surname"

        RefreshGrid()

    End Sub

    Private Sub radInvalidMobiles_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles radInvalidMobiles.CheckedChanged

        If Not radInvalidMobiles.Checked Then Exit Sub

        m_SQL = "select 'FAMILY' as 'type', family_id as 'family_id', fullname as 'Name', replace(tel_mobile,' ','') as 'Mobile'" & _
                " from contacts" & _
                " left join Family on Family.ID = Contacts.family_id" & _
                " where primary_cont = 1" & _
                " and len(tel_mobile) > 0" & _
                " and (LEN(replace(tel_mobile,' ','')) <> 11 OR SUBSTRING(replace(tel_mobile,' ',''),1,2) <> '07')" & _
                " and archived = 0" & _
                " order by family.surname"

        RefreshGrid()

    End Sub

    Private Sub radPrimaryNoMobile_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles radPrimaryNoMobile.CheckedChanged

        If Not radPrimaryNoMobile.Checked Then Exit Sub

        m_SQL = "select 'FAMILY' as 'type', family_id as 'family_id', fullname as 'Name'" & _
                " from contacts" & _
                " left join Family on Family.ID = Contacts.family_id" & _
                " where primary_cont = 1" & _
                " and len(tel_mobile) = 0" & _
                " and archived = 0" & _
                " order by family.surname"

        RefreshGrid()

    End Sub

    Private Sub radMealsNoComponents_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles radMealsNoComponents.CheckedChanged

        If Not radMealsNoComponents.Checked Then Exit Sub

        m_SQL = "select Meals.id, Meals.description" & _
                " from Meals" & _
                " where" & _
                " (select count(*) from MealComponents where MealComponents.meal_id = Meals.ID) = 0" & _
                " order by Meals.description"

        RefreshGrid()

    End Sub

    Private Sub radInvalidEmail_CheckedChanged(sender As Object, e As EventArgs) Handles radInvalidEmail.CheckedChanged

        If Not radInvalidEmail.Checked Then Exit Sub

        m_SQL = ""
        m_SQL += "select 'FAMILY' as 'type', family_id as 'family_id', fullname as 'Name', email as 'Email Address'"
        m_SQL += " from contacts"
        m_SQL += " left join Family on Family.ID = Contacts.family_id"
        m_SQL += " where primary_cont = 1"
        m_SQL += " and len(email) > 0"
        m_SQL += " and archived = 0"
        m_SQL += " AND"
        m_SQL += " ("
        m_SQL += " email not like '%@%' OR"
        m_SQL += " email not like '%.co%'"
        m_SQL += " AND email not like '%.net%'"
        m_SQL += " AND email not like '%.ie%'"
        m_SQL += " AND email not like '%.wales%'"
        m_SQL += " AND email not like '%.cymru%'"
        m_SQL += " AND email not like '%.ac.uk%'"
        m_SQL += " AND email not like '%.gov%'"
        m_SQL += " AND email not like '%.police%'"
        m_SQL += " AND email not like '%.org%'"
        m_SQL += " AND email not like '%.uk%'"
        m_SQL += ")"
        m_SQL += " order by family.surname"

        RefreshGrid()

    End Sub

    Private Sub grdResults_GridDoubleClick(sender As Object, e As EventArgs) Handles grdResults.GridDoubleClick

        Select Case grdResults.CurrentRow.Item("type").ToString.ToUpper

            Case "FAMILY"
                Business.Family.DrillDown(New Guid(grdResults.CurrentRow.Item("family_id").ToString))

            Case "CHILD"
                Business.Child.DrillDown(New Guid(grdResults.CurrentRow.Item("child_id").ToString))

        End Select

    End Sub

    Private Sub radAddressee_CheckedChanged(sender As Object, e As EventArgs) Handles radAddressee.CheckedChanged

        If Not radAddressee.Checked Then Exit Sub

        m_SQL = ""

        m_SQL += "select letter_name as 'Addressee', count(*) 'Count' from Family"
        m_SQL += " where archived = 0"
        m_SQL += " group by letter_name"
        m_SQL += " having COUNT(*) > 1"

        RefreshGrid()

    End Sub

    Private Sub radSiteFamily_CheckedChanged(sender As Object, e As EventArgs) Handles radSiteFamily.CheckedChanged

        If Not radSiteFamily.Checked Then Exit Sub

        m_SQL = "select 'FAMILY' as 'type', id as 'family_id', surname as 'Surname'" & _
        " from Family" & _
        " where site_id is null" & _
        " and archived = 0" & _
        " order by surname"

        RefreshGrid()

    End Sub

    Private Sub radSiteChild_CheckedChanged(sender As Object, e As EventArgs) Handles radSiteChild.CheckedChanged

        If Not radSiteChild.Checked Then Exit Sub

        m_SQL = "select 'CHILD' as 'type', c.id as 'child_id', c.fullname as 'Name', c.group_name as 'Group'" & _
        " from Children c" & _
        " left join Family f on f.id = c.family_id" & _
        " where c.site_id is null" & _
        " and f.archived = 0" & _
        " and c.status <> 'Left'" & _
        " order by c.surname"

        RefreshGrid()

    End Sub

    Private Sub btnInvalidOtherMobile_CheckedChanged(sender As Object, e As EventArgs) Handles btnInvalidOtherMobile.CheckedChanged

        If Not btnInvalidOtherMobile.Checked Then Exit Sub

        m_SQL = "select 'FAMILY' as 'type', family_id as 'family_id', fullname as 'Name', replace(tel_mobile,' ','') as 'Mobile'" & _
                " from contacts" & _
                " left join Family on Family.ID = Contacts.family_id" & _
                " where primary_cont = 0" & _
                " and len(tel_mobile) > 0" & _
                " and (LEN(replace(tel_mobile,' ','')) <> 11 OR SUBSTRING(replace(tel_mobile,' ',''),1,2) <> '07')" & _
                " and archived = 0" & _
                " order by family.surname"

        RefreshGrid()

    End Sub

    Private Sub btnInvalidOtherEmail_CheckedChanged(sender As Object, e As EventArgs) Handles btnInvalidOtherEmail.CheckedChanged

        If Not btnInvalidOtherEmail.Checked Then Exit Sub

        m_SQL = ""
        m_SQL += "select 'FAMILY' as 'type', family_id as 'family_id', fullname as 'Name', email as 'Email Address'"
        m_SQL += " from contacts"
        m_SQL += " left join Family on Family.ID = Contacts.family_id"
        m_SQL += " where primary_cont = 0"
        m_SQL += " and len(email) > 0"
        m_SQL += " and archived = 0"
        m_SQL += " AND"
        m_SQL += " ("
        m_SQL += " email not like '%@%' OR"
        m_SQL += " email not like '%.co%'"
        m_SQL += " AND email not like '%.net%'"
        m_SQL += " AND email not like '%.ie%'"
        m_SQL += " AND email not like '%.wales%'"
        m_SQL += " AND email not like '%.cymru%'"
        m_SQL += " AND email not like '%.ac.uk%'"
        m_SQL += " AND email not like '%.gov%'"
        m_SQL += " AND email not like '%.police%'"
        m_SQL += " AND email not like '%.org%'"
        m_SQL += " AND email not like '%.uk%'"
        m_SQL += ")"
        m_SQL += " order by family.surname"

        RefreshGrid()

    End Sub
End Class