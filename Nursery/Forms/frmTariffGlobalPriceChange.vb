﻿Imports Care.Global

Public Class frmTariffGlobalPriceChange

    Private Sub frmTariffGlobalPriceChange_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cbxMethod.AddItem("Percentage")
        cbxMethod.AddItem("Fixed Amount")

        cbxRound.AddItem("Penny")
        cbxRound.AddItem("5p")
        cbxRound.AddItem("10p")
        cbxRound.AddItem("50p")
        cbxRound.AddItem("Pound")

        txtIncrease.BackColor = Session.ChangeColour
        cbxMethod.BackColor = Session.ChangeColour
        cbxRound.BackColor = Session.ChangeColour

    End Sub

    Private Sub frmTariffGlobalPriceChange_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        cbxMethod.SelectedIndex = 0
        cbxRound.SelectedIndex = 0
        chkStandard.Checked = True
        txtIncrease.Focus()
    End Sub

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click

        If txtIncrease.Text = "" Then
            CareMessage("Please enter the percentage value increase you wish to apply to your tarrifs.", MessageBoxIcon.Exclamation, "Increase Amount")
            Exit Sub
        End If

        Dim _Increase As Decimal = ValueHandler.ConvertDecimal(txtIncrease.Text)

        If _Increase <= 0 Then
            CareMessage("Invalid increase percentage.", MessageBoxIcon.Exclamation, "Invalid")
            Exit Sub
        End If

        Dim _Mess As String = ""
        _Mess += "This routine will change the Prices for ALL SITES."
        _Mess += vbCrLf
        _Mess += vbCrLf
        _Mess += "Please type CONTINUE to apply your changes."

        If InputBox(_Mess).ToUpper <> "CONTINUE" Then Exit Sub

        btnRun.Enabled = False
        btnClose.Enabled = False
        Session.CursorWaiting()

        ExecutePriceChanges(_Increase)

        Session.CursorDefault()

        Me.DialogResult = DialogResult.OK
        Me.Close()

    End Sub

    Private Sub ExecutePriceChanges(ByVal Increase As Decimal)

        Dim _Tariffs As List(Of Business.Tariff) = Business.Tariff.RetreiveAll
        If _Tariffs IsNot Nothing Then

            Session.SetupProgressBar("Processing Tariff Increases...", _Tariffs.Count)

            For Each _t In _Tariffs

                If _t._Type = "Month Matrix" Then
                    If chkMonthly.Checked Then
                        Dim _Monthly As List(Of Business.TariffDay) = Business.TariffDay.RetreiveByTariffID(_t._ID.Value)
                        If _Monthly IsNot Nothing Then
                            For Each _m In _Monthly
                                _m._Days1 = Uplift(_m._Days1, Increase, cbxMethod.Text, cbxRound.Text)
                                _m._Days2 = Uplift(_m._Days2, Increase, cbxMethod.Text, cbxRound.Text)
                                _m._Days3 = Uplift(_m._Days3, Increase, cbxMethod.Text, cbxRound.Text)
                                _m._Days4 = Uplift(_m._Days4, Increase, cbxMethod.Text, cbxRound.Text)
                                _m._Days5 = Uplift(_m._Days5, Increase, cbxMethod.Text, cbxRound.Text)
                                _m._Days6 = Uplift(_m._Days6, Increase, cbxMethod.Text, cbxRound.Text)
                                _m._Days7 = Uplift(_m._Days7, Increase, cbxMethod.Text, cbxRound.Text)
                                _m.Store()
                            Next
                        End If
                    End If
                Else

                    'change value
                    If chkStandard.Checked Then
                        _t._Rate = Uplift(_t._Rate, Increase, cbxMethod.Text, cbxRound.Text)
                        _t.Store()
                    End If

                    'check if there is an Age Matrix Tariff which needs updating
                    If chkAgeMatrix.Checked Then
                        Dim _Matrix As List(Of Business.TariffMatrix) = Business.TariffMatrix.RetreiveByTariffID(_t._ID.Value)
                        If _Matrix IsNot Nothing Then
                            For Each _m In _Matrix
                                _m._Value = Uplift(_m._Value, Increase, cbxMethod.Text, cbxRound.Text)
                                _m.Store()
                            Next
                        End If
                    End If

                    'check if there is any variable rates
                    If chkVariable.Checked Then
                        If _t._VariableRate Then
                            Dim _Var As List(Of Business.TariffVariable) = Business.TariffVariable.RetreiveByTariffID(_t._ID.Value)
                            If _Var IsNot Nothing Then
                                For Each _v In _Var
                                    _v._Rate = Uplift(_v._Rate, Increase, cbxMethod.Text, cbxRound.Text)
                                    _v.Store()
                                Next
                            End If
                        End If
                    End If

                End If

                Session.StepProgressBar()

            Next

            Session.SetProgressMessage("Tariffs Updated", 5)

        End If

        If chkBoltOns.Checked Then

            Dim _BoltOns As List(Of Business.TariffBoltOn) = Business.TariffBoltOn.RetreiveAll
            If _BoltOns IsNot Nothing Then

                Session.SetupProgressBar("Processing Bolt-On Increases...", _BoltOns.Count)

                For Each _b In _BoltOns
                    _b._Price = Uplift(_b._Price, Increase, cbxMethod.Text, cbxRound.Text)
                    _b.Store()
                    Session.StepProgressBar()
                Next

                Session.SetProgressMessage("Bolt-Ons Updated", 5)

            End If

        End If

    End Sub

    Private Function Uplift(ByVal RateIn As Decimal, ByVal Increase As Decimal, ByVal Method As String, ByVal Rounding As String) As Decimal

        If RateIn = 0 Then Return 0

        Dim _Return As Double = 0

        If Method = "Fixed Amount" Then
            _Return = RateIn + Increase
        Else
            Dim _Inc As Double = ValueHandler.Convert2DP(RateIn / 100 * Increase)
            _Return = ValueHandler.Convert2DP(RateIn) + ValueHandler.Convert2DP(_Inc)
        End If

        Select Case Rounding
            Case "5p"
                While ValueHandler.Convert2DP((_Return / 0.05) Mod 1) > 0 AndAlso ValueHandler.Convert2DP((_Return / 0.05) Mod 1) < 1
                    _Return += 0.01
                    _Return = ValueHandler.Convert2DP(_Return)
                End While

            Case "10p"
                While ValueHandler.Convert2DP((_Return / 0.1) Mod 1) > 0 AndAlso ValueHandler.Convert2DP((_Return / 0.1) Mod 1) < 1
                    _Return += 0.01
                    _Return = ValueHandler.Convert2DP(_Return)
                End While

            Case "50p"
                While ValueHandler.Convert2DP((_Return / 0.5) Mod 1) > 0 AndAlso ValueHandler.Convert2DP((_Return / 0.5) Mod 1) < 1
                    _Return += 0.01
                    _Return = ValueHandler.Convert2DP(_Return)
                End While

            Case "Pound"
                _Return = Math.Ceiling(_Return)

            Case Else
                'already set to 2DP above

        End Select

        Return CDec(_Return)

    End Function

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

End Class