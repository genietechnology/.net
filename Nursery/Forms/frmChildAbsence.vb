﻿Imports Care.Global
Imports Care.Shared

Public Class frmChildAbsence

    Private m_ChildID As Guid
    Private m_AbsenceID As Guid?
    Private m_IsNew As Boolean = True

    Public Sub New(ByVal ChildID As Guid, ByVal AbsenceID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        m_ChildID = ChildID

        If AbsenceID.HasValue Then
            m_IsNew = False
            m_AbsenceID = AbsenceID
        Else
            m_IsNew = True
            m_AbsenceID = Nothing
        End If

    End Sub

    Private Sub frmChildHoliday_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cdtStartDate.BackColor = Session.ChangeColour
        cdtEndDate.BackColor = Session.ChangeColour
        cbxReason.BackColor = Session.ChangeColour
        txtNotes.BackColor = Session.ChangeColour

        cbxReason.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Child Absence Types"))

        If m_AbsenceID.HasValue Then
            Me.Text = "Edit Absence Record"
            DisplayRecord()
        Else
            Me.Text = "Create New Absence Record"
        End If

    End Sub

    Private Sub DisplayRecord()

        Dim _H As Business.ChildAbsence = Business.ChildAbsence.RetreiveByID(m_AbsenceID.Value)
        With _H
            cdtStartDate.Value = ._AbsFrom
            cdtEndDate.Value = ._AbsTo
            cbxReason.Text = ._AbsReason
            txtNotes.Text = ._AbsNotes
        End With

        _H = Nothing

    End Sub

    Private Function ValidateAbsence() As Boolean

        If cdtStartDate.Text = "" Then Return False
        If cdtStartDate.Value Is Nothing Then Return False
        If Not cdtStartDate.Value.HasValue Then Return False

        If cdtEndDate.Text = "" Then Return False
        If cdtEndDate.Value Is Nothing Then Return False
        If Not cdtEndDate.Value.HasValue Then Return False

        If cdtStartDate.Value.Value > cdtEndDate.Value.Value Then Return False

        If cbxReason.Text = "" Then Return False

        Return True

    End Function

    Private Sub SaveAndExit()

        If Not ValidateAbsence() Then Exit Sub

        Dim _A As Business.ChildAbsence = Nothing

        If m_IsNew Then
            _A = New Business.ChildAbsence
            _A._ID = Guid.NewGuid
            _A._ChildId = m_ChildID
        Else
            _A = Business.ChildAbsence.RetreiveByID(m_AbsenceID.Value)
        End If

        With _A
            ._AbsType = "Absence"
            ._AbsSource = "Workstation"
            ._AbsFrom = cdtStartDate.Value
            ._AbsTo = cdtEndDate.Value
            ._AbsReason = cbxReason.Text
            ._AbsNotes = txtNotes.Text
            ._AbsStaffId = Session.CurrentUser.ID
            ._AbsStaffName = Session.CurrentUser.FullName
            ._AbsStamp = Now
            .Store()
        End With

        _A = Nothing

        Me.DialogResult = DialogResult.OK
        Me.Close()

    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        SaveAndExit()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

End Class
