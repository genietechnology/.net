﻿Imports System.Windows.Forms
Imports Care.Global
Imports Care.Data
Imports Care.Shared

Public Class frmSparkSync

    Private m_SparkNurseryID As String = ""
    Private m_SparkRoomName As String = ""
    Private m_SparkUsername As String = ""
    Private m_SparkPassword As String = ""
    Private m_Records As New List(Of SyncRecord)

    Private Class SyncRecord
        Public Property SparkNurseryID As String
        Public Property SparkChildID As String
        Public Property OurChildID As String
        Public Property ChildName As String
        Public Property DOB As Date?
        Public Property Gender As String
        Public Property RoomName As String
        Public Property StartDate As Date?
        Public Property Funded As Boolean
        Public Property EYPP As Boolean
        Public Property Status As String
        Public Property ErrorText As String
        Public Property ChildObject As Business.Child
    End Class

    Private Sub frmFinancialsSync_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Session.CursorWaiting()

        m_SparkNurseryID = ParameterHandler.ReturnString("SPARKNURSERYID")
        m_SparkRoomName = ParameterHandler.ReturnString("SPARKROOM")
        m_SparkUsername = ParameterHandler.ReturnString("SPARKUSER")
        m_SparkPassword = ParameterHandler.ReturnString("SPARKPASSWORD")

        cbxOption.AddItem("Synchronise New Children Only")
        cbxOption.AddItem("Synchronise New and Existing Children")
        cbxOption.SelectedIndex = 0

        btnRefresh.Enabled = False
        btnRun.Enabled = False

    End Sub

    Private Sub frmFinancialsSync_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        BuildRecords()
        Session.CursorDefault()
    End Sub

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click
        Run()
    End Sub

    Private Sub BuildRecords()

        m_Records.Clear()

        Dim _Children As List(Of Business.Child) = Business.Child.RetrieveLiveChildren
        If _Children IsNot Nothing Then

            Session.SetupProgressBar("Building Synchronise Package...", _Children.Count)

            For Each _c As Business.Child In _Children

                Dim _R As New SyncRecord
                With _R

                    .SparkNurseryID = m_SparkNurseryID
                    .SparkChildID = _c._ExternalID
                    .OurChildID = _c._ID.ToString
                    .ChildName = _c._Fullname
                    .DOB = _c._Dob
                    .Gender = _c._Gender

                    If m_SparkRoomName <> "" Then
                        .RoomName = m_SparkRoomName
                    Else
                        .RoomName = _c._GroupName
                    End If

                    .StartDate = _c._Started
                    .Funded = False
                    .EYPP = False
                    .Status = "Ready to Sync"
                    .ErrorText = ""
                    .ChildObject = _c

                End With

                m_Records.Add(_R)
                Session.StepProgressBar()

            Next

            Session.HideProgressBar()

        End If

        cgRecords.Populate(m_Records)
        cgRecords.PreviewColumn = "ErrorText"

        cgRecords.Columns("ErrorText").Visible = False
        cgRecords.Columns("ChildObject").Visible = False

        If m_Records.Count > 0 Then btnRun.Enabled = True

    End Sub

    Private Sub Run()

        If CareMessage("Are you sure you want to Continue?", MessageBoxIcon.Question, MessageBoxButtons.YesNo, "Run") = DialogResult.No Then
            Exit Sub
        End If

        btnRefresh.Enabled = False
        btnRun.Enabled = False
        btnClose.Enabled = False

        Dim _Spark As New Spark.IntegrateServiceClient()
        _Spark.ClientCredentials.UserName.UserName = m_SparkUsername
        _Spark.ClientCredentials.UserName.Password = m_SparkPassword

        '_Spark.ClientCredentials.UserName.UserName = "sparkgenieservice"
        '_Spark.ClientCredentials.UserName.Password = "n?4@6A-P!2xS7KFe"

        Session.SetupProgressBar("Synchronising Children...", m_Records.Count)
        Session.CursorWaiting()

        For Each _R As SyncRecord In m_Records

            Dim _ExternalID As String = ""

            If _R.Status = "Ready to Sync" Then

                Try
                    _ExternalID = _Spark.AddChild(m_SparkNurseryID, _R.SparkChildID, _R.ChildName, _R.DOB, _R.Gender, _R.RoomName, _R.StartDate, _R.Funded, _R.EYPP)

                Catch ex As Exception
                    _R.Status = "Error"
                    _R.ErrorText = ex.Message
                    ErrorHandler.LogExceptionToDatabase(ex, False)
                End Try

                If _ExternalID <> "" Then
                    _R.ChildObject._ExternalId = _ExternalID
                    _R.ChildObject.Store()
                    _R.Status = "OK"
                End If

            End If

            cgRecords.UnderlyingGridControl.RefreshDataSource()
            Session.StepProgressBar()
            Application.DoEvents()

        Next

        btnRefresh.Enabled = True
        btnRun.Enabled = True
        btnClose.Enabled = True

        Session.HideProgressBar()
        Session.CursorDefault()

    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        BuildRecords()
    End Sub

    Private Sub cgRecords_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles cgRecords.RowStyle

        If e IsNot Nothing And e.RowHandle >= 0 Then

            Select Case cgRecords.GetRowCellValue(e.RowHandle, "Status").ToString

                Case "Error"
                    e.Appearance.BackColor = txtRed.BackColor

                Case "OK"
                    e.Appearance.BackColor = txtGreen.BackColor

            End Select

        End If

    End Sub

    Private Sub cgRecords_GridDoubleClick(sender As Object, e As EventArgs) Handles cgRecords.GridDoubleClick

        If cgRecords Is Nothing Then Exit Sub
        If cgRecords.RecordCount <= 0 Then Exit Sub
        If cgRecords.RowIndex < 0 Then Exit Sub

        Dim _SRO As Object = cgRecords.UnderlyingGridView.GetFocusedRow
        If _SRO IsNot Nothing Then

        End If

    End Sub
End Class