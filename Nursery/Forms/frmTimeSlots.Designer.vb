﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTimeSlots
    Inherits Care.Shared.frmGridMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtEnd = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.txtStart = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.txtName = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.txtShortName = New Care.Controls.CareTextBox(Me.components)
        Me.chkHeadcount = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.chkAM = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.chkPM = New Care.Controls.CareCheckBox(Me.components)
        Me.btn60 = New Care.Controls.CareButton(Me.components)
        Me.btn30 = New Care.Controls.CareButton(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.chkLunch = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.chkBreakfast = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel13 = New Care.Controls.CareLabel(Me.components)
        Me.chkSnack = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.chkTea = New Care.Controls.CareCheckBox(Me.components)
        Me.gbx.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.txtEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtShortName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkHeadcount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAM.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkPM.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkLunch.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkBreakfast.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSnack.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkTea.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbx
        '
        Me.gbx.Anchor = AnchorStyles.None
        Me.gbx.Controls.Add(Me.CareLabel13)
        Me.gbx.Controls.Add(Me.chkSnack)
        Me.gbx.Controls.Add(Me.CareLabel14)
        Me.gbx.Controls.Add(Me.chkTea)
        Me.gbx.Controls.Add(Me.CareLabel8)
        Me.gbx.Controls.Add(Me.chkLunch)
        Me.gbx.Controls.Add(Me.CareLabel9)
        Me.gbx.Controls.Add(Me.chkBreakfast)
        Me.gbx.Controls.Add(Me.CareLabel7)
        Me.gbx.Controls.Add(Me.chkPM)
        Me.gbx.Controls.Add(Me.CareLabel6)
        Me.gbx.Controls.Add(Me.chkAM)
        Me.gbx.Controls.Add(Me.CareLabel5)
        Me.gbx.Controls.Add(Me.CareLabel4)
        Me.gbx.Controls.Add(Me.chkHeadcount)
        Me.gbx.Controls.Add(Me.txtShortName)
        Me.gbx.Controls.Add(Me.txtEnd)
        Me.gbx.Controls.Add(Me.CareLabel3)
        Me.gbx.Controls.Add(Me.txtStart)
        Me.gbx.Controls.Add(Me.CareLabel2)
        Me.gbx.Controls.Add(Me.txtName)
        Me.gbx.Controls.Add(Me.CareLabel1)
        Me.gbx.Location = New System.Drawing.Point(78, 169)
        Me.gbx.Size = New System.Drawing.Size(629, 245)
        Me.gbx.TabIndex = 1
        Me.gbx.Controls.SetChildIndex(Me.Panel2, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel1, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtName, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel2, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtStart, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel3, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtEnd, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtShortName, 0)
        Me.gbx.Controls.SetChildIndex(Me.chkHeadcount, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel4, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel5, 0)
        Me.gbx.Controls.SetChildIndex(Me.chkAM, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel6, 0)
        Me.gbx.Controls.SetChildIndex(Me.chkPM, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel7, 0)
        Me.gbx.Controls.SetChildIndex(Me.chkBreakfast, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel9, 0)
        Me.gbx.Controls.SetChildIndex(Me.chkLunch, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel8, 0)
        Me.gbx.Controls.SetChildIndex(Me.chkTea, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel14, 0)
        Me.gbx.Controls.SetChildIndex(Me.chkSnack, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel13, 0)
        '
        'Panel2
        '
        Me.Panel2.Location = New System.Drawing.Point(3, 214)
        Me.Panel2.Size = New System.Drawing.Size(623, 28)
        Me.Panel2.TabIndex = 22
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(438, 0)
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(529, 0)
        '
        'Grid
        '
        Me.Grid.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Grid.Appearance.Options.UseFont = True
        Me.Grid.Size = New System.Drawing.Size(765, 558)
        Me.Grid.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btn30)
        Me.Panel1.Controls.Add(Me.btn60)
        Me.Panel1.Location = New System.Drawing.Point(0, 576)
        Me.Panel1.Size = New System.Drawing.Size(784, 35)
        Me.Panel1.Controls.SetChildIndex(Me.btnDelete, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnAdd, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnEdit, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnClose, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btn60, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btn30, 0)
        '
        'btnDelete
        '
        Me.btnDelete.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Appearance.Options.UseFont = True
        '
        'btnEdit
        '
        Me.btnEdit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.Appearance.Options.UseFont = True
        '
        'btnAdd
        '
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.Appearance.Options.UseFont = True
        '
        'btnClose
        '
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(960, 5)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'txtEnd
        '
        Me.txtEnd.CharacterCasing = CharacterCasing.Normal
        Me.txtEnd.EnterMoveNextControl = True
        Me.txtEnd.Location = New System.Drawing.Point(127, 106)
        Me.txtEnd.MaxLength = 5
        Me.txtEnd.Name = "txtEnd"
        Me.txtEnd.NumericAllowNegatives = False
        Me.txtEnd.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.Time
        Me.txtEnd.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtEnd.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtEnd.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtEnd.Properties.Appearance.Options.UseFont = True
        Me.txtEnd.Properties.Appearance.Options.UseTextOptions = True
        Me.txtEnd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtEnd.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic
        Me.txtEnd.Properties.Mask.EditMask = "(0?\d|1\d|2[0-3])\:[0-5]\d"
        Me.txtEnd.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtEnd.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtEnd.Properties.MaxLength = 5
        Me.txtEnd.Size = New System.Drawing.Size(58, 22)
        Me.txtEnd.TabIndex = 7
        Me.txtEnd.Tag = "AE"
        Me.txtEnd.TextAlign = HorizontalAlignment.Left
        Me.txtEnd.ToolTipText = ""
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(10, 109)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(50, 15)
        Me.CareLabel3.TabIndex = 6
        Me.CareLabel3.Text = "End Time"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtStart
        '
        Me.txtStart.CharacterCasing = CharacterCasing.Normal
        Me.txtStart.EnterMoveNextControl = True
        Me.txtStart.Location = New System.Drawing.Point(127, 78)
        Me.txtStart.MaxLength = 5
        Me.txtStart.Name = "txtStart"
        Me.txtStart.NumericAllowNegatives = False
        Me.txtStart.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.Time
        Me.txtStart.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStart.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtStart.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtStart.Properties.Appearance.Options.UseFont = True
        Me.txtStart.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStart.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtStart.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic
        Me.txtStart.Properties.Mask.EditMask = "(0?\d|1\d|2[0-3])\:[0-5]\d"
        Me.txtStart.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtStart.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtStart.Properties.MaxLength = 5
        Me.txtStart.Size = New System.Drawing.Size(58, 22)
        Me.txtStart.TabIndex = 5
        Me.txtStart.Tag = "AE"
        Me.txtStart.TextAlign = HorizontalAlignment.Left
        Me.txtStart.ToolTipText = ""
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(10, 81)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(54, 15)
        Me.CareLabel2.TabIndex = 4
        Me.CareLabel2.Text = "Start Time"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.CharacterCasing = CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(127, 22)
        Me.txtName.MaxLength = 30
        Me.txtName.Name = "txtName"
        Me.txtName.NumericAllowNegatives = False
        Me.txtName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtName.Properties.MaxLength = 30
        Me.txtName.Size = New System.Drawing.Size(365, 22)
        Me.txtName.TabIndex = 1
        Me.txtName.Tag = "AE"
        Me.txtName.TextAlign = HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(10, 25)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Name"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtShortName
        '
        Me.txtShortName.CharacterCasing = CharacterCasing.Normal
        Me.txtShortName.EnterMoveNextControl = True
        Me.txtShortName.Location = New System.Drawing.Point(127, 50)
        Me.txtShortName.MaxLength = 10
        Me.txtShortName.Name = "txtShortName"
        Me.txtShortName.NumericAllowNegatives = False
        Me.txtShortName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtShortName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtShortName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtShortName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtShortName.Properties.Appearance.Options.UseFont = True
        Me.txtShortName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtShortName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtShortName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtShortName.Properties.MaxLength = 10
        Me.txtShortName.Size = New System.Drawing.Size(139, 22)
        Me.txtShortName.TabIndex = 3
        Me.txtShortName.Tag = "AE"
        Me.txtShortName.TextAlign = HorizontalAlignment.Left
        Me.txtShortName.ToolTipText = ""
        '
        'chkHeadcount
        '
        Me.chkHeadcount.EnterMoveNextControl = True
        Me.chkHeadcount.Location = New System.Drawing.Point(125, 134)
        Me.chkHeadcount.Name = "chkHeadcount"
        Me.chkHeadcount.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkHeadcount.Properties.Appearance.Options.UseFont = True
        Me.chkHeadcount.Properties.Caption = ""
        Me.chkHeadcount.Size = New System.Drawing.Size(19, 19)
        Me.chkHeadcount.TabIndex = 9
        Me.chkHeadcount.Tag = "AE"
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(10, 136)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(109, 15)
        Me.CareLabel4.TabIndex = 8
        Me.CareLabel4.Text = "Show in Headcounts"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(10, 53)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(63, 15)
        Me.CareLabel5.TabIndex = 2
        Me.CareLabel5.Text = "Short Name"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(10, 161)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(76, 15)
        Me.CareLabel6.TabIndex = 10
        Me.CareLabel6.Text = "Classed as AM"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkAM
        '
        Me.chkAM.EnterMoveNextControl = True
        Me.chkAM.Location = New System.Drawing.Point(125, 159)
        Me.chkAM.Name = "chkAM"
        Me.chkAM.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkAM.Properties.Appearance.Options.UseFont = True
        Me.chkAM.Properties.Caption = ""
        Me.chkAM.Size = New System.Drawing.Size(19, 19)
        Me.chkAM.TabIndex = 11
        Me.chkAM.Tag = "AE"
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(171, 161)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(75, 15)
        Me.CareLabel7.TabIndex = 12
        Me.CareLabel7.Text = "Classed as PM"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkPM
        '
        Me.chkPM.EnterMoveNextControl = True
        Me.chkPM.Location = New System.Drawing.Point(286, 159)
        Me.chkPM.Name = "chkPM"
        Me.chkPM.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkPM.Properties.Appearance.Options.UseFont = True
        Me.chkPM.Properties.Caption = ""
        Me.chkPM.Size = New System.Drawing.Size(19, 19)
        Me.chkPM.TabIndex = 13
        Me.chkPM.Tag = "AE"
        '
        'btn60
        '
        Me.btn60.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btn60.Location = New System.Drawing.Point(283, 5)
        Me.btn60.Name = "btn60"
        Me.btn60.Size = New System.Drawing.Size(125, 25)
        Me.btn60.TabIndex = 9
        Me.btn60.Text = "Create Hour Slots"
        '
        'btn30
        '
        Me.btn30.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btn30.Location = New System.Drawing.Point(414, 5)
        Me.btn30.Name = "btn30"
        Me.btn30.Size = New System.Drawing.Size(125, 25)
        Me.btn30.TabIndex = 10
        Me.btn30.Text = "Create Half-Hour Slots"
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(171, 186)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(90, 15)
        Me.CareLabel8.TabIndex = 16
        Me.CareLabel8.Text = "Classed as Lunch"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkLunch
        '
        Me.chkLunch.EnterMoveNextControl = True
        Me.chkLunch.Location = New System.Drawing.Point(286, 184)
        Me.chkLunch.Name = "chkLunch"
        Me.chkLunch.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkLunch.Properties.Appearance.Options.UseFont = True
        Me.chkLunch.Properties.Caption = ""
        Me.chkLunch.Size = New System.Drawing.Size(19, 19)
        Me.chkLunch.TabIndex = 17
        Me.chkLunch.Tag = "AE"
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(10, 186)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(105, 15)
        Me.CareLabel9.TabIndex = 14
        Me.CareLabel9.Text = "Classed as Breakfast"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkBreakfast
        '
        Me.chkBreakfast.EnterMoveNextControl = True
        Me.chkBreakfast.Location = New System.Drawing.Point(125, 184)
        Me.chkBreakfast.Name = "chkBreakfast"
        Me.chkBreakfast.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkBreakfast.Properties.Appearance.Options.UseFont = True
        Me.chkBreakfast.Properties.Caption = ""
        Me.chkBreakfast.Size = New System.Drawing.Size(19, 19)
        Me.chkBreakfast.TabIndex = 15
        Me.chkBreakfast.Tag = "AE"
        '
        'CareLabel13
        '
        Me.CareLabel13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel13.Location = New System.Drawing.Point(486, 186)
        Me.CareLabel13.Name = "CareLabel13"
        Me.CareLabel13.Size = New System.Drawing.Size(88, 15)
        Me.CareLabel13.TabIndex = 20
        Me.CareLabel13.Text = "Classed as Snack"
        Me.CareLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkSnack
        '
        Me.chkSnack.EnterMoveNextControl = True
        Me.chkSnack.Location = New System.Drawing.Point(601, 184)
        Me.chkSnack.Name = "chkSnack"
        Me.chkSnack.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkSnack.Properties.Appearance.Options.UseFont = True
        Me.chkSnack.Properties.Caption = ""
        Me.chkSnack.Size = New System.Drawing.Size(19, 19)
        Me.chkSnack.TabIndex = 21
        Me.chkSnack.Tag = "AE"
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(325, 186)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(76, 15)
        Me.CareLabel14.TabIndex = 18
        Me.CareLabel14.Text = "Classed as Tea"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkTea
        '
        Me.chkTea.EnterMoveNextControl = True
        Me.chkTea.Location = New System.Drawing.Point(440, 184)
        Me.chkTea.Name = "chkTea"
        Me.chkTea.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkTea.Properties.Appearance.Options.UseFont = True
        Me.chkTea.Properties.Caption = ""
        Me.chkTea.Size = New System.Drawing.Size(19, 19)
        Me.chkTea.TabIndex = 19
        Me.chkTea.Tag = "AE"
        '
        'frmTimeSlots
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(784, 611)
        Me.Name = "frmTimeSlots"
        Me.gbx.ResumeLayout(False)
        Me.gbx.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.txtEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtShortName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkHeadcount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAM.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkPM.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkLunch.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkBreakfast.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSnack.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkTea.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtEnd As Care.Controls.CareTextBox
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents txtStart As Care.Controls.CareTextBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents chkHeadcount As Care.Controls.CareCheckBox
    Friend WithEvents txtShortName As Care.Controls.CareTextBox
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents chkPM As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents chkAM As Care.Controls.CareCheckBox
    Friend WithEvents btn30 As Care.Controls.CareButton
    Friend WithEvents btn60 As Care.Controls.CareButton
    Friend WithEvents CareLabel13 As Care.Controls.CareLabel
    Friend WithEvents chkSnack As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents chkTea As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents chkLunch As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents chkBreakfast As Care.Controls.CareCheckBox
End Class
