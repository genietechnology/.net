﻿

Imports Care.Global
Imports Care.Data

Public Class frmImportKLN

    Private m_DBConnection As String = "Data Source=192.168.254.1;Initial Catalog=<DB>;User Id=careuser;Password=careuser;"
    Private m_SiteID As Guid = Nothing
    Private m_SiteName As String = ""

    Private Sub frmImportKLN_Load(sender As Object, e As EventArgs) Handles Me.Load

        txtDB.Text = ""

        cbxSite.PopulateWithSQL(Session.ConnectionString, "select id, name from Sites order by name")
        cbxRoom.PopulateWithSQL(Session.ConnectionString, "select id, name from groups order by sequence")
        cbxKeyworker.PopulateWithSQL(Session.ConnectionString, "select id, fullname from staff where keyworker = 1 order by fullname")

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        If txtDB.Text = "" Then Exit Sub
        If cbxSite.SelectedIndex < 0 Then Exit Sub

        Run()

    End Sub

    Private Sub Run()

        m_SiteID = New Guid(cbxSite.SelectedValue.ToString)
        m_SiteName = cbxSite.Text

        m_DBConnection = m_DBConnection.Replace("<DB>", txtDB.text)

        Dim _SQL As String = ""
        _SQL += "select * from [Registration table]"
        _SQL += " where status = 'Active:'"
        _SQL += " order by Ref"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(m_DBConnection, _SQL)

        If _DT IsNot Nothing Then

            Session.SetupProgressBar("Importing...", _DT.Rows.Count)

            For Each _DR As DataRow In _DT.Rows

                'create IDs
                Dim _FamilyID As Guid = Guid.NewGuid
                Dim _ChildID As Guid = Guid.NewGuid

                Dim _PrimaryForename As String = GetName(_DR.Item("Mothers name:").ToString, True)
                Dim _PrimarySurname As String = GetName(_DR.Item("Mothers name:").ToString, False)

                '*************************************************************************************************************

                Dim _Letters As String = _DR.Item("Invoice to").ToString

                Dim _PostCode As String = _DR.Item("Childs home address post Code:").ToString
                Dim _Address As String = BuildAddress(_DR.Item("Childs home address street").ToString, _DR.Item("Childs home address Town:").ToString, _
                                                      _DR.Item("Childs home address County:").ToString, "", _PostCode)

                'create family
                CreateFamily(_FamilyID, m_SiteID, m_SiteName, _PrimarySurname, _Letters, _Address, _PostCode)

                '*************************************************************************************************************

                'create contacts

                CreateContact(_FamilyID, m_SiteName, _DR.Item("Mothers name:").ToString, "Mother", _
                              True, True, _DR.Item("Collect Password").ToString, _
                              _DR.Item("Telephone numbers 1 Home:").ToString, _
                              _DR.Item("Telephone numbers 1 Work:").ToString, _
                              _DR.Item("Telephone numbers 1 Mobile:").ToString, _
                              _DR.Item("Email:").ToString)

                CreateContact(_FamilyID, m_SiteName, _DR.Item("Fathers name:").ToString, "Father", _
                              False, True, _DR.Item("Collect Password").ToString, _
                              _DR.Item("Telephone numbers 2 Home:").ToString, _
                              _DR.Item("Telephone numbers 2 Work:").ToString, _
                              _DR.Item("Telephone numbers 2 Mobile:").ToString, _
                              "")

                CreateContact(_FamilyID, m_SiteName, _DR.Item("Emergency contact names1 :").ToString, _
                              _DR.Item("Relationship to Child 1:").ToString, _
                              False, True, "", _
                              _DR.Item("Contact 1 telephone number:").ToString, _
                              "", "", "")

                CreateContact(_FamilyID, m_SiteName, _DR.Item("Emergency contact names1 :").ToString, _
                              _DR.Item("Relationship to Child 1:").ToString, _
                              False, True, "", "", _
                              _DR.Item("Contact 1 telephone number:").ToString, _
                              "", "")

                CreateContact(_FamilyID, m_SiteName, _DR.Item("Emergency contact names2 :").ToString, _
                              _DR.Item("Relationship to Child 2:").ToString, _
                              False, True, "", "", _
                              _DR.Item("Contact 2 telephone number:").ToString, _
                              "", "")

                CreateContact(_FamilyID, m_SiteName, _DR.Item("Emergency contact names3 :").ToString, _
                              _DR.Item("Relationship to Child 3:").ToString, _
                              False, True, "", "", _
                              _DR.Item("Contact 3 telephone number:").ToString, _
                              "", "")

                '*************************************************************************************************************

                'create child
                CreateChild(_FamilyID, _ChildID, _PrimarySurname, m_SiteID, m_SiteName, _
                            _DR.Item("Childs full name First Name:").ToString, _DR.Item("Childs full name Surname:").ToString, _
                            _DR.Item("Male/Female").ToString, _DR.Item("Date of Birth").ToString, _
                            _DR.Item("Name of Doctor:").ToString, _DR.Item("Surgery:").ToString, _
                            _DR.Item("Doctor Telephone number:").ToString, _
                            _DR.Item("Name of Health Visitor:").ToString, _DR.Item("Surgery Telephone number:").ToString, _
                            _DR.Item("Food allergies/medical conditions").ToString, _DR.Item("Food allergies/medical conditions 2").ToString, _
                            _DR.Item("Food allergies/medical conditions 3").ToString, _DR.Item("other medical conditions").ToString, _
                            _DR.Item("Preferred Start Date").ToString)

                'store voucher details on primary contact
                StoreVouchers(_FamilyID, _
                              ValueHandler.ConvertBoolean(_DR.Item("Child care vouchers")), _
                              _DR.Item("Child care voucher provider").ToString, _
                              ValueHandler.ConvertDecimal(_DR.Item("Child care voucher amount")))

                'check if the table contains the "all day field"
                Dim _AllDay As Boolean = False

                For Each _Field As DataColumn In _DT.Columns
                    If _Field.ColumnName.ToLower.Contains("all day") Then
                        _AllDay = True
                        Exit For
                    End If
                Next

                If _AllDay Then

                    'create bookings
                    CreateBookings(_ChildID, _DR.Item("Preferred Start Date").ToString, _
                                  ValueHandler.ConvertBoolean(_DR.Item("Monday AM")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Monday Lunch")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Monday PM")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Mon all day")), _
                                  ValueHandler.ConvertDecimal(_DR.Item("Mon funded hrs")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Tuesday AM")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Tuesday Lunch")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Tuesday PM")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Tue all day")), _
                                  ValueHandler.ConvertDecimal(_DR.Item("Tue funded hrs")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Wednesday AM")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Wednesday Lunch")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Wednesday PM")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Wed all day")), _
                                  ValueHandler.ConvertDecimal(_DR.Item("Wed funded hrs")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Thursday AM")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Thursday Lunch")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Thursday PM")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Thu all day")), _
                                  ValueHandler.ConvertDecimal(_DR.Item("Thu funded hrs")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Friday AM")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Friday Lunch")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Friday PM")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Fri all day")), _
                                  ValueHandler.ConvertDecimal(_DR.Item("Fri funded hrs")))

                Else

                    'create bookings
                    CreateBookings(_ChildID, _DR.Item("Preferred Start Date").ToString, _
                                  ValueHandler.ConvertBoolean(_DR.Item("Monday AM")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Monday Lunch")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Monday PM")), _
                                  False, _
                                  0, _
                                  ValueHandler.ConvertBoolean(_DR.Item("Tuesday AM")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Tuesday Lunch")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Tuesday PM")), _
                                  False, _
                                  0, _
                                  ValueHandler.ConvertBoolean(_DR.Item("Wednesday AM")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Wednesday Lunch")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Wednesday PM")), _
                                  False, _
                                  0, _
                                  ValueHandler.ConvertBoolean(_DR.Item("Thursday AM")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Thursday Lunch")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Thursday PM")), _
                                  False, _
                                  0, _
                                  ValueHandler.ConvertBoolean(_DR.Item("Friday AM")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Friday Lunch")), _
                                  ValueHandler.ConvertBoolean(_DR.Item("Friday PM")), _
                                  False, _
                                  0)

                End If

                '*************************************************************************************************************

                Session.StepProgressBar()

            Next

        End If

    End Sub

    Private Sub CreateBookings(ByVal ChildID As Guid, ByVal DateFrom As String, _
                               ByVal MondayAM As Boolean, ByVal MondayLunch As Boolean, ByVal MondayPM As Boolean, ByVal MondayAllDay As Boolean, ByVal MondayFunded As Decimal, _
                               ByVal TuesdayAM As Boolean, ByVal TuesdayLunch As Boolean, ByVal TuesdayPM As Boolean, ByVal TuesdayAllDay As Boolean, ByVal TuesdayFunded As Decimal, _
                               ByVal WednesdayAM As Boolean, ByVal WednesdayLunch As Boolean, ByVal WednesdayPM As Boolean, ByVal WednesdayAllDay As Boolean, ByVal WednesdayFunded As Decimal, _
                               ByVal ThursdayAM As Boolean, ByVal ThursdayLunch As Boolean, ByVal ThursdayPM As Boolean, ByVal ThursdayAllDay As Boolean, ByVal ThursdayFunded As Decimal, _
                               ByVal FridayAM As Boolean, ByVal FridayLunch As Boolean, ByVal FridayPM As Boolean, ByVal FridayAllDay As Boolean, ByVal FridayFunded As Decimal)

        Dim _From As Date?
        If DateFrom = "" Then
            _From = Today
        Else
            _From = ValueHandler.ConvertDate(DateFrom)
        End If

        Dim _Monday As Date

        If _From.Value.DayOfWeek = DayOfWeek.Monday Then
            _Monday = _From.Value
        Else
            _Monday = ValueHandler.NearestDate(_From.Value, DayOfWeek.Monday, ValueHandler.EnumDirection.Backwards)
        End If

        Dim _S As New Business.ChildAdvanceSession
        With _S

            ._ChildId = ChildID
            ._Mode = "Weekly"
            ._DateFrom = _Monday

            ._PatternType = "Recurring"
            ._RecurringScope = 1
            ._RecurringScopeDesc = "Yes, All the time"

            ._MondayDesc = CalculateTariffName(MondayAM, MondayPM, MondayAllDay)
            ._Monday = ReturnTariffID(._MondayDesc)
            ._MondayBo = CalculateBoltOns(MondayLunch)
            ._MondayBoid = ReturnBoltOnID(._MondayBo)
            ._MondayFund = ReturnFunding(MondayFunded)

            ._TuesdayDesc = CalculateTariffName(TuesdayAM, TuesdayPM, TuesdayAllDay)
            ._Tuesday = ReturnTariffID(._TuesdayDesc)
            ._TuesdayBo = CalculateBoltOns(TuesdayLunch)
            ._TuesdayBoid = ReturnBoltOnID(._TuesdayBo)
            ._TuesdayFund = ReturnFunding(TuesdayFunded)

            ._WednesdayDesc = CalculateTariffName(WednesdayAM, WednesdayPM, WednesdayAllDay)
            ._Wednesday = ReturnTariffID(._WednesdayDesc)
            ._WednesdayBo = CalculateBoltOns(WednesdayLunch)
            ._WednesdayBoid = ReturnBoltOnID(._WednesdayBo)
            ._WednesdayFund = ReturnFunding(WednesdayFunded)

            ._ThursdayDesc = CalculateTariffName(ThursdayAM, ThursdayPM, ThursdayAllDay)
            ._Thursday = ReturnTariffID(._ThursdayDesc)
            ._ThursdayBo = CalculateBoltOns(ThursdayLunch)
            ._ThursdayBoid = ReturnBoltOnID(._ThursdayBo)
            ._ThursdayFund = ReturnFunding(ThursdayFunded)

            ._FridayDesc = CalculateTariffName(FridayAM, FridayPM, FridayAllDay)
            ._Friday = ReturnTariffID(._FridayDesc)
            ._FridayBo = CalculateBoltOns(FridayLunch)
            ._FridayBoid = ReturnBoltOnID(._FridayBo)
            ._FridayFund = ReturnFunding(FridayFunded)

            ._Comments = "Imported from Access Database"
            .Store()

        End With

    End Sub

    Private Function ReturnFunding(ByVal DecimalIn As Decimal) As Decimal
        Return DecimalIn
    End Function

    Private Function CalculateTariffName(ByVal AM As Boolean, ByVal PM As Boolean, ByVal AllDay As Boolean) As String

        Dim _Return As String = ""

        If AllDay Then
            _Return = "Full Day"
        Else
            If AM = True AndAlso PM = True Then
                _Return = "Full Day"
            Else
                If AM = True AndAlso PM = False Then
                    _Return = "Morning Session"
                Else
                    If AM = False AndAlso PM = True Then
                        _Return = "Afternoon Session"
                    Else
                        _Return = ""
                    End If
                End If
            End If
        End If


        If _Return <> "" Then
            Return txtPrefix.Text + " " + _Return
        Else
            Return ""
        End If

    End Function

    Private Function CalculateBoltOns(ByVal Lunch As Boolean) As String
        If Lunch Then
            Return "Lunch at " + txtPrefix.Text
        Else
            Return ""
        End If
    End Function

    Private Function ReturnTariffID(ByVal Name As String) As Guid?

        If Name = "" Then Return Nothing

        Dim _SQL As String = "select ID from Tariffs where name = '" + Name + "'"
        Dim _ID As Object = DAL.ReturnScalar(Session.ConnectionString, _SQL)

        If _ID Is Nothing Then
            Return Nothing
        Else
            Return New Guid(_ID.ToString)
        End If

    End Function

    Private Function ReturnBoltOnID(ByVal Name As String) As String

        If Name = "" Then Return Nothing

        Dim _SQL As String = "select ID from TariffBoltOns where name = '" + Name + "'"
        Dim _ID As Object = DAL.ReturnScalar(Session.ConnectionString, _SQL)

        If _ID Is Nothing Then
            Return Nothing
        Else
            Return _ID.ToString
        End If

    End Function

    Private Sub StoreVouchers(ByVal FamilyID As Guid, _
                              ByVal Vouchers As Boolean, ByVal Provider As String, ByVal Amount As Decimal)

        If Vouchers Then

            Dim _C As Business.Contact = Business.Contact.RetrievePrimaryContact(FamilyID.ToString)
            With _C
                ._Voucher = True
                ._VoucherRef = Provider
                ._VoucherValue = Amount
                .Store()
            End With

        End If

    End Sub

    Private Sub CreateChild(ByVal FamilyID As Guid, ByVal ChildID As Guid, ByVal FamilyName As String, ByVal SiteID As Guid, ByVal SiteName As String, ByVal Forename As String, ByVal Surname As String, _
                            ByVal Gender As String, ByVal DOB As String, ByVal GPName As String, ByVal GPSurgery As String, ByVal GPTel As String, _
                            ByVal HVName As String, ByVal HVTel As String, _
                            ByVal Med1 As String, ByVal Med2 As String, ByVal Med3 As String, ByVal MedOther As String, _
                            ByVal StartDate As String)

        Dim _C As New Business.Child
        With _C

            ._ID = ChildID
            ._Status = "On Waiting List"
            ._Started = ValueHandler.ConvertDate(StartDate)

            ._FamilyId = FamilyID
            ._FamilyName = FamilyName

            ._Forename = Forename
            ._Surname = Surname
            ._Fullname = Forename + " " + Surname

            ._SiteId = SiteID
            ._SiteName = SiteName

            ._GroupId = New Guid(cbxRoom.SelectedValue.ToString)
            ._GroupName = cbxRoom.Text
            ._GroupStarted = ._Started

            ._KeyworkerId = New Guid(cbxKeyworker.SelectedValue.ToString)
            ._KeyworkerName = cbxKeyworker.Text

            ._Gender = Gender
            If ._Gender = "" Then ._Gender = "U"

            ._Dob = ValueHandler.ConvertDate(DOB)

            ._SurgDoc = GPName
            ._SurgName = GPSurgery
            ._SurgTel = GPTel

            ._HvName = HVName
            ._HvTel = HVTel

            ._MedAllergies = Med1 + vbCrLf + Med2 + vbCrLf + Med3
            ._MedNotes = MedOther

            .Store()

        End With

    End Sub

    Private Sub CreateContact(ByVal FamilyID As Guid, ByVal SiteName As String, ByVal Name As String, ByVal Relationship As String, _
                              ByVal PrimaryContact As Boolean, ByVal Emergency As Boolean, ByVal Password As String, _
                              ByVal TelHome As String, ByVal TelWork As String, ByVal TelMobile As String, ByVal Email As String)

        Dim _Forename As String = GetName(Name, True)
        Dim _Surname As String = GetName(Name, False)

        'ensure the contact has not already been created
        If DuplicateContact(FamilyID, TelMobile) Then Exit Sub

        Dim _C As New Business.Contact
        With _C

            ._FamilyId = FamilyID
            ._SmsName = SiteName

            ._Forename = _Forename
            ._Surname = _Surname
            ._Fullname = ._Forename + " " + ._Surname

            ._PrimaryCont = PrimaryContact
            ._EmerCont = Emergency

            ._Relationship = Relationship
            ._Password = Password

            ._TelHome = TelHome
            ._JobTel = TelWork
            ._TelMobile = TelMobile
            ._Email = Email

            .Store()

        End With

    End Sub

    Private Function DuplicateContact(ByVal FamilyID As Guid, ByVal MobileNumber As String) As Boolean

        MobileNumber = MobileNumber.Replace(" ", "")

        Dim _SQL As String = ""

        _SQL += "select * from Contacts"
        _SQL += " where family_id = '" + FamilyID.ToString + "'"
        _SQL += " and replace(tel_mobile, ' ', '') = '" + MobileNumber + "'"

        Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DR IsNot Nothing Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Function GetName(ByVal NameIn As String, ByVal FirstName As Boolean) As String

        If NameIn = "" Then Return ""

        Dim _Words() As String = NameIn.Split(CChar(" "))

        If _Words.Count > 1 Then
            If FirstName Then
                Return _Words(0)
            Else
                Return _Words(_Words.Count - 1)
            End If
        Else
            Return NameIn
        End If

    End Function

    Private Sub CreateFamily(ByVal FamilyID As Guid, ByVal SiteID As Guid, ByVal SiteName As String, _
                             ByVal Surname As String, ByVal Letters As String, ByVal Address As String, ByVal Postcode As String)

        Dim _F As New Business.Family
        With _F
            ._ID = FamilyID
            ._SiteId = SiteID
            ._SiteName = SiteName
            ._Surname = Surname
            ._LetterName = Letters
            ._Address = Address
            ._Postcode = Postcode
            .Store()
        End With

    End Sub

    Private Function BuildAddress(ByVal Add1 As String, ByVal Add2 As String, ByVal Add3 As String, ByVal Add4 As String, ByVal PostCode As String) As String

        Dim _Return As String = ""

        If Add1.Trim <> "" Then _Return += Add1.Trim

        If Add2.Trim <> "" Then
            If _Return <> "" Then _Return += vbCrLf
            _Return += Add2.Trim
        End If

        If Add3.Trim <> "" Then
            If _Return <> "" Then _Return += vbCrLf
            _Return += Add3.Trim
        End If

        If Add4.Trim <> "" Then
            If _Return <> "" Then _Return += vbCrLf
            _Return += Add4.Trim
        End If

        If PostCode.Trim <> "" Then
            If _Return <> "" Then _Return += vbCrLf
            _Return += PostCode.Trim
        End If

        Return _Return

    End Function

End Class