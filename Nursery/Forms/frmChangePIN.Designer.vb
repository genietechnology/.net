﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmChangePIN
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl1 = New Care.Controls.CareFrame(Me.components)
        Me.txtPIN = New Care.Controls.CareTextBox(Me.components)
        Me.Label1 = New Label()
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.btnChange = New Care.Controls.CareButton(Me.components)
        Me.btnReset = New Care.Controls.CareButton(Me.components)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtPIN.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.txtPIN)
        Me.GroupControl1.Controls.Add(Me.Label1)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(288, 45)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "GroupControl1"
        '
        'txtPIN
        '
        Me.txtPIN.CharacterCasing = CharacterCasing.Normal
        Me.txtPIN.EnterMoveNextControl = True
        Me.txtPIN.Location = New System.Drawing.Point(43, 13)
        Me.txtPIN.MaxLength = 4
        Me.txtPIN.Name = "txtPIN"
        Me.txtPIN.NumericAllowNegatives = False
        Me.txtPIN.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtPIN.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPIN.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPIN.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPIN.Properties.Appearance.Options.UseFont = True
        Me.txtPIN.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPIN.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPIN.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPIN.Properties.MaxLength = 4
        Me.txtPIN.Size = New System.Drawing.Size(81, 22)
        Me.txtPIN.TabIndex = 1
        Me.txtPIN.Tag = "N"
        Me.txtPIN.TextAlign = HorizontalAlignment.Left
        Me.txtPIN.ToolTipText = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(24, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "PIN"
        '
        'btnClose
        '
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(215, 66)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(85, 25)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "Cancel"
        '
        'btnChange
        '
        Me.btnChange.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnChange.Appearance.Options.UseFont = True
        Me.btnChange.Location = New System.Drawing.Point(124, 66)
        Me.btnChange.Name = "btnChange"
        Me.btnChange.Size = New System.Drawing.Size(85, 25)
        Me.btnChange.TabIndex = 2
        Me.btnChange.Text = "Change PIN"
        '
        'btnReset
        '
        Me.btnReset.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.Appearance.Options.UseFont = True
        Me.btnReset.Location = New System.Drawing.Point(12, 66)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(85, 25)
        Me.btnReset.TabIndex = 1
        Me.btnReset.Text = "Reset PIN"
        '
        'frmChangePIN
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(312, 98)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnChange)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmChangePIN"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "Change PIN"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtPIN.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupControl1 As Care.Controls.CareFrame
    Friend WithEvents txtPIN As Care.Controls.CareTextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents btnChange As Care.Controls.CareButton
    Friend WithEvents btnReset As Care.Controls.CareButton
End Class
