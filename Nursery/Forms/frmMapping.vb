﻿Imports Care.Global
Imports System.Drawing
Imports System.Threading
Imports System.Windows.Forms
Imports Care.Data
Imports Care.Shared
Imports DevExpress.XtraMap
Imports DevExpress.XtraRichEdit.Commands

Public Class frmMapping

    Private Const MyBingKey As String = "AsEyZ0Lpuc2YGT50FhbMMyuSE0iVVkg-_x63CJjKsh5KxIauIAOiFU10SMGSTrJ1"

    Private m_PrimarySite As String = ParameterHandler.ReturnString("DEFSITE")
    Private m_PrimarySitePoint As GeoPoint = Nothing
    Private m_Storage As New MapItemStorage
    Private m_Addresses As New List(Of AddressRecord)

    Private Sub frmMapping_Load(sender As Object, e As EventArgs) Handles Me.Load

        Session.CursorWaiting()

        Dim _VectorLayer As New VectorItemsLayer
        _VectorLayer.Data = m_Storage

        MyMap.Layers.Add(_VectorLayer)

        'center over the whole UK
        MyMap.CenterPoint = New GeoPoint(53.9239435773579, -3.63607609375002)
        MyMap.ZoomLevel = 6

    End Sub

    Private Sub frmMapping_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        MapCoordinatesFromDB()

        If m_PrimarySitePoint IsNot Nothing Then
            MyMap.CenterPoint = m_PrimarySitePoint
            MyMap.ZoomLevel = 14
        End If

        Session.CursorDefault()

    End Sub

    Private Sub MapCoordinatesFromDB()

        m_Addresses.Clear()

        Dim _Sites As List(Of Business.Site) = Business.Site.RetreiveAll()
        For Each _Site In _Sites

            If _Site._Coordinates <> "" Then

                m_Addresses.Add(New AddressRecord("Site", _Site._ID.ToString, _Site._Address, _Site._Name, _Site._Coordinates))

                If m_PrimarySite = _Site._Name Then
                    Dim _Pipe As Integer = _Site._Coordinates.IndexOf("|")
                    Dim _Lat As Double = CDbl(_Site._Coordinates.Substring(0, _Pipe))
                    Dim _Long As Double = CDbl(_Site._Coordinates.Substring(_Pipe + 1))
                    m_PrimarySitePoint = New GeoPoint(_Lat, _Long)
                End If

            End If

        Next

        Dim _Families As List(Of Business.Family) = Business.Family.RetrieveCurrentFamilies
        For Each _F In _Families
            If _F._Coordinates <> "" Then
                m_Addresses.Add(New AddressRecord("Family", _F._ID.ToString, _F._Address, _F.ChildNames, _F._Coordinates))
            End If
        Next

        Dim _Staff As List(Of Business.Staff) = Business.Staff.RetrieveLiveStaff
        For Each _S In _Staff
            If _S._Coordinates <> "" Then
                m_Addresses.Add(New AddressRecord("Staff", _S._ID.ToString, _S._Address, _S._Fullname, _S._Coordinates))
            End If
        Next

        Dim _Leads As List(Of Business.Lead) = Business.Lead.RetrieveOpenLeads
        For Each _L In _Leads
            If _L._Coordinates <> "" Then
                m_Addresses.Add(New AddressRecord("Lead", _L._ID.ToString, _L._ContactAddress, _L._ContactFullname, _L._Coordinates))
            End If
        Next

        For Each _Add In m_Addresses
            AddPin(_Add)
        Next

    End Sub

    Private Sub UpdateDB()

        For Each _A In m_Addresses

            If _A.Latitude <> 0 AndAlso _A.Longitude <> 0 Then

                Dim _Coordinates As String = _A.Latitude.ToString + "|" + _A.Longitude.ToString

                If _A.TableName = "Lead" Then UpdateTable("Leads", _A.RecordID, _Coordinates)
                If _A.TableName = "Site" Then UpdateTable("Sites", _A.RecordID, _Coordinates)
                If _A.TableName = "Family" Then UpdateTable("Family", _A.RecordID, _Coordinates)
                If _A.TableName = "Staff" Then UpdateTable("Staff", _A.RecordID, _Coordinates)

            End If

        Next

    End Sub

    Private Sub UpdateTable(ByVal TableName As String, ByVal RecordID As String, ByVal Coordinates As String)
        Dim _SQL As String = "update " + TableName + " set coordinates = '" + Coordinates + "' where ID = '" + RecordID + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)
    End Sub

    Private Sub AddPin(ByRef AddressRecordIn As AddressRecord)

        If AddressRecordIn.Latitude = 0 Then Exit Sub
        If AddressRecordIn.Longitude = 0 Then Exit Sub

        Dim _Pin As New MapPushpin

        Dim _Image As Image = Nothing
        If AddressRecordIn.TableName = "Site" Then _Image = My.Resources.House
        If AddressRecordIn.TableName = "Family" Then _Image = My.Resources.Baby
        If AddressRecordIn.TableName = "Staff" Then _Image = My.Resources.Staff
        If AddressRecordIn.TableName = "Lead" Then _Image = My.Resources.Lead

        _Pin.Image = _Image
        _Pin.Location = New GeoPoint(AddressRecordIn.Latitude, AddressRecordIn.Longitude)

        Dim _Att As New MapItemAttribute
        _Att.Name = "AddressRecord"
        _Att.Value = AddressRecordIn
        _Pin.Attributes.Add(_Att)

        _Pin.ToolTipPattern = AddressRecordIn.ToolTip

        m_Storage.Items.Add(_Pin)

    End Sub

    Private Sub GenerateGeoPoints()

        btnGenerate.Enabled = False
        Session.CursorWaiting()

        m_Addresses.Clear()

        Dim _Sites As List(Of Business.Site) = Business.Site.RetreiveAll()
        For Each _Site In _Sites
            If _Site._Coordinates = "" Then
                m_Addresses.Add(New AddressRecord("Site", _Site._ID.ToString, _Site._Address, _Site._Name))
            End If
        Next

        Dim _Families As List(Of Business.Family) = Business.Family.RetreiveAll()
        For Each _F In _Families
            If _F._Coordinates = "" Then
                m_Addresses.Add(New AddressRecord("Family", _F._ID.ToString, _F._Address, _F.ChildNames))
            End If
        Next

        Dim _Staff As List(Of Business.Staff) = Business.Staff.RetreiveAll()
        For Each _S In _Staff
            If _S._Coordinates = "" Then
                m_Addresses.Add(New AddressRecord("Staff", _S._ID.ToString, _S._Address, _S._Fullname))
            End If
        Next

        Dim _Leads As List(Of Business.Lead) = Business.Lead.RetreiveAll()
        For Each _L In _Leads
            If _L._Coordinates = "" Then
                m_Addresses.Add(New AddressRecord("Lead", _L._ID.ToString, _L._ContactAddress, _L._ContactFullname))
            End If
        Next

        timRefresh.Start()

    End Sub

#Region "Control Events"

    Private Sub timRefresh_Tick(sender As Object, e As EventArgs) Handles timRefresh.Tick

        Dim _Searching As Integer = 0
        For Each _A In m_Addresses
            If _A.Searching Then
                _Searching += 1
            Else
                AddPin(_A)
            End If
        Next

        If _Searching = 0 Then

            timRefresh.Stop()

            UpdateDB()

            btnGenerate.Enabled = True
            Session.CursorDefault()

        Else
            timRefresh.Start()
        End If

    End Sub

    Private Sub btnGenerate_Click(sender As Object, e As EventArgs) Handles btnGenerate.Click
        If CareMessage("Are you sure you want to Generate Geopoints? This might take a few minutes...", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Generate GeoPoints") = DialogResult.Yes Then
            GenerateGeoPoints()
        End If
    End Sub

    Private Sub MyMap_MapItemDoubleClick(sender As Object, e As MapItemClickEventArgs) Handles MyMap.MapItemDoubleClick

        If e.Item.Attributes.Count = 1 Then

            Dim _Address As AddressRecord = CType(e.Item.Attributes.First.Value, AddressRecord)
            If _Address IsNot Nothing Then

                Dim _ID As Guid = New Guid(_Address.RecordID)
                Select Case _Address.TableName

                    Case "Family"
                        Session.CursorWaiting()
                        Business.Family.DrillDown(_ID)

                    Case "Staff"
                        Session.CursorWaiting()
                        Business.Staff.DrillDown(_ID)

                    Case "Lead"
                        Session.CursorWaiting()
                        Dim _frmLead As New frmLead(_ID)
                        _frmLead.ShowDialog()
                        _frmLead.Dispose()

                End Select

            End If

        End If

    End Sub

#End Region

    Private Class AddressRecord

        Private WithEvents _SearchProvider As New BingSearchDataProvider

        Property TableName As String
        Property RecordID As String
        Property Address As String
        Property ToolTip As String
        Property Latitude As Double
        Property Longitude As Double

        Property Searching As Boolean

        Public Sub New(ByVal TableNameIn As String, ByVal RecordIDIn As String, ByVal AddressIn As String, ByVal TooltipIn As String)
            Me.TableName = TableNameIn
            Me.RecordID = RecordIDIn
            Me.Address = FormatAddress(AddressIn)
            Me.ToolTip = TooltipIn
            Me.Longitude = 0
            Me.Latitude = 0
            Me.Searching = False
            GetGEOCode()
        End Sub

        Public Sub New(ByVal TableNameIn As String, ByVal RecordIDIn As String, ByVal AddressIn As String, ByVal TooltipIn As String, ByVal Coordinates As String)

            Me.TableName = TableNameIn
            Me.RecordID = RecordIDIn
            Me.Address = FormatAddress(AddressIn)
            Me.ToolTip = TooltipIn

            If Coordinates <> "" Then

                Dim _Pipe As Integer = Coordinates.IndexOf("|")
                Dim _Lat As String = Coordinates.Substring(0, _Pipe)
                Dim _Long As String = Coordinates.Substring(_Pipe + 1)

                Me.Latitude = CDbl(_Lat)
                Me.Longitude = CDbl(_Long)

            End If

            Me.Searching = False

        End Sub

        Private Function FormatAddress(ByVal AddressIn As String) As String
            Dim _Return As String = AddressIn
            _Return = _Return.Replace(Chr(10), ",")
            _Return = _Return.Replace(Chr(13), ",")
            _Return = _Return.Replace(" ,", ",")
            _Return = _Return.Replace(",,", ",")
            _Return = _Return.Replace(",,,", ",")
            Return _Return
        End Function

        Private Sub GetGEOCode()

            '_SearchProvider.BingKey = MyBingKey
            '_SearchProvider.SearchOptions.DistanceUnit = DistanceMeasureUnit.Mile
            '_SearchProvider.SearchOptions.SearchRadius = 50
            '_SearchProvider.SearchOptions.ResultsCount = 5
            '_SearchProvider.Search(Me.Address)

            'Me.Searching = True

        End Sub

        Private Sub MySearchProvider_SearchCompleted(sender As Object, e As BingSearchCompletedEventArgs) Handles _SearchProvider.SearchCompleted
            'Me.Searching = False
            'If e.RequestResult IsNot Nothing Then
            '    If e.RequestResult.ResultCode = RequestResultCode.Success Then
            '            If e.RequestResult.SearchRegion IsNot Nothing Then
            '                Me.Latitude = e.RequestResult.SearchRegion.Location.Latitude
            '                Me.Longitude = e.RequestResult.SearchRegion.Location.Longitude
            '            End If
            '        End If
            '    End If
        End Sub

    End Class

End Class
