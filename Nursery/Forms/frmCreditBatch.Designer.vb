﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmCreditBatch
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl1 = New Care.Controls.CareFrame(Me.components)
        Me.txtComments = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.btnPost = New Care.Controls.CareButton(Me.components)
        Me.txtStatus = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.txtBatchNo = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.cgbCredits = New Care.Controls.CareGrid()
        Me.btnAmendCredit = New Care.Controls.CareButton(Me.components)
        Me.btnDeleteCredit = New Care.Controls.CareButton(Me.components)
        Me.btnAddCredit = New Care.Controls.CareButton(Me.components)
        Me.txtRed = New Care.Controls.CareTextBox(Me.components)
        Me.txtYellow = New Care.Controls.CareTextBox(Me.components)
        Me.txtBlue = New Care.Controls.CareTextBox(Me.components)
        Me.txtGreen = New Care.Controls.CareTextBox(Me.components)
        Me.btnPrintBatch = New Care.Controls.CareButton(Me.components)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtComments.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBatchNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBlue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(857, 3)
        Me.btnCancel.TabIndex = 6
        Me.btnCancel.Text = "Close"
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(766, 3)
        Me.btnOK.TabIndex = 5
        Me.btnOK.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnPrintBatch)
        Me.Panel1.Controls.Add(Me.btnAddCredit)
        Me.Panel1.Controls.Add(Me.btnDeleteCredit)
        Me.Panel1.Controls.Add(Me.btnAmendCredit)
        Me.Panel1.Location = New System.Drawing.Point(0, 488)
        Me.Panel1.Size = New System.Drawing.Size(954, 36)
        Me.Panel1.TabIndex = 4
        Me.Panel1.Controls.SetChildIndex(Me.btnAmendCredit, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnDeleteCredit, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnAddCredit, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnPrintBatch, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnOK, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnCancel, 0)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.txtComments)
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Controls.Add(Me.btnPost)
        Me.GroupControl1.Controls.Add(Me.txtStatus)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.txtBatchNo)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 53)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(930, 87)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Batch Details"
        '
        'txtComments
        '
        Me.txtComments.CharacterCasing = CharacterCasing.Normal
        Me.txtComments.EnterMoveNextControl = True
        Me.txtComments.Location = New System.Drawing.Point(95, 57)
        Me.txtComments.MaxLength = 0
        Me.txtComments.Name = "txtComments"
        Me.txtComments.NumericAllowNegatives = False
        Me.txtComments.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtComments.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtComments.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtComments.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.txtComments.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtComments.Properties.Appearance.Options.UseBackColor = True
        Me.txtComments.Properties.Appearance.Options.UseFont = True
        Me.txtComments.Properties.Appearance.Options.UseTextOptions = True
        Me.txtComments.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtComments.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtComments.Properties.ReadOnly = True
        Me.txtComments.Size = New System.Drawing.Size(825, 22)
        Me.txtComments.TabIndex = 8
        Me.txtComments.TabStop = False
        Me.txtComments.TextAlign = HorizontalAlignment.Left
        Me.txtComments.ToolTipText = ""
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(12, 60)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(59, 15)
        Me.CareLabel3.TabIndex = 7
        Me.CareLabel3.Text = "Comments"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnPost
        '
        Me.btnPost.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnPost.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnPost.Appearance.Options.UseFont = True
        Me.btnPost.CausesValidation = False
        Me.btnPost.Location = New System.Drawing.Point(780, 27)
        Me.btnPost.Name = "btnPost"
        Me.btnPost.Size = New System.Drawing.Size(140, 25)
        Me.btnPost.TabIndex = 6
        Me.btnPost.Tag = ""
        Me.btnPost.Text = "Post Batch"
        Me.btnPost.ToolTip = "Once all invoices have been printed and/or emailed you can post the batch." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Pos" &
    "ting the batch updates your Financial System or Accounts package if integration " &
    "has been setup." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnPost.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.btnPost.ToolTipTitle = "Post Batch"
        '
        'txtStatus
        '
        Me.txtStatus.CharacterCasing = CharacterCasing.Normal
        Me.txtStatus.EnterMoveNextControl = True
        Me.txtStatus.Location = New System.Drawing.Point(249, 29)
        Me.txtStatus.MaxLength = 0
        Me.txtStatus.Name = "txtStatus"
        Me.txtStatus.NumericAllowNegatives = False
        Me.txtStatus.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtStatus.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStatus.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtStatus.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.txtStatus.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtStatus.Properties.Appearance.Options.UseBackColor = True
        Me.txtStatus.Properties.Appearance.Options.UseFont = True
        Me.txtStatus.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStatus.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtStatus.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtStatus.Properties.ReadOnly = True
        Me.txtStatus.Size = New System.Drawing.Size(198, 22)
        Me.txtStatus.TabIndex = 3
        Me.txtStatus.TabStop = False
        Me.txtStatus.TextAlign = HorizontalAlignment.Left
        Me.txtStatus.ToolTipText = ""
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(211, 32)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel2.TabIndex = 2
        Me.CareLabel2.Text = "Status"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBatchNo
        '
        Me.txtBatchNo.CharacterCasing = CharacterCasing.Normal
        Me.txtBatchNo.EnterMoveNextControl = True
        Me.txtBatchNo.Location = New System.Drawing.Point(95, 29)
        Me.txtBatchNo.MaxLength = 0
        Me.txtBatchNo.Name = "txtBatchNo"
        Me.txtBatchNo.NumericAllowNegatives = False
        Me.txtBatchNo.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtBatchNo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBatchNo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtBatchNo.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.txtBatchNo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtBatchNo.Properties.Appearance.Options.UseBackColor = True
        Me.txtBatchNo.Properties.Appearance.Options.UseFont = True
        Me.txtBatchNo.Properties.Appearance.Options.UseTextOptions = True
        Me.txtBatchNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtBatchNo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtBatchNo.Properties.ReadOnly = True
        Me.txtBatchNo.Size = New System.Drawing.Size(100, 22)
        Me.txtBatchNo.TabIndex = 1
        Me.txtBatchNo.TabStop = False
        Me.txtBatchNo.TextAlign = HorizontalAlignment.Left
        Me.txtBatchNo.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(12, 32)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(77, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Batch Number"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cgbCredits
        '
        Me.cgbCredits.AllowBuildColumns = True
        Me.cgbCredits.AllowEdit = False
        Me.cgbCredits.AllowHorizontalScroll = False
        Me.cgbCredits.AllowMultiSelect = False
        Me.cgbCredits.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgbCredits.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgbCredits.Appearance.Options.UseFont = True
        Me.cgbCredits.AutoSizeByData = True
        Me.cgbCredits.DisableAutoSize = False
        Me.cgbCredits.DisableDataFormatting = False
        Me.cgbCredits.FocusedRowHandle = -2147483648
        Me.cgbCredits.HideFirstColumn = False
        Me.cgbCredits.Location = New System.Drawing.Point(12, 146)
        Me.cgbCredits.Name = "cgbCredits"
        Me.cgbCredits.PreviewColumn = ""
        Me.cgbCredits.QueryID = Nothing
        Me.cgbCredits.RowAutoHeight = False
        Me.cgbCredits.SearchAsYouType = True
        Me.cgbCredits.ShowAutoFilterRow = True
        Me.cgbCredits.ShowFindPanel = True
        Me.cgbCredits.ShowGroupByBox = False
        Me.cgbCredits.ShowLoadingPanel = False
        Me.cgbCredits.ShowNavigator = False
        Me.cgbCredits.Size = New System.Drawing.Size(930, 336)
        Me.cgbCredits.TabIndex = 2
        '
        'btnAmendCredit
        '
        Me.btnAmendCredit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnAmendCredit.Appearance.Options.UseFont = True
        Me.btnAmendCredit.CausesValidation = False
        Me.btnAmendCredit.Location = New System.Drawing.Point(163, 3)
        Me.btnAmendCredit.Name = "btnAmendCredit"
        Me.btnAmendCredit.Size = New System.Drawing.Size(145, 25)
        Me.btnAmendCredit.TabIndex = 1
        Me.btnAmendCredit.Tag = ""
        Me.btnAmendCredit.Text = "Amend Credit Note"
        '
        'btnDeleteCredit
        '
        Me.btnDeleteCredit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnDeleteCredit.Appearance.Options.UseFont = True
        Me.btnDeleteCredit.CausesValidation = False
        Me.btnDeleteCredit.Location = New System.Drawing.Point(314, 3)
        Me.btnDeleteCredit.Name = "btnDeleteCredit"
        Me.btnDeleteCredit.Size = New System.Drawing.Size(145, 25)
        Me.btnDeleteCredit.TabIndex = 2
        Me.btnDeleteCredit.Tag = ""
        Me.btnDeleteCredit.Text = "Delete Credit Note"
        '
        'btnAddCredit
        '
        Me.btnAddCredit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnAddCredit.Appearance.Options.UseFont = True
        Me.btnAddCredit.CausesValidation = False
        Me.btnAddCredit.Location = New System.Drawing.Point(12, 3)
        Me.btnAddCredit.Name = "btnAddCredit"
        Me.btnAddCredit.Size = New System.Drawing.Size(145, 25)
        Me.btnAddCredit.TabIndex = 0
        Me.btnAddCredit.Tag = ""
        Me.btnAddCredit.Text = "Add Credit Note"
        '
        'txtRed
        '
        Me.txtRed.CharacterCasing = CharacterCasing.Normal
        Me.txtRed.EnterMoveNextControl = True
        Me.txtRed.Location = New System.Drawing.Point(24, 451)
        Me.txtRed.MaxLength = 0
        Me.txtRed.Name = "txtRed"
        Me.txtRed.NumericAllowNegatives = False
        Me.txtRed.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRed.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRed.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRed.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtRed.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRed.Properties.Appearance.Options.UseBackColor = True
        Me.txtRed.Properties.Appearance.Options.UseFont = True
        Me.txtRed.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRed.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRed.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRed.Size = New System.Drawing.Size(25, 22)
        Me.txtRed.TabIndex = 7
        Me.txtRed.TextAlign = HorizontalAlignment.Left
        Me.txtRed.ToolTipText = ""
        Me.txtRed.Visible = False
        '
        'txtYellow
        '
        Me.txtYellow.CharacterCasing = CharacterCasing.Normal
        Me.txtYellow.EnterMoveNextControl = True
        Me.txtYellow.Location = New System.Drawing.Point(115, 451)
        Me.txtYellow.MaxLength = 0
        Me.txtYellow.Name = "txtYellow"
        Me.txtYellow.NumericAllowNegatives = False
        Me.txtYellow.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtYellow.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtYellow.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtYellow.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtYellow.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtYellow.Properties.Appearance.Options.UseBackColor = True
        Me.txtYellow.Properties.Appearance.Options.UseFont = True
        Me.txtYellow.Properties.Appearance.Options.UseTextOptions = True
        Me.txtYellow.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtYellow.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtYellow.Size = New System.Drawing.Size(25, 22)
        Me.txtYellow.TabIndex = 10
        Me.txtYellow.TextAlign = HorizontalAlignment.Left
        Me.txtYellow.ToolTipText = ""
        Me.txtYellow.Visible = False
        '
        'txtBlue
        '
        Me.txtBlue.CharacterCasing = CharacterCasing.Normal
        Me.txtBlue.EnterMoveNextControl = True
        Me.txtBlue.Location = New System.Drawing.Point(84, 451)
        Me.txtBlue.MaxLength = 0
        Me.txtBlue.Name = "txtBlue"
        Me.txtBlue.NumericAllowNegatives = False
        Me.txtBlue.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtBlue.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBlue.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtBlue.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBlue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtBlue.Properties.Appearance.Options.UseBackColor = True
        Me.txtBlue.Properties.Appearance.Options.UseFont = True
        Me.txtBlue.Properties.Appearance.Options.UseTextOptions = True
        Me.txtBlue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtBlue.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtBlue.Size = New System.Drawing.Size(25, 22)
        Me.txtBlue.TabIndex = 9
        Me.txtBlue.TextAlign = HorizontalAlignment.Left
        Me.txtBlue.ToolTipText = ""
        Me.txtBlue.Visible = False
        '
        'txtGreen
        '
        Me.txtGreen.CharacterCasing = CharacterCasing.Normal
        Me.txtGreen.EnterMoveNextControl = True
        Me.txtGreen.Location = New System.Drawing.Point(53, 451)
        Me.txtGreen.MaxLength = 0
        Me.txtGreen.Name = "txtGreen"
        Me.txtGreen.NumericAllowNegatives = False
        Me.txtGreen.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtGreen.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtGreen.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtGreen.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtGreen.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtGreen.Properties.Appearance.Options.UseBackColor = True
        Me.txtGreen.Properties.Appearance.Options.UseFont = True
        Me.txtGreen.Properties.Appearance.Options.UseTextOptions = True
        Me.txtGreen.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtGreen.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtGreen.Size = New System.Drawing.Size(25, 22)
        Me.txtGreen.TabIndex = 8
        Me.txtGreen.TextAlign = HorizontalAlignment.Left
        Me.txtGreen.ToolTipText = ""
        Me.txtGreen.Visible = False
        '
        'btnPrintBatch
        '
        Me.btnPrintBatch.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnPrintBatch.Appearance.Options.UseFont = True
        Me.btnPrintBatch.CausesValidation = False
        Me.btnPrintBatch.Location = New System.Drawing.Point(465, 3)
        Me.btnPrintBatch.Name = "btnPrintBatch"
        Me.btnPrintBatch.Size = New System.Drawing.Size(140, 25)
        Me.btnPrintBatch.TabIndex = 5
        Me.btnPrintBatch.Tag = ""
        Me.btnPrintBatch.Text = "Print Batch Summary"
        Me.btnPrintBatch.Visible = False
        '
        'frmCreditBatch
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(954, 524)
        Me.Controls.Add(Me.txtRed)
        Me.Controls.Add(Me.txtYellow)
        Me.Controls.Add(Me.txtBlue)
        Me.Controls.Add(Me.txtGreen)
        Me.Controls.Add(Me.cgbCredits)
        Me.Controls.Add(Me.GroupControl1)
        Me.MaximizeBox = True
        Me.Name = "frmCreditBatch"
        Me.WindowState = FormWindowState.Maximized
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        Me.Controls.SetChildIndex(Me.cgbCredits, 0)
        Me.Controls.SetChildIndex(Me.txtGreen, 0)
        Me.Controls.SetChildIndex(Me.txtBlue, 0)
        Me.Controls.SetChildIndex(Me.txtYellow, 0)
        Me.Controls.SetChildIndex(Me.txtRed, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtComments.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBatchNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBlue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnPost As Care.Controls.CareButton
    Friend WithEvents txtStatus As Care.Controls.CareTextBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents txtBatchNo As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents btnAmendCredit As Care.Controls.CareButton
    Friend WithEvents cgbCredits As Care.Controls.CareGrid
    Friend WithEvents btnDeleteCredit As Care.Controls.CareButton
    Friend WithEvents btnAddCredit As Care.Controls.CareButton
    Friend WithEvents txtRed As Care.Controls.CareTextBox
    Friend WithEvents txtYellow As Care.Controls.CareTextBox
    Friend WithEvents txtBlue As Care.Controls.CareTextBox
    Friend WithEvents txtGreen As Care.Controls.CareTextBox
    Friend WithEvents txtComments As Care.Controls.CareTextBox
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents GroupControl1 As Care.Controls.CareFrame
    Friend WithEvents btnPrintBatch As Care.Controls.CareButton
End Class
