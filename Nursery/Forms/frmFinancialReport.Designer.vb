﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmFinancialReport
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.sccHorizontal = New DevExpress.XtraEditors.SplitContainerControl()
        Me.cgMonths = New Care.Controls.CareGrid()
        Me.cht = New DevExpress.XtraCharts.ChartControl()
        Me.btnRefresh = New Care.Controls.CareButton(Me.components)
        Me.gbxSites = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.cbxYear = New Care.Controls.CareComboBox(Me.components)
        Me.cbxSite = New Care.Controls.CareComboBox(Me.components)
        Me.GroupControl1 = New Care.Controls.CareFrame(Me.components)
        Me.btnGenerateDays = New Care.Controls.CareButton(Me.components)
        Me.radForecast = New Care.Controls.CareRadioButton(Me.components)
        Me.radTurnover = New Care.Controls.CareRadioButton(Me.components)
        Me.GroupControl2 = New Care.Controls.CareFrame(Me.components)
        Me.BookingsEngine1 = New Nursery.BookingsEngine(Me.components)
        Me.CareFrame1 = New Care.Controls.CareFrame(Me.components)
        Me.grdTasks = New Care.Controls.CareGrid()
        CType(Me.sccHorizontal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.sccHorizontal.SuspendLayout()
        CType(Me.cht, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxSites, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxSites.SuspendLayout()
        CType(Me.cbxYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.radForecast.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radTurnover.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.CareFrame1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CareFrame1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'sccHorizontal
        '
        Me.sccHorizontal.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.sccHorizontal.Horizontal = False
        Me.sccHorizontal.Location = New System.Drawing.Point(12, 83)
        Me.sccHorizontal.Name = "sccHorizontal"
        Me.sccHorizontal.Panel1.Controls.Add(Me.cgMonths)
        Me.sccHorizontal.Panel1.Text = "Panel1"
        Me.sccHorizontal.Panel2.Controls.Add(Me.cht)
        Me.sccHorizontal.Panel2.Text = "Panel2"
        Me.sccHorizontal.Size = New System.Drawing.Size(834, 505)
        Me.sccHorizontal.SplitterPosition = 274
        Me.sccHorizontal.TabIndex = 4
        Me.sccHorizontal.Text = "SplitContainerControl2"
        '
        'cgMonths
        '
        Me.cgMonths.AllowBuildColumns = True
        Me.cgMonths.AllowEdit = False
        Me.cgMonths.AllowHorizontalScroll = False
        Me.cgMonths.AllowMultiSelect = False
        Me.cgMonths.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgMonths.Appearance.Options.UseFont = True
        Me.cgMonths.AutoSizeByData = True
        Me.cgMonths.DisableAutoSize = False
        Me.cgMonths.DisableDataFormatting = False
        Me.cgMonths.Dock = DockStyle.Fill
        Me.cgMonths.FocusedRowHandle = -2147483648
        Me.cgMonths.HideFirstColumn = False
        Me.cgMonths.Location = New System.Drawing.Point(0, 0)
        Me.cgMonths.Name = "cgMonths"
        Me.cgMonths.PreviewColumn = ""
        Me.cgMonths.QueryID = Nothing
        Me.cgMonths.RowAutoHeight = False
        Me.cgMonths.SearchAsYouType = True
        Me.cgMonths.ShowAutoFilterRow = False
        Me.cgMonths.ShowFindPanel = False
        Me.cgMonths.ShowGroupByBox = False
        Me.cgMonths.ShowLoadingPanel = False
        Me.cgMonths.ShowNavigator = False
        Me.cgMonths.Size = New System.Drawing.Size(834, 274)
        Me.cgMonths.TabIndex = 0
        '
        'cht
        '
        Me.cht.Dock = DockStyle.Fill
        Me.cht.Location = New System.Drawing.Point(0, 0)
        Me.cht.Name = "cht"
        Me.cht.SeriesSerializable = New DevExpress.XtraCharts.Series(-1) {}
        Me.cht.Size = New System.Drawing.Size(834, 226)
        Me.cht.TabIndex = 3
        '
        'btnRefresh
        '
        Me.btnRefresh.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnRefresh.Location = New System.Drawing.Point(7, 38)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(150, 23)
        Me.btnRefresh.TabIndex = 3
        Me.btnRefresh.Text = "Build Report Data"
        '
        'gbxSites
        '
        Me.gbxSites.Controls.Add(Me.CareLabel1)
        Me.gbxSites.Controls.Add(Me.CareLabel4)
        Me.gbxSites.Controls.Add(Me.cbxYear)
        Me.gbxSites.Controls.Add(Me.cbxSite)
        Me.gbxSites.Location = New System.Drawing.Point(12, 9)
        Me.gbxSites.Name = "gbxSites"
        Me.gbxSites.ShowCaption = False
        Me.gbxSites.Size = New System.Drawing.Size(252, 68)
        Me.gbxSites.TabIndex = 0
        Me.gbxSites.Text = "Site"
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(7, 41)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(23, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Year"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(7, 13)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(19, 15)
        Me.CareLabel4.TabIndex = 0
        Me.CareLabel4.Text = "Site"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxYear
        '
        Me.cbxYear.AllowBlank = False
        Me.cbxYear.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxYear.DataSource = Nothing
        Me.cbxYear.DisplayMember = Nothing
        Me.cbxYear.EnterMoveNextControl = True
        Me.cbxYear.Location = New System.Drawing.Point(36, 38)
        Me.cbxYear.Name = "cbxYear"
        Me.cbxYear.Properties.AccessibleName = "Group"
        Me.cbxYear.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxYear.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxYear.Properties.Appearance.Options.UseFont = True
        Me.cbxYear.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxYear.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxYear.SelectedValue = Nothing
        Me.cbxYear.Size = New System.Drawing.Size(54, 22)
        Me.cbxYear.TabIndex = 1
        Me.cbxYear.Tag = ""
        Me.cbxYear.ValueMember = Nothing
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(36, 10)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AccessibleName = "Group"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(208, 22)
        Me.cbxSite.TabIndex = 1
        Me.cbxSite.Tag = ""
        Me.cbxSite.ValueMember = Nothing
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.btnGenerateDays)
        Me.GroupControl1.Controls.Add(Me.btnRefresh)
        Me.GroupControl1.Location = New System.Drawing.Point(684, 9)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(162, 68)
        Me.GroupControl1.TabIndex = 2
        Me.GroupControl1.Text = "Site"
        '
        'btnGenerateDays
        '
        Me.btnGenerateDays.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnGenerateDays.Location = New System.Drawing.Point(7, 9)
        Me.btnGenerateDays.Name = "btnGenerateDays"
        Me.btnGenerateDays.Size = New System.Drawing.Size(150, 23)
        Me.btnGenerateDays.TabIndex = 2
        Me.btnGenerateDays.Text = "Re-Build Invoice Days"
        '
        'radForecast
        '
        Me.radForecast.EditValue = True
        Me.radForecast.Location = New System.Drawing.Point(5, 11)
        Me.radForecast.Name = "radForecast"
        Me.radForecast.Properties.AutoWidth = True
        Me.radForecast.Properties.Caption = "Forecast"
        Me.radForecast.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radForecast.Properties.RadioGroupIndex = 1
        Me.radForecast.Size = New System.Drawing.Size(64, 19)
        Me.radForecast.TabIndex = 0
        '
        'radTurnover
        '
        Me.radTurnover.Location = New System.Drawing.Point(5, 39)
        Me.radTurnover.Name = "radTurnover"
        Me.radTurnover.Properties.AutoWidth = True
        Me.radTurnover.Properties.Caption = "Turnover"
        Me.radTurnover.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radTurnover.Properties.RadioGroupIndex = 1
        Me.radTurnover.Size = New System.Drawing.Size(66, 19)
        Me.radTurnover.TabIndex = 1
        Me.radTurnover.TabStop = False
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.radTurnover)
        Me.GroupControl2.Controls.Add(Me.radForecast)
        Me.GroupControl2.Location = New System.Drawing.Point(270, 9)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.ShowCaption = False
        Me.GroupControl2.Size = New System.Drawing.Size(80, 68)
        Me.GroupControl2.TabIndex = 1
        Me.GroupControl2.Text = "Site"
        '
        'CareFrame1
        '
        Me.CareFrame1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.CareFrame1.Controls.Add(Me.grdTasks)
        Me.CareFrame1.Location = New System.Drawing.Point(356, 9)
        Me.CareFrame1.Name = "CareFrame1"
        Me.CareFrame1.ShowCaption = False
        Me.CareFrame1.Size = New System.Drawing.Size(322, 68)
        Me.CareFrame1.TabIndex = 5
        Me.CareFrame1.Text = "Site"
        '
        'grdTasks
        '
        Me.grdTasks.AllowBuildColumns = True
        Me.grdTasks.AllowEdit = False
        Me.grdTasks.AllowHorizontalScroll = False
        Me.grdTasks.AllowMultiSelect = False
        Me.grdTasks.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.grdTasks.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdTasks.Appearance.Options.UseFont = True
        Me.grdTasks.AutoSizeByData = True
        Me.grdTasks.DisableAutoSize = False
        Me.grdTasks.DisableDataFormatting = False
        Me.grdTasks.FocusedRowHandle = -2147483648
        Me.grdTasks.HideFirstColumn = False
        Me.grdTasks.Location = New System.Drawing.Point(5, 5)
        Me.grdTasks.Name = "grdTasks"
        Me.grdTasks.PreviewColumn = ""
        Me.grdTasks.QueryID = Nothing
        Me.grdTasks.RowAutoHeight = False
        Me.grdTasks.SearchAsYouType = True
        Me.grdTasks.ShowAutoFilterRow = False
        Me.grdTasks.ShowFindPanel = False
        Me.grdTasks.ShowGroupByBox = False
        Me.grdTasks.ShowLoadingPanel = False
        Me.grdTasks.ShowNavigator = False
        Me.grdTasks.Size = New System.Drawing.Size(312, 58)
        Me.grdTasks.TabIndex = 8
        '
        'frmFinancialReport
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(858, 600)
        Me.Controls.Add(Me.CareFrame1)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.gbxSites)
        Me.Controls.Add(Me.sccHorizontal)
        Me.FormBorderStyle = FormBorderStyle.Sizable
        Me.Name = "frmFinancialReport"
        Me.Text = "frmFinancialReporting"
        Me.WindowState = FormWindowState.Maximized
        CType(Me.sccHorizontal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.sccHorizontal.ResumeLayout(False)
        CType(Me.cht, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxSites, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxSites.ResumeLayout(False)
        Me.gbxSites.PerformLayout()
        CType(Me.cbxYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.radForecast.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radTurnover.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.CareFrame1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CareFrame1.ResumeLayout(False)
        Me.CareFrame1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BookingsEngine1 As BookingsEngine
    Friend WithEvents sccHorizontal As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents cgMonths As Care.Controls.CareGrid
    Friend WithEvents btnRefresh As Care.Controls.CareButton
    Friend WithEvents cht As DevExpress.XtraCharts.ChartControl
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents radForecast As Care.Controls.CareRadioButton
    Friend WithEvents radTurnover As Care.Controls.CareRadioButton
    Friend WithEvents btnGenerateDays As Care.Controls.CareButton
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents cbxYear As Care.Controls.CareComboBox
    Friend WithEvents CareFrame1 As Care.Controls.CareFrame
    Friend WithEvents gbxSites As Care.Controls.CareFrame
    Friend WithEvents GroupControl1 As Care.Controls.CareFrame
    Friend WithEvents GroupControl2 As Care.Controls.CareFrame
    Friend WithEvents grdTasks As Care.Controls.CareGrid
End Class
