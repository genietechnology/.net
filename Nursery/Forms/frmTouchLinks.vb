﻿Imports Care.Global
Imports Care.Data

Public Class frmTouchLinks

    Dim m_TouchLinks As Business.TouchLink

    Private Sub frmTouchLinks_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.GridSQL = "select id, area as 'Area', type as 'Type', name as 'Name'" & _
                     " from TouchLinks" & _
                     " order by area, name"

        cbxType.AddItem("File")
        cbxType.AddItem("URL")

        gbx.Hide()

    End Sub

    Protected Overrides Sub SetBindings()

        m_TouchLinks = New Business.TouchLink
        bs.DataSource = m_TouchLinks

    End Sub

    Protected Overrides Sub BindToID(ID As System.Guid, IsNew As Boolean)

        If IsNew Then
            m_TouchLinks = New Business.TouchLink
        Else
            m_TouchLinks = Business.TouchLink.RetreiveByID(ID)
        End If

        bs.DataSource = m_TouchLinks

        txtArea.Text = m_TouchLinks._Area
        cbxType.Text = m_TouchLinks._Type
        txtName.EditValue = m_TouchLinks._Name
        txtPath.EditValue = m_TouchLinks._Path

    End Sub

    Protected Overrides Sub CommitUpdate()

        m_TouchLinks._Area = txtArea.Text
        m_TouchLinks._Type = cbxType.Text
        m_TouchLinks._Name = txtName.Text
        m_TouchLinks._Path = txtPath.Text

        m_TouchLinks.Store()

    End Sub

    Protected Overrides Sub CommitDelete(ByVal ID As Guid)

        Dim _SQL As String = "delete from TouchLinks where ID = '" + ID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

    End Sub

End Class