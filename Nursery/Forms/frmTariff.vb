﻿

Imports Care.Global
Imports Care.Data

Public Class frmTariff

    Private m_SiteID As Guid?
    Private m_SiteName As String

    Private Sub frmTariff_Load(sender As Object, e As EventArgs) Handles Me.Load

        With cbxSite
            .PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)
        End With

        If cbxSite.Items.Count > 0 Then cbxSite.SelectedIndex = 0

    End Sub

    Private Sub PopulateGrid()

        Dim _SQL As String = ""
        _SQL += "select t.ID, t.type as 'Type', t.name as 'Name', t.rate as 'Rate',"
        _SQL += " t.start_time as 'Start Time', t.end_time as 'End Time', t.duration as 'Duration', t.variable_rate as 'Variable?', t.funding_hours as 'Funding?',"
        _SQL += " t.bolt_ons as 'Bolt-Ons?', t.discount as 'Discount?', t.nl_code as 'NL Code', t.nl_tracking as 'NL Tracking', t.colour as 'Colour', t.summary_column as 'Summary Column'"

        If chkUsage.Checked Then
            _SQL += ", (select count (*) from ChildAdvanceSessions s left join Children c on c.ID = s.child_id where c.status = 'Current' and s.monday = t.ID) as 'Monday'"
            _SQL += ", (select count (*) from ChildAdvanceSessions s left join Children c on c.ID = s.child_id where c.status = 'Current' and s.tuesday = t.ID) as 'Tuesday'"
            _SQL += ", (select count (*) from ChildAdvanceSessions s left join Children c on c.ID = s.child_id where c.status = 'Current' and s.wednesday = t.ID) as 'Wednesday'"
            _SQL += ", (select count (*) from ChildAdvanceSessions s left join Children c on c.ID = s.child_id where c.status = 'Current' and s.thursday = t.ID) as 'Thursday'"
            _SQL += ", (select count (*) from ChildAdvanceSessions s left join Children c on c.ID = s.child_id where c.status = 'Current' and s.friday = t.ID) as 'Friday'"
        End If

        _SQL += " from Tariffs t"
        _SQL += " where t.ID is not null"

        If cbxSite.SelectedIndex >= 0 Then
            _SQL += " and site_name = '" + cbxSite.Text + "'"
        End If

        If radCurrent.Checked Then
            _SQL += " and archived = 0"
        Else
            If radArchived.Checked Then
                _SQL += " and archived = 1"
            End If
        End If

        _SQL += " order by name"

        cgTariffs.Populate(Session.ConnectionString, _SQL)
        cgTariffs.Columns("ID").Visible = False

    End Sub

    Private Sub cbxSite_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSite.SelectedIndexChanged
        If cbxSite.Text <> "" Then
            m_SiteID = New Guid(cbxSite.SelectedValue.ToString)
            m_SiteName = cbxSite.Text
            PopulateGrid()
        End If
    End Sub

    Private Sub radCurrent_CheckedChanged(sender As Object, e As EventArgs) Handles radCurrent.CheckedChanged
        If radCurrent.Checked Then PopulateGrid()
    End Sub

    Private Sub radFuture_CheckedChanged(sender As Object, e As EventArgs) Handles radFuture.CheckedChanged
        If radFuture.Checked Then PopulateGrid()
    End Sub

    Private Sub radArchived_CheckedChanged(sender As Object, e As EventArgs) Handles radArchived.CheckedChanged
        If radArchived.Checked Then PopulateGrid()
    End Sub

    Private Sub cgTariffs_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles cgTariffs.RowStyle
        If e IsNot Nothing AndAlso e.RowHandle >= 0 Then
            If chkShowColours.Checked Then
                Dim _ColourString As String = cgTariffs.UnderlyingGridView.GetRowCellDisplayText(e.RowHandle, "Colour")
                If _ColourString <> "" Then
                    Try
                        e.Appearance.BackColor = Drawing.Color.FromName(_ColourString)
                    Catch ex As Exception

                    End Try
                End If
            End If
        End If
    End Sub

    Private Sub chkShowColours_CheckedChanged(sender As Object, e As EventArgs) Handles chkShowColours.CheckedChanged
        PopulateGrid()
    End Sub

    Private Sub chkUsage_CheckedChanged(sender As Object, e As EventArgs) Handles chkUsage.CheckedChanged
        PopulateGrid()
    End Sub

    Private Sub cgTariffs_GridDoubleClick(sender As Object, e As EventArgs) Handles cgTariffs.GridDoubleClick
        EditTariff()
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        EditTariff()
    End Sub

    Private Function GetID() As Guid?
        If cgTariffs Is Nothing Then Return Nothing
        If cgTariffs.RecordCount < 1 Then Return Nothing
        If cgTariffs.CurrentRow(0).ToString = "" Then Return Nothing
        Return New Guid(cgTariffs.CurrentRow(0).ToString)
    End Function

    Private Function GetName() As String
        If cgTariffs Is Nothing Then Return Nothing
        If cgTariffs.RecordCount < 1 Then Return Nothing
        Return cgTariffs.CurrentRow("name").ToString
    End Function

    Private Sub EditTariff()

        Dim _ID As Guid? = GetID()
        If _ID.HasValue Then

            cgTariffs.PersistRowPosition()

            Dim _frmTariffPopup As New frmTariffPopup(m_SiteID.Value, m_SiteName, _ID)
            _frmTariffPopup.ShowDialog()

            If _frmTariffPopup.DialogResult = DialogResult.OK Then
                PopulateGrid()
            End If

            cgTariffs.RestoreRowPosition()

        End If

    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click

        Dim _frmTariffPopup As New frmTariffPopup(m_SiteID.Value, m_SiteName, Nothing)
        _frmTariffPopup.ShowDialog()

        If _frmTariffPopup.DialogResult = DialogResult.OK Then
            PopulateGrid()
        End If

    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Function SafeToRemove(ByVal TariffID As String, ByRef Names As String) As Integer

        'number of people linked to this tariff
        Dim _Return As Integer = 0

        Dim _SQL As String = ""
        _SQL += "select distinct(c.fullname) from ChildAdvanceSessions s"
        _SQL += " left join Children c on c.ID = s.child_id"
        _SQL += " where c.status <> 'Left'"
        _SQL += " and "
        _SQL += " (s.monday = '" + TariffID + "'"
        _SQL += " or s.tuesday = '" + TariffID + "'"
        _SQL += " or s.wednesday = '" + TariffID + "'"
        _SQL += " or s.thursday = '" + TariffID + "'"
        _SQL += " or s.friday = '" + TariffID + "'"
        _SQL += " or s.saturday = '" + TariffID + "'"
        _SQL += " or s.sunday = '" + TariffID + "')"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            If _DT.Rows.Count > 0 Then

                _Return = _DT.Rows.Count

                Names = ""
                For Each _DR As DataRow In _DT.Rows
                    If Names <> "" Then Names += vbCrLf
                    Names += _DR.Item("fullname").ToString
                Next

            End If

        End If

        Return _Return

    End Function

    Private Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click

        Dim _ID As Guid? = GetID()
        If _ID.HasValue Then

            Dim _Name As String = cgTariffs.CurrentRow("Name").ToString

            Dim _LinkedNames As String = ""
            Dim _ChildrenLinked As Integer = SafeToRemove(_ID.ToString, _LinkedNames)

            If _ChildrenLinked > 0 Then
                If _ChildrenLinked < 11 Then
                    CareMessage("Delete Cancelled. The following Children are linked to this Tariff:" + vbCrLf + vbCrLf + _LinkedNames, MessageBoxIcon.Exclamation, "Delete Tariff")
                Else
                    CareMessage("Delete Cancelled. There are " + _ChildrenLinked.ToString + " Children linked to this Tariff", MessageBoxIcon.Exclamation, "Delete Tariff")
                End If
            Else

                If CareMessage("Are you sure you want to remove the " + _Name + " tariff?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Remove Tariff") = DialogResult.Yes Then

                    Dim _SQL As String = ""

                    _SQL = "delete from Tariffs where ID = '" + _ID.Value.ToString + "'"
                    DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                    _SQL = "delete from TariffMatrix where tariff_id = '" + _ID.Value.ToString + "'"
                    DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                    _SQL = "delete from TariffDays where tariff_id = '" + _ID.Value.ToString + "'"
                    DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                    _SQL = "delete from TariffVariable where tariff_id = '" + _ID.Value.ToString + "'"
                    DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                    PopulateGrid()

                End If

            End If

        End If

    End Sub

    Private Sub btnUsage_Click(sender As Object, e As EventArgs) Handles btnUsage.Click
        Dim _ID As Guid? = GetID()
        If _ID.HasValue Then
            Dim _frm As New frmTariffUsage(_ID.Value, GetName)
            _frm.ShowDialog()
        End If
    End Sub

    Private Sub btnGlobalPriceChange_Click(sender As Object, e As EventArgs) Handles btnGlobalPriceChange.Click
        Dim _frm As New frmTariffGlobalPriceChange
        _frm.ShowDialog()
        If _frm.DialogResult = DialogResult.OK Then
            PopulateGrid()
        End If
        _frm.Dispose()
        _frm = Nothing
    End Sub
End Class
