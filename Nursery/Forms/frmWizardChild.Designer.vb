﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmWizardChild
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmWizardChild))
        Me.WizardControl1 = New DevExpress.XtraWizard.WizardControl()
        Me.wpWelcome = New DevExpress.XtraWizard.WelcomeWizardPage()
        Me.PictureBox1 = New PictureBox()
        Me.wpPrimary = New DevExpress.XtraWizard.WizardPage()
        Me.Panel1 = New Panel()
        Me.chkEInvoicing = New Care.Controls.CareCheckBox(Me.components)
        Me.txtPriEmail = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel15 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel16 = New Care.Controls.CareLabel(Me.components)
        Me.chkPriSMS = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.txtPriSecret = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.txtPriLandline = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.txtPriMobile = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.txtPriRelationship = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.txtPriSurname = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.txtPriForename = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.wpComplete = New DevExpress.XtraWizard.CompletionWizardPage()
        Me.lblTickBookings = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel38 = New Care.Controls.CareLabel(Me.components)
        Me.lblBookings = New Care.Controls.CareLabel(Me.components)
        Me.lblTickSessions = New Care.Controls.CareLabel(Me.components)
        Me.lblTickChild = New Care.Controls.CareLabel(Me.components)
        Me.lblTickContacts = New Care.Controls.CareLabel(Me.components)
        Me.lblTickFamily = New Care.Controls.CareLabel(Me.components)
        Me.wpAddress = New DevExpress.XtraWizard.WizardPage()
        Me.Panel2 = New Panel()
        Me.CareLabel10 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.txtAddress = New Care.Address.CareAddress()
        Me.txtAddressee = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.wpContact2 = New DevExpress.XtraWizard.WizardPage()
        Me.Panel3 = New Panel()
        Me.chk2Emergency = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel34 = New Care.Controls.CareLabel(Me.components)
        Me.chk2Collect = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel33 = New Care.Controls.CareLabel(Me.components)
        Me.chk2Parent = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel32 = New Care.Controls.CareLabel(Me.components)
        Me.txt2Email = New Care.Controls.CareTextBox(Me.components)
        Me.txt2Landline = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel21 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel18 = New Care.Controls.CareLabel(Me.components)
        Me.chkCont2 = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel19 = New Care.Controls.CareLabel(Me.components)
        Me.txt2Secret = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel20 = New Care.Controls.CareLabel(Me.components)
        Me.txt2Mobile = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel22 = New Care.Controls.CareLabel(Me.components)
        Me.txt2Relationship = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel23 = New Care.Controls.CareLabel(Me.components)
        Me.txt2Surname = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel24 = New Care.Controls.CareLabel(Me.components)
        Me.txt2Forename = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel25 = New Care.Controls.CareLabel(Me.components)
        Me.wpContact3 = New DevExpress.XtraWizard.WizardPage()
        Me.Panel4 = New Panel()
        Me.chk3Emergency = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel37 = New Care.Controls.CareLabel(Me.components)
        Me.chk3Collect = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel39 = New Care.Controls.CareLabel(Me.components)
        Me.chk3Parent = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel40 = New Care.Controls.CareLabel(Me.components)
        Me.txt3Email = New Care.Controls.CareTextBox(Me.components)
        Me.txt3Landline = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.chkCont3 = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel12 = New Care.Controls.CareLabel(Me.components)
        Me.txt3Secret = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel13 = New Care.Controls.CareLabel(Me.components)
        Me.txt3Mobile = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel17 = New Care.Controls.CareLabel(Me.components)
        Me.txt3Relationship = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel26 = New Care.Controls.CareLabel(Me.components)
        Me.txt3Surname = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel27 = New Care.Controls.CareLabel(Me.components)
        Me.txt3Forename = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel28 = New Care.Controls.CareLabel(Me.components)
        Me.wpChild = New DevExpress.XtraWizard.WizardPage()
        Me.Panel5 = New Panel()
        Me.cbxSite = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel31 = New Care.Controls.CareLabel(Me.components)
        Me.lblDOB = New Care.Controls.CareLabel(Me.components)
        Me.cbxChildKeyworker = New Care.Controls.CareComboBox(Me.components)
        Me.cbxChildGroup = New Care.Controls.CareComboBox(Me.components)
        Me.Label13 = New Care.Controls.CareLabel(Me.components)
        Me.Label14 = New Care.Controls.CareLabel(Me.components)
        Me.Label4 = New Care.Controls.CareLabel(Me.components)
        Me.cdtChildStart = New Care.Controls.CareDateTime(Me.components)
        Me.Label12 = New Care.Controls.CareLabel(Me.components)
        Me.cbxChildGender = New Care.Controls.CareComboBox(Me.components)
        Me.lblStartDate = New Care.Controls.CareLabel(Me.components)
        Me.Label16 = New Care.Controls.CareLabel(Me.components)
        Me.cdtChildDOB = New Care.Controls.CareDateTime(Me.components)
        Me.txtChildSurname = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel35 = New Care.Controls.CareLabel(Me.components)
        Me.txtChildForename = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel36 = New Care.Controls.CareLabel(Me.components)
        Me.wpSessionsMode = New DevExpress.XtraWizard.WizardPage()
        Me.CareLabel59 = New Care.Controls.CareLabel(Me.components)
        Me.Panel11 = New Panel()
        Me.CareLabel61 = New Care.Controls.CareLabel(Me.components)
        Me.cdtWC = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel60 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel30 = New Care.Controls.CareLabel(Me.components)
        Me.Panel6 = New Panel()
        Me.radSkip = New Care.Controls.CareRadioButton(Me.components)
        Me.radMultipleFullWeek = New Care.Controls.CareRadioButton(Me.components)
        Me.radSingleWeekDays = New Care.Controls.CareRadioButton(Me.components)
        Me.radMultiWeekDays = New Care.Controls.CareRadioButton(Me.components)
        Me.radSingleFullWeek = New Care.Controls.CareRadioButton(Me.components)
        Me.wpSessionSingle = New DevExpress.XtraWizard.WizardPage()
        Me.CareLabel42 = New Care.Controls.CareLabel(Me.components)
        Me.Panel8 = New Panel()
        Me.TableLayoutPanel1 = New TableLayoutPanel()
        Me.chkSingleSun = New Care.Controls.CareCheckBox(Me.components)
        Me.chkSingleSat = New Care.Controls.CareCheckBox(Me.components)
        Me.chkSingleFri = New Care.Controls.CareCheckBox(Me.components)
        Me.chkSingleThu = New Care.Controls.CareCheckBox(Me.components)
        Me.chkSingleWed = New Care.Controls.CareCheckBox(Me.components)
        Me.chkSingleTue = New Care.Controls.CareCheckBox(Me.components)
        Me.lblSFri = New Care.Controls.CareLabel(Me.components)
        Me.lblSWed = New Care.Controls.CareLabel(Me.components)
        Me.lblSTue = New Care.Controls.CareLabel(Me.components)
        Me.lblSMon = New Care.Controls.CareLabel(Me.components)
        Me.lblSThu = New Care.Controls.CareLabel(Me.components)
        Me.lblSSun = New Care.Controls.CareLabel(Me.components)
        Me.lblSSat = New Care.Controls.CareLabel(Me.components)
        Me.chkSingleMon = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel29 = New Care.Controls.CareLabel(Me.components)
        Me.Panel7 = New Panel()
        Me.tcSingle = New Nursery.TariffControl()
        Me.CareLabel41 = New Care.Controls.CareLabel(Me.components)
        Me.wpSessionMultiple = New DevExpress.XtraWizard.WizardPage()
        Me.CareLabel43 = New Care.Controls.CareLabel(Me.components)
        Me.Panel9 = New Panel()
        Me.TableLayoutPanel2 = New TableLayoutPanel()
        Me.chkMultiSun = New Care.Controls.CareCheckBox(Me.components)
        Me.chkMultiSat = New Care.Controls.CareCheckBox(Me.components)
        Me.chkMultiFri = New Care.Controls.CareCheckBox(Me.components)
        Me.chkMultiThu = New Care.Controls.CareCheckBox(Me.components)
        Me.chkMultiWed = New Care.Controls.CareCheckBox(Me.components)
        Me.chkMultiTue = New Care.Controls.CareCheckBox(Me.components)
        Me.lblMFri = New Care.Controls.CareLabel(Me.components)
        Me.lblMWed = New Care.Controls.CareLabel(Me.components)
        Me.lblMTue = New Care.Controls.CareLabel(Me.components)
        Me.lblMMon = New Care.Controls.CareLabel(Me.components)
        Me.lblMThu = New Care.Controls.CareLabel(Me.components)
        Me.lblMSun = New Care.Controls.CareLabel(Me.components)
        Me.lblMSat = New Care.Controls.CareLabel(Me.components)
        Me.chkMultiMon = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel51 = New Care.Controls.CareLabel(Me.components)
        Me.Panel10 = New Panel()
        Me.tc7 = New Nursery.TariffControl()
        Me.tc6 = New Nursery.TariffControl()
        Me.tc5 = New Nursery.TariffControl()
        Me.tc4 = New Nursery.TariffControl()
        Me.tc3 = New Nursery.TariffControl()
        Me.tc2 = New Nursery.TariffControl()
        Me.tc1 = New Nursery.TariffControl()
        Me.CareLabel58 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel57 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel56 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel55 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel54 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel53 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel52 = New Care.Controls.CareLabel(Me.components)
        Me.chkEmailInvoices2 = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel44 = New Care.Controls.CareLabel(Me.components)
        Me.chkEmailInvoices3 = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel45 = New Care.Controls.CareLabel(Me.components)
        CType(Me.WizardControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.WizardControl1.SuspendLayout()
        Me.wpWelcome.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wpPrimary.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.chkEInvoicing.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPriEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkPriSMS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPriSecret.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPriLandline.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPriMobile.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPriRelationship.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPriSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPriForename.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wpComplete.SuspendLayout()
        Me.wpAddress.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.txtAddressee.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wpContact2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.chk2Emergency.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chk2Collect.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chk2Parent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt2Email.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt2Landline.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkCont2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt2Secret.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt2Mobile.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt2Relationship.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt2Surname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt2Forename.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wpContact3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.chk3Emergency.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chk3Collect.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chk3Parent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt3Email.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt3Landline.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkCont3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt3Secret.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt3Mobile.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt3Relationship.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt3Surname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt3Forename.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wpChild.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxChildKeyworker.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxChildGroup.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtChildStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtChildStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxChildGender.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtChildDOB.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtChildDOB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtChildSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtChildForename.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wpSessionsMode.SuspendLayout()
        Me.Panel11.SuspendLayout()
        CType(Me.cdtWC.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtWC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel6.SuspendLayout()
        CType(Me.radSkip.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radMultipleFullWeek.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radSingleWeekDays.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radMultiWeekDays.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radSingleFullWeek.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wpSessionSingle.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.chkSingleSun.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSingleSat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSingleFri.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSingleThu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSingleWed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSingleTue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSingleMon.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel7.SuspendLayout()
        Me.wpSessionMultiple.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        CType(Me.chkMultiSun.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkMultiSat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkMultiFri.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkMultiThu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkMultiWed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkMultiTue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkMultiMon.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel10.SuspendLayout()
        CType(Me.chkEmailInvoices2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkEmailInvoices3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'WizardControl1
        '
        Me.WizardControl1.Controls.Add(Me.wpWelcome)
        Me.WizardControl1.Controls.Add(Me.wpPrimary)
        Me.WizardControl1.Controls.Add(Me.wpComplete)
        Me.WizardControl1.Controls.Add(Me.wpAddress)
        Me.WizardControl1.Controls.Add(Me.wpContact2)
        Me.WizardControl1.Controls.Add(Me.wpContact3)
        Me.WizardControl1.Controls.Add(Me.wpChild)
        Me.WizardControl1.Controls.Add(Me.wpSessionsMode)
        Me.WizardControl1.Controls.Add(Me.wpSessionSingle)
        Me.WizardControl1.Controls.Add(Me.wpSessionMultiple)
        Me.WizardControl1.Dock = DockStyle.Fill
        Me.WizardControl1.Location = New System.Drawing.Point(0, 0)
        Me.WizardControl1.Name = "WizardControl1"
        Me.WizardControl1.Pages.AddRange(New DevExpress.XtraWizard.BaseWizardPage() {Me.wpWelcome, Me.wpPrimary, Me.wpAddress, Me.wpContact2, Me.wpContact3, Me.wpChild, Me.wpSessionsMode, Me.wpSessionSingle, Me.wpSessionMultiple, Me.wpComplete})
        Me.WizardControl1.Size = New System.Drawing.Size(740, 523)
        Me.WizardControl1.Text = "Child, Family and Contact Wizard"
        Me.WizardControl1.WizardStyle = DevExpress.XtraWizard.WizardStyle.WizardAero
        '
        'wpWelcome
        '
        Me.wpWelcome.Controls.Add(Me.PictureBox1)
        Me.wpWelcome.Name = "wpWelcome"
        Me.wpWelcome.Size = New System.Drawing.Size(680, 355)
        Me.wpWelcome.Text = "Lets start entering our families..."
        '
        'PictureBox1
        '
        Me.PictureBox1.Dock = DockStyle.Fill
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(680, 355)
        Me.PictureBox1.SizeMode = PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'wpPrimary
        '
        Me.wpPrimary.Controls.Add(Me.Panel1)
        Me.wpPrimary.Name = "wpPrimary"
        Me.wpPrimary.Size = New System.Drawing.Size(680, 355)
        Me.wpPrimary.Text = "Enter the Primary Contact"
        '
        'Panel1
        '
        Me.Panel1.Anchor = AnchorStyles.None
        Me.Panel1.BorderStyle = BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.chkEInvoicing)
        Me.Panel1.Controls.Add(Me.txtPriEmail)
        Me.Panel1.Controls.Add(Me.CareLabel15)
        Me.Panel1.Controls.Add(Me.CareLabel16)
        Me.Panel1.Controls.Add(Me.chkPriSMS)
        Me.Panel1.Controls.Add(Me.CareLabel7)
        Me.Panel1.Controls.Add(Me.txtPriSecret)
        Me.Panel1.Controls.Add(Me.CareLabel6)
        Me.Panel1.Controls.Add(Me.txtPriLandline)
        Me.Panel1.Controls.Add(Me.CareLabel5)
        Me.Panel1.Controls.Add(Me.txtPriMobile)
        Me.Panel1.Controls.Add(Me.CareLabel4)
        Me.Panel1.Controls.Add(Me.txtPriRelationship)
        Me.Panel1.Controls.Add(Me.CareLabel3)
        Me.Panel1.Controls.Add(Me.txtPriSurname)
        Me.Panel1.Controls.Add(Me.CareLabel2)
        Me.Panel1.Controls.Add(Me.txtPriForename)
        Me.Panel1.Controls.Add(Me.CareLabel1)
        Me.Panel1.Location = New System.Drawing.Point(140, 68)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(401, 212)
        Me.Panel1.TabIndex = 0
        '
        'chkEInvoicing
        '
        Me.chkEInvoicing.EnterMoveNextControl = True
        Me.chkEInvoicing.Location = New System.Drawing.Point(372, 151)
        Me.chkEInvoicing.Name = "chkEInvoicing"
        Me.chkEInvoicing.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkEInvoicing.Properties.Appearance.Options.UseFont = True
        Me.chkEInvoicing.Properties.Caption = ""
        Me.chkEInvoicing.Size = New System.Drawing.Size(21, 19)
        Me.chkEInvoicing.TabIndex = 9
        Me.chkEInvoicing.TabStop = False
        '
        'txtPriEmail
        '
        Me.txtPriEmail.CharacterCasing = CharacterCasing.Normal
        Me.txtPriEmail.EnterMoveNextControl = True
        Me.txtPriEmail.Location = New System.Drawing.Point(100, 150)
        Me.txtPriEmail.MaxLength = 100
        Me.txtPriEmail.Name = "txtPriEmail"
        Me.txtPriEmail.NumericAllowNegatives = False
        Me.txtPriEmail.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtPriEmail.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPriEmail.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPriEmail.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtPriEmail.Properties.Appearance.Options.UseFont = True
        Me.txtPriEmail.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPriEmail.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPriEmail.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPriEmail.Properties.MaxLength = 100
        Me.txtPriEmail.Size = New System.Drawing.Size(200, 22)
        Me.txtPriEmail.TabIndex = 7
        Me.txtPriEmail.TextAlign = HorizontalAlignment.Left
        Me.txtPriEmail.ToolTipText = ""
        '
        'CareLabel15
        '
        Me.CareLabel15.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel15.Location = New System.Drawing.Point(10, 153)
        Me.CareLabel15.Name = "CareLabel15"
        Me.CareLabel15.Size = New System.Drawing.Size(74, 15)
        Me.CareLabel15.TabIndex = 14
        Me.CareLabel15.Text = "Email Address"
        Me.CareLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel16
        '
        Me.CareLabel16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel16.Location = New System.Drawing.Point(306, 153)
        Me.CareLabel16.Name = "CareLabel16"
        Me.CareLabel16.Size = New System.Drawing.Size(60, 15)
        Me.CareLabel16.TabIndex = 8
        Me.CareLabel16.Text = "e-invoicing"
        Me.CareLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkPriSMS
        '
        Me.chkPriSMS.EnterMoveNextControl = True
        Me.chkPriSMS.Location = New System.Drawing.Point(372, 95)
        Me.chkPriSMS.Name = "chkPriSMS"
        Me.chkPriSMS.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkPriSMS.Properties.Appearance.Options.UseFont = True
        Me.chkPriSMS.Properties.Caption = ""
        Me.chkPriSMS.Size = New System.Drawing.Size(21, 19)
        Me.chkPriSMS.TabIndex = 5
        Me.chkPriSMS.TabStop = False
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(306, 97)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(52, 15)
        Me.CareLabel7.TabIndex = 4
        Me.CareLabel7.Text = "Send SMS"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPriSecret
        '
        Me.txtPriSecret.CharacterCasing = CharacterCasing.Upper
        Me.txtPriSecret.EnterMoveNextControl = True
        Me.txtPriSecret.Location = New System.Drawing.Point(100, 178)
        Me.txtPriSecret.MaxLength = 30
        Me.txtPriSecret.Name = "txtPriSecret"
        Me.txtPriSecret.NumericAllowNegatives = False
        Me.txtPriSecret.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtPriSecret.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPriSecret.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPriSecret.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtPriSecret.Properties.Appearance.Options.UseFont = True
        Me.txtPriSecret.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPriSecret.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPriSecret.Properties.CharacterCasing = CharacterCasing.Upper
        Me.txtPriSecret.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPriSecret.Properties.MaxLength = 30
        Me.txtPriSecret.Properties.NullValuePrompt = "e.g. DOODLES"
        Me.txtPriSecret.Size = New System.Drawing.Size(200, 22)
        Me.txtPriSecret.TabIndex = 10
        Me.txtPriSecret.TextAlign = HorizontalAlignment.Left
        Me.txtPriSecret.ToolTipText = ""
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(10, 181)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(64, 15)
        Me.CareLabel6.TabIndex = 10
        Me.CareLabel6.Text = "Secret Word"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPriLandline
        '
        Me.txtPriLandline.CharacterCasing = CharacterCasing.Normal
        Me.txtPriLandline.EnterMoveNextControl = True
        Me.txtPriLandline.Location = New System.Drawing.Point(100, 122)
        Me.txtPriLandline.MaxLength = 15
        Me.txtPriLandline.Name = "txtPriLandline"
        Me.txtPriLandline.NumericAllowNegatives = False
        Me.txtPriLandline.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtPriLandline.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPriLandline.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPriLandline.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtPriLandline.Properties.Appearance.Options.UseFont = True
        Me.txtPriLandline.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPriLandline.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPriLandline.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPriLandline.Properties.MaxLength = 15
        Me.txtPriLandline.Size = New System.Drawing.Size(200, 22)
        Me.txtPriLandline.TabIndex = 6
        Me.txtPriLandline.TextAlign = HorizontalAlignment.Left
        Me.txtPriLandline.ToolTipText = ""
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(10, 125)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(45, 15)
        Me.CareLabel5.TabIndex = 8
        Me.CareLabel5.Text = "Landline"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPriMobile
        '
        Me.txtPriMobile.CharacterCasing = CharacterCasing.Normal
        Me.txtPriMobile.EnterMoveNextControl = True
        Me.txtPriMobile.Location = New System.Drawing.Point(100, 94)
        Me.txtPriMobile.MaxLength = 15
        Me.txtPriMobile.Name = "txtPriMobile"
        Me.txtPriMobile.NumericAllowNegatives = False
        Me.txtPriMobile.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtPriMobile.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPriMobile.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPriMobile.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtPriMobile.Properties.Appearance.Options.UseFont = True
        Me.txtPriMobile.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPriMobile.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPriMobile.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPriMobile.Properties.MaxLength = 15
        Me.txtPriMobile.Size = New System.Drawing.Size(200, 22)
        Me.txtPriMobile.TabIndex = 3
        Me.txtPriMobile.TextAlign = HorizontalAlignment.Left
        Me.txtPriMobile.ToolTipText = ""
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(10, 97)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(84, 15)
        Me.CareLabel4.TabIndex = 6
        Me.CareLabel4.Text = "Mobile Number"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPriRelationship
        '
        Me.txtPriRelationship.CharacterCasing = CharacterCasing.Normal
        Me.txtPriRelationship.EnterMoveNextControl = True
        Me.txtPriRelationship.Location = New System.Drawing.Point(100, 66)
        Me.txtPriRelationship.MaxLength = 30
        Me.txtPriRelationship.Name = "txtPriRelationship"
        Me.txtPriRelationship.NumericAllowNegatives = False
        Me.txtPriRelationship.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtPriRelationship.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPriRelationship.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPriRelationship.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtPriRelationship.Properties.Appearance.Options.UseFont = True
        Me.txtPriRelationship.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPriRelationship.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPriRelationship.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPriRelationship.Properties.MaxLength = 30
        Me.txtPriRelationship.Properties.NullValuePrompt = "e.g. Mother"
        Me.txtPriRelationship.Size = New System.Drawing.Size(200, 22)
        Me.txtPriRelationship.TabIndex = 2
        Me.txtPriRelationship.TextAlign = HorizontalAlignment.Left
        Me.txtPriRelationship.ToolTipText = ""
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(10, 69)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(65, 15)
        Me.CareLabel3.TabIndex = 4
        Me.CareLabel3.Text = "Relationship"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPriSurname
        '
        Me.txtPriSurname.CharacterCasing = CharacterCasing.Normal
        Me.txtPriSurname.EnterMoveNextControl = True
        Me.txtPriSurname.Location = New System.Drawing.Point(100, 38)
        Me.txtPriSurname.MaxLength = 30
        Me.txtPriSurname.Name = "txtPriSurname"
        Me.txtPriSurname.NumericAllowNegatives = False
        Me.txtPriSurname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtPriSurname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPriSurname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPriSurname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtPriSurname.Properties.Appearance.Options.UseFont = True
        Me.txtPriSurname.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPriSurname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPriSurname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPriSurname.Properties.MaxLength = 30
        Me.txtPriSurname.Size = New System.Drawing.Size(200, 22)
        Me.txtPriSurname.TabIndex = 1
        Me.txtPriSurname.TextAlign = HorizontalAlignment.Left
        Me.txtPriSurname.ToolTipText = ""
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(10, 41)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(47, 15)
        Me.CareLabel2.TabIndex = 2
        Me.CareLabel2.Text = "Surname"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPriForename
        '
        Me.txtPriForename.CharacterCasing = CharacterCasing.Normal
        Me.txtPriForename.EnterMoveNextControl = True
        Me.txtPriForename.Location = New System.Drawing.Point(100, 10)
        Me.txtPriForename.MaxLength = 30
        Me.txtPriForename.Name = "txtPriForename"
        Me.txtPriForename.NumericAllowNegatives = False
        Me.txtPriForename.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtPriForename.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPriForename.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPriForename.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtPriForename.Properties.Appearance.Options.UseFont = True
        Me.txtPriForename.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPriForename.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPriForename.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPriForename.Properties.MaxLength = 30
        Me.txtPriForename.Size = New System.Drawing.Size(200, 22)
        Me.txtPriForename.TabIndex = 0
        Me.txtPriForename.TextAlign = HorizontalAlignment.Left
        Me.txtPriForename.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(10, 13)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(53, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Forename"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'wpComplete
        '
        Me.wpComplete.Controls.Add(Me.lblTickBookings)
        Me.wpComplete.Controls.Add(Me.CareLabel38)
        Me.wpComplete.Controls.Add(Me.lblBookings)
        Me.wpComplete.Controls.Add(Me.lblTickSessions)
        Me.wpComplete.Controls.Add(Me.lblTickChild)
        Me.wpComplete.Controls.Add(Me.lblTickContacts)
        Me.wpComplete.Controls.Add(Me.lblTickFamily)
        Me.wpComplete.Name = "wpComplete"
        Me.wpComplete.Size = New System.Drawing.Size(680, 355)
        '
        'lblTickBookings
        '
        Me.lblTickBookings.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblTickBookings.Appearance.Image = CType(resources.GetObject("lblTickBookings.Appearance.Image"), System.Drawing.Image)
        Me.lblTickBookings.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblTickBookings.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblTickBookings.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter
        Me.lblTickBookings.Location = New System.Drawing.Point(36, 192)
        Me.lblTickBookings.Name = "lblTickBookings"
        Me.lblTickBookings.Size = New System.Drawing.Size(164, 20)
        Me.lblTickBookings.TabIndex = 11
        Me.lblTickBookings.Text = "Bookings Built Successfully"
        Me.lblTickBookings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel38
        '
        Me.CareLabel38.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel38.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel38.Location = New System.Drawing.Point(36, 26)
        Me.CareLabel38.Name = "CareLabel38"
        Me.CareLabel38.Size = New System.Drawing.Size(187, 15)
        Me.CareLabel38.TabIndex = 10
        Me.CareLabel38.Text = "Click Finish to create your records..."
        Me.CareLabel38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBookings
        '
        Me.lblBookings.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblBookings.Appearance.Image = CType(resources.GetObject("lblBookings.Appearance.Image"), System.Drawing.Image)
        Me.lblBookings.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblBookings.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblBookings.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter
        Me.lblBookings.Location = New System.Drawing.Point(36, 166)
        Me.lblBookings.Name = "lblBookings"
        Me.lblBookings.Size = New System.Drawing.Size(126, 20)
        Me.lblBookings.TabIndex = 9
        Me.lblBookings.Text = "Building Bookings..."
        Me.lblBookings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTickSessions
        '
        Me.lblTickSessions.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblTickSessions.Appearance.Image = CType(resources.GetObject("lblTickSessions.Appearance.Image"), System.Drawing.Image)
        Me.lblTickSessions.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblTickSessions.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblTickSessions.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter
        Me.lblTickSessions.Location = New System.Drawing.Point(36, 140)
        Me.lblTickSessions.Name = "lblTickSessions"
        Me.lblTickSessions.Size = New System.Drawing.Size(102, 20)
        Me.lblTickSessions.TabIndex = 8
        Me.lblTickSessions.Text = "Create Sessions"
        Me.lblTickSessions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTickChild
        '
        Me.lblTickChild.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblTickChild.Appearance.Image = CType(resources.GetObject("lblTickChild.Appearance.Image"), System.Drawing.Image)
        Me.lblTickChild.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblTickChild.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblTickChild.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter
        Me.lblTickChild.Location = New System.Drawing.Point(36, 114)
        Me.lblTickChild.Name = "lblTickChild"
        Me.lblTickChild.Size = New System.Drawing.Size(126, 20)
        Me.lblTickChild.TabIndex = 7
        Me.lblTickChild.Text = "Create Child Record"
        Me.lblTickChild.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTickContacts
        '
        Me.lblTickContacts.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblTickContacts.Appearance.Image = CType(resources.GetObject("lblTickContacts.Appearance.Image"), System.Drawing.Image)
        Me.lblTickContacts.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblTickContacts.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblTickContacts.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter
        Me.lblTickContacts.Location = New System.Drawing.Point(36, 88)
        Me.lblTickContacts.Name = "lblTickContacts"
        Me.lblTickContacts.Size = New System.Drawing.Size(105, 20)
        Me.lblTickContacts.TabIndex = 6
        Me.lblTickContacts.Text = "Create Contacts"
        Me.lblTickContacts.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTickFamily
        '
        Me.lblTickFamily.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblTickFamily.Appearance.Image = CType(resources.GetObject("lblTickFamily.Appearance.Image"), System.Drawing.Image)
        Me.lblTickFamily.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblTickFamily.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblTickFamily.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter
        Me.lblTickFamily.Location = New System.Drawing.Point(36, 62)
        Me.lblTickFamily.Name = "lblTickFamily"
        Me.lblTickFamily.Size = New System.Drawing.Size(133, 20)
        Me.lblTickFamily.TabIndex = 5
        Me.lblTickFamily.Text = "Create Family Record"
        Me.lblTickFamily.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'wpAddress
        '
        Me.wpAddress.Controls.Add(Me.Panel2)
        Me.wpAddress.Name = "wpAddress"
        Me.wpAddress.Size = New System.Drawing.Size(680, 355)
        Me.wpAddress.Text = "Enter Address Details"
        '
        'Panel2
        '
        Me.Panel2.Anchor = AnchorStyles.None
        Me.Panel2.BorderStyle = BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.CareLabel10)
        Me.Panel2.Controls.Add(Me.CareLabel8)
        Me.Panel2.Controls.Add(Me.txtAddress)
        Me.Panel2.Controls.Add(Me.txtAddressee)
        Me.Panel2.Controls.Add(Me.CareLabel9)
        Me.Panel2.Location = New System.Drawing.Point(140, 64)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(401, 218)
        Me.Panel2.TabIndex = 0
        '
        'CareLabel10
        '
        Me.CareLabel10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel10.Location = New System.Drawing.Point(100, 10)
        Me.CareLabel10.Name = "CareLabel10"
        Me.CareLabel10.Size = New System.Drawing.Size(222, 15)
        Me.CareLabel10.TabIndex = 0
        Me.CareLabel10.Text = "(Hit F3 to lookup an Address by Postcode)"
        Me.CareLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(10, 168)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(54, 15)
        Me.CareLabel8.TabIndex = 3
        Me.CareLabel8.Text = "Addressee"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAddress
        '
        Me.txtAddress.Address = ""
        Me.txtAddress.AddressBlock = ""
        Me.txtAddress.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.Appearance.Options.UseFont = True
        Me.txtAddress.County = ""
        Me.txtAddress.DisplayMode = Care.Address.CareAddress.EnumDisplayMode.SingleControl
        Me.txtAddress.Location = New System.Drawing.Point(100, 31)
        Me.txtAddress.MaxLength = 500
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.PostCode = ""
        Me.txtAddress.ReadOnly = False
        Me.txtAddress.Size = New System.Drawing.Size(280, 117)
        Me.txtAddress.TabIndex = 2
        Me.txtAddress.Town = ""
        '
        'txtAddressee
        '
        Me.txtAddressee.CharacterCasing = CharacterCasing.Normal
        Me.txtAddressee.EnterMoveNextControl = True
        Me.txtAddressee.Location = New System.Drawing.Point(100, 165)
        Me.txtAddressee.MaxLength = 30
        Me.txtAddressee.Name = "txtAddressee"
        Me.txtAddressee.NumericAllowNegatives = False
        Me.txtAddressee.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtAddressee.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtAddressee.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtAddressee.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtAddressee.Properties.Appearance.Options.UseFont = True
        Me.txtAddressee.Properties.Appearance.Options.UseTextOptions = True
        Me.txtAddressee.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtAddressee.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtAddressee.Properties.MaxLength = 30
        Me.txtAddressee.Properties.NullValuePrompt = "e.g. Mrs & Mrs Bloggs"
        Me.txtAddressee.Size = New System.Drawing.Size(280, 22)
        Me.txtAddressee.TabIndex = 4
        Me.txtAddressee.TextAlign = HorizontalAlignment.Left
        Me.txtAddressee.ToolTipText = ""
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(10, 32)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(42, 15)
        Me.CareLabel9.TabIndex = 1
        Me.CareLabel9.Text = "Address"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'wpContact2
        '
        Me.wpContact2.Controls.Add(Me.Panel3)
        Me.wpContact2.Name = "wpContact2"
        Me.wpContact2.Size = New System.Drawing.Size(680, 355)
        Me.wpContact2.Text = "Enter Second Contact"
        '
        'Panel3
        '
        Me.Panel3.Anchor = AnchorStyles.None
        Me.Panel3.BorderStyle = BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.chkEmailInvoices2)
        Me.Panel3.Controls.Add(Me.CareLabel44)
        Me.Panel3.Controls.Add(Me.chk2Emergency)
        Me.Panel3.Controls.Add(Me.CareLabel34)
        Me.Panel3.Controls.Add(Me.chk2Collect)
        Me.Panel3.Controls.Add(Me.CareLabel33)
        Me.Panel3.Controls.Add(Me.chk2Parent)
        Me.Panel3.Controls.Add(Me.CareLabel32)
        Me.Panel3.Controls.Add(Me.txt2Email)
        Me.Panel3.Controls.Add(Me.txt2Landline)
        Me.Panel3.Controls.Add(Me.CareLabel21)
        Me.Panel3.Controls.Add(Me.CareLabel18)
        Me.Panel3.Controls.Add(Me.chkCont2)
        Me.Panel3.Controls.Add(Me.CareLabel19)
        Me.Panel3.Controls.Add(Me.txt2Secret)
        Me.Panel3.Controls.Add(Me.CareLabel20)
        Me.Panel3.Controls.Add(Me.txt2Mobile)
        Me.Panel3.Controls.Add(Me.CareLabel22)
        Me.Panel3.Controls.Add(Me.txt2Relationship)
        Me.Panel3.Controls.Add(Me.CareLabel23)
        Me.Panel3.Controls.Add(Me.txt2Surname)
        Me.Panel3.Controls.Add(Me.CareLabel24)
        Me.Panel3.Controls.Add(Me.txt2Forename)
        Me.Panel3.Controls.Add(Me.CareLabel25)
        Me.Panel3.Location = New System.Drawing.Point(140, 35)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(401, 286)
        Me.Panel3.TabIndex = 0
        '
        'chk2Emergency
        '
        Me.chk2Emergency.EnterMoveNextControl = True
        Me.chk2Emergency.Location = New System.Drawing.Point(130, 259)
        Me.chk2Emergency.Name = "chk2Emergency"
        Me.chk2Emergency.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chk2Emergency.Properties.Appearance.Options.UseFont = True
        Me.chk2Emergency.Properties.Caption = ""
        Me.chk2Emergency.Size = New System.Drawing.Size(19, 19)
        Me.chk2Emergency.TabIndex = 21
        '
        'CareLabel34
        '
        Me.CareLabel34.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel34.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel34.Location = New System.Drawing.Point(10, 261)
        Me.CareLabel34.Name = "CareLabel34"
        Me.CareLabel34.Size = New System.Drawing.Size(115, 15)
        Me.CareLabel34.TabIndex = 20
        Me.CareLabel34.Text = "Is Emergency Contact"
        Me.CareLabel34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chk2Collect
        '
        Me.chk2Collect.EnterMoveNextControl = True
        Me.chk2Collect.Location = New System.Drawing.Point(130, 234)
        Me.chk2Collect.Name = "chk2Collect"
        Me.chk2Collect.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chk2Collect.Properties.Appearance.Options.UseFont = True
        Me.chk2Collect.Properties.Caption = ""
        Me.chk2Collect.Size = New System.Drawing.Size(19, 19)
        Me.chk2Collect.TabIndex = 17
        '
        'CareLabel33
        '
        Me.CareLabel33.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel33.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel33.Location = New System.Drawing.Point(10, 236)
        Me.CareLabel33.Name = "CareLabel33"
        Me.CareLabel33.Size = New System.Drawing.Size(92, 15)
        Me.CareLabel33.TabIndex = 16
        Me.CareLabel33.Text = "Can Collect Child"
        Me.CareLabel33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chk2Parent
        '
        Me.chk2Parent.EnterMoveNextControl = True
        Me.chk2Parent.Location = New System.Drawing.Point(364, 234)
        Me.chk2Parent.Name = "chk2Parent"
        Me.chk2Parent.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chk2Parent.Properties.Appearance.Options.UseFont = True
        Me.chk2Parent.Properties.Caption = ""
        Me.chk2Parent.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.chk2Parent.Size = New System.Drawing.Size(19, 19)
        Me.chk2Parent.TabIndex = 19
        '
        'CareLabel32
        '
        Me.CareLabel32.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel32.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel32.Location = New System.Drawing.Point(228, 236)
        Me.CareLabel32.Name = "CareLabel32"
        Me.CareLabel32.Size = New System.Drawing.Size(132, 15)
        Me.CareLabel32.TabIndex = 18
        Me.CareLabel32.Text = "Has Parental Resonsibilty"
        Me.CareLabel32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt2Email
        '
        Me.txt2Email.CharacterCasing = CharacterCasing.Normal
        Me.txt2Email.EnterMoveNextControl = True
        Me.txt2Email.Location = New System.Drawing.Point(132, 176)
        Me.txt2Email.MaxLength = 100
        Me.txt2Email.Name = "txt2Email"
        Me.txt2Email.NumericAllowNegatives = False
        Me.txt2Email.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txt2Email.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt2Email.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txt2Email.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txt2Email.Properties.Appearance.Options.UseFont = True
        Me.txt2Email.Properties.Appearance.Options.UseTextOptions = True
        Me.txt2Email.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txt2Email.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt2Email.Properties.MaxLength = 100
        Me.txt2Email.Size = New System.Drawing.Size(248, 22)
        Me.txt2Email.TabIndex = 13
        Me.txt2Email.TextAlign = HorizontalAlignment.Left
        Me.txt2Email.ToolTipText = ""
        '
        'txt2Landline
        '
        Me.txt2Landline.CharacterCasing = CharacterCasing.Normal
        Me.txt2Landline.EnterMoveNextControl = True
        Me.txt2Landline.Location = New System.Drawing.Point(132, 148)
        Me.txt2Landline.MaxLength = 15
        Me.txt2Landline.Name = "txt2Landline"
        Me.txt2Landline.NumericAllowNegatives = False
        Me.txt2Landline.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txt2Landline.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt2Landline.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txt2Landline.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txt2Landline.Properties.Appearance.Options.UseFont = True
        Me.txt2Landline.Properties.Appearance.Options.UseTextOptions = True
        Me.txt2Landline.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txt2Landline.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt2Landline.Properties.MaxLength = 15
        Me.txt2Landline.Size = New System.Drawing.Size(248, 22)
        Me.txt2Landline.TabIndex = 11
        Me.txt2Landline.TextAlign = HorizontalAlignment.Left
        Me.txt2Landline.ToolTipText = ""
        '
        'CareLabel21
        '
        Me.CareLabel21.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel21.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel21.Location = New System.Drawing.Point(10, 151)
        Me.CareLabel21.Name = "CareLabel21"
        Me.CareLabel21.Size = New System.Drawing.Size(45, 15)
        Me.CareLabel21.TabIndex = 10
        Me.CareLabel21.Text = "Landline"
        Me.CareLabel21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel18
        '
        Me.CareLabel18.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel18.Location = New System.Drawing.Point(10, 179)
        Me.CareLabel18.Name = "CareLabel18"
        Me.CareLabel18.Size = New System.Drawing.Size(74, 15)
        Me.CareLabel18.TabIndex = 12
        Me.CareLabel18.Text = "Email Address"
        Me.CareLabel18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkCont2
        '
        Me.chkCont2.EnterMoveNextControl = True
        Me.chkCont2.Location = New System.Drawing.Point(130, 11)
        Me.chkCont2.Name = "chkCont2"
        Me.chkCont2.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkCont2.Properties.Appearance.Options.UseFont = True
        Me.chkCont2.Properties.Caption = ""
        Me.chkCont2.Size = New System.Drawing.Size(19, 19)
        Me.chkCont2.TabIndex = 1
        '
        'CareLabel19
        '
        Me.CareLabel19.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel19.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel19.Location = New System.Drawing.Point(10, 13)
        Me.CareLabel19.Name = "CareLabel19"
        Me.CareLabel19.Size = New System.Drawing.Size(114, 15)
        Me.CareLabel19.TabIndex = 0
        Me.CareLabel19.Text = "Enter Second Contact"
        Me.CareLabel19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt2Secret
        '
        Me.txt2Secret.CharacterCasing = CharacterCasing.Upper
        Me.txt2Secret.EnterMoveNextControl = True
        Me.txt2Secret.Location = New System.Drawing.Point(132, 204)
        Me.txt2Secret.MaxLength = 30
        Me.txt2Secret.Name = "txt2Secret"
        Me.txt2Secret.NumericAllowNegatives = False
        Me.txt2Secret.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txt2Secret.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt2Secret.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txt2Secret.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txt2Secret.Properties.Appearance.Options.UseFont = True
        Me.txt2Secret.Properties.Appearance.Options.UseTextOptions = True
        Me.txt2Secret.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txt2Secret.Properties.CharacterCasing = CharacterCasing.Upper
        Me.txt2Secret.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt2Secret.Properties.MaxLength = 30
        Me.txt2Secret.Size = New System.Drawing.Size(248, 22)
        Me.txt2Secret.TabIndex = 15
        Me.txt2Secret.TextAlign = HorizontalAlignment.Left
        Me.txt2Secret.ToolTipText = ""
        '
        'CareLabel20
        '
        Me.CareLabel20.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel20.Location = New System.Drawing.Point(10, 207)
        Me.CareLabel20.Name = "CareLabel20"
        Me.CareLabel20.Size = New System.Drawing.Size(64, 15)
        Me.CareLabel20.TabIndex = 14
        Me.CareLabel20.Text = "Secret Word"
        Me.CareLabel20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt2Mobile
        '
        Me.txt2Mobile.CharacterCasing = CharacterCasing.Normal
        Me.txt2Mobile.EnterMoveNextControl = True
        Me.txt2Mobile.Location = New System.Drawing.Point(132, 120)
        Me.txt2Mobile.MaxLength = 15
        Me.txt2Mobile.Name = "txt2Mobile"
        Me.txt2Mobile.NumericAllowNegatives = False
        Me.txt2Mobile.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txt2Mobile.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt2Mobile.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txt2Mobile.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txt2Mobile.Properties.Appearance.Options.UseFont = True
        Me.txt2Mobile.Properties.Appearance.Options.UseTextOptions = True
        Me.txt2Mobile.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txt2Mobile.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt2Mobile.Properties.MaxLength = 15
        Me.txt2Mobile.Size = New System.Drawing.Size(248, 22)
        Me.txt2Mobile.TabIndex = 9
        Me.txt2Mobile.TextAlign = HorizontalAlignment.Left
        Me.txt2Mobile.ToolTipText = ""
        '
        'CareLabel22
        '
        Me.CareLabel22.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel22.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel22.Location = New System.Drawing.Point(10, 123)
        Me.CareLabel22.Name = "CareLabel22"
        Me.CareLabel22.Size = New System.Drawing.Size(84, 15)
        Me.CareLabel22.TabIndex = 8
        Me.CareLabel22.Text = "Mobile Number"
        Me.CareLabel22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt2Relationship
        '
        Me.txt2Relationship.CharacterCasing = CharacterCasing.Normal
        Me.txt2Relationship.EnterMoveNextControl = True
        Me.txt2Relationship.Location = New System.Drawing.Point(132, 92)
        Me.txt2Relationship.MaxLength = 30
        Me.txt2Relationship.Name = "txt2Relationship"
        Me.txt2Relationship.NumericAllowNegatives = False
        Me.txt2Relationship.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txt2Relationship.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt2Relationship.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txt2Relationship.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txt2Relationship.Properties.Appearance.Options.UseFont = True
        Me.txt2Relationship.Properties.Appearance.Options.UseTextOptions = True
        Me.txt2Relationship.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txt2Relationship.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt2Relationship.Properties.MaxLength = 30
        Me.txt2Relationship.Size = New System.Drawing.Size(248, 22)
        Me.txt2Relationship.TabIndex = 7
        Me.txt2Relationship.TextAlign = HorizontalAlignment.Left
        Me.txt2Relationship.ToolTipText = ""
        '
        'CareLabel23
        '
        Me.CareLabel23.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel23.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel23.Location = New System.Drawing.Point(10, 95)
        Me.CareLabel23.Name = "CareLabel23"
        Me.CareLabel23.Size = New System.Drawing.Size(65, 15)
        Me.CareLabel23.TabIndex = 6
        Me.CareLabel23.Text = "Relationship"
        Me.CareLabel23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt2Surname
        '
        Me.txt2Surname.CharacterCasing = CharacterCasing.Normal
        Me.txt2Surname.EnterMoveNextControl = True
        Me.txt2Surname.Location = New System.Drawing.Point(132, 64)
        Me.txt2Surname.MaxLength = 30
        Me.txt2Surname.Name = "txt2Surname"
        Me.txt2Surname.NumericAllowNegatives = False
        Me.txt2Surname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txt2Surname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt2Surname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txt2Surname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txt2Surname.Properties.Appearance.Options.UseFont = True
        Me.txt2Surname.Properties.Appearance.Options.UseTextOptions = True
        Me.txt2Surname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txt2Surname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt2Surname.Properties.MaxLength = 30
        Me.txt2Surname.Size = New System.Drawing.Size(248, 22)
        Me.txt2Surname.TabIndex = 5
        Me.txt2Surname.TextAlign = HorizontalAlignment.Left
        Me.txt2Surname.ToolTipText = ""
        '
        'CareLabel24
        '
        Me.CareLabel24.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel24.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel24.Location = New System.Drawing.Point(10, 67)
        Me.CareLabel24.Name = "CareLabel24"
        Me.CareLabel24.Size = New System.Drawing.Size(47, 15)
        Me.CareLabel24.TabIndex = 4
        Me.CareLabel24.Text = "Surname"
        Me.CareLabel24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt2Forename
        '
        Me.txt2Forename.CharacterCasing = CharacterCasing.Normal
        Me.txt2Forename.EnterMoveNextControl = True
        Me.txt2Forename.Location = New System.Drawing.Point(132, 36)
        Me.txt2Forename.MaxLength = 30
        Me.txt2Forename.Name = "txt2Forename"
        Me.txt2Forename.NumericAllowNegatives = False
        Me.txt2Forename.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txt2Forename.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt2Forename.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txt2Forename.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txt2Forename.Properties.Appearance.Options.UseFont = True
        Me.txt2Forename.Properties.Appearance.Options.UseTextOptions = True
        Me.txt2Forename.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txt2Forename.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt2Forename.Properties.MaxLength = 30
        Me.txt2Forename.Size = New System.Drawing.Size(248, 22)
        Me.txt2Forename.TabIndex = 3
        Me.txt2Forename.TextAlign = HorizontalAlignment.Left
        Me.txt2Forename.ToolTipText = ""
        '
        'CareLabel25
        '
        Me.CareLabel25.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel25.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel25.Location = New System.Drawing.Point(10, 39)
        Me.CareLabel25.Name = "CareLabel25"
        Me.CareLabel25.Size = New System.Drawing.Size(53, 15)
        Me.CareLabel25.TabIndex = 2
        Me.CareLabel25.Text = "Forename"
        Me.CareLabel25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'wpContact3
        '
        Me.wpContact3.Controls.Add(Me.Panel4)
        Me.wpContact3.Name = "wpContact3"
        Me.wpContact3.Size = New System.Drawing.Size(680, 355)
        Me.wpContact3.Text = "Enter Third Contact"
        '
        'Panel4
        '
        Me.Panel4.Anchor = AnchorStyles.None
        Me.Panel4.BorderStyle = BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.chkEmailInvoices3)
        Me.Panel4.Controls.Add(Me.CareLabel45)
        Me.Panel4.Controls.Add(Me.chk3Emergency)
        Me.Panel4.Controls.Add(Me.CareLabel37)
        Me.Panel4.Controls.Add(Me.chk3Collect)
        Me.Panel4.Controls.Add(Me.CareLabel39)
        Me.Panel4.Controls.Add(Me.chk3Parent)
        Me.Panel4.Controls.Add(Me.CareLabel40)
        Me.Panel4.Controls.Add(Me.txt3Email)
        Me.Panel4.Controls.Add(Me.txt3Landline)
        Me.Panel4.Controls.Add(Me.CareLabel14)
        Me.Panel4.Controls.Add(Me.CareLabel11)
        Me.Panel4.Controls.Add(Me.chkCont3)
        Me.Panel4.Controls.Add(Me.CareLabel12)
        Me.Panel4.Controls.Add(Me.txt3Secret)
        Me.Panel4.Controls.Add(Me.CareLabel13)
        Me.Panel4.Controls.Add(Me.txt3Mobile)
        Me.Panel4.Controls.Add(Me.CareLabel17)
        Me.Panel4.Controls.Add(Me.txt3Relationship)
        Me.Panel4.Controls.Add(Me.CareLabel26)
        Me.Panel4.Controls.Add(Me.txt3Surname)
        Me.Panel4.Controls.Add(Me.CareLabel27)
        Me.Panel4.Controls.Add(Me.txt3Forename)
        Me.Panel4.Controls.Add(Me.CareLabel28)
        Me.Panel4.Location = New System.Drawing.Point(140, 35)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(401, 286)
        Me.Panel4.TabIndex = 0
        '
        'chk3Emergency
        '
        Me.chk3Emergency.EnterMoveNextControl = True
        Me.chk3Emergency.Location = New System.Drawing.Point(132, 257)
        Me.chk3Emergency.Name = "chk3Emergency"
        Me.chk3Emergency.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chk3Emergency.Properties.Appearance.Options.UseFont = True
        Me.chk3Emergency.Properties.Caption = ""
        Me.chk3Emergency.Size = New System.Drawing.Size(19, 19)
        Me.chk3Emergency.TabIndex = 21
        '
        'CareLabel37
        '
        Me.CareLabel37.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel37.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel37.Location = New System.Drawing.Point(9, 259)
        Me.CareLabel37.Name = "CareLabel37"
        Me.CareLabel37.Size = New System.Drawing.Size(115, 15)
        Me.CareLabel37.TabIndex = 20
        Me.CareLabel37.Text = "Is Emergency Contact"
        Me.CareLabel37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chk3Collect
        '
        Me.chk3Collect.EnterMoveNextControl = True
        Me.chk3Collect.Location = New System.Drawing.Point(132, 232)
        Me.chk3Collect.Name = "chk3Collect"
        Me.chk3Collect.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chk3Collect.Properties.Appearance.Options.UseFont = True
        Me.chk3Collect.Properties.Caption = ""
        Me.chk3Collect.Size = New System.Drawing.Size(19, 19)
        Me.chk3Collect.TabIndex = 17
        '
        'CareLabel39
        '
        Me.CareLabel39.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel39.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel39.Location = New System.Drawing.Point(9, 234)
        Me.CareLabel39.Name = "CareLabel39"
        Me.CareLabel39.Size = New System.Drawing.Size(92, 15)
        Me.CareLabel39.TabIndex = 16
        Me.CareLabel39.Text = "Can Collect Child"
        Me.CareLabel39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chk3Parent
        '
        Me.chk3Parent.EnterMoveNextControl = True
        Me.chk3Parent.Location = New System.Drawing.Point(364, 232)
        Me.chk3Parent.Name = "chk3Parent"
        Me.chk3Parent.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chk3Parent.Properties.Appearance.Options.UseFont = True
        Me.chk3Parent.Properties.Caption = ""
        Me.chk3Parent.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.chk3Parent.Size = New System.Drawing.Size(19, 19)
        Me.chk3Parent.TabIndex = 19
        '
        'CareLabel40
        '
        Me.CareLabel40.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel40.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel40.Location = New System.Drawing.Point(228, 234)
        Me.CareLabel40.Name = "CareLabel40"
        Me.CareLabel40.Size = New System.Drawing.Size(132, 15)
        Me.CareLabel40.TabIndex = 18
        Me.CareLabel40.Text = "Has Parental Resonsibilty"
        Me.CareLabel40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt3Email
        '
        Me.txt3Email.CharacterCasing = CharacterCasing.Normal
        Me.txt3Email.EnterMoveNextControl = True
        Me.txt3Email.Location = New System.Drawing.Point(132, 176)
        Me.txt3Email.MaxLength = 100
        Me.txt3Email.Name = "txt3Email"
        Me.txt3Email.NumericAllowNegatives = False
        Me.txt3Email.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txt3Email.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt3Email.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txt3Email.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txt3Email.Properties.Appearance.Options.UseFont = True
        Me.txt3Email.Properties.Appearance.Options.UseTextOptions = True
        Me.txt3Email.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txt3Email.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt3Email.Properties.MaxLength = 100
        Me.txt3Email.Size = New System.Drawing.Size(248, 22)
        Me.txt3Email.TabIndex = 13
        Me.txt3Email.TextAlign = HorizontalAlignment.Left
        Me.txt3Email.ToolTipText = ""
        '
        'txt3Landline
        '
        Me.txt3Landline.CharacterCasing = CharacterCasing.Normal
        Me.txt3Landline.EnterMoveNextControl = True
        Me.txt3Landline.Location = New System.Drawing.Point(132, 148)
        Me.txt3Landline.MaxLength = 15
        Me.txt3Landline.Name = "txt3Landline"
        Me.txt3Landline.NumericAllowNegatives = False
        Me.txt3Landline.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txt3Landline.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt3Landline.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txt3Landline.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txt3Landline.Properties.Appearance.Options.UseFont = True
        Me.txt3Landline.Properties.Appearance.Options.UseTextOptions = True
        Me.txt3Landline.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txt3Landline.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt3Landline.Properties.MaxLength = 15
        Me.txt3Landline.Size = New System.Drawing.Size(248, 22)
        Me.txt3Landline.TabIndex = 11
        Me.txt3Landline.TextAlign = HorizontalAlignment.Left
        Me.txt3Landline.ToolTipText = ""
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(10, 151)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(45, 15)
        Me.CareLabel14.TabIndex = 10
        Me.CareLabel14.Text = "Landline"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(10, 179)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(74, 15)
        Me.CareLabel11.TabIndex = 12
        Me.CareLabel11.Text = "Email Address"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkCont3
        '
        Me.chkCont3.EnterMoveNextControl = True
        Me.chkCont3.Location = New System.Drawing.Point(130, 11)
        Me.chkCont3.Name = "chkCont3"
        Me.chkCont3.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkCont3.Properties.Appearance.Options.UseFont = True
        Me.chkCont3.Properties.Caption = ""
        Me.chkCont3.Size = New System.Drawing.Size(21, 19)
        Me.chkCont3.TabIndex = 1
        '
        'CareLabel12
        '
        Me.CareLabel12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel12.Location = New System.Drawing.Point(9, 13)
        Me.CareLabel12.Name = "CareLabel12"
        Me.CareLabel12.Size = New System.Drawing.Size(103, 15)
        Me.CareLabel12.TabIndex = 0
        Me.CareLabel12.Text = "Enter Third Contact"
        Me.CareLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt3Secret
        '
        Me.txt3Secret.CharacterCasing = CharacterCasing.Upper
        Me.txt3Secret.EnterMoveNextControl = True
        Me.txt3Secret.Location = New System.Drawing.Point(132, 204)
        Me.txt3Secret.MaxLength = 30
        Me.txt3Secret.Name = "txt3Secret"
        Me.txt3Secret.NumericAllowNegatives = False
        Me.txt3Secret.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txt3Secret.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt3Secret.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txt3Secret.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txt3Secret.Properties.Appearance.Options.UseFont = True
        Me.txt3Secret.Properties.Appearance.Options.UseTextOptions = True
        Me.txt3Secret.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txt3Secret.Properties.CharacterCasing = CharacterCasing.Upper
        Me.txt3Secret.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt3Secret.Properties.MaxLength = 30
        Me.txt3Secret.Size = New System.Drawing.Size(248, 22)
        Me.txt3Secret.TabIndex = 15
        Me.txt3Secret.TextAlign = HorizontalAlignment.Left
        Me.txt3Secret.ToolTipText = ""
        '
        'CareLabel13
        '
        Me.CareLabel13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel13.Location = New System.Drawing.Point(10, 207)
        Me.CareLabel13.Name = "CareLabel13"
        Me.CareLabel13.Size = New System.Drawing.Size(64, 15)
        Me.CareLabel13.TabIndex = 14
        Me.CareLabel13.Text = "Secret Word"
        Me.CareLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt3Mobile
        '
        Me.txt3Mobile.CharacterCasing = CharacterCasing.Normal
        Me.txt3Mobile.EnterMoveNextControl = True
        Me.txt3Mobile.Location = New System.Drawing.Point(132, 120)
        Me.txt3Mobile.MaxLength = 15
        Me.txt3Mobile.Name = "txt3Mobile"
        Me.txt3Mobile.NumericAllowNegatives = False
        Me.txt3Mobile.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txt3Mobile.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt3Mobile.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txt3Mobile.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txt3Mobile.Properties.Appearance.Options.UseFont = True
        Me.txt3Mobile.Properties.Appearance.Options.UseTextOptions = True
        Me.txt3Mobile.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txt3Mobile.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt3Mobile.Properties.MaxLength = 15
        Me.txt3Mobile.Size = New System.Drawing.Size(248, 22)
        Me.txt3Mobile.TabIndex = 9
        Me.txt3Mobile.TextAlign = HorizontalAlignment.Left
        Me.txt3Mobile.ToolTipText = ""
        '
        'CareLabel17
        '
        Me.CareLabel17.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel17.Location = New System.Drawing.Point(10, 123)
        Me.CareLabel17.Name = "CareLabel17"
        Me.CareLabel17.Size = New System.Drawing.Size(84, 15)
        Me.CareLabel17.TabIndex = 8
        Me.CareLabel17.Text = "Mobile Number"
        Me.CareLabel17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt3Relationship
        '
        Me.txt3Relationship.CharacterCasing = CharacterCasing.Normal
        Me.txt3Relationship.EnterMoveNextControl = True
        Me.txt3Relationship.Location = New System.Drawing.Point(132, 92)
        Me.txt3Relationship.MaxLength = 30
        Me.txt3Relationship.Name = "txt3Relationship"
        Me.txt3Relationship.NumericAllowNegatives = False
        Me.txt3Relationship.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txt3Relationship.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt3Relationship.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txt3Relationship.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txt3Relationship.Properties.Appearance.Options.UseFont = True
        Me.txt3Relationship.Properties.Appearance.Options.UseTextOptions = True
        Me.txt3Relationship.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txt3Relationship.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt3Relationship.Properties.MaxLength = 30
        Me.txt3Relationship.Size = New System.Drawing.Size(248, 22)
        Me.txt3Relationship.TabIndex = 7
        Me.txt3Relationship.TextAlign = HorizontalAlignment.Left
        Me.txt3Relationship.ToolTipText = ""
        '
        'CareLabel26
        '
        Me.CareLabel26.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel26.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel26.Location = New System.Drawing.Point(10, 95)
        Me.CareLabel26.Name = "CareLabel26"
        Me.CareLabel26.Size = New System.Drawing.Size(65, 15)
        Me.CareLabel26.TabIndex = 6
        Me.CareLabel26.Text = "Relationship"
        Me.CareLabel26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt3Surname
        '
        Me.txt3Surname.CharacterCasing = CharacterCasing.Normal
        Me.txt3Surname.EnterMoveNextControl = True
        Me.txt3Surname.Location = New System.Drawing.Point(132, 64)
        Me.txt3Surname.MaxLength = 30
        Me.txt3Surname.Name = "txt3Surname"
        Me.txt3Surname.NumericAllowNegatives = False
        Me.txt3Surname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txt3Surname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt3Surname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txt3Surname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txt3Surname.Properties.Appearance.Options.UseFont = True
        Me.txt3Surname.Properties.Appearance.Options.UseTextOptions = True
        Me.txt3Surname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txt3Surname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt3Surname.Properties.MaxLength = 30
        Me.txt3Surname.Size = New System.Drawing.Size(248, 22)
        Me.txt3Surname.TabIndex = 5
        Me.txt3Surname.TextAlign = HorizontalAlignment.Left
        Me.txt3Surname.ToolTipText = ""
        '
        'CareLabel27
        '
        Me.CareLabel27.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel27.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel27.Location = New System.Drawing.Point(10, 67)
        Me.CareLabel27.Name = "CareLabel27"
        Me.CareLabel27.Size = New System.Drawing.Size(47, 15)
        Me.CareLabel27.TabIndex = 4
        Me.CareLabel27.Text = "Surname"
        Me.CareLabel27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt3Forename
        '
        Me.txt3Forename.CharacterCasing = CharacterCasing.Normal
        Me.txt3Forename.EnterMoveNextControl = True
        Me.txt3Forename.Location = New System.Drawing.Point(132, 36)
        Me.txt3Forename.MaxLength = 30
        Me.txt3Forename.Name = "txt3Forename"
        Me.txt3Forename.NumericAllowNegatives = False
        Me.txt3Forename.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txt3Forename.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt3Forename.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txt3Forename.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txt3Forename.Properties.Appearance.Options.UseFont = True
        Me.txt3Forename.Properties.Appearance.Options.UseTextOptions = True
        Me.txt3Forename.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txt3Forename.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt3Forename.Properties.MaxLength = 30
        Me.txt3Forename.Size = New System.Drawing.Size(248, 22)
        Me.txt3Forename.TabIndex = 3
        Me.txt3Forename.TextAlign = HorizontalAlignment.Left
        Me.txt3Forename.ToolTipText = ""
        '
        'CareLabel28
        '
        Me.CareLabel28.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel28.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel28.Location = New System.Drawing.Point(9, 39)
        Me.CareLabel28.Name = "CareLabel28"
        Me.CareLabel28.Size = New System.Drawing.Size(53, 15)
        Me.CareLabel28.TabIndex = 2
        Me.CareLabel28.Text = "Forename"
        Me.CareLabel28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'wpChild
        '
        Me.wpChild.Controls.Add(Me.Panel5)
        Me.wpChild.Name = "wpChild"
        Me.wpChild.Size = New System.Drawing.Size(680, 355)
        Me.wpChild.Text = "Enter Child Details"
        '
        'Panel5
        '
        Me.Panel5.Anchor = AnchorStyles.None
        Me.Panel5.BorderStyle = BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.cbxSite)
        Me.Panel5.Controls.Add(Me.CareLabel31)
        Me.Panel5.Controls.Add(Me.lblDOB)
        Me.Panel5.Controls.Add(Me.cbxChildKeyworker)
        Me.Panel5.Controls.Add(Me.cbxChildGroup)
        Me.Panel5.Controls.Add(Me.Label13)
        Me.Panel5.Controls.Add(Me.Label14)
        Me.Panel5.Controls.Add(Me.Label4)
        Me.Panel5.Controls.Add(Me.cdtChildStart)
        Me.Panel5.Controls.Add(Me.Label12)
        Me.Panel5.Controls.Add(Me.cbxChildGender)
        Me.Panel5.Controls.Add(Me.lblStartDate)
        Me.Panel5.Controls.Add(Me.Label16)
        Me.Panel5.Controls.Add(Me.cdtChildDOB)
        Me.Panel5.Controls.Add(Me.txtChildSurname)
        Me.Panel5.Controls.Add(Me.CareLabel35)
        Me.Panel5.Controls.Add(Me.txtChildForename)
        Me.Panel5.Controls.Add(Me.CareLabel36)
        Me.Panel5.Location = New System.Drawing.Point(140, 49)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(401, 258)
        Me.Panel5.TabIndex = 0
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(100, 166)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AccessibleName = "Group"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(280, 22)
        Me.cbxSite.TabIndex = 13
        Me.cbxSite.Tag = "AEBM"
        Me.cbxSite.ValueMember = Nothing
        '
        'CareLabel31
        '
        Me.CareLabel31.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel31.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel31.Location = New System.Drawing.Point(10, 169)
        Me.CareLabel31.Name = "CareLabel31"
        Me.CareLabel31.Size = New System.Drawing.Size(19, 15)
        Me.CareLabel31.TabIndex = 12
        Me.CareLabel31.Text = "Site"
        Me.CareLabel31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDOB
        '
        Me.lblDOB.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDOB.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblDOB.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblDOB.Location = New System.Drawing.Point(210, 99)
        Me.lblDOB.Name = "lblDOB"
        Me.lblDOB.Size = New System.Drawing.Size(147, 15)
        Me.lblDOB.TabIndex = 8
        Me.lblDOB.Text = "1 year, 11 months, 21 days"
        Me.lblDOB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxChildKeyworker
        '
        Me.cbxChildKeyworker.AllowBlank = False
        Me.cbxChildKeyworker.DataSource = Nothing
        Me.cbxChildKeyworker.DisplayMember = Nothing
        Me.cbxChildKeyworker.EnterMoveNextControl = True
        Me.cbxChildKeyworker.Location = New System.Drawing.Point(100, 222)
        Me.cbxChildKeyworker.Name = "cbxChildKeyworker"
        Me.cbxChildKeyworker.Properties.AccessibleName = "Keyworker"
        Me.cbxChildKeyworker.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxChildKeyworker.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxChildKeyworker.Properties.Appearance.Options.UseFont = True
        Me.cbxChildKeyworker.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxChildKeyworker.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxChildKeyworker.SelectedValue = Nothing
        Me.cbxChildKeyworker.Size = New System.Drawing.Size(280, 22)
        Me.cbxChildKeyworker.TabIndex = 17
        Me.cbxChildKeyworker.Tag = "AEBM"
        Me.cbxChildKeyworker.ValueMember = Nothing
        '
        'cbxChildGroup
        '
        Me.cbxChildGroup.AllowBlank = False
        Me.cbxChildGroup.DataSource = Nothing
        Me.cbxChildGroup.DisplayMember = Nothing
        Me.cbxChildGroup.EnterMoveNextControl = True
        Me.cbxChildGroup.Location = New System.Drawing.Point(100, 194)
        Me.cbxChildGroup.Name = "cbxChildGroup"
        Me.cbxChildGroup.Properties.AccessibleName = "Group"
        Me.cbxChildGroup.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxChildGroup.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxChildGroup.Properties.Appearance.Options.UseFont = True
        Me.cbxChildGroup.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxChildGroup.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxChildGroup.SelectedValue = Nothing
        Me.cbxChildGroup.Size = New System.Drawing.Size(280, 22)
        Me.cbxChildGroup.TabIndex = 15
        Me.cbxChildGroup.Tag = "AEBM"
        Me.cbxChildGroup.ValueMember = Nothing
        '
        'Label13
        '
        Me.Label13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label13.Location = New System.Drawing.Point(11, 225)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(55, 15)
        Me.Label13.TabIndex = 16
        Me.Label13.Text = "Keyworker"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label14
        '
        Me.Label14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label14.Location = New System.Drawing.Point(10, 197)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(33, 15)
        Me.Label14.TabIndex = 14
        Me.Label14.Text = "Group"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label4.Location = New System.Drawing.Point(10, 141)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 15)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Start Date"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label4.ToolTip = "The date the child started in the Nursery. This date is used for invoicing purpos" & _
    "es."
        Me.Label4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'cdtChildStart
        '
        Me.cdtChildStart.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtChildStart.EnterMoveNextControl = True
        Me.cdtChildStart.Location = New System.Drawing.Point(100, 138)
        Me.cdtChildStart.Name = "cdtChildStart"
        Me.cdtChildStart.Properties.AccessibleName = "Joining Date"
        Me.cdtChildStart.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtChildStart.Properties.Appearance.Options.UseFont = True
        Me.cdtChildStart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtChildStart.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtChildStart.Size = New System.Drawing.Size(104, 22)
        Me.cdtChildStart.TabIndex = 10
        Me.cdtChildStart.Tag = "AEBM"
        Me.cdtChildStart.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'Label12
        '
        Me.Label12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label12.Location = New System.Drawing.Point(10, 71)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(38, 15)
        Me.Label12.TabIndex = 4
        Me.Label12.Text = "Gender"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxChildGender
        '
        Me.cbxChildGender.AllowBlank = False
        Me.cbxChildGender.DataSource = Nothing
        Me.cbxChildGender.DisplayMember = Nothing
        Me.cbxChildGender.EnterMoveNextControl = True
        Me.cbxChildGender.Location = New System.Drawing.Point(100, 68)
        Me.cbxChildGender.Name = "cbxChildGender"
        Me.cbxChildGender.Properties.AccessibleName = "Gender"
        Me.cbxChildGender.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxChildGender.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxChildGender.Properties.Appearance.Options.UseFont = True
        Me.cbxChildGender.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxChildGender.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxChildGender.SelectedValue = Nothing
        Me.cbxChildGender.Size = New System.Drawing.Size(104, 22)
        Me.cbxChildGender.TabIndex = 5
        Me.cbxChildGender.Tag = "AEBM"
        Me.cbxChildGender.ValueMember = Nothing
        '
        'lblStartDate
        '
        Me.lblStartDate.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblStartDate.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblStartDate.Location = New System.Drawing.Point(210, 141)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(147, 15)
        Me.lblStartDate.TabIndex = 11
        Me.lblStartDate.Text = "1 year, 11 months, 21 days"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label16
        '
        Me.Label16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label16.Location = New System.Drawing.Point(10, 99)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(66, 15)
        Me.Label16.TabIndex = 6
        Me.Label16.Text = "Date of Birth"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtChildDOB
        '
        Me.cdtChildDOB.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtChildDOB.EnterMoveNextControl = True
        Me.cdtChildDOB.Location = New System.Drawing.Point(100, 96)
        Me.cdtChildDOB.Name = "cdtChildDOB"
        Me.cdtChildDOB.Properties.AccessibleName = "Date of Birth"
        Me.cdtChildDOB.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtChildDOB.Properties.Appearance.Options.UseFont = True
        Me.cdtChildDOB.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtChildDOB.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtChildDOB.Size = New System.Drawing.Size(104, 22)
        Me.cdtChildDOB.TabIndex = 7
        Me.cdtChildDOB.Tag = "AEBM"
        Me.cdtChildDOB.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'txtChildSurname
        '
        Me.txtChildSurname.CharacterCasing = CharacterCasing.Normal
        Me.txtChildSurname.EnterMoveNextControl = True
        Me.txtChildSurname.Location = New System.Drawing.Point(100, 40)
        Me.txtChildSurname.MaxLength = 30
        Me.txtChildSurname.Name = "txtChildSurname"
        Me.txtChildSurname.NumericAllowNegatives = False
        Me.txtChildSurname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtChildSurname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtChildSurname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtChildSurname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtChildSurname.Properties.Appearance.Options.UseFont = True
        Me.txtChildSurname.Properties.Appearance.Options.UseTextOptions = True
        Me.txtChildSurname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtChildSurname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtChildSurname.Properties.MaxLength = 30
        Me.txtChildSurname.Size = New System.Drawing.Size(280, 22)
        Me.txtChildSurname.TabIndex = 3
        Me.txtChildSurname.TextAlign = HorizontalAlignment.Left
        Me.txtChildSurname.ToolTipText = ""
        '
        'CareLabel35
        '
        Me.CareLabel35.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel35.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel35.Location = New System.Drawing.Point(11, 43)
        Me.CareLabel35.Name = "CareLabel35"
        Me.CareLabel35.Size = New System.Drawing.Size(47, 15)
        Me.CareLabel35.TabIndex = 2
        Me.CareLabel35.Text = "Surname"
        Me.CareLabel35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtChildForename
        '
        Me.txtChildForename.CharacterCasing = CharacterCasing.Normal
        Me.txtChildForename.EnterMoveNextControl = True
        Me.txtChildForename.Location = New System.Drawing.Point(100, 12)
        Me.txtChildForename.MaxLength = 30
        Me.txtChildForename.Name = "txtChildForename"
        Me.txtChildForename.NumericAllowNegatives = False
        Me.txtChildForename.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtChildForename.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtChildForename.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtChildForename.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtChildForename.Properties.Appearance.Options.UseFont = True
        Me.txtChildForename.Properties.Appearance.Options.UseTextOptions = True
        Me.txtChildForename.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtChildForename.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtChildForename.Properties.MaxLength = 30
        Me.txtChildForename.Size = New System.Drawing.Size(280, 22)
        Me.txtChildForename.TabIndex = 1
        Me.txtChildForename.TextAlign = HorizontalAlignment.Left
        Me.txtChildForename.ToolTipText = ""
        '
        'CareLabel36
        '
        Me.CareLabel36.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel36.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel36.Location = New System.Drawing.Point(10, 15)
        Me.CareLabel36.Name = "CareLabel36"
        Me.CareLabel36.Size = New System.Drawing.Size(53, 15)
        Me.CareLabel36.TabIndex = 0
        Me.CareLabel36.Text = "Forename"
        Me.CareLabel36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'wpSessionsMode
        '
        Me.wpSessionsMode.Controls.Add(Me.CareLabel59)
        Me.wpSessionsMode.Controls.Add(Me.Panel11)
        Me.wpSessionsMode.Controls.Add(Me.CareLabel30)
        Me.wpSessionsMode.Controls.Add(Me.Panel6)
        Me.wpSessionsMode.Name = "wpSessionsMode"
        Me.wpSessionsMode.Size = New System.Drawing.Size(680, 355)
        Me.wpSessionsMode.Text = "Sessions Mode"
        '
        'CareLabel59
        '
        Me.CareLabel59.Anchor = AnchorStyles.None
        Me.CareLabel59.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel59.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel59.Location = New System.Drawing.Point(140, 245)
        Me.CareLabel59.Name = "CareLabel59"
        Me.CareLabel59.Size = New System.Drawing.Size(327, 15)
        Me.CareLabel59.TabIndex = 2
        Me.CareLabel59.Text = "Please enter week commencing date for chargeable sessions..."
        Me.CareLabel59.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel11
        '
        Me.Panel11.Anchor = AnchorStyles.None
        Me.Panel11.BorderStyle = BorderStyle.FixedSingle
        Me.Panel11.Controls.Add(Me.CareLabel61)
        Me.Panel11.Controls.Add(Me.cdtWC)
        Me.Panel11.Controls.Add(Me.CareLabel60)
        Me.Panel11.Location = New System.Drawing.Point(140, 266)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(401, 52)
        Me.Panel11.TabIndex = 3
        '
        'CareLabel61
        '
        Me.CareLabel61.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel61.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.CareLabel61.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel61.Location = New System.Drawing.Point(209, 17)
        Me.CareLabel61.Name = "CareLabel61"
        Me.CareLabel61.Size = New System.Drawing.Size(101, 15)
        Me.CareLabel61.TabIndex = 2
        Me.CareLabel61.Text = "Must be a Monday"
        Me.CareLabel61.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtWC
        '
        Me.cdtWC.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtWC.EnterMoveNextControl = True
        Me.cdtWC.Location = New System.Drawing.Point(121, 14)
        Me.cdtWC.Name = "cdtWC"
        Me.cdtWC.Properties.AccessibleName = "Date of Birth"
        Me.cdtWC.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtWC.Properties.Appearance.Options.UseFont = True
        Me.cdtWC.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtWC.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtWC.Size = New System.Drawing.Size(82, 22)
        Me.cdtWC.TabIndex = 1
        Me.cdtWC.Tag = "AEBM"
        Me.cdtWC.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'CareLabel60
        '
        Me.CareLabel60.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel60.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel60.Location = New System.Drawing.Point(10, 17)
        Me.CareLabel60.Name = "CareLabel60"
        Me.CareLabel60.Size = New System.Drawing.Size(105, 15)
        Me.CareLabel60.TabIndex = 0
        Me.CareLabel60.Text = "Week Commencing"
        Me.CareLabel60.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel30
        '
        Me.CareLabel30.Anchor = AnchorStyles.None
        Me.CareLabel30.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel30.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel30.Location = New System.Drawing.Point(140, 23)
        Me.CareLabel30.Name = "CareLabel30"
        Me.CareLabel30.Size = New System.Drawing.Size(303, 15)
        Me.CareLabel30.TabIndex = 0
        Me.CareLabel30.Text = "Choose an option that matches the sessions for this Child"
        Me.CareLabel30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel6
        '
        Me.Panel6.Anchor = AnchorStyles.None
        Me.Panel6.BorderStyle = BorderStyle.FixedSingle
        Me.Panel6.Controls.Add(Me.radSkip)
        Me.Panel6.Controls.Add(Me.radMultipleFullWeek)
        Me.Panel6.Controls.Add(Me.radSingleWeekDays)
        Me.Panel6.Controls.Add(Me.radMultiWeekDays)
        Me.Panel6.Controls.Add(Me.radSingleFullWeek)
        Me.Panel6.Location = New System.Drawing.Point(140, 44)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(401, 172)
        Me.Panel6.TabIndex = 1
        '
        'radSkip
        '
        Me.radSkip.CausesValidation = False
        Me.radSkip.EditValue = True
        Me.radSkip.Location = New System.Drawing.Point(14, 19)
        Me.radSkip.Name = "radSkip"
        Me.radSkip.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.radSkip.Properties.Appearance.Options.UseFont = True
        Me.radSkip.Properties.Appearance.Options.UseTextOptions = True
        Me.radSkip.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radSkip.Properties.AutoWidth = True
        Me.radSkip.Properties.Caption = "Skip entering sessions for now, I'll do them later."
        Me.radSkip.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radSkip.Properties.RadioGroupIndex = 0
        Me.radSkip.Size = New System.Drawing.Size(276, 19)
        Me.radSkip.TabIndex = 0
        '
        'radMultipleFullWeek
        '
        Me.radMultipleFullWeek.CausesValidation = False
        Me.radMultipleFullWeek.Location = New System.Drawing.Point(14, 104)
        Me.radMultipleFullWeek.Name = "radMultipleFullWeek"
        Me.radMultipleFullWeek.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.radMultipleFullWeek.Properties.Appearance.Options.UseFont = True
        Me.radMultipleFullWeek.Properties.Appearance.Options.UseTextOptions = True
        Me.radMultipleFullWeek.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radMultipleFullWeek.Properties.Caption = "Child attends multiple sessions every day of the week."
        Me.radMultipleFullWeek.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radMultipleFullWeek.Properties.RadioGroupIndex = 0
        Me.radMultipleFullWeek.Size = New System.Drawing.Size(366, 19)
        Me.radMultipleFullWeek.TabIndex = 3
        Me.radMultipleFullWeek.TabStop = False
        Me.radMultipleFullWeek.ToolTip = "e.g. Joe attends every day of the week for the breakfast club session and the mor" & _
    "ning session."
        '
        'radSingleWeekDays
        '
        Me.radSingleWeekDays.CausesValidation = False
        Me.radSingleWeekDays.Location = New System.Drawing.Point(14, 79)
        Me.radSingleWeekDays.Name = "radSingleWeekDays"
        Me.radSingleWeekDays.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.radSingleWeekDays.Properties.Appearance.Options.UseFont = True
        Me.radSingleWeekDays.Properties.Appearance.Options.UseTextOptions = True
        Me.radSingleWeekDays.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radSingleWeekDays.Properties.AutoWidth = True
        Me.radSingleWeekDays.Properties.Caption = "Child attends a single session on recurring weekdays."
        Me.radSingleWeekDays.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radSingleWeekDays.Properties.RadioGroupIndex = 0
        Me.radSingleWeekDays.Size = New System.Drawing.Size(301, 19)
        Me.radSingleWeekDays.TabIndex = 2
        Me.radSingleWeekDays.TabStop = False
        Me.radSingleWeekDays.ToolTip = "e.g. Joe attends an all day session on Monday and Thursday every week."
        '
        'radMultiWeekDays
        '
        Me.radMultiWeekDays.CausesValidation = False
        Me.radMultiWeekDays.Location = New System.Drawing.Point(14, 129)
        Me.radMultiWeekDays.Name = "radMultiWeekDays"
        Me.radMultiWeekDays.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.radMultiWeekDays.Properties.Appearance.Options.UseFont = True
        Me.radMultiWeekDays.Properties.Appearance.Options.UseTextOptions = True
        Me.radMultiWeekDays.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radMultiWeekDays.Properties.AutoWidth = True
        Me.radMultiWeekDays.Properties.Caption = "Child attends multiple sessions on recurring weekdays."
        Me.radMultiWeekDays.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radMultiWeekDays.Properties.RadioGroupIndex = 0
        Me.radMultiWeekDays.Size = New System.Drawing.Size(310, 19)
        Me.radMultiWeekDays.TabIndex = 4
        Me.radMultiWeekDays.TabStop = False
        Me.radMultiWeekDays.ToolTip = "e.g. Joe attends every Monday and Thursday for the breakfast club session and the" & _
    " morning session."
        '
        'radSingleFullWeek
        '
        Me.radSingleFullWeek.CausesValidation = False
        Me.radSingleFullWeek.Location = New System.Drawing.Point(14, 54)
        Me.radSingleFullWeek.Name = "radSingleFullWeek"
        Me.radSingleFullWeek.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.radSingleFullWeek.Properties.Appearance.Options.UseFont = True
        Me.radSingleFullWeek.Properties.Appearance.Options.UseTextOptions = True
        Me.radSingleFullWeek.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radSingleFullWeek.Properties.Caption = "Child attends a single session every day of the week."
        Me.radSingleFullWeek.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radSingleFullWeek.Properties.RadioGroupIndex = 0
        Me.radSingleFullWeek.Size = New System.Drawing.Size(366, 19)
        Me.radSingleFullWeek.TabIndex = 1
        Me.radSingleFullWeek.TabStop = False
        Me.radSingleFullWeek.ToolTip = "e.g. Joe attends every day of the week for a half day session."
        '
        'wpSessionSingle
        '
        Me.wpSessionSingle.Controls.Add(Me.CareLabel42)
        Me.wpSessionSingle.Controls.Add(Me.Panel8)
        Me.wpSessionSingle.Controls.Add(Me.CareLabel29)
        Me.wpSessionSingle.Controls.Add(Me.Panel7)
        Me.wpSessionSingle.Name = "wpSessionSingle"
        Me.wpSessionSingle.Size = New System.Drawing.Size(680, 355)
        Me.wpSessionSingle.Text = "Enter sessions"
        '
        'CareLabel42
        '
        Me.CareLabel42.Anchor = AnchorStyles.None
        Me.CareLabel42.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel42.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel42.Location = New System.Drawing.Point(140, 172)
        Me.CareLabel42.Name = "CareLabel42"
        Me.CareLabel42.Size = New System.Drawing.Size(289, 15)
        Me.CareLabel42.TabIndex = 2
        Me.CareLabel42.Text = "Select the day(s) the child will be attending this session"
        Me.CareLabel42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel8
        '
        Me.Panel8.Anchor = AnchorStyles.None
        Me.Panel8.BorderStyle = BorderStyle.FixedSingle
        Me.Panel8.Controls.Add(Me.TableLayoutPanel1)
        Me.Panel8.Location = New System.Drawing.Point(140, 192)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(401, 62)
        Me.Panel8.TabIndex = 7
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 7
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel1.Controls.Add(Me.chkSingleSun, 6, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.chkSingleSat, 5, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.chkSingleFri, 4, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.chkSingleThu, 3, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.chkSingleWed, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.chkSingleTue, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.lblSFri, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblSWed, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblSTue, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblSMon, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblSThu, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblSSun, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblSSat, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.chkSingleMon, 0, 1)
        Me.TableLayoutPanel1.Dock = DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New RowStyle(SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New RowStyle(SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New RowStyle(SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(399, 60)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'chkSingleSun
        '
        Me.chkSingleSun.Dock = DockStyle.Fill
        Me.chkSingleSun.EnterMoveNextControl = True
        Me.chkSingleSun.Location = New System.Drawing.Point(339, 33)
        Me.chkSingleSun.Name = "chkSingleSun"
        Me.chkSingleSun.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkSingleSun.Properties.Appearance.Options.UseFont = True
        Me.chkSingleSun.Properties.AutoHeight = False
        Me.chkSingleSun.Properties.Caption = ""
        Me.chkSingleSun.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkSingleSun.Size = New System.Drawing.Size(57, 24)
        Me.chkSingleSun.TabIndex = 13
        '
        'chkSingleSat
        '
        Me.chkSingleSat.Dock = DockStyle.Fill
        Me.chkSingleSat.EnterMoveNextControl = True
        Me.chkSingleSat.Location = New System.Drawing.Point(283, 33)
        Me.chkSingleSat.Name = "chkSingleSat"
        Me.chkSingleSat.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkSingleSat.Properties.Appearance.Options.UseFont = True
        Me.chkSingleSat.Properties.AutoHeight = False
        Me.chkSingleSat.Properties.Caption = ""
        Me.chkSingleSat.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkSingleSat.Size = New System.Drawing.Size(50, 24)
        Me.chkSingleSat.TabIndex = 12
        '
        'chkSingleFri
        '
        Me.chkSingleFri.Dock = DockStyle.Fill
        Me.chkSingleFri.EnterMoveNextControl = True
        Me.chkSingleFri.Location = New System.Drawing.Point(227, 33)
        Me.chkSingleFri.Name = "chkSingleFri"
        Me.chkSingleFri.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkSingleFri.Properties.Appearance.Options.UseFont = True
        Me.chkSingleFri.Properties.AutoHeight = False
        Me.chkSingleFri.Properties.Caption = ""
        Me.chkSingleFri.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkSingleFri.Size = New System.Drawing.Size(50, 24)
        Me.chkSingleFri.TabIndex = 11
        '
        'chkSingleThu
        '
        Me.chkSingleThu.Dock = DockStyle.Fill
        Me.chkSingleThu.EnterMoveNextControl = True
        Me.chkSingleThu.Location = New System.Drawing.Point(171, 33)
        Me.chkSingleThu.Name = "chkSingleThu"
        Me.chkSingleThu.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkSingleThu.Properties.Appearance.Options.UseFont = True
        Me.chkSingleThu.Properties.AutoHeight = False
        Me.chkSingleThu.Properties.Caption = ""
        Me.chkSingleThu.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkSingleThu.Size = New System.Drawing.Size(50, 24)
        Me.chkSingleThu.TabIndex = 10
        '
        'chkSingleWed
        '
        Me.chkSingleWed.Dock = DockStyle.Fill
        Me.chkSingleWed.EnterMoveNextControl = True
        Me.chkSingleWed.Location = New System.Drawing.Point(115, 33)
        Me.chkSingleWed.Name = "chkSingleWed"
        Me.chkSingleWed.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkSingleWed.Properties.Appearance.Options.UseFont = True
        Me.chkSingleWed.Properties.AutoHeight = False
        Me.chkSingleWed.Properties.Caption = ""
        Me.chkSingleWed.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkSingleWed.Size = New System.Drawing.Size(50, 24)
        Me.chkSingleWed.TabIndex = 9
        '
        'chkSingleTue
        '
        Me.chkSingleTue.Dock = DockStyle.Fill
        Me.chkSingleTue.EnterMoveNextControl = True
        Me.chkSingleTue.Location = New System.Drawing.Point(59, 33)
        Me.chkSingleTue.Name = "chkSingleTue"
        Me.chkSingleTue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkSingleTue.Properties.Appearance.Options.UseFont = True
        Me.chkSingleTue.Properties.AutoHeight = False
        Me.chkSingleTue.Properties.Caption = ""
        Me.chkSingleTue.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkSingleTue.Size = New System.Drawing.Size(50, 24)
        Me.chkSingleTue.TabIndex = 8
        '
        'lblSFri
        '
        Me.lblSFri.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblSFri.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lblSFri.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblSFri.Dock = DockStyle.Fill
        Me.lblSFri.Location = New System.Drawing.Point(227, 3)
        Me.lblSFri.Name = "lblSFri"
        Me.lblSFri.Size = New System.Drawing.Size(50, 24)
        Me.lblSFri.TabIndex = 4
        Me.lblSFri.Text = "Fri"
        Me.lblSFri.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSWed
        '
        Me.lblSWed.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblSWed.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lblSWed.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblSWed.Dock = DockStyle.Fill
        Me.lblSWed.Location = New System.Drawing.Point(115, 3)
        Me.lblSWed.Name = "lblSWed"
        Me.lblSWed.Size = New System.Drawing.Size(50, 24)
        Me.lblSWed.TabIndex = 2
        Me.lblSWed.Text = "Wed"
        Me.lblSWed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSTue
        '
        Me.lblSTue.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblSTue.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lblSTue.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblSTue.Dock = DockStyle.Fill
        Me.lblSTue.Location = New System.Drawing.Point(59, 3)
        Me.lblSTue.Name = "lblSTue"
        Me.lblSTue.Size = New System.Drawing.Size(50, 24)
        Me.lblSTue.TabIndex = 1
        Me.lblSTue.Text = "Tue"
        Me.lblSTue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSMon
        '
        Me.lblSMon.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblSMon.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lblSMon.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblSMon.Dock = DockStyle.Fill
        Me.lblSMon.Location = New System.Drawing.Point(3, 3)
        Me.lblSMon.Name = "lblSMon"
        Me.lblSMon.Size = New System.Drawing.Size(50, 24)
        Me.lblSMon.TabIndex = 0
        Me.lblSMon.Text = "Mon"
        Me.lblSMon.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSThu
        '
        Me.lblSThu.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblSThu.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lblSThu.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblSThu.Dock = DockStyle.Fill
        Me.lblSThu.Location = New System.Drawing.Point(171, 3)
        Me.lblSThu.Name = "lblSThu"
        Me.lblSThu.Size = New System.Drawing.Size(50, 24)
        Me.lblSThu.TabIndex = 3
        Me.lblSThu.Text = "Thurs"
        Me.lblSThu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSSun
        '
        Me.lblSSun.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblSSun.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lblSSun.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblSSun.Dock = DockStyle.Fill
        Me.lblSSun.Location = New System.Drawing.Point(339, 3)
        Me.lblSSun.Name = "lblSSun"
        Me.lblSSun.Size = New System.Drawing.Size(57, 24)
        Me.lblSSun.TabIndex = 6
        Me.lblSSun.Text = "Sun"
        Me.lblSSun.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSSat
        '
        Me.lblSSat.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblSSat.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lblSSat.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblSSat.Dock = DockStyle.Fill
        Me.lblSSat.Location = New System.Drawing.Point(283, 3)
        Me.lblSSat.Name = "lblSSat"
        Me.lblSSat.Size = New System.Drawing.Size(50, 24)
        Me.lblSSat.TabIndex = 5
        Me.lblSSat.Text = "Sat"
        Me.lblSSat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkSingleMon
        '
        Me.chkSingleMon.Dock = DockStyle.Fill
        Me.chkSingleMon.EnterMoveNextControl = True
        Me.chkSingleMon.Location = New System.Drawing.Point(3, 33)
        Me.chkSingleMon.Name = "chkSingleMon"
        Me.chkSingleMon.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkSingleMon.Properties.Appearance.Options.UseFont = True
        Me.chkSingleMon.Properties.AutoHeight = False
        Me.chkSingleMon.Properties.Caption = ""
        Me.chkSingleMon.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkSingleMon.Size = New System.Drawing.Size(50, 24)
        Me.chkSingleMon.TabIndex = 7
        '
        'CareLabel29
        '
        Me.CareLabel29.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel29.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel29.Location = New System.Drawing.Point(17, 83)
        Me.CareLabel29.Name = "CareLabel29"
        Me.CareLabel29.Size = New System.Drawing.Size(235, 15)
        Me.CareLabel29.TabIndex = 0
        Me.CareLabel29.Text = "Select the Session the Child will be attending"
        Me.CareLabel29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel7
        '
        Me.Panel7.BorderStyle = BorderStyle.FixedSingle
        Me.Panel7.Controls.Add(Me.tcSingle)
        Me.Panel7.Controls.Add(Me.CareLabel41)
        Me.Panel7.Location = New System.Drawing.Point(17, 104)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(649, 52)
        Me.Panel7.TabIndex = 1
        '
        'tcSingle
        '
        Me.tcSingle.BoltOns = ""
        Me.tcSingle.ComboValue = Nothing
        Me.tcSingle.Location = New System.Drawing.Point(59, 15)
        Me.tcSingle.Name = "tcSingle"
        Me.tcSingle.PopulateMode = Nursery.TariffControl.EnumPopulateMode.Weekly
        Me.tcSingle.SelectedIndex = -1
        Me.tcSingle.Size = New System.Drawing.Size(573, 20)
        Me.tcSingle.TabIndex = 1
        Me.tcSingle.Value1 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tcSingle.Value2 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tcSingle.ValueFEEE = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'CareLabel41
        '
        Me.CareLabel41.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel41.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel41.Location = New System.Drawing.Point(10, 17)
        Me.CareLabel41.Name = "CareLabel41"
        Me.CareLabel41.Size = New System.Drawing.Size(39, 15)
        Me.CareLabel41.TabIndex = 0
        Me.CareLabel41.Text = "Session"
        Me.CareLabel41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'wpSessionMultiple
        '
        Me.wpSessionMultiple.Controls.Add(Me.CareLabel43)
        Me.wpSessionMultiple.Controls.Add(Me.Panel9)
        Me.wpSessionMultiple.Controls.Add(Me.CareLabel51)
        Me.wpSessionMultiple.Controls.Add(Me.Panel10)
        Me.wpSessionMultiple.Name = "wpSessionMultiple"
        Me.wpSessionMultiple.Size = New System.Drawing.Size(680, 355)
        Me.wpSessionMultiple.Text = "Enter multiple sessions per day"
        '
        'CareLabel43
        '
        Me.CareLabel43.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel43.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel43.Location = New System.Drawing.Point(140, 253)
        Me.CareLabel43.Name = "CareLabel43"
        Me.CareLabel43.Size = New System.Drawing.Size(303, 15)
        Me.CareLabel43.TabIndex = 2
        Me.CareLabel43.Text = "Select the day(s) the child will be attending these sessions"
        Me.CareLabel43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel9
        '
        Me.Panel9.BorderStyle = BorderStyle.FixedSingle
        Me.Panel9.Controls.Add(Me.TableLayoutPanel2)
        Me.Panel9.Location = New System.Drawing.Point(140, 273)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(401, 62)
        Me.Panel9.TabIndex = 11
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 7
        Me.TableLayoutPanel2.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 14.28571!))
        Me.TableLayoutPanel2.Controls.Add(Me.chkMultiSun, 6, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.chkMultiSat, 5, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.chkMultiFri, 4, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.chkMultiThu, 3, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.chkMultiWed, 2, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.chkMultiTue, 1, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.lblMFri, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.lblMWed, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.lblMTue, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.lblMMon, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.lblMThu, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.lblMSun, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.lblMSat, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.chkMultiMon, 0, 1)
        Me.TableLayoutPanel2.Dock = DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New RowStyle(SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New RowStyle(SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New RowStyle(SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(399, 60)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'chkMultiSun
        '
        Me.chkMultiSun.Dock = DockStyle.Fill
        Me.chkMultiSun.EnterMoveNextControl = True
        Me.chkMultiSun.Location = New System.Drawing.Point(339, 33)
        Me.chkMultiSun.Name = "chkMultiSun"
        Me.chkMultiSun.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkMultiSun.Properties.Appearance.Options.UseFont = True
        Me.chkMultiSun.Properties.AutoHeight = False
        Me.chkMultiSun.Properties.Caption = ""
        Me.chkMultiSun.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkMultiSun.Size = New System.Drawing.Size(57, 24)
        Me.chkMultiSun.TabIndex = 13
        '
        'chkMultiSat
        '
        Me.chkMultiSat.Dock = DockStyle.Fill
        Me.chkMultiSat.EnterMoveNextControl = True
        Me.chkMultiSat.Location = New System.Drawing.Point(283, 33)
        Me.chkMultiSat.Name = "chkMultiSat"
        Me.chkMultiSat.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkMultiSat.Properties.Appearance.Options.UseFont = True
        Me.chkMultiSat.Properties.AutoHeight = False
        Me.chkMultiSat.Properties.Caption = ""
        Me.chkMultiSat.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkMultiSat.Size = New System.Drawing.Size(50, 24)
        Me.chkMultiSat.TabIndex = 12
        '
        'chkMultiFri
        '
        Me.chkMultiFri.Dock = DockStyle.Fill
        Me.chkMultiFri.EnterMoveNextControl = True
        Me.chkMultiFri.Location = New System.Drawing.Point(227, 33)
        Me.chkMultiFri.Name = "chkMultiFri"
        Me.chkMultiFri.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkMultiFri.Properties.Appearance.Options.UseFont = True
        Me.chkMultiFri.Properties.AutoHeight = False
        Me.chkMultiFri.Properties.Caption = ""
        Me.chkMultiFri.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkMultiFri.Size = New System.Drawing.Size(50, 24)
        Me.chkMultiFri.TabIndex = 11
        '
        'chkMultiThu
        '
        Me.chkMultiThu.Dock = DockStyle.Fill
        Me.chkMultiThu.EnterMoveNextControl = True
        Me.chkMultiThu.Location = New System.Drawing.Point(171, 33)
        Me.chkMultiThu.Name = "chkMultiThu"
        Me.chkMultiThu.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkMultiThu.Properties.Appearance.Options.UseFont = True
        Me.chkMultiThu.Properties.AutoHeight = False
        Me.chkMultiThu.Properties.Caption = ""
        Me.chkMultiThu.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkMultiThu.Size = New System.Drawing.Size(50, 24)
        Me.chkMultiThu.TabIndex = 10
        '
        'chkMultiWed
        '
        Me.chkMultiWed.Dock = DockStyle.Fill
        Me.chkMultiWed.EnterMoveNextControl = True
        Me.chkMultiWed.Location = New System.Drawing.Point(115, 33)
        Me.chkMultiWed.Name = "chkMultiWed"
        Me.chkMultiWed.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkMultiWed.Properties.Appearance.Options.UseFont = True
        Me.chkMultiWed.Properties.AutoHeight = False
        Me.chkMultiWed.Properties.Caption = ""
        Me.chkMultiWed.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkMultiWed.Size = New System.Drawing.Size(50, 24)
        Me.chkMultiWed.TabIndex = 9
        '
        'chkMultiTue
        '
        Me.chkMultiTue.Dock = DockStyle.Fill
        Me.chkMultiTue.EnterMoveNextControl = True
        Me.chkMultiTue.Location = New System.Drawing.Point(59, 33)
        Me.chkMultiTue.Name = "chkMultiTue"
        Me.chkMultiTue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkMultiTue.Properties.Appearance.Options.UseFont = True
        Me.chkMultiTue.Properties.AutoHeight = False
        Me.chkMultiTue.Properties.Caption = ""
        Me.chkMultiTue.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkMultiTue.Size = New System.Drawing.Size(50, 24)
        Me.chkMultiTue.TabIndex = 8
        '
        'lblMFri
        '
        Me.lblMFri.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblMFri.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lblMFri.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblMFri.Dock = DockStyle.Fill
        Me.lblMFri.Location = New System.Drawing.Point(227, 3)
        Me.lblMFri.Name = "lblMFri"
        Me.lblMFri.Size = New System.Drawing.Size(50, 24)
        Me.lblMFri.TabIndex = 4
        Me.lblMFri.Text = "Fri"
        Me.lblMFri.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblMWed
        '
        Me.lblMWed.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblMWed.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lblMWed.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblMWed.Dock = DockStyle.Fill
        Me.lblMWed.Location = New System.Drawing.Point(115, 3)
        Me.lblMWed.Name = "lblMWed"
        Me.lblMWed.Size = New System.Drawing.Size(50, 24)
        Me.lblMWed.TabIndex = 2
        Me.lblMWed.Text = "Wed"
        Me.lblMWed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblMTue
        '
        Me.lblMTue.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblMTue.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lblMTue.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblMTue.Dock = DockStyle.Fill
        Me.lblMTue.Location = New System.Drawing.Point(59, 3)
        Me.lblMTue.Name = "lblMTue"
        Me.lblMTue.Size = New System.Drawing.Size(50, 24)
        Me.lblMTue.TabIndex = 1
        Me.lblMTue.Text = "Tue"
        Me.lblMTue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblMMon
        '
        Me.lblMMon.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblMMon.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lblMMon.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblMMon.Dock = DockStyle.Fill
        Me.lblMMon.Location = New System.Drawing.Point(3, 3)
        Me.lblMMon.Name = "lblMMon"
        Me.lblMMon.Size = New System.Drawing.Size(50, 24)
        Me.lblMMon.TabIndex = 0
        Me.lblMMon.Text = "Mon"
        Me.lblMMon.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblMThu
        '
        Me.lblMThu.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblMThu.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lblMThu.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblMThu.Dock = DockStyle.Fill
        Me.lblMThu.Location = New System.Drawing.Point(171, 3)
        Me.lblMThu.Name = "lblMThu"
        Me.lblMThu.Size = New System.Drawing.Size(50, 24)
        Me.lblMThu.TabIndex = 3
        Me.lblMThu.Text = "Thurs"
        Me.lblMThu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblMSun
        '
        Me.lblMSun.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblMSun.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lblMSun.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblMSun.Dock = DockStyle.Fill
        Me.lblMSun.Location = New System.Drawing.Point(339, 3)
        Me.lblMSun.Name = "lblMSun"
        Me.lblMSun.Size = New System.Drawing.Size(57, 24)
        Me.lblMSun.TabIndex = 6
        Me.lblMSun.Text = "Sun"
        Me.lblMSun.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblMSat
        '
        Me.lblMSat.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblMSat.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lblMSat.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblMSat.Dock = DockStyle.Fill
        Me.lblMSat.Location = New System.Drawing.Point(283, 3)
        Me.lblMSat.Name = "lblMSat"
        Me.lblMSat.Size = New System.Drawing.Size(50, 24)
        Me.lblMSat.TabIndex = 5
        Me.lblMSat.Text = "Sat"
        Me.lblMSat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkMultiMon
        '
        Me.chkMultiMon.Dock = DockStyle.Fill
        Me.chkMultiMon.EnterMoveNextControl = True
        Me.chkMultiMon.Location = New System.Drawing.Point(3, 33)
        Me.chkMultiMon.Name = "chkMultiMon"
        Me.chkMultiMon.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkMultiMon.Properties.Appearance.Options.UseFont = True
        Me.chkMultiMon.Properties.AutoHeight = False
        Me.chkMultiMon.Properties.Caption = ""
        Me.chkMultiMon.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkMultiMon.Size = New System.Drawing.Size(50, 24)
        Me.chkMultiMon.TabIndex = 7
        '
        'CareLabel51
        '
        Me.CareLabel51.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel51.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel51.Location = New System.Drawing.Point(18, 14)
        Me.CareLabel51.Name = "CareLabel51"
        Me.CareLabel51.Size = New System.Drawing.Size(240, 15)
        Me.CareLabel51.TabIndex = 0
        Me.CareLabel51.Text = "Select the Sessions the Child will be attending"
        Me.CareLabel51.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel10
        '
        Me.Panel10.BorderStyle = BorderStyle.FixedSingle
        Me.Panel10.Controls.Add(Me.tc7)
        Me.Panel10.Controls.Add(Me.tc6)
        Me.Panel10.Controls.Add(Me.tc5)
        Me.Panel10.Controls.Add(Me.tc4)
        Me.Panel10.Controls.Add(Me.tc3)
        Me.Panel10.Controls.Add(Me.tc2)
        Me.Panel10.Controls.Add(Me.tc1)
        Me.Panel10.Controls.Add(Me.CareLabel58)
        Me.Panel10.Controls.Add(Me.CareLabel57)
        Me.Panel10.Controls.Add(Me.CareLabel56)
        Me.Panel10.Controls.Add(Me.CareLabel55)
        Me.Panel10.Controls.Add(Me.CareLabel54)
        Me.Panel10.Controls.Add(Me.CareLabel53)
        Me.Panel10.Controls.Add(Me.CareLabel52)
        Me.Panel10.Location = New System.Drawing.Point(18, 35)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(646, 200)
        Me.Panel10.TabIndex = 1
        '
        'tc7
        '
        Me.tc7.BoltOns = ""
        Me.tc7.ComboValue = Nothing
        Me.tc7.Location = New System.Drawing.Point(69, 168)
        Me.tc7.Name = "tc7"
        Me.tc7.PopulateMode = Nursery.TariffControl.EnumPopulateMode.Daily
        Me.tc7.SelectedIndex = -1
        Me.tc7.Size = New System.Drawing.Size(560, 20)
        Me.tc7.TabIndex = 13
        Me.tc7.Value1 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tc7.Value2 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tc7.ValueFEEE = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'tc6
        '
        Me.tc6.BoltOns = ""
        Me.tc6.ComboValue = Nothing
        Me.tc6.Location = New System.Drawing.Point(69, 142)
        Me.tc6.Name = "tc6"
        Me.tc6.PopulateMode = Nursery.TariffControl.EnumPopulateMode.Daily
        Me.tc6.SelectedIndex = -1
        Me.tc6.Size = New System.Drawing.Size(560, 20)
        Me.tc6.TabIndex = 11
        Me.tc6.Value1 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tc6.Value2 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tc6.ValueFEEE = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'tc5
        '
        Me.tc5.BoltOns = ""
        Me.tc5.ComboValue = Nothing
        Me.tc5.Location = New System.Drawing.Point(69, 116)
        Me.tc5.Name = "tc5"
        Me.tc5.PopulateMode = Nursery.TariffControl.EnumPopulateMode.Daily
        Me.tc5.SelectedIndex = -1
        Me.tc5.Size = New System.Drawing.Size(560, 20)
        Me.tc5.TabIndex = 9
        Me.tc5.Value1 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tc5.Value2 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tc5.ValueFEEE = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'tc4
        '
        Me.tc4.BoltOns = ""
        Me.tc4.ComboValue = Nothing
        Me.tc4.Location = New System.Drawing.Point(69, 90)
        Me.tc4.Name = "tc4"
        Me.tc4.PopulateMode = Nursery.TariffControl.EnumPopulateMode.Daily
        Me.tc4.SelectedIndex = -1
        Me.tc4.Size = New System.Drawing.Size(560, 20)
        Me.tc4.TabIndex = 7
        Me.tc4.Value1 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tc4.Value2 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tc4.ValueFEEE = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'tc3
        '
        Me.tc3.BoltOns = ""
        Me.tc3.ComboValue = Nothing
        Me.tc3.Location = New System.Drawing.Point(69, 64)
        Me.tc3.Name = "tc3"
        Me.tc3.PopulateMode = Nursery.TariffControl.EnumPopulateMode.Daily
        Me.tc3.SelectedIndex = -1
        Me.tc3.Size = New System.Drawing.Size(560, 20)
        Me.tc3.TabIndex = 5
        Me.tc3.Value1 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tc3.Value2 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tc3.ValueFEEE = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'tc2
        '
        Me.tc2.BoltOns = ""
        Me.tc2.ComboValue = Nothing
        Me.tc2.Location = New System.Drawing.Point(69, 38)
        Me.tc2.Name = "tc2"
        Me.tc2.PopulateMode = Nursery.TariffControl.EnumPopulateMode.Daily
        Me.tc2.SelectedIndex = -1
        Me.tc2.Size = New System.Drawing.Size(560, 20)
        Me.tc2.TabIndex = 3
        Me.tc2.Value1 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tc2.Value2 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tc2.ValueFEEE = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'tc1
        '
        Me.tc1.BoltOns = ""
        Me.tc1.ComboValue = Nothing
        Me.tc1.Location = New System.Drawing.Point(69, 12)
        Me.tc1.Name = "tc1"
        Me.tc1.PopulateMode = Nursery.TariffControl.EnumPopulateMode.Daily
        Me.tc1.SelectedIndex = -1
        Me.tc1.Size = New System.Drawing.Size(560, 20)
        Me.tc1.TabIndex = 1
        Me.tc1.Value1 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tc1.Value2 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tc1.ValueFEEE = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'CareLabel58
        '
        Me.CareLabel58.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel58.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel58.Location = New System.Drawing.Point(10, 170)
        Me.CareLabel58.Name = "CareLabel58"
        Me.CareLabel58.Size = New System.Drawing.Size(48, 15)
        Me.CareLabel58.TabIndex = 12
        Me.CareLabel58.Text = "Session 7"
        Me.CareLabel58.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel57
        '
        Me.CareLabel57.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel57.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel57.Location = New System.Drawing.Point(10, 144)
        Me.CareLabel57.Name = "CareLabel57"
        Me.CareLabel57.Size = New System.Drawing.Size(48, 15)
        Me.CareLabel57.TabIndex = 10
        Me.CareLabel57.Text = "Session 6"
        Me.CareLabel57.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel56
        '
        Me.CareLabel56.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel56.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel56.Location = New System.Drawing.Point(10, 118)
        Me.CareLabel56.Name = "CareLabel56"
        Me.CareLabel56.Size = New System.Drawing.Size(48, 15)
        Me.CareLabel56.TabIndex = 8
        Me.CareLabel56.Text = "Session 5"
        Me.CareLabel56.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel55
        '
        Me.CareLabel55.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel55.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel55.Location = New System.Drawing.Point(10, 92)
        Me.CareLabel55.Name = "CareLabel55"
        Me.CareLabel55.Size = New System.Drawing.Size(48, 15)
        Me.CareLabel55.TabIndex = 6
        Me.CareLabel55.Text = "Session 4"
        Me.CareLabel55.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel54
        '
        Me.CareLabel54.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel54.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel54.Location = New System.Drawing.Point(10, 66)
        Me.CareLabel54.Name = "CareLabel54"
        Me.CareLabel54.Size = New System.Drawing.Size(48, 15)
        Me.CareLabel54.TabIndex = 4
        Me.CareLabel54.Text = "Session 3"
        Me.CareLabel54.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel53
        '
        Me.CareLabel53.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel53.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel53.Location = New System.Drawing.Point(10, 40)
        Me.CareLabel53.Name = "CareLabel53"
        Me.CareLabel53.Size = New System.Drawing.Size(48, 15)
        Me.CareLabel53.TabIndex = 2
        Me.CareLabel53.Text = "Session 2"
        Me.CareLabel53.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel52
        '
        Me.CareLabel52.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel52.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel52.Location = New System.Drawing.Point(10, 15)
        Me.CareLabel52.Name = "CareLabel52"
        Me.CareLabel52.Size = New System.Drawing.Size(48, 15)
        Me.CareLabel52.TabIndex = 0
        Me.CareLabel52.Text = "Session 1"
        Me.CareLabel52.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkEmailInvoices2
        '
        Me.chkEmailInvoices2.EnterMoveNextControl = True
        Me.chkEmailInvoices2.Location = New System.Drawing.Point(366, 259)
        Me.chkEmailInvoices2.Name = "chkEmailInvoices2"
        Me.chkEmailInvoices2.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkEmailInvoices2.Properties.Appearance.Options.UseFont = True
        Me.chkEmailInvoices2.Properties.Caption = ""
        Me.chkEmailInvoices2.Size = New System.Drawing.Size(21, 19)
        Me.chkEmailInvoices2.TabIndex = 23
        Me.chkEmailInvoices2.TabStop = False
        '
        'CareLabel44
        '
        Me.CareLabel44.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel44.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel44.Location = New System.Drawing.Point(237, 261)
        Me.CareLabel44.Name = "CareLabel44"
        Me.CareLabel44.Size = New System.Drawing.Size(123, 15)
        Me.CareLabel44.TabIndex = 22
        Me.CareLabel44.Text = "Receives Email Invoices"
        Me.CareLabel44.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkEmailInvoices3
        '
        Me.chkEmailInvoices3.EnterMoveNextControl = True
        Me.chkEmailInvoices3.Location = New System.Drawing.Point(366, 257)
        Me.chkEmailInvoices3.Name = "chkEmailInvoices3"
        Me.chkEmailInvoices3.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkEmailInvoices3.Properties.Appearance.Options.UseFont = True
        Me.chkEmailInvoices3.Properties.Caption = ""
        Me.chkEmailInvoices3.Size = New System.Drawing.Size(21, 19)
        Me.chkEmailInvoices3.TabIndex = 25
        Me.chkEmailInvoices3.TabStop = False
        '
        'CareLabel45
        '
        Me.CareLabel45.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel45.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel45.Location = New System.Drawing.Point(237, 259)
        Me.CareLabel45.Name = "CareLabel45"
        Me.CareLabel45.Size = New System.Drawing.Size(123, 15)
        Me.CareLabel45.TabIndex = 24
        Me.CareLabel45.Text = "Receives Email Invoices"
        Me.CareLabel45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmWizardChild
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(740, 523)
        Me.ControlBox = False
        Me.Controls.Add(Me.WizardControl1)
        Me.Name = "frmWizardChild"
        Me.Text = ""
        CType(Me.WizardControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.WizardControl1.ResumeLayout(False)
        Me.wpWelcome.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wpPrimary.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.chkEInvoicing.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPriEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkPriSMS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPriSecret.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPriLandline.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPriMobile.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPriRelationship.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPriSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPriForename.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wpComplete.ResumeLayout(False)
        Me.wpComplete.PerformLayout()
        Me.wpAddress.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.txtAddressee.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wpContact2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.chk2Emergency.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chk2Collect.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chk2Parent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt2Email.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt2Landline.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkCont2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt2Secret.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt2Mobile.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt2Relationship.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt2Surname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt2Forename.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wpContact3.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.chk3Emergency.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chk3Collect.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chk3Parent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt3Email.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt3Landline.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkCont3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt3Secret.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt3Mobile.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt3Relationship.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt3Surname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt3Forename.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wpChild.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxChildKeyworker.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxChildGroup.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtChildStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtChildStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxChildGender.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtChildDOB.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtChildDOB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtChildSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtChildForename.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wpSessionsMode.ResumeLayout(False)
        Me.wpSessionsMode.PerformLayout()
        Me.Panel11.ResumeLayout(False)
        Me.Panel11.PerformLayout()
        CType(Me.cdtWC.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtWC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel6.ResumeLayout(False)
        CType(Me.radSkip.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radMultipleFullWeek.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radSingleWeekDays.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radMultiWeekDays.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radSingleFullWeek.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wpSessionSingle.ResumeLayout(False)
        Me.wpSessionSingle.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.chkSingleSun.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSingleSat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSingleFri.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSingleThu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSingleWed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSingleTue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSingleMon.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.wpSessionMultiple.ResumeLayout(False)
        Me.wpSessionMultiple.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        CType(Me.chkMultiSun.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkMultiSat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkMultiFri.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkMultiThu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkMultiWed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkMultiTue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkMultiMon.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        CType(Me.chkEmailInvoices2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkEmailInvoices3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents WizardControl1 As DevExpress.XtraWizard.WizardControl
    Friend WithEvents wpWelcome As DevExpress.XtraWizard.WelcomeWizardPage
    Friend WithEvents wpPrimary As DevExpress.XtraWizard.WizardPage
    Friend WithEvents wpComplete As DevExpress.XtraWizard.CompletionWizardPage
    Friend WithEvents wpAddress As DevExpress.XtraWizard.WizardPage
    Friend WithEvents Panel1 As Panel
    Friend WithEvents txtPriSecret As Care.Controls.CareTextBox
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents txtPriLandline As Care.Controls.CareTextBox
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents txtPriMobile As Care.Controls.CareTextBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents txtPriRelationship As Care.Controls.CareTextBox
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents txtPriSurname As Care.Controls.CareTextBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents txtPriForename As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents chkPriSMS As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents wpContact2 As DevExpress.XtraWizard.WizardPage
    Friend WithEvents chkEInvoicing As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel16 As Care.Controls.CareLabel
    Friend WithEvents txtPriEmail As Care.Controls.CareTextBox
    Friend WithEvents CareLabel15 As Care.Controls.CareLabel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents txtAddressee As Care.Controls.CareTextBox
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents Panel3 As Panel
    Friend WithEvents txt2Email As Care.Controls.CareTextBox
    Friend WithEvents CareLabel18 As Care.Controls.CareLabel
    Friend WithEvents chkCont2 As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel19 As Care.Controls.CareLabel
    Friend WithEvents txt2Secret As Care.Controls.CareTextBox
    Friend WithEvents CareLabel20 As Care.Controls.CareLabel
    Friend WithEvents txt2Landline As Care.Controls.CareTextBox
    Friend WithEvents CareLabel21 As Care.Controls.CareLabel
    Friend WithEvents txt2Mobile As Care.Controls.CareTextBox
    Friend WithEvents CareLabel22 As Care.Controls.CareLabel
    Friend WithEvents txt2Relationship As Care.Controls.CareTextBox
    Friend WithEvents CareLabel23 As Care.Controls.CareLabel
    Friend WithEvents txt2Surname As Care.Controls.CareTextBox
    Friend WithEvents CareLabel24 As Care.Controls.CareLabel
    Friend WithEvents txt2Forename As Care.Controls.CareTextBox
    Friend WithEvents CareLabel25 As Care.Controls.CareLabel
    Friend WithEvents wpContact3 As DevExpress.XtraWizard.WizardPage
    Friend WithEvents wpChild As DevExpress.XtraWizard.WizardPage
    Friend WithEvents CareLabel10 As Care.Controls.CareLabel
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents txtAddress As Care.Address.CareAddress
    Friend WithEvents Panel4 As Panel
    Friend WithEvents txt3Email As Care.Controls.CareTextBox
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents chkCont3 As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel12 As Care.Controls.CareLabel
    Friend WithEvents txt3Secret As Care.Controls.CareTextBox
    Friend WithEvents CareLabel13 As Care.Controls.CareLabel
    Friend WithEvents txt3Landline As Care.Controls.CareTextBox
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents txt3Mobile As Care.Controls.CareTextBox
    Friend WithEvents CareLabel17 As Care.Controls.CareLabel
    Friend WithEvents txt3Relationship As Care.Controls.CareTextBox
    Friend WithEvents CareLabel26 As Care.Controls.CareLabel
    Friend WithEvents txt3Surname As Care.Controls.CareTextBox
    Friend WithEvents CareLabel27 As Care.Controls.CareLabel
    Friend WithEvents txt3Forename As Care.Controls.CareTextBox
    Friend WithEvents CareLabel28 As Care.Controls.CareLabel
    Friend WithEvents Panel5 As Panel
    Friend WithEvents txtChildSurname As Care.Controls.CareTextBox
    Friend WithEvents CareLabel35 As Care.Controls.CareLabel
    Friend WithEvents txtChildForename As Care.Controls.CareTextBox
    Friend WithEvents CareLabel36 As Care.Controls.CareLabel
    Friend WithEvents Label12 As Care.Controls.CareLabel
    Friend WithEvents cbxChildGender As Care.Controls.CareComboBox
    Friend WithEvents lblStartDate As Care.Controls.CareLabel
    Friend WithEvents Label16 As Care.Controls.CareLabel
    Private WithEvents cdtChildDOB As Care.Controls.CareDateTime
    Friend WithEvents Label4 As Care.Controls.CareLabel
    Private WithEvents cdtChildStart As Care.Controls.CareDateTime
    Friend WithEvents lblDOB As Care.Controls.CareLabel
    Friend WithEvents cbxChildKeyworker As Care.Controls.CareComboBox
    Friend WithEvents cbxChildGroup As Care.Controls.CareComboBox
    Friend WithEvents Label13 As Care.Controls.CareLabel
    Friend WithEvents Label14 As Care.Controls.CareLabel
    Friend WithEvents wpSessionsMode As DevExpress.XtraWizard.WizardPage
    Friend WithEvents CareLabel30 As Care.Controls.CareLabel
    Friend WithEvents Panel6 As Panel
    Friend WithEvents radSkip As Care.Controls.CareRadioButton
    Friend WithEvents radMultipleFullWeek As Care.Controls.CareRadioButton
    Friend WithEvents radSingleWeekDays As Care.Controls.CareRadioButton
    Friend WithEvents radMultiWeekDays As Care.Controls.CareRadioButton
    Friend WithEvents radSingleFullWeek As Care.Controls.CareRadioButton
    Friend WithEvents CareLabel38 As Care.Controls.CareLabel
    Friend WithEvents lblBookings As Care.Controls.CareLabel
    Friend WithEvents lblTickSessions As Care.Controls.CareLabel
    Friend WithEvents lblTickChild As Care.Controls.CareLabel
    Friend WithEvents lblTickContacts As Care.Controls.CareLabel
    Friend WithEvents lblTickFamily As Care.Controls.CareLabel
    Friend WithEvents wpSessionSingle As DevExpress.XtraWizard.WizardPage
    Friend WithEvents wpSessionMultiple As DevExpress.XtraWizard.WizardPage
    Friend WithEvents Panel8 As Panel
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents CareLabel29 As Care.Controls.CareLabel
    Friend WithEvents Panel7 As Panel
    Friend WithEvents CareLabel42 As Care.Controls.CareLabel
    Friend WithEvents chkSingleSun As Care.Controls.CareCheckBox
    Friend WithEvents chkSingleSat As Care.Controls.CareCheckBox
    Friend WithEvents chkSingleFri As Care.Controls.CareCheckBox
    Friend WithEvents chkSingleThu As Care.Controls.CareCheckBox
    Friend WithEvents chkSingleWed As Care.Controls.CareCheckBox
    Friend WithEvents chkSingleTue As Care.Controls.CareCheckBox
    Friend WithEvents lblSFri As Care.Controls.CareLabel
    Friend WithEvents lblSWed As Care.Controls.CareLabel
    Friend WithEvents lblSTue As Care.Controls.CareLabel
    Friend WithEvents lblSMon As Care.Controls.CareLabel
    Friend WithEvents lblSThu As Care.Controls.CareLabel
    Friend WithEvents lblSSun As Care.Controls.CareLabel
    Friend WithEvents lblSSat As Care.Controls.CareLabel
    Friend WithEvents chkSingleMon As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel41 As Care.Controls.CareLabel
    Friend WithEvents CareLabel43 As Care.Controls.CareLabel
    Friend WithEvents Panel9 As Panel
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents chkMultiSun As Care.Controls.CareCheckBox
    Friend WithEvents chkMultiSat As Care.Controls.CareCheckBox
    Friend WithEvents chkMultiFri As Care.Controls.CareCheckBox
    Friend WithEvents chkMultiWed As Care.Controls.CareCheckBox
    Friend WithEvents chkMultiTue As Care.Controls.CareCheckBox
    Friend WithEvents lblMFri As Care.Controls.CareLabel
    Friend WithEvents lblMWed As Care.Controls.CareLabel
    Friend WithEvents lblMTue As Care.Controls.CareLabel
    Friend WithEvents lblMMon As Care.Controls.CareLabel
    Friend WithEvents lblMThu As Care.Controls.CareLabel
    Friend WithEvents lblMSun As Care.Controls.CareLabel
    Friend WithEvents lblMSat As Care.Controls.CareLabel
    Friend WithEvents chkMultiMon As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel51 As Care.Controls.CareLabel
    Friend WithEvents Panel10 As Panel
    Friend WithEvents CareLabel58 As Care.Controls.CareLabel
    Friend WithEvents CareLabel57 As Care.Controls.CareLabel
    Friend WithEvents CareLabel56 As Care.Controls.CareLabel
    Friend WithEvents CareLabel55 As Care.Controls.CareLabel
    Friend WithEvents CareLabel54 As Care.Controls.CareLabel
    Friend WithEvents CareLabel53 As Care.Controls.CareLabel
    Friend WithEvents CareLabel52 As Care.Controls.CareLabel
    Friend WithEvents lblTickBookings As Care.Controls.CareLabel
    Friend WithEvents chkMultiThu As Care.Controls.CareCheckBox
    Friend WithEvents tcSingle As Nursery.TariffControl
    Friend WithEvents tc7 As Nursery.TariffControl
    Friend WithEvents tc6 As Nursery.TariffControl
    Friend WithEvents tc5 As Nursery.TariffControl
    Friend WithEvents tc4 As Nursery.TariffControl
    Friend WithEvents tc3 As Nursery.TariffControl
    Friend WithEvents tc2 As Nursery.TariffControl
    Friend WithEvents tc1 As Nursery.TariffControl
    Friend WithEvents CareLabel59 As Care.Controls.CareLabel
    Friend WithEvents Panel11 As Panel
    Friend WithEvents CareLabel61 As Care.Controls.CareLabel
    Private WithEvents cdtWC As Care.Controls.CareDateTime
    Friend WithEvents CareLabel60 As Care.Controls.CareLabel
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
    Friend WithEvents CareLabel31 As Care.Controls.CareLabel
    Friend WithEvents chk2Emergency As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel34 As Care.Controls.CareLabel
    Friend WithEvents chk2Collect As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel33 As Care.Controls.CareLabel
    Friend WithEvents chk2Parent As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel32 As Care.Controls.CareLabel
    Friend WithEvents chk3Emergency As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel37 As Care.Controls.CareLabel
    Friend WithEvents chk3Collect As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel39 As Care.Controls.CareLabel
    Friend WithEvents chk3Parent As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel40 As Care.Controls.CareLabel
    Friend WithEvents chkEmailInvoices2 As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel44 As Care.Controls.CareLabel
    Friend WithEvents chkEmailInvoices3 As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel45 As Care.Controls.CareLabel

End Class
