﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTerms
    Inherits Care.Shared.frmGridMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtName = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.dtStarts = New Care.Controls.CareDateTime(Me.components)
        Me.dtEnds = New Care.Controls.CareDateTime(Me.components)
        Me.dtHTStarts = New Care.Controls.CareDateTime(Me.components)
        Me.dtHTEnds = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.cbxTermType = New Care.Controls.CareComboBox(Me.components)
        Me.gbx.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtStarts.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtStarts.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtEnds.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtEnds.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtHTStarts.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtHTStarts.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtHTEnds.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtHTEnds.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxTermType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbx
        '
        Me.gbx.Anchor = AnchorStyles.None
        Me.gbx.Controls.Add(Me.cbxTermType)
        Me.gbx.Controls.Add(Me.CareLabel6)
        Me.gbx.Controls.Add(Me.CareLabel5)
        Me.gbx.Controls.Add(Me.CareLabel4)
        Me.gbx.Controls.Add(Me.CareLabel3)
        Me.gbx.Controls.Add(Me.CareLabel2)
        Me.gbx.Controls.Add(Me.dtHTEnds)
        Me.gbx.Controls.Add(Me.dtHTStarts)
        Me.gbx.Controls.Add(Me.dtEnds)
        Me.gbx.Controls.Add(Me.dtStarts)
        Me.gbx.Controls.Add(Me.txtName)
        Me.gbx.Controls.Add(Me.CareLabel1)
        Me.gbx.Location = New System.Drawing.Point(165, 84)
        Me.gbx.Size = New System.Drawing.Size(441, 219)
        Me.gbx.TabIndex = 1
        Me.gbx.Controls.SetChildIndex(Me.Panel2, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel1, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtName, 0)
        Me.gbx.Controls.SetChildIndex(Me.dtStarts, 0)
        Me.gbx.Controls.SetChildIndex(Me.dtEnds, 0)
        Me.gbx.Controls.SetChildIndex(Me.dtHTStarts, 0)
        Me.gbx.Controls.SetChildIndex(Me.dtHTEnds, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel2, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel3, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel4, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel5, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel6, 0)
        Me.gbx.Controls.SetChildIndex(Me.cbxTermType, 0)
        '
        'Panel2
        '
        Me.Panel2.Location = New System.Drawing.Point(3, 188)
        Me.Panel2.Size = New System.Drawing.Size(435, 28)
        Me.Panel2.TabIndex = 12
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(250, 0)
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(341, 0)
        '
        'Grid
        '
        Me.Grid.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Grid.Appearance.Options.UseFont = True
        Me.Grid.Size = New System.Drawing.Size(752, 349)
        Me.Grid.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Size = New System.Drawing.Size(771, 35)
        '
        'btnDelete
        '
        Me.btnDelete.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Appearance.Options.UseFont = True
        '
        'btnEdit
        '
        Me.btnEdit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.Appearance.Options.UseFont = True
        '
        'btnAdd
        '
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.Appearance.Options.UseFont = True
        '
        'btnClose
        '
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(2074, 5)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'txtName
        '
        Me.txtName.CharacterCasing = CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(122, 19)
        Me.txtName.MaxLength = 30
        Me.txtName.Name = "txtName"
        Me.txtName.NumericAllowNegatives = False
        Me.txtName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AccessibleName = "Term Name"
        Me.txtName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtName.Properties.MaxLength = 30
        Me.txtName.ReadOnly = False
        Me.txtName.Size = New System.Drawing.Size(306, 20)
        Me.txtName.TabIndex = 1
        Me.txtName.Tag = "AEM"
        Me.txtName.TextAlign = HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(15, 22)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Name"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtStarts
        '
        Me.dtStarts.EditValue = Nothing
        Me.dtStarts.EnterMoveNextControl = True
        Me.dtStarts.Location = New System.Drawing.Point(122, 75)
        Me.dtStarts.Name = "dtStarts"
        Me.dtStarts.Properties.AccessibleName = "Term Start Date"
        Me.dtStarts.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.dtStarts.Properties.Appearance.Options.UseFont = True
        Me.dtStarts.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtStarts.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtStarts.ReadOnly = False
        Me.dtStarts.Size = New System.Drawing.Size(100, 20)
        Me.dtStarts.TabIndex = 5
        Me.dtStarts.Tag = "AE"
        Me.dtStarts.Value = Nothing
        '
        'dtEnds
        '
        Me.dtEnds.EditValue = Nothing
        Me.dtEnds.EnterMoveNextControl = True
        Me.dtEnds.Location = New System.Drawing.Point(122, 103)
        Me.dtEnds.Name = "dtEnds"
        Me.dtEnds.Properties.AccessibleName = "Term End Date"
        Me.dtEnds.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.dtEnds.Properties.Appearance.Options.UseFont = True
        Me.dtEnds.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtEnds.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtEnds.ReadOnly = False
        Me.dtEnds.Size = New System.Drawing.Size(100, 20)
        Me.dtEnds.TabIndex = 7
        Me.dtEnds.Tag = "AE"
        Me.dtEnds.Value = Nothing
        '
        'dtHTStarts
        '
        Me.dtHTStarts.EditValue = Nothing
        Me.dtHTStarts.EnterMoveNextControl = True
        Me.dtHTStarts.Location = New System.Drawing.Point(122, 131)
        Me.dtHTStarts.Name = "dtHTStarts"
        Me.dtHTStarts.Properties.AccessibleName = "Half Term Start Date"
        Me.dtHTStarts.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.dtHTStarts.Properties.Appearance.Options.UseFont = True
        Me.dtHTStarts.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtHTStarts.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtHTStarts.ReadOnly = False
        Me.dtHTStarts.Size = New System.Drawing.Size(100, 20)
        Me.dtHTStarts.TabIndex = 9
        Me.dtHTStarts.Tag = "AE"
        Me.dtHTStarts.Value = Nothing
        '
        'dtHTEnds
        '
        Me.dtHTEnds.EditValue = Nothing
        Me.dtHTEnds.EnterMoveNextControl = True
        Me.dtHTEnds.Location = New System.Drawing.Point(122, 159)
        Me.dtHTEnds.Name = "dtHTEnds"
        Me.dtHTEnds.Properties.AccessibleName = "Half Term End Date"
        Me.dtHTEnds.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.dtHTEnds.Properties.Appearance.Options.UseFont = True
        Me.dtHTEnds.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtHTEnds.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtHTEnds.ReadOnly = False
        Me.dtHTEnds.Size = New System.Drawing.Size(100, 20)
        Me.dtHTEnds.TabIndex = 11
        Me.dtHTEnds.Tag = "AE"
        Me.dtHTEnds.Value = Nothing
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(15, 78)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(60, 15)
        Me.CareLabel2.TabIndex = 4
        Me.CareLabel2.Text = "Term Starts"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(15, 106)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(56, 15)
        Me.CareLabel3.TabIndex = 6
        Me.CareLabel3.Text = "Term Ends"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(15, 134)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(85, 15)
        Me.CareLabel4.TabIndex = 8
        Me.CareLabel4.Text = "Half Term Starts"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(15, 162)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(81, 15)
        Me.CareLabel5.TabIndex = 10
        Me.CareLabel5.Text = "Half Term Ends"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(15, 50)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(57, 15)
        Me.CareLabel6.TabIndex = 2
        Me.CareLabel6.Text = "Term Type"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxTermType
        '
        Me.cbxTermType.AllowBlank = False
        Me.cbxTermType.DataSource = Nothing
        Me.cbxTermType.DisplayMember = Nothing
        Me.cbxTermType.EnterMoveNextControl = True
        Me.cbxTermType.Location = New System.Drawing.Point(122, 47)
        Me.cbxTermType.Name = "cbxTermType"
        Me.cbxTermType.Properties.AccessibleName = "Term Type"
        Me.cbxTermType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxTermType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxTermType.Properties.Appearance.Options.UseFont = True
        Me.cbxTermType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxTermType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxTermType.ReadOnly = False
        Me.cbxTermType.SelectedValue = Nothing
        Me.cbxTermType.Size = New System.Drawing.Size(150, 20)
        Me.cbxTermType.TabIndex = 3
        Me.cbxTermType.Tag = "AEM"
        Me.cbxTermType.ValueMember = Nothing
        '
        'frmTerms
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(771, 402)
        Me.Name = "frmTerms"
        Me.gbx.ResumeLayout(False)
        Me.gbx.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtStarts.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtStarts.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtEnds.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtEnds.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtHTStarts.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtHTStarts.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtHTEnds.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtHTEnds.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxTermType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dtHTEnds As Care.Controls.CareDateTime
    Friend WithEvents dtHTStarts As Care.Controls.CareDateTime
    Friend WithEvents dtEnds As Care.Controls.CareDateTime
    Friend WithEvents dtStarts As Care.Controls.CareDateTime
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents cbxTermType As Care.Controls.CareComboBox

End Class
