﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBookingsBuild
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnSpaces = New Care.Controls.CareButton(Me.components)
        Me.btnBookings = New Care.Controls.CareButton(Me.components)
        Me.GroupControl1 = New Care.Controls.CareFrame(Me.components)
        Me.grdTasks = New Care.Controls.CareGrid()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'btnSpaces
        '
        Me.btnSpaces.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnSpaces.Location = New System.Drawing.Point(137, 300)
        Me.btnSpaces.Name = "btnSpaces"
        Me.btnSpaces.Size = New System.Drawing.Size(119, 23)
        Me.btnSpaces.TabIndex = 4
        Me.btnSpaces.Text = "Initialise Spaces"
        '
        'btnBookings
        '
        Me.btnBookings.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnBookings.Location = New System.Drawing.Point(12, 300)
        Me.btnBookings.Name = "btnBookings"
        Me.btnBookings.Size = New System.Drawing.Size(119, 23)
        Me.btnBookings.TabIndex = 3
        Me.btnBookings.Text = "Build Bookings"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.grdTasks)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(243, 282)
        Me.GroupControl1.TabIndex = 7
        Me.GroupControl1.Text = "Scheduled Tasks"
        '
        'grdTasks
        '
        Me.grdTasks.AllowBuildColumns = True
        Me.grdTasks.AllowEdit = False
        Me.grdTasks.AllowHorizontalScroll = False
        Me.grdTasks.AllowMultiSelect = False
        Me.grdTasks.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.grdTasks.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdTasks.Appearance.Options.UseFont = True
        Me.grdTasks.AutoSizeByData = True
        Me.grdTasks.DisableAutoSize = False
        Me.grdTasks.DisableDataFormatting = False
        Me.grdTasks.FocusedRowHandle = -2147483648
        Me.grdTasks.HideFirstColumn = False
        Me.grdTasks.Location = New System.Drawing.Point(5, 23)
        Me.grdTasks.Name = "grdTasks"
        Me.grdTasks.PreviewColumn = ""
        Me.grdTasks.QueryID = Nothing
        Me.grdTasks.RowAutoHeight = False
        Me.grdTasks.SearchAsYouType = True
        Me.grdTasks.ShowAutoFilterRow = False
        Me.grdTasks.ShowFindPanel = False
        Me.grdTasks.ShowGroupByBox = False
        Me.grdTasks.ShowLoadingPanel = False
        Me.grdTasks.ShowNavigator = False
        Me.grdTasks.Size = New System.Drawing.Size(233, 254)
        Me.grdTasks.TabIndex = 7
        '
        'frmBookingsBuild
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(267, 332)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.btnSpaces)
        Me.Controls.Add(Me.btnBookings)
        Me.FormBorderStyle = FormBorderStyle.FixedToolWindow
        Me.Margin = New Padding(3, 5, 3, 5)
        Me.Name = "frmBookingsBuild"
        Me.Text = "frmBookingsBuild"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnBookings As Care.Controls.CareButton
    Friend WithEvents btnSpaces As Care.Controls.CareButton
    Friend WithEvents GroupControl1 As Care.Controls.CareFrame
    Friend WithEvents grdTasks As Care.Controls.CareGrid
End Class
