﻿

Imports Care.Global
Imports Care.Shared
Imports System.Windows.Forms

Public Class frmWizardChild


    Private m_SkipMandatory As Boolean = False '<<< used for developers to skip validation
    Private m_StartAgain As Boolean = False

    Private m_FamilyID As Guid? = Nothing
    Private m_ChildID As Guid? = Nothing
    Private m_SiteID As Guid? = Nothing

    Private Sub frmWizardChild_Load(sender As Object, e As EventArgs) Handles Me.Load

        With cbxChildGender
            .AddItem("Male", "M")
            .AddItem("Female", "F")
        End With

        With cbxChildKeyworker
            .PopulateWithSQL(Session.ConnectionString, "select id, fullname from staff where status = 'C' and keyworker = 1 order by fullname")
        End With

        With cbxSite
            .PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)
        End With

        If ParameterHandler.ReturnString("DEFCONTACT") <> "" Then wpPrimary.Visible = False

        Clear()

    End Sub

    Private Sub frmWizardChild_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        txtPriForename.Focus()
    End Sub

    Private Function Finish() As Boolean

        If Not SaveFamily() Then Return False
        lblTickFamily.Show()
        Application.DoEvents()

        If Not SaveContacts() Then Return False
        lblTickContacts.Show()
        Application.DoEvents()

        If Not SaveChild() Then Return False
        lblTickChild.Show()
        Application.DoEvents()

        If Not SaveSessions() Then Return False
        lblTickSessions.Show()
        Application.DoEvents()

        lblBookings.Show()
        Session.CursorWaiting()
        Application.DoEvents()

        Dim _B As New BookingsEngine
        _B.BuildBookings(m_ChildID.Value, cdtChildStart.Value.Value, Bookings.CalendarCutOff)

        lblTickBookings.Show()
        Application.DoEvents()

        Return True

    End Function

    Private Function SaveSessions() As Boolean
        If radSingleWeekDays.Checked Or radSingleFullWeek.Checked Then
            Return SaveSingleSession()
        Else
            Return SaveMultiSession()
        End If
    End Function

    Private Function SaveSingleSession() As Boolean

        Dim _S As New Business.ChildAdvanceSession
        With _S

            ._ChildId = m_ChildID
            ._DateFrom = cdtWC.Value.Value
            ._Mode = "Weekly"
            ._PatternType = "Recurring"

            ._NoRepeat = False
            ._RecurringScope = 1
            ._RecurringScopeDesc = "Yes, All the time"

            If chkSingleMon.Checked Then
                ._Monday = tcSingle.ComboValue
                ._MondayDesc = tcSingle.Description
                ._Monday1 = tcSingle.Value1
                ._Monday2 = tcSingle.Value2
                ._MondayFund = tcSingle.ValueFEEE
                ._MondayBoid = tcSingle.BoltOns
                ._MondayBo = tcSingle.BoltOnNames
            End If

            If chkSingleTue.Checked Then
                ._Tuesday = tcSingle.ComboValue
                ._TuesdayDesc = tcSingle.Description
                ._Tuesday1 = tcSingle.Value1
                ._Tuesday2 = tcSingle.Value2
                ._TuesdayFund = tcSingle.ValueFEEE
                ._TuesdayBoid = tcSingle.BoltOns
                ._TuesdayBo = tcSingle.BoltOnNames
            End If

            If chkSingleWed.Checked Then
                ._Wednesday = tcSingle.ComboValue
                ._WednesdayDesc = tcSingle.Description
                ._Wednesday1 = tcSingle.Value1
                ._Wednesday2 = tcSingle.Value2
                ._WednesdayFund = tcSingle.ValueFEEE
                ._WednesdayBoid = tcSingle.BoltOns
                ._WednesdayBo = tcSingle.BoltOnNames
            End If

            If chkSingleThu.Checked Then
                ._Thursday = tcSingle.ComboValue
                ._ThursdayDesc = tcSingle.Description
                ._Thursday1 = tcSingle.Value1
                ._Thursday2 = tcSingle.Value2
                ._ThursdayFund = tcSingle.ValueFEEE
                ._ThursdayBoid = tcSingle.BoltOns
                ._ThursdayBo = tcSingle.BoltOnNames
            End If

            If chkSingleFri.Checked Then
                ._Friday = tcSingle.ComboValue
                ._FridayDesc = tcSingle.Description
                ._Friday1 = tcSingle.Value1
                ._Friday2 = tcSingle.Value2
                ._FridayFund = tcSingle.ValueFEEE
                ._FridayBoid = tcSingle.BoltOns
                ._FridayBo = tcSingle.BoltOnNames
            End If

            If chkSingleSat.Checked Then
                ._Saturday = tcSingle.ComboValue
                ._SaturdayDesc = tcSingle.Description
                ._Saturday1 = tcSingle.Value1
                ._Saturday2 = tcSingle.Value2
                ._SaturdayFund = tcSingle.ValueFEEE
                ._SaturdayBoid = tcSingle.BoltOns
                ._SaturdayBo = tcSingle.BoltOnNames
            End If

            If chkSingleSun.Checked Then
                ._Sunday = tcSingle.ComboValue
                ._SundayDesc = tcSingle.Description
                ._Sunday1 = tcSingle.Value1
                ._Sunday2 = tcSingle.Value2
                ._SundayFund = tcSingle.ValueFEEE
                ._SundayBoid = tcSingle.BoltOns
                ._SundayBo = tcSingle.BoltOnNames
            End If

            ._Activated = False
            .Store()

        End With

        Return True

    End Function

    Private Sub CreateSession(ByVal DateFrom As Date)

        Dim _S As New Business.ChildAdvanceSession
        With _S

            ._ChildId = m_ChildID
            ._DateFrom = DateFrom
            ._Mode = "Daily"
            ._PatternType = "Recurring"

            ._NoRepeat = False
            ._RecurringScope = 1
            ._RecurringScopeDesc = "Yes, All the time"

            If tc1.ComboValue.HasValue Then
                ._Monday = tc1.ComboValue
                ._MondayDesc = tc1.Description
                ._Monday1 = tc1.Value1
                ._Monday2 = tc1.Value2
                ._MondayFund = tc1.ValueFEEE
                ._MondayBoid = tc1.BoltOns
                ._MondayBo = tc1.BoltOnNames
            End If

            If tc2.ComboValue.HasValue Then
                ._Tuesday = tc2.ComboValue
                ._TuesdayDesc = tc2.Description
                ._Tuesday1 = tc2.Value1
                ._Tuesday2 = tc2.Value2
                ._TuesdayFund = tc2.ValueFEEE
                ._TuesdayBoid = tc2.BoltOns
                ._TuesdayBo = tc2.BoltOnNames
            End If

            If tc3.ComboValue.HasValue Then
                ._Wednesday = tc3.ComboValue
                ._WednesdayDesc = tc3.Description
                ._Wednesday1 = tc3.Value1
                ._Wednesday2 = tc3.Value2
                ._WednesdayFund = tc3.ValueFEEE
                ._WednesdayBoid = tc3.BoltOns
                ._WednesdayBo = tc3.BoltOnNames
            End If

            If tc4.ComboValue.HasValue Then
                ._Thursday = tc4.ComboValue
                ._ThursdayDesc = tc4.Description
                ._Thursday1 = tc4.Value1
                ._Thursday2 = tc4.Value2
                ._ThursdayFund = tc4.ValueFEEE
                ._ThursdayBoid = tc4.BoltOns
                ._ThursdayBo = tc4.BoltOnNames
            End If

            If tc5.ComboValue.HasValue Then
                ._Friday = tc5.ComboValue
                ._FridayDesc = tc5.Description
                ._Friday1 = tc5.Value1
                ._Friday2 = tc5.Value2
                ._FridayFund = tc5.ValueFEEE
                ._FridayBoid = tc5.BoltOns
                ._FridayBo = tc5.BoltOnNames
            End If

            If tc6.ComboValue.HasValue Then
                ._Saturday = tc6.ComboValue
                ._SaturdayDesc = tc6.Description
                ._Saturday1 = tc6.Value1
                ._Saturday2 = tc6.Value2
                ._SaturdayFund = tc6.ValueFEEE
                ._SaturdayBoid = tc6.BoltOns
                ._SaturdayBo = tc6.BoltOnNames
            End If

            If tc7.ComboValue.HasValue Then
                ._Sunday = tc7.ComboValue
                ._SundayDesc = tc7.Description
                ._Sunday1 = tc7.Value1
                ._Sunday2 = tc7.Value2
                ._SundayFund = tc7.ValueFEEE
                ._SundayBoid = tc7.BoltOns
                ._SundayBo = tc7.BoltOnNames
            End If

            ._Activated = False
            .Store()

        End With

    End Sub

    Private Function SaveMultiSession() As Boolean

        Dim _Date As Date = cdtWC.Value.Value

        If chkMultiMon.Checked Then CreateSession(_Date)
        _Date = _Date.AddDays(1)

        If chkMultiTue.Checked Then CreateSession(_Date)
        _Date = _Date.AddDays(1)

        If chkMultiWed.Checked Then CreateSession(_Date)
        _Date = _Date.AddDays(1)

        If chkMultiThu.Checked Then CreateSession(_Date)
        _Date = _Date.AddDays(1)

        If chkMultiFri.Checked Then CreateSession(_Date)
        _Date = _Date.AddDays(1)

        If chkMultiSat.Checked Then CreateSession(_Date)
        _Date = _Date.AddDays(1)

        If chkMultiSun.Checked Then CreateSession(_Date)

        Return True

    End Function

    Private Function SaveFamily() As Boolean

        Dim _F As New Business.Family
        With _F

            ._Surname = txtPriSurname.Text
            ._LetterName = txtAddressee.Text
            ._eInvoicing = chkEInvoicing.Checked
            ._Address = txtAddress.Text
            ._Postcode = txtAddress.PostCode

            ._OpeningBalance = 0
            ._Balance = 0

            ._SiteId = New Guid(cbxSite.SelectedValue.ToString())
            ._SiteName = cbxSite.Text

            .Store()

            m_FamilyID = _F._ID

        End With

        _F = Nothing

        Return True

    End Function

    Private Function SaveContacts() As Boolean


        If ParameterHandler.ReturnString("DEFCONTACT") = "" Then
            If Not CreateContactRecord(True, txtPriForename.Text, txtPriSurname.Text, txtPriRelationship.Text,
                            txtPriMobile.Text, chkPriSMS.Checked, txtPriLandline.Text, txtPriEmail.Text, txtPriSecret.Text,
                            True, True, True, True) Then Return False
        End If

        If chkCont2.Checked Then

            If Not CreateContactRecord(False, txt2Forename.Text, txt2Surname.Text, txt2Relationship.Text, _
                                txt2Mobile.Text, False, txt2Landline.Text, txt2Email.Text, txt2Secret.Text, _
                                chk2Parent.Checked, chk2Collect.Checked, chk2Emergency.Checked, chkEmailInvoices2.Checked) Then Return False

        End If

        If chkCont3.Checked Then

            If Not CreateContactRecord(False, txt3Forename.Text, txt3Surname.Text, txt3Relationship.Text, _
                                txt3Mobile.Text, False, txt3Landline.Text, txt3Email.Text, txt3Secret.Text, _
                                chk3Parent.Checked, chk3Collect.Checked, chk3Emergency.Checked, chkEmailInvoices3.Checked) Then Return False

        End If

        Return True

    End Function

    Private Function SaveChild() As Boolean

        Dim _C As New Business.Child
        Dim skipWaitingList As Boolean = ParameterHandler.ReturnBoolean("SKIPWL")

        With _C

            ._FamilyId = m_FamilyID
            ._FamilyName = txtPriSurname.Text

            If cdtChildStart.Value <= Date.Today OrElse skipWaitingList Then
                ._Status = "Current"
            Else
                ._Status = "On Waiting List"
            End If

            ._Forename = txtChildForename.Text
            ._Surname = txtChildSurname.Text
            ._Fullname = ._Forename + " " + ._Surname

            ._Dob = cdtChildDOB.Value
            ._Gender = cbxChildGender.Text

            ._Started = cdtChildStart.Value
            ._GroupStarted = ._Started

            If cbxChildGroup.SelectedIndex >= 0 Then
                ._GroupId = New Guid(CType(cbxChildGroup.EditValue, Care.Controls.CareComboBox.ComboItem).Value)
                ._GroupName = CType(cbxChildGroup.EditValue, Care.Controls.CareComboBox.ComboItem).Text
            Else
                ._GroupId = Nothing
                ._GroupName = ""
            End If

            If cbxChildKeyworker.SelectedIndex >= 0 Then
                ._KeyworkerId = New Guid(CType(cbxChildKeyworker.EditValue, Care.Controls.CareComboBox.ComboItem).Value)
                ._KeyworkerName = CType(cbxChildKeyworker.EditValue, Care.Controls.CareComboBox.ComboItem).Text
            Else
                ._KeyworkerId = Nothing
                ._KeyworkerName = ""
            End If

            ._SiteId = New Guid(cbxSite.SelectedValue.ToString())
            ._SiteName = cbxSite.Text

            .Store()

            m_ChildID = ._ID

        End With

        Return True

    End Function

    Private Function CreateContactRecord(ByVal Primary As Boolean, ByVal Forename As String, ByVal Surname As String, ByVal Rel As String, _
                                         ByVal Mobile As String, ByVal SMS As Boolean, ByVal Landline As String, ByVal Email As String, ByVal Password As String, _
                                         ByVal ParentResp As Boolean, ByVal Collect As Boolean, ByVal Emergency As Boolean, ByVal EInvoices As Boolean) As Boolean

        Dim _C As New Business.Contact
        With _C

            ._FamilyId = m_FamilyID

            ._Surname = Surname
            ._Forename = Forename
            ._Fullname = Forename + " " + Surname
            ._Relationship = Rel

            ._PrimaryCont = Primary
            ._EmerCont = Emergency
            ._Parent = ParentResp
            ._Collect = Collect

            If Primary Then
                ._Address = txtAddress.Text
                ._Postcode = txtAddress.PostCode
            End If

            ._TelHome = Landline
            ._TelMobile = Mobile
            ._Sms = SMS
            ._Email = Email

            ._Password = Password

            ._InvoiceEmail = False

            If Email <> "" Then
                ._Report = True
                ._ReportDetail = "S"
                ._InvoiceEmail = EInvoices
            End If

            .Store()

        End With

        _C = Nothing

        Return True

    End Function

    Private Sub Clear()

        m_FamilyID = Nothing
        m_ChildID = Nothing

        txtPriForename.Text = ""
        txtPriSurname.Text = ""
        txtPriRelationship.Text = ""
        txtPriMobile.Text = ""
        chkPriSMS.Checked = False
        txtPriLandline.Text = ""
        txtPriEmail.Text = ""
        chkEInvoicing.Checked = True
        txtPriSecret.Text = ""

        txtAddress.Clear()
        txtAddressee.Text = ""

        chkCont2.Checked = False
        ToggleContact2(False)

        txt2Forename.Text = ""
        txt2Surname.Text = ""
        txt2Relationship.Text = ""
        txt2Mobile.Text = ""
        txt2Landline.Text = ""
        txt2Email.Text = ""
        txt2Secret.Text = ""
        chk2Collect.Checked = False
        chk2Emergency.Checked = False
        chk2Parent.Checked = False

        chkCont3.Checked = False
        ToggleContact3(False)

        txt3Forename.Text = ""
        txt3Surname.Text = ""
        txt3Relationship.Text = ""
        txt3Mobile.Text = ""
        txt3Landline.Text = ""
        txt3Email.Text = ""
        txt3Secret.Text = ""
        chk3Collect.Checked = False
        chk3Emergency.Checked = False
        chk3Parent.Checked = False

        txtChildForename.Text = ""
        txtChildSurname.Text = ""
        cbxChildGender.SelectedIndex = -1
        cdtChildDOB.Value = Nothing
        lblDOB.Hide()
        cdtChildStart.Value = Nothing
        lblStartDate.Hide()
        cbxSite.SelectedIndex = -1
        cbxChildGroup.SelectedIndex = -1
        cbxChildKeyworker.SelectedIndex = -1

        radSkip.Checked = True

        tcSingle.Clear()
        chkSingleMon.Checked = False
        chkSingleTue.Checked = False
        chkSingleWed.Checked = False
        chkSingleThu.Checked = False
        chkSingleFri.Checked = False
        chkSingleSat.Checked = False
        chkSingleSun.Checked = False

        tc1.Clear()
        tc2.Clear()
        tc3.Clear()
        tc4.Clear()
        tc5.Clear()
        tc6.Clear()
        tc7.Clear()

        chkMultiMon.Checked = False
        chkMultiTue.Checked = False
        chkMultiWed.Checked = False
        chkMultiThu.Checked = False
        chkMultiFri.Checked = False
        chkMultiSat.Checked = False
        chkMultiSun.Checked = False

        lblTickFamily.Hide()
        lblTickContacts.Hide()
        lblTickChild.Hide()
        lblTickSessions.Hide()
        lblBookings.Hide()
        lblTickBookings.Hide()

    End Sub

    Private Sub WizardControl1_CancelClick(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles WizardControl1.CancelClick
        Me.Close()
    End Sub

    Private Sub WizardControl1_FinishClick(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles WizardControl1.FinishClick

        If Finish() Then

            If CareMessage("Family Created Successfully. Do you want to start again?", MessageBoxIcon.Question, MessageBoxButtons.YesNo, "Completed") = DialogResult.Yes Then
                Clear()
                m_StartAgain = True
                WizardControl1.SelectedPage = wpPrimary
                txtPriForename.Focus()
                m_StartAgain = False
            Else
                Me.Close()
            End If

        End If

    End Sub

    Private Sub WizardControl1_SelectedPageChanged(sender As Object, e As DevExpress.XtraWizard.WizardPageChangedEventArgs) Handles WizardControl1.SelectedPageChanged

        Select Case e.Page.Name

            Case "wpPrimary"
                txtPriForename.Focus()

            Case "wpAddress"
                If txtAddressee.Text = "" Then txtAddressee.Text = txtPriForename.Text + " " + txtPriSurname.Text
                txtAddress.Focus()

            Case "wpContact2"
                If chkCont2.Checked Then txt2Forename.Focus()

            Case "wpContact3"
                If chkCont3.Checked Then txt3Forename.Focus()

            Case "wpChild"
                txtChildSurname.Text = txtPriSurname.Text
                txtChildForename.Focus()

            Case "wpSessionsMode"
                'default the week commencing date, based upon the child start date
                cdtWC.Value = ValueHandler.NearestDate(cdtChildStart.Value.Value, DayOfWeek.Monday, ValueHandler.EnumDirection.Backwards)

            Case "wpSessionSingle"

                If radSingleFullWeek.Checked Then
                    chkSingleMon.Checked = True
                    chkSingleTue.Checked = True
                    chkSingleWed.Checked = True
                    chkSingleThu.Checked = True
                    chkSingleFri.Checked = True
                    chkSingleSat.Checked = False
                    chkSingleSun.Checked = False
                Else
                    chkSingleMon.Checked = False
                    chkSingleTue.Checked = False
                    chkSingleWed.Checked = False
                    chkSingleThu.Checked = False
                    chkSingleFri.Checked = False
                    chkSingleSat.Checked = False
                    chkSingleSun.Checked = False
                End If

                SetTariffCombos(e.Page.Name)

            Case "wpSessionMultiple"
                If radMultipleFullWeek.Checked Then
                    chkMultiMon.Checked = True
                    chkMultiTue.Checked = True
                    chkMultiWed.Checked = True
                    chkMultiThu.Checked = True
                    chkMultiFri.Checked = True
                    chkMultiSat.Checked = False
                    chkMultiSun.Checked = False
                Else
                    chkMultiMon.Checked = False
                    chkMultiTue.Checked = False
                    chkMultiWed.Checked = False
                    chkMultiThu.Checked = False
                    chkMultiFri.Checked = False
                    chkMultiSat.Checked = False
                    chkMultiSun.Checked = False
                End If

                SetTariffCombos(e.Page.Name)

            Case "wpComplete"
                Session.CursorWaiting()
                Application.DoEvents()

        End Select

    End Sub

    Private Sub WizardControl1_SelectedPageChanging(sender As Object, e As DevExpress.XtraWizard.WizardPageChangingEventArgs) Handles WizardControl1.SelectedPageChanging

        If e.Direction = DevExpress.XtraWizard.Direction.Forward Then

            Select Case e.PrevPage.Name

                Case "wpPrimary"
                    If Not CheckTextBox(txtPriForename, "Primary Contact Forename") Then e.Cancel = True : Exit Sub
                    If Not CheckTextBox(txtPriSurname, "Primary Contact Surname") Then e.Cancel = True : Exit Sub
                    If Not CheckTextBox(txtPriRelationship, "Primary Contact Relationship") Then e.Cancel = True : Exit Sub

                Case "wpAddress"
                    If Not CheckAddress(txtAddress, "Address") Then e.Cancel = True : Exit Sub
                    If Not CheckTextBox(txtAddressee, "Addressee") Then e.Cancel = True : Exit Sub

                Case "wpContact2"
                    If chkCont2.Checked Then
                        If Not CheckTextBox(txt2Forename, "Second Contact Forename") Then e.Cancel = True : Exit Sub
                        If Not CheckTextBox(txt2Surname, "Second Contact Surname") Then e.Cancel = True : Exit Sub
                        If Not CheckTextBox(txt2Relationship, "Second Contact Relationship") Then e.Cancel = True : Exit Sub
                    End If

                Case "wpContact3"
                    If chkCont3.Checked Then
                        If Not CheckTextBox(txt3Forename, "Third Contact Forename") Then e.Cancel = True : Exit Sub
                        If Not CheckTextBox(txt3Surname, "Third Contact Surname") Then e.Cancel = True : Exit Sub
                        If Not CheckTextBox(txt3Relationship, "Third Contact Relationship") Then e.Cancel = True : Exit Sub
                    End If

                Case "wpChild"
                    If Not CheckTextBox(txtChildForename, "Child Forename") Then e.Cancel = True : Exit Sub
                    If Not CheckTextBox(txtChildSurname, "Child Surname") Then e.Cancel = True : Exit Sub
                    If Not CheckCombo(cbxChildGender, "Child Gender") Then e.Cancel = True : Exit Sub
                    If Not CheckDate(cdtChildDOB, "Child Date of Birth") Then e.Cancel = True : Exit Sub
                    If Not CheckDate(cdtChildStart, "Child Start Date") Then e.Cancel = True : Exit Sub
                    If Not CheckCombo(cbxSite, "Child Site") Then e.Cancel = True : Exit Sub
                    If Not CheckCombo(cbxChildGroup, "Child Group") Then e.Cancel = True : Exit Sub
                    If Not CheckCombo(cbxChildKeyworker, "Child Keyworker") Then e.Cancel = True : Exit Sub

                Case "wpSessionsMode"

                    If cdtWC.Value.HasValue Then
                        If cdtWC.Value.Value.DayOfWeek <> DayOfWeek.Monday Then
                            CareMessage("The session start date must be a Monday.", MessageBoxIcon.Exclamation, "Date Validation")
                            e.Cancel = True
                            Exit Sub
                        End If
                    Else
                        CareMessage("Please enter a Week Commencing Date.", MessageBoxIcon.Exclamation, "Date Validation")
                        e.Cancel = True
                        Exit Sub
                    End If

                    If radSkip.Checked Then
                        e.Page = wpComplete
                    Else
                        If radSingleFullWeek.Checked = True OrElse radSingleWeekDays.Checked = True Then
                            e.Page = wpSessionSingle
                        Else
                            e.Page = wpSessionMultiple
                        End If
                    End If

                Case "wpSessionSingle"

                    If Not tcSingle.ComboValue.HasValue Then
                        CareMessage("The session cannot be blank. Please enter a value before continuing...", MessageBoxIcon.Exclamation, "Mandatory Field")
                        e.Cancel = True
                        Exit Sub
                    Else

                        'ensure at least one day has been selected
                        Dim _Checked As Boolean = False

                        If chkSingleMon.Checked Then _Checked = True
                        If chkSingleTue.Checked Then _Checked = True
                        If chkSingleWed.Checked Then _Checked = True
                        If chkSingleThu.Checked Then _Checked = True
                        If chkSingleFri.Checked Then _Checked = True
                        If chkSingleSat.Checked Then _Checked = True
                        If chkSingleSun.Checked Then _Checked = True

                        If Not _Checked Then
                            CareMessage("Please select at least one day the Child attends...", MessageBoxIcon.Exclamation, "No Days")
                            e.Cancel = True
                            Exit Sub
                        End If

                    End If

                    e.Page = wpComplete

                Case "wpSessionMultiple"

                    'ensure at least one session has been selected
                    Dim _Selected As Boolean = False
                    If tc1.ComboValue.HasValue Then _Selected = True
                    If tc2.ComboValue.HasValue Then _Selected = True
                    If tc3.ComboValue.HasValue Then _Selected = True
                    If tc4.ComboValue.HasValue Then _Selected = True
                    If tc5.ComboValue.HasValue Then _Selected = True
                    If tc6.ComboValue.HasValue Then _Selected = True
                    If tc7.ComboValue.HasValue Then _Selected = True

                    If Not _Selected Then
                        CareMessage("Please select at least one session...", MessageBoxIcon.Exclamation, "No Sessions")
                        e.Cancel = True
                        Exit Sub
                    End If

                    'ensure at least one day has been selected
                    Dim _Checked As Boolean = False

                    If chkMultiMon.Checked Then _Checked = True
                    If chkMultiTue.Checked Then _Checked = True
                    If chkMultiWed.Checked Then _Checked = True
                    If chkMultiThu.Checked Then _Checked = True
                    If chkMultiFri.Checked Then _Checked = True
                    If chkMultiSat.Checked Then _Checked = True
                    If chkMultiSun.Checked Then _Checked = True

                    If Not _Checked Then
                        CareMessage("Please select at least one day the Child attends...", MessageBoxIcon.Exclamation, "No Days")
                        e.Cancel = True
                        Exit Sub
                    End If

                    e.Page = wpComplete

            End Select

        Else

            'backwards
            Select Case e.PrevPage.Name

                Case "wpSessionSingle", "wpSessionMultiple"
                    e.Page = wpSessionsMode

                Case "wpComplete"

                    If Not m_StartAgain Then
                        If radSkip.Checked Then
                            e.Page = wpSessionsMode
                        Else
                            If radSingleFullWeek.Checked = True OrElse radSingleWeekDays.Checked = True Then
                                e.Page = wpSessionSingle
                            Else
                                e.Page = wpSessionMultiple
                            End If
                        End If
                    End If

            End Select

        End If

    End Sub

    Private Function CheckTextBox(ByRef TextBoxIn As Care.Controls.CareTextBox, ByVal FriendlyName As String) As Boolean
        If m_SkipMandatory Then Return True
        If TextBoxIn.Text = "" Then
            CareMessage(FriendlyName + " cannot be blank. Please enter a value before continuing...", MessageBoxIcon.Exclamation, "Mandatory Field")
            TextBoxIn.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Private Function CheckAddress(ByRef TextBoxIn As Care.Address.CareAddress, ByVal FriendlyName As String) As Boolean
        If m_SkipMandatory Then Return True
        If TextBoxIn.Text = "" Then
            CareMessage(FriendlyName + " cannot be blank. Please enter a value before continuing...", MessageBoxIcon.Exclamation, "Mandatory Field")
            TextBoxIn.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Private Function CheckDate(ByRef TextBoxIn As Care.Controls.CareDateTime, ByVal FriendlyName As String) As Boolean
        If m_SkipMandatory Then Return True
        If TextBoxIn.Value.HasValue = False Then
            CareMessage(FriendlyName + " cannot be blank. Please enter a value before continuing...", MessageBoxIcon.Exclamation, "Mandatory Field")
            TextBoxIn.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Private Function CheckCombo(ByRef TextBoxIn As Care.Controls.CareComboBox, ByVal FriendlyName As String) As Boolean
        If m_SkipMandatory Then Return True
        If TextBoxIn.SelectedIndex < 0 Then
            CareMessage(FriendlyName + " cannot be blank. Please enter a value before continuing...", MessageBoxIcon.Exclamation, "Mandatory Field")
            TextBoxIn.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub cdtChildDOB_Validated(sender As Object, e As EventArgs) Handles cdtChildDOB.Validated
        If cdtChildDOB.Value.HasValue Then
            lblDOB.Text = ValueHandler.DateDifferenceAsText(cdtChildDOB.Value.Value, Date.Today)
            lblDOB.Show()
        Else
            lblDOB.Hide()
        End If
    End Sub

    Private Sub ToggleContact2(ByVal Enabled As Boolean)

        If Enabled Then
            txt2Surname.Text = txtPriSurname.Text
        Else
            txt2Forename.Text = ""
            txt2Surname.Text = ""
            txt2Relationship.Text = ""
            txt2Mobile.Text = ""
            txt2Landline.Text = ""
            txt2Email.Text = ""
            txt2Secret.Text = ""
            chk2Collect.Checked = False
            chk2Emergency.Checked = False
            chk2Parent.Checked = False
        End If

        txt2Forename.Enabled = Enabled
        txt2Surname.Enabled = Enabled
        txt2Relationship.Enabled = Enabled
        txt2Mobile.Enabled = Enabled
        txt2Landline.Enabled = Enabled
        txt2Email.Enabled = Enabled
        txt2Secret.Enabled = Enabled

    End Sub

    Private Sub ToggleContact3(ByVal Enabled As Boolean)

        If Enabled Then
            txt3Surname.Text = txtPriSurname.Text
        Else
            txt3Forename.Text = ""
            txt3Surname.Text = ""
            txt3Relationship.Text = ""
            txt3Mobile.Text = ""
            txt3Landline.Text = ""
            txt3Email.Text = ""
            txt3Secret.Text = ""
            chk3Collect.Checked = False
            chk3Emergency.Checked = False
            chk3Parent.Checked = False
        End If

        txt3Forename.Enabled = Enabled
        txt3Surname.Enabled = Enabled
        txt3Relationship.Enabled = Enabled
        txt3Mobile.Enabled = Enabled
        txt3Landline.Enabled = Enabled
        txt3Email.Enabled = Enabled
        txt3Secret.Enabled = Enabled

    End Sub

    Private Sub chkCont2_CheckedChanged(sender As Object, e As EventArgs) Handles chkCont2.CheckedChanged
        ToggleContact2(chkCont2.Checked)
    End Sub

    Private Sub chkCont3_CheckedChanged(sender As Object, e As EventArgs) Handles chkCont3.CheckedChanged
        ToggleContact3(chkCont3.Checked)
    End Sub

    Private Sub cbxSite_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSite.SelectedIndexChanged
        If cbxSite.Visible Then
            cbxChildGroup.PopulateWithSQL(Session.ConnectionString, Business.RoomRatios.RetreiveSiteRoomSQL(cbxSite.SelectedValue.ToString))
            m_SiteID = New Guid(cbxSite.SelectedValue.ToString)
        End If
    End Sub

    Private Sub SetTariffCombos(ByVal PageName As String)

        Select Case PageName

            Case "wpSessionSingle"
                tcSingle.SiteID = m_SiteID.Value

            Case "wpSessionMultiple"
                tc1.SiteID = m_SiteID.Value
                tc2.SiteID = m_SiteID.Value
                tc3.SiteID = m_SiteID.Value
                tc4.SiteID = m_SiteID.Value
                tc5.SiteID = m_SiteID.Value
                tc6.SiteID = m_SiteID.Value
                tc7.SiteID = m_SiteID.Value

        End Select

    End Sub

End Class
