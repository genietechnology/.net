﻿Imports Care.Global
Imports Care.Data
Imports Care.Shared
Imports System.Windows.Forms

Public Class frmRollingMenus

    Private Enum EnumMealType
        Breakfast
        SnackAM
        Lunch
        LunchDessert
        SnackPM
        Tea
        TeaDessert
    End Enum

    Private m_Season As Business.MenuSeason
    Private m_Days As New List(Of String)
    Private m_WeekID As Guid? = Nothing
    Private m_DayID As Guid? = Nothing

    Private Sub frmSeasons_Load(sender As Object, e As EventArgs) Handles Me.Load
        gbxMeals.Hide()
    End Sub

    Private Sub DisplayMeals()

        If Not m_WeekID.HasValue Then Exit Sub

        Dim _SQL As String = ""

        _SQL += "select d.ID, DATENAME(weekday, d.day) as 'Day',"
        _SQL += " d.breakfast, d.breakfast_name,"
        _SQL += " d.snack_am, d.snack_am_name,"
        _SQL += " d.lunch, d.lunch_name,"
        _SQL += " d.lunch_dessert, d.lunch_dessert_name,"
        _SQL += " d.snack_pm, d.snack_pm_name,"
        _SQL += " d.tea, d.tea_name,"
        _SQL += " d.tea_dessert, d.tea_dessert_name"
        _SQL += " from MenuWeek w"
        _SQL += " left join MenuDay d on d.season_id = w.season_id and d.week_id = w.ID"
        _SQL += " where w.season_id = '" + m_Season._ID.Value.ToString + "'"
        _SQL += " and week_id = '" + m_WeekID.Value.ToString + "'"
        _SQL += " order by d.day"

        grdDays.HideFirstColumn = True
        grdDays.Populate(Session.ConnectionString, _SQL)

        If grdDays.Columns.Count > 0 Then
            grdDays.Columns("breakfast").Visible = False
            grdDays.Columns("breakfast_name").Caption = "Breakfast"
            grdDays.Columns("snack_am").Visible = False
            grdDays.Columns("snack_am_name").Caption = "Snack"
            grdDays.Columns("lunch").Visible = False
            grdDays.Columns("lunch_name").Caption = "Lunch"
            grdDays.Columns("lunch_dessert").Visible = False
            grdDays.Columns("lunch_dessert_name").Caption = "Lunch Dessert"
            grdDays.Columns("snack_pm").Visible = False
            grdDays.Columns("snack_pm_name").Caption = "PM Snack"
            grdDays.Columns("tea").Visible = False
            grdDays.Columns("tea_name").Caption = "Tea"
            grdDays.Columns("tea_dessert").Visible = False
            grdDays.Columns("tea_dessert_name").Caption = "Tea Dessert"
        End If

    End Sub

    Private Sub DisplayRecord()
        DisplayWeeks()
    End Sub

    Private Sub DisplayWeeks()

        Dim _SQL As String = ""
        _SQL += "select ID, week as 'Week' from MenuWeek"
        _SQL += " where season_id = '" + m_Season._ID.Value.ToString + "'"
        _SQL += " order by week"

        grdWeeks.HideFirstColumn = True
        grdWeeks.Populate(Session.ConnectionString, _SQL)

        m_DayID = Nothing
        If grdWeeks.RecordCount > 0 Then grdWeeks.MoveFirst()

    End Sub

#Region "Overrides"

    Protected Overrides Sub SetBindings()

        m_Season = New Business.MenuSeason
        bs.DataSource = m_Season

        txtName.DataBindings.Add("Text", bs, "_Name")
        cdtStart.DataBindings.Add("Value", bs, "_DateFrom", True)

    End Sub

    Protected Overrides Sub AfterAcceptChanges()
        gbxWeeks.Enabled = True
        gbxDays.Enabled = True
        gbxMeals.Hide()
    End Sub

    Protected Overrides Sub AfterCancelChanges()
        gbxWeeks.Enabled = True
        gbxDays.Enabled = True
        gbxMeals.Hide()
    End Sub

    Protected Overrides Sub AfterAdd()
        gbxWeeks.Enabled = False
        gbxDays.Enabled = False
        gbxMeals.Enabled = False
    End Sub

    Protected Overrides Function BeforeDelete() As Boolean
        If InputBox("Type 'Delete' to Continue", "Delete Menu Season").ToUpper = "DELETE" Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Overrides Function BeforeCommitUpdate() As Boolean
        If gbxMeals.Visible Then Return False
        Return True
    End Function

    Protected Overrides Function BeforeCancelChanges() As Boolean
        If gbxMeals.Visible Then Return False
        Return True
    End Function

    Protected Overrides Sub CommitDelete()

        Dim _SQL As String = ""

        _SQL = "delete from MenuSeason where ID = '" + m_Season._ID.Value.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from MenuWeek where season_id = '" + m_Season._ID.Value.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from MenuDay where season_id  = '" + m_Season._ID.Value.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

    End Sub

    Protected Overrides Sub CommitUpdate()

        m_Season = CType(bs.Item(bs.Position), Business.MenuSeason)
        m_Season._Weeks = CByte(ReturnWeekCount())
        m_Season.Store()

        If Mode = "ADD" Then CreateWeek()
        DisplayRecord()

    End Sub

    Protected Overrides Sub FindRecord()

        Dim _Find As New GenericFind

        Dim _SQL As String = "select date_from as 'From', name as 'Name' from MenuSeason"

        With _Find
            .Caption = "Find Season"
            .ParentForm = ParentForm
            .PopulateOnLoad = True
            .ConnectionString = Session.ConnectionString
            .GridSQL = _SQL
            .GridOrderBy = "order by date_from, name"
            .ReturnField = "ID"
            .Show()
        End With

        If _Find.ReturnValue <> "" Then

            MyBase.RecordID = New Guid(_Find.ReturnValue)
            MyBase.RecordPopulated = True

            m_Season = Business.MenuSeason.RetreiveByID(New Guid(_Find.ReturnValue))
            bs.DataSource = m_Season

            DisplayRecord()

        End If

    End Sub

#End Region

    Private Function ReturnWeekCount() As Integer
        Dim _SQL As String = "select count(*) from MenuWeek where season_id = '" + m_Season._ID.Value.ToString + "'"
        Dim _i As Integer = ValueHandler.ConvertInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))
        Return _i
    End Function

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        If CareMessage("Add another week?", MessageBoxIcon.Question, MessageBoxButtons.YesNo, "Add Week") = DialogResult.Yes Then
            CreateWeek()
            DisplayWeeks()
            grdWeeks.MoveFirst()
        End If
    End Sub

    Private Function ReturnNextWeek(ByVal SeasonID As Guid) As Byte
        Dim _SQL As String = "select max(week) as 'last_week' from MenuWeek where season_id = '" + SeasonID.ToString + "'"
        Dim _i As Integer = ValueHandler.ConvertInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))
        _i += 1
        Return CByte(_i)
    End Function

    Private Sub CreateWeek()

        Dim _Week As New Business.MenuWeek
        With _Week

            ._SeasonId = m_Season._ID
            ._Week = ReturnNextWeek(._SeasonId.Value)

            CreateBlankDay(._SeasonId, ._ID, 0)
            CreateBlankDay(._SeasonId, ._ID, 1)
            CreateBlankDay(._SeasonId, ._ID, 2)
            CreateBlankDay(._SeasonId, ._ID, 3)
            CreateBlankDay(._SeasonId, ._ID, 4)
            CreateBlankDay(._SeasonId, ._ID, 5)
            CreateBlankDay(._SeasonId, ._ID, 6)

            .Store()

        End With

        m_WeekID = _Week._ID.Value

    End Sub

    Private Function CreateBlankDay(ByVal SeasonID As Guid?, ByVal WeekID As Guid?, ByVal WeekDay As Integer) As Guid

        Dim _Day As New Business.MenuDay
        With _Day
            ._SeasonId = SeasonID
            ._WeekId = WeekID
            ._Day = CByte(WeekDay)
            .Store()
        End With

        Return _Day._ID.Value

    End Function

    Private Function SafeGUID(ByVal ValueIn As Object) As Guid?
        If ValueIn Is Nothing Then
            Return Nothing
        Else
            If IsDBNull(ValueIn) Then
                Return Nothing
            Else
                Try
                    Return New Guid(ValueIn.ToString)
                Catch ex As Exception
                    Return Nothing
                End Try
            End If
        End If
    End Function

    Private Sub grdWeeks_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles grdWeeks.FocusedRowChanged

        If e Is Nothing Then Exit Sub
        If e.FocusedRowHandle < 0 Then Exit Sub

        m_WeekID = New Guid(grdWeeks.GetRowCellValue(e.FocusedRowHandle, "ID").ToString)
        DisplayMeals()

    End Sub

#Region "Meal Buttons"

    Private Sub btnSelectBreakfastSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectBreakfast.Click
        Dim _ReturnValue As String = Business.Meal.FindMeal(Me, Business.Meal.EnumMealType.Breakfast)
        If _ReturnValue <> "" Then
            Dim _Food As Business.Meal = Business.Meal.RetreiveByID(New Guid(_ReturnValue))
            txtBreakfast.AccessibleName = _ReturnValue
            txtBreakfast.Text = _Food._Name
            _Food = Nothing
        End If
    End Sub

    Private Sub btnClearBreakfast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearBreakfast.Click
        If CareMessage("Clear breakfast details?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Clear") = MsgBoxResult.Yes Then
            txtBreakfast.AccessibleName = ""
            txtBreakfast.Text = ""
        End If
    End Sub

    Private Sub btnSelectSnack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectSnack.Click
        Dim _ReturnValue As String = Business.Meal.FindMeal(Me, Business.Meal.EnumMealType.Snack)
        If _ReturnValue <> "" Then
            Dim _Food As Business.Meal = Business.Meal.RetreiveByID(New Guid(_ReturnValue))
            txtSnack.AccessibleName = _ReturnValue
            txtSnack.Text = _Food._Name
            _Food = Nothing
        End If
    End Sub

    Private Sub btnSelectLunch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectLunch.Click
        Dim _ReturnValue As String = Business.Meal.FindMeal(Me, Business.Meal.EnumMealType.Lunch)
        If _ReturnValue <> "" Then
            Dim _Food As Business.Meal = Business.Meal.RetreiveByID(New Guid(_ReturnValue))
            txtLunch.AccessibleName = _ReturnValue
            txtLunch.Text = _Food._Name
            _Food = Nothing
        End If
    End Sub

    Private Sub btnSelectTea_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectTea.Click
        Dim _ReturnValue As String = Business.Meal.FindMeal(Me, Business.Meal.EnumMealType.Tea)
        If _ReturnValue <> "" Then
            Dim _Food As Business.Meal = Business.Meal.RetreiveByID(New Guid(_ReturnValue))
            txtTea.AccessibleName = _ReturnValue
            txtTea.Text = _Food._Name
            _Food = Nothing
        End If
    End Sub

    Private Sub btnClearSnack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearSnack.Click
        If CareMessage("Clear snack details?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Clear") = MsgBoxResult.Yes Then
            txtSnack.AccessibleName = ""
            txtSnack.Text = ""
        End If
    End Sub

    Private Sub btnSelectPMSnack_Click(sender As Object, e As EventArgs) Handles btnSelectPMSnack.Click
        Dim _ReturnValue As String = Business.Meal.FindMeal(Me, Business.Meal.EnumMealType.Snack)
        If _ReturnValue <> "" Then
            Dim _Food As Business.Meal = Business.Meal.RetreiveByID(New Guid(_ReturnValue))
            txtPMSnack.AccessibleName = _ReturnValue
            txtPMSnack.Text = _Food._Name
            _Food = Nothing
        End If
    End Sub

    Private Sub btnClearPMSnack_Click(sender As Object, e As EventArgs) Handles btnClearPMSnack.Click
        If CareMessage("Clear snack details?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Clear") = MsgBoxResult.Yes Then
            txtPMSnack.AccessibleName = ""
            txtPMSnack.Text = ""
        End If
    End Sub

    Private Sub btnClearLunch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearLunch.Click
        If CareMessage("Clear lunch details?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Clear") = MsgBoxResult.Yes Then
            txtLunch.AccessibleName = ""
            txtLunch.Text = ""
        End If
    End Sub

    Private Sub btnClearTea_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearTea.Click
        If CareMessage("Clear tea details?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Clear") = MsgBoxResult.Yes Then
            txtTea.AccessibleName = ""
            txtTea.Text = ""
        End If
    End Sub

    Private Sub btnClearLunchDessert_Click(sender As System.Object, e As System.EventArgs) Handles btnClearLunchDessert.Click
        If CareMessage("Clear lunch dessert details?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Clear") = MsgBoxResult.Yes Then
            txtLunchDessert.AccessibleName = ""
            txtLunchDessert.Text = ""
        End If
    End Sub

    Private Sub btnClearTeaDessert_Click(sender As System.Object, e As System.EventArgs) Handles btnClearTeaDessert.Click
        If CareMessage("Clear tea dessert details?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Clear") = MsgBoxResult.Yes Then
            txtTeaDessert.AccessibleName = ""
            txtTeaDessert.Text = ""
        End If
    End Sub

    Private Sub btnSelectLunchDessert_Click(sender As System.Object, e As System.EventArgs) Handles btnSelectLunchDessert.Click
        Dim _ReturnValue As String = Business.Meal.FindMeal(Me, Business.Meal.EnumMealType.Dessert)
        If _ReturnValue <> "" Then
            Dim _Food As Business.Meal = Business.Meal.RetreiveByID(New Guid(_ReturnValue))
            txtLunchDessert.AccessibleName = _ReturnValue
            txtLunchDessert.Text = _Food._Name
            _Food = Nothing
        End If
    End Sub

    Private Sub btnSelectTeaDessert_Click(sender As System.Object, e As System.EventArgs) Handles btnSelectTeaDessert.Click
        Dim _ReturnValue As String = Business.Meal.FindMeal(Me, Business.Meal.EnumMealType.Dessert)
        If _ReturnValue <> "" Then
            Dim _Food As Business.Meal = Business.Meal.RetreiveByID(New Guid(_ReturnValue))
            txtTeaDessert.AccessibleName = _ReturnValue
            txtTeaDessert.Text = _Food._Name
            _Food = Nothing
        End If
    End Sub

#End Region

    Private Sub grdDays_GridDoubleClick(sender As Object, e As EventArgs) Handles grdDays.GridDoubleClick

        If Mode <> "EDIT" Then Exit Sub

        grdDays.Enabled = False
        m_DayID = New Guid(grdDays.GetRowCellValue(grdDays.RowIndex, "ID").ToString)

        txtBreakfast.Properties.AccessibleName = grdDays.GetRowCellValue(grdDays.RowIndex, "breakfast").ToString
        txtBreakfast.Text = grdDays.GetRowCellValue(grdDays.RowIndex, "breakfast_name").ToString

        txtSnack.Properties.AccessibleName = grdDays.GetRowCellValue(grdDays.RowIndex, "snack_am").ToString
        txtSnack.Text = grdDays.GetRowCellValue(grdDays.RowIndex, "snack_am_name").ToString

        txtLunch.Properties.AccessibleName = grdDays.GetRowCellValue(grdDays.RowIndex, "lunch").ToString
        txtLunch.Text = grdDays.GetRowCellValue(grdDays.RowIndex, "lunch_name").ToString

        txtLunchDessert.Properties.AccessibleName = grdDays.GetRowCellValue(grdDays.RowIndex, "lunch_dessert").ToString
        txtLunchDessert.Text = grdDays.GetRowCellValue(grdDays.RowIndex, "lunch_dessert_name").ToString

        txtPMSnack.Properties.AccessibleName = grdDays.GetRowCellValue(grdDays.RowIndex, "snack_pm").ToString
        txtPMSnack.Text = grdDays.GetRowCellValue(grdDays.RowIndex, "snack_pm_name").ToString

        txtTea.Properties.AccessibleName = grdDays.GetRowCellValue(grdDays.RowIndex, "tea").ToString
        txtTea.Text = grdDays.GetRowCellValue(grdDays.RowIndex, "tea_name").ToString

        txtTeaDessert.Properties.AccessibleName = grdDays.GetRowCellValue(grdDays.RowIndex, "tea_dessert").ToString
        txtTeaDessert.Text = grdDays.GetRowCellValue(grdDays.RowIndex, "tea_dessert_name").ToString

        gbxMeals.Show()

    End Sub

    Private Sub btnDayCancel_Click(sender As Object, e As EventArgs) Handles btnDayCancel.Click
        grdDays.Enabled = True
        gbxMeals.Hide()
    End Sub

    Private Sub btnDayOK_Click(sender As Object, e As EventArgs) Handles btnDayOK.Click

        Dim _Day As Business.MenuDay = Business.MenuDay.RetreiveByID(m_DayID.Value)
        With _Day

            ._Breakfast = SafeGUID(txtBreakfast.Properties.AccessibleName)
            ._BreakfastName = txtBreakfast.Text

            ._SnackAm = SafeGUID(txtSnack.Properties.AccessibleName)
            ._SnackAmName = txtSnack.Text

            ._Lunch = SafeGUID(txtLunch.Properties.AccessibleName)
            ._LunchName = txtLunch.Text

            ._LunchDessert = SafeGUID(txtLunchDessert.Properties.AccessibleName)
            ._LunchDessertName = txtLunchDessert.Text

            ._SnackPm = SafeGUID(txtPMSnack.Properties.AccessibleName)
            ._SnackPmName = txtPMSnack.Text

            ._Tea = SafeGUID(txtTea.Properties.AccessibleName)
            ._TeaName = txtTea.Text

            ._TeaDessert = SafeGUID(txtTeaDessert.Properties.AccessibleName)
            ._TeaDessertName = txtTeaDessert.Text

            .Store()

        End With

        grdDays.Enabled = True

        DisplayMeals()
        gbxMeals.Hide()

    End Sub

    Private Sub DuplicateMeals(ByVal MealTextBox As Care.Controls.CareTextBox, ByVal MealType As EnumMealType)

        Dim _MealTypeName As String = ""
        Dim _FieldID As String = ""
        Dim _FieldName As String = ""

        Select Case MealType

            Case EnumMealType.Breakfast
                _FieldID = "breakfast"
                _MealTypeName = "Breakfast"

            Case EnumMealType.SnackAM
                _FieldID = "snack_am"
                _MealTypeName = "AM Snack"

            Case EnumMealType.Lunch
                _FieldID = "lunch"
                _MealTypeName = "Lunch"

            Case EnumMealType.LunchDessert
                _FieldID = "lunch_dessert"
                _MealTypeName = "Lunch Dessert"

            Case EnumMealType.SnackPM
                _FieldID = "snack_pm"
                _MealTypeName = "PM Snack"

            Case EnumMealType.Tea
                _FieldID = "tea"
                _MealTypeName = "Tea"

            Case EnumMealType.TeaDessert
                _FieldID = "tea_dessert"
                _MealTypeName = "Tea Dessert"

        End Select

        _FieldName = _FieldID
        _FieldName += "_name"

        If CareMessage("Are you sure you want to duplicate " + MealTextBox.Text + " for the whole week for " + _MealTypeName + "?", MessageBoxIcon.Question, MessageBoxButtons.YesNo, "Duplicate Meal") = DialogResult.Yes Then

            Dim _SQL As String = ""

            _SQL += "update MenuDay set " + _FieldID + " = '" + MealTextBox.Properties.AccessibleName.ToString + "',"
            _SQL += " " + _FieldName + " = '" + MealTextBox.Text + "'"
            _SQL += " where season_id = '" + m_Season._ID.Value.ToString + "'"
            _SQL += " and week_id = '" + m_WeekID.Value.ToString + "'"

            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

            grdDays.Enabled = True
            DisplayMeals()
            gbxMeals.Hide()

        End If
    End Sub

    Private Sub btnDupTeaDessert_Click(sender As Object, e As EventArgs) Handles btnDupTeaDessert.Click
        DuplicateMeals(txtTeaDessert, EnumMealType.TeaDessert)
    End Sub

    Private Sub btnDupBreakfast_Click(sender As Object, e As EventArgs) Handles btnDupBreakfast.Click
        DuplicateMeals(txtBreakfast, EnumMealType.Breakfast)
    End Sub

    Private Sub btnDupSnack_Click(sender As Object, e As EventArgs) Handles btnDupSnack.Click
        DuplicateMeals(txtSnack, EnumMealType.SnackAM)
    End Sub

    Private Sub btnDupLunch_Click(sender As Object, e As EventArgs) Handles btnDupLunch.Click
        DuplicateMeals(txtLunch, EnumMealType.Lunch)
    End Sub

    Private Sub btnDupLunchDessert_Click(sender As Object, e As EventArgs) Handles btnDupLunchDessert.Click
        DuplicateMeals(txtLunchDessert, EnumMealType.LunchDessert)
    End Sub

    Private Sub btnDupTea_Click(sender As Object, e As EventArgs) Handles btnDupTea.Click
        DuplicateMeals(txtTea, EnumMealType.Tea)
    End Sub

    Private Sub btnDupPMSnack_Click(sender As Object, e As EventArgs) Handles btnDupPMSnack.Click
        DuplicateMeals(txtPMSnack, EnumMealType.SnackPM)
    End Sub

    Private Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click

        If CareMessage("Are you sure you want to Delete the selected Week?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Delete Week") = DialogResult.Yes Then

            Dim _SQL As String = ""

            _SQL = "delete from MenuDay where week_id = '" + m_WeekID.Value.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

            _SQL = "delete from MenuWeek where ID = '" + m_WeekID.Value.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

            DisplayWeeks()

        End If

    End Sub

End Class
