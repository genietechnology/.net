﻿Imports Care.Global

Public Class frmStaffQual

    Private m_StaffID As Guid
    Private m_RecordID As Guid?
    Private m_IsNew As Boolean = True

    Public Sub New(ByVal StaffID As Guid, ByVal RecordID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        m_StaffID = StaffID

        If RecordID.HasValue Then
            m_IsNew = False
            m_RecordID = RecordID
        Else
            m_IsNew = True
            m_RecordID = Nothing
        End If

    End Sub

    Private Sub frmChildCharge_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cdtDate.BackColor = Session.ChangeColour
        txtName.BackColor = Session.ChangeColour
        cdExpires.BackColor = Session.ChangeColour
        txtScore.BackColor = Session.ChangeColour

        If m_RecordID.HasValue Then
            Me.Text = "Edit Qualification Record"
            DisplayRecord()
        Else
            Me.Text = "Create New Qualification Record"
        End If

    End Sub

    Private Sub frmSession_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        cdtDate.Focus()
    End Sub

    Private Sub DisplayRecord()

        Dim _C As Business.StaffQual = Business.StaffQual.RetreiveByID(m_RecordID.Value)
        With _C
            cdtDate.Value = ._QualDate
            txtName.Text = ._QualName
            cdExpires.Value = ._QualExpires
            txtScore.Text = ValueHandler.MoneyAsText(_C._QualScore)
        End With

        _C = Nothing

    End Sub

    Private Function ValidateEntry() As Boolean
        Return MyControls.Validate(Me.Controls)
    End Function

    Private Sub SaveAndExit()

        If Not ValidateEntry() Then Exit Sub

        Dim _C As Business.StaffQual = Nothing

        If m_IsNew Then
            _C = New Business.StaffQual
            _C._ID = Guid.NewGuid
            _C._StaffId = m_StaffID
        Else
            _C = Business.StaffQual.RetreiveByID(m_RecordID.Value)
        End If

        With _C
            ._QualDate = cdtDate.Value
            ._QualName = txtName.Text
            ._QualExpires = cdExpires.Value
            ._QualScore = ValueHandler.ConvertDecimal(txtScore.Text)
            .Store()
        End With

        _C = Nothing

        Me.DialogResult = DialogResult.OK
        Me.Close()

    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        SaveAndExit()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub
End Class
