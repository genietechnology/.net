﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFinancialsPost
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFinancialsPost))
        Me.cgDocs = New Care.Controls.CareGrid()
        Me.GroupControl1 = New Care.Controls.CareFrame(Me.components)
        Me.txtRemaining = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.btnRetry = New Care.Controls.CareButton(Me.components)
        Me.btnPrint = New Care.Controls.CareButton(Me.components)
        Me.txtOrange = New Care.Controls.CareTextBox(Me.components)
        Me.txtRed = New Care.Controls.CareTextBox(Me.components)
        Me.txtYellow = New Care.Controls.CareTextBox(Me.components)
        Me.txtBlue = New Care.Controls.CareTextBox(Me.components)
        Me.txtGreen = New Care.Controls.CareTextBox(Me.components)
        Me.txtWarnings = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.txtSuccess = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.txtErrors = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel35 = New Care.Controls.CareLabel(Me.components)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtRemaining.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOrange.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBlue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtWarnings.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSuccess.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtErrors.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'cgDocs
        '
        Me.cgDocs.AllowBuildColumns = True
        Me.cgDocs.AllowEdit = False
        Me.cgDocs.AllowHorizontalScroll = False
        Me.cgDocs.AllowMultiSelect = False
        Me.cgDocs.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgDocs.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgDocs.Appearance.Options.UseFont = True
        Me.cgDocs.AutoSizeByData = True
        Me.cgDocs.DisableAutoSize = False
        Me.cgDocs.DisableDataFormatting = False
        Me.cgDocs.FocusedRowHandle = -2147483648
        Me.cgDocs.HideFirstColumn = False
        Me.cgDocs.Location = New System.Drawing.Point(12, 56)
        Me.cgDocs.Name = "cgDocs"
        Me.cgDocs.PreviewColumn = ""
        Me.cgDocs.QueryID = Nothing
        Me.cgDocs.RowAutoHeight = False
        Me.cgDocs.SearchAsYouType = True
        Me.cgDocs.ShowAutoFilterRow = False
        Me.cgDocs.ShowFindPanel = True
        Me.cgDocs.ShowGroupByBox = True
        Me.cgDocs.ShowLoadingPanel = False
        Me.cgDocs.ShowNavigator = True
        Me.cgDocs.Size = New System.Drawing.Size(960, 593)
        Me.cgDocs.TabIndex = 1
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.txtRemaining)
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Controls.Add(Me.btnRetry)
        Me.GroupControl1.Controls.Add(Me.btnPrint)
        Me.GroupControl1.Controls.Add(Me.txtOrange)
        Me.GroupControl1.Controls.Add(Me.txtRed)
        Me.GroupControl1.Controls.Add(Me.txtYellow)
        Me.GroupControl1.Controls.Add(Me.txtBlue)
        Me.GroupControl1.Controls.Add(Me.txtGreen)
        Me.GroupControl1.Controls.Add(Me.txtWarnings)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.txtSuccess)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Controls.Add(Me.txtErrors)
        Me.GroupControl1.Controls.Add(Me.CareLabel35)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 8)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(960, 41)
        Me.GroupControl1.TabIndex = 3
        Me.GroupControl1.Text = "Site"
        '
        'txtRemaining
        '
        Me.txtRemaining.CharacterCasing = CharacterCasing.Normal
        Me.txtRemaining.EnterMoveNextControl = True
        Me.txtRemaining.Location = New System.Drawing.Point(893, 10)
        Me.txtRemaining.MaxLength = 14
        Me.txtRemaining.Name = "txtRemaining"
        Me.txtRemaining.NumericAllowNegatives = True
        Me.txtRemaining.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.None
        Me.txtRemaining.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRemaining.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRemaining.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemaining.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.txtRemaining.Properties.Appearance.Options.UseFont = True
        Me.txtRemaining.Properties.Appearance.Options.UseForeColor = True
        Me.txtRemaining.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRemaining.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtRemaining.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRemaining.Properties.MaxLength = 14
        Me.txtRemaining.Properties.ReadOnly = True
        Me.txtRemaining.Size = New System.Drawing.Size(62, 22)
        Me.txtRemaining.TabIndex = 20
        Me.txtRemaining.Tag = ""
        Me.txtRemaining.TextAlign = HorizontalAlignment.Right
        Me.txtRemaining.ToolTipText = ""
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(830, 13)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(57, 15)
        Me.CareLabel3.TabIndex = 19
        Me.CareLabel3.Text = "Remaining"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnRetry
        '
        Me.btnRetry.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRetry.Appearance.Options.UseFont = True
        Me.btnRetry.Location = New System.Drawing.Point(128, 8)
        Me.btnRetry.Name = "btnRetry"
        Me.btnRetry.Size = New System.Drawing.Size(115, 24)
        Me.btnRetry.TabIndex = 18
        Me.btnRetry.Tag = ""
        Me.btnRetry.Text = "Retry Posting"
        '
        'btnPrint
        '
        Me.btnPrint.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.Appearance.Options.UseFont = True
        Me.btnPrint.Location = New System.Drawing.Point(7, 8)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(115, 24)
        Me.btnPrint.TabIndex = 17
        Me.btnPrint.Tag = ""
        Me.btnPrint.Text = "Posting Audit"
        '
        'txtOrange
        '
        Me.txtOrange.CharacterCasing = CharacterCasing.Normal
        Me.txtOrange.EnterMoveNextControl = True
        Me.txtOrange.Location = New System.Drawing.Point(371, 10)
        Me.txtOrange.MaxLength = 0
        Me.txtOrange.Name = "txtOrange"
        Me.txtOrange.NumericAllowNegatives = False
        Me.txtOrange.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtOrange.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtOrange.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtOrange.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtOrange.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtOrange.Properties.Appearance.Options.UseBackColor = True
        Me.txtOrange.Properties.Appearance.Options.UseFont = True
        Me.txtOrange.Properties.Appearance.Options.UseTextOptions = True
        Me.txtOrange.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtOrange.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtOrange.Size = New System.Drawing.Size(25, 22)
        Me.txtOrange.TabIndex = 16
        Me.txtOrange.TextAlign = HorizontalAlignment.Left
        Me.txtOrange.ToolTipText = ""
        Me.txtOrange.Visible = False
        '
        'txtRed
        '
        Me.txtRed.CharacterCasing = CharacterCasing.Normal
        Me.txtRed.EnterMoveNextControl = True
        Me.txtRed.Location = New System.Drawing.Point(249, 10)
        Me.txtRed.MaxLength = 0
        Me.txtRed.Name = "txtRed"
        Me.txtRed.NumericAllowNegatives = False
        Me.txtRed.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRed.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRed.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRed.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtRed.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRed.Properties.Appearance.Options.UseBackColor = True
        Me.txtRed.Properties.Appearance.Options.UseFont = True
        Me.txtRed.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRed.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRed.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRed.Size = New System.Drawing.Size(25, 22)
        Me.txtRed.TabIndex = 12
        Me.txtRed.TextAlign = HorizontalAlignment.Left
        Me.txtRed.ToolTipText = ""
        Me.txtRed.Visible = False
        '
        'txtYellow
        '
        Me.txtYellow.CharacterCasing = CharacterCasing.Normal
        Me.txtYellow.EnterMoveNextControl = True
        Me.txtYellow.Location = New System.Drawing.Point(340, 10)
        Me.txtYellow.MaxLength = 0
        Me.txtYellow.Name = "txtYellow"
        Me.txtYellow.NumericAllowNegatives = False
        Me.txtYellow.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtYellow.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtYellow.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtYellow.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtYellow.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtYellow.Properties.Appearance.Options.UseBackColor = True
        Me.txtYellow.Properties.Appearance.Options.UseFont = True
        Me.txtYellow.Properties.Appearance.Options.UseTextOptions = True
        Me.txtYellow.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtYellow.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtYellow.Size = New System.Drawing.Size(25, 22)
        Me.txtYellow.TabIndex = 15
        Me.txtYellow.TextAlign = HorizontalAlignment.Left
        Me.txtYellow.ToolTipText = ""
        Me.txtYellow.Visible = False
        '
        'txtBlue
        '
        Me.txtBlue.CharacterCasing = CharacterCasing.Normal
        Me.txtBlue.EnterMoveNextControl = True
        Me.txtBlue.Location = New System.Drawing.Point(309, 10)
        Me.txtBlue.MaxLength = 0
        Me.txtBlue.Name = "txtBlue"
        Me.txtBlue.NumericAllowNegatives = False
        Me.txtBlue.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtBlue.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBlue.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtBlue.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBlue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtBlue.Properties.Appearance.Options.UseBackColor = True
        Me.txtBlue.Properties.Appearance.Options.UseFont = True
        Me.txtBlue.Properties.Appearance.Options.UseTextOptions = True
        Me.txtBlue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtBlue.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtBlue.Size = New System.Drawing.Size(25, 22)
        Me.txtBlue.TabIndex = 14
        Me.txtBlue.TextAlign = HorizontalAlignment.Left
        Me.txtBlue.ToolTipText = ""
        Me.txtBlue.Visible = False
        '
        'txtGreen
        '
        Me.txtGreen.CharacterCasing = CharacterCasing.Normal
        Me.txtGreen.EnterMoveNextControl = True
        Me.txtGreen.Location = New System.Drawing.Point(278, 10)
        Me.txtGreen.MaxLength = 0
        Me.txtGreen.Name = "txtGreen"
        Me.txtGreen.NumericAllowNegatives = False
        Me.txtGreen.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtGreen.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtGreen.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtGreen.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtGreen.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtGreen.Properties.Appearance.Options.UseBackColor = True
        Me.txtGreen.Properties.Appearance.Options.UseFont = True
        Me.txtGreen.Properties.Appearance.Options.UseTextOptions = True
        Me.txtGreen.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtGreen.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtGreen.Size = New System.Drawing.Size(25, 22)
        Me.txtGreen.TabIndex = 13
        Me.txtGreen.TextAlign = HorizontalAlignment.Left
        Me.txtGreen.ToolTipText = ""
        Me.txtGreen.Visible = False
        '
        'txtWarnings
        '
        Me.txtWarnings.CharacterCasing = CharacterCasing.Normal
        Me.txtWarnings.EnterMoveNextControl = True
        Me.txtWarnings.Location = New System.Drawing.Point(648, 10)
        Me.txtWarnings.MaxLength = 14
        Me.txtWarnings.Name = "txtWarnings"
        Me.txtWarnings.NumericAllowNegatives = True
        Me.txtWarnings.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.None
        Me.txtWarnings.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtWarnings.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtWarnings.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWarnings.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtWarnings.Properties.Appearance.Options.UseFont = True
        Me.txtWarnings.Properties.Appearance.Options.UseForeColor = True
        Me.txtWarnings.Properties.Appearance.Options.UseTextOptions = True
        Me.txtWarnings.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtWarnings.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtWarnings.Properties.MaxLength = 14
        Me.txtWarnings.Properties.ReadOnly = True
        Me.txtWarnings.Size = New System.Drawing.Size(62, 22)
        Me.txtWarnings.TabIndex = 11
        Me.txtWarnings.Tag = ""
        Me.txtWarnings.TextAlign = HorizontalAlignment.Right
        Me.txtWarnings.ToolTip = resources.GetString("txtWarnings.ToolTip")
        Me.txtWarnings.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.txtWarnings.ToolTipText = resources.GetString("txtWarnings.ToolTipText")
        Me.txtWarnings.ToolTipTitle = "Warnings"
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(592, 13)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(50, 15)
        Me.CareLabel2.TabIndex = 10
        Me.CareLabel2.Text = "Warnings"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSuccess
        '
        Me.txtSuccess.CharacterCasing = CharacterCasing.Normal
        Me.txtSuccess.EnterMoveNextControl = True
        Me.txtSuccess.Location = New System.Drawing.Point(524, 10)
        Me.txtSuccess.MaxLength = 5
        Me.txtSuccess.Name = "txtSuccess"
        Me.txtSuccess.NumericAllowNegatives = True
        Me.txtSuccess.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.None
        Me.txtSuccess.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSuccess.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSuccess.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSuccess.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtSuccess.Properties.Appearance.Options.UseFont = True
        Me.txtSuccess.Properties.Appearance.Options.UseForeColor = True
        Me.txtSuccess.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSuccess.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtSuccess.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSuccess.Properties.MaxLength = 5
        Me.txtSuccess.Properties.ReadOnly = True
        Me.txtSuccess.Size = New System.Drawing.Size(62, 22)
        Me.txtSuccess.TabIndex = 9
        Me.txtSuccess.Tag = ""
        Me.txtSuccess.TextAlign = HorizontalAlignment.Right
        Me.txtSuccess.ToolTip = "The number of items that have been successfully posted to your Accounting Softwar" &
    "e"
        Me.txtSuccess.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.txtSuccess.ToolTipText = "The number of items that have been successfully posted to your Accounting Softwar" &
    "e"
        Me.txtSuccess.ToolTipTitle = "Success"
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(477, 13)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(41, 15)
        Me.CareLabel1.TabIndex = 8
        Me.CareLabel1.Text = "Success"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtErrors
        '
        Me.txtErrors.CharacterCasing = CharacterCasing.Normal
        Me.txtErrors.EnterMoveNextControl = True
        Me.txtErrors.Location = New System.Drawing.Point(752, 10)
        Me.txtErrors.MaxLength = 14
        Me.txtErrors.Name = "txtErrors"
        Me.txtErrors.NumericAllowNegatives = True
        Me.txtErrors.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.None
        Me.txtErrors.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtErrors.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtErrors.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtErrors.Properties.Appearance.ForeColor = System.Drawing.Color.Red
        Me.txtErrors.Properties.Appearance.Options.UseFont = True
        Me.txtErrors.Properties.Appearance.Options.UseForeColor = True
        Me.txtErrors.Properties.Appearance.Options.UseTextOptions = True
        Me.txtErrors.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtErrors.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtErrors.Properties.MaxLength = 14
        Me.txtErrors.Properties.ReadOnly = True
        Me.txtErrors.Size = New System.Drawing.Size(62, 22)
        Me.txtErrors.TabIndex = 7
        Me.txtErrors.Tag = ""
        Me.txtErrors.TextAlign = HorizontalAlignment.Right
        Me.txtErrors.ToolTip = resources.GetString("txtErrors.ToolTip")
        Me.txtErrors.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.txtErrors.ToolTipText = resources.GetString("txtErrors.ToolTipText")
        Me.txtErrors.ToolTipTitle = "Errors"
        '
        'CareLabel35
        '
        Me.CareLabel35.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel35.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel35.Location = New System.Drawing.Point(716, 13)
        Me.CareLabel35.Name = "CareLabel35"
        Me.CareLabel35.Size = New System.Drawing.Size(30, 15)
        Me.CareLabel35.TabIndex = 6
        Me.CareLabel35.Text = "Errors"
        Me.CareLabel35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmFinancialsPost
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(984, 661)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.cgDocs)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFinancialsPost"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "Batch Posting Progress"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtRemaining.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOrange.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBlue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtWarnings.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSuccess.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtErrors.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents cgDocs As Care.Controls.CareGrid
    Friend WithEvents GroupControl1 As Care.Controls.CareFrame
    Friend WithEvents txtWarnings As Care.Controls.CareTextBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents txtSuccess As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents txtErrors As Care.Controls.CareTextBox
    Friend WithEvents CareLabel35 As Care.Controls.CareLabel
    Friend WithEvents txtOrange As Care.Controls.CareTextBox
    Friend WithEvents txtRed As Care.Controls.CareTextBox
    Friend WithEvents txtYellow As Care.Controls.CareTextBox
    Friend WithEvents txtBlue As Care.Controls.CareTextBox
    Friend WithEvents txtGreen As Care.Controls.CareTextBox
    Friend WithEvents btnPrint As Care.Controls.CareButton
    Friend WithEvents btnRetry As Care.Controls.CareButton
    Friend WithEvents txtRemaining As Care.Controls.CareTextBox
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
End Class
