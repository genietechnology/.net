﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChildAnnual

    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmChildAnnual))
        Me.gbxWeekly = New Care.Controls.CareFrame()
        Me.btnDeactivate = New Care.Controls.CareButton(Me.components)
        Me.btnViewBatch = New Care.Controls.CareButton(Me.components)
        Me.btnViewInvoice = New Care.Controls.CareButton(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.cdtInvoiceDue = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.cbxSplitType = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.cdtInvoiceDate = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.txtSplitSegments = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.cdtDateTo = New Care.Controls.CareDateTime(Me.components)
        Me.txtSplitTotal = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.cdtDateFrom = New Care.Controls.CareDateTime(Me.components)
        Me.txtAnnualTotal = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.btnRecalculate = New Care.Controls.CareButton(Me.components)
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.lblAge = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel12 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.txtDOB = New Care.Controls.CareTextBox(Me.components)
        Me.txtName = New Care.Controls.CareTextBox(Me.components)
        CType(Me.gbxWeekly, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxWeekly.SuspendLayout()
        CType(Me.cdtInvoiceDue.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtInvoiceDue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSplitType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtInvoiceDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtInvoiceDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSplitSegments.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDateTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDateTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSplitTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDateFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDateFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAnnualTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtDOB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'gbxWeekly
        '
        Me.gbxWeekly.Controls.Add(Me.btnDeactivate)
        Me.gbxWeekly.Controls.Add(Me.btnViewBatch)
        Me.gbxWeekly.Controls.Add(Me.btnViewInvoice)
        Me.gbxWeekly.Controls.Add(Me.CareLabel8)
        Me.gbxWeekly.Controls.Add(Me.cdtInvoiceDue)
        Me.gbxWeekly.Controls.Add(Me.CareLabel6)
        Me.gbxWeekly.Controls.Add(Me.cbxSplitType)
        Me.gbxWeekly.Controls.Add(Me.CareLabel4)
        Me.gbxWeekly.Controls.Add(Me.cdtInvoiceDate)
        Me.gbxWeekly.Controls.Add(Me.CareLabel1)
        Me.gbxWeekly.Controls.Add(Me.CareLabel7)
        Me.gbxWeekly.Controls.Add(Me.CareLabel5)
        Me.gbxWeekly.Controls.Add(Me.txtSplitSegments)
        Me.gbxWeekly.Controls.Add(Me.CareLabel2)
        Me.gbxWeekly.Controls.Add(Me.cdtDateTo)
        Me.gbxWeekly.Controls.Add(Me.txtSplitTotal)
        Me.gbxWeekly.Controls.Add(Me.CareLabel11)
        Me.gbxWeekly.Controls.Add(Me.cdtDateFrom)
        Me.gbxWeekly.Controls.Add(Me.txtAnnualTotal)
        Me.gbxWeekly.Location = New System.Drawing.Point(12, 105)
        Me.gbxWeekly.Name = "gbxWeekly"
        Me.gbxWeekly.Size = New System.Drawing.Size(391, 198)
        Me.gbxWeekly.TabIndex = 1
        Me.gbxWeekly.Text = "Annual Calculation Parameters"
        '
        'btnDeactivate
        '
        Me.btnDeactivate.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnDeactivate.Appearance.Options.UseFont = True
        Me.btnDeactivate.Location = New System.Drawing.Point(220, 166)
        Me.btnDeactivate.Name = "btnDeactivate"
        Me.btnDeactivate.Size = New System.Drawing.Size(161, 22)
        Me.btnDeactivate.TabIndex = 18
        Me.btnDeactivate.Text = "Deactivate"
        '
        'btnViewBatch
        '
        Me.btnViewBatch.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnViewBatch.Appearance.Options.UseFont = True
        Me.btnViewBatch.Location = New System.Drawing.Point(220, 138)
        Me.btnViewBatch.Name = "btnViewBatch"
        Me.btnViewBatch.Size = New System.Drawing.Size(161, 22)
        Me.btnViewBatch.TabIndex = 15
        Me.btnViewBatch.Text = "View Segment Batch"
        '
        'btnViewInvoice
        '
        Me.btnViewInvoice.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnViewInvoice.Appearance.Options.UseFont = True
        Me.btnViewInvoice.Location = New System.Drawing.Point(220, 110)
        Me.btnViewInvoice.Name = "btnViewInvoice"
        Me.btnViewInvoice.Size = New System.Drawing.Size(161, 22)
        Me.btnViewInvoice.TabIndex = 12
        Me.btnViewInvoice.Text = "View Annual Invoice"
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(254, 85)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(21, 15)
        Me.CareLabel8.TabIndex = 8
        Me.CareLabel8.Text = "Due"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtInvoiceDue
        '
        Me.cdtInvoiceDue.EditValue = Nothing
        Me.cdtInvoiceDue.EnterMoveNextControl = True
        Me.cdtInvoiceDue.Location = New System.Drawing.Point(281, 82)
        Me.cdtInvoiceDue.Name = "cdtInvoiceDue"
        Me.cdtInvoiceDue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtInvoiceDue.Properties.Appearance.Options.UseFont = True
        Me.cdtInvoiceDue.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtInvoiceDue.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtInvoiceDue.Size = New System.Drawing.Size(100, 22)
        Me.cdtInvoiceDue.TabIndex = 9
        Me.cdtInvoiceDue.Tag = "AEM"
        Me.cdtInvoiceDue.Value = Nothing
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(12, 85)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(65, 15)
        Me.CareLabel6.TabIndex = 6
        Me.CareLabel6.Text = "Invoice Date"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxSplitType
        '
        Me.cbxSplitType.AllowBlank = False
        Me.cbxSplitType.DataSource = Nothing
        Me.cbxSplitType.DisplayMember = Nothing
        Me.cbxSplitType.EnterMoveNextControl = True
        Me.cbxSplitType.Location = New System.Drawing.Point(124, 28)
        Me.cbxSplitType.Name = "cbxSplitType"
        Me.cbxSplitType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSplitType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxSplitType.Properties.Appearance.Options.UseFont = True
        Me.cbxSplitType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSplitType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSplitType.SelectedValue = Nothing
        Me.cbxSplitType.Size = New System.Drawing.Size(257, 22)
        Me.cbxSplitType.TabIndex = 1
        Me.cbxSplitType.Tag = "AEM"
        Me.cbxSplitType.ValueMember = Nothing
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(12, 31)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(68, 15)
        Me.CareLabel4.TabIndex = 0
        Me.CareLabel4.Text = "Split Method"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtInvoiceDate
        '
        Me.cdtInvoiceDate.EditValue = Nothing
        Me.cdtInvoiceDate.EnterMoveNextControl = True
        Me.cdtInvoiceDate.Location = New System.Drawing.Point(124, 82)
        Me.cdtInvoiceDate.Name = "cdtInvoiceDate"
        Me.cdtInvoiceDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtInvoiceDate.Properties.Appearance.Options.UseFont = True
        Me.cdtInvoiceDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtInvoiceDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtInvoiceDate.Size = New System.Drawing.Size(100, 22)
        Me.cdtInvoiceDate.TabIndex = 7
        Me.cdtInvoiceDate.Tag = "AEM"
        Me.cdtInvoiceDate.Value = Nothing
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(12, 142)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(78, 15)
        Me.CareLabel1.TabIndex = 13
        Me.CareLabel1.Text = "Split Segments"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(12, 170)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(49, 15)
        Me.CareLabel7.TabIndex = 16
        Me.CareLabel7.Text = "Split Fees"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(12, 114)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(64, 15)
        Me.CareLabel5.TabIndex = 10
        Me.CareLabel5.Text = "Annual Fees"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSplitSegments
        '
        Me.txtSplitSegments.CharacterCasing = CharacterCasing.Normal
        Me.txtSplitSegments.EnterMoveNextControl = True
        Me.txtSplitSegments.Location = New System.Drawing.Point(124, 139)
        Me.txtSplitSegments.MaxLength = 0
        Me.txtSplitSegments.Name = "txtSplitSegments"
        Me.txtSplitSegments.NumericAllowNegatives = False
        Me.txtSplitSegments.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSplitSegments.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSplitSegments.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSplitSegments.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSplitSegments.Properties.Appearance.Options.UseFont = True
        Me.txtSplitSegments.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSplitSegments.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtSplitSegments.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSplitSegments.Properties.ReadOnly = True
        Me.txtSplitSegments.Size = New System.Drawing.Size(90, 22)
        Me.txtSplitSegments.TabIndex = 14
        Me.txtSplitSegments.TextAlign = HorizontalAlignment.Right
        Me.txtSplitSegments.ToolTipText = ""
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(261, 59)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(14, 15)
        Me.CareLabel2.TabIndex = 4
        Me.CareLabel2.Text = "To"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtDateTo
        '
        Me.cdtDateTo.EditValue = Nothing
        Me.cdtDateTo.EnterMoveNextControl = True
        Me.cdtDateTo.Location = New System.Drawing.Point(281, 56)
        Me.cdtDateTo.Name = "cdtDateTo"
        Me.cdtDateTo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtDateTo.Properties.Appearance.Options.UseFont = True
        Me.cdtDateTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDateTo.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtDateTo.Size = New System.Drawing.Size(100, 22)
        Me.cdtDateTo.TabIndex = 5
        Me.cdtDateTo.Tag = "AEM"
        Me.cdtDateTo.Value = Nothing
        '
        'txtSplitTotal
        '
        Me.txtSplitTotal.CharacterCasing = CharacterCasing.Normal
        Me.txtSplitTotal.EnterMoveNextControl = True
        Me.txtSplitTotal.Location = New System.Drawing.Point(124, 167)
        Me.txtSplitTotal.MaxLength = 0
        Me.txtSplitTotal.Name = "txtSplitTotal"
        Me.txtSplitTotal.NumericAllowNegatives = False
        Me.txtSplitTotal.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSplitTotal.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSplitTotal.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSplitTotal.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSplitTotal.Properties.Appearance.Options.UseFont = True
        Me.txtSplitTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSplitTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtSplitTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSplitTotal.Properties.ReadOnly = True
        Me.txtSplitTotal.Size = New System.Drawing.Size(90, 22)
        Me.txtSplitTotal.TabIndex = 17
        Me.txtSplitTotal.TextAlign = HorizontalAlignment.Right
        Me.txtSplitTotal.ToolTipText = ""
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(12, 59)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(28, 15)
        Me.CareLabel11.TabIndex = 2
        Me.CareLabel11.Text = "From"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtDateFrom
        '
        Me.cdtDateFrom.EditValue = Nothing
        Me.cdtDateFrom.EnterMoveNextControl = True
        Me.cdtDateFrom.Location = New System.Drawing.Point(124, 56)
        Me.cdtDateFrom.Name = "cdtDateFrom"
        Me.cdtDateFrom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtDateFrom.Properties.Appearance.Options.UseFont = True
        Me.cdtDateFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDateFrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtDateFrom.Size = New System.Drawing.Size(100, 22)
        Me.cdtDateFrom.TabIndex = 3
        Me.cdtDateFrom.Tag = "AEM"
        Me.cdtDateFrom.Value = Nothing
        '
        'txtAnnualTotal
        '
        Me.txtAnnualTotal.CharacterCasing = CharacterCasing.Normal
        Me.txtAnnualTotal.EnterMoveNextControl = True
        Me.txtAnnualTotal.Location = New System.Drawing.Point(124, 111)
        Me.txtAnnualTotal.MaxLength = 0
        Me.txtAnnualTotal.Name = "txtAnnualTotal"
        Me.txtAnnualTotal.NumericAllowNegatives = False
        Me.txtAnnualTotal.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtAnnualTotal.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtAnnualTotal.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtAnnualTotal.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtAnnualTotal.Properties.Appearance.Options.UseFont = True
        Me.txtAnnualTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.txtAnnualTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtAnnualTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtAnnualTotal.Properties.ReadOnly = True
        Me.txtAnnualTotal.Size = New System.Drawing.Size(90, 22)
        Me.txtAnnualTotal.TabIndex = 11
        Me.txtAnnualTotal.TextAlign = HorizontalAlignment.Right
        Me.txtAnnualTotal.ToolTipText = ""
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel3.Appearance.Image = CType(resources.GetObject("CareLabel3.Appearance.Image"), System.Drawing.Image)
        Me.CareLabel3.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.CareLabel3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.CareLabel3.Location = New System.Drawing.Point(173, 315)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(138, 16)
        Me.CareLabel3.TabIndex = 3
        Me.CareLabel3.Text = "Calculation Reminder"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CareLabel3.ToolTip = resources.GetString("CareLabel3.ToolTip")
        Me.CareLabel3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.CareLabel3.ToolTipTitle = "Annual Average Calculation"
        '
        'btnClose
        '
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.CausesValidation = False
        Me.btnClose.DialogResult = DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(318, 310)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(85, 25)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Text = "Close"
        '
        'btnRecalculate
        '
        Me.btnRecalculate.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnRecalculate.Appearance.Options.UseFont = True
        Me.btnRecalculate.Location = New System.Drawing.Point(12, 310)
        Me.btnRecalculate.Name = "btnRecalculate"
        Me.btnRecalculate.Size = New System.Drawing.Size(155, 25)
        Me.btnRecalculate.TabIndex = 2
        Me.btnRecalculate.Text = "Calculate Annual Invoice"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.lblAge)
        Me.GroupControl1.Controls.Add(Me.CareLabel12)
        Me.GroupControl1.Controls.Add(Me.CareLabel14)
        Me.GroupControl1.Controls.Add(Me.txtDOB)
        Me.GroupControl1.Controls.Add(Me.txtName)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(391, 87)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Child Details"
        '
        'lblAge
        '
        Me.lblAge.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAge.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblAge.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblAge.Location = New System.Drawing.Point(223, 60)
        Me.lblAge.Name = "lblAge"
        Me.lblAge.Size = New System.Drawing.Size(147, 15)
        Me.lblAge.TabIndex = 4
        Me.lblAge.Text = "1 year, 11 months, 21 days"
        Me.lblAge.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel12
        '
        Me.CareLabel12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel12.Location = New System.Drawing.Point(12, 60)
        Me.CareLabel12.Name = "CareLabel12"
        Me.CareLabel12.Size = New System.Drawing.Size(24, 15)
        Me.CareLabel12.TabIndex = 2
        Me.CareLabel12.Text = "DOB"
        Me.CareLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(12, 32)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel14.TabIndex = 0
        Me.CareLabel14.Text = "Name"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDOB
        '
        Me.txtDOB.CharacterCasing = CharacterCasing.Normal
        Me.txtDOB.EnterMoveNextControl = True
        Me.txtDOB.Location = New System.Drawing.Point(127, 57)
        Me.txtDOB.MaxLength = 0
        Me.txtDOB.Name = "txtDOB"
        Me.txtDOB.NumericAllowNegatives = False
        Me.txtDOB.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtDOB.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDOB.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtDOB.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtDOB.Properties.Appearance.Options.UseFont = True
        Me.txtDOB.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDOB.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtDOB.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDOB.Properties.ReadOnly = True
        Me.txtDOB.Size = New System.Drawing.Size(90, 22)
        Me.txtDOB.TabIndex = 3
        Me.txtDOB.TextAlign = HorizontalAlignment.Left
        Me.txtDOB.ToolTipText = ""
        '
        'txtName
        '
        Me.txtName.CharacterCasing = CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(127, 29)
        Me.txtName.MaxLength = 0
        Me.txtName.Name = "txtName"
        Me.txtName.NumericAllowNegatives = False
        Me.txtName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtName.Properties.ReadOnly = True
        Me.txtName.Size = New System.Drawing.Size(254, 22)
        Me.txtName.TabIndex = 1
        Me.txtName.TextAlign = HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'frmChildAnnual
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(414, 343)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnRecalculate)
        Me.Controls.Add(Me.gbxWeekly)
        Me.Controls.Add(Me.CareLabel3)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmChildAnnual"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = ""
        CType(Me.gbxWeekly, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxWeekly.ResumeLayout(False)
        Me.gbxWeekly.PerformLayout()
        CType(Me.cdtInvoiceDue.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtInvoiceDue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSplitType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtInvoiceDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtInvoiceDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSplitSegments.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDateTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDateTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSplitTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDateFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDateFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAnnualTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtDOB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxWeekly As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents cdtDateFrom As Care.Controls.CareDateTime
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents cdtDateTo As Care.Controls.CareDateTime
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents cbxSplitType As Care.Controls.CareComboBox
    Friend WithEvents txtSplitTotal As Care.Controls.CareTextBox
    Friend WithEvents btnViewInvoice As Care.Controls.CareButton
    Friend WithEvents btnRecalculate As Care.Controls.CareButton
    Friend WithEvents txtAnnualTotal As Care.Controls.CareTextBox
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents txtSplitSegments As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents cdtInvoiceDate As Care.Controls.CareDateTime
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents cdtInvoiceDue As Care.Controls.CareDateTime
    Friend WithEvents btnViewBatch As Care.Controls.CareButton
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel12 As Care.Controls.CareLabel
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents txtDOB As Care.Controls.CareTextBox
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents lblAge As Care.Controls.CareLabel
    Friend WithEvents btnDeactivate As Care.Controls.CareButton

End Class
