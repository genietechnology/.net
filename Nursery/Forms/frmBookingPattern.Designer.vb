﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBookingPattern
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBookingPattern))
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.btnAdvOK = New Care.Controls.CareButton()
        Me.btnAdvCancel = New Care.Controls.CareButton()
        Me.cdtWC = New Care.Controls.CareDateTime()
        Me.lblWC = New Care.Controls.CareLabel()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.lblMustBeMonday = New Care.Controls.CareLabel()
        Me.GroupControl2 = New Care.Controls.CareFrame()
        Me.radSessSingle = New Care.Controls.CareRadioButton()
        Me.radSessMulti = New Care.Controls.CareRadioButton()
        Me.CareLabel3 = New Care.Controls.CareLabel()
        Me.lblWednesday = New Care.Controls.CareLabel()
        Me.lblThursday = New Care.Controls.CareLabel()
        Me.lblFriday = New Care.Controls.CareLabel()
        Me.lblMonday = New Care.Controls.CareLabel()
        Me.lblTuesday = New Care.Controls.CareLabel()
        Me.lblSaturday = New Care.Controls.CareLabel()
        Me.lblSunday = New Care.Controls.CareLabel()
        Me.gbxWeekly = New Care.Controls.CareFrame()
        Me.grpDuplicate = New Care.Controls.CareFrame()
        Me.TableLayoutPanel1 = New TableLayoutPanel()
        Me.lblDFriday = New Care.Controls.CareLabel()
        Me.lblDThursday = New Care.Controls.CareLabel()
        Me.lblDWednesday = New Care.Controls.CareLabel()
        Me.lblDTuesday = New Care.Controls.CareLabel()
        Me.chkMonday = New Care.Controls.CareCheckBox()
        Me.chkTuesday = New Care.Controls.CareCheckBox()
        Me.chkWednesday = New Care.Controls.CareCheckBox()
        Me.chkThursday = New Care.Controls.CareCheckBox()
        Me.chkFriday = New Care.Controls.CareCheckBox()
        Me.lblDMonday = New Care.Controls.CareLabel()
        Me.GroupControl3 = New Care.Controls.CareFrame()
        Me.radAdditional = New Care.Controls.CareRadioButton()
        Me.radRecurring = New Care.Controls.CareRadioButton()
        Me.radOverride = New Care.Controls.CareRadioButton()
        Me.gbxScope = New Care.Controls.CareFrame()
        Me.cbxScope = New Care.Controls.CareComboBox()
        Me.CareLabel1 = New Care.Controls.CareLabel()
        Me.GroupControl4 = New Care.Controls.CareFrame()
        Me.txtComments = New DevExpress.XtraEditors.MemoEdit()
        Me.btnCopy = New Care.Controls.CareButton()
        Me.btnPaste = New Care.Controls.CareButton()
        Me.GroupControl6 = New Care.Controls.CareFrame()
        Me.radBooked = New Care.Controls.CareRadioButton()
        Me.radWaiting = New Care.Controls.CareRadioButton()
        Me.tcSunday = New Nursery.TariffControl()
        Me.tcSaturday = New Nursery.TariffControl()
        Me.tcFriday = New Nursery.TariffControl()
        Me.tcThursday = New Nursery.TariffControl()
        Me.tcWednesday = New Nursery.TariffControl()
        Me.tcTuesday = New Nursery.TariffControl()
        Me.tcMonday = New Nursery.TariffControl()
        CType(Me.cdtWC.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtWC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.radSessSingle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radSessMulti.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxWeekly, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxWeekly.SuspendLayout()
        CType(Me.grpDuplicate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpDuplicate.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.chkMonday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkTuesday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkWednesday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkThursday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkFriday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.radAdditional.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radRecurring.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radOverride.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxScope, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxScope.SuspendLayout()
        CType(Me.cbxScope.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.txtComments.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl6.SuspendLayout()
        CType(Me.radBooked.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radWaiting.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'btnAdvOK
        '
        Me.btnAdvOK.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnAdvOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnAdvOK.Appearance.Options.UseFont = True
        Me.btnAdvOK.Location = New System.Drawing.Point(696, 609)
        Me.btnAdvOK.Name = "btnAdvOK"
        Me.btnAdvOK.Size = New System.Drawing.Size(85, 25)
        Me.btnAdvOK.TabIndex = 10
        Me.btnAdvOK.Text = "OK"
        '
        'btnAdvCancel
        '
        Me.btnAdvCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnAdvCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnAdvCancel.Appearance.Options.UseFont = True
        Me.btnAdvCancel.CausesValidation = False
        Me.btnAdvCancel.DialogResult = DialogResult.Cancel
        Me.btnAdvCancel.Location = New System.Drawing.Point(787, 609)
        Me.btnAdvCancel.Name = "btnAdvCancel"
        Me.btnAdvCancel.Size = New System.Drawing.Size(85, 25)
        Me.btnAdvCancel.TabIndex = 11
        Me.btnAdvCancel.Text = "Cancel"
        '
        'cdtWC
        '
        Me.cdtWC.EditValue = Nothing
        Me.cdtWC.EnterMoveNextControl = True
        Me.cdtWC.Location = New System.Drawing.Point(180, 11)
        Me.cdtWC.Name = "cdtWC"
        Me.cdtWC.Properties.AccessibleName = "Week Commencing"
        Me.cdtWC.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtWC.Properties.Appearance.Options.UseFont = True
        Me.cdtWC.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtWC.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtWC.Size = New System.Drawing.Size(100, 22)
        Me.cdtWC.TabIndex = 1
        Me.cdtWC.Tag = ""
        Me.cdtWC.Value = Nothing
        '
        'lblWC
        '
        Me.lblWC.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWC.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblWC.Location = New System.Drawing.Point(10, 14)
        Me.lblWC.Name = "lblWC"
        Me.lblWC.Size = New System.Drawing.Size(105, 15)
        Me.lblWC.TabIndex = 0
        Me.lblWC.Text = "Week Commencing"
        Me.lblWC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblWC.ToolTip = "Double Click this label to default the Week Commencing date to the Start Date."
        Me.lblWC.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.lblWC.ToolTipTitle = "Default Start Date"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.lblMustBeMonday)
        Me.GroupControl1.Controls.Add(Me.cdtWC)
        Me.GroupControl1.Controls.Add(Me.lblWC)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 60)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(459, 43)
        Me.GroupControl1.TabIndex = 3
        '
        'lblMustBeMonday
        '
        Me.lblMustBeMonday.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMustBeMonday.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblMustBeMonday.Location = New System.Drawing.Point(286, 14)
        Me.lblMustBeMonday.Name = "lblMustBeMonday"
        Me.lblMustBeMonday.Size = New System.Drawing.Size(109, 15)
        Me.lblMustBeMonday.TabIndex = 2
        Me.lblMustBeMonday.Text = "(must be a Monday)"
        Me.lblMustBeMonday.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.radSessSingle)
        Me.GroupControl2.Controls.Add(Me.radSessMulti)
        Me.GroupControl2.Controls.Add(Me.CareLabel3)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.ShowCaption = False
        Me.GroupControl2.Size = New System.Drawing.Size(359, 42)
        Me.GroupControl2.TabIndex = 0
        '
        'radSessSingle
        '
        Me.radSessSingle.EditValue = True
        Me.radSessSingle.Location = New System.Drawing.Point(91, 12)
        Me.radSessSingle.Name = "radSessSingle"
        Me.radSessSingle.Properties.AutoWidth = True
        Me.radSessSingle.Properties.Caption = "Single Session (Weekly)"
        Me.radSessSingle.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radSessSingle.Properties.RadioGroupIndex = 1
        Me.radSessSingle.Size = New System.Drawing.Size(135, 19)
        Me.radSessSingle.TabIndex = 1
        Me.radSessSingle.ToolTip = "One Session per Day (used to be called Weekly Mode)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "e.g. Full Day Sessions" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.radSessSingle.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.radSessSingle.ToolTipTitle = "Single Session"
        '
        'radSessMulti
        '
        Me.radSessMulti.Location = New System.Drawing.Point(233, 12)
        Me.radSessMulti.Name = "radSessMulti"
        Me.radSessMulti.Properties.AutoWidth = True
        Me.radSessMulti.Properties.Caption = "Multi-Session (Daily)"
        Me.radSessMulti.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radSessMulti.Properties.RadioGroupIndex = 1
        Me.radSessMulti.Size = New System.Drawing.Size(118, 19)
        Me.radSessMulti.TabIndex = 2
        Me.radSessMulti.TabStop = False
        Me.radSessMulti.ToolTip = "Multiple Sessions per Day (used to be called Daily Mode)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "e.g. Breakfast Club & A" & _
    "fterschool Club"
        Me.radSessMulti.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.radSessMulti.ToolTipTitle = "Multi-Session"
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(10, 14)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(67, 15)
        Me.CareLabel3.TabIndex = 0
        Me.CareLabel3.Text = "Pattern Type"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblWednesday
        '
        Me.lblWednesday.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWednesday.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblWednesday.Location = New System.Drawing.Point(10, 87)
        Me.lblWednesday.Name = "lblWednesday"
        Me.lblWednesday.Size = New System.Drawing.Size(61, 15)
        Me.lblWednesday.TabIndex = 4
        Me.lblWednesday.Text = "Wednesday"
        Me.lblWednesday.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblThursday
        '
        Me.lblThursday.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblThursday.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblThursday.Location = New System.Drawing.Point(10, 113)
        Me.lblThursday.Name = "lblThursday"
        Me.lblThursday.Size = New System.Drawing.Size(49, 15)
        Me.lblThursday.TabIndex = 6
        Me.lblThursday.Text = "Thursday"
        Me.lblThursday.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFriday
        '
        Me.lblFriday.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFriday.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblFriday.Location = New System.Drawing.Point(10, 139)
        Me.lblFriday.Name = "lblFriday"
        Me.lblFriday.Size = New System.Drawing.Size(32, 15)
        Me.lblFriday.TabIndex = 8
        Me.lblFriday.Text = "Friday"
        Me.lblFriday.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMonday
        '
        Me.lblMonday.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMonday.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblMonday.Location = New System.Drawing.Point(10, 35)
        Me.lblMonday.Name = "lblMonday"
        Me.lblMonday.Size = New System.Drawing.Size(44, 15)
        Me.lblMonday.TabIndex = 0
        Me.lblMonday.Text = "Monday"
        Me.lblMonday.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTuesday
        '
        Me.lblTuesday.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTuesday.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblTuesday.Location = New System.Drawing.Point(10, 61)
        Me.lblTuesday.Name = "lblTuesday"
        Me.lblTuesday.Size = New System.Drawing.Size(44, 15)
        Me.lblTuesday.TabIndex = 2
        Me.lblTuesday.Text = "Tuesday"
        Me.lblTuesday.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSaturday
        '
        Me.lblSaturday.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSaturday.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblSaturday.Location = New System.Drawing.Point(10, 165)
        Me.lblSaturday.Name = "lblSaturday"
        Me.lblSaturday.Size = New System.Drawing.Size(46, 15)
        Me.lblSaturday.TabIndex = 10
        Me.lblSaturday.Text = "Saturday"
        Me.lblSaturday.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSunday
        '
        Me.lblSunday.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSunday.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblSunday.Location = New System.Drawing.Point(10, 191)
        Me.lblSunday.Name = "lblSunday"
        Me.lblSunday.Size = New System.Drawing.Size(39, 15)
        Me.lblSunday.TabIndex = 12
        Me.lblSunday.Text = "Sunday"
        Me.lblSunday.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbxWeekly
        '
        Me.gbxWeekly.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.gbxWeekly.Controls.Add(Me.tcSunday)
        Me.gbxWeekly.Controls.Add(Me.tcSaturday)
        Me.gbxWeekly.Controls.Add(Me.tcFriday)
        Me.gbxWeekly.Controls.Add(Me.tcThursday)
        Me.gbxWeekly.Controls.Add(Me.tcWednesday)
        Me.gbxWeekly.Controls.Add(Me.tcTuesday)
        Me.gbxWeekly.Controls.Add(Me.tcMonday)
        Me.gbxWeekly.Controls.Add(Me.lblSunday)
        Me.gbxWeekly.Controls.Add(Me.lblSaturday)
        Me.gbxWeekly.Controls.Add(Me.lblTuesday)
        Me.gbxWeekly.Controls.Add(Me.lblMonday)
        Me.gbxWeekly.Controls.Add(Me.lblFriday)
        Me.gbxWeekly.Controls.Add(Me.lblThursday)
        Me.gbxWeekly.Controls.Add(Me.lblWednesday)
        Me.gbxWeekly.Location = New System.Drawing.Point(12, 109)
        Me.gbxWeekly.Name = "gbxWeekly"
        Me.gbxWeekly.Size = New System.Drawing.Size(860, 216)
        Me.gbxWeekly.TabIndex = 5
        Me.gbxWeekly.Text = "Week Mode"
        '
        'grpDuplicate
        '
        Me.grpDuplicate.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.grpDuplicate.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.grpDuplicate.Appearance.Options.UseFont = True
        Me.grpDuplicate.Controls.Add(Me.TableLayoutPanel1)
        Me.grpDuplicate.Location = New System.Drawing.Point(12, 331)
        Me.grpDuplicate.Name = "grpDuplicate"
        Me.grpDuplicate.Size = New System.Drawing.Size(860, 71)
        Me.grpDuplicate.TabIndex = 6
        Me.grpDuplicate.Text = "Duplicate Pattern"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 5
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.lblDFriday, 4, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblDThursday, 3, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblDWednesday, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblDTuesday, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.chkMonday, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.chkTuesday, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.chkWednesday, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.chkThursday, 3, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.chkFriday, 4, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.lblDMonday, 0, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(11, 23)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New RowStyle(SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New RowStyle(SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(839, 44)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'lblDFriday
        '
        Me.lblDFriday.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblDFriday.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lblDFriday.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblDFriday.Dock = DockStyle.Fill
        Me.lblDFriday.Location = New System.Drawing.Point(671, 3)
        Me.lblDFriday.Name = "lblDFriday"
        Me.lblDFriday.Size = New System.Drawing.Size(165, 16)
        Me.lblDFriday.TabIndex = 8
        Me.lblDFriday.Text = "Friday"
        Me.lblDFriday.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDThursday
        '
        Me.lblDThursday.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblDThursday.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lblDThursday.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblDThursday.Dock = DockStyle.Fill
        Me.lblDThursday.Location = New System.Drawing.Point(504, 3)
        Me.lblDThursday.Name = "lblDThursday"
        Me.lblDThursday.Size = New System.Drawing.Size(161, 16)
        Me.lblDThursday.TabIndex = 6
        Me.lblDThursday.Text = "Thursday"
        Me.lblDThursday.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDWednesday
        '
        Me.lblDWednesday.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblDWednesday.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lblDWednesday.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblDWednesday.Dock = DockStyle.Fill
        Me.lblDWednesday.Location = New System.Drawing.Point(337, 3)
        Me.lblDWednesday.Name = "lblDWednesday"
        Me.lblDWednesday.Size = New System.Drawing.Size(161, 16)
        Me.lblDWednesday.TabIndex = 4
        Me.lblDWednesday.Text = "Wednesday"
        Me.lblDWednesday.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDTuesday
        '
        Me.lblDTuesday.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblDTuesday.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lblDTuesday.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblDTuesday.Dock = DockStyle.Fill
        Me.lblDTuesday.Location = New System.Drawing.Point(170, 3)
        Me.lblDTuesday.Name = "lblDTuesday"
        Me.lblDTuesday.Size = New System.Drawing.Size(161, 16)
        Me.lblDTuesday.TabIndex = 2
        Me.lblDTuesday.Text = "Tuesday"
        Me.lblDTuesday.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkMonday
        '
        Me.chkMonday.Dock = DockStyle.Fill
        Me.chkMonday.EnterMoveNextControl = True
        Me.chkMonday.Location = New System.Drawing.Point(3, 25)
        Me.chkMonday.Name = "chkMonday"
        Me.chkMonday.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkMonday.Properties.Appearance.Options.UseFont = True
        Me.chkMonday.Properties.AutoHeight = False
        Me.chkMonday.Properties.Caption = ""
        Me.chkMonday.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkMonday.Size = New System.Drawing.Size(161, 16)
        Me.chkMonday.TabIndex = 1
        Me.chkMonday.Tag = ""
        '
        'chkTuesday
        '
        Me.chkTuesday.Dock = DockStyle.Fill
        Me.chkTuesday.EnterMoveNextControl = True
        Me.chkTuesday.Location = New System.Drawing.Point(170, 25)
        Me.chkTuesday.Name = "chkTuesday"
        Me.chkTuesday.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkTuesday.Properties.Appearance.Options.UseFont = True
        Me.chkTuesday.Properties.AutoHeight = False
        Me.chkTuesday.Properties.Caption = ""
        Me.chkTuesday.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkTuesday.Size = New System.Drawing.Size(161, 16)
        Me.chkTuesday.TabIndex = 3
        Me.chkTuesday.Tag = ""
        '
        'chkWednesday
        '
        Me.chkWednesday.Dock = DockStyle.Fill
        Me.chkWednesday.EnterMoveNextControl = True
        Me.chkWednesday.Location = New System.Drawing.Point(337, 25)
        Me.chkWednesday.Name = "chkWednesday"
        Me.chkWednesday.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkWednesday.Properties.Appearance.Options.UseFont = True
        Me.chkWednesday.Properties.AutoHeight = False
        Me.chkWednesday.Properties.Caption = ""
        Me.chkWednesday.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkWednesday.Size = New System.Drawing.Size(161, 16)
        Me.chkWednesday.TabIndex = 5
        Me.chkWednesday.Tag = ""
        '
        'chkThursday
        '
        Me.chkThursday.Dock = DockStyle.Fill
        Me.chkThursday.EnterMoveNextControl = True
        Me.chkThursday.Location = New System.Drawing.Point(504, 25)
        Me.chkThursday.Name = "chkThursday"
        Me.chkThursday.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkThursday.Properties.Appearance.Options.UseFont = True
        Me.chkThursday.Properties.AutoHeight = False
        Me.chkThursday.Properties.Caption = ""
        Me.chkThursday.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkThursday.Size = New System.Drawing.Size(161, 16)
        Me.chkThursday.TabIndex = 7
        Me.chkThursday.Tag = ""
        '
        'chkFriday
        '
        Me.chkFriday.Dock = DockStyle.Fill
        Me.chkFriday.EnterMoveNextControl = True
        Me.chkFriday.Location = New System.Drawing.Point(671, 25)
        Me.chkFriday.Name = "chkFriday"
        Me.chkFriday.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkFriday.Properties.Appearance.Options.UseFont = True
        Me.chkFriday.Properties.AutoHeight = False
        Me.chkFriday.Properties.Caption = ""
        Me.chkFriday.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkFriday.Size = New System.Drawing.Size(165, 16)
        Me.chkFriday.TabIndex = 9
        Me.chkFriday.Tag = ""
        '
        'lblDMonday
        '
        Me.lblDMonday.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblDMonday.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lblDMonday.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblDMonday.Dock = DockStyle.Fill
        Me.lblDMonday.Location = New System.Drawing.Point(3, 3)
        Me.lblDMonday.Name = "lblDMonday"
        Me.lblDMonday.Size = New System.Drawing.Size(161, 16)
        Me.lblDMonday.TabIndex = 0
        Me.lblDMonday.Text = "Monday"
        Me.lblDMonday.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupControl3
        '
        Me.GroupControl3.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl3.Controls.Add(Me.radAdditional)
        Me.GroupControl3.Controls.Add(Me.radRecurring)
        Me.GroupControl3.Controls.Add(Me.radOverride)
        Me.GroupControl3.Location = New System.Drawing.Point(377, 12)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.ShowCaption = False
        Me.GroupControl3.Size = New System.Drawing.Size(352, 42)
        Me.GroupControl3.TabIndex = 1
        '
        'radAdditional
        '
        Me.radAdditional.Location = New System.Drawing.Point(231, 12)
        Me.radAdditional.Name = "radAdditional"
        Me.radAdditional.Properties.AutoWidth = True
        Me.radAdditional.Properties.Caption = "Additional Booking"
        Me.radAdditional.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radAdditional.Properties.RadioGroupIndex = 1
        Me.radAdditional.Size = New System.Drawing.Size(109, 19)
        Me.radAdditional.TabIndex = 2
        Me.radAdditional.TabStop = False
        Me.radAdditional.ToolTip = resources.GetString("radAdditional.ToolTip")
        Me.radAdditional.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.radAdditional.ToolTipTitle = "Additional Booking"
        '
        'radRecurring
        '
        Me.radRecurring.EditValue = True
        Me.radRecurring.Location = New System.Drawing.Point(9, 12)
        Me.radRecurring.Name = "radRecurring"
        Me.radRecurring.Properties.AutoWidth = True
        Me.radRecurring.Properties.Caption = "Recurring Pattern"
        Me.radRecurring.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radRecurring.Properties.RadioGroupIndex = 1
        Me.radRecurring.Size = New System.Drawing.Size(107, 19)
        Me.radRecurring.TabIndex = 0
        Me.radRecurring.ToolTip = "These patterns recur until another pattern supersedes it." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "If you wish to stop " & _
    "a recurring pattern - create a blank pattern."
        Me.radRecurring.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.radRecurring.ToolTipTitle = "Recurring Pattern"
        '
        'radOverride
        '
        Me.radOverride.Location = New System.Drawing.Point(122, 12)
        Me.radOverride.Name = "radOverride"
        Me.radOverride.Properties.AutoWidth = True
        Me.radOverride.Properties.Caption = "Override Pattern"
        Me.radOverride.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radOverride.Properties.RadioGroupIndex = 1
        Me.radOverride.Size = New System.Drawing.Size(103, 19)
        Me.radOverride.TabIndex = 1
        Me.radOverride.TabStop = False
        Me.radOverride.ToolTip = resources.GetString("radOverride.ToolTip")
        Me.radOverride.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.radOverride.ToolTipTitle = "Override Pattern"
        '
        'gbxScope
        '
        Me.gbxScope.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.gbxScope.Controls.Add(Me.cbxScope)
        Me.gbxScope.Controls.Add(Me.CareLabel1)
        Me.gbxScope.Location = New System.Drawing.Point(477, 60)
        Me.gbxScope.Name = "gbxScope"
        Me.gbxScope.ShowCaption = False
        Me.gbxScope.Size = New System.Drawing.Size(395, 43)
        Me.gbxScope.TabIndex = 4
        '
        'cbxScope
        '
        Me.cbxScope.AllowBlank = False
        Me.cbxScope.DataSource = Nothing
        Me.cbxScope.DisplayMember = Nothing
        Me.cbxScope.EnterMoveNextControl = True
        Me.cbxScope.Location = New System.Drawing.Point(68, 11)
        Me.cbxScope.Name = "cbxScope"
        Me.cbxScope.Properties.AccessibleName = "Gender"
        Me.cbxScope.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxScope.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxScope.Properties.Appearance.Options.UseFont = True
        Me.cbxScope.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxScope.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxScope.SelectedValue = Nothing
        Me.cbxScope.Size = New System.Drawing.Size(317, 22)
        Me.cbxScope.TabIndex = 1
        Me.cbxScope.Tag = ""
        Me.cbxScope.ValueMember = Nothing
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(11, 14)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(51, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Recurring"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel1.ToolTip = "Double Click this label to default the Week Commencing date to the Start Date."
        Me.CareLabel1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.CareLabel1.ToolTipTitle = "Default Start Date"
        '
        'GroupControl4
        '
        Me.GroupControl4.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl4.Appearance.Options.UseFont = True
        Me.GroupControl4.Controls.Add(Me.txtComments)
        Me.GroupControl4.Location = New System.Drawing.Point(12, 408)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(860, 195)
        Me.GroupControl4.TabIndex = 7
        Me.GroupControl4.Text = "Comments"
        '
        'txtComments
        '
        Me.txtComments.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtComments.Location = New System.Drawing.Point(10, 24)
        Me.txtComments.Name = "txtComments"
        Me.txtComments.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtComments.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtComments, True)
        Me.txtComments.Size = New System.Drawing.Size(840, 166)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtComments, OptionsSpelling1)
        Me.txtComments.TabIndex = 0
        '
        'btnCopy
        '
        Me.btnCopy.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnCopy.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCopy.Appearance.Options.UseFont = True
        Me.btnCopy.Location = New System.Drawing.Point(12, 609)
        Me.btnCopy.Name = "btnCopy"
        Me.btnCopy.Size = New System.Drawing.Size(85, 25)
        Me.btnCopy.TabIndex = 8
        Me.btnCopy.Text = "Copy Tariff"
        '
        'btnPaste
        '
        Me.btnPaste.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnPaste.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnPaste.Appearance.Options.UseFont = True
        Me.btnPaste.Location = New System.Drawing.Point(103, 609)
        Me.btnPaste.Name = "btnPaste"
        Me.btnPaste.Size = New System.Drawing.Size(85, 25)
        Me.btnPaste.TabIndex = 9
        Me.btnPaste.Text = "Paste Tariff"
        '
        'GroupControl6
        '
        Me.GroupControl6.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl6.Controls.Add(Me.radBooked)
        Me.GroupControl6.Controls.Add(Me.radWaiting)
        Me.GroupControl6.Location = New System.Drawing.Point(735, 12)
        Me.GroupControl6.Name = "GroupControl6"
        Me.GroupControl6.ShowCaption = False
        Me.GroupControl6.Size = New System.Drawing.Size(137, 42)
        Me.GroupControl6.TabIndex = 2
        '
        'radBooked
        '
        Me.radBooked.EditValue = True
        Me.radBooked.Location = New System.Drawing.Point(9, 12)
        Me.radBooked.Name = "radBooked"
        Me.radBooked.Properties.AutoWidth = True
        Me.radBooked.Properties.Caption = "Booked"
        Me.radBooked.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radBooked.Properties.RadioGroupIndex = 1
        Me.radBooked.Size = New System.Drawing.Size(57, 19)
        Me.radBooked.TabIndex = 0
        Me.radBooked.ToolTip = "Booked patterns are the system default for Current/Active children." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "If you are s" & _
    "peculatively adding a booking, use the Waiting option." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.radBooked.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.radBooked.ToolTipTitle = "Booked"
        '
        'radWaiting
        '
        Me.radWaiting.Location = New System.Drawing.Point(72, 12)
        Me.radWaiting.Name = "radWaiting"
        Me.radWaiting.Properties.AutoWidth = True
        Me.radWaiting.Properties.Caption = "Waiting"
        Me.radWaiting.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radWaiting.Properties.RadioGroupIndex = 1
        Me.radWaiting.Size = New System.Drawing.Size(58, 19)
        Me.radWaiting.TabIndex = 1
        Me.radWaiting.TabStop = False
        Me.radWaiting.ToolTip = resources.GetString("radWaiting.ToolTip")
        Me.radWaiting.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.radWaiting.ToolTipTitle = "Waiting"
        '
        'tcSunday
        '
        Me.tcSunday.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.tcSunday.BoltOns = ""
        Me.tcSunday.ComboValue = Nothing
        Me.tcSunday.Enabled = False
        Me.tcSunday.Location = New System.Drawing.Point(180, 186)
        Me.tcSunday.Name = "tcSunday"
        Me.tcSunday.PopulateMode = Nursery.TariffControl.EnumPopulateMode.Daily
        Me.tcSunday.SelectedIndex = -1
        Me.tcSunday.Size = New System.Drawing.Size(670, 20)
        Me.tcSunday.TabIndex = 13
        Me.tcSunday.Value1 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tcSunday.Value2 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tcSunday.ValueFEEE = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'tcSaturday
        '
        Me.tcSaturday.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.tcSaturday.BoltOns = ""
        Me.tcSaturday.ComboValue = Nothing
        Me.tcSaturday.Enabled = False
        Me.tcSaturday.Location = New System.Drawing.Point(180, 160)
        Me.tcSaturday.Name = "tcSaturday"
        Me.tcSaturday.PopulateMode = Nursery.TariffControl.EnumPopulateMode.Daily
        Me.tcSaturday.SelectedIndex = -1
        Me.tcSaturday.Size = New System.Drawing.Size(670, 20)
        Me.tcSaturday.TabIndex = 11
        Me.tcSaturday.Value1 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tcSaturday.Value2 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tcSaturday.ValueFEEE = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'tcFriday
        '
        Me.tcFriday.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.tcFriday.BoltOns = ""
        Me.tcFriday.ComboValue = Nothing
        Me.tcFriday.Enabled = False
        Me.tcFriday.Location = New System.Drawing.Point(180, 134)
        Me.tcFriday.Name = "tcFriday"
        Me.tcFriday.PopulateMode = Nursery.TariffControl.EnumPopulateMode.Daily
        Me.tcFriday.SelectedIndex = -1
        Me.tcFriday.Size = New System.Drawing.Size(670, 20)
        Me.tcFriday.TabIndex = 9
        Me.tcFriday.Value1 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tcFriday.Value2 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tcFriday.ValueFEEE = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'tcThursday
        '
        Me.tcThursday.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.tcThursday.BoltOns = ""
        Me.tcThursday.ComboValue = Nothing
        Me.tcThursday.Enabled = False
        Me.tcThursday.Location = New System.Drawing.Point(180, 108)
        Me.tcThursday.Name = "tcThursday"
        Me.tcThursday.PopulateMode = Nursery.TariffControl.EnumPopulateMode.Daily
        Me.tcThursday.SelectedIndex = -1
        Me.tcThursday.Size = New System.Drawing.Size(670, 20)
        Me.tcThursday.TabIndex = 7
        Me.tcThursday.Value1 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tcThursday.Value2 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tcThursday.ValueFEEE = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'tcWednesday
        '
        Me.tcWednesday.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.tcWednesday.BoltOns = ""
        Me.tcWednesday.ComboValue = Nothing
        Me.tcWednesday.Enabled = False
        Me.tcWednesday.Location = New System.Drawing.Point(180, 82)
        Me.tcWednesday.Name = "tcWednesday"
        Me.tcWednesday.PopulateMode = Nursery.TariffControl.EnumPopulateMode.Daily
        Me.tcWednesday.SelectedIndex = -1
        Me.tcWednesday.Size = New System.Drawing.Size(670, 20)
        Me.tcWednesday.TabIndex = 5
        Me.tcWednesday.Value1 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tcWednesday.Value2 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tcWednesday.ValueFEEE = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'tcTuesday
        '
        Me.tcTuesday.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.tcTuesday.BoltOns = ""
        Me.tcTuesday.ComboValue = Nothing
        Me.tcTuesday.Enabled = False
        Me.tcTuesday.Location = New System.Drawing.Point(180, 56)
        Me.tcTuesday.Name = "tcTuesday"
        Me.tcTuesday.PopulateMode = Nursery.TariffControl.EnumPopulateMode.Daily
        Me.tcTuesday.SelectedIndex = -1
        Me.tcTuesday.Size = New System.Drawing.Size(670, 20)
        Me.tcTuesday.TabIndex = 3
        Me.tcTuesday.Value1 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tcTuesday.Value2 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tcTuesday.ValueFEEE = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'tcMonday
        '
        Me.tcMonday.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.tcMonday.BoltOns = ""
        Me.tcMonday.ComboValue = Nothing
        Me.tcMonday.Enabled = False
        Me.tcMonday.Location = New System.Drawing.Point(180, 30)
        Me.tcMonday.Name = "tcMonday"
        Me.tcMonday.PopulateMode = Nursery.TariffControl.EnumPopulateMode.Daily
        Me.tcMonday.SelectedIndex = -1
        Me.tcMonday.Size = New System.Drawing.Size(670, 20)
        Me.tcMonday.TabIndex = 1
        Me.tcMonday.Value1 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tcMonday.Value2 = New Decimal(New Integer() {0, 0, 0, 0})
        Me.tcMonday.ValueFEEE = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'frmBookingPattern
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.CancelButton = Me.btnAdvCancel
        Me.ClientSize = New System.Drawing.Size(884, 641)
        Me.Controls.Add(Me.GroupControl6)
        Me.Controls.Add(Me.btnPaste)
        Me.Controls.Add(Me.btnCopy)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.gbxScope)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.grpDuplicate)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.btnAdvOK)
        Me.Controls.Add(Me.gbxWeekly)
        Me.Controls.Add(Me.btnAdvCancel)
        Me.Controls.Add(Me.GroupControl1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBookingPattern"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = ""
        CType(Me.cdtWC.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtWC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.radSessSingle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radSessMulti.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxWeekly, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxWeekly.ResumeLayout(False)
        Me.gbxWeekly.PerformLayout()
        CType(Me.grpDuplicate, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpDuplicate.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.chkMonday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkTuesday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkWednesday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkThursday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkFriday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.radAdditional.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radRecurring.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radOverride.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxScope, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxScope.ResumeLayout(False)
        Me.gbxScope.PerformLayout()
        CType(Me.cbxScope.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        CType(Me.txtComments.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl6.ResumeLayout(False)
        Me.GroupControl6.PerformLayout()
        CType(Me.radBooked.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radWaiting.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnAdvOK As Care.Controls.CareButton
    Friend WithEvents btnAdvCancel As Care.Controls.CareButton
    Friend WithEvents cdtWC As Care.Controls.CareDateTime
    Friend WithEvents lblWC As Care.Controls.CareLabel
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents lblMustBeMonday As Care.Controls.CareLabel
    Friend WithEvents lblWednesday As Care.Controls.CareLabel
    Friend WithEvents lblThursday As Care.Controls.CareLabel
    Friend WithEvents lblFriday As Care.Controls.CareLabel
    Friend WithEvents lblMonday As Care.Controls.CareLabel
    Friend WithEvents lblTuesday As Care.Controls.CareLabel
    Friend WithEvents lblSaturday As Care.Controls.CareLabel
    Friend WithEvents lblSunday As Care.Controls.CareLabel
    Friend WithEvents tcMonday As Nursery.TariffControl
    Friend WithEvents tcTuesday As Nursery.TariffControl
    Friend WithEvents tcWednesday As Nursery.TariffControl
    Friend WithEvents tcThursday As Nursery.TariffControl
    Friend WithEvents tcFriday As Nursery.TariffControl
    Friend WithEvents tcSaturday As Nursery.TariffControl
    Friend WithEvents tcSunday As Nursery.TariffControl
    Friend WithEvents gbxWeekly As DevExpress.XtraEditors.GroupControl
    Friend WithEvents radSessSingle As Care.Controls.CareRadioButton
    Friend WithEvents radSessMulti As Care.Controls.CareRadioButton
    Friend WithEvents grpDuplicate As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents lblDFriday As Care.Controls.CareLabel
    Friend WithEvents lblDThursday As Care.Controls.CareLabel
    Friend WithEvents lblDWednesday As Care.Controls.CareLabel
    Friend WithEvents lblDTuesday As Care.Controls.CareLabel
    Friend WithEvents chkMonday As Care.Controls.CareCheckBox
    Friend WithEvents chkTuesday As Care.Controls.CareCheckBox
    Friend WithEvents chkWednesday As Care.Controls.CareCheckBox
    Friend WithEvents chkThursday As Care.Controls.CareCheckBox
    Friend WithEvents chkFriday As Care.Controls.CareCheckBox
    Friend WithEvents lblDMonday As Care.Controls.CareLabel
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents radRecurring As Care.Controls.CareRadioButton
    Friend WithEvents radOverride As Care.Controls.CareRadioButton
    Friend WithEvents gbxScope As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents radAdditional As Care.Controls.CareRadioButton
    Friend WithEvents cbxScope As Care.Controls.CareComboBox
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtComments As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents btnCopy As Care.Controls.CareButton
    Friend WithEvents btnPaste As Care.Controls.CareButton
    Friend WithEvents GroupControl6 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents radBooked As Care.Controls.CareRadioButton
    Friend WithEvents radWaiting As Care.Controls.CareRadioButton
End Class
