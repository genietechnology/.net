﻿

Imports Care.Global
Imports System.Windows.Forms
Imports Care.Shared
Imports Care.Data
Imports DevExpress.XtraGrid.Views.Base

Public Class frmChildFind

    Public ReadOnly Property SelectedRecordID As String
        Get
            Return m_SelectedRecordID
        End Get
    End Property

    Private m_Loading As Boolean = True
    Private m_SelectedRecordID As String = ""
    Private m_GridLayout As New Care.Controls.Business.AppLayout
    Private m_LoadedAlready As Boolean = False
    Private m_LayoutPresent As Boolean = False
    Private m_LastSearchText As String = ""
    Private m_UserHasExclusions As Boolean = False

    Private Sub frmChildFind_Load(sender As Object, e As EventArgs) Handles Me.Load

        m_UserHasExclusions = Business.Site.UserHasExclusions

        If Not m_LoadedAlready Then

            m_LoadedAlready = True

            SetupLists()

            If Not LoadPersonality() Then
                Me.CenterToScreen()
            End If

        End If

    End Sub

    Private Sub frmChildFind_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        If m_Loading Then
            DoSearch()
            m_Loading = False
        End If

        If txtCriteria.Text.Length > 0 Then
            txtCriteria.SelectAll()
        End If

        txtCriteria.Focus()
        Session.CursorDefault()

    End Sub

    Private Sub SetupLists()

        cbxPreset.Clear()
        cbxPreset.PopulateStart()
        cbxPreset.AddItem("Current Children Only")
        cbxPreset.AddItem("Starters in the next 30 days")
        cbxPreset.AddItem("Confirmed Leavers in the next 30 days")
        cbxPreset.AddItem("Estimated Leavers in the next 30 days")
        cbxPreset.AddItem("Children left in the last 30 days")
        cbxPreset.AddItem("Children left in the last 90 days")
        cbxPreset.AddItem("Waiting Children Only")
        cbxPreset.AddItem("Leavers Only")
        cbxPreset.AddItem("Everyone")
        cbxPreset.PopulateEnd()
        cbxPreset.SelectedIndex = 0

        If m_UserHasExclusions Then
            cbxSite.PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)
            cbxSite.SelectedIndex = 0
        Else
            cbxSite.AllowBlank = True
            cbxSite.PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)
            cbxSite.SelectedIndex = -1
        End If

        cbxColumnSets.Properties.Items.Add(New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Booking Indicators"))
        cbxColumnSets.Properties.Items.Add(New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Start & End Dates"))
        cbxColumnSets.Properties.Items.Add(New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Room Moves"))
        cbxColumnSets.Properties.Items.Add(New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Finance"))
        cbxColumnSets.Properties.Items.Add(New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Funding"))
        cbxColumnSets.Properties.Items.Add(New DevExpress.XtraEditors.Controls.CheckedListBoxItem("School & Refs"))

        cbxAllergyAndIncidents.Clear()
        cbxAllergyAndIncidents.PopulateStart()
        cbxAllergyAndIncidents.AddItem("No Filter")
        cbxAllergyAndIncidents.AddItem("with Allergies")
        cbxAllergyAndIncidents.AddItem("with Dietary Requirements")
        cbxAllergyAndIncidents.PopulateEnd()
        cbxAllergyAndIncidents.SelectedIndex = 0

        cbxSort.Clear()
        cbxSort.PopulateStart()
        cbxSort.AddItem("Forename, Surname")
        cbxSort.AddItem("Surname, Forename")
        cbxSort.AddItem("Youngest > Oldest")
        cbxSort.AddItem("Oldest > Youngest")
        cbxSort.PopulateEnd()
        cbxSort.SelectedIndex = 0

        cbxPhotos.Clear()
        cbxPhotos.PopulateStart()
        cbxPhotos.AddItem("Hide")
        cbxPhotos.AddItem("Show")
        cbxPhotos.PopulateEnd()
        cbxPhotos.SelectedIndex = 0

        For Each _Row As DataRow In Care.Shared.ListHandler.ReturnItems("Allergy Matrix")
            cbxAdvAllergy.Properties.Items.Add(New DevExpress.XtraEditors.Controls.CheckedListBoxItem(_Row.Item(0).ToString, _Row.Item(1).ToString))
        Next

        For Each _Row As DataRow In Care.Shared.ListHandler.ReturnItems("Consent")
            cbxAdvConsent.Properties.Items.Add(New DevExpress.XtraEditors.Controls.CheckedListBoxItem(_Row.Item(0).ToString, _Row.Item(1).ToString))
        Next

        ControlHandler.PopulateCheckedListBox(cbxTags, GetAllTags)

    End Sub

    Private Function ReturnSelectedItems(ByRef List As DevExpress.XtraEditors.CheckedComboBoxEdit) As String

        Dim _Return As String = ""
        Dim _Separator As String = ""

        For Each _Item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In List.Properties.Items
            If _Item.CheckState = CheckState.Checked Then
                If _Return <> "" Then _Separator = ","
                Dim _Description As String = _Item.Description
                If _Description = "" Then _Description = _Item.Value.ToString
                _Return += _Separator + "'" + _Description + "'"
            End If
        Next

        Return _Return

    End Function

    Private Sub frmChildFind_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        SavePersonality()
    End Sub

    Private Sub SavePersonality()

        Try

            If m_GridLayout.IsNew Then
                m_GridLayout._DateCreated = Now
                m_GridLayout._FormName = Me.Name
                m_GridLayout._GridName = "grdSearch"
                m_GridLayout._UserId = Session.CurrentUser.ID
                m_GridLayout._Name = "Last Search Personality"
                m_GridLayout._Description = "Child Search Screen (Maintained by System)"
                m_GridLayout._Private = True
                m_GridLayout._QueryId = Nothing
            End If

            m_GridLayout._Layout = grdSearch.ConvertLayoutToString
            m_GridLayout.Store()

            Dim _GridLayoutID As String = m_GridLayout._ID.Value.ToString

            SavePreference("CHILDSEARCHLAYOUTID", _GridLayoutID)

            SavePreference("CHILDSEARCHWIDTH", Me.Width.ToString)
            SavePreference("CHILDSEARCHHEIGHT", Me.Height.ToString)
            SavePreference("CHILDSEARCHTOP", Me.Top.ToString)
            SavePreference("CHILDSEARCHLEFT", Me.Left.ToString)

            SavePreference("CHILDSEARCHPRESET", cbxPreset.SelectedIndex.ToString)
            SavePreference("CHILDSEARCHPHOTOS", cbxPhotos.SelectedIndex.ToString)
            SavePreference("CHILDSEARCHSORT", cbxSort.SelectedIndex.ToString)

        Catch ex As Exception
            ErrorHandler.LogExceptionToDatabase(ex, False)
        End Try

    End Sub

    Private Sub SavePreference(ByVal ParamID As String, ByVal ParamValue As String)
        If Care.Shared.UserPreferenceHandler.CheckExists(ParamID) Then
            Care.Shared.UserPreferenceHandler.SetString(ParamID, ParamValue)
        Else
            Care.Shared.UserPreferenceHandler.Create(ParamID, Care.Shared.UserPreferenceHandler.EnumUserPreferenceType.StringType, ParamValue)
        End If
    End Sub

    Private Sub SetIntegerProperty(ByVal ParamID As String, ByRef IntegerProperty As Integer)
        Dim _Value As Integer = Care.Shared.UserPreferenceHandler.ReturnInteger(ParamID)
        If _Value > 0 Then
            IntegerProperty = _Value
        End If
    End Sub

    Private Function LoadPersonality() As Boolean

        SetIntegerProperty("CHILDSEARCHTOP", Me.Top)
        SetIntegerProperty("CHILDSEARCHLEFT", Me.Left)
        SetIntegerProperty("CHILDSEARCHWIDTH", Me.Width)
        SetIntegerProperty("CHILDSEARCHHEIGHT", Me.Height)

        SetIntegerProperty("CHILDSEARCHPRESET", cbxPreset.SelectedIndex)
        SetIntegerProperty("CHILDSEARCHPHOTOS", cbxPhotos.SelectedIndex)
        SetIntegerProperty("CHILDSEARCHSORT", cbxSort.SelectedIndex)

        Return LoadGridLayout()

    End Function

    Private Function LoadGridLayout() As Boolean

        Try

            Dim _LayoutID As String = Care.Shared.UserPreferenceHandler.ReturnString("CHILDSEARCHLAYOUTID")
            If _LayoutID <> "" Then
                m_GridLayout = Care.Controls.Business.AppLayout.RetreiveByID(New Guid(_LayoutID))
                If m_GridLayout IsNot Nothing Then
                    m_LayoutPresent = True
                    grdSearch.LoadLayoutFromString(m_GridLayout._Layout)
                End If
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            ErrorHandler.LogExceptionToDatabase(ex, False)
            Return False
        End Try

    End Function

    Private Sub DoSearch()

        If m_UserHasExclusions AndAlso cbxSite.SelectedIndex < 0 Then
            Exit Sub
        End If

        Me.Cursor = Cursors.WaitCursor

        Dim _SQL As String = ""

        'set fields to select
        '**************************************************************************************************************************************************

        _SQL += "select c.ID,"

        If cbxPhotos.SelectedIndex = 1 Then _SQL += " data as 'photo',"

        _SQL += " forename, surname, status, dob, datediff(month, dob, getdate()) as 'age_months', datediff(day, dob, getdate()) as 'age_days',"

        If Care.Shared.ParameterHandler.ReturnBoolean("USECLASS") Then _SQL += " class as 'Classification',"

        _SQL += " site_name, group_name, keyworker_name, term_only, term_type, allergy_rating, diet_restrict"

        Dim _ColumnSets As String = ReturnSelectedItems(cbxColumnSets)

        If _ColumnSets.Contains("Start & End Dates") Then
            _SQL += ", started as 'Start Date', date_left as 'Leave Date', calc_leave_date as 'Calc. Leave Date', created_stamp as 'Created'"
        End If

        If _ColumnSets.Contains("Room Moves") Then
            _SQL += ", c.move_mode as 'Move Mode', c.move_date as 'Move Date', r.room_name as 'Move Target'"
        End If

        If _ColumnSets.Contains("Finance") Then
            _SQL += ", invoice_due as 'Invoices Due', payment_method as 'Payment Method', payment_ref as 'Payment Ref', invoice_freq as 'Invoice Frequency'"
        End If

        If _ColumnSets.Contains("Funding") Then
            _SQL += ", funding_type as 'Funding Type', funding_ref as 'Funding Ref'"
        End If

        If _ColumnSets.Contains("Booking Indicators") Then
            _SQL += ", monday as 'Mon', tuesday as 'Tue', wednesday as 'Wed', thursday as 'Thu', friday as 'Fri'"
        End If

        If _ColumnSets.Contains("School & Refs") Then
            _SQL += ", s.name as 'School', c.school_ref_1 as 'Ref 1', c.school_ref_2 as 'Ref 2'"
        End If

        _SQL += " from Children c"

        If cbxPhotos.SelectedIndex = 1 Then
            _SQL += " left join AppDocs on AppDocs.ID = c.photo"
        End If

        If _ColumnSets.Contains("Room Moves") Then
            _SQL += " left join SiteRoomRatios rr on rr.ID = c.move_ratio_id"
            _SQL += " left join SiteRooms r on r.id = rr.room_id"
        End If

        If _ColumnSets.Contains("School & Refs") Then
            _SQL += " left join Schools s on s.id = c.school"
        End If

        'filters
        '**************************************************************************************************************************************************

        _SQL += " where status <> '<BLANK>'"

        If cbxPreset.Text = "Current Children Only" Then _SQL += " and status = 'Current'"
        If cbxPreset.Text = "Starters in the next 30 days" Then _SQL += " and started between getdate() and dateadd(day, 30, getdate())"
        If cbxPreset.Text = "Confirmed Leavers in the next 30 days" Then _SQL += " and date_left between getdate() and dateadd(day, 30, getdate())"
        If cbxPreset.Text = "Estimated Leavers in the next 30 days" Then _SQL += " and calc_leave_date between getdate() and dateadd(day, 30, getdate())"
        If cbxPreset.Text = "Children left in the last 30 days" Then _SQL += " and date_left between dateadd(day, -30, getdate()) and getdate()"
        If cbxPreset.Text = "Children left in the last 90 days" Then _SQL += " and date_left between dateadd(day, -90, getdate()) and getdate()"
        If cbxPreset.Text = "Waiting Children Only" Then _SQL += " and status = 'On Waiting List'"
        If cbxPreset.Text = "Leavers Only" Then _SQL += " and status = 'Left'"

        If cbxAllergyAndIncidents.Text = "with Allergies" Then _SQL += " and allergy_rating <> 'None'"
        If cbxAllergyAndIncidents.Text = "with Dietary Requirements" Then _SQL += " and diet_restrict <> 'None'"

        Dim _SelectedAllergies As String = ReturnSelectedItems(cbxAdvAllergy)
        If _SelectedAllergies <> "" Then
            _SQL += " and exists ("
            _SQL += " select att.attribute from ChildAttrib att"
            _SQL += " where att.child_id = c.ID"
            _SQL += " and att.category = 'Allergies'"
            _SQL += " and att.attribute in (" + _SelectedAllergies + ")"
            _SQL += " )"
        End If

        Dim _SelectedConsent As String = ReturnSelectedItems(cbxAdvConsent)
        If _SelectedConsent <> "" Then
            _SQL += " and exists ("
            _SQL += " select con.consent from ChildConsent con"
            _SQL += " where con.child_id = c.ID"
            _SQL += " and con.given = 1"
            _SQL += " and con.consent in (" + _SelectedConsent + ")"
            _SQL += " )"
        End If

        Dim _SelectedTags As String = ReturnSelectedItems(cbxTags)
        If _SelectedTags <> "" Then
            _SQL += " and c.tags in (" + _SelectedTags + ")"
        End If

        If txtCriteria.Text <> "" Then
            _SQL += " and c.fullname like '%" + txtCriteria.Text + "%'"
        End If

        If cbxSite.Text <> "" Then
            _SQL += " and c.site_name = '" + cbxSite.Text + "'"
        End If

        'order by
        '**************************************************************************************************************************************************

        If cbxSort.Text = "Forename, Surname" Then _SQL += " order by forename, surname, age_days"
        If cbxSort.Text = "Surname, Forename" Then _SQL += " order by surname, forename, age_days"
        If cbxSort.Text = "Youngest > Oldest" Then _SQL += " order by age_days"
        If cbxSort.Text = "Oldest > Youngest" Then _SQL += " order by age_days desc"

        '**************************************************************************************************************************************************

        grdSearch.Populate(Session.ConnectionString, _SQL)

        If Not m_LayoutPresent Then grdSearch.AutoSizeColumns()

        If cbxPhotos.SelectedIndex = 1 Then
            grdSearch.Columns("photo").Visible = True
            grdSearch.Columns("photo").Width = 100
            grdSearch.UnderlyingGridView.OptionsView.RowAutoHeight = False
            grdSearch.UnderlyingGridView.RowHeight = 100
        Else
            grdSearch.UnderlyingGridView.RowHeight = 0
            grdSearch.UnderlyingGridView.OptionsView.RowAutoHeight = True
        End If

        Me.Cursor = Cursors.Default

    End Sub

#Region "Radios, Checkboxes and Combos"

    Private Sub cbxSort_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSort.SelectedIndexChanged
        If Not m_Loading Then DoSearch()
    End Sub

    Private Sub cbxPreset_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxPreset.SelectedIndexChanged
        If Not m_Loading Then DoSearch()
    End Sub

    Private Sub cbxBookings_SelectedIndexChanged(sender As Object, e As EventArgs)
        If Not m_Loading Then DoSearch()
    End Sub

    Private Sub cbxAllergyAndIncidents_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxAllergyAndIncidents.SelectedIndexChanged
        If Not m_Loading Then DoSearch()
    End Sub

    Private Sub cbxAdvAllergy_EditValueChanged(sender As Object, e As EventArgs) Handles cbxAdvAllergy.EditValueChanged
        If Not m_Loading Then DoSearch()
    End Sub

    Private Sub cbxAdvConsent_EditValueChanged(sender As Object, e As EventArgs) Handles cbxAdvConsent.EditValueChanged
        If Not m_Loading Then DoSearch()
    End Sub

    Private Sub cbxPhotos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxPhotos.SelectedIndexChanged
        If Not m_Loading Then DoSearch()
    End Sub

#End Region

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        If Not m_Loading Then DoSearch()
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        If m_Loading Then Exit Sub
        If CareMessage("Are you sure you want to Clear the search criteria?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Clear") = DialogResult.Yes Then
            ClearFields()
        End If
    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click

        If m_Loading Then Exit Sub

        If CareMessage("Are you sure you want to Reset the Child Search?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Reset") = DialogResult.Yes Then

            ClearFields()

            If m_GridLayout IsNot Nothing AndAlso m_GridLayout._ID.HasValue Then
                Care.Controls.Business.AppLayout.DeleteRecord(m_GridLayout._ID.Value)
            End If

            SavePreference("CHILDSEARCHLAYOUTID", "")

            SavePreference("CHILDSEARCHWIDTH", "")
            SavePreference("CHILDSEARCHHEIGHT", "")
            SavePreference("CHILDSEARCHTOP", "")
            SavePreference("CHILDSEARCHLEFT", "")

            SavePreference("CHILDSEARCHPRESET", "")
            SavePreference("CHILDSEARCHBOOKINGS", "")
            SavePreference("CHILDSEARCHPHOTOS", "")
            SavePreference("CHILDSEARCHSORT", "")

        End If

    End Sub

    Private Sub ClearFields()

        cbxPreset.SelectedIndex = 0
        cbxAllergyAndIncidents.SelectedIndex = 0
        cbxPhotos.SelectedIndex = 0
        cbxSort.SelectedIndex = 0

        txtCriteria.Text = ""
        grdSearch.Clear()

        txtCriteria.Focus()

    End Sub

    Private Sub grdSearch_ColumnsPopulated(sender As Object, e As EventArgs) Handles grdSearch.ColumnsPopulated

        grdSearch.Columns("ID").Visible = False
        grdSearch.Columns("forename").Caption = "Forename"
        grdSearch.Columns("surname").Caption = "Surname"
        grdSearch.Columns("status").Caption = "Status"
        grdSearch.Columns("dob").Caption = "DOB"
        grdSearch.Columns("age_months").Caption = "Age"
        grdSearch.Columns("age_days").Visible = False
        grdSearch.Columns("site_name").Caption = "Site"
        grdSearch.Columns("group_name").Caption = "Group"
        grdSearch.Columns("keyworker_name").Caption = "Keyworker"
        grdSearch.Columns("term_only").Caption = "T.T."
        grdSearch.Columns("term_type").Caption = "Term Type"
        grdSearch.Columns("allergy_rating").Caption = "Allergy Rating"
        grdSearch.Columns("diet_restrict").Caption = "Dietary Req."

    End Sub

    Private Sub grdSearch_GridDoubleClick(sender As Object, e As EventArgs) Handles grdSearch.GridDoubleClick
        If Not m_Loading Then SelectRecord()
    End Sub

    Private Sub SelectRecord()

        If grdSearch.RecordCount > 0 AndAlso grdSearch.CurrentRow IsNot Nothing Then

            m_SelectedRecordID = grdSearch.CurrentRow.Item("ID").ToString

            AuditHandler.LogAudit(AuditHandler.EnumAuditScope.Record, Me.Name, New Guid(m_SelectedRecordID), Me.Text, AuditHandler.EnumAuditStatus.Success)

            Me.DialogResult = DialogResult.OK
            Me.Close()

        End If

    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub grdSearch_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles grdSearch.RowStyle

        If e.RowHandle < 0 Then Exit Sub

        Dim _AllergyRating As String = ""

        Try
            _AllergyRating = grdSearch.UnderlyingGridView.GetRowCellDisplayText(e.RowHandle, "allergy_rating").ToString

        Catch ex As Exception
            ErrorHandler.LogExceptionToDatabase(ex, False)
        End Try

        Select Case _AllergyRating

            Case "Serious"
                e.Appearance.BackColor = txtRed.BackColor

            Case "Moderate"
                e.Appearance.BackColor = txtYellow.BackColor

            Case Else
                e.Appearance.Reset()

        End Select

    End Sub

    Private Sub txtCriteria_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtCriteria.Validating
        If Not m_Loading Then
            If txtCriteria.Text <> m_LastSearchText Then
                DoSearch()
                m_LastSearchText = txtCriteria.Text
            End If
        End If
    End Sub

    Private Sub cbxSite_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSite.SelectedIndexChanged
        If Not m_Loading Then DoSearch()
    End Sub

    Private Sub cbxColumnSets_EditValueChanged(sender As Object, e As EventArgs) Handles cbxColumnSets.EditValueChanged
        If Not m_Loading Then DoSearch()
    End Sub

    Private Function GetAllTags() As List(Of String)

        Dim _Tags As New List(Of String)

        Dim _SQL As String = "select tags from Children where status <> 'Left' and len(tags) > 0"
        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            'loop through each child
            For Each _DR As DataRow In _DT.Rows

                'fetch the tags and split into list of tags
                Dim _ChildTags As List(Of String) = _DR.Item("tags").ToString.Split(CChar(",")).ToList

                'loop through the tags and append to tag list where they are not already in the main list
                For Each _Tag In _ChildTags

                    If Not InList(_Tags, _Tag) Then
                        _Tags.Add(_Tag.Trim)
                    End If

                Next

            Next

        End If

        Return _Tags

    End Function

    Private Function InList(ByRef Tags As List(Of String), ByVal CheckTag As String) As Boolean

        For Each _Tag In Tags
            If _Tag.Trim.ToUpper = CheckTag.Trim.ToUpper Then
                Return True
            End If
        Next

        Return False

    End Function

    Private Sub cbxTags_EditValueChanged(sender As Object, e As EventArgs) Handles cbxTags.EditValueChanged
        If Not m_Loading Then DoSearch()
    End Sub

    Private Sub grdSearch_CustomColumnDisplayText(sender As Object, e As CustomColumnDisplayTextEventArgs) Handles grdSearch.CustomColumnDisplayText

        Try
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.Caption = "Age" AndAlso e.Value IsNot Nothing Then
                If IsNumeric(e.Value) Then
                    e.DisplayText = ValueHandler.AgeYearsMonths(CDbl(e.Value))
                End If
            End If

        Catch ex As Exception
            ErrorHandler.LogExceptionToDatabase(ex, False)
        End Try

    End Sub
End Class
