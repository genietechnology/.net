﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmContact
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Panel1 = New Panel()
        Me.btnOK = New Care.Controls.CareButton(Me.components)
        Me.btnEdit = New Care.Controls.CareButton(Me.components)
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        Me.OpenFileDialog = New OpenFileDialog()
        Me.ctxPopup = New ContextMenuStrip(Me.components)
        Me.mnuFullSize = New ToolStripMenuItem()
        Me.mnuUpload = New ToolStripMenuItem()
        Me.mnuDelete = New ToolStripMenuItem()
        Me.bs = New BindingSource(Me.components)
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.tabGeneral = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl4 = New Care.Controls.CareFrame()
        Me.chkCommMarketing = New Care.Controls.CareCheckBox(Me.components)
        Me.cbxCommMethod = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox8 = New Care.Controls.CareFrame()
        Me.txtVoucherAmount = New Care.Controls.CareTextBox(Me.components)
        Me.chkVoucher = New Care.Controls.CareCheckBox(Me.components)
        Me.cdtVouchDate = New Care.Controls.CareDateTime(Me.components)
        Me.cbxProvider = New Care.Controls.CareComboBox(Me.components)
        Me.Label22 = New Care.Controls.CareLabel(Me.components)
        Me.Label23 = New Care.Controls.CareLabel(Me.components)
        Me.txtVouchRef = New Care.Controls.CareTextBox(Me.components)
        Me.Label25 = New Care.Controls.CareLabel(Me.components)
        Me.Label27 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl3 = New Care.Controls.CareFrame()
        Me.txtEmailJob = New Care.[Shared].CareEmailAddress()
        Me.txtEmail = New Care.[Shared].CareEmailAddress()
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox7 = New Care.Controls.CareFrame()
        Me.chkEmployed = New Care.Controls.CareCheckBox(Me.components)
        Me.txtJobTitle = New Care.Controls.CareTextBox(Me.components)
        Me.Label18 = New Care.Controls.CareLabel(Me.components)
        Me.txtEmployer = New Care.Controls.CareTextBox(Me.components)
        Me.Label20 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox4 = New Care.Controls.CareFrame()
        Me.txtAddress = New Care.Address.CareAddress()
        Me.btnCopyAddress = New Care.Controls.CareButton(Me.components)
        Me.Label14 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl2 = New Care.Controls.CareFrame()
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.txtNI = New Care.Controls.CareTextBox(Me.components)
        Me.cdtDOB = New Care.Controls.CareDateTime(Me.components)
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.chkParent = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.chkCollect = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.Label7 = New Care.Controls.CareLabel(Me.components)
        Me.Label3 = New Care.Controls.CareLabel(Me.components)
        Me.chkPrimary = New Care.Controls.CareCheckBox(Me.components)
        Me.chkEmergency = New Care.Controls.CareCheckBox(Me.components)
        Me.GroupBox6 = New Care.Controls.CareFrame()
        Me.chkInvoiceEmail = New Care.Controls.CareCheckBox(Me.components)
        Me.cbxReportDetail = New Care.Controls.CareComboBox(Me.components)
        Me.Label11 = New Care.Controls.CareLabel(Me.components)
        Me.chkReports = New Care.Controls.CareCheckBox(Me.components)
        Me.Label6 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox5 = New Care.Controls.CareFrame()
        Me.txtSecret = New Care.Controls.CareTextBox(Me.components)
        Me.GroupBox2 = New Care.Controls.CareFrame()
        Me.txtTel = New Care.[Shared].CareTelephoneNumber()
        Me.txtTelJob = New Care.[Shared].CareTelephoneNumber()
        Me.txtMobile = New Care.[Shared].CareTelephoneNumber()
        Me.Label5 = New Care.Controls.CareLabel(Me.components)
        Me.chkSMS = New Care.Controls.CareCheckBox(Me.components)
        Me.Label13 = New Care.Controls.CareLabel(Me.components)
        Me.Label8 = New Care.Controls.CareLabel(Me.components)
        Me.Label9 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox1 = New Care.Controls.CareFrame()
        Me.txtRelationship = New Care.Controls.CareTextBox(Me.components)
        Me.Label4 = New Care.Controls.CareLabel(Me.components)
        Me.txtSurname = New Care.Controls.CareTextBox(Me.components)
        Me.Label2 = New Care.Controls.CareLabel(Me.components)
        Me.txtForename = New Care.Controls.CareTextBox(Me.components)
        Me.Label1 = New Care.Controls.CareLabel(Me.components)
        Me.txtFullname = New Care.Controls.CareTextBox(Me.components)
        Me.Label17 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox3 = New Care.Controls.CareFrame()
        Me.PictureBox = New Care.Controls.PhotoControl()
        Me.tabActivity = New DevExpress.XtraTab.XtraTabPage()
        Me.crm = New Care.[Shared].CRMActivity()
        Me.Panel1.SuspendLayout()
        Me.ctxPopup.SuspendLayout()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.tabGeneral.SuspendLayout()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.chkCommMarketing.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxCommMethod.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox8.SuspendLayout()
        CType(Me.txtVoucherAmount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkVoucher.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtVouchDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtVouchDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxProvider.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtVouchRef.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.GroupBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox7.SuspendLayout()
        CType(Me.chkEmployed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJobTitle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEmployer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.txtNI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDOB.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDOB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.chkParent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkCollect.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkPrimary.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkEmergency.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        CType(Me.chkInvoiceEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxReportDetail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkReports.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        CType(Me.txtSecret.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.chkSMS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtRelationship.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtForename.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFullname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.tabActivity.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnOK)
        Me.Panel1.Controls.Add(Me.btnEdit)
        Me.Panel1.Controls.Add(Me.btnCancel)
        Me.Panel1.Dock = DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 637)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(839, 31)
        Me.Panel1.TabIndex = 1
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(661, 0)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(80, 25)
        Me.btnOK.TabIndex = 0
        Me.btnOK.Tag = ""
        Me.btnOK.Text = "OK"
        '
        'btnEdit
        '
        Me.btnEdit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnEdit.Appearance.Options.UseFont = True
        Me.btnEdit.Location = New System.Drawing.Point(12, 0)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(115, 25)
        Me.btnEdit.TabIndex = 0
        Me.btnEdit.Text = "Edit Contact"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(747, 0)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(80, 25)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Tag = "Y"
        Me.btnCancel.Text = "Cancel"
        '
        'ctxPopup
        '
        Me.ctxPopup.Items.AddRange(New ToolStripItem() {Me.mnuFullSize, Me.mnuUpload, Me.mnuDelete})
        Me.ctxPopup.Name = "ctxPopup"
        Me.ctxPopup.ShowImageMargin = False
        Me.ctxPopup.Size = New System.Drawing.Size(128, 70)
        Me.ctxPopup.Text = "Picture Tasks"
        '
        'mnuFullSize
        '
        Me.mnuFullSize.Name = "mnuFullSize"
        Me.mnuFullSize.Size = New System.Drawing.Size(127, 22)
        Me.mnuFullSize.Text = "View Full Size"
        '
        'mnuUpload
        '
        Me.mnuUpload.Name = "mnuUpload"
        Me.mnuUpload.Size = New System.Drawing.Size(127, 22)
        Me.mnuUpload.Text = "Upload Picture"
        '
        'mnuDelete
        '
        Me.mnuDelete.Name = "mnuDelete"
        Me.mnuDelete.Size = New System.Drawing.Size(127, 22)
        Me.mnuDelete.Text = "Delete Picture"
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XtraTabControl1.Appearance.Options.UseFont = True
        Me.XtraTabControl1.AppearancePage.Header.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.XtraTabControl1.AppearancePage.Header.Options.UseFont = True
        Me.XtraTabControl1.AppearancePage.HeaderActive.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.XtraTabControl1.AppearancePage.HeaderActive.Options.UseFont = True
        Me.XtraTabControl1.AppearancePage.HeaderDisabled.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.XtraTabControl1.AppearancePage.HeaderDisabled.Options.UseFont = True
        Me.XtraTabControl1.AppearancePage.HeaderHotTracked.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.XtraTabControl1.AppearancePage.HeaderHotTracked.Options.UseFont = True
        Me.XtraTabControl1.AppearancePage.PageClient.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.XtraTabControl1.AppearancePage.PageClient.Options.UseFont = True
        Me.XtraTabControl1.Location = New System.Drawing.Point(12, 7)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.tabGeneral
        Me.XtraTabControl1.Size = New System.Drawing.Size(815, 625)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabGeneral, Me.tabActivity})
        '
        'tabGeneral
        '
        Me.tabGeneral.Controls.Add(Me.GroupControl4)
        Me.tabGeneral.Controls.Add(Me.GroupBox8)
        Me.tabGeneral.Controls.Add(Me.GroupControl3)
        Me.tabGeneral.Controls.Add(Me.GroupBox7)
        Me.tabGeneral.Controls.Add(Me.GroupBox4)
        Me.tabGeneral.Controls.Add(Me.GroupControl2)
        Me.tabGeneral.Controls.Add(Me.GroupControl1)
        Me.tabGeneral.Controls.Add(Me.GroupBox6)
        Me.tabGeneral.Controls.Add(Me.GroupBox5)
        Me.tabGeneral.Controls.Add(Me.GroupBox2)
        Me.tabGeneral.Controls.Add(Me.GroupBox1)
        Me.tabGeneral.Controls.Add(Me.GroupBox3)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Size = New System.Drawing.Size(809, 595)
        Me.tabGeneral.Text = "General"
        '
        'GroupControl4
        '
        Me.GroupControl4.AppearanceCaption.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl4.AppearanceCaption.Options.UseFont = True
        Me.GroupControl4.Controls.Add(Me.chkCommMarketing)
        Me.GroupControl4.Controls.Add(Me.cbxCommMethod)
        Me.GroupControl4.Controls.Add(Me.CareLabel7)
        Me.GroupControl4.Controls.Add(Me.CareLabel8)
        Me.GroupControl4.Location = New System.Drawing.Point(419, 400)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(380, 87)
        Me.GroupControl4.TabIndex = 10
        Me.GroupControl4.Text = "Contact Preferences"
        '
        'chkCommMarketing
        '
        Me.chkCommMarketing.EnterMoveNextControl = True
        Me.chkCommMarketing.Location = New System.Drawing.Point(111, 58)
        Me.chkCommMarketing.Name = "chkCommMarketing"
        Me.chkCommMarketing.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCommMarketing.Properties.Appearance.Options.UseFont = True
        Me.chkCommMarketing.Size = New System.Drawing.Size(20, 19)
        Me.chkCommMarketing.TabIndex = 3
        Me.chkCommMarketing.Tag = "AE"
        '
        'cbxCommMethod
        '
        Me.cbxCommMethod.AllowBlank = False
        Me.cbxCommMethod.DataSource = Nothing
        Me.cbxCommMethod.DisplayMember = Nothing
        Me.cbxCommMethod.EnterMoveNextControl = True
        Me.cbxCommMethod.Location = New System.Drawing.Point(111, 32)
        Me.cbxCommMethod.Name = "cbxCommMethod"
        Me.cbxCommMethod.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxCommMethod.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxCommMethod.Properties.Appearance.Options.UseFont = True
        Me.cbxCommMethod.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxCommMethod.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxCommMethod.SelectedValue = Nothing
        Me.cbxCommMethod.Size = New System.Drawing.Size(259, 22)
        Me.cbxCommMethod.TabIndex = 1
        Me.cbxCommMethod.Tag = "AE"
        Me.cbxCommMethod.ValueMember = Nothing
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(8, 60)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(78, 15)
        Me.CareLabel7.TabIndex = 2
        Me.CareLabel7.Text = "Marketing OK?"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(8, 35)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(87, 15)
        Me.CareLabel8.TabIndex = 0
        Me.CareLabel8.Text = "Contact Method"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox8
        '
        Me.GroupBox8.Anchor = AnchorStyles.Top
        Me.GroupBox8.AppearanceCaption.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox8.AppearanceCaption.Options.UseFont = True
        Me.GroupBox8.Controls.Add(Me.txtVoucherAmount)
        Me.GroupBox8.Controls.Add(Me.chkVoucher)
        Me.GroupBox8.Controls.Add(Me.cdtVouchDate)
        Me.GroupBox8.Controls.Add(Me.cbxProvider)
        Me.GroupBox8.Controls.Add(Me.Label22)
        Me.GroupBox8.Controls.Add(Me.Label23)
        Me.GroupBox8.Controls.Add(Me.txtVouchRef)
        Me.GroupBox8.Controls.Add(Me.Label25)
        Me.GroupBox8.Controls.Add(Me.Label27)
        Me.GroupBox8.Location = New System.Drawing.Point(419, 279)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(380, 115)
        Me.GroupBox8.TabIndex = 9
        Me.GroupBox8.Text = "Childcare Vouchers"
        '
        'txtVoucherAmount
        '
        Me.txtVoucherAmount.CharacterCasing = CharacterCasing.Normal
        Me.txtVoucherAmount.EnterMoveNextControl = True
        Me.txtVoucherAmount.Location = New System.Drawing.Point(285, 84)
        Me.txtVoucherAmount.MaxLength = 14
        Me.txtVoucherAmount.Name = "txtVoucherAmount"
        Me.txtVoucherAmount.NumericAllowNegatives = False
        Me.txtVoucherAmount.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtVoucherAmount.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtVoucherAmount.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtVoucherAmount.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVoucherAmount.Properties.Appearance.Options.UseFont = True
        Me.txtVoucherAmount.Properties.Appearance.Options.UseTextOptions = True
        Me.txtVoucherAmount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtVoucherAmount.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtVoucherAmount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtVoucherAmount.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtVoucherAmount.Properties.MaxLength = 14
        Me.txtVoucherAmount.Size = New System.Drawing.Size(85, 22)
        Me.txtVoucherAmount.TabIndex = 9
        Me.txtVoucherAmount.Tag = "AE"
        Me.txtVoucherAmount.TextAlign = HorizontalAlignment.Left
        Me.txtVoucherAmount.ToolTipText = ""
        '
        'chkVoucher
        '
        Me.chkVoucher.EnterMoveNextControl = True
        Me.chkVoucher.Location = New System.Drawing.Point(111, 29)
        Me.chkVoucher.Name = "chkVoucher"
        Me.chkVoucher.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkVoucher.Properties.Appearance.Options.UseFont = True
        Me.chkVoucher.Size = New System.Drawing.Size(20, 19)
        Me.chkVoucher.TabIndex = 1
        Me.chkVoucher.Tag = "AE"
        '
        'cdtVouchDate
        '
        Me.cdtVouchDate.EditValue = New Date(2012, 6, 18, 0, 0, 0, 0)
        Me.cdtVouchDate.EnterMoveNextControl = True
        Me.cdtVouchDate.Location = New System.Drawing.Point(111, 84)
        Me.cdtVouchDate.Name = "cdtVouchDate"
        Me.cdtVouchDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cdtVouchDate.Properties.Appearance.Options.UseFont = True
        Me.cdtVouchDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtVouchDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtVouchDate.Size = New System.Drawing.Size(85, 22)
        Me.cdtVouchDate.TabIndex = 7
        Me.cdtVouchDate.Tag = "AE"
        Me.cdtVouchDate.Value = New Date(2012, 6, 18, 0, 0, 0, 0)
        '
        'cbxProvider
        '
        Me.cbxProvider.AllowBlank = False
        Me.cbxProvider.DataSource = Nothing
        Me.cbxProvider.DisplayMember = Nothing
        Me.cbxProvider.EnterMoveNextControl = True
        Me.cbxProvider.Location = New System.Drawing.Point(137, 28)
        Me.cbxProvider.Name = "cbxProvider"
        Me.cbxProvider.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxProvider.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxProvider.Properties.Appearance.Options.UseFont = True
        Me.cbxProvider.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxProvider.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxProvider.SelectedValue = Nothing
        Me.cbxProvider.Size = New System.Drawing.Size(233, 22)
        Me.cbxProvider.TabIndex = 3
        Me.cbxProvider.Tag = "AE"
        Me.cbxProvider.ValueMember = Nothing
        '
        'Label22
        '
        Me.Label22.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label22.Location = New System.Drawing.Point(8, 87)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(24, 15)
        Me.Label22.TabIndex = 6
        Me.Label22.Text = "Date"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label23
        '
        Me.Label23.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label23.Location = New System.Drawing.Point(235, 87)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(44, 15)
        Me.Label23.TabIndex = 8
        Me.Label23.Text = "Amount"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtVouchRef
        '
        Me.txtVouchRef.CharacterCasing = CharacterCasing.Upper
        Me.txtVouchRef.EnterMoveNextControl = True
        Me.txtVouchRef.Location = New System.Drawing.Point(111, 56)
        Me.txtVouchRef.MaxLength = 20
        Me.txtVouchRef.Name = "txtVouchRef"
        Me.txtVouchRef.NumericAllowNegatives = False
        Me.txtVouchRef.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtVouchRef.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtVouchRef.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtVouchRef.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVouchRef.Properties.Appearance.Options.UseFont = True
        Me.txtVouchRef.Properties.Appearance.Options.UseTextOptions = True
        Me.txtVouchRef.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtVouchRef.Properties.CharacterCasing = CharacterCasing.Upper
        Me.txtVouchRef.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtVouchRef.Properties.MaxLength = 20
        Me.txtVouchRef.Size = New System.Drawing.Size(259, 22)
        Me.txtVouchRef.TabIndex = 5
        Me.txtVouchRef.Tag = "AE"
        Me.txtVouchRef.TextAlign = HorizontalAlignment.Left
        Me.txtVouchRef.ToolTipText = ""
        '
        'Label25
        '
        Me.Label25.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label25.Location = New System.Drawing.Point(8, 59)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(52, 15)
        Me.Label25.TabIndex = 4
        Me.Label25.Text = "Reference"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label27
        '
        Me.Label27.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label27.Location = New System.Drawing.Point(8, 31)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(56, 15)
        Me.Label27.TabIndex = 0
        Me.Label27.Text = "Receiving?"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl3.AppearanceCaption.Options.UseFont = True
        Me.GroupControl3.Controls.Add(Me.txtEmailJob)
        Me.GroupControl3.Controls.Add(Me.txtEmail)
        Me.GroupControl3.Controls.Add(Me.CareLabel4)
        Me.GroupControl3.Controls.Add(Me.CareLabel6)
        Me.GroupControl3.Location = New System.Drawing.Point(9, 493)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(401, 93)
        Me.GroupControl3.TabIndex = 8
        Me.GroupControl3.Text = "Email Addresses"
        '
        'txtEmailJob
        '
        Me.txtEmailJob.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmailJob.Appearance.Options.UseFont = True
        Me.txtEmailJob.Location = New System.Drawing.Point(113, 60)
        Me.txtEmailJob.MaxLength = 100
        Me.txtEmailJob.Name = "txtEmailJob"
        Me.txtEmailJob.NoButton = False
        Me.txtEmailJob.NoColours = False
        Me.txtEmailJob.NoValidate = False
        Me.txtEmailJob.ReadOnly = False
        Me.txtEmailJob.Size = New System.Drawing.Size(275, 22)
        Me.txtEmailJob.TabIndex = 3
        Me.txtEmailJob.Tag = "AE"
        '
        'txtEmail
        '
        Me.txtEmail.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail.Appearance.Options.UseFont = True
        Me.txtEmail.Location = New System.Drawing.Point(113, 32)
        Me.txtEmail.MaxLength = 100
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.NoButton = False
        Me.txtEmail.NoColours = False
        Me.txtEmail.NoValidate = False
        Me.txtEmail.ReadOnly = False
        Me.txtEmail.Size = New System.Drawing.Size(275, 22)
        Me.txtEmail.TabIndex = 1
        Me.txtEmail.Tag = "AE"
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(8, 62)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(73, 15)
        Me.CareLabel4.TabIndex = 2
        Me.CareLabel4.Text = "Work Address"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(8, 35)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(86, 15)
        Me.CareLabel6.TabIndex = 0
        Me.CareLabel6.Text = "Primary Address"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox7
        '
        Me.GroupBox7.Anchor = AnchorStyles.Top
        Me.GroupBox7.AppearanceCaption.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox7.AppearanceCaption.Options.UseFont = True
        Me.GroupBox7.Controls.Add(Me.chkEmployed)
        Me.GroupBox7.Controls.Add(Me.txtJobTitle)
        Me.GroupBox7.Controls.Add(Me.Label18)
        Me.GroupBox7.Controls.Add(Me.txtEmployer)
        Me.GroupBox7.Controls.Add(Me.Label20)
        Me.GroupBox7.Location = New System.Drawing.Point(9, 279)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(401, 85)
        Me.GroupBox7.TabIndex = 6
        Me.GroupBox7.Text = "Employment Information"
        '
        'chkEmployed
        '
        Me.chkEmployed.EnterMoveNextControl = True
        Me.chkEmployed.Location = New System.Drawing.Point(115, 29)
        Me.chkEmployed.Name = "chkEmployed"
        Me.chkEmployed.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEmployed.Properties.Appearance.Options.UseFont = True
        Me.chkEmployed.Size = New System.Drawing.Size(20, 19)
        Me.chkEmployed.TabIndex = 1
        Me.chkEmployed.Tag = "AE"
        '
        'txtJobTitle
        '
        Me.txtJobTitle.CharacterCasing = CharacterCasing.Normal
        Me.txtJobTitle.EnterMoveNextControl = True
        Me.txtJobTitle.Location = New System.Drawing.Point(115, 56)
        Me.txtJobTitle.MaxLength = 30
        Me.txtJobTitle.Name = "txtJobTitle"
        Me.txtJobTitle.NumericAllowNegatives = False
        Me.txtJobTitle.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtJobTitle.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtJobTitle.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtJobTitle.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobTitle.Properties.Appearance.Options.UseFont = True
        Me.txtJobTitle.Properties.Appearance.Options.UseTextOptions = True
        Me.txtJobTitle.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtJobTitle.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtJobTitle.Properties.MaxLength = 30
        Me.txtJobTitle.Size = New System.Drawing.Size(275, 22)
        Me.txtJobTitle.TabIndex = 4
        Me.txtJobTitle.Tag = "AE"
        Me.txtJobTitle.TextAlign = HorizontalAlignment.Left
        Me.txtJobTitle.ToolTipText = ""
        '
        'Label18
        '
        Me.Label18.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label18.Location = New System.Drawing.Point(8, 59)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(44, 15)
        Me.Label18.TabIndex = 3
        Me.Label18.Text = "Job Title"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmployer
        '
        Me.txtEmployer.CharacterCasing = CharacterCasing.Normal
        Me.txtEmployer.EnterMoveNextControl = True
        Me.txtEmployer.Location = New System.Drawing.Point(137, 28)
        Me.txtEmployer.MaxLength = 30
        Me.txtEmployer.Name = "txtEmployer"
        Me.txtEmployer.NumericAllowNegatives = False
        Me.txtEmployer.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtEmployer.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtEmployer.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtEmployer.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployer.Properties.Appearance.Options.UseFont = True
        Me.txtEmployer.Properties.Appearance.Options.UseTextOptions = True
        Me.txtEmployer.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtEmployer.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtEmployer.Properties.MaxLength = 30
        Me.txtEmployer.Size = New System.Drawing.Size(253, 22)
        Me.txtEmployer.TabIndex = 2
        Me.txtEmployer.Tag = "AE"
        Me.txtEmployer.TextAlign = HorizontalAlignment.Left
        Me.txtEmployer.ToolTipText = ""
        '
        'Label20
        '
        Me.Label20.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label20.Location = New System.Drawing.Point(8, 31)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(53, 15)
        Me.Label20.TabIndex = 0
        Me.Label20.Text = "Employed"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.txtAddress)
        Me.GroupBox4.Controls.Add(Me.btnCopyAddress)
        Me.GroupBox4.Controls.Add(Me.Label14)
        Me.GroupBox4.Location = New System.Drawing.Point(9, 142)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.ShowCaption = False
        Me.GroupBox4.Size = New System.Drawing.Size(309, 131)
        Me.GroupBox4.TabIndex = 1
        '
        'txtAddress
        '
        Me.txtAddress.Address = ""
        Me.txtAddress.AddressBlock = ""
        Me.txtAddress.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.Appearance.Options.UseFont = True
        Me.txtAddress.County = ""
        Me.txtAddress.DisplayMode = Care.Address.CareAddress.EnumDisplayMode.SingleControl
        Me.txtAddress.Location = New System.Drawing.Point(115, 10)
        Me.txtAddress.MaxLength = 500
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.PostCode = ""
        Me.txtAddress.ReadOnly = False
        Me.txtAddress.Size = New System.Drawing.Size(183, 110)
        Me.txtAddress.TabIndex = 1
        Me.txtAddress.Tag = "AE"
        Me.txtAddress.Town = ""
        '
        'btnCopyAddress
        '
        Me.btnCopyAddress.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCopyAddress.Appearance.Options.UseFont = True
        Me.btnCopyAddress.Appearance.Options.UseTextOptions = True
        Me.btnCopyAddress.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.btnCopyAddress.Location = New System.Drawing.Point(8, 31)
        Me.btnCopyAddress.Name = "btnCopyAddress"
        Me.btnCopyAddress.Size = New System.Drawing.Size(101, 50)
        Me.btnCopyAddress.TabIndex = 2
        Me.btnCopyAddress.Tag = ""
        Me.btnCopyAddress.Text = "Copy Family Address"
        '
        'Label14
        '
        Me.Label14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label14.Location = New System.Drawing.Point(8, 10)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(42, 15)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Address"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl2
        '
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.Controls.Add(Me.CareLabel5)
        Me.GroupControl2.Controls.Add(Me.CareLabel3)
        Me.GroupControl2.Controls.Add(Me.txtNI)
        Me.GroupControl2.Controls.Add(Me.cdtDOB)
        Me.GroupControl2.Location = New System.Drawing.Point(324, 9)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.ShowCaption = False
        Me.GroupControl2.Size = New System.Drawing.Size(209, 68)
        Me.GroupControl2.TabIndex = 2
        Me.GroupControl2.Text = "Email Reports"
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(8, 41)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(59, 15)
        Me.CareLabel5.TabIndex = 2
        Me.CareLabel5.Text = "NI Number"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(8, 13)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(24, 15)
        Me.CareLabel3.TabIndex = 0
        Me.CareLabel3.Text = "DOB"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNI
        '
        Me.txtNI.CharacterCasing = CharacterCasing.Upper
        Me.txtNI.EditValue = ""
        Me.txtNI.EnterMoveNextControl = True
        Me.txtNI.Location = New System.Drawing.Point(95, 38)
        Me.txtNI.MaxLength = 9
        Me.txtNI.Name = "txtNI"
        Me.txtNI.NumericAllowNegatives = False
        Me.txtNI.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtNI.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtNI.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtNI.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNI.Properties.Appearance.Options.UseFont = True
        Me.txtNI.Properties.Appearance.Options.UseTextOptions = True
        Me.txtNI.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtNI.Properties.CharacterCasing = CharacterCasing.Upper
        Me.txtNI.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtNI.Properties.MaxLength = 9
        Me.txtNI.Size = New System.Drawing.Size(105, 22)
        Me.txtNI.TabIndex = 3
        Me.txtNI.Tag = "AE"
        Me.txtNI.TextAlign = HorizontalAlignment.Left
        Me.txtNI.ToolTipText = ""
        '
        'cdtDOB
        '
        Me.cdtDOB.EditValue = Nothing
        Me.cdtDOB.EnterMoveNextControl = True
        Me.cdtDOB.Location = New System.Drawing.Point(95, 10)
        Me.cdtDOB.Name = "cdtDOB"
        Me.cdtDOB.Properties.AccessibleName = "Action Date"
        Me.cdtDOB.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtDOB.Properties.Appearance.Options.UseFont = True
        Me.cdtDOB.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDOB.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtDOB.Size = New System.Drawing.Size(105, 22)
        Me.cdtDOB.TabIndex = 1
        Me.cdtDOB.Tag = "AE"
        Me.cdtDOB.Value = Nothing
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.chkParent)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.chkCollect)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Controls.Add(Me.Label7)
        Me.GroupControl1.Controls.Add(Me.Label3)
        Me.GroupControl1.Controls.Add(Me.chkPrimary)
        Me.GroupControl1.Controls.Add(Me.chkEmergency)
        Me.GroupControl1.Location = New System.Drawing.Point(324, 83)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(209, 117)
        Me.GroupControl1.TabIndex = 3
        Me.GroupControl1.Text = "Email Reports"
        '
        'chkParent
        '
        Me.chkParent.EnterMoveNextControl = True
        Me.chkParent.Location = New System.Drawing.Point(180, 63)
        Me.chkParent.Name = "chkParent"
        Me.chkParent.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkParent.Properties.Appearance.Options.UseFont = True
        Me.chkParent.Size = New System.Drawing.Size(20, 19)
        Me.chkParent.TabIndex = 5
        Me.chkParent.Tag = "AE"
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(8, 40)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(61, 15)
        Me.CareLabel2.TabIndex = 2
        Me.CareLabel2.Text = "Can Collect"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkCollect
        '
        Me.chkCollect.EnterMoveNextControl = True
        Me.chkCollect.Location = New System.Drawing.Point(180, 38)
        Me.chkCollect.Name = "chkCollect"
        Me.chkCollect.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkCollect.Properties.Appearance.Options.UseFont = True
        Me.chkCollect.Size = New System.Drawing.Size(20, 19)
        Me.chkCollect.TabIndex = 3
        Me.chkCollect.Tag = "AE"
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(8, 65)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(119, 15)
        Me.CareLabel1.TabIndex = 4
        Me.CareLabel1.Text = "Parental Responsibility"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label7
        '
        Me.Label7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label7.Location = New System.Drawing.Point(8, 90)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(104, 15)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Emergency Contact"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label3.Location = New System.Drawing.Point(8, 15)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(86, 15)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Primary Contact"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkPrimary
        '
        Me.chkPrimary.EnterMoveNextControl = True
        Me.chkPrimary.Location = New System.Drawing.Point(180, 13)
        Me.chkPrimary.Name = "chkPrimary"
        Me.chkPrimary.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkPrimary.Properties.Appearance.Options.UseFont = True
        Me.chkPrimary.Size = New System.Drawing.Size(20, 19)
        Me.chkPrimary.TabIndex = 1
        Me.chkPrimary.Tag = "AE"
        '
        'chkEmergency
        '
        Me.chkEmergency.EnterMoveNextControl = True
        Me.chkEmergency.Location = New System.Drawing.Point(180, 88)
        Me.chkEmergency.Name = "chkEmergency"
        Me.chkEmergency.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkEmergency.Properties.Appearance.Options.UseFont = True
        Me.chkEmergency.Size = New System.Drawing.Size(20, 19)
        Me.chkEmergency.TabIndex = 7
        Me.chkEmergency.Tag = "AE"
        '
        'GroupBox6
        '
        Me.GroupBox6.AppearanceCaption.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox6.AppearanceCaption.Options.UseFont = True
        Me.GroupBox6.Controls.Add(Me.chkInvoiceEmail)
        Me.GroupBox6.Controls.Add(Me.cbxReportDetail)
        Me.GroupBox6.Controls.Add(Me.Label11)
        Me.GroupBox6.Controls.Add(Me.chkReports)
        Me.GroupBox6.Controls.Add(Me.Label6)
        Me.GroupBox6.Location = New System.Drawing.Point(419, 493)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(380, 93)
        Me.GroupBox6.TabIndex = 11
        Me.GroupBox6.Text = "Email Preferences"
        '
        'chkInvoiceEmail
        '
        Me.chkInvoiceEmail.EnterMoveNextControl = True
        Me.chkInvoiceEmail.Location = New System.Drawing.Point(111, 58)
        Me.chkInvoiceEmail.Name = "chkInvoiceEmail"
        Me.chkInvoiceEmail.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInvoiceEmail.Properties.Appearance.Options.UseFont = True
        Me.chkInvoiceEmail.Size = New System.Drawing.Size(20, 19)
        Me.chkInvoiceEmail.TabIndex = 4
        Me.chkInvoiceEmail.Tag = "AE"
        '
        'cbxReportDetail
        '
        Me.cbxReportDetail.AllowBlank = False
        Me.cbxReportDetail.DataSource = Nothing
        Me.cbxReportDetail.DisplayMember = Nothing
        Me.cbxReportDetail.EnterMoveNextControl = True
        Me.cbxReportDetail.Location = New System.Drawing.Point(137, 32)
        Me.cbxReportDetail.Name = "cbxReportDetail"
        Me.cbxReportDetail.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxReportDetail.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxReportDetail.Properties.Appearance.Options.UseFont = True
        Me.cbxReportDetail.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxReportDetail.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxReportDetail.SelectedValue = Nothing
        Me.cbxReportDetail.Size = New System.Drawing.Size(233, 22)
        Me.cbxReportDetail.TabIndex = 2
        Me.cbxReportDetail.Tag = "AE"
        Me.cbxReportDetail.ValueMember = Nothing
        '
        'Label11
        '
        Me.Label11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label11.Location = New System.Drawing.Point(8, 60)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(43, 15)
        Me.Label11.TabIndex = 3
        Me.Label11.Text = "Invoices"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkReports
        '
        Me.chkReports.EnterMoveNextControl = True
        Me.chkReports.Location = New System.Drawing.Point(111, 33)
        Me.chkReports.Name = "chkReports"
        Me.chkReports.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkReports.Properties.Appearance.Options.UseFont = True
        Me.chkReports.Size = New System.Drawing.Size(20, 19)
        Me.chkReports.TabIndex = 1
        Me.chkReports.Tag = "AE"
        '
        'Label6
        '
        Me.Label6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label6.Location = New System.Drawing.Point(8, 35)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(64, 15)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Daily Report"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox5
        '
        Me.GroupBox5.AppearanceCaption.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.AppearanceCaption.Options.UseFont = True
        Me.GroupBox5.Controls.Add(Me.txtSecret)
        Me.GroupBox5.Location = New System.Drawing.Point(324, 206)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(209, 67)
        Me.GroupBox5.TabIndex = 4
        Me.GroupBox5.Text = "Security Word / Password"
        '
        'txtSecret
        '
        Me.txtSecret.CharacterCasing = CharacterCasing.Upper
        Me.txtSecret.EnterMoveNextControl = True
        Me.txtSecret.Location = New System.Drawing.Point(8, 33)
        Me.txtSecret.MaxLength = 20
        Me.txtSecret.Name = "txtSecret"
        Me.txtSecret.NumericAllowNegatives = False
        Me.txtSecret.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSecret.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSecret.Properties.AccessibleName = "Secret Word"
        Me.txtSecret.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSecret.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSecret.Properties.Appearance.Options.UseFont = True
        Me.txtSecret.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSecret.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSecret.Properties.CharacterCasing = CharacterCasing.Upper
        Me.txtSecret.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSecret.Properties.MaxLength = 20
        Me.txtSecret.Size = New System.Drawing.Size(192, 22)
        Me.txtSecret.TabIndex = 1
        Me.txtSecret.Tag = "AEM"
        Me.txtSecret.TextAlign = HorizontalAlignment.Left
        Me.txtSecret.ToolTipText = ""
        '
        'GroupBox2
        '
        Me.GroupBox2.AppearanceCaption.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.AppearanceCaption.Options.UseFont = True
        Me.GroupBox2.Controls.Add(Me.txtTel)
        Me.GroupBox2.Controls.Add(Me.txtTelJob)
        Me.GroupBox2.Controls.Add(Me.txtMobile)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.chkSMS)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Location = New System.Drawing.Point(9, 370)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(401, 117)
        Me.GroupBox2.TabIndex = 7
        Me.GroupBox2.Text = "Telephone Numbers"
        '
        'txtTel
        '
        Me.txtTel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTel.Appearance.Options.UseFont = True
        Me.txtTel.IsMobile = True
        Me.txtTel.Location = New System.Drawing.Point(113, 32)
        Me.txtTel.MaxLength = 15
        Me.txtTel.MinimumSize = New System.Drawing.Size(0, 22)
        Me.txtTel.Name = "txtTel"
        Me.txtTel.ReadOnly = False
        Me.txtTel.Size = New System.Drawing.Size(173, 22)
        Me.txtTel.TabIndex = 1
        Me.txtTel.Tag = "AE"
        '
        'txtTelJob
        '
        Me.txtTelJob.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTelJob.Appearance.Options.UseFont = True
        Me.txtTelJob.IsMobile = True
        Me.txtTelJob.Location = New System.Drawing.Point(113, 88)
        Me.txtTelJob.MaxLength = 15
        Me.txtTelJob.MinimumSize = New System.Drawing.Size(0, 22)
        Me.txtTelJob.Name = "txtTelJob"
        Me.txtTelJob.ReadOnly = False
        Me.txtTelJob.Size = New System.Drawing.Size(173, 22)
        Me.txtTelJob.TabIndex = 7
        Me.txtTelJob.Tag = "AE"
        '
        'txtMobile
        '
        Me.txtMobile.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMobile.Appearance.Options.UseFont = True
        Me.txtMobile.IsMobile = True
        Me.txtMobile.Location = New System.Drawing.Point(113, 60)
        Me.txtMobile.MaxLength = 15
        Me.txtMobile.MinimumSize = New System.Drawing.Size(0, 22)
        Me.txtMobile.Name = "txtMobile"
        Me.txtMobile.ReadOnly = False
        Me.txtMobile.Size = New System.Drawing.Size(173, 22)
        Me.txtMobile.TabIndex = 3
        Me.txtMobile.Tag = "AE"
        '
        'Label5
        '
        Me.Label5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label5.Location = New System.Drawing.Point(300, 64)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(68, 15)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "SMS Enabled"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkSMS
        '
        Me.chkSMS.EnterMoveNextControl = True
        Me.chkSMS.Location = New System.Drawing.Point(374, 62)
        Me.chkSMS.Name = "chkSMS"
        Me.chkSMS.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkSMS.Properties.Appearance.Options.UseFont = True
        Me.chkSMS.Size = New System.Drawing.Size(20, 19)
        Me.chkSMS.TabIndex = 5
        Me.chkSMS.Tag = "AE"
        '
        'Label13
        '
        Me.Label13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label13.Location = New System.Drawing.Point(8, 90)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(28, 15)
        Me.Label13.TabIndex = 6
        Me.Label13.Text = "Work"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label8.Location = New System.Drawing.Point(8, 64)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(37, 15)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "Mobile"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label9.Location = New System.Drawing.Point(8, 35)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(33, 15)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Home"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtRelationship)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtSurname)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtForename)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtFullname)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Location = New System.Drawing.Point(9, 9)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.ShowCaption = False
        Me.GroupBox1.Size = New System.Drawing.Size(309, 127)
        Me.GroupBox1.TabIndex = 0
        '
        'txtRelationship
        '
        Me.txtRelationship.CharacterCasing = CharacterCasing.Normal
        Me.txtRelationship.EnterMoveNextControl = True
        Me.txtRelationship.Location = New System.Drawing.Point(113, 96)
        Me.txtRelationship.MaxLength = 30
        Me.txtRelationship.Name = "txtRelationship"
        Me.txtRelationship.NumericAllowNegatives = False
        Me.txtRelationship.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRelationship.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRelationship.Properties.AccessibleName = "Relationship"
        Me.txtRelationship.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRelationship.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRelationship.Properties.Appearance.Options.UseFont = True
        Me.txtRelationship.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRelationship.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRelationship.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRelationship.Properties.MaxLength = 30
        Me.txtRelationship.Size = New System.Drawing.Size(185, 22)
        Me.txtRelationship.TabIndex = 7
        Me.txtRelationship.Tag = "AETM"
        Me.txtRelationship.TextAlign = HorizontalAlignment.Left
        Me.txtRelationship.ToolTipText = ""
        '
        'Label4
        '
        Me.Label4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label4.Location = New System.Drawing.Point(8, 99)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(65, 15)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Relationship"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSurname
        '
        Me.txtSurname.CharacterCasing = CharacterCasing.Normal
        Me.txtSurname.EnterMoveNextControl = True
        Me.txtSurname.Location = New System.Drawing.Point(113, 67)
        Me.txtSurname.MaxLength = 30
        Me.txtSurname.Name = "txtSurname"
        Me.txtSurname.NumericAllowNegatives = False
        Me.txtSurname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSurname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSurname.Properties.AccessibleName = "Surname"
        Me.txtSurname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSurname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSurname.Properties.Appearance.Options.UseFont = True
        Me.txtSurname.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSurname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSurname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSurname.Properties.MaxLength = 30
        Me.txtSurname.Size = New System.Drawing.Size(185, 22)
        Me.txtSurname.TabIndex = 5
        Me.txtSurname.Tag = "AETM"
        Me.txtSurname.TextAlign = HorizontalAlignment.Left
        Me.txtSurname.ToolTipText = ""
        '
        'Label2
        '
        Me.Label2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label2.Location = New System.Drawing.Point(8, 70)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(47, 15)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Surname"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtForename
        '
        Me.txtForename.CharacterCasing = CharacterCasing.Normal
        Me.txtForename.EnterMoveNextControl = True
        Me.txtForename.Location = New System.Drawing.Point(113, 40)
        Me.txtForename.MaxLength = 30
        Me.txtForename.Name = "txtForename"
        Me.txtForename.NumericAllowNegatives = False
        Me.txtForename.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtForename.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtForename.Properties.AccessibleName = "Forename"
        Me.txtForename.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtForename.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtForename.Properties.Appearance.Options.UseFont = True
        Me.txtForename.Properties.Appearance.Options.UseTextOptions = True
        Me.txtForename.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtForename.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtForename.Properties.MaxLength = 30
        Me.txtForename.Size = New System.Drawing.Size(185, 22)
        Me.txtForename.TabIndex = 3
        Me.txtForename.Tag = "AETM"
        Me.txtForename.TextAlign = HorizontalAlignment.Left
        Me.txtForename.ToolTipText = ""
        '
        'Label1
        '
        Me.Label1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label1.Location = New System.Drawing.Point(8, 43)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 15)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Forename"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFullname
        '
        Me.txtFullname.CharacterCasing = CharacterCasing.Normal
        Me.txtFullname.EnterMoveNextControl = True
        Me.txtFullname.Location = New System.Drawing.Point(113, 12)
        Me.txtFullname.MaxLength = 0
        Me.txtFullname.Name = "txtFullname"
        Me.txtFullname.NumericAllowNegatives = False
        Me.txtFullname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtFullname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFullname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFullname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFullname.Properties.Appearance.Options.UseFont = True
        Me.txtFullname.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFullname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFullname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFullname.Size = New System.Drawing.Size(185, 22)
        Me.txtFullname.TabIndex = 1
        Me.txtFullname.Tag = "R"
        Me.txtFullname.TextAlign = HorizontalAlignment.Left
        Me.txtFullname.ToolTipText = ""
        '
        'Label17
        '
        Me.Label17.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label17.Location = New System.Drawing.Point(8, 15)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(49, 15)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Fullname"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox3
        '
        Me.GroupBox3.AppearanceCaption.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.AppearanceCaption.Options.UseFont = True
        Me.GroupBox3.Controls.Add(Me.PictureBox)
        Me.GroupBox3.Location = New System.Drawing.Point(539, 9)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(260, 264)
        Me.GroupBox3.TabIndex = 5
        Me.GroupBox3.Text = "Photo (Right Click to Amend Photo)"
        '
        'PictureBox
        '
        Me.PictureBox.Dock = DockStyle.Fill
        Me.PictureBox.DocumentID = Nothing
        Me.PictureBox.Image = Nothing
        Me.PictureBox.Name = "PictureBox"
        Me.PictureBox.Padding = New Padding(1, 1, 0, 0)
        Me.PictureBox.Size = New System.Drawing.Size(256, 240)
        Me.PictureBox.TabIndex = 0
        '
        'tabActivity
        '
        Me.tabActivity.Controls.Add(Me.crm)
        Me.tabActivity.Name = "tabActivity"
        Me.tabActivity.Size = New System.Drawing.Size(809, 595)
        Me.tabActivity.Text = "Activity"
        '
        'crm
        '
        Me.crm.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.crm.CRMContactEmail = Nothing
        Me.crm.CRMContactID = Nothing
        Me.crm.CRMContactMobile = Nothing
        Me.crm.CRMContactName = Nothing
        Me.crm.CRMLinkID = Nothing
        Me.crm.CRMLinkType = Care.[Shared].CRM.EnumLinkType.Family
        Me.crm.Location = New System.Drawing.Point(9, 8)
        Me.crm.Name = "crm"
        Me.crm.Size = New System.Drawing.Size(795, 580)
        Me.crm.TabIndex = 0
        '
        'frmContact
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(839, 668)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmContact"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Panel1.ResumeLayout(False)
        Me.ctxPopup.ResumeLayout(False)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.tabGeneral.ResumeLayout(False)
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.chkCommMarketing.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxCommMethod.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        CType(Me.txtVoucherAmount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkVoucher.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtVouchDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtVouchDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxProvider.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtVouchRef.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.GroupBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        CType(Me.chkEmployed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJobTitle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEmployer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.txtNI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDOB.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDOB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.chkParent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkCollect.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkPrimary.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkEmergency.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        CType(Me.chkInvoiceEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxReportDetail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkReports.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        CType(Me.txtSecret.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.chkSMS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.txtRelationship.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtForename.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFullname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.tabActivity.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents btnEdit As Care.Controls.CareButton
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents bs As BindingSource
    Friend WithEvents OpenFileDialog As OpenFileDialog
    Friend WithEvents ctxPopup As ContextMenuStrip
    Friend WithEvents mnuFullSize As ToolStripMenuItem
    Friend WithEvents mnuUpload As ToolStripMenuItem
    Friend WithEvents mnuDelete As ToolStripMenuItem
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents tabGeneral As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupBox6 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cbxReportDetail As Care.Controls.CareComboBox
    Friend WithEvents Label11 As Care.Controls.CareLabel
    Friend WithEvents chkReports As Care.Controls.CareCheckBox
    Friend WithEvents Label6 As Care.Controls.CareLabel
    Friend WithEvents GroupBox4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnCopyAddress As Care.Controls.CareButton
    Friend WithEvents Label14 As Care.Controls.CareLabel
    Friend WithEvents GroupBox5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtSecret As Care.Controls.CareTextBox
    Friend WithEvents GroupBox2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label5 As Care.Controls.CareLabel
    Friend WithEvents chkSMS As Care.Controls.CareCheckBox
    Friend WithEvents Label13 As Care.Controls.CareLabel
    Friend WithEvents Label8 As Care.Controls.CareLabel
    Friend WithEvents Label9 As Care.Controls.CareLabel
    Friend WithEvents GroupBox1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label7 As Care.Controls.CareLabel
    Friend WithEvents Label3 As Care.Controls.CareLabel
    Friend WithEvents chkEmergency As Care.Controls.CareCheckBox
    Friend WithEvents chkPrimary As Care.Controls.CareCheckBox
    Friend WithEvents txtRelationship As Care.Controls.CareTextBox
    Friend WithEvents Label4 As Care.Controls.CareLabel
    Friend WithEvents txtSurname As Care.Controls.CareTextBox
    Friend WithEvents Label2 As Care.Controls.CareLabel
    Friend WithEvents txtForename As Care.Controls.CareTextBox
    Friend WithEvents Label1 As Care.Controls.CareLabel
    Friend WithEvents txtFullname As Care.Controls.CareTextBox
    Friend WithEvents Label17 As Care.Controls.CareLabel
    Friend WithEvents GroupBox3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtAddress As Care.Address.CareAddress
    Friend WithEvents txtEmail As Care.Shared.CareEmailAddress
    Friend WithEvents txtTel As Care.Shared.CareTelephoneNumber
    Friend WithEvents txtMobile As Care.Shared.CareTelephoneNumber
    Friend WithEvents PictureBox As Care.Controls.PhotoControl
    Friend WithEvents tabActivity As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents crm As Care.Shared.CRMActivity
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents chkParent As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents chkCollect As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents chkInvoiceEmail As Care.Controls.CareCheckBox
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupBox7 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents chkEmployed As Care.Controls.CareCheckBox
    Friend WithEvents txtJobTitle As Care.Controls.CareTextBox
    Friend WithEvents Label18 As Care.Controls.CareLabel
    Friend WithEvents txtEmployer As Care.Controls.CareTextBox
    Friend WithEvents Label20 As Care.Controls.CareLabel
    Friend WithEvents txtNI As Care.Controls.CareTextBox
    Friend WithEvents cdtDOB As Care.Controls.CareDateTime
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtEmailJob As Care.Shared.CareEmailAddress
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents txtTelJob As Care.Shared.CareTelephoneNumber
    Friend WithEvents GroupBox8 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtVoucherAmount As Care.Controls.CareTextBox
    Friend WithEvents chkVoucher As Care.Controls.CareCheckBox
    Private WithEvents cdtVouchDate As Care.Controls.CareDateTime
    Friend WithEvents cbxProvider As Care.Controls.CareComboBox
    Friend WithEvents Label22 As Care.Controls.CareLabel
    Friend WithEvents Label23 As Care.Controls.CareLabel
    Friend WithEvents txtVouchRef As Care.Controls.CareTextBox
    Friend WithEvents Label25 As Care.Controls.CareLabel
    Friend WithEvents Label27 As Care.Controls.CareLabel
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents chkCommMarketing As Care.Controls.CareCheckBox
    Friend WithEvents cbxCommMethod As Care.Controls.CareComboBox
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
End Class
