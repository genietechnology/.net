﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmSparkSync
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.btnRun = New Care.Controls.CareButton(Me.components)
        Me.GroupControl1 = New Care.Controls.CareFrame(Me.components)
        Me.btnRefresh = New Care.Controls.CareButton(Me.components)
        Me.cbxOption = New Care.Controls.CareComboBox(Me.components)
        Me.Label10 = New Care.Controls.CareLabel(Me.components)
        Me.cgRecords = New Care.Controls.CareGrid()
        Me.txtRed = New Care.Controls.CareTextBox(Me.components)
        Me.txtYellow = New Care.Controls.CareTextBox(Me.components)
        Me.txtBlue = New Care.Controls.CareTextBox(Me.components)
        Me.txtGreen = New Care.Controls.CareTextBox(Me.components)
        Me.txtOrange = New Care.Controls.CareTextBox(Me.components)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.cbxOption.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBlue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOrange.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(731, 480)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(115, 24)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Tag = ""
        Me.btnClose.Text = "Close"
        '
        'btnRun
        '
        Me.btnRun.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnRun.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRun.Appearance.Options.UseFont = True
        Me.btnRun.Location = New System.Drawing.Point(610, 480)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(115, 24)
        Me.btnRun.TabIndex = 3
        Me.btnRun.Tag = ""
        Me.btnRun.Text = "Run"
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.btnRefresh)
        Me.GroupControl1.Controls.Add(Me.cbxOption)
        Me.GroupControl1.Controls.Add(Me.Label10)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 10)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(834, 42)
        Me.GroupControl1.TabIndex = 1
        Me.GroupControl1.Text = "Financials Integration"
        '
        'btnRefresh
        '
        Me.btnRefresh.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefresh.Appearance.Options.UseFont = True
        Me.btnRefresh.Location = New System.Drawing.Point(285, 9)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(60, 24)
        Me.btnRefresh.TabIndex = 4
        Me.btnRefresh.Tag = ""
        Me.btnRefresh.Text = "Refresh"
        '
        'cbxOption
        '
        Me.cbxOption.AllowBlank = False
        Me.cbxOption.DataSource = Nothing
        Me.cbxOption.DisplayMember = Nothing
        Me.cbxOption.EnterMoveNextControl = True
        Me.cbxOption.Location = New System.Drawing.Point(39, 11)
        Me.cbxOption.Name = "cbxOption"
        Me.cbxOption.Properties.AccessibleName = "Status"
        Me.cbxOption.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxOption.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxOption.Properties.Appearance.Options.UseFont = True
        Me.cbxOption.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxOption.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxOption.SelectedValue = Nothing
        Me.cbxOption.Size = New System.Drawing.Size(240, 22)
        Me.cbxOption.TabIndex = 1
        Me.cbxOption.Tag = ""
        Me.cbxOption.ValueMember = Nothing
        '
        'Label10
        '
        Me.Label10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label10.Location = New System.Drawing.Point(9, 14)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(24, 15)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Task"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cgRecords
        '
        Me.cgRecords.AllowBuildColumns = True
        Me.cgRecords.AllowEdit = False
        Me.cgRecords.AllowHorizontalScroll = False
        Me.cgRecords.AllowMultiSelect = False
        Me.cgRecords.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgRecords.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgRecords.Appearance.Options.UseFont = True
        Me.cgRecords.AutoSizeByData = True
        Me.cgRecords.DisableAutoSize = False
        Me.cgRecords.DisableDataFormatting = False
        Me.cgRecords.FocusedRowHandle = -2147483648
        Me.cgRecords.HideFirstColumn = False
        Me.cgRecords.Location = New System.Drawing.Point(12, 58)
        Me.cgRecords.Name = "cgRecords"
        Me.cgRecords.PreviewColumn = ""
        Me.cgRecords.QueryID = Nothing
        Me.cgRecords.RowAutoHeight = False
        Me.cgRecords.SearchAsYouType = True
        Me.cgRecords.ShowAutoFilterRow = False
        Me.cgRecords.ShowFindPanel = False
        Me.cgRecords.ShowGroupByBox = True
        Me.cgRecords.ShowLoadingPanel = False
        Me.cgRecords.ShowNavigator = False
        Me.cgRecords.Size = New System.Drawing.Size(834, 413)
        Me.cgRecords.TabIndex = 5
        '
        'txtRed
        '
        Me.txtRed.CharacterCasing = CharacterCasing.Normal
        Me.txtRed.EnterMoveNextControl = True
        Me.txtRed.Location = New System.Drawing.Point(12, 479)
        Me.txtRed.MaxLength = 0
        Me.txtRed.Name = "txtRed"
        Me.txtRed.NumericAllowNegatives = False
        Me.txtRed.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRed.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRed.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRed.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtRed.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRed.Properties.Appearance.Options.UseBackColor = True
        Me.txtRed.Properties.Appearance.Options.UseFont = True
        Me.txtRed.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRed.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRed.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRed.Size = New System.Drawing.Size(25, 22)
        Me.txtRed.TabIndex = 7
        Me.txtRed.TextAlign = HorizontalAlignment.Left
        Me.txtRed.ToolTipText = ""
        Me.txtRed.Visible = False
        '
        'txtYellow
        '
        Me.txtYellow.CharacterCasing = CharacterCasing.Normal
        Me.txtYellow.EnterMoveNextControl = True
        Me.txtYellow.Location = New System.Drawing.Point(103, 479)
        Me.txtYellow.MaxLength = 0
        Me.txtYellow.Name = "txtYellow"
        Me.txtYellow.NumericAllowNegatives = False
        Me.txtYellow.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtYellow.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtYellow.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtYellow.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtYellow.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtYellow.Properties.Appearance.Options.UseBackColor = True
        Me.txtYellow.Properties.Appearance.Options.UseFont = True
        Me.txtYellow.Properties.Appearance.Options.UseTextOptions = True
        Me.txtYellow.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtYellow.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtYellow.Size = New System.Drawing.Size(25, 22)
        Me.txtYellow.TabIndex = 10
        Me.txtYellow.TextAlign = HorizontalAlignment.Left
        Me.txtYellow.ToolTipText = ""
        Me.txtYellow.Visible = False
        '
        'txtBlue
        '
        Me.txtBlue.CharacterCasing = CharacterCasing.Normal
        Me.txtBlue.EnterMoveNextControl = True
        Me.txtBlue.Location = New System.Drawing.Point(72, 479)
        Me.txtBlue.MaxLength = 0
        Me.txtBlue.Name = "txtBlue"
        Me.txtBlue.NumericAllowNegatives = False
        Me.txtBlue.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtBlue.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBlue.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtBlue.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBlue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtBlue.Properties.Appearance.Options.UseBackColor = True
        Me.txtBlue.Properties.Appearance.Options.UseFont = True
        Me.txtBlue.Properties.Appearance.Options.UseTextOptions = True
        Me.txtBlue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtBlue.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtBlue.Size = New System.Drawing.Size(25, 22)
        Me.txtBlue.TabIndex = 9
        Me.txtBlue.TextAlign = HorizontalAlignment.Left
        Me.txtBlue.ToolTipText = ""
        Me.txtBlue.Visible = False
        '
        'txtGreen
        '
        Me.txtGreen.CharacterCasing = CharacterCasing.Normal
        Me.txtGreen.EnterMoveNextControl = True
        Me.txtGreen.Location = New System.Drawing.Point(41, 479)
        Me.txtGreen.MaxLength = 0
        Me.txtGreen.Name = "txtGreen"
        Me.txtGreen.NumericAllowNegatives = False
        Me.txtGreen.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtGreen.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtGreen.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtGreen.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtGreen.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtGreen.Properties.Appearance.Options.UseBackColor = True
        Me.txtGreen.Properties.Appearance.Options.UseFont = True
        Me.txtGreen.Properties.Appearance.Options.UseTextOptions = True
        Me.txtGreen.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtGreen.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtGreen.Size = New System.Drawing.Size(25, 22)
        Me.txtGreen.TabIndex = 8
        Me.txtGreen.TextAlign = HorizontalAlignment.Left
        Me.txtGreen.ToolTipText = ""
        Me.txtGreen.Visible = False
        '
        'txtOrange
        '
        Me.txtOrange.CharacterCasing = CharacterCasing.Normal
        Me.txtOrange.EnterMoveNextControl = True
        Me.txtOrange.Location = New System.Drawing.Point(134, 479)
        Me.txtOrange.MaxLength = 0
        Me.txtOrange.Name = "txtOrange"
        Me.txtOrange.NumericAllowNegatives = False
        Me.txtOrange.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtOrange.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtOrange.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtOrange.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtOrange.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtOrange.Properties.Appearance.Options.UseBackColor = True
        Me.txtOrange.Properties.Appearance.Options.UseFont = True
        Me.txtOrange.Properties.Appearance.Options.UseTextOptions = True
        Me.txtOrange.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtOrange.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtOrange.Size = New System.Drawing.Size(25, 22)
        Me.txtOrange.TabIndex = 11
        Me.txtOrange.TextAlign = HorizontalAlignment.Left
        Me.txtOrange.ToolTipText = ""
        Me.txtOrange.Visible = False
        '
        'frmSparkSync
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(858, 513)
        Me.Controls.Add(Me.txtOrange)
        Me.Controls.Add(Me.txtRed)
        Me.Controls.Add(Me.txtYellow)
        Me.Controls.Add(Me.txtBlue)
        Me.Controls.Add(Me.txtGreen)
        Me.Controls.Add(Me.cgRecords)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnRun)
        Me.FormBorderStyle = FormBorderStyle.Sizable
        Me.LoadMaximised = True
        Me.Name = "frmSparkSync"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = ""
        Me.WindowState = FormWindowState.Maximized
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.cbxOption.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBlue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOrange.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnRun As Care.Controls.CareButton
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents Label10 As Care.Controls.CareLabel
    Friend WithEvents cbxOption As Care.Controls.CareComboBox
    Friend WithEvents btnRefresh As Care.Controls.CareButton
    Friend WithEvents cgRecords As Care.Controls.CareGrid
    Friend WithEvents txtRed As Care.Controls.CareTextBox
    Friend WithEvents txtYellow As Care.Controls.CareTextBox
    Friend WithEvents txtBlue As Care.Controls.CareTextBox
    Friend WithEvents txtGreen As Care.Controls.CareTextBox
    Friend WithEvents txtOrange As Care.Controls.CareTextBox
    Friend WithEvents GroupControl1 As Care.Controls.CareFrame
End Class
