﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLeadManagement
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.chkShowDays = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.radDate = New Care.Controls.CareRadioButton(Me.components)
        Me.radStandard = New Care.Controls.CareRadioButton(Me.components)
        Me.radContact = New Care.Controls.CareRadioButton(Me.components)
        Me.cbxStage = New Care.Controls.CareComboBox(Me.components)
        Me.Panel1 = New Panel()
        Me.btnConvert = New Care.Controls.CareButton(Me.components)
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.btnNewLead = New Care.Controls.CareButton(Me.components)
        Me.btnProgressLead = New Care.Controls.CareButton(Me.components)
        Me.btnViewLead = New Care.Controls.CareButton(Me.components)
        Me.cgLeads = New Care.Controls.CareGrid()
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.cbxSite = New Care.Controls.CareComboBox(Me.components)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.chkShowDays.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radStandard.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radContact.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxStage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Controls.Add(Me.cbxSite)
        Me.GroupControl1.Controls.Add(Me.chkShowDays)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.radDate)
        Me.GroupControl1.Controls.Add(Me.radStandard)
        Me.GroupControl1.Controls.Add(Me.radContact)
        Me.GroupControl1.Controls.Add(Me.cbxStage)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 9)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(748, 85)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Grid Options"
        '
        'chkShowDays
        '
        Me.chkShowDays.EnterMoveNextControl = True
        Me.chkShowDays.Location = New System.Drawing.Point(307, 28)
        Me.chkShowDays.Name = "chkShowDays"
        Me.chkShowDays.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowDays.Properties.Appearance.Options.UseFont = True
        Me.chkShowDays.Properties.Caption = "Show Days Required"
        Me.chkShowDays.Size = New System.Drawing.Size(138, 19)
        Me.chkShowDays.TabIndex = 4
        Me.chkShowDays.Tag = ""
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(12, 58)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(58, 15)
        Me.CareLabel2.TabIndex = 2
        Me.CareLabel2.Text = "Stage Filter"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radDate
        '
        Me.radDate.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.radDate.Location = New System.Drawing.Point(638, 53)
        Me.radDate.Name = "radDate"
        Me.radDate.Properties.AutoWidth = True
        Me.radDate.Properties.Caption = "Date Columns"
        Me.radDate.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radDate.Properties.RadioGroupIndex = 1
        Me.radDate.Size = New System.Drawing.Size(88, 19)
        Me.radDate.TabIndex = 7
        Me.radDate.TabStop = False
        '
        'radStandard
        '
        Me.radStandard.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.radStandard.EditValue = True
        Me.radStandard.Location = New System.Drawing.Point(523, 28)
        Me.radStandard.Name = "radStandard"
        Me.radStandard.Properties.AutoWidth = True
        Me.radStandard.Properties.Caption = "Standard Columns"
        Me.radStandard.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radStandard.Properties.RadioGroupIndex = 1
        Me.radStandard.Size = New System.Drawing.Size(109, 19)
        Me.radStandard.TabIndex = 5
        '
        'radContact
        '
        Me.radContact.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.radContact.Location = New System.Drawing.Point(638, 28)
        Me.radContact.Name = "radContact"
        Me.radContact.Properties.AutoWidth = True
        Me.radContact.Properties.Caption = "Contact Columns"
        Me.radContact.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radContact.Properties.RadioGroupIndex = 1
        Me.radContact.Size = New System.Drawing.Size(103, 19)
        Me.radContact.TabIndex = 6
        Me.radContact.TabStop = False
        '
        'cbxStage
        '
        Me.cbxStage.AllowBlank = False
        Me.cbxStage.DataSource = Nothing
        Me.cbxStage.DisplayMember = Nothing
        Me.cbxStage.EnterMoveNextControl = True
        Me.cbxStage.Location = New System.Drawing.Point(81, 55)
        Me.cbxStage.Name = "cbxStage"
        Me.cbxStage.Properties.AccessibleName = "Gender"
        Me.cbxStage.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxStage.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxStage.Properties.Appearance.Options.UseFont = True
        Me.cbxStage.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxStage.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxStage.SelectedValue = Nothing
        Me.cbxStage.Size = New System.Drawing.Size(220, 22)
        Me.cbxStage.TabIndex = 3
        Me.cbxStage.Tag = ""
        Me.cbxStage.ValueMember = Nothing
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnConvert)
        Me.Panel1.Controls.Add(Me.btnClose)
        Me.Panel1.Controls.Add(Me.btnNewLead)
        Me.Panel1.Controls.Add(Me.btnProgressLead)
        Me.Panel1.Controls.Add(Me.btnViewLead)
        Me.Panel1.Dock = DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 287)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(772, 36)
        Me.Panel1.TabIndex = 2
        '
        'btnConvert
        '
        Me.btnConvert.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnConvert.Appearance.Options.UseFont = True
        Me.btnConvert.CausesValidation = False
        Me.btnConvert.Location = New System.Drawing.Point(465, 3)
        Me.btnConvert.Name = "btnConvert"
        Me.btnConvert.Size = New System.Drawing.Size(145, 25)
        Me.btnConvert.TabIndex = 3
        Me.btnConvert.Tag = ""
        Me.btnConvert.Text = "Convert Lead"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.CausesValidation = False
        Me.btnClose.DialogResult = DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(615, 3)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(145, 25)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Tag = ""
        Me.btnClose.Text = "Close"
        '
        'btnNewLead
        '
        Me.btnNewLead.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnNewLead.Appearance.Options.UseFont = True
        Me.btnNewLead.CausesValidation = False
        Me.btnNewLead.Location = New System.Drawing.Point(12, 3)
        Me.btnNewLead.Name = "btnNewLead"
        Me.btnNewLead.Size = New System.Drawing.Size(145, 25)
        Me.btnNewLead.TabIndex = 0
        Me.btnNewLead.Tag = ""
        Me.btnNewLead.Text = "Create New Lead"
        '
        'btnProgressLead
        '
        Me.btnProgressLead.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnProgressLead.Appearance.Options.UseFont = True
        Me.btnProgressLead.CausesValidation = False
        Me.btnProgressLead.Location = New System.Drawing.Point(314, 3)
        Me.btnProgressLead.Name = "btnProgressLead"
        Me.btnProgressLead.Size = New System.Drawing.Size(145, 25)
        Me.btnProgressLead.TabIndex = 2
        Me.btnProgressLead.Tag = ""
        Me.btnProgressLead.Text = "Progress Lead"
        '
        'btnViewLead
        '
        Me.btnViewLead.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnViewLead.Appearance.Options.UseFont = True
        Me.btnViewLead.CausesValidation = False
        Me.btnViewLead.Location = New System.Drawing.Point(163, 3)
        Me.btnViewLead.Name = "btnViewLead"
        Me.btnViewLead.Size = New System.Drawing.Size(145, 25)
        Me.btnViewLead.TabIndex = 1
        Me.btnViewLead.Tag = ""
        Me.btnViewLead.Text = "View Lead"
        '
        'cgLeads
        '
        Me.cgLeads.AllowBuildColumns = True
        Me.cgLeads.AllowEdit = False
        Me.cgLeads.AllowHorizontalScroll = False
        Me.cgLeads.AllowMultiSelect = False
        Me.cgLeads.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgLeads.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgLeads.Appearance.Options.UseFont = True
        Me.cgLeads.AutoSizeByData = True
        Me.cgLeads.DisableAutoSize = False
        Me.cgLeads.DisableDataFormatting = False
        Me.cgLeads.FocusedRowHandle = -2147483648
        Me.cgLeads.HideFirstColumn = False
        Me.cgLeads.Location = New System.Drawing.Point(12, 100)
        Me.cgLeads.Name = "cgLeads"
        Me.cgLeads.PreviewColumn = ""
        Me.cgLeads.QueryID = Nothing
        Me.cgLeads.RowAutoHeight = False
        Me.cgLeads.SearchAsYouType = True
        Me.cgLeads.ShowAutoFilterRow = False
        Me.cgLeads.ShowFindPanel = True
        Me.cgLeads.ShowGroupByBox = False
        Me.cgLeads.ShowLoadingPanel = False
        Me.cgLeads.ShowNavigator = False
        Me.cgLeads.Size = New System.Drawing.Size(748, 181)
        Me.cgLeads.TabIndex = 1
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(12, 30)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(19, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Site"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(81, 27)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AccessibleName = "Gender"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(220, 22)
        Me.cbxSite.TabIndex = 1
        Me.cbxSite.Tag = ""
        Me.cbxSite.ValueMember = Nothing
        '
        'frmLeadManagement
        '
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(772, 323)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.cgLeads)
        Me.Controls.Add(Me.GroupControl1)
        Me.Name = "frmLeadManagement"
        Me.Text = ""
        Me.WindowState = FormWindowState.Maximized
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.chkShowDays.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radStandard.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radContact.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxStage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btnNewLead As Care.Controls.CareButton
    Friend WithEvents btnProgressLead As Care.Controls.CareButton
    Friend WithEvents btnViewLead As Care.Controls.CareButton
    Friend WithEvents cbxStage As Care.Controls.CareComboBox
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents cgLeads As Care.Controls.CareGrid
    Friend WithEvents radDate As Care.Controls.CareRadioButton
    Friend WithEvents radStandard As Care.Controls.CareRadioButton
    Friend WithEvents radContact As Care.Controls.CareRadioButton
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents chkShowDays As Care.Controls.CareCheckBox
    Friend WithEvents btnConvert As Care.Controls.CareButton
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents cbxSite As Care.Controls.CareComboBox

End Class
