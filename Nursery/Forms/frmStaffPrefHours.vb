﻿

Imports Care.Global
Imports DevExpress.XtraEditors

Public Class frmStaffPrefHours

    Private m_RecordID As Guid?
    Private m_StaffRecord As Business.Staff
    Private m_IsNew As Boolean = True

    Public Sub New(ByVal StaffRecord As Business.Staff, ByVal RecordID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        m_StaffRecord = StaffRecord

        If RecordID.HasValue Then
            m_IsNew = False
            m_RecordID = RecordID
        Else
            m_IsNew = True
            m_RecordID = Nothing
        End If

    End Sub

    Private Sub frmStaffPrefHours_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cdtDate.BackColor = Session.ChangeColour

        SetControl(txtMonS)
        SetControl(txtMonF)

        SetControl(txtTueS)
        SetControl(txtTueF)

        SetControl(txtWedS)
        SetControl(txtWedF)

        SetControl(txtThuS)
        SetControl(txtThuF)

        SetControl(txtFriS)
        SetControl(txtFriF)

        SetControl(txtSatS)
        SetControl(txtSatF)

        SetControl(txtSunS)
        SetControl(txtSunF)

        If m_RecordID.HasValue Then
            Me.Text = "Edit Shift Pattern"
            DisplayRecord()
        Else
            Me.Text = "Create New Shift Pattern"
        End If

    End Sub

    Private Function HHMM(ByVal TimeSpanIn As TimeSpan?) As String
        If TimeSpanIn.HasValue Then
            Return Format(TimeSpanIn.Value.Hours, "00") + ":" + Format(TimeSpanIn.Value.Minutes, "00")
        Else
            Return ""
        End If
    End Function

    Private Sub DisplayRecord()

        Dim _PH As Business.StaffPrefHour = Business.StaffPrefHour.RetreiveByID(m_RecordID.Value)
        With _PH

            cdtDate.Value = ._Wc

            txtMonS.Text = HHMM(._MonStart)
            txtMonF.Text = HHMM(._MonFinish)

            txtTueS.Text = HHMM(._TueStart)
            txtTueF.Text = HHMM(._TueFinish)

            txtWedS.Text = HHMM(._WedStart)
            txtWedF.Text = HHMM(._WedFinish)

            txtThuS.Text = HHMM(._ThuStart)
            txtThuF.Text = HHMM(._ThuFinish)

            txtFriS.Text = HHMM(._FriStart)
            txtFriF.Text = HHMM(._FriFinish)

            txtSatS.Text = HHMM(._SatStart)
            txtSatF.Text = HHMM(._SatFinish)

            txtSunS.Text = HHMM(._SunStart)
            txtSunF.Text = HHMM(._SunFinish)

            CalculateAll()

        End With

    End Sub

    Private Sub CalculateAll()

        txtMonH.Text = CalculateHours(txtMonS.Text, txtMonF.Text)
        txtTueH.Text = CalculateHours(txtTueS.Text, txtTueF.Text)
        txtWedH.Text = CalculateHours(txtWedS.Text, txtWedF.Text)
        txtThuH.Text = CalculateHours(txtThuS.Text, txtThuF.Text)
        txtFriH.Text = CalculateHours(txtFriS.Text, txtFriF.Text)
        txtSatH.Text = CalculateHours(txtSatS.Text, txtSatF.Text)
        txtSunH.Text = CalculateHours(txtSunS.Text, txtSunF.Text)

        CalculateTotals()

    End Sub

    Private Sub CalculateTotals()

        Dim _Mon As Double = CalculateMins(txtMonS.Text, txtMonF.Text)
        Dim _Tue As Double = CalculateMins(txtTueS.Text, txtTueF.Text)
        Dim _Wed As Double = CalculateMins(txtWedS.Text, txtWedF.Text)
        Dim _Thu As Double = CalculateMins(txtThuS.Text, txtThuF.Text)
        Dim _Fri As Double = CalculateMins(txtFriS.Text, txtFriF.Text)
        Dim _Sat As Double = CalculateMins(txtSatS.Text, txtSatF.Text)
        Dim _Sun As Double = CalculateMins(txtSunS.Text, txtSunF.Text)

        Dim _Total As Double = _Mon + _Tue + _Wed + _Thu + _Fri + _Sat + _Sun
        Dim _Hours As Double = _Total / 60

        txtWeekH.Text = Format(_Hours, "0.00")
        txtWeekCost.Text = Format(_Hours * m_StaffRecord._SalaryHour, "0.00")

    End Sub

    Private Function CalculateHours(ByVal StartTime As String, ByVal FinishTime As String) As String
        Dim _Mins As Double = CalculateMins(StartTime, FinishTime)
        If _Mins = 0 Then
            Return "0.00"
        Else
            Dim _Hours As Double = _Mins / 60
            Return Format(_Hours, "0.00")
        End If
    End Function

    Private Function CalculateHours(ByVal StartTime As TimeSpan?, ByVal FinishTime As TimeSpan?) As Decimal
        Dim _Mins As Double = CalculateMins(StartTime, FinishTime)
        If _Mins = 0 Then
            Return 0
        Else
            Return CDec(_Mins / 60)
        End If
    End Function

    Private Function CalculateMins(ByVal StartTime As String, ByVal FinishTime As String) As Double

        If StartTime = "" OrElse FinishTime = "" Then Return 0

        Dim _s As TimeSpan? = HHMMToTimeSpan(StartTime)
        Dim _f As TimeSpan? = HHMMToTimeSpan(FinishTime)

        If Not _s.HasValue OrElse Not _f.HasValue Then Return 0

        Return CalculateMins(_s.Value, _f.Value)

    End Function

    Private Function CalculateMins(ByVal StartTime As TimeSpan?, ByVal FinishTime As TimeSpan?) As Double
        If Not StartTime.HasValue OrElse Not FinishTime.HasValue Then Return 0
        Dim _Diff As TimeSpan = FinishTime.Value - StartTime.Value
        Return _Diff.TotalMinutes
    End Function

    Private Function HHMMToTimeSpan(ByVal HHMM As String) As TimeSpan?

        If HHMM = "" Then Return Nothing

        If HHMM.Contains(":") Then
            Dim _HS As String = HHMM.Substring(0, HHMM.IndexOf(":"))
            Dim _MS As String = HHMM.Substring(HHMM.IndexOf(":") + 1)
            Dim _TS As New TimeSpan(CInt(_HS), CInt(_MS), 0)
            Return _TS
        Else
            Return Nothing
        End If

    End Function

    Private Sub SetControl(ByRef ControlIn As TextEdit)
        ControlIn.BackColor = Session.ChangeColour
        ControlIn.Properties.MaxLength = 5
        ControlIn.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        ControlIn.Properties.Mask.EditMask = "(0?\d|1\d|2[0-3])\:[0-5]\d"
        AddHandler ControlIn.InvalidValue, AddressOf InvalidValueHandler
        AddHandler ControlIn.LostFocus, AddressOf TimeLostFocus
    End Sub

    Private Sub InvalidValueHandler(sender As Object, e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs)
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub

    Private Sub TimeLostFocus(sender As Object, e As EventArgs)
        CalculateAll()
    End Sub

    Private Function ValidateEntry() As Boolean

        If Not MyControls.Validate(Me.Controls) Then Return False

        If Not CheckDates(txtMonS.Text, txtMonF.Text) Then Return False
        If Not CheckDates(txtTueS.Text, txtTueF.Text) Then Return False
        If Not CheckDates(txtWedS.Text, txtWedF.Text) Then Return False
        If Not CheckDates(txtThuS.Text, txtThuF.Text) Then Return False
        If Not CheckDates(txtFriS.Text, txtFriF.Text) Then Return False
        If Not CheckDates(txtSatS.Text, txtSatF.Text) Then Return False
        If Not CheckDates(txtSunS.Text, txtSunF.Text) Then Return False

        Return True

    End Function

    Private Function CheckDates(ByVal Start As String, ByVal Finish As String) As Boolean

        If Start = "" AndAlso Finish = "" Then Return True

        'assume error
        Dim _Return As Boolean = False

        If Start <> "" AndAlso Finish <> "" Then
            Dim _S As TimeSpan? = HHMMToTimeSpan(Start)
            Dim _F As TimeSpan? = HHMMToTimeSpan(Finish)
            If _S.HasValue AndAlso _F.HasValue Then
                If _S < _F Then
                    _Return = True
                End If
            End If
        End If

        If Not _Return Then
            CareMessage("Times entered did not pass validation. Please check.", MessageBoxIcon.Exclamation, "Validate Times")
        End If

        Return _Return

    End Function

    Private Sub SaveAndExit()

        If Not ValidateEntry() Then Exit Sub

        Dim _C As Business.StaffPrefHour = Nothing

        If m_IsNew Then
            _C = New Business.StaffPrefHour
            _C._ID = Guid.NewGuid
            _C._StaffId = m_StaffRecord._ID
        Else
            _C = Business.StaffPrefHour.RetreiveByID(m_RecordID.Value)
        End If

        With _C

            ._Wc = cdtDate.Value

            ._MonStart = ReturnTS(txtMonS.Text)
            ._MonFinish = ReturnTS(txtMonF.Text)
            ._MonHours = CalculateHours(._MonStart, ._MonFinish)

            ._TueStart = ReturnTS(txtTueS.Text)
            ._TueFinish = ReturnTS(txtTueF.Text)
            ._TueHours = CalculateHours(._TueStart, ._TueFinish)

            ._WedStart = ReturnTS(txtWedS.Text)
            ._WedFinish = ReturnTS(txtWedF.Text)
            ._WedHours = CalculateHours(._WedStart, ._WedFinish)

            ._ThuStart = ReturnTS(txtThuS.Text)
            ._ThuFinish = ReturnTS(txtThuF.Text)
            ._ThuHours = CalculateHours(._ThuStart, ._ThuFinish)

            ._FriStart = ReturnTS(txtFriS.Text)
            ._FriFinish = ReturnTS(txtFriF.Text)
            ._FriHours = CalculateHours(._FriStart, ._FriFinish)

            ._SatStart = ReturnTS(txtSatS.Text)
            ._SatFinish = ReturnTS(txtSatF.Text)
            ._SatHours = CalculateHours(._SatStart, ._SatFinish)

            ._SunStart = ReturnTS(txtSunS.Text)
            ._SunFinish = ReturnTS(txtSunF.Text)
            ._SunHours = CalculateHours(._SunStart, ._SunFinish)

            ._CostPh = m_StaffRecord._SalaryHour
            ._TotalHours = ._MonHours + ._TueHours + ._WedHours + ._ThuHours + ._FriHours + ._SatHours + ._SunHours
            ._TotalCost = ._TotalHours * ._CostPh

            .Store()

        End With

        _C = Nothing

        Me.DialogResult = DialogResult.OK
        Me.Close()

    End Sub

    Private Function ReturnTS(ByVal StringIn As String) As TimeSpan?

        If StringIn = "" Then Return Nothing

        Try
            Dim _DT As DateTime? = DateTime.Parse(StringIn)
            If _DT.HasValue Then
                Return _DT.Value.TimeOfDay
            Else
                Return Nothing
            End If

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        SaveAndExit()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

End Class