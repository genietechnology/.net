﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPlanner
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPlanner))
        Dim OptionsSpelling2 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.txtYellow = New DevExpress.XtraEditors.TextEdit()
        Me.gbxTariff = New Care.Controls.CareFrame(Me.components)
        Me.cbxLogicTariff = New Care.Controls.CareComboBox(Me.components)
        Me.cbxTariff = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl3 = New Care.Controls.CareFrame(Me.components)
        Me.cbxLogicBoltOns = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.cbxBoltOns = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.GroupControl2 = New Care.Controls.CareFrame(Me.components)
        Me.cbxSplit = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl5 = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel10 = New Care.Controls.CareLabel(Me.components)
        Me.chkGridOptions = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.GroupControl4 = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.chkLunch = New Care.Controls.CareCheckBox(Me.components)
        Me.chkTea = New Care.Controls.CareCheckBox(Me.components)
        Me.chkBreakfast = New Care.Controls.CareCheckBox(Me.components)
        Me.GroupControl8 = New Care.Controls.CareFrame(Me.components)
        Me.chkAllergies = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl7 = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel13 = New Care.Controls.CareLabel(Me.components)
        Me.chkFriday = New Care.Controls.CareCheckBox(Me.components)
        Me.chkThursday = New Care.Controls.CareCheckBox(Me.components)
        Me.chkTuesday = New Care.Controls.CareCheckBox(Me.components)
        Me.chkWednesday = New Care.Controls.CareCheckBox(Me.components)
        Me.chkMonday = New Care.Controls.CareCheckBox(Me.components)
        Me.GroupControl6 = New Care.Controls.CareFrame(Me.components)
        Me.cbxTagFilter = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.CareLabel12 = New Care.Controls.CareLabel(Me.components)
        Me.gbxRoom = New Care.Controls.CareFrame(Me.components)
        Me.cbxLogicTimes = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.cbxTimeSlots = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.gbxSort = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.cbxSort = New Care.Controls.CareComboBox(Me.components)
        Me.gbxRadioButtons = New Care.Controls.CareFrame(Me.components)
        Me.radGridStaff = New Care.Controls.CareRadioButton(Me.components)
        Me.radGridHeadCount = New Care.Controls.CareRadioButton(Me.components)
        Me.radGridPoints = New Care.Controls.CareRadioButton(Me.components)
        Me.radTimes = New Care.Controls.CareRadioButton(Me.components)
        Me.radSessions = New Care.Controls.CareRadioButton(Me.components)
        Me.gbxHeader = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.cbxSite = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.cbxSchool = New Care.Controls.CareComboBox(Me.components)
        Me.cbxGroup = New Care.Controls.CareComboBox(Me.components)
        Me.cdtWC = New Care.Controls.CareDateTime(Me.components)
        Me.Label1 = New Care.Controls.CareLabel(Me.components)
        Me.btnRun = New Care.Controls.CareButton(Me.components)
        Me.gbxGrid = New Care.Controls.CareFrame(Me.components)
        Me.btnPrint = New Care.Controls.CareDropDownButton()
        Me.btnSunday = New Care.Controls.CareDropDownButton()
        Me.btnSaturday = New Care.Controls.CareDropDownButton()
        Me.btnFriday = New Care.Controls.CareDropDownButton()
        Me.btnThursday = New Care.Controls.CareDropDownButton()
        Me.btnWednesday = New Care.Controls.CareDropDownButton()
        Me.btnTuesday = New Care.Controls.CareDropDownButton()
        Me.btnMonday = New Care.Controls.CareDropDownButton()
        Me.cgPlanner = New Care.Controls.CareGrid()
        Me.btnStaffRegister = New Care.Controls.CareButton(Me.components)
        Me.GroupControl1 = New Care.Controls.CareFrame(Me.components)
        Me.cgWC = New Care.Controls.CareGrid()
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.gbxPivot = New Care.Controls.CareFrame(Me.components)
        Me.btnPivotExport = New Care.Controls.CareButton(Me.components)
        Me.btnPivotPrint = New Care.Controls.CareButton(Me.components)
        Me.PivotGridControl1 = New DevExpress.XtraPivotGrid.PivotGridControl()
        Me.chkAllSlots = New Care.Controls.CareCheckBox(Me.components)
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.txtBlue = New DevExpress.XtraEditors.TextEdit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxTariff, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxTariff.SuspendLayout()
        CType(Me.cbxLogicTariff.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxTariff.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.cbxLogicBoltOns.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxBoltOns.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.cbxSplit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.chkGridOptions.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.chkLunch.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkTea.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkBreakfast.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl8.SuspendLayout()
        CType(Me.chkAllergies.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl7.SuspendLayout()
        CType(Me.chkFriday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkThursday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkTuesday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkWednesday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkMonday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl6.SuspendLayout()
        CType(Me.cbxTagFilter.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxRoom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxRoom.SuspendLayout()
        CType(Me.cbxLogicTimes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxTimeSlots.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxSort, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxSort.SuspendLayout()
        CType(Me.cbxSort.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxRadioButtons, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxRadioButtons.SuspendLayout()
        CType(Me.radGridStaff.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radGridHeadCount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radGridPoints.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radTimes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radSessions.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxHeader.SuspendLayout()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSchool.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxGroup.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtWC.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtWC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxGrid.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.gbxPivot, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxPivot.SuspendLayout()
        CType(Me.PivotGridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAllSlots.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBlue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'txtYellow
        '
        Me.txtYellow.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.txtYellow.Location = New System.Drawing.Point(946, -325)
        Me.txtYellow.Name = "txtYellow"
        Me.txtYellow.Properties.Appearance.BackColor = System.Drawing.Color.Khaki
        Me.txtYellow.Properties.Appearance.Options.UseBackColor = True
        Me.txtYellow.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtYellow, True)
        Me.txtYellow.Size = New System.Drawing.Size(50, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtYellow, OptionsSpelling1)
        Me.txtYellow.TabIndex = 16
        Me.txtYellow.TabStop = False
        Me.txtYellow.Visible = False
        '
        'gbxTariff
        '
        Me.gbxTariff.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.gbxTariff.Controls.Add(Me.cbxLogicTariff)
        Me.gbxTariff.Controls.Add(Me.cbxTariff)
        Me.gbxTariff.Controls.Add(Me.CareLabel4)
        Me.gbxTariff.Location = New System.Drawing.Point(135, 59)
        Me.gbxTariff.Name = "gbxTariff"
        Me.gbxTariff.ShowCaption = False
        Me.gbxTariff.Size = New System.Drawing.Size(471, 41)
        Me.gbxTariff.TabIndex = 2
        '
        'cbxLogicTariff
        '
        Me.cbxLogicTariff.AllowBlank = False
        Me.cbxLogicTariff.DataSource = Nothing
        Me.cbxLogicTariff.DisplayMember = Nothing
        Me.cbxLogicTariff.EnterMoveNextControl = True
        Me.cbxLogicTariff.Location = New System.Drawing.Point(97, 9)
        Me.cbxLogicTariff.Name = "cbxLogicTariff"
        Me.cbxLogicTariff.Properties.AccessibleName = "Group"
        Me.cbxLogicTariff.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxLogicTariff.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxLogicTariff.Properties.Appearance.Options.UseFont = True
        Me.cbxLogicTariff.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxLogicTariff.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxLogicTariff.SelectedValue = Nothing
        Me.cbxLogicTariff.Size = New System.Drawing.Size(56, 22)
        Me.cbxLogicTariff.TabIndex = 1
        Me.cbxLogicTariff.Tag = ""
        Me.cbxLogicTariff.ValueMember = Nothing
        '
        'cbxTariff
        '
        Me.cbxTariff.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxTariff.Location = New System.Drawing.Point(159, 11)
        Me.cbxTariff.Name = "cbxTariff"
        Me.cbxTariff.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxTariff.Properties.ItemAutoHeight = True
        Me.cbxTariff.Properties.SelectAllItemVisible = False
        Me.cbxTariff.Size = New System.Drawing.Size(301, 20)
        Me.cbxTariff.TabIndex = 2
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(10, 13)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(57, 15)
        Me.CareLabel4.TabIndex = 0
        Me.CareLabel4.Text = "Tariff Filter"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl3
        '
        Me.GroupControl3.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl3.Controls.Add(Me.cbxLogicBoltOns)
        Me.GroupControl3.Controls.Add(Me.CareLabel9)
        Me.GroupControl3.Controls.Add(Me.cbxBoltOns)
        Me.GroupControl3.Location = New System.Drawing.Point(135, 106)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.ShowCaption = False
        Me.GroupControl3.Size = New System.Drawing.Size(471, 41)
        Me.GroupControl3.TabIndex = 3
        '
        'cbxLogicBoltOns
        '
        Me.cbxLogicBoltOns.AllowBlank = False
        Me.cbxLogicBoltOns.DataSource = Nothing
        Me.cbxLogicBoltOns.DisplayMember = Nothing
        Me.cbxLogicBoltOns.EnterMoveNextControl = True
        Me.cbxLogicBoltOns.Location = New System.Drawing.Point(97, 10)
        Me.cbxLogicBoltOns.Name = "cbxLogicBoltOns"
        Me.cbxLogicBoltOns.Properties.AccessibleName = "Group"
        Me.cbxLogicBoltOns.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxLogicBoltOns.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxLogicBoltOns.Properties.Appearance.Options.UseFont = True
        Me.cbxLogicBoltOns.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxLogicBoltOns.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxLogicBoltOns.SelectedValue = Nothing
        Me.cbxLogicBoltOns.Size = New System.Drawing.Size(56, 22)
        Me.cbxLogicBoltOns.TabIndex = 1
        Me.cbxLogicBoltOns.Tag = ""
        Me.cbxLogicBoltOns.ValueMember = Nothing
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(10, 13)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(71, 15)
        Me.CareLabel9.TabIndex = 0
        Me.CareLabel9.Text = "Bolt-On Filter"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxBoltOns
        '
        Me.cbxBoltOns.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxBoltOns.Location = New System.Drawing.Point(159, 11)
        Me.cbxBoltOns.Name = "cbxBoltOns"
        Me.cbxBoltOns.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxBoltOns.Properties.ItemAutoHeight = True
        Me.cbxBoltOns.Properties.SelectAllItemVisible = False
        Me.cbxBoltOns.Size = New System.Drawing.Size(302, 20)
        Me.cbxBoltOns.TabIndex = 2
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl2.Controls.Add(Me.cbxSplit)
        Me.GroupControl2.Controls.Add(Me.CareLabel5)
        Me.GroupControl2.Location = New System.Drawing.Point(832, 153)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.ShowCaption = False
        Me.GroupControl2.Size = New System.Drawing.Size(164, 41)
        Me.GroupControl2.TabIndex = 8
        '
        'cbxSplit
        '
        Me.cbxSplit.AllowBlank = False
        Me.cbxSplit.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxSplit.DataSource = Nothing
        Me.cbxSplit.DisplayMember = Nothing
        Me.cbxSplit.EnterMoveNextControl = True
        Me.cbxSplit.Location = New System.Drawing.Point(77, 10)
        Me.cbxSplit.Name = "cbxSplit"
        Me.cbxSplit.Properties.AccessibleName = "Group"
        Me.cbxSplit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSplit.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxSplit.Properties.Appearance.Options.UseFont = True
        Me.cbxSplit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSplit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSplit.SelectedValue = Nothing
        Me.cbxSplit.Size = New System.Drawing.Size(77, 22)
        Me.cbxSplit.TabIndex = 1
        Me.cbxSplit.Tag = ""
        Me.cbxSplit.ValueMember = Nothing
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(10, 13)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(59, 15)
        Me.CareLabel5.TabIndex = 0
        Me.CareLabel5.Text = "Split Count"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl5
        '
        Me.GroupControl5.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl5.Controls.Add(Me.CareLabel10)
        Me.GroupControl5.Controls.Add(Me.chkGridOptions)
        Me.GroupControl5.Location = New System.Drawing.Point(612, 59)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.ShowCaption = False
        Me.GroupControl5.Size = New System.Drawing.Size(384, 41)
        Me.GroupControl5.TabIndex = 5
        '
        'CareLabel10
        '
        Me.CareLabel10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel10.Location = New System.Drawing.Point(10, 13)
        Me.CareLabel10.Name = "CareLabel10"
        Me.CareLabel10.Size = New System.Drawing.Size(67, 15)
        Me.CareLabel10.TabIndex = 0
        Me.CareLabel10.Text = "Grid Options"
        Me.CareLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkGridOptions
        '
        Me.chkGridOptions.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.chkGridOptions.Location = New System.Drawing.Point(97, 11)
        Me.chkGridOptions.Name = "chkGridOptions"
        Me.chkGridOptions.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.chkGridOptions.Properties.ItemAutoHeight = True
        Me.chkGridOptions.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize
        Me.chkGridOptions.Properties.SelectAllItemVisible = False
        Me.chkGridOptions.Size = New System.Drawing.Size(277, 20)
        Me.chkGridOptions.TabIndex = 1
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1
        Me.SplitContainerControl1.Dock = DockStyle.Fill
        Me.SplitContainerControl1.Horizontal = False
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl4)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl8)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl7)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl6)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl5)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl2)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl3)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.gbxTariff)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.gbxRoom)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.gbxSort)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.gbxRadioButtons)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.gbxHeader)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.gbxGrid)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.GroupControl1)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.gbxPivot)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.btnClose)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1008, 385)
        Me.SplitContainerControl1.SplitterPosition = 242
        Me.SplitContainerControl1.TabIndex = 0
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.CareLabel11)
        Me.GroupControl4.Controls.Add(Me.chkLunch)
        Me.GroupControl4.Controls.Add(Me.chkTea)
        Me.GroupControl4.Controls.Add(Me.chkBreakfast)
        Me.GroupControl4.Location = New System.Drawing.Point(473, 200)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.ShowCaption = False
        Me.GroupControl4.Size = New System.Drawing.Size(353, 41)
        Me.GroupControl4.TabIndex = 10
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(10, 12)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(64, 15)
        Me.CareLabel11.TabIndex = 5
        Me.CareLabel11.Text = "Meal Times:"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel11.ToolTip = resources.GetString("CareLabel11.ToolTip")
        Me.CareLabel11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.CareLabel11.ToolTipTitle = "Meal Times"
        '
        'chkLunch
        '
        Me.chkLunch.EnterMoveNextControl = True
        Me.chkLunch.Location = New System.Drawing.Point(167, 10)
        Me.chkLunch.Name = "chkLunch"
        Me.chkLunch.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkLunch.Properties.Appearance.Options.UseFont = True
        Me.chkLunch.Properties.AutoWidth = True
        Me.chkLunch.Properties.Caption = "Lunch"
        Me.chkLunch.Size = New System.Drawing.Size(55, 19)
        Me.chkLunch.TabIndex = 2
        Me.chkLunch.Tag = "AEB"
        '
        'chkTea
        '
        Me.chkTea.EnterMoveNextControl = True
        Me.chkTea.Location = New System.Drawing.Point(228, 10)
        Me.chkTea.Name = "chkTea"
        Me.chkTea.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkTea.Properties.Appearance.Options.UseFont = True
        Me.chkTea.Properties.AutoWidth = True
        Me.chkTea.Properties.Caption = "Tea"
        Me.chkTea.Size = New System.Drawing.Size(41, 19)
        Me.chkTea.TabIndex = 3
        Me.chkTea.Tag = "AEB"
        '
        'chkBreakfast
        '
        Me.chkBreakfast.EnterMoveNextControl = True
        Me.chkBreakfast.Location = New System.Drawing.Point(91, 10)
        Me.chkBreakfast.Name = "chkBreakfast"
        Me.chkBreakfast.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkBreakfast.Properties.Appearance.Options.UseFont = True
        Me.chkBreakfast.Properties.AutoWidth = True
        Me.chkBreakfast.Properties.Caption = "Breakfast"
        Me.chkBreakfast.Size = New System.Drawing.Size(70, 19)
        Me.chkBreakfast.TabIndex = 1
        Me.chkBreakfast.Tag = "AEB"
        '
        'GroupControl8
        '
        Me.GroupControl8.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl8.Controls.Add(Me.chkAllergies)
        Me.GroupControl8.Controls.Add(Me.CareLabel14)
        Me.GroupControl8.Location = New System.Drawing.Point(612, 106)
        Me.GroupControl8.Name = "GroupControl8"
        Me.GroupControl8.ShowCaption = False
        Me.GroupControl8.Size = New System.Drawing.Size(384, 41)
        Me.GroupControl8.TabIndex = 6
        '
        'chkAllergies
        '
        Me.chkAllergies.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.chkAllergies.Location = New System.Drawing.Point(97, 11)
        Me.chkAllergies.Name = "chkAllergies"
        Me.chkAllergies.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.chkAllergies.Properties.ItemAutoHeight = True
        Me.chkAllergies.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize
        Me.chkAllergies.Properties.SelectAllItemVisible = False
        Me.chkAllergies.Size = New System.Drawing.Size(277, 20)
        Me.chkAllergies.TabIndex = 2
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(10, 13)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(45, 15)
        Me.CareLabel14.TabIndex = 0
        Me.CareLabel14.Text = "Allergies"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl7
        '
        Me.GroupControl7.Controls.Add(Me.CareLabel13)
        Me.GroupControl7.Controls.Add(Me.chkFriday)
        Me.GroupControl7.Controls.Add(Me.chkThursday)
        Me.GroupControl7.Controls.Add(Me.chkTuesday)
        Me.GroupControl7.Controls.Add(Me.chkWednesday)
        Me.GroupControl7.Controls.Add(Me.chkMonday)
        Me.GroupControl7.Location = New System.Drawing.Point(12, 200)
        Me.GroupControl7.Name = "GroupControl7"
        Me.GroupControl7.ShowCaption = False
        Me.GroupControl7.Size = New System.Drawing.Size(455, 41)
        Me.GroupControl7.TabIndex = 9
        '
        'CareLabel13
        '
        Me.CareLabel13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel13.Location = New System.Drawing.Point(10, 12)
        Me.CareLabel13.Name = "CareLabel13"
        Me.CareLabel13.Size = New System.Drawing.Size(56, 15)
        Me.CareLabel13.TabIndex = 0
        Me.CareLabel13.Text = "Attending:"
        Me.CareLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel13.ToolTip = resources.GetString("CareLabel13.ToolTip")
        Me.CareLabel13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.CareLabel13.ToolTipTitle = "Attending"
        '
        'chkFriday
        '
        Me.chkFriday.EnterMoveNextControl = True
        Me.chkFriday.Location = New System.Drawing.Point(392, 10)
        Me.chkFriday.Name = "chkFriday"
        Me.chkFriday.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkFriday.Properties.Appearance.Options.UseFont = True
        Me.chkFriday.Properties.AutoWidth = True
        Me.chkFriday.Properties.Caption = "Friday"
        Me.chkFriday.Size = New System.Drawing.Size(54, 19)
        Me.chkFriday.TabIndex = 5
        Me.chkFriday.Tag = "AEB"
        '
        'chkThursday
        '
        Me.chkThursday.EnterMoveNextControl = True
        Me.chkThursday.Location = New System.Drawing.Point(315, 10)
        Me.chkThursday.Name = "chkThursday"
        Me.chkThursday.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkThursday.Properties.Appearance.Options.UseFont = True
        Me.chkThursday.Properties.AutoWidth = True
        Me.chkThursday.Properties.Caption = "Thursday"
        Me.chkThursday.Size = New System.Drawing.Size(71, 19)
        Me.chkThursday.TabIndex = 4
        Me.chkThursday.Tag = "AEB"
        '
        'chkTuesday
        '
        Me.chkTuesday.EnterMoveNextControl = True
        Me.chkTuesday.Location = New System.Drawing.Point(154, 10)
        Me.chkTuesday.Name = "chkTuesday"
        Me.chkTuesday.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkTuesday.Properties.Appearance.Options.UseFont = True
        Me.chkTuesday.Properties.AutoWidth = True
        Me.chkTuesday.Properties.Caption = "Tuesday"
        Me.chkTuesday.Size = New System.Drawing.Size(66, 19)
        Me.chkTuesday.TabIndex = 2
        Me.chkTuesday.Tag = "AEB"
        '
        'chkWednesday
        '
        Me.chkWednesday.EnterMoveNextControl = True
        Me.chkWednesday.Location = New System.Drawing.Point(226, 10)
        Me.chkWednesday.Name = "chkWednesday"
        Me.chkWednesday.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkWednesday.Properties.Appearance.Options.UseFont = True
        Me.chkWednesday.Properties.AutoWidth = True
        Me.chkWednesday.Properties.Caption = "Wednesday"
        Me.chkWednesday.Size = New System.Drawing.Size(83, 19)
        Me.chkWednesday.TabIndex = 3
        Me.chkWednesday.Tag = "AEB"
        '
        'chkMonday
        '
        Me.chkMonday.EnterMoveNextControl = True
        Me.chkMonday.Location = New System.Drawing.Point(82, 10)
        Me.chkMonday.Name = "chkMonday"
        Me.chkMonday.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkMonday.Properties.Appearance.Options.UseFont = True
        Me.chkMonday.Properties.AutoWidth = True
        Me.chkMonday.Properties.Caption = "Monday"
        Me.chkMonday.Size = New System.Drawing.Size(66, 19)
        Me.chkMonday.TabIndex = 1
        Me.chkMonday.Tag = "AEB"
        '
        'GroupControl6
        '
        Me.GroupControl6.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl6.Controls.Add(Me.cbxTagFilter)
        Me.GroupControl6.Controls.Add(Me.CareLabel12)
        Me.GroupControl6.Location = New System.Drawing.Point(832, 200)
        Me.GroupControl6.Name = "GroupControl6"
        Me.GroupControl6.ShowCaption = False
        Me.GroupControl6.Size = New System.Drawing.Size(164, 41)
        Me.GroupControl6.TabIndex = 10
        '
        'cbxTagFilter
        '
        Me.cbxTagFilter.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxTagFilter.Location = New System.Drawing.Point(67, 10)
        Me.cbxTagFilter.Name = "cbxTagFilter"
        Me.cbxTagFilter.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxTagFilter.Properties.ItemAutoHeight = True
        Me.cbxTagFilter.Properties.SelectAllItemVisible = False
        Me.cbxTagFilter.Size = New System.Drawing.Size(92, 20)
        Me.cbxTagFilter.TabIndex = 2
        '
        'CareLabel12
        '
        Me.CareLabel12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel12.Location = New System.Drawing.Point(10, 13)
        Me.CareLabel12.Name = "CareLabel12"
        Me.CareLabel12.Size = New System.Drawing.Size(49, 15)
        Me.CareLabel12.TabIndex = 0
        Me.CareLabel12.Text = "Tag Filter"
        Me.CareLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbxRoom
        '
        Me.gbxRoom.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.gbxRoom.Controls.Add(Me.cbxLogicTimes)
        Me.gbxRoom.Controls.Add(Me.CareLabel7)
        Me.gbxRoom.Controls.Add(Me.cbxTimeSlots)
        Me.gbxRoom.Location = New System.Drawing.Point(135, 153)
        Me.gbxRoom.Name = "gbxRoom"
        Me.gbxRoom.ShowCaption = False
        Me.gbxRoom.Size = New System.Drawing.Size(471, 41)
        Me.gbxRoom.TabIndex = 4
        '
        'cbxLogicTimes
        '
        Me.cbxLogicTimes.AllowBlank = False
        Me.cbxLogicTimes.DataSource = Nothing
        Me.cbxLogicTimes.DisplayMember = Nothing
        Me.cbxLogicTimes.EnterMoveNextControl = True
        Me.cbxLogicTimes.Location = New System.Drawing.Point(97, 10)
        Me.cbxLogicTimes.Name = "cbxLogicTimes"
        Me.cbxLogicTimes.Properties.AccessibleName = "Group"
        Me.cbxLogicTimes.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxLogicTimes.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxLogicTimes.Properties.Appearance.Options.UseFont = True
        Me.cbxLogicTimes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxLogicTimes.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxLogicTimes.SelectedValue = Nothing
        Me.cbxLogicTimes.Size = New System.Drawing.Size(56, 22)
        Me.cbxLogicTimes.TabIndex = 1
        Me.cbxLogicTimes.Tag = ""
        Me.cbxLogicTimes.ValueMember = Nothing
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(10, 13)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(79, 15)
        Me.CareLabel7.TabIndex = 0
        Me.CareLabel7.Text = "Time Slot Filter"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxTimeSlots
        '
        Me.cbxTimeSlots.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxTimeSlots.Location = New System.Drawing.Point(159, 11)
        Me.cbxTimeSlots.Name = "cbxTimeSlots"
        Me.cbxTimeSlots.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxTimeSlots.Properties.ItemAutoHeight = True
        Me.cbxTimeSlots.Properties.SelectAllItemVisible = False
        Me.cbxTimeSlots.Size = New System.Drawing.Size(302, 20)
        Me.cbxTimeSlots.TabIndex = 2
        '
        'gbxSort
        '
        Me.gbxSort.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.gbxSort.Controls.Add(Me.CareLabel6)
        Me.gbxSort.Controls.Add(Me.cbxSort)
        Me.gbxSort.Location = New System.Drawing.Point(612, 153)
        Me.gbxSort.Name = "gbxSort"
        Me.gbxSort.ShowCaption = False
        Me.gbxSort.Size = New System.Drawing.Size(214, 41)
        Me.gbxSort.TabIndex = 7
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(10, 13)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(21, 15)
        Me.CareLabel6.TabIndex = 0
        Me.CareLabel6.Text = "Sort"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxSort
        '
        Me.cbxSort.AllowBlank = False
        Me.cbxSort.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxSort.DataSource = Nothing
        Me.cbxSort.DisplayMember = Nothing
        Me.cbxSort.EnterMoveNextControl = True
        Me.cbxSort.Location = New System.Drawing.Point(36, 10)
        Me.cbxSort.Name = "cbxSort"
        Me.cbxSort.Properties.AccessibleName = "Group"
        Me.cbxSort.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSort.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxSort.Properties.Appearance.Options.UseFont = True
        Me.cbxSort.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSort.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSort.SelectedValue = Nothing
        Me.cbxSort.Size = New System.Drawing.Size(170, 22)
        Me.cbxSort.TabIndex = 1
        Me.cbxSort.Tag = ""
        Me.cbxSort.ValueMember = Nothing
        '
        'gbxRadioButtons
        '
        Me.gbxRadioButtons.Controls.Add(Me.radGridStaff)
        Me.gbxRadioButtons.Controls.Add(Me.radGridHeadCount)
        Me.gbxRadioButtons.Controls.Add(Me.radGridPoints)
        Me.gbxRadioButtons.Controls.Add(Me.radTimes)
        Me.gbxRadioButtons.Controls.Add(Me.radSessions)
        Me.gbxRadioButtons.Location = New System.Drawing.Point(12, 59)
        Me.gbxRadioButtons.Name = "gbxRadioButtons"
        Me.gbxRadioButtons.ShowCaption = False
        Me.gbxRadioButtons.Size = New System.Drawing.Size(117, 135)
        Me.gbxRadioButtons.TabIndex = 1
        '
        'radGridStaff
        '
        Me.radGridStaff.Location = New System.Drawing.Point(8, 110)
        Me.radGridStaff.Name = "radGridStaff"
        Me.radGridStaff.Properties.AutoWidth = True
        Me.radGridStaff.Properties.Caption = "Staff Grid"
        Me.radGridStaff.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radGridStaff.Properties.RadioGroupIndex = 0
        Me.radGridStaff.Size = New System.Drawing.Size(68, 19)
        Me.radGridStaff.TabIndex = 5
        Me.radGridStaff.TabStop = False
        '
        'radGridHeadCount
        '
        Me.radGridHeadCount.Location = New System.Drawing.Point(8, 60)
        Me.radGridHeadCount.Name = "radGridHeadCount"
        Me.radGridHeadCount.Properties.AutoWidth = True
        Me.radGridHeadCount.Properties.Caption = "Headcount Grid"
        Me.radGridHeadCount.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radGridHeadCount.Properties.RadioGroupIndex = 0
        Me.radGridHeadCount.Size = New System.Drawing.Size(96, 19)
        Me.radGridHeadCount.TabIndex = 3
        Me.radGridHeadCount.TabStop = False
        '
        'radGridPoints
        '
        Me.radGridPoints.Location = New System.Drawing.Point(8, 85)
        Me.radGridPoints.Name = "radGridPoints"
        Me.radGridPoints.Properties.AutoWidth = True
        Me.radGridPoints.Properties.Caption = "Points Grid"
        Me.radGridPoints.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radGridPoints.Properties.RadioGroupIndex = 0
        Me.radGridPoints.Size = New System.Drawing.Size(73, 19)
        Me.radGridPoints.TabIndex = 4
        Me.radGridPoints.TabStop = False
        '
        'radTimes
        '
        Me.radTimes.EditValue = True
        Me.radTimes.Location = New System.Drawing.Point(8, 11)
        Me.radTimes.Name = "radTimes"
        Me.radTimes.Properties.AutoWidth = True
        Me.radTimes.Properties.Caption = "Times List"
        Me.radTimes.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radTimes.Properties.RadioGroupIndex = 0
        Me.radTimes.Size = New System.Drawing.Size(68, 19)
        Me.radTimes.TabIndex = 0
        '
        'radSessions
        '
        Me.radSessions.Location = New System.Drawing.Point(8, 35)
        Me.radSessions.Name = "radSessions"
        Me.radSessions.Properties.AutoWidth = True
        Me.radSessions.Properties.Caption = "Session List"
        Me.radSessions.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radSessions.Properties.RadioGroupIndex = 0
        Me.radSessions.Size = New System.Drawing.Size(77, 19)
        Me.radSessions.TabIndex = 1
        Me.radSessions.TabStop = False
        '
        'gbxHeader
        '
        Me.gbxHeader.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.gbxHeader.Controls.Add(Me.CareLabel3)
        Me.gbxHeader.Controls.Add(Me.cbxSite)
        Me.gbxHeader.Controls.Add(Me.CareLabel2)
        Me.gbxHeader.Controls.Add(Me.CareLabel1)
        Me.gbxHeader.Controls.Add(Me.cbxSchool)
        Me.gbxHeader.Controls.Add(Me.cbxGroup)
        Me.gbxHeader.Controls.Add(Me.cdtWC)
        Me.gbxHeader.Controls.Add(Me.Label1)
        Me.gbxHeader.Controls.Add(Me.btnRun)
        Me.gbxHeader.Location = New System.Drawing.Point(12, 12)
        Me.gbxHeader.Name = "gbxHeader"
        Me.gbxHeader.ShowCaption = False
        Me.gbxHeader.Size = New System.Drawing.Size(984, 41)
        Me.gbxHeader.TabIndex = 0
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(330, 14)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(19, 15)
        Me.CareLabel3.TabIndex = 3
        Me.CareLabel3.Text = "Site"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(355, 11)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AccessibleName = "Group"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(166, 22)
        Me.cbxSite.TabIndex = 4
        Me.cbxSite.Tag = ""
        Me.cbxSite.ValueMember = Nothing
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(773, 15)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(36, 15)
        Me.CareLabel2.TabIndex = 7
        Me.CareLabel2.Text = "School"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(539, 14)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel1.TabIndex = 5
        Me.CareLabel1.Text = "Room"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxSchool
        '
        Me.cbxSchool.AllowBlank = False
        Me.cbxSchool.DataSource = Nothing
        Me.cbxSchool.DisplayMember = Nothing
        Me.cbxSchool.EnterMoveNextControl = True
        Me.cbxSchool.Location = New System.Drawing.Point(815, 11)
        Me.cbxSchool.Name = "cbxSchool"
        Me.cbxSchool.Properties.AccessibleName = "Group"
        Me.cbxSchool.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSchool.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxSchool.Properties.Appearance.Options.UseFont = True
        Me.cbxSchool.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSchool.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSchool.SelectedValue = Nothing
        Me.cbxSchool.Size = New System.Drawing.Size(159, 22)
        Me.cbxSchool.TabIndex = 8
        Me.cbxSchool.Tag = ""
        Me.cbxSchool.ValueMember = Nothing
        '
        'cbxGroup
        '
        Me.cbxGroup.AllowBlank = False
        Me.cbxGroup.DataSource = Nothing
        Me.cbxGroup.DisplayMember = Nothing
        Me.cbxGroup.EnterMoveNextControl = True
        Me.cbxGroup.Location = New System.Drawing.Point(577, 11)
        Me.cbxGroup.Name = "cbxGroup"
        Me.cbxGroup.Properties.AccessibleName = "Group"
        Me.cbxGroup.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxGroup.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxGroup.Properties.Appearance.Options.UseFont = True
        Me.cbxGroup.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxGroup.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxGroup.SelectedValue = Nothing
        Me.cbxGroup.Size = New System.Drawing.Size(186, 22)
        Me.cbxGroup.TabIndex = 6
        Me.cbxGroup.Tag = ""
        Me.cbxGroup.ValueMember = Nothing
        '
        'cdtWC
        '
        Me.cdtWC.EditValue = Nothing
        Me.cdtWC.EnterMoveNextControl = True
        Me.cdtWC.Location = New System.Drawing.Point(123, 11)
        Me.cdtWC.Name = "cdtWC"
        Me.cdtWC.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtWC.Properties.Appearance.Options.UseFont = True
        Me.cdtWC.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtWC.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtWC.Size = New System.Drawing.Size(88, 22)
        Me.cdtWC.TabIndex = 1
        Me.cdtWC.Value = Nothing
        '
        'Label1
        '
        Me.Label1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label1.Location = New System.Drawing.Point(12, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(105, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Week Commencing"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnRun
        '
        Me.btnRun.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRun.Appearance.Options.UseFont = True
        Me.btnRun.Location = New System.Drawing.Point(217, 11)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(80, 22)
        Me.btnRun.TabIndex = 2
        Me.btnRun.Tag = ""
        Me.btnRun.Text = "Refresh"
        '
        'gbxGrid
        '
        Me.gbxGrid.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.gbxGrid.Controls.Add(Me.btnPrint)
        Me.gbxGrid.Controls.Add(Me.btnSunday)
        Me.gbxGrid.Controls.Add(Me.btnSaturday)
        Me.gbxGrid.Controls.Add(Me.btnFriday)
        Me.gbxGrid.Controls.Add(Me.btnThursday)
        Me.gbxGrid.Controls.Add(Me.btnWednesday)
        Me.gbxGrid.Controls.Add(Me.btnTuesday)
        Me.gbxGrid.Controls.Add(Me.btnMonday)
        Me.gbxGrid.Controls.Add(Me.cgPlanner)
        Me.gbxGrid.Controls.Add(Me.btnStaffRegister)
        Me.gbxGrid.Location = New System.Drawing.Point(135, 6)
        Me.gbxGrid.Name = "gbxGrid"
        Me.gbxGrid.ShowCaption = False
        Me.gbxGrid.Size = New System.Drawing.Size(861, 120)
        Me.gbxGrid.TabIndex = 1
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnPrint.Caption = "Week Register"
        Me.btnPrint.Location = New System.Drawing.Point(624, 9)
        Me.btnPrint.MenuItems = Nothing
        Me.btnPrint.MinimumSize = New System.Drawing.Size(50, 23)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(110, 23)
        Me.btnPrint.TabIndex = 17
        '
        'btnSunday
        '
        Me.btnSunday.Caption = "Sunday"
        Me.btnSunday.Location = New System.Drawing.Point(524, 9)
        Me.btnSunday.MenuItems = Nothing
        Me.btnSunday.MinimumSize = New System.Drawing.Size(50, 23)
        Me.btnSunday.Name = "btnSunday"
        Me.btnSunday.Size = New System.Drawing.Size(80, 23)
        Me.btnSunday.TabIndex = 16
        '
        'btnSaturday
        '
        Me.btnSaturday.Caption = "Saturday"
        Me.btnSaturday.Location = New System.Drawing.Point(438, 9)
        Me.btnSaturday.MenuItems = Nothing
        Me.btnSaturday.MinimumSize = New System.Drawing.Size(50, 23)
        Me.btnSaturday.Name = "btnSaturday"
        Me.btnSaturday.Size = New System.Drawing.Size(80, 23)
        Me.btnSaturday.TabIndex = 15
        '
        'btnFriday
        '
        Me.btnFriday.Caption = "Friday"
        Me.btnFriday.Location = New System.Drawing.Point(352, 9)
        Me.btnFriday.MenuItems = Nothing
        Me.btnFriday.MinimumSize = New System.Drawing.Size(50, 23)
        Me.btnFriday.Name = "btnFriday"
        Me.btnFriday.Size = New System.Drawing.Size(80, 23)
        Me.btnFriday.TabIndex = 14
        '
        'btnThursday
        '
        Me.btnThursday.Caption = "Thursday"
        Me.btnThursday.Location = New System.Drawing.Point(266, 9)
        Me.btnThursday.MenuItems = Nothing
        Me.btnThursday.MinimumSize = New System.Drawing.Size(50, 23)
        Me.btnThursday.Name = "btnThursday"
        Me.btnThursday.Size = New System.Drawing.Size(80, 23)
        Me.btnThursday.TabIndex = 13
        '
        'btnWednesday
        '
        Me.btnWednesday.Caption = "Wednesday"
        Me.btnWednesday.Location = New System.Drawing.Point(180, 9)
        Me.btnWednesday.MenuItems = Nothing
        Me.btnWednesday.MinimumSize = New System.Drawing.Size(50, 23)
        Me.btnWednesday.Name = "btnWednesday"
        Me.btnWednesday.Size = New System.Drawing.Size(80, 23)
        Me.btnWednesday.TabIndex = 12
        '
        'btnTuesday
        '
        Me.btnTuesday.Caption = "Tuesday"
        Me.btnTuesday.Location = New System.Drawing.Point(94, 9)
        Me.btnTuesday.MenuItems = ""
        Me.btnTuesday.MinimumSize = New System.Drawing.Size(50, 23)
        Me.btnTuesday.Name = "btnTuesday"
        Me.btnTuesday.Size = New System.Drawing.Size(80, 23)
        Me.btnTuesday.TabIndex = 11
        '
        'btnMonday
        '
        Me.btnMonday.Caption = "Monday"
        Me.btnMonday.Location = New System.Drawing.Point(8, 9)
        Me.btnMonday.MenuItems = Nothing
        Me.btnMonday.MinimumSize = New System.Drawing.Size(50, 23)
        Me.btnMonday.Name = "btnMonday"
        Me.btnMonday.Size = New System.Drawing.Size(80, 23)
        Me.btnMonday.TabIndex = 10
        '
        'cgPlanner
        '
        Me.cgPlanner.AllowBuildColumns = True
        Me.cgPlanner.AllowEdit = False
        Me.cgPlanner.AllowHorizontalScroll = False
        Me.cgPlanner.AllowMultiSelect = False
        Me.cgPlanner.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgPlanner.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgPlanner.Appearance.Options.UseFont = True
        Me.cgPlanner.AutoSizeByData = False
        Me.cgPlanner.DisableAutoSize = True
        Me.cgPlanner.DisableDataFormatting = True
        Me.cgPlanner.FocusedRowHandle = -2147483648
        Me.cgPlanner.HideFirstColumn = False
        Me.cgPlanner.Location = New System.Drawing.Point(8, 37)
        Me.cgPlanner.Name = "cgPlanner"
        Me.cgPlanner.PreviewColumn = ""
        Me.cgPlanner.QueryID = Nothing
        Me.cgPlanner.RowAutoHeight = True
        Me.cgPlanner.SearchAsYouType = True
        Me.cgPlanner.ShowAutoFilterRow = False
        Me.cgPlanner.ShowFindPanel = False
        Me.cgPlanner.ShowGroupByBox = False
        Me.cgPlanner.ShowLoadingPanel = True
        Me.cgPlanner.ShowNavigator = False
        Me.cgPlanner.Size = New System.Drawing.Size(842, 74)
        Me.cgPlanner.TabIndex = 9
        Me.cgPlanner.TabStop = False
        '
        'btnStaffRegister
        '
        Me.btnStaffRegister.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnStaffRegister.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStaffRegister.Appearance.Options.UseFont = True
        Me.btnStaffRegister.Location = New System.Drawing.Point(740, 9)
        Me.btnStaffRegister.Name = "btnStaffRegister"
        Me.btnStaffRegister.Size = New System.Drawing.Size(110, 22)
        Me.btnStaffRegister.TabIndex = 8
        Me.btnStaffRegister.Tag = ""
        Me.btnStaffRegister.Text = "Staff Register"
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left), AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.cgWC)
        Me.GroupControl1.Controls.Add(Me.CareLabel8)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 6)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(117, 120)
        Me.GroupControl1.TabIndex = 0
        '
        'cgWC
        '
        Me.cgWC.AllowBuildColumns = True
        Me.cgWC.AllowEdit = False
        Me.cgWC.AllowHorizontalScroll = False
        Me.cgWC.AllowMultiSelect = False
        Me.cgWC.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgWC.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgWC.Appearance.Options.UseFont = True
        Me.cgWC.AutoSizeByData = False
        Me.cgWC.DisableAutoSize = True
        Me.cgWC.DisableDataFormatting = True
        Me.cgWC.FocusedRowHandle = -2147483648
        Me.cgWC.HideFirstColumn = False
        Me.cgWC.Location = New System.Drawing.Point(8, 35)
        Me.cgWC.Name = "cgWC"
        Me.cgWC.PreviewColumn = ""
        Me.cgWC.QueryID = Nothing
        Me.cgWC.RowAutoHeight = True
        Me.cgWC.SearchAsYouType = True
        Me.cgWC.ShowAutoFilterRow = False
        Me.cgWC.ShowFindPanel = False
        Me.cgWC.ShowGroupByBox = False
        Me.cgWC.ShowLoadingPanel = False
        Me.cgWC.ShowNavigator = False
        Me.cgWC.Size = New System.Drawing.Size(101, 76)
        Me.cgWC.TabIndex = 1
        Me.cgWC.TabStop = False
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(7, 13)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(105, 15)
        Me.CareLabel8.TabIndex = 0
        Me.CareLabel8.Text = "Week Commencing"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbxPivot
        '
        Me.gbxPivot.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.gbxPivot.Controls.Add(Me.btnPivotExport)
        Me.gbxPivot.Controls.Add(Me.btnPivotPrint)
        Me.gbxPivot.Controls.Add(Me.PivotGridControl1)
        Me.gbxPivot.Controls.Add(Me.chkAllSlots)
        Me.gbxPivot.Location = New System.Drawing.Point(135, 6)
        Me.gbxPivot.Name = "gbxPivot"
        Me.gbxPivot.ShowCaption = False
        Me.gbxPivot.Size = New System.Drawing.Size(861, 120)
        Me.gbxPivot.TabIndex = 19
        '
        'btnPivotExport
        '
        Me.btnPivotExport.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnPivotExport.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPivotExport.Appearance.Options.UseFont = True
        Me.btnPivotExport.Location = New System.Drawing.Point(624, 9)
        Me.btnPivotExport.Name = "btnPivotExport"
        Me.btnPivotExport.Size = New System.Drawing.Size(110, 22)
        Me.btnPivotExport.TabIndex = 11
        Me.btnPivotExport.Tag = ""
        Me.btnPivotExport.Text = "Export"
        '
        'btnPivotPrint
        '
        Me.btnPivotPrint.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnPivotPrint.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPivotPrint.Appearance.Options.UseFont = True
        Me.btnPivotPrint.Location = New System.Drawing.Point(740, 9)
        Me.btnPivotPrint.Name = "btnPivotPrint"
        Me.btnPivotPrint.Size = New System.Drawing.Size(110, 22)
        Me.btnPivotPrint.TabIndex = 9
        Me.btnPivotPrint.Tag = ""
        Me.btnPivotPrint.Text = "Print"
        '
        'PivotGridControl1
        '
        Me.PivotGridControl1.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.PivotGridControl1.Location = New System.Drawing.Point(12, 37)
        Me.PivotGridControl1.Name = "PivotGridControl1"
        Me.PivotGridControl1.Size = New System.Drawing.Size(838, 74)
        Me.PivotGridControl1.TabIndex = 1
        '
        'chkAllSlots
        '
        Me.chkAllSlots.EnterMoveNextControl = True
        Me.chkAllSlots.Location = New System.Drawing.Point(6, 11)
        Me.chkAllSlots.Name = "chkAllSlots"
        Me.chkAllSlots.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkAllSlots.Properties.Appearance.Options.UseFont = True
        Me.chkAllSlots.Properties.Caption = "Show All Timeslots"
        Me.chkAllSlots.Size = New System.Drawing.Size(129, 19)
        Me.chkAllSlots.TabIndex = 0
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.DialogResult = DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(892, 233)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(110, 22)
        Me.btnClose.TabIndex = 22
        Me.btnClose.Tag = ""
        Me.btnClose.Text = "Week Register"
        '
        'txtBlue
        '
        Me.txtBlue.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.txtBlue.Location = New System.Drawing.Point(479, 182)
        Me.txtBlue.Name = "txtBlue"
        Me.txtBlue.Properties.Appearance.BackColor = System.Drawing.Color.LightSkyBlue
        Me.txtBlue.Properties.Appearance.Options.UseBackColor = True
        Me.txtBlue.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtBlue, True)
        Me.txtBlue.Size = New System.Drawing.Size(50, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtBlue, OptionsSpelling2)
        Me.txtBlue.TabIndex = 17
        Me.txtBlue.TabStop = False
        Me.txtBlue.Visible = False
        '
        'frmPlanner
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1008, 385)
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Controls.Add(Me.txtYellow)
        Me.Controls.Add(Me.txtBlue)
        Me.FormBorderStyle = FormBorderStyle.Sizable
        Me.Margin = New Padding(3, 5, 3, 5)
        Me.MinimumSize = New System.Drawing.Size(1024, 300)
        Me.Name = "frmPlanner"
        Me.Text = "frmPlanner"
        Me.WindowState = FormWindowState.Maximized
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxTariff, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxTariff.ResumeLayout(False)
        Me.gbxTariff.PerformLayout()
        CType(Me.cbxLogicTariff.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxTariff.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.cbxLogicBoltOns.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxBoltOns.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.cbxSplit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.chkGridOptions.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.chkLunch.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkTea.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkBreakfast.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl8.ResumeLayout(False)
        Me.GroupControl8.PerformLayout()
        CType(Me.chkAllergies.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl7.ResumeLayout(False)
        Me.GroupControl7.PerformLayout()
        CType(Me.chkFriday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkThursday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkTuesday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkWednesday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkMonday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl6.ResumeLayout(False)
        Me.GroupControl6.PerformLayout()
        CType(Me.cbxTagFilter.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxRoom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxRoom.ResumeLayout(False)
        Me.gbxRoom.PerformLayout()
        CType(Me.cbxLogicTimes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxTimeSlots.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxSort, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxSort.ResumeLayout(False)
        Me.gbxSort.PerformLayout()
        CType(Me.cbxSort.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxRadioButtons, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxRadioButtons.ResumeLayout(False)
        Me.gbxRadioButtons.PerformLayout()
        CType(Me.radGridStaff.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radGridHeadCount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radGridPoints.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radTimes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radSessions.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxHeader.ResumeLayout(False)
        Me.gbxHeader.PerformLayout()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSchool.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxGroup.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtWC.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtWC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxGrid.ResumeLayout(False)
        Me.gbxGrid.PerformLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.gbxPivot, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxPivot.ResumeLayout(False)
        Me.gbxPivot.PerformLayout()
        CType(Me.PivotGridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAllSlots.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBlue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents txtYellow As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cbxLogicTariff As Care.Controls.CareComboBox
    Friend WithEvents cbxTariff As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents cbxLogicBoltOns As Care.Controls.CareComboBox
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents cbxBoltOns As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents cbxSplit As Care.Controls.CareComboBox
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents CareLabel10 As Care.Controls.CareLabel
    Friend WithEvents chkGridOptions As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents cbxTagFilter As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents CareLabel12 As Care.Controls.CareLabel
    Friend WithEvents cbxLogicTimes As Care.Controls.CareComboBox
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents cbxTimeSlots As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents cbxSort As Care.Controls.CareComboBox
    Friend WithEvents radGridStaff As Care.Controls.CareRadioButton
    Friend WithEvents radGridHeadCount As Care.Controls.CareRadioButton
    Friend WithEvents radGridPoints As Care.Controls.CareRadioButton
    Friend WithEvents radTimes As Care.Controls.CareRadioButton
    Friend WithEvents radSessions As Care.Controls.CareRadioButton
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents cbxSchool As Care.Controls.CareComboBox
    Friend WithEvents cbxGroup As Care.Controls.CareComboBox
    Friend WithEvents cdtWC As Care.Controls.CareDateTime
    Friend WithEvents Label1 As Care.Controls.CareLabel
    Friend WithEvents btnRun As Care.Controls.CareButton
    Friend WithEvents cgWC As Care.Controls.CareGrid
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents btnPivotPrint As Care.Controls.CareButton
    Friend WithEvents PivotGridControl1 As DevExpress.XtraPivotGrid.PivotGridControl
    Friend WithEvents chkAllSlots As Care.Controls.CareCheckBox
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents chkFriday As Care.Controls.CareCheckBox
    Friend WithEvents chkThursday As Care.Controls.CareCheckBox
    Friend WithEvents chkTuesday As Care.Controls.CareCheckBox
    Friend WithEvents chkWednesday As Care.Controls.CareCheckBox
    Friend WithEvents chkMonday As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel13 As Care.Controls.CareLabel
    Friend WithEvents btnPivotExport As Care.Controls.CareButton
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents chkLunch As Care.Controls.CareCheckBox
    Friend WithEvents chkTea As Care.Controls.CareCheckBox
    Friend WithEvents chkBreakfast As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents chkAllergies As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents btnSunday As Care.Controls.CareDropDownButton
    Friend WithEvents btnSaturday As Care.Controls.CareDropDownButton
    Friend WithEvents btnThursday As Care.Controls.CareDropDownButton
    Friend WithEvents btnWednesday As Care.Controls.CareDropDownButton
    Friend WithEvents btnTuesday As Care.Controls.CareDropDownButton
    Friend WithEvents btnMonday As Care.Controls.CareDropDownButton
    Friend WithEvents cgPlanner As Care.Controls.CareGrid
    Friend WithEvents btnFriday As Care.Controls.CareDropDownButton
    Friend WithEvents btnPrint As Care.Controls.CareDropDownButton
    Friend WithEvents btnStaffRegister As Care.Controls.CareButton
    Friend WithEvents txtBlue As DevExpress.XtraEditors.TextEdit
    Friend WithEvents gbxTariff As Care.Controls.CareFrame
    Friend WithEvents GroupControl3 As Care.Controls.CareFrame
    Friend WithEvents GroupControl2 As Care.Controls.CareFrame
    Friend WithEvents GroupControl5 As Care.Controls.CareFrame
    Friend WithEvents GroupControl6 As Care.Controls.CareFrame
    Friend WithEvents gbxRoom As Care.Controls.CareFrame
    Friend WithEvents gbxSort As Care.Controls.CareFrame
    Friend WithEvents gbxRadioButtons As Care.Controls.CareFrame
    Friend WithEvents gbxHeader As Care.Controls.CareFrame
    Friend WithEvents GroupControl1 As Care.Controls.CareFrame
    Friend WithEvents gbxPivot As Care.Controls.CareFrame
    Friend WithEvents GroupControl7 As Care.Controls.CareFrame
    Friend WithEvents GroupControl8 As Care.Controls.CareFrame
    Friend WithEvents GroupControl4 As Care.Controls.CareFrame
    Friend WithEvents gbxGrid As Care.Controls.CareFrame
End Class
