﻿

Imports Care.Data
Imports Care.Global
Imports Care.Shared
Imports DevExpress.Utils

Public Class frmBookingsDrillDown

    Private m_BookingDate As Date
    Private m_SelectedRoom As Guid?
    Private m_SelectedSite As Guid?
    Private m_SelectedDateSQL As String
    Private m_StaffPoints As Integer = 0
    Private m_SiteCapacity As Integer = 0

    Public Sub New(ByVal BookingDate As Date, ByVal SiteID As Guid?, ByVal RoomID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_BookingDate = BookingDate
        m_SelectedSite = SiteID
        m_SelectedRoom = RoomID
        m_SelectedDateSQL = ValueHandler.SQLDate(m_BookingDate)

        txtSelectedDate.Text = Format(m_BookingDate, "dddd d MMMM yyyy")

    End Sub

    Private Sub frmBookingsDrillDown_Load(sender As Object, e As EventArgs) Handles Me.Load

        m_StaffPoints = ParameterHandler.ReturnInteger("POINTSSTAFF", False)
        DisplayRooms()

        Session.CursorDefault()

    End Sub

    Private Sub DisplayRooms()

        Dim _SQL As String = ""

        If radHeadcount.Checked Then _SQL = HeadCountSQL()
        If radSpace.Checked Then _SQL = SpaceSQL()
        If radOccupancyPcnt.Checked Then _SQL = OccupancyPcntSQL()
        If radSpacePcnt.Checked Then _SQL = SpacePcntSQL()

        If _SQL = "" Then Exit Sub

        grdRooms.HideFirstColumn = True
        grdRooms.Populate(Session.ConnectionString, _SQL)
        grdRooms.Columns("room_id").Visible = False

        If m_SelectedRoom.HasValue Then
            For _i As Integer = 0 To grdRooms.RecordCount - 1
                Dim _RoomID As String = grdRooms.GetRowCellValue(_i, "room_id").ToString
                If _RoomID <> "" AndAlso _RoomID = m_SelectedRoom.ToString Then
                    grdRooms.UnderlyingGridView.FocusedRowHandle = _i
                    Exit For
                End If
            Next
        End If

    End Sub

    Private Function HeadCountSQL() As String

        Dim _SQL As String = ""

        _SQL += "select s.ratio_id, r.id as 'room_id', sit.name as 'Site', r.room_name as 'Room', s.capacity as 'Capacity',"
        _SQL += " s.wl_am as 'WL AM', s.wl_pm as 'WL PM', s.wl as 'WL',"
        _SQL += " s.bk_am as 'BK AM', s.bk_pm as 'BK PM', s.bk as 'BK',"
        _SQL += " (s.wl_am + s.bk_am) as 'Both AM', (s.wl_pm + s.bk_pm) as 'Both PM', (s.wl + s.bk) as 'Both'"
        _SQL += " from Spaces s"
        _SQL += " left join Sites sit on sit.ID = s.site_id"
        _SQL += " left join SiteRoomRatios rr on rr.ID = s.ratio_id"
        _SQL += " left join SiteRooms r on r.ID = s.room_id"
        _SQL += " where space_date = '" + m_SelectedDateSQL + "'"

        If m_SelectedSite.HasValue Then
            _SQL += " and sit.ID = '" + m_SelectedSite.ToString + "'"
        End If

        _SQL += " order by r.room_sequence, sit.name"

        Return _SQL

    End Function

    Private Function SpaceSQL() As String

        Dim _SQL As String = ""

        _SQL += "select s.ratio_id, r.id as 'room_id', sit.name as 'Site', r.room_name as 'Room', s.capacity as 'Capacity',"
        _SQL += " (s.capacity - (s.wl_am + s.bk_am)) as 'Space AM', (s.capacity - (s.wl_pm + s.bk_pm)) as 'Space PM', (s.capacity - (s.wl + s.bk)) as 'Space'"
        _SQL += " from Spaces s"
        _SQL += " left join Sites sit on sit.ID = s.site_id"
        _SQL += " left join SiteRoomRatios rr on rr.ID = s.ratio_id"
        _SQL += " left join SiteRooms r on r.ID = s.room_id"
        _SQL += " where space_date = '" + m_SelectedDateSQL + "'"

        If m_SelectedSite.HasValue Then
            _SQL += " and sit.ID = '" + m_SelectedSite.ToString + "'"
        End If

        _SQL += " order by r.room_sequence, sit.name"

        Return _SQL

    End Function

    Private Function OccupancyPcntSQL() As String

        Dim _SQL As String = ""

        _SQL += "select s.ratio_id, r.id as 'room_id', sit.name as 'Site', r.room_name as 'Room', s.capacity as 'Capacity',"
        _SQL += " cast(s.wl_am + s.bk_am as decimal) / s.capacity * 100 as 'AM Occupancy %',"
        _SQL += " cast(s.wl_pm + s.bk_pm as decimal) / s.capacity * 100 as 'PM Occupancy %',"
        _SQL += " cast(s.wl + s.bk as decimal) / s.capacity * 100 as 'Occupancy %'"
        _SQL += " from Spaces s"
        _SQL += " left join Sites sit on sit.ID = s.site_id"
        _SQL += " left join SiteRoomRatios rr on rr.ID = s.ratio_id"
        _SQL += " left join SiteRooms r on r.ID = s.room_id"
        _SQL += " where space_date = '" + m_SelectedDateSQL + "'"

        If m_SelectedSite.HasValue Then
            _SQL += " and sit.ID = '" + m_SelectedSite.ToString + "'"
        End If

        _SQL += " order by r.room_sequence, sit.name"

        Return _SQL

    End Function

    Private Function SpacePcntSQL() As String

        Dim _SQL As String = ""

        _SQL += "select s.ratio_id, r.id as 'room_id', sit.name as 'Site', r.room_name as 'Room', s.capacity as 'Capacity',"
        _SQL += " 100 - cast(s.wl_am + s.bk_am as decimal) / s.capacity * 100 as 'AM Space %',"
        _SQL += " 100 - cast(s.wl_pm + s.bk_pm as decimal) / s.capacity * 100 as 'PM Space %',"
        _SQL += " 100 - cast(s.wl + s.bk as decimal) / s.capacity * 100 as 'Space %'"
        _SQL += " from Spaces s"
        _SQL += " left join Sites sit on sit.ID = s.site_id"
        _SQL += " left join SiteRoomRatios rr on rr.ID = s.ratio_id"
        _SQL += " left join SiteRooms r on r.ID = s.room_id"
        _SQL += " where space_date = '" + m_SelectedDateSQL + "'"

        If m_SelectedSite.HasValue Then
            _SQL += " and sit.ID = '" + m_SelectedSite.ToString + "'"
        End If

        _SQL += " order by r.room_sequence, sit.name"

        Return _SQL

    End Function

    Private Sub DisplayChildren()

        Dim _RatioID As String = grdRooms.CurrentRow.Item("ratio_id").ToString

        Dim _o As Object = grdRooms.GetRowObject(grdRooms.RowIndex)
        If _o IsNot Nothing Then

            Dim _SQL As String = ""

            _SQL += "select child_id, b.booking_status as 'Status', c.fullname, c.surname_forename, child_dob as 'DOB', child_age as 'Months',"
            _SQL += " booking_from as 'From', booking_to as 'To',"
            _SQL += " b.tariff_name as 'Tariff', tariff_rate as 'Rate', b.hours as 'Hours', b.funded_hours as 'Funded Hours', points as 'Points'"
            _SQL += " from Bookings b"
            _SQL += " left join Children c on c.ID = b.child_id"
            _SQL += " where booking_date = '" & m_SelectedDateSQL & "'"
            _SQL += " and ratio_id = '" & _RatioID & "'"

            If radSortForename.Checked Then _SQL += " order by fullname"
            If radSortSurname.Checked Then _SQL += " order by c.surname, c.forename"
            If radSortYoungest.Checked Then _SQL += " order by dob desc"
            If radSortOldest.Checked Then _SQL += " order by dob"

            grdChildren.HideFirstColumn = True
            grdChildren.Populate(Session.ConnectionString, _SQL)

            If radSortSurname.Checked Then
                grdChildren.Columns("fullname").Visible = False
                grdChildren.Columns("surname_forename").Visible = True
            Else
                grdChildren.Columns("fullname").Visible = True
                grdChildren.Columns("surname_forename").Visible = False
            End If

            grdChildren.Columns("fullname").Caption = "Child"
            grdChildren.Columns("surname_forename").Caption = "Child"

        End If

    End Sub

    Private Sub grdRooms_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles grdRooms.FocusedRowChanged
        If e IsNot Nothing AndAlso e.FocusedRowHandle >= 0 Then
            DisplayChildren()
        End If
    End Sub

    Private Sub grdChildren_GridDoubleClick(sender As Object, e As EventArgs) Handles grdChildren.GridDoubleClick
        Dim _ID As Guid = New Guid(grdChildren.CurrentRow("child_id").ToString)
        Business.Child.DrillDown(_ID, True)
    End Sub

    Private Sub radHeadcount_CheckedChanged(sender As Object, e As EventArgs) Handles radHeadcount.CheckedChanged
        If radHeadcount.Checked Then DisplayRooms()
    End Sub

    Private Sub radSpace_CheckedChanged(sender As Object, e As EventArgs) Handles radSpace.CheckedChanged
        If radSpace.Checked Then DisplayRooms()
    End Sub

    Private Sub radOccupancyPcnt_CheckedChanged(sender As Object, e As EventArgs) Handles radOccupancyPcnt.CheckedChanged
        If radOccupancyPcnt.Checked Then DisplayRooms()
    End Sub

    Private Sub radSpacePcnt_CheckedChanged(sender As Object, e As EventArgs) Handles radSpacePcnt.CheckedChanged
        If radSpacePcnt.Checked Then DisplayRooms()
    End Sub

    Private Sub radSortForename_CheckedChanged(sender As Object, e As EventArgs) Handles radSortForename.CheckedChanged
        If radSortForename.Checked Then DisplayChildren()
    End Sub

    Private Sub radSortSurname_CheckedChanged(sender As Object, e As EventArgs) Handles radSortSurname.CheckedChanged
        If radSortSurname.Checked Then DisplayChildren()
    End Sub

    Private Sub radSortYoungest_CheckedChanged(sender As Object, e As EventArgs) Handles radSortYoungest.CheckedChanged
        If radSortYoungest.Checked Then DisplayChildren()
    End Sub

    Private Sub radSortOldest_CheckedChanged(sender As Object, e As EventArgs) Handles radSortOldest.CheckedChanged
        If radSortOldest.Checked Then DisplayChildren()
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub frmBookingsDrillDown_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        grdRooms.Focus()
    End Sub

    Private Sub grdChildren_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles grdChildren.RowStyle

        If e.RowHandle < 0 Then Exit Sub

        Try

            Dim _Status As String = grdChildren.UnderlyingGridView.GetRowCellValue(e.RowHandle, "Status").ToString

            If _Status = "Waiting" Then
                e.Appearance.BackColor = System.Drawing.Color.Khaki
            End If

        Catch ex As Exception

        End Try

    End Sub

End Class
