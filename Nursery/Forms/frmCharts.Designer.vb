﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCharts
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupControl3 = New Care.Controls.CareFrame()
        Me.cbxStatus = New Care.Controls.CareComboBox()
        Me.Label10 = New Care.Controls.CareLabel()
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.cht = New DevExpress.XtraCharts.ChartControl()
        Me.rc = New DevExpress.XtraEditors.RangeControl()
        Me.btnClose = New Care.Controls.CareButton()
        Me.btnPrint = New Care.Controls.CareButton()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.cbxStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.cht, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rc, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.btnPrint)
        Me.GroupControl3.Controls.Add(Me.cbxStatus)
        Me.GroupControl3.Controls.Add(Me.Label10)
        Me.GroupControl3.Dock = DockStyle.Fill
        Me.GroupControl3.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(858, 57)
        Me.GroupControl3.TabIndex = 2
        Me.GroupControl3.Text = "Chart Selection && Criteria"
        '
        'cbxStatus
        '
        Me.cbxStatus.AllowBlank = False
        Me.cbxStatus.DataSource = Nothing
        Me.cbxStatus.DisplayMember = Nothing
        Me.cbxStatus.EnterMoveNextControl = True
        Me.cbxStatus.Location = New System.Drawing.Point(80, 27)
        Me.cbxStatus.Name = "cbxStatus"
        Me.cbxStatus.Properties.AccessibleName = "Status"
        Me.cbxStatus.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxStatus.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxStatus.Properties.Appearance.Options.UseFont = True
        Me.cbxStatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxStatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxStatus.SelectedValue = Nothing
        Me.cbxStatus.Size = New System.Drawing.Size(215, 22)
        Me.cbxStatus.TabIndex = 13
        Me.cbxStatus.Tag = "AEM"
        Me.cbxStatus.ValueMember = Nothing
        '
        'Label10
        '
        Me.Label10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label10.Location = New System.Drawing.Point(11, 30)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(63, 15)
        Me.Label10.TabIndex = 12
        Me.Label10.Text = "Select Chart"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.SplitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1
        Me.SplitContainerControl1.Horizontal = False
        Me.SplitContainerControl1.Location = New System.Drawing.Point(12, 12)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl3)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.cht)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.rc)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.btnClose)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(858, 537)
        Me.SplitContainerControl1.SplitterPosition = 57
        Me.SplitContainerControl1.TabIndex = 3
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'cht
        '
        Me.cht.Dock = DockStyle.Fill
        Me.cht.Location = New System.Drawing.Point(0, 86)
        Me.cht.Name = "cht"
        Me.cht.SeriesSerializable = New DevExpress.XtraCharts.Series(-1) {}
        Me.cht.Size = New System.Drawing.Size(858, 389)
        Me.cht.TabIndex = 2
        '
        'rc
        '
        Me.rc.Client = Me.cht
        Me.rc.Dock = DockStyle.Top
        Me.rc.Location = New System.Drawing.Point(0, 0)
        Me.rc.Name = "rc"
        Me.rc.Size = New System.Drawing.Size(858, 86)
        Me.rc.TabIndex = 1
        Me.rc.Text = "RangeControl1"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.CausesValidation = False
        Me.btnClose.DialogResult = DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(773, 450)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(85, 25)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Text = "Close"
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnPrint.Location = New System.Drawing.Point(773, 26)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(75, 23)
        Me.btnPrint.TabIndex = 14
        Me.btnPrint.Text = "Print"
        '
        'frmCharts
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(884, 561)
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.FormBorderStyle = FormBorderStyle.Sizable
        Me.Margin = New Padding(3, 5, 3, 5)
        Me.MinimumSize = New System.Drawing.Size(450, 350)
        Me.Name = "frmCharts"
        Me.Text = "Charting"
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.cbxStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.cht, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rc, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents cht As DevExpress.XtraCharts.ChartControl
    Friend WithEvents rc As DevExpress.XtraEditors.RangeControl
    Friend WithEvents cbxStatus As Care.Controls.CareComboBox
    Friend WithEvents Label10 As Care.Controls.CareLabel
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents btnPrint As Care.Controls.CareButton
End Class
