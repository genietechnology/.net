﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSiteRatio
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnOK = New Care.Controls.CareButton(Me.components)
        Me.GroupControl4 = New Care.Controls.CareFrame()
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.txtCapacity = New Care.Controls.CareTextBox(Me.components)
        Me.txtRatio = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.txtAgeFrom = New Care.Controls.CareTextBox(Me.components)
        Me.txtAgeTo = New Care.Controls.CareTextBox(Me.components)
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.txtCapacity.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRatio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAgeFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAgeTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnOK.Location = New System.Drawing.Point(70, 167)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(85, 23)
        Me.btnOK.TabIndex = 1
        Me.btnOK.Text = "OK"
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.CareLabel3)
        Me.GroupControl4.Controls.Add(Me.CareLabel2)
        Me.GroupControl4.Controls.Add(Me.CareLabel1)
        Me.GroupControl4.Controls.Add(Me.txtCapacity)
        Me.GroupControl4.Controls.Add(Me.txtRatio)
        Me.GroupControl4.Controls.Add(Me.CareLabel4)
        Me.GroupControl4.Controls.Add(Me.txtAgeFrom)
        Me.GroupControl4.Controls.Add(Me.txtAgeTo)
        Me.GroupControl4.Location = New System.Drawing.Point(14, 14)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(231, 144)
        Me.GroupControl4.TabIndex = 0
        Me.GroupControl4.Text = "Ratio Details"
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(12, 116)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(99, 15)
        Me.CareLabel3.TabIndex = 6
        Me.CareLabel3.Text = "Staff to Child Ratio"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(12, 88)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(46, 15)
        Me.CareLabel2.TabIndex = 4
        Me.CareLabel2.Text = "Capacity"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(12, 60)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(90, 15)
        Me.CareLabel1.TabIndex = 2
        Me.CareLabel1.Text = "Age To (months)"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCapacity
        '
        Me.txtCapacity.CharacterCasing = CharacterCasing.Normal
        Me.txtCapacity.EnterMoveNextControl = True
        Me.txtCapacity.Location = New System.Drawing.Point(131, 85)
        Me.txtCapacity.MaxLength = 3
        Me.txtCapacity.Name = "txtCapacity"
        Me.txtCapacity.NumericAllowNegatives = False
        Me.txtCapacity.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtCapacity.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCapacity.Properties.AccessibleName = "Start Time"
        Me.txtCapacity.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtCapacity.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtCapacity.Properties.Appearance.Options.UseFont = True
        Me.txtCapacity.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCapacity.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtCapacity.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtCapacity.Properties.MaxLength = 3
        Me.txtCapacity.Size = New System.Drawing.Size(87, 22)
        Me.txtCapacity.TabIndex = 5
        Me.txtCapacity.Tag = "AMN"
        Me.txtCapacity.TextAlign = HorizontalAlignment.Left
        Me.txtCapacity.ToolTipText = ""
        '
        'txtRatio
        '
        Me.txtRatio.CharacterCasing = CharacterCasing.Normal
        Me.txtRatio.EnterMoveNextControl = True
        Me.txtRatio.Location = New System.Drawing.Point(131, 113)
        Me.txtRatio.MaxLength = 3
        Me.txtRatio.Name = "txtRatio"
        Me.txtRatio.NumericAllowNegatives = False
        Me.txtRatio.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRatio.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRatio.Properties.AccessibleName = "End Time"
        Me.txtRatio.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRatio.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRatio.Properties.Appearance.Options.UseFont = True
        Me.txtRatio.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRatio.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRatio.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRatio.Properties.MaxLength = 3
        Me.txtRatio.Size = New System.Drawing.Size(87, 22)
        Me.txtRatio.TabIndex = 7
        Me.txtRatio.Tag = "AMN"
        Me.txtRatio.TextAlign = HorizontalAlignment.Left
        Me.txtRatio.ToolTipText = ""
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(12, 33)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(104, 15)
        Me.CareLabel4.TabIndex = 0
        Me.CareLabel4.Text = "Age From (months)"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAgeFrom
        '
        Me.txtAgeFrom.CharacterCasing = CharacterCasing.Normal
        Me.txtAgeFrom.EnterMoveNextControl = True
        Me.txtAgeFrom.Location = New System.Drawing.Point(131, 29)
        Me.txtAgeFrom.MaxLength = 3
        Me.txtAgeFrom.Name = "txtAgeFrom"
        Me.txtAgeFrom.NumericAllowNegatives = False
        Me.txtAgeFrom.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtAgeFrom.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtAgeFrom.Properties.AccessibleName = "Start Time"
        Me.txtAgeFrom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtAgeFrom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtAgeFrom.Properties.Appearance.Options.UseFont = True
        Me.txtAgeFrom.Properties.Appearance.Options.UseTextOptions = True
        Me.txtAgeFrom.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtAgeFrom.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtAgeFrom.Properties.MaxLength = 3
        Me.txtAgeFrom.Size = New System.Drawing.Size(87, 22)
        Me.txtAgeFrom.TabIndex = 1
        Me.txtAgeFrom.Tag = "AMN"
        Me.txtAgeFrom.TextAlign = HorizontalAlignment.Left
        Me.txtAgeFrom.ToolTipText = ""
        '
        'txtAgeTo
        '
        Me.txtAgeTo.CharacterCasing = CharacterCasing.Normal
        Me.txtAgeTo.EnterMoveNextControl = True
        Me.txtAgeTo.Location = New System.Drawing.Point(131, 57)
        Me.txtAgeTo.MaxLength = 3
        Me.txtAgeTo.Name = "txtAgeTo"
        Me.txtAgeTo.NumericAllowNegatives = False
        Me.txtAgeTo.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtAgeTo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtAgeTo.Properties.AccessibleName = "End Time"
        Me.txtAgeTo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtAgeTo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtAgeTo.Properties.Appearance.Options.UseFont = True
        Me.txtAgeTo.Properties.Appearance.Options.UseTextOptions = True
        Me.txtAgeTo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtAgeTo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtAgeTo.Properties.MaxLength = 3
        Me.txtAgeTo.Size = New System.Drawing.Size(87, 22)
        Me.txtAgeTo.TabIndex = 3
        Me.txtAgeTo.Tag = "AMN"
        Me.txtAgeTo.TextAlign = HorizontalAlignment.Left
        Me.txtAgeTo.ToolTipText = ""
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnCancel.DialogResult = DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(161, 167)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 23)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "Cancel"
        '
        'frmSiteRatio
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(255, 199)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.btnCancel)
        Me.Margin = New Padding(3, 5, 3, 5)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSiteRatio"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = ""
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.txtCapacity.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRatio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAgeFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAgeTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents txtAgeFrom As Care.Controls.CareTextBox
    Friend WithEvents txtAgeTo As Care.Controls.CareTextBox
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents txtCapacity As Care.Controls.CareTextBox
    Friend WithEvents txtRatio As Care.Controls.CareTextBox
End Class
