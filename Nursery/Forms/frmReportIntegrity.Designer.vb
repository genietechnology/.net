﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportIntegrity
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.radSiteChild = New Care.Controls.CareRadioButton(Me.components)
        Me.radAddressee = New Care.Controls.CareRadioButton(Me.components)
        Me.radSiteFamily = New Care.Controls.CareRadioButton(Me.components)
        Me.radInvalidEmail = New Care.Controls.CareRadioButton(Me.components)
        Me.radMealsNoComponents = New Care.Controls.CareRadioButton(Me.components)
        Me.radInvalidMobiles = New Care.Controls.CareRadioButton(Me.components)
        Me.radPrimaryNoMobile = New Care.Controls.CareRadioButton(Me.components)
        Me.radElecInv = New Care.Controls.CareRadioButton(Me.components)
        Me.radPrimaryNoEmail = New Care.Controls.CareRadioButton(Me.components)
        Me.radNoPrimary = New Care.Controls.CareRadioButton(Me.components)
        Me.radNoContacts = New Care.Controls.CareRadioButton(Me.components)
        Me.grdResults = New Care.Controls.CareGrid()
        Me.btnInvalidOtherEmail = New Care.Controls.CareRadioButton(Me.components)
        Me.btnInvalidOtherMobile = New Care.Controls.CareRadioButton(Me.components)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.radSiteChild.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radAddressee.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radSiteFamily.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radInvalidEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radMealsNoComponents.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radInvalidMobiles.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radPrimaryNoMobile.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radElecInv.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radPrimaryNoEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radNoPrimary.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radNoContacts.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnInvalidOtherEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnInvalidOtherMobile.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl1.Appearance.Options.UseFont = True
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.btnInvalidOtherEmail)
        Me.GroupControl1.Controls.Add(Me.btnInvalidOtherMobile)
        Me.GroupControl1.Controls.Add(Me.radSiteChild)
        Me.GroupControl1.Controls.Add(Me.radAddressee)
        Me.GroupControl1.Controls.Add(Me.radSiteFamily)
        Me.GroupControl1.Controls.Add(Me.radInvalidEmail)
        Me.GroupControl1.Controls.Add(Me.radMealsNoComponents)
        Me.GroupControl1.Controls.Add(Me.radInvalidMobiles)
        Me.GroupControl1.Controls.Add(Me.radPrimaryNoMobile)
        Me.GroupControl1.Controls.Add(Me.radElecInv)
        Me.GroupControl1.Controls.Add(Me.radPrimaryNoEmail)
        Me.GroupControl1.Controls.Add(Me.radNoPrimary)
        Me.GroupControl1.Controls.Add(Me.radNoContacts)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(857, 157)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Select the Integrity Check you want to Run"
        '
        'radSiteChild
        '
        Me.radSiteChild.Location = New System.Drawing.Point(708, 55)
        Me.radSiteChild.Name = "radSiteChild"
        Me.radSiteChild.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radSiteChild.Properties.Appearance.Options.UseFont = True
        Me.radSiteChild.Properties.AutoWidth = True
        Me.radSiteChild.Properties.Caption = "Children with No Site"
        Me.radSiteChild.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radSiteChild.Properties.RadioGroupIndex = 0
        Me.radSiteChild.Size = New System.Drawing.Size(134, 19)
        Me.radSiteChild.TabIndex = 7
        Me.radSiteChild.TabStop = False
        '
        'radAddressee
        '
        Me.radAddressee.Location = New System.Drawing.Point(9, 80)
        Me.radAddressee.Name = "radAddressee"
        Me.radAddressee.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAddressee.Properties.Appearance.Options.UseFont = True
        Me.radAddressee.Properties.AutoWidth = True
        Me.radAddressee.Properties.Caption = "Duplicate Addressee Check"
        Me.radAddressee.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radAddressee.Properties.RadioGroupIndex = 0
        Me.radAddressee.Size = New System.Drawing.Size(165, 19)
        Me.radAddressee.TabIndex = 8
        Me.radAddressee.TabStop = False
        '
        'radSiteFamily
        '
        Me.radSiteFamily.Location = New System.Drawing.Point(708, 30)
        Me.radSiteFamily.Name = "radSiteFamily"
        Me.radSiteFamily.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radSiteFamily.Properties.Appearance.Options.UseFont = True
        Me.radSiteFamily.Properties.AutoWidth = True
        Me.radSiteFamily.Properties.Caption = "Families with No Site"
        Me.radSiteFamily.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radSiteFamily.Properties.RadioGroupIndex = 0
        Me.radSiteFamily.Size = New System.Drawing.Size(132, 19)
        Me.radSiteFamily.TabIndex = 3
        Me.radSiteFamily.TabStop = False
        '
        'radInvalidEmail
        '
        Me.radInvalidEmail.Location = New System.Drawing.Point(187, 80)
        Me.radInvalidEmail.Name = "radInvalidEmail"
        Me.radInvalidEmail.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radInvalidEmail.Properties.Appearance.Options.UseFont = True
        Me.radInvalidEmail.Properties.AutoWidth = True
        Me.radInvalidEmail.Properties.Caption = "Invalid Primary Contact Email Addresses"
        Me.radInvalidEmail.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radInvalidEmail.Properties.RadioGroupIndex = 0
        Me.radInvalidEmail.Size = New System.Drawing.Size(234, 19)
        Me.radInvalidEmail.TabIndex = 9
        Me.radInvalidEmail.TabStop = False
        '
        'radMealsNoComponents
        '
        Me.radMealsNoComponents.Location = New System.Drawing.Point(440, 80)
        Me.radMealsNoComponents.Name = "radMealsNoComponents"
        Me.radMealsNoComponents.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radMealsNoComponents.Properties.Appearance.Options.UseFont = True
        Me.radMealsNoComponents.Properties.AutoWidth = True
        Me.radMealsNoComponents.Properties.Caption = "Meals without Components"
        Me.radMealsNoComponents.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radMealsNoComponents.Properties.RadioGroupIndex = 0
        Me.radMealsNoComponents.Size = New System.Drawing.Size(169, 19)
        Me.radMealsNoComponents.TabIndex = 10
        Me.radMealsNoComponents.TabStop = False
        '
        'radInvalidMobiles
        '
        Me.radInvalidMobiles.Location = New System.Drawing.Point(187, 55)
        Me.radInvalidMobiles.Name = "radInvalidMobiles"
        Me.radInvalidMobiles.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radInvalidMobiles.Properties.Appearance.Options.UseFont = True
        Me.radInvalidMobiles.Properties.AutoWidth = True
        Me.radInvalidMobiles.Properties.Caption = "Invalid Primary Contact Mobile Numbers"
        Me.radInvalidMobiles.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radInvalidMobiles.Properties.RadioGroupIndex = 0
        Me.radInvalidMobiles.Size = New System.Drawing.Size(238, 19)
        Me.radInvalidMobiles.TabIndex = 5
        Me.radInvalidMobiles.TabStop = False
        '
        'radPrimaryNoMobile
        '
        Me.radPrimaryNoMobile.Location = New System.Drawing.Point(440, 55)
        Me.radPrimaryNoMobile.Name = "radPrimaryNoMobile"
        Me.radPrimaryNoMobile.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radPrimaryNoMobile.Properties.Appearance.Options.UseFont = True
        Me.radPrimaryNoMobile.Properties.AutoWidth = True
        Me.radPrimaryNoMobile.Properties.Caption = "Primary Contacts without Mobile Phone"
        Me.radPrimaryNoMobile.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radPrimaryNoMobile.Properties.RadioGroupIndex = 0
        Me.radPrimaryNoMobile.Size = New System.Drawing.Size(234, 19)
        Me.radPrimaryNoMobile.TabIndex = 6
        Me.radPrimaryNoMobile.TabStop = False
        '
        'radElecInv
        '
        Me.radElecInv.Location = New System.Drawing.Point(9, 55)
        Me.radElecInv.Name = "radElecInv"
        Me.radElecInv.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radElecInv.Properties.Appearance.Options.UseFont = True
        Me.radElecInv.Properties.AutoWidth = True
        Me.radElecInv.Properties.Caption = "Electronic Invoicing Checks"
        Me.radElecInv.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radElecInv.Properties.RadioGroupIndex = 0
        Me.radElecInv.Size = New System.Drawing.Size(167, 19)
        Me.radElecInv.TabIndex = 4
        Me.radElecInv.TabStop = False
        '
        'radPrimaryNoEmail
        '
        Me.radPrimaryNoEmail.Location = New System.Drawing.Point(440, 30)
        Me.radPrimaryNoEmail.Name = "radPrimaryNoEmail"
        Me.radPrimaryNoEmail.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radPrimaryNoEmail.Properties.Appearance.Options.UseFont = True
        Me.radPrimaryNoEmail.Properties.AutoWidth = True
        Me.radPrimaryNoEmail.Properties.Caption = "Primary Contacts without Email Addresses"
        Me.radPrimaryNoEmail.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radPrimaryNoEmail.Properties.RadioGroupIndex = 0
        Me.radPrimaryNoEmail.Size = New System.Drawing.Size(245, 19)
        Me.radPrimaryNoEmail.TabIndex = 2
        Me.radPrimaryNoEmail.TabStop = False
        '
        'radNoPrimary
        '
        Me.radNoPrimary.Location = New System.Drawing.Point(187, 30)
        Me.radNoPrimary.Name = "radNoPrimary"
        Me.radNoPrimary.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radNoPrimary.Properties.Appearance.Options.UseFont = True
        Me.radNoPrimary.Properties.AutoWidth = True
        Me.radNoPrimary.Properties.Caption = "Families with No Primary Contact"
        Me.radNoPrimary.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radNoPrimary.Properties.RadioGroupIndex = 0
        Me.radNoPrimary.Size = New System.Drawing.Size(199, 19)
        Me.radNoPrimary.TabIndex = 1
        Me.radNoPrimary.TabStop = False
        '
        'radNoContacts
        '
        Me.radNoContacts.EditValue = True
        Me.radNoContacts.Location = New System.Drawing.Point(9, 30)
        Me.radNoContacts.Name = "radNoContacts"
        Me.radNoContacts.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radNoContacts.Properties.Appearance.Options.UseFont = True
        Me.radNoContacts.Properties.AutoWidth = True
        Me.radNoContacts.Properties.Caption = "Families with No Contacts"
        Me.radNoContacts.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radNoContacts.Properties.RadioGroupIndex = 0
        Me.radNoContacts.Size = New System.Drawing.Size(160, 19)
        Me.radNoContacts.TabIndex = 0
        '
        'grdResults
        '
        Me.grdResults.AllowBuildColumns = True
        Me.grdResults.AllowEdit = False
        Me.grdResults.AllowHorizontalScroll = False
        Me.grdResults.AllowMultiSelect = False
        Me.grdResults.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.grdResults.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdResults.Appearance.Options.UseFont = True
        Me.grdResults.AutoSizeByData = True
        Me.grdResults.DisableAutoSize = False
        Me.grdResults.DisableDataFormatting = False
        Me.grdResults.FocusedRowHandle = -2147483648
        Me.grdResults.HideFirstColumn = False
        Me.grdResults.Location = New System.Drawing.Point(12, 175)
        Me.grdResults.Name = "grdResults"
        Me.grdResults.PreviewColumn = ""
        Me.grdResults.QueryID = Nothing
        Me.grdResults.RowAutoHeight = False
        Me.grdResults.SearchAsYouType = True
        Me.grdResults.ShowAutoFilterRow = False
        Me.grdResults.ShowFindPanel = False
        Me.grdResults.ShowGroupByBox = True
        Me.grdResults.ShowLoadingPanel = False
        Me.grdResults.ShowNavigator = False
        Me.grdResults.Size = New System.Drawing.Size(857, 299)
        Me.grdResults.TabIndex = 1
        '
        'btnInvalidOtherEmail
        '
        Me.btnInvalidOtherEmail.Location = New System.Drawing.Point(187, 130)
        Me.btnInvalidOtherEmail.Name = "btnInvalidOtherEmail"
        Me.btnInvalidOtherEmail.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnInvalidOtherEmail.Properties.Appearance.Options.UseFont = True
        Me.btnInvalidOtherEmail.Properties.AutoWidth = True
        Me.btnInvalidOtherEmail.Properties.Caption = "Invalid Email Addresses"
        Me.btnInvalidOtherEmail.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.btnInvalidOtherEmail.Properties.RadioGroupIndex = 0
        Me.btnInvalidOtherEmail.Size = New System.Drawing.Size(145, 19)
        Me.btnInvalidOtherEmail.TabIndex = 12
        Me.btnInvalidOtherEmail.TabStop = False
        '
        'btnInvalidOtherMobile
        '
        Me.btnInvalidOtherMobile.Location = New System.Drawing.Point(187, 105)
        Me.btnInvalidOtherMobile.Name = "btnInvalidOtherMobile"
        Me.btnInvalidOtherMobile.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnInvalidOtherMobile.Properties.Appearance.Options.UseFont = True
        Me.btnInvalidOtherMobile.Properties.AutoWidth = True
        Me.btnInvalidOtherMobile.Properties.Caption = "Invalid Mobile Numbers"
        Me.btnInvalidOtherMobile.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.btnInvalidOtherMobile.Properties.RadioGroupIndex = 0
        Me.btnInvalidOtherMobile.Size = New System.Drawing.Size(149, 19)
        Me.btnInvalidOtherMobile.TabIndex = 11
        Me.btnInvalidOtherMobile.TabStop = False
        '
        'frmReportIntegrity
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(882, 486)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.grdResults)
        Me.MinimumSize = New System.Drawing.Size(704, 524)
        Me.Name = "frmReportIntegrity"
        Me.Text = "frmIntegrity"
        Me.WindowState = FormWindowState.Maximized
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.radSiteChild.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radAddressee.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radSiteFamily.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radInvalidEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radMealsNoComponents.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radInvalidMobiles.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radPrimaryNoMobile.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radElecInv.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radPrimaryNoEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radNoPrimary.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radNoContacts.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnInvalidOtherEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnInvalidOtherMobile.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents radInvalidMobiles As Care.Controls.CareRadioButton
    Friend WithEvents radPrimaryNoMobile As Care.Controls.CareRadioButton
    Friend WithEvents radElecInv As Care.Controls.CareRadioButton
    Friend WithEvents radPrimaryNoEmail As Care.Controls.CareRadioButton
    Friend WithEvents radNoPrimary As Care.Controls.CareRadioButton
    Friend WithEvents radNoContacts As Care.Controls.CareRadioButton
    Friend WithEvents grdResults As Care.Controls.CareGrid
    Friend WithEvents radMealsNoComponents As Care.Controls.CareRadioButton
    Friend WithEvents radInvalidEmail As Care.Controls.CareRadioButton
    Friend WithEvents radAddressee As Care.Controls.CareRadioButton
    Friend WithEvents radSiteChild As Care.Controls.CareRadioButton
    Friend WithEvents radSiteFamily As Care.Controls.CareRadioButton
    Friend WithEvents btnInvalidOtherEmail As Care.Controls.CareRadioButton
    Friend WithEvents btnInvalidOtherMobile As Care.Controls.CareRadioButton
End Class
