﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRegister
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.txtGreen = New Care.Controls.CareTextBox(Me.components)
        Me.txtBlue = New Care.Controls.CareTextBox(Me.components)
        Me.txtYellow = New Care.Controls.CareTextBox(Me.components)
        Me.txtRed = New Care.Controls.CareTextBox(Me.components)
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.cgStaff = New Care.Controls.CareGrid()
        Me.cgChildren = New Care.Controls.CareGrid()
        Me.gbxHeader = New Care.Controls.CareFrame()
        Me.radHistorical = New Care.Controls.CareRadioButton(Me.components)
        Me.radRealtime = New Care.Controls.CareRadioButton(Me.components)
        Me.gbxHistorical = New Care.Controls.CareFrame()
        Me.radHistRegister = New Care.Controls.CareRadioButton(Me.components)
        Me.radHistAbsence = New Care.Controls.CareRadioButton(Me.components)
        Me.cdtDate = New Care.Controls.CareDateTime(Me.components)
        Me.btnRefresh = New Care.Controls.CareButton(Me.components)
        Me.gbxRealtime = New Care.Controls.CareFrame()
        Me.radCheckedIn = New Care.Controls.CareRadioButton(Me.components)
        Me.radCheckedOut = New Care.Controls.CareRadioButton(Me.components)
        Me.radAbsent = New Care.Controls.CareRadioButton(Me.components)
        Me.radVisitors = New Care.Controls.CareRadioButton(Me.components)
        Me.radHistVisitors = New Care.Controls.CareRadioButton(Me.components)
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBlue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.gbxHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxHeader.SuspendLayout()
        CType(Me.radHistorical.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radRealtime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxHistorical, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxHistorical.SuspendLayout()
        CType(Me.radHistRegister.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radHistAbsence.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxRealtime, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxRealtime.SuspendLayout()
        CType(Me.radCheckedIn.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radCheckedOut.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radAbsent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radVisitors.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radHistVisitors.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.DialogResult = DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(924, 529)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(85, 24)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "Close"
        '
        'txtGreen
        '
        Me.txtGreen.CharacterCasing = CharacterCasing.Normal
        Me.txtGreen.EnterMoveNextControl = True
        Me.txtGreen.Location = New System.Drawing.Point(41, 533)
        Me.txtGreen.MaxLength = 0
        Me.txtGreen.Name = "txtGreen"
        Me.txtGreen.NumericAllowNegatives = False
        Me.txtGreen.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtGreen.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtGreen.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtGreen.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtGreen.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtGreen.Properties.Appearance.Options.UseBackColor = True
        Me.txtGreen.Properties.Appearance.Options.UseFont = True
        Me.txtGreen.Properties.Appearance.Options.UseTextOptions = True
        Me.txtGreen.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtGreen.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtGreen.Size = New System.Drawing.Size(25, 22)
        Me.txtGreen.TabIndex = 4
        Me.txtGreen.TextAlign = HorizontalAlignment.Left
        Me.txtGreen.ToolTipText = ""
        Me.txtGreen.Visible = False
        '
        'txtBlue
        '
        Me.txtBlue.CharacterCasing = CharacterCasing.Normal
        Me.txtBlue.EnterMoveNextControl = True
        Me.txtBlue.Location = New System.Drawing.Point(72, 533)
        Me.txtBlue.MaxLength = 0
        Me.txtBlue.Name = "txtBlue"
        Me.txtBlue.NumericAllowNegatives = False
        Me.txtBlue.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtBlue.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBlue.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtBlue.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBlue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtBlue.Properties.Appearance.Options.UseBackColor = True
        Me.txtBlue.Properties.Appearance.Options.UseFont = True
        Me.txtBlue.Properties.Appearance.Options.UseTextOptions = True
        Me.txtBlue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtBlue.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtBlue.Size = New System.Drawing.Size(25, 22)
        Me.txtBlue.TabIndex = 5
        Me.txtBlue.TextAlign = HorizontalAlignment.Left
        Me.txtBlue.ToolTipText = ""
        Me.txtBlue.Visible = False
        '
        'txtYellow
        '
        Me.txtYellow.CharacterCasing = CharacterCasing.Normal
        Me.txtYellow.EnterMoveNextControl = True
        Me.txtYellow.Location = New System.Drawing.Point(103, 533)
        Me.txtYellow.MaxLength = 0
        Me.txtYellow.Name = "txtYellow"
        Me.txtYellow.NumericAllowNegatives = False
        Me.txtYellow.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtYellow.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtYellow.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtYellow.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtYellow.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtYellow.Properties.Appearance.Options.UseBackColor = True
        Me.txtYellow.Properties.Appearance.Options.UseFont = True
        Me.txtYellow.Properties.Appearance.Options.UseTextOptions = True
        Me.txtYellow.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtYellow.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtYellow.Size = New System.Drawing.Size(25, 22)
        Me.txtYellow.TabIndex = 6
        Me.txtYellow.TextAlign = HorizontalAlignment.Left
        Me.txtYellow.ToolTipText = ""
        Me.txtYellow.Visible = False
        '
        'txtRed
        '
        Me.txtRed.CharacterCasing = CharacterCasing.Normal
        Me.txtRed.EnterMoveNextControl = True
        Me.txtRed.Location = New System.Drawing.Point(12, 533)
        Me.txtRed.MaxLength = 0
        Me.txtRed.Name = "txtRed"
        Me.txtRed.NumericAllowNegatives = False
        Me.txtRed.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRed.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRed.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRed.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtRed.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRed.Properties.Appearance.Options.UseBackColor = True
        Me.txtRed.Properties.Appearance.Options.UseFont = True
        Me.txtRed.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRed.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRed.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRed.Size = New System.Drawing.Size(25, 22)
        Me.txtRed.TabIndex = 3
        Me.txtRed.TextAlign = HorizontalAlignment.Left
        Me.txtRed.ToolTipText = ""
        Me.txtRed.Visible = False
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.SplitContainerControl1.Location = New System.Drawing.Point(12, 104)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.cgStaff)
        Me.SplitContainerControl1.Panel1.Text = "panLeft"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.cgChildren)
        Me.SplitContainerControl1.Panel2.Text = "panRight"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(997, 418)
        Me.SplitContainerControl1.SplitterPosition = 423
        Me.SplitContainerControl1.TabIndex = 1
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'cgStaff
        '
        Me.cgStaff.AllowBuildColumns = True
        Me.cgStaff.AllowEdit = False
        Me.cgStaff.AllowHorizontalScroll = False
        Me.cgStaff.AllowMultiSelect = False
        Me.cgStaff.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgStaff.Appearance.Options.UseFont = True
        Me.cgStaff.AutoSizeByData = True
        Me.cgStaff.DisableAutoSize = False
        Me.cgStaff.DisableDataFormatting = False
        Me.cgStaff.Dock = DockStyle.Fill
        Me.cgStaff.FocusedRowHandle = -2147483648
        Me.cgStaff.HideFirstColumn = False
        Me.cgStaff.Location = New System.Drawing.Point(0, 0)
        Me.cgStaff.Name = "cgStaff"
        Me.cgStaff.PreviewColumn = ""
        Me.cgStaff.QueryID = Nothing
        Me.cgStaff.RowAutoHeight = False
        Me.cgStaff.SearchAsYouType = True
        Me.cgStaff.ShowAutoFilterRow = False
        Me.cgStaff.ShowFindPanel = False
        Me.cgStaff.ShowGroupByBox = True
        Me.cgStaff.ShowLoadingPanel = False
        Me.cgStaff.ShowNavigator = False
        Me.cgStaff.Size = New System.Drawing.Size(423, 418)
        Me.cgStaff.TabIndex = 0
        '
        'cgChildren
        '
        Me.cgChildren.AllowBuildColumns = True
        Me.cgChildren.AllowEdit = False
        Me.cgChildren.AllowHorizontalScroll = False
        Me.cgChildren.AllowMultiSelect = False
        Me.cgChildren.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgChildren.Appearance.Options.UseFont = True
        Me.cgChildren.AutoSizeByData = True
        Me.cgChildren.DisableAutoSize = False
        Me.cgChildren.DisableDataFormatting = False
        Me.cgChildren.Dock = DockStyle.Fill
        Me.cgChildren.FocusedRowHandle = -2147483648
        Me.cgChildren.HideFirstColumn = False
        Me.cgChildren.Location = New System.Drawing.Point(0, 0)
        Me.cgChildren.Name = "cgChildren"
        Me.cgChildren.PreviewColumn = ""
        Me.cgChildren.QueryID = Nothing
        Me.cgChildren.RowAutoHeight = False
        Me.cgChildren.SearchAsYouType = True
        Me.cgChildren.ShowAutoFilterRow = False
        Me.cgChildren.ShowFindPanel = False
        Me.cgChildren.ShowGroupByBox = True
        Me.cgChildren.ShowLoadingPanel = False
        Me.cgChildren.ShowNavigator = False
        Me.cgChildren.Size = New System.Drawing.Size(569, 418)
        Me.cgChildren.TabIndex = 0
        '
        'gbxHeader
        '
        Me.gbxHeader.Controls.Add(Me.radHistorical)
        Me.gbxHeader.Controls.Add(Me.radRealtime)
        Me.gbxHeader.Location = New System.Drawing.Point(12, 10)
        Me.gbxHeader.Name = "gbxHeader"
        Me.gbxHeader.ShowCaption = False
        Me.gbxHeader.Size = New System.Drawing.Size(154, 86)
        Me.gbxHeader.TabIndex = 0
        '
        'radHistorical
        '
        Me.radHistorical.Location = New System.Drawing.Point(9, 59)
        Me.radHistorical.Name = "radHistorical"
        Me.radHistorical.Properties.AutoWidth = True
        Me.radHistorical.Properties.Caption = "Historical (Specify Date)"
        Me.radHistorical.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radHistorical.Properties.RadioGroupIndex = 0
        Me.radHistorical.Size = New System.Drawing.Size(137, 19)
        Me.radHistorical.TabIndex = 1
        Me.radHistorical.TabStop = False
        '
        'radRealtime
        '
        Me.radRealtime.EditValue = True
        Me.radRealtime.Location = New System.Drawing.Point(9, 10)
        Me.radRealtime.Name = "radRealtime"
        Me.radRealtime.Properties.AutoWidth = True
        Me.radRealtime.Properties.Caption = "Reatime (Today/Now)"
        Me.radRealtime.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radRealtime.Properties.RadioGroupIndex = 0
        Me.radRealtime.Size = New System.Drawing.Size(127, 19)
        Me.radRealtime.TabIndex = 0
        '
        'gbxHistorical
        '
        Me.gbxHistorical.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.gbxHistorical.Controls.Add(Me.radHistVisitors)
        Me.gbxHistorical.Controls.Add(Me.radHistRegister)
        Me.gbxHistorical.Controls.Add(Me.radHistAbsence)
        Me.gbxHistorical.Controls.Add(Me.cdtDate)
        Me.gbxHistorical.Controls.Add(Me.btnRefresh)
        Me.gbxHistorical.Location = New System.Drawing.Point(172, 55)
        Me.gbxHistorical.Name = "gbxHistorical"
        Me.gbxHistorical.ShowCaption = False
        Me.gbxHistorical.Size = New System.Drawing.Size(837, 41)
        Me.gbxHistorical.TabIndex = 2
        '
        'radHistRegister
        '
        Me.radHistRegister.EditValue = True
        Me.radHistRegister.Location = New System.Drawing.Point(200, 12)
        Me.radHistRegister.Name = "radHistRegister"
        Me.radHistRegister.Properties.AutoWidth = True
        Me.radHistRegister.Properties.Caption = "Register"
        Me.radHistRegister.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radHistRegister.Properties.RadioGroupIndex = 0
        Me.radHistRegister.Size = New System.Drawing.Size(62, 19)
        Me.radHistRegister.TabIndex = 2
        '
        'radHistAbsence
        '
        Me.radHistAbsence.Location = New System.Drawing.Point(268, 12)
        Me.radHistAbsence.Name = "radHistAbsence"
        Me.radHistAbsence.Properties.AutoWidth = True
        Me.radHistAbsence.Properties.Caption = "Absences"
        Me.radHistAbsence.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radHistAbsence.Properties.RadioGroupIndex = 0
        Me.radHistAbsence.Size = New System.Drawing.Size(68, 19)
        Me.radHistAbsence.TabIndex = 3
        Me.radHistAbsence.TabStop = False
        '
        'cdtDate
        '
        Me.cdtDate.EditValue = Nothing
        Me.cdtDate.EnterMoveNextControl = True
        Me.cdtDate.Location = New System.Drawing.Point(11, 11)
        Me.cdtDate.Name = "cdtDate"
        Me.cdtDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtDate.Properties.Appearance.Options.UseFont = True
        Me.cdtDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtDate.Size = New System.Drawing.Size(97, 22)
        Me.cdtDate.TabIndex = 0
        Me.cdtDate.Value = Nothing
        '
        'btnRefresh
        '
        Me.btnRefresh.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefresh.Appearance.Options.UseFont = True
        Me.btnRefresh.Location = New System.Drawing.Point(114, 10)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(80, 22)
        Me.btnRefresh.TabIndex = 1
        Me.btnRefresh.Tag = ""
        Me.btnRefresh.Text = "Refresh"
        '
        'gbxRealtime
        '
        Me.gbxRealtime.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.gbxRealtime.Controls.Add(Me.radVisitors)
        Me.gbxRealtime.Controls.Add(Me.radCheckedIn)
        Me.gbxRealtime.Controls.Add(Me.radCheckedOut)
        Me.gbxRealtime.Controls.Add(Me.radAbsent)
        Me.gbxRealtime.Location = New System.Drawing.Point(172, 10)
        Me.gbxRealtime.Name = "gbxRealtime"
        Me.gbxRealtime.ShowCaption = False
        Me.gbxRealtime.Size = New System.Drawing.Size(837, 39)
        Me.gbxRealtime.TabIndex = 1
        '
        'radCheckedIn
        '
        Me.radCheckedIn.EditValue = True
        Me.radCheckedIn.Location = New System.Drawing.Point(9, 10)
        Me.radCheckedIn.Name = "radCheckedIn"
        Me.radCheckedIn.Properties.AutoWidth = True
        Me.radCheckedIn.Properties.Caption = "Checked-In"
        Me.radCheckedIn.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radCheckedIn.Properties.RadioGroupIndex = 0
        Me.radCheckedIn.Size = New System.Drawing.Size(77, 19)
        Me.radCheckedIn.TabIndex = 0
        '
        'radCheckedOut
        '
        Me.radCheckedOut.Location = New System.Drawing.Point(92, 10)
        Me.radCheckedOut.Name = "radCheckedOut"
        Me.radCheckedOut.Properties.AutoWidth = True
        Me.radCheckedOut.Properties.Caption = "Checked Out"
        Me.radCheckedOut.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radCheckedOut.Properties.RadioGroupIndex = 0
        Me.radCheckedOut.Size = New System.Drawing.Size(84, 19)
        Me.radCheckedOut.TabIndex = 1
        Me.radCheckedOut.TabStop = False
        '
        'radAbsent
        '
        Me.radAbsent.Location = New System.Drawing.Point(182, 10)
        Me.radAbsent.Name = "radAbsent"
        Me.radAbsent.Properties.AutoWidth = True
        Me.radAbsent.Properties.Caption = "Absent"
        Me.radAbsent.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radAbsent.Properties.RadioGroupIndex = 0
        Me.radAbsent.Size = New System.Drawing.Size(56, 19)
        Me.radAbsent.TabIndex = 2
        Me.radAbsent.TabStop = False
        '
        'radVisitors
        '
        Me.radVisitors.Location = New System.Drawing.Point(244, 10)
        Me.radVisitors.Name = "radVisitors"
        Me.radVisitors.Properties.AutoWidth = True
        Me.radVisitors.Properties.Caption = "Visitors"
        Me.radVisitors.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radVisitors.Properties.RadioGroupIndex = 0
        Me.radVisitors.Size = New System.Drawing.Size(56, 19)
        Me.radVisitors.TabIndex = 3
        Me.radVisitors.TabStop = False
        '
        'radHistVisitors
        '
        Me.radHistVisitors.Location = New System.Drawing.Point(342, 12)
        Me.radHistVisitors.Name = "radHistVisitors"
        Me.radHistVisitors.Properties.AutoWidth = True
        Me.radHistVisitors.Properties.Caption = "Visitors"
        Me.radHistVisitors.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radHistVisitors.Properties.RadioGroupIndex = 0
        Me.radHistVisitors.Size = New System.Drawing.Size(56, 19)
        Me.radHistVisitors.TabIndex = 4
        Me.radHistVisitors.TabStop = False
        '
        'frmRegister
        '
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(1021, 560)
        Me.Controls.Add(Me.gbxRealtime)
        Me.Controls.Add(Me.gbxHistorical)
        Me.Controls.Add(Me.gbxHeader)
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Controls.Add(Me.txtRed)
        Me.Controls.Add(Me.txtYellow)
        Me.Controls.Add(Me.txtBlue)
        Me.Controls.Add(Me.txtGreen)
        Me.Controls.Add(Me.btnClose)
        Me.MaximizeBox = False
        Me.Name = "frmRegister"
        Me.WindowState = FormWindowState.Maximized
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBlue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.gbxHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxHeader.ResumeLayout(False)
        CType(Me.radHistorical.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radRealtime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxHistorical, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxHistorical.ResumeLayout(False)
        Me.gbxHistorical.PerformLayout()
        CType(Me.radHistRegister.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radHistAbsence.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxRealtime, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxRealtime.ResumeLayout(False)
        Me.gbxRealtime.PerformLayout()
        CType(Me.radCheckedIn.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radCheckedOut.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radAbsent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radVisitors.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radHistVisitors.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents txtGreen As Care.Controls.CareTextBox
    Friend WithEvents txtBlue As Care.Controls.CareTextBox
    Friend WithEvents txtYellow As Care.Controls.CareTextBox
    Friend WithEvents txtRed As Care.Controls.CareTextBox
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents cgStaff As Care.Controls.CareGrid
    Friend WithEvents cgChildren As Care.Controls.CareGrid
    Friend WithEvents gbxHeader As DevExpress.XtraEditors.GroupControl
    Friend WithEvents radRealtime As Care.Controls.CareRadioButton
    Friend WithEvents radHistorical As Care.Controls.CareRadioButton
    Friend WithEvents gbxHistorical As DevExpress.XtraEditors.GroupControl
    Friend WithEvents radHistRegister As Care.Controls.CareRadioButton
    Friend WithEvents radHistAbsence As Care.Controls.CareRadioButton
    Friend WithEvents cdtDate As Care.Controls.CareDateTime
    Friend WithEvents btnRefresh As Care.Controls.CareButton
    Friend WithEvents gbxRealtime As DevExpress.XtraEditors.GroupControl
    Friend WithEvents radCheckedIn As Care.Controls.CareRadioButton
    Friend WithEvents radCheckedOut As Care.Controls.CareRadioButton
    Friend WithEvents radAbsent As Care.Controls.CareRadioButton
    Friend WithEvents radHistVisitors As Care.Controls.CareRadioButton
    Friend WithEvents radVisitors As Care.Controls.CareRadioButton

End Class
