﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmWUFOO
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.tabMain = New Care.Controls.CareTab(Me.components)
        Me.tabFormSetup = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupBox1 = New Care.Controls.CareFrame()
        Me.cbxSites = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.txtLastEntry = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.txtName = New Care.Controls.CareTextBox(Me.components)
        Me.cbxMode = New Care.Controls.CareComboBox(Me.components)
        Me.txtFormName = New Care.Controls.CareTextBox(Me.components)
        Me.Label3 = New Care.Controls.CareLabel(Me.components)
        Me.Label2 = New Care.Controls.CareLabel(Me.components)
        Me.Label1 = New Care.Controls.CareLabel(Me.components)
        Me.gbxEntries = New Care.Controls.CareFrame()
        Me.btnFetchNew = New Care.Controls.CareButton(Me.components)
        Me.btnImportEntry = New Care.Controls.CareButton(Me.components)
        Me.cgEntries = New Care.Controls.CareGrid()
        Me.tabMapping = New DevExpress.XtraTab.XtraTabPage()
        Me.gbxMappingHeader = New Care.Controls.CareFrame()
        Me.btnMap = New Care.Controls.CareButton(Me.components)
        Me.btnClearMapping = New Care.Controls.CareButton(Me.components)
        Me.btnFormFields = New Care.Controls.CareButton(Me.components)
        Me.gbxField = New Care.Controls.CareFrame()
        Me.btnDelete = New Care.Controls.CareButton(Me.components)
        Me.btnClear = New Care.Controls.CareButton(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.txtTitle = New Care.Controls.CareTextBox(Me.components)
        Me.btnMapOK = New Care.Controls.CareButton(Me.components)
        Me.btnMapCancel = New Care.Controls.CareButton(Me.components)
        Me.txtOverride = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.cbxProperty = New Care.Controls.CareComboBox(Me.components)
        Me.cbxObject = New Care.Controls.CareComboBox(Me.components)
        Me.chkOverride = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.txtID = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.cgFields = New Care.Controls.CareGrid()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.tabMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabMain.SuspendLayout()
        Me.tabFormSetup.SuspendLayout()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.cbxSites.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLastEntry.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxMode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFormName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxEntries, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxEntries.SuspendLayout()
        Me.tabMapping.SuspendLayout()
        CType(Me.gbxMappingHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxMappingHeader.SuspendLayout()
        CType(Me.gbxField, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxField.SuspendLayout()
        CType(Me.txtTitle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOverride.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxProperty.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxObject.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkOverride.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(6731, 3)
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(6640, 3)
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Left), AnchorStyles)
        Me.Panel1.Dock = DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 625)
        Me.Panel1.Size = New System.Drawing.Size(1008, 36)
        Me.Panel1.TabIndex = 1
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'tabMain
        '
        Me.tabMain.Location = New System.Drawing.Point(12, 53)
        Me.tabMain.Name = "tabMain"
        Me.tabMain.SelectedTabPage = Me.tabFormSetup
        Me.tabMain.Size = New System.Drawing.Size(989, 566)
        Me.tabMain.TabIndex = 0
        Me.tabMain.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabFormSetup, Me.tabMapping})
        '
        'tabFormSetup
        '
        Me.tabFormSetup.Controls.Add(Me.GroupBox1)
        Me.tabFormSetup.Controls.Add(Me.gbxEntries)
        Me.tabFormSetup.Name = "tabFormSetup"
        Me.tabFormSetup.Size = New System.Drawing.Size(983, 538)
        Me.tabFormSetup.Text = "Form Setup"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cbxSites)
        Me.GroupBox1.Controls.Add(Me.CareLabel4)
        Me.GroupBox1.Controls.Add(Me.txtLastEntry)
        Me.GroupBox1.Controls.Add(Me.CareLabel3)
        Me.GroupBox1.Controls.Add(Me.txtName)
        Me.GroupBox1.Controls.Add(Me.cbxMode)
        Me.GroupBox1.Controls.Add(Me.txtFormName)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(11, 9)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(442, 176)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.Text = "Form Details"
        '
        'cbxSites
        '
        Me.cbxSites.AllowBlank = False
        Me.cbxSites.DataSource = Nothing
        Me.cbxSites.DisplayMember = Nothing
        Me.cbxSites.EnterMoveNextControl = True
        Me.cbxSites.Location = New System.Drawing.Point(113, 108)
        Me.cbxSites.Name = "cbxSites"
        Me.cbxSites.Properties.AccessibleName = "Import Type"
        Me.cbxSites.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSites.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSites.Properties.Appearance.Options.UseFont = True
        Me.cbxSites.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSites.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSites.SelectedValue = Nothing
        Me.cbxSites.Size = New System.Drawing.Size(310, 22)
        Me.cbxSites.TabIndex = 7
        Me.cbxSites.Tag = "AE"
        Me.cbxSites.ValueMember = Nothing
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(8, 111)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(60, 15)
        Me.CareLabel4.TabIndex = 6
        Me.CareLabel4.Text = "Default Site"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLastEntry
        '
        Me.txtLastEntry.CharacterCasing = CharacterCasing.Normal
        Me.txtLastEntry.EnterMoveNextControl = True
        Me.txtLastEntry.Location = New System.Drawing.Point(113, 136)
        Me.txtLastEntry.MaxLength = 5
        Me.txtLastEntry.Name = "txtLastEntry"
        Me.txtLastEntry.NumericAllowNegatives = False
        Me.txtLastEntry.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtLastEntry.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLastEntry.Properties.AccessibleName = "Forename"
        Me.txtLastEntry.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtLastEntry.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLastEntry.Properties.Appearance.Options.UseFont = True
        Me.txtLastEntry.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLastEntry.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtLastEntry.Properties.Mask.EditMask = "##########;"
        Me.txtLastEntry.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtLastEntry.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtLastEntry.Properties.MaxLength = 5
        Me.txtLastEntry.Size = New System.Drawing.Size(65, 22)
        Me.txtLastEntry.TabIndex = 9
        Me.txtLastEntry.Tag = "AE"
        Me.txtLastEntry.TextAlign = HorizontalAlignment.Left
        Me.txtLastEntry.ToolTipText = ""
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(8, 139)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(70, 15)
        Me.CareLabel3.TabIndex = 8
        Me.CareLabel3.Text = "Last Entry No"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.CharacterCasing = CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(113, 28)
        Me.txtName.MaxLength = 30
        Me.txtName.Name = "txtName"
        Me.txtName.NumericAllowNegatives = False
        Me.txtName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AccessibleName = "Import Name"
        Me.txtName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtName.Properties.MaxLength = 30
        Me.txtName.Size = New System.Drawing.Size(310, 22)
        Me.txtName.TabIndex = 1
        Me.txtName.Tag = "AEM"
        Me.txtName.TextAlign = HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'cbxMode
        '
        Me.cbxMode.AllowBlank = False
        Me.cbxMode.DataSource = Nothing
        Me.cbxMode.DisplayMember = Nothing
        Me.cbxMode.EnterMoveNextControl = True
        Me.cbxMode.Location = New System.Drawing.Point(113, 80)
        Me.cbxMode.Name = "cbxMode"
        Me.cbxMode.Properties.AccessibleName = "Import Type"
        Me.cbxMode.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxMode.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxMode.Properties.Appearance.Options.UseFont = True
        Me.cbxMode.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxMode.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxMode.SelectedValue = Nothing
        Me.cbxMode.Size = New System.Drawing.Size(164, 22)
        Me.cbxMode.TabIndex = 5
        Me.cbxMode.Tag = "AEM"
        Me.cbxMode.ValueMember = Nothing
        '
        'txtFormName
        '
        Me.txtFormName.CharacterCasing = CharacterCasing.Normal
        Me.txtFormName.EnterMoveNextControl = True
        Me.txtFormName.Location = New System.Drawing.Point(113, 54)
        Me.txtFormName.MaxLength = 30
        Me.txtFormName.Name = "txtFormName"
        Me.txtFormName.NumericAllowNegatives = False
        Me.txtFormName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtFormName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFormName.Properties.AccessibleName = "WUFoo Form Name"
        Me.txtFormName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFormName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFormName.Properties.Appearance.Options.UseFont = True
        Me.txtFormName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFormName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFormName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFormName.Properties.MaxLength = 30
        Me.txtFormName.Size = New System.Drawing.Size(310, 22)
        Me.txtFormName.TabIndex = 3
        Me.txtFormName.Tag = "AEM"
        Me.txtFormName.TextAlign = HorizontalAlignment.Left
        Me.txtFormName.ToolTipText = ""
        '
        'Label3
        '
        Me.Label3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label3.Location = New System.Drawing.Point(8, 57)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 15)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Form Name"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label2.Location = New System.Drawing.Point(8, 83)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 15)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Import Type"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label1.Location = New System.Drawing.Point(8, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Name"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbxEntries
        '
        Me.gbxEntries.Controls.Add(Me.btnFetchNew)
        Me.gbxEntries.Controls.Add(Me.btnImportEntry)
        Me.gbxEntries.Controls.Add(Me.cgEntries)
        Me.gbxEntries.Location = New System.Drawing.Point(459, 9)
        Me.gbxEntries.Name = "gbxEntries"
        Me.gbxEntries.Size = New System.Drawing.Size(516, 521)
        Me.gbxEntries.TabIndex = 1
        Me.gbxEntries.Text = "Entries available for Importing"
        Me.gbxEntries.UseDisabledStatePainter = False
        '
        'btnFetchNew
        '
        Me.btnFetchNew.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnFetchNew.Appearance.Options.UseFont = True
        Me.btnFetchNew.Location = New System.Drawing.Point(5, 493)
        Me.btnFetchNew.Name = "btnFetchNew"
        Me.btnFetchNew.Size = New System.Drawing.Size(160, 23)
        Me.btnFetchNew.TabIndex = 1
        Me.btnFetchNew.Text = "Fetch New Entries"
        '
        'btnImportEntry
        '
        Me.btnImportEntry.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnImportEntry.Appearance.Options.UseFont = True
        Me.btnImportEntry.Location = New System.Drawing.Point(171, 493)
        Me.btnImportEntry.Name = "btnImportEntry"
        Me.btnImportEntry.Size = New System.Drawing.Size(160, 23)
        Me.btnImportEntry.TabIndex = 2
        Me.btnImportEntry.Text = "Import Entry"
        '
        'cgEntries
        '
        Me.cgEntries.AllowBuildColumns = True
        Me.cgEntries.AllowEdit = False
        Me.cgEntries.AllowHorizontalScroll = False
        Me.cgEntries.AllowMultiSelect = False
        Me.cgEntries.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgEntries.Appearance.Options.UseFont = True
        Me.cgEntries.AutoSizeByData = True
        Me.cgEntries.DisableAutoSize = False
        Me.cgEntries.DisableDataFormatting = False
        Me.cgEntries.FocusedRowHandle = -2147483648
        Me.cgEntries.HideFirstColumn = False
        Me.cgEntries.Location = New System.Drawing.Point(5, 24)
        Me.cgEntries.Name = "cgEntries"
        Me.cgEntries.PreviewColumn = ""
        Me.cgEntries.QueryID = Nothing
        Me.cgEntries.RowAutoHeight = False
        Me.cgEntries.SearchAsYouType = True
        Me.cgEntries.ShowAutoFilterRow = False
        Me.cgEntries.ShowFindPanel = False
        Me.cgEntries.ShowGroupByBox = True
        Me.cgEntries.ShowLoadingPanel = False
        Me.cgEntries.ShowNavigator = False
        Me.cgEntries.Size = New System.Drawing.Size(506, 463)
        Me.cgEntries.TabIndex = 1
        '
        'tabMapping
        '
        Me.tabMapping.Controls.Add(Me.gbxMappingHeader)
        Me.tabMapping.Controls.Add(Me.gbxField)
        Me.tabMapping.Controls.Add(Me.cgFields)
        Me.tabMapping.Name = "tabMapping"
        Me.tabMapping.Size = New System.Drawing.Size(983, 538)
        Me.tabMapping.Text = "Field Mapping"
        '
        'gbxMappingHeader
        '
        Me.gbxMappingHeader.Controls.Add(Me.btnMap)
        Me.gbxMappingHeader.Controls.Add(Me.btnClearMapping)
        Me.gbxMappingHeader.Controls.Add(Me.btnFormFields)
        Me.gbxMappingHeader.Location = New System.Drawing.Point(3, 6)
        Me.gbxMappingHeader.Name = "gbxMappingHeader"
        Me.gbxMappingHeader.ShowCaption = False
        Me.gbxMappingHeader.Size = New System.Drawing.Size(977, 35)
        Me.gbxMappingHeader.TabIndex = 0
        Me.gbxMappingHeader.Text = "Personal Details"
        '
        'btnMap
        '
        Me.btnMap.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnMap.Appearance.Options.UseFont = True
        Me.btnMap.Location = New System.Drawing.Point(337, 6)
        Me.btnMap.Name = "btnMap"
        Me.btnMap.Size = New System.Drawing.Size(160, 23)
        Me.btnMap.TabIndex = 2
        Me.btnMap.Text = "Auto Map"
        '
        'btnClearMapping
        '
        Me.btnClearMapping.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClearMapping.Appearance.Options.UseFont = True
        Me.btnClearMapping.Location = New System.Drawing.Point(171, 6)
        Me.btnClearMapping.Name = "btnClearMapping"
        Me.btnClearMapping.Size = New System.Drawing.Size(160, 23)
        Me.btnClearMapping.TabIndex = 1
        Me.btnClearMapping.Text = "Clear Mapping"
        '
        'btnFormFields
        '
        Me.btnFormFields.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnFormFields.Appearance.Options.UseFont = True
        Me.btnFormFields.Location = New System.Drawing.Point(5, 6)
        Me.btnFormFields.Name = "btnFormFields"
        Me.btnFormFields.Size = New System.Drawing.Size(160, 23)
        Me.btnFormFields.TabIndex = 0
        Me.btnFormFields.Text = "Fetch Form Fields"
        '
        'gbxField
        '
        Me.gbxField.Controls.Add(Me.btnDelete)
        Me.gbxField.Controls.Add(Me.btnClear)
        Me.gbxField.Controls.Add(Me.CareLabel1)
        Me.gbxField.Controls.Add(Me.txtTitle)
        Me.gbxField.Controls.Add(Me.btnMapOK)
        Me.gbxField.Controls.Add(Me.btnMapCancel)
        Me.gbxField.Controls.Add(Me.txtOverride)
        Me.gbxField.Controls.Add(Me.CareLabel2)
        Me.gbxField.Controls.Add(Me.cbxProperty)
        Me.gbxField.Controls.Add(Me.cbxObject)
        Me.gbxField.Controls.Add(Me.chkOverride)
        Me.gbxField.Controls.Add(Me.CareLabel6)
        Me.gbxField.Controls.Add(Me.CareLabel7)
        Me.gbxField.Controls.Add(Me.txtID)
        Me.gbxField.Controls.Add(Me.CareLabel8)
        Me.gbxField.Location = New System.Drawing.Point(270, 156)
        Me.gbxField.Name = "gbxField"
        Me.gbxField.Size = New System.Drawing.Size(442, 198)
        Me.gbxField.TabIndex = 2
        Me.gbxField.Text = "Edit Mapping"
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnDelete.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnDelete.Appearance.Options.UseFont = True
        Me.btnDelete.Location = New System.Drawing.Point(103, 165)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(85, 23)
        Me.btnDelete.TabIndex = 12
        Me.btnDelete.Text = "Delete"
        '
        'btnClear
        '
        Me.btnClear.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnClear.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClear.Appearance.Options.UseFont = True
        Me.btnClear.Location = New System.Drawing.Point(12, 165)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(85, 23)
        Me.btnClear.TabIndex = 11
        Me.btnClear.Text = "Clear"
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(12, 57)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(23, 15)
        Me.CareLabel1.TabIndex = 2
        Me.CareLabel1.Text = "Title"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTitle
        '
        Me.txtTitle.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtTitle.CharacterCasing = CharacterCasing.Normal
        Me.txtTitle.EnterMoveNextControl = True
        Me.txtTitle.Location = New System.Drawing.Point(113, 54)
        Me.txtTitle.MaxLength = 60
        Me.txtTitle.Name = "txtTitle"
        Me.txtTitle.NumericAllowNegatives = False
        Me.txtTitle.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtTitle.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTitle.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtTitle.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTitle.Properties.Appearance.Options.UseFont = True
        Me.txtTitle.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTitle.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtTitle.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTitle.Properties.MaxLength = 60
        Me.txtTitle.Properties.ReadOnly = True
        Me.txtTitle.Size = New System.Drawing.Size(310, 20)
        Me.txtTitle.TabIndex = 3
        Me.txtTitle.Tag = ""
        Me.txtTitle.TextAlign = HorizontalAlignment.Left
        Me.txtTitle.ToolTipText = ""
        '
        'btnMapOK
        '
        Me.btnMapOK.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnMapOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnMapOK.Appearance.Options.UseFont = True
        Me.btnMapOK.Location = New System.Drawing.Point(247, 165)
        Me.btnMapOK.Name = "btnMapOK"
        Me.btnMapOK.Size = New System.Drawing.Size(85, 23)
        Me.btnMapOK.TabIndex = 13
        Me.btnMapOK.Text = "OK"
        '
        'btnMapCancel
        '
        Me.btnMapCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnMapCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnMapCancel.Appearance.Options.UseFont = True
        Me.btnMapCancel.Location = New System.Drawing.Point(338, 165)
        Me.btnMapCancel.Name = "btnMapCancel"
        Me.btnMapCancel.Size = New System.Drawing.Size(85, 23)
        Me.btnMapCancel.TabIndex = 14
        Me.btnMapCancel.Text = "Cancel"
        '
        'txtOverride
        '
        Me.txtOverride.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtOverride.CharacterCasing = CharacterCasing.Normal
        Me.txtOverride.EnterMoveNextControl = True
        Me.txtOverride.Location = New System.Drawing.Point(137, 132)
        Me.txtOverride.MaxLength = 30
        Me.txtOverride.Name = "txtOverride"
        Me.txtOverride.NumericAllowNegatives = False
        Me.txtOverride.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtOverride.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtOverride.Properties.AccessibleName = "Surname"
        Me.txtOverride.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtOverride.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOverride.Properties.Appearance.Options.UseFont = True
        Me.txtOverride.Properties.Appearance.Options.UseTextOptions = True
        Me.txtOverride.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtOverride.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtOverride.Properties.MaxLength = 30
        Me.txtOverride.Size = New System.Drawing.Size(286, 20)
        Me.txtOverride.TabIndex = 10
        Me.txtOverride.Tag = ""
        Me.txtOverride.TextAlign = HorizontalAlignment.Left
        Me.txtOverride.ToolTipText = ""
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(12, 135)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(77, 15)
        Me.CareLabel2.TabIndex = 8
        Me.CareLabel2.Text = "Override Value"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxProperty
        '
        Me.cbxProperty.AllowBlank = False
        Me.cbxProperty.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxProperty.DataSource = Nothing
        Me.cbxProperty.DisplayMember = Nothing
        Me.cbxProperty.EnterMoveNextControl = True
        Me.cbxProperty.Location = New System.Drawing.Point(113, 106)
        Me.cbxProperty.Name = "cbxProperty"
        Me.cbxProperty.Properties.AccessibleName = "Contract Type"
        Me.cbxProperty.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxProperty.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxProperty.Properties.Appearance.Options.UseFont = True
        Me.cbxProperty.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxProperty.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxProperty.SelectedValue = Nothing
        Me.cbxProperty.Size = New System.Drawing.Size(310, 20)
        Me.cbxProperty.TabIndex = 7
        Me.cbxProperty.Tag = "E"
        Me.cbxProperty.ValueMember = Nothing
        '
        'cbxObject
        '
        Me.cbxObject.AllowBlank = False
        Me.cbxObject.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxObject.DataSource = Nothing
        Me.cbxObject.DisplayMember = Nothing
        Me.cbxObject.EnterMoveNextControl = True
        Me.cbxObject.Location = New System.Drawing.Point(113, 80)
        Me.cbxObject.Name = "cbxObject"
        Me.cbxObject.Properties.AccessibleName = "Status"
        Me.cbxObject.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxObject.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxObject.Properties.Appearance.Options.UseFont = True
        Me.cbxObject.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxObject.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxObject.SelectedValue = Nothing
        Me.cbxObject.Size = New System.Drawing.Size(310, 20)
        Me.cbxObject.TabIndex = 5
        Me.cbxObject.Tag = "E"
        Me.cbxObject.ValueMember = Nothing
        '
        'chkOverride
        '
        Me.chkOverride.EnterMoveNextControl = True
        Me.chkOverride.Location = New System.Drawing.Point(111, 133)
        Me.chkOverride.Name = "chkOverride"
        Me.chkOverride.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOverride.Properties.Appearance.Options.UseFont = True
        Me.chkOverride.Size = New System.Drawing.Size(20, 19)
        Me.chkOverride.TabIndex = 9
        Me.chkOverride.Tag = "E"
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(12, 83)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(35, 15)
        Me.CareLabel6.TabIndex = 4
        Me.CareLabel6.Text = "Object"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(12, 109)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(63, 15)
        Me.CareLabel7.TabIndex = 6
        Me.CareLabel7.Text = "Object Field"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtID
        '
        Me.txtID.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtID.CharacterCasing = CharacterCasing.Normal
        Me.txtID.EnterMoveNextControl = True
        Me.txtID.Location = New System.Drawing.Point(113, 28)
        Me.txtID.MaxLength = 60
        Me.txtID.Name = "txtID"
        Me.txtID.NumericAllowNegatives = False
        Me.txtID.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtID.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtID.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtID.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtID.Properties.Appearance.Options.UseFont = True
        Me.txtID.Properties.Appearance.Options.UseTextOptions = True
        Me.txtID.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtID.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtID.Properties.MaxLength = 60
        Me.txtID.Properties.ReadOnly = True
        Me.txtID.Size = New System.Drawing.Size(310, 20)
        Me.txtID.TabIndex = 1
        Me.txtID.Tag = ""
        Me.txtID.TextAlign = HorizontalAlignment.Left
        Me.txtID.ToolTipText = ""
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(12, 31)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(55, 15)
        Me.CareLabel8.TabIndex = 0
        Me.CareLabel8.Text = "Entry Field"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cgFields
        '
        Me.cgFields.AllowBuildColumns = True
        Me.cgFields.AllowEdit = False
        Me.cgFields.AllowHorizontalScroll = False
        Me.cgFields.AllowMultiSelect = False
        Me.cgFields.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgFields.Appearance.Options.UseFont = True
        Me.cgFields.AutoSizeByData = True
        Me.cgFields.DisableAutoSize = False
        Me.cgFields.DisableDataFormatting = False
        Me.cgFields.FocusedRowHandle = -2147483648
        Me.cgFields.HideFirstColumn = False
        Me.cgFields.Location = New System.Drawing.Point(3, 47)
        Me.cgFields.Name = "cgFields"
        Me.cgFields.PreviewColumn = ""
        Me.cgFields.QueryID = Nothing
        Me.cgFields.RowAutoHeight = False
        Me.cgFields.SearchAsYouType = True
        Me.cgFields.ShowAutoFilterRow = True
        Me.cgFields.ShowFindPanel = True
        Me.cgFields.ShowGroupByBox = True
        Me.cgFields.ShowLoadingPanel = False
        Me.cgFields.ShowNavigator = False
        Me.cgFields.Size = New System.Drawing.Size(977, 488)
        Me.cgFields.TabIndex = 1
        '
        'frmWUFOO
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(1008, 661)
        Me.Controls.Add(Me.tabMain)
        Me.Name = "frmWUFOO"
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.tabMain, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.tabMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabMain.ResumeLayout(False)
        Me.tabFormSetup.ResumeLayout(False)
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.cbxSites.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLastEntry.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxMode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFormName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxEntries, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxEntries.ResumeLayout(False)
        Me.tabMapping.ResumeLayout(False)
        CType(Me.gbxMappingHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxMappingHeader.ResumeLayout(False)
        CType(Me.gbxField, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxField.ResumeLayout(False)
        Me.gbxField.PerformLayout()
        CType(Me.txtTitle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOverride.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxProperty.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxObject.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkOverride.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tabMain As Care.Controls.CareTab
    Friend WithEvents tabFormSetup As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupBox1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cbxMode As Care.Controls.CareComboBox
    Friend WithEvents txtFormName As Care.Controls.CareTextBox
    Friend WithEvents Label3 As Care.Controls.CareLabel
    Friend WithEvents Label2 As Care.Controls.CareLabel
    Friend WithEvents Label1 As Care.Controls.CareLabel
    Friend WithEvents gbxEntries As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cgEntries As Care.Controls.CareGrid
    Friend WithEvents tabMapping As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cgFields As Care.Controls.CareGrid
    Friend WithEvents gbxField As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtOverride As Care.Controls.CareTextBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents cbxProperty As Care.Controls.CareComboBox
    Friend WithEvents cbxObject As Care.Controls.CareComboBox
    Friend WithEvents chkOverride As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents txtID As Care.Controls.CareTextBox
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents btnFetchNew As Care.Controls.CareButton
    Friend WithEvents btnImportEntry As Care.Controls.CareButton
    Friend WithEvents gbxMappingHeader As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnFormFields As Care.Controls.CareButton
    Friend WithEvents btnClearMapping As Care.Controls.CareButton
    Friend WithEvents btnMapOK As Care.Controls.CareButton
    Friend WithEvents btnMapCancel As Care.Controls.CareButton
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents txtTitle As Care.Controls.CareTextBox
    Friend WithEvents txtLastEntry As Care.Controls.CareTextBox
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents btnClear As Care.Controls.CareButton
    Friend WithEvents btnDelete As Care.Controls.CareButton
    Friend WithEvents btnMap As Care.Controls.CareButton
    Friend WithEvents cbxSites As Care.Controls.CareComboBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel

End Class
