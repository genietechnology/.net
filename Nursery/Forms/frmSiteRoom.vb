﻿
Imports Care.Global
Imports Care.Data

Public Class frmSiteRoom

    Private m_SiteID As Guid
    Private m_RoomID As Guid?
    Private m_Room As New Business.SiteRoom

    Public Sub New(ByVal SiteID As Guid, ByVal RoomID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_SiteID = SiteID
        m_RoomID = RoomID

    End Sub

    Private Sub frmSiteRoom_Load(sender As Object, e As EventArgs) Handles Me.Load

        MyControls.SetControls(Care.Shared.ControlHandler.Mode.Add, Me.Controls)
        ceColour.BackColor = SessiON.CHANGECOLOUR
        If m_RoomID.HasValue Then
            Me.Text = "Edit Room"
            DisplayRecord()
        Else
            Me.Text = "Create New Room"
            txtOpenFrom.Text = "00:00"
            txtOpenTo.Text = "23:59"
        End If

        txtName.Focus()

    End Sub

    Private Sub DisplayRecord()

        m_Room = Business.SiteRoom.RetreiveByID(m_RoomID.Value)
        With m_Room

            txtName.Text = ._RoomName
            ceColour.Text = ._RoomColour
            chkChild.Checked = ._RoomCheckChildren
            chkStaff.Checked = ._RoomCheckStaff
            txtSequence.Text = ._RoomSequence.ToString

            txtOpenFrom.EditValue = ._RoomHoursFrom
            txtOpenTo.EditValue = ._RoomHoursTo

            cdtCloseFrom.Value = ._RoomCloseFrom
            cdtCloseTo.Value = ._RoomCloseTo

            chkMonitor.Checked = ._RoomMonitor
            txtMonitorSerial.Text = ._RoomMonitorSerial

        End With

    End Sub

    Private Function CheckMandatory() As Boolean

        If txtName.Text = "" Then
            CareMessage("Room Name is mandatory. Please enter a room name.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        Return True

    End Function

    Private Sub Save()

        If Not m_RoomID.HasValue Then
            m_Room._SiteId = m_SiteID
        End If

        With m_Room
            ._RoomName = txtName.Text
            ._RoomHoursFrom = Care.Data.CleanData.ReturnTimeSpan(txtOpenFrom.Text)
            ._RoomHoursTo = Care.Data.CleanData.ReturnTimeSpan(txtOpenTo.Text)
            ._RoomCloseFrom = cdtCloseFrom.Value
            ._RoomCloseTo = cdtCloseTo.Value
            ._RoomCheckChildren = chkChild.Checked
            ._RoomCheckStaff = chkStaff.Checked
            ._RoomMonitor = chkMonitor.Checked
            ._RoomMonitorSerial = txtMonitorSerial.Text
            ._RoomColour = ceColour.Text
            ._RoomSequence = ValueHandler.ConvertInteger(txtSequence.Text)
            .Store()
        End With

        'update the site name on all children and staff
        Dim _SQL As String = "update Children set group_name = '" + m_Room._RoomName + "' where group_id = '" + m_Room._ID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "update Staff set group_name = '" + m_Room._RoomName + "' where group_id = '" + m_Room._ID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        If CheckMandatory() Then
            Save()
            Me.DialogResult = DialogResult.OK
            Me.Close()
        End If
    End Sub
End Class
