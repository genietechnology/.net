﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmCreditLine
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.gbxLine = New Care.Controls.CareFrame(Me.components)
        Me.lblRef1 = New Care.Controls.CareLabel(Me.components)
        Me.lblRef2 = New Care.Controls.CareLabel(Me.components)
        Me.lblRef3 = New Care.Controls.CareLabel(Me.components)
        Me.lblDayName = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel16 = New Care.Controls.CareLabel(Me.components)
        Me.cbxNLCode = New Care.Controls.CareComboBox(Me.components)
        Me.cbxNLTracking = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel17 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel13 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.txtLineValue = New Care.Controls.CareTextBox(Me.components)
        Me.txtLineDescription = New DevExpress.XtraEditors.MemoEdit()
        Me.cdtLineActionDate = New Care.Controls.CareDateTime(Me.components)
        Me.txtLineNo = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.btnLineCancel = New Care.Controls.CareButton(Me.components)
        Me.btnLineOK = New Care.Controls.CareButton(Me.components)
        CType(Me.gbxLine, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxLine.SuspendLayout()
        CType(Me.cbxNLCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxNLTracking.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLineValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLineDescription.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtLineActionDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtLineActionDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLineNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'gbxLine
        '
        Me.gbxLine.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.gbxLine.Controls.Add(Me.lblRef1)
        Me.gbxLine.Controls.Add(Me.lblRef2)
        Me.gbxLine.Controls.Add(Me.lblRef3)
        Me.gbxLine.Controls.Add(Me.lblDayName)
        Me.gbxLine.Controls.Add(Me.CareLabel16)
        Me.gbxLine.Controls.Add(Me.cbxNLCode)
        Me.gbxLine.Controls.Add(Me.cbxNLTracking)
        Me.gbxLine.Controls.Add(Me.CareLabel17)
        Me.gbxLine.Controls.Add(Me.CareLabel14)
        Me.gbxLine.Controls.Add(Me.CareLabel13)
        Me.gbxLine.Controls.Add(Me.CareLabel11)
        Me.gbxLine.Controls.Add(Me.txtLineValue)
        Me.gbxLine.Controls.Add(Me.txtLineDescription)
        Me.gbxLine.Controls.Add(Me.cdtLineActionDate)
        Me.gbxLine.Controls.Add(Me.txtLineNo)
        Me.gbxLine.Controls.Add(Me.CareLabel7)
        Me.gbxLine.Location = New System.Drawing.Point(14, 13)
        Me.gbxLine.Name = "gbxLine"
        Me.gbxLine.Size = New System.Drawing.Size(810, 645)
        Me.gbxLine.TabIndex = 0
        Me.gbxLine.Text = "Credit Line"
        '
        'lblRef1
        '
        Me.lblRef1.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.lblRef1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRef1.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblRef1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblRef1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.lblRef1.Location = New System.Drawing.Point(434, 36)
        Me.lblRef1.Name = "lblRef1"
        Me.lblRef1.Size = New System.Drawing.Size(363, 15)
        Me.lblRef1.TabIndex = 17
        Me.lblRef1.Text = "<Ref1>"
        Me.lblRef1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblRef2
        '
        Me.lblRef2.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.lblRef2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRef2.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblRef2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblRef2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.lblRef2.Location = New System.Drawing.Point(434, 64)
        Me.lblRef2.Name = "lblRef2"
        Me.lblRef2.Size = New System.Drawing.Size(363, 15)
        Me.lblRef2.TabIndex = 16
        Me.lblRef2.Text = "<Ref2>"
        Me.lblRef2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblRef3
        '
        Me.lblRef3.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.lblRef3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRef3.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblRef3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblRef3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.lblRef3.Location = New System.Drawing.Point(434, 617)
        Me.lblRef3.Name = "lblRef3"
        Me.lblRef3.Size = New System.Drawing.Size(363, 15)
        Me.lblRef3.TabIndex = 15
        Me.lblRef3.Text = "<Ref3>"
        Me.lblRef3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDayName
        '
        Me.lblDayName.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDayName.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblDayName.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblDayName.Location = New System.Drawing.Point(251, 64)
        Me.lblDayName.Name = "lblDayName"
        Me.lblDayName.Size = New System.Drawing.Size(68, 15)
        Me.lblDayName.TabIndex = 14
        Me.lblDayName.Text = "<Dayname>"
        Me.lblDayName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel16
        '
        Me.CareLabel16.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.CareLabel16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel16.Location = New System.Drawing.Point(475, 590)
        Me.CareLabel16.Name = "CareLabel16"
        Me.CareLabel16.Size = New System.Drawing.Size(64, 15)
        Me.CareLabel16.TabIndex = 8
        Me.CareLabel16.Text = "NL Tracking"
        Me.CareLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxNLCode
        '
        Me.cbxNLCode.AllowBlank = False
        Me.cbxNLCode.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.cbxNLCode.DataSource = Nothing
        Me.cbxNLCode.DisplayMember = Nothing
        Me.cbxNLCode.EnterMoveNextControl = True
        Me.cbxNLCode.Location = New System.Drawing.Point(128, 586)
        Me.cbxNLCode.Name = "cbxNLCode"
        Me.cbxNLCode.Properties.AccessibleName = "NL Code"
        Me.cbxNLCode.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxNLCode.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxNLCode.Properties.Appearance.Options.UseFont = True
        Me.cbxNLCode.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxNLCode.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxNLCode.SelectedValue = Nothing
        Me.cbxNLCode.Size = New System.Drawing.Size(240, 22)
        Me.cbxNLCode.TabIndex = 7
        Me.cbxNLCode.Tag = "AEM"
        Me.cbxNLCode.ValueMember = Nothing
        '
        'cbxNLTracking
        '
        Me.cbxNLTracking.AllowBlank = False
        Me.cbxNLTracking.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.cbxNLTracking.DataSource = Nothing
        Me.cbxNLTracking.DisplayMember = Nothing
        Me.cbxNLTracking.EnterMoveNextControl = True
        Me.cbxNLTracking.Location = New System.Drawing.Point(557, 586)
        Me.cbxNLTracking.Name = "cbxNLTracking"
        Me.cbxNLTracking.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxNLTracking.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxNLTracking.Properties.Appearance.Options.UseFont = True
        Me.cbxNLTracking.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxNLTracking.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxNLTracking.SelectedValue = Nothing
        Me.cbxNLTracking.Size = New System.Drawing.Size(240, 22)
        Me.cbxNLTracking.TabIndex = 9
        Me.cbxNLTracking.Tag = "AE"
        Me.cbxNLTracking.ValueMember = Nothing
        '
        'CareLabel17
        '
        Me.CareLabel17.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.CareLabel17.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel17.Location = New System.Drawing.Point(13, 590)
        Me.CareLabel17.Name = "CareLabel17"
        Me.CareLabel17.Size = New System.Drawing.Size(46, 15)
        Me.CareLabel17.TabIndex = 6
        Me.CareLabel17.Text = "NL Code"
        Me.CareLabel17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel14
        '
        Me.CareLabel14.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(13, 617)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(29, 15)
        Me.CareLabel14.TabIndex = 10
        Me.CareLabel14.Text = "Value"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel13
        '
        Me.CareLabel13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel13.Location = New System.Drawing.Point(13, 93)
        Me.CareLabel13.Name = "CareLabel13"
        Me.CareLabel13.Size = New System.Drawing.Size(60, 15)
        Me.CareLabel13.TabIndex = 4
        Me.CareLabel13.Text = "Description"
        Me.CareLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(13, 64)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(62, 15)
        Me.CareLabel11.TabIndex = 2
        Me.CareLabel11.Text = "Action Date"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLineValue
        '
        Me.txtLineValue.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.txtLineValue.CharacterCasing = CharacterCasing.Normal
        Me.txtLineValue.EnterMoveNextControl = True
        Me.txtLineValue.Location = New System.Drawing.Point(128, 614)
        Me.txtLineValue.MaxLength = 14
        Me.txtLineValue.Name = "txtLineValue"
        Me.txtLineValue.NumericAllowNegatives = True
        Me.txtLineValue.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtLineValue.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLineValue.Properties.AccessibleName = "Invoice Value"
        Me.txtLineValue.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtLineValue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtLineValue.Properties.Appearance.Options.UseFont = True
        Me.txtLineValue.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLineValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtLineValue.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txtLineValue.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtLineValue.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtLineValue.Properties.MaxLength = 14
        Me.txtLineValue.Size = New System.Drawing.Size(117, 22)
        Me.txtLineValue.TabIndex = 11
        Me.txtLineValue.Tag = "AEM"
        Me.txtLineValue.TextAlign = HorizontalAlignment.Left
        Me.txtLineValue.ToolTipText = ""
        '
        'txtLineDescription
        '
        Me.txtLineDescription.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtLineDescription.Location = New System.Drawing.Point(128, 89)
        Me.txtLineDescription.Name = "txtLineDescription"
        Me.txtLineDescription.Properties.AccessibleName = "Line Description"
        Me.txtLineDescription.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLineDescription.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtLineDescription, True)
        Me.txtLineDescription.Size = New System.Drawing.Size(669, 491)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtLineDescription, OptionsSpelling1)
        Me.txtLineDescription.TabIndex = 5
        Me.txtLineDescription.Tag = "AEM"
        '
        'cdtLineActionDate
        '
        Me.cdtLineActionDate.EditValue = Nothing
        Me.cdtLineActionDate.EnterMoveNextControl = True
        Me.cdtLineActionDate.Location = New System.Drawing.Point(128, 61)
        Me.cdtLineActionDate.Name = "cdtLineActionDate"
        Me.cdtLineActionDate.Properties.AccessibleName = "Action Date"
        Me.cdtLineActionDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtLineActionDate.Properties.Appearance.Options.UseFont = True
        Me.cdtLineActionDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtLineActionDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtLineActionDate.Size = New System.Drawing.Size(117, 22)
        Me.cdtLineActionDate.TabIndex = 3
        Me.cdtLineActionDate.Tag = "AEM"
        Me.cdtLineActionDate.Value = Nothing
        '
        'txtLineNo
        '
        Me.txtLineNo.CharacterCasing = CharacterCasing.Normal
        Me.txtLineNo.EnterMoveNextControl = True
        Me.txtLineNo.Location = New System.Drawing.Point(128, 33)
        Me.txtLineNo.MaxLength = 10
        Me.txtLineNo.Name = "txtLineNo"
        Me.txtLineNo.NumericAllowNegatives = False
        Me.txtLineNo.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtLineNo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLineNo.Properties.AccessibleName = "Line Number"
        Me.txtLineNo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtLineNo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtLineNo.Properties.Appearance.Options.UseFont = True
        Me.txtLineNo.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLineNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtLineNo.Properties.Mask.EditMask = "##########;"
        Me.txtLineNo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtLineNo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtLineNo.Properties.MaxLength = 10
        Me.txtLineNo.Size = New System.Drawing.Size(117, 22)
        Me.txtLineNo.TabIndex = 1
        Me.txtLineNo.Tag = "AEM"
        Me.txtLineNo.TextAlign = HorizontalAlignment.Left
        Me.txtLineNo.ToolTipText = ""
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(13, 36)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(41, 15)
        Me.CareLabel7.TabIndex = 0
        Me.CareLabel7.Text = "Line No"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnLineCancel
        '
        Me.btnLineCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnLineCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnLineCancel.Appearance.Options.UseFont = True
        Me.btnLineCancel.DialogResult = DialogResult.Cancel
        Me.btnLineCancel.Location = New System.Drawing.Point(736, 666)
        Me.btnLineCancel.Name = "btnLineCancel"
        Me.btnLineCancel.Size = New System.Drawing.Size(87, 27)
        Me.btnLineCancel.TabIndex = 2
        Me.btnLineCancel.Text = "Cancel"
        '
        'btnLineOK
        '
        Me.btnLineOK.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnLineOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnLineOK.Appearance.Options.UseFont = True
        Me.btnLineOK.Location = New System.Drawing.Point(642, 666)
        Me.btnLineOK.Name = "btnLineOK"
        Me.btnLineOK.Size = New System.Drawing.Size(87, 27)
        Me.btnLineOK.TabIndex = 1
        Me.btnLineOK.Text = "OK"
        '
        'frmCreditLine
        '
        Me.AcceptButton = Me.btnLineOK
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.CancelButton = Me.btnLineCancel
        Me.ClientSize = New System.Drawing.Size(834, 701)
        Me.Controls.Add(Me.btnLineCancel)
        Me.Controls.Add(Me.btnLineOK)
        Me.Controls.Add(Me.gbxLine)
        Me.KeyPreview = False
        Me.Margin = New Padding(3, 5, 3, 5)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCreditLine"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "frmCreditLine"
        CType(Me.gbxLine, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxLine.ResumeLayout(False)
        Me.gbxLine.PerformLayout()
        CType(Me.cbxNLCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxNLTracking.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLineValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLineDescription.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtLineActionDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtLineActionDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLineNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CareLabel16 As Care.Controls.CareLabel
    Friend WithEvents cbxNLCode As Care.Controls.CareComboBox
    Friend WithEvents cbxNLTracking As Care.Controls.CareComboBox
    Friend WithEvents CareLabel17 As Care.Controls.CareLabel
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents CareLabel13 As Care.Controls.CareLabel
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents txtLineValue As Care.Controls.CareTextBox
    Friend WithEvents txtLineDescription As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents cdtLineActionDate As Care.Controls.CareDateTime
    Friend WithEvents txtLineNo As Care.Controls.CareTextBox
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents btnLineCancel As Care.Controls.CareButton
    Friend WithEvents btnLineOK As Care.Controls.CareButton
    Friend WithEvents lblDayName As Care.Controls.CareLabel
    Friend WithEvents lblRef3 As Care.Controls.CareLabel
    Friend WithEvents lblRef1 As Care.Controls.CareLabel
    Friend WithEvents lblRef2 As Care.Controls.CareLabel
    Friend WithEvents gbxLine As Care.Controls.CareFrame
End Class
