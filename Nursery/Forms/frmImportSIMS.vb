﻿Imports Care.Global
Imports Care.Data
Imports Care.Shared

Public Class frmImportSIMS

    Private Sub btnImport_Click(sender As Object, e As EventArgs) Handles btnImport.Click

        If cbxSite.Text = "" Then Exit Sub
        If cbxGroup.Text = "" Then Exit Sub
        If cbxKeyworker.Text = "" Then Exit Sub

        Dim _SelectedRows As Integer() = cgRecords.UnderlyingGridView.GetSelectedRows()
        If _SelectedRows.Length > 0 AndAlso _SelectedRows.Length >= 1 Then

            If CareMessage("Are you sure you want to import the " + _SelectedRows.Length.ToString + " selected record(s)?", Windows.Forms.MessageBoxIcon.Exclamation, Windows.Forms.MessageBoxButtons.YesNo, "Change Records") = Windows.Forms.DialogResult.Yes Then

                Session.MDIForm.SetupProgressBar("Importing records...", _SelectedRows.Count)

                For Each _RowIndex As Integer In _SelectedRows

                    Dim _DR As DataRow = cgRecords.GetRow(_RowIndex)
                    If _DR IsNot Nothing Then

                        'we will create a family for each child (and check for duplicates later, manually)
                        Dim _FamilyID As Guid = Guid.NewGuid

                        'create family
                        Dim _f As New Business.Family
                        _f._ID = _FamilyID

                        _f._SiteId = New Guid(cbxSite.SelectedValue.ToString)
                        _f._SiteName = cbxSite.Text

                        _f._Surname = _DR.Item("surname").ToString

                        'build address
                        '**********************************************************************************************************
                        Dim _Apartment As String = _DR.Item("apartment").ToString.Trim
                        Dim _HouseName As String = _DR.Item("house_name").ToString.Trim
                        Dim _HouseNo As String = _DR.Item("house_number").ToString.Trim
                        Dim _Street As String = _DR.Item("street_description").ToString.Trim
                        Dim _District As String = _DR.Item("district").ToString.Trim
                        Dim _Town As String = _DR.Item("town").ToString.Trim
                        Dim _County As String = _DR.Item("county").ToString.Trim
                        Dim _PostCode As String = _DR.Item("postcode").ToString.Trim

                        _f._Address = ""

                        If _HouseNo <> "" Then
                            If _Apartment = "" AndAlso _HouseName = "" Then
                                _f._Address += _HouseNo + " " + _Street
                            Else
                                If _HouseName <> "" AndAlso _Apartment = "" Then
                                    _f._Address += _HouseName + vbCrLf + _Street
                                Else
                                    _f._Address += _Apartment + " " + _HouseName + vbCrLf + _HouseNo + " " + _Street
                                End If
                            End If
                        Else
                            If _HouseName <> "" AndAlso _Apartment = "" Then
                                _f._Address += _HouseName + vbCrLf + _Street
                            Else
                                _f._Address += _Apartment + " " + _HouseName + vbCrLf + _Street
                            End If
                        End If

                        If _District <> "" Then _f._Address += vbCrLf + _District
                        If _Town <> "" Then _f._Address += vbCrLf + _Town
                        If _County <> "" Then _f._Address += vbCrLf + _County
                        If _PostCode <> "" Then _f._Address += vbCrLf + _PostCode

                        '**********************************************************************************************************

                        'Process the contacts, returning the primary contacts salutation
                        Dim _PriSalutation As String = ProcessContacts(_DR.Item("person_id").ToString, _FamilyID, _f._Address)

                        _f._LetterName = _PriSalutation
                        _f.Store()

                        '**********************************************************************************************************

                        'create child
                        Dim _ChildID As Guid = Guid.NewGuid

                        Dim _c As New Business.Child

                        _c._ID = _ChildID
                        _c._SiteId = _f._SiteId
                        _c._SiteName = _f._SiteName

                        _c._FamilyId = _FamilyID
                        _c._FamilyName = _f._Surname

                        _c._Started = GetStartDate(_DR.Item("person_id").ToString)
                        If _c._Started.HasValue Then
                            If _c._Started < Today Then
                                _c._Status = "Current"
                            Else
                                _c._Status = "On Waiting List"
                            End If
                        End If

                        _c._Forename = _DR.Item("forename").ToString
                        _c._Surname = _DR.Item("surname").ToString
                        _c._Fullname = _c._Forename + " " + _c._Surname
                        _c._Knownas = _DR.Item("chosen_forename").ToString

                        _c._Dob = ValueHandler.ConvertDate(_DR.Item("dob"))
                        _c._Gender = _DR.Item("gender").ToString
                        _c._MedNotes = _DR.Item("medical_notes").ToString

                        _c._KeyworkerId = New Guid(cbxKeyworker.SelectedValue.ToString)
                        _c._KeyworkerName = cbxKeyworker.Text

                        _c._GroupId = New Guid(cbxGroup.SelectedValue.ToString)
                        _c._GroupName = cbxGroup.Text
                        _c._GroupStarted = _c._Started

                        Dim _Pic As Byte() = Nothing

                        Try
                            _Pic = CType(_DR.Item("photo"), Byte())

                        Catch ex As Exception

                        End Try

                        If _Pic IsNot Nothing Then
                            Dim _ID As Guid = DocumentHandler.InsertDocument(_c._FamilyId, DocumentHandler.DocumentType.Application, _Pic, "Photo of " + _c._Fullname)
                            _c._Photo = _ID
                        End If

                        _c.Store()

                        '**********************************************************************************************************

                        'map consent items
                        ImportConsent(_ChildID, _DR.Item("person_id").ToString)

                        'medical stuff
                        SetMedical(_ChildID, _DR.Item("person_id").ToString, _c._MedNotes)

                        '**********************************************************************************************************

                    End If

                    Session.MDIForm.StepProgressBar()

                Next

            End If

        End If

    End Sub

    Private Sub ImportConsent(ByVal ChildID As Guid, ByVal PersonID As String)

        Dim _SQL As String = ""
        _SQL += "select description from <s>.sims_parental_consent c"
        _SQL += " left Join <s>.sims_parental_consent_type ct on ct.lookup_value_id = c.parental_consent_type_id"
        _SQL += " where c.student_id = " + PersonID

        _SQL = _SQL.Replace("<s>", txtSchema.Text)

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(txtCN.Text, _SQL)
        If _DT IsNot Nothing Then
            For Each _DR As DataRow In _DT.Rows
                Dim _C As New Business.ChildConsent
                _C._ChildId = ChildID
                _C._Consent = _DR.Item("description").ToString.Trim
                _C._Given = True
                _C.Store()
            Next
        End If

    End Sub

    Private Sub SetMedical(ByVal ChildID As Guid, ByVal PersonID As String, ByRef MedicalConditions As String)

        Dim _Med As String = ""

        Dim _SQL As String = ""
        _SQL += "Select med.description from <s>.sims_person_medical_condition m"
        _SQL += " left join <s>.sims_medical_condition med on med.lookup_value_id = m.medical_condition_id"
        _SQL += " where m.person_id = " + PersonID

        _SQL = _SQL.Replace("<s>", txtSchema.Text)

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(txtCN.Text, _SQL)
        If _DT IsNot Nothing Then
            For Each _DR As DataRow In _DT.Rows
                If _Med = "" Then
                    _Med += _DR.Item("description").ToString.Trim
                Else
                    _Med += ", " + _DR.Item("description").ToString.Trim
                End If
            Next
        End If

        If _Med <> "" Then
            If MedicalConditions = "" Then
                MedicalConditions = _Med
            Else
                MedicalConditions += vbCrLf + _Med
            End If
        End If

    End Sub

    Private Function GetStartDate(ByVal PersonID As String) As Date

        Dim _Return As Date? = Nothing

        Dim _SQL As String = ""
        _SQL += "Select top 1 start_date from <s>.stud_school_history"
        _SQL += " where person_id = " + PersonID
        _SQL += " order by start_date"

        _SQL = _SQL.Replace("<s>", txtSchema.Text)

        Dim _DR As DataRow = DAL.GetRowfromSQL(txtCN.Text, _SQL)
        If _DR IsNot Nothing Then
            _Return = ValueHandler.ConvertDate(_DR.Item("start_date"))
        End If

        Return _Return

    End Function

    Private Function ProcessContacts(ByVal PersonID As String, ByVal FamilyID As Guid, ByVal Address As String) As String

        Dim _SQL As String = ""
        Dim _Return As String = ""

        _SQL += "Select rt.description As 'relation', r.parental_responsibility,"
        _SQL += " p.forename, p.surname, p.gender, p.dob, p.addressee, p.salutation"
        _SQL += " from <s>.stud_relation r"
        _SQL += " left Join <s>.sims_person p on p.person_id = r.contact_id"
        _SQL += " left Join <s>.stud_relation_type rt on rt.relation_type_id = r.relation_type_id"
        _SQL += " where r.student_id = " + PersonID
        _SQL += " order by r.priority"

        _SQL = _SQL.Replace("<s>", txtSchema.Text)

        Dim _dt As DataTable = DAL.GetDataTablefromSQL(txtCN.Text, _SQL)

        Dim _i As Integer = 1
        For Each _DR As DataRow In _dt.Rows

            Dim _c As New Business.Contact
            _c._FamilyId = FamilyID

            _c._Forename = _DR.Item("forename").ToString
            _c._Surname = _DR.Item("surname").ToString
            _c._Fullname = _c._Forename + " " + _c._Surname

            If _DR.Item("parental_responsibility").ToString = "T" Then
                _c._Parent = True
            End If

            If _i = 1 Then
                _c._PrimaryCont = True
                _c._EmerCont = True
                _c._Collect = True
                _c._InvoiceEmail = True
                _c._Report = True
                _c._ReportDetail = "Standard"
                _Return = _c._Fullname
            End If

            _c._Relationship = _DR.Item("relation").ToString
            _c._Dob = ValueHandler.ConvertDate(_DR.Item("dob"))
            _c._Address = Address

            'fetch the telephone numbers
            _SQL = ""
            _SQL += "select location, number from <s>.sims_telephone where person_id = " + PersonID
            _SQL = _SQL.Replace("<s>", txtSchema.Text)

            Dim _dtPhones As DataTable = DAL.GetDataTablefromSQL(txtCN.Text, _SQL)
            If _dtPhones IsNot Nothing Then
                For Each _DRP As DataRow In _dtPhones.Rows
                    If _DRP.Item("location").ToString = "H" Then _c._TelHome = _DRP.Item("number").ToString
                    If _DRP.Item("location").ToString = "W" Then _c._JobTel = _DRP.Item("number").ToString
                    If _DRP.Item("location").ToString = "M" Then _c._TelMobile = _DRP.Item("number").ToString
                Next
            End If

            'fetch email addresses
            _SQL = ""
            _SQL += "select location, email_address from <s>.sims_email where person_id = " + PersonID
            _SQL = _SQL.Replace("<s>", txtSchema.Text)

            Dim _dtEmails As DataTable = DAL.GetDataTablefromSQL(txtCN.Text, _SQL)
            If _dtEmails IsNot Nothing Then
                For Each _DRE As DataRow In _dtEmails.Rows
                    If _DRE.Item("location").ToString = "H" Then _c._Email = _DRE.Item("email_address").ToString
                    If _DRE.Item("location").ToString = "W" Then _c._JobEmail = _DRE.Item("email_address").ToString
                Next
            End If

            _c.Store()

            _i += 1

        Next

        Return _Return

    End Function

    Private Sub frmImportSIMS_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        txtCN.Text = Session.ConnectionString

        With cbxSite
            .AllowBlank = True
            .PopulateWithSQL(Session.ConnectionString, "Select id, name from sites order by name")
        End With

        With cbxKeyworker
            .AllowBlank = True
            .PopulateWithSQL(Session.ConnectionString, "Select id, fullname from staff where status = 'C' and keyworker = 1 order by fullname")
        End With

    End Sub

    Private Sub btnFetchData_Click(sender As Object, e As EventArgs) Handles btnFetchData.Click

        Dim _SQL As String = ""

        _SQL += "select s.person_id, s.current_ncyear, p.forename, p.surname, p.gender, p.chosen_forename, p.dob,"
        _SQL += " s.salutation,"
        _SQL += " adr.house_number, adr.house_name, adr.apartment, adr.street_description, adr.district, adr.town, adr.county, adr.postcode,"
        _SQL += " s.medical_notes, pic.photo"
        _SQL += " from <s>.stud_student s"
        _SQL += " left join <s>.sims_person p on p.person_id = s.person_id"
        _SQL += " left join <s>.sims_residence res on res.person_id = s.person_id and res.end_date is null"
        _SQL += " left join <s>.sims_address adr on adr.address_id = res.address_id"
        _SQL += " left join <s>.sims_photo pic on pic.photo_id = p.photo_id"

        If txtWhere.Text <> "" Then
            _SQL += " " + txtWhere.Text
        End If

        _SQL = _SQL.Replace("<s>", txtSchema.Text)

        cgRecords.Populate(txtCN.Text, _SQL)

    End Sub

    Private Sub cbxSite_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbxSite.SelectedValueChanged
        If cbxSite.Text = "" Then
            cbxGroup.Clear()
            cbxGroup.SelectedIndex = -1
        Else
            cbxGroup.PopulateWithSQL(Session.ConnectionString, Business.RoomRatios.RetreiveSiteRoomSQL(cbxSite.SelectedValue.ToString))
        End If
    End Sub
End Class