﻿
Imports Care.Global
Imports Care.Data
Imports System.Windows.Forms

Public Class frmAvailability

    Private m_AvailabiltySummary As New List(Of AvailabilitySummary)

    Private Sub frmAvailability_Load(sender As Object, e As EventArgs) Handles Me.Load

        cbxType.AddItem("Availability")
        cbxType.AddItem("Density")
        cbxType.SelectedIndex = 0

        cbxSite.PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)
        cbxSite.SelectedIndex = 0

        cdtFrom.Value = Today

    End Sub

    Private Sub frmAvailability_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        btnRefresh.Enabled = False
        Application.DoEvents()

        Session.CursorWaiting()

        Session.SetProgressMessage("Initialising Spaces...")
        InitialiseSpaces()

        Session.SetProgressMessage("Building Availability...")
        Populate()

        btnRefresh.Enabled = True
        Session.SetProgressMessage("Build Complete", 3)

        Session.CursorDefault()

    End Sub

    Private Sub InitialiseSpaces()

        DAL.ExecuteSQL(Session.ConnectionString, "truncate table Spaces")

        Dim _Eng As New SpaceEngine
        _Eng.InitialiseSpaces()

    End Sub

    Private Sub Populate()

        m_AvailabiltySummary.Clear()

        If cdtFrom.Text = "" Then Exit Sub
        If cbxSite.Text = "" Then Exit Sub

        Session.CursorWaiting()

        Dim _From As Date = cdtFrom.Value.Value

        Dim _SQL As String = ""
        _SQL += "select space_date, sum(bk_am) as 'bk_am', sum(bk_pm) as 'bk_pm', sum(wl_am) as 'wl_am', sum(wl_pm) as 'wl_pm', sum(capacity) as 'capacity' from Spaces"
        _SQL += " where space_date >= '" & ValueHandler.SQLDate(_From) & "'"
        _SQL += " and site_id = '" & cbxSite.SelectedValue.ToString & "'"

        If cbxRoom.Text <> "" Then
            _SQL += " and room_id = '" & cbxRoom.SelectedValue.ToString + "'"
        End If

        _SQL += " group by space_date"
        _SQL += " order by space_date"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _S As New AvailabilitySummary

                _S.BookingDate = ValueHandler.ConvertDate(_DR.Item("space_date")).Value

                If chkExclWL.Checked Then
                    _S.AM = ValueHandler.ConvertInteger(_DR.Item("bk_am"))
                    _S.PM = ValueHandler.ConvertInteger(_DR.Item("bk_pm"))
                Else
                    _S.AM = ValueHandler.ConvertInteger(_DR.Item("bk_am")) + ValueHandler.ConvertInteger(_DR.Item("wl_am"))
                    _S.PM = ValueHandler.ConvertInteger(_DR.Item("bk_pm")) + ValueHandler.ConvertInteger(_DR.Item("wl_pm"))
                End If

                _S.Capacity = ValueHandler.ConvertInteger(_DR.Item("capacity"))
                _S.VacanciesAM = _S.Capacity - _S.AM
                _S.VacanciesPM = _S.Capacity - _S.PM

                m_AvailabiltySummary.Add(_S)

            Next

            If cbxType.Text = "Availability" Then
                YearView1.Populate(m_AvailabiltySummary, _From, False)
            Else
                YearView1.Populate(m_AvailabiltySummary, _From, True)
            End If

        End If

        Session.CursorDefault()

    End Sub

    Private Sub btnPreview_Click(sender As Object, e As EventArgs)
        YearView1.Print()
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        Populate()
    End Sub

    Private Sub YearView1_DoubleClick(sender As Object, e As Nursery.YearView.YearViewDoubleClickArgs) Handles YearView1.DoubleClick
        If cbxRoom.Text = "" Then
            Bookings.BookingsDrillDown(e.HitDate.Value, New Guid(cbxSite.SelectedValue.ToString), Nothing, "Availability Drilldown")
        Else
            Bookings.BookingsDrillDown(e.HitDate.Value, New Guid(cbxSite.SelectedValue.ToString), New Guid(cbxRoom.SelectedValue.ToString), "Availability Drilldown")
        End If
    End Sub

    Private Sub cbxSite_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSite.SelectedIndexChanged
        If cbxSite.Text = "" Then
            cbxRoom.Clear()
            cbxRoom.SelectedIndex = -1
            cbxRoom.AllowBlank = False
        Else
            cbxRoom.AllowBlank = True
            cbxRoom.PopulateWithSQL(Session.ConnectionString, Business.RoomRatios.RetreiveSiteRoomSQL(cbxSite.SelectedValue.ToString))
        End If
    End Sub

    Private Sub cbxRoom_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxRoom.SelectedIndexChanged
        Populate()
    End Sub

    Private Sub cbxType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxType.SelectedIndexChanged
        Populate()
    End Sub

End Class
