﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMapping
    Inherits Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim ImageTilesLayer2 As DevExpress.XtraMap.ImageLayer = New DevExpress.XtraMap.ImageLayer()
        Dim BingMapDataProvider2 As DevExpress.XtraMap.BingMapDataProvider = New DevExpress.XtraMap.BingMapDataProvider()
        Me.MyMap = New DevExpress.XtraMap.MapControl()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.btnGenerate = New Care.Controls.CareButton()
        Me.timRefresh = New Timer()
        CType(Me.MyMap, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MyMap
        '
        Me.MyMap.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        BingMapDataProvider2.BingKey = "AsEyZ0Lpuc2YGT50FhbMMyuSE0iVVkg-_x63CJjKsh5KxIauIAOiFU10SMGSTrJ1"
        BingMapDataProvider2.TileSource = Nothing
        ImageTilesLayer2.DataProvider = BingMapDataProvider2
        Me.MyMap.Layers.Add(ImageTilesLayer2)
        Me.MyMap.Location = New System.Drawing.Point(12, 60)
        Me.MyMap.Name = "MyMap"
        Me.MyMap.Size = New System.Drawing.Size(676, 409)
        Me.MyMap.TabIndex = 0
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.btnGenerate)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(676, 42)
        Me.GroupControl1.TabIndex = 1
        Me.GroupControl1.Text = "GroupControl1"
        '
        'btnGenerate
        '
        Me.btnGenerate.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnGenerate.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnGenerate.Appearance.Options.UseFont = True
        Me.btnGenerate.Location = New System.Drawing.Point(7, 10)
        Me.btnGenerate.Name = "btnGenerate"
        Me.btnGenerate.Size = New System.Drawing.Size(149, 23)
        Me.btnGenerate.TabIndex = 2
        Me.btnGenerate.Text = "Generate GeoPoints"
        '
        'timRefresh
        '
        Me.timRefresh.Interval = 10000
        '
        'frmMapping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(700, 481)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.MyMap)
        Me.Name = "frmMapping"
        Me.Text = "frmMapping"
        Me.WindowState = FormWindowState.Maximized
        CType(Me.MyMap, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MyMap As DevExpress.XtraMap.MapControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnGenerate As Care.Controls.CareButton
    Friend WithEvents timRefresh As Timer
End Class
