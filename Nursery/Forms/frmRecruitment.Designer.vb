﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRecruitment
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ctMain = New Care.Controls.CareTab(Me.components)
        Me.tabVacancy = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl6 = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel18 = New Care.Controls.CareLabel(Me.components)
        Me.txtRef = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.txtName = New Care.Controls.CareTextBox(Me.components)
        Me.cbxStatus = New Care.Controls.CareComboBox(Me.components)
        Me.lblFamily = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl5 = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel16 = New Care.Controls.CareLabel(Me.components)
        Me.txtShortlisted = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel15 = New Care.Controls.CareLabel(Me.components)
        Me.txtTotalApplicants = New Care.Controls.CareTextBox(Me.components)
        Me.GroupControl4 = New Care.Controls.CareFrame(Me.components)
        Me.cgAds = New Care.Controls.CareGridWithButtons()
        Me.GroupControl3 = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel10 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel12 = New Care.Controls.CareLabel(Me.components)
        Me.txtContHours = New Care.Controls.CareTextBox(Me.components)
        Me.txtContSalary = New Care.Controls.CareTextBox(Me.components)
        Me.GroupControl1 = New Care.Controls.CareFrame(Me.components)
        Me.lblStarting = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.cdtDateStart = New Care.Controls.CareDateTime(Me.components)
        Me.lblInterviews = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.cdtDateInt = New Care.Controls.CareDateTime(Me.components)
        Me.lblClosing = New Care.Controls.CareLabel(Me.components)
        Me.lblOpening = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.cdtDateFrom = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel13 = New Care.Controls.CareLabel(Me.components)
        Me.cdtDateTo = New Care.Controls.CareDateTime(Me.components)
        Me.GroupBox1 = New Care.Controls.CareFrame(Me.components)
        Me.txtJobTitle = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.cbxLineManager = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.cbxDepartment = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.cbxSite = New Care.Controls.CareComboBox(Me.components)
        Me.tabApplicants = New DevExpress.XtraTab.XtraTabPage()
        Me.cgApps = New Care.Controls.CareGridWithButtons()
        Me.GroupControl2 = New Care.Controls.CareFrame(Me.components)
        Me.lblStageFilter = New Care.Controls.CareLabel(Me.components)
        Me.cbxStage = New Care.Controls.CareComboBox(Me.components)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.ctMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ctMain.SuspendLayout()
        Me.tabVacancy.SuspendLayout()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl6.SuspendLayout()
        CType(Me.txtRef.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.txtShortlisted.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalApplicants.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.txtContHours.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtContSalary.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.cdtDateStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDateStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDateInt.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDateInt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDateFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDateFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDateTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDateTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtJobTitle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxLineManager.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxDepartment.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabApplicants.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.cbxStage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(737, 3)
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(646, 3)
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(0, 541)
        Me.Panel1.Size = New System.Drawing.Size(834, 36)
        Me.Panel1.TabIndex = 1
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'ctMain
        '
        Me.ctMain.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.ctMain.Location = New System.Drawing.Point(10, 52)
        Me.ctMain.Name = "ctMain"
        Me.ctMain.SelectedTabPage = Me.tabVacancy
        Me.ctMain.Size = New System.Drawing.Size(816, 489)
        Me.ctMain.TabIndex = 0
        Me.ctMain.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabVacancy, Me.tabApplicants})
        '
        'tabVacancy
        '
        Me.tabVacancy.Controls.Add(Me.GroupControl6)
        Me.tabVacancy.Controls.Add(Me.GroupControl5)
        Me.tabVacancy.Controls.Add(Me.GroupControl4)
        Me.tabVacancy.Controls.Add(Me.GroupControl3)
        Me.tabVacancy.Controls.Add(Me.GroupControl1)
        Me.tabVacancy.Controls.Add(Me.GroupBox1)
        Me.tabVacancy.Name = "tabVacancy"
        Me.tabVacancy.Size = New System.Drawing.Size(810, 461)
        Me.tabVacancy.Text = "Vacancy Details"
        '
        'GroupControl6
        '
        Me.GroupControl6.Controls.Add(Me.CareLabel18)
        Me.GroupControl6.Controls.Add(Me.txtRef)
        Me.GroupControl6.Controls.Add(Me.CareLabel7)
        Me.GroupControl6.Controls.Add(Me.txtName)
        Me.GroupControl6.Controls.Add(Me.cbxStatus)
        Me.GroupControl6.Controls.Add(Me.lblFamily)
        Me.GroupControl6.Location = New System.Drawing.Point(6, 6)
        Me.GroupControl6.Name = "GroupControl6"
        Me.GroupControl6.Size = New System.Drawing.Size(795, 58)
        Me.GroupControl6.TabIndex = 0
        Me.GroupControl6.Text = "Vacancy Details"
        '
        'CareLabel18
        '
        Me.CareLabel18.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel18.Location = New System.Drawing.Point(238, 31)
        Me.CareLabel18.Name = "CareLabel18"
        Me.CareLabel18.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel18.TabIndex = 2
        Me.CareLabel18.Text = "Name"
        Me.CareLabel18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRef
        '
        Me.txtRef.CharacterCasing = CharacterCasing.Upper
        Me.txtRef.EnterMoveNextControl = True
        Me.txtRef.Location = New System.Drawing.Point(110, 28)
        Me.txtRef.MaxLength = 10
        Me.txtRef.Name = "txtRef"
        Me.txtRef.NumericAllowNegatives = False
        Me.txtRef.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRef.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRef.Properties.AccessibleDescription = ""
        Me.txtRef.Properties.AccessibleName = "Vacancy Reference"
        Me.txtRef.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRef.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRef.Properties.Appearance.Options.UseFont = True
        Me.txtRef.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRef.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRef.Properties.CharacterCasing = CharacterCasing.Upper
        Me.txtRef.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRef.Properties.MaxLength = 10
        Me.txtRef.Size = New System.Drawing.Size(110, 22)
        Me.txtRef.TabIndex = 1
        Me.txtRef.Tag = "AEM"
        Me.txtRef.TextAlign = HorizontalAlignment.Left
        Me.txtRef.ToolTipText = ""
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(655, 31)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel7.TabIndex = 4
        Me.CareLabel7.Text = "Status"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.CharacterCasing = CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(276, 28)
        Me.txtName.MaxLength = 100
        Me.txtName.Name = "txtName"
        Me.txtName.NumericAllowNegatives = False
        Me.txtName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AccessibleName = "Vacancy Name"
        Me.txtName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtName.Properties.MaxLength = 100
        Me.txtName.Size = New System.Drawing.Size(360, 22)
        Me.txtName.TabIndex = 3
        Me.txtName.Tag = "AEM"
        Me.txtName.TextAlign = HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'cbxStatus
        '
        Me.cbxStatus.AllowBlank = False
        Me.cbxStatus.DataSource = Nothing
        Me.cbxStatus.DisplayMember = Nothing
        Me.cbxStatus.EnterMoveNextControl = True
        Me.cbxStatus.Location = New System.Drawing.Point(693, 28)
        Me.cbxStatus.Name = "cbxStatus"
        Me.cbxStatus.Properties.AccessibleName = "Status"
        Me.cbxStatus.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxStatus.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxStatus.Properties.Appearance.Options.UseFont = True
        Me.cbxStatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxStatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxStatus.SelectedValue = Nothing
        Me.cbxStatus.Size = New System.Drawing.Size(94, 22)
        Me.cbxStatus.TabIndex = 5
        Me.cbxStatus.Tag = "AEM"
        Me.cbxStatus.ValueMember = Nothing
        '
        'lblFamily
        '
        Me.lblFamily.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblFamily.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblFamily.Location = New System.Drawing.Point(10, 31)
        Me.lblFamily.Name = "lblFamily"
        Me.lblFamily.Size = New System.Drawing.Size(52, 15)
        Me.lblFamily.TabIndex = 0
        Me.lblFamily.Text = "Reference"
        Me.lblFamily.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl5
        '
        Me.GroupControl5.Controls.Add(Me.CareLabel16)
        Me.GroupControl5.Controls.Add(Me.txtShortlisted)
        Me.GroupControl5.Controls.Add(Me.CareLabel15)
        Me.GroupControl5.Controls.Add(Me.txtTotalApplicants)
        Me.GroupControl5.Location = New System.Drawing.Point(388, 70)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(413, 58)
        Me.GroupControl5.TabIndex = 4
        Me.GroupControl5.Text = "Statistics"
        Me.GroupControl5.Visible = False
        '
        'CareLabel16
        '
        Me.CareLabel16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel16.Location = New System.Drawing.Point(249, 31)
        Me.CareLabel16.Name = "CareLabel16"
        Me.CareLabel16.Size = New System.Drawing.Size(56, 15)
        Me.CareLabel16.TabIndex = 2
        Me.CareLabel16.Text = "Shortlisted"
        Me.CareLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtShortlisted
        '
        Me.txtShortlisted.CharacterCasing = CharacterCasing.Normal
        Me.txtShortlisted.EnterMoveNextControl = True
        Me.txtShortlisted.Location = New System.Drawing.Point(311, 28)
        Me.txtShortlisted.MaxLength = 30
        Me.txtShortlisted.Name = "txtShortlisted"
        Me.txtShortlisted.NumericAllowNegatives = False
        Me.txtShortlisted.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtShortlisted.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtShortlisted.Properties.AccessibleDescription = ""
        Me.txtShortlisted.Properties.AccessibleName = "Forename"
        Me.txtShortlisted.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtShortlisted.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtShortlisted.Properties.Appearance.Options.UseFont = True
        Me.txtShortlisted.Properties.Appearance.Options.UseTextOptions = True
        Me.txtShortlisted.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtShortlisted.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtShortlisted.Properties.MaxLength = 30
        Me.txtShortlisted.Size = New System.Drawing.Size(94, 22)
        Me.txtShortlisted.TabIndex = 3
        Me.txtShortlisted.Tag = "R"
        Me.txtShortlisted.TextAlign = HorizontalAlignment.Left
        Me.txtShortlisted.ToolTipText = ""
        '
        'CareLabel15
        '
        Me.CareLabel15.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel15.Location = New System.Drawing.Point(10, 31)
        Me.CareLabel15.Name = "CareLabel15"
        Me.CareLabel15.Size = New System.Drawing.Size(86, 15)
        Me.CareLabel15.TabIndex = 0
        Me.CareLabel15.Text = "Total Applicants"
        Me.CareLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTotalApplicants
        '
        Me.txtTotalApplicants.CharacterCasing = CharacterCasing.Normal
        Me.txtTotalApplicants.EnterMoveNextControl = True
        Me.txtTotalApplicants.Location = New System.Drawing.Point(110, 28)
        Me.txtTotalApplicants.MaxLength = 30
        Me.txtTotalApplicants.Name = "txtTotalApplicants"
        Me.txtTotalApplicants.NumericAllowNegatives = False
        Me.txtTotalApplicants.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtTotalApplicants.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTotalApplicants.Properties.AccessibleDescription = ""
        Me.txtTotalApplicants.Properties.AccessibleName = "Forename"
        Me.txtTotalApplicants.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtTotalApplicants.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtTotalApplicants.Properties.Appearance.Options.UseFont = True
        Me.txtTotalApplicants.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTotalApplicants.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtTotalApplicants.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTotalApplicants.Properties.MaxLength = 30
        Me.txtTotalApplicants.Size = New System.Drawing.Size(85, 22)
        Me.txtTotalApplicants.TabIndex = 1
        Me.txtTotalApplicants.Tag = "R"
        Me.txtTotalApplicants.TextAlign = HorizontalAlignment.Left
        Me.txtTotalApplicants.ToolTipText = ""
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.cgAds)
        Me.GroupControl4.Location = New System.Drawing.Point(388, 134)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(413, 320)
        Me.GroupControl4.TabIndex = 5
        Me.GroupControl4.Text = "Adverts"
        Me.GroupControl4.Visible = False
        '
        'cgAds
        '
        Me.cgAds.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgAds.ButtonsEnabled = False
        Me.cgAds.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgAds.HideFirstColumn = False
        Me.cgAds.Location = New System.Drawing.Point(8, 26)
        Me.cgAds.Name = "cgAds"
        Me.cgAds.PreviewColumn = ""
        Me.cgAds.Size = New System.Drawing.Size(397, 287)
        Me.cgAds.TabIndex = 0
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.CareLabel4)
        Me.GroupControl3.Controls.Add(Me.CareLabel10)
        Me.GroupControl3.Controls.Add(Me.CareLabel12)
        Me.GroupControl3.Controls.Add(Me.txtContHours)
        Me.GroupControl3.Controls.Add(Me.txtContSalary)
        Me.GroupControl3.Location = New System.Drawing.Point(6, 367)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(376, 87)
        Me.GroupControl3.TabIndex = 3
        Me.GroupControl3.Text = "Salary Details"
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(201, 59)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(46, 15)
        Me.CareLabel4.TabIndex = 4
        Me.CareLabel4.Text = "(Annual)"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel10
        '
        Me.CareLabel10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel10.Location = New System.Drawing.Point(10, 59)
        Me.CareLabel10.Name = "CareLabel10"
        Me.CareLabel10.Size = New System.Drawing.Size(93, 15)
        Me.CareLabel10.TabIndex = 2
        Me.CareLabel10.Text = "Contracted Salary"
        Me.CareLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel12
        '
        Me.CareLabel12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel12.Location = New System.Drawing.Point(10, 31)
        Me.CareLabel12.Name = "CareLabel12"
        Me.CareLabel12.Size = New System.Drawing.Size(94, 15)
        Me.CareLabel12.TabIndex = 0
        Me.CareLabel12.Text = "Contracted Hours"
        Me.CareLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtContHours
        '
        Me.txtContHours.CharacterCasing = CharacterCasing.Normal
        Me.txtContHours.EnterMoveNextControl = True
        Me.txtContHours.Location = New System.Drawing.Point(110, 28)
        Me.txtContHours.MaxLength = 8
        Me.txtContHours.Name = "txtContHours"
        Me.txtContHours.NumericAllowNegatives = False
        Me.txtContHours.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtContHours.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtContHours.Properties.AccessibleDescription = ""
        Me.txtContHours.Properties.AccessibleName = "Forename"
        Me.txtContHours.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtContHours.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtContHours.Properties.Appearance.Options.UseFont = True
        Me.txtContHours.Properties.Appearance.Options.UseTextOptions = True
        Me.txtContHours.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtContHours.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtContHours.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtContHours.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtContHours.Properties.MaxLength = 8
        Me.txtContHours.Size = New System.Drawing.Size(85, 22)
        Me.txtContHours.TabIndex = 1
        Me.txtContHours.Tag = "AEM"
        Me.txtContHours.TextAlign = HorizontalAlignment.Left
        Me.txtContHours.ToolTipText = ""
        '
        'txtContSalary
        '
        Me.txtContSalary.CharacterCasing = CharacterCasing.Normal
        Me.txtContSalary.EnterMoveNextControl = True
        Me.txtContSalary.Location = New System.Drawing.Point(110, 56)
        Me.txtContSalary.MaxLength = 8
        Me.txtContSalary.Name = "txtContSalary"
        Me.txtContSalary.NumericAllowNegatives = False
        Me.txtContSalary.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtContSalary.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtContSalary.Properties.AccessibleName = "Family"
        Me.txtContSalary.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtContSalary.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtContSalary.Properties.Appearance.Options.UseFont = True
        Me.txtContSalary.Properties.Appearance.Options.UseTextOptions = True
        Me.txtContSalary.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtContSalary.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtContSalary.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtContSalary.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtContSalary.Properties.MaxLength = 8
        Me.txtContSalary.Size = New System.Drawing.Size(85, 22)
        Me.txtContSalary.TabIndex = 3
        Me.txtContSalary.Tag = "AE"
        Me.txtContSalary.TextAlign = HorizontalAlignment.Left
        Me.txtContSalary.ToolTipText = ""
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.lblStarting)
        Me.GroupControl1.Controls.Add(Me.CareLabel14)
        Me.GroupControl1.Controls.Add(Me.cdtDateStart)
        Me.GroupControl1.Controls.Add(Me.lblInterviews)
        Me.GroupControl1.Controls.Add(Me.CareLabel5)
        Me.GroupControl1.Controls.Add(Me.cdtDateInt)
        Me.GroupControl1.Controls.Add(Me.lblClosing)
        Me.GroupControl1.Controls.Add(Me.lblOpening)
        Me.GroupControl1.Controls.Add(Me.CareLabel11)
        Me.GroupControl1.Controls.Add(Me.cdtDateFrom)
        Me.GroupControl1.Controls.Add(Me.CareLabel13)
        Me.GroupControl1.Controls.Add(Me.cdtDateTo)
        Me.GroupControl1.Location = New System.Drawing.Point(6, 219)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(376, 142)
        Me.GroupControl1.TabIndex = 2
        Me.GroupControl1.Text = "Dates"
        '
        'lblStarting
        '
        Me.lblStarting.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStarting.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblStarting.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblStarting.Location = New System.Drawing.Point(201, 115)
        Me.lblStarting.Name = "lblStarting"
        Me.lblStarting.Size = New System.Drawing.Size(147, 15)
        Me.lblStarting.TabIndex = 11
        Me.lblStarting.Text = "1 year, 11 months, 21 days"
        Me.lblStarting.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(11, 115)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(51, 15)
        Me.CareLabel14.TabIndex = 9
        Me.CareLabel14.Text = "Start Date"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel14.ToolTip = "The last date the child will attend the Nursery. This date is used for invoicing " &
    "purposes."
        Me.CareLabel14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'cdtDateStart
        '
        Me.cdtDateStart.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtDateStart.EnterMoveNextControl = True
        Me.cdtDateStart.Location = New System.Drawing.Point(110, 112)
        Me.cdtDateStart.Name = "cdtDateStart"
        Me.cdtDateStart.Properties.AccessibleName = "Start Date"
        Me.cdtDateStart.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtDateStart.Properties.Appearance.Options.UseFont = True
        Me.cdtDateStart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDateStart.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtDateStart.Size = New System.Drawing.Size(85, 22)
        Me.cdtDateStart.TabIndex = 10
        Me.cdtDateStart.Tag = "AEM"
        Me.cdtDateStart.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'lblInterviews
        '
        Me.lblInterviews.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterviews.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblInterviews.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblInterviews.Location = New System.Drawing.Point(201, 87)
        Me.lblInterviews.Name = "lblInterviews"
        Me.lblInterviews.Size = New System.Drawing.Size(147, 15)
        Me.lblInterviews.TabIndex = 8
        Me.lblInterviews.Text = "1 year, 11 months, 21 days"
        Me.lblInterviews.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(10, 87)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(84, 15)
        Me.CareLabel5.TabIndex = 6
        Me.CareLabel5.Text = "Interviews From"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel5.ToolTip = "The last date the child will attend the Nursery. This date is used for invoicing " &
    "purposes."
        Me.CareLabel5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'cdtDateInt
        '
        Me.cdtDateInt.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtDateInt.EnterMoveNextControl = True
        Me.cdtDateInt.Location = New System.Drawing.Point(110, 84)
        Me.cdtDateInt.Name = "cdtDateInt"
        Me.cdtDateInt.Properties.AccessibleName = "Interviews From"
        Me.cdtDateInt.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtDateInt.Properties.Appearance.Options.UseFont = True
        Me.cdtDateInt.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDateInt.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtDateInt.Size = New System.Drawing.Size(85, 22)
        Me.cdtDateInt.TabIndex = 7
        Me.cdtDateInt.Tag = "AEM"
        Me.cdtDateInt.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'lblClosing
        '
        Me.lblClosing.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClosing.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblClosing.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblClosing.Location = New System.Drawing.Point(201, 59)
        Me.lblClosing.Name = "lblClosing"
        Me.lblClosing.Size = New System.Drawing.Size(147, 15)
        Me.lblClosing.TabIndex = 5
        Me.lblClosing.Text = "1 year, 11 months, 21 days"
        Me.lblClosing.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOpening
        '
        Me.lblOpening.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOpening.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblOpening.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblOpening.Location = New System.Drawing.Point(201, 31)
        Me.lblOpening.Name = "lblOpening"
        Me.lblOpening.Size = New System.Drawing.Size(147, 15)
        Me.lblOpening.TabIndex = 2
        Me.lblOpening.Text = "1 year, 11 months, 21 days"
        Me.lblOpening.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(11, 31)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(73, 15)
        Me.CareLabel11.TabIndex = 0
        Me.CareLabel11.Text = "Opening Date"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtDateFrom
        '
        Me.cdtDateFrom.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtDateFrom.EnterMoveNextControl = True
        Me.cdtDateFrom.Location = New System.Drawing.Point(110, 28)
        Me.cdtDateFrom.Name = "cdtDateFrom"
        Me.cdtDateFrom.Properties.AccessibleName = "Opening Date"
        Me.cdtDateFrom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtDateFrom.Properties.Appearance.Options.UseFont = True
        Me.cdtDateFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDateFrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtDateFrom.Size = New System.Drawing.Size(85, 22)
        Me.cdtDateFrom.TabIndex = 1
        Me.cdtDateFrom.Tag = "AEM"
        Me.cdtDateFrom.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'CareLabel13
        '
        Me.CareLabel13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel13.Location = New System.Drawing.Point(11, 59)
        Me.CareLabel13.Name = "CareLabel13"
        Me.CareLabel13.Size = New System.Drawing.Size(67, 15)
        Me.CareLabel13.TabIndex = 3
        Me.CareLabel13.Text = "Closing Date"
        Me.CareLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel13.ToolTip = "The date the child started in the Nursery. This date is used for invoicing purpos" &
    "es."
        Me.CareLabel13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'cdtDateTo
        '
        Me.cdtDateTo.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtDateTo.EnterMoveNextControl = True
        Me.cdtDateTo.Location = New System.Drawing.Point(110, 56)
        Me.cdtDateTo.Name = "cdtDateTo"
        Me.cdtDateTo.Properties.AccessibleName = "Closing Date"
        Me.cdtDateTo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtDateTo.Properties.Appearance.Options.UseFont = True
        Me.cdtDateTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDateTo.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtDateTo.Size = New System.Drawing.Size(85, 22)
        Me.cdtDateTo.TabIndex = 4
        Me.cdtDateTo.Tag = "AEM"
        Me.cdtDateTo.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtJobTitle)
        Me.GroupBox1.Controls.Add(Me.CareLabel8)
        Me.GroupBox1.Controls.Add(Me.cbxLineManager)
        Me.GroupBox1.Controls.Add(Me.CareLabel3)
        Me.GroupBox1.Controls.Add(Me.cbxDepartment)
        Me.GroupBox1.Controls.Add(Me.CareLabel2)
        Me.GroupBox1.Controls.Add(Me.CareLabel1)
        Me.GroupBox1.Controls.Add(Me.cbxSite)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 70)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(376, 143)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.Text = "Job Details"
        '
        'txtJobTitle
        '
        Me.txtJobTitle.CharacterCasing = CharacterCasing.Normal
        Me.txtJobTitle.EnterMoveNextControl = True
        Me.txtJobTitle.Location = New System.Drawing.Point(109, 28)
        Me.txtJobTitle.MaxLength = 100
        Me.txtJobTitle.Name = "txtJobTitle"
        Me.txtJobTitle.NumericAllowNegatives = False
        Me.txtJobTitle.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtJobTitle.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtJobTitle.Properties.AccessibleName = "Job Title"
        Me.txtJobTitle.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtJobTitle.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtJobTitle.Properties.Appearance.Options.UseFont = True
        Me.txtJobTitle.Properties.Appearance.Options.UseTextOptions = True
        Me.txtJobTitle.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtJobTitle.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtJobTitle.Properties.MaxLength = 100
        Me.txtJobTitle.Size = New System.Drawing.Size(259, 22)
        Me.txtJobTitle.TabIndex = 1
        Me.txtJobTitle.Tag = "AEM"
        Me.txtJobTitle.TextAlign = HorizontalAlignment.Left
        Me.txtJobTitle.ToolTipText = ""
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(10, 115)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(72, 15)
        Me.CareLabel8.TabIndex = 6
        Me.CareLabel8.Text = "Line Manager"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxLineManager
        '
        Me.cbxLineManager.AllowBlank = False
        Me.cbxLineManager.DataSource = Nothing
        Me.cbxLineManager.DisplayMember = Nothing
        Me.cbxLineManager.EnterMoveNextControl = True
        Me.cbxLineManager.Location = New System.Drawing.Point(110, 112)
        Me.cbxLineManager.Name = "cbxLineManager"
        Me.cbxLineManager.Properties.AccessibleName = "Status"
        Me.cbxLineManager.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxLineManager.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxLineManager.Properties.Appearance.Options.UseFont = True
        Me.cbxLineManager.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxLineManager.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxLineManager.SelectedValue = Nothing
        Me.cbxLineManager.Size = New System.Drawing.Size(258, 22)
        Me.cbxLineManager.TabIndex = 7
        Me.cbxLineManager.Tag = "AE"
        Me.cbxLineManager.ValueMember = Nothing
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(10, 87)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(63, 15)
        Me.CareLabel3.TabIndex = 4
        Me.CareLabel3.Text = "Department"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxDepartment
        '
        Me.cbxDepartment.AllowBlank = False
        Me.cbxDepartment.DataSource = Nothing
        Me.cbxDepartment.DisplayMember = Nothing
        Me.cbxDepartment.EnterMoveNextControl = True
        Me.cbxDepartment.Location = New System.Drawing.Point(110, 84)
        Me.cbxDepartment.Name = "cbxDepartment"
        Me.cbxDepartment.Properties.AccessibleName = "Status"
        Me.cbxDepartment.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxDepartment.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxDepartment.Properties.Appearance.Options.UseFont = True
        Me.cbxDepartment.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxDepartment.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxDepartment.SelectedValue = Nothing
        Me.cbxDepartment.Size = New System.Drawing.Size(258, 22)
        Me.cbxDepartment.TabIndex = 5
        Me.cbxDepartment.Tag = "AE"
        Me.cbxDepartment.ValueMember = Nothing
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(10, 59)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(19, 15)
        Me.CareLabel2.TabIndex = 2
        Me.CareLabel2.Text = "Site"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(10, 31)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(44, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Job Title"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(110, 56)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AccessibleName = "Site"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(258, 22)
        Me.cbxSite.TabIndex = 3
        Me.cbxSite.Tag = "AEM"
        Me.cbxSite.ValueMember = Nothing
        '
        'tabApplicants
        '
        Me.tabApplicants.Controls.Add(Me.cgApps)
        Me.tabApplicants.Controls.Add(Me.GroupControl2)
        Me.tabApplicants.Name = "tabApplicants"
        Me.tabApplicants.Size = New System.Drawing.Size(810, 461)
        Me.tabApplicants.Text = "Applicants"
        '
        'cgApps
        '
        Me.cgApps.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgApps.ButtonsEnabled = False
        Me.cgApps.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgApps.HideFirstColumn = False
        Me.cgApps.Location = New System.Drawing.Point(6, 51)
        Me.cgApps.Name = "cgApps"
        Me.cgApps.PreviewColumn = ""
        Me.cgApps.Size = New System.Drawing.Size(797, 404)
        Me.cgApps.TabIndex = 1
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl2.Controls.Add(Me.lblStageFilter)
        Me.GroupControl2.Controls.Add(Me.cbxStage)
        Me.GroupControl2.Location = New System.Drawing.Point(6, 6)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.ShowCaption = False
        Me.GroupControl2.Size = New System.Drawing.Size(797, 39)
        Me.GroupControl2.TabIndex = 0
        '
        'lblStageFilter
        '
        Me.lblStageFilter.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStageFilter.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblStageFilter.Location = New System.Drawing.Point(9, 12)
        Me.lblStageFilter.Name = "lblStageFilter"
        Me.lblStageFilter.Size = New System.Drawing.Size(58, 15)
        Me.lblStageFilter.TabIndex = 4
        Me.lblStageFilter.Text = "Stage Filter"
        Me.lblStageFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxStage
        '
        Me.cbxStage.AllowBlank = True
        Me.cbxStage.DataSource = Nothing
        Me.cbxStage.DisplayMember = Nothing
        Me.cbxStage.EnterMoveNextControl = True
        Me.cbxStage.Location = New System.Drawing.Point(78, 9)
        Me.cbxStage.Name = "cbxStage"
        Me.cbxStage.Properties.AccessibleName = ""
        Me.cbxStage.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxStage.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxStage.Properties.Appearance.Options.UseFont = True
        Me.cbxStage.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxStage.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxStage.SelectedValue = Nothing
        Me.cbxStage.Size = New System.Drawing.Size(220, 22)
        Me.cbxStage.TabIndex = 5
        Me.cbxStage.Tag = ""
        Me.cbxStage.ValueMember = Nothing
        '
        'frmRecruitment
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(834, 577)
        Me.Controls.Add(Me.ctMain)
        Me.Name = "frmRecruitment"
        Me.Text = "frmRecruitment"
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.ctMain, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.ctMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ctMain.ResumeLayout(False)
        Me.ctMain.PerformLayout()
        Me.tabVacancy.ResumeLayout(False)
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl6.ResumeLayout(False)
        Me.GroupControl6.PerformLayout()
        CType(Me.txtRef.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.txtShortlisted.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalApplicants.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.txtContHours.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtContSalary.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.cdtDateStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDateStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDateInt.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDateInt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDateFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDateFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDateTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDateTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.txtJobTitle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxLineManager.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxDepartment.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabApplicants.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.cbxStage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ctMain As Care.Controls.CareTab
    Friend WithEvents tabVacancy As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabApplicants As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
    Friend WithEvents lblFamily As Care.Controls.CareLabel
    Friend WithEvents txtRef As Care.Controls.CareTextBox
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents CareLabel15 As Care.Controls.CareLabel
    Friend WithEvents txtTotalApplicants As Care.Controls.CareTextBox
    Friend WithEvents cgAds As Care.Controls.CareGridWithButtons
    Friend WithEvents CareLabel10 As Care.Controls.CareLabel
    Friend WithEvents CareLabel12 As Care.Controls.CareLabel
    Friend WithEvents txtContHours As Care.Controls.CareTextBox
    Friend WithEvents txtContSalary As Care.Controls.CareTextBox
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Private WithEvents cdtDateStart As Care.Controls.CareDateTime
    Friend WithEvents lblInterviews As Care.Controls.CareLabel
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Private WithEvents cdtDateInt As Care.Controls.CareDateTime
    Friend WithEvents lblClosing As Care.Controls.CareLabel
    Friend WithEvents lblOpening As Care.Controls.CareLabel
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Private WithEvents cdtDateFrom As Care.Controls.CareDateTime
    Friend WithEvents CareLabel13 As Care.Controls.CareLabel
    Private WithEvents cdtDateTo As Care.Controls.CareDateTime
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents cbxDepartment As Care.Controls.CareComboBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents cgApps As Care.Controls.CareGridWithButtons
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents cbxStatus As Care.Controls.CareComboBox
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents cbxLineManager As Care.Controls.CareComboBox
    Friend WithEvents CareLabel18 As Care.Controls.CareLabel
    Friend WithEvents txtJobTitle As Care.Controls.CareTextBox
    Friend WithEvents CareLabel16 As Care.Controls.CareLabel
    Friend WithEvents txtShortlisted As Care.Controls.CareTextBox
    Friend WithEvents lblStarting As Care.Controls.CareLabel
    Friend WithEvents lblStageFilter As Care.Controls.CareLabel
    Friend WithEvents cbxStage As Care.Controls.CareComboBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents GroupBox1 As Care.Controls.CareFrame
    Friend WithEvents GroupControl5 As Care.Controls.CareFrame
    Friend WithEvents GroupControl4 As Care.Controls.CareFrame
    Friend WithEvents GroupControl3 As Care.Controls.CareFrame
    Friend WithEvents GroupControl1 As Care.Controls.CareFrame
    Friend WithEvents GroupControl2 As Care.Controls.CareFrame
    Friend WithEvents GroupControl6 As Care.Controls.CareFrame
End Class
