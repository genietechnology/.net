﻿

Imports Care.Global
Imports Care.Data
Imports Care.Shared

Public Class frmLeadChangeStage

    Private m_LeadID As Guid? = Nothing
    Private m_Lead As Business.Lead = Nothing
    Private m_Stage As Business.LeadStage = Nothing

    Public Sub New(ByVal LeadID As Guid)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_LeadID = LeadID
        m_Lead = Business.Lead.RetreiveByID(LeadID)

    End Sub

    Private Sub frmLeadChangeStage_Load(sender As Object, e As EventArgs) Handles Me.Load

        Me.Text = "Change Stage"

        txtContact.Text = m_Lead._ContactFullname
        txtChild.Text = m_Lead._ChildFullname
        txtStage.Text = m_Lead._Stage

        Dim _SQL As String = "select id, name from LeadStages where pcnt_complete > " + m_Lead._StagePcnt.ToString + " order by pcnt_complete, name"
        cbxStage.PopulateWithSQL(Session.ConnectionString, _SQL)

        cdtActionDate.Value = Today
        txtRecipient.Text = m_Lead._ContactEmail

    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click

        If cdtActionDate.Text = "" Then
            CareMessage("Please enter the Action Date.", MessageBoxIcon.Exclamation, "Date Mandatory")
            Exit Sub
        End If

        If cbxStage.SelectedIndex < 0 Then
            CareMessage("Please select a new Stage.", MessageBoxIcon.Exclamation, "Stage Mandatory")
            Exit Sub
        End If

        btnOK.Enabled = False
        btnCancel.Enabled = False
        Session.CursorWaiting()

        If m_Stage IsNot Nothing Then

            With m_Lead

                Dim _OldStage As String = ._Stage

                ._StageId = m_Stage._ID
                ._Stage = m_Stage._Name
                ._StagePcnt = m_Stage._PcntComplete

                Dim _ActionDate As Date? = Nothing
                If txtActionTime.Text <> "" Then
                    Dim _Time As TimeSpan? = ValueHandler.ConvertTimeSpan(txtActionTime.Text)
                    If _Time.HasValue Then
                        _ActionDate = ValueHandler.BuildDateTime(cdtActionDate.Value.Value, _Time.Value)
                    End If
                Else
                    _ActionDate = cdtActionDate.Value
                End If

                If Not ._DateFirstContacted.HasValue Then ._DateFirstContacted = _ActionDate

                If m_Stage._ActionStart Then
                    If Not ._StageStart.HasValue Then ._StageStart = _ActionDate
                End If

                If m_Stage._ActionContact Then ._DateLastContacted = _ActionDate
                If m_Stage._ActionPack Then ._DatePackSent = _ActionDate
                If m_Stage._ActionViewing Then ._DateViewing = _ActionDate
                If m_Stage._ActionQuote Then ._DateQuoted = _ActionDate
                If m_Stage._ActionList Then ._DateListed = _ActionDate

                If m_Stage._ActionClose Then
                    ._Closed = True
                    ._ClosedDate = _ActionDate
                End If

                ._DateLastContacted = Now
                .Store()

                Dim _Subject As String = _OldStage + " > " + m_Stage._Name
                CRM.CreateActivityRecord(m_Lead._ID.Value, CRM.EnumLinkType.Lead, CRM.EnumActivityType.Note, Nothing, ._ContactFullname, _Subject, txtEmailBody.Text)

            End With

            If chkEmail.Checked Then
                If txtRecipient.Text <> "" AndAlso txtEmailSubject.Text <> "" AndAlso txtEmailBody.Text <> "" Then
                    EmailHandler.SendEmail(Business.Site.ReturnEmailAddress(m_Lead._SiteId.Value, Business.Site.EnumEmailAddressType.General), txtRecipient.Text, txtEmailSubject.Text, txtEmailBody.Text)
                End If
            End If

            Session.CursorDefault()

            Me.DialogResult = DialogResult.OK
            Me.Close()

        End If

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub cbxStage_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxStage.SelectedIndexChanged

        m_Stage = Business.LeadStage.RetreiveByID(New Guid(cbxStage.SelectedValue.ToString))
        If m_Stage IsNot Nothing Then

            chkEmail.Checked = m_Stage._Email

            If m_Stage._Email Then
                txtEmailSubject.Text = MergeFields.MergeData(m_Stage._EmailSubject, m_Lead)
                txtEmailBody.Text = MergeFields.MergeData(m_Stage._EmailBody, m_Lead)
            Else
                txtEmailSubject.Text = ""
                txtEmailBody.Text = ""
            End If

        End If

    End Sub

End Class
