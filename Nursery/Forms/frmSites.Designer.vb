﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSites
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.tabMain = New Care.Controls.CareTab(Me.components)
        Me.tabSite = New DevExpress.XtraTab.XtraTabPage()
        Me.CareFrame1 = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel20 = New Care.Controls.CareLabel(Me.components)
        Me.txtFinSageVer = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel17 = New Care.Controls.CareLabel(Me.components)
        Me.chkFinOverride = New Care.Controls.CareCheckBox(Me.components)
        Me.cbxFinSystem = New Care.Controls.CareComboBox(Me.components)
        Me.txtFinSagePwd = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel21 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel18 = New Care.Controls.CareLabel(Me.components)
        Me.txtFinSagePath = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel19 = New Care.Controls.CareLabel(Me.components)
        Me.txtFinPfxPath = New Care.Controls.CareTextBox(Me.components)
        Me.txtFinSageUsername = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel16 = New Care.Controls.CareLabel(Me.components)
        Me.txtFinPfxPassword = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel15 = New Care.Controls.CareLabel(Me.components)
        Me.txtFinApiKey = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel13 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.txtFinApiSecret = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel12 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl6 = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.txtFinanceEmail = New Care.[Shared].CareEmailAddress()
        Me.txtFinanceSender = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel10 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl5 = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.txtReportEmail = New Care.[Shared].CareEmailAddress()
        Me.txtReportSender = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl4 = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.txtGenEmail = New Care.[Shared].CareEmailAddress()
        Me.txtGenSender = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl1 = New Care.Controls.CareFrame(Me.components)
        Me.txtAccNext = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.txtAccPrefix = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.cbxNLTrack = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel22 = New Care.Controls.CareLabel(Me.components)
        Me.cbxNLCode = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox1 = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.txtTel = New Care.[Shared].CareTelephoneNumber()
        Me.txtAddress = New Care.Address.CareAddress()
        Me.Label2 = New Care.Controls.CareLabel(Me.components)
        Me.txtName = New Care.Controls.CareTextBox(Me.components)
        Me.Label1 = New Care.Controls.CareLabel(Me.components)
        Me.tabRooms = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl3 = New Care.Controls.CareFrame(Me.components)
        Me.cgRatios = New Care.Controls.CareGridWithButtons()
        Me.GroupControl2 = New Care.Controls.CareFrame(Me.components)
        Me.cgRooms = New Care.Controls.CareGridWithButtons()
        Me.tabUsers = New DevExpress.XtraTab.XtraTabPage()
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.cgUsers = New Care.Controls.CareGridWithButtons()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.tabMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabMain.SuspendLayout()
        Me.tabSite.SuspendLayout()
        CType(Me.CareFrame1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CareFrame1.SuspendLayout()
        CType(Me.txtFinSageVer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkFinOverride.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxFinSystem.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFinSagePwd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFinSagePath.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFinPfxPath.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFinSageUsername.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFinPfxPassword.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFinApiKey.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFinApiSecret.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl6.SuspendLayout()
        CType(Me.txtFinanceSender.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.txtReportSender.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.txtGenSender.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtAccNext.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAccPrefix.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxNLTrack.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxNLCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabRooms.SuspendLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        Me.tabUsers.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(651, 3)
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(560, 3)
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(0, 555)
        Me.Panel1.Size = New System.Drawing.Size(748, 36)
        Me.Panel1.TabIndex = 1
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'tabMain
        '
        Me.tabMain.Location = New System.Drawing.Point(12, 56)
        Me.tabMain.Name = "tabMain"
        Me.tabMain.SelectedTabPage = Me.tabSite
        Me.tabMain.Size = New System.Drawing.Size(724, 496)
        Me.tabMain.TabIndex = 0
        Me.tabMain.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabSite, Me.tabRooms, Me.tabUsers})
        '
        'tabSite
        '
        Me.tabSite.Controls.Add(Me.CareFrame1)
        Me.tabSite.Controls.Add(Me.GroupControl6)
        Me.tabSite.Controls.Add(Me.GroupControl5)
        Me.tabSite.Controls.Add(Me.GroupControl4)
        Me.tabSite.Controls.Add(Me.GroupControl1)
        Me.tabSite.Controls.Add(Me.GroupBox1)
        Me.tabSite.Name = "tabSite"
        Me.tabSite.Size = New System.Drawing.Size(718, 465)
        Me.tabSite.Text = "Site Details"
        '
        'CareFrame1
        '
        Me.CareFrame1.Controls.Add(Me.CareLabel20)
        Me.CareFrame1.Controls.Add(Me.txtFinSageVer)
        Me.CareFrame1.Controls.Add(Me.CareLabel17)
        Me.CareFrame1.Controls.Add(Me.chkFinOverride)
        Me.CareFrame1.Controls.Add(Me.cbxFinSystem)
        Me.CareFrame1.Controls.Add(Me.txtFinSagePwd)
        Me.CareFrame1.Controls.Add(Me.CareLabel21)
        Me.CareFrame1.Controls.Add(Me.CareLabel18)
        Me.CareFrame1.Controls.Add(Me.txtFinSagePath)
        Me.CareFrame1.Controls.Add(Me.CareLabel19)
        Me.CareFrame1.Controls.Add(Me.txtFinPfxPath)
        Me.CareFrame1.Controls.Add(Me.txtFinSageUsername)
        Me.CareFrame1.Controls.Add(Me.CareLabel16)
        Me.CareFrame1.Controls.Add(Me.txtFinPfxPassword)
        Me.CareFrame1.Controls.Add(Me.CareLabel15)
        Me.CareFrame1.Controls.Add(Me.txtFinApiKey)
        Me.CareFrame1.Controls.Add(Me.CareLabel13)
        Me.CareFrame1.Controls.Add(Me.CareLabel11)
        Me.CareFrame1.Controls.Add(Me.txtFinApiSecret)
        Me.CareFrame1.Controls.Add(Me.CareLabel12)
        Me.CareFrame1.Location = New System.Drawing.Point(10, 291)
        Me.CareFrame1.Name = "CareFrame1"
        Me.CareFrame1.Size = New System.Drawing.Size(697, 170)
        Me.CareFrame1.TabIndex = 5
        Me.CareFrame1.Text = "Financial Integration"
        '
        'CareLabel20
        '
        Me.CareLabel20.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel20.Location = New System.Drawing.Point(9, 31)
        Me.CareLabel20.Name = "CareLabel20"
        Me.CareLabel20.Size = New System.Drawing.Size(91, 15)
        Me.CareLabel20.TabIndex = 0
        Me.CareLabel20.Text = "Override Defaults"
        Me.CareLabel20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFinSageVer
        '
        Me.txtFinSageVer.CharacterCasing = CharacterCasing.Normal
        Me.txtFinSageVer.EnterMoveNextControl = True
        Me.txtFinSageVer.Location = New System.Drawing.Point(122, 112)
        Me.txtFinSageVer.MaxLength = 4
        Me.txtFinSageVer.Name = "txtFinSageVer"
        Me.txtFinSageVer.NumericAllowNegatives = False
        Me.txtFinSageVer.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtFinSageVer.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFinSageVer.Properties.AccessibleName = "Site Name"
        Me.txtFinSageVer.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFinSageVer.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtFinSageVer.Properties.Appearance.Options.UseFont = True
        Me.txtFinSageVer.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFinSageVer.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFinSageVer.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFinSageVer.Properties.MaxLength = 4
        Me.txtFinSageVer.Size = New System.Drawing.Size(42, 22)
        Me.txtFinSageVer.TabIndex = 13
        Me.txtFinSageVer.Tag = "AE"
        Me.txtFinSageVer.TextAlign = HorizontalAlignment.Left
        Me.txtFinSageVer.ToolTipText = ""
        '
        'CareLabel17
        '
        Me.CareLabel17.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel17.Location = New System.Drawing.Point(237, 115)
        Me.CareLabel17.Name = "CareLabel17"
        Me.CareLabel17.Size = New System.Drawing.Size(81, 15)
        Me.CareLabel17.TabIndex = 14
        Me.CareLabel17.Text = "Sage Username"
        Me.CareLabel17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkFinOverride
        '
        Me.chkFinOverride.EnterMoveNextControl = True
        Me.chkFinOverride.Location = New System.Drawing.Point(122, 29)
        Me.chkFinOverride.Name = "chkFinOverride"
        Me.chkFinOverride.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkFinOverride.Properties.Appearance.Options.UseFont = True
        Me.chkFinOverride.Properties.Caption = ""
        Me.chkFinOverride.Size = New System.Drawing.Size(19, 19)
        Me.chkFinOverride.TabIndex = 1
        Me.chkFinOverride.Tag = "AEB"
        '
        'cbxFinSystem
        '
        Me.cbxFinSystem.AllowBlank = False
        Me.cbxFinSystem.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxFinSystem.DataSource = Nothing
        Me.cbxFinSystem.DisplayMember = Nothing
        Me.cbxFinSystem.EnterMoveNextControl = True
        Me.cbxFinSystem.Location = New System.Drawing.Point(122, 56)
        Me.cbxFinSystem.Name = "cbxFinSystem"
        Me.cbxFinSystem.Properties.AccessibleName = "Tariff Type"
        Me.cbxFinSystem.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxFinSystem.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxFinSystem.Properties.Appearance.Options.UseFont = True
        Me.cbxFinSystem.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxFinSystem.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxFinSystem.SelectedValue = Nothing
        Me.cbxFinSystem.Size = New System.Drawing.Size(170, 22)
        Me.cbxFinSystem.TabIndex = 3
        Me.cbxFinSystem.Tag = "AE"
        Me.cbxFinSystem.ValueMember = Nothing
        '
        'txtFinSagePwd
        '
        Me.txtFinSagePwd.CharacterCasing = CharacterCasing.Normal
        Me.txtFinSagePwd.EnterMoveNextControl = True
        Me.txtFinSagePwd.Location = New System.Drawing.Point(554, 112)
        Me.txtFinSagePwd.MaxLength = 20
        Me.txtFinSagePwd.Name = "txtFinSagePwd"
        Me.txtFinSagePwd.NumericAllowNegatives = False
        Me.txtFinSagePwd.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtFinSagePwd.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtFinSagePwd.Properties.AccessibleName = "Site Name"
        Me.txtFinSagePwd.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFinSagePwd.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtFinSagePwd.Properties.Appearance.Options.UseFont = True
        Me.txtFinSagePwd.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFinSagePwd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFinSagePwd.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFinSagePwd.Properties.MaxLength = 20
        Me.txtFinSagePwd.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtFinSagePwd.Size = New System.Drawing.Size(129, 22)
        Me.txtFinSagePwd.TabIndex = 17
        Me.txtFinSagePwd.Tag = "AE"
        Me.txtFinSagePwd.TextAlign = HorizontalAlignment.Left
        Me.txtFinSagePwd.ToolTipText = ""
        '
        'CareLabel21
        '
        Me.CareLabel21.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel21.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel21.Location = New System.Drawing.Point(470, 115)
        Me.CareLabel21.Name = "CareLabel21"
        Me.CareLabel21.Size = New System.Drawing.Size(78, 15)
        Me.CareLabel21.TabIndex = 16
        Me.CareLabel21.Text = "Sage Password"
        Me.CareLabel21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel18
        '
        Me.CareLabel18.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel18.Location = New System.Drawing.Point(9, 143)
        Me.CareLabel18.Name = "CareLabel18"
        Me.CareLabel18.Size = New System.Drawing.Size(107, 15)
        Me.CareLabel18.TabIndex = 18
        Me.CareLabel18.Text = "Sage Company Path"
        Me.CareLabel18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel18.ToolTip = "The full path to the Sage ACCDATA folder for the appropriate company."
        Me.CareLabel18.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.CareLabel18.ToolTipTitle = "Path"
        '
        'txtFinSagePath
        '
        Me.txtFinSagePath.CharacterCasing = CharacterCasing.Normal
        Me.txtFinSagePath.EnterMoveNextControl = True
        Me.txtFinSagePath.Location = New System.Drawing.Point(122, 140)
        Me.txtFinSagePath.MaxLength = 250
        Me.txtFinSagePath.Name = "txtFinSagePath"
        Me.txtFinSagePath.NumericAllowNegatives = False
        Me.txtFinSagePath.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtFinSagePath.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFinSagePath.Properties.AccessibleName = "Site Name"
        Me.txtFinSagePath.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFinSagePath.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtFinSagePath.Properties.Appearance.Options.UseFont = True
        Me.txtFinSagePath.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFinSagePath.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFinSagePath.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFinSagePath.Properties.MaxLength = 250
        Me.txtFinSagePath.Size = New System.Drawing.Size(561, 22)
        Me.txtFinSagePath.TabIndex = 19
        Me.txtFinSagePath.Tag = "AE"
        Me.txtFinSagePath.TextAlign = HorizontalAlignment.Left
        Me.txtFinSagePath.ToolTipText = ""
        '
        'CareLabel19
        '
        Me.CareLabel19.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel19.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel19.Location = New System.Drawing.Point(9, 87)
        Me.CareLabel19.Name = "CareLabel19"
        Me.CareLabel19.Size = New System.Drawing.Size(47, 15)
        Me.CareLabel19.TabIndex = 8
        Me.CareLabel19.Text = "PFX Path"
        Me.CareLabel19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFinPfxPath
        '
        Me.txtFinPfxPath.CharacterCasing = CharacterCasing.Normal
        Me.txtFinPfxPath.EnterMoveNextControl = True
        Me.txtFinPfxPath.Location = New System.Drawing.Point(122, 84)
        Me.txtFinPfxPath.MaxLength = 250
        Me.txtFinPfxPath.Name = "txtFinPfxPath"
        Me.txtFinPfxPath.NumericAllowNegatives = False
        Me.txtFinPfxPath.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtFinPfxPath.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFinPfxPath.Properties.AccessibleName = "Site Name"
        Me.txtFinPfxPath.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFinPfxPath.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtFinPfxPath.Properties.Appearance.Options.UseFont = True
        Me.txtFinPfxPath.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFinPfxPath.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFinPfxPath.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFinPfxPath.Properties.MaxLength = 250
        Me.txtFinPfxPath.Size = New System.Drawing.Size(329, 22)
        Me.txtFinPfxPath.TabIndex = 9
        Me.txtFinPfxPath.Tag = "AE"
        Me.txtFinPfxPath.TextAlign = HorizontalAlignment.Left
        Me.txtFinPfxPath.ToolTipText = ""
        '
        'txtFinSageUsername
        '
        Me.txtFinSageUsername.CharacterCasing = CharacterCasing.Normal
        Me.txtFinSageUsername.EnterMoveNextControl = True
        Me.txtFinSageUsername.Location = New System.Drawing.Point(324, 112)
        Me.txtFinSageUsername.MaxLength = 20
        Me.txtFinSageUsername.Name = "txtFinSageUsername"
        Me.txtFinSageUsername.NumericAllowNegatives = False
        Me.txtFinSageUsername.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtFinSageUsername.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFinSageUsername.Properties.AccessibleName = "Site Name"
        Me.txtFinSageUsername.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFinSageUsername.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtFinSageUsername.Properties.Appearance.Options.UseFont = True
        Me.txtFinSageUsername.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFinSageUsername.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFinSageUsername.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFinSageUsername.Properties.MaxLength = 20
        Me.txtFinSageUsername.Size = New System.Drawing.Size(127, 22)
        Me.txtFinSageUsername.TabIndex = 15
        Me.txtFinSageUsername.Tag = "AE"
        Me.txtFinSageUsername.TextAlign = HorizontalAlignment.Left
        Me.txtFinSageUsername.ToolTipText = ""
        '
        'CareLabel16
        '
        Me.CareLabel16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel16.Location = New System.Drawing.Point(9, 115)
        Me.CareLabel16.Name = "CareLabel16"
        Me.CareLabel16.Size = New System.Drawing.Size(67, 15)
        Me.CareLabel16.TabIndex = 12
        Me.CareLabel16.Text = "Sage Version"
        Me.CareLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFinPfxPassword
        '
        Me.txtFinPfxPassword.CharacterCasing = CharacterCasing.Normal
        Me.txtFinPfxPassword.EnterMoveNextControl = True
        Me.txtFinPfxPassword.Location = New System.Drawing.Point(554, 84)
        Me.txtFinPfxPassword.MaxLength = 20
        Me.txtFinPfxPassword.Name = "txtFinPfxPassword"
        Me.txtFinPfxPassword.NumericAllowNegatives = False
        Me.txtFinPfxPassword.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtFinPfxPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtFinPfxPassword.Properties.AccessibleName = "Site Name"
        Me.txtFinPfxPassword.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFinPfxPassword.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtFinPfxPassword.Properties.Appearance.Options.UseFont = True
        Me.txtFinPfxPassword.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFinPfxPassword.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFinPfxPassword.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFinPfxPassword.Properties.MaxLength = 20
        Me.txtFinPfxPassword.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtFinPfxPassword.Size = New System.Drawing.Size(129, 22)
        Me.txtFinPfxPassword.TabIndex = 11
        Me.txtFinPfxPassword.Tag = "AE"
        Me.txtFinPfxPassword.TextAlign = HorizontalAlignment.Left
        Me.txtFinPfxPassword.ToolTipText = ""
        '
        'CareLabel15
        '
        Me.CareLabel15.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel15.Location = New System.Drawing.Point(470, 87)
        Me.CareLabel15.Name = "CareLabel15"
        Me.CareLabel15.Size = New System.Drawing.Size(73, 15)
        Me.CareLabel15.TabIndex = 10
        Me.CareLabel15.Text = "PFX Password"
        Me.CareLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFinApiKey
        '
        Me.txtFinApiKey.CharacterCasing = CharacterCasing.Normal
        Me.txtFinApiKey.EnterMoveNextControl = True
        Me.txtFinApiKey.Location = New System.Drawing.Point(389, 28)
        Me.txtFinApiKey.MaxLength = 40
        Me.txtFinApiKey.Name = "txtFinApiKey"
        Me.txtFinApiKey.NumericAllowNegatives = False
        Me.txtFinApiKey.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtFinApiKey.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFinApiKey.Properties.AccessibleName = "Site Name"
        Me.txtFinApiKey.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFinApiKey.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtFinApiKey.Properties.Appearance.Options.UseFont = True
        Me.txtFinApiKey.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFinApiKey.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFinApiKey.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFinApiKey.Properties.MaxLength = 40
        Me.txtFinApiKey.Size = New System.Drawing.Size(294, 22)
        Me.txtFinApiKey.TabIndex = 5
        Me.txtFinApiKey.Tag = "AE"
        Me.txtFinApiKey.TextAlign = HorizontalAlignment.Left
        Me.txtFinApiKey.ToolTipText = ""
        '
        'CareLabel13
        '
        Me.CareLabel13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel13.Location = New System.Drawing.Point(330, 59)
        Me.CareLabel13.Name = "CareLabel13"
        Me.CareLabel13.Size = New System.Drawing.Size(53, 15)
        Me.CareLabel13.TabIndex = 6
        Me.CareLabel13.Text = "API Secret"
        Me.CareLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(9, 59)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(88, 15)
        Me.CareLabel11.TabIndex = 2
        Me.CareLabel11.Text = "Financial System"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFinApiSecret
        '
        Me.txtFinApiSecret.CharacterCasing = CharacterCasing.Normal
        Me.txtFinApiSecret.EnterMoveNextControl = True
        Me.txtFinApiSecret.Location = New System.Drawing.Point(389, 56)
        Me.txtFinApiSecret.MaxLength = 40
        Me.txtFinApiSecret.Name = "txtFinApiSecret"
        Me.txtFinApiSecret.NumericAllowNegatives = False
        Me.txtFinApiSecret.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtFinApiSecret.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFinApiSecret.Properties.AccessibleName = "Site Name"
        Me.txtFinApiSecret.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFinApiSecret.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtFinApiSecret.Properties.Appearance.Options.UseFont = True
        Me.txtFinApiSecret.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFinApiSecret.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFinApiSecret.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFinApiSecret.Properties.MaxLength = 40
        Me.txtFinApiSecret.Size = New System.Drawing.Size(294, 22)
        Me.txtFinApiSecret.TabIndex = 7
        Me.txtFinApiSecret.Tag = "AE"
        Me.txtFinApiSecret.TextAlign = HorizontalAlignment.Left
        Me.txtFinApiSecret.ToolTipText = ""
        '
        'CareLabel12
        '
        Me.CareLabel12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel12.Location = New System.Drawing.Point(330, 31)
        Me.CareLabel12.Name = "CareLabel12"
        Me.CareLabel12.Size = New System.Drawing.Size(40, 15)
        Me.CareLabel12.TabIndex = 4
        Me.CareLabel12.Text = "API Key"
        Me.CareLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl6
        '
        Me.GroupControl6.Controls.Add(Me.CareLabel9)
        Me.GroupControl6.Controls.Add(Me.txtFinanceEmail)
        Me.GroupControl6.Controls.Add(Me.txtFinanceSender)
        Me.GroupControl6.Controls.Add(Me.CareLabel10)
        Me.GroupControl6.Location = New System.Drawing.Point(373, 197)
        Me.GroupControl6.Name = "GroupControl6"
        Me.GroupControl6.Size = New System.Drawing.Size(334, 88)
        Me.GroupControl6.TabIndex = 4
        Me.GroupControl6.Text = "Financial / Accounts Correspondence"
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(9, 59)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(74, 15)
        Me.CareLabel9.TabIndex = 2
        Me.CareLabel9.Text = "Email Address"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFinanceEmail
        '
        Me.txtFinanceEmail.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtFinanceEmail.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFinanceEmail.Appearance.Options.UseFont = True
        Me.txtFinanceEmail.Location = New System.Drawing.Point(111, 56)
        Me.txtFinanceEmail.MaxLength = 250
        Me.txtFinanceEmail.Name = "txtFinanceEmail"
        Me.txtFinanceEmail.NoButton = False
        Me.txtFinanceEmail.NoColours = False
        Me.txtFinanceEmail.NoValidate = False
        Me.txtFinanceEmail.ReadOnly = False
        Me.txtFinanceEmail.Size = New System.Drawing.Size(210, 22)
        Me.txtFinanceEmail.TabIndex = 3
        Me.txtFinanceEmail.Tag = "AE"
        '
        'txtFinanceSender
        '
        Me.txtFinanceSender.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtFinanceSender.CharacterCasing = CharacterCasing.Normal
        Me.txtFinanceSender.EnterMoveNextControl = True
        Me.txtFinanceSender.Location = New System.Drawing.Point(111, 28)
        Me.txtFinanceSender.MaxLength = 250
        Me.txtFinanceSender.Name = "txtFinanceSender"
        Me.txtFinanceSender.NumericAllowNegatives = False
        Me.txtFinanceSender.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtFinanceSender.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFinanceSender.Properties.AccessibleName = "Site Name"
        Me.txtFinanceSender.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFinanceSender.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtFinanceSender.Properties.Appearance.Options.UseFont = True
        Me.txtFinanceSender.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFinanceSender.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFinanceSender.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFinanceSender.Properties.MaxLength = 250
        Me.txtFinanceSender.Size = New System.Drawing.Size(210, 22)
        Me.txtFinanceSender.TabIndex = 1
        Me.txtFinanceSender.Tag = "AE"
        Me.txtFinanceSender.TextAlign = HorizontalAlignment.Left
        Me.txtFinanceSender.ToolTipText = ""
        '
        'CareLabel10
        '
        Me.CareLabel10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel10.Location = New System.Drawing.Point(9, 31)
        Me.CareLabel10.Name = "CareLabel10"
        Me.CareLabel10.Size = New System.Drawing.Size(71, 15)
        Me.CareLabel10.TabIndex = 0
        Me.CareLabel10.Text = "Sender Name"
        Me.CareLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl5
        '
        Me.GroupControl5.Controls.Add(Me.CareLabel6)
        Me.GroupControl5.Controls.Add(Me.txtReportEmail)
        Me.GroupControl5.Controls.Add(Me.txtReportSender)
        Me.GroupControl5.Controls.Add(Me.CareLabel8)
        Me.GroupControl5.Location = New System.Drawing.Point(373, 103)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(334, 88)
        Me.GroupControl5.TabIndex = 3
        Me.GroupControl5.Text = "Daily Reports"
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(8, 58)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(74, 15)
        Me.CareLabel6.TabIndex = 2
        Me.CareLabel6.Text = "Email Address"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtReportEmail
        '
        Me.txtReportEmail.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtReportEmail.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReportEmail.Appearance.Options.UseFont = True
        Me.txtReportEmail.Location = New System.Drawing.Point(111, 55)
        Me.txtReportEmail.MaxLength = 250
        Me.txtReportEmail.Name = "txtReportEmail"
        Me.txtReportEmail.NoButton = False
        Me.txtReportEmail.NoColours = False
        Me.txtReportEmail.NoValidate = False
        Me.txtReportEmail.ReadOnly = False
        Me.txtReportEmail.Size = New System.Drawing.Size(210, 22)
        Me.txtReportEmail.TabIndex = 3
        Me.txtReportEmail.Tag = "AE"
        '
        'txtReportSender
        '
        Me.txtReportSender.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtReportSender.CharacterCasing = CharacterCasing.Normal
        Me.txtReportSender.EnterMoveNextControl = True
        Me.txtReportSender.Location = New System.Drawing.Point(111, 27)
        Me.txtReportSender.MaxLength = 250
        Me.txtReportSender.Name = "txtReportSender"
        Me.txtReportSender.NumericAllowNegatives = False
        Me.txtReportSender.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtReportSender.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtReportSender.Properties.AccessibleName = "Site Name"
        Me.txtReportSender.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtReportSender.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtReportSender.Properties.Appearance.Options.UseFont = True
        Me.txtReportSender.Properties.Appearance.Options.UseTextOptions = True
        Me.txtReportSender.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtReportSender.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtReportSender.Properties.MaxLength = 250
        Me.txtReportSender.Size = New System.Drawing.Size(210, 22)
        Me.txtReportSender.TabIndex = 1
        Me.txtReportSender.Tag = "AE"
        Me.txtReportSender.TextAlign = HorizontalAlignment.Left
        Me.txtReportSender.ToolTipText = ""
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(9, 30)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(71, 15)
        Me.CareLabel8.TabIndex = 0
        Me.CareLabel8.Text = "Sender Name"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.CareLabel5)
        Me.GroupControl4.Controls.Add(Me.txtGenEmail)
        Me.GroupControl4.Controls.Add(Me.txtGenSender)
        Me.GroupControl4.Controls.Add(Me.CareLabel7)
        Me.GroupControl4.Location = New System.Drawing.Point(373, 9)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(334, 88)
        Me.GroupControl4.TabIndex = 2
        Me.GroupControl4.Text = "General Correspondence"
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(9, 59)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(74, 15)
        Me.CareLabel5.TabIndex = 2
        Me.CareLabel5.Text = "Email Address"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtGenEmail
        '
        Me.txtGenEmail.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtGenEmail.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGenEmail.Appearance.Options.UseFont = True
        Me.txtGenEmail.Location = New System.Drawing.Point(111, 56)
        Me.txtGenEmail.MaxLength = 250
        Me.txtGenEmail.Name = "txtGenEmail"
        Me.txtGenEmail.NoButton = False
        Me.txtGenEmail.NoColours = False
        Me.txtGenEmail.NoValidate = False
        Me.txtGenEmail.ReadOnly = False
        Me.txtGenEmail.Size = New System.Drawing.Size(210, 22)
        Me.txtGenEmail.TabIndex = 3
        Me.txtGenEmail.Tag = "AE"
        '
        'txtGenSender
        '
        Me.txtGenSender.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtGenSender.CharacterCasing = CharacterCasing.Normal
        Me.txtGenSender.EnterMoveNextControl = True
        Me.txtGenSender.Location = New System.Drawing.Point(111, 28)
        Me.txtGenSender.MaxLength = 250
        Me.txtGenSender.Name = "txtGenSender"
        Me.txtGenSender.NumericAllowNegatives = False
        Me.txtGenSender.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtGenSender.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtGenSender.Properties.AccessibleName = "Site Name"
        Me.txtGenSender.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtGenSender.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtGenSender.Properties.Appearance.Options.UseFont = True
        Me.txtGenSender.Properties.Appearance.Options.UseTextOptions = True
        Me.txtGenSender.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtGenSender.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtGenSender.Properties.MaxLength = 250
        Me.txtGenSender.Size = New System.Drawing.Size(210, 22)
        Me.txtGenSender.TabIndex = 1
        Me.txtGenSender.Tag = "AE"
        Me.txtGenSender.TextAlign = HorizontalAlignment.Left
        Me.txtGenSender.ToolTipText = ""
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(9, 31)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(71, 15)
        Me.CareLabel7.TabIndex = 0
        Me.CareLabel7.Text = "Sender Name"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.txtAccNext)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.txtAccPrefix)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Controls.Add(Me.cbxNLTrack)
        Me.GroupControl1.Controls.Add(Me.CareLabel22)
        Me.GroupControl1.Controls.Add(Me.cbxNLCode)
        Me.GroupControl1.Controls.Add(Me.CareLabel14)
        Me.GroupControl1.Location = New System.Drawing.Point(10, 186)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(357, 99)
        Me.GroupControl1.TabIndex = 1
        '
        'txtAccNext
        '
        Me.txtAccNext.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtAccNext.CharacterCasing = CharacterCasing.Normal
        Me.txtAccNext.EnterMoveNextControl = True
        Me.txtAccNext.Location = New System.Drawing.Point(286, 67)
        Me.txtAccNext.MaxLength = 5
        Me.txtAccNext.Name = "txtAccNext"
        Me.txtAccNext.NumericAllowNegatives = False
        Me.txtAccNext.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtAccNext.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtAccNext.Properties.AccessibleName = "Next Account Number"
        Me.txtAccNext.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtAccNext.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtAccNext.Properties.Appearance.Options.UseFont = True
        Me.txtAccNext.Properties.Appearance.Options.UseTextOptions = True
        Me.txtAccNext.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtAccNext.Properties.Mask.EditMask = "##########;"
        Me.txtAccNext.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtAccNext.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtAccNext.Properties.MaxLength = 5
        Me.txtAccNext.Size = New System.Drawing.Size(62, 22)
        Me.txtAccNext.TabIndex = 7
        Me.txtAccNext.Tag = "AEM"
        Me.txtAccNext.TextAlign = HorizontalAlignment.Left
        Me.txtAccNext.ToolTipText = ""
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(189, 70)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(91, 15)
        Me.CareLabel2.TabIndex = 6
        Me.CareLabel2.Text = "Next Account No"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAccPrefix
        '
        Me.txtAccPrefix.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtAccPrefix.CharacterCasing = CharacterCasing.Upper
        Me.txtAccPrefix.EnterMoveNextControl = True
        Me.txtAccPrefix.Location = New System.Drawing.Point(111, 67)
        Me.txtAccPrefix.MaxLength = 5
        Me.txtAccPrefix.Name = "txtAccPrefix"
        Me.txtAccPrefix.NumericAllowNegatives = False
        Me.txtAccPrefix.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtAccPrefix.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtAccPrefix.Properties.AccessibleName = "Name"
        Me.txtAccPrefix.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtAccPrefix.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtAccPrefix.Properties.Appearance.Options.UseFont = True
        Me.txtAccPrefix.Properties.Appearance.Options.UseTextOptions = True
        Me.txtAccPrefix.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtAccPrefix.Properties.CharacterCasing = CharacterCasing.Upper
        Me.txtAccPrefix.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtAccPrefix.Properties.MaxLength = 5
        Me.txtAccPrefix.Size = New System.Drawing.Size(72, 22)
        Me.txtAccPrefix.TabIndex = 5
        Me.txtAccPrefix.Tag = "AE"
        Me.txtAccPrefix.TextAlign = HorizontalAlignment.Left
        Me.txtAccPrefix.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(8, 70)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(96, 15)
        Me.CareLabel1.TabIndex = 4
        Me.CareLabel1.Text = "Account No Prefix"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxNLTrack
        '
        Me.cbxNLTrack.AllowBlank = False
        Me.cbxNLTrack.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxNLTrack.DataSource = Nothing
        Me.cbxNLTrack.DisplayMember = Nothing
        Me.cbxNLTrack.EnterMoveNextControl = True
        Me.cbxNLTrack.Location = New System.Drawing.Point(111, 39)
        Me.cbxNLTrack.Name = "cbxNLTrack"
        Me.cbxNLTrack.Properties.AccessibleName = "Tariff Type"
        Me.cbxNLTrack.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxNLTrack.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxNLTrack.Properties.Appearance.Options.UseFont = True
        Me.cbxNLTrack.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxNLTrack.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxNLTrack.SelectedValue = Nothing
        Me.cbxNLTrack.Size = New System.Drawing.Size(237, 22)
        Me.cbxNLTrack.TabIndex = 3
        Me.cbxNLTrack.Tag = "AE"
        Me.cbxNLTrack.ValueMember = Nothing
        '
        'CareLabel22
        '
        Me.CareLabel22.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel22.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel22.Location = New System.Drawing.Point(8, 42)
        Me.CareLabel22.Name = "CareLabel22"
        Me.CareLabel22.Size = New System.Drawing.Size(77, 15)
        Me.CareLabel22.TabIndex = 2
        Me.CareLabel22.Text = "Tracking Code"
        Me.CareLabel22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxNLCode
        '
        Me.cbxNLCode.AllowBlank = False
        Me.cbxNLCode.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxNLCode.DataSource = Nothing
        Me.cbxNLCode.DisplayMember = Nothing
        Me.cbxNLCode.EnterMoveNextControl = True
        Me.cbxNLCode.Location = New System.Drawing.Point(111, 11)
        Me.cbxNLCode.Name = "cbxNLCode"
        Me.cbxNLCode.Properties.AccessibleName = "Tariff Type"
        Me.cbxNLCode.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxNLCode.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxNLCode.Properties.Appearance.Options.UseFont = True
        Me.cbxNLCode.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxNLCode.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxNLCode.SelectedValue = Nothing
        Me.cbxNLCode.Size = New System.Drawing.Size(237, 22)
        Me.cbxNLCode.TabIndex = 1
        Me.cbxNLCode.Tag = "AE"
        Me.cbxNLCode.ValueMember = Nothing
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(8, 14)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(46, 15)
        Me.CareLabel14.TabIndex = 0
        Me.CareLabel14.Text = "NL Code"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CareLabel3)
        Me.GroupBox1.Controls.Add(Me.txtTel)
        Me.GroupBox1.Controls.Add(Me.txtAddress)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtName)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(10, 9)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.ShowCaption = False
        Me.GroupBox1.Size = New System.Drawing.Size(357, 171)
        Me.GroupBox1.TabIndex = 0
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(9, 148)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(75, 15)
        Me.CareLabel3.TabIndex = 4
        Me.CareLabel3.Text = "Telephone No"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTel
        '
        Me.txtTel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTel.Appearance.Options.UseFont = True
        Me.txtTel.IsMobile = False
        Me.txtTel.Location = New System.Drawing.Point(111, 145)
        Me.txtTel.MaxLength = 15
        Me.txtTel.MinimumSize = New System.Drawing.Size(0, 22)
        Me.txtTel.Name = "txtTel"
        Me.txtTel.ReadOnly = False
        Me.txtTel.Size = New System.Drawing.Size(233, 22)
        Me.txtTel.TabIndex = 5
        Me.txtTel.Tag = "AE"
        '
        'txtAddress
        '
        Me.txtAddress.Address = ""
        Me.txtAddress.AddressBlock = ""
        Me.txtAddress.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.Appearance.Options.UseFont = True
        Me.txtAddress.County = ""
        Me.txtAddress.DisplayMode = Care.Address.CareAddress.EnumDisplayMode.SingleControl
        Me.txtAddress.Location = New System.Drawing.Point(111, 40)
        Me.txtAddress.MaxLength = 500
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.PostCode = ""
        Me.txtAddress.ReadOnly = False
        Me.txtAddress.Size = New System.Drawing.Size(233, 99)
        Me.txtAddress.TabIndex = 3
        Me.txtAddress.Tag = "AE"
        Me.txtAddress.Town = ""
        '
        'Label2
        '
        Me.Label2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label2.Location = New System.Drawing.Point(8, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 15)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Address"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.CharacterCasing = CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(111, 12)
        Me.txtName.MaxLength = 40
        Me.txtName.Name = "txtName"
        Me.txtName.NumericAllowNegatives = False
        Me.txtName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AccessibleName = "Site Name"
        Me.txtName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtName.Properties.MaxLength = 40
        Me.txtName.Size = New System.Drawing.Size(233, 22)
        Me.txtName.TabIndex = 1
        Me.txtName.Tag = "AEBM"
        Me.txtName.TextAlign = HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'Label1
        '
        Me.Label1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label1.Location = New System.Drawing.Point(8, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Site Name"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabRooms
        '
        Me.tabRooms.Controls.Add(Me.GroupControl3)
        Me.tabRooms.Controls.Add(Me.GroupControl2)
        Me.tabRooms.Name = "tabRooms"
        Me.tabRooms.Size = New System.Drawing.Size(718, 465)
        Me.tabRooms.Text = "Rooms && Ratios"
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.cgRatios)
        Me.GroupControl3.Location = New System.Drawing.Point(3, 292)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(712, 173)
        Me.GroupControl3.TabIndex = 5
        Me.GroupControl3.Text = "Room Ratios"
        '
        'cgRatios
        '
        Me.cgRatios.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgRatios.ButtonsEnabled = False
        Me.cgRatios.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgRatios.HideFirstColumn = False
        Me.cgRatios.Location = New System.Drawing.Point(5, 23)
        Me.cgRatios.Name = "cgRatios"
        Me.cgRatios.PreviewColumn = ""
        Me.cgRatios.Size = New System.Drawing.Size(702, 145)
        Me.cgRatios.TabIndex = 0
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.cgRooms)
        Me.GroupControl2.Location = New System.Drawing.Point(3, 3)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(712, 283)
        Me.GroupControl2.TabIndex = 4
        Me.GroupControl2.Text = "Rooms"
        '
        'cgRooms
        '
        Me.cgRooms.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgRooms.ButtonsEnabled = False
        Me.cgRooms.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgRooms.HideFirstColumn = False
        Me.cgRooms.Location = New System.Drawing.Point(5, 23)
        Me.cgRooms.Name = "cgRooms"
        Me.cgRooms.PreviewColumn = ""
        Me.cgRooms.Size = New System.Drawing.Size(702, 255)
        Me.cgRooms.TabIndex = 0
        '
        'tabUsers
        '
        Me.tabUsers.Controls.Add(Me.CareLabel4)
        Me.tabUsers.Controls.Add(Me.cgUsers)
        Me.tabUsers.Name = "tabUsers"
        Me.tabUsers.Size = New System.Drawing.Size(718, 465)
        Me.tabUsers.Text = "User Access"
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel4.Appearance.ForeColor = System.Drawing.Color.Red
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(7, 3)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(570, 21)
        Me.CareLabel4.TabIndex = 1
        Me.CareLabel4.Text = "We assume all Users can access all Sites. Users in this list CANNOT access this S" &
    "ite."
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cgUsers
        '
        Me.cgUsers.ButtonsEnabled = False
        Me.cgUsers.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgUsers.HideFirstColumn = False
        Me.cgUsers.Location = New System.Drawing.Point(3, 30)
        Me.cgUsers.Name = "cgUsers"
        Me.cgUsers.PreviewColumn = ""
        Me.cgUsers.Size = New System.Drawing.Size(712, 435)
        Me.cgUsers.TabIndex = 0
        '
        'frmSites
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(748, 591)
        Me.Controls.Add(Me.tabMain)
        Me.Name = "frmSites"
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.tabMain, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.tabMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabMain.ResumeLayout(False)
        Me.tabMain.PerformLayout()
        Me.tabSite.ResumeLayout(False)
        CType(Me.CareFrame1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CareFrame1.ResumeLayout(False)
        Me.CareFrame1.PerformLayout()
        CType(Me.txtFinSageVer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkFinOverride.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxFinSystem.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFinSagePwd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFinSagePath.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFinPfxPath.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFinSageUsername.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFinPfxPassword.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFinApiKey.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFinApiSecret.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl6.ResumeLayout(False)
        Me.GroupControl6.PerformLayout()
        CType(Me.txtFinanceSender.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.txtReportSender.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.txtGenSender.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtAccNext.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAccPrefix.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxNLTrack.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxNLCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabRooms.ResumeLayout(False)
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        Me.tabUsers.ResumeLayout(False)
        Me.tabUsers.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tabMain As Care.Controls.CareTab
    Friend WithEvents tabSite As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents txtAccNext As Care.Controls.CareTextBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents txtAccPrefix As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents cbxNLTrack As Care.Controls.CareComboBox
    Friend WithEvents CareLabel22 As Care.Controls.CareLabel
    Friend WithEvents cbxNLCode As Care.Controls.CareComboBox
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents txtTel As Care.Shared.CareTelephoneNumber
    Friend WithEvents txtAddress As Care.Address.CareAddress
    Friend WithEvents Label2 As Care.Controls.CareLabel
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents Label1 As Care.Controls.CareLabel
    Friend WithEvents tabRooms As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cgRatios As Care.Controls.CareGridWithButtons
    Friend WithEvents cgRooms As Care.Controls.CareGridWithButtons
    Friend WithEvents tabUsers As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cgUsers As Care.Controls.CareGridWithButtons
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents txtFinanceEmail As Care.Shared.CareEmailAddress
    Friend WithEvents txtFinanceSender As Care.Controls.CareTextBox
    Friend WithEvents CareLabel10 As Care.Controls.CareLabel
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents txtReportEmail As Care.Shared.CareEmailAddress
    Friend WithEvents txtReportSender As Care.Controls.CareTextBox
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents txtGenEmail As Care.Shared.CareEmailAddress
    Friend WithEvents txtGenSender As Care.Controls.CareTextBox
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents CareFrame1 As Care.Controls.CareFrame
    Friend WithEvents txtFinSagePwd As Care.Controls.CareTextBox
    Friend WithEvents CareLabel21 As Care.Controls.CareLabel
    Friend WithEvents CareLabel18 As Care.Controls.CareLabel
    Friend WithEvents txtFinSagePath As Care.Controls.CareTextBox
    Friend WithEvents CareLabel19 As Care.Controls.CareLabel
    Friend WithEvents txtFinPfxPath As Care.Controls.CareTextBox
    Friend WithEvents txtFinSageUsername As Care.Controls.CareTextBox
    Friend WithEvents CareLabel16 As Care.Controls.CareLabel
    Friend WithEvents txtFinPfxPassword As Care.Controls.CareTextBox
    Friend WithEvents CareLabel15 As Care.Controls.CareLabel
    Friend WithEvents txtFinApiKey As Care.Controls.CareTextBox
    Friend WithEvents CareLabel13 As Care.Controls.CareLabel
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents txtFinApiSecret As Care.Controls.CareTextBox
    Friend WithEvents CareLabel12 As Care.Controls.CareLabel
    Friend WithEvents GroupControl1 As Care.Controls.CareFrame
    Friend WithEvents GroupBox1 As Care.Controls.CareFrame
    Friend WithEvents GroupControl3 As Care.Controls.CareFrame
    Friend WithEvents GroupControl2 As Care.Controls.CareFrame
    Friend WithEvents GroupControl6 As Care.Controls.CareFrame
    Friend WithEvents GroupControl5 As Care.Controls.CareFrame
    Friend WithEvents GroupControl4 As Care.Controls.CareFrame
    Friend WithEvents CareLabel20 As Care.Controls.CareLabel
    Friend WithEvents txtFinSageVer As Care.Controls.CareTextBox
    Friend WithEvents CareLabel17 As Care.Controls.CareLabel
    Friend WithEvents chkFinOverride As Care.Controls.CareCheckBox
    Friend WithEvents cbxFinSystem As Care.Controls.CareComboBox
End Class
