﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLeadConvert
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl6 = New Care.Controls.CareFrame()
        Me.Label12 = New Care.Controls.CareLabel(Me.components)
        Me.cbxChildGender = New Care.Controls.CareComboBox(Me.components)
        Me.lblAge = New Care.Controls.CareLabel(Me.components)
        Me.Label16 = New Care.Controls.CareLabel(Me.components)
        Me.cdtDOB = New Care.Controls.CareDateTime(Me.components)
        Me.txtChildSurname = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel23 = New Care.Controls.CareLabel(Me.components)
        Me.txtChildForename = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel24 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.chkEmailReports = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.chkEmailInvoices = New Care.Controls.CareCheckBox(Me.components)
        Me.txtTel = New Care.[Shared].CareTelephoneNumber()
        Me.txtMobile = New Care.[Shared].CareTelephoneNumber()
        Me.txtEmail = New Care.[Shared].CareEmailAddress()
        Me.Label13 = New Care.Controls.CareLabel(Me.components)
        Me.Label8 = New Care.Controls.CareLabel(Me.components)
        Me.Label9 = New Care.Controls.CareLabel(Me.components)
        Me.txtAddressFull = New Care.Address.CareAddress()
        Me.Label2 = New Care.Controls.CareLabel(Me.components)
        Me.txtContactRelationship = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.txtContactSurname = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.txtContactForename = New Care.Controls.CareTextBox(Me.components)
        Me.GroupControl2 = New Care.Controls.CareFrame()
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel10 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.cbxKeyworker = New Care.Controls.CareComboBox(Me.components)
        Me.cdtStart = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel12 = New Care.Controls.CareLabel(Me.components)
        Me.cbxRoom = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel13 = New Care.Controls.CareLabel(Me.components)
        Me.cbxSite = New Care.Controls.CareComboBox(Me.components)
        Me.btnOK = New Care.Controls.CareButton(Me.components)
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl6.SuspendLayout()
        CType(Me.cbxChildGender.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDOB.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDOB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtChildSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtChildForename.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.chkEmailReports.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkEmailInvoices.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtContactRelationship.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtContactSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtContactForename.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.cbxKeyworker.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxRoom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl6
        '
        Me.GroupControl6.Controls.Add(Me.Label12)
        Me.GroupControl6.Controls.Add(Me.cbxChildGender)
        Me.GroupControl6.Controls.Add(Me.lblAge)
        Me.GroupControl6.Controls.Add(Me.Label16)
        Me.GroupControl6.Controls.Add(Me.cdtDOB)
        Me.GroupControl6.Controls.Add(Me.txtChildSurname)
        Me.GroupControl6.Controls.Add(Me.CareLabel23)
        Me.GroupControl6.Controls.Add(Me.txtChildForename)
        Me.GroupControl6.Controls.Add(Me.CareLabel24)
        Me.GroupControl6.Location = New System.Drawing.Point(362, 12)
        Me.GroupControl6.Name = "GroupControl6"
        Me.GroupControl6.Size = New System.Drawing.Size(345, 140)
        Me.GroupControl6.TabIndex = 1
        Me.GroupControl6.Text = "Child Details"
        '
        'Label12
        '
        Me.Label12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label12.Location = New System.Drawing.Point(10, 86)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(38, 15)
        Me.Label12.TabIndex = 4
        Me.Label12.Text = "Gender"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxChildGender
        '
        Me.cbxChildGender.AllowBlank = False
        Me.cbxChildGender.DataSource = Nothing
        Me.cbxChildGender.DisplayMember = Nothing
        Me.cbxChildGender.EnterMoveNextControl = True
        Me.cbxChildGender.Location = New System.Drawing.Point(100, 83)
        Me.cbxChildGender.Name = "cbxChildGender"
        Me.cbxChildGender.Properties.AccessibleName = "Gender"
        Me.cbxChildGender.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxChildGender.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxChildGender.Properties.Appearance.Options.UseFont = True
        Me.cbxChildGender.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxChildGender.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxChildGender.SelectedValue = Nothing
        Me.cbxChildGender.Size = New System.Drawing.Size(104, 22)
        Me.cbxChildGender.TabIndex = 5
        Me.cbxChildGender.Tag = "AEM"
        Me.cbxChildGender.ValueMember = Nothing
        '
        'lblAge
        '
        Me.lblAge.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAge.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblAge.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblAge.Location = New System.Drawing.Point(210, 114)
        Me.lblAge.Name = "lblAge"
        Me.lblAge.Size = New System.Drawing.Size(99, 15)
        Me.lblAge.TabIndex = 8
        Me.lblAge.Text = "1 year, 11 months"
        Me.lblAge.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label16
        '
        Me.Label16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label16.Location = New System.Drawing.Point(10, 114)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(66, 15)
        Me.Label16.TabIndex = 6
        Me.Label16.Text = "Date of Birth"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtDOB
        '
        Me.cdtDOB.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtDOB.EnterMoveNextControl = True
        Me.cdtDOB.Location = New System.Drawing.Point(100, 111)
        Me.cdtDOB.Name = "cdtDOB"
        Me.cdtDOB.Properties.AccessibleName = "Date of Birth"
        Me.cdtDOB.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtDOB.Properties.Appearance.Options.UseFont = True
        Me.cdtDOB.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDOB.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtDOB.Size = New System.Drawing.Size(104, 22)
        Me.cdtDOB.TabIndex = 7
        Me.cdtDOB.Tag = "AEM"
        Me.cdtDOB.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'txtChildSurname
        '
        Me.txtChildSurname.CharacterCasing = CharacterCasing.Normal
        Me.txtChildSurname.EnterMoveNextControl = True
        Me.txtChildSurname.Location = New System.Drawing.Point(100, 57)
        Me.txtChildSurname.MaxLength = 30
        Me.txtChildSurname.Name = "txtChildSurname"
        Me.txtChildSurname.NumericAllowNegatives = False
        Me.txtChildSurname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtChildSurname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtChildSurname.Properties.AccessibleName = "Child Surname"
        Me.txtChildSurname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtChildSurname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtChildSurname.Properties.Appearance.Options.UseFont = True
        Me.txtChildSurname.Properties.Appearance.Options.UseTextOptions = True
        Me.txtChildSurname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtChildSurname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtChildSurname.Properties.MaxLength = 30
        Me.txtChildSurname.Size = New System.Drawing.Size(235, 22)
        Me.txtChildSurname.TabIndex = 3
        Me.txtChildSurname.Tag = "AEM"
        Me.txtChildSurname.TextAlign = HorizontalAlignment.Left
        Me.txtChildSurname.ToolTipText = ""
        '
        'CareLabel23
        '
        Me.CareLabel23.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel23.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel23.Location = New System.Drawing.Point(10, 60)
        Me.CareLabel23.Name = "CareLabel23"
        Me.CareLabel23.Size = New System.Drawing.Size(47, 15)
        Me.CareLabel23.TabIndex = 2
        Me.CareLabel23.Text = "Surname"
        Me.CareLabel23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtChildForename
        '
        Me.txtChildForename.CharacterCasing = CharacterCasing.Normal
        Me.txtChildForename.EnterMoveNextControl = True
        Me.txtChildForename.Location = New System.Drawing.Point(100, 29)
        Me.txtChildForename.MaxLength = 30
        Me.txtChildForename.Name = "txtChildForename"
        Me.txtChildForename.NumericAllowNegatives = False
        Me.txtChildForename.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtChildForename.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtChildForename.Properties.AccessibleName = "Child Forename"
        Me.txtChildForename.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtChildForename.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtChildForename.Properties.Appearance.Options.UseFont = True
        Me.txtChildForename.Properties.Appearance.Options.UseTextOptions = True
        Me.txtChildForename.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtChildForename.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtChildForename.Properties.MaxLength = 30
        Me.txtChildForename.Size = New System.Drawing.Size(235, 22)
        Me.txtChildForename.TabIndex = 1
        Me.txtChildForename.Tag = "AEM"
        Me.txtChildForename.TextAlign = HorizontalAlignment.Left
        Me.txtChildForename.ToolTipText = ""
        '
        'CareLabel24
        '
        Me.CareLabel24.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel24.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel24.Location = New System.Drawing.Point(10, 32)
        Me.CareLabel24.Name = "CareLabel24"
        Me.CareLabel24.Size = New System.Drawing.Size(53, 15)
        Me.CareLabel24.TabIndex = 0
        Me.CareLabel24.Text = "Forename"
        Me.CareLabel24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Controls.Add(Me.chkEmailReports)
        Me.GroupControl1.Controls.Add(Me.CareLabel7)
        Me.GroupControl1.Controls.Add(Me.chkEmailInvoices)
        Me.GroupControl1.Controls.Add(Me.txtTel)
        Me.GroupControl1.Controls.Add(Me.txtMobile)
        Me.GroupControl1.Controls.Add(Me.txtEmail)
        Me.GroupControl1.Controls.Add(Me.Label13)
        Me.GroupControl1.Controls.Add(Me.Label8)
        Me.GroupControl1.Controls.Add(Me.Label9)
        Me.GroupControl1.Controls.Add(Me.txtAddressFull)
        Me.GroupControl1.Controls.Add(Me.Label2)
        Me.GroupControl1.Controls.Add(Me.txtContactRelationship)
        Me.GroupControl1.Controls.Add(Me.CareLabel4)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Controls.Add(Me.txtContactSurname)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.txtContactForename)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(344, 317)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Primary Contact Details"
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(207, 294)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(101, 15)
        Me.CareLabel3.TabIndex = 16
        Me.CareLabel3.Text = "Email Daily Reports"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkEmailReports
        '
        Me.chkEmailReports.EnterMoveNextControl = True
        Me.chkEmailReports.Location = New System.Drawing.Point(314, 292)
        Me.chkEmailReports.Name = "chkEmailReports"
        Me.chkEmailReports.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEmailReports.Properties.Appearance.Options.UseFont = True
        Me.chkEmailReports.Size = New System.Drawing.Size(20, 19)
        Me.chkEmailReports.TabIndex = 17
        Me.chkEmailReports.Tag = "AE"
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(10, 294)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(75, 15)
        Me.CareLabel7.TabIndex = 14
        Me.CareLabel7.Text = "Email Invoices"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkEmailInvoices
        '
        Me.chkEmailInvoices.EnterMoveNextControl = True
        Me.chkEmailInvoices.Location = New System.Drawing.Point(99, 292)
        Me.chkEmailInvoices.Name = "chkEmailInvoices"
        Me.chkEmailInvoices.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEmailInvoices.Properties.Appearance.Options.UseFont = True
        Me.chkEmailInvoices.Size = New System.Drawing.Size(20, 19)
        Me.chkEmailInvoices.TabIndex = 15
        Me.chkEmailInvoices.Tag = "AE"
        '
        'txtTel
        '
        Me.txtTel.AccessibleName = "Contact Telephone Number"
        Me.txtTel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTel.Appearance.Options.UseFont = True
        Me.txtTel.IsMobile = True
        Me.txtTel.Location = New System.Drawing.Point(100, 208)
        Me.txtTel.MaxLength = 15
        Me.txtTel.MinimumSize = New System.Drawing.Size(0, 22)
        Me.txtTel.Name = "txtTel"
        Me.txtTel.ReadOnly = False
        Me.txtTel.Size = New System.Drawing.Size(234, 22)
        Me.txtTel.TabIndex = 9
        Me.txtTel.Tag = "AE"
        '
        'txtMobile
        '
        Me.txtMobile.AccessibleName = "Contact Mobile Number"
        Me.txtMobile.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMobile.Appearance.Options.UseFont = True
        Me.txtMobile.IsMobile = True
        Me.txtMobile.Location = New System.Drawing.Point(100, 236)
        Me.txtMobile.MaxLength = 15
        Me.txtMobile.MinimumSize = New System.Drawing.Size(0, 22)
        Me.txtMobile.Name = "txtMobile"
        Me.txtMobile.ReadOnly = False
        Me.txtMobile.Size = New System.Drawing.Size(234, 22)
        Me.txtMobile.TabIndex = 11
        Me.txtMobile.Tag = "AE"
        '
        'txtEmail
        '
        Me.txtEmail.AccessibleName = "Contact Email"
        Me.txtEmail.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail.Appearance.Options.UseFont = True
        Me.txtEmail.Location = New System.Drawing.Point(100, 264)
        Me.txtEmail.MaxLength = 100
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.NoButton = False
        Me.txtEmail.NoColours = False
        Me.txtEmail.NoValidate = False
        Me.txtEmail.ReadOnly = False
        Me.txtEmail.Size = New System.Drawing.Size(234, 22)
        Me.txtEmail.TabIndex = 13
        Me.txtEmail.Tag = "AE"
        '
        'Label13
        '
        Me.Label13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label13.Location = New System.Drawing.Point(10, 268)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(29, 15)
        Me.Label13.TabIndex = 12
        Me.Label13.Text = "Email"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label8.Location = New System.Drawing.Point(10, 240)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(37, 15)
        Me.Label8.TabIndex = 10
        Me.Label8.Text = "Mobile"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label9.Location = New System.Drawing.Point(10, 212)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(56, 15)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "Telephone"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAddressFull
        '
        Me.txtAddressFull.AccessibleName = "Address"
        Me.txtAddressFull.Address = ""
        Me.txtAddressFull.AddressBlock = ""
        Me.txtAddressFull.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddressFull.Appearance.Options.UseFont = True
        Me.txtAddressFull.County = ""
        Me.txtAddressFull.DisplayMode = Care.Address.CareAddress.EnumDisplayMode.SingleControl
        Me.txtAddressFull.Location = New System.Drawing.Point(100, 107)
        Me.txtAddressFull.MaxLength = 500
        Me.txtAddressFull.Name = "txtAddressFull"
        Me.txtAddressFull.PostCode = ""
        Me.txtAddressFull.ReadOnly = False
        Me.txtAddressFull.Size = New System.Drawing.Size(234, 95)
        Me.txtAddressFull.TabIndex = 7
        Me.txtAddressFull.Tag = "AEM"
        Me.txtAddressFull.Town = ""
        '
        'Label2
        '
        Me.Label2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label2.Location = New System.Drawing.Point(10, 112)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 15)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Address"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtContactRelationship
        '
        Me.txtContactRelationship.CharacterCasing = CharacterCasing.Normal
        Me.txtContactRelationship.EnterMoveNextControl = True
        Me.txtContactRelationship.Location = New System.Drawing.Point(100, 81)
        Me.txtContactRelationship.MaxLength = 30
        Me.txtContactRelationship.Name = "txtContactRelationship"
        Me.txtContactRelationship.NumericAllowNegatives = False
        Me.txtContactRelationship.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtContactRelationship.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtContactRelationship.Properties.AccessibleName = "Relationship"
        Me.txtContactRelationship.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtContactRelationship.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtContactRelationship.Properties.Appearance.Options.UseFont = True
        Me.txtContactRelationship.Properties.Appearance.Options.UseTextOptions = True
        Me.txtContactRelationship.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtContactRelationship.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtContactRelationship.Properties.MaxLength = 30
        Me.txtContactRelationship.Size = New System.Drawing.Size(234, 22)
        Me.txtContactRelationship.TabIndex = 5
        Me.txtContactRelationship.Tag = "AEM"
        Me.txtContactRelationship.TextAlign = HorizontalAlignment.Left
        Me.txtContactRelationship.ToolTipText = ""
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(10, 84)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(65, 15)
        Me.CareLabel4.TabIndex = 4
        Me.CareLabel4.Text = "Relationship"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(10, 32)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(53, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Forename"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtContactSurname
        '
        Me.txtContactSurname.CharacterCasing = CharacterCasing.Normal
        Me.txtContactSurname.EnterMoveNextControl = True
        Me.txtContactSurname.Location = New System.Drawing.Point(100, 55)
        Me.txtContactSurname.MaxLength = 30
        Me.txtContactSurname.Name = "txtContactSurname"
        Me.txtContactSurname.NumericAllowNegatives = False
        Me.txtContactSurname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtContactSurname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtContactSurname.Properties.AccessibleName = "Contact Surname"
        Me.txtContactSurname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtContactSurname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtContactSurname.Properties.Appearance.Options.UseFont = True
        Me.txtContactSurname.Properties.Appearance.Options.UseTextOptions = True
        Me.txtContactSurname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtContactSurname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtContactSurname.Properties.MaxLength = 30
        Me.txtContactSurname.Size = New System.Drawing.Size(234, 22)
        Me.txtContactSurname.TabIndex = 3
        Me.txtContactSurname.Tag = "AEM"
        Me.txtContactSurname.TextAlign = HorizontalAlignment.Left
        Me.txtContactSurname.ToolTipText = ""
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(10, 58)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(47, 15)
        Me.CareLabel2.TabIndex = 2
        Me.CareLabel2.Text = "Surname"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtContactForename
        '
        Me.txtContactForename.CharacterCasing = CharacterCasing.Normal
        Me.txtContactForename.EnterMoveNextControl = True
        Me.txtContactForename.Location = New System.Drawing.Point(100, 29)
        Me.txtContactForename.MaxLength = 30
        Me.txtContactForename.Name = "txtContactForename"
        Me.txtContactForename.NumericAllowNegatives = False
        Me.txtContactForename.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtContactForename.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtContactForename.Properties.AccessibleName = "Contact Forename"
        Me.txtContactForename.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtContactForename.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtContactForename.Properties.Appearance.Options.UseFont = True
        Me.txtContactForename.Properties.Appearance.Options.UseTextOptions = True
        Me.txtContactForename.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtContactForename.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtContactForename.Properties.MaxLength = 30
        Me.txtContactForename.Size = New System.Drawing.Size(234, 22)
        Me.txtContactForename.TabIndex = 1
        Me.txtContactForename.Tag = "AEM"
        Me.txtContactForename.TextAlign = HorizontalAlignment.Left
        Me.txtContactForename.ToolTipText = ""
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.CareLabel9)
        Me.GroupControl2.Controls.Add(Me.CareLabel10)
        Me.GroupControl2.Controls.Add(Me.CareLabel11)
        Me.GroupControl2.Controls.Add(Me.cbxKeyworker)
        Me.GroupControl2.Controls.Add(Me.cdtStart)
        Me.GroupControl2.Controls.Add(Me.CareLabel12)
        Me.GroupControl2.Controls.Add(Me.cbxRoom)
        Me.GroupControl2.Controls.Add(Me.CareLabel13)
        Me.GroupControl2.Controls.Add(Me.cbxSite)
        Me.GroupControl2.Location = New System.Drawing.Point(362, 158)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(345, 171)
        Me.GroupControl2.TabIndex = 2
        Me.GroupControl2.Text = "Child Details"
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel9.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(210, 116)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(99, 15)
        Me.CareLabel9.TabIndex = 8
        Me.CareLabel9.Text = "1 year, 11 months"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel10
        '
        Me.CareLabel10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel10.Location = New System.Drawing.Point(10, 88)
        Me.CareLabel10.Name = "CareLabel10"
        Me.CareLabel10.Size = New System.Drawing.Size(55, 15)
        Me.CareLabel10.TabIndex = 4
        Me.CareLabel10.Text = "Keyworker"
        Me.CareLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(10, 116)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(51, 15)
        Me.CareLabel11.TabIndex = 6
        Me.CareLabel11.Text = "Start Date"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxKeyworker
        '
        Me.cbxKeyworker.AllowBlank = False
        Me.cbxKeyworker.DataSource = Nothing
        Me.cbxKeyworker.DisplayMember = Nothing
        Me.cbxKeyworker.EnterMoveNextControl = True
        Me.cbxKeyworker.Location = New System.Drawing.Point(100, 85)
        Me.cbxKeyworker.Name = "cbxKeyworker"
        Me.cbxKeyworker.Properties.AccessibleName = "Keyworker"
        Me.cbxKeyworker.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxKeyworker.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxKeyworker.Properties.Appearance.Options.UseFont = True
        Me.cbxKeyworker.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxKeyworker.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxKeyworker.SelectedValue = Nothing
        Me.cbxKeyworker.Size = New System.Drawing.Size(234, 22)
        Me.cbxKeyworker.TabIndex = 5
        Me.cbxKeyworker.Tag = "AEM"
        Me.cbxKeyworker.ValueMember = Nothing
        '
        'cdtStart
        '
        Me.cdtStart.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtStart.EnterMoveNextControl = True
        Me.cdtStart.Location = New System.Drawing.Point(100, 113)
        Me.cdtStart.Name = "cdtStart"
        Me.cdtStart.Properties.AccessibleName = "Start Date"
        Me.cdtStart.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtStart.Properties.Appearance.Options.UseFont = True
        Me.cdtStart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtStart.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtStart.Size = New System.Drawing.Size(104, 22)
        Me.cdtStart.TabIndex = 7
        Me.cdtStart.Tag = "AEM"
        Me.cdtStart.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'CareLabel12
        '
        Me.CareLabel12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel12.Location = New System.Drawing.Point(10, 60)
        Me.CareLabel12.Name = "CareLabel12"
        Me.CareLabel12.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel12.TabIndex = 2
        Me.CareLabel12.Text = "Room"
        Me.CareLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxRoom
        '
        Me.cbxRoom.AllowBlank = False
        Me.cbxRoom.DataSource = Nothing
        Me.cbxRoom.DisplayMember = Nothing
        Me.cbxRoom.EnterMoveNextControl = True
        Me.cbxRoom.Location = New System.Drawing.Point(100, 57)
        Me.cbxRoom.Name = "cbxRoom"
        Me.cbxRoom.Properties.AccessibleName = "Room"
        Me.cbxRoom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxRoom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxRoom.Properties.Appearance.Options.UseFont = True
        Me.cbxRoom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxRoom.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxRoom.SelectedValue = Nothing
        Me.cbxRoom.Size = New System.Drawing.Size(234, 22)
        Me.cbxRoom.TabIndex = 3
        Me.cbxRoom.Tag = "AEM"
        Me.cbxRoom.ValueMember = Nothing
        '
        'CareLabel13
        '
        Me.CareLabel13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel13.Location = New System.Drawing.Point(10, 32)
        Me.CareLabel13.Name = "CareLabel13"
        Me.CareLabel13.Size = New System.Drawing.Size(19, 15)
        Me.CareLabel13.TabIndex = 0
        Me.CareLabel13.Text = "Site"
        Me.CareLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(100, 29)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AccessibleName = "Site"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(234, 22)
        Me.cbxSite.TabIndex = 1
        Me.cbxSite.Tag = "AEM"
        Me.cbxSite.ValueMember = Nothing
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(531, 336)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(85, 23)
        Me.btnOK.TabIndex = 3
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.DialogResult = DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(622, 336)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 23)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "Cancel"
        '
        'frmLeadConvert
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(717, 367)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl6)
        Me.Controls.Add(Me.GroupControl1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLeadConvert"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "Convert Lead"
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl6.ResumeLayout(False)
        Me.GroupControl6.PerformLayout()
        CType(Me.cbxChildGender.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDOB.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDOB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtChildSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtChildForename.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.chkEmailReports.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkEmailInvoices.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtContactRelationship.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtContactSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtContactForename.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.cbxKeyworker.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxRoom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl6 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label12 As Care.Controls.CareLabel
    Friend WithEvents cbxChildGender As Care.Controls.CareComboBox
    Friend WithEvents lblAge As Care.Controls.CareLabel
    Friend WithEvents Label16 As Care.Controls.CareLabel
    Private WithEvents cdtDOB As Care.Controls.CareDateTime
    Friend WithEvents txtChildSurname As Care.Controls.CareTextBox
    Friend WithEvents CareLabel23 As Care.Controls.CareLabel
    Friend WithEvents txtChildForename As Care.Controls.CareTextBox
    Friend WithEvents CareLabel24 As Care.Controls.CareLabel
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtTel As Care.Shared.CareTelephoneNumber
    Friend WithEvents txtMobile As Care.Shared.CareTelephoneNumber
    Friend WithEvents txtEmail As Care.Shared.CareEmailAddress
    Friend WithEvents Label13 As Care.Controls.CareLabel
    Friend WithEvents Label8 As Care.Controls.CareLabel
    Friend WithEvents Label9 As Care.Controls.CareLabel
    Friend WithEvents txtAddressFull As Care.Address.CareAddress
    Friend WithEvents Label2 As Care.Controls.CareLabel
    Friend WithEvents txtContactRelationship As Care.Controls.CareTextBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents txtContactSurname As Care.Controls.CareTextBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents txtContactForename As Care.Controls.CareTextBox
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents chkEmailInvoices As Care.Controls.CareCheckBox
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents CareLabel10 As Care.Controls.CareLabel
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents cbxKeyworker As Care.Controls.CareComboBox
    Private WithEvents cdtStart As Care.Controls.CareDateTime
    Friend WithEvents CareLabel12 As Care.Controls.CareLabel
    Friend WithEvents cbxRoom As Care.Controls.CareComboBox
    Friend WithEvents CareLabel13 As Care.Controls.CareLabel
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents chkEmailReports As Care.Controls.CareCheckBox
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents btnCancel As Care.Controls.CareButton

End Class
