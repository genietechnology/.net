﻿Imports Care.Global
Imports Care.Data

Public Class frmTimeSlots

    Dim m_TimeSlot As Business.TimeSlot

    Private Sub frmTimeSlots_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.GridSQL = "select id, name as 'Name', short_name as 'Short Name', start_time as 'Start Time', end_time as 'End Time'," &
                     " headcount as 'In Headcounts', am as 'AM', pm as 'PM', breakfast as 'BRK', lunch as 'LUN', tea as 'TEA', snack as 'SNK'" &
                     " from TimeSlots" &
                     " order by start_time"

        gbx.Hide()

    End Sub

    Protected Overrides Sub SetBindings()

        m_TimeSlot = New Business.TimeSlot
        bs.DataSource = m_TimeSlot

    End Sub

    Protected Overrides Sub BindToID(ID As System.Guid, IsNew As Boolean)

        If IsNew Then
            m_TimeSlot = New Business.TimeSlot
        Else
            m_TimeSlot = Business.TimeSlot.RetreiveByID(ID)
        End If

        bs.DataSource = m_TimeSlot

        txtName.Text = m_TimeSlot._Name
        txtShortName.Text = m_TimeSlot._ShortName
        txtStart.EditValue = m_TimeSlot._StartTime
        txtEnd.EditValue = m_TimeSlot._EndTime
        chkHeadcount.Checked = m_TimeSlot._Headcount
        chkAM.Checked = m_TimeSlot._Am
        chkPM.Checked = m_TimeSlot._Pm

        chkBreakfast.Checked = m_TimeSlot._Breakfast
        chkLunch.Checked = m_TimeSlot._Lunch
        chkTea.Checked = m_TimeSlot._Tea
        chkSnack.Checked = m_TimeSlot._Snack

    End Sub

    Protected Overrides Sub CommitUpdate()

        m_TimeSlot._Name = txtName.Text
        m_TimeSlot._ShortName = txtShortName.Text

        If txtStart.Text = "" Then
            m_TimeSlot._StartTime = Nothing
        Else
            m_TimeSlot._StartTime = TimeSpan.Parse(txtStart.EditValue.ToString)
        End If

        If txtEnd.Text = "" Then
            m_TimeSlot._EndTime = Nothing
        Else
            m_TimeSlot._EndTime = TimeSpan.Parse(txtEnd.EditValue.ToString)
        End If

        m_TimeSlot._Headcount = chkHeadcount.Checked
        m_TimeSlot._Am = chkAM.Checked
        m_TimeSlot._Pm = chkPM.Checked

        m_TimeSlot._Breakfast = chkBreakfast.Checked
        m_TimeSlot._Lunch = chkLunch.Checked
        m_TimeSlot._Tea = chkTea.Checked
        m_TimeSlot._Snack = chkSnack.Checked

        m_TimeSlot.Store()

    End Sub

    Protected Overrides Sub CommitDelete(ByVal ID As Guid)

        Dim _SQL As String = "delete from TimeSlots where ID = '" + ID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

    End Sub

    Private Sub btn30_Click(sender As Object, e As EventArgs) Handles btn30.Click

        btn30.Enabled = False
        Session.CursorWaiting()

        ClearSlots()

        CreateSlot(7, 1, 29)
        CreateSlot(7, 31, 59)
        CreateSlot(8, 1, 29)
        CreateSlot(8, 31, 59)
        CreateSlot(9, 1, 29)
        CreateSlot(9, 31, 59)
        CreateSlot(10, 1, 29)
        CreateSlot(10, 31, 59)
        CreateSlot(11, 1, 29)
        CreateSlot(11, 31, 59)
        CreateSlot(12, 1, 29)
        CreateSlot(12, 31, 59)
        CreateSlot(13, 1, 29)
        CreateSlot(13, 31, 59)
        CreateSlot(14, 1, 29)
        CreateSlot(14, 31, 59)
        CreateSlot(15, 1, 29)
        CreateSlot(15, 31, 59)
        CreateSlot(16, 1, 29)
        CreateSlot(16, 31, 59)
        CreateSlot(17, 1, 29)
        CreateSlot(17, 31, 59)
        CreateSlot(18, 1, 29)
        CreateSlot(18, 31, 59)

        Me.SetGrid()

        btn30.Enabled = True
        Session.CursorDefault()

    End Sub

    Private Function HHMM(ByVal TimeSpanIn As TimeSpan) As String
        Return Format(TimeSpanIn.Hours, "00") + ":" + Format(TimeSpanIn.Minutes, "00")
    End Function

    Private Sub CreateSlot(ByVal Hour As Integer, ByVal StartMin As Integer, ByVal EndMin As Integer)

        Dim _From As New TimeSpan(Hour, StartMin, 0)
        Dim _To As New TimeSpan(Hour, EndMin, 0)

        Dim _Name As String = HHMM(_From) + " - " + HHMM(_To)

        Dim _TS As New Business.TimeSlot
        With _TS

            ._Name = _Name
            ._ShortName = HHMM(_From)
            ._StartTime = _From
            ._EndTime = _To
            ._Headcount = True

            If Hour < 12 Then
                ._Am = True
                ._Pm = False
            Else
                ._Am = False
                ._Pm = True
            End If

            ._Breakfast = False
            ._Lunch = False
            ._Tea = False
            ._Snack = False

            .Store()

        End With

    End Sub

    Private Sub btn60_Click(sender As Object, e As EventArgs) Handles btn60.Click

        btn60.Enabled = False
        Session.CursorWaiting()

        ClearSlots()

        CreateSlot(7, 1, 59)
        CreateSlot(8, 1, 59)
        CreateSlot(9, 1, 59)
        CreateSlot(10, 1, 59)
        CreateSlot(11, 1, 59)
        CreateSlot(12, 1, 59)
        CreateSlot(13, 1, 59)
        CreateSlot(14, 1, 59)
        CreateSlot(15, 1, 59)
        CreateSlot(16, 1, 59)
        CreateSlot(17, 1, 59)
        CreateSlot(18, 1, 59)

        Me.SetGrid()

        btn60.Enabled = True
        Session.CursorDefault()

    End Sub

    Private Sub ClearSlots()
        Dim _SQL As String = "delete from Timeslots"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)
    End Sub

End Class