﻿Imports Care.Global
Imports Care.Data

Public Class frmTariffPopup

    Private m_Tariff As Business.Tariff
    Private m_SiteID As Guid
    Private m_SiteName As String = ""
    Private m_New As Boolean = False

    Public Sub New(ByVal SiteID As Guid, ByVal SiteName As String, ByVal TariffID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_SiteID = SiteID
        m_SiteName = SiteName

        If TariffID.HasValue Then
            m_Tariff = Business.Tariff.RetreiveByID(TariffID.Value)
            Me.Text = "Edit Tariff: " + m_Tariff._Name + " for " + SiteName
        Else
            m_New = True
            m_Tariff = New Business.Tariff
            Me.Text = "Create New Tariff for " + SiteName
        End If

    End Sub

    Private Sub frmTariffPopup_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        If m_New Then
            MyControls.SetControls(Care.Shared.ControlHandler.Mode.Add, Me.Controls)
        Else
            MyControls.SetControls(Care.Shared.ControlHandler.Mode.Edit, Me.Controls)
            DisplayRecord()
        End If
    End Sub

    Private Sub frmTariff_Load(sender As Object, e As EventArgs) Handles Me.Load

        lblMinutes.Hide()
        lblMinutes.Text = ""

        lblPerHour.Hide()
        lblPerHour.Text = ""

        btnMatrix.Enabled = False
        btnVariable.Enabled = False

        With cbxType
            .AddItem("Automatic")
            .AddItem("Complimentary")
            .AddItem("Age Matrix")
            .AddItem("Age Matrix (Rate)")
            .AddItem("Funded")
            .AddItem("Fixed Price")
            .AddItem("Duration (Hours)")
            .AddItem("Manual Times")
            .AddItem("Time Matrix")
            .AddItem("Month Matrix")
            .AddItem("Group Matrix")
            .AddItem("Age Matrix (Times)")
        End With

        cbxNLCode.AllowBlank = True
        cbxNLCode.PopulateFromListMaintenance(Care.Shared.ListHandler.ReturnListFromCache("NL Codes"))

        cbxNLTrack.AllowBlank = True
        cbxNLTrack.PopulateFromListMaintenance(Care.Shared.ListHandler.ReturnListFromCache("NL Tracking"))

    End Sub

    'Protected Overrides Function BeforeCommitUpdate() As Boolean


    'End Function

    Private Sub DisplayRecord()

        txtName.Text = m_Tariff._Name
        txtInvoiceText.Text = m_Tariff._InvoiceText
        cbxType.Text = m_Tariff._Type
        cbxSummaryColumn.Text = m_Tariff._SummaryColumn

        ceColour.Text = m_Tariff._Colour

        '**********************************************************************

        chkWeekly.Checked = m_Tariff._Weekly
        chkDaily.Checked = m_Tariff._Daily
        chkOverride.Checked = m_Tariff._Override
        chkBoltOns.Checked = m_Tariff._BoltOns
        chkTimesVsDuration.Checked = m_Tariff._ValidateTimesDur
        chkDiscount.Checked = m_Tariff._Discount

        '**********************************************************************

        txtDuration.Text = ValueHandler.MoneyAsText(m_Tariff._Duration)
        txtRate.Text = ValueHandler.MoneyAsText(m_Tariff._Rate)
        chkVariable.Checked = m_Tariff._VariableRate
        btnVariable.Enabled = m_Tariff._VariableRate

        '**********************************************************************

        If m_Tariff._Holiday Then
            chkHoliday.Enabled = False
            PopulateTariffCombo()
            chkHoliday.Checked = True
            cbxTariff.SelectedValue = m_Tariff._HolidayTariff.Value.ToString
        Else
            chkHoliday.Checked = False
            cbxTariff.SelectedIndex = -1
        End If

        '**********************************************************************
        cbxNLCode.Text = m_Tariff._NlCode
        cbxNLTrack.Text = m_Tariff._NlTracking
        '**********************************************************************

        If m_Tariff._FundingHours Then
            chkFundingHours.Checked = True
            txtRateFund24.EditValue = m_Tariff._RateFund24
            txtRateFund36.EditValue = m_Tariff._RateFund36
        Else
            chkFundingHours.Checked = False
            txtRateFund24.Text = ""
            txtRateFund36.Text = ""
        End If

        SetFundingControls()

        '**********************************************************************
        txtStartTime.EditValue = m_Tariff._StartTime
        txtEndTime.EditValue = m_Tariff._EndTime
        txtVoidStart.EditValue = m_Tariff._VoidStart
        txtVoidEnd.EditValue = m_Tariff._VoidEnd
        '**********************************************************************

        If m_Tariff._SessHol Then
            chkSessHol.Checked = True
            txtSessHolDiscount.EditValue = m_Tariff._SessHolDiscount
        Else
            chkSessHol.Checked = False
            txtSessHolDiscount.Text = ""
        End If

        '**********************************************************************

        CalculateMinutes()

        CalculatePerHour()

    End Sub

    Private Sub CommitUpdate()

        '*****************************************************************************************

        m_Tariff._SiteId = m_SiteID
        m_Tariff._SiteName = m_SiteName
        m_Tariff._Archived = False

        '*****************************************************************************************

        m_Tariff._Name = txtName.Text
        m_Tariff._InvoiceText = txtInvoiceText.Text
        m_Tariff._Type = cbxType.Text
        m_Tariff._SummaryColumn = cbxSummaryColumn.Text

        If ceColour.Text = "" Then
            m_Tariff._Colour = "White"
        Else
            m_Tariff._Colour = ceColour.Text
        End If

        '*****************************************************************************************

        m_Tariff._Weekly = chkWeekly.Checked
        m_Tariff._Daily = chkDaily.Checked
        m_Tariff._BoltOns = chkBoltOns.Checked
        m_Tariff._ValidateTimesDur = chkTimesVsDuration.Checked
        m_Tariff._Discount = chkDiscount.Checked

        '*****************************************************************************************

        'handle override
        Select Case m_Tariff._Type

            Case "Manual Times", "Time Matrix", "Age Matrix (Times)"
                m_Tariff._Override = True

            Case "Fixed Price", "Duration (Hours)", "Age Matrix (Rate)", "Group Matrix"
                m_Tariff._Override = chkOverride.Checked

            Case Else
                m_Tariff._Override = False

        End Select

        '*****************************************************************************************

        m_Tariff._Duration = ValueHandler.ConvertDecimal(txtDuration.Text)
        m_Tariff._Rate = ValueHandler.ConvertDecimal(txtRate.Text)
        m_Tariff._VariableRate = chkVariable.Checked

        '*****************************************************************************************

        'school holidays
        If chkHoliday.Checked Then
            m_Tariff._Holiday = True
            m_Tariff._HolidayTariff = New Guid(cbxTariff.SelectedValue.ToString)
        Else
            m_Tariff._Holiday = False
            m_Tariff._HolidayTariff = Nothing
        End If

        '*****************************************************************************************

        m_Tariff._NlCode = cbxNLCode.Text
        m_Tariff._NlTracking = cbxNLTrack.Text

        '*****************************************************************************************

        'handle specify funding
        Select Case m_Tariff._Type

            Case "Fixed Price", "Duration (Hours)", "Manual Times", "Age Matrix", "Time Matrix", "Group Matrix", "Age Matrix (Times)"
                m_Tariff._FundingHours = chkFundingHours.Checked

            Case Else
                m_Tariff._FundingHours = False

        End Select

        If m_Tariff._FundingHours Then
            m_Tariff._RateFund24 = CleanData.ReturnDecimal(txtRateFund24.EditValue)
            m_Tariff._RateFund36 = CleanData.ReturnDecimal(txtRateFund36.EditValue)
        Else
            m_Tariff._RateFund24 = 0
            m_Tariff._RateFund36 = 0
        End If

        '****************************************************************************************

        m_Tariff._StartTime = CleanData.ReturnTimeSpan(txtStartTime.EditValue)
        m_Tariff._EndTime = CleanData.ReturnTimeSpan(txtEndTime.EditValue)
        m_Tariff._VoidStart = CleanData.ReturnTimeSpan(txtVoidStart.EditValue)
        m_Tariff._VoidEnd = CleanData.ReturnTimeSpan(txtVoidEnd.EditValue)

        '*****************************************************************************************

        If chkSessHol.Checked Then
            m_Tariff._SessHol = True
            m_Tariff._SessHolDiscount = CleanData.ReturnDecimal(txtSessHolDiscount.EditValue)
        Else
            m_Tariff._SessHol = False
            m_Tariff._SessHolDiscount = 0
        End If

        '*****************************************************************************************

        'avg fields no longer used
        m_Tariff._AvgDays = 0
        m_Tariff._AvgWeeks = 0
        m_Tariff._Avg = 0

        '*****************************************************************************************

        m_Tariff.Store()

        Me.DialogResult = DialogResult.OK
        Me.Close()

    End Sub

    Private Sub PopulateTariffCombo()

        Dim _SQL As String = ""
        _SQL += "select id, name from Tariffs"
        _SQL += " where id <> '" + m_Tariff._ID.ToString + "'"
        _SQL += " and site_id = '" + m_SiteID.ToString + "'"
        _SQL += " and type = '" + cbxType.Text + "'"
        _SQL += " order by name"

        cbxTariff.Clear()
        cbxTariff.PopulateWithSQL(Session.ConnectionString, _SQL)

    End Sub

    Private Sub CalculatePerHour()

        If txtRate.Text = "" Then
            lblPerHour.Hide()
            Exit Sub
        End If

        Dim _PerHour As Double = 0

        Try
            _PerHour = CDbl(txtRate.Text) / (CDbl(lblMinutes.Tag) / 60)

        Catch ex As Exception
            _PerHour = 0
        End Try

        If _PerHour = 0 Then
            lblPerHour.Hide()
        Else
            lblPerHour.Text = "£" + Format(_PerHour, "0.00").ToString + " per hour"
            lblPerHour.Show()
        End If

    End Sub

    Private Sub cbxType_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbxType.SelectedValueChanged
        SetCalculationTypeFields()
    End Sub

    Private Sub SetCalculationTypeFields()

        If Not cbxType.ReadOnly Then

            lblRate.Show()
            txtRate.Show()

            Select Case cbxType.Text

                Case "Fixed Price"
                    lblRate.Text = "Price"

                Case "Funded", "Complimentary", "Age Matrix", "Age Matrix (Rate)", "Time Matrix", "Month Matrix", "Group Matrix", "Age Matrix (Times)"
                    lblRate.Hide()
                    txtRate.Hide()
                    lblPerHour.Hide()

                Case Else
                    lblRate.Text = "Rate per Hour"

            End Select

            If cbxType.Text.Contains("Matrix") Then
                btnMatrix.Enabled = True
            Else
                btnMatrix.Enabled = False
            End If

        End If

    End Sub

    Private Sub txtDuration_Validated(sender As Object, e As EventArgs) Handles txtDuration.Validated
        CalculateMinutes()
    End Sub

    Private Sub txtRate_Validated(sender As Object, e As EventArgs) Handles txtRate.Validated
        CalculatePerHour()
    End Sub

    Private Sub CalculateMinutes()

        If txtDuration.Text <> "" Then

            Dim _Mins As Double = 0

            Try
                _Mins = CDbl(txtDuration.Text) * 60

            Catch ex As Exception
                _Mins = 0
            End Try

            lblMinutes.Text = _Mins.ToString + " minutes"
            lblMinutes.Tag = _Mins

        End If

    End Sub

    Private Sub btnMatrix_Click(sender As Object, e As EventArgs) Handles btnMatrix.Click

        Dim _XMode As frmTariffMatrix.EnumXMode = frmTariffMatrix.EnumXMode.Age
        Dim _Mode As frmTariffMatrix.EnumValueMode

        Select Case cbxType.Text

            Case "Group Matrix"
                _Mode = frmTariffMatrix.EnumValueMode.Price
                _XMode = frmTariffMatrix.EnumXMode.Group

            Case "Age Matrix", "Age Matrix (Times)"
                _Mode = frmTariffMatrix.EnumValueMode.Price

            Case "Age Matrix (Rate)", "Time Matrix"
                _Mode = frmTariffMatrix.EnumValueMode.Rate

            Case "Month Matrix"
                MonthMatrix()
                Exit Sub

        End Select

        Dim _frm As New frmTariffMatrix(m_Tariff._ID.Value, txtName.Text, m_SiteID, _Mode, _XMode)
        _frm.ShowDialog()

        _frm.Dispose()
        _frm = Nothing

    End Sub

    Private Sub MonthMatrix()

        Dim _frm As New frmTariffMonthMatrix(m_Tariff._ID.Value, txtName.Text)
        _frm.ShowDialog()

        _frm.Dispose()
        _frm = Nothing

    End Sub

    Private Function ValidateTimes() As Boolean

        'return is hooked to e.cancel, so false is actually good!

        Dim _from As TimeSpan? = ConverttoTimeSpan(txtStartTime.Text)
        Dim _to As TimeSpan? = ConverttoTimeSpan(txtEndTime.Text)

        If _from.HasValue AndAlso _to.HasValue Then
            If _from.Value >= _to.Value Then
                txtStartTime.BackColor = Drawing.Color.LightCoral
                txtEndTime.BackColor = Drawing.Color.LightCoral
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If

    End Function

    Private Function ConverttoTimeSpan(ByVal TextIn As Object) As TimeSpan?

        If TextIn Is Nothing Then Return Nothing
        If TextIn.ToString = "" Then Return Nothing

        Try
            Return TimeSpan.Parse(TextIn.ToString)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Private Sub txtStartTime_InvalidValue(sender As Object, e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles txtStartTime.InvalidValue
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub

    Private Sub txtStartTime_Validating(sender As Object, e As ComponentModel.CancelEventArgs) Handles txtStartTime.Validating
        e.Cancel = ValidateTimes()
    End Sub

    Private Sub txtEndTime_InvalidValue(sender As Object, e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles txtEndTime.InvalidValue
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub

    Private Sub txtEndTime_Validating(sender As Object, e As ComponentModel.CancelEventArgs) Handles txtEndTime.Validating
        e.Cancel = ValidateTimes()
    End Sub

    Private Sub chkHoliday_CheckedChanged(sender As Object, e As EventArgs) Handles chkHoliday.CheckedChanged
        If chkHoliday.Enabled Then
            If chkHoliday.Checked Then
                PopulateTariffCombo()
                cbxTariff.ReadOnly = False
                cbxTariff.BackColor = Session.ChangeColour
            Else
                cbxTariff.SelectedIndex = -1
                cbxTariff.ReadOnly = True
                cbxTariff.ResetBackColor()
            End If
        End If
    End Sub

    Private Sub btnBreakdown_Click(sender As Object, e As EventArgs) Handles btnBreakdown.Click
        'Dim _frm As New frmTariffUsage(m_Tariff)
        '_frm.ShowDialog()
        '_frm.Dispose()
        '_frm = Nothing
    End Sub

    Private Sub chkFundingHours_CheckedChanged(sender As Object, e As EventArgs) Handles chkFundingHours.CheckedChanged
        SetFundingControls()
    End Sub

    Private Sub SetFundingControls()

        If chkFundingHours.Enabled Then

            txtRateFund24.ReadOnly = Not chkFundingHours.Checked
            txtRateFund36.ReadOnly = Not chkFundingHours.Checked

            If chkFundingHours.Checked Then
                txtRateFund24.BackColor = Session.ChangeColour
                txtRateFund36.BackColor = Session.ChangeColour
            Else
                txtRateFund24.Text = ""
                txtRateFund24.ResetBackColor()
                txtRateFund36.Text = ""
                txtRateFund36.ResetBackColor()
            End If

        End If

    End Sub

    Private Sub chkSessHol_CheckedChanged(sender As Object, e As EventArgs) Handles chkSessHol.CheckedChanged

        If chkSessHol.Enabled Then

            txtSessHolDiscount.ReadOnly = Not chkFundingHours.Checked

            If chkSessHol.Checked Then
                txtSessHolDiscount.BackColor = Session.ChangeColour
            Else
                txtSessHolDiscount.Text = ""
                txtSessHolDiscount.ResetBackColor()
            End If

        End If
    End Sub

    Private Sub btnVariable_Click(sender As Object, e As EventArgs) Handles btnVariable.Click

        Dim _frm As New frmTariffVariable(m_Tariff._ID.Value, m_Tariff._Name)
        _frm.ShowDialog()

        _frm.Dispose()
        _frm = Nothing

    End Sub

    Private Sub chkVariable_CheckedChanged(sender As Object, e As EventArgs) Handles chkVariable.CheckedChanged
        If chkVariable.Enabled Then
            btnVariable.Enabled = chkVariable.Checked
        End If
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        If ValidateEntry Then
            CommitUpdate()
        End If
    End Sub

    Private Function ValidateEntry() As Boolean

        If Not MyControls.Validate(Me.Controls) Then Return False

        If chkDaily.Checked = False AndAlso chkWeekly.Checked = False Then
            CareMessage("Please select whether this tariff is single-session, multi-session or both.", MessageBoxIcon.Exclamation, "Tariff Check")
            Return False
        End If

        If ValueHandler.ConvertDecimal(txtDuration.Text) < 1 Then
            CareMessage("Please ensure the Duration entered is greater than zero.", MessageBoxIcon.Exclamation, "Tariff Check")
            Return False
        End If

        Return True

    End Function

    Private Sub cbxType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxType.SelectedIndexChanged

        If cbxType.Text = "Funded" Then
            cbxSummaryColumn.Clear()
            cbxSummaryColumn.AddItem("0")
            cbxSummaryColumn.SelectedIndex = 0
        Else
            cbxSummaryColumn.Clear()
            cbxSummaryColumn.AddItem("1")
            cbxSummaryColumn.AddItem("2")
            cbxSummaryColumn.AddItem("3")
            cbxSummaryColumn.AddItem("4")
            cbxSummaryColumn.AddItem("5")
            cbxSummaryColumn.AddItem("6")
            cbxSummaryColumn.AddItem("7")
            cbxSummaryColumn.AddItem("8")
            cbxSummaryColumn.AddItem("9")
            cbxSummaryColumn.SelectedIndex = -1
        End If

    End Sub
End Class