﻿

Imports System.Text
Imports System.Net
Imports System.IO
Imports System.Xml
Imports System.Reflection
Imports System.Windows.Forms

Imports Care.Shared
Imports Care.Global
Imports Care.Data

Public Class frmWUFOO

    Private m_Form As Business.WFForm

    Private m_Entries As New List(Of WuFooEntry)

    Private m_ChildProperties As New List(Of Field)
    Private m_ContactProperties As New List(Of Field)
    Private m_StaffProperties As New List(Of Field)
    Private m_LeadProperties As New List(Of Field)

    Private m_SubDomain As String = ""
    Private m_Key As String = ""
    Private m_Password As String = ""
    Private m_ConfigOK As Boolean = False

    Private Enum EnumObject
        Mother
        Father
        EmergencyContact1
        EmergencyContact2
        PrimaryContact
        OtherContact
        Child
        AllergyItem
        ConsentItem
        ImmunisationItem
        Staff
        Lead
    End Enum

    Private Sub frmWUFOO_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim _Contact As New Business.Contact
        Dim _Child As New Business.Child
        Dim _Staff As New Business.Staff
        Dim _Lead As New Business.Lead

        m_ContactProperties = _Contact.FieldList(DataObjectBase.EnumNameFormat.PropertyExact)
        m_ChildProperties = _Child.FieldList(DataObjectBase.EnumNameFormat.PropertyExact)
        m_StaffProperties = _Staff.FieldList(DataObjectBase.EnumNameFormat.PropertyExact)
        m_LeadProperties = _Lead.FieldList(DataObjectBase.EnumNameFormat.PropertyExact)

        cbxMode.AddItem("Import Family")
        cbxMode.AddItem("Import Staff")
        cbxMode.AddItem("Import Lead")

        cbxSites.PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)

        gbxField.Hide()

        m_SubDomain = ParameterHandler.ReturnString("WUFOODOMAIN")
        m_Key = ParameterHandler.ReturnString("WUFOOKEY")
        m_Password = ParameterHandler.ReturnString("WUFOOPASSWORD")

        If m_SubDomain <> "" AndAlso m_Key <> "" AndAlso m_Password <> "" Then
            m_ConfigOK = True
        Else
            CareMessage("WUFOO Parameters have not been defined. Connection to WUFOO disabled.", MessageBoxIcon.Warning, "Connection Check")
        End If

        SetCMDs(False)

    End Sub

    Private Sub SetCMDs(ByVal Enabled As Boolean)

        Dim _MappingDefined As Boolean = False

        If Enabled Then
            If m_ConfigOK Then _MappingDefined = IsMappingDefined()
            btnFormFields.Enabled = m_ConfigOK
        Else
            btnFormFields.Enabled = False
        End If

        btnFetchNew.Enabled = _MappingDefined

        If _MappingDefined Then
            If cgEntries.RecordCount > 0 Then
                btnImportEntry.Enabled = True
            Else
                btnImportEntry.Enabled = False
            End If
        Else
            btnImportEntry.Enabled = False
        End If

        btnClearMapping.Enabled = _MappingDefined

    End Sub

    Private Function IsMappingDefined() As Boolean
        Dim _SQL As String = "select count(*) from WFMapping where form_id = '" + m_Form._ID.Value.ToString + "' and value_object <> ''"
        Dim _Count As Integer = ValueHandler.ConvertInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))
        If _Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub DisplayRecord(ByVal ID As Guid)

        MyBase.RecordID = ID
        MyBase.RecordPopulated = True

        m_Form = Business.WFForm.RetreiveByID(ID)
        bs.DataSource = m_Form

        DisplayMappingGrid()

    End Sub

    Private Sub DisplayMappingGrid()

        Dim _SQL As String = ""
        Dim _ParentSubQuery As String = "(select form_label from WFMapping p where p.form_id = WFMapping.form_id and p.form_field = WFMapping.parent_field and p.parent_field = '')"

        _SQL += "select ID, parent_field as 'Parent Field',"
        _SQL += " " + _ParentSubQuery + " as 'Parent Label',"
        _SQL += " form_field as 'Field', form_label as 'Label', value_object as 'Object', value_property as 'Property', value_override as 'Override'"
        _SQL += " from WFMapping"
        _SQL += " where form_id = '" + MyBase.RecordID.Value.ToString + "'"
        _SQL += " order by "
        _SQL += _ParentSubQuery
        _SQL += ", form_label, form_field"

        cgFields.HideFirstColumn = True
        cgFields.Populate(Session.ConnectionString, _SQL)

        cgFields.AutoSizeColumns()

        SetCMDs(True)

    End Sub

    Protected Overrides Function BeforeDelete() As Boolean
        Me.DeleteMessage = "Are you sure you wish to delete this WUFOO Form and all associated Mapping?"
        Return ParameterHandler.ManagerAuthorised
    End Function

    Protected Overrides Sub AfterEdit()
        txtName.Focus()
    End Sub

    Protected Overrides Sub AfterAdd()
        txtName.Focus()
    End Sub

    Protected Overrides Sub CommitDelete()

        If m_Form IsNot Nothing AndAlso m_Form._ID.HasValue Then

            Dim _SQL As String = "delete from WFMapping where form_id = '" + m_Form._ID.ToString() + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

            Business.WFForm.DeleteRecord(m_Form._ID.Value)

        End If

    End Sub

    Protected Overrides Sub CommitUpdate()

        Dim _Form As Business.WFForm = CType(bs.Item(bs.Position), Business.WFForm)

        If cbxSites.Text = "" Then
            _Form._SiteId = Nothing
        Else
            _Form._SiteId = New Guid(cbxSites.SelectedValue.ToString)
        End If

        _Form.Store()

    End Sub

    Protected Overrides Sub FindRecord()

        Dim _ReturnValue As String = ""
        Dim _Find As New GenericFind
        With _Find
            .Caption = "Find Form"
            .ParentForm = Me
            .PopulateOnLoad = True
            .ConnectionString = Session.ConnectionString
            .GridSQL = "select name as 'Name', mode as 'Mode' from WFForm"
            .GridOrderBy = "order by name"
            .ReturnField = "ID"
            .Show()
            _ReturnValue = .ReturnValue
        End With

        If _ReturnValue <> "" Then
            DisplayRecord(New Guid(_ReturnValue))
        End If

    End Sub

    Protected Overrides Sub SetBindings()

        m_Form = New Business.WFForm
        bs.DataSource = m_Form

        txtName.DataBindings.Add("Text", bs, "_Name")
        txtFormName.DataBindings.Add("Text", bs, "_FormName")
        cbxMode.DataBindings.Add("Text", bs, "_Mode")
        txtLastEntry.DataBindings.Add("Text", bs, "_LastEntry")
        cbxSites.DataBindings.Add("Text", bs, "_SiteName")

    End Sub

    Private Sub ImportFields()

        Dim _Fields As New List(Of WuFooField)
        Dim _Text As String = ""

        If SetXMLResponse("fields.xml", _Text) = 0 Then

            Dim _xml As New XmlDocument
            _xml.LoadXml(_Text)

            Dim _XEntries As XmlNodeList = _xml.DocumentElement.ChildNodes
            For Each _XEntry As XmlNode In _XEntries

                Dim _Field As New WuFooField
                Dim _Elements As XmlNodeList = _XEntry.ChildNodes

                For Each _Element As XmlNode In _Elements

                    If _Element.Name = "Title" Then _Field.Title = _Element.InnerText
                    If _Element.Name = "Type" Then _Field.Type = _Element.InnerText
                    If _Element.Name = "ID" Then _Field.ID = _Element.InnerText
                    If _Element.Name = "IsRequired" Then _Field.IsRequired = ConvertToBoolean(_Element.InnerText)
                    If _Element.Name = "HasOtherField" Then _Field.HasOtherField = ConvertToBoolean(_Element.InnerText)

                    If _Element.Name = "SubFields" Then

                        For Each _e As XmlNode In _Element.ChildNodes
                            If _e.Name = "Subfield" Then

                                Dim _ID As String = ""
                                Dim _Label As String = ""

                                For Each _sf As XmlNode In _e.ChildNodes
                                    If _sf.Name = "ID" Then _ID = _sf.InnerText
                                    If _sf.Name = "Label" Then _Label = _sf.InnerText
                                Next

                                _Field.SubFields.Add(New WuFooSubField(_ID, _Label))

                            End If
                        Next

                    End If

                    If _Element.Name = "Choices" Then
                        For Each _e As XmlNode In _Element.ChildNodes
                            If _e.Name = "Choice" Then
                                For Each _sf As XmlNode In _e.ChildNodes
                                    _Field.Choices.Add(New WuFooChoice(_sf.InnerText))
                                Next
                            End If
                        Next
                    End If

                Next

                _Fields.Add(_Field)

            Next

            For Each _f As WuFooField In _Fields

                'check if there is an existing mapping
                Dim _m As New Business.WFMapping

                If Not ExistingMapping(_m, _f) Then

                    With _m

                        ._FormId = Me.RecordID
                        ._FormField = _f.ID
                        ._FormLabel = _f.Title

                        _m.Store()

                        For Each _sf In _f.SubFields
                            Dim _sfm As New Business.WFMapping
                            With _sfm
                                ._ParentField = _m._FormField
                                ._FormId = Me.RecordID
                                ._FormField = _sf.ID
                                ._FormLabel = _sf.Label
                                .Store()
                            End With
                        Next

                    End With

                End If

            Next

        Else
            CareMessage("Unable to Download Fields from WOFOO. Check the Domain and Form Name is correct.", MessageBoxIcon.Exclamation, "Download Fields")
        End If

    End Sub

    Private Function ExistingMapping(ByRef MappingRecord As Business.WFMapping, ByRef Field As WuFooField) As Boolean

        Dim _Return As Boolean = False
        Dim _SQL As String = "select * from WFMapping where form_id = '" + m_Form._ID.Value.ToString + "' and form_field = '" + Field.ID + "'"
        Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DR IsNot Nothing Then
            _Return = True
            _DR = Nothing
        End If

        Return _Return

    End Function

    Private Function SetXMLResponse(ByVal XMLFile As String, ByRef XMLString As String, Optional ByVal Filter As String = "") As Integer

        'Code to force TLS 1.2
        ServicePointManager.Expect100Continue = True
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12

        Dim _url As New StringBuilder
        _url.Append("https://" + m_SubDomain + "/api/v3/forms/" + txtFormName.Text + "/" + XMLFile + Filter)

        Dim _request As HttpWebRequest = CType(HttpWebRequest.Create(_url.ToString), HttpWebRequest)

        Dim _authInfo As String = m_Key + ":" + m_Password
        _authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(_authInfo))

        _request.Headers("Authorization") = "Basic " + _authInfo

        Try

            Using _webresponse As HttpWebResponse = CType(_request.GetResponse, HttpWebResponse)
                Using _reader As New StreamReader(_webresponse.GetResponseStream)
                    XMLString = _reader.ReadToEnd
                End Using
            End Using

        Catch ex As Exception
            ErrorHandler.LogExceptionToDatabase(ex, False)
            If ex.Message.Contains("401") Then
                Return 401
            Else
                Return 99
            End If
        End Try

        Return 0

    End Function

    Private Function ConvertToBoolean(ByVal StringIn As String) As Boolean
        If StringIn = "1" Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub GetEntries()

        m_Entries.Clear()

        Dim _Text As String = ""

        If m_Form._LastEntry.ToString = "" Then m_Form._LastEntry = 0

        Dim _Filter As String = "?Filter1=EntryId+Is_greater_than+" + m_Form._LastEntry.ToString + "&pageSize=100"
        Dim _Response As Integer = SetXMLResponse("entries.xml", _Text, _Filter)
        If _Response = 0 Then

            Dim _xml As New XmlDocument
            _xml.LoadXml(_Text)

            Dim _XEntries As XmlNodeList = _xml.DocumentElement.ChildNodes
            For Each _XEntry As XmlNode In _XEntries

                Dim _Entry As New WuFooEntry
                _Entry.EntryID = _XEntry.FirstChild.InnerText

                Dim _Elements As XmlNodeList = _XEntry.ChildNodes
                For Each _Element As XmlNode In _Elements
                    Dim _Field As New WuFooEntryField(_Element.Name, _Element.InnerText)
                    _Entry.Fields.Add(_Field)
                Next

                m_Entries.Add(_Entry)

            Next

        Else
            If _Response = 401 Then
                CareMessage("WUFOO Credentials Error. Please check username and password.", MessageBoxIcon.Exclamation, "GetEntries")
            Else
                CareMessage("Error connecting to WUFOO. See Error Log for more details.", MessageBoxIcon.Exclamation, "GetEntries")
            End If
        End If

        cgEntries.Populate(m_Entries)

    End Sub

    Private Sub cgEntries_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs)

        'If e Is Nothing OrElse e.FocusedRowHandle < 0 Then
        '    cgEntryFields.Clear()
        '    Exit Sub
        'End If

        'Dim _Entry As WuFooEntry = m_Entries(e.FocusedRowHandle)
        'cgEntryFields.Populate(_Entry.Fields)

    End Sub

    Private Sub cbxObject_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxObject.SelectedIndexChanged

        cbxProperty.Clear()

        Select Case m_Form._Mode

            Case "Import Staff"
                TogglePropertiesCombo(True)
                PopulateCombo(cbxProperty, m_StaffProperties)

            Case "Import Lead"
                TogglePropertiesCombo(True)
                PopulateCombo(cbxProperty, m_LeadProperties)

            Case Else

                Select Case cbxObject.SelectedIndex

                    'contacts
                    Case 0, 1, 2, 3, 4, 5
                        TogglePropertiesCombo(True)
                        PopulateCombo(cbxProperty, m_ContactProperties)

                        'child
                    Case 6
                        TogglePropertiesCombo(True)
                        PopulateCombo(cbxProperty, m_ChildProperties)

                        'allergy, consent, immunisation
                    Case 7, 8, 9
                        TogglePropertiesCombo(False)




                End Select

        End Select

    End Sub

    Private Sub TogglePropertiesCombo(ByVal Enable As Boolean)
        If Enable Then
            cbxProperty.Enabled = True
            cbxProperty.BackColor = Session.ChangeColour
            cbxProperty.SelectedIndex = 0
        Else
            cbxProperty.Enabled = False
            cbxProperty.ResetBackColor()
            cbxProperty.SelectedIndex = -1
        End If
    End Sub


    Private Sub PopulateCombo(ByRef Combo As Care.Controls.CareComboBox, ByVal Properties As List(Of Field))
        Combo.Clear()
        For Each _f As Field In Properties
            Combo.AddItem(_f.Name)
        Next
    End Sub

    Private Sub btnFetchNew_Click(sender As Object, e As EventArgs) Handles btnFetchNew.Click

        SetCMDs(False)
        Session.CursorWaiting()

        GetEntries()

        Session.CursorDefault()
        SetCMDs(True)

    End Sub

    Private Sub btnFormFields_Click(sender As Object, e As EventArgs) Handles btnFormFields.Click

        gbxMappingHeader.Enabled = False
        Session.CursorWaiting()

        ImportFields()
        DisplayMappingGrid()

        gbxMappingHeader.Enabled = True
        Session.CursorDefault()

    End Sub

    Private Sub cgFields_GridDoubleClick(sender As Object, e As EventArgs) Handles cgFields.GridDoubleClick

        If cgFields.RecordCount <= 0 Then Exit Sub
        If Mode <> "EDIT" Then Exit Sub

        gbxField.Tag = cgFields.CurrentRow.Item("ID").ToString
        txtID.Text = cgFields.CurrentRow.Item("Field").ToString
        txtTitle.Text = cgFields.CurrentRow.Item("Label").ToString

        'filter the object types depending on the type of import
        cbxObject.Clear()

        Select Case m_Form._Mode

            Case "Import Staff"
                cbxObject.AddItem("Staff")

            Case "Import Lead"
                cbxObject.AddItem("Lead")

            Case Else
                For Each i In [Enum].GetValues(GetType(EnumObject))
                    If i.ToString <> "Staff" AndAlso i.ToString <> "Lead" Then
                        cbxObject.AddItem(i.ToString)
                    End If
                Next

        End Select

        cbxObject.Text = cgFields.CurrentRow.Item("Object").ToString
        cbxProperty.Text = cgFields.CurrentRow.Item("Property").ToString
        txtOverride.Text = cgFields.CurrentRow.Item("Override").ToString

        gbxField.Show()

        If cbxObject.ItemCount = 1 Then cbxObject.SelectedIndex = 0

        If cgFields.CurrentRow.Item("Object").ToString = "" Then
            cbxObject.Focus()
        Else
            cbxProperty.Focus()
        End If

    End Sub

    Private Sub btnClearMapping_Click(sender As Object, e As EventArgs) Handles btnClearMapping.Click
        If CareMessage("Are you sure you want to Clear the Mapping?", MessageBoxIcon.Warning, MessageBoxButtons.YesNo, "Clear Mapping") = DialogResult.Yes Then
            Dim _SQL As String = "delete from WFMapping where form_id = '" + MyBase.RecordID.Value.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)
            DisplayMappingGrid()
        End If
    End Sub

    Private Sub btnMapCancel_Click(sender As Object, e As EventArgs) Handles btnMapCancel.Click
        gbxField.Hide()
    End Sub

    Private Sub btnMapOK_Click(sender As Object, e As EventArgs) Handles btnMapOK.Click

        If gbxField.Tag IsNot Nothing AndAlso gbxField.Tag.ToString <> "" Then

            Dim _m As Business.WFMapping = Business.WFMapping.RetreiveByID(New Guid(gbxField.Tag.ToString))
            With _m

                ._ValueObject = cbxObject.Text
                ._ValueProperty = cbxProperty.Text

                If chkOverride.Checked Then
                    If txtOverride.Text <> "" Then
                        ._ValueOverride = txtOverride.Text
                    Else
                        ._ValueOverride = Nothing
                    End If
                Else
                    ._ValueOverride = Nothing
                End If

                .Store()

            End With

            cgFields.PersistRowPosition()
            DisplayMappingGrid()
            cgFields.RestoreRowPosition()

            gbxField.Hide()

        End If

    End Sub

    Private Sub btnImportEntry_Click(sender As Object, e As EventArgs) Handles btnImportEntry.Click

        SetCMDs(False)
        Session.CursorWaiting()

        Dim _Entry As WuFooEntry = CType(cgEntries.UnderlyingGridView.GetRow(cgEntries.RowIndex), WuFooEntry)
        Dim _ImportOK As Boolean = False

        Select Case cbxMode.Text

            Case "Import Family"
                _ImportOK = ImportFamily(_Entry)

            Case "Import Staff"
                _ImportOK = ImportStaff(_Entry)

            Case "Import Lead"
                _ImportOK = ImportLead(_Entry)

        End Select

        If _ImportOK Then
            m_Form._LastEntry = ValueHandler.ConvertInteger(_Entry.EntryID)
            m_Form.Store()
            DisplayRecord(m_Form._ID.Value)
        End If

        Session.CursorDefault()
        SetCMDs(True)

    End Sub

    Private Function ImportStaff(ByRef EntryIn As WuFooEntry) As Boolean

        Dim _Staff As New Business.Staff

        If ProcessProperties(Of Business.Staff)(EnumObject.Staff, _Staff, EntryIn) Then

            _Staff._Fullname = _Staff._Forename + " " + _Staff._Surname

            If Not _Staff._SiteId.HasValue Then
                _Staff._SiteId = m_Form._SiteId
                _Staff._SiteName = m_Form._SiteName
            End If

            _Staff.Store()

            CareMessage("Staff Record Imported Successfully", MessageBoxIcon.Information, "Import Entry")
            Return True

        Else
            CareMessage("Error Importing Staff Record.", MessageBoxIcon.Warning, "Import Entry")
            Return False
        End If

    End Function

    Private Function ImportLead(ByRef EntryIn As WuFooEntry) As Boolean

        Dim _Lead As New Business.Lead

        If ProcessProperties(Of Business.Lead)(EnumObject.Lead, _Lead, EntryIn) Then

            With _Lead

                If m_Form._SiteId.HasValue Then
                    _Lead._SiteId = m_Form._SiteId
                    _Lead._SiteName = m_Form._SiteName
                End If

                ._DateEntered = Now
                ._Via = "Web Form"
                ._ContactFullname = ._ContactForename + " " + ._ContactSurname
                ._ChildFullname = ._ChildForename + " " + ._ChildSurname

                Dim _FirstStage As Business.LeadStage = Business.LeadStage.RetreiveFirstStage()
                If _FirstStage IsNot Nothing Then
                    ._StageId = _FirstStage._ID
                    ._Stage = _FirstStage._Name
                    ._StagePcnt = _FirstStage._PcntComplete
                    ._StageStart = Now
                End If

                .Store()

                CRM.CreateActivityRecord(._ID.Value, CRM.EnumLinkType.Lead, CRM.EnumActivityType.Note, Nothing, ._ContactFullname, "Lead Imported from WUFoo", "EntryID: " + EntryIn.EntryID)

            End With

            CareMessage("Lead Imported Successfully", MessageBoxIcon.Information, "Import Entry")
            Return True

        Else
            CareMessage("Error Importing Lead Record.", MessageBoxIcon.Warning, "Import Entry")
            Return False
        End If

    End Function

    Private Function ImportFamily(ByRef EntryIn As WuFooEntry) As Boolean

        'check if we have a Mum record....
        Dim _PrimaryFound As Boolean = False
        Dim _IsMother As Boolean = False
        Dim _Primary As New Business.Contact

        If ProcessProperties(Of Business.Contact)(EnumObject.PrimaryContact, _Primary, EntryIn) Then
            _PrimaryFound = True
        Else
            If ProcessProperties(Of Business.Contact)(EnumObject.Mother, _Primary, EntryIn) Then
                _PrimaryFound = True
                _IsMother = True
            End If
        End If

        'check if we have a Primary Contact Record...
        If _PrimaryFound Then

            'create a new family record
            Dim _F As New Business.Family

            If m_Form._SiteId.HasValue Then
                _F._SiteId = m_Form._SiteId
                _F._SiteName = m_Form._SiteName
            End If

            _F.Store()

            _Primary._FamilyId = _F._ID
            _Primary._PrimaryCont = True
            _Primary._EmerCont = True

            If _IsMother Then _Primary._Relationship = "Mother"

            _Primary._Fullname = _Primary._Forename + " " + _Primary._Surname
            _Primary._Address = BuildAddress(_Primary._Add1, _Primary._Add2, _Primary._Add3, _Primary._Add4, _Primary._Postcode)
            _Primary.Store()

            With _F
                ._Surname = _Primary._Surname
                ._LetterName = _Primary._Fullname
                ._Address = _Primary._Address
                .Store()
            End With

            '****************************************************************************************************************************************

            Dim _SecondaryFound As Boolean = False
            Dim _IsFather As Boolean = False
            Dim _Secondary As New Business.Contact

            If ProcessProperties(Of Business.Contact)(EnumObject.OtherContact, _Secondary, EntryIn) Then
                _SecondaryFound = True
            Else
                If ProcessProperties(Of Business.Contact)(EnumObject.Father, _Secondary, EntryIn) Then
                    _SecondaryFound = True
                    _IsFather = True
                End If
            End If

            If _SecondaryFound Then

                _Secondary._FamilyId = _F._ID
                _Secondary._Relationship = "Father"
                _Primary._EmerCont = True

                _Secondary._Fullname = _Secondary._Forename + " " + _Secondary._Surname
                _Secondary._Address = BuildAddress(_Secondary._Add1, _Secondary._Add2, _Secondary._Add3, _Secondary._Add4, _Secondary._Postcode)

                _Secondary.Store()

            End If

            '****************************************************************************************************************************************

            Dim _EC1 As New Business.Contact
            If ProcessProperties(Of Business.Contact)(EnumObject.EmergencyContact1, _EC1, EntryIn) Then
                _EC1._FamilyId = _F._ID
                _EC1._Fullname = _EC1._Forename + " " + _EC1._Surname
                _EC1._EmerCont = True
                _EC1.Store()
            End If

            '****************************************************************************************************************************************

            Dim _EC2 As New Business.Contact
            If ProcessProperties(Of Business.Contact)(EnumObject.EmergencyContact2, _EC2, EntryIn) Then
                _EC2._FamilyId = _F._ID
                _EC2._Fullname = _EC2._Forename + " " + _EC2._Surname
                _EC2._EmerCont = True
                _EC2.Store()
            End If

            '****************************************************************************************************************************************

            Dim _Child As New Business.Child
            If ProcessProperties(Of Business.Child)(EnumObject.Child, _Child, EntryIn) Then

                _Child._FamilyId = _F._ID
                _Child._FamilyName = _F._Surname

                _Child._Fullname = _Child._Forename + " " + _Child._Surname
                _Child._Status = "On Waiting List"

                If m_Form._SiteId.HasValue Then
                    _Child._SiteId = m_Form._SiteId
                    _Child._SiteName = m_Form._SiteName
                End If

                _Child.Store()

            End If

            '****************************************************************************************************************************************
            ProcessAllergies(_Child._ID.Value, EntryIn)
            ProcessConsent(_Child._ID.Value, _Primary._Fullname, EntryIn)
            ProcessImmunisations(_Child._ID.Value, EntryIn)

            '****************************************************************************************************************************************

            CareMessage("Family Imported Successfully", MessageBoxIcon.Information, "Import Entry")
            Return True

        Else
            'primary contact not found
            CareMessage("Unable to find Primary Contact - Import Cancelled.", MessageBoxIcon.Warning, "Import Entry")
            Return False
        End If

    End Function

    Private Sub ProcessAllergies(ByVal ChildID As Guid, ByRef EntryIn As WuFooEntry)

        For Each _f As WuFooEntryField In EntryIn.Fields

            If _f.FieldValue <> "" Then

                Dim _Allergy As String = FetchFormLabel(EnumObject.AllergyItem, _f.FieldID)
                If _Allergy <> "" Then

                    Dim _A As New Business.ChildAttrib
                    With _A
                        ._ChildId = ChildID
                        ._Category = "Allergies"
                        ._Attribute = _Allergy
                        .Store()
                    End With

                End If

            End If

        Next

    End Sub

    Private Sub ProcessImmunisations(ByVal ChildID As Guid, ByRef EntryIn As WuFooEntry)

        For Each _f As WuFooEntryField In EntryIn.Fields

            If _f.FieldValue <> "" Then

                Dim _Imm As String = FetchFormLabel(EnumObject.ImmunisationItem, _f.FieldID)
                If _Imm <> "" Then

                    Dim _I As New Business.ChildImm
                    With _I
                        ._ChildId = ChildID
                        ._Immunisation = _Imm
                        .Store()
                    End With

                End If

            End If

        Next

    End Sub

    Private Sub ProcessConsent(ByVal ChildID As Guid, ByVal PrimaryContactName As String, ByRef EntryIn As WuFooEntry)

        For Each _f As WuFooEntryField In EntryIn.Fields

            If _f.FieldValue <> "" Then

                Dim _Consent As String = FetchFormLabel(EnumObject.ConsentItem, _f.FieldID)
                If _Consent <> "" Then

                    Dim _C As New Business.ChildConsent
                    With _C
                        ._ChildId = ChildID
                        ._Consent = _Consent
                        ._ConsentPerson = PrimaryContactName
                        ._Given = True
                        .Store()
                    End With

                End If

            End If

        Next

    End Sub

    Private Function BuildAddress(ByVal Add1 As String, ByVal Add2 As String, ByVal Add3 As String, ByVal Add4 As String, ByVal PostCode As String) As String

        Dim _Return As String = ""

        If Add1.Trim <> "" Then _Return += Add1.Trim

        If Add2.Trim <> "" Then
            If _Return <> "" Then _Return += vbCrLf
            _Return += Add2.Trim
        End If

        If Add3.Trim <> "" Then
            If _Return <> "" Then _Return += vbCrLf
            _Return += Add3.Trim
        End If

        If Add4.Trim <> "" Then
            If _Return <> "" Then _Return += vbCrLf
            _Return += Add4.Trim
        End If

        If PostCode.Trim <> "" Then
            If _Return <> "" Then _Return += vbCrLf
            _Return += PostCode.Trim
        End If

        Return _Return

    End Function

    Private Function ImportPhoto(ByRef EntryIn As WuFooEntry, ByVal Photo As String, ByVal RecordID As Guid) As Guid?

        Try

            Dim _PhotoID As Guid? = Nothing
            Dim _TempFile As String = Guid.NewGuid.ToString

            'extract the URL from the brackets
            'mummy_pig.png (https://caresoftware.wufoo.com/cabinet/ejJpMDVucTFrZHpnMzc=/gd3u1iwaVdg%3D/mummy_pig.png)

            '08/01/2019 - Path now looks like this:
            '1497349295camerondiaz.jpg (https://caresoftware.wufoo.com/cabinet/4f903fb7-d00e-49dc-af16-abc331621014)

            Dim _OpenBracket As Integer = Photo.IndexOf("(")
            Dim _CloseBracket As Integer = Photo.IndexOf(")")

            If _OpenBracket > 0 AndAlso _CloseBracket > 0 Then

                Dim _Length As Integer = (_CloseBracket - _OpenBracket) - 1
                Dim _Cabinet As String = Photo.Substring(_OpenBracket + 1, _Length)

                'remove the cabinet from the photo string
                Photo = Photo.Replace(_Cabinet, "").Trim
                Photo = Photo.Replace("(", "").Trim
                Photo = Photo.Replace(")", "").Trim

                Dim _URL As String = _Cabinet
                Dim _Photo As Byte() = Nothing

                If DownloadPhoto(_URL, _Photo) Then

                    DocumentHandler.InsertDocument(RecordID, DocumentHandler.DocumentType.Application, _Photo, "Photo Downloaded from WUFOO", "original")

                    Dim _Thumbnail As Byte() = DocumentHandler.ReturnThumbnail(_Photo, 325)
                    If _Thumbnail IsNot Nothing Then
                        Dim _ID As Guid? = DocumentHandler.InsertDocument(RecordID, DocumentHandler.DocumentType.Application, _Thumbnail, "Thumbnail of Photo Downloaded from WUFOO", "thumbnail")
                        If _ID.HasValue Then _PhotoID = _ID
                    End If

                End If

            End If

            Return _PhotoID

        Catch ex As Exception
            ErrorHandler.LogExceptionToDatabase(ex, False)
        End Try

    End Function

    Private Function DownloadPhoto(ByVal URL As String, ByRef Photo As Byte()) As Boolean

        Dim _Return As Boolean = True
        Dim _WebClient As New System.Net.WebClient

        Try
            Photo = _WebClient.DownloadData(URL)

        Catch ex As Exception
            ErrorHandler.LogExceptionToDatabase(ex, False)
            Photo = Nothing
            _Return = False
        End Try

        _WebClient.Dispose()
        _WebClient = Nothing

        Return _Return

    End Function

    Private Function ProcessProperties(Of T)(ByVal ObjectType As EnumObject, ByRef ObjectIn As T, ByRef EntryIn As WuFooEntry) As Boolean

        Dim _Return As Boolean = False
        Dim _RecordID As Guid? = Nothing

        Dim _t As Type = GetType(T)
        Dim _Properties() As PropertyInfo = _t.GetProperties()
        For Each _p As PropertyInfo In _Properties
            If _p.CanWrite AndAlso _p.Name.StartsWith("_") Then

                If _p.Name = "_ID" Then
                    Dim _oID As Object = CallByName(ObjectIn, _p.Name, CallType.Get, Nothing)
                    If _oID IsNot Nothing Then
                        _RecordID = CType(_oID, Guid?)
                    End If
                Else

                    Dim _Value As String = FetchField(ObjectType, _p.Name, EntryIn)
                    If _Value <> "" Then

                        If _p.Name = "_Photo" Then
                            Dim _PhotoID As Guid? = ImportPhoto(EntryIn, _Value, _RecordID.Value)
                            If _PhotoID.HasValue Then
                                CallByName(ObjectIn, _p.Name, CallType.Set, _PhotoID)
                            End If
                        Else

                            Select Case _p.PropertyType.Name

                                Case "Boolean"
                                    CallByName(ObjectIn, _p.Name, CallType.Set, HandleBoolean(_Value))

                                Case Else
                                    CallByName(ObjectIn, _p.Name, CallType.Set, _Value)

                            End Select

                        End If

                        _Return = True

                    End If

                End If

            End If
        Next

        Return _Return

    End Function

    Private Function HandleBoolean(ByVal ValueIn As String) As Boolean

        Select Case ValueIn.ToUpper

            Case "YES", "TRUE"
                Return True

            Case Else
                Return False
        End Select

    End Function

    Private Function FetchField(ByVal ObjectIn As EnumObject, ByVal PropertyName As String, ByRef Entry As WuFooEntry) As String

        Dim _Return As String = ""
        Dim _SQL As String = ""
        Dim _ObjectName As String = ObjectIn.ToString

        'check if this field is mapped

        _SQL += "select form_field, value_override from WFMapping"
        _SQL += " where form_id = '" + MyBase.RecordID.Value.ToString + "'"
        _SQL += " and value_object = '" + _ObjectName + "'"
        _SQL += " and value_property = '" + PropertyName + "'"

        Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DR IsNot Nothing Then

            'check for an override value
            If _DR.Item("value_override").ToString <> "" Then
                _Return = _DR.Item("value_override").ToString
            Else
                If _DR.Item("form_field").ToString <> "" Then
                    _Return = ExtractValueFromEntry(_DR.Item("form_field").ToString, Entry.Fields)
                End If
            End If

        End If

        Return _Return

    End Function

    Private Function FetchFormLabel(ByVal ObjectIn As EnumObject, ByVal FormField As String) As String

        Dim _Return As String = ""
        Dim _SQL As String = ""
        Dim _ObjectName As String = ObjectIn.ToString

        'check if this field is mapped

        _SQL += "select form_label from WFMapping"
        _SQL += " where form_id = '" + MyBase.RecordID.Value.ToString + "'"
        _SQL += " and value_object = '" + _ObjectName + "'"
        _SQL += " and form_field = '" + FormField + "'"

        Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DR IsNot Nothing Then
            If _DR.Item("form_label").ToString <> "" Then
                _Return = _DR.Item("form_label").ToString
            End If
        End If

        Return _Return

    End Function

    Private Function ExtractValueFromEntry(ByVal FormField As String, ByRef Fields As List(Of WuFooEntryField)) As String

        Dim _Return As String = ""

        For Each _e As WuFooEntryField In Fields
            If _e.FieldID = FormField Then
                _Return = _e.FieldValue
            End If
        Next

        Return _Return

    End Function

#Region "Internal Classes for Import"

    Public Class WuFooField
        Public Property Title As String
        Public Property Type As String
        Public Property ID As String
        Public Property IsRequired As Boolean
        Public Property HasOtherField As Boolean
        Public Property SubFields As New List(Of WuFooSubField)
        Public Property Choices As New List(Of WuFooChoice)
    End Class

    Public Class WuFooChoice

        Public Sub New(ByVal Label As String)
            Me.Label = Label
        End Sub

        Public Property Label As String
        Public Property Score As String

    End Class

    Public Class WuFooSubField

        Public Sub New(ByVal ID As String, ByVal Label As String)
            Me.ID = ID
            Me.Label = Label
        End Sub

        Public Property ID As String
        Public Property Label As String

    End Class

    Public Class WuFooEntry
        Public Property EntryID As String
        Public Property Fields As New List(Of WuFooEntryField)
    End Class

    Public Class WuFooEntryField

        Public Sub New(ByVal FieldID As String, ByVal FieldValue As String)
            Me.FieldID = FieldID
            Me.FieldValue = FieldValue
        End Sub

        Public Property FieldID As String
        Public Property FieldValue As String

    End Class

#End Region

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click

        If gbxField.Tag IsNot Nothing AndAlso gbxField.Tag.ToString <> "" Then
            If CareMessage("Are you sure you want to clear this field mapping?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Clear Mapping") = DialogResult.Yes Then

                Dim _m As Business.WFMapping = Business.WFMapping.RetreiveByID(New Guid(gbxField.Tag.ToString))
                With _m
                    ._ValueObject = Nothing
                    ._ValueProperty = Nothing
                    ._ValueOverride = Nothing
                    .Store()
                End With

                cgFields.PersistRowPosition()
                DisplayMappingGrid()
                cgFields.RestoreRowPosition()

                gbxField.Hide()

            End If
        End If

    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click

        If gbxField.Tag IsNot Nothing AndAlso gbxField.Tag.ToString <> "" Then
            If CareMessage("Are you sure you want to delete this field?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Delete Field") = DialogResult.Yes Then

                Business.WFMapping.DeleteRecord(New Guid(gbxField.Tag.ToString))

                cgFields.PersistRowPosition()
                DisplayMappingGrid()
                cgFields.RestoreRowPosition()

                gbxField.Hide()

            End If
        End If

    End Sub

    Private Sub btnMap_Click(sender As Object, e As EventArgs) Handles btnMap.Click
        AutoMap()
    End Sub

    Private Function ReturnParentLabel(ByRef Fields As List(Of Business.WFMapping), ByVal Field As String) As String

        Dim _Q As IEnumerable(Of Business.WFMapping) = From _M As Business.WFMapping In Fields
                                                       Where _M._FormField = Field _
                                                       And _M._ParentField = ""

        If _Q IsNot Nothing AndAlso _Q.Count > 0 Then
            Return _Q.First._FormLabel
        End If

        Return ""

    End Function

    Private Sub AutoMap()

        btnMap.Enabled = False
        Session.CursorWaiting()

        Dim _Mapping As List(Of Business.WFMapping) = Business.WFMapping.RetreiveByFormID(m_Form._ID)
        For Each M As Business.WFMapping In _Mapping

            If M._ValueObject = "" Then

                Dim _ParentLabel As String = ReturnParentLabel(_Mapping, M._ParentField)

                MapContact("Primary", "PrimaryContact", M, _ParentLabel)
                MapContact("Secondary", "OtherContact", M, _ParentLabel)
                MapContact("First Emergency", "EmergencyContact1", M, _ParentLabel)
                MapContact("Second Emergency", "EmergencyContact2", M, _ParentLabel)

                If _ParentLabel.ToLower.Contains("child") AndAlso _ParentLabel.ToLower.Contains("name") Then

                    If M._FormLabel = "First" Then
                        M._ValueObject = "Child"
                        M._ValueProperty = "_Forename"
                    End If

                    If M._FormLabel = "Last" Then
                        M._ValueObject = "Child"
                        M._ValueProperty = "_Surname"
                    End If

                End If

                If M._FormLabel.ToLower.Contains("child") Then

                    If M._FormLabel.ToLower.Contains("date of birth") OrElse M._FormLabel.Contains("DOB") Then
                        M._ValueObject = "Child"
                        M._ValueProperty = "_Dob"
                    End If

                    If M._FormLabel.ToLower.Contains("gender") Then
                        M._ValueObject = "Child"
                        M._ValueProperty = "_Gender"
                    End If

                    If M._FormLabel.ToLower.Contains("nationality") Then
                        M._ValueObject = "Child"
                        M._ValueProperty = "_Nationality"
                    End If

                    If M._FormLabel.ToLower.Contains("ethnicity") Then
                        M._ValueObject = "Child"
                        M._ValueProperty = "_Ethnicity"
                    End If

                    If M._FormLabel.ToLower.Contains("religion") Then
                        M._ValueObject = "Child"
                        M._ValueProperty = "_Religion"
                    End If

                    If M._FormLabel.ToLower.Contains("language") Then
                        M._ValueObject = "Child"
                        M._ValueProperty = "_Language"
                    End If

                    If _ParentLabel = "" Then
                        If M._FormLabel.ToLower.Contains("photo") Then
                            M._ValueObject = "Child"
                            M._ValueProperty = "_Photo"
                        End If
                    End If

                    If M._FormLabel.ToLower.Contains("doctor") OrElse M._FormLabel.Contains("GP") Then

                        If M._FormLabel.ToLower.Contains("surgery") Then
                            M._ValueObject = "Child"
                            M._ValueProperty = "_SurgName"
                        End If

                        If M._FormLabel.ToLower.Contains("tel") Then
                            M._ValueObject = "Child"
                            M._ValueProperty = "_SurgTel"
                        End If

                        If Not M._FormLabel.ToLower.Contains("surgery") Then
                            If M._FormLabel.ToLower.Contains("name") Then
                                M._ValueObject = "Child"
                                M._ValueProperty = "_SurgDoc"
                            End If
                        End If

                    End If

                Else
                    'not a child field
                    If _ParentLabel.ToLower.Contains("consent") OrElse _ParentLabel.ToLower.Contains("permission") Then
                        M._ValueObject = "ConsentItem"
                        M._ValueProperty = ""
                    Else
                        If _ParentLabel.ToLower.Contains("immunisation") OrElse _ParentLabel.ToLower.Contains("vaccination") Then
                            M._ValueObject = "ImmunisationItem"
                            M._ValueProperty = ""
                        Else
                            If _ParentLabel.ToLower.Contains("allergy") OrElse _ParentLabel.ToLower.Contains("allergies") Then
                                M._ValueObject = "AllergyItem"
                                M._ValueProperty = ""
                            End If
                        End If
                    End If
                End If

            End If

        Next

        Business.WFMapping.SaveAll(_Mapping)
        DisplayMappingGrid()

        btnMap.Enabled = True
        Session.CursorDefault()

    End Sub

    Private Sub MapContact(ByVal MapSearch As String, ByVal ObjectName As String, ByRef M As Business.WFMapping, ByVal ParentLabel As String)

        If ParentLabel.Contains(MapSearch) AndAlso ParentLabel.ToLower.Contains("name") Then

            If M._FormLabel = "First" Then
                M._ValueObject = ObjectName
                M._ValueProperty = "_Forename"
            End If

            If M._FormLabel = "Last" Then
                M._ValueObject = ObjectName
                M._ValueProperty = "_Surname"
            End If

        End If

        If M._FormLabel.Contains(MapSearch) Then

            If M._FormLabel.ToLower.Contains("relationship") Then
                M._ValueObject = ObjectName
                M._ValueProperty = "_Relationship"
            End If

            If M._FormLabel.ToLower.Contains("email") Then
                M._ValueObject = ObjectName
                M._ValueProperty = "_Email"
            End If

            If M._FormLabel.ToLower.Contains("mobile") Then
                M._ValueObject = ObjectName
                M._ValueProperty = "_TelMobile"
            End If

            If M._FormLabel.ToLower.Contains("home") AndAlso M._FormLabel.ToLower.Contains("tel") Then
                M._ValueObject = ObjectName
                M._ValueProperty = "_TelHome"
            End If

            If M._FormLabel.ToLower.Contains("work") AndAlso M._FormLabel.ToLower.Contains("tel") Then
                M._ValueObject = ObjectName
                M._ValueProperty = "_JobTel"
            End If

            If M._FormLabel.ToLower.Contains("password") Then
                M._ValueObject = ObjectName
                M._ValueProperty = "_Password"
            End If

            If M._FormLabel.ToLower.Contains("photo") Then
                M._ValueObject = ObjectName
                M._ValueProperty = "_Photo"
            End If

        End If

        If ParentLabel.Contains(MapSearch) AndAlso ParentLabel.ToLower.Contains("address") Then

            If Not ParentLabel.Contains("different") Then

                If M._FormLabel = "Street Address" Then
                    M._ValueObject = ObjectName
                    M._ValueProperty = "_Add1"
                End If

                If M._FormLabel = "Address Line 2" Then
                    M._ValueObject = ObjectName
                    M._ValueProperty = "_Add2"
                End If

                If M._FormLabel = "City" Then
                    M._ValueObject = ObjectName
                    M._ValueProperty = "_Add3"
                End If

                If M._FormLabel = "State / Province / Region" Then
                    M._ValueObject = ObjectName
                    M._ValueProperty = "_Add4"
                End If

                If M._FormLabel = "Postal / Zip Code" Then
                    M._ValueObject = ObjectName
                    M._ValueProperty = "_Postcode"
                End If

            End If

        End If

    End Sub

End Class
