﻿

Imports Care.Global
Imports Care.Data
Imports Care.Shared

Public Class frmInvoiceCrossCheck

    Private m_RegisterCheckData As New List(Of RegisterCheckRecord)
    Private m_BatchHeader As Business.InvoiceBatch
    Private m_BatchFrom As String
    Private m_BatchTo As String

    Private Sub frmInvoiceCrossCheck_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Me.Text = "Batch Cross-Checking [Batch: " + m_BatchHeader._BatchNo.ToString + "]"

        If m_BatchHeader._DateFirst.HasValue AndAlso m_BatchHeader._DateLast.HasValue Then

            m_BatchFrom = ValueHandler.SQLDate(m_BatchHeader._DateFirst.Value, True)
            m_BatchTo = ValueHandler.SQLDate(m_BatchHeader._DateLast.Value, True)

            BasicCheck()

            Me.Text += " " + Format(m_BatchHeader._DateFirst.Value, "dd/MM/yyyy") + " - " + Format(m_BatchHeader._DateLast.Value, "dd/MM/yyyy")

        Else
            gbxMode.Enabled = False
        End If

        radComparison.Enabled = False

    End Sub

    Public Sub New(ByVal BatchHeader As Business.InvoiceBatch)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_BatchHeader = BatchHeader

    End Sub

    Private Sub radBasic_CheckedChanged(sender As Object, e As EventArgs) Handles radBasic.CheckedChanged
        If radBasic.Checked Then BasicCheck()
    End Sub

    Private Sub BasicCheck()

        gbxComparison.Enabled = False

        Dim _SQL As String = ""

        _SQL += "select fullname as 'Name', class as 'Class', group_name as 'Room', status as 'Status',"
        _SQL += " started as 'Started', date_left as 'Leaving Date',"
        _SQL += " (select count(*) from Invoices i"
        _SQL += " left join InvoiceBatch b on b.ID = i.batch_id"
        _SQL += " where i.child_id = c.ID"
        _SQL += " and b.batch_period not in ('Annual Recalculation', 'Annual Segments')"
        _SQL += " and b.batch_status not in ('Abandoned')"
        _SQL += " and i.invoice_period between " + m_BatchFrom + " and " + m_BatchTo + ") as 'Invoice Count',"
        _SQL += " (select isnull(sum(invoice_total),0) from Invoices i"
        _SQL += " left join InvoiceBatch b on b.ID = i.batch_id"
        _SQL += " where i.child_id = c.ID"
        _SQL += " and b.batch_period not in ('Annual Recalculation', 'Annual Segments')"
        _SQL += " and b.batch_status not in ('Abandoned')"
        _SQL += " and i.invoice_period between " + m_BatchFrom + " and " + m_BatchTo + ") as 'Invoiced'"
        _SQL += " from Children c"

        cgData.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub radRegister_CheckedChanged(sender As Object, e As EventArgs) Handles radRegister.CheckedChanged
        If radRegister.Checked Then RegisterCheck()
    End Sub

    Private Sub RegisterCheck()

        gbxComparison.Enabled = False

        Session.CursorWaiting()

        m_RegisterCheckData.Clear()
        Dim _Children As List(Of Business.Child) = Business.Child.RetreiveAll

        Session.SetupProgressBar("Checking Children...", _Children.Count)

        For Each _c In _Children
            ProcessDays(_c)
            Session.StepProgressBar()
        Next

        Session.HideProgressBar()
        Session.CursorDefault()

        cgData.Populate(m_RegisterCheckData)

    End Sub

    Private Sub ProcessDays(ByVal Child As Business.Child)

        'loop through each day in the range
        Dim _CheckDate As Date = m_BatchHeader._DateFirst.Value

        Dim _CheckRecord As New RegisterCheckRecord
        _CheckRecord.ChildName = Child._Fullname
        _CheckRecord.ChildRoom = Child._GroupName
        _CheckRecord.ChildStarted = Child._Started
        _CheckRecord.ChildLeft = Child._DateLeft
        _CheckRecord.RegisteredHours = 0
        _CheckRecord.InvoicedHours = 0

        While _CheckDate <= m_BatchHeader._DateLast.Value

            'get the registered hours
            _CheckRecord.RegisteredHours += ReturnRegisteredHours(Child._ID.ToString, _CheckDate)

            'get the invoiced hours
            _CheckRecord.InvoicedHours += ReturnInvoicedHours(Child._ID.ToString, _CheckDate)

            _CheckDate = _CheckDate.AddDays(1)

        End While

        m_RegisterCheckData.Add(_CheckRecord)

    End Sub

    Private Function ReturnRegisteredHours(ByVal ChildID As String, ByVal CheckDate As Date) As Double

        Dim _TimeIn As Date? = Nothing
        Dim _TimeOut As Date? = Nothing
        Dim _SQL As String = ""

        _SQL += "SELECT top 1 stamp_in FROM Register i"
        _SQL += " WHERE i.date = " + ValueHandler.SQLDate(CheckDate, True)
        _SQL += " AND i.person_id = '" + ChildID + "'"
        _SQL += " AND i.in_out = 'I' ORDER BY i.stamp_in"

        _TimeIn = CleanDate(DAL.ReturnScalar(Session.ConnectionString, _SQL))

        _SQL = ""
        _SQL += "SELECT top 1 stamp_out FROM Register o"
        _SQL += " WHERE o.date = " + ValueHandler.SQLDate(CheckDate, True)
        _SQL += " AND o.person_id = '" + ChildID + "'"
        _SQL += " AND o.in_out = 'O' ORDER BY o.stamp_out desc"

        _TimeOut = CleanDate(DAL.ReturnScalar(Session.ConnectionString, _SQL))

        If _TimeIn.HasValue AndAlso _TimeOut.HasValue Then
            Return DateDiff(DateInterval.Minute, _TimeIn.Value, _TimeOut.Value)
        Else
            Return 0
        End If

    End Function

    Private Function CleanDate(ByVal ObjectIn As Object) As Date?
        If ObjectIn Is Nothing Then
            Return Nothing
        Else
            Return CDate(ObjectIn)
        End If
    End Function

    Private Function ReturnInvoicedHours(ByVal ChildID As String, ByVal CheckDate As Date) As Double

        Dim _SQL As String = ""
        _SQL += "select sum(isnull(l.hours,0)) as 'hours' from InvoiceLines l"
        _SQL += " left join Invoices i on i.ID = l.invoice_id"
        _SQL += " left join InvoiceBatch b on b.ID = i.batch_id"
        _SQL += " where i.child_id = '" + ChildID + "'"
        _SQL += " and l.action_date = " + ValueHandler.SQLDate(CheckDate, True)
        _SQL += " and b.batch_status not in ('Abandoned')"
        _SQL += " and b.batch_period not in ('Annual Recalculation', 'Annual Segments')"

        Dim _V As Object = DAL.ReturnScalar(Session.ConnectionString, _SQL)
        If _V Is Nothing Then
            Return 0
        Else
            If IsDBNull(_V) Then
                Return 0
            Else
                Return CDbl(_V) * 60
            End If
        End If

    End Function

    Private Class RegisterCheckRecord
        Public Property ChildName As String
        Public Property ChildRoom As String
        Public Property ChildStarted As Date?
        Public Property ChildLeft As Date?
        Public Property InvoicedHours As Double
        Public Property RegisteredHours As Double
    End Class

End Class
