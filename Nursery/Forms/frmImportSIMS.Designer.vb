﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportSIMS
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxFamily = New Care.Controls.CareFrame(Me.components)
        Me.btnFetchData = New Care.Controls.CareButton(Me.components)
        Me.txtWhere = New Care.Controls.CareTextBox(Me.components)
        Me.txtCN = New Care.Controls.CareTextBox(Me.components)
        Me.Label15 = New Care.Controls.CareLabel(Me.components)
        Me.Label14 = New Care.Controls.CareLabel(Me.components)
        Me.cgRecords = New Care.Controls.CareGrid()
        Me.btnImport = New Care.Controls.CareButton(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.cbxSite = New Care.Controls.CareComboBox(Me.components)
        Me.cbxKeyworker = New Care.Controls.CareComboBox(Me.components)
        Me.cbxGroup = New Care.Controls.CareComboBox(Me.components)
        Me.Label13 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.txtSchema = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        CType(Me.gbxFamily, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxFamily.SuspendLayout()
        CType(Me.txtWhere.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCN.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxKeyworker.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxGroup.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSchema.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'gbxFamily
        '
        Me.gbxFamily.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxFamily.Controls.Add(Me.CareLabel3)
        Me.gbxFamily.Controls.Add(Me.txtSchema)
        Me.gbxFamily.Controls.Add(Me.CareLabel1)
        Me.gbxFamily.Controls.Add(Me.cbxSite)
        Me.gbxFamily.Controls.Add(Me.cbxKeyworker)
        Me.gbxFamily.Controls.Add(Me.cbxGroup)
        Me.gbxFamily.Controls.Add(Me.Label13)
        Me.gbxFamily.Controls.Add(Me.CareLabel2)
        Me.gbxFamily.Controls.Add(Me.btnFetchData)
        Me.gbxFamily.Controls.Add(Me.txtWhere)
        Me.gbxFamily.Controls.Add(Me.txtCN)
        Me.gbxFamily.Controls.Add(Me.Label15)
        Me.gbxFamily.Controls.Add(Me.Label14)
        Me.gbxFamily.Location = New System.Drawing.Point(12, 12)
        Me.gbxFamily.Name = "gbxFamily"
        Me.gbxFamily.ShowCaption = False
        Me.gbxFamily.Size = New System.Drawing.Size(759, 126)
        Me.gbxFamily.TabIndex = 0
        Me.gbxFamily.Text = "GroupControl1"
        '
        'btnFetchData
        '
        Me.btnFetchData.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFetchData.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFetchData.Appearance.Options.UseFont = True
        Me.btnFetchData.Location = New System.Drawing.Point(632, 39)
        Me.btnFetchData.Name = "btnFetchData"
        Me.btnFetchData.Size = New System.Drawing.Size(112, 24)
        Me.btnFetchData.TabIndex = 6
        Me.btnFetchData.Tag = ""
        Me.btnFetchData.Text = "Fetch SIMS Data"
        '
        'txtWhere
        '
        Me.txtWhere.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtWhere.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtWhere.EditValue = "where s.current_year_code is not null"
        Me.txtWhere.EnterMoveNextControl = True
        Me.txtWhere.Location = New System.Drawing.Point(435, 39)
        Me.txtWhere.MaxLength = 0
        Me.txtWhere.Name = "txtWhere"
        Me.txtWhere.NumericAllowNegatives = False
        Me.txtWhere.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtWhere.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtWhere.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtWhere.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.txtWhere.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtWhere.Properties.Appearance.Options.UseBackColor = True
        Me.txtWhere.Properties.Appearance.Options.UseFont = True
        Me.txtWhere.Properties.Appearance.Options.UseTextOptions = True
        Me.txtWhere.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtWhere.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtWhere.Size = New System.Drawing.Size(191, 22)
        Me.txtWhere.TabIndex = 5
        Me.txtWhere.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtWhere.ToolTipText = ""
        '
        'txtCN
        '
        Me.txtCN.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCN.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtCN.Location = New System.Drawing.Point(179, 11)
        Me.txtCN.MaxLength = 0
        Me.txtCN.Name = "txtCN"
        Me.txtCN.NumericAllowNegatives = False
        Me.txtCN.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtCN.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCN.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtCN.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.txtCN.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtCN.Properties.Appearance.Options.UseBackColor = True
        Me.txtCN.Properties.Appearance.Options.UseFont = True
        Me.txtCN.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCN.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtCN.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtCN.Size = New System.Drawing.Size(565, 22)
        Me.txtCN.TabIndex = 1
        Me.txtCN.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCN.ToolTipText = ""
        '
        'Label15
        '
        Me.Label15.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label15.Location = New System.Drawing.Point(12, 14)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(125, 15)
        Me.Label15.TabIndex = 0
        Me.Label15.Text = "SIMS Connection String"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label15.ToolTip = "The group start date indicates when the child started in the particular group."
        Me.Label15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'Label14
        '
        Me.Label14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label14.Location = New System.Drawing.Point(268, 42)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(161, 15)
        Me.Label14.TabIndex = 4
        Me.Label14.Text = "Where Clause for stud_student"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cgRecords
        '
        Me.cgRecords.AllowBuildColumns = True
        Me.cgRecords.AllowEdit = False
        Me.cgRecords.AllowHorizontalScroll = False
        Me.cgRecords.AllowMultiSelect = True
        Me.cgRecords.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgRecords.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgRecords.Appearance.Options.UseFont = True
        Me.cgRecords.AutoSizeByData = True
        Me.cgRecords.DisableAutoSize = False
        Me.cgRecords.DisableDataFormatting = False
        Me.cgRecords.FocusedRowHandle = -2147483648
        Me.cgRecords.HideFirstColumn = False
        Me.cgRecords.Location = New System.Drawing.Point(12, 144)
        Me.cgRecords.Name = "cgRecords"
        Me.cgRecords.PreviewColumn = ""
        Me.cgRecords.QueryID = Nothing
        Me.cgRecords.RowAutoHeight = False
        Me.cgRecords.SearchAsYouType = True
        Me.cgRecords.ShowAutoFilterRow = False
        Me.cgRecords.ShowFindPanel = True
        Me.cgRecords.ShowGroupByBox = True
        Me.cgRecords.ShowLoadingPanel = False
        Me.cgRecords.ShowNavigator = True
        Me.cgRecords.Size = New System.Drawing.Size(759, 408)
        Me.cgRecords.TabIndex = 1
        '
        'btnImport
        '
        Me.btnImport.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnImport.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImport.Appearance.Options.UseFont = True
        Me.btnImport.Location = New System.Drawing.Point(12, 558)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(65, 24)
        Me.btnImport.TabIndex = 2
        Me.btnImport.Tag = ""
        Me.btnImport.Text = "Import"
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(12, 98)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(33, 15)
        Me.CareLabel1.TabIndex = 9
        Me.CareLabel1.Text = "Group"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(179, 67)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AccessibleName = "Group"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(250, 22)
        Me.cbxSite.TabIndex = 8
        Me.cbxSite.Tag = ""
        Me.cbxSite.ValueMember = Nothing
        '
        'cbxKeyworker
        '
        Me.cbxKeyworker.AllowBlank = False
        Me.cbxKeyworker.DataSource = Nothing
        Me.cbxKeyworker.DisplayMember = Nothing
        Me.cbxKeyworker.EnterMoveNextControl = True
        Me.cbxKeyworker.Location = New System.Drawing.Point(507, 95)
        Me.cbxKeyworker.Name = "cbxKeyworker"
        Me.cbxKeyworker.Properties.AccessibleName = "Keyworker"
        Me.cbxKeyworker.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxKeyworker.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxKeyworker.Properties.Appearance.Options.UseFont = True
        Me.cbxKeyworker.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxKeyworker.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxKeyworker.SelectedValue = Nothing
        Me.cbxKeyworker.Size = New System.Drawing.Size(237, 22)
        Me.cbxKeyworker.TabIndex = 12
        Me.cbxKeyworker.Tag = ""
        Me.cbxKeyworker.ValueMember = Nothing
        '
        'cbxGroup
        '
        Me.cbxGroup.AllowBlank = False
        Me.cbxGroup.DataSource = Nothing
        Me.cbxGroup.DisplayMember = Nothing
        Me.cbxGroup.EnterMoveNextControl = True
        Me.cbxGroup.Location = New System.Drawing.Point(179, 95)
        Me.cbxGroup.Name = "cbxGroup"
        Me.cbxGroup.Properties.AccessibleName = "Group"
        Me.cbxGroup.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxGroup.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxGroup.Properties.Appearance.Options.UseFont = True
        Me.cbxGroup.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxGroup.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxGroup.SelectedValue = Nothing
        Me.cbxGroup.Size = New System.Drawing.Size(250, 22)
        Me.cbxGroup.TabIndex = 10
        Me.cbxGroup.Tag = ""
        Me.cbxGroup.ValueMember = Nothing
        '
        'Label13
        '
        Me.Label13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label13.Location = New System.Drawing.Point(446, 98)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(55, 15)
        Me.Label13.TabIndex = 11
        Me.Label13.Text = "Keyworker"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(12, 70)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(19, 15)
        Me.CareLabel2.TabIndex = 7
        Me.CareLabel2.Text = "Site"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSchema
        '
        Me.txtSchema.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtSchema.EditValue = "sims"
        Me.txtSchema.EnterMoveNextControl = True
        Me.txtSchema.Location = New System.Drawing.Point(179, 39)
        Me.txtSchema.MaxLength = 0
        Me.txtSchema.Name = "txtSchema"
        Me.txtSchema.NumericAllowNegatives = False
        Me.txtSchema.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSchema.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSchema.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSchema.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.txtSchema.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSchema.Properties.Appearance.Options.UseBackColor = True
        Me.txtSchema.Properties.Appearance.Options.UseFont = True
        Me.txtSchema.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSchema.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSchema.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSchema.Size = New System.Drawing.Size(66, 22)
        Me.txtSchema.TabIndex = 3
        Me.txtSchema.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSchema.ToolTipText = ""
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(12, 44)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(71, 15)
        Me.CareLabel3.TabIndex = 2
        Me.CareLabel3.Text = "SIMS Schema"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel3.ToolTip = "The group start date indicates when the child started in the particular group."
        Me.CareLabel3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'frmImportSIMS
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(783, 590)
        Me.Controls.Add(Me.btnImport)
        Me.Controls.Add(Me.gbxFamily)
        Me.Controls.Add(Me.cgRecords)
        Me.LoadMaximised = True
        Me.Name = "frmImportSIMS"
        Me.Text = "frmImportSIMS"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.gbxFamily, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxFamily.ResumeLayout(False)
        Me.gbxFamily.PerformLayout()
        CType(Me.txtWhere.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCN.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxKeyworker.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxGroup.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSchema.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents gbxFamily As Care.Controls.CareFrame
    Friend WithEvents Label15 As Care.Controls.CareLabel
    Friend WithEvents Label14 As Care.Controls.CareLabel
    Friend WithEvents cgRecords As Care.Controls.CareGrid
    Friend WithEvents txtWhere As Care.Controls.CareTextBox
    Friend WithEvents txtCN As Care.Controls.CareTextBox
    Friend WithEvents btnImport As Care.Controls.CareButton
    Friend WithEvents btnFetchData As Care.Controls.CareButton
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
    Friend WithEvents cbxKeyworker As Care.Controls.CareComboBox
    Friend WithEvents cbxGroup As Care.Controls.CareComboBox
    Friend WithEvents Label13 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents txtSchema As Care.Controls.CareTextBox
End Class
