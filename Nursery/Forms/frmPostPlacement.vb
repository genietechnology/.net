﻿Option Explicit On

Imports System.Windows.Forms
Imports Care.Global
Public Class frmPostPlacement

    Private mChild As Business.Child

    Public Sub New(child As Business.Child)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        mChild = child

    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click

        mChild._PlacementForename = txtForename.Text
        mChild._PlacementSurname = txtSurname.Text
        If lblFamily.Tag IsNot Nothing AndAlso lblFamily.Tag.ToString <> "" Then
            mChild._PlacementFamilyId = New Guid(lblFamily.Tag.ToString)
        Else
            mChild._PlacementFamilyId = Nothing
        End If
        mChild._PlacementName = txtPlacement.Text
        mChild._PlacementAgency = txtPlacementAgency.Text
        mChild._PlacementReferee = txtPlacementReferee.Text

        Close()

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Close()
    End Sub

    Private Sub frmPostPlacement_Load(sender As Object, e As EventArgs) Handles Me.Load
        MyControls.SetControls(Care.Shared.ControlHandler.Mode.Edit, Me.Controls)
        Display()
    End Sub

    Private Sub Display()
        txtForename.Text = mChild._PlacementForename
        txtSurname.Text = mChild._PlacementSurname
        lblFamily.Tag = mChild._PlacementFamilyId.ToString

        If Not String.IsNullOrWhiteSpace(lblFamily.Tag.ToString) Then
            Dim family As Business.Family = Business.Family.RetreiveByID(New Guid(lblFamily.Tag.ToString))
            txtFamily.Text = family._Surname
            family = Nothing
        End If

        txtPlacement.Text = mChild._PlacementName
        txtPlacementAgency.Text = mChild._PlacementAgency
        txtPlacementReferee.Text = mChild._PlacementReferee

    End Sub

    Private Sub SetFamily()

        Dim familyID As String = Business.Family.FindFamily(Me, True)

        If familyID <> "" Then

            lblFamily.Tag = familyID

            Dim family As Business.Family = Business.Family.RetreiveByID(New Guid(lblFamily.Tag.ToString))
            txtFamily.Text = family._Surname
            txtSurname.Text = family._Surname
            family = Nothing

        End If

    End Sub

    Private Sub btnFamilyView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFamilyView.Click
        If Me.ReadOnlyPermissions Then
            CareMessage("You do not have permission to drill down.", MessageBoxIcon.Exclamation, "Permissions Check")
        Else
            If lblFamily.Tag.ToString <> "" Then
                Business.Family.DrillDown(New Guid(lblFamily.Tag.ToString))
            End If
        End If
    End Sub

    Private Sub btnFamilySet_Click(sender As Object, e As EventArgs) Handles btnFamilySet.Click
        SetFamily()
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        txtFamily.Text = ""
        lblFamily.Tag = ""
    End Sub
End Class