﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmParentReports
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.btnRun = New Care.Controls.CareButton(Me.components)
        Me.GroupBox1 = New Care.Controls.CareFrame()
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.cbxSite = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.cbxRoom = New Care.Controls.CareComboBox(Me.components)
        Me.udtDate = New Care.Controls.CareDateTime(Me.components)
        Me.Label2 = New Care.Controls.CareLabel(Me.components)
        Me.Label1 = New Care.Controls.CareLabel(Me.components)
        Me.cbxMode = New Care.Controls.CareComboBox(Me.components)
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxRoom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.udtDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.udtDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxMode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'btnClose
        '
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(339, 148)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 25)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "Close"
        '
        'btnRun
        '
        Me.btnRun.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnRun.Appearance.Options.UseFont = True
        Me.btnRun.Location = New System.Drawing.Point(258, 148)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(75, 25)
        Me.btnRun.TabIndex = 1
        Me.btnRun.Text = "Run"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CareLabel2)
        Me.GroupBox1.Controls.Add(Me.cbxSite)
        Me.GroupBox1.Controls.Add(Me.CareLabel1)
        Me.GroupBox1.Controls.Add(Me.cbxRoom)
        Me.GroupBox1.Controls.Add(Me.udtDate)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cbxMode)
        Me.GroupBox1.Location = New System.Drawing.Point(9, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.ShowCaption = False
        Me.GroupBox1.Size = New System.Drawing.Size(405, 130)
        Me.GroupBox1.TabIndex = 0
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(12, 100)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel2.TabIndex = 6
        Me.CareLabel2.Text = "Room"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(102, 69)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AccessibleName = "Group"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(290, 22)
        Me.cbxSite.TabIndex = 5
        Me.cbxSite.Tag = ""
        Me.cbxSite.ValueMember = Nothing
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(12, 72)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(19, 15)
        Me.CareLabel1.TabIndex = 4
        Me.CareLabel1.Text = "Site"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxRoom
        '
        Me.cbxRoom.AllowBlank = False
        Me.cbxRoom.DataSource = Nothing
        Me.cbxRoom.DisplayMember = Nothing
        Me.cbxRoom.EnterMoveNextControl = True
        Me.cbxRoom.Location = New System.Drawing.Point(102, 97)
        Me.cbxRoom.Name = "cbxRoom"
        Me.cbxRoom.Properties.AccessibleName = "Group"
        Me.cbxRoom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxRoom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxRoom.Properties.Appearance.Options.UseFont = True
        Me.cbxRoom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxRoom.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxRoom.SelectedValue = Nothing
        Me.cbxRoom.Size = New System.Drawing.Size(290, 22)
        Me.cbxRoom.TabIndex = 7
        Me.cbxRoom.Tag = ""
        Me.cbxRoom.ValueMember = Nothing
        '
        'udtDate
        '
        Me.udtDate.EditValue = Nothing
        Me.udtDate.EnterMoveNextControl = True
        Me.udtDate.Location = New System.Drawing.Point(102, 13)
        Me.udtDate.Name = "udtDate"
        Me.udtDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.udtDate.Properties.Appearance.Options.UseFont = True
        Me.udtDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.udtDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.udtDate.Size = New System.Drawing.Size(100, 22)
        Me.udtDate.TabIndex = 1
        Me.udtDate.Value = Nothing
        '
        'Label2
        '
        Me.Label2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label2.Location = New System.Drawing.Point(12, 44)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 15)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Output Mode"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label1.Location = New System.Drawing.Point(12, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Report Date"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxMode
        '
        Me.cbxMode.AllowBlank = False
        Me.cbxMode.DataSource = Nothing
        Me.cbxMode.DisplayMember = Nothing
        Me.cbxMode.EnterMoveNextControl = True
        Me.cbxMode.Location = New System.Drawing.Point(102, 41)
        Me.cbxMode.Name = "cbxMode"
        Me.cbxMode.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxMode.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxMode.Properties.Appearance.Options.UseFont = True
        Me.cbxMode.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxMode.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxMode.SelectedValue = Nothing
        Me.cbxMode.Size = New System.Drawing.Size(290, 22)
        Me.cbxMode.TabIndex = 3
        Me.cbxMode.ValueMember = Nothing
        '
        'frmParentReports
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(425, 181)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnRun)
        Me.Controls.Add(Me.btnClose)
        Me.MaximizeBox = False
        Me.Name = "frmParentReports"
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxRoom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.udtDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.udtDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxMode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents btnRun As Care.Controls.CareButton
    Friend WithEvents GroupBox1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cbxMode As Care.Controls.CareComboBox
    Friend WithEvents Label2 As Care.Controls.CareLabel
    Friend WithEvents Label1 As Care.Controls.CareLabel
    Friend WithEvents udtDate As Care.Controls.CareDateTime
    Friend WithEvents cbxRoom As Care.Controls.CareComboBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents cbxSite As Care.Controls.CareComboBox

End Class
