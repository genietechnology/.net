﻿Imports Care.Global
Imports Care.Data

Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.BandedGrid

Public Class frmCDAPEnquiry

    Dim WithEvents m_BGV As New BandedGridView

    Dim m_DT As DataTable
    Dim m_Step As String = ""
    Dim m_AgeMin As Integer = 0
    Dim m_AgeMax As Integer = 0

    Private Sub frmCDAPEnquiry_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        PopulateSteps()
    End Sub

    Private Sub PopulateSteps()

        Dim _SQL As String = "select step as 'Step', age_min as 'Min Age', age_max as 'Max Age' from CDAPSteps" & _
                             " order by step"

        cgSteps.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub PopulateCDAP(ByVal StepNo As String)

        If m_DT IsNot Nothing Then
            m_DT.Dispose()
            m_DT = Nothing
        End If

        m_DT = New DataTable("CDAP")

        Dim _SQL As String = ""

        SetupChildBand(m_BGV)

        'fetch all the areas
        _SQL = "select id, name from CDAPAreas order by seq"
        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        For Each _DR As DataRow In _DT.Rows
            SetupAreaBand(m_BGV, _DR.Item("id").ToString, _DR.Item("name").ToString)
        Next

        'now fetch all the children that fall within the selected step
        _SQL = "select id, fullname, gender, dob, datediff(month, dob, getdate()) as 'age' from Children" & _
               " where datediff(month, dob, getdate()) between " + m_AgeMin.ToString + " and " + m_AgeMax.ToString

        Dim _Children As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _Children IsNot Nothing Then

            For Each _C As DataRow In _Children.Rows

                Dim _R As DataRow = m_DT.NewRow
                _R.Item("Child_Name") = _C.Item("fullname").ToString
                _R.Item("Child_Gender") = _C.Item("gender").ToString
                _R.Item("Child_Age") = _C.Item("age").ToString

                SetCDAP(_C.Item("ID").ToString, StepNo, _R)

                m_DT.Rows.Add(_R)

            Next

        End If

        m_BGV.OptionsBehavior.Editable = False

        cgCDAP.Clear()
        cgCDAP.UnderlyingGridControl.MainView = m_BGV
        cgCDAP.UnderlyingGridControl.DataSource = m_DT

    End Sub

    Private Sub SetCDAP(ByVal ChildID As String, ByVal StepNo As String, ByRef Row As DataRow)

        Dim _SQL As String = "select c.id, c.date, a.ID as 'area_id', d.letter from ChildCDAP c" + _
                             " left join CDAPDefinitions d on d.ID = c.cdap_id" + _
                             " left join CDAPAreas a on a.name = d.area" + _
                             " where c.child_id = '" + ChildID + "'"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _Field As String = _DR.Item("area_id").ToString + ":" + _DR.Item("letter").ToString

                Try

                    Dim _Value As String = Format(CDate(_DR.Item("date")), "dd/MM/yyyy")
                    _Value += vbNewLine + "~" + _DR.Item("id").ToString

                    Row.Item(_Field) = _Value

                Catch ex As Exception
                    'field does not exist
                End Try

            Next

        End If

    End Sub

    Private Sub SetupChildBand(ByRef BandedView As BandedGridView)

        Dim _BGC As BandedGridColumn

        Dim _B As New GridBand
        _B.Caption = "Child"

        _BGC = BandedView.Columns.AddField("Child_Name")
        _BGC.Caption = "Name"
        _BGC.OwnerBand = _B
        _BGC.Visible = True

        m_DT.Columns.Add("Child_Name")

        _BGC = BandedView.Columns.AddField("Child_Gender")
        _BGC.Caption = "Gender"
        _BGC.OwnerBand = _B
        _BGC.Visible = True

        m_DT.Columns.Add("Child_Gender")

        _BGC = BandedView.Columns.AddField("Child_Age")
        _BGC.Caption = "Age"
        _BGC.OwnerBand = _B
        _BGC.Visible = True

        m_DT.Columns.Add("Child_Age")

    End Sub

    Private Sub SetupAreaBand(ByRef BandedView As BandedGridView, ByVal AreaID As String, ByVal AreaName As String)

        Dim _B As New GridBand
        _B.Caption = AreaName

        Dim _SQL As String = "select letter, name from CDAPDefinitions" + _
                             " where area = '" + AreaName + "'" + _
                             " and step = " + m_Step + _
                             " order by letter"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        For Each _DR As DataRow In _DT.Rows

            Dim _Letter As String = _DR.Item("letter").ToString
            Dim _CN As String = AreaID + ":" + _Letter

            Dim _BGC As BandedGridColumn = BandedView.Columns.AddField(_CN)
            _BGC.Caption = _Letter
            _BGC.OwnerBand = _B
            _BGC.Visible = True

            m_DT.Columns.Add(_CN)

        Next

    End Sub

    Private Sub m_BGV_DoubleClick(sender As Object, e As EventArgs) Handles m_BGV.DoubleClick
        Dim _Value As String = m_BGV.GetFocusedRowCellValue(m_BGV.FocusedColumn).ToString
        If _Value.Contains("~") Then
            Dim _ID As String = _Value.Substring(_Value.IndexOf("~") + 1)
            CDAP.DisplayCDAPDetail(New Guid(_ID))
        End If
    End Sub

    Private Sub m_BGV_RowCellStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs) Handles m_BGV.RowCellStyle

        If e.Column.FieldName.Contains(":") Then
            If e.CellValue.ToString <> "" Then
                e.Appearance.BackColor = txtGreen.BackColor
            Else
                e.Appearance.BackColor = txtRed.BackColor
            End If
        Else
            'stub
        End If

    End Sub

    Private Sub cgSteps_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles cgSteps.FocusedRowChanged
        m_Step = cgSteps.CurrentRow("Step").ToString
        m_AgeMin = ValueHandler.ConvertInteger(cgSteps.CurrentRow("Min Age"))
        m_AgeMax = ValueHandler.ConvertInteger(cgSteps.CurrentRow("Max Age"))
        PopulateCDAP(cgSteps.CurrentRow("Step").ToString)
    End Sub

End Class
