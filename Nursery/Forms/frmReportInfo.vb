﻿Imports Care.Global
Imports System.Windows.Forms
Imports Care.Shared
Imports Care.Data

Public Class frmReportInfo

    Private Sub Run()

        Cursor.Current = Cursors.WaitCursor
        btnRun.Enabled = False
        btnClose.Enabled = False

        Dim _SQL As String = ""
        Dim _ReportSQL As String = ""
        Dim _SubReports As New List(Of SubReport)

        Dim _Status As String = "Current"
        If chkBlank.Checked Then _Status = "<BLANK>"

        _SQL = "select Family.ID as 'family_id', Family.surname as 'surname', Family.letter_name as 'letter_name', Family.address as 'Address'," & _
               " Family.e_invoicing as 'e_invoicing'" & _
               " from Family" & _
               " where Family.ID in (select Children.family_id from Children where Children.status = '" & _Status & "')" & _
               " order by Family.surname, Family.id"

        _ReportSQL = _SQL

        _SQL = "select Children.family_id as 'child_family_id', Children.fullname as 'child_name', Children.gender as 'child_gender', Children.dob as 'child_dob'," & _
               " Children.paper_report as 'paper_report', Children.report_detail as 'report_detail'," & _
               " Children.med_allergies, Children.med_medication, Children.med_notes," & _
               " Children.surg_name, Children.surg_doc, Children.surg_tel," & _
               " Children.sms_milk, Children.sms_food, Children.sms_toilet, Children.sms_sleep," & _
               " (select Tariffs.Name from Tariffs where Tariffs.ID = Children.Monday) as 'monday'," & _
               " (select Tariffs.Name from Tariffs where Tariffs.ID = Children.Tuesday) as 'tuesday'," & _
               " (select Tariffs.Name from Tariffs where Tariffs.ID = Children.Wednesday) as 'wednesday'," & _
               " (select Tariffs.Name from Tariffs where Tariffs.ID = Children.Thursday) as 'thursday'," & _
               " (select Tariffs.Name from Tariffs where Tariffs.ID = Children.Friday) as 'friday'," & _
               " (select Tariffs.Name from Tariffs where Tariffs.ID = Children.Saturday) as 'saturday'," & _
               " (select Tariffs.Name from Tariffs where Tariffs.ID = Children.Sunday) as 'sunday'" & _
               " from Children" & _
               " where Children.status = '" & _Status & "'"

        Dim _SRChild As New SubReport
        _SRChild.ReportFile = "info_child.rpt"
        _SRChild.ReportData = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        _SubReports.Add(_SRChild)

        _SQL = " select Contacts.family_id as 'contact_family_id', Contacts.fullname as 'contact_name', Contacts.relationship as 'contact_rel', Contacts.password as 'contact_pwd'," & _
               " Contacts.primary_cont as 'primary_contact', Contacts.emer_cont as 'emergency_contact', Contacts.sms as 'contact_sms', Contacts.sms_name as 'contact_sms_name'," & _
               " Contacts.address as 'contact_address', Contacts.tel_home as 'contact_tel', Contacts.tel_mobile as 'contact_mobile', Contacts.email as 'contact_email'," & _
               " Contacts.job as 'contact_job', Contacts.job_title as 'contact_job_title', Contacts.job_employer as 'contact_employer', Contacts.job_address as 'contact_job_add'," & _
               " Contacts.job_tel as 'contact_job_tel', Contacts.job_email as 'contact_job_email'," & _
               " Contacts.voucher, Contacts.voucher_name, Contacts.voucher_value," & _
               " Contacts.report, Contacts.report_detail, Contacts.report_method" & _
               " from Contacts" & _
               " where Contacts.family_id in (select Children.family_id from Children where Children.status = '" & _Status & "')"

        Dim _SRContact As New SubReport
        _SRContact.ReportFile = "info_contact.rpt"
        _SRContact.ReportData = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        _SubReports.Add(_SRContact)

        Dim _Rpt As New ReportHandler
        With _Rpt
            .ReportOutput = ReportHandler.OutputMode.Preview
            .ReportName = "Information Report"
            .ReportFile = "info.rpt"
            .ReportSQL = _ReportSQL
            .SubReportList = _SubReports
            .BlankReport = True
            .RunReport(False)
        End With

        _SRChild = Nothing
        _SubReports = Nothing

        _Rpt = Nothing

        btnRun.Enabled = True
        btnClose.Enabled = True
        Cursor.Current = Cursors.Default

    End Sub

    Private Sub btnRun_Click(sender As System.Object, e As System.EventArgs) Handles btnRun.Click
        Run()
    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class
