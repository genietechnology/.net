﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChildCalc
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxWeekly = New Care.Controls.CareFrame()
        Me.Label42 = New Care.Controls.CareLabel(Me.components)
        Me.chkActive = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel12 = New Care.Controls.CareLabel(Me.components)
        Me.txtSubTotalPerSplit = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel10 = New Care.Controls.CareLabel(Me.components)
        Me.txtSubTotalWeek = New Care.Controls.CareTextBox(Me.components)
        Me.cbxInvoiceSplit = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.txtSubTotalAnnum = New Care.Controls.CareTextBox(Me.components)
        Me.txtSubTotalMonth = New Care.Controls.CareTextBox(Me.components)
        Me.mc5 = New Nursery.ManualCalculation()
        Me.mc4 = New Nursery.ManualCalculation()
        Me.mc3 = New Nursery.ManualCalculation()
        Me.mc2 = New Nursery.ManualCalculation()
        Me.cbxMc5 = New Care.Controls.CareComboBox(Me.components)
        Me.cbxMc4 = New Care.Controls.CareComboBox(Me.components)
        Me.cbxMc3 = New Care.Controls.CareComboBox(Me.components)
        Me.cbxMc2 = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel15 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel16 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.mcAnnual = New Nursery.ManualCalculation()
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.cdtDateFrom = New Care.Controls.CareDateTime(Me.components)
        Me.txtTotalPerSplit = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.txtTotalWeek = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.txtTotalAnnum = New Care.Controls.CareTextBox(Me.components)
        Me.txtTotalMonth = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        Me.btnOK = New Care.Controls.CareButton(Me.components)
        Me.CareLabel13 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.mcFunded = New Nursery.ManualCalculation()
        Me.GroupControl2 = New Care.Controls.CareFrame()
        Me.btnViewText = New Care.Controls.CareButton(Me.components)
        CType(Me.gbxWeekly, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxWeekly.SuspendLayout()
        CType(Me.chkActive.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSubTotalPerSplit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSubTotalWeek.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxInvoiceSplit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSubTotalAnnum.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSubTotalMonth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxMc5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxMc4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxMc3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxMc2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDateFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDateFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalPerSplit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalWeek.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalAnnum.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalMonth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'gbxWeekly
        '
        Me.gbxWeekly.Controls.Add(Me.Label42)
        Me.gbxWeekly.Controls.Add(Me.chkActive)
        Me.gbxWeekly.Controls.Add(Me.CareLabel7)
        Me.gbxWeekly.Controls.Add(Me.CareLabel12)
        Me.gbxWeekly.Controls.Add(Me.txtSubTotalPerSplit)
        Me.gbxWeekly.Controls.Add(Me.CareLabel10)
        Me.gbxWeekly.Controls.Add(Me.txtSubTotalWeek)
        Me.gbxWeekly.Controls.Add(Me.cbxInvoiceSplit)
        Me.gbxWeekly.Controls.Add(Me.CareLabel2)
        Me.gbxWeekly.Controls.Add(Me.txtSubTotalAnnum)
        Me.gbxWeekly.Controls.Add(Me.txtSubTotalMonth)
        Me.gbxWeekly.Controls.Add(Me.mc5)
        Me.gbxWeekly.Controls.Add(Me.mc4)
        Me.gbxWeekly.Controls.Add(Me.mc3)
        Me.gbxWeekly.Controls.Add(Me.mc2)
        Me.gbxWeekly.Controls.Add(Me.cbxMc5)
        Me.gbxWeekly.Controls.Add(Me.cbxMc4)
        Me.gbxWeekly.Controls.Add(Me.cbxMc3)
        Me.gbxWeekly.Controls.Add(Me.cbxMc2)
        Me.gbxWeekly.Controls.Add(Me.CareLabel15)
        Me.gbxWeekly.Controls.Add(Me.CareLabel16)
        Me.gbxWeekly.Controls.Add(Me.CareLabel1)
        Me.gbxWeekly.Controls.Add(Me.mcAnnual)
        Me.gbxWeekly.Controls.Add(Me.CareLabel11)
        Me.gbxWeekly.Controls.Add(Me.cdtDateFrom)
        Me.gbxWeekly.Location = New System.Drawing.Point(12, 12)
        Me.gbxWeekly.Name = "gbxWeekly"
        Me.gbxWeekly.Size = New System.Drawing.Size(960, 388)
        Me.gbxWeekly.TabIndex = 0
        Me.gbxWeekly.Text = "Annual Calculation Parameters"
        '
        'Label42
        '
        Me.Label42.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label42.Location = New System.Drawing.Point(401, 32)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(33, 15)
        Me.Label42.TabIndex = 4
        Me.Label42.Text = "Active"
        Me.Label42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkActive
        '
        Me.chkActive.EnterMoveNextControl = True
        Me.chkActive.Location = New System.Drawing.Point(440, 30)
        Me.chkActive.Name = "chkActive"
        Me.chkActive.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkActive.Properties.Appearance.Options.UseFont = True
        Me.chkActive.Size = New System.Drawing.Size(20, 19)
        Me.chkActive.TabIndex = 5
        Me.chkActive.Tag = ""
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(591, 359)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(50, 15)
        Me.CareLabel7.TabIndex = 21
        Me.CareLabel7.Text = "Sub Total"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel12
        '
        Me.CareLabel12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel12.Location = New System.Drawing.Point(17, 359)
        Me.CareLabel12.Name = "CareLabel12"
        Me.CareLabel12.Size = New System.Drawing.Size(151, 15)
        Me.CareLabel12.TabIndex = 16
        Me.CareLabel12.Text = "Sub Total as per Invoice Split"
        Me.CareLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSubTotalPerSplit
        '
        Me.txtSubTotalPerSplit.CharacterCasing = CharacterCasing.Normal
        Me.txtSubTotalPerSplit.EditValue = ""
        Me.txtSubTotalPerSplit.EnterMoveNextControl = True
        Me.txtSubTotalPerSplit.Location = New System.Drawing.Point(188, 356)
        Me.txtSubTotalPerSplit.MaxLength = 14
        Me.txtSubTotalPerSplit.Name = "txtSubTotalPerSplit"
        Me.txtSubTotalPerSplit.NumericAllowNegatives = False
        Me.txtSubTotalPerSplit.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtSubTotalPerSplit.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSubTotalPerSplit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSubTotalPerSplit.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSubTotalPerSplit.Properties.Appearance.Options.UseFont = True
        Me.txtSubTotalPerSplit.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSubTotalPerSplit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtSubTotalPerSplit.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtSubTotalPerSplit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtSubTotalPerSplit.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSubTotalPerSplit.Properties.MaxLength = 14
        Me.txtSubTotalPerSplit.Properties.ReadOnly = True
        Me.txtSubTotalPerSplit.Size = New System.Drawing.Size(77, 22)
        Me.txtSubTotalPerSplit.TabIndex = 17
        Me.txtSubTotalPerSplit.TabStop = False
        Me.txtSubTotalPerSplit.TextAlign = HorizontalAlignment.Right
        Me.txtSubTotalPerSplit.ToolTipText = ""
        Me.txtSubTotalPerSplit.UseWaitCursor = True
        '
        'CareLabel10
        '
        Me.CareLabel10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.CareLabel10.Location = New System.Drawing.Point(647, 335)
        Me.CareLabel10.Name = "CareLabel10"
        Me.CareLabel10.Size = New System.Drawing.Size(77, 15)
        Me.CareLabel10.TabIndex = 18
        Me.CareLabel10.Text = "per week"
        Me.CareLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtSubTotalWeek
        '
        Me.txtSubTotalWeek.CharacterCasing = CharacterCasing.Normal
        Me.txtSubTotalWeek.EditValue = ""
        Me.txtSubTotalWeek.EnterMoveNextControl = True
        Me.txtSubTotalWeek.Location = New System.Drawing.Point(647, 356)
        Me.txtSubTotalWeek.MaxLength = 14
        Me.txtSubTotalWeek.Name = "txtSubTotalWeek"
        Me.txtSubTotalWeek.NumericAllowNegatives = False
        Me.txtSubTotalWeek.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtSubTotalWeek.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSubTotalWeek.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSubTotalWeek.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSubTotalWeek.Properties.Appearance.Options.UseFont = True
        Me.txtSubTotalWeek.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSubTotalWeek.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtSubTotalWeek.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtSubTotalWeek.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtSubTotalWeek.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSubTotalWeek.Properties.MaxLength = 14
        Me.txtSubTotalWeek.Properties.ReadOnly = True
        Me.txtSubTotalWeek.Size = New System.Drawing.Size(77, 22)
        Me.txtSubTotalWeek.TabIndex = 22
        Me.txtSubTotalWeek.TabStop = False
        Me.txtSubTotalWeek.TextAlign = HorizontalAlignment.Right
        Me.txtSubTotalWeek.ToolTipText = ""
        '
        'cbxInvoiceSplit
        '
        Me.cbxInvoiceSplit.AllowBlank = False
        Me.cbxInvoiceSplit.DataSource = Nothing
        Me.cbxInvoiceSplit.DisplayMember = Nothing
        Me.cbxInvoiceSplit.EnterMoveNextControl = True
        Me.cbxInvoiceSplit.Location = New System.Drawing.Point(275, 29)
        Me.cbxInvoiceSplit.Name = "cbxInvoiceSplit"
        Me.cbxInvoiceSplit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxInvoiceSplit.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxInvoiceSplit.Properties.Appearance.Options.UseFont = True
        Me.cbxInvoiceSplit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxInvoiceSplit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxInvoiceSplit.SelectedValue = Nothing
        Me.cbxInvoiceSplit.Size = New System.Drawing.Size(110, 22)
        Me.cbxInvoiceSplit.TabIndex = 3
        Me.cbxInvoiceSplit.Tag = "AEM"
        Me.cbxInvoiceSplit.ValueMember = Nothing
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(178, 32)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(91, 15)
        Me.CareLabel2.TabIndex = 2
        Me.CareLabel2.Text = "Invoicing Split by"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSubTotalAnnum
        '
        Me.txtSubTotalAnnum.CharacterCasing = CharacterCasing.Normal
        Me.txtSubTotalAnnum.EnterMoveNextControl = True
        Me.txtSubTotalAnnum.Location = New System.Drawing.Point(813, 356)
        Me.txtSubTotalAnnum.MaxLength = 14
        Me.txtSubTotalAnnum.Name = "txtSubTotalAnnum"
        Me.txtSubTotalAnnum.NumericAllowNegatives = False
        Me.txtSubTotalAnnum.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtSubTotalAnnum.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSubTotalAnnum.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSubTotalAnnum.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSubTotalAnnum.Properties.Appearance.Options.UseFont = True
        Me.txtSubTotalAnnum.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSubTotalAnnum.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtSubTotalAnnum.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtSubTotalAnnum.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtSubTotalAnnum.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSubTotalAnnum.Properties.MaxLength = 14
        Me.txtSubTotalAnnum.Properties.ReadOnly = True
        Me.txtSubTotalAnnum.Size = New System.Drawing.Size(77, 22)
        Me.txtSubTotalAnnum.TabIndex = 24
        Me.txtSubTotalAnnum.TabStop = False
        Me.txtSubTotalAnnum.TextAlign = HorizontalAlignment.Right
        Me.txtSubTotalAnnum.ToolTipText = ""
        '
        'txtSubTotalMonth
        '
        Me.txtSubTotalMonth.CharacterCasing = CharacterCasing.Normal
        Me.txtSubTotalMonth.EnterMoveNextControl = True
        Me.txtSubTotalMonth.Location = New System.Drawing.Point(730, 356)
        Me.txtSubTotalMonth.MaxLength = 14
        Me.txtSubTotalMonth.Name = "txtSubTotalMonth"
        Me.txtSubTotalMonth.NumericAllowNegatives = False
        Me.txtSubTotalMonth.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtSubTotalMonth.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSubTotalMonth.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSubTotalMonth.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSubTotalMonth.Properties.Appearance.Options.UseFont = True
        Me.txtSubTotalMonth.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSubTotalMonth.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtSubTotalMonth.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtSubTotalMonth.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtSubTotalMonth.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSubTotalMonth.Properties.MaxLength = 14
        Me.txtSubTotalMonth.Properties.ReadOnly = True
        Me.txtSubTotalMonth.Size = New System.Drawing.Size(77, 22)
        Me.txtSubTotalMonth.TabIndex = 23
        Me.txtSubTotalMonth.TabStop = False
        Me.txtSubTotalMonth.TextAlign = HorizontalAlignment.Right
        Me.txtSubTotalMonth.ToolTipText = ""
        '
        'mc5
        '
        Me.mc5.CalcForeColor = System.Drawing.Color.Empty
        Me.mc5.Location = New System.Drawing.Point(94, 276)
        Me.mc5.Multiplier = "days per week"
        Me.mc5.Name = "mc5"
        Me.mc5.Per = "day"
        Me.mc5.Rate = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.mc5.Size = New System.Drawing.Size(854, 46)
        Me.mc5.TabIndex = 15
        Me.mc5.Units = New Decimal(New Integer() {0, 0, 0, 0})
        Me.mc5.Weeks = New Decimal(New Integer() {0, 0, 0, 131072})
        '
        'mc4
        '
        Me.mc4.CalcForeColor = System.Drawing.Color.Empty
        Me.mc4.Location = New System.Drawing.Point(94, 222)
        Me.mc4.Multiplier = "days per week"
        Me.mc4.Name = "mc4"
        Me.mc4.Per = "day"
        Me.mc4.Rate = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.mc4.Size = New System.Drawing.Size(854, 46)
        Me.mc4.TabIndex = 13
        Me.mc4.Units = New Decimal(New Integer() {0, 0, 0, 0})
        Me.mc4.Weeks = New Decimal(New Integer() {0, 0, 0, 131072})
        '
        'mc3
        '
        Me.mc3.CalcForeColor = System.Drawing.Color.Empty
        Me.mc3.Location = New System.Drawing.Point(94, 166)
        Me.mc3.Multiplier = "days per week"
        Me.mc3.Name = "mc3"
        Me.mc3.Per = "day"
        Me.mc3.Rate = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.mc3.Size = New System.Drawing.Size(854, 46)
        Me.mc3.TabIndex = 11
        Me.mc3.Units = New Decimal(New Integer() {0, 0, 0, 0})
        Me.mc3.Weeks = New Decimal(New Integer() {0, 0, 0, 131072})
        '
        'mc2
        '
        Me.mc2.CalcForeColor = System.Drawing.Color.Empty
        Me.mc2.Location = New System.Drawing.Point(94, 112)
        Me.mc2.Multiplier = "days per week"
        Me.mc2.Name = "mc2"
        Me.mc2.Per = "day"
        Me.mc2.Rate = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.mc2.Size = New System.Drawing.Size(854, 46)
        Me.mc2.TabIndex = 9
        Me.mc2.Units = New Decimal(New Integer() {0, 0, 0, 0})
        Me.mc2.Weeks = New Decimal(New Integer() {0, 0, 0, 131072})
        '
        'cbxMc5
        '
        Me.cbxMc5.AllowBlank = False
        Me.cbxMc5.DataSource = Nothing
        Me.cbxMc5.DisplayMember = Nothing
        Me.cbxMc5.EnterMoveNextControl = True
        Me.cbxMc5.Location = New System.Drawing.Point(12, 300)
        Me.cbxMc5.Name = "cbxMc5"
        Me.cbxMc5.Properties.AccessibleName = "Gender"
        Me.cbxMc5.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxMc5.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxMc5.Properties.Appearance.Options.UseFont = True
        Me.cbxMc5.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxMc5.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxMc5.SelectedValue = Nothing
        Me.cbxMc5.Size = New System.Drawing.Size(76, 22)
        Me.cbxMc5.TabIndex = 14
        Me.cbxMc5.Tag = "AEBM"
        Me.cbxMc5.ValueMember = Nothing
        '
        'cbxMc4
        '
        Me.cbxMc4.AllowBlank = False
        Me.cbxMc4.DataSource = Nothing
        Me.cbxMc4.DisplayMember = Nothing
        Me.cbxMc4.EnterMoveNextControl = True
        Me.cbxMc4.Location = New System.Drawing.Point(12, 246)
        Me.cbxMc4.Name = "cbxMc4"
        Me.cbxMc4.Properties.AccessibleName = "Gender"
        Me.cbxMc4.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxMc4.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxMc4.Properties.Appearance.Options.UseFont = True
        Me.cbxMc4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxMc4.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxMc4.SelectedValue = Nothing
        Me.cbxMc4.Size = New System.Drawing.Size(76, 22)
        Me.cbxMc4.TabIndex = 12
        Me.cbxMc4.Tag = "AEBM"
        Me.cbxMc4.ValueMember = Nothing
        '
        'cbxMc3
        '
        Me.cbxMc3.AllowBlank = False
        Me.cbxMc3.DataSource = Nothing
        Me.cbxMc3.DisplayMember = Nothing
        Me.cbxMc3.EnterMoveNextControl = True
        Me.cbxMc3.Location = New System.Drawing.Point(12, 190)
        Me.cbxMc3.Name = "cbxMc3"
        Me.cbxMc3.Properties.AccessibleName = "Gender"
        Me.cbxMc3.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxMc3.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxMc3.Properties.Appearance.Options.UseFont = True
        Me.cbxMc3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxMc3.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxMc3.SelectedValue = Nothing
        Me.cbxMc3.Size = New System.Drawing.Size(76, 22)
        Me.cbxMc3.TabIndex = 10
        Me.cbxMc3.Tag = "AEBM"
        Me.cbxMc3.ValueMember = Nothing
        '
        'cbxMc2
        '
        Me.cbxMc2.AllowBlank = False
        Me.cbxMc2.DataSource = Nothing
        Me.cbxMc2.DisplayMember = Nothing
        Me.cbxMc2.EnterMoveNextControl = True
        Me.cbxMc2.Location = New System.Drawing.Point(12, 135)
        Me.cbxMc2.Name = "cbxMc2"
        Me.cbxMc2.Properties.AccessibleName = "Gender"
        Me.cbxMc2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxMc2.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxMc2.Properties.Appearance.Options.UseFont = True
        Me.cbxMc2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxMc2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxMc2.SelectedValue = Nothing
        Me.cbxMc2.Size = New System.Drawing.Size(76, 22)
        Me.cbxMc2.TabIndex = 8
        Me.cbxMc2.Tag = "AEBM"
        Me.cbxMc2.ValueMember = Nothing
        '
        'CareLabel15
        '
        Me.CareLabel15.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.CareLabel15.Location = New System.Drawing.Point(813, 335)
        Me.CareLabel15.Name = "CareLabel15"
        Me.CareLabel15.Size = New System.Drawing.Size(77, 15)
        Me.CareLabel15.TabIndex = 20
        Me.CareLabel15.Text = "per annum"
        Me.CareLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CareLabel16
        '
        Me.CareLabel16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel16.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.CareLabel16.Location = New System.Drawing.Point(730, 335)
        Me.CareLabel16.Name = "CareLabel16"
        Me.CareLabel16.Size = New System.Drawing.Size(77, 15)
        Me.CareLabel16.TabIndex = 19
        Me.CareLabel16.Text = "per month"
        Me.CareLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(12, 84)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(38, 15)
        Me.CareLabel1.TabIndex = 6
        Me.CareLabel1.Text = "Annual"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'mcAnnual
        '
        Me.mcAnnual.CalcForeColor = System.Drawing.Color.Empty
        Me.mcAnnual.Location = New System.Drawing.Point(94, 57)
        Me.mcAnnual.Multiplier = "days per week"
        Me.mcAnnual.Name = "mcAnnual"
        Me.mcAnnual.Per = "day"
        Me.mcAnnual.Rate = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.mcAnnual.Size = New System.Drawing.Size(862, 49)
        Me.mcAnnual.TabIndex = 7
        Me.mcAnnual.Units = New Decimal(New Integer() {0, 0, 0, 0})
        Me.mcAnnual.Weeks = New Decimal(New Integer() {0, 0, 0, 131072})
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(12, 32)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(28, 15)
        Me.CareLabel11.TabIndex = 0
        Me.CareLabel11.Text = "From"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtDateFrom
        '
        Me.cdtDateFrom.EditValue = Nothing
        Me.cdtDateFrom.EnterMoveNextControl = True
        Me.cdtDateFrom.Location = New System.Drawing.Point(59, 29)
        Me.cdtDateFrom.Name = "cdtDateFrom"
        Me.cdtDateFrom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtDateFrom.Properties.Appearance.Options.UseFont = True
        Me.cdtDateFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDateFrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtDateFrom.Size = New System.Drawing.Size(100, 22)
        Me.cdtDateFrom.TabIndex = 1
        Me.cdtDateFrom.Tag = "AEM"
        Me.cdtDateFrom.Value = Nothing
        '
        'txtTotalPerSplit
        '
        Me.txtTotalPerSplit.CharacterCasing = CharacterCasing.Normal
        Me.txtTotalPerSplit.EditValue = ""
        Me.txtTotalPerSplit.EnterMoveNextControl = True
        Me.txtTotalPerSplit.Location = New System.Drawing.Point(188, 38)
        Me.txtTotalPerSplit.MaxLength = 14
        Me.txtTotalPerSplit.Name = "txtTotalPerSplit"
        Me.txtTotalPerSplit.NumericAllowNegatives = False
        Me.txtTotalPerSplit.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtTotalPerSplit.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTotalPerSplit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtTotalPerSplit.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalPerSplit.Properties.Appearance.Options.UseFont = True
        Me.txtTotalPerSplit.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTotalPerSplit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtTotalPerSplit.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtTotalPerSplit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtTotalPerSplit.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTotalPerSplit.Properties.MaxLength = 14
        Me.txtTotalPerSplit.Properties.ReadOnly = True
        Me.txtTotalPerSplit.Size = New System.Drawing.Size(77, 22)
        Me.txtTotalPerSplit.TabIndex = 1
        Me.txtTotalPerSplit.TabStop = False
        Me.txtTotalPerSplit.TextAlign = HorizontalAlignment.Right
        Me.txtTotalPerSplit.ToolTipText = ""
        Me.txtTotalPerSplit.UseWaitCursor = True
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.CareLabel6.Location = New System.Drawing.Point(647, 27)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(77, 15)
        Me.CareLabel6.TabIndex = 2
        Me.CareLabel6.Text = "per week"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtTotalWeek
        '
        Me.txtTotalWeek.CharacterCasing = CharacterCasing.Normal
        Me.txtTotalWeek.EditValue = ""
        Me.txtTotalWeek.EnterMoveNextControl = True
        Me.txtTotalWeek.Location = New System.Drawing.Point(647, 48)
        Me.txtTotalWeek.MaxLength = 14
        Me.txtTotalWeek.Name = "txtTotalWeek"
        Me.txtTotalWeek.NumericAllowNegatives = False
        Me.txtTotalWeek.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtTotalWeek.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTotalWeek.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtTotalWeek.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalWeek.Properties.Appearance.Options.UseFont = True
        Me.txtTotalWeek.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTotalWeek.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtTotalWeek.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtTotalWeek.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtTotalWeek.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTotalWeek.Properties.MaxLength = 14
        Me.txtTotalWeek.Properties.ReadOnly = True
        Me.txtTotalWeek.Size = New System.Drawing.Size(77, 22)
        Me.txtTotalWeek.TabIndex = 6
        Me.txtTotalWeek.TabStop = False
        Me.txtTotalWeek.TextAlign = HorizontalAlignment.Right
        Me.txtTotalWeek.ToolTipText = ""
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.CareLabel4.Location = New System.Drawing.Point(813, 27)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(77, 15)
        Me.CareLabel4.TabIndex = 4
        Me.CareLabel4.Text = "per annum"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.CareLabel5.Location = New System.Drawing.Point(730, 27)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(77, 15)
        Me.CareLabel5.TabIndex = 3
        Me.CareLabel5.Text = "per month"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtTotalAnnum
        '
        Me.txtTotalAnnum.CharacterCasing = CharacterCasing.Normal
        Me.txtTotalAnnum.EnterMoveNextControl = True
        Me.txtTotalAnnum.Location = New System.Drawing.Point(813, 48)
        Me.txtTotalAnnum.MaxLength = 14
        Me.txtTotalAnnum.Name = "txtTotalAnnum"
        Me.txtTotalAnnum.NumericAllowNegatives = False
        Me.txtTotalAnnum.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtTotalAnnum.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTotalAnnum.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtTotalAnnum.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalAnnum.Properties.Appearance.Options.UseFont = True
        Me.txtTotalAnnum.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTotalAnnum.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtTotalAnnum.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtTotalAnnum.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtTotalAnnum.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTotalAnnum.Properties.MaxLength = 14
        Me.txtTotalAnnum.Properties.ReadOnly = True
        Me.txtTotalAnnum.Size = New System.Drawing.Size(77, 22)
        Me.txtTotalAnnum.TabIndex = 8
        Me.txtTotalAnnum.TabStop = False
        Me.txtTotalAnnum.TextAlign = HorizontalAlignment.Right
        Me.txtTotalAnnum.ToolTipText = ""
        '
        'txtTotalMonth
        '
        Me.txtTotalMonth.CharacterCasing = CharacterCasing.Normal
        Me.txtTotalMonth.EnterMoveNextControl = True
        Me.txtTotalMonth.Location = New System.Drawing.Point(730, 48)
        Me.txtTotalMonth.MaxLength = 14
        Me.txtTotalMonth.Name = "txtTotalMonth"
        Me.txtTotalMonth.NumericAllowNegatives = False
        Me.txtTotalMonth.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtTotalMonth.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTotalMonth.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtTotalMonth.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalMonth.Properties.Appearance.Options.UseFont = True
        Me.txtTotalMonth.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTotalMonth.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtTotalMonth.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtTotalMonth.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtTotalMonth.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTotalMonth.Properties.MaxLength = 14
        Me.txtTotalMonth.Properties.ReadOnly = True
        Me.txtTotalMonth.Size = New System.Drawing.Size(77, 22)
        Me.txtTotalMonth.TabIndex = 7
        Me.txtTotalMonth.TabStop = False
        Me.txtTotalMonth.TextAlign = HorizontalAlignment.Right
        Me.txtTotalMonth.ToolTipText = ""
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(17, 50)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(69, 15)
        Me.CareLabel3.TabIndex = 0
        Me.CareLabel3.Text = "Less Funding"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.DialogResult = DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(892, 577)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(80, 22)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(806, 577)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(80, 22)
        Me.btnOK.TabIndex = 3
        Me.btnOK.Text = "OK"
        '
        'CareLabel13
        '
        Me.CareLabel13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel13.Location = New System.Drawing.Point(17, 41)
        Me.CareLabel13.Name = "CareLabel13"
        Me.CareLabel13.Size = New System.Drawing.Size(138, 15)
        Me.CareLabel13.TabIndex = 0
        Me.CareLabel13.Text = "TOTAL as per Invoice Split"
        Me.CareLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(604, 51)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(37, 15)
        Me.CareLabel8.TabIndex = 5
        Me.CareLabel8.Text = "TOTAL"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.mcFunded)
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 406)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(960, 79)
        Me.GroupControl1.TabIndex = 1
        Me.GroupControl1.Text = "Funding"
        '
        'mcFunded
        '
        Me.mcFunded.CalcForeColor = System.Drawing.Color.Empty
        Me.mcFunded.Location = New System.Drawing.Point(94, 24)
        Me.mcFunded.Multiplier = "days per week"
        Me.mcFunded.Name = "mcFunded"
        Me.mcFunded.Per = "day"
        Me.mcFunded.Rate = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.mcFunded.Size = New System.Drawing.Size(854, 49)
        Me.mcFunded.TabIndex = 1
        Me.mcFunded.Units = New Decimal(New Integer() {0, 0, 0, 0})
        Me.mcFunded.Weeks = New Decimal(New Integer() {0, 0, 0, 131072})
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.CareLabel8)
        Me.GroupControl2.Controls.Add(Me.txtTotalAnnum)
        Me.GroupControl2.Controls.Add(Me.txtTotalMonth)
        Me.GroupControl2.Controls.Add(Me.CareLabel13)
        Me.GroupControl2.Controls.Add(Me.CareLabel5)
        Me.GroupControl2.Controls.Add(Me.CareLabel4)
        Me.GroupControl2.Controls.Add(Me.txtTotalWeek)
        Me.GroupControl2.Controls.Add(Me.CareLabel6)
        Me.GroupControl2.Controls.Add(Me.txtTotalPerSplit)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 491)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(960, 80)
        Me.GroupControl2.TabIndex = 2
        Me.GroupControl2.Text = "TOTAL"
        '
        'btnViewText
        '
        Me.btnViewText.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnViewText.Appearance.Options.UseFont = True
        Me.btnViewText.Location = New System.Drawing.Point(12, 577)
        Me.btnViewText.Name = "btnViewText"
        Me.btnViewText.Size = New System.Drawing.Size(168, 22)
        Me.btnViewText.TabIndex = 5
        Me.btnViewText.Text = "View Calculation Text"
        '
        'frmChildCalc
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(984, 606)
        Me.Controls.Add(Me.btnViewText)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.gbxWeekly)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmChildCalc"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = ""
        CType(Me.gbxWeekly, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxWeekly.ResumeLayout(False)
        Me.gbxWeekly.PerformLayout()
        CType(Me.chkActive.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSubTotalPerSplit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSubTotalWeek.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxInvoiceSplit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSubTotalAnnum.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSubTotalMonth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxMc5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxMc4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxMc3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxMc2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDateFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDateFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalPerSplit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalWeek.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalAnnum.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalMonth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxWeekly As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents cdtDateFrom As Care.Controls.CareDateTime
    Friend WithEvents mcAnnual As Nursery.ManualCalculation
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents mcFunded As Nursery.ManualCalculation
    Friend WithEvents CareLabel15 As Care.Controls.CareLabel
    Friend WithEvents CareLabel16 As Care.Controls.CareLabel
    Friend WithEvents txtTotalAnnum As Care.Controls.CareTextBox
    Friend WithEvents txtTotalMonth As Care.Controls.CareTextBox
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents cbxMc5 As Care.Controls.CareComboBox
    Friend WithEvents cbxMc4 As Care.Controls.CareComboBox
    Friend WithEvents cbxMc3 As Care.Controls.CareComboBox
    Friend WithEvents cbxMc2 As Care.Controls.CareComboBox
    Friend WithEvents mc5 As Nursery.ManualCalculation
    Friend WithEvents mc4 As Nursery.ManualCalculation
    Friend WithEvents mc3 As Nursery.ManualCalculation
    Friend WithEvents mc2 As Nursery.ManualCalculation
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents txtSubTotalAnnum As Care.Controls.CareTextBox
    Friend WithEvents txtSubTotalMonth As Care.Controls.CareTextBox
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents txtTotalWeek As Care.Controls.CareTextBox
    Friend WithEvents cbxInvoiceSplit As Care.Controls.CareComboBox
    Friend WithEvents txtTotalPerSplit As Care.Controls.CareTextBox
    Friend WithEvents txtSubTotalPerSplit As Care.Controls.CareTextBox
    Friend WithEvents CareLabel10 As Care.Controls.CareLabel
    Friend WithEvents txtSubTotalWeek As Care.Controls.CareTextBox
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents CareLabel13 As Care.Controls.CareLabel
    Friend WithEvents CareLabel12 As Care.Controls.CareLabel
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label42 As Care.Controls.CareLabel
    Friend WithEvents chkActive As Care.Controls.CareCheckBox
    Friend WithEvents btnViewText As Care.Controls.CareButton
End Class
