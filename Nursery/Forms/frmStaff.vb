﻿

Imports Care.Global
Imports Care.Shared
Imports Care.Data
Imports Care.TAAC

Public Class frmStaff

    Private m_Staff As Business.Staff
    Private WithEvents m_Controller As Controller

    Private m_Paxton As Boolean = False
    Private m_PaxtonConnected As Boolean = False
    Private m_PayrollIntegration As String = ""

    Private m_DrillDownStaffID As Guid = Nothing
    Private m_DrillDown As Boolean = False
    Private m_DrillDownShifts As Boolean = False

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub New(ByVal StaffID As Guid, Optional ByVal Shifts As Boolean = False)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        m_DrillDownStaffID = StaffID
        m_DrillDown = True
        m_DrillDownShifts = Shifts

    End Sub

    Private Sub frmStaff_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lblAge.Text = ""
        lblYearsService.Text = ""
        lblYearsLeft.Text = ""

        Session.SetProgressMessage("Populating Lists...")

        cbxSite.PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)

        With cbxStatus
            .PopulateStart()
            .AddItem("Current", "C")
            .AddItem("Leaver", "X")
            .PopulateEnd()
        End With

        With cbxQualLevel
            .PopulateStart()
            .AddItem("Unqualified")
            .AddItem("Level 1")
            .AddItem("Level 2")
            .AddItem("Level 3")
            .AddItem("Level 4")
            .AddItem("Level 5")
            .AddItem("Level 6")
            .AddItem("Level 7")
            .PopulateEnd()
        End With

        cbxContractType.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Staff Contract Type"))

        With cbxHolsFrom
            .PopulateStart()
            .AddItem("January")
            .AddItem("February")
            .AddItem("March")
            .AddItem("April")
            .AddItem("May")
            .AddItem("June")
            .AddItem("July")
            .AddItem("August")
            .AddItem("September")
            .AddItem("October")
            .AddItem("November")
            .AddItem("December")
            .PopulateEnd()
        End With

        With cbxSalaryUnit
            .PopulateStart()
            .AddItem("per Hour")
            .AddItem("per Annum")
            .PopulateEnd()
        End With

        cbxAbsenceType.AllowBlank = True
        cbxAbsenceType.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Absence Type"))
        cbxAbsenceType.SelectedValue = 0

        cbxDepartment.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Department"))
        cbxManager.PopulateWithSQL(Session.ConnectionString, Business.Staff.LineManagerSQL)

        cbxReligion.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Religion"))
        ccxLanguages.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Language"))
        ccxNationality.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Nationality"))
        cbxEthnicity.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Ethnicity"))

        cbxDBSCheckType.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("DBS Check Type"))

        Session.HideProgressBar()

        tabMain.DisableTabs()
        DisableButtons()

        m_Paxton = ParameterHandler.ReturnBoolean("PAXTON")
        If m_Paxton Then

            Session.SetProgressMessage("Connecting to Net2 Server...")

            Dim _IP As String = ParameterHandler.ReturnString("PAXTONSERVER")
            Dim _User As String = ParameterHandler.ReturnString("PAXTONUSER")
            Dim _Password As String = ParameterHandler.ReturnString("PAXTONPASSWORD")

            Dim _Errors As String = ""
            m_Controller = New Controller(True, _IP, _User, _Password)

            If m_Controller.Connect(_Errors) Then

                m_PaxtonConnected = True

                cbxPaxtonAccessLevel.PopulateWithDictionary(m_Controller.AccessLevels)
                cbxPaxtonDepartment.PopulateWithDictionary(m_Controller.Departments)

                Session.SetProgressMessage("Connected to Net2 Server.", 3)

            Else
                Session.SetProgressMessage("Unable to Connect to Net2 Server.", 3)
            End If

            gbxPaxton.Enabled = m_PaxtonConnected

        Else
            gbxPaxton.Enabled = False
        End If

        m_PayrollIntegration = ParameterHandler.ReturnString("PAYROLL")
        If m_PayrollIntegration = "" Then
            gbxPayroll.Enabled = False
        Else
            gbxPayroll.Enabled = True
        End If

        If Not m_DrillDownStaffID = Nothing Then
            DisplayRecord(m_DrillDownStaffID)
            If m_DrillDownShifts Then tabMain.SelectedTabPage = tabShifts
        End If

        PictureBox.Enabled = False
        btnPIN.Enabled = False
        btnRecruit.Enabled = False

    End Sub

    Private Sub frmStaff_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        If m_DrillDown Then
            ToolbarMode = ToolbarEnum.EditOnly
        End If
    End Sub

    Private Sub DisplayRecord(ByVal StaffID As Guid)

        MyBase.RecordID = StaffID
        MyBase.RecordPopulated = True

        lblAge.Text = ""

        m_Staff = Business.Staff.RetreiveByID(StaffID)
        bs.DataSource = m_Staff

        txtFullname.Text = m_Staff._Fullname

        cbxSite.Text = m_Staff._SiteName
        PopulateRooms()

        cbxGroup.Text = m_Staff._GroupName

        txtTelHome.Text = m_Staff._TelHome
        txtTelMobile.Text = m_Staff._TelMobile
        txtEmail.Text = m_Staff._Email

        txtAddress.Text = m_Staff._Address
        txtAddress.PostCode = m_Staff._Postcode

        txtTouchPIN.Text = m_Staff._TouchPin

        txtPaxtonUserID.Text = m_Staff._ClockNo
        cbxPaxtonAccessLevel.SelectedValue = ValueHandler.ConvertInteger(m_Staff._ClockPerms)
        cbxPaxtonDepartment.SelectedValue = ValueHandler.ConvertInteger(m_Staff._ClockGroup)

        If txtPaxtonUserID.Text = "" Then
            btnPaxtonUpdate.Text = "Create Paxton User"
        Else
            btnPaxtonUpdate.Text = "Update Paxton User"
        End If

        PictureBox.Enabled = False
        If m_Staff._Photo.HasValue Then
            PictureBox.DocumentID = m_Staff._Photo
            PictureBox.Image = DocumentHandler.GetImageByID(m_Staff._Photo)
        Else
            PictureBox.DocumentID = Nothing
            PictureBox.Clear()
        End If

        DisplayAge()
        DisplayYearsService()
        DisplayYearsLeft()

        tabMain.EnableTabs()
        tabMain.FirstPage()
        DisableButtons()

        SetRecruitmentInformation()

        Me.Text = "Staff - " + m_Staff._Fullname + ", " + m_Staff._GroupName

    End Sub

    Protected Overrides Sub AfterAcceptChanges()

        MyBase.AfterAcceptChanges()
        PictureBox.Enabled = False
        ToggleGrids(False)

        btnPIN.Enabled = False
        btnPaxtonDisable.Enabled = False
        btnPaxtonLink.Enabled = False
        btnPaxtonUpdate.Enabled = False

    End Sub

    Protected Overrides Sub AfterCancelChanges()

        MyBase.AfterCancelChanges()
        PictureBox.Enabled = False
        ToggleGrids(False)

        btnPIN.Enabled = False
        btnPaxtonDisable.Enabled = False
        btnPaxtonLink.Enabled = False
        btnPaxtonUpdate.Enabled = False

    End Sub

    Protected Overrides Function BeforeAdd() As Boolean
        Return True
    End Function

    Protected Overrides Sub AfterAdd()

        MyBase.AfterAdd()

        Me.Text = "Create New Staff Record"
        txtFullname.Text = ""
        lblAge.Text = ""

        m_Staff._Photo = Nothing
        PictureBox.Clear()

        btnPIN.Enabled = True
        gbxPaxton.Enabled = False

        txtForename.Focus()

    End Sub

    Protected Overrides Sub AfterEdit()

        MyBase.AfterEdit()

        Select Case tabMain.SelectedTabPage.Name

            Case "tabPersonal"
                PictureBox.Enabled = True
                btnPIN.Enabled = True
                txtForename.Focus()

            Case "tabCar"

                gbxPaxton.Enabled = m_PaxtonConnected
                btnPaxtonLink.Enabled = m_PaxtonConnected
                btnPaxtonDisable.Enabled = m_PaxtonConnected
                btnPaxtonUpdate.Enabled = m_PaxtonConnected

                If m_PaxtonConnected Then
                    cbxPaxtonAccessLevel.SelectedIndex = 0
                    cbxPaxtonDepartment.SelectedIndex = 0
                End If

                udtCRBLast.Focus()

            Case "tabTimesheets"
                txtSalary.Focus()

        End Select

        ToggleGrids(True)

    End Sub

    Protected Overrides Sub FindRecord()

        Dim _SQL As String = ""

        _SQL += "select status as 'Status', fullname as 'Name', site_name as 'Site', group_name as 'Group', job_title as 'Job Title',"
        _SQL += " contract_type as 'Contract', line_manager as 'Line Manager', keyworker as 'Key Person', qual_level as 'Level', clock_no as 'Clock No',"
        _SQL += " (select top 1 p.wc from StaffPrefHours p where p.staff_id = Staff.ID order by p.wc desc) as 'Latest Pattern'"
        _SQL += " from Staff"

        Dim _ReturnValue As String = ""
        Dim _Find As New GenericFind
        With _Find
            .Caption = "Find Staff"
            .ParentForm = Me
            .PopulateOnLoad = True
            .ConnectionString = Session.ConnectionString
            .GridSQL = _SQL
            .GridOrderBy = "order by forename, surname"
            .ReturnField = "ID"
            .GridWhereClause = " and site_id not in (select site_id from SiteExcludedUsers where user_id = '" + Session.CurrentUser.ID.ToString + "')"
            .Option1Caption = "Name"
            .Option1Field = "fullname"
            .Option2Caption = "Group"
            .Option2Field = "group_name"
            .FormWidth = 900
            .Show()
            _ReturnValue = .ReturnValue
        End With

        If _ReturnValue <> "" Then
            DisplayRecord(New Guid(_ReturnValue))
        End If

    End Sub

    Private Sub DisableButtons()
        btnPaxtonLink.Enabled = False
        btnPaxtonDisable.Enabled = False
        btnPaxtonUpdate.Enabled = False
    End Sub

    Protected Overrides Function BeforeCommitUpdate() As Boolean

        'paxton stuff
        If m_Paxton AndAlso tabMain.SelectedTabPage.Name = "tabCar" Then

            If cbxPaxtonAccessLevel.SelectedIndex < 0 Then
                CareMessage("Please select the Access Level for this user.", MessageBoxIcon.Exclamation, "Mandatory Field")
                Return False
            End If

            If cbxPaxtonDepartment.SelectedIndex < 0 Then
                CareMessage("Please select the Department for this user.", MessageBoxIcon.Exclamation, "Mandatory Field")
                Return False
            End If

        End If

        Return True

    End Function

    Protected Overrides Function BeforeDelete() As Boolean

        'check if the staff record is associated with any children
        Dim _SQL As String = "select count(*) as 'Count' from Children" &
                             " where status in ('Current', 'On Waiting List')" &
                             " and keyworker_id = '" & m_Staff._ID.ToString & "'"

        Dim _Count As Integer = ValueHandler.ConvertInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))
        If _Count > 0 Then
            CareMessage("There are " + _Count.ToString + " children records associated with this Keyworker." + vbCrLf + "Delete Aborted.", MessageBoxIcon.Exclamation, "Delete Record")
            Return False
        Else
            Return True
        End If

    End Function

    Protected Overrides Sub CommitDelete()

        If InputBox("Please type DELETE to continue.", "Delete Staff Record").ToUpper = "DELETE" Then

            Dim _SQL As String = ""

            _SQL = "delete from Staff where ID = '" + m_Staff._ID.Value.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

            _SQL = "delete from StaffAbsence where ID = '" + m_Staff._ID.Value.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

            _SQL = "delete from StaffAppDis where ID = '" + m_Staff._ID.Value.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

            _SQL = "delete from StaffClockIn where ID = '" + m_Staff._ID.Value.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

            _SQL = "delete from StaffQual where ID = '" + m_Staff._ID.Value.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

            _SQL = "delete from StaffTrain where ID = '" + m_Staff._ID.Value.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

            m_Staff = New Business.Staff
            bs.DataSource = m_Staff

            Me.Text = "Staff"

        End If
    End Sub

    Protected Overrides Sub CommitUpdate()

        m_Staff = CType(bs.Item(bs.Position), Business.Staff)

        Select Case tabMain.SelectedTabPage.Name

            Case "tabPersonal"

                m_Staff._SiteId = New Guid(cbxSite.SelectedValue.ToString)
                m_Staff._SiteName = cbxSite.Text

                If cbxGroup.Text = "" Then
                    m_Staff._GroupId = Nothing
                    m_Staff._GroupName = ""
                Else
                    m_Staff._GroupId = New Guid(cbxGroup.SelectedValue.ToString)
                    m_Staff._GroupName = cbxGroup.Text
                End If

                m_Staff._Fullname = m_Staff._Forename + " " + m_Staff._Surname

                If m_Staff._Ethnicity = "" Then m_Staff._Ethnicity = "Unknown"
                If m_Staff._Language = "" Then m_Staff._Language = "Unknown"
                If m_Staff._Nationality = "" Then m_Staff._Nationality = "Unknown"
                If m_Staff._Religion = "" Then m_Staff._Religion = "Unknown"

                m_Staff._TouchPin = txtTouchPIN.Text

                DocumentHandler.UpdatePhoto(m_Staff._Photo, m_Staff._ID, PictureBox, m_Staff._Fullname)
                PictureBox.Enabled = False

                cbxManager.Clear()
                cbxManager.PopulateWithSQL(Session.ConnectionString, Business.Staff.LineManagerSQL)

            Case "tabCar"

                m_Staff._TelHome = txtTelHome.Text
                m_Staff._TelMobile = txtTelMobile.Text
                m_Staff._Email = txtEmail.Text

                If m_Staff._Address <> txtAddress.Text Then m_Staff._Coordinates = ""
                m_Staff._Address = txtAddress.Address
                m_Staff._Postcode = txtAddress.PostCode

                If m_Paxton AndAlso tabMain.SelectedTabPage.Name = "tabCar" Then
                    m_Staff._ClockNo = txtPaxtonUserID.Text
                    m_Staff._ClockPerms = cbxPaxtonAccessLevel.SelectedValue.ToString
                    m_Staff._ClockGroup = cbxPaxtonDepartment.SelectedValue.ToString
                End If

            Case "tabAbsence"
                m_Staff.UpdateStatistics()
                m_Staff.Store()

                DisplayAbsenceStatistics()

            Case "tabTimesheets"
                If cbxSalaryUnit.Text = "per Hour" Then
                    m_Staff._SalaryHour = ValueHandler.ConvertDecimal(txtSalary.Text)
                    m_Staff._SalaryValue = ValueHandler.ConvertDecimal(txtSalaryPerHour.Text)
                Else
                    m_Staff._SalaryHour = ValueHandler.ConvertDecimal(txtSalaryPerHour.Text)
                    m_Staff._SalaryValue = ValueHandler.ConvertDecimal(txtSalary.Text)
                End If

        End Select

        m_Staff.Store()

        Me.Text = "Staff - " + m_Staff._Fullname + ", " + m_Staff._GroupName

    End Sub

    Protected Overrides Sub SetBindings()

        m_Staff = New Business.Staff
        bs.DataSource = m_Staff

        'personal
        '******************************************************************************************************************************
        txtForename.DataBindings.Add("Text", bs, "_Forename")
        txtSurname.DataBindings.Add("Text", bs, "_Surname")
        cbxStatus.DataBindings.Add("SelectedValue", bs, "_Status")
        cbxContractType.DataBindings.Add("Text", bs, "_ContractType")
        udtDOB.DataBindings.Add("Value", bs, "_DOB", True)

        cbxDepartment.DataBindings.Add("Text", bs, "_Department")
        txtJobTitle.DataBindings.Add("Text", bs, "_JobTitle")
        cbxManager.DataBindings.Add("Text", bs, "_ReportsTo")
        chkLineManager.DataBindings.Add("Checked", bs, "_LineManager")
        chkKeyworker.DataBindings.Add("Checked", bs, "_Keyworker")
        udtStarted.DataBindings.Add("Value", bs, "_DateStarted", True)
        udtLeft.DataBindings.Add("Value", bs, "_DateLeft", True)

        ccxNationality.DataBindings.Add("Text", bs, "_Nationality")
        cbxEthnicity.DataBindings.Add("Text", bs, "_Ethnicity")
        cbxReligion.DataBindings.Add("Text", bs, "_Religion")
        ccxLanguages.DataBindings.Add("Text", bs, "_Language")

        cbxQualLevel.DataBindings.Add("SelectedIndex", bs, "_QualLevel")

        chkAidWork.DataBindings.Add("Checked", bs, "_Fa")
        chkPaeds.DataBindings.Add("Checked", bs, "_FaPaed")
        chkFS.DataBindings.Add("Checked", bs, "_Forest")
        chkFSAid.DataBindings.Add("Checked", bs, "_FaForest")
        chkHygiene.DataBindings.Add("Checked", bs, "_FoodHygiene")
        chkSENCO.DataBindings.Add("Checked", bs, "_Senco")
        chkENCO.DataBindings.Add("Checked", bs, "_Enco")
        chkSafe.DataBindings.Add("Checked", bs, "_Safeguard")

        'DBS, car, address
        '******************************************************************************************************************************

        udtCRBLast.DataBindings.Add("Value", bs, "_CrbLast", True)
        udtCRBDue.DataBindings.Add("Value", bs, "_CrbDue", True)
        cdtDBSIssued.DataBindings.Add("Value", bs, "_DbsIssued", True)
        cbxDBSCheckType.DataBindings.Add("Text", bs, "_DbsCheckType")
        txtCRBRef.DataBindings.Add("Text", bs, "_CrbRef")
        txtDBSPosition.DataBindings.Add("Text", bs, "_DbsPosition")

        chkDriver.DataBindings.Add("Checked", bs, "_CarDriver")
        chkCar.DataBindings.Add("Checked", bs, "_CarOwner")
        txtReg.DataBindings.Add("Text", bs, "_CarReg")
        txtMakeModel.DataBindings.Add("Text", bs, "_CarDetails")
        udtInsCheck.DataBindings.Add("Value", bs, "_CarInsChecked", True)

        txtICEName.DataBindings.Add("Text", bs, "_ICEName")
        txtICERel.DataBindings.Add("Text", bs, "_ICERel")
        txtICEHome.DataBindings.Add("Text", bs, "_ICETel")
        txtICEMobile.DataBindings.Add("Text", bs, "_ICEMobile")

        'absence
        '******************************************************************************************************************************
        cbxHolsFrom.DataBindings.Add("Text", bs, "_HolsFrom")
        txtHolsEntitlement.DataBindings.Add("Text", bs, "_HolsEntitlement")

        'timesheets and payroll
        '******************************************************************************************************************************
        txtPayrollID.DataBindings.Add("Text", bs, "_PayrollId")
        txtNICat.DataBindings.Add("Text", bs, "_NiCat")
        txtTaxCode.DataBindings.Add("Text", bs, "_TaxCode")
        txtNINO.DataBindings.Add("Text", bs, "_Nino")

        cbxSalaryUnit.DataBindings.Add("SelectedValue", bs, "_SalaryUnit")
        txtHoursFTE.DataBindings.Add("Text", bs, "_HoursFte")
        txtHoursContracted.DataBindings.Add("Text", bs, "_HoursContracted")

    End Sub

    Private Sub btnClockUpdate_Click(sender As Object, e As EventArgs) Handles btnPaxtonUpdate.Click

        If m_PaxtonConnected Then

            btnPaxtonLink.Enabled = False
            btnPaxtonDisable.Enabled = False
            btnPaxtonUpdate.Enabled = False
            Session.CursorWaiting()

            If txtPaxtonUserID.Text = "" Then

                Dim _UserID As Integer = m_Controller.CreateUser(CInt(cbxPaxtonAccessLevel.SelectedValue), CInt(cbxPaxtonDepartment.SelectedValue),
                                                               m_Staff._Forename, m_Staff._Surname, m_Staff._TelMobile, m_Staff._TouchPin, Nothing)
                If _UserID <= 0 Then
                    txtPaxtonUserID.Text = ""
                Else
                    txtPaxtonUserID.Text = _UserID.ToString
                End If

            Else

                m_Controller.UpdateUser(CInt(txtPaxtonUserID.Text), CInt(cbxPaxtonAccessLevel.SelectedValue), CInt(cbxPaxtonDepartment.SelectedValue),
                                        m_Staff._Forename, m_Staff._Surname, m_Staff._TelMobile, m_Staff._TouchPin, Nothing)
            End If

            btnPaxtonLink.Enabled = True
            btnPaxtonDisable.Enabled = True
            btnPaxtonUpdate.Enabled = True
            Session.CursorDefault()

            If txtPaxtonUserID.Text = "" Then
                btnPaxtonUpdate.Text = "Create Paxton User"
            Else
                btnPaxtonUpdate.Text = "Update Paxton User"
            End If

        End If

    End Sub

    Private Sub btnClockDisable_Click(sender As Object, e As EventArgs) Handles btnPaxtonDisable.Click

        If CareMessage("Are you sure you want to disable this user?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Disable User") = DialogResult.Yes Then

            btnPaxtonDisable.Enabled = False
            Session.CursorWaiting()

            'TAACHelper.UpdateUser(txtClockNo.Text, txtClockName.Text, cbxClockPerms.SelectedIndex, False)

            btnPaxtonDisable.Enabled = True
            Session.CursorDefault()

        End If

    End Sub

    Private Sub btnClockLink_Click(sender As Object, e As EventArgs) Handles btnPaxtonLink.Click

        Dim _Users As List(Of Care.TAAC.UserData) = m_Controller.GetUsers

        Dim _frm As New frmGenericListFind("Select Paxton User")
        _frm.Populate(Of Care.TAAC.UserData)(_Users, "UserID,AccessLevelID,DepartmentID")
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            Dim _SelectedUser As Care.TAAC.UserData = CType(_frm.ReturnValue, Care.TAAC.UserData)
            txtPaxtonUserID.Text = _SelectedUser.UserID.ToString
        End If

        _frm.Dispose()
        _frm = Nothing

        If txtPaxtonUserID.Text = "" Then
            btnPaxtonUpdate.Text = "Create Paxton User"
        Else
            btnPaxtonUpdate.Text = "Update Paxton User"
        End If

    End Sub

    Private Sub btnPayEmployee_Click(sender As Object, e As EventArgs) Handles btnPayEmployee.Click
        If txtPayrollID.Text = "" Then Exit Sub
        Care.Financials.ClearBooksEngine.OpenEmployee(Session.FinancialsAPIID, txtPayrollID.Text)
    End Sub

    Private Sub btnPaySlip_Click(sender As Object, e As EventArgs) Handles btnPaySlip.Click

        If txtPayrollID.Text = "" Then Exit Sub

        Dim _Year As Integer = ParameterHandler.ReturnInteger("PAYROLLYEAR", False)
        If _Year <= 0 Then Exit Sub

        Dim _Period As Integer = ParameterHandler.ReturnInteger("PAYROLLPERIOD", False)
        If _Period <= 0 Then Exit Sub

        Care.Financials.ClearBooksEngine.OpenPayslip(Session.FinancialsAPIID, _Year, _Period, txtPayrollID.Text)

    End Sub

    Private Sub CalculateSalary()

        ClearSalaryFields()

        If txtSalary.Text = "" Then Exit Sub
        Dim _Salary As Double = CDbl(txtSalary.Text)
        If _Salary <= 0 Then Exit Sub

        If txtHoursContracted.Text = "" Then Exit Sub
        Dim _ContHours As Double = CDbl(txtHoursContracted.Text)
        If _ContHours <= 0 Then Exit Sub

        If txtHoursFTE.Text = "" Then Exit Sub
        Dim _FTEHours As Double = CDbl(txtHoursFTE.Text)
        If _FTEHours <= 0 Then Exit Sub

        Dim _SalaryPerHour As Double = 0
        Dim _FullSalaryPerAnnum As Double = 0
        Dim _ContSalaryPerAnnum As Double = 0
        Dim _SalaryPerDay As Double = 0
        Dim _HoursPerDay As Double = 0

        _HoursPerDay = _ContHours / 5

        If cbxSalaryUnit.SelectedIndex = 0 Then
            _SalaryPerHour = _Salary
            _FullSalaryPerAnnum = _Salary * 52 * _FTEHours
            lblSalaryPer.Text = "Salary per Annum"
            txtSalaryPerHour.Text = Format(_FullSalaryPerAnnum, "0.00")
        Else
            _SalaryPerHour = _Salary / 52 / _FTEHours
            _FullSalaryPerAnnum = _Salary
            lblSalaryPer.Text = "Salary per Hour"
            txtSalaryPerHour.Text = Format(_SalaryPerHour, "0.00")
        End If

        _ContSalaryPerAnnum = _FullSalaryPerAnnum / _FTEHours * _ContHours

        txtFTEWeek.Text = Format(_FullSalaryPerAnnum / 52, "0.00")
        txtFTE4Week.Text = Format(_FullSalaryPerAnnum / 12, "0.00")
        txtFTEAnnual.Text = Format(_FullSalaryPerAnnum, "0.00")

        txtContractedWeek.Text = Format(_ContSalaryPerAnnum / 52, "0.00")
        txtContracted4Week.Text = Format(_ContSalaryPerAnnum / 12, "0.00")
        txtContractedAnnual.Text = Format(_ContSalaryPerAnnum, "0.00")

    End Sub

    Private Sub ClearSalaryFields()

        txtSalaryPerHour.Text = "0.00"

        txtFTEWeek.Text = "0.00"
        txtFTE4Week.Text = "0.00"
        txtFTEAnnual.Text = "0.00"

        txtContractedWeek.Text = "0.00"
        txtContracted4Week.Text = "0.00"
        txtContractedAnnual.Text = "0.00"

    End Sub

    Private Sub txtSalary_Validated(sender As Object, e As EventArgs) Handles txtSalary.Validated
        CalculateSalary()
    End Sub

    Private Sub cbxSalaryUnit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSalaryUnit.SelectedIndexChanged
        CalculateSalary()
    End Sub

    Private Sub txtHoursFTE_Validated(sender As Object, e As EventArgs) Handles txtHoursFTE.Validated
        CalculateSalary()
    End Sub

    Private Sub txtHoursContracted_Validated(sender As Object, e As EventArgs) Handles txtHoursContracted.Validated
        CalculateSalary()
    End Sub

    Private Sub tabMain_SelectedPageChanged(sender As Object, e As DevExpress.XtraTab.TabPageChangedEventArgs) Handles tabMain.SelectedPageChanged

        If e.Page.Name = "tabTimesheets" Then
            CalculateSalary()
        End If

    End Sub

    Private Sub tabMain_Selecting(sender As Object, e As DevExpress.XtraTab.TabPageCancelEventArgs) Handles tabMain.Selecting
        DisplayTabPage(e.Page.Name)
    End Sub

    Private Sub DisplayTabPage(ByVal TabPageName As String, Optional ByVal SetToolbar As Boolean = True)

        If m_Staff Is Nothing Then Exit Sub
        Select Case TabPageName

            Case "tabPersonal"
                If SetToolbar Then Me.ToolbarMode = ToolbarEnum.Standard

            Case "tabCar"
                DisplayCarAndAccessControl()
                If SetToolbar Then Me.ToolbarMode = ToolbarEnum.FindandEdit

            Case "tabChildren"
                DisplayChildren()
                If SetToolbar Then Me.ToolbarMode = ToolbarEnum.FindOnly

            Case "tabQualsTrain"
                DisplayTrainingAndQualifications()
                If SetToolbar Then Me.ToolbarMode = ToolbarEnum.FindandEdit

            Case "tabApps"
                DisplayAppraisalsAndDisciplinaries()
                If SetToolbar Then Me.ToolbarMode = ToolbarEnum.FindandEdit

            Case "tabAbsence"
                DisplayAbsence()
                If SetToolbar Then Me.ToolbarMode = ToolbarEnum.FindandEdit

            Case "tabPlanner"
                DisplayPlanner()
                If SetToolbar Then Me.ToolbarMode = ToolbarEnum.FindandEdit

            Case "tabShifts"
                DisplayShifts()
                If SetToolbar Then Me.ToolbarMode = ToolbarEnum.FindandEdit

            Case "tabTimesheets"
                If m_Staff._SalaryUnit = "per Hour" Then
                    txtSalary.Text = ValueHandler.MoneyAsText(m_Staff._SalaryHour)
                Else
                    txtSalary.Text = ValueHandler.MoneyAsText(m_Staff._SalaryValue)
                End If
                CalculateSalary()
                DisplayTimeSheets()
                If SetToolbar Then Me.ToolbarMode = ToolbarEnum.FindandEdit

            Case "tabActivity"
                DisplayCRM()
                If SetToolbar Then Me.ToolbarMode = ToolbarEnum.FindOnly

        End Select

    End Sub

    Private Sub DisplayShifts()
        If radShiftPreferences.Checked Then DisplayShiftPrefs()
    End Sub

    Private Sub DisplayShiftPrefs()

        Dim _SQL As String = ""
        _SQL += "select ID, wc as 'WC', mon_start as 'Mon S.', mon_finish as 'Mon F.', tue_start as 'Tue S.', tue_finish as 'Tue F.', wed_start as 'Wed S.', wed_finish as 'Wed F.',"
        _SQL += " thu_start as 'Thu S.', thu_finish as 'Thu F.', fri_start as 'Fri S.', fri_finish as 'Fri F.', total_hours as 'Total Hrs', total_cost as 'Cost'"
        _SQL += " from StaffPrefHours"
        _SQL += " where staff_id = '" + m_Staff._ID.ToString + "'"
        _SQL += " order by wc desc"

        cgShifts.HideFirstColumn = True
        cgShifts.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub DisplayCarAndAccessControl()

    End Sub

    Private Sub DisplayCRM()

        With CrmActivity1
            .CRMLinkID = m_Staff._ID
            .CRMContactName = m_Staff._Fullname
            .CRMContactEmail = m_Staff._Email
            .CRMContactMobile = m_Staff._TelMobile
            .Populate()
        End With

    End Sub

    Private Sub DisplayTrainingAndQualifications()

        If m_Staff Is Nothing OrElse m_Staff._ID.HasValue = False OrElse m_Staff._ID.Value.ToString = "" Then Exit Sub

        Dim _SQL As String = ""

        _SQL += "select id, train_from as 'From', train_to as 'To', train_course as 'Course',"
        _SQL += " train_venue as 'Venue', train_cost as 'Cost' from StaffTrain"
        _SQL += " where staff_id = '" + m_Staff._ID.Value.ToString + "'"
        _SQL += " order by train_from desc"

        cgTraining.HideFirstColumn = True
        cgTraining.Populate(Session.ConnectionString, _SQL)

        _SQL = ""
        _SQL += "select id, qual_date as 'Qualification Date', qual_name as 'Qualification', qual_expires as 'Expires',"
        _SQL += " qual_score as 'Score' from StaffQual"
        _SQL += " where staff_id = '" + m_Staff._ID.Value.ToString + "'"
        _SQL += " order by qual_date desc"

        cgQuals.HideFirstColumn = True
        cgQuals.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub DisplayChildren()

        If m_Staff Is Nothing OrElse m_Staff._ID.HasValue = False OrElse m_Staff._ID.Value.ToString = "" Then Exit Sub

        Dim _SQL As String = ""

        _SQL += "select id, fullname as 'Name', group_name as 'Room', dob as 'DOB' from Children"
        _SQL += " where(date_left Is null Or date_left > getdate())"
        _SQL += " and keyworker_id = '" + m_Staff._ID.Value.ToString + "'"
        _SQL += " order by dob desc"

        cgChildren.HideFirstColumn = True
        cgChildren.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub DisplayAppraisalsAndDisciplinaries()

        If m_Staff Is Nothing OrElse m_Staff._ID.HasValue = False OrElse m_Staff._ID.Value.ToString = "" Then Exit Sub

        Dim _SQL As String = ""

        _SQL += "select id, action_date as 'Date', subject as 'Subject' from StaffAppDis"
        _SQL += " where staff_id = '" + m_Staff._ID.Value.ToString + "'"
        _SQL += " and type = 'Appraisal'"
        _SQL += " order by action_date desc"

        cgAppraisals.HideFirstColumn = True
        cgAppraisals.Populate(Session.ConnectionString, _SQL)

        _SQL = ""
        _SQL += "select id, action_date as 'Date', subject as 'Subject' from StaffAppDis"
        _SQL += " where staff_id = '" + m_Staff._ID.Value.ToString + "'"
        _SQL += " and type = 'Disciplinary'"
        _SQL += " order by action_date desc"

        cgDis.HideFirstColumn = True
        cgDis.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub DisplayAbsence()

        chkAbsenceNotes.Enabled = True

        If m_Staff Is Nothing OrElse m_Staff._ID.HasValue = False OrElse m_Staff._ID.Value.ToString = "" Then Exit Sub

        Dim _SQL As String = ""

        _SQL = String.Concat("select id, abs_type as 'Type', abs_from as 'From', datename(weekday, abs_from) as 'From Day',",
                                " abs_to as 'To', datename(weekday, abs_to) as 'To Day',",
                                " (SELECT (DATEDIFF(dd, abs_from, abs_to) + 1)",
                                " -(DATEDIFF(wk, abs_from, abs_to) * 2)",
                                " -(CASE WHEN DATENAME(dw, abs_from) = 'Sunday' THEN 1 ELSE 0 END)",
                                " -(CASE WHEN DATENAME(dw, abs_to) = 'Saturday' THEN 1 ELSE 0 END)) as 'Days',",
                                " abs_reason as 'Reason', abs_cost as 'Cost', abs_notes",
                                " from StaffAbsence",
                                " where staff_id = '", m_Staff._ID.Value.ToString, "'")

        If cbxAbsenceType.SelectedIndex > 0 Then
            _SQL += " and abs_type = '" + cbxAbsenceType.Text + "'"
        End If

        _SQL += " order by abs_from desc"

        cgAbs.HideFirstColumn = True
        cgAbs.Populate(Session.ConnectionString, _SQL)

        cbxAbsenceType.Enabled = True
        cbxAbsenceType.ReadOnly = False

        DisplayAbsenceStatistics()
        ToggleNotes()

    End Sub

    Private Sub DisplayAssessment()

    End Sub

    Private Sub DisplayFoyer()

    End Sub

    Private Sub DisplayTimeSheets()

        Dim _SQL As String = Business.Register.ReturnStaffRegister(m_Staff._ID.Value)
        cgTimesheets.Populate(Session.ConnectionString, _SQL)

        cgTimesheets.Columns("In").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        cgTimesheets.Columns("In").DisplayFormat.FormatString = "HH:mm"

        cgTimesheets.Columns("Out").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        cgTimesheets.Columns("Out").DisplayFormat.FormatString = "HH:mm"

    End Sub

    Private Sub DisplayAge()
        If udtDOB.Value Is Nothing Then Exit Sub
        lblAge.Text = ValueHandler.DateDifferenceAsText(CType(udtDOB.Value, Date), Date.Today)
    End Sub

    Private Sub udtDOB_LostFocus(sender As Object, e As EventArgs) Handles udtDOB.LostFocus
        DisplayAge()
    End Sub

    Private Sub ToggleGrids(ByVal Enable As Boolean)

        cgTraining.ButtonsEnabled = Enable
        cgQuals.ButtonsEnabled = Enable
        cgAppraisals.ButtonsEnabled = Enable
        cgDis.ButtonsEnabled = Enable
        cgAbs.ButtonsEnabled = Enable
        cgShifts.ButtonsEnabled = Enable

        cbxAbsenceType.Enabled = True
        cbxAbsenceType.ReadOnly = False

    End Sub

#Region "Planner"

    Private Sub DisplayPlanner()

        Dim _StartDate As Date = DateSerial(Date.Today.Year, 1, 1)
        If m_Staff Is Nothing OrElse m_Staff._ID.HasValue = False OrElse m_Staff._ID.Value.ToString = "" Then Exit Sub

        Dim _AbsenceRecords As List(Of Business.StaffAbsence) = Business.StaffAbsence.RetreiveByStaffID(m_Staff._ID.Value)
        If _AbsenceRecords IsNot Nothing Then
            If _AbsenceRecords.Count > 0 Then
                YearView1.Populate(_AbsenceRecords, _StartDate)
            Else
                YearView1.Clear()
            End If
        Else
            YearView1.Clear()
        End If

    End Sub

#End Region

#Region "Qualifications"

    Private Sub cgQuals_AddClick(sender As Object, e As EventArgs) Handles cgQuals.AddClick
        ShowQualificationsForm(Nothing)
    End Sub

    Private Sub cgQuals_EditClick(sender As Object, e As EventArgs) Handles cgQuals.EditClick
        ShowQualificationsForm(New Guid(cgQuals.CurrentRow("ID").ToString))
    End Sub

    Private Sub cgQuals_RemoveClick(sender As Object, e As EventArgs) Handles cgQuals.RemoveClick
        If cgQuals.CurrentRow IsNot Nothing Then DeleteGridRow(cgQuals.CurrentRow, "StaffAppDis")
    End Sub

    Private Sub ShowQualificationsForm(ByVal RecordID As Guid?)

        If m_Staff Is Nothing OrElse m_Staff._ID.HasValue = False OrElse m_Staff._ID.Value.ToString = "" Then Exit Sub

        Dim _frm As New frmStaffQual(m_Staff._ID.Value, RecordID)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            cgQuals.RePopulate()
        End If

    End Sub

#End Region

#Region "Appraisals"

    Private Sub cgAppraisals_AddClick(sender As Object, e As EventArgs) Handles cgAppraisals.AddClick
        ShowPerformanceForm(frmStaffPerf.EnumAppDis.Appraisal, Nothing)
    End Sub

    Private Sub cgAppraisals_EditClick(sender As Object, e As EventArgs) Handles cgAppraisals.EditClick
        ShowPerformanceForm(frmStaffPerf.EnumAppDis.Appraisal, New Guid(cgAppraisals.CurrentRow("ID").ToString))
    End Sub

    Private Sub cgAppraisals_RemoveClick(sender As Object, e As EventArgs) Handles cgAppraisals.RemoveClick
        If cgAppraisals.CurrentRow IsNot Nothing Then DeleteGridRow(cgAppraisals.CurrentRow, "StaffAppDis")
    End Sub

    Private Sub ShowPerformanceForm(ByVal TypeName As frmStaffPerf.EnumAppDis, ByVal RecordID As Guid?)

        If m_Staff Is Nothing OrElse m_Staff._ID.HasValue = False OrElse m_Staff._ID.Value.ToString = "" Then Exit Sub

        Dim _frm As New frmStaffPerf(TypeName, m_Staff._ID.Value, RecordID)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            If TypeName = frmStaffPerf.EnumAppDis.Appraisal Then
                cgAppraisals.RePopulate()
            Else
                cgDis.RePopulate()
            End If
        End If

    End Sub

#End Region

#Region "Disciplinaries"

    Private Sub cgDis_AddClick(sender As Object, e As EventArgs) Handles cgDis.AddClick
        ShowPerformanceForm(frmStaffPerf.EnumAppDis.Disciplinary, Nothing)
    End Sub

    Private Sub cgDis_EditClick(sender As Object, e As EventArgs) Handles cgDis.EditClick
        ShowPerformanceForm(frmStaffPerf.EnumAppDis.Disciplinary, New Guid(cgDis.CurrentRow("ID").ToString))
    End Sub

    Private Sub cgDis_RemoveClick(sender As Object, e As EventArgs) Handles cgDis.RemoveClick
        If cgDis.CurrentRow IsNot Nothing Then DeleteGridRow(cgDis.CurrentRow, "StaffAppDis")
    End Sub

#End Region

#Region "Absence"

    Private Sub cgAbs_AddClick(sender As Object, e As EventArgs) Handles cgAbs.AddClick
        ShowAbsenceForm(Nothing)
    End Sub

    Private Sub cgAbs_EditClick(sender As Object, e As EventArgs) Handles cgAbs.EditClick
        ShowAbsenceForm(New Guid(cgAbs.CurrentRow("ID").ToString))
    End Sub

    Private Sub cgAbs_RemoveClick(sender As Object, e As EventArgs) Handles cgAbs.RemoveClick
        If cgAbs.CurrentRow IsNot Nothing Then DeleteGridRow(cgAbs.CurrentRow, "StaffAbsence")
    End Sub

    Private Sub ShowAbsenceForm(ByVal RecordID As Guid?)

        If m_Staff Is Nothing OrElse m_Staff._ID.HasValue = False OrElse m_Staff._ID.Value.ToString = "" Then Exit Sub

        Dim _frm As New frmStaffAbsence(m_Staff._ID.Value, RecordID)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then

            cgAbs.RePopulate()

            cgAbs.PreviewColumn = "abs_notes"
            cgAbs.Columns("abs_notes").Visible = False

        End If

    End Sub

    Private Sub cbxAbsenceType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxAbsenceType.SelectedIndexChanged
        DisplayAbsence()
    End Sub

#End Region

#Region "Training"

    Private Sub ShowTrainingForm(ByVal RecordID As Guid?)

        If m_Staff Is Nothing OrElse m_Staff._ID.HasValue = False OrElse m_Staff._ID.Value.ToString = "" Then Exit Sub

        Dim _frm As New frmStaffTrain(m_Staff._ID.Value, RecordID)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            cgTraining.RePopulate()
        End If

    End Sub

    Private Sub cgTraining_AddClick(sender As Object, e As EventArgs) Handles cgTraining.AddClick
        ShowTrainingForm(Nothing)
    End Sub

    Private Sub cgTraining_EditClick(sender As Object, e As EventArgs) Handles cgTraining.EditClick
        ShowTrainingForm(New Guid(cgTraining.CurrentRow("ID").ToString))
    End Sub

    Private Sub cgTraining_RemoveClick(sender As Object, e As EventArgs) Handles cgTraining.RemoveClick
        If cgTraining.CurrentRow IsNot Nothing Then DeleteGridRow(cgTraining.CurrentRow, "StaffTrain")
    End Sub

#End Region

    Private Sub DeleteGridRow(ByRef DR As DataRow, ByVal TableName As String)
        If DR IsNot Nothing Then
            Dim _ID As String = DR.Item("ID").ToString
            If _ID <> "" Then
                If CareMessage("Are you sure you want to Delete this row?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Delete Row") = DialogResult.Yes Then
                    Dim _SQL As String = "delete from " + TableName + " where ID = '" + _ID + "'"
                    DAL.ExecuteSQL(Session.ConnectionString, _SQL)
                    DisplayTabPage(tabMain.SelectedTabPage.Name, False)
                End If
            End If
        End If
    End Sub

    Private Sub cgChildren_GridDoubleClick(sender As Object, e As EventArgs) Handles cgChildren.GridDoubleClick

        If cgChildren.RecordCount < 1 Then Exit Sub

        If cgChildren.CurrentRow(0).ToString <> "" Then
            Dim _ID As New Guid(cgChildren.CurrentRow(0).ToString)
            Business.Child.DrillDown(_ID)
            DisplayChildren()
        End If

    End Sub

    Private Sub udtStarted_LostFocus(sender As Object, e As EventArgs) Handles udtStarted.LostFocus
        DisplayYearsService()
    End Sub

    Private Sub udtLeft_LostFocus(sender As Object, e As EventArgs) Handles udtLeft.LostFocus
        DisplayYearsLeft()
    End Sub

    Private Sub DisplayYearsService()
        If udtStarted.Value Is Nothing Then lblYearsService.Text = "" : Exit Sub
        lblYearsService.Text = ValueHandler.DateDifferenceAsText(CType(udtStarted.Value, Date), Date.Today)
    End Sub

    Private Sub DisplayYearsLeft()
        If udtLeft.Value Is Nothing Then lblYearsLeft.Text = "" : Exit Sub
        lblYearsLeft.Text = ValueHandler.DateDifferenceAsText(CType(udtLeft.Value, Date), Date.Today)
    End Sub

    Private Sub cbxSite_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSite.SelectedIndexChanged
        PopulateRooms()
    End Sub

    Private Sub PopulateRooms()
        If cbxSite.Text = "" Then
            cbxGroup.Clear()
            cbxGroup.SelectedIndex = -1
        Else
            cbxGroup.PopulateWithSQL(Session.ConnectionString, Business.RoomRatios.RetreiveSiteRoomSQL(cbxSite.SelectedValue.ToString))
        End If
    End Sub

    Private Sub btnPIN_Click(sender As Object, e As EventArgs) Handles btnPIN.Click
        If udtDOB.Text <> "" Then
            If udtDOB.Value.HasValue Then
                txtTouchPIN.Text = Format(udtDOB.Value.Value, "ddMM")
            End If
        End If
    End Sub

    Private Sub cgShifts_AddClick(sender As Object, e As EventArgs) Handles cgShifts.AddClick
        ShowShiftPrefForm(Nothing)
    End Sub

    Private Sub cgShifts_EditClick(sender As Object, e As EventArgs) Handles cgShifts.EditClick
        ShowShiftPrefForm(New Guid(cgShifts.CurrentRow("ID").ToString))
    End Sub

    Private Sub cgShifts_RemoveClick(sender As Object, e As EventArgs) Handles cgShifts.RemoveClick
        If cgShifts.CurrentRow IsNot Nothing Then DeleteGridRow(cgShifts.CurrentRow, "StaffPrefHours")
    End Sub

    Private Sub ShowShiftPrefForm(ByVal RecordID As Guid?)

        If m_Staff Is Nothing OrElse m_Staff._ID.HasValue = False OrElse m_Staff._ID.Value.ToString = "" Then Exit Sub

        Dim _frm As New frmStaffPrefHours(m_Staff, RecordID)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            cgShifts.RePopulate()
        End If

    End Sub

    Private Sub btnRecruit_Click(sender As Object, e As EventArgs) Handles btnRecruit.Click

        Dim _frm As New frmRecruit(m_Staff._ApplicantId)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            If Not m_Staff._ApplicantId.HasValue Then
                m_Staff._ApplicantId = _frm.RecordID
                m_Staff.Store()
                SetRecruitmentInformation()
            End If
        End If

    End Sub

    Private Sub SetRecruitmentInformation()
        btnRecruit.Enabled = True
        If m_Staff._ApplicantId.HasValue Then
            btnRecruit.Text = "View Application"
        Else
            btnRecruit.Text = "Link Application"
        End If
    End Sub

    Private Sub DisplayAbsenceStatistics()

        Dim _Month As String = m_Staff._HolsFrom
        If _Month = "" Then _Month = "January"
        lblDaysSince.Text = "Days since " + _Month

        txtStatBS.Text = m_Staff._BradScore.ToString
        txtStatHolsLeft.Text = ValueHandler.MoneyAsText(m_Staff._HolsEntitlement - m_Staff._HolsTaken)

        txtStatHols.Text = ValueHandler.MoneyAsText(m_Staff._HolsTaken)
        txtStatSickness.Text = ValueHandler.MoneyAsText(m_Staff._SickTaken)
        txtStatOther.Text = ValueHandler.MoneyAsText(m_Staff._AbsTaken)
        txtStatTotal.Text = ValueHandler.MoneyAsText(m_Staff._HolsTaken + m_Staff._SickTaken + m_Staff._AbsTaken)

    End Sub

    Private Sub chkAbsenceNotes_CheckedChanged(sender As Object, e As EventArgs) Handles chkAbsenceNotes.CheckedChanged
        ToggleNotes()
    End Sub

    Private Sub ToggleNotes()
        If chkAbsenceNotes.Checked Then
            cgAbs.PreviewColumn = "abs_notes"
        Else
            cgAbs.PreviewColumn = ""
        End If
        cgAbs.Columns("abs_notes").Visible = False
    End Sub

    Private Sub lblBradfordScore_DoubleClick(sender As Object, e As EventArgs) Handles lblBradfordScore.DoubleClick

        If CareMessage("Recalculate Score for all Staff?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Recalculate Scores?") = DialogResult.Yes Then

            Dim _Staff As List(Of Business.Staff) = Business.Staff.RetreiveAll
            Session.SetupProgressBar("Processing Staff...", _Staff.Count)

            For Each _s In _Staff
                _s.UpdateStatistics()
                _s.Store()
                Session.StepProgressBar()
            Next

            Session.HideProgressBar()

        End If

    End Sub

End Class



