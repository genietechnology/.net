﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmForecasting
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling2 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling3 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.GroupControl1 = New Care.Controls.CareFrame(Me.components)
        Me.chkExclHolidays = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.cdtTo = New Care.Controls.CareDateTime(Me.components)
        Me.cdtFrom = New Care.Controls.CareDateTime(Me.components)
        Me.btnPrint = New Care.Controls.CareButton(Me.components)
        Me.radCustom = New Care.Controls.CareRadioButton(Me.components)
        Me.radMonthNext = New Care.Controls.CareRadioButton(Me.components)
        Me.radMonthThis = New Care.Controls.CareRadioButton(Me.components)
        Me.radWeekNext = New Care.Controls.CareRadioButton(Me.components)
        Me.radWeekThis = New Care.Controls.CareRadioButton(Me.components)
        Me.btnRefresh = New Care.Controls.CareButton(Me.components)
        Me.PivotGridControl1 = New DevExpress.XtraPivotGrid.PivotGridControl()
        Me.txtYellow = New DevExpress.XtraEditors.TextEdit()
        Me.txtRed = New DevExpress.XtraEditors.TextEdit()
        Me.txtGreen = New DevExpress.XtraEditors.TextEdit()
        Me.GroupControl2 = New Care.Controls.CareFrame(Me.components)
        Me.radTurnOverRoom = New Care.Controls.CareRadioButton(Me.components)
        Me.radTurnOverDate = New Care.Controls.CareRadioButton(Me.components)
        Me.radHeadCountRoom = New Care.Controls.CareRadioButton(Me.components)
        Me.radHeadCountDate = New Care.Controls.CareRadioButton(Me.components)
        Me.gbxSites = New Care.Controls.CareFrame(Me.components)
        Me.cbxSite = New Care.Controls.CareComboBox(Me.components)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.chkExclHolidays.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radCustom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radMonthNext.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radMonthThis.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radWeekNext.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radWeekThis.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PivotGridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.radTurnOverRoom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radTurnOverDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radHeadCountRoom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radHeadCountDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxSites, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxSites.SuspendLayout()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.chkExclHolidays)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Controls.Add(Me.cdtTo)
        Me.GroupControl1.Controls.Add(Me.cdtFrom)
        Me.GroupControl1.Controls.Add(Me.btnPrint)
        Me.GroupControl1.Controls.Add(Me.radCustom)
        Me.GroupControl1.Controls.Add(Me.radMonthNext)
        Me.GroupControl1.Controls.Add(Me.radMonthThis)
        Me.GroupControl1.Controls.Add(Me.radWeekNext)
        Me.GroupControl1.Controls.Add(Me.radWeekThis)
        Me.GroupControl1.Controls.Add(Me.btnRefresh)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 73)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(901, 62)
        Me.GroupControl1.TabIndex = 2
        Me.GroupControl1.Text = "Forecasting Range"
        '
        'chkExclHolidays
        '
        Me.chkExclHolidays.EnterMoveNextControl = True
        Me.chkExclHolidays.Location = New System.Drawing.Point(632, 31)
        Me.chkExclHolidays.Name = "chkExclHolidays"
        Me.chkExclHolidays.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkExclHolidays.Properties.Appearance.Options.UseFont = True
        Me.chkExclHolidays.Properties.Caption = "Exclude Hols"
        Me.chkExclHolidays.Size = New System.Drawing.Size(97, 19)
        Me.chkExclHolidays.TabIndex = 8
        Me.chkExclHolidays.Tag = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(524, 33)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(11, 15)
        Me.CareLabel1.TabIndex = 6
        Me.CareLabel1.Text = "to"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtTo
        '
        Me.cdtTo.EditValue = Nothing
        Me.cdtTo.EnterMoveNextControl = True
        Me.cdtTo.Location = New System.Drawing.Point(541, 30)
        Me.cdtTo.Name = "cdtTo"
        Me.cdtTo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtTo.Properties.Appearance.Options.UseFont = True
        Me.cdtTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtTo.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtTo.Size = New System.Drawing.Size(85, 22)
        Me.cdtTo.TabIndex = 7
        Me.cdtTo.Value = Nothing
        '
        'cdtFrom
        '
        Me.cdtFrom.EditValue = Nothing
        Me.cdtFrom.EnterMoveNextControl = True
        Me.cdtFrom.Location = New System.Drawing.Point(433, 30)
        Me.cdtFrom.Name = "cdtFrom"
        Me.cdtFrom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtFrom.Properties.Appearance.Options.UseFont = True
        Me.cdtFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtFrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtFrom.Size = New System.Drawing.Size(85, 22)
        Me.cdtFrom.TabIndex = 5
        Me.cdtFrom.Value = Nothing
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnPrint.Location = New System.Drawing.Point(735, 29)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(75, 23)
        Me.btnPrint.TabIndex = 9
        Me.btnPrint.Text = "Print"
        '
        'radCustom
        '
        Me.radCustom.Location = New System.Drawing.Point(369, 31)
        Me.radCustom.Name = "radCustom"
        Me.radCustom.Properties.AutoWidth = True
        Me.radCustom.Properties.Caption = "Custom"
        Me.radCustom.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radCustom.Properties.RadioGroupIndex = 0
        Me.radCustom.Size = New System.Drawing.Size(58, 19)
        Me.radCustom.TabIndex = 4
        Me.radCustom.TabStop = False
        '
        'radMonthNext
        '
        Me.radMonthNext.Location = New System.Drawing.Point(276, 31)
        Me.radMonthNext.Name = "radMonthNext"
        Me.radMonthNext.Properties.AutoWidth = True
        Me.radMonthNext.Properties.Caption = "Next Month"
        Me.radMonthNext.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radMonthNext.Properties.RadioGroupIndex = 0
        Me.radMonthNext.Size = New System.Drawing.Size(78, 19)
        Me.radMonthNext.TabIndex = 3
        Me.radMonthNext.TabStop = False
        '
        'radMonthThis
        '
        Me.radMonthThis.Location = New System.Drawing.Point(188, 31)
        Me.radMonthThis.Name = "radMonthThis"
        Me.radMonthThis.Properties.AutoWidth = True
        Me.radMonthThis.Properties.Caption = "This Month"
        Me.radMonthThis.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radMonthThis.Properties.RadioGroupIndex = 0
        Me.radMonthThis.Size = New System.Drawing.Size(74, 19)
        Me.radMonthThis.TabIndex = 2
        Me.radMonthThis.TabStop = False
        '
        'radWeekNext
        '
        Me.radWeekNext.Location = New System.Drawing.Point(100, 31)
        Me.radWeekNext.Name = "radWeekNext"
        Me.radWeekNext.Properties.AutoWidth = True
        Me.radWeekNext.Properties.Caption = "Next Week"
        Me.radWeekNext.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radWeekNext.Properties.RadioGroupIndex = 0
        Me.radWeekNext.Size = New System.Drawing.Size(75, 19)
        Me.radWeekNext.TabIndex = 1
        Me.radWeekNext.TabStop = False
        '
        'radWeekThis
        '
        Me.radWeekThis.EditValue = True
        Me.radWeekThis.Location = New System.Drawing.Point(12, 31)
        Me.radWeekThis.Name = "radWeekThis"
        Me.radWeekThis.Properties.AutoWidth = True
        Me.radWeekThis.Properties.Caption = "This week"
        Me.radWeekThis.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radWeekThis.Properties.RadioGroupIndex = 0
        Me.radWeekThis.Size = New System.Drawing.Size(69, 19)
        Me.radWeekThis.TabIndex = 0
        '
        'btnRefresh
        '
        Me.btnRefresh.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnRefresh.Location = New System.Drawing.Point(816, 29)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(75, 23)
        Me.btnRefresh.TabIndex = 10
        Me.btnRefresh.Text = "Refresh"
        '
        'PivotGridControl1
        '
        Me.PivotGridControl1.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.PivotGridControl1.Location = New System.Drawing.Point(12, 141)
        Me.PivotGridControl1.Name = "PivotGridControl1"
        Me.PivotGridControl1.Size = New System.Drawing.Size(901, 279)
        Me.PivotGridControl1.TabIndex = 3
        '
        'txtYellow
        '
        Me.txtYellow.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.txtYellow.Location = New System.Drawing.Point(863, 372)
        Me.txtYellow.Name = "txtYellow"
        Me.txtYellow.Properties.Appearance.BackColor = System.Drawing.Color.Khaki
        Me.txtYellow.Properties.Appearance.Options.UseBackColor = True
        Me.txtYellow.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtYellow, True)
        Me.txtYellow.Size = New System.Drawing.Size(50, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtYellow, OptionsSpelling1)
        Me.txtYellow.TabIndex = 12
        Me.txtYellow.TabStop = False
        Me.txtYellow.Visible = False
        '
        'txtRed
        '
        Me.txtRed.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.txtRed.Location = New System.Drawing.Point(863, 398)
        Me.txtRed.Name = "txtRed"
        Me.txtRed.Properties.Appearance.BackColor = System.Drawing.Color.LightCoral
        Me.txtRed.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRed.Properties.Appearance.Options.UseBackColor = True
        Me.txtRed.Properties.Appearance.Options.UseFont = True
        Me.txtRed.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtRed, True)
        Me.txtRed.Size = New System.Drawing.Size(50, 22)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtRed, OptionsSpelling2)
        Me.txtRed.TabIndex = 11
        Me.txtRed.TabStop = False
        Me.txtRed.Visible = False
        '
        'txtGreen
        '
        Me.txtGreen.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.txtGreen.Location = New System.Drawing.Point(863, 346)
        Me.txtGreen.Name = "txtGreen"
        Me.txtGreen.Properties.Appearance.BackColor = System.Drawing.Color.LightGreen
        Me.txtGreen.Properties.Appearance.Options.UseBackColor = True
        Me.txtGreen.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtGreen, True)
        Me.txtGreen.Size = New System.Drawing.Size(50, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtGreen, OptionsSpelling3)
        Me.txtGreen.TabIndex = 10
        Me.txtGreen.TabStop = False
        Me.txtGreen.Visible = False
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl2.Controls.Add(Me.radTurnOverRoom)
        Me.GroupControl2.Controls.Add(Me.radTurnOverDate)
        Me.GroupControl2.Controls.Add(Me.radHeadCountRoom)
        Me.GroupControl2.Controls.Add(Me.radHeadCountDate)
        Me.GroupControl2.Location = New System.Drawing.Point(221, 9)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(691, 57)
        Me.GroupControl2.TabIndex = 1
        Me.GroupControl2.Text = "Layouts"
        '
        'radTurnOverRoom
        '
        Me.radTurnOverRoom.Location = New System.Drawing.Point(529, 29)
        Me.radTurnOverRoom.Name = "radTurnOverRoom"
        Me.radTurnOverRoom.Properties.AutoWidth = True
        Me.radTurnOverRoom.Properties.Caption = "Turnover by room, by date"
        Me.radTurnOverRoom.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radTurnOverRoom.Properties.RadioGroupIndex = 1
        Me.radTurnOverRoom.Size = New System.Drawing.Size(152, 19)
        Me.radTurnOverRoom.TabIndex = 3
        Me.radTurnOverRoom.TabStop = False
        '
        'radTurnOverDate
        '
        Me.radTurnOverDate.Location = New System.Drawing.Point(363, 29)
        Me.radTurnOverDate.Name = "radTurnOverDate"
        Me.radTurnOverDate.Properties.AutoWidth = True
        Me.radTurnOverDate.Properties.Caption = "Turnover by date, by child"
        Me.radTurnOverDate.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radTurnOverDate.Properties.RadioGroupIndex = 1
        Me.radTurnOverDate.Size = New System.Drawing.Size(149, 19)
        Me.radTurnOverDate.TabIndex = 2
        Me.radTurnOverDate.TabStop = False
        '
        'radHeadCountRoom
        '
        Me.radHeadCountRoom.Location = New System.Drawing.Point(188, 29)
        Me.radHeadCountRoom.Name = "radHeadCountRoom"
        Me.radHeadCountRoom.Properties.AutoWidth = True
        Me.radHeadCountRoom.Properties.Caption = "Headcount by room, by date"
        Me.radHeadCountRoom.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radHeadCountRoom.Properties.RadioGroupIndex = 1
        Me.radHeadCountRoom.Size = New System.Drawing.Size(160, 19)
        Me.radHeadCountRoom.TabIndex = 1
        Me.radHeadCountRoom.TabStop = False
        '
        'radHeadCountDate
        '
        Me.radHeadCountDate.EditValue = True
        Me.radHeadCountDate.Location = New System.Drawing.Point(12, 29)
        Me.radHeadCountDate.Name = "radHeadCountDate"
        Me.radHeadCountDate.Properties.AutoWidth = True
        Me.radHeadCountDate.Properties.Caption = "Headcount by date, by child"
        Me.radHeadCountDate.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radHeadCountDate.Properties.RadioGroupIndex = 1
        Me.radHeadCountDate.Size = New System.Drawing.Size(157, 19)
        Me.radHeadCountDate.TabIndex = 0
        '
        'gbxSites
        '
        Me.gbxSites.Controls.Add(Me.cbxSite)
        Me.gbxSites.Location = New System.Drawing.Point(12, 9)
        Me.gbxSites.Name = "gbxSites"
        Me.gbxSites.Size = New System.Drawing.Size(203, 57)
        Me.gbxSites.TabIndex = 0
        Me.gbxSites.Text = "Site"
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(9, 28)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AccessibleName = "Group"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(186, 20)
        Me.cbxSite.TabIndex = 1
        Me.cbxSite.Tag = ""
        Me.cbxSite.ValueMember = Nothing
        '
        'frmForecasting
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(924, 429)
        Me.Controls.Add(Me.gbxSites)
        Me.Controls.Add(Me.txtYellow)
        Me.Controls.Add(Me.txtRed)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.txtGreen)
        Me.Controls.Add(Me.PivotGridControl1)
        Me.Controls.Add(Me.GroupControl1)
        Me.FormBorderStyle = FormBorderStyle.Sizable
        Me.Name = "frmForecasting"
        Me.Text = ""
        Me.WindowState = FormWindowState.Maximized
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.chkExclHolidays.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radCustom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radMonthNext.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radMonthThis.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radWeekNext.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radWeekThis.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PivotGridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.radTurnOverRoom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radTurnOverDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radHeadCountRoom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radHeadCountDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxSites, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxSites.ResumeLayout(False)
        Me.gbxSites.PerformLayout()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnRefresh As Care.Controls.CareButton
    Friend WithEvents PivotGridControl1 As DevExpress.XtraPivotGrid.PivotGridControl
    Friend WithEvents txtYellow As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRed As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtGreen As DevExpress.XtraEditors.TextEdit
    Friend WithEvents radHeadCountDate As Care.Controls.CareRadioButton
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents cdtTo As Care.Controls.CareDateTime
    Friend WithEvents cdtFrom As Care.Controls.CareDateTime
    Friend WithEvents btnPrint As Care.Controls.CareButton
    Friend WithEvents radCustom As Care.Controls.CareRadioButton
    Friend WithEvents radMonthNext As Care.Controls.CareRadioButton
    Friend WithEvents radMonthThis As Care.Controls.CareRadioButton
    Friend WithEvents radWeekNext As Care.Controls.CareRadioButton
    Friend WithEvents radWeekThis As Care.Controls.CareRadioButton
    Friend WithEvents radHeadCountRoom As Care.Controls.CareRadioButton
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
    Friend WithEvents radTurnOverDate As Care.Controls.CareRadioButton
    Friend WithEvents radTurnOverRoom As Care.Controls.CareRadioButton
    Friend WithEvents GroupControl1 As Care.Controls.CareFrame
    Friend WithEvents GroupControl2 As Care.Controls.CareFrame
    Friend WithEvents gbxSites As Care.Controls.CareFrame
    Friend WithEvents chkExclHolidays As Care.Controls.CareCheckBox
End Class
