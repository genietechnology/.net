﻿

Imports Care.Global
Imports Care.Shared

Public Class frmImportParenta

    Private m_RawRecord As New List(Of String)
    Private m_FamilyRecords As New List(Of ImportRecord)
    Private m_StaffRecords As New List(Of StaffRecord)

    Private m_ImportFailed As Integer = 0

    Private m_FamilyID As Guid? = Nothing
    Private m_ChildID As Guid? = Nothing

    Private Sub frmImportParenta_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cdtStartDate.Value = Today

        ofd.Filter = "XML Files|*.xml"
        ofd.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.Desktop

        With cbxGroup
            .AllowBlank = True
            .PopulateWithSQL(Session.ConnectionString, "select id, name from groups order by sequence")
        End With

        With cbxKeyworker
            .AllowBlank = True
            .PopulateWithSQL(Session.ConnectionString, "select id, fullname from staff where keyworker = 1 order by fullname")
        End With

        With cbxSite
            .AllowBlank = True
            .PopulateWithSQL(Session.ConnectionString, "select id, name from sites order by name")
        End With

        btnImport.Enabled = False

    End Sub

    Private Sub ImportFamilyRecords()

        If cdtStartDate.Text = "" Then
            CareMessage("Please enter a Start Date.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Exit Sub
        End If

        If cbxSite.Text = "" Then
            CareMessage("Please select a Site.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Exit Sub
        End If

        If cbxGroup.Text = "" Then
            CareMessage("Please select a Group/Room.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Exit Sub
        End If

        If cbxKeyworker.Text = "" Then
            CareMessage("Please select a Keyworker.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Exit Sub
        End If

        Session.SetupProgressBar("Creating Family Records...", m_FamilyRecords.Count)

        For Each _r In m_FamilyRecords

            If SaveFamily(_r) Then
                If SaveContacts(_r) Then
                    If SaveChild(_r) Then
                        m_ImportFailed += 1
                    End If
                Else
                    m_ImportFailed += 1
                End If
            Else
                m_ImportFailed += 1
            End If

            Session.StepProgressBar()

        Next

        Session.SetProgressMessage("Import Complete", 3)

    End Sub

    Private Sub ImportStaffRecords()

        If cdtStartDate.Text = "" Then
            CareMessage("Please enter a Start Date.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Exit Sub
        End If

        If cbxSite.Text = "" Then
            CareMessage("Please select a Site.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Exit Sub
        End If

        If cbxGroup.Text = "" Then
            CareMessage("Please select a Group/Room.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Exit Sub
        End If

        Session.SetupProgressBar("Creating Staff Records...", m_StaffRecords.Count)

        For Each _r In m_StaffRecords

            If Not SaveStaff(_r) Then
                m_ImportFailed += 1
            End If

            Session.StepProgressBar()

        Next

        Session.SetProgressMessage("Import Complete", 3)

    End Sub

    Private Function SaveStaff(ByVal StaffRecordIn As StaffRecord) As Boolean

        Dim _S As New Business.Staff
        With _S

            ._SiteId = New Guid(cbxSite.SelectedValue.ToString)
            ._SiteName = cbxSite.Text

            ._Status = "C"
            ._ContractType = "Permanent"
            ._Keyworker = True

            ._Forename = StaffRecordIn.Forename
            ._Surname = StaffRecordIn.Surname
            ._Fullname = StaffRecordIn.Forename + " " + StaffRecordIn.Surname
            ._JobTitle = StaffRecordIn.JobTitle

            ._GroupId = New Guid(cbxGroup.SelectedValue.ToString)
            ._GroupName = cbxGroup.Text

            ._Address = StaffRecordIn.Address
            ._TelHome = StaffRecordIn.TelHome
            ._TelMobile = StaffRecordIn.TelMobile

            ._IceName = StaffRecordIn.EmerName
            ._IceRel = StaffRecordIn.EmerRelationship
            ._IceTel = StaffRecordIn.EmerHome
            ._IceMobile = StaffRecordIn.EmerMobile

            .Store()

        End With

        _S = Nothing

        Return True

    End Function

    Private Sub ProcessStaffData()

        m_RawRecord.Clear()
        m_StaffRecords.Clear()

        Dim myXMLString As String = IO.File.OpenText(txtFile.Text).ReadToEnd

        Dim _Pos As Integer = 0

        While myXMLString.IndexOf("<grpStaffId", _Pos) > 0

            Dim _RecordStart As Integer = myXMLString.IndexOf("<grpStaffId", _Pos)
            Dim _RecordEnd As Integer = myXMLString.IndexOf("</grpStaffId>", _RecordStart + 1)
            Dim _RecordData As String = myXMLString.Substring(_RecordStart, (_RecordEnd - _RecordStart))

            m_RawRecord.Add(_RecordData)

            _Pos = _RecordEnd + 1

        End While

        For Each _Record In m_RawRecord
            ProcessStaffRecord(_Record)
        Next

        cgRecords.Populate(m_StaffRecords)

    End Sub

    Private Sub ProcessChildData()

        m_RawRecord.Clear()
        m_FamilyRecords.Clear()

        Dim myXMLString As String = IO.File.OpenText(txtFile.Text).ReadToEnd

        Dim _Pos As Integer = 0

        While myXMLString.IndexOf("<table1_Group1>", _Pos) > 0

            Dim _RecordStart As Integer = myXMLString.IndexOf("<table1_Group1>", _Pos)
            Dim _RecordEnd As Integer = myXMLString.IndexOf("</table1_Group1>", _RecordStart + 1)
            Dim _RecordData As String = myXMLString.Substring(_RecordStart, (_RecordEnd - _RecordStart))

            m_RawRecord.Add(_RecordData)

            _Pos = _RecordEnd + 1

        End While

        For Each _Record In m_RawRecord
            ProcessFamilyRecord(_Record)
        Next

        cgRecords.Populate(m_FamilyRecords)

    End Sub

    Private Sub ProcessStaffRecord(ByVal RawData As String)

        Dim _Pos As Integer = 0

        Dim _Record As New StaffRecord

        _Pos = ExtractField(_Pos, RawData, "Name", _Record.Forename)
        _Pos = ExtractField(_Pos, RawData, "SurName", _Record.Surname)
        _Pos = ExtractField(_Pos, RawData, "textbox30", _Record.Gender)
        _Pos = ExtractField(_Pos, RawData, "textbox14", _Record.TelHome)
        _Pos = ExtractField(_Pos, RawData, "MobileNo", _Record.TelMobile)

        _Pos = ExtractField(_Pos, RawData, "Address", _Record.Address)
        CleanAddress(_Record.Address)

        _Pos = ExtractField(_Pos, RawData, "textbox50", _Record.JobTitle)

        _Pos = ExtractField(_Pos, RawData, "EmergencyName", _Record.EmerName)
        _Pos = ExtractField(_Pos, RawData, "EmergencyName2", _Record.EmerRelationship)
        _Pos = ExtractField(_Pos, RawData, "EmergencyNumber", _Record.EmerHome)
        _Pos = ExtractField(_Pos, RawData, "Position", _Record.EmerWork)
        _Pos = ExtractField(_Pos, RawData, "textbox26", _Record.EmerMobile)

        _Pos = ExtractField(_Pos, RawData, "textbox32", _Record.EmerAddress)
        CleanAddress(_Record.EmerAddress)

        m_StaffRecords.Add(_Record)

    End Sub

    Private Sub ProcessFamilyRecord(ByVal RawData As String)

        Dim _Pos As Integer = 0

        Dim _Record As New ImportRecord

        '*********************************************************************************************

        'primary contact details

        _Pos = ExtractField(_Pos, RawData, "textbox17", _Record.PriForename)
        _Pos = ExtractField(_Pos, RawData, "textbox61", _Record.PriSurname)
        _Pos = ExtractField(_Pos, RawData, "PRelation", _Record.PriRelationship)

        _Pos = ExtractField(_Pos, RawData, "textbox57", _Record.PriAddress)
        CleanAddress(_Record.PriAddress)

        _Pos = ExtractField(_Pos, RawData, "textbox53", _Record.PriTelHome)
        _Pos = ExtractField(_Pos, RawData, "textbox55", _Record.PriTelWork)
        _Pos = ExtractField(_Pos, RawData, "textbox49", _Record.PriTelMobile)
        _Pos = ExtractField(_Pos, RawData, "textbox51", _Record.PriEmail)
        _Pos = ExtractField(_Pos, RawData, "textbox359", _Record.PriPassword)

        '*********************************************************************************************

        'child details

        _Pos = ExtractField(_Pos, RawData, "textbox21", _Record.ChildForename)
        _Pos = ExtractField(_Pos, RawData, "textbox23", _Record.ChildDOB)
        _Pos = ExtractField(_Pos, RawData, "textbox33", _Record.ChildSurname)

        '*********************************************************************************************

        'extract contacts, there could be several of these, so we do this in a loop.
        _Pos += 1

        Dim _ContactCount As Integer = 0
        While RawData.IndexOf("textbox4=", _Pos) > 0

            Dim _ContactStart As Integer = RawData.IndexOf("textbox4=", _Pos) - 1

            Dim _Name As String = ""
            _Pos = ExtractField(_ContactStart, RawData, "textbox4", _Name)

            _ContactCount += 1

            _Record.Contacts.Add(New Contact(_ContactCount, _Name))

        End While

        '*********************************************************************************************

        ProcessContactField(_Pos, RawData, _Record, "textbox110", "Address")
        ProcessContactField(_Pos, RawData, _Record, "textbox114", "TelHome")
        ProcessContactField(_Pos, RawData, _Record, "textbox113", "TelWork")
        ProcessContactField(_Pos, RawData, _Record, "Textbox15", "TelMobile")
        ProcessContactField(_Pos, RawData, _Record, "Textbox22", "Relationship")
        ProcessContactField(_Pos, RawData, _Record, "textbox241", "Emergency")
        ProcessContactField(_Pos, RawData, _Record, "textbox238", "Parent")
        ProcessContactField(_Pos, RawData, _Record, "textbox235", "Collect")

        '*********************************************************************************************

        m_FamilyRecords.Add(_Record)

    End Sub

    Private Function ExtractField(ByVal StartPos As Integer, ByVal RecordIn As String, ByVal FieldName As String, ByRef ReturnValue As String) As Integer

        Dim _TagStart As Integer = RecordIn.IndexOf(FieldName, StartPos)
        If _TagStart > 0 Then
            Dim _DataStart As Integer = RecordIn.IndexOf("=" + Chr(34), _TagStart) + 2
            Dim _DataEnd As Integer = RecordIn.IndexOf(Chr(34), _DataStart)
            ReturnValue = RecordIn.Substring(_DataStart, _DataEnd - _DataStart)
            Return _DataEnd
        Else
            Return StartPos
        End If

    End Function

    Private Sub UpdateField(ByRef ContactList As List(Of Contact), RecordNo As Integer, ByVal ContactField As String, Value As String)

        For Each _C In ContactList

            If _C.No = RecordNo Then

                If ContactField = "Address" Then _C.Address = Value
                CleanAddress(_C.Address)

                If ContactField = "TelHome" Then _C.TelHome = Value
                If ContactField = "TelWork" Then _C.TelWork = Value
                If ContactField = "TelMobile" Then _C.TelMobile = Value
                If ContactField = "Relationship" Then _C.Relationship = Value

                Select Case ContactField

                    Case "Emergency", "Parent", "Collect"

                        Dim _SlashValue As String = Value.Trim
                        Dim _BooleanValue As Boolean = False
                        If _SlashValue = "/" Then _BooleanValue = True

                        If ContactField = "Emergency" Then _C.Emergency = _BooleanValue
                        If ContactField = "Parent" Then _C.Parent = _BooleanValue
                        If ContactField = "Collect" Then _C.Collect = _BooleanValue

                End Select

                Exit For

            End If

        Next

    End Sub

    Private Sub CleanAddress(ByRef AddressIn As String)
        If AddressIn Is Nothing Then AddressIn = ""
        AddressIn = AddressIn.Trim
        AddressIn = AddressIn.Replace("&#xD;&#xA;", vbCrLf)
        AddressIn = AddressIn.Replace("&#xD;", vbCrLf)
    End Sub

    Private Sub ProcessContactField(ByRef Pos As Integer, ByVal RawData As String, ByRef Record As ImportRecord, ByVal XMLField As String, ByVal ImportRecordField As String)

        Dim _Count As Integer = 0
        While RawData.IndexOf(XMLField + "=", Pos) > 0

            Dim _Start As Integer = RawData.IndexOf(XMLField + "=", Pos) - 1

            Dim _Value As String = ""
            Pos = ExtractField(_Start, RawData, XMLField, _Value)

            _Count += 1

            UpdateField(Record.Contacts, _Count, ImportRecordField, _Value)

        End While

    End Sub

    Private Function SaveFamily(ByVal DataIn As ImportRecord) As Boolean

        Dim _F As New Business.Family
        With _F

            ._SiteId = New Guid(cbxSite.SelectedValue.ToString)
            ._SiteName = cbxSite.Text

            ._Surname = DataIn.PriSurname
            ._LetterName = DataIn.PriForename + " " + DataIn.PriSurname
            ._eInvoicing = False
            ._Address = DataIn.PriAddress

            ._OpeningBalance = 0
            ._Balance = 0

            .Store()

            m_FamilyID = _F._ID

        End With

        _F = Nothing

        Return True

    End Function

    Private Function SaveContacts(ByVal DataIn As ImportRecord) As Boolean

        CreateContactRecord(True, DataIn.PriForename, DataIn.PriSurname, DataIn.PriRelationship, _
                            DataIn.PriTelMobile, DataIn.PriTelHome, DataIn.PriTelWork, DataIn.PriPassword, DataIn.PriAddress, True, True, True, DataIn.PriEmail)

        For Each _C In DataIn.Contacts

            _C.Name = _C.Name.TrimStart

            If _C.Name <> "" Then

                Dim _Space As Integer = _C.Name.IndexOf(" ")
                Dim _Forename As String = ""
                Dim _Surname As String = ""

                If _Space > 0 Then
                    _Forename = _C.Name.Substring(0, _Space - 1)
                    _Surname = _C.Name.Substring(_Space + 1)
                Else
                    _Surname = _C.Name
                End If

                If Not CreateContactRecord(False, _Forename, _Surname, _C.Relationship, _
                                           _C.TelMobile, _C.TelHome, _C.TelWork, _C.Password, _C.Address, True, _C.Parent, _C.Collect, "") Then Return False

            End If

        Next

        Return True

    End Function

    Private Function SaveChild(ByVal DataIn As ImportRecord) As Boolean

        Dim _C As New Business.Child
        With _C

            ._FamilyId = m_FamilyID
            ._FamilyName = DataIn.PriSurname

            ._SiteId = New Guid(cbxSite.SelectedValue.ToString)
            ._SiteName = cbxSite.Text

            ._Status = "On Waiting List"

            ._Forename = DataIn.ChildForename
            ._Surname = DataIn.ChildSurname
            ._Fullname = ._Forename + " " + ._Surname

            Dim _DOB As Date? = ValueHandler.ConvertDate(DataIn.ChildDOB)
            ._Dob = _DOB

            ._Gender = "U"

            ._Started = cdtStartDate.Value
            ._GroupStarted = ._Started

            ._GroupId = New Guid(cbxGroup.SelectedValue.ToString)
            ._GroupName = cbxGroup.Text

            ._KeyworkerId = New Guid(cbxKeyworker.SelectedValue.ToString)
            ._KeyworkerName = cbxKeyworker.Text

            .Store()

            m_ChildID = ._ID

        End With

        Return True

    End Function

    Private Function CreateContactRecord(ByVal Primary As Boolean, ByVal Forename As String, ByVal Surname As String, ByVal Rel As String, _
                                         ByVal Mobile As String, ByVal Home As String, ByVal Work As String, ByVal Password As String, _
                                         ByVal Address As String, ByVal Emergency As Boolean, Parent As Boolean, Collect As Boolean, ByVal Email As String) As Boolean

        Dim _C As New Business.Contact
        With _C

            ._FamilyId = m_FamilyID

            ._Surname = Surname
            ._Forename = Forename
            ._Fullname = Forename + " " + Surname
            ._Relationship = Rel

            If Primary Then
                ._PrimaryCont = True
                ._EmerCont = True
                ._Collect = True
                ._Parent = True
                ._Email = Email
            Else
                ._PrimaryCont = False
                ._EmerCont = Emergency
                ._Parent = Parent
                ._Collect = Collect
            End If

            ._Address = Address

            ._TelHome = Home
            ._TelMobile = Mobile
            ._JobTel = Work

            ._Password = Password

            ._ReportEmail = cbxSite.SelectedValue.ToString

            .Store()

        End With

        _C = Nothing

        Return True

    End Function

#Region "Controls"

    Private Sub btnBrowse_Click(sender As Object, e As EventArgs) Handles btnBrowse.Click

        If ofd.ShowDialog = DialogResult.OK Then

            txtFile.Text = ofd.FileName

            If radFamily.Checked Then ProcessChildData()
            If radStaff.Checked Then ProcessStaffData()

            gbxFamily.Enabled = True
            btnImport.Enabled = True

        End If

    End Sub

    Private Sub radStaff_CheckedChanged(sender As Object, e As EventArgs) Handles radStaff.CheckedChanged
        If radStaff.Enabled Then cbxKeyworker.Enabled = False
    End Sub

    Private Sub radFamily_CheckedChanged(sender As Object, e As EventArgs) Handles radFamily.CheckedChanged
        If radFamily.Enabled Then cbxKeyworker.Enabled = True
    End Sub

    Private Sub btnImport_Click(sender As Object, e As EventArgs) Handles btnImport.Click

        If CareMessage("Are you sure you want to Import this data?", MessageBoxIcon.Information, MessageBoxButtons.YesNo, "Import Data") = DialogResult.Yes Then

            btnImport.Enabled = False
            Session.CursorWaiting()

            If radFamily.Checked Then ImportFamilyRecords()
            If radStaff.Checked Then ImportStaffRecords()

            btnImport.Enabled = True
            Session.CursorDefault()

        End If

    End Sub

#End Region

#Region "Private Classes"

    Private Class StaffRecord
        Property Forename As String
        Property Surname As String
        Property Gender As String
        Property JobTitle As String
        Property Address As String
        Property TelHome As String
        Property TelMobile As String
        Property EmerName As String
        Property EmerRelationship As String
        Property EmerHome As String
        Property EmerWork As String
        Property EmerMobile As String
        Property EmerAddress As String
    End Class

    Private Class ImportRecord

        Property PriForename As String
        Property PriSurname As String
        Property PriRelationship As String
        Property PriAddress As String
        Property PriTelHome As String
        Property PriTelWork As String
        Property PriTelMobile As String
        Property PriEmail As String
        Property PriPassword As String

        Property ChildForename As String
        Property ChildSurname As String
        Property ChildDOB As String

        Property Contacts As New List(Of Contact)

    End Class

    Private Class Contact

        Public Sub New()

        End Sub

        Public Sub New(ByVal No As Integer, ByVal Name As String)
            Me.No = No
            Me.Name = Name
        End Sub

        Property No As Integer
        Property Name As String
        Property Address As String
        Property TelHome As String
        Property TelWork As String
        Property TelMobile As String
        Property Relationship As String
        Property Password As String
        Property Emergency As Boolean
        Property Parent As Boolean
        Property Collect As Boolean

    End Class

#End Region

End Class
