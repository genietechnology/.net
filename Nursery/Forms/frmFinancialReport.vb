﻿

Imports Care.Global
Imports Care.Data
Imports DevExpress.XtraCharts
Imports DevExpress.XtraGrid.Views.Grid

Public Class frmFinancialReport

    Private m_Loading As Boolean = True

    Private Sub frmFinancialForecast_Load(sender As Object, e As EventArgs) Handles Me.Load

        radTurnover.Hide()

        cbxYear.AddItem(Today.Year.ToString)
        cbxYear.AddItem((Today.Year + 1).ToString)
        cbxYear.AddItem((Today.Year + 2).ToString)
        cbxYear.SelectedIndex = 0

        With cbxSite
            .AllowBlank = False
            .PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)
        End With

        If cbxSite.Items.Count > 0 Then
            cbxSite.SelectedIndex = 0
        End If

        Dim _SQL As String = ""
        _SQL += "select top 20 last_run as 'Last Run', args as 'Year',"
        _SQL += " CASE WHEN last_result = 0 THEN 'Successful' ELSE 'Failed' END as 'Status'"
        _SQL += " from AppTasks t"
        _SQL += " where command = 'BuildForecastData'"
        _SQL += " order by last_run desc"

        grdTasks.Populate(Session.ConnectionString, _SQL)

        m_Loading = False

    End Sub

    Private Sub DisplayGrid()

        Dim _SQL As String = ""

        _SQL += "select f.batch_id, f.fc_year as 'Year', datename(month, f.inv_from) as 'Month',"
        _SQL += " f.invoice_count as 'Invoice Count', (f.invoice_count - isnull(p.invoice_count,0)) as 'Count Movement',"
        _SQL += " f.non_funded_hours As 'Hours Non-Funded', f.funded_hours as 'Hours Funded',"
        _SQL += " f.non_funded_value as 'Value Non-Funded', f.funded_value as 'Value Funded',"
        _SQL += " f.total_hours as 'Hours Total', f.invoice_sub as 'Sub Total', f.invoice_discount as 'Discounts',"
        _SQL += " (f.invoice_total - isnull(p.invoice_total,0)) as 'Total Movement', f.invoice_total as 'TOTAL'"
        _SQL += " from Forecasting f"
        _SQL += " left join Forecasting p on p.fc_year = f.fc_year and p.fc_month = (f.fc_month - 1) and p.site_id = f.site_id"
        _SQL += " where f.fc_year = " + cbxYear.Text
        _SQL += " and f.site_name = '" + cbxSite.Text + "'"
        _SQL += " order by f.fc_month"

        cgMonths.HideFirstColumn = True
        cgMonths.Populate(Session.ConnectionString, _SQL)
        cgMonths.AutoSizeColumns()

    End Sub

    Private Sub cbxSite_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSite.SelectedIndexChanged
        RefreshGridandGraph()
    End Sub

    Private Sub DisplayChart()

        cht.DataSource = Nothing
        cht.Series.Clear()
        cht.Legend.Visibility = DevExpress.Utils.DefaultBoolean.False

        Dim _SQL As String = ""

        _SQL += "select datename(month, inv_from) as 'Month', invoice_total"
        _SQL += " from Forecasting"
        _SQL += " where fc_year = " + cbxYear.Text
        _SQL += " and site_name = '" + cbxSite.Text + "'"
        _SQL += " order by fc_month"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then
            AddSeries("Month", ScaleType.Auto, "Invoice Total", "invoice_total", ViewType.Bar)
            cht.DataSource = _DT
        End If

        CType(cht.Diagram, XYDiagram).AxisY.Label.TextPattern = "{A:n0}"

    End Sub

    Private Sub AddSeries(ByVal ArgumentField As String, ByVal ArgumentScaleType As ScaleType, ByVal SeriesName As String, ByVal SeriesField As String, ByVal ViewType As ViewType)

        Dim _S As New Series(SeriesName, ViewType)

        _S.ArgumentScaleType = ArgumentScaleType
        _S.ArgumentDataMember = ArgumentField

        _S.ValueScaleType = ScaleType.Numerical
        _S.ValueDataMembers.AddRange(New String() {SeriesField})

        If ViewType = DevExpress.XtraCharts.ViewType.Doughnut Then
            _S.Label.TextPattern = "{A}: {VP:p2}"
            CType(_S.Label, DoughnutSeriesLabel).Position = PieSeriesLabelPosition.TwoColumns
        End If

        cht.Series.Add(_S)

    End Sub

    Private Sub cgMonths_GridDoubleClick(sender As Object, e As EventArgs) Handles cgMonths.GridDoubleClick
        Dim _ID As String = cgMonths.CurrentRow.Item("batch_id").ToString
        Invoicing.ViewBatch(New Guid(_ID), "View Forecast Batch")
    End Sub

    Private Sub cgMonths_RowCellStyle(sender As Object, e As RowCellStyleEventArgs) Handles cgMonths.RowCellStyle

        If e.RowHandle < 0 Then Exit Sub

        Select Case e.Column.FieldName

            Case "Count Movement", "Total Movement"

                Dim _Value As Decimal = ValueHandler.ConvertDecimal(cgMonths.GetRowCellValue(e.RowHandle, e.Column.FieldName))
                If _Value = 0 Then
                    e.Appearance.ForeColor = System.Drawing.Color.Black
                Else
                    If _Value > 0 Then
                        e.Appearance.ForeColor = System.Drawing.Color.ForestGreen
                    Else
                        e.Appearance.ForeColor = System.Drawing.Color.Red
                    End If
                End If

        End Select

    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click

        Dim _Mess As String = ""

        If radForecast.Checked Then
            _Mess += "Rebuilding the forecasting data can take over an hour. This task is normally scheduled automatically overnight."
        Else
            _Mess += "Rebuilding the turnover data can take over an hour. This task is normally scheduled automatically overnight."
        End If

        _Mess += vbCrLf + "Are you sure you wish to Run this now?"
        If CareMessage(_Mess, MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Rebuild") = DialogResult.Yes Then

            SetCMDs(False)
            Session.CursorWaiting()

            Dim _F As New Business.FinancialUtilities

            If radForecast.Checked Then
                _F.BuildForecastData(CInt(cbxYear.Text))
            Else
                _F.BuildTurnoverData()
            End If

            RefreshGridandGraph()

            SetCMDs(True)
            Session.CursorDefault()

        End If

    End Sub

    Private Sub SetCMDs(ByVal Enabled As Boolean)
        btnRefresh.Enabled = Enabled
        btnGenerateDays.Enabled = Enabled
    End Sub

    Private Sub btnGenerateDays_Click(sender As Object, e As EventArgs) Handles btnGenerateDays.Click

        SetCMDs(False)

        Dim _F As New Business.FinancialUtilities
        _F.BuildInvoiceDays()

        SetCMDs(True)

    End Sub

    Private Sub cbxYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxYear.SelectedIndexChanged
        RefreshGridandGraph()
    End Sub

    Private Sub RefreshGridandGraph()

        If m_Loading Then Exit Sub
        If cbxSite.Text = "" Then Exit Sub
        If cbxYear.Text = "" Then Exit Sub

        DisplayGrid()
        DisplayChart()

    End Sub

    Private Sub frmFinancialReport_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        RefreshGridandGraph()
    End Sub
End Class