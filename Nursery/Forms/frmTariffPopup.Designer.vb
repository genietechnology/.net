﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTariffPopup
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTariffPopup))
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.btnMatrix = New Care.Controls.CareButton()
        Me.CareLabel3 = New Care.Controls.CareLabel()
        Me.txtInvoiceText = New Care.Controls.CareTextBox()
        Me.CareLabel5 = New Care.Controls.CareLabel()
        Me.Label12 = New Care.Controls.CareLabel()
        Me.cbxType = New Care.Controls.CareComboBox()
        Me.txtName = New Care.Controls.CareTextBox()
        Me.Label17 = New Care.Controls.CareLabel()
        Me.ceColour = New DevExpress.XtraEditors.ColorPickEdit()
        Me.CareLabel2 = New Care.Controls.CareLabel()
        Me.chkOverride = New Care.Controls.CareCheckBox()
        Me.lblPerHour = New Care.Controls.CareLabel()
        Me.lblMinutes = New Care.Controls.CareLabel()
        Me.lblRate = New Care.Controls.CareLabel()
        Me.txtRate = New Care.Controls.CareTextBox()
        Me.CareLabel1 = New Care.Controls.CareLabel()
        Me.txtDuration = New Care.Controls.CareTextBox()
        Me.GroupControl2 = New Care.Controls.CareFrame()
        Me.btnVariable = New Care.Controls.CareButton()
        Me.chkVariable = New Care.Controls.CareCheckBox()
        Me.CareLabel18 = New Care.Controls.CareLabel()
        Me.chkFundingHours = New Care.Controls.CareCheckBox()
        Me.CareLabel9 = New Care.Controls.CareLabel()
        Me.GroupControl3 = New Care.Controls.CareFrame()
        Me.CareLabel19 = New Care.Controls.CareLabel()
        Me.chkTimesVsDuration = New Care.Controls.CareCheckBox()
        Me.chkDiscount = New Care.Controls.CareCheckBox()
        Me.CareLabel20 = New Care.Controls.CareLabel()
        Me.CareLabel21 = New Care.Controls.CareLabel()
        Me.chkBoltOns = New Care.Controls.CareCheckBox()
        Me.CareLabel7 = New Care.Controls.CareLabel()
        Me.chkDaily = New Care.Controls.CareCheckBox()
        Me.CareLabel6 = New Care.Controls.CareLabel()
        Me.chkWeekly = New Care.Controls.CareCheckBox()
        Me.GroupControl4 = New Care.Controls.CareFrame()
        Me.txtVoidEnd = New Care.Controls.CareTextBox()
        Me.txtVoidStart = New Care.Controls.CareTextBox()
        Me.CareLabel15 = New Care.Controls.CareLabel()
        Me.CareLabel16 = New Care.Controls.CareLabel()
        Me.txtEndTime = New Care.Controls.CareTextBox()
        Me.txtStartTime = New Care.Controls.CareTextBox()
        Me.CareLabel4 = New Care.Controls.CareLabel()
        Me.CareLabel8 = New Care.Controls.CareLabel()
        Me.GroupControl5 = New Care.Controls.CareFrame()
        Me.CareLabel13 = New Care.Controls.CareLabel()
        Me.CareLabel12 = New Care.Controls.CareLabel()
        Me.txtRateFund36 = New Care.Controls.CareTextBox()
        Me.txtRateFund24 = New Care.Controls.CareTextBox()
        Me.chkHoliday = New Care.Controls.CareCheckBox()
        Me.CareLabel10 = New Care.Controls.CareLabel()
        Me.cbxTariff = New Care.Controls.CareComboBox()
        Me.GroupControl7 = New Care.Controls.CareFrame()
        Me.btnBreakdown = New Care.Controls.CareButton()
        Me.GroupControl8 = New Care.Controls.CareFrame()
        Me.cbxNLTrack = New Care.Controls.CareComboBox()
        Me.CareLabel22 = New Care.Controls.CareLabel()
        Me.cbxNLCode = New Care.Controls.CareComboBox()
        Me.CareLabel14 = New Care.Controls.CareLabel()
        Me.GroupControl9 = New Care.Controls.CareFrame()
        Me.chkSessHol = New Care.Controls.CareCheckBox()
        Me.txtSessHolDiscount = New Care.Controls.CareTextBox()
        Me.CareLabel17 = New Care.Controls.CareLabel()
        Me.CareLabel11 = New Care.Controls.CareLabel()
        Me.lblUsage = New Care.Controls.CareLabel()
        Me.btnCancel = New Care.Controls.CareButton()
        Me.btnOK = New Care.Controls.CareButton()
        Me.GroupControl10 = New Care.Controls.CareFrame()
        Me.cbxSummaryColumn = New Care.Controls.CareComboBox()
        Me.CareLabel26 = New Care.Controls.CareLabel()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtInvoiceText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceColour.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkOverride.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDuration.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.chkVariable.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkFundingHours.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.chkTimesVsDuration.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkDiscount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkBoltOns.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkDaily.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkWeekly.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.txtVoidEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtVoidStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEndTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStartTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.txtRateFund36.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRateFund24.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkHoliday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxTariff.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl7.SuspendLayout()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl8.SuspendLayout()
        CType(Me.cbxNLTrack.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxNLCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl9.SuspendLayout()
        CType(Me.chkSessHol.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSessHolDiscount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl10.SuspendLayout()
        CType(Me.cbxSummaryColumn.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.btnMatrix)
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Controls.Add(Me.txtInvoiceText)
        Me.GroupControl1.Controls.Add(Me.CareLabel5)
        Me.GroupControl1.Controls.Add(Me.Label12)
        Me.GroupControl1.Controls.Add(Me.cbxType)
        Me.GroupControl1.Controls.Add(Me.txtName)
        Me.GroupControl1.Controls.Add(Me.Label17)
        Me.GroupControl1.Controls.Add(Me.ceColour)
        Me.GroupControl1.Location = New System.Drawing.Point(13, 9)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(703, 97)
        Me.GroupControl1.TabIndex = 0
        '
        'btnMatrix
        '
        Me.btnMatrix.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnMatrix.Location = New System.Drawing.Point(388, 65)
        Me.btnMatrix.Name = "btnMatrix"
        Me.btnMatrix.Size = New System.Drawing.Size(73, 23)
        Me.btnMatrix.TabIndex = 6
        Me.btnMatrix.Text = "Matrix"
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(488, 70)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(36, 15)
        Me.CareLabel3.TabIndex = 7
        Me.CareLabel3.Text = "Colour"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtInvoiceText
        '
        Me.txtInvoiceText.CharacterCasing = CharacterCasing.Normal
        Me.txtInvoiceText.EnterMoveNextControl = True
        Me.txtInvoiceText.Location = New System.Drawing.Point(137, 38)
        Me.txtInvoiceText.MaxLength = 0
        Me.txtInvoiceText.Name = "txtInvoiceText"
        Me.txtInvoiceText.NumericAllowNegatives = False
        Me.txtInvoiceText.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtInvoiceText.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtInvoiceText.Properties.AccessibleName = "Invoice Text"
        Me.txtInvoiceText.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtInvoiceText.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtInvoiceText.Properties.Appearance.Options.UseFont = True
        Me.txtInvoiceText.Properties.Appearance.Options.UseTextOptions = True
        Me.txtInvoiceText.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtInvoiceText.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtInvoiceText.Size = New System.Drawing.Size(550, 22)
        Me.txtInvoiceText.TabIndex = 3
        Me.txtInvoiceText.Tag = "AEM"
        Me.txtInvoiceText.TextAlign = HorizontalAlignment.Left
        Me.txtInvoiceText.ToolTipText = ""
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(12, 41)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(63, 15)
        Me.CareLabel5.TabIndex = 2
        Me.CareLabel5.Text = "Invoice Text"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label12
        '
        Me.Label12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label12.Location = New System.Drawing.Point(12, 69)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(26, 15)
        Me.Label12.TabIndex = 4
        Me.Label12.Text = "Type"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxType
        '
        Me.cbxType.AllowBlank = False
        Me.cbxType.DataSource = Nothing
        Me.cbxType.DisplayMember = Nothing
        Me.cbxType.EnterMoveNextControl = True
        Me.cbxType.Location = New System.Drawing.Point(137, 66)
        Me.cbxType.Name = "cbxType"
        Me.cbxType.Properties.AccessibleName = "Tariff Type"
        Me.cbxType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxType.Properties.Appearance.Options.UseFont = True
        Me.cbxType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxType.SelectedValue = Nothing
        Me.cbxType.Size = New System.Drawing.Size(245, 22)
        Me.cbxType.TabIndex = 5
        Me.cbxType.Tag = "AEM"
        Me.cbxType.ValueMember = Nothing
        '
        'txtName
        '
        Me.txtName.CharacterCasing = CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(137, 10)
        Me.txtName.MaxLength = 0
        Me.txtName.Name = "txtName"
        Me.txtName.NumericAllowNegatives = False
        Me.txtName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AccessibleName = "Name"
        Me.txtName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtName.Size = New System.Drawing.Size(550, 22)
        Me.txtName.TabIndex = 1
        Me.txtName.Tag = "AEM"
        Me.txtName.TextAlign = HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'Label17
        '
        Me.Label17.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label17.Location = New System.Drawing.Point(12, 13)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(32, 15)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Name"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ceColour
        '
        Me.ceColour.EditValue = System.Drawing.Color.Empty
        Me.ceColour.Location = New System.Drawing.Point(530, 67)
        Me.ceColour.Name = "ceColour"
        Me.ceColour.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ceColour.Properties.ShowColorDialog = False
        Me.ceColour.Properties.ShowCustomColors = False
        Me.ceColour.Properties.ShowSystemColors = False
        Me.ceColour.Size = New System.Drawing.Size(157, 20)
        Me.ceColour.TabIndex = 8
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(336, 13)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(99, 15)
        Me.CareLabel2.TabIndex = 4
        Me.CareLabel2.Text = "Allow rate override"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel2.ToolTip = "Selecting this option allows users to override the rate when applying the session" & _
    " to a child."
        Me.CareLabel2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.CareLabel2.ToolTipTitle = "Allow Rate Override"
        '
        'chkOverride
        '
        Me.chkOverride.EnterMoveNextControl = True
        Me.chkOverride.Location = New System.Drawing.Point(441, 11)
        Me.chkOverride.Name = "chkOverride"
        Me.chkOverride.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOverride.Properties.Appearance.Options.UseFont = True
        Me.chkOverride.Size = New System.Drawing.Size(20, 19)
        Me.chkOverride.TabIndex = 5
        Me.chkOverride.Tag = "AE"
        '
        'lblPerHour
        '
        Me.lblPerHour.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPerHour.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblPerHour.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblPerHour.Location = New System.Drawing.Point(218, 41)
        Me.lblPerHour.Name = "lblPerHour"
        Me.lblPerHour.Size = New System.Drawing.Size(82, 15)
        Me.lblPerHour.TabIndex = 5
        Me.lblPerHour.Text = "£9.50 per hour"
        Me.lblPerHour.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMinutes
        '
        Me.lblMinutes.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMinutes.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblMinutes.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblMinutes.Location = New System.Drawing.Point(218, 13)
        Me.lblMinutes.Name = "lblMinutes"
        Me.lblMinutes.Size = New System.Drawing.Size(69, 15)
        Me.lblMinutes.TabIndex = 2
        Me.lblMinutes.Text = "180 minutes"
        Me.lblMinutes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRate
        '
        Me.lblRate.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblRate.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblRate.Location = New System.Drawing.Point(12, 41)
        Me.lblRate.Name = "lblRate"
        Me.lblRate.Size = New System.Drawing.Size(23, 15)
        Me.lblRate.TabIndex = 3
        Me.lblRate.Text = "Rate"
        Me.lblRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRate
        '
        Me.txtRate.CharacterCasing = CharacterCasing.Normal
        Me.txtRate.EnterMoveNextControl = True
        Me.txtRate.Location = New System.Drawing.Point(137, 38)
        Me.txtRate.MaxLength = 14
        Me.txtRate.Name = "txtRate"
        Me.txtRate.NumericAllowNegatives = False
        Me.txtRate.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtRate.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRate.Properties.AccessibleName = "Rate"
        Me.txtRate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRate.Properties.Appearance.Options.UseFont = True
        Me.txtRate.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRate.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtRate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtRate.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRate.Properties.MaxLength = 14
        Me.txtRate.Size = New System.Drawing.Size(75, 22)
        Me.txtRate.TabIndex = 4
        Me.txtRate.Tag = "AEM"
        Me.txtRate.TextAlign = HorizontalAlignment.Left
        Me.txtRate.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(12, 13)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(87, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Duration (hours)"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDuration
        '
        Me.txtDuration.CharacterCasing = CharacterCasing.Normal
        Me.txtDuration.EnterMoveNextControl = True
        Me.txtDuration.Location = New System.Drawing.Point(137, 10)
        Me.txtDuration.MaxLength = 14
        Me.txtDuration.Name = "txtDuration"
        Me.txtDuration.NumericAllowNegatives = False
        Me.txtDuration.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtDuration.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDuration.Properties.AccessibleName = "Duration"
        Me.txtDuration.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtDuration.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtDuration.Properties.Appearance.Options.UseFont = True
        Me.txtDuration.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDuration.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtDuration.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtDuration.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtDuration.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDuration.Properties.MaxLength = 14
        Me.txtDuration.Size = New System.Drawing.Size(75, 22)
        Me.txtDuration.TabIndex = 1
        Me.txtDuration.Tag = "AEM"
        Me.txtDuration.TextAlign = HorizontalAlignment.Left
        Me.txtDuration.ToolTipText = ""
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.btnVariable)
        Me.GroupControl2.Controls.Add(Me.chkVariable)
        Me.GroupControl2.Controls.Add(Me.CareLabel18)
        Me.GroupControl2.Controls.Add(Me.txtDuration)
        Me.GroupControl2.Controls.Add(Me.CareLabel1)
        Me.GroupControl2.Controls.Add(Me.txtRate)
        Me.GroupControl2.Controls.Add(Me.lblPerHour)
        Me.GroupControl2.Controls.Add(Me.lblRate)
        Me.GroupControl2.Controls.Add(Me.lblMinutes)
        Me.GroupControl2.Location = New System.Drawing.Point(13, 185)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.ShowCaption = False
        Me.GroupControl2.Size = New System.Drawing.Size(470, 68)
        Me.GroupControl2.TabIndex = 2
        '
        'btnVariable
        '
        Me.btnVariable.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnVariable.Location = New System.Drawing.Point(336, 37)
        Me.btnVariable.Name = "btnVariable"
        Me.btnVariable.Size = New System.Drawing.Size(125, 23)
        Me.btnVariable.TabIndex = 8
        Me.btnVariable.Text = "Variable Rate"
        '
        'chkVariable
        '
        Me.chkVariable.EnterMoveNextControl = True
        Me.chkVariable.Location = New System.Drawing.Point(441, 11)
        Me.chkVariable.Name = "chkVariable"
        Me.chkVariable.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkVariable.Properties.Appearance.Options.UseFont = True
        Me.chkVariable.Size = New System.Drawing.Size(20, 19)
        Me.chkVariable.TabIndex = 7
        Me.chkVariable.Tag = "E"
        '
        'CareLabel18
        '
        Me.CareLabel18.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel18.Location = New System.Drawing.Point(336, 13)
        Me.CareLabel18.Name = "CareLabel18"
        Me.CareLabel18.Size = New System.Drawing.Size(90, 15)
        Me.CareLabel18.TabIndex = 6
        Me.CareLabel18.Text = "Use Variable Rate"
        Me.CareLabel18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkFundingHours
        '
        Me.chkFundingHours.EnterMoveNextControl = True
        Me.chkFundingHours.Location = New System.Drawing.Point(230, 30)
        Me.chkFundingHours.Name = "chkFundingHours"
        Me.chkFundingHours.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFundingHours.Properties.Appearance.Options.UseFont = True
        Me.chkFundingHours.Size = New System.Drawing.Size(20, 19)
        Me.chkFundingHours.TabIndex = 1
        Me.chkFundingHours.Tag = "AE"
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(12, 32)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(190, 15)
        Me.CareLabel9.TabIndex = 0
        Me.CareLabel9.Text = "Allow Funding hours to be specified"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.CareLabel19)
        Me.GroupControl3.Controls.Add(Me.chkTimesVsDuration)
        Me.GroupControl3.Controls.Add(Me.chkDiscount)
        Me.GroupControl3.Controls.Add(Me.CareLabel20)
        Me.GroupControl3.Controls.Add(Me.CareLabel21)
        Me.GroupControl3.Controls.Add(Me.chkBoltOns)
        Me.GroupControl3.Controls.Add(Me.CareLabel7)
        Me.GroupControl3.Controls.Add(Me.chkDaily)
        Me.GroupControl3.Controls.Add(Me.chkOverride)
        Me.GroupControl3.Controls.Add(Me.CareLabel2)
        Me.GroupControl3.Controls.Add(Me.CareLabel6)
        Me.GroupControl3.Controls.Add(Me.chkWeekly)
        Me.GroupControl3.Location = New System.Drawing.Point(13, 112)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.ShowCaption = False
        Me.GroupControl3.Size = New System.Drawing.Size(470, 67)
        Me.GroupControl3.TabIndex = 1
        '
        'CareLabel19
        '
        Me.CareLabel19.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel19.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel19.Location = New System.Drawing.Point(171, 39)
        Me.CareLabel19.Name = "CareLabel19"
        Me.CareLabel19.Size = New System.Drawing.Size(96, 15)
        Me.CareLabel19.TabIndex = 8
        Me.CareLabel19.Text = "Times Vs Duration"
        Me.CareLabel19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel19.ToolTip = resources.GetString("CareLabel19.ToolTip")
        Me.CareLabel19.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.CareLabel19.ToolTipTitle = "Times Vs Duration"
        '
        'chkTimesVsDuration
        '
        Me.chkTimesVsDuration.EnterMoveNextControl = True
        Me.chkTimesVsDuration.Location = New System.Drawing.Point(297, 37)
        Me.chkTimesVsDuration.Name = "chkTimesVsDuration"
        Me.chkTimesVsDuration.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTimesVsDuration.Properties.Appearance.Options.UseFont = True
        Me.chkTimesVsDuration.Size = New System.Drawing.Size(20, 19)
        Me.chkTimesVsDuration.TabIndex = 9
        Me.chkTimesVsDuration.Tag = "AE"
        '
        'chkDiscount
        '
        Me.chkDiscount.EnterMoveNextControl = True
        Me.chkDiscount.Location = New System.Drawing.Point(441, 37)
        Me.chkDiscount.Name = "chkDiscount"
        Me.chkDiscount.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDiscount.Properties.Appearance.Options.UseFont = True
        Me.chkDiscount.Size = New System.Drawing.Size(20, 19)
        Me.chkDiscount.TabIndex = 11
        Me.chkDiscount.Tag = "AE"
        '
        'CareLabel20
        '
        Me.CareLabel20.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel20.Location = New System.Drawing.Point(335, 39)
        Me.CareLabel20.Name = "CareLabel20"
        Me.CareLabel20.Size = New System.Drawing.Size(85, 15)
        Me.CareLabel20.TabIndex = 10
        Me.CareLabel20.Text = "Allow Discounts"
        Me.CareLabel20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel21
        '
        Me.CareLabel21.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel21.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel21.Location = New System.Drawing.Point(11, 39)
        Me.CareLabel21.Name = "CareLabel21"
        Me.CareLabel21.Size = New System.Drawing.Size(80, 15)
        Me.CareLabel21.TabIndex = 6
        Me.CareLabel21.Text = "Allow Bolt-Ons"
        Me.CareLabel21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel21.ToolTip = "Selecting this option allows you to select additional bolt-ons as well as a tarif" & _
    "f." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "This function is typically used for chargeable meals or activities." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.CareLabel21.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.CareLabel21.ToolTipTitle = "Allow Bolt-Ons"
        '
        'chkBoltOns
        '
        Me.chkBoltOns.EnterMoveNextControl = True
        Me.chkBoltOns.Location = New System.Drawing.Point(135, 37)
        Me.chkBoltOns.Name = "chkBoltOns"
        Me.chkBoltOns.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkBoltOns.Properties.Appearance.Options.UseFont = True
        Me.chkBoltOns.Size = New System.Drawing.Size(20, 19)
        Me.chkBoltOns.TabIndex = 7
        Me.chkBoltOns.Tag = "AE"
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(171, 13)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(120, 15)
        Me.CareLabel7.TabIndex = 2
        Me.CareLabel7.Text = "Multiple Session Mode"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel7.ToolTip = resources.GetString("CareLabel7.ToolTip")
        Me.CareLabel7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.CareLabel7.ToolTipTitle = "Multiple Session Mode"
        '
        'chkDaily
        '
        Me.chkDaily.EnterMoveNextControl = True
        Me.chkDaily.Location = New System.Drawing.Point(297, 11)
        Me.chkDaily.Name = "chkDaily"
        Me.chkDaily.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDaily.Properties.Appearance.Options.UseFont = True
        Me.chkDaily.Size = New System.Drawing.Size(20, 19)
        Me.chkDaily.TabIndex = 3
        Me.chkDaily.Tag = "AE"
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(12, 13)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(108, 15)
        Me.CareLabel6.TabIndex = 0
        Me.CareLabel6.Text = "Single Session Mode"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel6.ToolTip = resources.GetString("CareLabel6.ToolTip")
        Me.CareLabel6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.CareLabel6.ToolTipTitle = "Single Session Mode"
        '
        'chkWeekly
        '
        Me.chkWeekly.EnterMoveNextControl = True
        Me.chkWeekly.Location = New System.Drawing.Point(135, 11)
        Me.chkWeekly.Name = "chkWeekly"
        Me.chkWeekly.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkWeekly.Properties.Appearance.Options.UseFont = True
        Me.chkWeekly.Size = New System.Drawing.Size(20, 19)
        Me.chkWeekly.TabIndex = 1
        Me.chkWeekly.Tag = "AE"
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.txtVoidEnd)
        Me.GroupControl4.Controls.Add(Me.txtVoidStart)
        Me.GroupControl4.Controls.Add(Me.CareLabel15)
        Me.GroupControl4.Controls.Add(Me.CareLabel16)
        Me.GroupControl4.Controls.Add(Me.txtEndTime)
        Me.GroupControl4.Controls.Add(Me.txtStartTime)
        Me.GroupControl4.Controls.Add(Me.CareLabel4)
        Me.GroupControl4.Controls.Add(Me.CareLabel8)
        Me.GroupControl4.Location = New System.Drawing.Point(489, 112)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.ShowCaption = False
        Me.GroupControl4.Size = New System.Drawing.Size(227, 130)
        Me.GroupControl4.TabIndex = 5
        '
        'txtVoidEnd
        '
        Me.txtVoidEnd.CharacterCasing = CharacterCasing.Normal
        Me.txtVoidEnd.EnterMoveNextControl = True
        Me.txtVoidEnd.Location = New System.Drawing.Point(136, 97)
        Me.txtVoidEnd.MaxLength = 8
        Me.txtVoidEnd.Name = "txtVoidEnd"
        Me.txtVoidEnd.NumericAllowNegatives = False
        Me.txtVoidEnd.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.Time
        Me.txtVoidEnd.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtVoidEnd.Properties.AccessibleName = "Void End Time"
        Me.txtVoidEnd.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtVoidEnd.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtVoidEnd.Properties.Appearance.Options.UseFont = True
        Me.txtVoidEnd.Properties.Appearance.Options.UseTextOptions = True
        Me.txtVoidEnd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtVoidEnd.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic
        Me.txtVoidEnd.Properties.Mask.EditMask = "(0?\d|1\d|2[0-3])\:[0-5]\d"
        Me.txtVoidEnd.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtVoidEnd.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtVoidEnd.Properties.MaxLength = 8
        Me.txtVoidEnd.Size = New System.Drawing.Size(75, 22)
        Me.txtVoidEnd.TabIndex = 7
        Me.txtVoidEnd.Tag = "AE"
        Me.txtVoidEnd.TextAlign = HorizontalAlignment.Left
        Me.txtVoidEnd.ToolTipText = ""
        '
        'txtVoidStart
        '
        Me.txtVoidStart.CharacterCasing = CharacterCasing.Normal
        Me.txtVoidStart.EnterMoveNextControl = True
        Me.txtVoidStart.Location = New System.Drawing.Point(136, 69)
        Me.txtVoidStart.MaxLength = 8
        Me.txtVoidStart.Name = "txtVoidStart"
        Me.txtVoidStart.NumericAllowNegatives = False
        Me.txtVoidStart.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.Time
        Me.txtVoidStart.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtVoidStart.Properties.AccessibleName = "Void Start Time"
        Me.txtVoidStart.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtVoidStart.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtVoidStart.Properties.Appearance.Options.UseFont = True
        Me.txtVoidStart.Properties.Appearance.Options.UseTextOptions = True
        Me.txtVoidStart.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtVoidStart.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic
        Me.txtVoidStart.Properties.Mask.EditMask = "(0?\d|1\d|2[0-3])\:[0-5]\d"
        Me.txtVoidStart.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtVoidStart.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtVoidStart.Properties.MaxLength = 8
        Me.txtVoidStart.Size = New System.Drawing.Size(75, 22)
        Me.txtVoidStart.TabIndex = 5
        Me.txtVoidStart.Tag = "AE"
        Me.txtVoidStart.TextAlign = HorizontalAlignment.Left
        Me.txtVoidStart.ToolTipText = ""
        '
        'CareLabel15
        '
        Me.CareLabel15.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel15.Location = New System.Drawing.Point(12, 72)
        Me.CareLabel15.Name = "CareLabel15"
        Me.CareLabel15.Size = New System.Drawing.Size(81, 15)
        Me.CareLabel15.TabIndex = 4
        Me.CareLabel15.Text = "Void Start Time"
        Me.CareLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel16
        '
        Me.CareLabel16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel16.Location = New System.Drawing.Point(12, 100)
        Me.CareLabel16.Name = "CareLabel16"
        Me.CareLabel16.Size = New System.Drawing.Size(77, 15)
        Me.CareLabel16.TabIndex = 6
        Me.CareLabel16.Text = "Void End Time"
        Me.CareLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEndTime
        '
        Me.txtEndTime.CharacterCasing = CharacterCasing.Normal
        Me.txtEndTime.EnterMoveNextControl = True
        Me.txtEndTime.Location = New System.Drawing.Point(136, 41)
        Me.txtEndTime.MaxLength = 8
        Me.txtEndTime.Name = "txtEndTime"
        Me.txtEndTime.NumericAllowNegatives = False
        Me.txtEndTime.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.Time
        Me.txtEndTime.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtEndTime.Properties.AccessibleName = "End Time"
        Me.txtEndTime.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtEndTime.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtEndTime.Properties.Appearance.Options.UseFont = True
        Me.txtEndTime.Properties.Appearance.Options.UseTextOptions = True
        Me.txtEndTime.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtEndTime.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic
        Me.txtEndTime.Properties.Mask.EditMask = "(0?\d|1\d|2[0-3])\:[0-5]\d"
        Me.txtEndTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtEndTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtEndTime.Properties.MaxLength = 8
        Me.txtEndTime.Size = New System.Drawing.Size(75, 22)
        Me.txtEndTime.TabIndex = 3
        Me.txtEndTime.Tag = "AEM"
        Me.txtEndTime.TextAlign = HorizontalAlignment.Left
        Me.txtEndTime.ToolTipText = ""
        '
        'txtStartTime
        '
        Me.txtStartTime.CharacterCasing = CharacterCasing.Normal
        Me.txtStartTime.EnterMoveNextControl = True
        Me.txtStartTime.Location = New System.Drawing.Point(136, 12)
        Me.txtStartTime.MaxLength = 8
        Me.txtStartTime.Name = "txtStartTime"
        Me.txtStartTime.NumericAllowNegatives = False
        Me.txtStartTime.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.Time
        Me.txtStartTime.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStartTime.Properties.AccessibleName = "Start Time"
        Me.txtStartTime.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtStartTime.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtStartTime.Properties.Appearance.Options.UseFont = True
        Me.txtStartTime.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStartTime.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtStartTime.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic
        Me.txtStartTime.Properties.Mask.EditMask = "(0?\d|1\d|2[0-3])\:[0-5]\d"
        Me.txtStartTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtStartTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtStartTime.Properties.MaxLength = 8
        Me.txtStartTime.Size = New System.Drawing.Size(75, 22)
        Me.txtStartTime.TabIndex = 1
        Me.txtStartTime.Tag = "AEM"
        Me.txtStartTime.TextAlign = HorizontalAlignment.Left
        Me.txtStartTime.ToolTipText = ""
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(11, 15)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(96, 15)
        Me.CareLabel4.TabIndex = 0
        Me.CareLabel4.Text = "Session Start Time"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(11, 44)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(92, 15)
        Me.CareLabel8.TabIndex = 2
        Me.CareLabel8.Text = "Session End Time"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl5
        '
        Me.GroupControl5.Controls.Add(Me.CareLabel13)
        Me.GroupControl5.Controls.Add(Me.CareLabel12)
        Me.GroupControl5.Controls.Add(Me.txtRateFund36)
        Me.GroupControl5.Controls.Add(Me.txtRateFund24)
        Me.GroupControl5.Controls.Add(Me.CareLabel9)
        Me.GroupControl5.Controls.Add(Me.chkFundingHours)
        Me.GroupControl5.Location = New System.Drawing.Point(13, 373)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(703, 60)
        Me.GroupControl5.TabIndex = 8
        Me.GroupControl5.Text = "Funding"
        '
        'CareLabel13
        '
        Me.CareLabel13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel13.Location = New System.Drawing.Point(495, 32)
        Me.CareLabel13.Name = "CareLabel13"
        Me.CareLabel13.Size = New System.Drawing.Size(111, 15)
        Me.CareLabel13.TabIndex = 4
        Me.CareLabel13.Text = "Rate for 3-4 year olds"
        Me.CareLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel12
        '
        Me.CareLabel12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel12.Location = New System.Drawing.Point(280, 32)
        Me.CareLabel12.Name = "CareLabel12"
        Me.CareLabel12.Size = New System.Drawing.Size(100, 15)
        Me.CareLabel12.TabIndex = 2
        Me.CareLabel12.Text = "Rate for 2 year olds"
        Me.CareLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRateFund36
        '
        Me.txtRateFund36.CharacterCasing = CharacterCasing.Normal
        Me.txtRateFund36.EnterMoveNextControl = True
        Me.txtRateFund36.Location = New System.Drawing.Point(612, 29)
        Me.txtRateFund36.MaxLength = 14
        Me.txtRateFund36.Name = "txtRateFund36"
        Me.txtRateFund36.NumericAllowNegatives = False
        Me.txtRateFund36.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtRateFund36.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRateFund36.Properties.AccessibleName = ""
        Me.txtRateFund36.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRateFund36.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRateFund36.Properties.Appearance.Options.UseFont = True
        Me.txtRateFund36.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRateFund36.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRateFund36.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtRateFund36.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtRateFund36.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRateFund36.Properties.MaxLength = 14
        Me.txtRateFund36.Size = New System.Drawing.Size(75, 22)
        Me.txtRateFund36.TabIndex = 5
        Me.txtRateFund36.Tag = ""
        Me.txtRateFund36.TextAlign = HorizontalAlignment.Left
        Me.txtRateFund36.ToolTipText = ""
        '
        'txtRateFund24
        '
        Me.txtRateFund24.CharacterCasing = CharacterCasing.Normal
        Me.txtRateFund24.EnterMoveNextControl = True
        Me.txtRateFund24.Location = New System.Drawing.Point(386, 29)
        Me.txtRateFund24.MaxLength = 14
        Me.txtRateFund24.Name = "txtRateFund24"
        Me.txtRateFund24.NumericAllowNegatives = False
        Me.txtRateFund24.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtRateFund24.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRateFund24.Properties.AccessibleName = ""
        Me.txtRateFund24.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRateFund24.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRateFund24.Properties.Appearance.Options.UseFont = True
        Me.txtRateFund24.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRateFund24.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRateFund24.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtRateFund24.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtRateFund24.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRateFund24.Properties.MaxLength = 14
        Me.txtRateFund24.Size = New System.Drawing.Size(75, 22)
        Me.txtRateFund24.TabIndex = 3
        Me.txtRateFund24.Tag = ""
        Me.txtRateFund24.TextAlign = HorizontalAlignment.Left
        Me.txtRateFund24.ToolTipText = ""
        '
        'chkHoliday
        '
        Me.chkHoliday.EnterMoveNextControl = True
        Me.chkHoliday.Location = New System.Drawing.Point(230, 29)
        Me.chkHoliday.Name = "chkHoliday"
        Me.chkHoliday.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkHoliday.Properties.Appearance.Options.UseFont = True
        Me.chkHoliday.Size = New System.Drawing.Size(20, 19)
        Me.chkHoliday.TabIndex = 1
        Me.chkHoliday.Tag = "E"
        '
        'CareLabel10
        '
        Me.CareLabel10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel10.Location = New System.Drawing.Point(12, 31)
        Me.CareLabel10.Name = "CareLabel10"
        Me.CareLabel10.Size = New System.Drawing.Size(212, 15)
        Me.CareLabel10.TabIndex = 0
        Me.CareLabel10.Text = "Use separate rate during school holidays"
        Me.CareLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxTariff
        '
        Me.cbxTariff.AllowBlank = False
        Me.cbxTariff.DataSource = Nothing
        Me.cbxTariff.DisplayMember = Nothing
        Me.cbxTariff.EnterMoveNextControl = True
        Me.cbxTariff.Location = New System.Drawing.Point(256, 28)
        Me.cbxTariff.Name = "cbxTariff"
        Me.cbxTariff.Properties.AccessibleName = "Tariff Type"
        Me.cbxTariff.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxTariff.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxTariff.Properties.Appearance.Options.UseFont = True
        Me.cbxTariff.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxTariff.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxTariff.SelectedValue = Nothing
        Me.cbxTariff.Size = New System.Drawing.Size(205, 22)
        Me.cbxTariff.TabIndex = 2
        Me.cbxTariff.Tag = ""
        Me.cbxTariff.ValueMember = Nothing
        '
        'GroupControl7
        '
        Me.GroupControl7.Controls.Add(Me.CareLabel10)
        Me.GroupControl7.Controls.Add(Me.cbxTariff)
        Me.GroupControl7.Controls.Add(Me.chkHoliday)
        Me.GroupControl7.Location = New System.Drawing.Point(13, 259)
        Me.GroupControl7.Name = "GroupControl7"
        Me.GroupControl7.Size = New System.Drawing.Size(470, 60)
        Me.GroupControl7.TabIndex = 3
        Me.GroupControl7.Text = "School Holidays"
        '
        'btnBreakdown
        '
        Me.btnBreakdown.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnBreakdown.Location = New System.Drawing.Point(13, 3)
        Me.btnBreakdown.Name = "btnBreakdown"
        Me.btnBreakdown.Size = New System.Drawing.Size(110, 25)
        Me.btnBreakdown.TabIndex = 0
        Me.btnBreakdown.Text = "Tariff Usage"
        '
        'GroupControl8
        '
        Me.GroupControl8.Controls.Add(Me.cbxNLTrack)
        Me.GroupControl8.Controls.Add(Me.CareLabel22)
        Me.GroupControl8.Controls.Add(Me.cbxNLCode)
        Me.GroupControl8.Controls.Add(Me.CareLabel14)
        Me.GroupControl8.Location = New System.Drawing.Point(13, 325)
        Me.GroupControl8.Name = "GroupControl8"
        Me.GroupControl8.ShowCaption = False
        Me.GroupControl8.Size = New System.Drawing.Size(470, 42)
        Me.GroupControl8.TabIndex = 4
        '
        'cbxNLTrack
        '
        Me.cbxNLTrack.AllowBlank = False
        Me.cbxNLTrack.DataSource = Nothing
        Me.cbxNLTrack.DisplayMember = Nothing
        Me.cbxNLTrack.EnterMoveNextControl = True
        Me.cbxNLTrack.Location = New System.Drawing.Point(339, 11)
        Me.cbxNLTrack.Name = "cbxNLTrack"
        Me.cbxNLTrack.Properties.AccessibleName = "Tariff Type"
        Me.cbxNLTrack.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxNLTrack.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxNLTrack.Properties.Appearance.Options.UseFont = True
        Me.cbxNLTrack.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxNLTrack.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxNLTrack.SelectedValue = Nothing
        Me.cbxNLTrack.Size = New System.Drawing.Size(122, 22)
        Me.cbxNLTrack.TabIndex = 3
        Me.cbxNLTrack.Tag = "AE"
        Me.cbxNLTrack.ValueMember = Nothing
        '
        'CareLabel22
        '
        Me.CareLabel22.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel22.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel22.Location = New System.Drawing.Point(256, 14)
        Me.CareLabel22.Name = "CareLabel22"
        Me.CareLabel22.Size = New System.Drawing.Size(77, 15)
        Me.CareLabel22.TabIndex = 2
        Me.CareLabel22.Text = "Tracking Code"
        Me.CareLabel22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxNLCode
        '
        Me.cbxNLCode.AllowBlank = False
        Me.cbxNLCode.DataSource = Nothing
        Me.cbxNLCode.DisplayMember = Nothing
        Me.cbxNLCode.EnterMoveNextControl = True
        Me.cbxNLCode.Location = New System.Drawing.Point(137, 11)
        Me.cbxNLCode.Name = "cbxNLCode"
        Me.cbxNLCode.Properties.AccessibleName = "Tariff Type"
        Me.cbxNLCode.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxNLCode.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxNLCode.Properties.Appearance.Options.UseFont = True
        Me.cbxNLCode.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxNLCode.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxNLCode.SelectedValue = Nothing
        Me.cbxNLCode.Size = New System.Drawing.Size(113, 22)
        Me.cbxNLCode.TabIndex = 1
        Me.cbxNLCode.Tag = "AE"
        Me.cbxNLCode.ValueMember = Nothing
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(11, 14)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(119, 15)
        Me.CareLabel14.TabIndex = 0
        Me.CareLabel14.Text = "NL Code for Financials"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl9
        '
        Me.GroupControl9.Controls.Add(Me.chkSessHol)
        Me.GroupControl9.Controls.Add(Me.txtSessHolDiscount)
        Me.GroupControl9.Controls.Add(Me.CareLabel17)
        Me.GroupControl9.Controls.Add(Me.CareLabel11)
        Me.GroupControl9.Location = New System.Drawing.Point(489, 248)
        Me.GroupControl9.Name = "GroupControl9"
        Me.GroupControl9.Size = New System.Drawing.Size(227, 71)
        Me.GroupControl9.TabIndex = 6
        Me.GroupControl9.Text = "Session Holidays"
        '
        'chkSessHol
        '
        Me.chkSessHol.EnterMoveNextControl = True
        Me.chkSessHol.Location = New System.Drawing.Point(136, 23)
        Me.chkSessHol.Name = "chkSessHol"
        Me.chkSessHol.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSessHol.Properties.Appearance.Options.UseFont = True
        Me.chkSessHol.Size = New System.Drawing.Size(20, 19)
        Me.chkSessHol.TabIndex = 1
        Me.chkSessHol.Tag = "AE"
        '
        'txtSessHolDiscount
        '
        Me.txtSessHolDiscount.CharacterCasing = CharacterCasing.Normal
        Me.txtSessHolDiscount.EnterMoveNextControl = True
        Me.txtSessHolDiscount.Location = New System.Drawing.Point(136, 44)
        Me.txtSessHolDiscount.MaxLength = 14
        Me.txtSessHolDiscount.Name = "txtSessHolDiscount"
        Me.txtSessHolDiscount.NumericAllowNegatives = False
        Me.txtSessHolDiscount.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtSessHolDiscount.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSessHolDiscount.Properties.AccessibleName = ""
        Me.txtSessHolDiscount.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSessHolDiscount.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSessHolDiscount.Properties.Appearance.Options.UseFont = True
        Me.txtSessHolDiscount.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSessHolDiscount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSessHolDiscount.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtSessHolDiscount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtSessHolDiscount.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSessHolDiscount.Properties.MaxLength = 14
        Me.txtSessHolDiscount.Size = New System.Drawing.Size(75, 22)
        Me.txtSessHolDiscount.TabIndex = 3
        Me.txtSessHolDiscount.Tag = ""
        Me.txtSessHolDiscount.TextAlign = HorizontalAlignment.Left
        Me.txtSessHolDiscount.ToolTipText = ""
        '
        'CareLabel17
        '
        Me.CareLabel17.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel17.Location = New System.Drawing.Point(11, 47)
        Me.CareLabel17.Name = "CareLabel17"
        Me.CareLabel17.Size = New System.Drawing.Size(60, 15)
        Me.CareLabel17.TabIndex = 2
        Me.CareLabel17.Text = "Discount %"
        Me.CareLabel17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(11, 25)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(94, 15)
        Me.CareLabel11.TabIndex = 0
        Me.CareLabel11.Text = "Discount Sessions"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblUsage
        '
        Me.lblUsage.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUsage.Appearance.ForeColor = System.Drawing.Color.Orange
        Me.lblUsage.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblUsage.Location = New System.Drawing.Point(129, 8)
        Me.lblUsage.Name = "lblUsage"
        Me.lblUsage.Size = New System.Drawing.Size(172, 15)
        Me.lblUsage.TabIndex = 1
        Me.lblUsage.Text = "x Children linked to this Tariff"
        Me.lblUsage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnCancel.DialogResult = DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(643, 440)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(73, 23)
        Me.btnCancel.TabIndex = 10
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnOK.Location = New System.Drawing.Point(564, 440)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(73, 23)
        Me.btnOK.TabIndex = 9
        Me.btnOK.Text = "OK"
        '
        'GroupControl10
        '
        Me.GroupControl10.Controls.Add(Me.cbxSummaryColumn)
        Me.GroupControl10.Controls.Add(Me.CareLabel26)
        Me.GroupControl10.Location = New System.Drawing.Point(489, 325)
        Me.GroupControl10.Name = "GroupControl10"
        Me.GroupControl10.ShowCaption = False
        Me.GroupControl10.Size = New System.Drawing.Size(227, 42)
        Me.GroupControl10.TabIndex = 7
        '
        'cbxSummaryColumn
        '
        Me.cbxSummaryColumn.AllowBlank = False
        Me.cbxSummaryColumn.DataSource = Nothing
        Me.cbxSummaryColumn.DisplayMember = Nothing
        Me.cbxSummaryColumn.EnterMoveNextControl = True
        Me.cbxSummaryColumn.Location = New System.Drawing.Point(137, 11)
        Me.cbxSummaryColumn.Name = "cbxSummaryColumn"
        Me.cbxSummaryColumn.Properties.AccessibleName = "Summary Column"
        Me.cbxSummaryColumn.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSummaryColumn.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSummaryColumn.Properties.Appearance.Options.UseFont = True
        Me.cbxSummaryColumn.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSummaryColumn.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSummaryColumn.SelectedValue = Nothing
        Me.cbxSummaryColumn.Size = New System.Drawing.Size(74, 22)
        Me.cbxSummaryColumn.TabIndex = 1
        Me.cbxSummaryColumn.Tag = "AEM"
        Me.cbxSummaryColumn.ValueMember = Nothing
        '
        'CareLabel26
        '
        Me.CareLabel26.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel26.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel26.Location = New System.Drawing.Point(11, 14)
        Me.CareLabel26.Name = "CareLabel26"
        Me.CareLabel26.Size = New System.Drawing.Size(97, 15)
        Me.CareLabel26.TabIndex = 0
        Me.CareLabel26.Text = "Summary Column"
        Me.CareLabel26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmTariffPopup
        '
        Me.AcceptButton = Me.btnOK
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(728, 471)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupControl10)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.GroupControl9)
        Me.Controls.Add(Me.GroupControl8)
        Me.Controls.Add(Me.GroupControl7)
        Me.Controls.Add(Me.GroupControl5)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.Name = "frmTariffPopup"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "frmTariff"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtInvoiceText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceColour.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkOverride.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDuration.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.chkVariable.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkFundingHours.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.chkTimesVsDuration.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkDiscount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkBoltOns.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkDaily.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkWeekly.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.txtVoidEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtVoidStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEndTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStartTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.txtRateFund36.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRateFund24.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkHoliday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxTariff.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl7.ResumeLayout(False)
        Me.GroupControl7.PerformLayout()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl8.ResumeLayout(False)
        Me.GroupControl8.PerformLayout()
        CType(Me.cbxNLTrack.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxNLCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl9.ResumeLayout(False)
        Me.GroupControl9.PerformLayout()
        CType(Me.chkSessHol.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSessHolDiscount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl10.ResumeLayout(False)
        Me.GroupControl10.PerformLayout()
        CType(Me.cbxSummaryColumn.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtInvoiceText As Care.Controls.CareTextBox
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents lblRate As Care.Controls.CareLabel
    Friend WithEvents txtRate As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents txtDuration As Care.Controls.CareTextBox
    Friend WithEvents Label12 As Care.Controls.CareLabel
    Friend WithEvents cbxType As Care.Controls.CareComboBox
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents Label17 As Care.Controls.CareLabel
    Friend WithEvents lblMinutes As Care.Controls.CareLabel
    Friend WithEvents lblPerHour As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents chkOverride As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents ceColour As DevExpress.XtraEditors.ColorPickEdit
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents chkDaily As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents chkWeekly As Care.Controls.CareCheckBox
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents btnMatrix As Care.Controls.CareButton
    Friend WithEvents chkFundingHours As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents txtEndTime As Care.Controls.CareTextBox
    Friend WithEvents txtStartTime As Care.Controls.CareTextBox
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents chkHoliday As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel10 As Care.Controls.CareLabel
    Friend WithEvents cbxTariff As Care.Controls.CareComboBox
    Friend WithEvents GroupControl7 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnBreakdown As Care.Controls.CareButton
    Friend WithEvents txtVoidEnd As Care.Controls.CareTextBox
    Friend WithEvents txtVoidStart As Care.Controls.CareTextBox
    Friend WithEvents CareLabel15 As Care.Controls.CareLabel
    Friend WithEvents CareLabel16 As Care.Controls.CareLabel
    Friend WithEvents CareLabel13 As Care.Controls.CareLabel
    Friend WithEvents CareLabel12 As Care.Controls.CareLabel
    Friend WithEvents txtRateFund36 As Care.Controls.CareTextBox
    Friend WithEvents txtRateFund24 As Care.Controls.CareTextBox
    Friend WithEvents GroupControl8 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents GroupControl9 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtSessHolDiscount As Care.Controls.CareTextBox
    Friend WithEvents CareLabel17 As Care.Controls.CareLabel
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents chkSessHol As Care.Controls.CareCheckBox
    Friend WithEvents btnVariable As Care.Controls.CareButton
    Friend WithEvents chkVariable As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel18 As Care.Controls.CareLabel
    Friend WithEvents CareLabel19 As Care.Controls.CareLabel
    Friend WithEvents chkTimesVsDuration As Care.Controls.CareCheckBox
    Friend WithEvents chkDiscount As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel20 As Care.Controls.CareLabel
    Friend WithEvents CareLabel21 As Care.Controls.CareLabel
    Friend WithEvents chkBoltOns As Care.Controls.CareCheckBox
    Friend WithEvents lblUsage As Care.Controls.CareLabel
    Friend WithEvents cbxNLCode As Care.Controls.CareComboBox
    Friend WithEvents cbxNLTrack As Care.Controls.CareComboBox
    Friend WithEvents CareLabel22 As Care.Controls.CareLabel
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents GroupControl10 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cbxSummaryColumn As Care.Controls.CareComboBox
    Friend WithEvents CareLabel26 As Care.Controls.CareLabel
End Class
