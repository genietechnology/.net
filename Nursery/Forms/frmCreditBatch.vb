﻿Imports Care.Global
Imports Care.Shared
Imports Care.Data

Public Class frmCreditBatch

    Private m_BatchID As Guid? = Nothing
    Private m_BatchHeader As Business.CreditBatch = Nothing
    Private m_PrintPassword As Boolean = False
    Private m_PasswordEntered As Boolean = False

    Public Sub New()
        ' This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub

    Public Sub New(ByVal BatchID As Guid, ByVal Caption As String)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        m_BatchID = BatchID
        DisplayRecord()

        Me.Text = Caption
        Me.MinimizeBox = False
        Me.MaximizeBox = False
        Me.WindowState = FormWindowState.Normal
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.ToolbarMode = ToolbarEnum.EditOnly

    End Sub

    Private Sub frmInvoiceBatch_Load(sender As Object, e As EventArgs) Handles Me.Load
        m_PrintPassword = ParameterHandler.ReturnBoolean("FINPRINTMGRPASS")
        Me.ToolbarMode = ToolbarEnum.FindandNew
        SetCMDs()
    End Sub

    Protected Overrides Sub AfterAdd()

        'm_BatchID = Nothing
        'Clear()

        'Dim _Inv As New frmcre
        '_Inv.ShowDialog()

        'If _Inv.DialogResult = DialogResult.OK Then
        '    m_BatchID = CType(_Inv.Tag, Guid)
        '    DisplayRecord()
        'Else
        '    Me.ToolbarMode = ToolbarEnum.FindandNew
        'End If

        ToolAccept()

    End Sub

    Private Sub Clear()
        Me.RecordPopulated = False
        txtBatchNo.Text = ""
        txtStatus.Text = ""
        txtComments.Text = ""
        cgbCredits.Clear()
        SetCMDs()
    End Sub

    Private Sub DisplayCredits()

        Dim _SQL As String = ""
        _SQL += "select c.ID, c.family_account_no as 'Account No', c.credit_ref as 'Ref.', c.credit_status as 'Status',"
        _SQL += " c.credit_date as 'Credit Date', c.credit_due as 'Credit Due', c.credit_total as 'Total'"
        _SQL += " from Credits c"
        _SQL += " where c.batch_id = '" + m_BatchID.Value.ToString + "'"
        _SQL += " order by c.credit_no"

        cgbCredits.HideFirstColumn = True
        cgbCredits.Populate(Session.ConnectionString, _SQL)

        If cgbCredits.RecordCount > 0 Then

        End If

    End Sub

    Private Sub SetItemCMDs(ByVal Enabled As Boolean)
        btnAmendCredit.Enabled = Enabled
        btnDeleteCredit.Enabled = Enabled
    End Sub

    Protected Overrides Function BeforeDelete() As Boolean
        If txtStatus.Text <> "Open" Then
            CareMessage("Only Open batches can be Abandoned.", MessageBoxIcon.Exclamation, "Abandon Batch")
            Return False
        Else
            Return True
        End If
    End Function

    Protected Overrides Sub CommitDelete()

        m_BatchHeader._BatchStatus = "Abandoned"
        m_BatchHeader.Store()

        Clear()
        Me.ToolbarMode = ToolbarEnum.FindandNew

    End Sub

    Protected Overrides Function BeforeEdit() As Boolean
        CareMessage("Credit Batches cannot be amended.", MessageBoxIcon.Exclamation, "Edit Batch")
        Return False
    End Function

    Protected Overrides Sub AfterEdit()

        Dim _Inv As New frmInvoiceBatchCreate(m_BatchID.Value)
        _Inv.ShowDialog()

        ToolAccept()
        Me.ToolbarMode = ToolbarEnum.Standard

    End Sub

    Protected Overrides Sub CommitUpdate()
        'stub for creating new batch etc
    End Sub

    Protected Overrides Sub SetBindings()
        'stub
    End Sub

    Protected Overrides Sub FindRecord()

        Dim _Find As New GenericFind
        With _Find

            .Caption = "Find Credit Batch"
            .ParentForm = ParentForm
            .PopulateOnLoad = True
            .ConnectionString = Session.ConnectionString

            .GridSQL = "select batch_no as 'Batch No', batch_status as 'Status'," &
                       " (select count(*) from Credits where Credits.batch_id = CreditBatch.ID) 'Items', comments" &
                       " from CreditBatch"

            .GridOrderBy = "order by batch_no desc"
            .ReturnField = "ID"
            .FormWidth = 700
            .PreviewField = "comments"
            .Show()

            If .ReturnValue <> "" Then
                m_BatchID = New Guid(.ReturnValue)
                DisplayRecord()
            End If

        End With

    End Sub

    Private Sub DisplayRecord()

        Session.CursorWaiting()

        'fetch the header record
        m_BatchHeader = Business.CreditBatch.RetreiveByID(m_BatchID.Value)

        DisplayCredits()

        Me.ToolbarMode = ToolbarEnum.Standard
        Me.RecordPopulated = True

        txtBatchNo.Text = m_BatchHeader._BatchNo.ToString
        txtStatus.Text = m_BatchHeader._BatchStatus
        txtComments.Text = m_BatchHeader._Comments

        SetCMDs()

        Me.ToolbarMode = ToolbarEnum.Standard
        Session.CursorDefault()

    End Sub

    Private Sub SetCMDs()

        If m_BatchHeader Is Nothing Then
            btnAddCredit.Enabled = False
            btnPost.Enabled = False
            btnPrintBatch.Enabled = False
            SetItemCMDs(False)
        Else

            'confirmed isnt used anymore, but we need to keep it as some people might have confirmed batches in their system
            If m_BatchHeader._BatchStatus = "Open" Or m_BatchHeader._BatchStatus = "Confirmed" Then
                btnAddCredit.Enabled = True
                If cgbCredits.RecordCount > 0 Then
                    btnPost.Enabled = True
                    SetItemCMDs(True)
                Else
                    btnPost.Enabled = False
                    SetItemCMDs(False)
                End If
            Else
                btnAddCredit.Enabled = False
                btnPost.Enabled = False
                SetItemCMDs(False)
            End If

            If cgbCredits.RecordCount > 0 Then
                btnPrintBatch.Enabled = True
            Else
                btnPrintBatch.Enabled = False
            End If

        End If

    End Sub

    Protected Overrides Sub SetBindingsByID(ID As Guid)
        'stub
    End Sub

    Private Sub btnPrintSummary_Click(sender As Object, e As EventArgs)

    End Sub

    Private Function ReturnInvoiceList() As List(Of Guid)

        Dim _Invoices As New List(Of Guid)

        Dim _SQL As String = "select ID from Invoices where batch_id = '" + m_BatchID.Value.ToString + "'"
        Dim _DT As DataTable = Care.Data.DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows
                Dim _ID As Guid = New Guid(_DR.Item("ID").ToString)
                _Invoices.Add(_ID)
            Next

            _DT.Dispose()
            _DT = Nothing

        End If

        Return _Invoices

    End Function

    Private Sub btnAmendInvoice_Click(sender As Object, e As EventArgs) Handles btnAmendCredit.Click
        AmendInvoice()
    End Sub

    Private Sub cgbInvoices_GridDoubleClick(sender As Object, e As EventArgs) Handles cgbCredits.GridDoubleClick
        AmendInvoice()
    End Sub

    Private Sub AmendInvoice()

        If cgbCredits.RecordCount < 1 Then Exit Sub
        Dim _ID As New Guid(cgbCredits.CurrentRow.Item("id").ToString)

        If btnAmendCredit.Enabled Then

            cgbCredits.PersistRowPosition()
            If Credits.AmendCredit(_ID) Then
                DisplayCredits()
            End If
            cgbCredits.RestoreRowPosition()

        Else
            Invoicing.ViewInvoice(_ID)
        End If

    End Sub

    Private Sub btnPost_Click(sender As Object, e As EventArgs) Handles btnPost.Click

        If Not PostCheck() Then Exit Sub

        If Session.FinancialsIntegrated AndAlso Session.FinancialSystem.ToUpper = "QUICKBOOKS" Then

            'THIS NEEDS TO BE DONE FOR PLAYTIME NURSERIES
            '*******************************************************
            'Dim _frm As New frmInvoiceQBPreview(m_BatchID.Value)
            '_frm.Text = "Quickbooks Data Export Data"
            '_frm.ShowDialog()

            'Clear()
            'Me.ToolbarMode = ToolbarEnum.FindandNew

        Else

            If CareMessage("Are you sure you want to Post this Credit Batch?", MessageBoxIcon.Question, MessageBoxButtons.YesNo, "Post Credits") = DialogResult.Yes Then
                btnPost.Enabled = False
                Credits.PostBatch(m_BatchID.Value, m_BatchHeader._SiteId.Value)
                DisplayRecord()
                btnPost.Enabled = True
            End If

        End If

    End Sub

    Private Function PostCheck() As Boolean

        'if we are integrated with a financials system, we need to ensure all credits have been printed or emailed
        If Session.FinancialsIntegrated Then

            If Not Invoicing.BatchReadyToPost(m_BatchID.Value) Then

                Dim _Mess As String = "One or more credits have not yet been printed or emailed. Are you sure you want to Continue?"
                If CareMessage(_Mess, MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Post Check") = DialogResult.Yes Then
                    If InputBox("Please type POST to continue.", "Force Post").ToUpper <> "POST" Then
                        Return False
                    End If
                Else
                    Return False
                End If

            End If

        End If

        Return True

    End Function

    Private Sub btnDeleteInvoice_Click(sender As Object, e As EventArgs) Handles btnDeleteCredit.Click

        If cgbCredits.RecordCount < 1 Then Exit Sub

        If CareMessage("Are you sure you want to Delete this Credit?", MessageBoxIcon.Warning, MessageBoxButtons.YesNo, "Delete Invoice") = DialogResult.Yes Then
            Dim _ID As String = cgbCredits.CurrentRow.Item("id").ToString
            Credits.DeleteCredit(_ID)
            DisplayRecord()
        End If

    End Sub

    Private Sub btnAddCredit_Click(sender As Object, e As EventArgs) Handles btnAddCredit.Click

        Dim _ChildID As String = Business.Child.FindChild(Me)

        If _ChildID <> "" Then

            'check if this child is already in this batch
            If ChildInBatch(_ChildID) Then
                If CareMessage("This Child is already in this Invoice Batch. Are you sure you want to Add them again?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Add Child") = DialogResult.No Then
                    Exit Sub
                End If
            End If

            Dim _frm As New frmCredit(frmCredit.EnumMode.CreateCreditNote, m_BatchID.Value, New Guid(_ChildID))
            _frm.ShowDialog()

            If _frm.DialogResult = DialogResult.OK Then
                DisplayRecord()
                SetCMDs()
            End If

            _frm.Dispose()
            _frm = Nothing

        End If

    End Sub

    Private Function ChildInBatch(ByVal ChildID As String) As Boolean

        Dim _SQL As String = ""
        _SQL += "select * from Credits"
        _SQL += " where batch_id = '" + m_BatchID.Value.ToString + "'"
        _SQL += " and child_id = '" + ChildID + "'"

        Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DR IsNot Nothing Then
            _DR = Nothing
            Return True
        Else
            Return False
        End If

    End Function

    Private Sub cgbInvoices_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles cgbCredits.RowStyle
        If e.RowHandle < 0 Then Exit Sub
        'If cgbCredits.UnderlyingGridView.GetRowCellDisplayText(e.RowHandle, "changed_by").ToString <> "" Then
        '    e.Appearance.BackColor = txtYellow.BackColor
        'End If
    End Sub

    Private Sub txtStatus_DoubleClick(sender As Object, e As EventArgs) Handles txtStatus.DoubleClick
        If txtStatus.Text = "Posted" Then
            If InputBox("Enter password to Reset Batch back to Open:", "Override Batch") = "care122126" Then
                m_BatchHeader._BatchStatus = "Open"
                m_BatchHeader.Store()
                DisplayRecord()
            End If
        End If
    End Sub

End Class
