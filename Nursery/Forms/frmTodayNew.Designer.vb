﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTodayNew
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxNew = New Care.Controls.CareFrame()
        Me.radMonday = New Care.Controls.CareRadioButton(Me.components)
        Me.radTomorrow = New Care.Controls.CareRadioButton(Me.components)
        Me.radToday = New Care.Controls.CareRadioButton(Me.components)
        Me.btnNewOK = New Care.Controls.CareButton(Me.components)
        Me.btnNewCancel = New Care.Controls.CareButton(Me.components)
        CType(Me.gbxNew, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxNew.SuspendLayout()
        CType(Me.radMonday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radTomorrow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radToday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'gbxNew
        '
        Me.gbxNew.Controls.Add(Me.radMonday)
        Me.gbxNew.Controls.Add(Me.radTomorrow)
        Me.gbxNew.Controls.Add(Me.radToday)
        Me.gbxNew.Location = New System.Drawing.Point(12, 12)
        Me.gbxNew.Name = "gbxNew"
        Me.gbxNew.ShowCaption = False
        Me.gbxNew.Size = New System.Drawing.Size(156, 90)
        Me.gbxNew.TabIndex = 1
        '
        'radMonday
        '
        Me.radMonday.Location = New System.Drawing.Point(11, 60)
        Me.radMonday.Name = "radMonday"
        Me.radMonday.Properties.AutoWidth = True
        Me.radMonday.Properties.Caption = "Monday"
        Me.radMonday.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radMonday.Properties.RadioGroupIndex = 0
        Me.radMonday.Size = New System.Drawing.Size(60, 19)
        Me.radMonday.TabIndex = 2
        Me.radMonday.TabStop = False
        '
        'radTomorrow
        '
        Me.radTomorrow.Location = New System.Drawing.Point(11, 35)
        Me.radTomorrow.Name = "radTomorrow"
        Me.radTomorrow.Properties.AutoWidth = True
        Me.radTomorrow.Properties.Caption = "Tomorrow"
        Me.radTomorrow.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radTomorrow.Properties.RadioGroupIndex = 0
        Me.radTomorrow.Size = New System.Drawing.Size(70, 19)
        Me.radTomorrow.TabIndex = 1
        Me.radTomorrow.TabStop = False
        '
        'radToday
        '
        Me.radToday.EditValue = True
        Me.radToday.Location = New System.Drawing.Point(11, 10)
        Me.radToday.Name = "radToday"
        Me.radToday.Properties.AutoWidth = True
        Me.radToday.Properties.Caption = "Today"
        Me.radToday.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radToday.Properties.RadioGroupIndex = 0
        Me.radToday.Size = New System.Drawing.Size(52, 19)
        Me.radToday.TabIndex = 0
        '
        'btnNewOK
        '
        Me.btnNewOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnNewOK.Appearance.Options.UseFont = True
        Me.btnNewOK.DialogResult = DialogResult.Cancel
        Me.btnNewOK.Location = New System.Drawing.Point(12, 108)
        Me.btnNewOK.Name = "btnNewOK"
        Me.btnNewOK.Size = New System.Drawing.Size(75, 25)
        Me.btnNewOK.TabIndex = 3
        Me.btnNewOK.Tag = ""
        Me.btnNewOK.Text = "OK"
        '
        'btnNewCancel
        '
        Me.btnNewCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnNewCancel.Appearance.Options.UseFont = True
        Me.btnNewCancel.DialogResult = DialogResult.Cancel
        Me.btnNewCancel.Location = New System.Drawing.Point(93, 108)
        Me.btnNewCancel.Name = "btnNewCancel"
        Me.btnNewCancel.Size = New System.Drawing.Size(75, 25)
        Me.btnNewCancel.TabIndex = 4
        Me.btnNewCancel.Tag = ""
        Me.btnNewCancel.Text = "Cancel"
        '
        'frmTodayNew
        '
        Me.AcceptButton = Me.btnNewOK
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.CancelButton = Me.btnNewCancel
        Me.ClientSize = New System.Drawing.Size(179, 143)
        Me.Controls.Add(Me.gbxNew)
        Me.Controls.Add(Me.btnNewCancel)
        Me.Controls.Add(Me.btnNewOK)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTodayNew"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "Create New Day"
        CType(Me.gbxNew, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxNew.ResumeLayout(False)
        CType(Me.radMonday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radTomorrow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radToday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxNew As DevExpress.XtraEditors.GroupControl
    Friend WithEvents radMonday As Care.Controls.CareRadioButton
    Friend WithEvents radTomorrow As Care.Controls.CareRadioButton
    Friend WithEvents radToday As Care.Controls.CareRadioButton
    Friend WithEvents btnNewOK As Care.Controls.CareButton
    Friend WithEvents btnNewCancel As Care.Controls.CareButton
End Class
