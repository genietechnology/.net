﻿Imports Care.Global
Imports System.Windows.Forms
Imports Care.Shared

Public Class frmReportTariffs

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnRun_Click(sender As System.Object, e As System.EventArgs) Handles btnRun.Click

        Cursor.Current = Cursors.WaitCursor
        btnRun.Enabled = False
        btnClose.Enabled = False

        Dim _SQL As String = "select name, rate_1, rate_2, rate_3, rate_4, rate_5, rate_total from Tariffs" & _
                             " order by rate_1, name"

        Dim _Rpt As New ReportHandler
        With _Rpt
            .ReportOutput = ReportHandler.OutputMode.Preview
            .ReportName = "Tariff Report"
            .ReportFile = "tariffs.rpt"
            .ReportSQL = _SQL
            .RunReport(False)
        End With

        _Rpt = Nothing

        btnRun.Enabled = True
        btnClose.Enabled = True
        Cursor.Current = Cursors.Default

    End Sub
End Class