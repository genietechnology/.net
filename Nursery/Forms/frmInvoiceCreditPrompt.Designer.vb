﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInvoiceCreditPrompt
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.btnCreditBlank = New Care.Controls.CareButton(Me.components)
        Me.btnInvoice = New Care.Controls.CareButton(Me.components)
        Me.btnInvoiceBlank = New Care.Controls.CareButton(Me.components)
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'btnClose
        '
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.CausesValidation = False
        Me.btnClose.DialogResult = DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(452, 14)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(140, 25)
        Me.btnClose.TabIndex = 17
        Me.btnClose.Tag = ""
        Me.btnClose.Text = "Cancel"
        '
        'btnCreditBlank
        '
        Me.btnCreditBlank.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCreditBlank.Appearance.Options.UseFont = True
        Me.btnCreditBlank.CausesValidation = False
        Me.btnCreditBlank.Location = New System.Drawing.Point(306, 14)
        Me.btnCreditBlank.Name = "btnCreditBlank"
        Me.btnCreditBlank.Size = New System.Drawing.Size(140, 25)
        Me.btnCreditBlank.TabIndex = 16
        Me.btnCreditBlank.Tag = ""
        Me.btnCreditBlank.Text = "Raise Blank Credit"
        '
        'btnInvoice
        '
        Me.btnInvoice.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnInvoice.Appearance.Options.UseFont = True
        Me.btnInvoice.CausesValidation = False
        Me.btnInvoice.Location = New System.Drawing.Point(14, 14)
        Me.btnInvoice.Name = "btnInvoice"
        Me.btnInvoice.Size = New System.Drawing.Size(140, 25)
        Me.btnInvoice.TabIndex = 13
        Me.btnInvoice.Tag = ""
        Me.btnInvoice.Text = "Raise Invoice"
        '
        'btnInvoiceBlank
        '
        Me.btnInvoiceBlank.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnInvoiceBlank.Appearance.Options.UseFont = True
        Me.btnInvoiceBlank.CausesValidation = False
        Me.btnInvoiceBlank.Location = New System.Drawing.Point(160, 14)
        Me.btnInvoiceBlank.Name = "btnInvoiceBlank"
        Me.btnInvoiceBlank.Size = New System.Drawing.Size(140, 25)
        Me.btnInvoiceBlank.TabIndex = 14
        Me.btnInvoiceBlank.Tag = ""
        Me.btnInvoiceBlank.Text = "Raise Blank Invoice"
        '
        'frmInvoiceCreditPrompt
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(605, 52)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnCreditBlank)
        Me.Controls.Add(Me.btnInvoice)
        Me.Controls.Add(Me.btnInvoiceBlank)
        Me.Margin = New Padding(3, 5, 3, 5)
        Me.Name = "frmInvoiceCreditPrompt"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "Raise Invoice or Credit Note"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents btnCreditBlank As Care.Controls.CareButton
    Friend WithEvents btnInvoice As Care.Controls.CareButton
    Friend WithEvents btnInvoiceBlank As Care.Controls.CareButton
End Class
