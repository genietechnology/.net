﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCDAPUpload
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New Care.Controls.CareLabel()
        Me.Panel1 = New Panel()
        Me.btnUpload = New Care.Controls.CareButton()
        Me.btnClose = New Care.Controls.CareButton()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.Label1.Appearance.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.Label1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.Label1.Location = New System.Drawing.Point(12, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(281, 40)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Connect your Digital Camera to the computer and click Upload."
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnUpload)
        Me.Panel1.Controls.Add(Me.btnClose)
        Me.Panel1.Dock = DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 67)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(305, 38)
        Me.Panel1.TabIndex = 6
        '
        'btnUpload
        '
        Me.btnUpload.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnUpload.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnUpload.Appearance.Options.UseFont = True
        Me.btnUpload.Location = New System.Drawing.Point(117, 3)
        Me.btnUpload.Name = "btnUpload"
        Me.btnUpload.Size = New System.Drawing.Size(85, 25)
        Me.btnUpload.TabIndex = 1
        Me.btnUpload.Text = "Upload"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(208, 3)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(85, 25)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        '
        'frmCDAPUpload
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(305, 105)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCDAPUpload"
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As Care.Controls.CareLabel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btnUpload As Care.Controls.CareButton
    Friend WithEvents btnClose As Care.Controls.CareButton

End Class
