﻿Imports Care.Global

Public Class frmStaffPerf

    Private m_StaffID As Guid
    Private m_RecordID As Guid?
    Private m_IsNew As Boolean = True
    Private m_AppDis As EnumAppDis = EnumAppDis.Appraisal

    Public Enum EnumAppDis
        Appraisal
        Disciplinary
    End Enum

    Public Sub New(ByVal AppDisType As EnumAppDis, ByVal StaffID As Guid, ByVal RecordID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        m_StaffID = StaffID

        If RecordID.HasValue Then
            m_IsNew = False
            m_RecordID = RecordID
        Else
            m_IsNew = True
            m_RecordID = Nothing
        End If

        m_AppDis = AppDisType

    End Sub

    Private Sub frmChildCharge_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cdtDate.BackColor = Session.ChangeColour
        txtSubject.BackColor = Session.ChangeColour
        txtNotes.BackColor = Session.ChangeColour

        If m_RecordID.HasValue Then
            Me.Text = "Edit " + m_AppDis.ToString + " Record"
            DisplayRecord()
        Else
            Me.Text = "Create New " + m_AppDis.ToString + " Record"
        End If

        gbx.Text = m_AppDis.ToString + " Details"

    End Sub

    Private Sub frmSession_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        cdtDate.Focus()
    End Sub

    Private Sub DisplayRecord()

        Dim _C As Business.StaffAppDis = Business.StaffAppDis.RetreiveByID(m_RecordID.Value)
        With _C
            cdtDate.Value = ._ActionDate
            txtSubject.Text = ._Subject
            txtNotes.Text = ._Notes
        End With

        _C = Nothing

    End Sub

    Private Function ValidateEntry() As Boolean
        Return MyControls.Validate(Me.Controls)
    End Function

    Private Sub SaveAndExit()

        If Not ValidateEntry() Then Exit Sub

        Dim _C As Business.StaffAppDis = Nothing

        If m_IsNew Then
            _C = New Business.StaffAppDis
            _C._ID = Guid.NewGuid
            _C._StaffId = m_StaffID
            _C._Type = m_AppDis.ToString
        Else
            _C = Business.StaffAppDis.RetreiveByID(m_RecordID.Value)
        End If

        With _C
            ._ActionDate = cdtDate.Value
            ._Subject = txtSubject.Text
            ._Notes = txtNotes.Text
            .Store()
        End With

        _C = Nothing

        Me.DialogResult = DialogResult.OK
        Me.Close()

    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        SaveAndExit()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub
End Class
