﻿
Imports Care.Shared
Imports Care.Global
Imports DevExpress.XtraGrid.Views.Grid

Public Class frmRosterShiftNew

    Private m_RotaID As Guid
    Private m_SiteID As Guid
    Private m_RotaDate As Date
    Private m_RatioID As Guid
    Private m_RoomName As String

    Public Sub New(ByVal RotaID As Guid, ByVal SiteID As Guid, ByVal RotaDate As Date, ByVal RatioID As Guid, ByVal RoomName As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_RotaID = RotaID
        m_SiteID = SiteID
        m_RotaDate = RotaDate
        m_RatioID = RatioID
        m_RoomName = RoomName

    End Sub
    Private Sub frmRosterShiftNew_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Me.Text = "Create New Shift"

        txtStaff.Tag = ""
        txtShiftCost.Tag = 0

        txtDate.Text = Format(m_RotaDate, "dddd d MMMM yyyy")
        txtRoom.Text = m_RoomName

        txtShiftStart.BackColor = Session.ChangeColour
        txtShiftEnd.BackColor = Session.ChangeColour

        DisplayAvailable()
        btnView.Enabled = False

    End Sub

    Private Sub btnSelect_Click(sender As Object, e As EventArgs) Handles btnSelect.Click
        SelectStaff()
    End Sub

    Private Sub SelectStaff()

        Dim _SQL As String = ""

        _SQL += "select status as 'Status', fullname as 'Name', site_name as 'Site', group_name as 'Group', job_title as 'Job Title',"
        _SQL += " contract_type as 'Contract', line_manager as 'Line Manager', keyworker as 'Key Person', qual_level as 'Level', clock_no as 'Clock No',"
        _SQL += " (select top 1 p.wc from StaffPrefHours p where p.staff_id = Staff.ID order by p.wc desc) as 'Latest Pattern'"
        _SQL += " from Staff"

        Dim _ReturnValue As String = ""
        Dim _Find As New GenericFind
        With _Find
            .Caption = "Find Staff"
            .ParentForm = Me
            .PopulateOnLoad = True
            .ConnectionString = Session.ConnectionString
            .GridSQL = _SQL
            .GridOrderBy = "order by forename, surname"
            .ReturnField = "ID"
            .GridWhereClause = " and status = 'C' and site_id = '" + m_SiteID.ToString + "'"
            .Option1Caption = "Name"
            .Option1Field = "fullname"
            .Option2Caption = "Group"
            .Option2Field = "group_name"
            .FormWidth = 900
            .Show()
            _ReturnValue = .ReturnValue
        End With

        If _ReturnValue <> "" Then

            Dim _S As Business.Staff = Business.Staff.RetreiveByID(New Guid(_ReturnValue))
            txtStaff.Tag = _S._ID.ToString
            txtStaff.Text = _S._Fullname
            txtShiftCost.Tag = _S._SalaryHour

            DisplayShifts(_S._ID.ToString)

            btnView.Enabled = True
            txtShiftStart.Focus()

        End If

    End Sub

    Private Sub DisplayShifts(ByVal StaffID As String)

        Dim _SQL As String = ""

        _SQL += "select RotaStaff.ID, rs_date As 'Date', datename(DW, rs_date) as 'Day', rs_room as 'Room',"
        _SQL += " rs_start As 'Shift Start', rs_finish as 'Shift End', rs_hours as 'Hours' from RotaStaff"
        _SQL += " where rs_rota_id = '" + m_RotaID.ToString + "'"
        _SQL += " and rs_staff_id = '" + StaffID + "'"
        _SQL += " order by rs_date"

        cgShifts.HideFirstColumn = True
        cgShifts.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub DisplayAvailable()

        Dim _SQL As String = ""
        Dim _Date As String = ValueHandler.SQLDate(m_RotaDate, True)

        _SQL += "select s.ID, s.fullname As 'Name', s.group_name as 'Room', s.job_title as 'Job Title' from Staff s"
        _SQL += " where s.site_id = '" + m_SiteID.ToString + "'"
        _SQL += " and s.status = 'C'"
        _SQL += " and s.id not in (select rs.rs_staff_id from RotaStaff rs where rs.rs_rota_id = '" + m_RotaID.ToString + "' and rs.rs_date = " + _Date + " and rs.rs_staff_id = s.ID)"
        _SQL += " and s.id not in (select a.staff_id from StaffAbsence a where a.staff_id = s.ID and " + _Date + " between a.abs_from and a.abs_to)"
        _SQL += " order by s.fullname"

        cgNotRostered.HideFirstColumn = True
        cgNotRostered.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub btnShiftOK_Click(sender As Object, e As EventArgs) Handles btnShiftOK.Click


        '--use RotaSlotStaff to update RotaStaff

        '--update RotaSlot (the loop), with the New summary

        'Me.Close()

    End Sub

    Private Sub AllocateSlots(ByVal RotaID As Guid, ByVal StaffID As Guid, ByVal RotaDate As Date, ByVal TimeStart As TimeSpan, ByVal TimeEnd As TimeSpan)

        Dim _Staff As Business.Staff = Business.Staff.RetreiveByID(StaffID)
        If _Staff IsNot Nothing Then

            'get the slots defined from RotaSlot using the rotaid, date and times
            Dim _Slots As List(Of Business.RotaSlot) = Business.RotaSlot.RetreiveByDate(RotaID, RotaDate)
            For Each _S In _Slots

                'loop through these allocating the staff member, updating RotaSlotStaff

                'Dim _SlotStaff As New Business.RotaSlotStaff

                '_SlotStaff._RssRotaId = RotaID
                '_SlotStaff._RssSlotId = _S._SlotId
                '_SlotStaff._RssStatus = "Planned"

                '_SlotStaff._RssDate = _Slot._SlotDate
                '_SlotStaff._RssStart = FixTime(_Slot._SlotStart.Value)
                '_SlotStaff._RssFinish = FixTime(_Slot._SlotFinish.Value)

                '_SlotStaff._RssRatio = _Slot._SlotRatio
                '_SlotStaff._RssRoom = _Slot._SlotRoom

                'Dim _Diff As TimeSpan = _SlotStaff._RssFinish.Value - _SlotStaff._RssStart.Value
                '_SlotStaff._RssHours = ValueHandler.ConvertDecimal(_Diff.TotalHours)
                '_SlotStaff._RssCost = _SlotStaff._RssHours * _Staff.StaffRecord._SalaryHour

                '_SlotStaff._RssStaffId = _Staff.StaffID
                '_SlotStaff._RssStaffName = _Staff.StaffName

                '_SlotStaff._RssStaffLevel = _Staff.StaffRecord._QualLevel
                '_SlotStaff._RssStaffRate = _Staff.StaffRecord._SalaryHour
                '_SlotStaff._RssStaffPaed = _Staff.StaffRecord._FaPaed
                '_SlotStaff._RssStaffFaid = _Staff.StaffRecord._Fa
                '_SlotStaff._RssStaffSenco = _Staff.StaffRecord._Senco

                'm_SlotStaff.Add(_SlotStaff)






            Next

        End If

    End Sub

    Private Sub btnShiftCancel_Click(sender As Object, e As EventArgs) Handles btnShiftCancel.Click
        Me.Close()
    End Sub

    Private Sub txtShiftStart_LostFocus(sender As Object, e As EventArgs) Handles txtShiftStart.LostFocus
        txtShiftHours.Text = CalculateHours(txtShiftStart.Text, txtShiftEnd.Text)
        CalculateCost(CalculateMins(txtShiftStart.Text, txtShiftEnd.Text), CDec(txtShiftCost.Tag))
    End Sub

    Private Sub txtShiftEnd_LostFocus(sender As Object, e As EventArgs) Handles txtShiftEnd.LostFocus
        txtShiftHours.Text = CalculateHours(txtShiftStart.Text, txtShiftEnd.Text)
        CalculateCost(CalculateMins(txtShiftStart.Text, txtShiftEnd.Text), CDec(txtShiftCost.Tag))
    End Sub

    Private Sub CalculateCost(ByVal Minutes As Double, ByVal Rate As Decimal)
        If Minutes > 0 Then
            Dim _Hours As Decimal = CDec(Minutes / 60)
            txtShiftCost.Text = Format(_Hours * Rate, "0.00")
        Else
            txtShiftCost.Text = "0.00"
        End If
    End Sub

    Private Function CalculateHours(ByVal StartTime As String, ByVal FinishTime As String) As String
        Dim _Mins As Double = CalculateMins(StartTime, FinishTime)
        If _Mins = 0 Then
            Return "0.00"
        Else
            Dim _Hours As Double = _Mins / 60
            Return Format(_Hours, "0.00")
        End If
    End Function

    Private Function CalculateHours(ByVal StartTime As TimeSpan?, ByVal FinishTime As TimeSpan?) As Decimal
        Dim _Mins As Double = CalculateMins(StartTime, FinishTime)
        If _Mins = 0 Then
            Return 0
        Else
            Return CDec(_Mins / 60)
        End If
    End Function

    Private Function CalculateMins(ByVal StartTime As String, ByVal FinishTime As String) As Double

        If StartTime = "" OrElse FinishTime = "" Then Return 0

        Dim _s As TimeSpan? = HHMMToTimeSpan(StartTime)
        Dim _f As TimeSpan? = HHMMToTimeSpan(FinishTime)

        If Not _s.HasValue OrElse Not _f.HasValue Then Return 0

        Return CalculateMins(_s.Value, _f.Value)

    End Function

    Private Function CalculateMins(ByVal StartTime As TimeSpan?, ByVal FinishTime As TimeSpan?) As Double
        If Not StartTime.HasValue OrElse Not FinishTime.HasValue Then Return 0
        Dim _Diff As TimeSpan = FinishTime.Value - StartTime.Value
        Return _Diff.TotalMinutes
    End Function

    Private Function HHMMToTimeSpan(ByVal HHMM As String) As TimeSpan?

        If HHMM = "" Then Return Nothing

        If HHMM.Contains(":") Then
            Dim _HS As String = HHMM.Substring(0, HHMM.IndexOf(":"))
            Dim _MS As String = HHMM.Substring(HHMM.IndexOf(":") + 1)
            Dim _TS As New TimeSpan(CInt(_HS), CInt(_MS), 0)
            Return _TS
        Else
            Return Nothing
        End If

    End Function

    Private Function GetID(ByRef GridControl As Care.Controls.CareGrid) As Guid?

        If GridControl Is Nothing Then Return Nothing
        If GridControl.RecordCount < 0 Then Return Nothing
        If GridControl.CurrentRow Is Nothing Then Return Nothing

        Dim _ID As String = GridControl.CurrentRow.Item(0).ToString

        Return New Guid(_ID)

    End Function

    Private Sub btnView_Click(sender As Object, e As EventArgs) Handles btnView.Click
        If txtStaff.Tag.ToString = "" Then Exit Sub
        Business.Staff.DrillDown(New Guid(txtStaff.Tag.ToString))
    End Sub

    Private Sub cgNotRostered_GridDoubleClick(sender As Object, e As EventArgs) Handles cgNotRostered.GridDoubleClick
        Dim _ID As Guid? = GetID(cgNotRostered)
        If _ID.HasValue Then
            Business.Staff.DrillDown(_ID.Value)
        End If
    End Sub

    Private Sub cgShifts_RowStyle(sender As Object, e As RowStyleEventArgs) Handles cgShifts.RowStyle

        If e Is Nothing Then Exit Sub
        If e.RowHandle < 0 Then Exit Sub

        Dim _Date As Date? = ValueHandler.ConvertDate(cgShifts.GetRowCellValue(e.RowHandle, "Date"))
        If _Date.HasValue Then
            If _Date.Value = m_RotaDate Then
                e.Appearance.BackColor = System.Drawing.Color.LightCoral
            Else
                e.Appearance.Reset()
            End If
        End If

    End Sub

End Class
