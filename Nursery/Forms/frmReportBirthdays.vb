﻿Imports Care.Global
Imports System.Windows.Forms
Imports Care.Shared

Public Class frmReportBirthdays

    Private Sub btnRun_Click(sender As System.Object, e As System.EventArgs) Handles btnRun.Click

        Cursor.Current = Cursors.WaitCursor
        btnRun.Enabled = False
        btnClose.Enabled = False

        Dim _SQL As String = "select fullname, MONTH(dob) as 'Month', DAY(dob) as 'Date', dob from Children where status = 'Current'" & _
                             " union" & _
                             " select fullname, MONTH(dob) as 'Month', DAY(dob), dob from Staff where status = 'Current'" & _
                             " order by MONTH(dob), DAY(dob)"

        Dim _Rpt As New ReportHandler
        With _Rpt
            .ReportOutput = ReportHandler.OutputMode.Preview
            .ReportName = "Birthdays Report"
            .ReportFile = "birthdays.rpt"
            .ReportSQL = _SQL
            .RunReport(False)
        End With

        _Rpt = Nothing

        btnRun.Enabled = True
        btnClose.Enabled = True
        Cursor.Current = Cursors.Default

    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class