﻿

Imports Care.Global
Imports Care.Shared
Imports Care.Data
Imports System.Windows.Forms
Imports Care.Financials

Public Class frmFamily

    Implements ICTIForm

    Private m_ManualAccountNo As Boolean = False
    Private m_Family As Business.Family

    Private Sub frmFamily_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        m_ManualAccountNo = ParameterHandler.ReturnBoolean("MANUALACCOUNTNO")

        lblBalance.Hide()
        tabStatement.PageVisible = False
        tabPayments.PageVisible = False

        cbxSite.PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)
        cbxInvoiceFreq.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Invoice Frequency"))

        With cbxInvoiceDue
            .AddItem("x days from invoice date")
            .AddItem("1st of the invoiced month")
            .AddItem("1st of the following invoiced month")
            .AddItem("28th of the invoiced month")
            .AddItem("28th of the following invoiced month")
            .AddItem("Last day of the invoiced month")
            .AddItem("Last day of the following invoiced month")
            .AddItem("Last day of the previous invoiced month")
        End With

        DisableButtons()

    End Sub

    Private Sub DisableButtons()

        lblWarning.Hide()
        ToggleReportButtons(False)
        ToggleContactButtons(False)
        SetFinancialButtons(False)

        XtraTabControl1.DisableTabs()

    End Sub

    Public Sub CTIDrillDown(Info As DevExpress.XtraBars.Alerter.AlertInfo) Implements ICTIForm.DrillDown

        If Info.Tag IsNot Nothing Then

            Dim _Tag As String = Info.Tag.ToString

            If _Tag <> "" Then

                Dim _FamilyID As String = ""
                Dim _ContactID As String = ""

                If _Tag.Contains("|") Then
                    'we have family and contact
                    _FamilyID = _Tag.Substring(0, _Tag.IndexOf("|"))
                    _ContactID = _Tag.Substring(_Tag.IndexOf("|") + 1)
                Else
                    'we have just the family
                    _FamilyID = _Tag
                End If

                Me.Text = "CTI DrillDown"
                Me.Show()

                SetBindingsByID(New Guid(_FamilyID))
                ShowGeneral()

                If _ContactID <> "" Then
                    Business.Contact.ViewContact(New Guid(_ContactID))
                End If

            End If

        End If

        Session.CursorDefault()

    End Sub

    'Private Sub SetFinancials()

    '    lblWarning.Hide()

    '    Select Case Mode

    '        Case "ADD"
    '            fraFinancials.Enabled = False

    '        Case "EDIT"

    '            If m_Family._ExclInvoicing Then
    '                fraFinancials.Enabled = False
    '            Else
    '                If Session.FinancialsIntegrated Then
    '                    fraFinancials.Enabled = True
    '                    If m_Family._ExclFinancials Then
    '                        btnCustomerCreate.Enabled = False
    '                        btnCustomerOpen.Enabled = False
    '                        btnCustomerStatement.Enabled = False
    '                    Else
    '                        If m_Family._FinancialsId = "" Then
    '                            btnCustomerCreate.Enabled = True
    '                        Else
    '                            btnCustomerCreate.Enabled = False
    '                        End If
    '                        btnCustomerOpen.Enabled = False
    '                        btnCustomerStatement.Enabled = False
    '                    End If
    '                Else
    '                    fraFinancials.Enabled = False
    '                End If
    '            End If

    '            CheckWarnings()

    '        Case ""

    '            If m_Family._ExclInvoicing Then
    '                fraFinancials.Enabled = False
    '            Else
    '                If Session.FinancialsIntegrated Then
    '                    fraFinancials.Enabled = True
    '                    If m_Family._ExclFinancials Then
    '                        btnCustomerCreate.Enabled = False
    '                        btnCustomerOpen.Enabled = False
    '                        btnCustomerStatement.Enabled = False
    '                    Else
    '                        If m_Family._FinancialsId = "" Then
    '                            btnCustomerCreate.Enabled = True
    '                        Else
    '                            btnCustomerCreate.Enabled = False
    '                        End If
    '                        btnCustomerOpen.Enabled = False
    '                        btnCustomerStatement.Enabled = False
    '                    End If
    '                Else
    '                    fraFinancials.Enabled = False
    '                End If
    '            End If

    '            CheckWarnings()

    '    End Select

    'End Sub

    Private Sub CheckWarnings()
        If m_Family.IsNew Then
            lblWarning.Hide()
        Else
            Dim _Warning As String = m_Family.PrimaryContactWarning
            If _Warning = "" Then
                lblWarning.Hide()
            Else
                lblWarning.Text = _Warning
                lblWarning.Show()
            End If
        End If
    End Sub

#Region "Overrides"

    Protected Overrides Sub AfterAdd()
        GridChildren.Clear()
        GridContacts.Clear()
        GridStatement.Clear()
        GridInvoices.Clear()
        GridPayments.Clear()
        DisableButtons()
        chkExclInvoicing.Enabled = False
        fraFinancials.Enabled = False
        lblSageAccount.Text = ""
        lblWarning.Text = ""
        lblBalance.Text = ""
        txtSurname.Focus()
    End Sub

    Protected Overrides Sub AfterEdit()

        fraFinancials.Enabled = True
        SetFinancialButtons(Not chkExclFinancials.Checked)

        If m_ManualAccountNo Then
            txtAccountNo.ReadOnly = False
            txtAccountNo.TabStop = True
            txtAccountNo.BackColor = Session.ChangeColour
        End If

        txtSurname.Focus()

    End Sub

    Protected Overrides Sub AfterCommitUpdate()
        fraFinancials.Enabled = True
        ToggleContactButtons(True)
    End Sub

    Protected Overrides Sub AfterCancelChanges()
        fraFinancials.Enabled = True
        DisableButtons()
    End Sub

    Protected Overrides Sub FindRecord()

        Dim _ReturnValue As String = Business.Family.FindFamily(Me)
        If _ReturnValue <> "" Then

            MyBase.RecordID = New Guid(_ReturnValue)
            MyBase.RecordPopulated = True

            SetBindingsByID(MyBase.RecordID.Value)
            XtraTabControl1.FirstPage()

        End If

    End Sub

    Protected Overrides Function BeforeDelete() As Boolean

        Dim _SQL As String = ""
        Dim _Entered As String = InputBox("Type 'Delete' to Continue", "Delete Family").ToUpper

        If _Entered = "DELETE!" Then

            _SQL = "select count(*) from Invoices where family_id = '" + m_Family._ID.ToString + "' and invoice_status = 'Posted'"
            Dim _Invoices As Integer = CleanData.ReturnInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))

            If _Invoices > 0 Then
                CareMessage("Unable to Delete Family when invoices have been posted.", MessageBoxIcon.Exclamation, "Delete Family")
                Return False
            Else
                If CareMessage("Really? This will Delete the Family and all associated Children, Contacts etc!", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Delete Family") = DialogResult.Yes Then
                    Return True
                Else
                    Return False
                End If
            End If

        End If

        If _Entered = "DELETE" Then

            _SQL = "select count(*) from Children where family_id = '" + m_Family._ID.ToString + "'"
            Dim _Children As Integer = CleanData.ReturnInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))

            _SQL = "select count(*) from Contacts where family_id = '" + m_Family._ID.ToString + "'"
            Dim _Contacts As Integer = CleanData.ReturnInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))

            _SQL = "select count(*) from Invoices where family_id = '" + m_Family._ID.ToString + "' and invoice_status = 'Posted'"
            Dim _Invoices As Integer = CleanData.ReturnInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))

            If _Invoices > 0 Then
                CareMessage("Unable to Delete Family when invoices have been posted.", MessageBoxIcon.Exclamation, "Delete Family")
                Return False
            End If

            If _Children > 0 Then
                CareMessage("Unable to Delete Family when Children are linked.", MessageBoxIcon.Exclamation, "Delete Family")
                Return False
            End If

            If _Contacts > 0 Then
                CareMessage("Unable to Delete Family when Contacts are linked.", MessageBoxIcon.Exclamation, "Delete Family")
                Return False
            End If

            Return True

        Else
            Return False
        End If

    End Function

    Protected Overrides Sub CommitDelete()

        m_Family.Delete()

        ShowContacts()
        ShowChildren()

        ToggleContactButtons(False)
        ToggleReportButtons(False)
        lblWarning.Hide()

    End Sub

    Protected Overrides Sub CommitUpdate()

        Dim _Family As Business.Family = CType(bs.Item(bs.Position), Business.Family)

        If cbxSite.SelectedValue.ToString <> "" Then
            _Family._SiteId = New Guid(cbxSite.SelectedValue.ToString)
            _Family._SiteName = cbxSite.Text
        End If

        _Family._ExclInvoicing = chkExclInvoicing.Checked
        _Family._ExclFinancials = chkExclFinancials.Checked
        _Family._eInvoicing = chkEInvoicing.Checked
        _Family._Archived = chkArchived.Checked

        If _Family._Coordinates <> "" Then
            If _Family._Address <> txtAddressFull.Text Then
                'address has changed, reset the coordinates
                _Family._Coordinates = ""
            End If
        End If

        _Family._Address = txtAddressFull.Address
        _Family._Postcode = txtAddressFull.PostCode

        If m_ManualAccountNo Then
            _Family._AccountNo = txtAccountNo.Text
            _Family._FinancialsId = txtAccountNo.Text
        End If

        Business.Family.SaveRecord(_Family)

        If Mode = "ADD" Then
            MyBase.RecordPopulated = True
            m_Family = Business.Family.RetreiveByID(_Family._ID.Value)
            bs.DataSource = m_Family
            ShowGeneral()
        End If

        _Family = Nothing

        'used to ensure financials buttons are changed correctly
        Mode = ""

    End Sub

    Protected Overrides Sub SetBindings()

        m_Family = New Business.Family
        bs.DataSource = m_Family

        txtAccountNo.DataBindings.Add("Text", bs, "_AccountNo")
        txtSurname.DataBindings.Add("Text", bs, "_Surname")
        txtLetterName.DataBindings.Add("Text", bs, "_LetterName")

        cbxInvoiceFreq.DataBindings.Add("Text", bs, "_InvoiceFreq")
        cbxInvoiceDue.DataBindings.Add("Text", bs, "_InvoiceDue")

        txtOpeningBalance.DataBindings.Add("Text", bs, "_OpeningBalance")

    End Sub

    Protected Overrides Sub SetBindingsByID(ByVal ID As System.Guid)

        MyBase.RecordPopulated = True
        m_Family = Business.Family.RetreiveByID(ID)
        bs.DataSource = m_Family

        ShowGeneral()

    End Sub

#End Region

    Private Sub ShowGeneral()

        Me.Text = "Family [" + m_Family._AccountNo + "]  " + m_Family._LetterName + ", " + m_Family._InvoiceFreq

        txtAddressFull.Text = m_Family._Address
        txtAddressFull.PostCode = m_Family._Postcode

        cbxSite.SelectedValue = m_Family._SiteId

        chkExclInvoicing.Checked = m_Family._ExclInvoicing
        chkExclFinancials.Checked = m_Family._ExclFinancials
        chkEInvoicing.Checked = m_Family._eInvoicing
        chkArchived.Checked = m_Family._Archived

        If m_Family._FinancialsId <> "" Then
            btnCustomerCreate.Text = "Unlink"
        Else
            btnCustomerCreate.Text = "Create / Link"
        End If

        lblBalance.Show()
        lblBalance.Text = ValueHandler.MoneyAsText(m_Family._Balance)
        lblBalance.ForeColor = ValueHandler.ReturnValueColour(m_Family._Balance)

        ShowContacts()
        ShowChildren()

        ToggleReportButtons(True)
        SetFinancialButtons(True)
        XtraTabControl1.EnableTabs()

        Session.CursorDefault()

    End Sub

    Private Sub ShowStatement()

        Dim _SQL As String = "select statement_date, bfwd, charges, payments, balance" & _
                             " from FamilyStatement" & _
                             " where family_id = '" & m_Family._ID.ToString & "'" & _
                             " order by statement_date desc"

        GridStatement.Populate(Session.ConnectionString, _SQL)

        GridStatement.Columns("statement_date").Caption = "Date"
        GridStatement.Columns("bfwd").Caption = "BFwd"
        GridStatement.Columns("charges").Caption = "Charges"
        GridStatement.Columns("payments").Caption = "Payments"
        GridStatement.Columns("balance").Caption = "Balance"

    End Sub

    Private Sub ShowInvoices()

        Dim _SQL As String = ""

        _SQL += "select i.id, b.batch_no, i.invoice_no, i.invoice_status, i.invoice_date, i.invoice_period, i.child_name, i.invoice_sub, i.invoice_discount, i.invoice_total"
        _SQL += " from Invoices i"
        _SQL += " left join InvoiceBatch b on i.batch_id = b.ID"
        _SQL += " where i.family_id = '" & m_Family._ID.ToString & "'"
        _SQL += " and b.batch_status <> 'Abandoned'"
        _SQL += " and b.batch_period not in ('Annual Invoice', 'Annual Segments', 'Forecasting')"

        If radPosted.Checked Then
            _SQL += " and i.invoice_status = 'Posted'"
        Else
            _SQL += " and i.invoice_status <> 'Posted'"
        End If

        _SQL += " order by i.invoice_no desc"

        GridInvoices.HideFirstColumn = True
        GridInvoices.Populate(Session.ConnectionString, _SQL)

        GridInvoices.Columns("batch_no").Caption = "Batch No"
        GridInvoices.Columns("invoice_no").Caption = "Invoice No"

        If radPosted.Checked Then
            GridInvoices.Columns("invoice_status").Visible = False
        Else
            GridInvoices.Columns("invoice_status").Caption = "Status"
        End If

        GridInvoices.Columns("invoice_date").Caption = "Invoice Date"
        GridInvoices.Columns("invoice_period").Caption = "Period"
        GridInvoices.Columns("child_name").Caption = "Child"
        GridInvoices.Columns("invoice_sub").Caption = "Sub Total"
        GridInvoices.Columns("invoice_discount").Caption = "Discount"
        GridInvoices.Columns("invoice_total").Caption = "Invoice Total"

        If GridInvoices.RecordCount > 0 Then
            btnInvoiceEdit.Enabled = True
        Else
            btnInvoiceEdit.Enabled = False
        End If

    End Sub

    Private Sub ShowPayments()

        Dim _SQL As String = "select id, pay_date, method_name, ref_1, ref_2, ref_3, pay_amount, stamp" & _
                             " from Payments" & _
                             " where family_id = '" & m_Family._ID.ToString & "'" & _
                             " order by pay_date desc"

        GridPayments.HideFirstColumn = True
        GridPayments.Populate(Session.ConnectionString, _SQL)

        GridPayments.Columns("pay_date").Caption = "Payment Date"
        GridPayments.Columns("method_name").Caption = "Payment Method"
        GridPayments.Columns("ref_1").Caption = "Ref 1"
        GridPayments.Columns("ref_2").Caption = "Ref 2"
        GridPayments.Columns("ref_3").Caption = "Ref 3"
        GridPayments.Columns("pay_amount").Caption = "Sub Total"
        GridPayments.Columns("stamp").Caption = "Invoice Total"

    End Sub

    Private Sub ShowContacts()

        Dim _SQL As String = ""
        _SQL = "select id, fullname as 'Name', relationship as 'Relationship'," & _
               " tel_home as 'Home', tel_mobile as 'Mobile'," & _
               " CAST(CASE WHEN (len(rtrim(email)) > 0) THEN 1 ELSE 0 END AS BIT) as 'Email'," & _
               " sms as 'SMS?', primary_cont as 'Primary?', parent as 'Parent', emer_cont as 'Emergency?', collect as 'Collect?', report as 'Reports?', invoice_email as 'Invoices?'" & _
               " from contacts" & _
               " where family_id = '" & m_Family._ID.ToString & "'" & _
               " order by primary_cont desc, parent desc, forename"

        GridContacts.HideFirstColumn = True
        GridContacts.Populate(Session.ConnectionString, _SQL)

        ToggleContactButtons(True)
        CheckWarnings()

    End Sub

    Private Sub ShowChildren()

        Dim _SQL As String = ""
        _SQL = "select id, fullname as 'Name', group_name as 'Group'" & _
               " from children" & _
               " where family_id = '" & m_Family._ID.ToString & "'" & _
               " order by dob, forename"

        GridChildren.HideFirstColumn = True
        GridChildren.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        If MyBase.RecordPopulated Then
            If m_Family._ID.Value.ToString <> "" Then
                Dim _ID As New Guid(m_Family._ID.Value.ToString)
                Business.Contact.AddContact(_ID)
                ShowContacts()
            End If
        End If

    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewContact()
    End Sub

    Private Sub ViewContact()

        If GridContacts.RecordCount < 1 Then Exit Sub

        If GridContacts.CurrentRow(0).ToString <> "" Then
            Dim _ID As New Guid(GridContacts.CurrentRow(0).ToString)
            Business.Contact.ViewContact(_ID)
            ShowContacts()
        End If

    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If CareMessage("Are you sure you want to delete this Contact?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Delete Contact") = DialogResult.Yes Then
            If GridContacts.RecordCount > 0 Then
                Dim _ID As New Guid(GridContacts.CurrentRow(0).ToString)
                Business.Contact.DeleteContact(_ID)
                ShowContacts()
            End If
        End If
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub cmdCopy_Click(sender As System.Object, e As System.EventArgs)
        Clipboard.Clear()
        Clipboard.SetText(txtAddressFull.Text)
    End Sub

    Private Sub XtraTabControl1_SelectedPageChanged(sender As Object, e As DevExpress.XtraTab.TabPageChangedEventArgs) Handles XtraTabControl1.SelectedPageChanging

        Select Case e.Page.Text

            Case "Family"
                ShowGeneral()
                Me.ToolbarMode = ToolbarEnum.Standard

            Case "Invoices"
                ShowInvoices()
                Me.ToolbarMode = ToolbarEnum.FindOnly

            Case "Activity"
                ShowActivity()
                Me.ToolbarMode = ToolbarEnum.FindOnly

            Case "Statement"
                ShowStatement()
                Me.ToolbarMode = ToolbarEnum.FindOnly

            Case "Payment History"
                ShowPayments()
                Me.ToolbarMode = ToolbarEnum.FindOnly

        End Select

    End Sub

    Private Sub ShowActivity()

        'Public Property CRMLinkID As Guid?

        '<Browsable(False)>
        'Public Property CRMContactID As Guid?

        '<Browsable(False)>
        'Public Property CRMContactName As String

        '<Browsable(False)>
        'Public Property CRMContactMobile As String

        '<Browsable(False)>
        'Public Property CRMContactEmail As String

        With crmFamily
            .CRMLinkID = m_Family._ID
            .Populate()
        End With

    End Sub


    Private Sub GridContacts_GridDoubleClick(sender As Object, e As System.EventArgs) Handles GridContacts.GridDoubleClick
        ViewContact()
    End Sub

    Private Sub GridChildren_GridDoubleClick(sender As Object, e As System.EventArgs) Handles GridChildren.GridDoubleClick

        If GridChildren.RecordCount < 1 Then Exit Sub

        If GridChildren.CurrentRow(0).ToString <> "" Then
            Session.CursorWaiting()
            Dim _ID As New Guid(GridChildren.CurrentRow(0).ToString)
            Business.Child.DrillDown(_ID)
            ShowContacts()
        End If

    End Sub

    Private Sub GridInvoices_GridDoubleClick(sender As Object, e As System.EventArgs) Handles GridInvoices.GridDoubleClick
        AmendInvoice()
    End Sub

    Private Sub btnInvoiceEdit_Click(sender As System.Object, e As System.EventArgs) Handles btnInvoiceEdit.Click
        AmendInvoice()
    End Sub

    Private Sub AmendInvoice()

        If GridInvoices.RecordCount < 1 Then Exit Sub
        If GridInvoices.CurrentRow(0).ToString <> "" Then
            Dim _id As New Guid(GridInvoices.CurrentRow(0).ToString)
            Invoicing.ViewInvoice(_id)
        End If

    End Sub

    Private Sub btnPrintReport_Click(sender As System.Object, e As System.EventArgs) Handles btnPrintReport.Click
        ToggleReportButtons(False)
        Dim _DailyReport As New Reports.DailyReport(Reports.DailyReport.EnumRunMode.ForcedPrint, Today.Date, m_Family._SiteId.Value, m_Family._ID.ToString, "", True)
        _DailyReport.Run()
        ToggleReportButtons(True)
    End Sub

    Private Sub btnEmailReport_Click(sender As System.Object, e As System.EventArgs) Handles btnEmailReport.Click
        ToggleReportButtons(False)
        Dim _DailyReport As New Reports.DailyReport(Reports.DailyReport.EnumRunMode.ForcedEmail, Today.Date, m_Family._SiteId.Value, m_Family._ID.ToString, "", True)
        _DailyReport.Run()
        ToggleReportButtons(True)
    End Sub

    Private Sub ToggleReportButtons(Enabled As Boolean)
        btnPrintReport.Enabled = Enabled
        btnEmailReport.Enabled = Enabled
        btnMerge.Enabled = Enabled
    End Sub

    Private Sub ToggleContactButtons(Enabled As Boolean)

        If Enabled = False Then
            btnAdd.Enabled = False
            btnEdit.Enabled = False
            btnDelete.Enabled = False
        Else
            If GridContacts IsNot Nothing Then
                If GridContacts.RecordCount > 0 Then
                    btnAdd.Enabled = True
                    btnEdit.Enabled = True
                    btnDelete.Enabled = True
                Else
                    btnAdd.Enabled = True
                    btnEdit.Enabled = False
                    btnDelete.Enabled = False
                End If
            End If
        End If

    End Sub

    Private Sub btnCreateCustomer_Click(sender As Object, e As EventArgs) Handles btnCustomerCreate.Click
        If m_Family._FinancialsId <> "" Then
            UnLink()
        Else
            Link()
        End If
    End Sub

    Private Sub UnLink()
        If CareMessage("Are you sure you want to UnLink this Customer from your Financial System?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Unlink") = DialogResult.Yes Then
            m_Family._FinancialsId = Nothing
            m_Family.Store()
            ToolAccept()
            ShowGeneral()
        End If
    End Sub

    Private Sub Link()

        'default to creating a link to an existing customer record in the accounting system
        Dim _LinkToExisting As Boolean = True
        Dim _FinancialSystem As String = FinanceShared.ReturnFinancialSystem(m_Family._SiteId)

        Select Case _FinancialSystem

            Case "XERO"
                If CareMessage("Does your customer already exist in your Financial System?", MessageBoxIcon.Question, MessageBoxButtons.YesNo, "Customer Exists?") = DialogResult.No Then
                    _LinkToExisting = False
                End If

        End Select

        If _LinkToExisting Then

            Dim _Ref As String = InputBox("Please enter the Customer Reference from Financial System", "Enter External Customer Reference")
            If _Ref <> "" Then

                Dim _Valid As Boolean = False
                Select Case _FinancialSystem

                    Case "XERO"
                        _Valid = ValidateGUID(_Ref)

                    Case Else
                        _Valid = True

                End Select

                If _Valid Then
                    m_Family._FinancialsId = _Ref
                    ToolAccept()
                    ShowGeneral()
                Else
                    CareMessage("The Customer Reference appears to be Invalid. Please check the reference and try again.", MessageBoxIcon.Exclamation, "Invalid Reference")
                End If

            End If

        Else

            If CareMessage("Do you want to create a customer record now?", MessageBoxIcon.Question, MessageBoxButtons.YesNo, "Confirm Create Customer") = DialogResult.Yes Then

                btnCustomerCreate.Enabled = False
                Session.CursorWaiting()

                Dim _OK As Boolean = False
                Dim _PC As Business.Contact = Business.Contact.RetrievePrimaryContact(m_Family._ID.ToString)
                If _PC IsNot Nothing Then

                    Dim _E As New Entity
                    With _E

                        .ContactName = m_Family._LetterName

                        'split the address into parts
                        If m_Family._Address <> "" Then

                            Dim _i As Integer = 1
                            Dim _Lines As String() = m_Family._Address.Split(CChar(vbCrLf))
                            For Each _Line As String In _Lines
                                If _Line <> "" Then
                                    If _i = 1 Then .Address1 = _Line
                                    If _i = 2 Then .Address2 = _Line
                                    If _i = 3 Then .Address3 = _Line
                                    If _i = 4 Then .Address4 = _Line
                                    If _i = 5 Then .Address5 = _Line
                                    _i += 1
                                End If
                            Next

                        End If

                        .Forename = _PC._Forename
                        .Surname = _PC._Surname

                        .Phone = _PC._TelHome
                        .Mobile = _PC._TelMobile
                        .Email = _PC._Email

                    End With

                    Dim _Eng As New Care.Financials.Engine(FinanceShared.ReturnFinancialsIntegration(m_Family._SiteId))
                    Dim _Response As Care.Financials.Response = _Eng.CreateEntity(_E)
                    If _Response.HasErrors Then
                        CareMessage("Create Customer Record Failed - " + _Response.ErrorText, MessageBoxIcon.Information, "Create Customer")
                    Else
                        _OK = True
                    End If

                    _PC = Nothing

                End If

                btnCustomerCreate.Enabled = True
                Session.CursorDefault()

                If _OK Then
                    CareMessage("Customer Record Created Successfully.", MessageBoxIcon.Information, "Create Customer")
                    ToolAccept()
                    ShowGeneral()
                End If

            End If

        End If

    End Sub

    Private Sub btnOpenCustomer_Click(sender As Object, e As EventArgs) Handles btnCustomerOpen.Click
        If m_Family._FinancialsId.ToString = "" Then Exit Sub
        Dim _E As New Engine(FinanceShared.ReturnFinancialsIntegration(m_Family._SiteId))
        _E.OpenEntity(m_Family._FinancialsId.ToString)
    End Sub

    Private Sub btnCustomerStatement_Click(sender As Object, e As EventArgs) Handles btnCustomerStatement.Click
        If m_Family._FinancialsId.ToString = "" Then Exit Sub
        Dim _E As New Engine(FinanceShared.ReturnFinancialsIntegration(m_Family._SiteId))
        _E.OpenStatement(m_Family._FinancialsId.ToString)
    End Sub

    Private Sub chkExclInvoicing_EditValueChanged(sender As Object, e As EventArgs) Handles chkExclInvoicing.EditValueChanged
        If chkExclInvoicing.Checked Then
            chkExclFinancials.Checked = True
            chkEInvoicing.Checked = False
        End If
    End Sub

    Private Sub chkExclFinancials_EditValueChanged(sender As Object, e As EventArgs) Handles chkExclFinancials.EditValueChanged
        SetFinancialButtons(Not chkExclFinancials.Checked)
    End Sub

    Private Sub chkExclInvoicing_CheckedChanged(sender As Object, e As EventArgs) Handles chkExclInvoicing.CheckedChanged
        If chkExclInvoicing.Checked Then
            chkExclFinancials.Checked = True
        End If
    End Sub

    Private Sub SetFinancialButtons(ByVal Enabled As Boolean)

        lblSageAccount.Visible = False

        btnCustomerCreate.Enabled = False
        btnCustomerOpen.Enabled = False
        btnCustomerStatement.Enabled = False

        If Enabled Then

            If Session.FinancialsIntegrated Then

                Dim _ViewAccountNumber As Boolean = False
                Dim _OpenAccount As Boolean = False
                Dim _OpenStatement As Boolean = False

                'buttons are set depending on integration type and current status
                Select Case Session.FinancialSystem.ToUpper

                    Case "XERO"
                        _OpenAccount = True
                        _OpenStatement = True

                    Case "SAGE"
                        _ViewAccountNumber = True
                        _OpenAccount = True

                    Case "CLEARBOOKS"
                        _OpenAccount = True
                        _OpenStatement = True

                    Case Else
                        _ViewAccountNumber = True

                End Select

                btnCustomerCreate.Enabled = True
                btnCustomerOpen.Enabled = _OpenAccount
                btnCustomerStatement.Enabled = _OpenStatement

                If _ViewAccountNumber Then
                    lblSageAccount.Visible = True
                    lblSageAccount.Text = m_Family._FinancialsId
                Else
                    lblSageAccount.Visible = False
                    lblSageAccount.Text = ""
                End If

            End If

        End If

    End Sub

    Private Function ValidateGUID(ByVal StringIn As String) As Boolean
        Dim _ID As Guid = Nothing
        If Guid.TryParse(StringIn, _ID) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub btnMerge_Click(sender As Object, e As EventArgs) Handles btnMerge.Click

        Dim _frm As New frmFamilyMerge(m_Family._ID.Value)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            SetBindingsByID(New Guid(_frm.Tag.ToString))
            ShowGeneral()
        End If

    End Sub

    Private Sub btnDeleteInvoice_Click(sender As Object, e As EventArgs) Handles btnDeleteInvoice.Click

        If GridInvoices Is Nothing Then Exit Sub
        If GridInvoices.RecordCount < 1 Then Exit Sub
        If GridInvoices.CurrentRow(0).ToString = "" Then Exit Sub

        btnDeleteInvoice.Enabled = False

        If CareMessage("Are you sure you want to Delete this Invoice?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Delete Invoice") = DialogResult.Yes Then

            If ParameterHandler.ManagerAuthorised Then

                Dim _SQL As String = ""
                Dim _ID As String = GridInvoices.CurrentRow(0).ToString

                _SQL = "delete from Invoices where ID = '" + _ID + "'"
                DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                _SQL = "delete from InvoiceLines where invoice_id = '" + _ID + "'"
                DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                ShowInvoices()

            Else
                'auth failed
            End If
        Else
            'selected no
        End If

        btnDeleteInvoice.Enabled = True

    End Sub

    Private Sub lblBalance_DoubleClick(sender As Object, e As EventArgs) Handles lblBalance.DoubleClick

        If CareMessage("Are you sure you want to refresh all Balances?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Refresh Balances") = DialogResult.Yes Then

            Session.WaitFormShow()

            Dim _F As New Business.FinancialUtilities
            _F.UpdateBalances()

            Session.WaitFormClose()

        End If

    End Sub

    Private Sub lblBalance_Click(sender As Object, e As EventArgs) Handles lblBalance.Click

    End Sub

    Private Sub radPosted_CheckedChanged(sender As Object, e As EventArgs) Handles radPosted.CheckedChanged
        ShowInvoices()
    End Sub

    Private Sub radNonPosted_CheckedChanged(sender As Object, e As EventArgs) Handles radNonPosted.CheckedChanged
        ShowInvoices()
    End Sub
End Class
