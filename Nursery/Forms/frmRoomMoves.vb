﻿

Imports Care.Global
Imports Care.Data
Imports Care.Shared

Public Class frmRoomMoves

    Private Sub frmRoomMoves_Load(sender As Object, e As EventArgs) Handles Me.Load

        cbxSite.PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)
        cbxSite.SelectedIndex = 0

        cbxRoom.Enabled = False

        cbxKeyworker.PopulateWithSQL(Session.ConnectionString, "select ID, fullname from Staff where status = 'C' and keyworker = 1 order by fullname")
        cbxKeyworker.Enabled = False

        cbxFilter.AddItem("Children, sorted by Forename")
        cbxFilter.AddItem("Children, sorted by Surname")
        cbxFilter.AddItem("Children, sorted Oldest > Youngest")
        cbxFilter.AddItem("Children, sorted Youngest > Oldest")
        cbxFilter.AddItem("Children, sorted by Room, Forename")
        cbxFilter.AddItem("Children, sorted by Room, Surname")
        cbxFilter.AddItem("Children, sorted by Room, Oldest > Youngest")
        cbxFilter.AddItem("Children, sorted by Room, Youngest > Oldest")

    End Sub

    Private Sub cbxFilter_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxFilter.SelectedIndexChanged
        RefreshGrid()
    End Sub

    Private Sub RefreshGrid()

        If cbxSite.Text = "" Then Exit Sub

        Dim _SQL As String = ""
        Dim _Order As String = ""

        _SQL += "select c.ID, forename as 'Forename', surname as 'Surname',"
        _SQL += " dob as 'DOB', datename(month, dob) as 'MOB', datepart(yyyy, dob) as 'YOB', "
        _SQL += " group_name as 'Room', keyworker_name as 'Key Worker' from Children c"
        _SQL += " left join Groups g on g.ID = c.group_id"
        _SQL += " where status <> 'Left'"
        _SQL += " and site_id = '" + cbxSite.SelectedValue.ToString + "'"
        _SQL += " and fullname <> ''"

        Select Case cbxFilter.Text

            Case "Children, sorted by Forename"
                _Order = " order by forename"

            Case "Children, sorted by Surname"
                _Order = " order by surname"

            Case "Children, sorted Oldest > Youngest"
                _Order = " order by dob"

            Case "Children, sorted Youngest > Oldest"
                _Order = " order by dob desc"

            Case "Children, sorted by Room, Forename"
                _Order = " order by g.sequence, forename"

            Case "Children, sorted by Room, Surname"
                _Order = " order by g.sequence, surname"

            Case "Children, sorted by Room, Oldest > Youngest"
                _Order = " order by g.sequence, dob"

            Case "Children, sorted by Room, Youngest > Oldest"
                _Order = " order by g.sequence, dob desc"

        End Select

        _SQL += _Order
        cgRecords.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub btnChange_Click(sender As Object, e As EventArgs) Handles btnChange.Click

        Dim _SelectedRows As Integer() = cgRecords.UnderlyingGridView.GetSelectedRows()
        If _SelectedRows.Length > 0 AndAlso _SelectedRows.Length >= 1 Then

            If (chkRoom.Checked And cbxRoom.SelectedIndex >= 0) OrElse (chkKeyWorker.Checked And cbxKeyworker.SelectedIndex >= 0) Then

                If CareMessage("Are you sure you want to change the " + _SelectedRows.Length.ToString + " selected record(s)?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Change Records") = DialogResult.Yes Then

                    btnChange.Enabled = False

                    Session.SetProgressMessage("Updating selected records...", _SelectedRows.Count)
                    Session.CursorWaiting()

                    For Each _RowIndex As Integer In cgRecords.UnderlyingGridView.GetSelectedRows()

                        Dim _DR As DataRow = cgRecords.UnderlyingGridView.GetDataRow(_RowIndex)
                        Dim _ID As String = _DR.Item("ID").ToString

                        Dim _Child As Business.Child = Business.Child.RetreiveByID(New Guid(_ID))
                        If _Child IsNot Nothing Then

                            If chkRoom.Checked Then
                                _Child._GroupId = New Guid(cbxRoom.SelectedValue.ToString)
                                _Child._GroupName = cbxRoom.Text
                            End If

                            If chkKeyWorker.Checked Then
                                _Child._KeyworkerId = New Guid(cbxKeyworker.SelectedValue.ToString)
                                _Child._KeyworkerName = cbxKeyworker.Text
                            End If

                            _Child.Store()

                        End If

                        Session.StepProgressBar()

                    Next

                    Session.SetProgressMessage("Records Updated Successfully.", 3)

                    chkRoom.Checked = False
                    chkKeyWorker.Checked = False

                    RefreshGrid()

                    btnChange.Enabled = True
                    Session.CursorDefault()

                End If

            Else
                'nothing ticked (to change)
                CareMessage("Please choose what changes you wish to make to the selected records.", MessageBoxIcon.Exclamation, "Change Records")
            End If

        Else
            'nothing selected
            CareMessage("Please select which records you wish to change.", MessageBoxIcon.Exclamation, "Change Records")

        End If

    End Sub

    Private Sub chkRoom_CheckedChanged(sender As Object, e As EventArgs) Handles chkRoom.CheckedChanged
        cbxRoom.Enabled = chkRoom.Checked
        If cbxRoom.Enabled = False Then cbxRoom.SelectedIndex = -1
    End Sub

    Private Sub chkKeyWorker_CheckedChanged(sender As Object, e As EventArgs) Handles chkKeyWorker.CheckedChanged
        cbxKeyworker.Enabled = chkKeyWorker.Checked
        If cbxKeyworker.Enabled = False Then cbxKeyworker.SelectedIndex = -1
    End Sub

    Private Sub cgRecords_GridDoubleClick(sender As Object, e As EventArgs)
        If cgRecords.RecordCount > 0 Then
            If cgRecords.CurrentRow IsNot Nothing Then
                Dim _ID As String = cgRecords.CurrentRow.Item("ID").ToString
                Business.Child.DrillDown(New Guid(_ID))
            End If
        End If
    End Sub

    Private Sub cbxSite_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSite.SelectedIndexChanged
        If cbxSite.Text = "" Then
            cbxRoom.Clear()
            cbxRoom.SelectedIndex = -1
        Else
            cbxRoom.PopulateWithSQL(Session.ConnectionString, Business.RoomRatios.RetreiveSiteRoomSQL(cbxSite.SelectedValue.ToString))
            RefreshGrid()
        End If
    End Sub
End Class
