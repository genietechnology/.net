﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTariffMatrix
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.grdMatrix = New Care.Controls.CareGrid()
        Me.btnAmendOK = New Care.Controls.CareButton(Me.components)
        Me.btnAmendCancel = New Care.Controls.CareButton(Me.components)
        Me.txtDescription = New Care.Controls.CareTextBox(Me.components)
        Me.gbxMatrix = New Care.Controls.CareFrame()
        Me.lblValue = New Care.Controls.CareLabel(Me.components)
        Me.txtValue = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.btnUpdate = New Care.Controls.CareButton(Me.components)
        CType(Me.txtDescription.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxMatrix, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxMatrix.SuspendLayout()
        CType(Me.txtValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'grdMatrix
        '
        Me.grdMatrix.AllowBuildColumns = True
        Me.grdMatrix.AllowEdit = False
        Me.grdMatrix.AllowHorizontalScroll = False
        Me.grdMatrix.AllowMultiSelect = False
        Me.grdMatrix.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.grdMatrix.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdMatrix.Appearance.Options.UseFont = True
        Me.grdMatrix.AutoSizeByData = True
        Me.grdMatrix.DisableAutoSize = False
        Me.grdMatrix.DisableDataFormatting = False
        Me.grdMatrix.FocusedRowHandle = -2147483648
        Me.grdMatrix.HideFirstColumn = False
        Me.grdMatrix.Location = New System.Drawing.Point(12, 12)
        Me.grdMatrix.Name = "grdMatrix"
        Me.grdMatrix.PreviewColumn = ""
        Me.grdMatrix.QueryID = Nothing
        Me.grdMatrix.RowAutoHeight = False
        Me.grdMatrix.SearchAsYouType = True
        Me.grdMatrix.ShowAutoFilterRow = False
        Me.grdMatrix.ShowFindPanel = False
        Me.grdMatrix.ShowGroupByBox = False
        Me.grdMatrix.ShowLoadingPanel = False
        Me.grdMatrix.ShowNavigator = False
        Me.grdMatrix.Size = New System.Drawing.Size(752, 410)
        Me.grdMatrix.TabIndex = 0
        Me.grdMatrix.TabStop = False
        '
        'btnAmendOK
        '
        Me.btnAmendOK.Location = New System.Drawing.Point(179, 105)
        Me.btnAmendOK.Name = "btnAmendOK"
        Me.btnAmendOK.Size = New System.Drawing.Size(75, 23)
        Me.btnAmendOK.TabIndex = 4
        Me.btnAmendOK.Text = "OK"
        '
        'btnAmendCancel
        '
        Me.btnAmendCancel.Location = New System.Drawing.Point(260, 105)
        Me.btnAmendCancel.Name = "btnAmendCancel"
        Me.btnAmendCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnAmendCancel.TabIndex = 5
        Me.btnAmendCancel.Text = "Cancel"
        '
        'txtDescription
        '
        Me.txtDescription.CharacterCasing = CharacterCasing.Normal
        Me.txtDescription.EnterMoveNextControl = True
        Me.txtDescription.Location = New System.Drawing.Point(113, 29)
        Me.txtDescription.MaxLength = 100
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.NumericAllowNegatives = False
        Me.txtDescription.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtDescription.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDescription.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtDescription.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtDescription.Properties.Appearance.Options.UseFont = True
        Me.txtDescription.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDescription.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtDescription.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDescription.Properties.MaxLength = 100
        Me.txtDescription.Size = New System.Drawing.Size(222, 22)
        Me.txtDescription.TabIndex = 1
        Me.txtDescription.TextAlign = HorizontalAlignment.Left
        Me.txtDescription.ToolTipText = ""
        '
        'gbxMatrix
        '
        Me.gbxMatrix.Anchor = AnchorStyles.None
        Me.gbxMatrix.Controls.Add(Me.lblValue)
        Me.gbxMatrix.Controls.Add(Me.txtValue)
        Me.gbxMatrix.Controls.Add(Me.btnAmendCancel)
        Me.gbxMatrix.Controls.Add(Me.btnAmendOK)
        Me.gbxMatrix.Controls.Add(Me.CareLabel1)
        Me.gbxMatrix.Controls.Add(Me.txtDescription)
        Me.gbxMatrix.Location = New System.Drawing.Point(215, 162)
        Me.gbxMatrix.Name = "gbxMatrix"
        Me.gbxMatrix.Size = New System.Drawing.Size(347, 136)
        Me.gbxMatrix.TabIndex = 1
        Me.gbxMatrix.Text = "Amend Matrix Value"
        '
        'lblValue
        '
        Me.lblValue.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblValue.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblValue.Location = New System.Drawing.Point(12, 60)
        Me.lblValue.Name = "lblValue"
        Me.lblValue.Size = New System.Drawing.Size(29, 15)
        Me.lblValue.TabIndex = 2
        Me.lblValue.Text = "Value"
        Me.lblValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtValue
        '
        Me.txtValue.CharacterCasing = CharacterCasing.Normal
        Me.txtValue.EnterMoveNextControl = True
        Me.txtValue.Location = New System.Drawing.Point(113, 57)
        Me.txtValue.MaxLength = 5
        Me.txtValue.Name = "txtValue"
        Me.txtValue.NumericAllowNegatives = False
        Me.txtValue.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtValue.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtValue.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtValue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtValue.Properties.Appearance.Options.UseFont = True
        Me.txtValue.Properties.Appearance.Options.UseTextOptions = True
        Me.txtValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtValue.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtValue.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtValue.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtValue.Properties.MaxLength = 5
        Me.txtValue.Size = New System.Drawing.Size(72, 22)
        Me.txtValue.TabIndex = 3
        Me.txtValue.Tag = ""
        Me.txtValue.TextAlign = HorizontalAlignment.Left
        Me.txtValue.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(12, 32)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(60, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Description"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnClose.Location = New System.Drawing.Point(689, 428)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 5
        Me.btnClose.Text = "Close"
        '
        'btnUpdate
        '
        Me.btnUpdate.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnUpdate.Location = New System.Drawing.Point(608, 428)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(75, 23)
        Me.btnUpdate.TabIndex = 4
        Me.btnUpdate.Text = "Update"
        '
        'frmTariffMatrix
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(776, 459)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.gbxMatrix)
        Me.Controls.Add(Me.grdMatrix)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTariffMatrix"
        Me.StartPosition = FormStartPosition.CenterParent
        Me.Text = ""
        CType(Me.txtDescription.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxMatrix, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxMatrix.ResumeLayout(False)
        Me.gbxMatrix.PerformLayout()
        CType(Me.txtValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grdMatrix As Care.Controls.CareGrid
    Friend WithEvents btnAmendOK As Care.Controls.CareButton
    Friend WithEvents btnAmendCancel As Care.Controls.CareButton
    Friend WithEvents gbxMatrix As DevExpress.XtraEditors.GroupControl
    Friend WithEvents lblValue As Care.Controls.CareLabel
    Friend WithEvents txtValue As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents btnUpdate As Care.Controls.CareButton
    Friend WithEvents txtDescription As Care.Controls.CareTextBox

End Class
