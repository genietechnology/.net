﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportBookings
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportBookings))
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.Wiz = New DevExpress.XtraWizard.WizardControl()
        Me.wpWelcome = New DevExpress.XtraWizard.WelcomeWizardPage()
        Me.wpComplete = New DevExpress.XtraWizard.CompletionWizardPage()
        Me.wpSpreadsheet = New DevExpress.XtraWizard.WizardPage()
        Me.sc = New DevExpress.XtraSpreadsheet.SpreadsheetControl()
        Me.wpPreview = New DevExpress.XtraWizard.WizardPage()
        Me.cgRecords = New Care.Controls.CareGrid()
        Me.wpProgress = New DevExpress.XtraWizard.WizardPage()
        Me.memProgress = New DevExpress.XtraEditors.MemoEdit()
        Me.ofd = New OpenFileDialog()
        CType(Me.Wiz, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Wiz.SuspendLayout()
        Me.wpSpreadsheet.SuspendLayout()
        Me.wpPreview.SuspendLayout()
        Me.wpProgress.SuspendLayout()
        CType(Me.memProgress.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'Wiz
        '
        Me.Wiz.Appearance.ExteriorPage.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Wiz.Appearance.ExteriorPage.Options.UseFont = True
        Me.Wiz.Appearance.PageTitle.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Wiz.Appearance.PageTitle.Options.UseFont = True
        Me.Wiz.Controls.Add(Me.wpWelcome)
        Me.Wiz.Controls.Add(Me.wpComplete)
        Me.Wiz.Controls.Add(Me.wpSpreadsheet)
        Me.Wiz.Controls.Add(Me.wpPreview)
        Me.Wiz.Controls.Add(Me.wpProgress)
        Me.Wiz.Dock = DockStyle.Fill
        Me.Wiz.Location = New System.Drawing.Point(0, 0)
        Me.Wiz.MinimumSize = New System.Drawing.Size(117, 115)
        Me.Wiz.Name = "Wiz"
        Me.Wiz.Pages.AddRange(New DevExpress.XtraWizard.BaseWizardPage() {Me.wpWelcome, Me.wpSpreadsheet, Me.wpPreview, Me.wpProgress, Me.wpComplete})
        Me.Wiz.Size = New System.Drawing.Size(922, 517)
        '
        'wpWelcome
        '
        Me.wpWelcome.IntroductionText = resources.GetString("wpWelcome.IntroductionText")
        Me.wpWelcome.Name = "wpWelcome"
        Me.wpWelcome.ProceedText = "To continue and select your Spreadsheet, click Next"
        Me.wpWelcome.Size = New System.Drawing.Size(705, 384)
        Me.wpWelcome.Text = "Welcome to the Data Import Wizard"
        '
        'wpComplete
        '
        Me.wpComplete.FinishText = "Congratulations!" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "You have successfully completed the import wizard." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.wpComplete.Name = "wpComplete"
        Me.wpComplete.Size = New System.Drawing.Size(705, 384)
        Me.wpComplete.Text = "Import Complete"
        '
        'wpSpreadsheet
        '
        Me.wpSpreadsheet.Controls.Add(Me.sc)
        Me.wpSpreadsheet.DescriptionText = "Check this is correct spreadsheet, then click Next when ready..."
        Me.wpSpreadsheet.Name = "wpSpreadsheet"
        Me.wpSpreadsheet.Size = New System.Drawing.Size(890, 372)
        Me.wpSpreadsheet.Text = "Spreadsheet Preview"
        '
        'sc
        '
        Me.sc.Dock = DockStyle.Fill
        Me.sc.Location = New System.Drawing.Point(0, 0)
        Me.sc.Name = "sc"
        Me.sc.Size = New System.Drawing.Size(890, 372)
        Me.sc.TabIndex = 2
        Me.sc.Text = "SpreadsheetControl1"
        '
        'wpPreview
        '
        Me.wpPreview.Controls.Add(Me.cgRecords)
        Me.wpPreview.DescriptionText = "Providing all your data is valid, Click Next to import your data..."
        Me.wpPreview.Name = "wpPreview"
        Me.wpPreview.Size = New System.Drawing.Size(890, 372)
        Me.wpPreview.Text = "Import Preview"
        '
        'cgRecords
        '
        Me.cgRecords.AllowBuildColumns = True
        Me.cgRecords.AllowEdit = False
        Me.cgRecords.AllowHorizontalScroll = True
        Me.cgRecords.AllowMultiSelect = False
        Me.cgRecords.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgRecords.Appearance.Options.UseFont = True
        Me.cgRecords.AutoSizeByData = True
        Me.cgRecords.DisableAutoSize = False
        Me.cgRecords.DisableDataFormatting = False
        Me.cgRecords.Dock = DockStyle.Fill
        Me.cgRecords.FocusedRowHandle = -2147483648
        Me.cgRecords.HideFirstColumn = False
        Me.cgRecords.Location = New System.Drawing.Point(0, 0)
        Me.cgRecords.Name = "cgRecords"
        Me.cgRecords.PreviewColumn = ""
        Me.cgRecords.QueryID = Nothing
        Me.cgRecords.RowAutoHeight = False
        Me.cgRecords.SearchAsYouType = True
        Me.cgRecords.ShowAutoFilterRow = False
        Me.cgRecords.ShowFindPanel = False
        Me.cgRecords.ShowGroupByBox = True
        Me.cgRecords.ShowLoadingPanel = False
        Me.cgRecords.ShowNavigator = False
        Me.cgRecords.Size = New System.Drawing.Size(890, 372)
        Me.cgRecords.TabIndex = 6
        '
        'wpProgress
        '
        Me.wpProgress.Controls.Add(Me.memProgress)
        Me.wpProgress.DescriptionText = "View the progress of your import..."
        Me.wpProgress.Name = "wpProgress"
        Me.wpProgress.Size = New System.Drawing.Size(890, 372)
        Me.wpProgress.Text = "Import Progress"
        '
        'memProgress
        '
        Me.memProgress.Dock = DockStyle.Fill
        Me.memProgress.Location = New System.Drawing.Point(0, 0)
        Me.memProgress.Name = "memProgress"
        Me.memProgress.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.memProgress, True)
        Me.memProgress.Size = New System.Drawing.Size(890, 372)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.memProgress, OptionsSpelling1)
        Me.memProgress.TabIndex = 0
        '
        'frmImportBookings
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(922, 517)
        Me.Controls.Add(Me.Wiz)
        Me.LoadMaximised = True
        Me.Margin = New Padding(3, 5, 3, 5)
        Me.Name = "frmImportBookings"
        Me.Text = "frmImportBookings"
        Me.WindowState = FormWindowState.Maximized
        CType(Me.Wiz, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Wiz.ResumeLayout(False)
        Me.wpSpreadsheet.ResumeLayout(False)
        Me.wpPreview.ResumeLayout(False)
        Me.wpProgress.ResumeLayout(False)
        CType(Me.memProgress.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Wiz As DevExpress.XtraWizard.WizardControl
    Friend WithEvents wpWelcome As DevExpress.XtraWizard.WelcomeWizardPage
    Friend WithEvents wpComplete As DevExpress.XtraWizard.CompletionWizardPage
    Friend WithEvents wpSpreadsheet As DevExpress.XtraWizard.WizardPage
    Friend WithEvents sc As DevExpress.XtraSpreadsheet.SpreadsheetControl
    Friend WithEvents wpPreview As DevExpress.XtraWizard.WizardPage
    Friend WithEvents cgRecords As Care.Controls.CareGrid
    Friend WithEvents wpProgress As DevExpress.XtraWizard.WizardPage
    Friend WithEvents memProgress As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents ofd As OpenFileDialog
End Class
