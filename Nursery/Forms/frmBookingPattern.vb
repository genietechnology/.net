﻿Imports Care.Global
Imports Care.Data
Imports System.Windows.Forms

Public Class frmBookingPattern

    Public Enum EnumPatternType
        Recurring
        Override
        Additional
    End Enum

    Private m_SiteID As Guid
    Private m_ChildID As Guid
    Private m_Waiting As Boolean = False
    Private m_SessionID As Guid?
    Private m_IsNew As Boolean = True
    Private m_PatternType As EnumPatternType
    Private m_CopyTariff As Business.ChildAdvanceSession = Nothing

    Public ReadOnly Property CopyTariff As Business.ChildAdvanceSession
        Get
            Return m_CopyTariff
        End Get
    End Property

    Public Sub New(ByVal SiteID As Guid, ByVal ChildID As Guid, ByVal OnWaitingList As Boolean, ByVal SessionID As Guid?, ByVal PatternType As EnumPatternType, ByVal SingleSession As Boolean, ByVal CopyTariff As Business.ChildAdvanceSession)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        m_SiteID = SiteID
        m_ChildID = ChildID
        m_Waiting = OnWaitingList

        m_CopyTariff = CopyTariff
        If m_CopyTariff Is Nothing Then
            btnPaste.Enabled = False
        Else
            btnPaste.Enabled = True
        End If

        If SessionID.HasValue Then
            m_IsNew = False
            m_SessionID = SessionID
        Else
            m_IsNew = True
            m_SessionID = Nothing
        End If

        m_PatternType = PatternType
        If m_PatternType = EnumPatternType.Recurring Then radRecurring.Checked = True
        If m_PatternType = EnumPatternType.Override Then radOverride.Checked = True
        If m_PatternType = EnumPatternType.Additional Then radAdditional.Checked = True

        If m_IsNew Then

            If SingleSession Then
                radSessSingle.Checked = True
                grpDuplicate.Enabled = False
            Else
                radSessMulti.Checked = True
                grpDuplicate.Enabled = True
            End If

            radWaiting.Checked = m_Waiting

        Else
            grpDuplicate.Enabled = False
        End If

    End Sub

    Private Sub frmSession_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cdtWC.ReadOnly = False
        cdtWC.BackColor = Session.ChangeColour

        cbxScope.BackColor = Session.ChangeColour
        cbxScope.AddItem("No, Pattern disabled")
        cbxScope.AddItem("Yes, All the time")
        cbxScope.AddItem("Yes, During Term Time Only")
        cbxScope.AddItem("Yes, During Term Holidays Only")

        txtComments.BackColor = Session.ChangeColour

        tcMonday.SiteID = m_SiteID
        tcTuesday.SiteID = m_SiteID
        tcWednesday.SiteID = m_SiteID
        tcThursday.SiteID = m_SiteID
        tcFriday.SiteID = m_SiteID
        tcSaturday.SiteID = m_SiteID
        tcSunday.SiteID = m_SiteID

        If m_SessionID.HasValue Then
            Me.Text = "Edit " + m_PatternType.ToString + " Booking Pattern"
            DisplayRecord()
        Else
            Me.Text = "Create " + m_PatternType.ToString + " Booking Pattern"
            SetControlsByType()
        End If

    End Sub

    Private Sub frmSession_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        cdtWC.Focus()
    End Sub

    Private Sub DisplayRecord()
        Dim _S As Business.ChildAdvanceSession = Business.ChildAdvanceSession.RetreiveByID(m_SessionID.Value)
        SetControlsFromAdvanceSessionObject(_S)
    End Sub

    Private Sub SetControlsFromAdvanceSessionObject(ByRef ChildAdvanceSessionObject As Business.ChildAdvanceSession)

        With ChildAdvanceSessionObject

            If ._Mode = "Daily" Then radSessMulti.Checked = True
            If ._Mode = "Weekly" Then radSessSingle.Checked = True

            If ._PatternType = "Recurring" Then radRecurring.Checked = True
            If ._PatternType = "Override" Then radOverride.Checked = True
            If ._PatternType = "Additional" Then radAdditional.Checked = True

            If ._BookingStatus = "Waiting" Then radWaiting.Checked = True

            SetControlsByType()

            cdtWC.Value = ._DateFrom
            SetDateLabels()

            tcMonday.ComboValue = ._Monday
            tcMonday.Value1 = ._Monday1
            tcMonday.Value2 = ._Monday2
            tcMonday.ValueFEEE = ._MondayFund
            tcMonday.BoltOns = ._MondayBoid

            tcTuesday.ComboValue = ._Tuesday
            tcTuesday.Value1 = ._Tuesday1
            tcTuesday.Value2 = ._Tuesday2
            tcTuesday.ValueFEEE = ._TuesdayFund
            tcTuesday.BoltOns = ._TuesdayBoid

            tcWednesday.ComboValue = ._Wednesday
            tcWednesday.Value1 = ._Wednesday1
            tcWednesday.Value2 = ._Wednesday2
            tcWednesday.ValueFEEE = ._WednesdayFund
            tcWednesday.BoltOns = ._WednesdayBoid

            tcThursday.ComboValue = ._Thursday
            tcThursday.Value1 = ._Thursday1
            tcThursday.Value2 = ._Thursday2
            tcThursday.ValueFEEE = ._ThursdayFund
            tcThursday.BoltOns = ._ThursdayBoid

            tcFriday.ComboValue = ._Friday
            tcFriday.Value1 = ._Friday1
            tcFriday.Value2 = ._Friday2
            tcFriday.ValueFEEE = ._FridayFund
            tcFriday.BoltOns = ._FridayBoid

            tcSaturday.ComboValue = ._Saturday
            tcSaturday.Value1 = ._Saturday1
            tcSaturday.Value2 = ._Saturday2
            tcSaturday.ValueFEEE = ._SaturdayFund
            tcSaturday.BoltOns = ._SaturdayBoid

            tcSunday.ComboValue = ._Sunday
            tcSunday.Value1 = ._Sunday1
            tcSunday.Value2 = ._Sunday2
            tcSunday.ValueFEEE = ._SundayFund
            tcSunday.BoltOns = ._SundayBoid

            SetScopes()
            If ._PatternType = "Recurring" Then
                cbxScope.SelectedIndex = ._RecurringScope
            End If

            txtComments.Text = ._Comments

        End With

    End Sub

    Private Function ValidateAdvancedSession() As Boolean

        If cdtWC.Text = "" Then Return False

        If cdtWC.Value Is Nothing Then Return False
        If Not cdtWC.Value.HasValue Then Return False

        If radSessSingle.Checked Then
            If cdtWC.Value.Value.DayOfWeek <> DayOfWeek.Monday Then
                CareMessage("The session start date must be a Monday.", MessageBoxIcon.Exclamation, "Date Validation")
                Return False
            End If
        End If

        If Not ValidateTariffControl(tcMonday) Then Return False
        If Not ValidateTariffControl(tcTuesday) Then Return False
        If Not ValidateTariffControl(tcWednesday) Then Return False
        If Not ValidateTariffControl(tcThursday) Then Return False
        If Not ValidateTariffControl(tcFriday) Then Return False
        If Not ValidateTariffControl(tcSaturday) Then Return False
        If Not ValidateTariffControl(tcSunday) Then Return False

        If grpDuplicate.Enabled Then
            If chkMonday.Checked = False AndAlso chkTuesday.Checked = False AndAlso chkWednesday.Checked = False AndAlso chkThursday.Checked = False AndAlso chkFriday.Checked = False Then
                'no duplicated ticked
            Else
                If CareMessage("You have chosen to duplicate this pattern. Are you sure you wish to continue?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Create Duplicate Sessions") = DialogResult.Yes Then
                    'cool beans, lets carry on...
                Else
                    Return False
                End If
            End If
        End If

        'ok, now we check each session, if we find one is set - we return true
        'the first value in each session is blank
        If tcMonday.SelectedIndex > 0 Then Return True
        If tcTuesday.SelectedIndex > 0 Then Return True
        If tcWednesday.SelectedIndex > 0 Then Return True
        If tcThursday.SelectedIndex > 0 Then Return True
        If tcFriday.SelectedIndex > 0 Then Return True
        If tcSaturday.SelectedIndex > 0 Then Return True
        If tcSunday.SelectedIndex > 0 Then Return True

        'no sessions were set, warn the user
        Dim _Mess As String = "You have not selected any sessions for this period." + vbNewLine _
                              + "Therefore, no charges will be raised during Invoicing." + vbNewLine + vbNewLine _
                              + "Are you sure you wish to continue?"

        If CareMessage(_Mess, MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Session Validation") = DialogResult.Yes Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Function ValidateTariffControl(ByRef TariffControlIn As TariffControl) As Boolean

        If TariffControlIn.OverrideAvailable Then

            Select Case TariffControlIn.OverrideType

                Case "Manual Times", "Time Matrix", "Age Matrix (Times)"
                    If Not TariffControlIn.TimesValid Then
                        TariffControlIn.HighlightError()
                        Return False
                    Else
                        Return True
                    End If

                Case Else
                    If TariffControlIn.Value1 <= 0 Then
                        TariffControlIn.HighlightError()
                        Return False
                    Else
                        Return True
                    End If

            End Select

        Else
            Return True
        End If

    End Function

    Private Sub SetDateLabels()

        If cdtWC.Value Is Nothing Then Exit Sub
        If Not cdtWC.Value.HasValue Then Exit Sub

        lblMustBeMonday.ResetForeColor()

        If radSessMulti.Checked Then
            If m_PatternType = EnumPatternType.Recurring Then
                lblMustBeMonday.Text = "recurring every " + cdtWC.Value.Value.DayOfWeek.ToString
            End If
        Else

            If cdtWC.Value.Value.DayOfWeek = DayOfWeek.Monday Then

                Dim _Date As Date = cdtWC.Value.Value
                lblMonday.Text = "Monday (" + _Date.Day.ToString + " " + MonthName(_Date.Month) + ")"
                _Date = _Date.AddDays(1)
                lblTuesday.Text = "Tuesday (" + _Date.Day.ToString + " " + MonthName(_Date.Month) + ")"
                _Date = _Date.AddDays(1)
                lblWednesday.Text = "Wednesday (" + _Date.Day.ToString + " " + MonthName(_Date.Month) + ")"
                _Date = _Date.AddDays(1)
                lblThursday.Text = "Thursday (" + _Date.Day.ToString + " " + MonthName(_Date.Month) + ")"
                _Date = _Date.AddDays(1)
                lblFriday.Text = "Friday (" + _Date.Day.ToString + " " + MonthName(_Date.Month) + ")"
                _Date = _Date.AddDays(1)
                lblSaturday.Text = "Saturday (" + _Date.Day.ToString + " " + MonthName(_Date.Month) + ")"
                _Date = _Date.AddDays(1)
                lblSunday.Text = "Sunday (" + _Date.Day.ToString + " " + MonthName(_Date.Month) + ")"

            Else
                lblMustBeMonday.ForeColor = Drawing.Color.Red
            End If

        End If

    End Sub

    Private Sub SetControlsByType()

        Dim _Days As Boolean = True
        Dim _PopulationType As TariffControl.EnumPopulateMode = TariffControl.EnumPopulateMode.Daily

        lblMonday.Text = "Monday"
        lblTuesday.Text = "Tuesday"
        lblWednesday.Text = "Wednesday"
        lblThursday.Text = "Thursday"
        lblFriday.Text = "Friday"
        lblSaturday.Text = "Saturday"
        lblSunday.Text = "Sunday"

        If m_IsNew Then
            grpDuplicate.Enabled = radSessMulti.Checked
        Else
            grpDuplicate.Enabled = False
        End If

        If radSessMulti.Checked Then

            _PopulationType = TariffControl.EnumPopulateMode.Daily

            If m_PatternType = EnumPatternType.Recurring Then
                lblWC.Text = "From"
            Else
                lblWC.Text = "Date"
            End If

            lblMustBeMonday.Text = ""

            lblMonday.Text = "Session 1"
            lblTuesday.Text = "Session 2"
            lblWednesday.Text = "Session 3"
            lblThursday.Text = "Session 4"
            lblFriday.Text = "Session 5"
            lblSaturday.Text = "Session 6"
            lblSunday.Text = "Session 7"

            gbxWeekly.Text = "Multiple Sessions per Day:"

        Else

            _PopulationType = TariffControl.EnumPopulateMode.Weekly
            lblWC.Text = "Week Commencing"
            lblMustBeMonday.Text = "(must be a Monday)"

            gbxWeekly.Text = "Single Session per Day:"

            chkMonday.Checked = False
            chkTuesday.Checked = False
            chkWednesday.Checked = False
            chkThursday.Checked = False
            chkFriday.Checked = False

        End If

        tcMonday.Enabled = _Days
        tcMonday.PopulateMode = _PopulationType
        tcTuesday.Enabled = _Days
        tcTuesday.PopulateMode = _PopulationType
        tcWednesday.Enabled = _Days
        tcWednesday.PopulateMode = _PopulationType
        tcThursday.Enabled = _Days
        tcThursday.PopulateMode = _PopulationType
        tcFriday.Enabled = _Days
        tcFriday.PopulateMode = _PopulationType
        tcSaturday.Enabled = _Days
        tcSaturday.PopulateMode = _PopulationType
        tcSunday.Enabled = _Days
        tcSunday.PopulateMode = _PopulationType

        If cdtWC.Value.HasValue Then
            cdtWC.Text = ""
        End If

        SetScopes()

    End Sub

    Private Sub SaveAndExit()

        btnAdvOK.Enabled = False
        btnAdvCancel.Enabled = False

        Session.CursorWaiting()

        If Save() Then
            Me.DialogResult = DialogResult.OK
            Me.Close()
        Else
            btnAdvOK.Enabled = True
            btnAdvCancel.Enabled = True
        End If

        Session.CursorDefault()

    End Sub

    Private Function Save() As Boolean

        If Not ValidateAdvancedSession() Then Return False

        Dim _S As Business.ChildAdvanceSession = SetTariffObject()
        _S.Store()

        m_SessionID = _S._ID

        'duplicate sessions
        If m_IsNew AndAlso radSessMulti.Checked Then
            DuplicateSessions(_S)
        End If

        _S = Nothing

        Return True

    End Function

    Private Function SetTariffObject() As Business.ChildAdvanceSession

        Dim _S As Business.ChildAdvanceSession = Nothing

        If m_IsNew Then
            _S = New Business.ChildAdvanceSession
            _S._ID = Guid.NewGuid
            _S._ChildId = m_ChildID
            _S._PatternType = m_PatternType.ToString
            _S._CreatedBy = Session.CurrentUser.FullName
            _S._CreatedStamp = Now
        Else
            _S = Business.ChildAdvanceSession.RetreiveByID(m_SessionID.Value)
            _S._ModifiedBy = Session.CurrentUser.FullName
            _S._ModifiedStamp = Now
        End If

        With _S

            ._DateFrom = cdtWC.Value

            If radSessMulti.Checked Then ._Mode = "Daily"
            If radSessSingle.Checked Then ._Mode = "Weekly"

            ._PatternType = m_PatternType.ToString
            If m_PatternType = EnumPatternType.Recurring Then
                ._NoRepeat = False
                ._RecurringScope = CByte(cbxScope.SelectedIndex)
                ._RecurringScopeDesc = cbxScope.Text
            Else
                ._NoRepeat = True
                ._RecurringScope = Nothing
                ._RecurringScopeDesc = Nothing
            End If

            If radBooked.Checked Then ._BookingStatus = "Booked"
            If radWaiting.Checked Then ._BookingStatus = "Waiting"

            SetDays(_S)

            ._Comments = txtComments.Text

        End With

        Return _S

    End Function

    Private Sub DuplicateSessions(ByRef CAS As Business.ChildAdvanceSession)

        If Not CAS._DateFrom.HasValue Then Exit Sub

        If chkMonday.Checked = False AndAlso chkTuesday.Checked = False AndAlso chkWednesday.Checked = False AndAlso chkThursday.Checked = False AndAlso chkFriday.Checked = False Then
            Exit Sub
        End If

        Dim _DateMonday As Date = ValueHandler.NearestDate(CAS._DateFrom.Value, DayOfWeek.Monday, ValueHandler.EnumDirection.Forwards)
        Dim _DateTuesday As Date = ValueHandler.NearestDate(CAS._DateFrom.Value, DayOfWeek.Tuesday, ValueHandler.EnumDirection.Forwards)
        Dim _DateWednesday As Date = ValueHandler.NearestDate(CAS._DateFrom.Value, DayOfWeek.Wednesday, ValueHandler.EnumDirection.Forwards)
        Dim _DateThursday As Date = ValueHandler.NearestDate(CAS._DateFrom.Value, DayOfWeek.Thursday, ValueHandler.EnumDirection.Forwards)
        Dim _DateFriday As Date = ValueHandler.NearestDate(CAS._DateFrom.Value, DayOfWeek.Friday, ValueHandler.EnumDirection.Forwards)

        Select Case CAS._DateFrom.Value.DayOfWeek

            Case DayOfWeek.Monday
                If chkTuesday.Checked Then CreateDuplicateSessionRecord(_DateTuesday, CAS)
                If chkWednesday.Checked Then CreateDuplicateSessionRecord(_DateWednesday, CAS)
                If chkThursday.Checked Then CreateDuplicateSessionRecord(_DateThursday, CAS)
                If chkFriday.Checked Then CreateDuplicateSessionRecord(_DateFriday, CAS)

            Case DayOfWeek.Tuesday
                If chkMonday.Checked Then CreateDuplicateSessionRecord(_DateMonday, CAS)
                If chkWednesday.Checked Then CreateDuplicateSessionRecord(_DateWednesday, CAS)
                If chkThursday.Checked Then CreateDuplicateSessionRecord(_DateThursday, CAS)
                If chkFriday.Checked Then CreateDuplicateSessionRecord(_DateFriday, CAS)

            Case DayOfWeek.Wednesday
                If chkMonday.Checked Then CreateDuplicateSessionRecord(_DateMonday, CAS)
                If chkTuesday.Checked Then CreateDuplicateSessionRecord(_DateTuesday, CAS)
                If chkThursday.Checked Then CreateDuplicateSessionRecord(_DateThursday, CAS)
                If chkFriday.Checked Then CreateDuplicateSessionRecord(_DateFriday, CAS)

            Case DayOfWeek.Thursday
                If chkMonday.Checked Then CreateDuplicateSessionRecord(_DateMonday, CAS)
                If chkTuesday.Checked Then CreateDuplicateSessionRecord(_DateTuesday, CAS)
                If chkWednesday.Checked Then CreateDuplicateSessionRecord(_DateWednesday, CAS)
                If chkFriday.Checked Then CreateDuplicateSessionRecord(_DateFriday, CAS)

            Case DayOfWeek.Friday
                If chkMonday.Checked Then CreateDuplicateSessionRecord(_DateMonday, CAS)
                If chkTuesday.Checked Then CreateDuplicateSessionRecord(_DateTuesday, CAS)
                If chkWednesday.Checked Then CreateDuplicateSessionRecord(_DateWednesday, CAS)
                If chkThursday.Checked Then CreateDuplicateSessionRecord(_DateThursday, CAS)

        End Select

    End Sub

    Private Sub CreateDuplicateSessionRecord(ByVal DateFrom As Date, ByRef CAS As Business.ChildAdvanceSession)

        Dim _S As New Business.ChildAdvanceSession

        _S._ID = Guid.NewGuid
        _S._ChildId = CAS._ChildId
        _S._PatternType = CAS._PatternType

        With _S

            ._DateFrom = DateFrom
            ._Mode = CAS._Mode
            ._NoRepeat = CAS._NoRepeat
            ._RecurringScope = CAS._RecurringScope
            ._RecurringScopeDesc = CAS._RecurringScopeDesc

            SetDays(_S)

            .Store()

        End With


    End Sub

    Private Sub SetDays(ByRef CAS As Business.ChildAdvanceSession)

        With CAS

            ._Monday = tcMonday.ComboValue
            ._MondayDesc = tcMonday.Description
            ._Monday1 = tcMonday.Value1
            ._Monday2 = tcMonday.Value2
            ._MondayFund = tcMonday.ValueFEEE
            ._MondayBoid = tcMonday.BoltOns
            ._MondayBo = tcMonday.BoltOnNames

            ._Tuesday = tcTuesday.ComboValue
            ._TuesdayDesc = tcTuesday.Description
            ._Tuesday1 = tcTuesday.Value1
            ._Tuesday2 = tcTuesday.Value2
            ._TuesdayFund = tcTuesday.ValueFEEE
            ._TuesdayBoid = tcTuesday.BoltOns
            ._TuesdayBo = tcTuesday.BoltOnNames

            ._Wednesday = tcWednesday.ComboValue
            ._WednesdayDesc = tcWednesday.Description
            ._Wednesday1 = tcWednesday.Value1
            ._Wednesday2 = tcWednesday.Value2
            ._WednesdayFund = tcWednesday.ValueFEEE
            ._WednesdayBoid = tcWednesday.BoltOns
            ._WednesdayBo = tcWednesday.BoltOnNames

            ._Thursday = tcThursday.ComboValue
            ._ThursdayDesc = tcThursday.Description
            ._Thursday1 = tcThursday.Value1
            ._Thursday2 = tcThursday.Value2
            ._ThursdayFund = tcThursday.ValueFEEE
            ._ThursdayBoid = tcThursday.BoltOns
            ._ThursdayBo = tcThursday.BoltOnNames

            ._Friday = tcFriday.ComboValue
            ._FridayDesc = tcFriday.Description
            ._Friday1 = tcFriday.Value1
            ._Friday2 = tcFriday.Value2
            ._FridayFund = tcFriday.ValueFEEE
            ._FridayBoid = tcFriday.BoltOns
            ._FridayBo = tcFriday.BoltOnNames

            ._Saturday = tcSaturday.ComboValue
            ._SaturdayDesc = tcSaturday.Description
            ._Saturday1 = tcSaturday.Value1
            ._Saturday2 = tcSaturday.Value2
            ._SaturdayFund = tcSaturday.ValueFEEE
            ._SaturdayBoid = tcSaturday.BoltOns
            ._SaturdayBo = tcSaturday.BoltOnNames

            ._Sunday = tcSunday.ComboValue
            ._SundayDesc = tcSunday.Description
            ._Sunday1 = tcSunday.Value1
            ._Sunday2 = tcSunday.Value2
            ._SundayFund = tcSunday.ValueFEEE
            ._SundayBoid = tcSunday.BoltOns
            ._SundayBo = tcSunday.BoltOnNames

        End With

    End Sub


#Region "Controls"

    Private Sub cdtWC_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles cdtWC.Validating

        If Not cdtWC.Value.HasValue Then Exit Sub
        If Not radSessMulti.Checked Then Exit Sub

        lblMustBeMonday.ResetForeColor()
        If m_PatternType = EnumPatternType.Recurring Then

            lblMustBeMonday.Text = "recurring every " + cdtWC.Value.Value.DayOfWeek.ToString

            lblDMonday.Enabled = True
            chkMonday.Enabled = True
            chkMonday.Checked = False

            lblDTuesday.Enabled = True
            chkTuesday.Enabled = True
            chkTuesday.Checked = False

            lblDWednesday.Enabled = True
            chkWednesday.Enabled = True
            chkWednesday.Checked = False

            lblDThursday.Enabled = True
            chkThursday.Enabled = True
            chkThursday.Checked = False

            lblDFriday.Enabled = True
            chkFriday.Enabled = True
            chkFriday.Checked = False

            Select Case cdtWC.Value.Value.DayOfWeek

                Case DayOfWeek.Monday
                    lblDMonday.Enabled = False
                    chkMonday.Enabled = False

                Case DayOfWeek.Tuesday
                    lblDTuesday.Enabled = False
                    chkTuesday.Enabled = False

                Case DayOfWeek.Wednesday
                    lblDWednesday.Enabled = False
                    chkWednesday.Enabled = False

                Case DayOfWeek.Thursday
                    lblDThursday.Enabled = False
                    chkThursday.Enabled = False

                Case DayOfWeek.Friday
                    lblDFriday.Enabled = False
                    chkFriday.Enabled = False

            End Select

        Else
            lblMustBeMonday.Text = cdtWC.Value.Value.DayOfWeek.ToString
        End If

    End Sub

    Private Sub cdtWC_Validated(sender As Object, e As EventArgs) Handles cdtWC.Validated
        SetDateLabels()
    End Sub

    Private Sub btnAdvOK_Click(sender As Object, e As EventArgs) Handles btnAdvOK.Click
        SaveAndExit()
    End Sub

    Private Sub btnAdvCancel_Click(sender As Object, e As EventArgs) Handles btnAdvCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

#End Region

    Private Sub lblWC_DoubleClick(sender As Object, e As EventArgs) Handles lblWC.DoubleClick

        Dim _C As Business.Child = Business.Child.RetreiveByID(m_ChildID)
        If _C IsNot Nothing AndAlso _C._Started.HasValue Then
            If radSessSingle.Checked Then
                cdtWC.Value = ValueHandler.NearestDate(_C._Started.Value, DayOfWeek.Monday, ValueHandler.EnumDirection.Backwards)
            Else
                cdtWC.Value = _C._Started
            End If
            _C = Nothing
        End If

    End Sub

    Private Sub radSessSingle_CheckedChanged(sender As Object, e As EventArgs) Handles radSessSingle.CheckedChanged
        SetControlsByType()
        If Me.Visible Then cdtWC.Focus()
    End Sub

    Private Sub radSessMulti_CheckedChanged(sender As Object, e As EventArgs) Handles radSessMulti.CheckedChanged
        SetControlsByType()
        If Me.Visible Then cdtWC.Focus()
    End Sub

    Private Sub radRecurring_CheckedChanged(sender As Object, e As EventArgs) Handles radRecurring.CheckedChanged
        If radRecurring.Checked Then SetScopes()
    End Sub

    Private Sub radOverride_CheckedChanged(sender As Object, e As EventArgs) Handles radOverride.CheckedChanged
        If radOverride.Checked Then SetScopes()
    End Sub

    Private Sub radAdditional_CheckedChanged(sender As Object, e As EventArgs) Handles radAdditional.CheckedChanged
        If radAdditional.Checked Then SetScopes()
    End Sub

    Private Sub SetScopes()

        If radRecurring.Checked Then m_PatternType = EnumPatternType.Recurring
        If radAdditional.Checked Then m_PatternType = EnumPatternType.Additional
        If radOverride.Checked Then m_PatternType = EnumPatternType.Override

        If radRecurring.Checked Then
            gbxScope.Enabled = True
            cbxScope.BackColor = Session.ChangeColour
            If cbxScope.SelectedIndex < 0 Then cbxScope.SelectedIndex = 1
        Else
            cbxScope.ResetBackColor()
            cbxScope.SelectedIndex = -1
            gbxScope.Enabled = False
        End If

    End Sub

    Private Sub btnCopy_Click(sender As Object, e As EventArgs) Handles btnCopy.Click
        If Not ValidateAdvancedSession() Then Exit Sub
        m_CopyTariff = SetTariffObject()
        CareMessage("Tariff copied Successfully.", MessageBoxIcon.Information, "Copy Tariff")
    End Sub

    Private Sub btnPaste_Click(sender As Object, e As EventArgs) Handles btnPaste.Click

        Dim _Mess As String = "Are you sure you want to paste the stored tariff?"
        If Not m_IsNew Then _Mess = "Are you sure you want overwrite with the stored tariff?"

        If CareMessage(_Mess, MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "") = DialogResult.Yes Then
            SetControlsFromAdvanceSessionObject(m_CopyTariff)
        End If

    End Sub

End Class