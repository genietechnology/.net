﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRosterShiftNew
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxShift = New Care.Controls.CareFrame()
        Me.txtShiftEnd = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.txtShiftHours = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel16 = New Care.Controls.CareLabel(Me.components)
        Me.txtShiftCost = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel19 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel20 = New Care.Controls.CareLabel(Me.components)
        Me.txtShiftStart = New Care.Controls.CareTextBox(Me.components)
        Me.btnShiftOK = New Care.Controls.CareButton(Me.components)
        Me.btnShiftCancel = New Care.Controls.CareButton(Me.components)
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.btnView = New Care.Controls.CareButton(Me.components)
        Me.btnSelect = New Care.Controls.CareButton(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.txtRoom = New Care.Controls.CareTextBox(Me.components)
        Me.txtStaff = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.Label2 = New Care.Controls.CareLabel(Me.components)
        Me.txtDate = New Care.Controls.CareTextBox(Me.components)
        Me.GroupControl3 = New Care.Controls.CareFrame()
        Me.cgShifts = New Care.Controls.CareGrid()
        Me.GroupControl2 = New Care.Controls.CareFrame()
        Me.cgNotRostered = New Care.Controls.CareGrid()
        CType(Me.gbxShift, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxShift.SuspendLayout()
        CType(Me.txtShiftEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtShiftHours.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtShiftCost.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtShiftStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtRoom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStaff.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'gbxShift
        '
        Me.gbxShift.Controls.Add(Me.txtShiftEnd)
        Me.gbxShift.Controls.Add(Me.CareLabel11)
        Me.gbxShift.Controls.Add(Me.txtShiftHours)
        Me.gbxShift.Controls.Add(Me.CareLabel16)
        Me.gbxShift.Controls.Add(Me.txtShiftCost)
        Me.gbxShift.Controls.Add(Me.CareLabel19)
        Me.gbxShift.Controls.Add(Me.CareLabel20)
        Me.gbxShift.Controls.Add(Me.txtShiftStart)
        Me.gbxShift.Location = New System.Drawing.Point(503, 12)
        Me.gbxShift.Name = "gbxShift"
        Me.gbxShift.Size = New System.Drawing.Size(485, 114)
        Me.gbxShift.TabIndex = 2
        Me.gbxShift.Text = "Shift Details"
        '
        'txtShiftEnd
        '
        Me.txtShiftEnd.CharacterCasing = CharacterCasing.Normal
        Me.txtShiftEnd.EnterMoveNextControl = True
        Me.txtShiftEnd.Location = New System.Drawing.Point(266, 28)
        Me.txtShiftEnd.MaxLength = 5
        Me.txtShiftEnd.Name = "txtShiftEnd"
        Me.txtShiftEnd.NumericAllowNegatives = False
        Me.txtShiftEnd.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.Time
        Me.txtShiftEnd.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtShiftEnd.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtShiftEnd.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtShiftEnd.Properties.Appearance.Options.UseFont = True
        Me.txtShiftEnd.Properties.Appearance.Options.UseTextOptions = True
        Me.txtShiftEnd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtShiftEnd.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic
        Me.txtShiftEnd.Properties.Mask.EditMask = "(0?\d|1\d|2[0-3])\:[0-5]\d"
        Me.txtShiftEnd.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtShiftEnd.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtShiftEnd.Properties.MaxLength = 5
        Me.txtShiftEnd.Size = New System.Drawing.Size(81, 22)
        Me.txtShiftEnd.TabIndex = 3
        Me.txtShiftEnd.Tag = ""
        Me.txtShiftEnd.TextAlign = HorizontalAlignment.Left
        Me.txtShiftEnd.ToolTipText = ""
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(210, 31)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(50, 15)
        Me.CareLabel11.TabIndex = 2
        Me.CareLabel11.Text = "End Time"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtShiftHours
        '
        Me.txtShiftHours.CharacterCasing = CharacterCasing.Normal
        Me.txtShiftHours.EnterMoveNextControl = True
        Me.txtShiftHours.Location = New System.Drawing.Point(108, 56)
        Me.txtShiftHours.MaxLength = 0
        Me.txtShiftHours.Name = "txtShiftHours"
        Me.txtShiftHours.NumericAllowNegatives = False
        Me.txtShiftHours.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtShiftHours.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtShiftHours.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtShiftHours.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtShiftHours.Properties.Appearance.Options.UseFont = True
        Me.txtShiftHours.Properties.Appearance.Options.UseTextOptions = True
        Me.txtShiftHours.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtShiftHours.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtShiftHours.Size = New System.Drawing.Size(81, 22)
        Me.txtShiftHours.TabIndex = 5
        Me.txtShiftHours.Tag = "R"
        Me.txtShiftHours.TextAlign = HorizontalAlignment.Left
        Me.txtShiftHours.ToolTipText = ""
        '
        'CareLabel16
        '
        Me.CareLabel16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel16.Location = New System.Drawing.Point(9, 87)
        Me.CareLabel16.Name = "CareLabel16"
        Me.CareLabel16.Size = New System.Drawing.Size(51, 15)
        Me.CareLabel16.TabIndex = 6
        Me.CareLabel16.Text = "Shift Cost"
        Me.CareLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtShiftCost
        '
        Me.txtShiftCost.CharacterCasing = CharacterCasing.Normal
        Me.txtShiftCost.EnterMoveNextControl = True
        Me.txtShiftCost.Location = New System.Drawing.Point(108, 84)
        Me.txtShiftCost.MaxLength = 0
        Me.txtShiftCost.Name = "txtShiftCost"
        Me.txtShiftCost.NumericAllowNegatives = False
        Me.txtShiftCost.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtShiftCost.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtShiftCost.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtShiftCost.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtShiftCost.Properties.Appearance.Options.UseFont = True
        Me.txtShiftCost.Properties.Appearance.Options.UseTextOptions = True
        Me.txtShiftCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtShiftCost.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtShiftCost.Size = New System.Drawing.Size(81, 22)
        Me.txtShiftCost.TabIndex = 7
        Me.txtShiftCost.Tag = "R"
        Me.txtShiftCost.TextAlign = HorizontalAlignment.Left
        Me.txtShiftCost.ToolTipText = ""
        '
        'CareLabel19
        '
        Me.CareLabel19.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel19.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel19.Location = New System.Drawing.Point(9, 59)
        Me.CareLabel19.Name = "CareLabel19"
        Me.CareLabel19.Size = New System.Drawing.Size(59, 15)
        Me.CareLabel19.TabIndex = 4
        Me.CareLabel19.Text = "Shift Hours"
        Me.CareLabel19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel20
        '
        Me.CareLabel20.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel20.Location = New System.Drawing.Point(9, 31)
        Me.CareLabel20.Name = "CareLabel20"
        Me.CareLabel20.Size = New System.Drawing.Size(81, 15)
        Me.CareLabel20.TabIndex = 0
        Me.CareLabel20.Text = "Shift Start Time"
        Me.CareLabel20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtShiftStart
        '
        Me.txtShiftStart.CharacterCasing = CharacterCasing.Normal
        Me.txtShiftStart.EnterMoveNextControl = True
        Me.txtShiftStart.Location = New System.Drawing.Point(108, 28)
        Me.txtShiftStart.MaxLength = 5
        Me.txtShiftStart.Name = "txtShiftStart"
        Me.txtShiftStart.NumericAllowNegatives = False
        Me.txtShiftStart.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.Time
        Me.txtShiftStart.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtShiftStart.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtShiftStart.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtShiftStart.Properties.Appearance.Options.UseFont = True
        Me.txtShiftStart.Properties.Appearance.Options.UseTextOptions = True
        Me.txtShiftStart.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtShiftStart.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic
        Me.txtShiftStart.Properties.Mask.EditMask = "(0?\d|1\d|2[0-3])\:[0-5]\d"
        Me.txtShiftStart.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtShiftStart.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtShiftStart.Properties.MaxLength = 5
        Me.txtShiftStart.Size = New System.Drawing.Size(81, 22)
        Me.txtShiftStart.TabIndex = 1
        Me.txtShiftStart.Tag = ""
        Me.txtShiftStart.TextAlign = HorizontalAlignment.Left
        Me.txtShiftStart.ToolTipText = ""
        '
        'btnShiftOK
        '
        Me.btnShiftOK.Location = New System.Drawing.Point(812, 383)
        Me.btnShiftOK.Name = "btnShiftOK"
        Me.btnShiftOK.Size = New System.Drawing.Size(85, 25)
        Me.btnShiftOK.TabIndex = 4
        Me.btnShiftOK.Text = "OK"
        '
        'btnShiftCancel
        '
        Me.btnShiftCancel.Location = New System.Drawing.Point(903, 383)
        Me.btnShiftCancel.Name = "btnShiftCancel"
        Me.btnShiftCancel.Size = New System.Drawing.Size(85, 25)
        Me.btnShiftCancel.TabIndex = 5
        Me.btnShiftCancel.Text = "Cancel"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.btnView)
        Me.GroupControl1.Controls.Add(Me.btnSelect)
        Me.GroupControl1.Controls.Add(Me.CareLabel6)
        Me.GroupControl1.Controls.Add(Me.txtRoom)
        Me.GroupControl1.Controls.Add(Me.txtStaff)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Controls.Add(Me.Label2)
        Me.GroupControl1.Controls.Add(Me.txtDate)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(485, 114)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Selected Staff Member"
        '
        'btnView
        '
        Me.btnView.Location = New System.Drawing.Point(390, 53)
        Me.btnView.Name = "btnView"
        Me.btnView.Size = New System.Drawing.Size(85, 25)
        Me.btnView.TabIndex = 5
        Me.btnView.Text = "View"
        '
        'btnSelect
        '
        Me.btnSelect.Location = New System.Drawing.Point(390, 25)
        Me.btnSelect.Name = "btnSelect"
        Me.btnSelect.Size = New System.Drawing.Size(85, 25)
        Me.btnSelect.TabIndex = 2
        Me.btnSelect.Text = "Select"
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(10, 86)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(59, 15)
        Me.CareLabel6.TabIndex = 6
        Me.CareLabel6.Text = "Shift Room"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRoom
        '
        Me.txtRoom.CharacterCasing = CharacterCasing.Normal
        Me.txtRoom.EnterMoveNextControl = True
        Me.txtRoom.Location = New System.Drawing.Point(110, 83)
        Me.txtRoom.MaxLength = 0
        Me.txtRoom.Name = "txtRoom"
        Me.txtRoom.NumericAllowNegatives = False
        Me.txtRoom.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRoom.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRoom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRoom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRoom.Properties.Appearance.Options.UseFont = True
        Me.txtRoom.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRoom.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRoom.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRoom.Size = New System.Drawing.Size(365, 22)
        Me.txtRoom.TabIndex = 7
        Me.txtRoom.Tag = "R"
        Me.txtRoom.TextAlign = HorizontalAlignment.Left
        Me.txtRoom.ToolTipText = ""
        '
        'txtStaff
        '
        Me.txtStaff.CharacterCasing = CharacterCasing.Normal
        Me.txtStaff.EnterMoveNextControl = True
        Me.txtStaff.Location = New System.Drawing.Point(110, 27)
        Me.txtStaff.MaxLength = 0
        Me.txtStaff.Name = "txtStaff"
        Me.txtStaff.NumericAllowNegatives = False
        Me.txtStaff.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtStaff.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStaff.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtStaff.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtStaff.Properties.Appearance.Options.UseFont = True
        Me.txtStaff.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStaff.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtStaff.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtStaff.Size = New System.Drawing.Size(274, 22)
        Me.txtStaff.TabIndex = 1
        Me.txtStaff.Tag = "R"
        Me.txtStaff.TextAlign = HorizontalAlignment.Left
        Me.txtStaff.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(10, 58)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(51, 15)
        Me.CareLabel1.TabIndex = 3
        Me.CareLabel1.Text = "Shift Date"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label2.Location = New System.Drawing.Point(10, 30)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 15)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Staff Member"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDate
        '
        Me.txtDate.CharacterCasing = CharacterCasing.Normal
        Me.txtDate.EnterMoveNextControl = True
        Me.txtDate.Location = New System.Drawing.Point(110, 55)
        Me.txtDate.MaxLength = 0
        Me.txtDate.Name = "txtDate"
        Me.txtDate.NumericAllowNegatives = False
        Me.txtDate.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtDate.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtDate.Properties.Appearance.Options.UseFont = True
        Me.txtDate.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtDate.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDate.Size = New System.Drawing.Size(274, 22)
        Me.txtDate.TabIndex = 4
        Me.txtDate.Tag = "R"
        Me.txtDate.TextAlign = HorizontalAlignment.Left
        Me.txtDate.ToolTipText = ""
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.cgShifts)
        Me.GroupControl3.Location = New System.Drawing.Point(12, 132)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(485, 245)
        Me.GroupControl3.TabIndex = 1
        Me.GroupControl3.Text = "Staff Member Shifts"
        '
        'cgShifts
        '
        Me.cgShifts.AllowBuildColumns = True
        Me.cgShifts.AllowEdit = False
        Me.cgShifts.AllowHorizontalScroll = False
        Me.cgShifts.AllowMultiSelect = False
        Me.cgShifts.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgShifts.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgShifts.Appearance.Options.UseFont = True
        Me.cgShifts.AutoSizeByData = True
        Me.cgShifts.DisableAutoSize = False
        Me.cgShifts.DisableDataFormatting = False
        Me.cgShifts.FocusedRowHandle = -2147483648
        Me.cgShifts.HideFirstColumn = False
        Me.cgShifts.Location = New System.Drawing.Point(5, 23)
        Me.cgShifts.Name = "cgShifts"
        Me.cgShifts.PreviewColumn = ""
        Me.cgShifts.QueryID = Nothing
        Me.cgShifts.RowAutoHeight = True
        Me.cgShifts.SearchAsYouType = True
        Me.cgShifts.ShowAutoFilterRow = False
        Me.cgShifts.ShowFindPanel = False
        Me.cgShifts.ShowGroupByBox = False
        Me.cgShifts.ShowLoadingPanel = False
        Me.cgShifts.ShowNavigator = False
        Me.cgShifts.Size = New System.Drawing.Size(475, 217)
        Me.cgShifts.TabIndex = 0
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.cgNotRostered)
        Me.GroupControl2.Location = New System.Drawing.Point(503, 132)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(485, 245)
        Me.GroupControl2.TabIndex = 3
        Me.GroupControl2.Text = "Available Staff"
        '
        'cgNotRostered
        '
        Me.cgNotRostered.AllowBuildColumns = True
        Me.cgNotRostered.AllowEdit = False
        Me.cgNotRostered.AllowHorizontalScroll = False
        Me.cgNotRostered.AllowMultiSelect = False
        Me.cgNotRostered.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgNotRostered.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgNotRostered.Appearance.Options.UseFont = True
        Me.cgNotRostered.AutoSizeByData = True
        Me.cgNotRostered.DisableAutoSize = False
        Me.cgNotRostered.DisableDataFormatting = False
        Me.cgNotRostered.FocusedRowHandle = -2147483648
        Me.cgNotRostered.HideFirstColumn = False
        Me.cgNotRostered.Location = New System.Drawing.Point(5, 23)
        Me.cgNotRostered.Name = "cgNotRostered"
        Me.cgNotRostered.PreviewColumn = ""
        Me.cgNotRostered.QueryID = Nothing
        Me.cgNotRostered.RowAutoHeight = True
        Me.cgNotRostered.SearchAsYouType = True
        Me.cgNotRostered.ShowAutoFilterRow = False
        Me.cgNotRostered.ShowFindPanel = False
        Me.cgNotRostered.ShowGroupByBox = False
        Me.cgNotRostered.ShowLoadingPanel = False
        Me.cgNotRostered.ShowNavigator = False
        Me.cgNotRostered.Size = New System.Drawing.Size(475, 217)
        Me.cgNotRostered.TabIndex = 0
        '
        'frmRosterShiftNew
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(998, 415)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.btnShiftOK)
        Me.Controls.Add(Me.gbxShift)
        Me.Controls.Add(Me.btnShiftCancel)
        Me.Controls.Add(Me.GroupControl1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmRosterShiftNew"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = ""
        CType(Me.gbxShift, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxShift.ResumeLayout(False)
        Me.gbxShift.PerformLayout()
        CType(Me.txtShiftEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtShiftHours.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtShiftCost.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtShiftStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtRoom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStaff.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents gbxShift As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnShiftOK As Care.Controls.CareButton
    Friend WithEvents btnShiftCancel As Care.Controls.CareButton
    Friend WithEvents txtShiftEnd As Care.Controls.CareTextBox
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents txtShiftHours As Care.Controls.CareTextBox
    Friend WithEvents CareLabel16 As Care.Controls.CareLabel
    Friend WithEvents txtShiftCost As Care.Controls.CareTextBox
    Friend WithEvents CareLabel19 As Care.Controls.CareLabel
    Friend WithEvents CareLabel20 As Care.Controls.CareLabel
    Friend WithEvents txtShiftStart As Care.Controls.CareTextBox
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents txtRoom As Care.Controls.CareTextBox
    Friend WithEvents txtStaff As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents Label2 As Care.Controls.CareLabel
    Friend WithEvents txtDate As Care.Controls.CareTextBox
    Friend WithEvents btnSelect As Care.Controls.CareButton
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cgShifts As Care.Controls.CareGrid
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cgNotRostered As Care.Controls.CareGrid
    Friend WithEvents btnView As Care.Controls.CareButton
End Class
