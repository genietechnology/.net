﻿
Imports System.Windows.Forms
Imports Care.Global
Imports Care.Shared
Imports Care.Data

Public Class frmLeadStages

    Private m_Stage As Business.LeadStage
    Private m_LastControl As Control = Nothing
    Private m_LastPostition As Integer = -1

    Private Sub frmLeadStages_Load(sender As Object, e As EventArgs) Handles Me.Load

        Dim _Lead As New Business.Lead
        Dim _Fields As List(Of Field) = _Lead.FieldList(DataObjectBase.EnumNameFormat.MergeClassAndProperty)

        For Each _F In _Fields
            cbxFields.AddItem(_F.Name)
        Next

        ToggleEmail()

    End Sub

#Region "Overrides"

    Protected Overrides Sub FindRecord()

        Dim _SQL As String = ""
        _SQL += "select name as 'Name', action_start as 'Start', action_contact as 'Contact', action_pack as 'Pack',"
        _SQL += " action_viewing as 'Viewing', action_quote as 'Quote', action_list as 'List', action_close as 'Close',"
        _SQL += " email as 'Email Enabled', pcnt_complete as '% Complete'"
        _SQL += " from LeadStages"

        Dim _Find As New GenericFind
        With _Find
            .Caption = "Find Stage"
            .ParentForm = ParentForm
            .PopulateOnLoad = True
            .ConnectionString = Session.ConnectionString
            .GridSQL = _SQL
            .ReturnField = "ID"
            .GridOrderBy = " order by pcnt_complete"
            .FormWidth = 900
            .Show()
        End With

        If _Find.ReturnValue <> "" Then

            MyBase.RecordID = New Guid(_Find.ReturnValue)
            MyBase.RecordPopulated = True

            m_Stage = Business.LeadStage.RetreiveByID(New Guid(_Find.ReturnValue))
            bs.DataSource = m_Stage

        End If

        _Find = Nothing

    End Sub

    Protected Overrides Sub AfterAcceptChanges()
        MyBase.AfterCommitUpdate()
        txtEmailBody.ResetBackColor()
        txtEmailBody.Properties.ReadOnly = True
    End Sub

    Protected Overrides Sub AfterCancelChanges()
        MyBase.AfterCancelChanges()
        txtEmailBody.ResetBackColor()
        txtEmailBody.Properties.ReadOnly = True
    End Sub

    Protected Overrides Sub CommitUpdate()
        m_Stage = CType(bs.Item(bs.Position), Business.LeadStage)
        m_Stage.Store()
    End Sub

    Protected Overrides Sub SetBindings()

        m_Stage = New Business.LeadStage
        bs.DataSource = m_Stage

        txtName.DataBindings.Add("Text", bs, "_Name")
        txtPcntComplete.DataBindings.Add("Text", bs, "_PcntComplete")

        chkActionStart.DataBindings.Add("Checked", bs, "_ActionStart")
        chkActionContact.DataBindings.Add("Checked", bs, "_ActionContact")
        chkActionPack.DataBindings.Add("Checked", bs, "_ActionPack")
        chkActionViewing.DataBindings.Add("Checked", bs, "_ActionViewing")
        chkActionQuote.DataBindings.Add("Checked", bs, "_ActionQuote")
        chkActionList.DataBindings.Add("Checked", bs, "_ActionList")
        chkActionClose.DataBindings.Add("Checked", bs, "_ActionClose")

        chkEmail.DataBindings.Add("Checked", bs, "_Email")
        txtEmailSubject.DataBindings.Add("Text", bs, "_EmailSubject")
        txtEmailBody.DataBindings.Add("Text", bs, "_EmailBody")

    End Sub

    Protected Overrides Sub AfterAdd()
        ToggleEmail()
    End Sub

    Protected Overrides Sub AfterEdit()
        ToggleEmail()
    End Sub

    Protected Overrides Sub CommitDelete()
        Dim _SQL As String = "delete from LeadStages where ID = '" + m_Stage._ID.Value.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)
    End Sub

#End Region

    Private Sub chkEmail_CheckedChanged(sender As Object, e As EventArgs) Handles chkEmail.CheckedChanged
        ToggleEmail()
    End Sub

    Private Sub ToggleEmail()

        If chkEmail.Checked Then
            If Mode = "" Then Exit Sub
            txtEmailSubject.BackColor = Session.ChangeColour
            txtEmailSubject.ReadOnly = False
            txtEmailBody.BackColor = Session.ChangeColour
            txtEmailBody.Properties.ReadOnly = False
            cbxFields.BackColor = Session.ChangeColour
            cbxFields.Properties.ReadOnly = False
        Else
            txtEmailSubject.Text = ""
            txtEmailSubject.ResetBackColor()
            txtEmailSubject.ReadOnly = True
            txtEmailBody.Text = ""
            txtEmailBody.ResetBackColor()
            txtEmailBody.Properties.ReadOnly = True
            cbxFields.ResetBackColor()
            cbxFields.Properties.ReadOnly = True
        End If

    End Sub

    Private Sub txtEmailSubject_GotFocus(sender As Object, e As EventArgs) Handles txtEmailSubject.GotFocus
        m_LastControl = Nothing
        m_LastPostition = -1
    End Sub

    Private Sub txtEmailSubject_LostFocus(sender As Object, e As EventArgs) Handles txtEmailSubject.LostFocus
        m_LastControl = txtEmailSubject
        m_LastPostition = txtEmailSubject.SelectionStart
    End Sub

    Private Sub txtEmailBody_GotFocus(sender As Object, e As EventArgs) Handles txtEmailBody.GotFocus
        m_LastControl = Nothing
        m_LastPostition = -1
    End Sub

    Private Sub txtEmailBody_LostFocus(sender As Object, e As EventArgs) Handles txtEmailBody.LostFocus
        m_LastControl = txtEmailBody
        m_LastPostition = txtEmailBody.SelectionStart
    End Sub

    Private Sub cbxFields_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbxFields.SelectedValueChanged

        If m_LastControl IsNot Nothing AndAlso m_LastPostition >= 0 AndAlso cbxFields.SelectedIndex >= 0 Then
            m_LastControl.Text = m_LastControl.Text.Insert(m_LastPostition, cbxFields.Text)
            cbxFields.SelectedIndex = -1
        End If

    End Sub
End Class
