﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRiskHazard
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling4 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling5 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling2 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.cbxHazards = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.GroupBox1 = New Care.Controls.CareFrame()
        Me.txtActivity = New Care.Controls.CareTextBox(Me.components)
        Me.Label1 = New Care.Controls.CareLabel(Me.components)
        Me.txtControlMeasures = New DevExpress.XtraEditors.MemoEdit()
        Me.GroupControl5 = New Care.Controls.CareFrame()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.txtHazards = New DevExpress.XtraEditors.MemoEdit()
        Me.GroupControl2 = New Care.Controls.CareFrame()
        Me.txtPeople = New DevExpress.XtraEditors.MemoEdit()
        Me.cbxPeople = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.GroupControl3 = New Care.Controls.CareFrame()
        Me.txtRisks = New DevExpress.XtraEditors.MemoEdit()
        Me.cbxRisks = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.GroupControl4 = New Care.Controls.CareFrame()
        Me.rcResidual = New Nursery.RiskControl()
        Me.GroupControl6 = New Care.Controls.CareFrame()
        Me.rcUncontrolled = New Nursery.RiskControl()
        Me.btnOK = New Care.Controls.CareButton(Me.components)
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        CType(Me.cbxHazards.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtActivity.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtControlMeasures.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtHazards.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.txtPeople.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxPeople.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.txtRisks.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxRisks.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl6.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'cbxHazards
        '
        Me.cbxHazards.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxHazards.EditValue = ""
        Me.cbxHazards.Location = New System.Drawing.Point(8, 27)
        Me.cbxHazards.Name = "cbxHazards"
        Me.cbxHazards.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxHazards.Size = New System.Drawing.Size(284, 20)
        Me.cbxHazards.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtActivity)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.ShowCaption = False
        Me.GroupBox1.Size = New System.Drawing.Size(912, 40)
        Me.GroupBox1.TabIndex = 0
        '
        'txtActivity
        '
        Me.txtActivity.CharacterCasing = CharacterCasing.Normal
        Me.txtActivity.EnterMoveNextControl = True
        Me.txtActivity.Location = New System.Drawing.Point(86, 10)
        Me.txtActivity.MaxLength = 40
        Me.txtActivity.Name = "txtActivity"
        Me.txtActivity.NumericAllowNegatives = False
        Me.txtActivity.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtActivity.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtActivity.Properties.AccessibleName = "Site Name"
        Me.txtActivity.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtActivity.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtActivity.Properties.Appearance.Options.UseFont = True
        Me.txtActivity.Properties.Appearance.Options.UseTextOptions = True
        Me.txtActivity.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtActivity.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtActivity.Properties.MaxLength = 40
        Me.txtActivity.Size = New System.Drawing.Size(818, 22)
        Me.txtActivity.TabIndex = 1
        Me.txtActivity.Tag = "AEBM"
        Me.txtActivity.TextAlign = HorizontalAlignment.Left
        Me.txtActivity.ToolTipText = ""
        '
        'Label1
        '
        Me.Label1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label1.Location = New System.Drawing.Point(12, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Activity"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtControlMeasures
        '
        Me.txtControlMeasures.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtControlMeasures.Location = New System.Drawing.Point(8, 26)
        Me.txtControlMeasures.Name = "txtControlMeasures"
        Me.txtControlMeasures.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtControlMeasures.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtControlMeasures, True)
        Me.txtControlMeasures.Size = New System.Drawing.Size(720, 297)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtControlMeasures, OptionsSpelling4)
        Me.txtControlMeasures.TabIndex = 0
        Me.txtControlMeasures.Tag = ""
        '
        'GroupControl5
        '
        Me.GroupControl5.Controls.Add(Me.txtControlMeasures)
        Me.GroupControl5.Location = New System.Drawing.Point(12, 204)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(736, 330)
        Me.GroupControl5.TabIndex = 4
        Me.GroupControl5.Text = "Control Measures"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.txtHazards)
        Me.GroupControl1.Controls.Add(Me.cbxHazards)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 58)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(300, 140)
        Me.GroupControl1.TabIndex = 1
        Me.GroupControl1.Text = "What are the Hazards?"
        '
        'txtHazards
        '
        Me.txtHazards.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtHazards.Location = New System.Drawing.Point(8, 53)
        Me.txtHazards.Name = "txtHazards"
        Me.txtHazards.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHazards.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtHazards, True)
        Me.txtHazards.Size = New System.Drawing.Size(284, 79)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtHazards, OptionsSpelling5)
        Me.txtHazards.TabIndex = 1
        Me.txtHazards.Tag = "M"
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.txtPeople)
        Me.GroupControl2.Controls.Add(Me.cbxPeople)
        Me.GroupControl2.Location = New System.Drawing.Point(624, 58)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(300, 140)
        Me.GroupControl2.TabIndex = 3
        Me.GroupControl2.Text = "Who might be harmed?"
        '
        'txtPeople
        '
        Me.txtPeople.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtPeople.Location = New System.Drawing.Point(8, 53)
        Me.txtPeople.Name = "txtPeople"
        Me.txtPeople.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPeople.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtPeople, True)
        Me.txtPeople.Size = New System.Drawing.Size(284, 79)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtPeople, OptionsSpelling1)
        Me.txtPeople.TabIndex = 1
        Me.txtPeople.Tag = ""
        '
        'cbxPeople
        '
        Me.cbxPeople.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxPeople.EditValue = ""
        Me.cbxPeople.Location = New System.Drawing.Point(8, 27)
        Me.cbxPeople.Name = "cbxPeople"
        Me.cbxPeople.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxPeople.Size = New System.Drawing.Size(284, 20)
        Me.cbxPeople.TabIndex = 0
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.txtRisks)
        Me.GroupControl3.Controls.Add(Me.cbxRisks)
        Me.GroupControl3.Location = New System.Drawing.Point(318, 58)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(300, 140)
        Me.GroupControl3.TabIndex = 2
        Me.GroupControl3.Text = "How might they be harmed?"
        '
        'txtRisks
        '
        Me.txtRisks.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtRisks.Location = New System.Drawing.Point(8, 53)
        Me.txtRisks.Name = "txtRisks"
        Me.txtRisks.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRisks.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtRisks, True)
        Me.txtRisks.Size = New System.Drawing.Size(284, 79)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtRisks, OptionsSpelling2)
        Me.txtRisks.TabIndex = 1
        Me.txtRisks.Tag = ""
        '
        'cbxRisks
        '
        Me.cbxRisks.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxRisks.EditValue = ""
        Me.cbxRisks.Location = New System.Drawing.Point(8, 27)
        Me.cbxRisks.Name = "cbxRisks"
        Me.cbxRisks.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxRisks.Size = New System.Drawing.Size(284, 20)
        Me.cbxRisks.TabIndex = 0
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.rcResidual)
        Me.GroupControl4.Location = New System.Drawing.Point(754, 372)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(170, 162)
        Me.GroupControl4.TabIndex = 6
        Me.GroupControl4.Text = "Residual Risk Rating"
        '
        'rcResidual
        '
        Me.rcResidual.Likelihood = 0
        Me.rcResidual.Location = New System.Drawing.Point(5, 23)
        Me.rcResidual.MaximumSize = New System.Drawing.Size(162, 135)
        Me.rcResidual.MinimumSize = New System.Drawing.Size(162, 135)
        Me.rcResidual.Name = "rcResidual"
        Me.rcResidual.Severity = 0
        Me.rcResidual.Size = New System.Drawing.Size(162, 135)
        Me.rcResidual.TabIndex = 0
        '
        'GroupControl6
        '
        Me.GroupControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.GroupControl6.Controls.Add(Me.rcUncontrolled)
        Me.GroupControl6.Location = New System.Drawing.Point(754, 204)
        Me.GroupControl6.Name = "GroupControl6"
        Me.GroupControl6.Size = New System.Drawing.Size(170, 162)
        Me.GroupControl6.TabIndex = 5
        Me.GroupControl6.Text = "Uncontrolled Risk Rating"
        '
        'rcUncontrolled
        '
        Me.rcUncontrolled.Likelihood = 0
        Me.rcUncontrolled.Location = New System.Drawing.Point(5, 23)
        Me.rcUncontrolled.MaximumSize = New System.Drawing.Size(162, 135)
        Me.rcUncontrolled.MinimumSize = New System.Drawing.Size(162, 135)
        Me.rcUncontrolled.Name = "rcUncontrolled"
        Me.rcUncontrolled.Severity = 0
        Me.rcUncontrolled.Size = New System.Drawing.Size(162, 135)
        Me.rcUncontrolled.TabIndex = 0
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(748, 540)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(85, 25)
        Me.btnOK.TabIndex = 7
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.CausesValidation = False
        Me.btnCancel.DialogResult = DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(839, 540)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 25)
        Me.btnCancel.TabIndex = 8
        Me.btnCancel.Text = "Cancel"
        '
        'frmRiskHazard
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(934, 572)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.GroupControl6)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.GroupControl5)
        Me.Controls.Add(Me.GroupBox1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmRiskHazard"
        Me.StartPosition = FormStartPosition.CenterScreen
        CType(Me.cbxHazards.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.txtActivity.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtControlMeasures.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.txtHazards.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.txtPeople.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxPeople.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.txtRisks.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxRisks.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl6.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cbxHazards As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents GroupBox1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtActivity As Care.Controls.CareTextBox
    Friend WithEvents Label1 As Care.Controls.CareLabel
    Friend WithEvents txtControlMeasures As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtHazards As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtPeople As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents cbxPeople As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtRisks As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents cbxRisks As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents rcResidual As Nursery.RiskControl
    Friend WithEvents GroupControl6 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents rcUncontrolled As Nursery.RiskControl
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents btnCancel As Care.Controls.CareButton

End Class
