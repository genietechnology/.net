﻿Imports Care.Global
Imports Care.Data
Imports System.Windows.Forms

Public Class frmMeals

    Dim m_Meal As Business.Meal

#Region "Overrides"

    Protected Overrides Sub SetBindings()

        m_Meal = New Business.Meal
        bs.DataSource = m_Meal

        txtName.DataBindings.Add("Text", bs, "_Name")
        txtDescription.DataBindings.Add("Text", bs, "_description")
        chkBreakfast.DataBindings.Add("Checked", bs, "_breakfast")
        chkSnack.DataBindings.Add("Checked", bs, "_snack")
        chkLunch.DataBindings.Add("Checked", bs, "_lunch")
        chkTea.DataBindings.Add("Checked", bs, "_tea")
        chkDessert.DataBindings.Add("Checked", bs, "_dessert")

    End Sub

    Protected Overrides Sub CommitDelete()
        DAL.ExecuteSQL(Session.ConnectionString, "delete from Meals where id = '" & m_Meal._ID.Value.ToString & "'")
        DAL.ExecuteSQL(Session.ConnectionString, "delete from MealComponents where meal_id = '" & m_Meal._ID.Value.ToString & "'")
        ShowIngredients()
    End Sub

    Protected Overrides Function BeforeCommitUpdate() As Boolean
        If Not chkBreakfast.Checked And Not chkSnack.Checked And Not chkLunch.Checked And Not chkTea.Checked And Not chkDessert.Checked Then
            Msgbox("Please select a least one category for this meal.", MessageBoxIcon.Exclamation, "Select a Category")
            Return False
        Else
            Return True
        End If
    End Function

    Protected Overrides Sub CommitUpdate()
        m_Meal = CType(bs.Item(bs.Position), Business.Meal)
        Business.Meal.SaveRecord(m_Meal)
    End Sub

    Protected Overrides Sub AfterCommitUpdate()

        If Mode = "ADD" Then
            Find(m_Meal._ID.ToString)
            MyBase.ToolEdit()
            AddComponent()
        End If

    End Sub

    Protected Overrides Sub FindRecord()

        Dim _ReturnValue As String = Business.Meal.FindMeal(Me)
        If _ReturnValue <> "" Then
            Find(_ReturnValue)
        End If

    End Sub

    Private Sub Find(ByVal MealID As String)

        MyBase.RecordID = New Guid(MealID)
        MyBase.RecordPopulated = True

        m_Meal = Business.Meal.RetreiveByID(New Guid(MealID))
        bs.DataSource = m_Meal

        ShowIngredients()

    End Sub

    Protected Overrides Sub AfterAdd()
        btnAdd.Enabled = False
        btnEdit.Enabled = False
        btnRemove.Enabled = False
        GridIngredients.Clear()
    End Sub

    Protected Overrides Sub AfterEdit()
        btnAdd.Enabled = True
        btnEdit.Enabled = True
        btnRemove.Enabled = True
    End Sub

#End Region

    Private Sub ShowIngredients()

        gbxQty.Visible = False

        Dim _SQL As String = "select MealComponents.ID as 'ID', Food.ID as 'food_id', Food.name as 'food_name'," & _
                             " food.group_name as 'group_name', MealComponents.food_qty as 'qty' from MealComponents" & _
                             " left join Food on Food.ID = MealComponents.food_id" & _
                             " where MealComponents.meal_id = '" & m_Meal._ID.ToString & "'" & _
                             " order by Food.name"

        GridIngredients.Populate(Session.ConnectionString, _SQL)

        GridIngredients.Columns("ID").Visible = False
        GridIngredients.Columns("food_id").Visible = False
        GridIngredients.Columns("food_name").Caption = "Name"
        GridIngredients.Columns("group_name").Caption = "Group Name"
        GridIngredients.Columns("qty").Caption = "Qty"

        If GridIngredients.RecordCount > 0 Then
            GridIngredients.MoveFirst()
        End If

    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        AddComponent()
    End Sub

    Private Sub AddComponent()

        Dim _ID As String = Business.Food.FindFood(Me)
        If _ID = "" Then Exit Sub

        Dim _Food As Business.Food = Business.Food.RetreiveByID(New Guid(_ID))
        If Not _Food Is Nothing Then

            Dim _Component As New Business.MealComponent

            _Component._ID = Guid.NewGuid
            _Component._MealId = m_Meal._ID
            _Component._FoodId = _Food._ID
            _Component._FoodName = _Food._Name
            _Component._FoodQty = 1

            Business.MealComponent.SaveMealComponent(_Component)
            ShowIngredients()

        End If

        _Food = Nothing

    End Sub

    Private Sub ShowQuantity(ByVal Mode As String, ByVal Name As String, ByVal Qty As String)

        lblID.Text = Mode
        txtQtyName.Text = Name
        txtQty.Text = Qty

        gbxQty.Visible = True
        txtQty.Focus()

    End Sub

    Private Sub frmMeals_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gbxQty.Visible = False
    End Sub

    Private Sub btnQtyCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQtyCancel.Click
        gbxQty.Visible = False
    End Sub

    Private Sub SaveComponent()

        Dim _Component As New Business.MealComponent

        If lblID.Text = "ADD" Then
            _Component._ID = Guid.NewGuid
            _Component._MealId = m_Meal._ID
            _Component._FoodId = New Guid(txtQtyName.Tag.ToString)
            _Component._FoodName = txtQtyName.Text
            _Component._FoodQty = 1
        Else
            _Component = Business.MealComponent.RetreiveByID(New Guid(lblID.Text))
            _Component._FoodQty = Long.Parse(txtQty.Text)
        End If

        Business.MealComponent.SaveMealComponent(_Component)
        ShowIngredients()

    End Sub

    Private Sub btnQtyOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQtyOK.Click
        SaveComponent()
    End Sub

    Private Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click

        If GridIngredients.RecordCount < 1 Then Exit Sub
        If GridIngredients.CurrentRow("id").ToString <> "" Then
            Dim _id As New Guid(GridIngredients.CurrentRow("id").ToString)
            Business.MealComponent.DeletebyID(_id)
            ShowIngredients()
        End If

    End Sub

    Private Sub txtQty_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles txtQty.KeyDown
        If e.KeyCode = Keys.Enter Then
            SaveComponent()
        End If
    End Sub

    Private Sub txtName_LostFocus(sender As Object, e As System.EventArgs) Handles txtName.LostFocus
        If txtDescription.Text = "" Then txtDescription.Text = txtName.Text
    End Sub

End Class
