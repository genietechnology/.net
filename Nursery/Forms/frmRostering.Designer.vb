﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmRostering
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim TimeRuler1 As DevExpress.XtraScheduler.TimeRuler = New DevExpress.XtraScheduler.TimeRuler()
        Dim TimeRuler2 As DevExpress.XtraScheduler.TimeRuler = New DevExpress.XtraScheduler.TimeRuler()
        Dim TimeRuler3 As DevExpress.XtraScheduler.TimeRuler = New DevExpress.XtraScheduler.TimeRuler()
        Me.tabMain = New Care.Controls.CareTab(Me.components)
        Me.tabSummary = New DevExpress.XtraTab.XtraTabPage()
        Me.panMain = New DevExpress.XtraEditors.PanelControl()
        Me.tlpMain = New TableLayoutPanel()
        Me.dshCost = New Care.Controls.DashboardLabel()
        Me.dshHoliday = New Care.Controls.DashboardLabel()
        Me.dshShort = New Care.Controls.DashboardLabel()
        Me.dshRostered = New Care.Controls.DashboardLabel()
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.cgSummary = New Care.Controls.CareGrid()
        Me.cgDetail = New Care.Controls.CareGrid()
        Me.GroupControl2 = New Care.Controls.CareFrame(Me.components)
        Me.btnRefresh = New Care.Controls.CareButton(Me.components)
        Me.btnCommit = New Care.Controls.CareButton(Me.components)
        Me.btnStaffAdd = New Care.Controls.CareButton(Me.components)
        Me.btnShiftSummary = New Care.Controls.CareButton(Me.components)
        Me.btnSendShifts = New Care.Controls.CareButton(Me.components)
        Me.tabStaffSummary = New DevExpress.XtraTab.XtraTabPage()
        Me.SplitContainerControl2 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.cgStaff = New Care.Controls.CareGrid()
        Me.cgStaffShifts = New Care.Controls.CareGrid()
        Me.GroupControl1 = New Care.Controls.CareFrame(Me.components)
        Me.btnShifts = New Care.Controls.CareButton(Me.components)
        Me.btnStaffAmendShift = New Care.Controls.CareButton(Me.components)
        Me.btnStaffEmailShift = New Care.Controls.CareButton(Me.components)
        Me.btnStaffPrintShift = New Care.Controls.CareButton(Me.components)
        Me.tabCalendar = New DevExpress.XtraTab.XtraTabPage()
        Me.SchedulerControl1 = New DevExpress.XtraScheduler.SchedulerControl()
        Me.SchedulerStorage1 = New DevExpress.XtraScheduler.SchedulerStorage(Me.components)
        Me.tabDetail = New DevExpress.XtraTab.XtraTabPage()
        Me.SplitContainerControl3 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.cgTimeslotDate = New Care.Controls.CareGrid()
        Me.TableLayoutPanel1 = New TableLayoutPanel()
        Me.cgTimeSlotStaff = New Care.Controls.CareGrid()
        Me.cgTimeSlotChildren = New Care.Controls.CareGrid()
        Me.cgTimeSlotDetail = New Care.Controls.CareGrid()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.tabMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabMain.SuspendLayout()
        Me.tabSummary.SuspendLayout()
        CType(Me.panMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panMain.SuspendLayout()
        Me.tlpMain.SuspendLayout()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        Me.tabStaffSummary.SuspendLayout()
        CType(Me.SplitContainerControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl2.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        Me.tabCalendar.SuspendLayout()
        CType(Me.SchedulerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SchedulerStorage1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabDetail.SuspendLayout()
        CType(Me.SplitContainerControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl3.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(832, 3)
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(741, 3)
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(0, 525)
        Me.Panel1.Size = New System.Drawing.Size(929, 36)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'tabMain
        '
        Me.tabMain.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.tabMain.Location = New System.Drawing.Point(4, 52)
        Me.tabMain.Name = "tabMain"
        Me.tabMain.SelectedTabPage = Me.tabSummary
        Me.tabMain.Size = New System.Drawing.Size(913, 506)
        Me.tabMain.TabIndex = 7
        Me.tabMain.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabSummary, Me.tabStaffSummary, Me.tabCalendar, Me.tabDetail})
        '
        'tabSummary
        '
        Me.tabSummary.Controls.Add(Me.panMain)
        Me.tabSummary.Name = "tabSummary"
        Me.tabSummary.Size = New System.Drawing.Size(907, 478)
        Me.tabSummary.Text = "Summary"
        '
        'panMain
        '
        Me.panMain.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.panMain.Controls.Add(Me.tlpMain)
        Me.panMain.Location = New System.Drawing.Point(3, 3)
        Me.panMain.Name = "panMain"
        Me.panMain.Size = New System.Drawing.Size(901, 472)
        Me.panMain.TabIndex = 3
        '
        'tlpMain
        '
        Me.tlpMain.ColumnCount = 5
        Me.tlpMain.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 20.0!))
        Me.tlpMain.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 20.0!))
        Me.tlpMain.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 20.0!))
        Me.tlpMain.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 20.0!))
        Me.tlpMain.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 20.0!))
        Me.tlpMain.Controls.Add(Me.dshCost, 4, 3)
        Me.tlpMain.Controls.Add(Me.dshHoliday, 4, 2)
        Me.tlpMain.Controls.Add(Me.dshShort, 4, 1)
        Me.tlpMain.Controls.Add(Me.dshRostered, 4, 0)
        Me.tlpMain.Controls.Add(Me.SplitContainerControl1, 0, 0)
        Me.tlpMain.Dock = DockStyle.Fill
        Me.tlpMain.Location = New System.Drawing.Point(2, 2)
        Me.tlpMain.Name = "tlpMain"
        Me.tlpMain.RowCount = 4
        Me.tlpMain.RowStyles.Add(New RowStyle(SizeType.Percent, 20.0!))
        Me.tlpMain.RowStyles.Add(New RowStyle(SizeType.Percent, 20.0!))
        Me.tlpMain.RowStyles.Add(New RowStyle(SizeType.Percent, 20.0!))
        Me.tlpMain.RowStyles.Add(New RowStyle(SizeType.Percent, 20.0!))
        Me.tlpMain.RowStyles.Add(New RowStyle(SizeType.Absolute, 20.0!))
        Me.tlpMain.Size = New System.Drawing.Size(897, 468)
        Me.tlpMain.TabIndex = 2
        '
        'dshCost
        '
        Me.dshCost.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.dshCost.Dock = DockStyle.Fill
        Me.dshCost.LabelBottom = "£"
        Me.dshCost.LabelMiddle = "55"
        Me.dshCost.LabelTop = "Estimated Cost"
        Me.dshCost.Location = New System.Drawing.Point(719, 354)
        Me.dshCost.Name = "dshCost"
        Me.dshCost.Size = New System.Drawing.Size(175, 111)
        Me.dshCost.TabIndex = 9
        '
        'dshHoliday
        '
        Me.dshHoliday.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.dshHoliday.Dock = DockStyle.Fill
        Me.dshHoliday.LabelBottom = "Headcount"
        Me.dshHoliday.LabelMiddle = "55"
        Me.dshHoliday.LabelTop = "Holiday"
        Me.dshHoliday.Location = New System.Drawing.Point(719, 237)
        Me.dshHoliday.Name = "dshHoliday"
        Me.dshHoliday.Size = New System.Drawing.Size(175, 111)
        Me.dshHoliday.TabIndex = 10
        '
        'dshShort
        '
        Me.dshShort.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.dshShort.Dock = DockStyle.Fill
        Me.dshShort.LabelBottom = "Slots"
        Me.dshShort.LabelMiddle = "55"
        Me.dshShort.LabelTop = "Short"
        Me.dshShort.Location = New System.Drawing.Point(719, 120)
        Me.dshShort.Name = "dshShort"
        Me.dshShort.Size = New System.Drawing.Size(175, 111)
        Me.dshShort.TabIndex = 11
        '
        'dshRostered
        '
        Me.dshRostered.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.dshRostered.Dock = DockStyle.Fill
        Me.dshRostered.LabelBottom = "Headcount"
        Me.dshRostered.LabelMiddle = "55"
        Me.dshRostered.LabelTop = "Rostered"
        Me.dshRostered.Location = New System.Drawing.Point(719, 3)
        Me.dshRostered.Name = "dshRostered"
        Me.dshRostered.Size = New System.Drawing.Size(175, 111)
        Me.dshRostered.TabIndex = 12
        '
        'SplitContainerControl1
        '
        Me.tlpMain.SetColumnSpan(Me.SplitContainerControl1, 4)
        Me.SplitContainerControl1.Dock = DockStyle.Fill
        Me.SplitContainerControl1.Horizontal = False
        Me.SplitContainerControl1.Location = New System.Drawing.Point(3, 3)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.cgSummary)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.cgDetail)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.GroupControl2)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.tlpMain.SetRowSpan(Me.SplitContainerControl1, 4)
        Me.SplitContainerControl1.Size = New System.Drawing.Size(710, 462)
        Me.SplitContainerControl1.SplitterPosition = 156
        Me.SplitContainerControl1.TabIndex = 13
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'cgSummary
        '
        Me.cgSummary.AllowBuildColumns = True
        Me.cgSummary.AllowEdit = False
        Me.cgSummary.AllowHorizontalScroll = False
        Me.cgSummary.AllowMultiSelect = False
        Me.cgSummary.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgSummary.Appearance.Options.UseFont = True
        Me.cgSummary.AutoSizeByData = True
        Me.cgSummary.DisableAutoSize = False
        Me.cgSummary.DisableDataFormatting = False
        Me.cgSummary.Dock = DockStyle.Fill
        Me.cgSummary.FocusedRowHandle = -2147483648
        Me.cgSummary.HideFirstColumn = False
        Me.cgSummary.Location = New System.Drawing.Point(0, 0)
        Me.cgSummary.Name = "cgSummary"
        Me.cgSummary.PreviewColumn = ""
        Me.cgSummary.QueryID = Nothing
        Me.cgSummary.RowAutoHeight = False
        Me.cgSummary.SearchAsYouType = True
        Me.cgSummary.ShowAutoFilterRow = False
        Me.cgSummary.ShowFindPanel = False
        Me.cgSummary.ShowGroupByBox = False
        Me.cgSummary.ShowLoadingPanel = False
        Me.cgSummary.ShowNavigator = False
        Me.cgSummary.Size = New System.Drawing.Size(710, 156)
        Me.cgSummary.TabIndex = 14
        '
        'cgDetail
        '
        Me.cgDetail.AllowBuildColumns = True
        Me.cgDetail.AllowEdit = False
        Me.cgDetail.AllowHorizontalScroll = False
        Me.cgDetail.AllowMultiSelect = False
        Me.cgDetail.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgDetail.Appearance.Options.UseFont = True
        Me.cgDetail.AutoSizeByData = True
        Me.cgDetail.DisableAutoSize = False
        Me.cgDetail.DisableDataFormatting = False
        Me.cgDetail.Dock = DockStyle.Fill
        Me.cgDetail.FocusedRowHandle = -2147483648
        Me.cgDetail.HideFirstColumn = False
        Me.cgDetail.Location = New System.Drawing.Point(0, 0)
        Me.cgDetail.Name = "cgDetail"
        Me.cgDetail.PreviewColumn = ""
        Me.cgDetail.QueryID = Nothing
        Me.cgDetail.RowAutoHeight = False
        Me.cgDetail.SearchAsYouType = True
        Me.cgDetail.ShowAutoFilterRow = False
        Me.cgDetail.ShowFindPanel = False
        Me.cgDetail.ShowGroupByBox = False
        Me.cgDetail.ShowLoadingPanel = False
        Me.cgDetail.ShowNavigator = False
        Me.cgDetail.Size = New System.Drawing.Size(710, 266)
        Me.cgDetail.TabIndex = 15
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.btnRefresh)
        Me.GroupControl2.Controls.Add(Me.btnCommit)
        Me.GroupControl2.Controls.Add(Me.btnStaffAdd)
        Me.GroupControl2.Controls.Add(Me.btnShiftSummary)
        Me.GroupControl2.Controls.Add(Me.btnSendShifts)
        Me.GroupControl2.Dock = DockStyle.Bottom
        Me.GroupControl2.Location = New System.Drawing.Point(0, 266)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.ShowCaption = False
        Me.GroupControl2.Size = New System.Drawing.Size(710, 35)
        Me.GroupControl2.TabIndex = 18
        Me.GroupControl2.Text = "GroupControl2"
        '
        'btnRefresh
        '
        Me.btnRefresh.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnRefresh.Location = New System.Drawing.Point(580, 5)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(125, 25)
        Me.btnRefresh.TabIndex = 11
        Me.btnRefresh.Text = "Refresh"
        '
        'btnCommit
        '
        Me.btnCommit.Location = New System.Drawing.Point(136, 5)
        Me.btnCommit.Name = "btnCommit"
        Me.btnCommit.Size = New System.Drawing.Size(125, 25)
        Me.btnCommit.TabIndex = 10
        Me.btnCommit.Text = "Commit"
        '
        'btnStaffAdd
        '
        Me.btnStaffAdd.Location = New System.Drawing.Point(5, 5)
        Me.btnStaffAdd.Name = "btnStaffAdd"
        Me.btnStaffAdd.Size = New System.Drawing.Size(125, 25)
        Me.btnStaffAdd.TabIndex = 8
        Me.btnStaffAdd.Text = "Add Staff"
        '
        'btnShiftSummary
        '
        Me.btnShiftSummary.Location = New System.Drawing.Point(398, 5)
        Me.btnShiftSummary.Name = "btnShiftSummary"
        Me.btnShiftSummary.Size = New System.Drawing.Size(125, 25)
        Me.btnShiftSummary.TabIndex = 7
        Me.btnShiftSummary.Text = "Shift Summary"
        '
        'btnSendShifts
        '
        Me.btnSendShifts.Location = New System.Drawing.Point(267, 5)
        Me.btnSendShifts.Name = "btnSendShifts"
        Me.btnSendShifts.Size = New System.Drawing.Size(125, 25)
        Me.btnSendShifts.TabIndex = 6
        Me.btnSendShifts.Text = "Send Shifts"
        '
        'tabStaffSummary
        '
        Me.tabStaffSummary.Controls.Add(Me.SplitContainerControl2)
        Me.tabStaffSummary.Name = "tabStaffSummary"
        Me.tabStaffSummary.Size = New System.Drawing.Size(907, 478)
        Me.tabStaffSummary.Text = "Staff Summary"
        '
        'SplitContainerControl2
        '
        Me.SplitContainerControl2.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.SplitContainerControl2.Location = New System.Drawing.Point(3, 3)
        Me.SplitContainerControl2.Name = "SplitContainerControl2"
        Me.SplitContainerControl2.Panel1.Controls.Add(Me.cgStaff)
        Me.SplitContainerControl2.Panel1.Text = "Panel1"
        Me.SplitContainerControl2.Panel2.Controls.Add(Me.cgStaffShifts)
        Me.SplitContainerControl2.Panel2.Controls.Add(Me.GroupControl1)
        Me.SplitContainerControl2.Panel2.Text = "Panel2"
        Me.SplitContainerControl2.Size = New System.Drawing.Size(901, 472)
        Me.SplitContainerControl2.SplitterPosition = 195
        Me.SplitContainerControl2.TabIndex = 0
        Me.SplitContainerControl2.Text = "SplitContainerControl2"
        '
        'cgStaff
        '
        Me.cgStaff.AllowBuildColumns = True
        Me.cgStaff.AllowEdit = False
        Me.cgStaff.AllowHorizontalScroll = False
        Me.cgStaff.AllowMultiSelect = False
        Me.cgStaff.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgStaff.Appearance.Options.UseFont = True
        Me.cgStaff.AutoSizeByData = True
        Me.cgStaff.DisableAutoSize = False
        Me.cgStaff.DisableDataFormatting = False
        Me.cgStaff.Dock = DockStyle.Fill
        Me.cgStaff.FocusedRowHandle = -2147483648
        Me.cgStaff.HideFirstColumn = False
        Me.cgStaff.Location = New System.Drawing.Point(0, 0)
        Me.cgStaff.Name = "cgStaff"
        Me.cgStaff.PreviewColumn = ""
        Me.cgStaff.QueryID = Nothing
        Me.cgStaff.RowAutoHeight = False
        Me.cgStaff.SearchAsYouType = True
        Me.cgStaff.ShowAutoFilterRow = False
        Me.cgStaff.ShowFindPanel = False
        Me.cgStaff.ShowGroupByBox = False
        Me.cgStaff.ShowLoadingPanel = False
        Me.cgStaff.ShowNavigator = False
        Me.cgStaff.Size = New System.Drawing.Size(195, 472)
        Me.cgStaff.TabIndex = 16
        '
        'cgStaffShifts
        '
        Me.cgStaffShifts.AllowBuildColumns = True
        Me.cgStaffShifts.AllowEdit = False
        Me.cgStaffShifts.AllowHorizontalScroll = False
        Me.cgStaffShifts.AllowMultiSelect = False
        Me.cgStaffShifts.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgStaffShifts.Appearance.Options.UseFont = True
        Me.cgStaffShifts.AutoSizeByData = True
        Me.cgStaffShifts.DisableAutoSize = False
        Me.cgStaffShifts.DisableDataFormatting = False
        Me.cgStaffShifts.Dock = DockStyle.Fill
        Me.cgStaffShifts.FocusedRowHandle = -2147483648
        Me.cgStaffShifts.HideFirstColumn = False
        Me.cgStaffShifts.Location = New System.Drawing.Point(0, 35)
        Me.cgStaffShifts.Name = "cgStaffShifts"
        Me.cgStaffShifts.Padding = New Padding(0, 5, 0, 0)
        Me.cgStaffShifts.PreviewColumn = ""
        Me.cgStaffShifts.QueryID = Nothing
        Me.cgStaffShifts.RowAutoHeight = False
        Me.cgStaffShifts.SearchAsYouType = True
        Me.cgStaffShifts.ShowAutoFilterRow = False
        Me.cgStaffShifts.ShowFindPanel = False
        Me.cgStaffShifts.ShowGroupByBox = False
        Me.cgStaffShifts.ShowLoadingPanel = False
        Me.cgStaffShifts.ShowNavigator = False
        Me.cgStaffShifts.Size = New System.Drawing.Size(701, 437)
        Me.cgStaffShifts.TabIndex = 16
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.btnShifts)
        Me.GroupControl1.Controls.Add(Me.btnStaffAmendShift)
        Me.GroupControl1.Controls.Add(Me.btnStaffEmailShift)
        Me.GroupControl1.Controls.Add(Me.btnStaffPrintShift)
        Me.GroupControl1.Dock = DockStyle.Top
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(701, 35)
        Me.GroupControl1.TabIndex = 17
        Me.GroupControl1.Text = "GroupControl1"
        '
        'btnShifts
        '
        Me.btnShifts.Location = New System.Drawing.Point(136, 5)
        Me.btnShifts.Name = "btnShifts"
        Me.btnShifts.Size = New System.Drawing.Size(125, 25)
        Me.btnShifts.TabIndex = 10
        Me.btnShifts.Text = "Staff Preferences"
        '
        'btnStaffAmendShift
        '
        Me.btnStaffAmendShift.Location = New System.Drawing.Point(5, 5)
        Me.btnStaffAmendShift.Name = "btnStaffAmendShift"
        Me.btnStaffAmendShift.Size = New System.Drawing.Size(125, 25)
        Me.btnStaffAmendShift.TabIndex = 8
        Me.btnStaffAmendShift.Text = "Amend Shift"
        '
        'btnStaffEmailShift
        '
        Me.btnStaffEmailShift.Location = New System.Drawing.Point(398, 5)
        Me.btnStaffEmailShift.Name = "btnStaffEmailShift"
        Me.btnStaffEmailShift.Size = New System.Drawing.Size(125, 25)
        Me.btnStaffEmailShift.TabIndex = 7
        Me.btnStaffEmailShift.Text = "Email Shift Summary"
        '
        'btnStaffPrintShift
        '
        Me.btnStaffPrintShift.Location = New System.Drawing.Point(267, 5)
        Me.btnStaffPrintShift.Name = "btnStaffPrintShift"
        Me.btnStaffPrintShift.Size = New System.Drawing.Size(125, 25)
        Me.btnStaffPrintShift.TabIndex = 6
        Me.btnStaffPrintShift.Text = "Print Shift Summary"
        '
        'tabCalendar
        '
        Me.tabCalendar.Controls.Add(Me.SchedulerControl1)
        Me.tabCalendar.Name = "tabCalendar"
        Me.tabCalendar.Size = New System.Drawing.Size(907, 478)
        Me.tabCalendar.Text = "Room Calendar"
        '
        'SchedulerControl1
        '
        Me.SchedulerControl1.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.SchedulerControl1.Location = New System.Drawing.Point(8, 7)
        Me.SchedulerControl1.Name = "SchedulerControl1"
        Me.SchedulerControl1.Size = New System.Drawing.Size(892, 463)
        Me.SchedulerControl1.Start = New Date(2017, 5, 1, 0, 0, 0, 0)
        Me.SchedulerControl1.Storage = Me.SchedulerStorage1
        Me.SchedulerControl1.TabIndex = 0
        Me.SchedulerControl1.Text = "SchedulerControl1"
        Me.SchedulerControl1.Views.DayView.TimeRulers.Add(TimeRuler1)
        Me.SchedulerControl1.Views.FullWeekView.Enabled = True
        Me.SchedulerControl1.Views.FullWeekView.TimeRulers.Add(TimeRuler2)
        Me.SchedulerControl1.Views.WeekView.Enabled = False
        Me.SchedulerControl1.Views.WorkWeekView.TimeRulers.Add(TimeRuler3)
        '
        'tabDetail
        '
        Me.tabDetail.Controls.Add(Me.SplitContainerControl3)
        Me.tabDetail.Name = "tabDetail"
        Me.tabDetail.Size = New System.Drawing.Size(907, 478)
        Me.tabDetail.Text = "Timeslot Detail"
        '
        'SplitContainerControl3
        '
        Me.SplitContainerControl3.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.SplitContainerControl3.Location = New System.Drawing.Point(3, 3)
        Me.SplitContainerControl3.Name = "SplitContainerControl3"
        Me.SplitContainerControl3.Panel1.Controls.Add(Me.cgTimeslotDate)
        Me.SplitContainerControl3.Panel1.Text = "Panel1"
        Me.SplitContainerControl3.Panel2.Controls.Add(Me.TableLayoutPanel1)
        Me.SplitContainerControl3.Panel2.Text = "Panel2"
        Me.SplitContainerControl3.Size = New System.Drawing.Size(901, 472)
        Me.SplitContainerControl3.SplitterPosition = 251
        Me.SplitContainerControl3.TabIndex = 1
        Me.SplitContainerControl3.Text = "SplitContainerControl3"
        '
        'cgTimeslotDate
        '
        Me.cgTimeslotDate.AllowBuildColumns = True
        Me.cgTimeslotDate.AllowEdit = False
        Me.cgTimeslotDate.AllowHorizontalScroll = False
        Me.cgTimeslotDate.AllowMultiSelect = False
        Me.cgTimeslotDate.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgTimeslotDate.Appearance.Options.UseFont = True
        Me.cgTimeslotDate.AutoSizeByData = True
        Me.cgTimeslotDate.DisableAutoSize = False
        Me.cgTimeslotDate.DisableDataFormatting = False
        Me.cgTimeslotDate.Dock = DockStyle.Fill
        Me.cgTimeslotDate.FocusedRowHandle = -2147483648
        Me.cgTimeslotDate.HideFirstColumn = False
        Me.cgTimeslotDate.Location = New System.Drawing.Point(0, 0)
        Me.cgTimeslotDate.Name = "cgTimeslotDate"
        Me.cgTimeslotDate.PreviewColumn = ""
        Me.cgTimeslotDate.QueryID = Nothing
        Me.cgTimeslotDate.RowAutoHeight = False
        Me.cgTimeslotDate.SearchAsYouType = True
        Me.cgTimeslotDate.ShowAutoFilterRow = False
        Me.cgTimeslotDate.ShowFindPanel = False
        Me.cgTimeslotDate.ShowGroupByBox = False
        Me.cgTimeslotDate.ShowLoadingPanel = False
        Me.cgTimeslotDate.ShowNavigator = False
        Me.cgTimeslotDate.Size = New System.Drawing.Size(251, 472)
        Me.cgTimeslotDate.TabIndex = 16
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 5
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.cgTimeSlotStaff, 4, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.cgTimeSlotChildren, 3, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.cgTimeSlotDetail, 0, 0)
        Me.TableLayoutPanel1.Dock = DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New RowStyle(SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New RowStyle(SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New RowStyle(SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(645, 472)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'cgTimeSlotStaff
        '
        Me.cgTimeSlotStaff.AllowBuildColumns = True
        Me.cgTimeSlotStaff.AllowEdit = False
        Me.cgTimeSlotStaff.AllowHorizontalScroll = False
        Me.cgTimeSlotStaff.AllowMultiSelect = False
        Me.cgTimeSlotStaff.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgTimeSlotStaff.Appearance.Options.UseFont = True
        Me.cgTimeSlotStaff.AutoSizeByData = True
        Me.cgTimeSlotStaff.DisableAutoSize = False
        Me.cgTimeSlotStaff.DisableDataFormatting = False
        Me.cgTimeSlotStaff.Dock = DockStyle.Fill
        Me.cgTimeSlotStaff.FocusedRowHandle = -2147483648
        Me.cgTimeSlotStaff.HideFirstColumn = False
        Me.cgTimeSlotStaff.Location = New System.Drawing.Point(519, 3)
        Me.cgTimeSlotStaff.Name = "cgTimeSlotStaff"
        Me.cgTimeSlotStaff.PreviewColumn = ""
        Me.cgTimeSlotStaff.QueryID = Nothing
        Me.cgTimeSlotStaff.RowAutoHeight = False
        Me.cgTimeSlotStaff.SearchAsYouType = True
        Me.cgTimeSlotStaff.ShowAutoFilterRow = False
        Me.cgTimeSlotStaff.ShowFindPanel = False
        Me.cgTimeSlotStaff.ShowGroupByBox = False
        Me.cgTimeSlotStaff.ShowLoadingPanel = False
        Me.cgTimeSlotStaff.ShowNavigator = False
        Me.cgTimeSlotStaff.Size = New System.Drawing.Size(123, 466)
        Me.cgTimeSlotStaff.TabIndex = 19
        '
        'cgTimeSlotChildren
        '
        Me.cgTimeSlotChildren.AllowBuildColumns = True
        Me.cgTimeSlotChildren.AllowEdit = False
        Me.cgTimeSlotChildren.AllowHorizontalScroll = False
        Me.cgTimeSlotChildren.AllowMultiSelect = False
        Me.cgTimeSlotChildren.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgTimeSlotChildren.Appearance.Options.UseFont = True
        Me.cgTimeSlotChildren.AutoSizeByData = True
        Me.cgTimeSlotChildren.DisableAutoSize = False
        Me.cgTimeSlotChildren.DisableDataFormatting = False
        Me.cgTimeSlotChildren.Dock = DockStyle.Fill
        Me.cgTimeSlotChildren.FocusedRowHandle = -2147483648
        Me.cgTimeSlotChildren.HideFirstColumn = False
        Me.cgTimeSlotChildren.Location = New System.Drawing.Point(390, 3)
        Me.cgTimeSlotChildren.Name = "cgTimeSlotChildren"
        Me.cgTimeSlotChildren.PreviewColumn = ""
        Me.cgTimeSlotChildren.QueryID = Nothing
        Me.cgTimeSlotChildren.RowAutoHeight = False
        Me.cgTimeSlotChildren.SearchAsYouType = True
        Me.cgTimeSlotChildren.ShowAutoFilterRow = False
        Me.cgTimeSlotChildren.ShowFindPanel = False
        Me.cgTimeSlotChildren.ShowGroupByBox = False
        Me.cgTimeSlotChildren.ShowLoadingPanel = False
        Me.cgTimeSlotChildren.ShowNavigator = False
        Me.cgTimeSlotChildren.Size = New System.Drawing.Size(123, 466)
        Me.cgTimeSlotChildren.TabIndex = 18
        '
        'cgTimeSlotDetail
        '
        Me.cgTimeSlotDetail.AllowBuildColumns = True
        Me.cgTimeSlotDetail.AllowEdit = False
        Me.cgTimeSlotDetail.AllowHorizontalScroll = False
        Me.cgTimeSlotDetail.AllowMultiSelect = False
        Me.cgTimeSlotDetail.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgTimeSlotDetail.Appearance.Options.UseFont = True
        Me.cgTimeSlotDetail.AutoSizeByData = True
        Me.TableLayoutPanel1.SetColumnSpan(Me.cgTimeSlotDetail, 3)
        Me.cgTimeSlotDetail.DisableAutoSize = False
        Me.cgTimeSlotDetail.DisableDataFormatting = False
        Me.cgTimeSlotDetail.Dock = DockStyle.Fill
        Me.cgTimeSlotDetail.FocusedRowHandle = -2147483648
        Me.cgTimeSlotDetail.HideFirstColumn = False
        Me.cgTimeSlotDetail.Location = New System.Drawing.Point(3, 3)
        Me.cgTimeSlotDetail.Name = "cgTimeSlotDetail"
        Me.cgTimeSlotDetail.PreviewColumn = ""
        Me.cgTimeSlotDetail.QueryID = Nothing
        Me.cgTimeSlotDetail.RowAutoHeight = False
        Me.cgTimeSlotDetail.SearchAsYouType = True
        Me.cgTimeSlotDetail.ShowAutoFilterRow = False
        Me.cgTimeSlotDetail.ShowFindPanel = False
        Me.cgTimeSlotDetail.ShowGroupByBox = False
        Me.cgTimeSlotDetail.ShowLoadingPanel = False
        Me.cgTimeSlotDetail.ShowNavigator = False
        Me.cgTimeSlotDetail.Size = New System.Drawing.Size(381, 466)
        Me.cgTimeSlotDetail.TabIndex = 17
        '
        'frmRostering
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(929, 561)
        Me.Controls.Add(Me.tabMain)
        Me.FormBorderStyle = FormBorderStyle.Sizable
        Me.MaximizeBox = True
        Me.Name = "frmRostering"
        Me.WindowState = FormWindowState.Maximized
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.tabMain, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.tabMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabMain.ResumeLayout(False)
        Me.tabMain.PerformLayout()
        Me.tabSummary.ResumeLayout(False)
        CType(Me.panMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panMain.ResumeLayout(False)
        Me.tlpMain.ResumeLayout(False)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        Me.tabStaffSummary.ResumeLayout(False)
        CType(Me.SplitContainerControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl2.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        Me.tabCalendar.ResumeLayout(False)
        CType(Me.SchedulerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SchedulerStorage1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabDetail.ResumeLayout(False)
        CType(Me.SplitContainerControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl3.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tabMain As Care.Controls.CareTab
    Friend WithEvents tabSummary As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabCalendar As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents SchedulerControl1 As DevExpress.XtraScheduler.SchedulerControl
    Friend WithEvents SchedulerStorage1 As DevExpress.XtraScheduler.SchedulerStorage
    Friend WithEvents panMain As DevExpress.XtraEditors.PanelControl
    Friend WithEvents tlpMain As TableLayoutPanel
    Friend WithEvents dshCost As Care.Controls.DashboardLabel
    Friend WithEvents dshHoliday As Care.Controls.DashboardLabel
    Friend WithEvents dshRostered As Care.Controls.DashboardLabel
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents cgSummary As Care.Controls.CareGrid
    Friend WithEvents cgDetail As Care.Controls.CareGrid
    Friend WithEvents tabStaffSummary As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents SplitContainerControl2 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents cgStaff As Care.Controls.CareGrid
    Friend WithEvents cgStaffShifts As Care.Controls.CareGrid
    Friend WithEvents tabDetail As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents SplitContainerControl3 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents cgTimeslotDate As Care.Controls.CareGrid
    Friend WithEvents btnStaffAmendShift As Care.Controls.CareButton
    Friend WithEvents btnStaffEmailShift As Care.Controls.CareButton
    Friend WithEvents btnStaffPrintShift As Care.Controls.CareButton
    Friend WithEvents btnShifts As Care.Controls.CareButton
    Friend WithEvents btnCommit As Care.Controls.CareButton
    Friend WithEvents btnStaffAdd As Care.Controls.CareButton
    Friend WithEvents btnShiftSummary As Care.Controls.CareButton
    Friend WithEvents btnSendShifts As Care.Controls.CareButton
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents cgTimeSlotStaff As Care.Controls.CareGrid
    Friend WithEvents cgTimeSlotChildren As Care.Controls.CareGrid
    Friend WithEvents cgTimeSlotDetail As Care.Controls.CareGrid
    Friend WithEvents btnRefresh As Care.Controls.CareButton
    Friend WithEvents GroupControl1 As Care.Controls.CareFrame
    Friend WithEvents GroupControl2 As Care.Controls.CareFrame
    Friend WithEvents dshShort As Care.Controls.DashboardLabel
End Class
