﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmAnnualCalcs
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Panel1 = New Panel()
        Me.btnSend = New Care.Controls.CareButton(Me.components)
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.btnNew = New Care.Controls.CareButton(Me.components)
        Me.btnInvoice = New Care.Controls.CareButton(Me.components)
        Me.btnCalc = New Care.Controls.CareButton(Me.components)
        Me.cgAnnualInvoices = New Care.Controls.CareGrid()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.cbxSite = New Care.Controls.CareComboBox(Me.components)
        Me.gbxNew = New Care.Controls.CareFrame()
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.txtMonths = New Care.Controls.CareTextBox(Me.components)
        Me.cbxGeneration = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.btnRun = New Care.Controls.CareButton(Me.components)
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.cdtInvoiceDue = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.cdtInvoiceDate = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.cdtDateTo = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.cdtDateFrom = New Care.Controls.CareDateTime(Me.components)
        Me.Panel1.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxNew, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxNew.SuspendLayout()
        CType(Me.txtMonths.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxGeneration.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtInvoiceDue.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtInvoiceDue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtInvoiceDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtInvoiceDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDateTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDateTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDateFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDateFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnSend)
        Me.Panel1.Controls.Add(Me.btnClose)
        Me.Panel1.Controls.Add(Me.btnNew)
        Me.Panel1.Controls.Add(Me.btnInvoice)
        Me.Panel1.Controls.Add(Me.btnCalc)
        Me.Panel1.Dock = DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 425)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(884, 36)
        Me.Panel1.TabIndex = 3
        '
        'btnSend
        '
        Me.btnSend.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSend.Appearance.Options.UseFont = True
        Me.btnSend.CausesValidation = False
        Me.btnSend.Location = New System.Drawing.Point(465, 3)
        Me.btnSend.Name = "btnSend"
        Me.btnSend.Size = New System.Drawing.Size(145, 25)
        Me.btnSend.TabIndex = 3
        Me.btnSend.Tag = ""
        Me.btnSend.Text = "Send Annual Invoices"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.CausesValidation = False
        Me.btnClose.DialogResult = DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(727, 3)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(145, 25)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Tag = ""
        Me.btnClose.Text = "Close"
        '
        'btnNew
        '
        Me.btnNew.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnNew.Appearance.Options.UseFont = True
        Me.btnNew.CausesValidation = False
        Me.btnNew.Location = New System.Drawing.Point(12, 3)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(145, 25)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Tag = ""
        Me.btnNew.Text = "Create New Calculations"
        '
        'btnInvoice
        '
        Me.btnInvoice.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnInvoice.Appearance.Options.UseFont = True
        Me.btnInvoice.CausesValidation = False
        Me.btnInvoice.Location = New System.Drawing.Point(314, 3)
        Me.btnInvoice.Name = "btnInvoice"
        Me.btnInvoice.Size = New System.Drawing.Size(145, 25)
        Me.btnInvoice.TabIndex = 2
        Me.btnInvoice.Tag = ""
        Me.btnInvoice.Text = "View Annual Invoice"
        '
        'btnCalc
        '
        Me.btnCalc.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCalc.Appearance.Options.UseFont = True
        Me.btnCalc.CausesValidation = False
        Me.btnCalc.Location = New System.Drawing.Point(163, 3)
        Me.btnCalc.Name = "btnCalc"
        Me.btnCalc.Size = New System.Drawing.Size(145, 25)
        Me.btnCalc.TabIndex = 1
        Me.btnCalc.Tag = ""
        Me.btnCalc.Text = "View Calculation"
        '
        'cgAnnualInvoices
        '
        Me.cgAnnualInvoices.AllowBuildColumns = True
        Me.cgAnnualInvoices.AllowEdit = False
        Me.cgAnnualInvoices.AllowHorizontalScroll = False
        Me.cgAnnualInvoices.AllowMultiSelect = True
        Me.cgAnnualInvoices.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgAnnualInvoices.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgAnnualInvoices.Appearance.Options.UseFont = True
        Me.cgAnnualInvoices.AutoSizeByData = True
        Me.cgAnnualInvoices.DisableAutoSize = False
        Me.cgAnnualInvoices.DisableDataFormatting = False
        Me.cgAnnualInvoices.FocusedRowHandle = -2147483648
        Me.cgAnnualInvoices.HideFirstColumn = False
        Me.cgAnnualInvoices.Location = New System.Drawing.Point(12, 57)
        Me.cgAnnualInvoices.Name = "cgAnnualInvoices"
        Me.cgAnnualInvoices.PreviewColumn = ""
        Me.cgAnnualInvoices.QueryID = Nothing
        Me.cgAnnualInvoices.RowAutoHeight = False
        Me.cgAnnualInvoices.SearchAsYouType = True
        Me.cgAnnualInvoices.ShowAutoFilterRow = False
        Me.cgAnnualInvoices.ShowFindPanel = True
        Me.cgAnnualInvoices.ShowGroupByBox = False
        Me.cgAnnualInvoices.ShowLoadingPanel = False
        Me.cgAnnualInvoices.ShowNavigator = True
        Me.cgAnnualInvoices.Size = New System.Drawing.Size(860, 362)
        Me.cgAnnualInvoices.TabIndex = 1
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Controls.Add(Me.cbxSite)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(860, 39)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Grid Options"
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(12, 12)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(19, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Site"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(48, 9)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AccessibleName = "Gender"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(220, 22)
        Me.cbxSite.TabIndex = 1
        Me.cbxSite.Tag = ""
        Me.cbxSite.ValueMember = Nothing
        '
        'gbxNew
        '
        Me.gbxNew.Anchor = AnchorStyles.None
        Me.gbxNew.Controls.Add(Me.CareLabel4)
        Me.gbxNew.Controls.Add(Me.txtMonths)
        Me.gbxNew.Controls.Add(Me.cbxGeneration)
        Me.gbxNew.Controls.Add(Me.CareLabel2)
        Me.gbxNew.Controls.Add(Me.btnRun)
        Me.gbxNew.Controls.Add(Me.btnCancel)
        Me.gbxNew.Controls.Add(Me.CareLabel8)
        Me.gbxNew.Controls.Add(Me.cdtInvoiceDue)
        Me.gbxNew.Controls.Add(Me.CareLabel6)
        Me.gbxNew.Controls.Add(Me.cdtInvoiceDate)
        Me.gbxNew.Controls.Add(Me.CareLabel3)
        Me.gbxNew.Controls.Add(Me.cdtDateTo)
        Me.gbxNew.Controls.Add(Me.CareLabel11)
        Me.gbxNew.Controls.Add(Me.cdtDateFrom)
        Me.gbxNew.Location = New System.Drawing.Point(247, 160)
        Me.gbxNew.Name = "gbxNew"
        Me.gbxNew.Size = New System.Drawing.Size(391, 176)
        Me.gbxNew.TabIndex = 2
        Me.gbxNew.Text = "Create New Annual Calculations"
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(12, 88)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(81, 15)
        Me.CareLabel4.TabIndex = 15
        Me.CareLabel4.Text = "Months to Split"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMonths
        '
        Me.txtMonths.CharacterCasing = CharacterCasing.Normal
        Me.txtMonths.EnterMoveNextControl = True
        Me.txtMonths.Location = New System.Drawing.Point(124, 85)
        Me.txtMonths.MaxLength = 30
        Me.txtMonths.Name = "txtMonths"
        Me.txtMonths.NumericAllowNegatives = False
        Me.txtMonths.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtMonths.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtMonths.Properties.AccessibleDescription = ""
        Me.txtMonths.Properties.AccessibleName = "Forename"
        Me.txtMonths.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtMonths.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtMonths.Properties.Appearance.Options.UseFont = True
        Me.txtMonths.Properties.Appearance.Options.UseTextOptions = True
        Me.txtMonths.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtMonths.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtMonths.Properties.MaxLength = 30
        Me.txtMonths.Properties.ReadOnly = True
        Me.txtMonths.Size = New System.Drawing.Size(100, 22)
        Me.txtMonths.TabIndex = 14
        Me.txtMonths.Tag = ""
        Me.txtMonths.TextAlign = HorizontalAlignment.Left
        Me.txtMonths.ToolTipText = ""
        '
        'cbxGeneration
        '
        Me.cbxGeneration.AllowBlank = False
        Me.cbxGeneration.DataSource = Nothing
        Me.cbxGeneration.DisplayMember = Nothing
        Me.cbxGeneration.EnterMoveNextControl = True
        Me.cbxGeneration.Location = New System.Drawing.Point(124, 29)
        Me.cbxGeneration.Name = "cbxGeneration"
        Me.cbxGeneration.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxGeneration.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxGeneration.Properties.Appearance.Options.UseFont = True
        Me.cbxGeneration.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxGeneration.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxGeneration.SelectedValue = Nothing
        Me.cbxGeneration.Size = New System.Drawing.Size(257, 22)
        Me.cbxGeneration.TabIndex = 1
        Me.cbxGeneration.Tag = "AEM"
        Me.cbxGeneration.ValueMember = Nothing
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(12, 32)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(92, 15)
        Me.CareLabel2.TabIndex = 0
        Me.CareLabel2.Text = "Generation Mode"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnRun
        '
        Me.btnRun.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnRun.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnRun.Appearance.Options.UseFont = True
        Me.btnRun.Location = New System.Drawing.Point(205, 147)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(85, 22)
        Me.btnRun.TabIndex = 12
        Me.btnRun.Text = "Run"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(296, 147)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 22)
        Me.btnCancel.TabIndex = 13
        Me.btnCancel.Text = "Cancel"
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(254, 116)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(21, 15)
        Me.CareLabel8.TabIndex = 10
        Me.CareLabel8.Text = "Due"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtInvoiceDue
        '
        Me.cdtInvoiceDue.EditValue = Nothing
        Me.cdtInvoiceDue.EnterMoveNextControl = True
        Me.cdtInvoiceDue.Location = New System.Drawing.Point(281, 113)
        Me.cdtInvoiceDue.Name = "cdtInvoiceDue"
        Me.cdtInvoiceDue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtInvoiceDue.Properties.Appearance.Options.UseFont = True
        Me.cdtInvoiceDue.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtInvoiceDue.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtInvoiceDue.Size = New System.Drawing.Size(100, 22)
        Me.cdtInvoiceDue.TabIndex = 11
        Me.cdtInvoiceDue.Tag = "AEM"
        Me.cdtInvoiceDue.Value = Nothing
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(12, 116)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(65, 15)
        Me.CareLabel6.TabIndex = 8
        Me.CareLabel6.Text = "Invoice Date"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtInvoiceDate
        '
        Me.cdtInvoiceDate.EditValue = Nothing
        Me.cdtInvoiceDate.EnterMoveNextControl = True
        Me.cdtInvoiceDate.Location = New System.Drawing.Point(124, 113)
        Me.cdtInvoiceDate.Name = "cdtInvoiceDate"
        Me.cdtInvoiceDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtInvoiceDate.Properties.Appearance.Options.UseFont = True
        Me.cdtInvoiceDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtInvoiceDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtInvoiceDate.Size = New System.Drawing.Size(100, 22)
        Me.cdtInvoiceDate.TabIndex = 9
        Me.cdtInvoiceDate.Tag = "AEM"
        Me.cdtInvoiceDate.Value = Nothing
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(261, 60)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(14, 15)
        Me.CareLabel3.TabIndex = 6
        Me.CareLabel3.Text = "To"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtDateTo
        '
        Me.cdtDateTo.EditValue = Nothing
        Me.cdtDateTo.EnterMoveNextControl = True
        Me.cdtDateTo.Location = New System.Drawing.Point(281, 57)
        Me.cdtDateTo.Name = "cdtDateTo"
        Me.cdtDateTo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtDateTo.Properties.Appearance.Options.UseFont = True
        Me.cdtDateTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDateTo.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtDateTo.Size = New System.Drawing.Size(100, 22)
        Me.cdtDateTo.TabIndex = 7
        Me.cdtDateTo.Tag = "AEM"
        Me.cdtDateTo.Value = Nothing
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(12, 60)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(28, 15)
        Me.CareLabel11.TabIndex = 4
        Me.CareLabel11.Text = "From"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtDateFrom
        '
        Me.cdtDateFrom.EditValue = Nothing
        Me.cdtDateFrom.EnterMoveNextControl = True
        Me.cdtDateFrom.Location = New System.Drawing.Point(124, 57)
        Me.cdtDateFrom.Name = "cdtDateFrom"
        Me.cdtDateFrom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtDateFrom.Properties.Appearance.Options.UseFont = True
        Me.cdtDateFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDateFrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtDateFrom.Size = New System.Drawing.Size(100, 22)
        Me.cdtDateFrom.TabIndex = 5
        Me.cdtDateFrom.Tag = "AEM"
        Me.cdtDateFrom.Value = Nothing
        '
        'frmAnnualCalcs
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(884, 461)
        Me.Controls.Add(Me.gbxNew)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.cgAnnualInvoices)
        Me.Controls.Add(Me.GroupControl1)
        Me.FormBorderStyle = FormBorderStyle.Sizable
        Me.Name = "frmAnnualCalcs"
        Me.WindowState = FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxNew, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxNew.ResumeLayout(False)
        Me.gbxNew.PerformLayout()
        CType(Me.txtMonths.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxGeneration.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtInvoiceDue.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtInvoiceDue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtInvoiceDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtInvoiceDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDateTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDateTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDateFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDateFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btnSend As Care.Controls.CareButton
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents btnNew As Care.Controls.CareButton
    Friend WithEvents btnInvoice As Care.Controls.CareButton
    Friend WithEvents btnCalc As Care.Controls.CareButton
    Friend WithEvents cgAnnualInvoices As Care.Controls.CareGrid
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
    Friend WithEvents gbxNew As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnRun As Care.Controls.CareButton
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents cdtInvoiceDue As Care.Controls.CareDateTime
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents cdtInvoiceDate As Care.Controls.CareDateTime
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents cdtDateTo As Care.Controls.CareDateTime
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents cdtDateFrom As Care.Controls.CareDateTime
    Friend WithEvents cbxGeneration As Care.Controls.CareComboBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents txtMonths As Care.Controls.CareTextBox

End Class
