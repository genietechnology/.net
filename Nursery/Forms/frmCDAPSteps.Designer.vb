﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCDAPSteps

    Inherits Care.Shared.frmGridMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtMin = New Care.Controls.CareTextBox()
        Me.Label2 = New Care.Controls.CareLabel()
        Me.txtStep = New Care.Controls.CareTextBox()
        Me.Label1 = New Care.Controls.CareLabel()
        Me.txtMax = New Care.Controls.CareTextBox()
        Me.CareLabel1 = New Care.Controls.CareLabel()
        Me.gbx.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.txtMin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStep.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMax.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbx
        '
        Me.gbx.Controls.Add(Me.txtMax)
        Me.gbx.Controls.Add(Me.CareLabel1)
        Me.gbx.Controls.Add(Me.txtMin)
        Me.gbx.Controls.Add(Me.Label2)
        Me.gbx.Controls.Add(Me.txtStep)
        Me.gbx.Controls.Add(Me.Label1)
        Me.gbx.Size = New System.Drawing.Size(369, 144)
        Me.gbx.Controls.SetChildIndex(Me.Panel2, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label1, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtStep, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label2, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtMin, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel1, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtMax, 0)
        '
        'Panel2
        '
        Me.Panel2.Location = New System.Drawing.Point(3, 113)
        Me.Panel2.TabIndex = 3
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        '
        'Grid
        '
        Me.Grid.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Grid.Appearance.Options.UseFont = True
        '
        'btnDelete
        '
        Me.btnDelete.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Appearance.Options.UseFont = True
        '
        'btnEdit
        '
        Me.btnEdit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.Appearance.Options.UseFont = True
        '
        'btnAdd
        '
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.Appearance.Options.UseFont = True
        '
        'btnClose
        '
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'txtMin
        '
        Me.txtMin.CharacterCasing = CharacterCasing.Normal
        Me.txtMin.EditValue = ""
        Me.txtMin.EnterMoveNextControl = True
        Me.txtMin.Location = New System.Drawing.Point(110, 50)
        Me.txtMin.MaxLength = 3
        Me.txtMin.Name = "txtMin"
        Me.txtMin.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtMin.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtMin.Properties.Appearance.Options.UseFont = True
        Me.txtMin.Properties.Appearance.Options.UseTextOptions = True
        Me.txtMin.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtMin.Properties.MaxLength = 3
        Me.txtMin.ReadOnly = False
        Me.txtMin.Size = New System.Drawing.Size(58, 22)
        Me.txtMin.TabIndex = 1
        Me.txtMin.Tag = "AEN"
        Me.txtMin.TextAlign = HorizontalAlignment.Left
        Me.txtMin.ToolTipText = ""
        '
        'Label2
        '
        Me.Label2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label2.Location = New System.Drawing.Point(12, 53)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(77, 15)
        Me.Label2.TabIndex = 28
        Me.Label2.Text = "Minimum Age"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtStep
        '
        Me.txtStep.CharacterCasing = CharacterCasing.Normal
        Me.txtStep.EnterMoveNextControl = True
        Me.txtStep.Location = New System.Drawing.Point(110, 22)
        Me.txtStep.MaxLength = 100
        Me.txtStep.Name = "txtStep"
        Me.txtStep.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStep.Properties.AccessibleName = "Name"
        Me.txtStep.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtStep.Properties.Appearance.Options.UseFont = True
        Me.txtStep.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStep.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtStep.Properties.MaxLength = 100
        Me.txtStep.ReadOnly = False
        Me.txtStep.Size = New System.Drawing.Size(58, 22)
        Me.txtStep.TabIndex = 0
        Me.txtStep.Tag = "AEMN"
        Me.txtStep.TextAlign = HorizontalAlignment.Left
        Me.txtStep.ToolTipText = ""
        '
        'Label1
        '
        Me.Label1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label1.Location = New System.Drawing.Point(12, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(70, 15)
        Me.Label1.TabIndex = 27
        Me.Label1.Text = "Step Number"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMax
        '
        Me.txtMax.CharacterCasing = CharacterCasing.Normal
        Me.txtMax.EditValue = ""
        Me.txtMax.EnterMoveNextControl = True
        Me.txtMax.Location = New System.Drawing.Point(110, 78)
        Me.txtMax.MaxLength = 3
        Me.txtMax.Name = "txtMax"
        Me.txtMax.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtMax.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtMax.Properties.Appearance.Options.UseFont = True
        Me.txtMax.Properties.Appearance.Options.UseTextOptions = True
        Me.txtMax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtMax.Properties.MaxLength = 3
        Me.txtMax.ReadOnly = False
        Me.txtMax.Size = New System.Drawing.Size(58, 22)
        Me.txtMax.TabIndex = 2
        Me.txtMax.Tag = "AEN"
        Me.txtMax.TextAlign = HorizontalAlignment.Left
        Me.txtMax.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(12, 81)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(78, 15)
        Me.CareLabel1.TabIndex = 30
        Me.CareLabel1.Text = "Maximum Age"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmCDAPSteps
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(424, 402)
        Me.Name = "frmCDAPSteps"
        Me.Text = "frmCDAPSteps"
        Me.gbx.ResumeLayout(False)
        Me.gbx.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.txtMin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStep.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMax.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtMin As Care.Controls.CareTextBox
    Friend WithEvents Label2 As Care.Controls.CareLabel
    Friend WithEvents txtStep As Care.Controls.CareTextBox
    Friend WithEvents Label1 As Care.Controls.CareLabel
    Friend WithEvents txtMax As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
End Class
