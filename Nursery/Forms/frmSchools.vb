﻿

Imports Care.Global
Imports Care.Shared
Imports Care.Data

Public Class frmSchools

    Private m_School As Business.School

    Private Sub DisplayRecord(ByVal SchoolID As String)

        MyBase.RecordID = New Guid(SchoolID)
        MyBase.RecordPopulated = True

        m_School = Business.School.RetreiveByID(MyBase.RecordID.Value)

        txtName.Text = m_School._Name
        txtAddress.Text = m_School._Address
        txtTel.Text = m_School._Tel

        txtDropOffFees.Text = ValueHandler.MoneyAsText(m_School._DropoffCharge)
        txtCollectionFees.Text = ValueHandler.MoneyAsText(m_School._PickupCharge)

    End Sub


#Region "Overrides"

    Protected Overrides Sub AfterAdd()

        m_School = New Business.School
        MyBase.RecordID = m_School._ID
        MyBase.RecordPopulated = False

        cgInsetDays.Clear()
        txtName.Focus()

    End Sub

    Protected Overrides Sub AfterEdit()
        txtName.Focus()
    End Sub

    Protected Overrides Sub FindRecord()

        Dim _Find As New GenericFind

        Dim _SQL As String = "select name as 'Name' from Schools"

        With _Find
            .Caption = "Find School"
            .ParentForm = ParentForm
            .PopulateOnLoad = True
            .ConnectionString = Session.ConnectionString
            .GridSQL = _SQL
            .GridOrderBy = "order by name"
            .ReturnField = "ID"
            .Show()
        End With

        If _Find.ReturnValue <> "" Then
            DisplayRecord(_Find.ReturnValue)
            XtraTabControl1.FirstPage()
        End If

    End Sub

    Protected Overrides Sub CommitDelete()

        Dim _SQL As String = ""

        _SQL = "delete from Schools where ID = '" + RecordID.Value.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        MyBase.RecordID = Guid.NewGuid
        MyBase.RecordPopulated = False

    End Sub

    Protected Overrides Sub CommitUpdate()

        With m_School
            ._Name = txtName.Text
            ._Address = txtAddress.Text
            ._Tel = txtTel.Text
            ._DropoffCharge = ValueHandler.ConvertDecimal(txtDropOffFees.Text)
            ._PickupCharge = ValueHandler.ConvertDecimal(txtCollectionFees.Text)
            .Store()
        End With

        If Mode = "ADD" Then
            MyBase.RecordPopulated = True
            m_School = Business.School.RetreiveByID(m_School._ID.Value)
            bs.DataSource = m_School
            DisplayRecord(m_School._ID.Value.ToString)
        End If

    End Sub

    Protected Overrides Sub SetBindings()
        'using DisplayRecord
    End Sub

    Protected Overrides Sub SetBindingsByID(ByVal ID As System.Guid)
        'not sure if needed
    End Sub

#End Region

End Class
