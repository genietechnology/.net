﻿

Imports Care.Global
Imports Care.Data
Imports DevExpress.XtraScheduler
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraGrid.Views.Grid
Imports System.Drawing
Imports DevExpress.XtraTab
Imports Care.Shared

Public Class frmRostering

    Private Enum EnumStaffOrChildren
        Staff
        Children
    End Enum

    Private m_Labels As New Dictionary(Of String, Integer)
    Private m_SummaryData As New List(Of SummaryRecord)
    Private m_Appointments As New BindingList(Of CustomAppointment)()

    Private m_SiteID As Guid
    Private m_RotaID As Guid
    Private m_WC As Date
    Private m_WE As Date
    Private m_SelectedDate As Date?
    Private m_SelectedRatioID As Guid?
    Private m_SelectedRoomName As String

    Private Sub ToggleFooterButtons(ByVal Enabled As Boolean)
        btnStaffAdd.Enabled = Enabled
        btnCommit.Enabled = Enabled
        btnSendShifts.Enabled = Enabled
        btnShiftSummary.Enabled = Enabled
        btnRefresh.Enabled = Enabled
    End Sub

    Private Sub frmRostering_Load(sender As Object, e As EventArgs) Handles Me.Load

        tabMain.DisableTabs()

        Me.ToolbarMode = ToolbarEnum.FindandNew

        dshRostered.Hide()
        dshHoliday.Hide()
        dshShort.Hide()
        dshCost.Hide()

        MapScheduler()

        ToggleFooterButtons(False)

    End Sub

    Protected Overrides Sub SetBindings()
        'nothing here...
    End Sub

    Private Sub DisplayTiles()

        Dim _SQL As String = ""

        '***********************************************************************************************************************************************************************

        _SQL = ""
        _SQL += "select sum(staff_short) as 'short' from  RotaSlot"
        _SQL += " where rota_id = '" + m_RotaID.ToString + "'"

        Dim _DRStats As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DRStats IsNot Nothing Then

            Dim _staffShort As Decimal = ValueHandler.ConvertDecimal(_DRStats.Item("short"))

            If _staffShort = 0 Then
                dshShort.SetValue(_staffShort, Care.Controls.DashboardLabel.EnumMiddleFormat.WholeNumber, Care.Controls.DashboardLabel.EnumColourCode.Green)
            Else
                dshShort.SetValue(_staffShort, Care.Controls.DashboardLabel.EnumMiddleFormat.WholeNumber, Care.Controls.DashboardLabel.EnumColourCode.Red)
            End If

        End If

        '***********************************************************************************************************************************************************************

        _SQL = ""
        _SQL += "select count(distinct(rs_staff_id)) 'staff_count', sum(rs_cost) as 'staff_cost' from RotaStaff"
        _SQL += " where rs_rota_id = '" + m_RotaID.ToString + "'"

        Dim _DRStaff As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DRStaff IsNot Nothing Then

            Dim _StaffCount As Decimal = ValueHandler.ConvertDecimal(_DRStaff.Item("staff_count"))
            dshRostered.SetValue(_StaffCount, Care.Controls.DashboardLabel.EnumMiddleFormat.WholeNumber, Care.Controls.DashboardLabel.EnumColourCode.Green)

            Dim _Cost As Decimal = ValueHandler.ConvertDecimal(_DRStaff.Item("staff_cost"))
            dshCost.SetValue(_Cost, Care.Controls.DashboardLabel.EnumMiddleFormat.TwoDecimalPlaces, Care.Controls.DashboardLabel.EnumColourCode.Red)

        End If

        '***********************************************************************************************************************************************************************

        _SQL = ""
        _SQL += "select count(distinct(staff_id)) 'hols' from StaffAbsence"
        _SQL += " where abs_type = 'Annual Leave'"
        _SQL += " and abs_from between " + ValueHandler.SQLDate(m_WC, True) + " and " + ValueHandler.SQLDate(m_WE, True)

        Dim _DRHol As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DRHol IsNot Nothing Then
            Dim _HolCount As Decimal = ValueHandler.ConvertDecimal(_DRHol.Item("hols"))
            If _HolCount > 0 Then
                dshHoliday.SetValue(_HolCount, Care.Controls.DashboardLabel.EnumMiddleFormat.WholeNumber, Care.Controls.DashboardLabel.EnumColourCode.Red)
            Else
                dshHoliday.SetValue(_HolCount, Care.Controls.DashboardLabel.EnumMiddleFormat.WholeNumber, Care.Controls.DashboardLabel.EnumColourCode.Green)
            End If
        End If

        '***********************************************************************************************************************************************************************

        dshShort.Show()
        dshRostered.Show()
        dshCost.Show()
        dshHoliday.Show()

    End Sub

#Region "Scheduler"

    Public Class CustomAppointment
        Public Property StartTime() As Date
        Public Property EndTime() As Date
        Public Property Subject() As String
        Public Property Status() As Integer
        Public Property Description() As String
        Public Property Label() As Integer
        Public Property Location() As String
        Public Property AllDay() As Boolean
        Public Property EventType() As Integer
        Public Property RecurrenceInfo() As String
        Public Property ReminderInfo() As String
        Public Property OwnerId() As Object
        Public Property ID As Guid?
    End Class

    Private Sub SetSchedulerLabels()

        Dim _i As Integer = 0
        m_Labels.Clear()
        SchedulerStorage1.Appointments.Labels.Clear()

        Dim _Rooms As List(Of Business.SiteRoom) = Business.SiteRoom.RetreiveBySite(m_SiteID)
        For Each _r In _Rooms
            SchedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromName(_r._RoomColour), _r._RoomName)
            m_Labels.Add(_r._RoomName, _i)
            _i += 1
        Next

    End Sub

    Private Sub MapScheduler()

        Dim _M As AppointmentMappingInfo = Me.SchedulerStorage1.Appointments.Mappings
        _M.Start = "StartTime"
        _M.End = "EndTime"
        _M.Subject = "Subject"
        _M.AllDay = "AllDay"
        _M.Description = "Description"
        _M.Label = "Label"
        _M.Location = "Location"
        _M.RecurrenceInfo = "RecurrenceInfo"
        _M.ReminderInfo = "ReminderInfo"
        _M.ResourceId = "OwnerId"
        _M.Status = "Status"
        _M.Type = "EventType"

    End Sub

    Private Sub LoadScheduleData()
        SetSchedulerLabels()
        GenerateEvents(m_Appointments)
        Me.SchedulerStorage1.Appointments.DataSource = m_Appointments
        Me.SchedulerControl1.ActiveViewType = SchedulerViewType.FullWeek
        Me.SchedulerControl1.GoToDate(m_WC)
    End Sub

    Private Sub GenerateEvents(ByVal Appointments As BindingList(Of CustomAppointment))

        Dim _SQL As String = ""

        Appointments.Clear()

        _SQL += "Select * from RotaStaff"
        _SQL += " where rs_rota_id = '" + m_RotaID.ToString + "'"
        _SQL += " order by rs_date, rs_staff_name, rs_start"

        Dim _DTAbsence As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DTAbsence IsNot Nothing Then

            For Each _DR As DataRow In _DTAbsence.Rows

                Dim _ID As Guid = New Guid(_DR.Item("ID").ToString)
                Dim _Date As Date? = ValueHandler.ConvertDate(_DR.Item("rs_date"))
                Dim _StartTime As TimeSpan? = ValueHandler.ConvertTimeSpan(_DR.Item("rs_start"))
                Dim _FinishTime As TimeSpan? = ValueHandler.ConvertTimeSpan(_DR.Item("rs_finish"))

                Dim _Start As Date? = ValueHandler.BuildDateTime(_Date.Value, _StartTime.Value)
                Dim _Finish As Date? = ValueHandler.BuildDateTime(_Date.Value, _FinishTime.Value)

                Dim _Staff As String = _DR.Item("rs_staff_name").ToString
                Dim _Room As String = _DR.Item("rs_room").ToString

                Appointments.Add(CreateEvent(Nothing, _Start, _Finish, _Staff, _Room))

            Next

        End If

    End Sub

    Private Function CreateEvent(ByVal ID As Guid, ByVal FromDate As Date?, ByVal ToDate As Date?, ByVal StaffName As String, ByVal Location As String) As CustomAppointment

        Dim _A As New CustomAppointment()

        If FromDate.HasValue Then _A.StartTime = FromDate.Value
        If ToDate.HasValue Then _A.EndTime = ToDate.Value

        _A.ID = ID
        _A.Subject = StaffName
        _A.Location = Location
        _A.Label = m_Labels(Location)
        _A.StartTime = FromDate.Value
        _A.EndTime = ToDate.Value
        _A.AllDay = False
        _A.Status = 0

        Return _A

    End Function

#End Region

#Region "Summary Grid"

    Public Class SummaryRecord
        Property RatioID As Guid
        Property RoomName As String
        Property RoomSeq As Integer
        Property RoomCapacity As Integer
        Property RoomRatio As Integer
        Property MonCount As Integer
        Property MonStaff As Integer
        Property MonShort As Integer
        Property TueCount As Integer
        Property TueStaff As Integer
        Property TueShort As Integer
        Property WedCount As Integer
        Property WedStaff As Integer
        Property WedShort As Integer
        Property ThuCount As Integer
        Property ThuStaff As Integer
        Property ThuShort As Integer
        Property FriCount As Integer
        Property FriStaff As Integer
        Property FriShort As Integer
        Property SatCount As Integer
        Property SatStaff As Integer
        Property SatShort As Integer
        Property SunCount As Integer
        Property SunStaff As Integer
        Property SunShort As Integer

    End Class

    Private Sub DisplaySummaryGrid()

        m_SummaryData.Clear()

        Dim _SQL As String = ""
        _SQL += "select srr.ID, sr.room_name as 'Room', srr.age_from as 'Age From', srr.age_to as 'Age To', srr.capacity as 'Capacity', srr.ratio as 'Ratio'," + vbCrLf

        _SQL += ReturnCountSQL(DayOfWeek.Monday) + "," + vbCrLf
        _SQL += ReturnCountSQL(DayOfWeek.Tuesday) + "," + vbCrLf
        _SQL += ReturnCountSQL(DayOfWeek.Wednesday) + "," + vbCrLf
        _SQL += ReturnCountSQL(DayOfWeek.Thursday) + "," + vbCrLf
        _SQL += ReturnCountSQL(DayOfWeek.Friday) + "," + vbCrLf
        _SQL += ReturnCountSQL(DayOfWeek.Saturday) + "," + vbCrLf
        _SQL += ReturnCountSQL(DayOfWeek.Sunday) + vbCrLf

        _SQL += " from SiteRoomRatios srr" + vbCrLf
        _SQL += " left join SiteRooms sr on sr.ID = srr.room_id" + vbCrLf
        _SQL += " where sr.site_id = '" + m_SiteID.ToString + "'" + vbCrLf
        _SQL += " order by sr.room_sequence"

        cgSummary.HideFirstColumn = True
        cgSummary.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Function ReturnCountSQL(ByVal Day As DayOfWeek) As String

        Dim _Date As Date = m_WC
        If Day = DayOfWeek.Tuesday Then _Date = _Date.AddDays(1)
        If Day = DayOfWeek.Wednesday Then _Date = _Date.AddDays(2)
        If Day = DayOfWeek.Thursday Then _Date = _Date.AddDays(3)
        If Day = DayOfWeek.Friday Then _Date = _Date.AddDays(4)
        If Day = DayOfWeek.Saturday Then _Date = _Date.AddDays(5)
        If Day = DayOfWeek.Sunday Then _Date = _Date.AddDays(6)

        Dim _DayAbrv As String = Day.ToString.Substring(0, 3)

        Dim _C As String = ""
        _C += "(select count(*) from RotaChildren"
        _C += " where rc_rota_id = '" + m_RotaID.ToString + "' and rc_date = " + ValueHandler.SQLDate(_Date, True) + " and rc_ratio_id = srr.ID)"

        Dim _S As String = ""
        _S += "(select count(*) from RotaStaff"
        _S += " where rs_rota_id = '" + m_RotaID.ToString + "' and rs_date = " + ValueHandler.SQLDate(_Date, True) + " and rs_ratio_id = srr.ID)"

        Dim _short As String = ""
        _short &= "(select sum(staff_short) from RotaSlot 
                    where rota_id = '" & m_RotaID.ToString & "' and slot_date = " & ValueHandler.SQLDate(_Date, True) & " and slot_ratio = srr.id)"

        Dim _Return As String = ""

        _Return += _C + " as '" + _DayAbrv + "HC',"
        _Return += _S + " as '" + _DayAbrv + "Staff',"
        _Return &= "iif(" & _C & " = 0, 0," & _short & ") As '" & _DayAbrv & "Short'"

        Return _Return

    End Function

#End Region

    Protected Overrides Sub AfterAdd()

        Dim _frm As New frmRosterBuild
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            DisplayRecord(_frm.RotaID)
        End If

        _frm.Dispose()

        ToolAccept()

    End Sub

    Protected Overrides Sub FindRecord()

        Dim _SQL As String = ""

        _SQL += "select site_name as 'Site', wc as 'Week Commencing', status as 'Status', create_user as 'Created by', create_stamp as 'Created'"
        _SQL += " from Rotas"

        Dim _ReturnValue As String = ""
        Dim _Find As New GenericFind
        With _Find
            .Caption = "Find Rota"
            .ParentForm = Me
            .PopulateOnLoad = True
            .ConnectionString = Session.ConnectionString
            .GridSQL = _SQL
            .GridOrderBy = "order by create_stamp desc"
            .ReturnField = "ID"
            .GridWhereClause = ""
            .Show()
            _ReturnValue = .ReturnValue
        End With

        If _ReturnValue <> "" Then
            DisplayRecord(New Guid(_ReturnValue))
        End If

    End Sub

    Protected Overrides Sub CommitUpdate()

    End Sub

    Protected Overrides Sub CommitDelete()
        Dim _SQL As String = ""

        _SQL = "delete from Rotas where ID = '" + m_RotaID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from RotaChildren where rc_rota_id = '" + m_RotaID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from RotaStaff where rs_rota_id = '" + m_RotaID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from RotaSlot where rota_id = '" + m_RotaID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from RotaSlotStaff where rss_rota_id = '" + m_RotaID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

    End Sub

    Private Sub DisplayRecord(ByVal RotaID As Guid)

        Dim _R As Business.Rota = Business.Rota.RetreiveByID(RotaID)
        If _R IsNot Nothing Then

            Me.ToolbarMode = ToolbarEnum.Standard
            Me.RecordPopulated = True
            Me.RecordID = RotaID

            tabMain.EnableTabs()

            m_SiteID = _R._SiteId.Value
            m_RotaID = _R._ID.Value
            m_WC = _R._Wc.Value
            m_WE = _R._Wc.Value.AddDays(6)

            LoadScheduleData()

            DisplayTab()
            ToggleFooterButtons(True)

        End If

    End Sub

    Private Sub DisplayDetail(ByVal e As RowCellClickEventArgs)

        btnStaffAdd.Enabled = False
        m_SelectedDate = Nothing

        'skip if we have selected any of the "room" columns
        If e.Column.ColumnHandle < 6 Then Exit Sub

        Dim _ColumnName As String = e.Column.FieldName

        'we cannot display anything for short staff either - as there is no staff!
        If _ColumnName.Contains("Short") Then
            btnStaffAdd.Enabled = True
            Exit Sub
        End If

        'staff or count
        Dim _Mode As EnumStaffOrChildren = EnumStaffOrChildren.Staff
        If _ColumnName.Contains("HC") Then _Mode = EnumStaffOrChildren.Children

        Dim _AddDays As Integer = 0
        Dim _Day As String = e.Column.FieldName.Substring(0, 3)

        If _Day = "Tue" Then _AddDays = 1
        If _Day = "Wed" Then _AddDays = 2
        If _Day = "Thu" Then _AddDays = 3
        If _Day = "Fri" Then _AddDays = 4
        If _Day = "Sat" Then _AddDays = 5
        If _Day = "Sun" Then _AddDays = 6

        Dim _Date As Date = DateAdd(DateInterval.Day, _AddDays, m_WC)
        Dim _RatioID As String = cgSummary.GetRowCellValue(e.RowHandle, "ID").ToString
        Dim _RoomName As String = cgSummary.GetRowCellValue(e.RowHandle, "Room").ToString

        Dim _SQL As String = ""

        If _Mode = EnumStaffOrChildren.Staff Then

            _SQL += "select rs_staff_id As 'staff_id', rs_staff_name as 'Name',"
            _SQL += " rs_staff_level As 'Level', rs_staff_age as 'Age',"
            _SQL += " rs_staff_paed as 'PAED', rs_staff_faid as 'F AID', rs_staff_senco as 'SENCO',"
            _SQL += " rs_start As 'Start', rs_finish as 'Finish',"
            _SQL += " rs_hours as 'Hours', rs_staff_rate as 'Rate', rs_cost as 'Cost'"
            _SQL += " from RotaStaff"
            _SQL += " where rs_date = " + ValueHandler.SQLDate(_Date, True)
            _SQL += " and rs_ratio_id = '" + _RatioID + "'"
            _SQL += " order by rs_start"

            btnStaffAdd.Enabled = True
            m_SelectedDate = _Date
            m_SelectedRatioID = New Guid(_RatioID)
            m_SelectedRoomName = _RoomName

        Else

            _SQL += "select rc_child_id As 'staff_id', rc_child_name as 'Name',"
            _SQL += " rc_child_dob As 'DOB', rc_child_age as 'Age',"
            _SQL += " rc_start As 'Start', rc_finish as 'Finish', rc_hours as 'Hours'"
            _SQL += " from RotaChildren"
            _SQL += " where rc_date = " + ValueHandler.SQLDate(_Date, True)
            _SQL += " and rc_ratio_id = '" + _RatioID + "'"
            _SQL += " order by rc_start"

        End If

        cgDetail.HideFirstColumn = True
        cgDetail.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub cgSummary_RowCellClick(sender As Object, e As RowCellClickEventArgs) Handles cgSummary.RowCellClick
        DisplayDetail(e)
    End Sub

    Private Sub cgSummary_RowCellStyle(sender As Object, e As RowCellStyleEventArgs) Handles cgSummary.RowCellStyle

        If e Is Nothing Then Exit Sub
        If e.RowHandle < 0 Then Exit Sub

        Select Case True

            Case e.Column.FieldName = "Room",
                    e.Column.FieldName = "Age From",
                    e.Column.FieldName = "Age To",
                    e.Column.FieldName = "Capacity",
                    e.Column.FieldName = "Ratio"

                e.Appearance.BackColor = Color.LightGray

            Case e.Column.FieldName.Contains("Tue"),
                    e.Column.FieldName.Contains("Thu"),
                    e.Column.FieldName.Contains("Sat")

                If e.RowHandle = cgSummary.FocusedRowHandle Then
                    e.Appearance.BackColor = ActiveGlowColor
                Else
                    e.Appearance.BackColor = Color.Cornsilk
                End If

        End Select

        If e.Column.FieldName.Contains("Short") Then

            Dim _short As Integer = ValueHandler.ConvertInteger(cgSummary.GetRowCellValue(e.RowHandle, e.Column.FieldName))

            If _short > 0 Then
                e.Appearance.BackColor = Color.LightCoral
            End If

        End If

    End Sub

    Private Sub tabMain_SelectedPageChanged(sender As Object, e As TabPageChangedEventArgs) Handles tabMain.SelectedPageChanged
        DisplayTab()
    End Sub

    Private Sub DisplayTab()

        m_SelectedDate = Nothing
        m_SelectedRatioID = Nothing
        m_SelectedRoomName = ""

        Select Case tabMain.SelectedTabPage.Name

            Case "tabSummary"
                DisplaySummaryGrid()
                DisplayTiles()

            Case "tabStaffSummary"
                DisplayStaffSummary()

            Case "tabCalendar"

            Case "tabDetail"
                DisplayTimeSlotDates

        End Select

    End Sub

    Private Sub DisplayTimeSlotDates()

        Dim _SQL As String = ""

        _SQL += "select distinct(slot_date), datename(weekday, slot_date) as 'Day', slot_ratio, slot_room as 'Room', staff_ratio as 'Staff Ratio'"
        _SQL += " from RotaSlot"
        _SQL += " where rota_id = '" + m_RotaID.ToString + "'"
        _SQL += " order by slot_date, slot_room"

        cgTimeslotDate.Populate(Session.ConnectionString, _SQL)

        If cgTimeslotDate.RecordCount > 0 Then
            cgTimeslotDate.Columns("slot_date").Visible = False
            cgTimeslotDate.Columns("slot_ratio").Visible = False
        End If

    End Sub

    Private Sub DisplayStaffSummary()

        Dim _SQL As String = ""

        _SQL += "select distinct(rs_staff_id), srr.room_id, sr.room_name, rs_staff_name, sr.room_sequence from RotaStaff"
        _SQL += " left join SiteRoomRatios srr On srr.ID = rs_ratio_id"
        _SQL += " left join SiteRooms sr on sr.ID = srr.room_id"
        _SQL += " where rs_rota_id = '" + m_RotaID.ToString + "'"
        _SQL += " order by sr.room_sequence"

        cgStaff.Populate(Session.ConnectionString, _SQL)

        cgStaff.Columns("rs_staff_id").Visible = False
        cgStaff.Columns("room_id").Visible = False
        cgStaff.Columns("room_name").Caption = "Room"
        cgStaff.Columns("rs_staff_name").Caption = "Name"
        cgStaff.Columns("room_sequence").Visible = False

    End Sub

    Private Sub cgStaff_GridDoubleClick(sender As Object, e As EventArgs) Handles cgStaff.GridDoubleClick

    End Sub

    Private Sub cgStaff_FocusedRowChanged(sender As Object, e As FocusedRowChangedEventArgs) Handles cgStaff.FocusedRowChanged

        If e Is Nothing Then Exit Sub
        If e.FocusedRowHandle < 0 Then Exit Sub

        Dim _StaffID As String = cgStaff.GetRowCellValue(e.FocusedRowHandle, "rs_staff_id").ToString

        Dim _SQL As String = ""

        _SQL += "select RotaStaff.ID, rs_date As 'Date', datename(DW, rs_date) as 'Day', rs_room as 'Room',"
        _SQL += " rs_start As 'Shift Start', rs_finish as 'Shift End', rs_hours as 'Hours', rs_cost as 'Cost' from RotaStaff"
        _SQL += " left join Staff s on s.ID = rs_staff_id"
        _SQL += " where rs_rota_id = '" + m_RotaID.ToString + "'"
        _SQL += " and rs_staff_id = '" + _StaffID + "'"
        _SQL += " order by rs_date"

        cgStaffShifts.HideFirstColumn = True
        cgStaffShifts.Populate(Session.ConnectionString, _SQL)


    End Sub

    Private Sub btnStaffPrintShift_Click(sender As Object, e As EventArgs) Handles btnStaffPrintShift.Click

        Dim _ID As Guid? = GetID(cgStaff)
        If Not _ID.HasValue Then Exit Sub
        Dim _SQL As String = ""

        _SQL += "select rs_staff_name, rs_room, rs_date, cast(rs_start as datetime) as 'rs_start', cast(rs_finish as datetime) as 'rs_finish', rs_hours, rs_cost from RotaStaff"
        _SQL += " where rs_rota_id = '" + m_RotaID.ToString + "'"
        _SQL += " and rs_staff_id = '" + _ID.ToString + "'"
        _SQL += " order by rs_date"

        btnStaffPrintShift.Enabled = False
        Session.CursorWaiting()

        If btnStaffPrintShift.ShiftDown Then
            ReportHandler.DesignReport("RotaStaffIndividual.repx ", Session.ConnectionString, _SQL)
        Else
            ReportHandler.RunReport("RotaStaffIndividual.repx ", Session.ConnectionString, _SQL)
        End If

        btnStaffPrintShift.Enabled = True

    End Sub

    Private Function GetID(ByRef GridControl As Care.Controls.CareGrid) As Guid?

        If GridControl Is Nothing Then Return Nothing
        If GridControl.RecordCount < 0 Then Return Nothing
        If GridControl.CurrentRow Is Nothing Then Return Nothing

        Dim _ID As String = GridControl.CurrentRow.Item(0).ToString

        Return New Guid(_ID)

    End Function

    Private Function GetCellValue(ByRef GridControl As Care.Controls.CareGrid, ByVal ColumnName As String) As Object

        If GridControl Is Nothing Then Return Nothing
        If GridControl.RecordCount < 0 Then Return Nothing
        If GridControl.CurrentRow Is Nothing Then Return Nothing

        Return GridControl.CurrentRow.Item(ColumnName)

    End Function

    Private Sub AmendShift()
        Dim _ID As Guid? = GetID(cgStaffShifts)
        If _ID.HasValue Then
            Dim _frm As New frmRosterShiftChange(_ID)
            _frm.ShowDialog()
        End If
    End Sub

    Private Sub cgStaffShifts_GridDoubleClick(sender As Object, e As EventArgs) Handles cgStaffShifts.GridDoubleClick
        StaffTabAmendShift()
    End Sub

    Private Sub btnStaffAmendShift_Click(sender As Object, e As EventArgs) Handles btnStaffAmendShift.Click
        StaffTabAmendShift()
    End Sub

    Private Sub StaffTabAmendShift()
        AmendShift()
    End Sub

    Private Sub btnStaffAdd_Click(sender As Object, e As EventArgs) Handles btnStaffAdd.Click

        If Not m_SelectedDate.HasValue Then Exit Sub
        If Not m_SelectedRatioID.HasValue Then Exit Sub
        If m_SelectedRoomName = "" Then Exit Sub

        Dim _frm As New frmRosterShiftNew(m_RotaID, m_SiteID, m_SelectedDate.Value, m_SelectedRatioID.Value, m_SelectedRoomName)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then

        End If

    End Sub

    Private Sub cgSummary_GridDoubleClick(sender As Object, e As EventArgs) Handles cgSummary.GridDoubleClick

    End Sub

    Private Sub btnShiftSummary_Click(sender As Object, e As EventArgs) Handles btnShiftSummary.Click

        Dim _SQL As String = ""

        _SQL += "select rs_rota_id, rs_date, rs_room, rs_start, rs_finish, rs_hours, rs_cost,"
        _SQL += " rs_staff_name, rs_staff_age, rs_staff_level, rs_staff_rate, rs_staff_paed, rs_staff_faid, rs_staff_senco"
        _SQL += " from RotaStaff"
        _SQL += " where rs_rota_id = '" + m_RotaID.ToString + "'"

        btnShiftSummary.Enabled = False
        Session.CursorWaiting()

        If btnStaffPrintShift.ShiftDown Then
            ReportHandler.DesignReport("RotaStaffSummary.repx ", Session.ConnectionString, _SQL)
        Else
            ReportHandler.RunReport("RotaStaffSummary.repx ", Session.ConnectionString, _SQL)
        End If

        btnShiftSummary.Enabled = True

    End Sub

    Private Sub btnShifts_Click(sender As Object, e As EventArgs) Handles btnShifts.Click
        Dim _ID As Guid? = GetID(cgStaff)
        If _ID.HasValue Then

            Session.CursorApplicationStarting()

            Dim _frm As New frmStaff(_ID.Value, True)
            _frm.StartPosition = FormStartPosition.CenterScreen
            _frm.ShowDialog()

        End If
    End Sub

    Private Sub dshHoliday_DoubleClick(sender As Object, e As Care.Controls.DashboardLabel.DashboardEventArgs) Handles dshHoliday.DoubleClick

        Dim _SQL As String = ""

        _SQL = ""
        _SQL += "select Staff.ID, Staff.fullname as 'Name', Staff.group_name as 'Room', abs_from as 'From', abs_to as 'To' from StaffAbsence"
        _SQL += " left join Staff on Staff.ID = StaffAbsence.staff_id"
        _SQL += " where abs_type = 'Annual Leave'"
        _SQL += " and abs_from between " + ValueHandler.SQLDate(m_WC, True) + " and " + ValueHandler.SQLDate(m_WE, True)

        DashboardDrillDown(_SQL, "Staff on Annual Leave")

    End Sub

    Private Sub DashboardDrillDown(ByVal SQL As String, ByVal FormCaption As String)

        Session.CursorWaiting()

        Dim _frm As New frmTodayDrillDown(SQL, FormCaption, frmTodayDrillDown.EnumFormToLoad.Staff)
        _frm.ShowDialog()

        _frm.Dispose()
        _frm = Nothing

    End Sub

    Private Sub dshRostered_DoubleClick(sender As Object, e As Care.Controls.DashboardLabel.DashboardEventArgs) Handles dshRostered.DoubleClick
        tabMain.SelectedTabPage = tabStaffSummary
    End Sub

    Private Sub cgTimeslotDate_FocusedRowChanged(sender As Object, e As FocusedRowChangedEventArgs) Handles cgTimeslotDate.FocusedRowChanged

        If e Is Nothing Then Exit Sub
        If e.FocusedRowHandle < 0 Then Exit Sub

        Dim _Date As Date? = ValueHandler.ConvertDate(GetCellValue(cgTimeslotDate, "slot_date"))
        Dim _RatioID As String = GetCellValue(cgTimeslotDate, "slot_ratio").ToString

        Dim _SQL As String = ""
        _SQL += "select slot_start As 'Start', slot_finish as 'Finish', child_count as 'Children', staff_required as 'Staff Required',"
        _SQL += " staff_available As 'Staff Available', staff_rostered as 'Rostered',"
        _SQL += " staff_short as 'Staff Short', staff_spare as 'Staff Spare'"
        _SQL += " from RotaSlot"
        _SQL += " where rota_id = '" + m_RotaID.ToString + "'"
        _SQL += " and slot_date = " + ValueHandler.SQLDate(_Date.Value, True)
        _SQL += " and slot_ratio = '" + _RatioID + "'"
        _SQL += " order by slot_start"

        cgTimeSlotDetail.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub cgTimeSlotDetail_FocusedRowChanged(sender As Object, e As FocusedRowChangedEventArgs) Handles cgTimeSlotDetail.FocusedRowChanged

        If e Is Nothing Then Exit Sub
        If e.FocusedRowHandle < 0 Then Exit Sub

        Dim _Date As Date? = ValueHandler.ConvertDate(GetCellValue(cgTimeslotDate, "slot_date"))
        Dim _RatioID As String = GetCellValue(cgTimeslotDate, "slot_ratio").ToString

    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click

        Dim sql As String = ""

        Session.CursorWaiting()

        sql = "delete from RotaChildren where rc_rota_id = '" + m_RotaID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, sql)

        sql = "delete from RotaStaff where rs_rota_id = '" + m_RotaID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, sql)

        sql = "delete from RotaSlot where rota_id = '" + m_RotaID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, sql)

        sql = "delete from RotaSlotStaff where rss_rota_id = '" + m_RotaID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, sql)

        Dim rosterBuild As New frmRosterBuild
        rosterBuild.UpdateRoster(m_RotaID, m_SiteID, m_WC)
        rosterBuild.Close()

        DisplayRecord(m_RotaID)
        Session.CursorDefault()

    End Sub

    Private Sub cgTimeSlotDetail_RowStyle(sender As Object, e As RowStyleEventArgs) Handles cgTimeSlotDetail.RowStyle
        If ValueHandler.ConvertInteger(cgTimeSlotDetail.GetRowCellValue(e.RowHandle, "Staff Short")) > 0 Then
            e.Appearance.BackColor = Color.LightCoral
        End If
    End Sub
End Class
