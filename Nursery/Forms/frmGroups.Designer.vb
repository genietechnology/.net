﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGroups
    Inherits Care.Shared.frmGridMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New Care.Controls.CareLabel(Me.components)
        Me.txtName = New Care.Controls.CareTextBox(Me.components)
        Me.Label2 = New Care.Controls.CareLabel(Me.components)
        Me.txtSeq = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.txtAgeMax = New Care.Controls.CareTextBox(Me.components)
        Me.txtAgeMin = New Care.Controls.CareTextBox(Me.components)
        Me.gbx.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSeq.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAgeMax.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAgeMin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbx
        '
        Me.gbx.Controls.Add(Me.txtAgeMin)
        Me.gbx.Controls.Add(Me.txtAgeMax)
        Me.gbx.Controls.Add(Me.CareLabel2)
        Me.gbx.Controls.Add(Me.CareLabel1)
        Me.gbx.Controls.Add(Me.txtSeq)
        Me.gbx.Controls.Add(Me.Label2)
        Me.gbx.Controls.Add(Me.txtName)
        Me.gbx.Controls.Add(Me.Label1)
        Me.gbx.Location = New System.Drawing.Point(33, 80)
        Me.gbx.Size = New System.Drawing.Size(379, 166)
        Me.gbx.TabIndex = 1
        Me.gbx.Controls.SetChildIndex(Me.Label1, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtName, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label2, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtSeq, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel1, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel2, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtAgeMax, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtAgeMin, 0)
        Me.gbx.Controls.SetChildIndex(Me.Panel2, 0)
        '
        'Panel2
        '
        Me.Panel2.Location = New System.Drawing.Point(3, 135)
        Me.Panel2.Size = New System.Drawing.Size(373, 28)
        Me.Panel2.TabIndex = 4
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(188, 0)
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(279, 0)
        '
        'Grid
        '
        Me.Grid.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Grid.Appearance.Options.UseFont = True
        Me.Grid.Location = New System.Drawing.Point(10, 10)
        Me.Grid.Size = New System.Drawing.Size(425, 351)
        Me.Grid.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Size = New System.Drawing.Size(444, 35)
        '
        'btnDelete
        '
        Me.btnDelete.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Appearance.Options.UseFont = True
        '
        'btnEdit
        '
        Me.btnEdit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.Appearance.Options.UseFont = True
        '
        'btnAdd
        '
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.Appearance.Options.UseFont = True
        '
        'btnClose
        '
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(370, 5)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'Label1
        '
        Me.Label1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label1.Location = New System.Drawing.Point(10, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 15)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Name"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.CharacterCasing = CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(127, 21)
        Me.txtName.MaxLength = 30
        Me.txtName.Name = "txtName"
        Me.txtName.NumericAllowNegatives = False
        Me.txtName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AccessibleName = "Group Name"
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.MaxLength = 30
        Me.txtName.ReadOnly = False
        Me.txtName.Size = New System.Drawing.Size(244, 20)
        Me.txtName.TabIndex = 0
        Me.txtName.Tag = "AEM"
        Me.txtName.TextAlign = HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'Label2
        '
        Me.Label2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label2.Location = New System.Drawing.Point(10, 51)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(97, 15)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Min Age (months)"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSeq
        '
        Me.txtSeq.CharacterCasing = CharacterCasing.Normal
        Me.txtSeq.EnterMoveNextControl = True
        Me.txtSeq.Location = New System.Drawing.Point(127, 102)
        Me.txtSeq.MaxLength = 3
        Me.txtSeq.Name = "txtSeq"
        Me.txtSeq.NumericAllowNegatives = False
        Me.txtSeq.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSeq.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSeq.Properties.AccessibleName = "Display Sequence"
        Me.txtSeq.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSeq.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSeq.Properties.MaxLength = 3
        Me.txtSeq.ReadOnly = False
        Me.txtSeq.Size = New System.Drawing.Size(56, 20)
        Me.txtSeq.TabIndex = 3
        Me.txtSeq.Tag = "AEN"
        Me.txtSeq.TextAlign = HorizontalAlignment.Left
        Me.txtSeq.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(10, 77)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(98, 15)
        Me.CareLabel1.TabIndex = 6
        Me.CareLabel1.Text = "Max Age (months)"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(10, 104)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(92, 15)
        Me.CareLabel2.TabIndex = 8
        Me.CareLabel2.Text = "Display Sequence"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAgeMax
        '
        Me.txtAgeMax.CharacterCasing = CharacterCasing.Normal
        Me.txtAgeMax.EnterMoveNextControl = True
        Me.txtAgeMax.Location = New System.Drawing.Point(127, 75)
        Me.txtAgeMax.MaxLength = 3
        Me.txtAgeMax.Name = "txtAgeMax"
        Me.txtAgeMax.NumericAllowNegatives = False
        Me.txtAgeMax.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtAgeMax.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtAgeMax.Properties.AccessibleName = "Display Sequence"
        Me.txtAgeMax.Properties.Appearance.Options.UseTextOptions = True
        Me.txtAgeMax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtAgeMax.Properties.MaxLength = 3
        Me.txtAgeMax.ReadOnly = False
        Me.txtAgeMax.Size = New System.Drawing.Size(56, 20)
        Me.txtAgeMax.TabIndex = 2
        Me.txtAgeMax.Tag = "AEN"
        Me.txtAgeMax.TextAlign = HorizontalAlignment.Left
        Me.txtAgeMax.ToolTipText = ""
        '
        'txtAgeMin
        '
        Me.txtAgeMin.CharacterCasing = CharacterCasing.Normal
        Me.txtAgeMin.EnterMoveNextControl = True
        Me.txtAgeMin.Location = New System.Drawing.Point(127, 49)
        Me.txtAgeMin.MaxLength = 3
        Me.txtAgeMin.Name = "txtAgeMin"
        Me.txtAgeMin.NumericAllowNegatives = False
        Me.txtAgeMin.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtAgeMin.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtAgeMin.Properties.AccessibleName = "Display Sequence"
        Me.txtAgeMin.Properties.Appearance.Options.UseTextOptions = True
        Me.txtAgeMin.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtAgeMin.Properties.MaxLength = 3
        Me.txtAgeMin.ReadOnly = False
        Me.txtAgeMin.Size = New System.Drawing.Size(56, 20)
        Me.txtAgeMin.TabIndex = 1
        Me.txtAgeMin.Tag = "AEN"
        Me.txtAgeMin.TextAlign = HorizontalAlignment.Left
        Me.txtAgeMin.ToolTipText = ""
        '
        'frmGroups
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(444, 402)
        Me.MinimumSize = New System.Drawing.Size(460, 359)
        Me.Name = "frmGroups"
        Me.gbx.ResumeLayout(False)
        Me.gbx.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSeq.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAgeMax.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAgeMin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtSeq As Care.Controls.CareTextBox
    Friend WithEvents Label2 As Care.Controls.CareLabel
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents Label1 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents txtAgeMin As Care.Controls.CareTextBox
    Friend WithEvents txtAgeMax As Care.Controls.CareTextBox

End Class
