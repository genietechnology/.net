﻿

Imports Care.Global
Imports Care.Data
Imports Care.Shared

Public Class frmSites

    Private m_Site As Business.Site
    Private m_FormCaption As String = ""

    Private Sub frmSites_Load(sender As Object, e As EventArgs) Handles Me.Load

        m_FormCaption = Me.Text

        cbxNLCode.AllowBlank = True
        cbxNLCode.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("NL Codes"))

        cbxNLTrack.AllowBlank = True
        cbxNLTrack.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("NL Tracking"))

        cbxFinSystem.AddItem("Xero")
        cbxFinSystem.AddItem("Sage")
        cbxFinSystem.AddItem("Quickbooks")
        cbxFinSystem.AddItem("Clearbooks")

    End Sub

#Region "Overrides"

    Protected Overrides Sub AfterEdit()

        Select Case tabMain.SelectedTabPage.Name

            Case "tabSite"
                txtName.Focus()

            Case "tabRooms"
                cgRooms.ButtonsEnabled = True
                cgRatios.ButtonsEnabled = True

            Case "tabUsers"
                cgUsers.ButtonsEnabled = True

        End Select

    End Sub

    Protected Overrides Sub SetBindings()

        m_Site = New Business.Site
        bs.DataSource = m_Site

        txtName.DataBindings.Add("Text", bs, "_Name")

        cbxNLCode.DataBindings.Add("Text", bs, "_NlCode")
        cbxNLTrack.DataBindings.Add("Text", bs, "_NlTracking")

        txtAccPrefix.DataBindings.Add("Text", bs, "_AccPrefix")
        txtAccNext.DataBindings.Add("Text", bs, "_AccNext")

        chkFinOverride.DataBindings.Add("Checked", bs, "_FinOverride")
        cbxFinSystem.DataBindings.Add("Text", bs, "_FinSystem")

        txtFinApiKey.DataBindings.Add("Text", bs, "_FinApiKey")
        txtFinApiSecret.DataBindings.Add("Text", bs, "_FinApiSecret")

        txtFinPfxPath.DataBindings.Add("Text", bs, "_FinPfxPath")

        txtFinSageVer.DataBindings.Add("Text", bs, "_FinSageVer")
        txtFinSageUsername.DataBindings.Add("Text", bs, "_FinSageUser")
        txtFinSagePath.DataBindings.Add("Text", bs, "_FinSagePath")

    End Sub

    Protected Overrides Function BeforeDelete() As Boolean

        Dim _SQL As String = ""
        Dim _Count As Integer

        'check if there are any children associated with this site
        _SQL = "select count (*) from Children where site_id = '" + m_Site._ID.ToString + "'"
        _Count = ValueHandler.ConvertInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))
        If _Count > 0 Then
            CareMessage("There are " + _Count.ToString + " children associated with this Site. Delete Cancelled.", MessageBoxIcon.Exclamation, "Child Check")
            Return False
        End If

        'check if there are any staff associated with this site
        _SQL = "select count (*) from Staff where site_id = '" + m_Site._ID.ToString + "'"
        _Count = ValueHandler.ConvertInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))
        If _Count > 0 Then
            CareMessage("There are " + _Count.ToString + " staff associated with this Site. Delete Cancelled.", MessageBoxIcon.Exclamation, "Staff Check")
            Return False
        End If

        Return True

    End Function

    Protected Overrides Sub AfterAcceptChanges()
        cgRooms.ButtonsEnabled = False
        cgRatios.ButtonsEnabled = False
        cgUsers.ButtonsEnabled = False
    End Sub

    Protected Overrides Sub AfterCancelChanges()
        cgRooms.ButtonsEnabled = False
        cgRatios.ButtonsEnabled = False
        cgUsers.ButtonsEnabled = False
    End Sub

    Protected Overrides Sub CommitDelete()

        Dim _SQL As String = ""

        _SQL = "delete from Sites where ID = '" + m_Site._ID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from SiteRooms where site_id = '" + m_Site._ID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from SiteRoomRatios where room_id not in (select ID from SiteRooms)"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from Spaces where spc_site_id = '" + m_Site._ID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "delete from Bookings where site_id = '" + m_Site._ID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

    End Sub

    Protected Overrides Sub CommitUpdate()

        m_Site = CType(bs.Item(bs.Position), Business.Site)

        m_Site._Address = txtAddress.Address
        m_Site._Postcode = txtAddress.PostCode
        m_Site._Tel = txtTel.Text

        m_Site._eGenFrom = txtGenSender.Text
        m_Site._eGenAddress = txtGenEmail.Text

        m_Site._eReportFrom = txtReportSender.Text
        m_Site._eReportAddress = txtReportEmail.Text

        m_Site._eInvFrom = txtFinanceSender.Text
        m_Site._eInvAddress = txtFinanceEmail.Text

        m_Site._NlCode = cbxNLCode.Text
        m_Site._NlTracking = cbxNLTrack.Text

        If txtFinPfxPassword.Text = "" Then
            m_Site._FinPfxPwd = ""
        Else
            m_Site._FinPfxPwd = EncyptionHandler.EncryptString(txtFinPfxPassword.Text)
        End If

        If txtFinSagePwd.Text = "" Then
            m_Site._FinSagePwd = ""
        Else
            m_Site._FinSagePwd = EncyptionHandler.EncryptString(txtFinSagePwd.Text)
        End If

        m_Site.Store()

        'update the site name on all children and families
        Dim _SQL As String = "update Children set site_name = '" + m_Site._Name + "' where site_id = '" + m_Site._ID.Value.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "update Family set site_name = '" + m_Site._Name + "' where site_id = '" + m_Site._ID.Value.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "update Bookings set site_name = '" + m_Site._Name + "' where site_id = '" + m_Site._ID.Value.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "update InvoiceBatch set site_name = '" + m_Site._Name + "' where site_id = '" + m_Site._ID.Value.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "update Leads set site_name = '" + m_Site._Name + "' where site_id = '" + m_Site._ID.Value.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "update Staff set site_name = '" + m_Site._Name + "' where site_id = '" + m_Site._ID.Value.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        _SQL = "update Tariffs set site_name = '" + m_Site._Name + "' where site_id = '" + m_Site._ID.Value.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

    End Sub

    Protected Overrides Sub FindRecord()

        Dim _Find As New GenericFind

        Dim _SQL As String = "select name as 'Name', hidden As 'Hidden',"
        _SQL += " CAST(CASE WHEN len(e_gen_address) > 0 THEN 1 ELSE 0 END AS BIT) AS 'General Emails',"
        _SQL += " CAST(CASE WHEN len(e_inv_address) > 0 THEN 1 ELSE 0 END AS BIT) AS 'Invoices',"
        _SQL += " CAST(CASE WHEN len(e_report_address) > 0 THEN 1 ELSE 0 END AS BIT) AS 'Daily Reports',"
        _SQL += " nl_code As 'NL Code', nl_tracking as 'NL Tracking',"
        _SQL += " fin_override as 'Financials Override', fin_system as 'Financials System' from Sites"

        With _Find
            .Caption = "Find Site"
            .ParentForm = ParentForm
            .PopulateOnLoad = True
            .ConnectionString = Session.ConnectionString
            .GridSQL = _SQL
            .GridOrderBy = "order by name"
            .ReturnField = "ID"
            .FormWidth = 800
            .Show()
        End With

        If _Find.ReturnValue <> "" Then

            MyBase.RecordID = New Guid(_Find.ReturnValue)
            MyBase.RecordPopulated = True

            m_Site = Business.Site.RetreiveByID(New Guid(_Find.ReturnValue))
            bs.DataSource = m_Site

            DisplayRecord()

        End If

    End Sub

#End Region

    Private Sub tabMain_SelectedPageChanging(sender As Object, e As DevExpress.XtraTab.TabPageChangedEventArgs) Handles tabMain.SelectedPageChanging

        Select Case e.Page.Name

            Case "tabSite"
                Me.ToolbarMode = ToolbarEnum.Standard

            Case "tabRooms"
                Me.ToolbarMode = ToolbarEnum.FindandEdit

            Case "tabUsers"
                Me.ToolbarMode = ToolbarEnum.FindandEdit

        End Select

    End Sub

    Private Sub DisplayRecord()

        If Me.RecordPopulated = False Then Exit Sub

        Select Case tabMain.SelectedTabPage.Name

            Case "tabSite"

                Me.Text = m_FormCaption + " - " + m_Site._Name

                txtAddress.Address = m_Site._Address
                txtAddress.PostCode = m_Site._Postcode
                txtTel.Text = m_Site._Tel

                txtGenSender.Text = m_Site._eGenFrom
                txtGenEmail.Text = m_Site._eGenAddress

                txtReportSender.Text = m_Site._eReportFrom
                txtReportEmail.Text = m_Site._eReportAddress

                txtFinanceSender.Text = m_Site._eInvFrom
                txtFinanceEmail.Text = m_Site._eInvAddress

                txtFinSagePwd.Text = EncyptionHandler.DecryptString(m_Site._FinSagePwd)
                txtFinPfxPassword.Text = EncyptionHandler.DecryptString(m_Site._FinPfxPwd)

            Case "tabRooms"
                DisplayRooms()

            Case "tabUsers"
                DisplayUsers()

        End Select

    End Sub

    Private Sub tabMain_SelectedPageChanged(sender As Object, e As DevExpress.XtraTab.TabPageChangedEventArgs) Handles tabMain.SelectedPageChanged
        DisplayRecord()
    End Sub

    Private Sub DisplayRooms()

        Dim _SQL As String = ""

        _SQL += "select ID, room_name as 'Name', room_hours_from as 'Opens', room_hours_to as 'Closes', "
        _SQL += " room_check_children as 'Children', room_check_staff as 'Staff', room_monitor as 'Monitored' from SiteRooms"
        _SQL += " where site_id = '" + m_Site._ID.ToString + "'"
        _SQL += " order by room_sequence"

        cgRooms.HideFirstColumn = True
        cgRooms.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub DisplayUsers()

        Dim _SQL As String = ""
        _SQL += "select e.ID, u.forename as 'Forename', u.surname as 'Surname', u.department as 'Department', u.job_title as 'Job Title' from SiteExcludedUsers e"
        _SQL += " left join AppUsers u on u.ID = e.user_id"
        _SQL += " where e.site_id = '" + m_Site._ID.ToString + "'"
        _SQL += " order by u.forename"

        cgUsers.HideFirstColumn = True
        cgUsers.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub cgUsers_AddClick(sender As Object, e As EventArgs) Handles cgUsers.AddClick

        Dim _SQL As String = ""

        _SQL += "select forename as 'Forename', surname as 'Surname', department as 'Department', job_title as 'Job Title' from AppUsers"

        Dim _Order As String = "order by forename, surname"
        Dim _Where As String = " and ID NOT IN (select user_id from SiteExcludedUsers where site_id = '" + m_Site._ID.ToString + "')"

        Dim _Find As New GenericFind
        With _Find

            .Caption = "Find User"
            .ParentForm = ParentForm
            .PopulateOnLoad = True
            .ConnectionString = Session.ConnectionString
            .GridSQL = _SQL
            .GridWhereClause = _Where
            .GridOrderBy = _Order
            .ReturnField = "ID"
            .FormWidth = 600
            .Show()

            If .ReturnValue <> "" Then
                ExcludeUser(.ReturnValue)
                DisplayUsers()
            End If

        End With

    End Sub

    Private Sub ExcludeUser(ByVal UserID As String)
        Dim _XU As New Business.SiteExcludedUser
        _XU._SiteId = m_Site._ID.Value
        _XU._UserId = New Guid(UserID)
        _XU.Store()
    End Sub

    Private Sub cgUsers_EditClick(sender As Object, e As EventArgs) Handles cgUsers.EditClick
        CareMessage("You cannot Edit an Excluded User. Use Remove instead.", MessageBoxIcon.Warning, "Edit")
    End Sub

    Private Sub cgUsers_RemoveClick(sender As Object, e As EventArgs) Handles cgUsers.RemoveClick
        If cgUsers.RecordCount > 0 Then
            If cgUsers.CurrentRow IsNot Nothing Then
                If CareMessage("Are you sure you want to remove this Excluded User?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Remove User") = DialogResult.Yes Then
                    Dim _ID As String = cgUsers.CurrentRow(0).ToString
                    Dim _SQL As String = "delete from SiteExcludedUsers where ID = '" + _ID + "'"
                    DAL.ExecuteSQL(Session.ConnectionString, _SQL)
                    DisplayUsers()
                End If
            End If
        End If
    End Sub

    Private Sub cgRooms_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles cgRooms.FocusedRowChanged

        If e Is Nothing Then Exit Sub
        If e.FocusedRowHandle < 0 Then Exit Sub

        Dim _ID As String = cgRooms.UnderlyingGridView.GetRowCellValue(e.FocusedRowHandle, "ID").ToString
        DisplayRatios(_ID)

    End Sub

    Private Sub DisplayRatios(ByVal RoomID As String)

        Dim _SQL As String = ""

        _SQL += "select ID, age_from as 'Age From (months)', age_to as 'Age To (months)', capacity as 'Capacity', ratio as 'Staff Ratio'"
        _SQL += " from SiteRoomRatios"
        _SQL += " where room_id = '" + RoomID + "'"
        _SQL += " order by age_from"

        cgRatios.HideFirstColumn = True
        cgRatios.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub cgRooms_AddClick(sender As Object, e As EventArgs) Handles cgRooms.AddClick
        EditRoom(Nothing)
    End Sub

    Private Sub cgRooms_EditClick(sender As Object, e As EventArgs) Handles cgRooms.EditClick
        EditRoom(GetID(cgRooms))
    End Sub

    Private Sub cgRooms_RemoveClick(sender As Object, e As EventArgs) Handles cgRooms.RemoveClick

        Dim _RoomID As Guid? = GetID(cgRooms)
        If _RoomID.HasValue Then

            If CareMessage("Are you sure you want to Remove this Room?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Remove Room") = DialogResult.Yes Then

                Dim _SQL As String = ""

                _SQL = "delete from SiteRoomRatios where room_id = '" + _RoomID.Value.ToString + "'"
                DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                _SQL = "delete from SiteRooms where ID = '" + _RoomID.Value.ToString + "'"
                DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                cgRooms.RePopulate()

            End If

        End If

    End Sub

    Private Sub EditRoom(ByVal RoomID As Guid?)

        Dim _frm As New frmSiteRoom(m_Site._ID.Value, RoomID)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            DisplayRooms()
        End If

        _frm.Dispose()
        _frm = Nothing

    End Sub

    Private Sub EditRatio(ByVal RatioID As Guid?)

        Dim _RoomID As Guid? = GetID(cgRooms)
        If _RoomID.HasValue Then

            Dim _frm As New frmSiteRatio(_RoomID.Value, RatioID)
            _frm.ShowDialog()

            If _frm.DialogResult = DialogResult.OK Then
                DisplayRatios(_RoomID.ToString)
            End If

            _frm.Dispose()
            _frm = Nothing

        End If

    End Sub

    Private Function GetID(ByRef GridControl As Care.Controls.CareGridWithButtons) As Guid?

        If GridControl Is Nothing Then Return Nothing
        If GridControl.RecordCount < 0 Then Return Nothing
        If GridControl.CurrentRow Is Nothing Then Return Nothing

        Dim _ID As String = GridControl.CurrentRow.Item("ID").ToString

        Return New Guid(_ID)

    End Function

    Private Sub cgRatios_AddClick(sender As Object, e As EventArgs) Handles cgRatios.AddClick
        EditRatio(Nothing)
    End Sub

    Private Sub cgRatios_EditClick(sender As Object, e As EventArgs) Handles cgRatios.EditClick
        EditRatio(GetID(cgRatios))
    End Sub

    Private Sub cgRatios_RemoveClick(sender As Object, e As EventArgs) Handles cgRatios.RemoveClick

        Dim _RatioID As Guid? = GetID(cgRatios)
        If _RatioID.HasValue Then

            If CareMessage("Are you sure you want to Remove this Ratio?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Remove Ratio") = DialogResult.Yes Then

                Dim _SQL As String = ""
                _SQL = "delete from SiteRoomRatios where ID = '" + _RatioID.Value.ToString + "'"
                DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                cgRatios.RePopulate()

            End If

        End If

    End Sub

End Class
