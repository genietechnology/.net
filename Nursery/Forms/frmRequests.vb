﻿Imports Care.Global

Public Class frmRequests

    Dim m_Request As Business.Request

    Private Sub frmTariff_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.GridSQL = "select id, description as 'Description'" & _
                     " from Requests" & _
                     " order by description"

        gbx.Hide()

    End Sub

    Protected Overrides Sub SetBindings()

        m_Request = New Business.Request
        bs.DataSource = m_Request

        txtDescription.DataBindings.Add("Text", bs, "_Description")
        txtReportText.DataBindings.Add("Text", bs, "_ReportText")

    End Sub

    Protected Overrides Sub BindToID(ID As System.Guid, IsNew As Boolean)
        m_Request = Business.Request.RetreiveByID(ID)
        bs.DataSource = m_Request
    End Sub

    Protected Overrides Sub CommitUpdate()
        m_Request.Store()
        'Business.Request.SaveTariff(CType(bs.Item(bs.Position), Business.Tariff))
    End Sub
End Class
