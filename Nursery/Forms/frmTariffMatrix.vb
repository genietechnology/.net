﻿Imports Care.Global
Imports Care.Data
Imports DevExpress.XtraGrid.Columns

Public Class frmTariffMatrix

    Public Enum EnumValueMode
        Price
        Rate
    End Enum

    Public Enum EnumXMode
        Age
        Group
    End Enum

    Private m_ValueMode As EnumValueMode
    Private m_XMode As EnumXMode
    Private m_TariffID As Guid = Nothing
    Private m_TariffDesc As String = ""
    Private m_Classifications As New List(Of Classification)
    Private m_AgeBands As List(Of Business.TariffAgeBand) = Nothing
    Private m_Rooms As List(Of Business.SiteRoom) = Nothing
    Private m_MatrixData As List(Of Business.TariffMatrix) = Nothing
    Private m_SelectedMatrixRecord As Business.TariffMatrix = Nothing
    Private m_SiteID As Guid = Nothing

    Public Sub New(ByVal TariffID As Guid, ByVal TariffDesc As String, ByVal SiteID As Guid, ByVal ValueMode As EnumValueMode, ByVal XMode As EnumXMode)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_TariffID = TariffID
        m_TariffDesc = TariffDesc
        m_ValueMode = ValueMode
        m_XMode = XMode
        m_SiteID = SiteID

        Me.Text = "Matrix for " + m_TariffDesc

        If m_ValueMode = EnumValueMode.Price Then
            lblValue.Text = "Price"
        Else
            lblValue.Text = "Rate Per Hour"
        End If

    End Sub

    Private Sub frmTariffMatrix_Load(sender As Object, e As EventArgs) Handles Me.Load

        txtDescription.BackColor = Session.ChangeColour
        txtValue.BackColor = Session.ChangeColour
        gbxMatrix.Hide()

        'fetch and cache all the data from DB
        m_Classifications.Clear()
        m_AgeBands = Business.TariffAgeBand.RetreiveAll
        m_Rooms = Business.SiteRoom.RetreiveBySite(m_SiteID)

        m_MatrixData = Business.TariffMatrix.RetreiveAll

        grdMatrix.HideFirstColumn = True

        If m_XMode = EnumXMode.Age Then
            grdMatrix.Populate(m_AgeBands)
        Else
            grdMatrix.Populate(m_Rooms)
        End If

        'create the unbound columns for the Classifications...
        Dim _Classes As DataRowCollection = Care.Shared.ListHandler.ReturnItems("Classification")
        For Each _DR As DataRow In _Classes
            Dim _c As New Classification(_DR.Item(0).ToString, _DR.Item(1).ToString)
            m_Classifications.Add(_c)
            Dim _col As GridColumn = grdMatrix.UnderlyingGridView.Columns.AddVisible(_c.ID, _c.Description)
            _col.UnboundType = DevExpress.Data.UnboundColumnType.Decimal
            _col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            _col.DisplayFormat.FormatString = "0.00"
        Next

        If m_XMode = EnumXMode.Age Then
            grdMatrix.Columns("_Description").Caption = "Age Band"
            grdMatrix.Columns("_MinAge").Caption = "Minimum Age"
            grdMatrix.Columns("_MaxAge").Caption = "Maximum Age"
            grdMatrix.Columns("_MinAge").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        Else
            grdMatrix.Columns("_SiteId").Visible = False
            grdMatrix.Columns("_RoomName").Caption = "Room Name"
            grdMatrix.Columns("_RoomHoursFrom").Visible = False
            grdMatrix.Columns("_RoomHoursTo").Visible = False
            grdMatrix.Columns("_RoomCloseFrom").Visible = False
            grdMatrix.Columns("_RoomCloseTo").Visible = False
            grdMatrix.Columns("_RoomCheckStaff").Visible = False
            grdMatrix.Columns("_RoomCheckChildren").Visible = False
            grdMatrix.Columns("_RoomMonitor").Visible = False
            grdMatrix.Columns("_RoomMonitorSerial").Visible = False
            grdMatrix.Columns("_RoomColour").Visible = False
            grdMatrix.Columns("_RoomPhoto").Visible = False
            grdMatrix.Columns("_RoomSequence").Visible = False
        End If

        grdMatrix.Columns("IsNew").Visible = False
        grdMatrix.Columns("IsDeleted").Visible = False

    End Sub

    Private Sub grdMatrix_CustomUnboundColumnData(sender As Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs) Handles grdMatrix.CustomUnboundColumnData
        If e.IsGetData Then
            If e.Row IsNot Nothing Then

                Dim _c As String = e.Column.Caption
                Dim _bandID As Guid? = Nothing

                If m_XMode = EnumXMode.Age Then
                    Dim _ab As Business.TariffAgeBand = CType(e.Row, Business.TariffAgeBand)
                    If _ab IsNot Nothing Then
                        _bandID = _ab._ID.Value
                    End If
                Else
                    Dim _g As Business.SiteRoom = CType(e.Row, Business.SiteRoom)
                    If _g IsNot Nothing Then
                        _bandID = _g._ID
                    End If
                End If

                If _bandID.HasValue Then
                    Dim _m As Business.TariffMatrix = GetMatrixRecord(_bandID, _c)
                    If _m Is Nothing Then
                        e.Value = 0
                    Else
                        e.Value = GetMatrixRecord(_bandID, _c)._Value
                    End If
                Else
                    e.Value = 0
                End If

            Else
                e.Value = 0
            End If
        End If
    End Sub

    Private Function GetMatrixRecord(ByVal AgeBandID As Guid?, ByVal Classification As String) As Business.TariffMatrix
        Dim _Q As IEnumerable(Of Business.TariffMatrix) = From _M As Business.TariffMatrix In m_MatrixData _
                                                          Where _M._TariffId = m_TariffID _
                                                          And _M._AgeId.Value = AgeBandID _
                                                          And _M._Class = Classification
        If _Q IsNot Nothing Then
            If _Q.Count = 1 Then
                Return _Q.First
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function

    Private Sub grdMatrix_GridDoubleClick(sender As Object, e As EventArgs) Handles grdMatrix.GridDoubleClick
        AmendValue()
    End Sub

    Private Sub AmendValue()

        Dim _BandID As String = grdMatrix.GetRowCellValue(grdMatrix.UnderlyingGridView.FocusedRowHandle, "_ID").ToString

        Dim _BandName As String = ""
        If m_XMode = EnumXMode.Age Then
            _BandName = grdMatrix.GetRowCellValue(grdMatrix.UnderlyingGridView.FocusedRowHandle, "_Description").ToString
        Else
            _BandName = grdMatrix.GetRowCellValue(grdMatrix.UnderlyingGridView.FocusedRowHandle, "_RoomName").ToString
        End If

        Dim _Class As String = grdMatrix.UnderlyingGridView.FocusedColumn.Caption

        If Not IsClassificationSelected(_Class) Then Exit Sub

        Dim _M As Business.TariffMatrix = GetMatrixRecord(New Guid(_BandID), _Class)
        If _M IsNot Nothing Then
            m_SelectedMatrixRecord = _M
            txtDescription.Text = _M._InvoiceText
            txtValue.Text = Format(_M._Value, "0.00")
        Else
            m_SelectedMatrixRecord = Nothing
            txtDescription.Text = m_TariffDesc
            txtValue.Text = ""
        End If

        gbxMatrix.Text = "Amend " + m_ValueMode.ToString + " for " + _Class + ", " + _BandName

        grdMatrix.Enabled = False
        gbxMatrix.Show()

        txtValue.Focus()

    End Sub

    Private Function IsClassificationSelected(ByVal ClassName As String) As Boolean
        For Each _C In m_Classifications
            If _C.Description = ClassName Then Return True
        Next
        Return False
    End Function

    Private Sub btnAmendCancel_Click(sender As Object, e As EventArgs) Handles btnAmendCancel.Click
        grdMatrix.Enabled = True
        gbxMatrix.Hide()
    End Sub

    Private Sub btnAmendOK_Click(sender As Object, e As EventArgs) Handles btnAmendOK.Click

        If Not CheckFields() Then Exit Sub
        UpdateMatrix()

        grdMatrix.Enabled = True
        grdMatrix.UnderlyingGridView.RefreshRow(grdMatrix.UnderlyingGridView.FocusedRowHandle)

        gbxMatrix.Hide()

    End Sub

    Private Function CheckFields() As Boolean

        If txtDescription.Text = "" Then
            CareMessage("Please enter the Invoice Text.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If txtValue.Text = "" Then
            CareMessage("Please enter the Matrix Value.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If CDbl(txtValue.Text) <= 0 Then
            CareMessage("Please enter a valid Matrix Value.", MessageBoxIcon.Exclamation, "Invalid Value")
            Return False
        End If

        Return True

    End Function

    Private Sub UpdateMatrix()

        If m_SelectedMatrixRecord Is Nothing Then

            Dim _AgeID As String = grdMatrix.GetRowCellValue(grdMatrix.UnderlyingGridView.FocusedRowHandle, "_ID").ToString
            Dim _Class As String = grdMatrix.UnderlyingGridView.FocusedColumn.Caption

            Dim _M As New Business.TariffMatrix
            With _M
                ._TariffId = m_TariffID
                ._AgeId = New Guid(_AgeID)
                ._Class = _Class
                ._InvoiceText = txtDescription.Text
                ._Value = ValueHandler.ConvertDecimal(txtValue.Text)
            End With

            m_MatrixData.Add(_M)

        Else
            m_MatrixData.Remove(m_SelectedMatrixRecord)
            m_SelectedMatrixRecord._InvoiceText = txtDescription.Text
            m_SelectedMatrixRecord._Value = ValueHandler.ConvertDecimal(txtValue.Text)
            m_MatrixData.Add(m_SelectedMatrixRecord)
        End If

    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click

        Me.DialogResult = DialogResult.OK
        Business.TariffMatrix.SaveAll(m_MatrixData)
        Me.Close()

    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Class Classification
        Public ID As String
        Public Description As String
        Public Sub New(ByVal IDString As String, ByVal Desc As String)
            ID = IDString
            Description = Desc
        End Sub
    End Class

End Class
