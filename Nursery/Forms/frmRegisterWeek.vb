﻿Imports Care.Global
Imports Care.Shared

Public Class frmRegisterWeek

    Private Sub frmRegisterWeek_Load(sender As Object, e As EventArgs) Handles Me.Load

        cdtWC.Value = ValueHandler.NearestDate(Today, DayOfWeek.Monday, ValueHandler.EnumDirection.Backwards)

        With cbxSite
            .AllowBlank = True
            .PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)
            .SelectedIndex = 0
        End With

    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click

        If cdtWC.Text = "" Then Exit Sub

        If cdtWC.Value.Value.DayOfWeek <> DayOfWeek.Monday Then
            CareMessage("Week Commencing date must be a Monday.", MessageBoxIcon.Exclamation, "Refresh Grid")
            Exit Sub
        End If

        SetCMDs(False)
        Session.CursorWaiting()

        Dim _SQL As String = BuildSQL(cdtWC.Value.Value)

        If Debugger.IsAttached AndAlso My.Computer.Name = "JAMES-W530" Then
            cgRegister.Populate(Session.ConnectionString.Replace("dev", "finkley"), _SQL)
        Else
            cgRegister.Populate(Session.ConnectionString, _SQL)
        End If

        FormatGrid()

        SetCMDs(True)
        Session.CursorDefault()

    End Sub

    Private Sub FormatGrid()

        If cgRegister.Columns.Count <= 1 Then Exit Sub

        cgRegister.BeginUpdate()

        cgRegister.Columns("person_id").Visible = False
        cgRegister.Columns("wc").Caption = "WC"
        cgRegister.Columns("site_name").Caption = "Site"
        cgRegister.Columns("group_id").Visible = False
        cgRegister.Columns("group_name").Caption = "Room"
        cgRegister.Columns("fullname").Caption = "Name"
        cgRegister.Columns("job_title").Caption = "Job Title"

        cgRegister.Columns("mon_in").Caption = "Mon In"
        cgRegister.Columns("mon_out").Caption = "Mon Out"
        cgRegister.Columns("mon_hrs").Caption = "Mon Hrs"
        cgRegister.Columns("mon_min").Visible = False

        cgRegister.Columns("tue_in").Caption = "Tue In"
        cgRegister.Columns("tue_out").Caption = "Tue Out"
        cgRegister.Columns("tue_hrs").Caption = "Tue Hrs"
        cgRegister.Columns("tue_min").Visible = False

        cgRegister.Columns("wed_in").Caption = "Wed In"
        cgRegister.Columns("wed_out").Caption = "Wed Out"
        cgRegister.Columns("wed_hrs").Caption = "Wed Hrs"
        cgRegister.Columns("wed_min").Visible = False

        cgRegister.Columns("thu_in").Caption = "Thu In"
        cgRegister.Columns("thu_out").Caption = "Thu Out"
        cgRegister.Columns("thu_hrs").Caption = "Thu Hrs"
        cgRegister.Columns("thu_min").Visible = False

        cgRegister.Columns("fri_in").Caption = "Fri In"
        cgRegister.Columns("fri_out").Caption = "Fri Out"
        cgRegister.Columns("fri_hrs").Caption = "Fri Hrs"
        cgRegister.Columns("fri_min").Visible = False

        For Each _c As DevExpress.XtraGrid.Columns.GridColumn In cgRegister.Columns
            If _c.FieldName.Contains("_in") OrElse _c.FieldName.Contains("_out") Then
                _c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
                _c.DisplayFormat.FormatString = "HH:mm"
            End If
        Next

        cgRegister.AutoSizeColumns()

        cgRegister.EndUpdate()

    End Sub

    Private Function BuildSQL(ByVal WC As Date) As String

        Dim _SQL As String = ""

        _SQL += "select distinct r.person_id, cast(" + ValueHandler.SQLDate(WC, True) + " as date) as 'wc', s.site_name, s.group_id, s.group_name, s.fullname, s.job_title," + vbCrLf
        _SQL += BuildDay("mon") + "," + vbCrLf
        _SQL += BuildDay("tue") + "," + vbCrLf
        _SQL += BuildDay("wed") + "," + vbCrLf
        _SQL += BuildDay("thu") + "," + vbCrLf
        _SQL += BuildDay("fri") + vbCrLf
        _SQL += "from RegisterSummary r" + vbCrLf
        _SQL += "left join Staff s on s.ID = r.person_id" + vbCrLf
        _SQL += "left join RegisterSummary mon on mon.person_id = r.person_id and mon.date = " + ValueHandler.SQLDate(WC, True) + vbCrLf
        _SQL += "left join RegisterSummary tue on tue.person_id = r.person_id and tue.Date = " + ValueHandler.SQLDate(DateAdd(DateInterval.Day, 1, WC), True) + vbCrLf
        _SQL += "left join RegisterSummary wed on wed.person_id = r.person_id and wed.date = " + ValueHandler.SQLDate(DateAdd(DateInterval.Day, 2, WC), True) + vbCrLf
        _SQL += "left join RegisterSummary thu on thu.person_id = r.person_id and thu.Date = " + ValueHandler.SQLDate(DateAdd(DateInterval.Day, 3, WC), True) + vbCrLf
        _SQL += "left join RegisterSummary fri on fri.person_id = r.person_id and fri.date = " + ValueHandler.SQLDate(DateAdd(DateInterval.Day, 4, WC), True) + vbCrLf
        _SQL += "where r.Date between " + ValueHandler.SQLDate(WC, True) + " and " + ValueHandler.SQLDate(DateAdd(DateInterval.Day, 4, WC), True) + vbCrLf
        _SQL += "and r.person_type = 'S'" + vbCrLf

        If cbxSite.Text <> "" Then
            _SQL += "and s.site_name = '" + cbxSite.Text + "'" + vbCrLf
        End If

        Return _SQL

    End Function

    Private Function BuildDay(ByVal DayAbrv As String) As String

        'mon.first_stamp as 'mon_in', mon.last_stamp as 'mon_out', datediff(SECOND, mon.first_stamp, mon.last_stamp) / 3600.0 as 'mon_hrs', datediff(MINUTE, mon.first_stamp, mon.last_stamp) as 'mon_min'

        Dim _SQL As String = ""

        _SQL += DayAbrv + ".first_stamp as '" + DayAbrv + "_in', " + DayAbrv + ".last_stamp as '" + DayAbrv + "_out',"
        _SQL += " isnull((datediff(SECOND, " + DayAbrv + ".first_stamp, " + DayAbrv + ".last_stamp) / 3600.0),0) as '" + DayAbrv + "_hrs',"
        _SQL += " isnull(datediff(MINUTE, " + DayAbrv + ".first_stamp, " + DayAbrv + ".last_stamp),0) as '" + DayAbrv + "_min'"

        Return _SQL

    End Function

    Private Sub btnHoursByWeek_Click(sender As Object, e As EventArgs) Handles btnHoursByWeek.Click

        If cdtWC.Text = "" Then Exit Sub

        If cdtWC.Value.Value.DayOfWeek <> DayOfWeek.Monday Then
            CareMessage("Week Commencing date must be a Monday.", MessageBoxIcon.Exclamation, "Refresh Grid")
            Exit Sub
        End If

        SetCMDs(False)
        Session.CursorWaiting()

        Dim _cs As String = Session.ConnectionString
        If Debugger.IsAttached AndAlso My.Computer.Name = "JAMES-W530" Then
            _cs = Session.ConnectionString.Replace("dev", "finkley")
        End If

        Dim _SQL As String = BuildSQL(cdtWC.Value.Value)

        If btnHoursByWeek.ShiftDown Then
            ReportHandler.DesignReport("StaffHoursByWeek.repx", _cs, _SQL)
        Else
            ReportHandler.RunReport("StaffHoursByWeek.repx", _cs, _SQL)
        End If

        SetCMDs(True)
        Session.CursorDefault()

    End Sub

    Private Sub SetCMDs(ByVal Enabled As Boolean)
        btnRefresh.Enabled = Enabled
        btnHoursByWeek.Enabled = Enabled
    End Sub

End Class