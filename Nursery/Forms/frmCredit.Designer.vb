﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmCredit
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnRemove = New Care.Controls.CareButton(Me.components)
        Me.btnAdd = New Care.Controls.CareButton(Me.components)
        Me.btnEdit = New Care.Controls.CareButton(Me.components)
        Me.btnSave = New Care.Controls.CareButton(Me.components)
        Me.btnPrint = New Care.Controls.CareButton(Me.components)
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        Me.GroupControl2 = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.txtTOTAL = New Care.Controls.CareTextBox(Me.components)
        Me.grdItems = New Care.Controls.CareGrid()
        Me.gbxHeader = New Care.Controls.CareFrame(Me.components)
        Me.btnFamily = New Care.Controls.CareButton(Me.components)
        Me.txtStatus = New Care.Controls.CareTextBox(Me.components)
        Me.btnSessions = New Care.Controls.CareButton(Me.components)
        Me.txtChild = New Care.Controls.CareTextBox(Me.components)
        Me.txtInvoiceNo = New Care.Controls.CareTextBox(Me.components)
        Me.txtInvoiceDate = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.txtAddress = New Care.Address.CareAddress()
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.btnFinancials = New Care.Controls.CareButton(Me.components)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.txtTOTAL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxHeader.SuspendLayout()
        CType(Me.txtStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtChild.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtInvoiceNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtInvoiceDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'btnRemove
        '
        Me.btnRemove.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnRemove.Appearance.Options.UseFont = True
        Me.btnRemove.Location = New System.Drawing.Point(194, 161)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(85, 23)
        Me.btnRemove.TabIndex = 3
        Me.btnRemove.Text = "Remove Item"
        '
        'btnAdd
        '
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnAdd.Appearance.Options.UseFont = True
        Me.btnAdd.Location = New System.Drawing.Point(12, 161)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(85, 23)
        Me.btnAdd.TabIndex = 1
        Me.btnAdd.Text = "Add Item"
        '
        'btnEdit
        '
        Me.btnEdit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnEdit.Appearance.Options.UseFont = True
        Me.btnEdit.Location = New System.Drawing.Point(103, 161)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(85, 23)
        Me.btnEdit.TabIndex = 2
        Me.btnEdit.Text = "Edit Item"
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnSave.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSave.Appearance.Options.UseFont = True
        Me.btnSave.Location = New System.Drawing.Point(647, 669)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(85, 23)
        Me.btnSave.TabIndex = 12
        Me.btnSave.Text = "Save && Close"
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnPrint.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnPrint.Appearance.Options.UseFont = True
        Me.btnPrint.Location = New System.Drawing.Point(12, 669)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(125, 23)
        Me.btnPrint.TabIndex = 8
        Me.btnPrint.Text = "Print Preview"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.DialogResult = DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(738, 669)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 23)
        Me.btnCancel.TabIndex = 13
        Me.btnCancel.Text = "Close"
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType(((AnchorStyles.Bottom Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl2.Controls.Add(Me.CareLabel8)
        Me.GroupControl2.Controls.Add(Me.txtTOTAL)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 630)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.ShowCaption = False
        Me.GroupControl2.Size = New System.Drawing.Size(811, 33)
        Me.GroupControl2.TabIndex = 7
        Me.GroupControl2.Text = "GroupControl2"
        '
        'CareLabel8
        '
        Me.CareLabel8.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.CareLabel8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.CareLabel8.Location = New System.Drawing.Point(645, 8)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(75, 15)
        Me.CareLabel8.TabIndex = 4
        Me.CareLabel8.Text = "TOTAL"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTOTAL
        '
        Me.txtTOTAL.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.txtTOTAL.CharacterCasing = CharacterCasing.Normal
        Me.txtTOTAL.EnterMoveNextControl = True
        Me.txtTOTAL.Location = New System.Drawing.Point(726, 5)
        Me.txtTOTAL.MaxLength = 0
        Me.txtTOTAL.Name = "txtTOTAL"
        Me.txtTOTAL.NumericAllowNegatives = False
        Me.txtTOTAL.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtTOTAL.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTOTAL.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtTOTAL.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTOTAL.Properties.Appearance.Options.UseFont = True
        Me.txtTOTAL.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTOTAL.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtTOTAL.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTOTAL.Properties.ReadOnly = True
        Me.txtTOTAL.Size = New System.Drawing.Size(75, 20)
        Me.txtTOTAL.TabIndex = 7
        Me.txtTOTAL.TabStop = False
        Me.txtTOTAL.TextAlign = HorizontalAlignment.Right
        Me.txtTOTAL.ToolTipText = ""
        '
        'grdItems
        '
        Me.grdItems.AllowBuildColumns = False
        Me.grdItems.AllowEdit = False
        Me.grdItems.AllowHorizontalScroll = False
        Me.grdItems.AllowMultiSelect = False
        Me.grdItems.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.grdItems.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdItems.Appearance.Options.UseFont = True
        Me.grdItems.AutoSizeByData = True
        Me.grdItems.DisableAutoSize = False
        Me.grdItems.DisableDataFormatting = False
        Me.grdItems.FocusedRowHandle = -2147483648
        Me.grdItems.HideFirstColumn = False
        Me.grdItems.Location = New System.Drawing.Point(12, 190)
        Me.grdItems.Name = "grdItems"
        Me.grdItems.PreviewColumn = ""
        Me.grdItems.QueryID = Nothing
        Me.grdItems.RowAutoHeight = True
        Me.grdItems.SearchAsYouType = True
        Me.grdItems.ShowAutoFilterRow = False
        Me.grdItems.ShowFindPanel = False
        Me.grdItems.ShowGroupByBox = False
        Me.grdItems.ShowLoadingPanel = False
        Me.grdItems.ShowNavigator = True
        Me.grdItems.Size = New System.Drawing.Size(811, 434)
        Me.grdItems.TabIndex = 6
        Me.grdItems.TabStop = False
        '
        'gbxHeader
        '
        Me.gbxHeader.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.gbxHeader.Controls.Add(Me.btnFamily)
        Me.gbxHeader.Controls.Add(Me.txtStatus)
        Me.gbxHeader.Controls.Add(Me.btnSessions)
        Me.gbxHeader.Controls.Add(Me.txtChild)
        Me.gbxHeader.Controls.Add(Me.txtInvoiceNo)
        Me.gbxHeader.Controls.Add(Me.txtInvoiceDate)
        Me.gbxHeader.Controls.Add(Me.CareLabel5)
        Me.gbxHeader.Controls.Add(Me.txtAddress)
        Me.gbxHeader.Controls.Add(Me.CareLabel4)
        Me.gbxHeader.Controls.Add(Me.CareLabel2)
        Me.gbxHeader.Controls.Add(Me.CareLabel1)
        Me.gbxHeader.Location = New System.Drawing.Point(12, 12)
        Me.gbxHeader.Name = "gbxHeader"
        Me.gbxHeader.Size = New System.Drawing.Size(811, 143)
        Me.gbxHeader.TabIndex = 0
        Me.gbxHeader.Text = "Credit Header"
        '
        'btnFamily
        '
        Me.btnFamily.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnFamily.Appearance.Options.UseFont = True
        Me.btnFamily.Location = New System.Drawing.Point(362, 83)
        Me.btnFamily.Name = "btnFamily"
        Me.btnFamily.Size = New System.Drawing.Size(99, 23)
        Me.btnFamily.TabIndex = 15
        Me.btnFamily.Text = "View Family"
        '
        'txtStatus
        '
        Me.txtStatus.CharacterCasing = CharacterCasing.Normal
        Me.txtStatus.EnterMoveNextControl = True
        Me.txtStatus.Location = New System.Drawing.Point(182, 28)
        Me.txtStatus.MaxLength = 0
        Me.txtStatus.Name = "txtStatus"
        Me.txtStatus.NumericAllowNegatives = False
        Me.txtStatus.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtStatus.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStatus.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtStatus.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtStatus.Properties.Appearance.Options.UseFont = True
        Me.txtStatus.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStatus.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtStatus.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtStatus.Properties.ReadOnly = True
        Me.txtStatus.Size = New System.Drawing.Size(174, 22)
        Me.txtStatus.TabIndex = 2
        Me.txtStatus.TabStop = False
        Me.txtStatus.TextAlign = HorizontalAlignment.Left
        Me.txtStatus.ToolTipText = ""
        '
        'btnSessions
        '
        Me.btnSessions.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSessions.Appearance.Options.UseFont = True
        Me.btnSessions.Location = New System.Drawing.Point(362, 111)
        Me.btnSessions.Name = "btnSessions"
        Me.btnSessions.Size = New System.Drawing.Size(99, 23)
        Me.btnSessions.TabIndex = 16
        Me.btnSessions.Text = "View Sessions"
        '
        'txtChild
        '
        Me.txtChild.CharacterCasing = CharacterCasing.Normal
        Me.txtChild.EnterMoveNextControl = True
        Me.txtChild.Location = New System.Drawing.Point(110, 84)
        Me.txtChild.MaxLength = 0
        Me.txtChild.Name = "txtChild"
        Me.txtChild.NumericAllowNegatives = False
        Me.txtChild.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtChild.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtChild.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtChild.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtChild.Properties.Appearance.Options.UseFont = True
        Me.txtChild.Properties.Appearance.Options.UseTextOptions = True
        Me.txtChild.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtChild.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtChild.Properties.ReadOnly = True
        Me.txtChild.Size = New System.Drawing.Size(246, 22)
        Me.txtChild.TabIndex = 12
        Me.txtChild.TabStop = False
        Me.txtChild.TextAlign = HorizontalAlignment.Left
        Me.txtChild.ToolTipText = ""
        '
        'txtInvoiceNo
        '
        Me.txtInvoiceNo.CharacterCasing = CharacterCasing.Normal
        Me.txtInvoiceNo.EnterMoveNextControl = True
        Me.txtInvoiceNo.Location = New System.Drawing.Point(110, 28)
        Me.txtInvoiceNo.MaxLength = 0
        Me.txtInvoiceNo.Name = "txtInvoiceNo"
        Me.txtInvoiceNo.NumericAllowNegatives = False
        Me.txtInvoiceNo.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtInvoiceNo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtInvoiceNo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtInvoiceNo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtInvoiceNo.Properties.Appearance.Options.UseFont = True
        Me.txtInvoiceNo.Properties.Appearance.Options.UseTextOptions = True
        Me.txtInvoiceNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtInvoiceNo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtInvoiceNo.Properties.ReadOnly = True
        Me.txtInvoiceNo.Size = New System.Drawing.Size(66, 22)
        Me.txtInvoiceNo.TabIndex = 1
        Me.txtInvoiceNo.TabStop = False
        Me.txtInvoiceNo.TextAlign = HorizontalAlignment.Left
        Me.txtInvoiceNo.ToolTipText = ""
        '
        'txtInvoiceDate
        '
        Me.txtInvoiceDate.CharacterCasing = CharacterCasing.Normal
        Me.txtInvoiceDate.EnterMoveNextControl = True
        Me.txtInvoiceDate.Location = New System.Drawing.Point(110, 56)
        Me.txtInvoiceDate.MaxLength = 0
        Me.txtInvoiceDate.Name = "txtInvoiceDate"
        Me.txtInvoiceDate.NumericAllowNegatives = False
        Me.txtInvoiceDate.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtInvoiceDate.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtInvoiceDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtInvoiceDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtInvoiceDate.Properties.Appearance.Options.UseFont = True
        Me.txtInvoiceDate.Properties.Appearance.Options.UseTextOptions = True
        Me.txtInvoiceDate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtInvoiceDate.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtInvoiceDate.Properties.ReadOnly = True
        Me.txtInvoiceDate.Size = New System.Drawing.Size(66, 22)
        Me.txtInvoiceDate.TabIndex = 4
        Me.txtInvoiceDate.TabStop = False
        Me.txtInvoiceDate.TextAlign = HorizontalAlignment.Left
        Me.txtInvoiceDate.ToolTipText = ""
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(419, 31)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(42, 15)
        Me.CareLabel5.TabIndex = 13
        Me.CareLabel5.Text = "Address"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAddress
        '
        Me.txtAddress.Address = ""
        Me.txtAddress.AddressBlock = ""
        Me.txtAddress.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtAddress.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.Appearance.Options.UseFont = True
        Me.txtAddress.County = ""
        Me.txtAddress.DisplayMode = Care.Address.CareAddress.EnumDisplayMode.SingleControl
        Me.txtAddress.Location = New System.Drawing.Point(467, 29)
        Me.txtAddress.MaxLength = 500
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.PostCode = ""
        Me.txtAddress.ReadOnly = True
        Me.txtAddress.Size = New System.Drawing.Size(333, 105)
        Me.txtAddress.TabIndex = 14
        Me.txtAddress.Tag = ""
        Me.txtAddress.Town = ""
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(11, 87)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(28, 15)
        Me.CareLabel4.TabIndex = 11
        Me.CareLabel4.Text = "Child"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(11, 59)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(59, 15)
        Me.CareLabel2.TabIndex = 3
        Me.CareLabel2.Text = "Credit Date"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(11, 31)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(51, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Credit No"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnFinancials
        '
        Me.btnFinancials.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnFinancials.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnFinancials.Appearance.Options.UseFont = True
        Me.btnFinancials.Location = New System.Drawing.Point(143, 669)
        Me.btnFinancials.Name = "btnFinancials"
        Me.btnFinancials.Size = New System.Drawing.Size(125, 23)
        Me.btnFinancials.TabIndex = 11
        Me.btnFinancials.Text = "Financials Credit"
        '
        'frmCredit
        '
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(834, 701)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnFinancials)
        Me.Controls.Add(Me.btnRemove)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.grdItems)
        Me.Controls.Add(Me.gbxHeader)
        Me.FormBorderStyle = FormBorderStyle.Sizable
        Me.MinimumSize = New System.Drawing.Size(850, 740)
        Me.Name = "frmCredit"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "Amend Credit"
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.txtTOTAL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxHeader.ResumeLayout(False)
        Me.gbxHeader.PerformLayout()
        CType(Me.txtStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtChild.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtInvoiceNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtInvoiceDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents txtAddress As Care.Address.CareAddress
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents grdItems As Care.Controls.CareGrid
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents txtTOTAL As Care.Controls.CareTextBox
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents btnPrint As Care.Controls.CareButton
    Friend WithEvents btnSave As Care.Controls.CareButton
    Friend WithEvents btnAdd As Care.Controls.CareButton
    Friend WithEvents btnEdit As Care.Controls.CareButton
    Friend WithEvents btnRemove As Care.Controls.CareButton
    Friend WithEvents txtChild As Care.Controls.CareTextBox
    Friend WithEvents txtInvoiceNo As Care.Controls.CareTextBox
    Friend WithEvents txtInvoiceDate As Care.Controls.CareTextBox
    Friend WithEvents btnFinancials As Care.Controls.CareButton
    Friend WithEvents btnSessions As Care.Controls.CareButton
    Friend WithEvents txtStatus As Care.Controls.CareTextBox
    Friend WithEvents btnFamily As Care.Controls.CareButton
    Friend WithEvents gbxHeader As Care.Controls.CareFrame
    Friend WithEvents GroupControl2 As Care.Controls.CareFrame
End Class
