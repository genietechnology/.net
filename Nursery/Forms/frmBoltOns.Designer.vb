﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBoltOns
    Inherits Care.Shared.frmGridMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label9 = New Care.Controls.CareLabel()
        Me.cbxCategory = New Care.Controls.CareComboBox()
        Me.txtName = New Care.Controls.CareTextBox()
        Me.Label8 = New Care.Controls.CareLabel()
        Me.Label2 = New Care.Controls.CareLabel()
        Me.txtValue = New Care.Controls.CareTextBox()
        Me.CareLabel1 = New Care.Controls.CareLabel()
        Me.cbxNLCode = New Care.Controls.CareComboBox()
        Me.CareLabel2 = New Care.Controls.CareLabel()
        Me.cbxNLTracking = New Care.Controls.CareComboBox()
        Me.chkDiscounts = New Care.Controls.CareCheckBox()
        Me.CareLabel3 = New Care.Controls.CareLabel()
        Me.CareLabel4 = New Care.Controls.CareLabel()
        Me.cbxScope = New Care.Controls.CareComboBox()
        Me.CareLabel5 = New Care.Controls.CareLabel()
        Me.cbxSummaryColumn = New Care.Controls.CareComboBox()
        Me.CareLabel6 = New Care.Controls.CareLabel()
        Me.chkChargeClosed = New Care.Controls.CareCheckBox()
        Me.CareLabel7 = New Care.Controls.CareLabel()
        Me.chkChargeHolidays = New Care.Controls.CareCheckBox()
        Me.CareLabel8 = New Care.Controls.CareLabel()
        Me.gbx.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.cbxCategory.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxNLCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxNLTracking.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkDiscounts.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxScope.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSummaryColumn.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkChargeClosed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkChargeHolidays.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbx
        '
        Me.gbx.Anchor = AnchorStyles.None
        Me.gbx.Controls.Add(Me.CareLabel8)
        Me.gbx.Controls.Add(Me.CareLabel7)
        Me.gbx.Controls.Add(Me.chkChargeHolidays)
        Me.gbx.Controls.Add(Me.CareLabel6)
        Me.gbx.Controls.Add(Me.chkChargeClosed)
        Me.gbx.Controls.Add(Me.CareLabel5)
        Me.gbx.Controls.Add(Me.cbxSummaryColumn)
        Me.gbx.Controls.Add(Me.CareLabel4)
        Me.gbx.Controls.Add(Me.cbxScope)
        Me.gbx.Controls.Add(Me.CareLabel3)
        Me.gbx.Controls.Add(Me.chkDiscounts)
        Me.gbx.Controls.Add(Me.CareLabel2)
        Me.gbx.Controls.Add(Me.cbxNLTracking)
        Me.gbx.Controls.Add(Me.CareLabel1)
        Me.gbx.Controls.Add(Me.cbxNLCode)
        Me.gbx.Controls.Add(Me.txtValue)
        Me.gbx.Controls.Add(Me.Label9)
        Me.gbx.Controls.Add(Me.cbxCategory)
        Me.gbx.Controls.Add(Me.txtName)
        Me.gbx.Controls.Add(Me.Label8)
        Me.gbx.Controls.Add(Me.Label2)
        Me.gbx.Location = New System.Drawing.Point(204, 56)
        Me.gbx.Size = New System.Drawing.Size(427, 332)
        Me.gbx.TabIndex = 1
        Me.gbx.Controls.SetChildIndex(Me.Panel2, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label2, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label8, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtName, 0)
        Me.gbx.Controls.SetChildIndex(Me.cbxCategory, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label9, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtValue, 0)
        Me.gbx.Controls.SetChildIndex(Me.cbxNLCode, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel1, 0)
        Me.gbx.Controls.SetChildIndex(Me.cbxNLTracking, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel2, 0)
        Me.gbx.Controls.SetChildIndex(Me.chkDiscounts, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel3, 0)
        Me.gbx.Controls.SetChildIndex(Me.cbxScope, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel4, 0)
        Me.gbx.Controls.SetChildIndex(Me.cbxSummaryColumn, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel5, 0)
        Me.gbx.Controls.SetChildIndex(Me.chkChargeClosed, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel6, 0)
        Me.gbx.Controls.SetChildIndex(Me.chkChargeHolidays, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel7, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel8, 0)
        '
        'Panel2
        '
        Me.Panel2.Location = New System.Drawing.Point(3, 301)
        Me.Panel2.Size = New System.Drawing.Size(421, 28)
        Me.Panel2.TabIndex = 21
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(236, 0)
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(327, 0)
        '
        'Grid
        '
        Me.Grid.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Grid.Appearance.Options.UseFont = True
        Me.Grid.Size = New System.Drawing.Size(815, 408)
        Me.Grid.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(0, 426)
        Me.Panel1.Size = New System.Drawing.Size(834, 35)
        '
        'btnDelete
        '
        Me.btnDelete.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Appearance.Options.UseFont = True
        '
        'btnEdit
        '
        Me.btnEdit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.Appearance.Options.UseFont = True
        '
        'btnAdd
        '
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.Appearance.Options.UseFont = True
        '
        'btnClose
        '
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(1100, 5)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'Label9
        '
        Me.Label9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label9.Location = New System.Drawing.Point(8, 22)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(48, 15)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Category"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxCategory
        '
        Me.cbxCategory.AllowBlank = False
        Me.cbxCategory.DataSource = Nothing
        Me.cbxCategory.DisplayMember = Nothing
        Me.cbxCategory.EnterMoveNextControl = True
        Me.cbxCategory.Location = New System.Drawing.Point(121, 19)
        Me.cbxCategory.Name = "cbxCategory"
        Me.cbxCategory.Properties.AccessibleName = "Category"
        Me.cbxCategory.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxCategory.Properties.Appearance.Options.UseFont = True
        Me.cbxCategory.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxCategory.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxCategory.SelectedValue = Nothing
        Me.cbxCategory.Size = New System.Drawing.Size(297, 22)
        Me.cbxCategory.TabIndex = 1
        Me.cbxCategory.Tag = "AEM"
        Me.cbxCategory.ValueMember = Nothing
        '
        'txtName
        '
        Me.txtName.CharacterCasing = CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(121, 47)
        Me.txtName.MaxLength = 30
        Me.txtName.Name = "txtName"
        Me.txtName.NumericAllowNegatives = False
        Me.txtName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AccessibleName = "Description"
        Me.txtName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtName.Properties.MaxLength = 30
        Me.txtName.Size = New System.Drawing.Size(297, 22)
        Me.txtName.TabIndex = 3
        Me.txtName.Tag = "AEM"
        Me.txtName.TextAlign = HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'Label8
        '
        Me.Label8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label8.Location = New System.Drawing.Point(8, 50)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(32, 15)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "Name"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label2.Location = New System.Drawing.Point(8, 78)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(29, 15)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Value"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtValue
        '
        Me.txtValue.CharacterCasing = CharacterCasing.Normal
        Me.txtValue.EnterMoveNextControl = True
        Me.txtValue.Location = New System.Drawing.Point(121, 75)
        Me.txtValue.MaxLength = 8
        Me.txtValue.Name = "txtValue"
        Me.txtValue.NumericAllowNegatives = True
        Me.txtValue.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtValue.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtValue.Properties.AccessibleName = "Value"
        Me.txtValue.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtValue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtValue.Properties.Appearance.Options.UseFont = True
        Me.txtValue.Properties.Appearance.Options.UseTextOptions = True
        Me.txtValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtValue.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txtValue.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtValue.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtValue.Properties.MaxLength = 8
        Me.txtValue.Size = New System.Drawing.Size(82, 22)
        Me.txtValue.TabIndex = 5
        Me.txtValue.Tag = "AEM"
        Me.txtValue.TextAlign = HorizontalAlignment.Left
        Me.txtValue.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(8, 246)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(46, 15)
        Me.CareLabel1.TabIndex = 17
        Me.CareLabel1.Text = "NL Code"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxNLCode
        '
        Me.cbxNLCode.AllowBlank = False
        Me.cbxNLCode.DataSource = Nothing
        Me.cbxNLCode.DisplayMember = Nothing
        Me.cbxNLCode.EnterMoveNextControl = True
        Me.cbxNLCode.Location = New System.Drawing.Point(121, 243)
        Me.cbxNLCode.Name = "cbxNLCode"
        Me.cbxNLCode.Properties.AccessibleName = ""
        Me.cbxNLCode.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxNLCode.Properties.Appearance.Options.UseFont = True
        Me.cbxNLCode.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxNLCode.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxNLCode.SelectedValue = Nothing
        Me.cbxNLCode.Size = New System.Drawing.Size(172, 22)
        Me.cbxNLCode.TabIndex = 18
        Me.cbxNLCode.Tag = "AE"
        Me.cbxNLCode.ValueMember = Nothing
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(8, 274)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(64, 15)
        Me.CareLabel2.TabIndex = 19
        Me.CareLabel2.Text = "NL Tracking"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxNLTracking
        '
        Me.cbxNLTracking.AllowBlank = False
        Me.cbxNLTracking.DataSource = Nothing
        Me.cbxNLTracking.DisplayMember = Nothing
        Me.cbxNLTracking.EnterMoveNextControl = True
        Me.cbxNLTracking.Location = New System.Drawing.Point(121, 271)
        Me.cbxNLTracking.Name = "cbxNLTracking"
        Me.cbxNLTracking.Properties.AccessibleName = ""
        Me.cbxNLTracking.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxNLTracking.Properties.Appearance.Options.UseFont = True
        Me.cbxNLTracking.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxNLTracking.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxNLTracking.SelectedValue = Nothing
        Me.cbxNLTracking.Size = New System.Drawing.Size(172, 22)
        Me.cbxNLTracking.TabIndex = 20
        Me.cbxNLTracking.Tag = "AE"
        Me.cbxNLTracking.ValueMember = Nothing
        '
        'chkDiscounts
        '
        Me.chkDiscounts.EnterMoveNextControl = True
        Me.chkDiscounts.Location = New System.Drawing.Point(121, 132)
        Me.chkDiscounts.Name = "chkDiscounts"
        Me.chkDiscounts.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDiscounts.Properties.Appearance.Options.UseFont = True
        Me.chkDiscounts.Size = New System.Drawing.Size(20, 19)
        Me.chkDiscounts.TabIndex = 9
        Me.chkDiscounts.Tag = "AE"
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(8, 134)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(85, 15)
        Me.CareLabel3.TabIndex = 8
        Me.CareLabel3.Text = "Allow Discounts"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(8, 106)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel4.TabIndex = 6
        Me.CareLabel4.Text = "Scope"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxScope
        '
        Me.cbxScope.AllowBlank = False
        Me.cbxScope.DataSource = Nothing
        Me.cbxScope.DisplayMember = Nothing
        Me.cbxScope.EnterMoveNextControl = True
        Me.cbxScope.Location = New System.Drawing.Point(121, 103)
        Me.cbxScope.Name = "cbxScope"
        Me.cbxScope.Properties.AccessibleName = "Scope"
        Me.cbxScope.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxScope.Properties.Appearance.Options.UseFont = True
        Me.cbxScope.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxScope.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxScope.SelectedValue = Nothing
        Me.cbxScope.Size = New System.Drawing.Size(297, 22)
        Me.cbxScope.TabIndex = 7
        Me.cbxScope.Tag = "AEM"
        Me.cbxScope.ValueMember = Nothing
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(8, 218)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(97, 15)
        Me.CareLabel5.TabIndex = 15
        Me.CareLabel5.Text = "Summary Column"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxSummaryColumn
        '
        Me.cbxSummaryColumn.AllowBlank = False
        Me.cbxSummaryColumn.DataSource = Nothing
        Me.cbxSummaryColumn.DisplayMember = Nothing
        Me.cbxSummaryColumn.EnterMoveNextControl = True
        Me.cbxSummaryColumn.Location = New System.Drawing.Point(121, 215)
        Me.cbxSummaryColumn.Name = "cbxSummaryColumn"
        Me.cbxSummaryColumn.Properties.AccessibleName = "Summary Column"
        Me.cbxSummaryColumn.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxSummaryColumn.Properties.Appearance.Options.UseFont = True
        Me.cbxSummaryColumn.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSummaryColumn.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSummaryColumn.SelectedValue = Nothing
        Me.cbxSummaryColumn.Size = New System.Drawing.Size(82, 22)
        Me.cbxSummaryColumn.TabIndex = 16
        Me.cbxSummaryColumn.Tag = "AEM"
        Me.cbxSummaryColumn.ValueMember = Nothing
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(8, 162)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(109, 15)
        Me.CareLabel6.TabIndex = 10
        Me.CareLabel6.Text = "Charge when Closed"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkChargeClosed
        '
        Me.chkChargeClosed.EnterMoveNextControl = True
        Me.chkChargeClosed.Location = New System.Drawing.Point(121, 160)
        Me.chkChargeClosed.Name = "chkChargeClosed"
        Me.chkChargeClosed.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkChargeClosed.Properties.Appearance.Options.UseFont = True
        Me.chkChargeClosed.Size = New System.Drawing.Size(20, 19)
        Me.chkChargeClosed.TabIndex = 11
        Me.chkChargeClosed.Tag = "AE"
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(8, 190)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(99, 15)
        Me.CareLabel7.TabIndex = 12
        Me.CareLabel7.Text = "Charge on Holiday"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkChargeHolidays
        '
        Me.chkChargeHolidays.EnterMoveNextControl = True
        Me.chkChargeHolidays.Location = New System.Drawing.Point(121, 188)
        Me.chkChargeHolidays.Name = "chkChargeHolidays"
        Me.chkChargeHolidays.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkChargeHolidays.Properties.Appearance.Options.UseFont = True
        Me.chkChargeHolidays.Size = New System.Drawing.Size(20, 19)
        Me.chkChargeHolidays.TabIndex = 13
        Me.chkChargeHolidays.Tag = "AE"
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(147, 190)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(189, 15)
        Me.CareLabel8.TabIndex = 14
        Me.CareLabel8.Text = "(Child Holidays, not Term Holidays)"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmBoltOns
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(834, 461)
        Me.MinimumSize = New System.Drawing.Size(440, 441)
        Me.Name = "frmBoltOns"
        Me.gbx.ResumeLayout(False)
        Me.gbx.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.cbxCategory.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxNLCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxNLTracking.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkDiscounts.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxScope.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSummaryColumn.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkChargeClosed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkChargeHolidays.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label9 As Care.Controls.CareLabel
    Friend WithEvents cbxCategory As Care.Controls.CareComboBox
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents Label8 As Care.Controls.CareLabel
    Friend WithEvents Label2 As Care.Controls.CareLabel
    Friend WithEvents txtValue As Care.Controls.CareTextBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents cbxNLTracking As Care.Controls.CareComboBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents cbxNLCode As Care.Controls.CareComboBox
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents chkDiscounts As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents cbxScope As Care.Controls.CareComboBox
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents cbxSummaryColumn As Care.Controls.CareComboBox
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents chkChargeClosed As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents chkChargeHolidays As Care.Controls.CareCheckBox

End Class
