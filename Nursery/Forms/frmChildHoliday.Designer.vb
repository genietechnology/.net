﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChildHoliday
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.gbxWeekly = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.chkToHalf = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.chkFromHalf = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.cdtEndDate = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.txtDiscount = New Care.Controls.CareTextBox(Me.components)
        Me.cdtStartDate = New Care.Controls.CareDateTime(Me.components)
        Me.btnOK = New Care.Controls.CareButton(Me.components)
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        Me.GroupControl1 = New Care.Controls.CareFrame(Me.components)
        Me.txtNotes = New DevExpress.XtraEditors.MemoEdit()
        CType(Me.gbxWeekly, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxWeekly.SuspendLayout()
        CType(Me.chkToHalf.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkFromHalf.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtEndDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtEndDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDiscount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtStartDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtStartDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtNotes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'gbxWeekly
        '
        Me.gbxWeekly.Controls.Add(Me.CareLabel6)
        Me.gbxWeekly.Controls.Add(Me.chkToHalf)
        Me.gbxWeekly.Controls.Add(Me.CareLabel5)
        Me.gbxWeekly.Controls.Add(Me.chkFromHalf)
        Me.gbxWeekly.Controls.Add(Me.CareLabel2)
        Me.gbxWeekly.Controls.Add(Me.cdtEndDate)
        Me.gbxWeekly.Controls.Add(Me.CareLabel14)
        Me.gbxWeekly.Controls.Add(Me.CareLabel11)
        Me.gbxWeekly.Controls.Add(Me.txtDiscount)
        Me.gbxWeekly.Controls.Add(Me.cdtStartDate)
        Me.gbxWeekly.Location = New System.Drawing.Point(12, 12)
        Me.gbxWeekly.Name = "gbxWeekly"
        Me.gbxWeekly.Size = New System.Drawing.Size(486, 117)
        Me.gbxWeekly.TabIndex = 0
        Me.gbxWeekly.Text = "Holiday Details"
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(255, 60)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(45, 15)
        Me.CareLabel6.TabIndex = 7
        Me.CareLabel6.Text = "Half Day"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkToHalf
        '
        Me.chkToHalf.EnterMoveNextControl = True
        Me.chkToHalf.Location = New System.Drawing.Point(230, 58)
        Me.chkToHalf.Name = "chkToHalf"
        Me.chkToHalf.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkToHalf.Properties.Appearance.Options.UseFont = True
        Me.chkToHalf.Size = New System.Drawing.Size(19, 19)
        Me.chkToHalf.TabIndex = 6
        Me.chkToHalf.Tag = "AE"
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(255, 32)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(45, 15)
        Me.CareLabel5.TabIndex = 3
        Me.CareLabel5.Text = "Half Day"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkFromHalf
        '
        Me.chkFromHalf.EnterMoveNextControl = True
        Me.chkFromHalf.Location = New System.Drawing.Point(230, 30)
        Me.chkFromHalf.Name = "chkFromHalf"
        Me.chkFromHalf.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkFromHalf.Properties.Appearance.Options.UseFont = True
        Me.chkFromHalf.Size = New System.Drawing.Size(19, 19)
        Me.chkFromHalf.TabIndex = 2
        Me.chkFromHalf.Tag = "AE"
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(14, 60)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(91, 15)
        Me.CareLabel2.TabIndex = 4
        Me.CareLabel2.Text = "Holiday End Date"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtEndDate
        '
        Me.cdtEndDate.EditValue = Nothing
        Me.cdtEndDate.EnterMoveNextControl = True
        Me.cdtEndDate.Location = New System.Drawing.Point(124, 57)
        Me.cdtEndDate.Name = "cdtEndDate"
        Me.cdtEndDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtEndDate.Properties.Appearance.Options.UseFont = True
        Me.cdtEndDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtEndDate.Size = New System.Drawing.Size(100, 22)
        Me.cdtEndDate.TabIndex = 5
        Me.cdtEndDate.Tag = "AEM"
        Me.cdtEndDate.Value = Nothing
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(14, 88)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(60, 15)
        Me.CareLabel14.TabIndex = 8
        Me.CareLabel14.Text = "Discount %"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(14, 32)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(95, 15)
        Me.CareLabel11.TabIndex = 0
        Me.CareLabel11.Text = "Holiday Start Date"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDiscount
        '
        Me.txtDiscount.CharacterCasing = CharacterCasing.Normal
        Me.txtDiscount.EnterMoveNextControl = True
        Me.txtDiscount.Location = New System.Drawing.Point(124, 85)
        Me.txtDiscount.MaxLength = 14
        Me.txtDiscount.Name = "txtDiscount"
        Me.txtDiscount.NumericAllowNegatives = False
        Me.txtDiscount.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtDiscount.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDiscount.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtDiscount.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtDiscount.Properties.Appearance.Options.UseFont = True
        Me.txtDiscount.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDiscount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtDiscount.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtDiscount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtDiscount.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDiscount.Properties.MaxLength = 14
        Me.txtDiscount.Size = New System.Drawing.Size(100, 22)
        Me.txtDiscount.TabIndex = 9
        Me.txtDiscount.Tag = "AEM"
        Me.txtDiscount.TextAlign = HorizontalAlignment.Left
        Me.txtDiscount.ToolTipText = ""
        '
        'cdtStartDate
        '
        Me.cdtStartDate.EditValue = Nothing
        Me.cdtStartDate.EnterMoveNextControl = True
        Me.cdtStartDate.Location = New System.Drawing.Point(124, 29)
        Me.cdtStartDate.Name = "cdtStartDate"
        Me.cdtStartDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtStartDate.Properties.Appearance.Options.UseFont = True
        Me.cdtStartDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtStartDate.Size = New System.Drawing.Size(100, 22)
        Me.cdtStartDate.TabIndex = 1
        Me.cdtStartDate.Tag = "AEM"
        Me.cdtStartDate.Value = Nothing
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(322, 320)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(85, 25)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.CausesValidation = False
        Me.btnCancel.DialogResult = DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(413, 320)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 25)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.txtNotes)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 135)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(486, 177)
        Me.GroupControl1.TabIndex = 1
        Me.GroupControl1.Text = "Comments"
        '
        'txtNotes
        '
        Me.txtNotes.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtNotes.Location = New System.Drawing.Point(5, 23)
        Me.txtNotes.Name = "txtNotes"
        Me.txtNotes.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNotes.Properties.Appearance.Options.UseFont = True
        Me.txtNotes.Properties.MaxLength = 1024
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtNotes, True)
        Me.txtNotes.Size = New System.Drawing.Size(476, 149)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtNotes, OptionsSpelling1)
        Me.txtNotes.TabIndex = 0
        Me.txtNotes.Tag = ""
        '
        'frmChildHoliday
        '
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(510, 354)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.gbxWeekly)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmChildHoliday"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "Session Holiday"
        CType(Me.gbxWeekly, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxWeekly.ResumeLayout(False)
        Me.gbxWeekly.PerformLayout()
        CType(Me.chkToHalf.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkFromHalf.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtEndDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtEndDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDiscount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtStartDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtStartDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtNotes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents txtDiscount As Care.Controls.CareTextBox
    Friend WithEvents cdtStartDate As Care.Controls.CareDateTime
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents cdtEndDate As Care.Controls.CareDateTime
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents chkToHalf As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents chkFromHalf As Care.Controls.CareCheckBox
    Friend WithEvents txtNotes As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents gbxWeekly As Care.Controls.CareFrame
    Friend WithEvents GroupControl1 As Care.Controls.CareFrame
End Class
