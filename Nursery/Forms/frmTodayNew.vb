﻿

Imports Care.Global
Imports Care.Data
Imports Care.Shared

Public Class frmTodayNew

    Private m_ID As Guid? = Nothing

    Private m_TodayIsCreated As Boolean = False
    Private m_TomorrowIsCreated As Boolean = False
    Private m_NextMondayIsCreated As Boolean = False

    Private m_DefaultActivityText As String = ""

    Public ReadOnly Property DayID As String
        Get
            If m_ID.HasValue Then
                Return m_ID.ToString()
            Else
                Return ""
            End If
        End Get
    End Property

    Private Sub frmTodayNew_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        m_DefaultActivityText = ParameterHandler.ReturnString("ACTIVITYTEXT")
        SetDayIndicators()
    End Sub

    Private Sub frmTodayNew_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        radToday.Enabled = Not m_TodayIsCreated

        Select Case Today.DayOfWeek

            Case DayOfWeek.Monday, DayOfWeek.Sunday
                radTomorrow.Enabled = Not m_TomorrowIsCreated
                radMonday.Enabled = False

            Case DayOfWeek.Tuesday, DayOfWeek.Wednesday, DayOfWeek.Thursday
                radTomorrow.Enabled = Not m_TomorrowIsCreated
                radMonday.Enabled = False

            Case DayOfWeek.Friday, DayOfWeek.Saturday
                radTomorrow.Enabled = Not m_TomorrowIsCreated
                radMonday.Enabled = Not m_NextMondayIsCreated

        End Select

        If radToday.Enabled = False And radTomorrow.Enabled = False And radMonday.Enabled = False Then
            btnNewOK.Enabled = False
        Else

            If radToday.Enabled Then
                radToday.Checked = True
            Else
                If radTomorrow.Enabled Then
                    radTomorrow.Checked = True
                Else
                    radMonday.Checked = True
                End If
            End If

            btnNewOK.Enabled = True

        End If

    End Sub

    Private Sub CreateDay()

        Dim _RecordCreated As Boolean = False

        If radToday.Checked Then
            If Not m_TodayIsCreated Then
                CreateDayRecord(Today)
                _RecordCreated = True
            End If
        End If

        If radTomorrow.Checked Then
            If Not m_TomorrowIsCreated Then
                CreateDayRecord(Today.AddDays(1))
                _RecordCreated = True
            End If
        End If

        If radMonday.Checked Then
            If Not m_NextMondayIsCreated Then
                CreateDayRecord(ValueHandler.NearestDate(Today, DayOfWeek.Monday, ValueHandler.EnumDirection.Forwards))
                _RecordCreated = True
            End If
        End If

        If _RecordCreated Then
            Business.Child.BuildDayPatternsForEveryone(Today)
        End If

    End Sub

    Private Sub CreateDayRecord(ByVal DateIn As Date)

        Dim _Sites As List(Of Business.Site) = Business.Site.RetreiveAll
        If _Sites IsNot Nothing Then

            Dim _Record As Integer = 1

            For Each _Site In _Sites

                Dim _Day As New Business.Day

                _Day._SiteId = _Site._ID
                _Day._SiteName = _Site._Name
                _Day._Date = DateIn

                _Day.Store()

                CreateBlankActivity(_Day._ID.Value, _Day._SiteId.Value)

                If _Record = 1 Then
                    m_ID = _Day._ID
                End If

                _Day = Nothing
                _Record += 1

            Next

        End If

    End Sub

    Private Sub CreateBlankActivity(ByVal DayID As Guid, ByVal SiteID As Guid)

        Dim _SQL As String = ""
        _SQL += "select ID, room_name from SiteRooms"
        _SQL += " where site_id = '" + SiteID.ToString + "'"
        _SQL += " and room_check_children = 1"
        _SQL += " order by room_sequence"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            For Each _Row As DataRow In _DT.Rows

                Dim _A As New Business.DayActivity

                _A._DayId = DayID
                _A._GroupId = New Guid(_Row.Item("ID").ToString)

                If m_DefaultActivityText = "" Then
                    _A._ActivityText = "Today in the " + _Row.Item("room_name").ToString + " we..."
                Else
                    _A._ActivityText = m_DefaultActivityText
                    _A._ActivityText = _A._ActivityText.Replace("<GroupName>", _Row.Item("room_name").ToString)
                End If

                _A.Store()

            Next

        End If

    End Sub

    Private Sub SetDayIndicators()
        m_TodayIsCreated = SetFlag(Today)
        m_TomorrowIsCreated = SetFlag(Today.AddDays(1))
        m_NextMondayIsCreated = SetFlag(ValueHandler.NearestDate(Today, DayOfWeek.Monday, ValueHandler.EnumDirection.Forwards))
    End Sub

    Private Function SetFlag(ByVal DateIn As Date) As Boolean

        Dim _Day As Business.Day = Business.Day.RetreiveByDateOnly(DateIn)
        If _Day IsNot Nothing Then

            If _Day._Date.HasValue Then
                Return True
            End If

            _Day = Nothing

        End If

        Return False

    End Function

    Private Sub btnNewCancel_Click(sender As Object, e As EventArgs) Handles btnNewCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnNewOK_Click(sender As Object, e As EventArgs) Handles btnNewOK.Click

        btnNewOK.Enabled = False
        btnNewCancel.Enabled = False

        Session.CursorWaiting()

        CreateDay()

        Me.DialogResult = DialogResult.OK
        Me.Close()

    End Sub

End Class