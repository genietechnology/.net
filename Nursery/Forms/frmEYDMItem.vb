﻿Imports Care.Global
Imports Care.Data

Public Class frmEYDMItem

    Private m_ID As Guid?

    Public Sub New(ByVal RecordID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        m_ID = RecordID

    End Sub

    Private Sub frmEYDMItem_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim _SQL As String = ""

        _SQL = "select id, name + ' - ' + aspect from EYAreas order by display_seq"
        cbxArea.PopulateWithSQL(Session.ConnectionString, _SQL)

        _SQL = "select id, name from EYAgeBands order by months_from"
        cbxAge.PopulateWithSQL(Session.ConnectionString, _SQL)

        cbxArea.BackColor = Session.ChangeColour
        cbxAge.BackColor = Session.ChangeColour
        txtStatement.BackColor = Session.ChangeColour
        txtSeq.BackColor = Session.ChangeColour

        DisplayRecord()

    End Sub

    Private Sub DisplayRecord()

        If Not m_ID.HasValue Then Exit Sub

        Dim _S As Business.EYStatement = Business.EYStatement.RetreiveByID(m_ID.Value)
        If _S IsNot Nothing Then
            cbxArea.SelectedValue = _S._AreaId
            cbxAge.SelectedValue = _S._AgeId
            txtStatement.Text = _S._Statement
            txtSeq.Text = _S._DisplaySeq.ToString
            _S = Nothing
        End If

    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnSaveClose_Click(sender As Object, e As EventArgs) Handles btnSaveClose.Click
        If Save() Then
            Me.DialogResult = DialogResult.OK
            Me.Close()
        End If
    End Sub

    Private Sub btnSaveAdd_Click(sender As Object, e As EventArgs) Handles btnSaveAdd.Click
        If Save() Then
            txtStatement.Text = ""
            txtSeq.Text = ""
            txtStatement.Focus()
        End If
    End Sub

    Private Function Save() As Boolean

        Dim _DM As New Business.EYStatement

        If m_ID.HasValue Then
            _DM = Business.EYStatement.RetreiveByID(m_ID.Value)
        End If

        With _DM

            If cbxArea.SelectedIndex < 0 Then
                ._AreaId = Nothing
            Else
                ._AreaId = New Guid(cbxArea.SelectedValue.ToString)
            End If

            If cbxAge.SelectedIndex < 0 Then
                ._AgeId = Nothing
            Else
                ._AgeId = New Guid(cbxAge.SelectedValue.ToString)
            End If

            ._Statement = txtStatement.Text
            ._DisplaySeq = ValueHandler.ConvertInteger(txtSeq.Text)

            .Store()

        End With

        Return True

    End Function

End Class