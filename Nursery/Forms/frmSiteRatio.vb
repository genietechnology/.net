﻿
Imports Care.Global

Public Class frmSiteRatio

    Private m_RoomID As Guid
    Private m_RatioID As Guid?
    Private m_Ratio As New Business.SiteRoomRatio

    Public Sub New(ByVal RoomID As Guid, ByVal RatioID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_RoomID = RoomID
        m_RatioID = RatioID

    End Sub

    Private Sub frmSiteRatio_Load(sender As Object, e As EventArgs) Handles Me.Load

        MyControls.SetControls(Care.Shared.ControlHandler.Mode.Add, Me.Controls)

        If m_RatioID.HasValue Then
            Me.Text = "Edit Ratio"
            DisplayRecord()
        Else
            Me.Text = "Create New Ratio"
        End If

        txtAgeFrom.Focus()

    End Sub

    Private Sub DisplayRecord()

        m_Ratio = Business.SiteRoomRatio.RetreiveByID(m_RatioID.Value)
        With m_Ratio
            txtAgeFrom.Text = ._AgeFrom.ToString
            txtAgeTo.Text = ._AgeTo.ToString
            txtCapacity.Text = ._Capacity.ToString
            txtRatio.Text = ._Ratio.ToString
        End With

    End Sub

    Private Function CheckMandatory() As Boolean
        Return True
    End Function

    Private Sub Save()

        If Not m_RatioID.HasValue Then
            m_Ratio._RoomId = m_RoomID
        End If

        With m_Ratio
            ._AgeFrom = ValueHandler.ConvertInteger(txtAgeFrom.Text)
            ._AgeTo = ValueHandler.ConvertInteger(txtAgeTo.Text)
            ._Capacity = ValueHandler.ConvertInteger(txtCapacity.Text)
            ._Ratio = ValueHandler.ConvertInteger(txtRatio.Text)
            .Store()
        End With

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        If CheckMandatory() Then
            Save()
            Me.DialogResult = DialogResult.OK
            Me.Close()
        End If
    End Sub

End Class
