﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTariffAge
    Inherits Care.Shared.frmGridMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtMaxAge = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.txtMinAge = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.txtDescription = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.gbx.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.txtMaxAge.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMinAge.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescription.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbx
        '
        Me.gbx.Controls.Add(Me.txtMaxAge)
        Me.gbx.Controls.Add(Me.CareLabel3)
        Me.gbx.Controls.Add(Me.txtMinAge)
        Me.gbx.Controls.Add(Me.CareLabel2)
        Me.gbx.Controls.Add(Me.txtDescription)
        Me.gbx.Controls.Add(Me.CareLabel1)
        Me.gbx.Location = New System.Drawing.Point(28, 151)
        Me.gbx.Size = New System.Drawing.Size(436, 136)
        Me.gbx.TabIndex = 1
        Me.gbx.Controls.SetChildIndex(Me.Panel2, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel1, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtDescription, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel2, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtMinAge, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel3, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtMaxAge, 0)
        '
        'Panel2
        '
        Me.Panel2.Location = New System.Drawing.Point(3, 105)
        Me.Panel2.Size = New System.Drawing.Size(430, 28)
        Me.Panel2.TabIndex = 6
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(245, 0)
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(336, 0)
        '
        'Grid
        '
        Me.Grid.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Grid.Appearance.Options.UseFont = True
        Me.Grid.Size = New System.Drawing.Size(472, 434)
        Me.Grid.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(0, 452)
        Me.Panel1.Size = New System.Drawing.Size(491, 35)
        '
        'btnDelete
        '
        Me.btnDelete.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Appearance.Options.UseFont = True
        '
        'btnEdit
        '
        Me.btnEdit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.Appearance.Options.UseFont = True
        '
        'btnAdd
        '
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.Appearance.Options.UseFont = True
        '
        'btnClose
        '
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(674, 5)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'txtMaxAge
        '
        Me.txtMaxAge.CharacterCasing = CharacterCasing.Normal
        Me.txtMaxAge.EnterMoveNextControl = True
        Me.txtMaxAge.Location = New System.Drawing.Point(119, 78)
        Me.txtMaxAge.MaxLength = 4
        Me.txtMaxAge.Name = "txtMaxAge"
        Me.txtMaxAge.NumericAllowNegatives = False
        Me.txtMaxAge.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtMaxAge.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtMaxAge.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtMaxAge.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtMaxAge.Properties.Appearance.Options.UseFont = True
        Me.txtMaxAge.Properties.Appearance.Options.UseTextOptions = True
        Me.txtMaxAge.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtMaxAge.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtMaxAge.Properties.MaxLength = 4
        Me.txtMaxAge.ReadOnly = False
        Me.txtMaxAge.Size = New System.Drawing.Size(40, 22)
        Me.txtMaxAge.TabIndex = 5
        Me.txtMaxAge.Tag = "AEN"
        Me.txtMaxAge.TextAlign = HorizontalAlignment.Left
        Me.txtMaxAge.ToolTipText = ""
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(12, 81)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(98, 15)
        Me.CareLabel3.TabIndex = 4
        Me.CareLabel3.Text = "Max Age (months)"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMinAge
        '
        Me.txtMinAge.CharacterCasing = CharacterCasing.Normal
        Me.txtMinAge.EnterMoveNextControl = True
        Me.txtMinAge.Location = New System.Drawing.Point(119, 50)
        Me.txtMinAge.MaxLength = 4
        Me.txtMinAge.Name = "txtMinAge"
        Me.txtMinAge.NumericAllowNegatives = False
        Me.txtMinAge.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtMinAge.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtMinAge.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtMinAge.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtMinAge.Properties.Appearance.Options.UseFont = True
        Me.txtMinAge.Properties.Appearance.Options.UseTextOptions = True
        Me.txtMinAge.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtMinAge.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtMinAge.Properties.MaxLength = 4
        Me.txtMinAge.ReadOnly = False
        Me.txtMinAge.Size = New System.Drawing.Size(40, 22)
        Me.txtMinAge.TabIndex = 3
        Me.txtMinAge.Tag = "AEN"
        Me.txtMinAge.TextAlign = HorizontalAlignment.Left
        Me.txtMinAge.ToolTipText = ""
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(12, 53)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(97, 15)
        Me.CareLabel2.TabIndex = 2
        Me.CareLabel2.Text = "Min Age (months)"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDescription
        '
        Me.txtDescription.CharacterCasing = CharacterCasing.Normal
        Me.txtDescription.EnterMoveNextControl = True
        Me.txtDescription.Location = New System.Drawing.Point(119, 22)
        Me.txtDescription.MaxLength = 30
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.NumericAllowNegatives = False
        Me.txtDescription.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtDescription.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDescription.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtDescription.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtDescription.Properties.Appearance.Options.UseFont = True
        Me.txtDescription.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDescription.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtDescription.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDescription.Properties.MaxLength = 30
        Me.txtDescription.ReadOnly = False
        Me.txtDescription.Size = New System.Drawing.Size(306, 22)
        Me.txtDescription.TabIndex = 1
        Me.txtDescription.Tag = "AEB"
        Me.txtDescription.TextAlign = HorizontalAlignment.Left
        Me.txtDescription.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(12, 25)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Name"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmTariffAge
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(491, 487)
        Me.Name = "frmTariffAge"
        Me.Text = "frmTariffAge"
        Me.gbx.ResumeLayout(False)
        Me.gbx.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.txtMaxAge.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMinAge.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescription.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtMaxAge As Care.Controls.CareTextBox
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents txtMinAge As Care.Controls.CareTextBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents txtDescription As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
End Class
