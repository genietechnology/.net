﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMeals
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New Care.Controls.CareFrame()
        Me.chkDessert = New Care.Controls.CareCheckBox()
        Me.chkTea = New Care.Controls.CareCheckBox()
        Me.chkLunch = New Care.Controls.CareCheckBox()
        Me.chkSnack = New Care.Controls.CareCheckBox()
        Me.chkBreakfast = New Care.Controls.CareCheckBox()
        Me.Label5 = New Care.Controls.CareLabel()
        Me.Label2 = New Care.Controls.CareLabel()
        Me.txtName = New Care.Controls.CareTextBox()
        Me.Label1 = New Care.Controls.CareLabel()
        Me.GroupBox2 = New Care.Controls.CareFrame()
        Me.gbxQty = New Care.Controls.CareFrame()
        Me.lblID = New Care.Controls.CareLabel()
        Me.txtQty = New Care.Controls.CareTextBox()
        Me.Label4 = New Care.Controls.CareLabel()
        Me.btnQtyCancel = New Care.Controls.CareButton()
        Me.btnQtyOK = New Care.Controls.CareButton()
        Me.txtQtyName = New Care.Controls.CareTextBox()
        Me.Label3 = New Care.Controls.CareLabel()
        Me.btnEdit = New Care.Controls.CareButton()
        Me.btnRemove = New Care.Controls.CareButton()
        Me.btnAdd = New Care.Controls.CareButton()
        Me.GridIngredients = New Care.Controls.CareGrid()
        Me.txtDescription = New DevExpress.XtraEditors.MemoEdit()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.chkDessert.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkTea.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkLunch.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSnack.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkBreakfast.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.gbxQty, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxQty.SuspendLayout()
        CType(Me.txtQty.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtQtyName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescription.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(0, 521)
        Me.Panel1.Size = New System.Drawing.Size(552, 35)
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(364, 5)
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(455, 5)
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.chkDessert)
        Me.GroupBox1.Controls.Add(Me.chkTea)
        Me.GroupBox1.Controls.Add(Me.chkLunch)
        Me.GroupBox1.Controls.Add(Me.chkSnack)
        Me.GroupBox1.Controls.Add(Me.chkBreakfast)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtName)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtDescription)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 53)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.ShowCaption = False
        Me.GroupBox1.Size = New System.Drawing.Size(528, 184)
        Me.GroupBox1.TabIndex = 0
        '
        'chkDessert
        '
        Me.chkDessert.EnterMoveNextControl = True
        Me.chkDessert.Location = New System.Drawing.Point(418, 130)
        Me.chkDessert.Name = "chkDessert"
        Me.chkDessert.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkDessert.Properties.Appearance.Options.UseFont = True
        Me.chkDessert.Properties.Caption = "Dessert"
        Me.chkDessert.Size = New System.Drawing.Size(96, 20)
        Me.chkDessert.TabIndex = 6
        Me.chkDessert.Tag = "AEB"
        '
        'chkTea
        '
        Me.chkTea.EnterMoveNextControl = True
        Me.chkTea.Location = New System.Drawing.Point(266, 154)
        Me.chkTea.Name = "chkTea"
        Me.chkTea.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkTea.Properties.Appearance.Options.UseFont = True
        Me.chkTea.Properties.Caption = "Afternoon Tea"
        Me.chkTea.Size = New System.Drawing.Size(121, 20)
        Me.chkTea.TabIndex = 5
        Me.chkTea.Tag = "AEB"
        '
        'chkLunch
        '
        Me.chkLunch.EnterMoveNextControl = True
        Me.chkLunch.Location = New System.Drawing.Point(109, 155)
        Me.chkLunch.Name = "chkLunch"
        Me.chkLunch.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkLunch.Properties.Appearance.Options.UseFont = True
        Me.chkLunch.Properties.Caption = "Lunch"
        Me.chkLunch.Size = New System.Drawing.Size(134, 20)
        Me.chkLunch.TabIndex = 4
        Me.chkLunch.Tag = "AEB"
        '
        'chkSnack
        '
        Me.chkSnack.EnterMoveNextControl = True
        Me.chkSnack.Location = New System.Drawing.Point(266, 129)
        Me.chkSnack.Name = "chkSnack"
        Me.chkSnack.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkSnack.Properties.Appearance.Options.UseFont = True
        Me.chkSnack.Properties.Caption = "Snack"
        Me.chkSnack.Size = New System.Drawing.Size(121, 20)
        Me.chkSnack.TabIndex = 3
        Me.chkSnack.Tag = "AEB"
        '
        'chkBreakfast
        '
        Me.chkBreakfast.EnterMoveNextControl = True
        Me.chkBreakfast.Location = New System.Drawing.Point(109, 129)
        Me.chkBreakfast.Name = "chkBreakfast"
        Me.chkBreakfast.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkBreakfast.Properties.Appearance.Options.UseFont = True
        Me.chkBreakfast.Properties.Caption = "Breakfast"
        Me.chkBreakfast.Size = New System.Drawing.Size(134, 20)
        Me.chkBreakfast.TabIndex = 2
        Me.chkBreakfast.Tag = "AEB"
        '
        'Label5
        '
        Me.Label5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label5.Location = New System.Drawing.Point(8, 132)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(54, 15)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Served As:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label2.Location = New System.Drawing.Point(8, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 15)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Description"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtName.CharacterCasing = CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(111, 12)
        Me.txtName.MaxLength = 40
        Me.txtName.Name = "txtName"
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AccessibleName = "Name"
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.MaxLength = 40
        Me.txtName.ReadOnly = False
        Me.txtName.Size = New System.Drawing.Size(403, 22)
        Me.txtName.TabIndex = 0
        Me.txtName.Tag = "AEBM"
        Me.txtName.TextAlign = HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'Label1
        '
        Me.Label1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label1.Location = New System.Drawing.Point(8, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Name"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.gbxQty)
        Me.GroupBox2.Controls.Add(Me.btnEdit)
        Me.GroupBox2.Controls.Add(Me.btnRemove)
        Me.GroupBox2.Controls.Add(Me.btnAdd)
        Me.GroupBox2.Controls.Add(Me.GridIngredients)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 247)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(528, 269)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.Text = "Ingredients"
        '
        'gbxQty
        '
        Me.gbxQty.Controls.Add(Me.lblID)
        Me.gbxQty.Controls.Add(Me.txtQty)
        Me.gbxQty.Controls.Add(Me.Label4)
        Me.gbxQty.Controls.Add(Me.btnQtyCancel)
        Me.gbxQty.Controls.Add(Me.btnQtyOK)
        Me.gbxQty.Controls.Add(Me.txtQtyName)
        Me.gbxQty.Controls.Add(Me.Label3)
        Me.gbxQty.Location = New System.Drawing.Point(27, 64)
        Me.gbxQty.Name = "gbxQty"
        Me.gbxQty.ShowCaption = False
        Me.gbxQty.Size = New System.Drawing.Size(468, 116)
        Me.gbxQty.TabIndex = 0
        '
        'lblID
        '
        Me.lblID.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblID.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblID.Location = New System.Drawing.Point(6, 82)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(61, 15)
        Me.lblID.TabIndex = 7
        Me.lblID.Text = "ID - Hidden"
        Me.lblID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblID.Visible = False
        '
        'txtQty
        '
        Me.txtQty.CharacterCasing = CharacterCasing.Normal
        Me.txtQty.EnterMoveNextControl = True
        Me.txtQty.Location = New System.Drawing.Point(116, 50)
        Me.txtQty.MaxLength = 3
        Me.txtQty.Name = "txtQty"
        Me.txtQty.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtQty.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtQty.Properties.Appearance.Options.UseFont = True
        Me.txtQty.Properties.Appearance.Options.UseTextOptions = True
        Me.txtQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtQty.Properties.MaxLength = 3
        Me.txtQty.ReadOnly = False
        Me.txtQty.Size = New System.Drawing.Size(100, 22)
        Me.txtQty.TabIndex = 1
        Me.txtQty.Tag = "EBN"
        Me.txtQty.TextAlign = HorizontalAlignment.Left
        Me.txtQty.ToolTipText = ""
        '
        'Label4
        '
        Me.Label4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label4.Location = New System.Drawing.Point(6, 53)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 15)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Quantity"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnQtyCancel
        '
        Me.btnQtyCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnQtyCancel.Appearance.Options.UseFont = True
        Me.btnQtyCancel.Location = New System.Drawing.Point(366, 82)
        Me.btnQtyCancel.Name = "btnQtyCancel"
        Me.btnQtyCancel.Size = New System.Drawing.Size(85, 25)
        Me.btnQtyCancel.TabIndex = 3
        Me.btnQtyCancel.Text = "Cancel"
        '
        'btnQtyOK
        '
        Me.btnQtyOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnQtyOK.Appearance.Options.UseFont = True
        Me.btnQtyOK.Location = New System.Drawing.Point(275, 82)
        Me.btnQtyOK.Name = "btnQtyOK"
        Me.btnQtyOK.Size = New System.Drawing.Size(85, 25)
        Me.btnQtyOK.TabIndex = 2
        Me.btnQtyOK.Text = "OK"
        '
        'txtQtyName
        '
        Me.txtQtyName.CharacterCasing = CharacterCasing.Normal
        Me.txtQtyName.EnterMoveNextControl = True
        Me.txtQtyName.Location = New System.Drawing.Point(116, 21)
        Me.txtQtyName.MaxLength = 0
        Me.txtQtyName.Name = "txtQtyName"
        Me.txtQtyName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtQtyName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtQtyName.Properties.Appearance.Options.UseFont = True
        Me.txtQtyName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtQtyName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtQtyName.ReadOnly = False
        Me.txtQtyName.Size = New System.Drawing.Size(335, 22)
        Me.txtQtyName.TabIndex = 0
        Me.txtQtyName.Tag = "R"
        Me.txtQtyName.TextAlign = HorizontalAlignment.Left
        Me.txtQtyName.ToolTipText = ""
        '
        'Label3
        '
        Me.Label3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label3.Location = New System.Drawing.Point(6, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(32, 15)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Name"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnEdit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.Appearance.Options.UseFont = True
        Me.btnEdit.Location = New System.Drawing.Point(429, 238)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(85, 25)
        Me.btnEdit.TabIndex = 3
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.Visible = False
        '
        'btnRemove
        '
        Me.btnRemove.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnRemove.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRemove.Appearance.Options.UseFont = True
        Me.btnRemove.Location = New System.Drawing.Point(101, 238)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(85, 25)
        Me.btnRemove.TabIndex = 4
        Me.btnRemove.Text = "Remove"
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.Appearance.Options.UseFont = True
        Me.btnAdd.Location = New System.Drawing.Point(10, 238)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(85, 25)
        Me.btnAdd.TabIndex = 2
        Me.btnAdd.Text = "Add"
        '
        'GridIngredients
        '
        Me.GridIngredients.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridIngredients.Appearance.Options.UseFont = True
        Me.GridIngredients.HideFirstColumn = False
        Me.GridIngredients.Location = New System.Drawing.Point(11, 32)
        Me.GridIngredients.Name = "GridIngredients"
        Me.GridIngredients.ShowGroupByBox = False
        Me.GridIngredients.Size = New System.Drawing.Size(503, 200)
        Me.GridIngredients.TabIndex = 6
        '
        'txtDescription
        '
        Me.txtDescription.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtDescription.Location = New System.Drawing.Point(111, 40)
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Properties.AccessibleName = "Description"
        Me.txtDescription.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtDescription.Properties.Appearance.Options.UseFont = True
        Me.txtDescription.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDescription.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtDescription.Properties.MaxLength = 1024
        Me.txtDescription.Size = New System.Drawing.Size(403, 83)
        Me.txtDescription.TabIndex = 1
        Me.txtDescription.Tag = "AEB"
        '
        'frmMeals
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.ClientSize = New System.Drawing.Size(552, 556)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.MinimumSize = New System.Drawing.Size(544, 289)
        Me.Name = "frmMeals"
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.GroupBox2, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.chkDessert.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkTea.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkLunch.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSnack.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkBreakfast.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.gbxQty, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxQty.ResumeLayout(False)
        Me.gbxQty.PerformLayout()
        CType(Me.txtQty.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtQtyName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescription.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

End Sub
    Friend WithEvents GroupBox1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label2 As Care.Controls.CareLabel
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents Label1 As Care.Controls.CareLabel
    Friend WithEvents GroupBox2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnEdit As Care.Controls.CareButton
    Friend WithEvents btnRemove As Care.Controls.CareButton
    Friend WithEvents btnAdd As Care.Controls.CareButton
    Friend WithEvents gbxQty As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnQtyCancel As Care.Controls.CareButton
    Friend WithEvents btnQtyOK As Care.Controls.CareButton
    Friend WithEvents txtQtyName As Care.Controls.CareTextBox
    Friend WithEvents Label3 As Care.Controls.CareLabel
    Friend WithEvents txtQty As Care.Controls.CareTextBox
    Friend WithEvents Label4 As Care.Controls.CareLabel
    Friend WithEvents lblID As Care.Controls.CareLabel
    Friend WithEvents Label5 As Care.Controls.CareLabel
    Friend WithEvents chkTea As Care.Controls.CareCheckBox
    Friend WithEvents chkLunch As Care.Controls.CareCheckBox
    Friend WithEvents chkSnack As Care.Controls.CareCheckBox
    Friend WithEvents chkBreakfast As Care.Controls.CareCheckBox
    Friend WithEvents chkDessert As Care.Controls.CareCheckBox
    Friend WithEvents GridIngredients As Care.Controls.CareGrid
    Friend WithEvents txtDescription As DevExpress.XtraEditors.MemoEdit

End Class
