﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsentExplorer
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.grdConsent = New Care.Controls.CareGrid()
        Me.CareFrame2 = New Care.Controls.CareFrame()
        Me.CareFrame1 = New Care.Controls.CareFrame()
        Me.grdChildren = New Care.Controls.CareGrid()
        Me.GroupControl10 = New Care.Controls.CareFrame()
        Me.cbxSite = New Care.Controls.CareComboBox()
        Me.CareFrame3 = New Care.Controls.CareFrame()
        Me.radWith = New Care.Controls.CareRadioButton()
        Me.radWithout = New Care.Controls.CareRadioButton()
        Me.btnRefresh = New Care.Controls.CareButton()
        CType(Me.CareFrame2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CareFrame2.SuspendLayout()
        CType(Me.CareFrame1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CareFrame1.SuspendLayout()
        CType(Me.GroupControl10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl10.SuspendLayout()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CareFrame3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CareFrame3.SuspendLayout()
        CType(Me.radWith.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radWithout.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'grdConsent
        '
        Me.grdConsent.AllowBuildColumns = True
        Me.grdConsent.AllowEdit = True
        Me.grdConsent.AllowHorizontalScroll = False
        Me.grdConsent.AllowMultiSelect = True
        Me.grdConsent.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdConsent.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdConsent.Appearance.Options.UseFont = True
        Me.grdConsent.AutoSizeByData = True
        Me.grdConsent.DisableAutoSize = False
        Me.grdConsent.DisableDataFormatting = False
        Me.grdConsent.FocusedRowHandle = -2147483648
        Me.grdConsent.HideFirstColumn = False
        Me.grdConsent.Location = New System.Drawing.Point(5, 23)
        Me.grdConsent.Name = "grdConsent"
        Me.grdConsent.PreviewColumn = ""
        Me.grdConsent.QueryID = Nothing
        Me.grdConsent.RowAutoHeight = False
        Me.grdConsent.SearchAsYouType = True
        Me.grdConsent.ShowAutoFilterRow = False
        Me.grdConsent.ShowFindPanel = True
        Me.grdConsent.ShowGroupByBox = False
        Me.grdConsent.ShowLoadingPanel = False
        Me.grdConsent.ShowNavigator = False
        Me.grdConsent.Size = New System.Drawing.Size(277, 411)
        Me.grdConsent.TabIndex = 0
        '
        'CareFrame2
        '
        Me.CareFrame2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.CareFrame2.Controls.Add(Me.grdConsent)
        Me.CareFrame2.Location = New System.Drawing.Point(12, 72)
        Me.CareFrame2.Name = "CareFrame2"
        Me.CareFrame2.Size = New System.Drawing.Size(287, 439)
        Me.CareFrame2.TabIndex = 3
        Me.CareFrame2.Text = "Select Consent Item(s)"
        '
        'CareFrame1
        '
        Me.CareFrame1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CareFrame1.Controls.Add(Me.grdChildren)
        Me.CareFrame1.Location = New System.Drawing.Point(305, 72)
        Me.CareFrame1.Name = "CareFrame1"
        Me.CareFrame1.Size = New System.Drawing.Size(443, 439)
        Me.CareFrame1.TabIndex = 4
        Me.CareFrame1.Text = "Children"
        '
        'grdChildren
        '
        Me.grdChildren.AllowBuildColumns = True
        Me.grdChildren.AllowEdit = False
        Me.grdChildren.AllowHorizontalScroll = False
        Me.grdChildren.AllowMultiSelect = True
        Me.grdChildren.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdChildren.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdChildren.Appearance.Options.UseFont = True
        Me.grdChildren.AutoSizeByData = True
        Me.grdChildren.DisableAutoSize = False
        Me.grdChildren.DisableDataFormatting = False
        Me.grdChildren.FocusedRowHandle = -2147483648
        Me.grdChildren.HideFirstColumn = False
        Me.grdChildren.Location = New System.Drawing.Point(5, 23)
        Me.grdChildren.Name = "grdChildren"
        Me.grdChildren.PreviewColumn = ""
        Me.grdChildren.QueryID = Nothing
        Me.grdChildren.RowAutoHeight = False
        Me.grdChildren.SearchAsYouType = True
        Me.grdChildren.ShowAutoFilterRow = False
        Me.grdChildren.ShowFindPanel = True
        Me.grdChildren.ShowGroupByBox = False
        Me.grdChildren.ShowLoadingPanel = False
        Me.grdChildren.ShowNavigator = False
        Me.grdChildren.Size = New System.Drawing.Size(433, 411)
        Me.grdChildren.TabIndex = 0
        '
        'GroupControl10
        '
        Me.GroupControl10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl10.Appearance.Options.UseFont = True
        Me.GroupControl10.Controls.Add(Me.cbxSite)
        Me.GroupControl10.Location = New System.Drawing.Point(329, 12)
        Me.GroupControl10.Name = "GroupControl10"
        Me.GroupControl10.Size = New System.Drawing.Size(235, 54)
        Me.GroupControl10.TabIndex = 1
        Me.GroupControl10.Text = "Site"
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(5, 24)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AccessibleName = "Gender"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(225, 22)
        Me.cbxSite.TabIndex = 0
        Me.cbxSite.Tag = ""
        Me.cbxSite.ValueMember = Nothing
        '
        'CareFrame3
        '
        Me.CareFrame3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.CareFrame3.Appearance.Options.UseFont = True
        Me.CareFrame3.Controls.Add(Me.radWith)
        Me.CareFrame3.Controls.Add(Me.radWithout)
        Me.CareFrame3.Location = New System.Drawing.Point(12, 12)
        Me.CareFrame3.Name = "CareFrame3"
        Me.CareFrame3.Size = New System.Drawing.Size(311, 54)
        Me.CareFrame3.TabIndex = 0
        Me.CareFrame3.Text = "Child Filter"
        '
        'radWith
        '
        Me.radWith.EditValue = True
        Me.radWith.Location = New System.Drawing.Point(14, 25)
        Me.radWith.Name = "radWith"
        Me.radWith.Properties.AutoWidth = True
        Me.radWith.Properties.Caption = "Children with consent"
        Me.radWith.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radWith.Properties.RadioGroupIndex = 0
        Me.radWith.Size = New System.Drawing.Size(125, 19)
        Me.radWith.TabIndex = 0
        '
        'radWithout
        '
        Me.radWithout.Location = New System.Drawing.Point(152, 25)
        Me.radWithout.Name = "radWithout"
        Me.radWithout.Properties.AutoWidth = True
        Me.radWithout.Properties.Caption = "Children without Consent"
        Me.radWithout.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radWithout.Properties.RadioGroupIndex = 0
        Me.radWithout.Size = New System.Drawing.Size(143, 19)
        Me.radWithout.TabIndex = 1
        Me.radWithout.TabStop = False
        '
        'btnRefresh
        '
        Me.btnRefresh.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnRefresh.Appearance.Options.UseFont = True
        Me.btnRefresh.Location = New System.Drawing.Point(570, 35)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(85, 23)
        Me.btnRefresh.TabIndex = 2
        Me.btnRefresh.Text = "Refresh"
        '
        'frmConsentExplorer
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(760, 520)
        Me.Controls.Add(Me.btnRefresh)
        Me.Controls.Add(Me.CareFrame3)
        Me.Controls.Add(Me.GroupControl10)
        Me.Controls.Add(Me.CareFrame1)
        Me.Controls.Add(Me.CareFrame2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.LoadMaximised = True
        Me.Name = "frmConsentExplorer"
        Me.Text = "frmConsentExplorer"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.CareFrame2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CareFrame2.ResumeLayout(False)
        CType(Me.CareFrame1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CareFrame1.ResumeLayout(False)
        CType(Me.GroupControl10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl10.ResumeLayout(False)
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CareFrame3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CareFrame3.ResumeLayout(False)
        Me.CareFrame3.PerformLayout()
        CType(Me.radWith.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radWithout.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grdConsent As Care.Controls.CareGrid
    Friend WithEvents CareFrame2 As Care.Controls.CareFrame
    Friend WithEvents CareFrame1 As Care.Controls.CareFrame
    Friend WithEvents grdChildren As Care.Controls.CareGrid
    Friend WithEvents GroupControl10 As Care.Controls.CareFrame
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
    Friend WithEvents CareFrame3 As Care.Controls.CareFrame
    Friend WithEvents radWith As Care.Controls.CareRadioButton
    Friend WithEvents radWithout As Care.Controls.CareRadioButton
    Friend WithEvents btnRefresh As Care.Controls.CareButton
End Class
