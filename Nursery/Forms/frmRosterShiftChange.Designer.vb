﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRosterShiftChange
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.txtACost = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.txtAHours = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.txtARoom = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.txtATo = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.txtAStaff = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.Label2 = New Care.Controls.CareLabel(Me.components)
        Me.txtAFrom = New Care.Controls.CareTextBox(Me.components)
        Me.txtADate = New Care.Controls.CareTextBox(Me.components)
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        Me.gbxSwapHeader = New Care.Controls.CareFrame()
        Me.btnBSelect = New Care.Controls.CareButton(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.txtBCost = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.txtBHours = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.txtBRoom = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel10 = New Care.Controls.CareLabel(Me.components)
        Me.txtBTo = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel12 = New Care.Controls.CareLabel(Me.components)
        Me.txtBStaff = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel13 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel15 = New Care.Controls.CareLabel(Me.components)
        Me.txtBFrom = New Care.Controls.CareTextBox(Me.components)
        Me.txtBDate = New Care.Controls.CareTextBox(Me.components)
        Me.GroupControl3 = New Care.Controls.CareFrame()
        Me.gbxShift = New Care.Controls.CareFrame()
        Me.btnShiftOK = New Care.Controls.CareButton(Me.components)
        Me.btnShiftCancel = New Care.Controls.CareButton(Me.components)
        Me.txtShiftEnd = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.txtShiftHours = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel16 = New Care.Controls.CareLabel(Me.components)
        Me.txtShiftCost = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel19 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel20 = New Care.Controls.CareLabel(Me.components)
        Me.txtShiftStart = New Care.Controls.CareTextBox(Me.components)
        Me.cgASlots = New Care.Controls.CareGrid()
        Me.gbxSwapSlots = New Care.Controls.CareFrame()
        Me.cgBSlots = New Care.Controls.CareGrid()
        Me.btnDelete = New Care.Controls.CareButton(Me.components)
        Me.btnTimes = New Care.Controls.CareButton(Me.components)
        Me.btnSwap = New Care.Controls.CareButton(Me.components)
        Me.btnSwapComplete = New Care.Controls.CareButton(Me.components)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtACost.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAHours.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtARoom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtATo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAStaff.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtADate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxSwapHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxSwapHeader.SuspendLayout()
        CType(Me.txtBCost.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBHours.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBRoom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBStaff.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.gbxShift, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxShift.SuspendLayout()
        CType(Me.txtShiftEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtShiftHours.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtShiftCost.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtShiftStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxSwapSlots, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxSwapSlots.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.CareLabel14)
        Me.GroupControl1.Controls.Add(Me.txtACost)
        Me.GroupControl1.Controls.Add(Me.CareLabel4)
        Me.GroupControl1.Controls.Add(Me.txtAHours)
        Me.GroupControl1.Controls.Add(Me.CareLabel6)
        Me.GroupControl1.Controls.Add(Me.txtARoom)
        Me.GroupControl1.Controls.Add(Me.CareLabel5)
        Me.GroupControl1.Controls.Add(Me.txtATo)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.txtAStaff)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Controls.Add(Me.Label2)
        Me.GroupControl1.Controls.Add(Me.txtAFrom)
        Me.GroupControl1.Controls.Add(Me.txtADate)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(485, 182)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "GroupControl1"
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(10, 152)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(51, 15)
        Me.CareLabel14.TabIndex = 12
        Me.CareLabel14.Text = "Shift Cost"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtACost
        '
        Me.txtACost.CharacterCasing = CharacterCasing.Normal
        Me.txtACost.EnterMoveNextControl = True
        Me.txtACost.Location = New System.Drawing.Point(110, 149)
        Me.txtACost.MaxLength = 0
        Me.txtACost.Name = "txtACost"
        Me.txtACost.NumericAllowNegatives = False
        Me.txtACost.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtACost.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtACost.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtACost.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtACost.Properties.Appearance.Options.UseFont = True
        Me.txtACost.Properties.Appearance.Options.UseTextOptions = True
        Me.txtACost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtACost.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtACost.Size = New System.Drawing.Size(81, 22)
        Me.txtACost.TabIndex = 13
        Me.txtACost.Tag = "R"
        Me.txtACost.TextAlign = HorizontalAlignment.Left
        Me.txtACost.ToolTipText = ""
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(10, 124)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(59, 15)
        Me.CareLabel4.TabIndex = 10
        Me.CareLabel4.Text = "Shift Hours"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAHours
        '
        Me.txtAHours.CharacterCasing = CharacterCasing.Normal
        Me.txtAHours.EnterMoveNextControl = True
        Me.txtAHours.Location = New System.Drawing.Point(110, 121)
        Me.txtAHours.MaxLength = 0
        Me.txtAHours.Name = "txtAHours"
        Me.txtAHours.NumericAllowNegatives = False
        Me.txtAHours.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtAHours.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtAHours.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtAHours.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtAHours.Properties.Appearance.Options.UseFont = True
        Me.txtAHours.Properties.Appearance.Options.UseTextOptions = True
        Me.txtAHours.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtAHours.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtAHours.Size = New System.Drawing.Size(81, 22)
        Me.txtAHours.TabIndex = 11
        Me.txtAHours.Tag = "R"
        Me.txtAHours.TextAlign = HorizontalAlignment.Left
        Me.txtAHours.ToolTipText = ""
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(10, 68)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(59, 15)
        Me.CareLabel6.TabIndex = 4
        Me.CareLabel6.Text = "Shift Room"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtARoom
        '
        Me.txtARoom.CharacterCasing = CharacterCasing.Normal
        Me.txtARoom.EnterMoveNextControl = True
        Me.txtARoom.Location = New System.Drawing.Point(110, 65)
        Me.txtARoom.MaxLength = 0
        Me.txtARoom.Name = "txtARoom"
        Me.txtARoom.NumericAllowNegatives = False
        Me.txtARoom.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtARoom.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtARoom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtARoom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtARoom.Properties.Appearance.Options.UseFont = True
        Me.txtARoom.Properties.Appearance.Options.UseTextOptions = True
        Me.txtARoom.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtARoom.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtARoom.Size = New System.Drawing.Size(365, 22)
        Me.txtARoom.TabIndex = 5
        Me.txtARoom.Tag = "R"
        Me.txtARoom.TextAlign = HorizontalAlignment.Left
        Me.txtARoom.ToolTipText = ""
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(197, 96)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(11, 15)
        Me.CareLabel5.TabIndex = 8
        Me.CareLabel5.Text = "to"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtATo
        '
        Me.txtATo.CharacterCasing = CharacterCasing.Normal
        Me.txtATo.EnterMoveNextControl = True
        Me.txtATo.Location = New System.Drawing.Point(214, 93)
        Me.txtATo.MaxLength = 0
        Me.txtATo.Name = "txtATo"
        Me.txtATo.NumericAllowNegatives = False
        Me.txtATo.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtATo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtATo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtATo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtATo.Properties.Appearance.Options.UseFont = True
        Me.txtATo.Properties.Appearance.Options.UseTextOptions = True
        Me.txtATo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtATo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtATo.Size = New System.Drawing.Size(81, 22)
        Me.txtATo.TabIndex = 9
        Me.txtATo.Tag = "R"
        Me.txtATo.TextAlign = HorizontalAlignment.Left
        Me.txtATo.ToolTipText = ""
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(10, 96)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(59, 15)
        Me.CareLabel2.TabIndex = 6
        Me.CareLabel2.Text = "Shift Times"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAStaff
        '
        Me.txtAStaff.CharacterCasing = CharacterCasing.Normal
        Me.txtAStaff.EnterMoveNextControl = True
        Me.txtAStaff.Location = New System.Drawing.Point(110, 9)
        Me.txtAStaff.MaxLength = 0
        Me.txtAStaff.Name = "txtAStaff"
        Me.txtAStaff.NumericAllowNegatives = False
        Me.txtAStaff.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtAStaff.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtAStaff.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtAStaff.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtAStaff.Properties.Appearance.Options.UseFont = True
        Me.txtAStaff.Properties.Appearance.Options.UseTextOptions = True
        Me.txtAStaff.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtAStaff.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtAStaff.Size = New System.Drawing.Size(365, 22)
        Me.txtAStaff.TabIndex = 1
        Me.txtAStaff.Tag = "R"
        Me.txtAStaff.TextAlign = HorizontalAlignment.Left
        Me.txtAStaff.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(10, 40)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(51, 15)
        Me.CareLabel1.TabIndex = 2
        Me.CareLabel1.Text = "Shift Date"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label2.Location = New System.Drawing.Point(10, 12)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(83, 15)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Staff Member A"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAFrom
        '
        Me.txtAFrom.CharacterCasing = CharacterCasing.Normal
        Me.txtAFrom.EnterMoveNextControl = True
        Me.txtAFrom.Location = New System.Drawing.Point(110, 93)
        Me.txtAFrom.MaxLength = 0
        Me.txtAFrom.Name = "txtAFrom"
        Me.txtAFrom.NumericAllowNegatives = False
        Me.txtAFrom.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtAFrom.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtAFrom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtAFrom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtAFrom.Properties.Appearance.Options.UseFont = True
        Me.txtAFrom.Properties.Appearance.Options.UseTextOptions = True
        Me.txtAFrom.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtAFrom.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtAFrom.Size = New System.Drawing.Size(81, 22)
        Me.txtAFrom.TabIndex = 7
        Me.txtAFrom.Tag = "R"
        Me.txtAFrom.TextAlign = HorizontalAlignment.Left
        Me.txtAFrom.ToolTipText = ""
        '
        'txtADate
        '
        Me.txtADate.CharacterCasing = CharacterCasing.Normal
        Me.txtADate.EnterMoveNextControl = True
        Me.txtADate.Location = New System.Drawing.Point(110, 37)
        Me.txtADate.MaxLength = 0
        Me.txtADate.Name = "txtADate"
        Me.txtADate.NumericAllowNegatives = False
        Me.txtADate.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtADate.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtADate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtADate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtADate.Properties.Appearance.Options.UseFont = True
        Me.txtADate.Properties.Appearance.Options.UseTextOptions = True
        Me.txtADate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtADate.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtADate.Size = New System.Drawing.Size(365, 22)
        Me.txtADate.TabIndex = 3
        Me.txtADate.Tag = "R"
        Me.txtADate.TextAlign = HorizontalAlignment.Left
        Me.txtADate.ToolTipText = ""
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnCancel.DialogResult = DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(903, 577)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 25)
        Me.btnCancel.TabIndex = 8
        Me.btnCancel.Text = "Cancel"
        '
        'gbxSwapHeader
        '
        Me.gbxSwapHeader.Controls.Add(Me.btnBSelect)
        Me.gbxSwapHeader.Controls.Add(Me.CareLabel7)
        Me.gbxSwapHeader.Controls.Add(Me.txtBCost)
        Me.gbxSwapHeader.Controls.Add(Me.CareLabel8)
        Me.gbxSwapHeader.Controls.Add(Me.txtBHours)
        Me.gbxSwapHeader.Controls.Add(Me.CareLabel9)
        Me.gbxSwapHeader.Controls.Add(Me.txtBRoom)
        Me.gbxSwapHeader.Controls.Add(Me.CareLabel10)
        Me.gbxSwapHeader.Controls.Add(Me.txtBTo)
        Me.gbxSwapHeader.Controls.Add(Me.CareLabel12)
        Me.gbxSwapHeader.Controls.Add(Me.txtBStaff)
        Me.gbxSwapHeader.Controls.Add(Me.CareLabel13)
        Me.gbxSwapHeader.Controls.Add(Me.CareLabel15)
        Me.gbxSwapHeader.Controls.Add(Me.txtBFrom)
        Me.gbxSwapHeader.Controls.Add(Me.txtBDate)
        Me.gbxSwapHeader.Location = New System.Drawing.Point(503, 12)
        Me.gbxSwapHeader.Name = "gbxSwapHeader"
        Me.gbxSwapHeader.ShowCaption = False
        Me.gbxSwapHeader.Size = New System.Drawing.Size(485, 182)
        Me.gbxSwapHeader.TabIndex = 1
        Me.gbxSwapHeader.Text = "GroupControl2"
        '
        'btnBSelect
        '
        Me.btnBSelect.Location = New System.Drawing.Point(389, 7)
        Me.btnBSelect.Name = "btnBSelect"
        Me.btnBSelect.Size = New System.Drawing.Size(85, 25)
        Me.btnBSelect.TabIndex = 15
        Me.btnBSelect.Text = "Select"
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(10, 152)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(51, 15)
        Me.CareLabel7.TabIndex = 13
        Me.CareLabel7.Text = "Shift Cost"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBCost
        '
        Me.txtBCost.CharacterCasing = CharacterCasing.Normal
        Me.txtBCost.EnterMoveNextControl = True
        Me.txtBCost.Location = New System.Drawing.Point(110, 149)
        Me.txtBCost.MaxLength = 0
        Me.txtBCost.Name = "txtBCost"
        Me.txtBCost.NumericAllowNegatives = False
        Me.txtBCost.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtBCost.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBCost.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtBCost.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtBCost.Properties.Appearance.Options.UseFont = True
        Me.txtBCost.Properties.Appearance.Options.UseTextOptions = True
        Me.txtBCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtBCost.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtBCost.Size = New System.Drawing.Size(81, 22)
        Me.txtBCost.TabIndex = 14
        Me.txtBCost.Tag = "R"
        Me.txtBCost.TextAlign = HorizontalAlignment.Left
        Me.txtBCost.ToolTipText = ""
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(10, 124)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(59, 15)
        Me.CareLabel8.TabIndex = 11
        Me.CareLabel8.Text = "Shift Hours"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBHours
        '
        Me.txtBHours.CharacterCasing = CharacterCasing.Normal
        Me.txtBHours.EnterMoveNextControl = True
        Me.txtBHours.Location = New System.Drawing.Point(110, 121)
        Me.txtBHours.MaxLength = 0
        Me.txtBHours.Name = "txtBHours"
        Me.txtBHours.NumericAllowNegatives = False
        Me.txtBHours.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtBHours.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBHours.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtBHours.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtBHours.Properties.Appearance.Options.UseFont = True
        Me.txtBHours.Properties.Appearance.Options.UseTextOptions = True
        Me.txtBHours.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtBHours.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtBHours.Size = New System.Drawing.Size(81, 22)
        Me.txtBHours.TabIndex = 12
        Me.txtBHours.Tag = "R"
        Me.txtBHours.TextAlign = HorizontalAlignment.Left
        Me.txtBHours.ToolTipText = ""
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(10, 68)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(59, 15)
        Me.CareLabel9.TabIndex = 5
        Me.CareLabel9.Text = "Shift Room"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBRoom
        '
        Me.txtBRoom.CharacterCasing = CharacterCasing.Normal
        Me.txtBRoom.EnterMoveNextControl = True
        Me.txtBRoom.Location = New System.Drawing.Point(110, 65)
        Me.txtBRoom.MaxLength = 0
        Me.txtBRoom.Name = "txtBRoom"
        Me.txtBRoom.NumericAllowNegatives = False
        Me.txtBRoom.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtBRoom.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBRoom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtBRoom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtBRoom.Properties.Appearance.Options.UseFont = True
        Me.txtBRoom.Properties.Appearance.Options.UseTextOptions = True
        Me.txtBRoom.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtBRoom.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtBRoom.Size = New System.Drawing.Size(364, 22)
        Me.txtBRoom.TabIndex = 6
        Me.txtBRoom.Tag = "R"
        Me.txtBRoom.TextAlign = HorizontalAlignment.Left
        Me.txtBRoom.ToolTipText = ""
        '
        'CareLabel10
        '
        Me.CareLabel10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel10.Location = New System.Drawing.Point(197, 96)
        Me.CareLabel10.Name = "CareLabel10"
        Me.CareLabel10.Size = New System.Drawing.Size(11, 15)
        Me.CareLabel10.TabIndex = 9
        Me.CareLabel10.Text = "to"
        Me.CareLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBTo
        '
        Me.txtBTo.CharacterCasing = CharacterCasing.Normal
        Me.txtBTo.EnterMoveNextControl = True
        Me.txtBTo.Location = New System.Drawing.Point(214, 93)
        Me.txtBTo.MaxLength = 0
        Me.txtBTo.Name = "txtBTo"
        Me.txtBTo.NumericAllowNegatives = False
        Me.txtBTo.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtBTo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBTo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtBTo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtBTo.Properties.Appearance.Options.UseFont = True
        Me.txtBTo.Properties.Appearance.Options.UseTextOptions = True
        Me.txtBTo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtBTo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtBTo.Size = New System.Drawing.Size(81, 22)
        Me.txtBTo.TabIndex = 10
        Me.txtBTo.Tag = "R"
        Me.txtBTo.TextAlign = HorizontalAlignment.Left
        Me.txtBTo.ToolTipText = ""
        '
        'CareLabel12
        '
        Me.CareLabel12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel12.Location = New System.Drawing.Point(10, 96)
        Me.CareLabel12.Name = "CareLabel12"
        Me.CareLabel12.Size = New System.Drawing.Size(59, 15)
        Me.CareLabel12.TabIndex = 7
        Me.CareLabel12.Text = "Shift Times"
        Me.CareLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBStaff
        '
        Me.txtBStaff.CharacterCasing = CharacterCasing.Normal
        Me.txtBStaff.EnterMoveNextControl = True
        Me.txtBStaff.Location = New System.Drawing.Point(110, 9)
        Me.txtBStaff.MaxLength = 0
        Me.txtBStaff.Name = "txtBStaff"
        Me.txtBStaff.NumericAllowNegatives = False
        Me.txtBStaff.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtBStaff.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBStaff.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtBStaff.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtBStaff.Properties.Appearance.Options.UseFont = True
        Me.txtBStaff.Properties.Appearance.Options.UseTextOptions = True
        Me.txtBStaff.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtBStaff.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtBStaff.Size = New System.Drawing.Size(273, 22)
        Me.txtBStaff.TabIndex = 1
        Me.txtBStaff.Tag = "R"
        Me.txtBStaff.TextAlign = HorizontalAlignment.Left
        Me.txtBStaff.ToolTipText = ""
        '
        'CareLabel13
        '
        Me.CareLabel13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel13.Location = New System.Drawing.Point(10, 40)
        Me.CareLabel13.Name = "CareLabel13"
        Me.CareLabel13.Size = New System.Drawing.Size(51, 15)
        Me.CareLabel13.TabIndex = 3
        Me.CareLabel13.Text = "Shift Date"
        Me.CareLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel15
        '
        Me.CareLabel15.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel15.Location = New System.Drawing.Point(10, 12)
        Me.CareLabel15.Name = "CareLabel15"
        Me.CareLabel15.Size = New System.Drawing.Size(82, 15)
        Me.CareLabel15.TabIndex = 0
        Me.CareLabel15.Text = "Staff Member B"
        Me.CareLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBFrom
        '
        Me.txtBFrom.CharacterCasing = CharacterCasing.Normal
        Me.txtBFrom.EnterMoveNextControl = True
        Me.txtBFrom.Location = New System.Drawing.Point(110, 93)
        Me.txtBFrom.MaxLength = 0
        Me.txtBFrom.Name = "txtBFrom"
        Me.txtBFrom.NumericAllowNegatives = False
        Me.txtBFrom.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtBFrom.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBFrom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtBFrom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtBFrom.Properties.Appearance.Options.UseFont = True
        Me.txtBFrom.Properties.Appearance.Options.UseTextOptions = True
        Me.txtBFrom.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtBFrom.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtBFrom.Size = New System.Drawing.Size(81, 22)
        Me.txtBFrom.TabIndex = 8
        Me.txtBFrom.Tag = "R"
        Me.txtBFrom.TextAlign = HorizontalAlignment.Left
        Me.txtBFrom.ToolTipText = ""
        '
        'txtBDate
        '
        Me.txtBDate.CharacterCasing = CharacterCasing.Normal
        Me.txtBDate.EnterMoveNextControl = True
        Me.txtBDate.Location = New System.Drawing.Point(110, 37)
        Me.txtBDate.MaxLength = 0
        Me.txtBDate.Name = "txtBDate"
        Me.txtBDate.NumericAllowNegatives = False
        Me.txtBDate.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtBDate.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtBDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtBDate.Properties.Appearance.Options.UseFont = True
        Me.txtBDate.Properties.Appearance.Options.UseTextOptions = True
        Me.txtBDate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtBDate.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtBDate.Size = New System.Drawing.Size(364, 22)
        Me.txtBDate.TabIndex = 4
        Me.txtBDate.Tag = "R"
        Me.txtBDate.TextAlign = HorizontalAlignment.Left
        Me.txtBDate.ToolTipText = ""
        '
        'GroupControl3
        '
        Me.GroupControl3.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left), AnchorStyles)
        Me.GroupControl3.Controls.Add(Me.gbxShift)
        Me.GroupControl3.Controls.Add(Me.cgASlots)
        Me.GroupControl3.Location = New System.Drawing.Point(12, 200)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.ShowCaption = False
        Me.GroupControl3.Size = New System.Drawing.Size(485, 369)
        Me.GroupControl3.TabIndex = 2
        Me.GroupControl3.Text = "GroupControl3"
        '
        'gbxShift
        '
        Me.gbxShift.Anchor = AnchorStyles.None
        Me.gbxShift.Controls.Add(Me.btnShiftOK)
        Me.gbxShift.Controls.Add(Me.btnShiftCancel)
        Me.gbxShift.Controls.Add(Me.txtShiftEnd)
        Me.gbxShift.Controls.Add(Me.CareLabel11)
        Me.gbxShift.Controls.Add(Me.txtShiftHours)
        Me.gbxShift.Controls.Add(Me.CareLabel16)
        Me.gbxShift.Controls.Add(Me.txtShiftCost)
        Me.gbxShift.Controls.Add(Me.CareLabel19)
        Me.gbxShift.Controls.Add(Me.CareLabel20)
        Me.gbxShift.Controls.Add(Me.txtShiftStart)
        Me.gbxShift.Location = New System.Drawing.Point(57, 96)
        Me.gbxShift.Name = "gbxShift"
        Me.gbxShift.Size = New System.Drawing.Size(361, 152)
        Me.gbxShift.TabIndex = 1
        Me.gbxShift.Text = "Amend Shift Times"
        '
        'btnShiftOK
        '
        Me.btnShiftOK.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnShiftOK.Location = New System.Drawing.Point(172, 119)
        Me.btnShiftOK.Name = "btnShiftOK"
        Me.btnShiftOK.Size = New System.Drawing.Size(85, 25)
        Me.btnShiftOK.TabIndex = 8
        Me.btnShiftOK.Text = "OK"
        '
        'btnShiftCancel
        '
        Me.btnShiftCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnShiftCancel.Location = New System.Drawing.Point(263, 119)
        Me.btnShiftCancel.Name = "btnShiftCancel"
        Me.btnShiftCancel.Size = New System.Drawing.Size(85, 25)
        Me.btnShiftCancel.TabIndex = 9
        Me.btnShiftCancel.Text = "Cancel"
        '
        'txtShiftEnd
        '
        Me.txtShiftEnd.CharacterCasing = CharacterCasing.Normal
        Me.txtShiftEnd.EnterMoveNextControl = True
        Me.txtShiftEnd.Location = New System.Drawing.Point(267, 28)
        Me.txtShiftEnd.MaxLength = 5
        Me.txtShiftEnd.Name = "txtShiftEnd"
        Me.txtShiftEnd.NumericAllowNegatives = False
        Me.txtShiftEnd.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.Time
        Me.txtShiftEnd.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtShiftEnd.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtShiftEnd.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtShiftEnd.Properties.Appearance.Options.UseFont = True
        Me.txtShiftEnd.Properties.Appearance.Options.UseTextOptions = True
        Me.txtShiftEnd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtShiftEnd.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic
        Me.txtShiftEnd.Properties.Mask.EditMask = "(0?\d|1\d|2[0-3])\:[0-5]\d"
        Me.txtShiftEnd.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtShiftEnd.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtShiftEnd.Properties.MaxLength = 5
        Me.txtShiftEnd.Size = New System.Drawing.Size(81, 22)
        Me.txtShiftEnd.TabIndex = 3
        Me.txtShiftEnd.Tag = ""
        Me.txtShiftEnd.TextAlign = HorizontalAlignment.Left
        Me.txtShiftEnd.ToolTipText = ""
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(211, 31)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(50, 15)
        Me.CareLabel11.TabIndex = 2
        Me.CareLabel11.Text = "End Time"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtShiftHours
        '
        Me.txtShiftHours.CharacterCasing = CharacterCasing.Normal
        Me.txtShiftHours.EnterMoveNextControl = True
        Me.txtShiftHours.Location = New System.Drawing.Point(108, 56)
        Me.txtShiftHours.MaxLength = 0
        Me.txtShiftHours.Name = "txtShiftHours"
        Me.txtShiftHours.NumericAllowNegatives = False
        Me.txtShiftHours.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtShiftHours.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtShiftHours.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtShiftHours.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtShiftHours.Properties.Appearance.Options.UseFont = True
        Me.txtShiftHours.Properties.Appearance.Options.UseTextOptions = True
        Me.txtShiftHours.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtShiftHours.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtShiftHours.Size = New System.Drawing.Size(81, 22)
        Me.txtShiftHours.TabIndex = 5
        Me.txtShiftHours.Tag = "R"
        Me.txtShiftHours.TextAlign = HorizontalAlignment.Left
        Me.txtShiftHours.ToolTipText = ""
        '
        'CareLabel16
        '
        Me.CareLabel16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel16.Location = New System.Drawing.Point(9, 87)
        Me.CareLabel16.Name = "CareLabel16"
        Me.CareLabel16.Size = New System.Drawing.Size(51, 15)
        Me.CareLabel16.TabIndex = 6
        Me.CareLabel16.Text = "Shift Cost"
        Me.CareLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtShiftCost
        '
        Me.txtShiftCost.CharacterCasing = CharacterCasing.Normal
        Me.txtShiftCost.EnterMoveNextControl = True
        Me.txtShiftCost.Location = New System.Drawing.Point(108, 84)
        Me.txtShiftCost.MaxLength = 0
        Me.txtShiftCost.Name = "txtShiftCost"
        Me.txtShiftCost.NumericAllowNegatives = False
        Me.txtShiftCost.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtShiftCost.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtShiftCost.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtShiftCost.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtShiftCost.Properties.Appearance.Options.UseFont = True
        Me.txtShiftCost.Properties.Appearance.Options.UseTextOptions = True
        Me.txtShiftCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtShiftCost.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtShiftCost.Size = New System.Drawing.Size(81, 22)
        Me.txtShiftCost.TabIndex = 7
        Me.txtShiftCost.Tag = "R"
        Me.txtShiftCost.TextAlign = HorizontalAlignment.Left
        Me.txtShiftCost.ToolTipText = ""
        '
        'CareLabel19
        '
        Me.CareLabel19.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel19.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel19.Location = New System.Drawing.Point(9, 59)
        Me.CareLabel19.Name = "CareLabel19"
        Me.CareLabel19.Size = New System.Drawing.Size(59, 15)
        Me.CareLabel19.TabIndex = 4
        Me.CareLabel19.Text = "Shift Hours"
        Me.CareLabel19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel20
        '
        Me.CareLabel20.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel20.Location = New System.Drawing.Point(9, 31)
        Me.CareLabel20.Name = "CareLabel20"
        Me.CareLabel20.Size = New System.Drawing.Size(81, 15)
        Me.CareLabel20.TabIndex = 0
        Me.CareLabel20.Text = "Shift Start Time"
        Me.CareLabel20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtShiftStart
        '
        Me.txtShiftStart.CharacterCasing = CharacterCasing.Normal
        Me.txtShiftStart.EnterMoveNextControl = True
        Me.txtShiftStart.Location = New System.Drawing.Point(108, 28)
        Me.txtShiftStart.MaxLength = 5
        Me.txtShiftStart.Name = "txtShiftStart"
        Me.txtShiftStart.NumericAllowNegatives = False
        Me.txtShiftStart.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.Time
        Me.txtShiftStart.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtShiftStart.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtShiftStart.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtShiftStart.Properties.Appearance.Options.UseFont = True
        Me.txtShiftStart.Properties.Appearance.Options.UseTextOptions = True
        Me.txtShiftStart.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtShiftStart.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic
        Me.txtShiftStart.Properties.Mask.EditMask = "(0?\d|1\d|2[0-3])\:[0-5]\d"
        Me.txtShiftStart.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtShiftStart.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtShiftStart.Properties.MaxLength = 5
        Me.txtShiftStart.Size = New System.Drawing.Size(81, 22)
        Me.txtShiftStart.TabIndex = 1
        Me.txtShiftStart.Tag = ""
        Me.txtShiftStart.TextAlign = HorizontalAlignment.Left
        Me.txtShiftStart.ToolTipText = ""
        '
        'cgASlots
        '
        Me.cgASlots.AllowBuildColumns = True
        Me.cgASlots.AllowEdit = False
        Me.cgASlots.AllowHorizontalScroll = False
        Me.cgASlots.AllowMultiSelect = False
        Me.cgASlots.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgASlots.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgASlots.Appearance.Options.UseFont = True
        Me.cgASlots.AutoSizeByData = True
        Me.cgASlots.DisableAutoSize = False
        Me.cgASlots.DisableDataFormatting = False
        Me.cgASlots.FocusedRowHandle = -2147483648
        Me.cgASlots.HideFirstColumn = False
        Me.cgASlots.Location = New System.Drawing.Point(5, 5)
        Me.cgASlots.Name = "cgASlots"
        Me.cgASlots.PreviewColumn = ""
        Me.cgASlots.QueryID = Nothing
        Me.cgASlots.RowAutoHeight = True
        Me.cgASlots.SearchAsYouType = True
        Me.cgASlots.ShowAutoFilterRow = False
        Me.cgASlots.ShowFindPanel = False
        Me.cgASlots.ShowGroupByBox = False
        Me.cgASlots.ShowLoadingPanel = False
        Me.cgASlots.ShowNavigator = False
        Me.cgASlots.Size = New System.Drawing.Size(475, 359)
        Me.cgASlots.TabIndex = 0
        '
        'gbxSwapSlots
        '
        Me.gbxSwapSlots.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left), AnchorStyles)
        Me.gbxSwapSlots.Controls.Add(Me.cgBSlots)
        Me.gbxSwapSlots.Location = New System.Drawing.Point(503, 200)
        Me.gbxSwapSlots.Name = "gbxSwapSlots"
        Me.gbxSwapSlots.ShowCaption = False
        Me.gbxSwapSlots.Size = New System.Drawing.Size(485, 369)
        Me.gbxSwapSlots.TabIndex = 3
        Me.gbxSwapSlots.Text = "GroupControl4"
        '
        'cgBSlots
        '
        Me.cgBSlots.AllowBuildColumns = True
        Me.cgBSlots.AllowEdit = False
        Me.cgBSlots.AllowHorizontalScroll = False
        Me.cgBSlots.AllowMultiSelect = False
        Me.cgBSlots.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgBSlots.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgBSlots.Appearance.Options.UseFont = True
        Me.cgBSlots.AutoSizeByData = True
        Me.cgBSlots.DisableAutoSize = False
        Me.cgBSlots.DisableDataFormatting = False
        Me.cgBSlots.FocusedRowHandle = -2147483648
        Me.cgBSlots.HideFirstColumn = False
        Me.cgBSlots.Location = New System.Drawing.Point(5, 5)
        Me.cgBSlots.Name = "cgBSlots"
        Me.cgBSlots.PreviewColumn = ""
        Me.cgBSlots.QueryID = Nothing
        Me.cgBSlots.RowAutoHeight = True
        Me.cgBSlots.SearchAsYouType = True
        Me.cgBSlots.ShowAutoFilterRow = False
        Me.cgBSlots.ShowFindPanel = False
        Me.cgBSlots.ShowGroupByBox = False
        Me.cgBSlots.ShowLoadingPanel = False
        Me.cgBSlots.ShowNavigator = False
        Me.cgBSlots.Size = New System.Drawing.Size(475, 359)
        Me.cgBSlots.TabIndex = 0
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnDelete.Location = New System.Drawing.Point(12, 577)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(125, 25)
        Me.btnDelete.TabIndex = 4
        Me.btnDelete.Text = "Delete Shift"
        '
        'btnTimes
        '
        Me.btnTimes.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnTimes.Location = New System.Drawing.Point(143, 577)
        Me.btnTimes.Name = "btnTimes"
        Me.btnTimes.Size = New System.Drawing.Size(125, 25)
        Me.btnTimes.TabIndex = 5
        Me.btnTimes.Text = "Amend Shift Times"
        '
        'btnSwap
        '
        Me.btnSwap.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnSwap.Location = New System.Drawing.Point(274, 577)
        Me.btnSwap.Name = "btnSwap"
        Me.btnSwap.Size = New System.Drawing.Size(125, 25)
        Me.btnSwap.TabIndex = 6
        Me.btnSwap.Text = "Swap Shift"
        '
        'btnSwapComplete
        '
        Me.btnSwapComplete.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnSwapComplete.Location = New System.Drawing.Point(772, 577)
        Me.btnSwapComplete.Name = "btnSwapComplete"
        Me.btnSwapComplete.Size = New System.Drawing.Size(125, 25)
        Me.btnSwapComplete.TabIndex = 7
        Me.btnSwapComplete.Text = "Complete Swap"
        '
        'frmRosterShiftChange
        '
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(1000, 611)
        Me.Controls.Add(Me.btnSwapComplete)
        Me.Controls.Add(Me.btnSwap)
        Me.Controls.Add(Me.btnTimes)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.gbxSwapSlots)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.gbxSwapHeader)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.GroupControl1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmRosterShiftChange"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "Change Shift"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtACost.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAHours.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtARoom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtATo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAStaff.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtADate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxSwapHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxSwapHeader.ResumeLayout(False)
        Me.gbxSwapHeader.PerformLayout()
        CType(Me.txtBCost.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBHours.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBRoom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBStaff.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.gbxShift, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxShift.ResumeLayout(False)
        Me.gbxShift.PerformLayout()
        CType(Me.txtShiftEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtShiftHours.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtShiftCost.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtShiftStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxSwapSlots, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxSwapSlots.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtAFrom As Care.Controls.CareTextBox
    Friend WithEvents txtADate As Care.Controls.CareTextBox
    Friend WithEvents txtAStaff As Care.Controls.CareTextBox
    Friend WithEvents Label2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents txtATo As Care.Controls.CareTextBox
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents txtARoom As Care.Controls.CareTextBox
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents txtACost As Care.Controls.CareTextBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents txtAHours As Care.Controls.CareTextBox
    Friend WithEvents gbxSwapHeader As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents txtBCost As Care.Controls.CareTextBox
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents txtBHours As Care.Controls.CareTextBox
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents txtBRoom As Care.Controls.CareTextBox
    Friend WithEvents CareLabel10 As Care.Controls.CareLabel
    Friend WithEvents txtBTo As Care.Controls.CareTextBox
    Friend WithEvents CareLabel12 As Care.Controls.CareLabel
    Friend WithEvents txtBStaff As Care.Controls.CareTextBox
    Friend WithEvents CareLabel13 As Care.Controls.CareLabel
    Friend WithEvents CareLabel15 As Care.Controls.CareLabel
    Friend WithEvents txtBFrom As Care.Controls.CareTextBox
    Friend WithEvents txtBDate As Care.Controls.CareTextBox
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cgASlots As Care.Controls.CareGrid
    Friend WithEvents gbxSwapSlots As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cgBSlots As Care.Controls.CareGrid
    Friend WithEvents btnDelete As Care.Controls.CareButton
    Friend WithEvents btnTimes As Care.Controls.CareButton
    Friend WithEvents btnSwap As Care.Controls.CareButton
    Friend WithEvents btnSwapComplete As Care.Controls.CareButton
    Friend WithEvents gbxShift As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnShiftOK As Care.Controls.CareButton
    Friend WithEvents btnShiftCancel As Care.Controls.CareButton
    Friend WithEvents txtShiftEnd As Care.Controls.CareTextBox
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents txtShiftHours As Care.Controls.CareTextBox
    Friend WithEvents CareLabel16 As Care.Controls.CareLabel
    Friend WithEvents txtShiftCost As Care.Controls.CareTextBox
    Friend WithEvents CareLabel19 As Care.Controls.CareLabel
    Friend WithEvents CareLabel20 As Care.Controls.CareLabel
    Friend WithEvents txtShiftStart As Care.Controls.CareTextBox
    Friend WithEvents btnBSelect As Care.Controls.CareButton
End Class
