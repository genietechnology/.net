﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChildFind
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling3 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling4 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.GroupBox12 = New Care.Controls.CareFrame()
        Me.cbxPreset = New Care.Controls.CareComboBox(Me.components)
        Me.txtCriteria = New Care.Controls.CareTextBox(Me.components)
        Me.GroupControl2 = New Care.Controls.CareFrame()
        Me.cbxAllergyAndIncidents = New Care.Controls.CareComboBox(Me.components)
        Me.GroupControl3 = New Care.Controls.CareFrame()
        Me.Label1 = New Care.Controls.CareLabel(Me.components)
        Me.btnReset = New Care.Controls.CareButton(Me.components)
        Me.btnClear = New Care.Controls.CareButton(Me.components)
        Me.btnSearch = New Care.Controls.CareButton(Me.components)
        Me.grdSearch = New Care.Controls.CareGrid()
        Me.GroupControl5 = New Care.Controls.CareFrame()
        Me.txtGreen = New DevExpress.XtraEditors.TextEdit()
        Me.txtRed = New DevExpress.XtraEditors.TextEdit()
        Me.txtYellow = New DevExpress.XtraEditors.TextEdit()
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.GroupControl6 = New Care.Controls.CareFrame()
        Me.cbxSort = New Care.Controls.CareComboBox(Me.components)
        Me.GroupControl4 = New Care.Controls.CareFrame()
        Me.cbxAdvAllergy = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.GroupControl7 = New Care.Controls.CareFrame()
        Me.cbxAdvConsent = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.GroupControl8 = New Care.Controls.CareFrame()
        Me.cbxPhotos = New Care.Controls.CareComboBox(Me.components)
        Me.GroupControl9 = New Care.Controls.CareFrame()
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl10 = New Care.Controls.CareFrame()
        Me.cbxSite = New Care.Controls.CareComboBox(Me.components)
        Me.cbxSpare = New Care.Controls.CareComboBox(Me.components)
        Me.GroupControl11 = New Care.Controls.CareFrame()
        Me.cbxColumnSets = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.GroupControl12 = New Care.Controls.CareFrame()
        Me.cbxTags = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        CType(Me.GroupBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox12.SuspendLayout()
        CType(Me.cbxPreset.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCriteria.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.cbxAllergyAndIncidents.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl6.SuspendLayout()
        CType(Me.cbxSort.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.cbxAdvAllergy.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl7.SuspendLayout()
        CType(Me.cbxAdvConsent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl8.SuspendLayout()
        CType(Me.cbxPhotos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl9.SuspendLayout()
        CType(Me.GroupControl10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl10.SuspendLayout()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSpare.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl11.SuspendLayout()
        CType(Me.cbxColumnSets.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl12.SuspendLayout()
        CType(Me.cbxTags.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupBox12
        '
        Me.GroupBox12.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupBox12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupBox12.Appearance.Options.UseFont = True
        Me.GroupBox12.Controls.Add(Me.cbxPreset)
        Me.GroupBox12.Location = New System.Drawing.Point(9, 9)
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.Size = New System.Drawing.Size(388, 54)
        Me.GroupBox12.TabIndex = 0
        Me.GroupBox12.Text = "Preset Filters"
        '
        'cbxPreset
        '
        Me.cbxPreset.AllowBlank = False
        Me.cbxPreset.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxPreset.DataSource = Nothing
        Me.cbxPreset.DisplayMember = Nothing
        Me.cbxPreset.EnterMoveNextControl = True
        Me.cbxPreset.Location = New System.Drawing.Point(10, 25)
        Me.cbxPreset.Name = "cbxPreset"
        Me.cbxPreset.Properties.AccessibleName = "Gender"
        Me.cbxPreset.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxPreset.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxPreset.Properties.Appearance.Options.UseFont = True
        Me.cbxPreset.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxPreset.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxPreset.SelectedValue = Nothing
        Me.cbxPreset.Size = New System.Drawing.Size(370, 20)
        Me.cbxPreset.TabIndex = 0
        Me.cbxPreset.Tag = ""
        Me.cbxPreset.ValueMember = Nothing
        '
        'txtCriteria
        '
        Me.txtCriteria.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtCriteria.CharacterCasing = CharacterCasing.Upper
        Me.txtCriteria.EnterMoveNextControl = True
        Me.txtCriteria.Location = New System.Drawing.Point(129, 27)
        Me.txtCriteria.MaxLength = 40
        Me.txtCriteria.Name = "txtCriteria"
        Me.txtCriteria.NumericAllowNegatives = False
        Me.txtCriteria.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtCriteria.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCriteria.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtCriteria.Properties.Appearance.BackColor = System.Drawing.Color.PaleGreen
        Me.txtCriteria.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtCriteria.Properties.Appearance.Options.UseBackColor = True
        Me.txtCriteria.Properties.Appearance.Options.UseFont = True
        Me.txtCriteria.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCriteria.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtCriteria.Properties.CharacterCasing = CharacterCasing.Upper
        Me.txtCriteria.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.None
        Me.txtCriteria.Properties.Mask.EditMask = "(\w*|\w*\s\w*)"
        Me.txtCriteria.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtCriteria.Properties.Mask.SaveLiteral = False
        Me.txtCriteria.Properties.Mask.ShowPlaceHolders = False
        Me.txtCriteria.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtCriteria.Properties.MaxLength = 40
        Me.txtCriteria.Properties.ValidateOnEnterKey = True
        Me.txtCriteria.Size = New System.Drawing.Size(288, 20)
        Me.txtCriteria.TabIndex = 1
        Me.txtCriteria.TextAlign = HorizontalAlignment.Left
        Me.txtCriteria.ToolTipText = ""
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl2.Appearance.Options.UseFont = True
        Me.GroupControl2.Controls.Add(Me.cbxAllergyAndIncidents)
        Me.GroupControl2.Location = New System.Drawing.Point(403, 69)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(180, 57)
        Me.GroupControl2.TabIndex = 5
        Me.GroupControl2.Text = "Allergies and Dietary Filtering"
        '
        'cbxAllergyAndIncidents
        '
        Me.cbxAllergyAndIncidents.AllowBlank = False
        Me.cbxAllergyAndIncidents.DataSource = Nothing
        Me.cbxAllergyAndIncidents.DisplayMember = Nothing
        Me.cbxAllergyAndIncidents.EnterMoveNextControl = True
        Me.cbxAllergyAndIncidents.Location = New System.Drawing.Point(5, 26)
        Me.cbxAllergyAndIncidents.Name = "cbxAllergyAndIncidents"
        Me.cbxAllergyAndIncidents.Properties.AccessibleName = "Gender"
        Me.cbxAllergyAndIncidents.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxAllergyAndIncidents.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxAllergyAndIncidents.Properties.Appearance.Options.UseFont = True
        Me.cbxAllergyAndIncidents.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxAllergyAndIncidents.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxAllergyAndIncidents.SelectedValue = Nothing
        Me.cbxAllergyAndIncidents.Size = New System.Drawing.Size(170, 22)
        Me.cbxAllergyAndIncidents.TabIndex = 0
        Me.cbxAllergyAndIncidents.Tag = ""
        Me.cbxAllergyAndIncidents.ValueMember = Nothing
        '
        'GroupControl3
        '
        Me.GroupControl3.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl3.Appearance.Options.UseFont = True
        Me.GroupControl3.Controls.Add(Me.Label1)
        Me.GroupControl3.Controls.Add(Me.btnReset)
        Me.GroupControl3.Controls.Add(Me.btnClear)
        Me.GroupControl3.Controls.Add(Me.btnSearch)
        Me.GroupControl3.Controls.Add(Me.txtCriteria)
        Me.GroupControl3.Location = New System.Drawing.Point(9, 132)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(667, 57)
        Me.GroupControl3.TabIndex = 8
        Me.GroupControl3.Text = "Search by Name (Enter any part of the name and hit Enter or click Search)"
        '
        'Label1
        '
        Me.Label1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label1.Location = New System.Drawing.Point(12, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(111, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Forename / Surname"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnReset.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnReset.Appearance.Options.UseFont = True
        Me.btnReset.CausesValidation = False
        Me.btnReset.Location = New System.Drawing.Point(585, 25)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(75, 25)
        Me.btnReset.TabIndex = 4
        Me.btnReset.Tag = ""
        Me.btnReset.Text = "Reset"
        Me.btnReset.ToolTip = "Performing a reset will take the Search back to the defaults. This includes any c" & _
    "olumn movements, as well as the size and location of this form."
        Me.btnReset.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.btnReset.ToolTipTitle = "Reset"
        '
        'btnClear
        '
        Me.btnClear.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnClear.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClear.Appearance.Options.UseFont = True
        Me.btnClear.CausesValidation = False
        Me.btnClear.Location = New System.Drawing.Point(504, 25)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(75, 25)
        Me.btnClear.TabIndex = 3
        Me.btnClear.Tag = ""
        Me.btnClear.Text = "Clear"
        Me.btnClear.ToolTip = "Performing a clear will reset all filters back to the default"
        Me.btnClear.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.btnClear.ToolTipTitle = "Clear"
        '
        'btnSearch
        '
        Me.btnSearch.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnSearch.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSearch.Appearance.Options.UseFont = True
        Me.btnSearch.Location = New System.Drawing.Point(423, 25)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(75, 25)
        Me.btnSearch.TabIndex = 2
        Me.btnSearch.Tag = ""
        Me.btnSearch.Text = "Search"
        '
        'grdSearch
        '
        Me.grdSearch.AllowBuildColumns = True
        Me.grdSearch.AllowEdit = False
        Me.grdSearch.AllowHorizontalScroll = False
        Me.grdSearch.AllowMultiSelect = False
        Me.grdSearch.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.grdSearch.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdSearch.Appearance.Options.UseFont = True
        Me.grdSearch.AutoSizeByData = True
        Me.grdSearch.DisableAutoSize = False
        Me.grdSearch.DisableDataFormatting = False
        Me.grdSearch.FocusedRowHandle = -2147483648
        Me.grdSearch.HideFirstColumn = False
        Me.grdSearch.Location = New System.Drawing.Point(5, 24)
        Me.grdSearch.Name = "grdSearch"
        Me.grdSearch.PreviewColumn = ""
        Me.grdSearch.QueryID = Nothing
        Me.grdSearch.RowAutoHeight = False
        Me.grdSearch.SearchAsYouType = True
        Me.grdSearch.ShowAutoFilterRow = False
        Me.grdSearch.ShowFindPanel = False
        Me.grdSearch.ShowGroupByBox = False
        Me.grdSearch.ShowLoadingPanel = True
        Me.grdSearch.ShowNavigator = True
        Me.grdSearch.Size = New System.Drawing.Size(976, 469)
        Me.grdSearch.TabIndex = 0
        Me.grdSearch.TabStop = False
        '
        'GroupControl5
        '
        Me.GroupControl5.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl5.Appearance.Options.UseFont = True
        Me.GroupControl5.Controls.Add(Me.txtGreen)
        Me.GroupControl5.Controls.Add(Me.txtRed)
        Me.GroupControl5.Controls.Add(Me.txtYellow)
        Me.GroupControl5.Controls.Add(Me.grdSearch)
        Me.GroupControl5.Controls.Add(Me.btnClose)
        Me.GroupControl5.Location = New System.Drawing.Point(9, 195)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(986, 498)
        Me.GroupControl5.TabIndex = 11
        Me.GroupControl5.Text = "Child Records (Double-Click to select record)"
        '
        'txtGreen
        '
        Me.txtGreen.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.txtGreen.Location = New System.Drawing.Point(819, -43)
        Me.txtGreen.Name = "txtGreen"
        Me.txtGreen.Properties.Appearance.BackColor = System.Drawing.Color.LightGreen
        Me.txtGreen.Properties.Appearance.Options.UseBackColor = True
        Me.txtGreen.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtGreen, True)
        Me.txtGreen.Size = New System.Drawing.Size(50, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtGreen, OptionsSpelling3)
        Me.txtGreen.TabIndex = 0
        Me.txtGreen.TabStop = False
        Me.txtGreen.Visible = False
        '
        'txtRed
        '
        Me.txtRed.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.txtRed.Location = New System.Drawing.Point(931, -43)
        Me.txtRed.Name = "txtRed"
        Me.txtRed.Properties.Appearance.BackColor = System.Drawing.Color.LightCoral
        Me.txtRed.Properties.Appearance.Options.UseBackColor = True
        Me.txtRed.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtRed, True)
        Me.txtRed.Size = New System.Drawing.Size(50, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtRed, OptionsSpelling4)
        Me.txtRed.TabIndex = 2
        Me.txtRed.TabStop = False
        Me.txtRed.Visible = False
        '
        'txtYellow
        '
        Me.txtYellow.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.txtYellow.Location = New System.Drawing.Point(875, -43)
        Me.txtYellow.Name = "txtYellow"
        Me.txtYellow.Properties.Appearance.BackColor = System.Drawing.Color.Khaki
        Me.txtYellow.Properties.Appearance.Options.UseBackColor = True
        Me.txtYellow.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtYellow, True)
        Me.txtYellow.Size = New System.Drawing.Size(50, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtYellow, OptionsSpelling1)
        Me.txtYellow.TabIndex = 1
        Me.txtYellow.TabStop = False
        Me.txtYellow.Visible = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.DialogResult = DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(911, 473)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 25)
        Me.btnClose.TabIndex = 9
        Me.btnClose.Tag = ""
        Me.btnClose.Text = "Close"
        Me.btnClose.ToolTip = "Performing a reset will take the Search back to the defaults. This includes any c" & _
    "olumn movements, as well as the size and location of this form."
        Me.btnClose.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.btnClose.ToolTipTitle = "Reset"
        '
        'GroupControl6
        '
        Me.GroupControl6.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl6.Appearance.Options.UseFont = True
        Me.GroupControl6.Controls.Add(Me.cbxSort)
        Me.GroupControl6.Location = New System.Drawing.Point(775, 132)
        Me.GroupControl6.Name = "GroupControl6"
        Me.GroupControl6.Size = New System.Drawing.Size(220, 57)
        Me.GroupControl6.TabIndex = 10
        Me.GroupControl6.Text = "Sorting"
        '
        'cbxSort
        '
        Me.cbxSort.AllowBlank = False
        Me.cbxSort.DataSource = Nothing
        Me.cbxSort.DisplayMember = Nothing
        Me.cbxSort.EnterMoveNextControl = True
        Me.cbxSort.Location = New System.Drawing.Point(5, 27)
        Me.cbxSort.Name = "cbxSort"
        Me.cbxSort.Properties.AccessibleName = "Gender"
        Me.cbxSort.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSort.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSort.Properties.Appearance.Options.UseFont = True
        Me.cbxSort.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSort.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSort.SelectedValue = Nothing
        Me.cbxSort.Size = New System.Drawing.Size(210, 22)
        Me.cbxSort.TabIndex = 0
        Me.cbxSort.Tag = ""
        Me.cbxSort.ValueMember = Nothing
        '
        'GroupControl4
        '
        Me.GroupControl4.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl4.Appearance.Options.UseFont = True
        Me.GroupControl4.Controls.Add(Me.cbxAdvAllergy)
        Me.GroupControl4.Location = New System.Drawing.Point(589, 69)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(180, 57)
        Me.GroupControl4.TabIndex = 6
        Me.GroupControl4.Text = "Advanced Allergy Filtering"
        '
        'cbxAdvAllergy
        '
        Me.cbxAdvAllergy.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.cbxAdvAllergy.Location = New System.Drawing.Point(5, 26)
        Me.cbxAdvAllergy.Name = "cbxAdvAllergy"
        Me.cbxAdvAllergy.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxAdvAllergy.Properties.SelectAllItemVisible = False
        Me.cbxAdvAllergy.Size = New System.Drawing.Size(170, 20)
        Me.cbxAdvAllergy.TabIndex = 0
        '
        'GroupControl7
        '
        Me.GroupControl7.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl7.Appearance.Options.UseFont = True
        Me.GroupControl7.Controls.Add(Me.cbxAdvConsent)
        Me.GroupControl7.Location = New System.Drawing.Point(775, 69)
        Me.GroupControl7.Name = "GroupControl7"
        Me.GroupControl7.Size = New System.Drawing.Size(220, 57)
        Me.GroupControl7.TabIndex = 7
        Me.GroupControl7.Text = "Advanced Consent Filtering"
        '
        'cbxAdvConsent
        '
        Me.cbxAdvConsent.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.cbxAdvConsent.Location = New System.Drawing.Point(5, 26)
        Me.cbxAdvConsent.Name = "cbxAdvConsent"
        Me.cbxAdvConsent.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxAdvConsent.Properties.SelectAllItemVisible = False
        Me.cbxAdvConsent.Size = New System.Drawing.Size(210, 20)
        Me.cbxAdvConsent.TabIndex = 0
        '
        'GroupControl8
        '
        Me.GroupControl8.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl8.Appearance.Options.UseFont = True
        Me.GroupControl8.Controls.Add(Me.cbxPhotos)
        Me.GroupControl8.Location = New System.Drawing.Point(682, 132)
        Me.GroupControl8.Name = "GroupControl8"
        Me.GroupControl8.Size = New System.Drawing.Size(87, 57)
        Me.GroupControl8.TabIndex = 9
        Me.GroupControl8.Text = "Photos"
        '
        'cbxPhotos
        '
        Me.cbxPhotos.AllowBlank = False
        Me.cbxPhotos.DataSource = Nothing
        Me.cbxPhotos.DisplayMember = Nothing
        Me.cbxPhotos.EnterMoveNextControl = True
        Me.cbxPhotos.Location = New System.Drawing.Point(5, 26)
        Me.cbxPhotos.Name = "cbxPhotos"
        Me.cbxPhotos.Properties.AccessibleName = "Gender"
        Me.cbxPhotos.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxPhotos.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxPhotos.Properties.Appearance.Options.UseFont = True
        Me.cbxPhotos.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxPhotos.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxPhotos.SelectedValue = Nothing
        Me.cbxPhotos.Size = New System.Drawing.Size(77, 22)
        Me.cbxPhotos.TabIndex = 0
        Me.cbxPhotos.Tag = ""
        Me.cbxPhotos.ValueMember = Nothing
        '
        'GroupControl9
        '
        Me.GroupControl9.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl9.Appearance.Options.UseFont = True
        Me.GroupControl9.Controls.Add(Me.cbxTags)
        Me.GroupControl9.Controls.Add(Me.CareLabel1)
        Me.GroupControl9.Location = New System.Drawing.Point(9, 69)
        Me.GroupControl9.Name = "GroupControl9"
        Me.GroupControl9.Size = New System.Drawing.Size(388, 57)
        Me.GroupControl9.TabIndex = 4
        Me.GroupControl9.Text = "Tag Filtering"
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(12, 30)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(99, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Tag Search Criteria"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl10
        '
        Me.GroupControl10.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl10.Appearance.Options.UseFont = True
        Me.GroupControl10.Controls.Add(Me.cbxSite)
        Me.GroupControl10.Location = New System.Drawing.Point(403, 9)
        Me.GroupControl10.Name = "GroupControl10"
        Me.GroupControl10.Size = New System.Drawing.Size(180, 54)
        Me.GroupControl10.TabIndex = 1
        Me.GroupControl10.Text = "Site"
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(5, 24)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AccessibleName = "Gender"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(170, 22)
        Me.cbxSite.TabIndex = 0
        Me.cbxSite.Tag = ""
        Me.cbxSite.ValueMember = Nothing
        '
        'cbxSpare
        '
        Me.cbxSpare.AllowBlank = False
        Me.cbxSpare.DataSource = Nothing
        Me.cbxSpare.DisplayMember = Nothing
        Me.cbxSpare.EnterMoveNextControl = True
        Me.cbxSpare.Location = New System.Drawing.Point(5, 25)
        Me.cbxSpare.Name = "cbxSpare"
        Me.cbxSpare.Properties.AccessibleName = "Gender"
        Me.cbxSpare.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSpare.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSpare.Properties.Appearance.Options.UseFont = True
        Me.cbxSpare.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSpare.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSpare.SelectedValue = Nothing
        Me.cbxSpare.Size = New System.Drawing.Size(170, 22)
        Me.cbxSpare.TabIndex = 0
        Me.cbxSpare.Tag = ""
        Me.cbxSpare.ValueMember = Nothing
        '
        'GroupControl11
        '
        Me.GroupControl11.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl11.Appearance.Options.UseFont = True
        Me.GroupControl11.Controls.Add(Me.cbxColumnSets)
        Me.GroupControl11.Location = New System.Drawing.Point(775, 9)
        Me.GroupControl11.Name = "GroupControl11"
        Me.GroupControl11.Size = New System.Drawing.Size(220, 54)
        Me.GroupControl11.TabIndex = 3
        Me.GroupControl11.Text = "Column Sets"
        '
        'cbxColumnSets
        '
        Me.cbxColumnSets.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.cbxColumnSets.Location = New System.Drawing.Point(5, 26)
        Me.cbxColumnSets.Name = "cbxColumnSets"
        Me.cbxColumnSets.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxColumnSets.Properties.SelectAllItemVisible = False
        Me.cbxColumnSets.Size = New System.Drawing.Size(210, 20)
        Me.cbxColumnSets.TabIndex = 0
        '
        'GroupControl12
        '
        Me.GroupControl12.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl12.Appearance.Options.UseFont = True
        Me.GroupControl12.Controls.Add(Me.cbxSpare)
        Me.GroupControl12.Location = New System.Drawing.Point(589, 9)
        Me.GroupControl12.Name = "GroupControl12"
        Me.GroupControl12.Size = New System.Drawing.Size(180, 54)
        Me.GroupControl12.TabIndex = 2
        Me.GroupControl12.Text = "Spare"
        '
        'cbxTags
        '
        Me.cbxTags.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxTags.Location = New System.Drawing.Point(117, 27)
        Me.cbxTags.Name = "cbxTags"
        Me.cbxTags.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxTags.Size = New System.Drawing.Size(263, 20)
        Me.cbxTags.TabIndex = 10
        '
        'frmChildFind
        '
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(1004, 701)
        Me.Controls.Add(Me.GroupControl12)
        Me.Controls.Add(Me.GroupControl11)
        Me.Controls.Add(Me.GroupControl10)
        Me.Controls.Add(Me.GroupControl9)
        Me.Controls.Add(Me.GroupControl8)
        Me.Controls.Add(Me.GroupControl7)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.GroupControl6)
        Me.Controls.Add(Me.GroupControl5)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupBox12)
        Me.FormBorderStyle = FormBorderStyle.Sizable
        Me.KeyPreview = False
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(1020, 360)
        Me.Name = "frmChildFind"
        Me.StartPosition = FormStartPosition.Manual
        Me.Text = "Find Child"
        CType(Me.GroupBox12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox12.ResumeLayout(False)
        CType(Me.cbxPreset.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCriteria.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.cbxAllergyAndIncidents.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl6.ResumeLayout(False)
        CType(Me.cbxSort.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        CType(Me.cbxAdvAllergy.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl7.ResumeLayout(False)
        CType(Me.cbxAdvConsent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl8.ResumeLayout(False)
        CType(Me.cbxPhotos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl9.ResumeLayout(False)
        Me.GroupControl9.PerformLayout()
        CType(Me.GroupControl10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl10.ResumeLayout(False)
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSpare.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl11.ResumeLayout(False)
        CType(Me.cbxColumnSets.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl12.ResumeLayout(False)
        CType(Me.cbxTags.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox12 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtCriteria As Care.Controls.CareTextBox
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents grdSearch As Care.Controls.CareGrid
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnReset As Care.Controls.CareButton
    Friend WithEvents btnClear As Care.Controls.CareButton
    Friend WithEvents btnSearch As Care.Controls.CareButton
    Friend WithEvents GroupControl6 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cbxSort As Care.Controls.CareComboBox
    Friend WithEvents Label1 As Care.Controls.CareLabel
    Friend WithEvents cbxPreset As Care.Controls.CareComboBox
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl7 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl8 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cbxPhotos As Care.Controls.CareComboBox
    Friend WithEvents cbxAllergyAndIncidents As Care.Controls.CareComboBox
    Friend WithEvents cbxAdvAllergy As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents cbxAdvConsent As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents txtGreen As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRed As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtYellow As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupControl9 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents GroupControl10 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cbxSpare As Care.Controls.CareComboBox
    Friend WithEvents GroupControl11 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl12 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cbxColumnSets As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
    Friend WithEvents cbxTags As DevExpress.XtraEditors.CheckedComboBoxEdit

End Class
