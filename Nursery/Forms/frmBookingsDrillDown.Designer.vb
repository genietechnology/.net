﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBookingsDrillDown
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.grdRooms = New Care.Controls.CareGrid()
        Me.gbxChildren = New Care.Controls.CareFrame()
        Me.radSortOldest = New Care.Controls.CareRadioButton()
        Me.radSortYoungest = New Care.Controls.CareRadioButton()
        Me.radSortSurname = New Care.Controls.CareRadioButton()
        Me.radSortForename = New Care.Controls.CareRadioButton()
        Me.grdChildren = New Care.Controls.CareGrid()
        Me.gbxDate = New Care.Controls.CareFrame()
        Me.txtSelectedDate = New Care.Controls.CareTextBox()
        Me.gbxView = New Care.Controls.CareFrame()
        Me.radSpacePcnt = New Care.Controls.CareRadioButton()
        Me.radOccupancyPcnt = New Care.Controls.CareRadioButton()
        Me.radSpace = New Care.Controls.CareRadioButton()
        Me.radHeadcount = New Care.Controls.CareRadioButton()
        Me.btnClose = New Care.Controls.CareButton()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.gbxChildren, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxChildren.SuspendLayout()
        CType(Me.radSortOldest.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radSortYoungest.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radSortSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radSortForename.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxDate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxDate.SuspendLayout()
        CType(Me.txtSelectedDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxView.SuspendLayout()
        CType(Me.radSpacePcnt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radOccupancyPcnt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radSpace.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radHeadcount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.SplitContainerControl1.Horizontal = False
        Me.SplitContainerControl1.Location = New System.Drawing.Point(12, 69)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.gbxChildren)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(960, 580)
        Me.SplitContainerControl1.SplitterPosition = 200
        Me.SplitContainerControl1.TabIndex = 2
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.grdRooms)
        Me.GroupControl1.Dock = DockStyle.Fill
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(960, 200)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Rooms"
        '
        'grdRooms
        '
        Me.grdRooms.AllowBuildColumns = True
        Me.grdRooms.AllowEdit = False
        Me.grdRooms.AllowHorizontalScroll = False
        Me.grdRooms.AllowMultiSelect = False
        Me.grdRooms.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.grdRooms.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdRooms.Appearance.Options.UseFont = True
        Me.grdRooms.AutoSizeByData = True
        Me.grdRooms.DisableAutoSize = False
        Me.grdRooms.DisableDataFormatting = False
        Me.grdRooms.FocusedRowHandle = -2147483648
        Me.grdRooms.HideFirstColumn = False
        Me.grdRooms.Location = New System.Drawing.Point(2, 22)
        Me.grdRooms.Name = "grdRooms"
        Me.grdRooms.PreviewColumn = ""
        Me.grdRooms.QueryID = Nothing
        Me.grdRooms.RowAutoHeight = False
        Me.grdRooms.SearchAsYouType = True
        Me.grdRooms.ShowAutoFilterRow = False
        Me.grdRooms.ShowFindPanel = False
        Me.grdRooms.ShowGroupByBox = False
        Me.grdRooms.ShowLoadingPanel = False
        Me.grdRooms.ShowNavigator = False
        Me.grdRooms.Size = New System.Drawing.Size(956, 175)
        Me.grdRooms.TabIndex = 4
        '
        'gbxChildren
        '
        Me.gbxChildren.Controls.Add(Me.radSortOldest)
        Me.gbxChildren.Controls.Add(Me.radSortYoungest)
        Me.gbxChildren.Controls.Add(Me.radSortSurname)
        Me.gbxChildren.Controls.Add(Me.radSortForename)
        Me.gbxChildren.Controls.Add(Me.grdChildren)
        Me.gbxChildren.Dock = DockStyle.Fill
        Me.gbxChildren.Location = New System.Drawing.Point(0, 0)
        Me.gbxChildren.Name = "gbxChildren"
        Me.gbxChildren.Size = New System.Drawing.Size(960, 375)
        Me.gbxChildren.TabIndex = 2
        Me.gbxChildren.Text = "Children"
        '
        'radSortOldest
        '
        Me.radSortOldest.Location = New System.Drawing.Point(373, 26)
        Me.radSortOldest.Name = "radSortOldest"
        Me.radSortOldest.Properties.AutoWidth = True
        Me.radSortOldest.Properties.Caption = "Oldest > Youngest"
        Me.radSortOldest.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radSortOldest.Properties.RadioGroupIndex = 2
        Me.radSortOldest.Size = New System.Drawing.Size(112, 19)
        Me.radSortOldest.TabIndex = 9
        Me.radSortOldest.TabStop = False
        '
        'radSortYoungest
        '
        Me.radSortYoungest.Location = New System.Drawing.Point(255, 26)
        Me.radSortYoungest.Name = "radSortYoungest"
        Me.radSortYoungest.Properties.AutoWidth = True
        Me.radSortYoungest.Properties.Caption = "Youngest > Oldest"
        Me.radSortYoungest.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radSortYoungest.Properties.RadioGroupIndex = 2
        Me.radSortYoungest.Size = New System.Drawing.Size(112, 19)
        Me.radSortYoungest.TabIndex = 8
        Me.radSortYoungest.TabStop = False
        '
        'radSortSurname
        '
        Me.radSortSurname.Location = New System.Drawing.Point(130, 26)
        Me.radSortSurname.Name = "radSortSurname"
        Me.radSortSurname.Properties.AutoWidth = True
        Me.radSortSurname.Properties.Caption = "Surname, Forename"
        Me.radSortSurname.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radSortSurname.Properties.RadioGroupIndex = 2
        Me.radSortSurname.Size = New System.Drawing.Size(119, 19)
        Me.radSortSurname.TabIndex = 7
        Me.radSortSurname.TabStop = False
        '
        'radSortForename
        '
        Me.radSortForename.EditValue = True
        Me.radSortForename.Location = New System.Drawing.Point(5, 26)
        Me.radSortForename.Name = "radSortForename"
        Me.radSortForename.Properties.AutoWidth = True
        Me.radSortForename.Properties.Caption = "Forename, Surname"
        Me.radSortForename.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radSortForename.Properties.RadioGroupIndex = 2
        Me.radSortForename.Size = New System.Drawing.Size(119, 19)
        Me.radSortForename.TabIndex = 6
        '
        'grdChildren
        '
        Me.grdChildren.AllowBuildColumns = True
        Me.grdChildren.AllowEdit = False
        Me.grdChildren.AllowHorizontalScroll = False
        Me.grdChildren.AllowMultiSelect = False
        Me.grdChildren.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.grdChildren.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdChildren.Appearance.Options.UseFont = True
        Me.grdChildren.AutoSizeByData = True
        Me.grdChildren.DisableAutoSize = False
        Me.grdChildren.DisableDataFormatting = False
        Me.grdChildren.FocusedRowHandle = -2147483648
        Me.grdChildren.HideFirstColumn = False
        Me.grdChildren.Location = New System.Drawing.Point(2, 51)
        Me.grdChildren.Name = "grdChildren"
        Me.grdChildren.PreviewColumn = ""
        Me.grdChildren.QueryID = Nothing
        Me.grdChildren.RowAutoHeight = False
        Me.grdChildren.SearchAsYouType = True
        Me.grdChildren.ShowAutoFilterRow = False
        Me.grdChildren.ShowFindPanel = False
        Me.grdChildren.ShowGroupByBox = False
        Me.grdChildren.ShowLoadingPanel = False
        Me.grdChildren.ShowNavigator = True
        Me.grdChildren.Size = New System.Drawing.Size(956, 324)
        Me.grdChildren.TabIndex = 5
        '
        'gbxDate
        '
        Me.gbxDate.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.gbxDate.Controls.Add(Me.txtSelectedDate)
        Me.gbxDate.Location = New System.Drawing.Point(12, 9)
        Me.gbxDate.Name = "gbxDate"
        Me.gbxDate.Size = New System.Drawing.Size(232, 54)
        Me.gbxDate.TabIndex = 0
        Me.gbxDate.Text = "Selected Date"
        '
        'txtSelectedDate
        '
        Me.txtSelectedDate.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtSelectedDate.CharacterCasing = CharacterCasing.Normal
        Me.txtSelectedDate.EnterMoveNextControl = True
        Me.txtSelectedDate.Location = New System.Drawing.Point(7, 26)
        Me.txtSelectedDate.MaxLength = 0
        Me.txtSelectedDate.Name = "txtSelectedDate"
        Me.txtSelectedDate.NumericAllowNegatives = False
        Me.txtSelectedDate.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSelectedDate.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSelectedDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSelectedDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSelectedDate.Properties.Appearance.Options.UseFont = True
        Me.txtSelectedDate.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSelectedDate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSelectedDate.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSelectedDate.Properties.ReadOnly = True
        Me.txtSelectedDate.Size = New System.Drawing.Size(218, 20)
        Me.txtSelectedDate.TabIndex = 1
        Me.txtSelectedDate.TextAlign = HorizontalAlignment.Left
        Me.txtSelectedDate.ToolTipText = ""
        '
        'gbxView
        '
        Me.gbxView.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.gbxView.Controls.Add(Me.radSpacePcnt)
        Me.gbxView.Controls.Add(Me.radOccupancyPcnt)
        Me.gbxView.Controls.Add(Me.radSpace)
        Me.gbxView.Controls.Add(Me.radHeadcount)
        Me.gbxView.Location = New System.Drawing.Point(250, 9)
        Me.gbxView.Name = "gbxView"
        Me.gbxView.Size = New System.Drawing.Size(722, 54)
        Me.gbxView.TabIndex = 1
        Me.gbxView.Text = "View"
        '
        'radSpacePcnt
        '
        Me.radSpacePcnt.Location = New System.Drawing.Point(316, 27)
        Me.radSpacePcnt.Name = "radSpacePcnt"
        Me.radSpacePcnt.Properties.AutoWidth = True
        Me.radSpacePcnt.Properties.Caption = "Space %"
        Me.radSpacePcnt.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radSpacePcnt.Properties.RadioGroupIndex = 0
        Me.radSpacePcnt.Size = New System.Drawing.Size(65, 19)
        Me.radSpacePcnt.TabIndex = 3
        Me.radSpacePcnt.TabStop = False
        '
        'radOccupancyPcnt
        '
        Me.radOccupancyPcnt.Location = New System.Drawing.Point(198, 27)
        Me.radOccupancyPcnt.Name = "radOccupancyPcnt"
        Me.radOccupancyPcnt.Properties.AutoWidth = True
        Me.radOccupancyPcnt.Properties.Caption = "Occupancy %"
        Me.radOccupancyPcnt.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radOccupancyPcnt.Properties.RadioGroupIndex = 0
        Me.radOccupancyPcnt.Size = New System.Drawing.Size(89, 19)
        Me.radOccupancyPcnt.TabIndex = 2
        Me.radOccupancyPcnt.TabStop = False
        '
        'radSpace
        '
        Me.radSpace.Location = New System.Drawing.Point(107, 27)
        Me.radSpace.Name = "radSpace"
        Me.radSpace.Properties.AutoWidth = True
        Me.radSpace.Properties.Caption = "Spaces"
        Me.radSpace.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radSpace.Properties.RadioGroupIndex = 0
        Me.radSpace.Size = New System.Drawing.Size(56, 19)
        Me.radSpace.TabIndex = 1
        Me.radSpace.TabStop = False
        '
        'radHeadcount
        '
        Me.radHeadcount.EditValue = True
        Me.radHeadcount.Location = New System.Drawing.Point(11, 27)
        Me.radHeadcount.Name = "radHeadcount"
        Me.radHeadcount.Properties.AutoWidth = True
        Me.radHeadcount.Properties.Caption = "Headcount"
        Me.radHeadcount.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radHeadcount.Properties.RadioGroupIndex = 0
        Me.radHeadcount.Size = New System.Drawing.Size(74, 19)
        Me.radHeadcount.TabIndex = 0
        '
        'btnClose
        '
        Me.btnClose.DialogResult = DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(897, 626)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 10
        Me.btnClose.Text = "Close"
        '
        'frmBookingsDrillDown
        '
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(984, 661)
        Me.Controls.Add(Me.gbxView)
        Me.Controls.Add(Me.gbxDate)
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Controls.Add(Me.btnClose)
        Me.FormBorderStyle = FormBorderStyle.Sizable
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(1000, 700)
        Me.Name = "frmBookingsDrillDown"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = ""
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.gbxChildren, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxChildren.ResumeLayout(False)
        CType(Me.radSortOldest.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radSortYoungest.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radSortSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radSortForename.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxDate, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxDate.ResumeLayout(False)
        CType(Me.txtSelectedDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxView.ResumeLayout(False)
        CType(Me.radSpacePcnt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radOccupancyPcnt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radSpace.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radHeadcount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents grdRooms As Care.Controls.CareGrid
    Friend WithEvents gbxChildren As DevExpress.XtraEditors.GroupControl
    Friend WithEvents grdChildren As Care.Controls.CareGrid
    Friend WithEvents gbxDate As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtSelectedDate As Care.Controls.CareTextBox
    Friend WithEvents gbxView As DevExpress.XtraEditors.GroupControl
    Friend WithEvents radOccupancyPcnt As Care.Controls.CareRadioButton
    Friend WithEvents radSpace As Care.Controls.CareRadioButton
    Friend WithEvents radHeadcount As Care.Controls.CareRadioButton
    Friend WithEvents radSortOldest As Care.Controls.CareRadioButton
    Friend WithEvents radSortYoungest As Care.Controls.CareRadioButton
    Friend WithEvents radSortSurname As Care.Controls.CareRadioButton
    Friend WithEvents radSortForename As Care.Controls.CareRadioButton
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents radSpacePcnt As Care.Controls.CareRadioButton

End Class
