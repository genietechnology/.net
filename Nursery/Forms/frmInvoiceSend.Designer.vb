﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInvoiceSend
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling2 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInvoiceSend))
        Me.btnClose = New Care.Controls.CareButton()
        Me.GroupBox1 = New Care.Controls.CareFrame()
        Me.txtNarrative = New DevExpress.XtraEditors.MemoEdit()
        Me.btnPrint = New Care.Controls.CareButton()
        Me.btnPrintAll = New Care.Controls.CareButton()
        Me.btnEmail = New Care.Controls.CareButton()
        Me.GroupControl2 = New Care.Controls.CareFrame()
        Me.txtBody = New DevExpress.XtraEditors.MemoEdit()
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.txtSubject = New Care.Controls.CareTextBox()
        Me.GroupControl3 = New Care.Controls.CareFrame()
        Me.CareLabel1 = New Care.Controls.CareLabel()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtNarrative.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.txtBody.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtSubject.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(863, 627)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(110, 25)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "Close"
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((AnchorStyles.Bottom Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.txtNarrative)
        Me.GroupBox1.Location = New System.Drawing.Point(5, 443)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(472, 165)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.Text = "Narrative (Field available in the Report Designer - displayed on all Invoices)"
        '
        'txtNarrative
        '
        Me.txtNarrative.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtNarrative.Location = New System.Drawing.Point(5, 24)
        Me.txtNarrative.Name = "txtNarrative"
        Me.txtNarrative.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNarrative.Properties.Appearance.Options.UseFont = True
        Me.txtNarrative.Properties.Appearance.Options.UseTextOptions = True
        Me.txtNarrative.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtNarrative, True)
        Me.txtNarrative.Size = New System.Drawing.Size(462, 136)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtNarrative, OptionsSpelling1)
        Me.txtNarrative.TabIndex = 3
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnPrint.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnPrint.Appearance.Options.UseFont = True
        Me.btnPrint.Location = New System.Drawing.Point(128, 627)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(110, 25)
        Me.btnPrint.TabIndex = 3
        Me.btnPrint.Text = "Print"
        Me.btnPrint.ToolTip = "Prints invoices excluding any Families that have opted for Email Invoicing." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Invo" &
    "ices will be marked as printed." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnPrint.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.btnPrint.ToolTipTitle = "Print Invoices"
        '
        'btnPrintAll
        '
        Me.btnPrintAll.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnPrintAll.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnPrintAll.Appearance.Options.UseFont = True
        Me.btnPrintAll.Location = New System.Drawing.Point(244, 627)
        Me.btnPrintAll.Name = "btnPrintAll"
        Me.btnPrintAll.Size = New System.Drawing.Size(110, 25)
        Me.btnPrintAll.TabIndex = 4
        Me.btnPrintAll.Text = "Print All"
        Me.btnPrintAll.ToolTip = "Prints all invoices regardless of the Email Invoices option on the Family Record." &
    "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Invoices will NOT be marked as printed."
        Me.btnPrintAll.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.btnPrintAll.ToolTipTitle = "Print All Invoices"
        '
        'btnEmail
        '
        Me.btnEmail.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnEmail.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnEmail.Appearance.Options.UseFont = True
        Me.btnEmail.Location = New System.Drawing.Point(12, 627)
        Me.btnEmail.Name = "btnEmail"
        Me.btnEmail.Size = New System.Drawing.Size(110, 25)
        Me.btnEmail.TabIndex = 5
        Me.btnEmail.Text = "Send"
        Me.btnEmail.ToolTip = "Sends invoices via Email for all Families with ""Email Invoices"" ticked." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Invoices" &
    " will be marked as Emailed."
        Me.btnEmail.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.btnEmail.ToolTipTitle = "Email Invoices"
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl2.Controls.Add(Me.txtBody)
        Me.GroupControl2.Location = New System.Drawing.Point(5, 55)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(472, 382)
        Me.GroupControl2.TabIndex = 7
        Me.GroupControl2.Text = "Email Body (Use Placeholders)"
        '
        'txtBody
        '
        Me.txtBody.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtBody.Location = New System.Drawing.Point(5, 23)
        Me.txtBody.Name = "txtBody"
        Me.txtBody.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBody.Properties.Appearance.Options.UseFont = True
        Me.txtBody.Properties.Appearance.Options.UseTextOptions = True
        Me.txtBody.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtBody, True)
        Me.txtBody.Size = New System.Drawing.Size(462, 353)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtBody, OptionsSpelling2)
        Me.txtBody.TabIndex = 3
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.SplitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2
        Me.SplitContainerControl1.IsSplitterFixed = True
        Me.SplitContainerControl1.Location = New System.Drawing.Point(12, 12)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl2)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupBox1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.GroupControl3)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(960, 608)
        Me.SplitContainerControl1.SplitterPosition = 478
        Me.SplitContainerControl1.TabIndex = 8
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.txtSubject)
        Me.GroupControl1.Location = New System.Drawing.Point(5, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(472, 49)
        Me.GroupControl1.TabIndex = 7
        Me.GroupControl1.Text = "Email Subject (Use Placeholders)"
        '
        'txtSubject
        '
        Me.txtSubject.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtSubject.CharacterCasing = CharacterCasing.Normal
        Me.txtSubject.EnterMoveNextControl = True
        Me.txtSubject.Location = New System.Drawing.Point(5, 23)
        Me.txtSubject.MaxLength = 0
        Me.txtSubject.Name = "txtSubject"
        Me.txtSubject.NumericAllowNegatives = False
        Me.txtSubject.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSubject.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSubject.Properties.AccessibleName = "Contact Forename"
        Me.txtSubject.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSubject.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSubject.Properties.Appearance.Options.UseFont = True
        Me.txtSubject.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSubject.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSubject.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSubject.Size = New System.Drawing.Size(462, 20)
        Me.txtSubject.TabIndex = 1
        Me.txtSubject.Tag = ""
        Me.txtSubject.TextAlign = HorizontalAlignment.Left
        Me.txtSubject.ToolTipText = ""
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.CareLabel1)
        Me.GroupControl3.Dock = DockStyle.Fill
        Me.GroupControl3.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(477, 608)
        Me.GroupControl3.TabIndex = 8
        Me.GroupControl3.Text = "Available Placeholders"
        '
        'CareLabel1
        '
        Me.CareLabel1.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.CareLabel1.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.CareLabel1.Location = New System.Drawing.Point(5, 26)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(467, 577)
        Me.CareLabel1.TabIndex = 7
        Me.CareLabel1.Text = resources.GetString("CareLabel1.Text")
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmInvoiceSend
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(984, 661)
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Controls.Add(Me.btnEmail)
        Me.Controls.Add(Me.btnPrintAll)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.btnClose)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmInvoiceSend"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "Print & Email Invoices"
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.txtNarrative.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.txtBody.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtSubject.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents txtNarrative As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents btnPrint As Care.Controls.CareButton
    Friend WithEvents btnPrintAll As Care.Controls.CareButton
    Friend WithEvents btnEmail As Care.Controls.CareButton
    Friend WithEvents txtBody As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents txtSubject As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents GroupBox1 As Care.Controls.CareFrame
    Friend WithEvents GroupControl2 As Care.Controls.CareFrame
    Friend WithEvents GroupControl1 As Care.Controls.CareFrame
    Friend WithEvents GroupControl3 As Care.Controls.CareFrame
End Class
