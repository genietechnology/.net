﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmAvailability
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl1 = New Care.Controls.CareFrame(Me.components)
        Me.btnRefresh = New Care.Controls.CareButton(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.cbxType = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.cbxSite = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.cbxRoom = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.cdtFrom = New Care.Controls.CareDateTime(Me.components)
        Me.chkExclWL = New Care.Controls.CareCheckBox(Me.components)
        Me.YearView1 = New Nursery.YearView()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.cbxType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxRoom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkExclWL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.chkExclWL)
        Me.GroupControl1.Controls.Add(Me.btnRefresh)
        Me.GroupControl1.Controls.Add(Me.CareLabel4)
        Me.GroupControl1.Controls.Add(Me.cbxType)
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Controls.Add(Me.cbxSite)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.cbxRoom)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Controls.Add(Me.cdtFrom)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 10)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(768, 66)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Availability Options"
        '
        'btnRefresh
        '
        Me.btnRefresh.Location = New System.Drawing.Point(689, 11)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(60, 23)
        Me.btnRefresh.TabIndex = 10
        Me.btnRefresh.Text = "Refresh"
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(10, 15)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(26, 15)
        Me.CareLabel4.TabIndex = 0
        Me.CareLabel4.Text = "Type"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxType
        '
        Me.cbxType.AllowBlank = False
        Me.cbxType.DataSource = Nothing
        Me.cbxType.DisplayMember = Nothing
        Me.cbxType.EnterMoveNextControl = True
        Me.cbxType.Location = New System.Drawing.Point(42, 12)
        Me.cbxType.Name = "cbxType"
        Me.cbxType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxType.Properties.Appearance.Options.UseFont = True
        Me.cbxType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxType.SelectedValue = Nothing
        Me.cbxType.Size = New System.Drawing.Size(90, 22)
        Me.cbxType.TabIndex = 1
        Me.cbxType.ValueMember = Nothing
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(281, 15)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(19, 15)
        Me.CareLabel3.TabIndex = 4
        Me.CareLabel3.Text = "Site"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(306, 12)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(165, 22)
        Me.cbxSite.TabIndex = 5
        Me.cbxSite.ValueMember = Nothing
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(477, 15)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel2.TabIndex = 6
        Me.CareLabel2.Text = "Room"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxRoom
        '
        Me.cbxRoom.AllowBlank = False
        Me.cbxRoom.DataSource = Nothing
        Me.cbxRoom.DisplayMember = Nothing
        Me.cbxRoom.EnterMoveNextControl = True
        Me.cbxRoom.Location = New System.Drawing.Point(515, 12)
        Me.cbxRoom.Name = "cbxRoom"
        Me.cbxRoom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxRoom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxRoom.Properties.Appearance.Options.UseFont = True
        Me.cbxRoom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxRoom.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxRoom.SelectedValue = Nothing
        Me.cbxRoom.Size = New System.Drawing.Size(165, 22)
        Me.cbxRoom.TabIndex = 7
        Me.cbxRoom.ValueMember = Nothing
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(147, 15)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(28, 15)
        Me.CareLabel1.TabIndex = 2
        Me.CareLabel1.Text = "From"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtFrom
        '
        Me.cdtFrom.EditValue = Nothing
        Me.cdtFrom.EnterMoveNextControl = True
        Me.cdtFrom.Location = New System.Drawing.Point(181, 12)
        Me.cdtFrom.Name = "cdtFrom"
        Me.cdtFrom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtFrom.Properties.Appearance.Options.UseFont = True
        Me.cdtFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtFrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtFrom.Size = New System.Drawing.Size(85, 22)
        Me.cdtFrom.TabIndex = 3
        Me.cdtFrom.Value = Nothing
        '
        'chkExclWL
        '
        Me.chkExclWL.EditValue = True
        Me.chkExclWL.EnterMoveNextControl = True
        Me.chkExclWL.Location = New System.Drawing.Point(10, 40)
        Me.chkExclWL.Name = "chkExclWL"
        Me.chkExclWL.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkExclWL.Properties.Appearance.Options.UseFont = True
        Me.chkExclWL.Properties.Caption = "Exclude Waiting List Children"
        Me.chkExclWL.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.chkExclWL.Size = New System.Drawing.Size(182, 19)
        Me.chkExclWL.TabIndex = 9
        Me.chkExclWL.Tag = ""
        '
        'YearView1
        '
        Me.YearView1.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.YearView1.CalendarFont = New System.Drawing.Font("Tahoma", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.YearView1.Location = New System.Drawing.Point(12, 82)
        Me.YearView1.Name = "YearView1"
        Me.YearView1.Size = New System.Drawing.Size(768, 437)
        Me.YearView1.TabIndex = 1
        '
        'frmAvailability
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(792, 531)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.YearView1)
        Me.FormBorderStyle = FormBorderStyle.Sizable
        Me.LoadMaximised = True
        Me.MinimumSize = New System.Drawing.Size(808, 570)
        Me.Name = "frmAvailability"
        Me.WindowState = FormWindowState.Maximized
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.cbxType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxRoom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkExclWL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents YearView1 As Nursery.YearView
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents cdtFrom As Care.Controls.CareDateTime
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents cbxRoom As Care.Controls.CareComboBox
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents cbxType As Care.Controls.CareComboBox
    Friend WithEvents btnRefresh As Care.Controls.CareButton
    Friend WithEvents GroupControl1 As Care.Controls.CareFrame
    Friend WithEvents chkExclWL As Care.Controls.CareCheckBox
End Class
