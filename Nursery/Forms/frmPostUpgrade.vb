﻿Imports Care.Global
Imports Care.Data
Imports Care.Shared
Imports System.Windows.Forms

Public Class frmPostUpgrade

    Private m_LastVersion As Version = Nothing

    Public Sub New(ByVal LastVersion As Version)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_LastVersion = LastVersion

    End Sub

    Private Sub frmPostUpgrade_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Application.DoEvents()
        Run()
        Me.Close()
    End Sub

    Private Sub Run()

        Session.SetProgressMessage("Checking Parameters...")
        CreateParameters()

        Session.SetProgressMessage("Checking Lists...")
        CreateLists()

        Session.SetProgressMessage("Checking Age Points...")
        CreateAgePoints()

        Session.SetProgressMessage("Generating Thumbnails...")
        GenerateThumbnails

        If VersionLowerThan1610() Then

            Session.SetProgressMessage("Checking Site Rooms and Ratios...")
            CreateSiteRooms()

            'Session.SetProgressMessage("Generating Invoice Days...")
            'GenerateInvoiceDays()

        End If

        If Version1610() Then
            Session.SetProgressMessage("Rebuilding Tariffs")
            RebuildTariffs()
        End If

    End Sub

    Private Function VersionLowerThan1610() As Boolean

        If m_LastVersion Is Nothing Then Return True

        If m_LastVersion.Minor = 16 Then
            If m_LastVersion.Build < 10 Then
                Return True
            End If
        End If

        Return False

    End Function

    Private Function Version1610() As Boolean

        If m_LastVersion Is Nothing Then Return True

        If m_LastVersion.Minor = 16 Then
            If m_LastVersion.Build = 10 Then
                Return True
            End If
        End If

        Return False

    End Function

    Private Sub RebuildTariffs()

        Dim _CurrentTariffs As List(Of Business.Tariff) = Business.Tariff.RetreiveAll

        Dim _Sites As List(Of Business.Site) = Business.Site.RetreiveAll
        For Each _S As Business.Site In _Sites

            For Each _CT As Business.Tariff In _CurrentTariffs

                Dim _T As New Business.Tariff

                _T._Type = _CT._Type

                If _T._Type = "Funded" Then
                    _T._SummaryColumn = "0"
                Else
                    _T._SummaryColumn = "1"
                End If

                _T._Name = _CT._Name
                _T._InvoiceText = _CT._InvoiceText
                _T._Duration = _CT._Duration
                _T._Minutes = _CT._Minutes
                _T._Rate = _CT._Rate
                _T._AvgDays = _CT._AvgDays
                _T._AvgWeeks = _CT._AvgWeeks
                _T._Avg = _CT._Avg
                _T._Override = _CT._Override
                _T._Daily = _CT._Daily
                _T._Weekly = _CT._Weekly
                _T._Monthly = _CT._Monthly
                _T._Colour = _CT._Colour
                _T._Am = _CT._Am
                _T._Pm = _CT._Pm
                _T._StartTime = _CT._StartTime
                _T._EndTime = _CT._EndTime
                _T._NlCode = _CT._NlCode
                _T._FundingHours = _CT._FundingHours
                _T._Holiday = _CT._Holiday
                _T._HolidayTariff = _CT._HolidayTariff
                _T._SessHol = _CT._SessHol
                _T._SessHolDiscount = _CT._SessHolDiscount
                _T._RateFund24 = _CT._RateFund24
                _T._RateFund36 = _CT._RateFund36
                _T._VoidStart = _CT._VoidStart
                _T._VoidEnd = _CT._VoidEnd
                _T._VariableRate = _CT._VariableRate
                _T._NlTracking = _CT._NlTracking
                _T._ValidateTimesDur = _CT._ValidateTimesDur
                _T._BoltOns = _CT._BoltOns
                _T._Discount = _CT._Discount

                _T._SiteId = _S._ID

                'store the old tariff id on this new tariff so we can find it easy
                _T._SiteName = _CT._ID.ToString

                _T._Archived = False

                'create entries in other tables associated with this tariff
                UpdateTariffSubTables(_CT._ID.Value, _T._ID.Value)

                _T.Store()

            Next

        Next

        Session.SetProgressMessage("Archiving Old Tariffs")

        'now archive the current tariffs...
        For Each _T In _CurrentTariffs
            _T._Archived = True
            _T.Store()
        Next

        'fetch all the tariffs again...
        _CurrentTariffs = Business.Tariff.RetreiveAll

        'loop through all children
        Dim _Children As List(Of Business.Child) = Business.Child.RetreiveAll
        If _Children.Count > 0 Then

            Session.SetupProgressBar("Changing Child Tariffs...", _Children.Count)

            For Each _C In _Children
                ReplaceTariffs(_C._ID.Value, _C._SiteId.Value, _CurrentTariffs)
                Session.StepProgressBar()
            Next

        End If

        'reset the tariff description back
        Dim _SQL As String = ""
        _SQL = "update Tariffs set Tariffs.site_name = (select name from Sites where Sites.ID = Tariffs.site_id)"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

    End Sub

    Private Sub UpdateTariffSubTables(ByVal OldTariffID As Guid, ByVal NewTariffID As Guid)

        Dim _SQL As String = ""
        Dim _DT As DataTable = Nothing

        '******************************************************************************************

        _SQL = "select * from TariffMatrix where tariff_id = '" + OldTariffID.ToString + "'"
        _DT = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then
            For Each _DR As DataRow In _DT.Rows
                Dim _TM As New Business.TariffMatrix
                With _TM
                    ._TariffId = NewTariffID
                    ._AgeId = New Guid(_DR.Item("age_id").ToString)
                    ._Class = _DR.Item("class").ToString
                    ._InvoiceText = _DR.Item("invoice_text").ToString
                    ._Value = ValueHandler.ConvertDecimal(_DR.Item("value"))
                    .Store()
                End With
            Next
        End If

        '******************************************************************************************

        _SQL = "select * from TariffDays where tariff_id = '" + OldTariffID.ToString + "'"
        _DT = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then
            For Each _DR As DataRow In _DT.Rows
                Dim _TD As New Business.TariffDay
                With _TD
                    ._TariffId = NewTariffID
                    ._AgeId = New Guid(_DR.Item("age_id").ToString)
                    ._InvoiceText = _DR.Item("invoice_text").ToString
                    ._Days1 = ValueHandler.ConvertDecimal(_DR.Item("days_1"))
                    ._Days2 = ValueHandler.ConvertDecimal(_DR.Item("days_2"))
                    ._Days3 = ValueHandler.ConvertDecimal(_DR.Item("days_3"))
                    ._Days4 = ValueHandler.ConvertDecimal(_DR.Item("days_4"))
                    ._Days5 = ValueHandler.ConvertDecimal(_DR.Item("days_5"))
                    ._Days6 = ValueHandler.ConvertDecimal(_DR.Item("days_6"))
                    ._Days7 = ValueHandler.ConvertDecimal(_DR.Item("days_7"))
                    .Store()
                End With
            Next
        End If

        '******************************************************************************************

        _SQL = "select * from TariffVariable where tariff_id = '" + OldTariffID.ToString + "'"
        _DT = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then
            For Each _DR As DataRow In _DT.Rows
                Dim _TM As New Business.TariffVariable
                With _TM
                    ._TariffId = NewTariffID
                    ._StartTime = Business.TariffVariable.GetTimeSpan(_DR.Item("start_time"))
                    ._EndTime = Business.TariffVariable.GetTimeSpan(_DR.Item("end_time"))
                    ._Units = ValueHandler.ConvertInteger(_DR.Item("units"))
                    ._Rate = ValueHandler.ConvertDecimal(_DR.Item("rate"))
                    .Store()
                End With
            Next
        End If

        '******************************************************************************************

    End Sub

    Private Sub ReplaceTariffs(ByVal ChildID As Guid, ByVal SiteID As Guid, ByRef CurrentTariffs As List(Of Business.Tariff))

        For Each _S In Business.ChildAdvanceSession.RetrieveByChildID(ChildID)
            _S._Monday = GetTariff(SiteID, _S._Monday, CurrentTariffs)
            _S._Tuesday = GetTariff(SiteID, _S._Tuesday, CurrentTariffs)
            _S._Wednesday = GetTariff(SiteID, _S._Wednesday, CurrentTariffs)
            _S._Thursday = GetTariff(SiteID, _S._Thursday, CurrentTariffs)
            _S._Friday = GetTariff(SiteID, _S._Friday, CurrentTariffs)
            _S._Saturday = GetTariff(SiteID, _S._Saturday, CurrentTariffs)
            _S._Sunday = GetTariff(SiteID, _S._Sunday, CurrentTariffs)
            _S.Store()
        Next

    End Sub

    Private Function GetTariff(ByVal SiteID As Guid?, ByVal OldTariff As Guid?, ByRef CurrentTariffs As List(Of Business.Tariff)) As Guid?

        If OldTariff.HasValue Then

            'fetch the "new" tariff based upon the old tariff and the site

            Dim _Q As IEnumerable(Of Business.Tariff) = Nothing

            _Q = From _T As Business.Tariff In CurrentTariffs
                 Where _T._SiteId = SiteID _
                 And _T._SiteName = OldTariff.ToString

            If _Q IsNot Nothing Then
                If _Q.Count = 1 Then
                    Return _Q.First._ID
                End If
            End If

        End If

        Return OldTariff

    End Function

    Private Sub CreateAgePoints()

        Dim _SQL As String = "select count(*) as 'Count' from AgePoints"

        Dim _Count As Integer = ValueHandler.ConvertInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))
        If _Count = 0 Then
            CreatePointRecord(0, 23, 8)
            CreatePointRecord(24, 35, 6)
            CreatePointRecord(36, 95, 3)
            CreatePointRecord(96, 999, 2)
        End If

    End Sub

    Private Sub CreatePointRecord(ByVal MinAge As Integer, ByVal MaxAge As Integer, ByVal Points As Integer)
        Dim _P As New Business.AgePoint
        _P._AgeMin = MinAge
        _P._AgeMax = MaxAge
        _P._Points = Points
        _P.Store()
    End Sub

    Private Sub CreateSiteRooms()

        Dim _SiteRooms As List(Of Business.SiteRoom) = Business.SiteRoom.RetreiveAll
        If _SiteRooms.Count = 0 Then

            For Each _Site As Business.Site In Business.Site.RetreiveAll

                Dim _Sequence As Integer = 0
                For Each _OldRoom As Business.Room In Business.Room.RetreiveAllOrderByAge

                    _Sequence += 10

                    Dim _NewRoomID As Guid = Guid.NewGuid

                    Dim _Room As New Business.SiteRoom
                    With _Room
                        ._ID = _NewRoomID
                        ._SiteId = _Site._ID.Value
                        ._RoomName = _OldRoom._Name
                        ._RoomHoursFrom = New TimeSpan(0, 0, 0)
                        ._RoomHoursTo = New TimeSpan(23, 59, 0)
                        ._RoomCheckChildren = True
                        ._RoomCheckStaff = True
                        ._RoomSequence = _Sequence
                        .Store()
                    End With

                    Dim _Ratio As New Business.SiteRoomRatio
                    With _Ratio
                        ._RoomId = _NewRoomID
                        ._AgeFrom = _OldRoom._MinAge
                        ._AgeTo = _OldRoom._MaxAge
                        ._Capacity = _OldRoom._Capacity
                        ._Ratio = _OldRoom._StaffRatio
                        .Store()
                    End With

                    Dim _SQL As String = ""

                    'change the room on all children
                    _SQL = "update Children set group_id = '" + _NewRoomID.ToString + "'"
                    _SQL += " where site_id = '" + _Site._ID.ToString + "'"
                    _SQL += " and group_name = '" + _OldRoom._Name.ToString + "'"

                    DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                    'change the room on all staff
                    _SQL = "update Staff set group_id = '" + _NewRoomID.ToString + "'"
                    _SQL += " where site_id = '" + _Site._ID.ToString + "'"
                    _SQL += " and group_name = '" + _OldRoom._Name.ToString + "'"

                    DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                    'TODO add the ratio ID on all ChildMove records

                Next

            Next

        End If

    End Sub

    Private Sub CreateParameters()

        If Not ParameterHandler.CheckExists("LASTVERSION") Then ParameterHandler.Create("LASTVERSION", ParameterHandler.EnumParameterType.StringType, "System", "Last Version", "")

        'default values
        '****************************************************************************************************************************************************************************************************************************
        If Not ParameterHandler.CheckExists("DEFSITE") Then ParameterHandler.Create("DEFSITE", ParameterHandler.EnumParameterType.StringType, "Default Values", "Default Site Name", "Primary Site")

        If Not ParameterHandler.CheckExists("DEFCHILDCLASS") Then ParameterHandler.Create("DEFCHILDCLASS", ParameterHandler.EnumParameterType.StringType, "Default Values", "Default Classification", "")
        If Not ParameterHandler.CheckExists("DEFCHILDTERM") Then ParameterHandler.Create("DEFCHILDTERM", ParameterHandler.EnumParameterType.StringType, "Default Values", "Default Term", "School Term")
        If Not ParameterHandler.CheckExists("DEFMOVEMODE") Then ParameterHandler.Create("DEFMOVEMODE", ParameterHandler.EnumParameterType.StringType, "Default Values", "Default Move Mode", "Disabled")

        If Not ParameterHandler.CheckExists("DEFINVFREQ") Then ParameterHandler.Create("DEFINVFREQ", ParameterHandler.EnumParameterType.StringType, "Default Values", "Invoice Frequency", "Monthly")
        If Not ParameterHandler.CheckExists("DEFINVDUE") Then ParameterHandler.Create("DEFINVDUE", ParameterHandler.EnumParameterType.StringType, "Default Values", "Invoice Due Date", "x days from invoice date")

        If Not ParameterHandler.CheckExists("DEFXCHECKTHRESH") Then ParameterHandler.Create("DEFXCHECKTHRESH", ParameterHandler.EnumParameterType.DecimalType, "Default Values", "XCheck Threshold", "15")
        If Not ParameterHandler.CheckExists("DEFXCHECKPERHOUR") Then ParameterHandler.Create("DEFXCHECKPERHOUR", ParameterHandler.EnumParameterType.DecimalType, "Default Values", "XCheck Per Hour", "5.00")

        If Not ParameterHandler.CheckExists("NLCODE") Then ParameterHandler.Create("NLCODE", ParameterHandler.EnumParameterType.DecimalType, "Default Values", "Default NL Code", "200")
        If Not ParameterHandler.CheckExists("NLTRACKING") Then ParameterHandler.Create("NLTRACKING", ParameterHandler.EnumParameterType.DecimalType, "Default Values", "Default NL Tracking", "")
        If Not ParameterHandler.CheckExists("NLTRACKINGCAT") Then ParameterHandler.Create("NLTRACKINGCAT", ParameterHandler.EnumParameterType.DecimalType, "Default Values", "NL Tracking Category", "")

        If Not ParameterHandler.CheckExists("ACTIVITYTEXT") Then ParameterHandler.Create("ACTIVITYTEXT", ParameterHandler.EnumParameterType.StringType, "Default Values", "Activity Text", "Today in the <GroupName> we...")

        '****************************************************************************************************************************************************************************************************************************

        If Not ParameterHandler.CheckExists("FINSYNCNAME") Then ParameterHandler.Create("FINSYNCNAME", ParameterHandler.EnumParameterType.StringType, "Financials", "Name Sync Mode", "Use Addressee from Family Record")
        If Not ParameterHandler.CheckExists("FINNEXTACC") Then ParameterHandler.Create("FINNEXTACC", ParameterHandler.EnumParameterType.IntegerType, "Financials", "Next Account No", "1")
        If Not ParameterHandler.CheckExists("FINACCMASK") Then ParameterHandler.Create("FINACCMASK", ParameterHandler.EnumParameterType.StringType, "Financials", "Account Number Mask", "00000")
        If Not ParameterHandler.CheckExists("FINPRINTMGRPASS") Then ParameterHandler.Create("FINPRINTMGRPASS", ParameterHandler.EnumParameterType.BooleanType, "Financials", "Require Manager Pwd to Print", "False")

        If Not ParameterHandler.CheckExists("ARCHIVEDAYS") Then ParameterHandler.Create("ARCHIVEDAYS", ParameterHandler.EnumParameterType.IntegerType, "Options", "Days until Archive", "60")
        If Not ParameterHandler.CheckExists("MANAGERPASSWORD") Then ParameterHandler.Create("MANAGERPASSWORD", ParameterHandler.EnumParameterType.StringType, "Options", "Manager Password", "password")
        If Not ParameterHandler.CheckExists("SCHOOLMONTHS") Then ParameterHandler.Create("SCHOOLMONTHS", ParameterHandler.EnumParameterType.IntegerType, "Options", "School Start (months)", "48")
        If Not ParameterHandler.CheckExists("POINTSSTAFF") Then ParameterHandler.Create("POINTSSTAFF", ParameterHandler.EnumParameterType.IntegerType, "Options", "Points for Staff", "24")
        If Not ParameterHandler.CheckExists("MOVEAUTO") Then ParameterHandler.Create("MOVEAUTO", ParameterHandler.EnumParameterType.BooleanType, "Options", "Automatically Move Children", "False")
        If Not ParameterHandler.CheckExists("REGROOMCHILD") Then ParameterHandler.Create("REGROOMCHILD", ParameterHandler.EnumParameterType.BooleanType, "Options", "Use Child Room for Registers", "True")
        If Not ParameterHandler.CheckExists("SKIPWL") Then ParameterHandler.Create("SKIPWL", ParameterHandler.EnumParameterType.BooleanType, "Options", "Skip Waiting List", "False")

        If Not ParameterHandler.CheckExists("HIDESICKNESS") Then ParameterHandler.Create("HIDESICKNESS", ParameterHandler.EnumParameterType.BooleanType, "Nursery Calendar", "Hide Sickness Reasons", "False")

        If Not ParameterHandler.CheckExists("TEMPANNUALCALC") Then ParameterHandler.Create("TEMPANNUALCALC", ParameterHandler.EnumParameterType.StringType, "Templates", "Annual Calculation Text", "")
        If Not ParameterHandler.CheckExists("TEMPANNUALFUNDCALC") Then ParameterHandler.Create("TEMPANNUALFUNDCALC", ParameterHandler.EnumParameterType.StringType, "Templates", "Annual Funding Calc Text", "")
        If Not ParameterHandler.CheckExists("TEMPANNUALHEADER") Then ParameterHandler.Create("TEMPANNUALHEADER", ParameterHandler.EnumParameterType.StringType, "Templates", "Annual Calculation Header", "")
        If Not ParameterHandler.CheckExists("TEMPANNUALFOOTER") Then ParameterHandler.Create("TEMPANNUALFOOTER", ParameterHandler.EnumParameterType.StringType, "Templates", "Annual Calculation Footer", "")
        If Not ParameterHandler.CheckExists("TEMPANNUALSUBTOTAL") Then ParameterHandler.Create("TEMPANNUALSUBTOTAL", ParameterHandler.EnumParameterType.StringType, "Templates", "Annual Calculation Sub Total", "")
        If Not ParameterHandler.CheckExists("TEMPANNUALFUNDING") Then ParameterHandler.Create("TEMPANNUALFUNDING", ParameterHandler.EnumParameterType.StringType, "Templates", "Annual Calculation Funding", "")

        If Not ParameterHandler.CheckExists("WUFOOKEY") Then ParameterHandler.Create("WUFOOKEY", ParameterHandler.EnumParameterType.StringType, "WuFoo Integration", "API Key", "")
        If Not ParameterHandler.CheckExists("WUFOOPASSWORD") Then ParameterHandler.Create("WUFOOPASSWORD", ParameterHandler.EnumParameterType.StringType, "WuFoo Integration", "Password", "")
        If Not ParameterHandler.CheckExists("WUFOODOMAIN") Then ParameterHandler.Create("WUFOODOMAIN", ParameterHandler.EnumParameterType.StringType, "WuFoo Integration", "Sub Domain", "")

        If Not ParameterHandler.CheckExists("ROLLINGMENU") Then ParameterHandler.Create("ROLLINGMENU", ParameterHandler.EnumParameterType.BooleanType, "Rolling Menus", "Use Rolling Menus", "False")
        If Not ParameterHandler.CheckExists("ROLLINGMENUWEEK") Then ParameterHandler.Create("ROLLINGMENUWEEK", ParameterHandler.EnumParameterType.IntegerType, "Rolling Menus", "Current Week", "1")

        '****************************************************************************************************************************************************************************************************************************

        If Not ParameterHandler.CheckExists("PAXTON") Then ParameterHandler.Create("PAXTON", ParameterHandler.EnumParameterType.BooleanType, "Paxton Integration", "Enabled", "False")
        If Not ParameterHandler.CheckExists("PAXTONSERVER") Then ParameterHandler.Create("PAXTONSERVER", ParameterHandler.EnumParameterType.StringType, "Paxton Integration", "Net2 Server IP", "")
        If Not ParameterHandler.CheckExists("PAXTONUSER") Then ParameterHandler.Create("PAXTONUSER", ParameterHandler.EnumParameterType.StringType, "Paxton Integration", "Net2 User", "")
        If Not ParameterHandler.CheckExists("PAXTONPASSWORD") Then ParameterHandler.Create("PAXTONPASSWORD", ParameterHandler.EnumParameterType.StringType, "Paxton Integration", "Net2 Password", "")
        If Not ParameterHandler.CheckExists("PAXTONACU") Then ParameterHandler.Create("PAXTONACU", ParameterHandler.EnumParameterType.StringType, "Paxton Integration", "Net2 ACU Serial", "")

        '****************************************************************************************************************************************************************************************************************************

        If Not ParameterHandler.CheckExists("INVOICESUBJECT") Then ParameterHandler.Create("INVOICESUBJECT", ParameterHandler.EnumParameterType.StringType, "Email Templates", "Invoice Email Subject Template", "")
        If Not ParameterHandler.CheckExists("INVOICEBODY") Then ParameterHandler.Create("INVOICEBODY", ParameterHandler.EnumParameterType.StringType, "Email Templates", "Invoice Email Body Template", "")

        Dim _Subject As String = ParameterHandler.ReturnString("INVOICESUBJECT")
        If _Subject = "" Then
            _Subject = "Invoice for {ChildForename} covering {InvoicePeriod}"
            ParameterHandler.SetString("INVOICESUBJECT", _Subject)
            ParameterHandler.SetString("INVOICEBODY", "")
        End If

        Dim _Body As String = ParameterHandler.ReturnString("INVOICEBODY")
        If _Body = "" Then
            _Body = ""
            _Body += "Dear {ContactForename},"
            _Body += vbCrLf
            _Body += vbCrLf
            _Body += "Please find attached your Invoice for {ChildForename} for {InvoiceMonth}."
            _Body += vbCrLf
            _Body += vbCrLf
            _Body += "£{InvoiceTotal} is due on {InvoiceDue}."
            ParameterHandler.SetString("INVOICEBODY", _Body)
        End If

        '********** 1.18.1.0 **********
        If Not ParameterHandler.CheckExists("DEFCONTACT") Then ParameterHandler.Create("DEFCONTACT", ParameterHandler.EnumParameterType.StringType, "Default Values", "Default Primary Contact", "")
        If Not ParameterHandler.CheckExists("POSTPLACEMENT") Then ParameterHandler.Create("POSTPLACEMENT", ParameterHandler.EnumParameterType.BooleanType, "Options", "Show Post Placement button", "False")

    End Sub

    Private Sub CreateLists()

        'ensure lists required by the application have been created.

        CreateList("Food Manufacturers")
        CreateList("Food Groups")
        CreateList("Nature of Illness")
        CreateList("Medicine")
        CreateList("Dosage")
        CreateList("Classification")
        CreateList("NL Codes")
        CreateList("NL Tracking")
        CreateList("Lead Source")
        CreateList("Lead Via")

        CreateList("Incident Location")
        CreateList("Incident Detail")
        CreateList("Incident Injury")
        CreateList("Incident Treatment")
        CreateList("Incident Action")

        CreateList("Term Type")
        CreateListItem("Term Type", "School Term", 10)
        CreateListItem("Term Type", "Student Term", 20)

        CreateList("Diet Restrictions")
        CreateListItem("Diet Restrictions", "None", 10)
        CreateListItem("Diet Restrictions", "Vegetarian", 20)
        CreateListItem("Diet Restrictions", "Kosher", 30)
        CreateListItem("Diet Restrictions", "Halal", 40)

        CreateList("Invoice Layouts")
        CreateListItem("Invoice Layouts", "InvoiceWithSummary.repx", 10)

        CreateList("Invoice Frequency")
        CreateListItem("Invoice Frequency", "Weekly", 10)
        CreateListItem("Invoice Frequency", "Monthly", 20)

        CreateList("Absence Type")
        CreateListItem("Absence Type", "Sickness", 10)
        CreateListItem("Absence Type", "Annual Leave", 20)
        CreateListItem("Absence Type", "Maternity Leave", 30)
        CreateListItem("Absence Type", "Paternity Leave", 40)
        CreateListItem("Absence Type", "Adoption Leave", 50)
        CreateListItem("Absence Type", "Parental Leave", 60)
        CreateListItem("Absence Type", "Compassionate Leave", 70)
        CreateListItem("Absence Type", "Educational Leave", 80)
        CreateListItem("Absence Type", "TOIL", 90)
        CreateListItem("Absence Type", "Unauthorised Absence", 100)

        CreateList("Nationality")
        CreateListItem("Nationality", "Unknown", 0)
        CreateListItem("Nationality", "British", 10)
        CreateListItem("Nationality", "English", 20)
        CreateListItem("Nationality", "Welsh", 30)
        CreateListItem("Nationality", "Scottish", 40)
        CreateListItem("Nationality", "Irish", 50)
        CreateListItem("Nationality", "Pakistani", 60)
        CreateListItem("Nationality", "African", 70)
        CreateListItem("Nationality", "Chinese", 80)
        CreateListItem("Nationality", "Other", 90)

        CreateList("Ethnicity")
        CreateListItem("Ethnicity", "Unknown", 0)
        CreateListItem("Ethnicity", "White British", 10)
        CreateListItem("Ethnicity", "White (other)", 20)
        CreateListItem("Ethnicity", "Indian", 30)
        CreateListItem("Ethnicity", "Pakistani", 40)
        CreateListItem("Ethnicity", "White Irish", 50)
        CreateListItem("Ethnicity", "Mixed Race", 60)
        CreateListItem("Ethnicity", "Black Caribbean", 70)
        CreateListItem("Ethnicity", "Black African", 80)
        CreateListItem("Ethnicity", "Bangladeshi", 90)
        CreateListItem("Ethnicity", "Other Asian (non-Chinese)", 100)
        CreateListItem("Ethnicity", "Chinese", 110)
        CreateListItem("Ethnicity", "Other", 120)
        CreateListItem("Ethnicity", "Black (others)", 130)

        CreateList("Religion")
        CreateListItem("Religion", "Unknown", 0)
        CreateListItem("Religion", "Christian", 10)
        CreateListItem("Religion", "Muslim", 20)
        CreateListItem("Religion", "Hindu", 30)
        CreateListItem("Religion", "Sikh", 40)
        CreateListItem("Religion", "Jewish", 50)
        CreateListItem("Religion", "Buddhist", 60)
        CreateListItem("Religion", "Other", 70)

        CreateList("Language")
        CreateListItem("Language", "Unknown", 0)
        CreateListItem("Language", "English", 10)
        CreateListItem("Language", "Welsh", 20)

        CreateList("Immunisation")
        CreateListItem("Immunisation", "5-in-1 (DTaP/IPV/Hib)", 10)
        CreateListItem("Immunisation", "Pneumococcal (PCV)", 20)
        CreateListItem("Immunisation", "Rotavirus", 30)
        CreateListItem("Immunisation", "Men B", 40)
        CreateListItem("Immunisation", "Men C", 50)
        CreateListItem("Immunisation", "Hib/Men C (booster)", 60)
        CreateListItem("Immunisation", "MMR", 70)
        CreateListItem("Immunisation", "Flu", 80)
        CreateListItem("Immunisation", "4-in-1 pre-school booster", 90)

        CreateList("Consent")
        CreateListItem("Consent", "Outings", 10, "General")
        CreateListItem("Consent", "Outdoor Play", 20, "General")
        CreateListItem("Consent", "Medical Treatment", 30, "Medical")
        CreateListItem("Consent", "Calpol", 40, "Medical")
        CreateListItem("Consent", "Plasters", 50, "Medical")
        CreateListItem("Consent", "Sun Cream", 60, "Medical")
        CreateListItem("Consent", "Photographs", 70, "Photography")

        CreateList("Attribute")
        CreateListItem("Attribute", "Swimmer", 10)
        CreateListItem("Attribute", "No Face Painting", 20)

        CreateList("Allergy Matrix")
        CreateListItem("Allergy Matrix", "Dairy", 10)
        CreateListItem("Allergy Matrix", "Eggs", 20)
        CreateListItem("Allergy Matrix", "Soya", 30)
        CreateListItem("Allergy Matrix", "Nuts", 40)
        CreateListItem("Allergy Matrix", "Wheat", 50)
        CreateListItem("Allergy Matrix", "Gluten", 60)
        CreateListItem("Allergy Matrix", "Latex", 70)
        CreateListItem("Allergy Matrix", "Oral Allergy Syndrome", 80)
        CreateListItem("Allergy Matrix", "Penicillin", 90)
        CreateListItem("Allergy Matrix", "Insect Stings", 100)

        CreateList("Bolt-On Categories")
        CreateListItem("Bolt-On Categories", "Meals", 10)
        CreateListItem("Bolt-On Categories", "Activities", 20)
        CreateListItem("Bolt-On Categories", "Other", 30)

        CreateList("Child Absence Types")
        CreateListItem("Child Absence Types", "Illness", 10)
        CreateListItem("Child Absence Types", "Transport/Travel Issue", 20)

        CreateList("Report Categories")
        CreateListItem("Report Categories", "Child Reports", 10)
        CreateListItem("Report Categories", "Contact Reports", 20)

        CreateList("Staff Contract Type")
        CreateListItem("Staff Contract Type", "Permanent", 10)
        CreateListItem("Staff Contract Type", "Temporary", 20)
        CreateListItem("Staff Contract Type", "Term-Time", 30)
        CreateListItem("Staff Contract Type", "Zero Hours", 40)

        CreateList("Payment Method")
        CreateListItem("Payment Method", "Unknown", 10)
        CreateListItem("Payment Method", "Direct Debit", 20)
        CreateListItem("Payment Method", "Standing Order", 30)

        CreateList("Funding Type")
        CreateListItem("Funding Type", "None", 10)
        CreateListItem("Funding Type", "2 Years Funding", 20)
        CreateListItem("Funding Type", "3-4 Years Funding", 30)
        CreateListItem("Funding Type", "EYPP", 40)

        CreateList("Voucher Providers")
        CreateListItem("Voucher Providers", "Allsave", 10)
        CreateListItem("Voucher Providers", "Computershare", 20)
        CreateListItem("Voucher Providers", "Edenred", 30)
        CreateListItem("Voucher Providers", "Employers 4 Childcare Vouchers", 40)
        CreateListItem("Voucher Providers", "Enjoy Benefits", 50)
        CreateListItem("Voucher Providers", "Faircare", 60)
        CreateListItem("Voucher Providers", "care-4", 70)
        CreateListItem("Voucher Providers", "Sodexo", 80)
        CreateListItem("Voucher Providers", "KiddiVouchers", 90)
        CreateListItem("Voucher Providers", "Fideliti", 100)

        CreateList("Absence Reason")
        CreateListItem("Absence Reason", "Minor illness", 10)
        CreateListItem("Absence Reason", "Musculoskeletal injuries", 20)
        CreateListItem("Absence Reason", "Back pain", 30)
        CreateListItem("Absence Reason", "Stress", 40)
        CreateListItem("Absence Reason", "Recurring medical conditions", 50)
        CreateListItem("Absence Reason", "Mental ill-health", 60)
        CreateListItem("Absence Reason", "Home/family/carer resp.", 70)
        CreateListItem("Absence Reason", "Injuries/accidents not work", 80)
        CreateListItem("Absence Reason", "Injuries/accidents work", 90)
        CreateListItem("Absence Reason", "Acute medical conditions", 100)
        CreateListItem("Absence Reason", "Pregnancy related", 110)
        CreateListItem("Absence Reason", "Drink/drug related", 120)
        CreateListItem("Absence Reason", "Non genuine", 130)
        CreateListItem("Absence Reason", "Stress", 140)

        CreateList("Equipment Category")
        CreateListItem("Equipment Category", "Heating", 10)
        CreateListItem("Equipment Category", "Safety Equipment", 20)
        CreateListItem("Equipment Category", "Vehicles", 30)

        CreateList("Department")
        CreateListItem("Department", "Practitioners", 10)
        CreateListItem("Department", "Senior Staff", 20)
        CreateListItem("Department", "Bank Staff", 30)
        CreateListItem("Department", "Support Staff", 40)
        CreateListItem("Department", "Owners", 50)

        CreateList("Recruitment Stages")
        CreateListItem("Recruitment Stages", "Applied", 10)
        CreateListItem("Recruitment Stages", "Excluded", 20)
        CreateListItem("Recruitment Stages", "Shortlisted", 30)
        CreateListItem("Recruitment Stages", "Invite to Interview", 40)
        CreateListItem("Recruitment Stages", "Interviewed", 50)
        CreateListItem("Recruitment Stages", "Invite to Second Interview", 60)
        CreateListItem("Recruitment Stages", "Offered", 70)
        CreateListItem("Recruitment Stages", "Successful", 80)
        CreateListItem("Recruitment Stages", "Unsuccessful", 90)

        CreateList("Risk Assessment Hazard")
        CreateListItem("Risk Assessment Hazard", "Slips & Trips", 0)
        CreateListItem("Risk Assessment Hazard", "Manual Handling", 0)
        CreateListItem("Risk Assessment Hazard", "Contact with Heat", 0)
        CreateListItem("Risk Assessment Hazard", "Wet Hands", 0)
        CreateListItem("Risk Assessment Hazard", "Flooring", 0)
        CreateListItem("Risk Assessment Hazard", "Contact with Chemicals", 0)
        CreateListItem("Risk Assessment Hazard", "Electrical Equipment", 0)
        CreateListItem("Risk Assessment Hazard", "Fire", 0)
        CreateListItem("Risk Assessment Hazard", "Working at Height", 0)
        CreateListItem("Risk Assessment Hazard", "Vehicles", 0)
        CreateListItem("Risk Assessment Hazard", "Infectious Diseases", 0)
        CreateListItem("Risk Assessment Hazard", "Asbestos", 0)
        CreateListItem("Risk Assessment Hazard", "Legionella", 0)
        CreateListItem("Risk Assessment Hazard", "Inexperienced Staff", 0)
        CreateListItem("Risk Assessment Hazard", "Pregnant Persons", 0)
        CreateListItem("Risk Assessment Hazard", "Unattended Children", 0)
        CreateListItem("Risk Assessment Hazard", "Security of External Doors", 0)
        CreateListItem("Risk Assessment Hazard", "Medicine", 0)
        CreateListItem("Risk Assessment Hazard", "Outings", 0)

        CreateList("Risk Assessment Who")
        CreateListItem("Risk Assessment Who", "Anyone", 10)
        CreateListItem("Risk Assessment Who", "Children", 20)
        CreateListItem("Risk Assessment Who", "Employees", 30)
        CreateListItem("Risk Assessment Who", "Parents", 40)
        CreateListItem("Risk Assessment Who", "Visitors", 50)
        CreateListItem("Risk Assessment Who", "Contractors", 60)
        CreateListItem("Risk Assessment Who", "Public", 70)

        CreateList("Communication Preferences")
        CreateListItem("Communication Preferences", "Unknown", 10)
        CreateListItem("Communication Preferences", "All Methods", 20)
        CreateListItem("Communication Preferences", "Email Only", 30)
        CreateListItem("Communication Preferences", "SMS Only", 40)

        CreateList("Register Weekly Layout")
        CreateListItem("Register Weekly Layout", "Standard", 10, "RegisterWeek.repx")

        CreateList("Register Daily Layout")
        CreateListItem("Register Daily Layout", "Standard", 10, "RegisterDay.repx")

        CreateList("DBS Check Type")
        CreateListItem("DBS Check Type", "Standard", 10)
        CreateListItem("DBS Check Type", "Enhanced", 20)
        CreateListItem("DBS Check Type", "Enhanced with barred list", 30)

    End Sub

    Private Sub CreateList(ByVal ListName As String)
        If Not ListHandler.ListExists(ListName) Then ListHandler.CreateList(ListName, 0)
    End Sub

    Private Sub CreateListItem(ByVal ListName As String, ByVal ItemName As String, ByVal DisplaySeq As Integer, Optional ByVal Ref1 As String = "")
        If ListHandler.ListExists(ListName) Then
            Dim _ListID As String = ""
            If DAL.ReturnField(Session.ConnectionString, "AppLists", "name", ListName, "ID", _ListID) Then
                If Not ListHandler.ItemExists(ListName, ItemName) Then ListHandler.CreateItem(_ListID, ItemName, DisplaySeq, Ref1)
            End If
        End If
    End Sub

    'Private Sub GenerateInvoiceDays()

    '    Dim _SQL As String = ""
    '    _SQL += "select ID from Invoices where ID not in"
    '    _SQL += " (select InvoiceDays.invoice_id from InvoiceDays where InvoiceDays.invoice_id = Invoices.ID)"

    '    Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
    '    If _DT IsNot Nothing Then

    '        Session.SetupProgressBar("Generating Invoice Days...", _DT.Rows.Count)
    '        For Each _DR As DataRow In _DT.Rows

    '            Dim _ID As Guid = New Guid(_DR.Item("ID").ToString)
    '            Invoicing.GenerateInvoiceDays(_ID)

    '            Dim _Invoice As Business.Invoice = Business.Invoice.RetreiveByID(_ID)
    '            Invoicing.CalculateInvoiceTotals(_Invoice, Invoicing.EnumAnnualised.None)
    '            _Invoice.Store()

    '            Session.StepProgressBar()

    '        Next

    '        _DT.Dispose()
    '        _DT = Nothing

    '    End If

    '    _SQL = "update InvoiceDays set child_forename = (select Children.forename from Children where Children.ID = InvoiceDays.child_id)"
    '    DAL.ExecuteSQL(Session.ConnectionString, _SQL)

    '    _SQL = "update InvoiceDays set child_surname = (select Children.surname from Children where Children.ID = InvoiceDays.child_id)"
    '    DAL.ExecuteSQL(Session.ConnectionString, _SQL)

    'End Sub

    Private Sub GenerateThumbnails()

        Dim _ThumbnailSize As Integer = 325

        Dim _SQL As String = ""
        _SQL += "select ID from AppDocs"
        _SQL += " where tags <> 'original' and tags <> 'thumbnail'"
        _SQL += " and (file_ext in ('jpg','jpeg','png','tiff','bmp') or subject like '%photo%')"
        _SQL += " order by stamp desc"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _ID As Guid = New Guid(_DR.Item("ID").ToString)

                'fetch the document
                Dim _SourceDoc As Care.Shared.Business.Document = Care.Shared.Business.Document.RetreiveByID(Session.ConnectionString, _ID)
                If _SourceDoc IsNot Nothing Then

                    Dim _SourceImage As System.Drawing.Image = Care.Shared.DocumentHandler.GetImagefromByteArray(_SourceDoc._Data)
                    If _SourceImage IsNot Nothing Then
                        If _SourceImage.Size.Height > _ThumbnailSize OrElse _SourceImage.Size.Width > _ThumbnailSize Then

                            'create a new document based upon the original
                            DocumentHandler.InsertDocument(_SourceDoc._KeyId, Care.Shared.DocumentHandler.DocumentType.Application, _SourceDoc._Data, "Original " + _SourceDoc._Subject, "original")

                            'overwrite it with the thumbnail
                            _SourceDoc._Data = Care.Shared.DocumentHandler.ReturnThumbnail(_SourceDoc._Data, _ThumbnailSize)
                            _SourceDoc._FileSize = _SourceDoc._Data.Length
                            _SourceDoc._Tags = "thumbnail"

                            'save document
                            Care.Shared.Business.Document.SaveRecord(_SourceDoc)

                        End If
                    End If
                End If
            Next

        End If

        _DT.Dispose()
        _DT = Nothing

    End Sub



End Class