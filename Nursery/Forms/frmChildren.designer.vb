﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmChildren
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmChildren))
        Dim OptionsSpelling4 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling5 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling6 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling7 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling8 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling3 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling2 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.SchedulerStorage1 = New DevExpress.XtraScheduler.SchedulerStorage(Me.components)
        Me.GroupBox3 = New Care.Controls.CareFrame(Me.components)
        Me.PictureBox = New Care.Controls.PhotoControl()
        Me.GroupBox4 = New Care.Controls.CareFrame(Me.components)
        Me.cbxSite = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.cbxKeyworker = New Care.Controls.CareComboBox(Me.components)
        Me.cbxGroup = New Care.Controls.CareComboBox(Me.components)
        Me.Label13 = New Care.Controls.CareLabel(Me.components)
        Me.Label14 = New Care.Controls.CareLabel(Me.components)
        Me.OpenFileDialog = New System.Windows.Forms.OpenFileDialog()
        Me.tcMain = New Care.Controls.CareTab(Me.components)
        Me.tabGeneral = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl20 = New Care.Controls.CareFrame(Me.components)
        Me.lblMoveIn = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel24 = New Care.Controls.CareLabel(Me.components)
        Me.cbxMoveMode = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel42 = New Care.Controls.CareLabel(Me.components)
        Me.cdtMoveDate = New Care.Controls.CareDateTime(Me.components)
        Me.cbxMoveRoom = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel41 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl18 = New Care.Controls.CareFrame(Me.components)
        Me.txtTags = New DevExpress.XtraEditors.TokenEdit()
        Me.GroupControl1 = New Care.Controls.CareFrame(Me.components)
        Me.ccxNationality = New Care.Controls.CareCheckedComboBox(Me.components)
        Me.ccxLanguage = New Care.Controls.CareCheckedComboBox(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel21 = New Care.Controls.CareLabel(Me.components)
        Me.cbxEthnicity = New Care.Controls.CareComboBox(Me.components)
        Me.cbxReligion = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl7 = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel45 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel44 = New Care.Controls.CareLabel(Me.components)
        Me.txtSchoolRef2 = New Care.Controls.CareTextBox(Me.components)
        Me.txtSchoolRef1 = New Care.Controls.CareTextBox(Me.components)
        Me.cbxSchool = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel19 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox1 = New Care.Controls.CareFrame(Me.components)
        Me.txtKnownAs = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel40 = New Care.Controls.CareLabel(Me.components)
        Me.lblLeaveDate = New Care.Controls.CareLabel(Me.components)
        Me.Label31 = New Care.Controls.CareLabel(Me.components)
        Me.udtLeft = New Care.Controls.CareDateTime(Me.components)
        Me.lblStartDate = New Care.Controls.CareLabel(Me.components)
        Me.lblClass = New Care.Controls.CareLabel(Me.components)
        Me.cbxClass = New Care.Controls.CareComboBox(Me.components)
        Me.Label12 = New Care.Controls.CareLabel(Me.components)
        Me.cbxGender = New Care.Controls.CareComboBox(Me.components)
        Me.lblAge = New Care.Controls.CareLabel(Me.components)
        Me.cbxStatus = New Care.Controls.CareComboBox(Me.components)
        Me.btnFamilyView = New Care.Controls.CareButton(Me.components)
        Me.btnFamilySet = New Care.Controls.CareButton(Me.components)
        Me.Label18 = New Care.Controls.CareLabel(Me.components)
        Me.Label16 = New Care.Controls.CareLabel(Me.components)
        Me.udtDOB = New Care.Controls.CareDateTime(Me.components)
        Me.txtSurname = New Care.Controls.CareTextBox(Me.components)
        Me.lblFamily = New Care.Controls.CareLabel(Me.components)
        Me.Label4 = New Care.Controls.CareLabel(Me.components)
        Me.udtJoined = New Care.Controls.CareDateTime(Me.components)
        Me.txtForename = New Care.Controls.CareTextBox(Me.components)
        Me.txtFamily = New Care.Controls.CareTextBox(Me.components)
        Me.Label2 = New Care.Controls.CareLabel(Me.components)
        Me.Label1 = New Care.Controls.CareLabel(Me.components)
        Me.tabSessions = New DevExpress.XtraTab.XtraTabPage()
        Me.tcSessions = New Care.Controls.CareTab(Me.components)
        Me.tabSessPatterns = New DevExpress.XtraTab.XtraTabPage()
        Me.gbxSessions = New Care.Controls.CareFrame(Me.components)
        Me.radSessAll = New Care.Controls.CareRadioButton(Me.components)
        Me.radSessAdditional = New Care.Controls.CareRadioButton(Me.components)
        Me.radSessRecurring = New Care.Controls.CareRadioButton(Me.components)
        Me.radSessOverride = New Care.Controls.CareRadioButton(Me.components)
        Me.radSessSingle = New Care.Controls.CareRadioButton(Me.components)
        Me.radSessMulti = New Care.Controls.CareRadioButton(Me.components)
        Me.grdPatterns = New Care.Controls.CareGrid()
        Me.tabSessProfile = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl21 = New Care.Controls.CareFrame(Me.components)
        Me.txtVoucherSequence = New Care.Controls.CareTextBox(Me.components)
        Me.txtVoucherProportion = New Care.Controls.CareTextBox(Me.components)
        Me.lblSessVoucherValue = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel10 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel16 = New Care.Controls.CareLabel(Me.components)
        Me.cbxVoucherMode = New Care.Controls.CareComboBox(Me.components)
        Me.GroupControl17 = New Care.Controls.CareFrame(Me.components)
        Me.cbxTermType = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel20 = New Care.Controls.CareLabel(Me.components)
        Me.chkTermTime = New Care.Controls.CareCheckBox(Me.components)
        Me.GroupControl14 = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel37 = New Care.Controls.CareLabel(Me.components)
        Me.txtFundRef = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel36 = New Care.Controls.CareLabel(Me.components)
        Me.cbxFundType = New Care.Controls.CareComboBox(Me.components)
        Me.GroupControl6 = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel18 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel15 = New Care.Controls.CareLabel(Me.components)
        Me.cbxNLCode = New Care.Controls.CareComboBox(Me.components)
        Me.cbxNLTracking = New Care.Controls.CareComboBox(Me.components)
        Me.GroupBox2 = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel13 = New Care.Controls.CareLabel(Me.components)
        Me.txtPaymentRef = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel35 = New Care.Controls.CareLabel(Me.components)
        Me.cbxPaymentDue = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel39 = New Care.Controls.CareLabel(Me.components)
        Me.cbxPaymentFreq = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel38 = New Care.Controls.CareLabel(Me.components)
        Me.cbxPaymentMethod = New Care.Controls.CareComboBox(Me.components)
        Me.Label30 = New Care.Controls.CareLabel(Me.components)
        Me.cbxDiscount = New Care.Controls.CareComboBox(Me.components)
        Me.tabSessAnnual = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl4 = New Care.Controls.CareFrame(Me.components)
        Me.radCalcAnnual = New Care.Controls.CareRadioButton(Me.components)
        Me.radCalcMonthly = New Care.Controls.CareRadioButton(Me.components)
        Me.radCalcWeekly = New Care.Controls.CareRadioButton(Me.components)
        Me.radAnnualPatterns = New Care.Controls.CareRadioButton(Me.components)
        Me.radAnnualManual = New Care.Controls.CareRadioButton(Me.components)
        Me.grdAnnual = New Care.Controls.CareGrid()
        Me.tabSessMonthly = New DevExpress.XtraTab.XtraTabPage()
        Me.grdSessMonthly = New Care.Controls.CareGrid()
        Me.tabSessHolidays = New DevExpress.XtraTab.XtraTabPage()
        Me.CareFrame3 = New Care.Controls.CareFrame(Me.components)
        Me.radHolidaysAll = New Care.Controls.CareRadioButton(Me.components)
        Me.radHolidays = New Care.Controls.CareRadioButton(Me.components)
        Me.radAbsence = New Care.Controls.CareRadioButton(Me.components)
        Me.grdHolidays = New Care.Controls.CareGrid()
        Me.tabSessOther = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl8 = New Care.Controls.CareFrame(Me.components)
        Me.radAddAll = New Care.Controls.CareRadioButton(Me.components)
        Me.radAddMonthly = New Care.Controls.CareRadioButton(Me.components)
        Me.radAddOneOff = New Care.Controls.CareRadioButton(Me.components)
        Me.radAddWeekly = New Care.Controls.CareRadioButton(Me.components)
        Me.grdAdditional = New Care.Controls.CareGrid()
        Me.tabSessDeposits = New DevExpress.XtraTab.XtraTabPage()
        Me.CareFrame2 = New Care.Controls.CareFrame(Me.components)
        Me.lblDepositBalance = New Care.Controls.CareLabel(Me.components)
        Me.grdDeposits = New Care.Controls.CareGrid()
        Me.tabSessDiscounts = New DevExpress.XtraTab.XtraTabPage()
        Me.grdDiscounts = New Care.Controls.CareGrid()
        Me.tabInvoices = New DevExpress.XtraTab.XtraTabPage()
        Me.CareFrame1 = New Care.Controls.CareFrame(Me.components)
        Me.radInvoicesPosted = New Care.Controls.CareRadioButton(Me.components)
        Me.radNonPostedCredits = New Care.Controls.CareRadioButton(Me.components)
        Me.radInvoices = New Care.Controls.CareRadioButton(Me.components)
        Me.radPostedCredits = New Care.Controls.CareRadioButton(Me.components)
        Me.cgInvoices = New Care.Controls.CareGrid()
        Me.lblHolidays = New Care.Controls.CareLabel(Me.components)
        Me.btnAdvAdd = New Care.Controls.CareButton(Me.components)
        Me.btnAdvEdit = New Care.Controls.CareButton(Me.components)
        Me.btnAdvRemove = New Care.Controls.CareButton(Me.components)
        Me.tabCalendar = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl5 = New Care.Controls.CareFrame(Me.components)
        Me.radBookCalendar = New Care.Controls.CareRadioButton(Me.components)
        Me.radBookRegister = New Care.Controls.CareRadioButton(Me.components)
        Me.radBookActual = New Care.Controls.CareRadioButton(Me.components)
        Me.btnBuildBookings = New Care.Controls.CareButton(Me.components)
        Me.yvBookings = New Nursery.YearView()
        Me.cgBookings = New Care.Controls.CareGridWithButtons()
        Me.tabHealth = New DevExpress.XtraTab.XtraTabPage()
        Me.tcHealth = New Care.Controls.CareTab(Me.components)
        Me.tabAllergyDiet = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl11 = New Care.Controls.CareFrame(Me.components)
        Me.cgAllergyMatrix = New Care.Controls.CareGrid()
        Me.GroupControl9 = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.cbxDietRestrict = New Care.Controls.CareComboBox(Me.components)
        Me.txtDietNotes = New DevExpress.XtraEditors.MemoEdit()
        Me.GroupBox8 = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.cbxAllergyGrade = New Care.Controls.CareComboBox(Me.components)
        Me.txtMedAllergies = New DevExpress.XtraEditors.MemoEdit()
        Me.tabMedication = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupBox6 = New Care.Controls.CareFrame(Me.components)
        Me.txtMedMedication = New DevExpress.XtraEditors.MemoEdit()
        Me.GroupBox9 = New Care.Controls.CareFrame(Me.components)
        Me.txtMedNotes = New DevExpress.XtraEditors.MemoEdit()
        Me.tabMedContact = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl10 = New Care.Controls.CareFrame(Me.components)
        Me.txtHVTelephone = New Care.[Shared].CareTelephoneNumber()
        Me.txtHVMobile = New Care.[Shared].CareTelephoneNumber()
        Me.txtHVEmail = New Care.[Shared].CareEmailAddress()
        Me.CareLabel23 = New Care.Controls.CareLabel(Me.components)
        Me.txtHVName = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox7 = New Care.Controls.CareFrame(Me.components)
        Me.txtSurgTel = New Care.[Shared].CareTelephoneNumber()
        Me.txtSurgDoctor = New Care.Controls.CareTextBox(Me.components)
        Me.Label33 = New Care.Controls.CareLabel(Me.components)
        Me.txtSurgName = New Care.Controls.CareTextBox(Me.components)
        Me.Label34 = New Care.Controls.CareLabel(Me.components)
        Me.Label32 = New Care.Controls.CareLabel(Me.components)
        Me.tabSEND = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl12 = New Care.Controls.CareFrame(Me.components)
        Me.chkSEN = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel22 = New Care.Controls.CareLabel(Me.components)
        Me.txtSENNotes = New DevExpress.XtraEditors.MemoEdit()
        Me.tabImms = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl16 = New Care.Controls.CareFrame(Me.components)
        Me.gbxImm = New Care.Controls.CareFrame(Me.components)
        Me.btnImmCancel = New Care.Controls.CareButton(Me.components)
        Me.btnImmOK = New Care.Controls.CareButton(Me.components)
        Me.cbxImm = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel30 = New Care.Controls.CareLabel(Me.components)
        Me.cdtImmDate = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel32 = New Care.Controls.CareLabel(Me.components)
        Me.cgImms = New Care.Controls.CareGridWithButtons()
        Me.tabConsent = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl15 = New Care.Controls.CareFrame(Me.components)
        Me.gbxConsent = New Care.Controls.CareFrame(Me.components)
        Me.btnConsentClear = New Care.Controls.CareButton(Me.components)
        Me.txtConsentItem = New Care.Controls.CareTextBox(Me.components)
        Me.cbxConsentContact = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel27 = New Care.Controls.CareLabel(Me.components)
        Me.btnConsentCancel = New Care.Controls.CareButton(Me.components)
        Me.btnConsentOK = New Care.Controls.CareButton(Me.components)
        Me.CareLabel25 = New Care.Controls.CareLabel(Me.components)
        Me.cdtConsentDate = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel26 = New Care.Controls.CareLabel(Me.components)
        Me.cgConsent = New Care.Controls.CareGrid()
        Me.tabAttributes = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl13 = New Care.Controls.CareFrame(Me.components)
        Me.clbAttributes = New DevExpress.XtraEditors.CheckedListBoxControl()
        Me.tabRepeatingMedication = New DevExpress.XtraTab.XtraTabPage()
        Me.gbxRepeatMedication = New Care.Controls.CareFrame(Me.components)
        Me.txtManualTimeEntry = New Care.Controls.CareTextBox(Me.components)
        Me.txtStartTime = New Care.Controls.CareTextBox(Me.components)
        Me.btnManualTimeEntryRemove = New Care.Controls.CareButton(Me.components)
        Me.btnManualTimeEntryAdd = New Care.Controls.CareButton(Me.components)
        Me.chkAsRequired = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel47 = New Care.Controls.CareLabel(Me.components)
        Me.lblManualTimeEntries = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel43 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel33 = New Care.Controls.CareLabel(Me.components)
        Me.btnRepeatingMedicationCancel = New Care.Controls.CareButton(Me.components)
        Me.btnRepeatingMedicationOK = New Care.Controls.CareButton(Me.components)
        Me.txtFrequency = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel31 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel29 = New Care.Controls.CareLabel(Me.components)
        Me.cbxDosageRequired = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel28 = New Care.Controls.CareLabel(Me.components)
        Me.cbxMedicationRequired = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel17 = New Care.Controls.CareLabel(Me.components)
        Me.cbxNatureOfIllness = New Care.Controls.CareComboBox(Me.components)
        Me.cgRepeatingMedication = New Care.Controls.CareGridWithButtons()
        Me.tabMedical = New DevExpress.XtraTab.XtraTabPage()
        Me.yvMedInc = New Nursery.YearView()
        Me.btnMedPrintAll = New Care.Controls.CareButton(Me.components)
        Me.btnDelete = New Care.Controls.CareButton(Me.components)
        Me.btnMedPrint = New Care.Controls.CareButton(Me.components)
        Me.grdMedical = New Care.Controls.CareGrid()
        Me.GroupControl2 = New Care.Controls.CareFrame(Me.components)
        Me.radMedCalendar = New Care.Controls.CareRadioButton(Me.components)
        Me.radMedPriorIncidents = New Care.Controls.CareRadioButton(Me.components)
        Me.radMedIncidents = New Care.Controls.CareRadioButton(Me.components)
        Me.radMedAccidents = New Care.Controls.CareRadioButton(Me.components)
        Me.radMedGiven = New Care.Controls.CareRadioButton(Me.components)
        Me.radMedRequests = New Care.Controls.CareRadioButton(Me.components)
        Me.tabReports = New DevExpress.XtraTab.XtraTabPage()
        Me.CareFrame4 = New Care.Controls.CareFrame(Me.components)
        Me.btnPIN = New Care.Controls.CareButton(Me.components)
        Me.txtPIN = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel34 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl19 = New Care.Controls.CareFrame(Me.components)
        Me.chkNappies = New Care.Controls.CareCheckBox(Me.components)
        Me.chkMilk = New Care.Controls.CareCheckBox(Me.components)
        Me.chkFoodDetail = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.chkOffMenu = New Care.Controls.CareCheckBox(Me.components)
        Me.chkBabyFood = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel12 = New Care.Controls.CareLabel(Me.components)
        Me.Label35 = New Care.Controls.CareLabel(Me.components)
        Me.Label25 = New Care.Controls.CareLabel(Me.components)
        Me.Label26 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox10 = New Care.Controls.CareFrame(Me.components)
        Me.lblSleep = New Care.Controls.CareLabel(Me.components)
        Me.lblMedicine = New Care.Controls.CareLabel(Me.components)
        Me.chkSMSMilk = New Care.Controls.CareCheckBox(Me.components)
        Me.chkSMSSleep = New Care.Controls.CareCheckBox(Me.components)
        Me.lblObs = New Care.Controls.CareLabel(Me.components)
        Me.lblFood = New Care.Controls.CareLabel(Me.components)
        Me.lblMilk = New Care.Controls.CareLabel(Me.components)
        Me.chkSMSFood = New Care.Controls.CareCheckBox(Me.components)
        Me.chkSMSMedicine = New Care.Controls.CareCheckBox(Me.components)
        Me.lblNappy = New Care.Controls.CareLabel(Me.components)
        Me.chkSMSNappy = New Care.Controls.CareCheckBox(Me.components)
        Me.chkSMSObs = New Care.Controls.CareCheckBox(Me.components)
        Me.GroupBox12 = New Care.Controls.CareFrame(Me.components)
        Me.Label42 = New Care.Controls.CareLabel(Me.components)
        Me.cbxReportDetail = New Care.Controls.CareComboBox(Me.components)
        Me.chkPaperReport = New Care.Controls.CareCheckBox(Me.components)
        Me.btnEmail = New Care.Controls.CareButton(Me.components)
        Me.btnPrint = New Care.Controls.CareButton(Me.components)
        Me.grdReports = New Care.Controls.CareGrid()
        Me.tabAssessment = New DevExpress.XtraTab.XtraTabPage()
        Me.btnPrintProfile = New Care.Controls.CareButton(Me.components)
        Me.GroupControl3 = New Care.Controls.CareFrame(Me.components)
        Me.radCDAPNotAttained = New Care.Controls.CareRadioButton(Me.components)
        Me.radCDAPAttained = New Care.Controls.CareRadioButton(Me.components)
        Me.radCDAPAll = New Care.Controls.CareRadioButton(Me.components)
        Me.grdAssessment = New Care.Controls.CareGrid()
        Me.tabActivity = New DevExpress.XtraTab.XtraTabPage()
        Me.grdActivity = New Care.Controls.CareGrid()
        Me.ctxPopup = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuUpload = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFullSize = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDelete = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnLabelTablemats = New Care.Controls.CareButton(Me.components)
        Me.bgwBookings = New System.ComponentModel.BackgroundWorker()
        Me.txtYellow = New DevExpress.XtraEditors.TextEdit()
        Me.txtRed = New DevExpress.XtraEditors.TextEdit()
        Me.txtGreen = New DevExpress.XtraEditors.TextEdit()
        Me.ssm = New DevExpress.XtraSplashScreen.SplashScreenManager(Me, GetType(Global.Nursery.frmBookingsModal), True, True)
        Me.btnPostPlacement = New Care.Controls.CareButton(Me.components)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.SchedulerStorage1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.GroupBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxKeyworker.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxGroup.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tcMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tcMain.SuspendLayout()
        Me.tabGeneral.SuspendLayout()
        CType(Me.GroupControl20, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl20.SuspendLayout()
        CType(Me.cbxMoveMode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtMoveDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtMoveDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxMoveRoom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl18, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl18.SuspendLayout()
        CType(Me.txtTags.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.ccxNationality.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ccxLanguage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxEthnicity.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxReligion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl7.SuspendLayout()
        CType(Me.txtSchoolRef2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSchoolRef1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSchool.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtKnownAs.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.udtLeft.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.udtLeft.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxClass.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxGender.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.udtDOB.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.udtDOB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.udtJoined.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.udtJoined.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtForename.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFamily.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabSessions.SuspendLayout()
        CType(Me.tcSessions, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tcSessions.SuspendLayout()
        Me.tabSessPatterns.SuspendLayout()
        CType(Me.gbxSessions, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxSessions.SuspendLayout()
        CType(Me.radSessAll.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radSessAdditional.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radSessRecurring.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radSessOverride.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radSessSingle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radSessMulti.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabSessProfile.SuspendLayout()
        CType(Me.GroupControl21, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl21.SuspendLayout()
        CType(Me.txtVoucherSequence.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtVoucherProportion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxVoucherMode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl17, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl17.SuspendLayout()
        CType(Me.cbxTermType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkTermTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl14, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl14.SuspendLayout()
        CType(Me.txtFundRef.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxFundType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl6.SuspendLayout()
        CType(Me.cbxNLCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxNLTracking.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.txtPaymentRef.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxPaymentDue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxPaymentFreq.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxPaymentMethod.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxDiscount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabSessAnnual.SuspendLayout()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.radCalcAnnual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radCalcMonthly.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radCalcWeekly.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radAnnualPatterns.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radAnnualManual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabSessMonthly.SuspendLayout()
        Me.tabSessHolidays.SuspendLayout()
        CType(Me.CareFrame3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CareFrame3.SuspendLayout()
        CType(Me.radHolidaysAll.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radHolidays.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radAbsence.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabSessOther.SuspendLayout()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl8.SuspendLayout()
        CType(Me.radAddAll.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radAddMonthly.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radAddOneOff.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radAddWeekly.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabSessDeposits.SuspendLayout()
        CType(Me.CareFrame2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CareFrame2.SuspendLayout()
        Me.tabSessDiscounts.SuspendLayout()
        Me.tabInvoices.SuspendLayout()
        CType(Me.CareFrame1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CareFrame1.SuspendLayout()
        CType(Me.radInvoicesPosted.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radNonPostedCredits.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radInvoices.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radPostedCredits.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabCalendar.SuspendLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.radBookCalendar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radBookRegister.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radBookActual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabHealth.SuspendLayout()
        CType(Me.tcHealth, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tcHealth.SuspendLayout()
        Me.tabAllergyDiet.SuspendLayout()
        CType(Me.GroupControl11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl11.SuspendLayout()
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl9.SuspendLayout()
        CType(Me.cbxDietRestrict.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDietNotes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox8.SuspendLayout()
        CType(Me.cbxAllergyGrade.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMedAllergies.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabMedication.SuspendLayout()
        CType(Me.GroupBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        CType(Me.txtMedMedication.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox9.SuspendLayout()
        CType(Me.txtMedNotes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabMedContact.SuspendLayout()
        CType(Me.GroupControl10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl10.SuspendLayout()
        CType(Me.txtHVName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox7.SuspendLayout()
        CType(Me.txtSurgDoctor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSurgName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabSEND.SuspendLayout()
        CType(Me.GroupControl12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl12.SuspendLayout()
        CType(Me.chkSEN.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSENNotes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabImms.SuspendLayout()
        CType(Me.GroupControl16, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl16.SuspendLayout()
        CType(Me.gbxImm, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxImm.SuspendLayout()
        CType(Me.cbxImm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtImmDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtImmDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabConsent.SuspendLayout()
        CType(Me.GroupControl15, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl15.SuspendLayout()
        CType(Me.gbxConsent, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxConsent.SuspendLayout()
        CType(Me.txtConsentItem.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxConsentContact.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtConsentDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtConsentDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabAttributes.SuspendLayout()
        CType(Me.GroupControl13, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl13.SuspendLayout()
        CType(Me.clbAttributes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabRepeatingMedication.SuspendLayout()
        CType(Me.gbxRepeatMedication, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxRepeatMedication.SuspendLayout()
        CType(Me.txtManualTimeEntry.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStartTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAsRequired.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFrequency.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxDosageRequired.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxMedicationRequired.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxNatureOfIllness.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabMedical.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.radMedCalendar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radMedPriorIncidents.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radMedIncidents.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radMedAccidents.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radMedGiven.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radMedRequests.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabReports.SuspendLayout()
        CType(Me.CareFrame4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CareFrame4.SuspendLayout()
        CType(Me.txtPIN.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl19, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl19.SuspendLayout()
        CType(Me.chkNappies.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkMilk.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkFoodDetail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkOffMenu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkBabyFood.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox10.SuspendLayout()
        CType(Me.chkSMSMilk.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSMSSleep.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSMSFood.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSMSMedicine.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSMSNappy.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSMSObs.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox12.SuspendLayout()
        CType(Me.cbxReportDetail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkPaperReport.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabAssessment.SuspendLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.radCDAPNotAttained.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radCDAPAttained.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radCDAPAll.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabActivity.SuspendLayout()
        Me.ctxPopup.SuspendLayout()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnCancel.Location = New System.Drawing.Point(759, 4)
        Me.btnCancel.TabIndex = 2
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(668, 4)
        Me.btnOK.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnPostPlacement)
        Me.Panel1.Controls.Add(Me.txtGreen)
        Me.Panel1.Controls.Add(Me.txtRed)
        Me.Panel1.Controls.Add(Me.txtYellow)
        Me.Panel1.Controls.Add(Me.btnLabelTablemats)
        Me.Panel1.Location = New System.Drawing.Point(0, 565)
        Me.Panel1.Size = New System.Drawing.Size(858, 35)
        Me.Panel1.TabIndex = 1
        Me.Panel1.Controls.SetChildIndex(Me.btnOK, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnCancel, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnLabelTablemats, 0)
        Me.Panel1.Controls.SetChildIndex(Me.txtYellow, 0)
        Me.Panel1.Controls.SetChildIndex(Me.txtRed, 0)
        Me.Panel1.Controls.SetChildIndex(Me.txtGreen, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnPostPlacement, 0)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.PictureBox)
        Me.GroupBox3.Location = New System.Drawing.Point(494, 7)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(329, 256)
        Me.GroupBox3.TabIndex = 1
        Me.GroupBox3.Text = "Photo (Right Click to Amend Photo)"
        '
        'PictureBox
        '
        Me.PictureBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBox.DocumentID = Nothing
        Me.PictureBox.Image = Nothing
        Me.PictureBox.Location = New System.Drawing.Point(2, 20)
        Me.PictureBox.Margin = New System.Windows.Forms.Padding(4)
        Me.PictureBox.Name = "PictureBox"
        Me.PictureBox.Padding = New System.Windows.Forms.Padding(1, 1, 0, 0)
        Me.PictureBox.Size = New System.Drawing.Size(325, 234)
        Me.PictureBox.TabIndex = 0
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.cbxSite)
        Me.GroupBox4.Controls.Add(Me.CareLabel14)
        Me.GroupBox4.Controls.Add(Me.cbxKeyworker)
        Me.GroupBox4.Controls.Add(Me.cbxGroup)
        Me.GroupBox4.Controls.Add(Me.Label13)
        Me.GroupBox4.Controls.Add(Me.Label14)
        Me.GroupBox4.Location = New System.Drawing.Point(9, 361)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(305, 115)
        Me.GroupBox4.TabIndex = 4
        Me.GroupBox4.Text = "Site, Room && Keyworker Information"
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(91, 29)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AccessibleName = "Site"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(202, 20)
        Me.cbxSite.TabIndex = 16
        Me.cbxSite.Tag = "AEBM"
        Me.cbxSite.ValueMember = Nothing
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel14.Appearance.Options.UseFont = True
        Me.CareLabel14.Appearance.Options.UseTextOptions = True
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(12, 32)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(19, 15)
        Me.CareLabel14.TabIndex = 0
        Me.CareLabel14.Text = "Site"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel14.ToolTip = "The last date the child will attend the Nursery. This date is used for invoicing " &
    "purposes."
        Me.CareLabel14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'cbxKeyworker
        '
        Me.cbxKeyworker.AllowBlank = False
        Me.cbxKeyworker.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxKeyworker.DataSource = Nothing
        Me.cbxKeyworker.DisplayMember = Nothing
        Me.cbxKeyworker.EnterMoveNextControl = True
        Me.cbxKeyworker.Location = New System.Drawing.Point(91, 85)
        Me.cbxKeyworker.Name = "cbxKeyworker"
        Me.cbxKeyworker.Properties.AccessibleName = "Keyworker"
        Me.cbxKeyworker.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxKeyworker.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxKeyworker.Properties.Appearance.Options.UseFont = True
        Me.cbxKeyworker.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxKeyworker.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxKeyworker.SelectedValue = Nothing
        Me.cbxKeyworker.Size = New System.Drawing.Size(202, 20)
        Me.cbxKeyworker.TabIndex = 18
        Me.cbxKeyworker.Tag = "AEBM"
        Me.cbxKeyworker.ValueMember = Nothing
        '
        'cbxGroup
        '
        Me.cbxGroup.AllowBlank = False
        Me.cbxGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxGroup.DataSource = Nothing
        Me.cbxGroup.DisplayMember = Nothing
        Me.cbxGroup.EnterMoveNextControl = True
        Me.cbxGroup.Location = New System.Drawing.Point(91, 57)
        Me.cbxGroup.Name = "cbxGroup"
        Me.cbxGroup.Properties.AccessibleName = "Group"
        Me.cbxGroup.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxGroup.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxGroup.Properties.Appearance.Options.UseFont = True
        Me.cbxGroup.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxGroup.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxGroup.SelectedValue = Nothing
        Me.cbxGroup.Size = New System.Drawing.Size(202, 20)
        Me.cbxGroup.TabIndex = 17
        Me.cbxGroup.Tag = "AEBM"
        Me.cbxGroup.ValueMember = Nothing
        '
        'Label13
        '
        Me.Label13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label13.Appearance.Options.UseFont = True
        Me.Label13.Appearance.Options.UseTextOptions = True
        Me.Label13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label13.Location = New System.Drawing.Point(11, 88)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(58, 15)
        Me.Label13.TabIndex = 4
        Me.Label13.Text = "Key Person"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label14
        '
        Me.Label14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label14.Appearance.Options.UseFont = True
        Me.Label14.Appearance.Options.UseTextOptions = True
        Me.Label14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label14.Location = New System.Drawing.Point(11, 60)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(32, 15)
        Me.Label14.TabIndex = 2
        Me.Label14.Text = "Room"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tcMain
        '
        Me.tcMain.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tcMain.Appearance.Options.UseFont = True
        Me.tcMain.AppearancePage.Header.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tcMain.AppearancePage.Header.Options.UseFont = True
        Me.tcMain.Location = New System.Drawing.Point(9, 53)
        Me.tcMain.Name = "tcMain"
        Me.tcMain.SelectedTabPage = Me.tabGeneral
        Me.tcMain.Size = New System.Drawing.Size(838, 511)
        Me.tcMain.TabIndex = 0
        Me.tcMain.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabGeneral, Me.tabSessions, Me.tabCalendar, Me.tabHealth, Me.tabMedical, Me.tabReports, Me.tabAssessment, Me.tabActivity})
        '
        'tabGeneral
        '
        Me.tabGeneral.Controls.Add(Me.GroupControl20)
        Me.tabGeneral.Controls.Add(Me.GroupControl18)
        Me.tabGeneral.Controls.Add(Me.GroupControl1)
        Me.tabGeneral.Controls.Add(Me.GroupControl7)
        Me.tabGeneral.Controls.Add(Me.GroupBox1)
        Me.tabGeneral.Controls.Add(Me.GroupBox4)
        Me.tabGeneral.Controls.Add(Me.GroupBox3)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Size = New System.Drawing.Size(832, 481)
        Me.tabGeneral.Text = "General"
        '
        'GroupControl20
        '
        Me.GroupControl20.Controls.Add(Me.lblMoveIn)
        Me.GroupControl20.Controls.Add(Me.CareLabel24)
        Me.GroupControl20.Controls.Add(Me.cbxMoveMode)
        Me.GroupControl20.Controls.Add(Me.CareLabel42)
        Me.GroupControl20.Controls.Add(Me.cdtMoveDate)
        Me.GroupControl20.Controls.Add(Me.cbxMoveRoom)
        Me.GroupControl20.Controls.Add(Me.CareLabel41)
        Me.GroupControl20.Location = New System.Drawing.Point(320, 361)
        Me.GroupControl20.Name = "GroupControl20"
        Me.GroupControl20.Size = New System.Drawing.Size(274, 115)
        Me.GroupControl20.TabIndex = 5
        Me.GroupControl20.Text = "Future Room Move"
        '
        'lblMoveIn
        '
        Me.lblMoveIn.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMoveIn.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblMoveIn.Appearance.Options.UseFont = True
        Me.lblMoveIn.Appearance.Options.UseForeColor = True
        Me.lblMoveIn.Appearance.Options.UseTextOptions = True
        Me.lblMoveIn.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblMoveIn.Location = New System.Drawing.Point(143, 60)
        Me.lblMoveIn.Name = "lblMoveIn"
        Me.lblMoveIn.Size = New System.Drawing.Size(99, 15)
        Me.lblMoveIn.TabIndex = 25
        Me.lblMoveIn.Text = "1 year, 11 months"
        Me.lblMoveIn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel24
        '
        Me.CareLabel24.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel24.Appearance.Options.UseFont = True
        Me.CareLabel24.Appearance.Options.UseTextOptions = True
        Me.CareLabel24.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel24.Location = New System.Drawing.Point(10, 32)
        Me.CareLabel24.Name = "CareLabel24"
        Me.CareLabel24.Size = New System.Drawing.Size(31, 15)
        Me.CareLabel24.TabIndex = 0
        Me.CareLabel24.Text = "Mode"
        Me.CareLabel24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxMoveMode
        '
        Me.cbxMoveMode.AllowBlank = False
        Me.cbxMoveMode.DataSource = Nothing
        Me.cbxMoveMode.DisplayMember = Nothing
        Me.cbxMoveMode.EnterMoveNextControl = True
        Me.cbxMoveMode.Location = New System.Drawing.Point(52, 29)
        Me.cbxMoveMode.Name = "cbxMoveMode"
        Me.cbxMoveMode.Properties.AccessibleName = "Group"
        Me.cbxMoveMode.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxMoveMode.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxMoveMode.Properties.Appearance.Options.UseFont = True
        Me.cbxMoveMode.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxMoveMode.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxMoveMode.SelectedValue = Nothing
        Me.cbxMoveMode.Size = New System.Drawing.Size(85, 22)
        Me.cbxMoveMode.TabIndex = 19
        Me.cbxMoveMode.Tag = "AEB"
        Me.cbxMoveMode.ValueMember = Nothing
        '
        'CareLabel42
        '
        Me.CareLabel42.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel42.Appearance.Options.UseFont = True
        Me.CareLabel42.Appearance.Options.UseTextOptions = True
        Me.CareLabel42.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel42.Location = New System.Drawing.Point(10, 60)
        Me.CareLabel42.Name = "CareLabel42"
        Me.CareLabel42.Size = New System.Drawing.Size(24, 15)
        Me.CareLabel42.TabIndex = 2
        Me.CareLabel42.Text = "Date"
        Me.CareLabel42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtMoveDate
        '
        Me.cdtMoveDate.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtMoveDate.EnterMoveNextControl = True
        Me.cdtMoveDate.Location = New System.Drawing.Point(52, 57)
        Me.cdtMoveDate.Name = "cdtMoveDate"
        Me.cdtMoveDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtMoveDate.Properties.Appearance.Options.UseFont = True
        Me.cdtMoveDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtMoveDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtMoveDate.Size = New System.Drawing.Size(85, 22)
        Me.cdtMoveDate.TabIndex = 20
        Me.cdtMoveDate.Tag = ""
        Me.cdtMoveDate.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'cbxMoveRoom
        '
        Me.cbxMoveRoom.AllowBlank = False
        Me.cbxMoveRoom.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxMoveRoom.DataSource = Nothing
        Me.cbxMoveRoom.DisplayMember = Nothing
        Me.cbxMoveRoom.EnterMoveNextControl = True
        Me.cbxMoveRoom.Location = New System.Drawing.Point(52, 85)
        Me.cbxMoveRoom.Name = "cbxMoveRoom"
        Me.cbxMoveRoom.Properties.AccessibleName = "Group"
        Me.cbxMoveRoom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxMoveRoom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxMoveRoom.Properties.Appearance.Options.UseFont = True
        Me.cbxMoveRoom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxMoveRoom.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxMoveRoom.SelectedValue = Nothing
        Me.cbxMoveRoom.Size = New System.Drawing.Size(212, 20)
        Me.cbxMoveRoom.TabIndex = 21
        Me.cbxMoveRoom.Tag = ""
        Me.cbxMoveRoom.ValueMember = Nothing
        '
        'CareLabel41
        '
        Me.CareLabel41.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel41.Appearance.Options.UseFont = True
        Me.CareLabel41.Appearance.Options.UseTextOptions = True
        Me.CareLabel41.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel41.Location = New System.Drawing.Point(10, 88)
        Me.CareLabel41.Name = "CareLabel41"
        Me.CareLabel41.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel41.TabIndex = 4
        Me.CareLabel41.Text = "Room"
        Me.CareLabel41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl18
        '
        Me.GroupControl18.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl18.Appearance.Options.UseFont = True
        Me.GroupControl18.Controls.Add(Me.txtTags)
        Me.GroupControl18.Location = New System.Drawing.Point(600, 269)
        Me.GroupControl18.Name = "GroupControl18"
        Me.GroupControl18.Size = New System.Drawing.Size(223, 86)
        Me.GroupControl18.TabIndex = 3
        Me.GroupControl18.Text = "Tags"
        '
        'txtTags
        '
        Me.txtTags.Location = New System.Drawing.Point(5, 30)
        Me.txtTags.Name = "txtTags"
        Me.txtTags.Properties.AutoHeightMode = DevExpress.XtraEditors.TokenEditAutoHeightMode.RestrictedExpand
        Me.txtTags.Properties.EditMode = DevExpress.XtraEditors.TokenEditMode.Manual
        Me.txtTags.Properties.MaxExpandLines = 1
        Me.txtTags.Properties.Separators.AddRange(New String() {",", ";"})
        Me.txtTags.Properties.ShowDropDown = False
        Me.txtTags.Properties.ShowRemoveTokenButtons = True
        Me.txtTags.Size = New System.Drawing.Size(213, 20)
        Me.txtTags.TabIndex = 15
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.ccxNationality)
        Me.GroupControl1.Controls.Add(Me.ccxLanguage)
        Me.GroupControl1.Controls.Add(Me.CareLabel7)
        Me.GroupControl1.Controls.Add(Me.CareLabel21)
        Me.GroupControl1.Controls.Add(Me.cbxEthnicity)
        Me.GroupControl1.Controls.Add(Me.cbxReligion)
        Me.GroupControl1.Controls.Add(Me.CareLabel9)
        Me.GroupControl1.Controls.Add(Me.CareLabel11)
        Me.GroupControl1.Location = New System.Drawing.Point(9, 269)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(585, 86)
        Me.GroupControl1.TabIndex = 2
        Me.GroupControl1.Text = "Other Details"
        '
        'ccxNationality
        '
        Me.ccxNationality.DataSource = Nothing
        Me.ccxNationality.DisplayMember = Nothing
        Me.ccxNationality.EditValue = ""
        Me.ccxNationality.EnterMoveNextControl = True
        Me.ccxNationality.Location = New System.Drawing.Point(394, 28)
        Me.ccxNationality.Name = "ccxNationality"
        Me.ccxNationality.Properties.AccessibleName = "Keyworker"
        Me.ccxNationality.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.ccxNationality.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.ccxNationality.Properties.Appearance.Options.UseFont = True
        Me.ccxNationality.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ccxNationality.SelectedItems = Nothing
        Me.ccxNationality.Size = New System.Drawing.Size(181, 22)
        Me.ccxNationality.TabIndex = 13
        Me.ccxNationality.Tag = "AEB"
        Me.ccxNationality.ValueMember = Nothing
        '
        'ccxLanguage
        '
        Me.ccxLanguage.DataSource = Nothing
        Me.ccxLanguage.DisplayMember = Nothing
        Me.ccxLanguage.EditValue = ""
        Me.ccxLanguage.EnterMoveNextControl = True
        Me.ccxLanguage.Location = New System.Drawing.Point(394, 56)
        Me.ccxLanguage.Name = "ccxLanguage"
        Me.ccxLanguage.Properties.AccessibleName = "Keyworker"
        Me.ccxLanguage.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.ccxLanguage.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.ccxLanguage.Properties.Appearance.Options.UseFont = True
        Me.ccxLanguage.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ccxLanguage.SelectedItems = Nothing
        Me.ccxLanguage.Size = New System.Drawing.Size(181, 22)
        Me.ccxLanguage.TabIndex = 14
        Me.ccxLanguage.Tag = "AEB"
        Me.ccxLanguage.ValueMember = Nothing
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel7.Appearance.Options.UseFont = True
        Me.CareLabel7.Appearance.Options.UseTextOptions = True
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(314, 59)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(65, 15)
        Me.CareLabel7.TabIndex = 6
        Me.CareLabel7.Text = "Language(s)"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel21
        '
        Me.CareLabel21.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel21.Appearance.Options.UseFont = True
        Me.CareLabel21.Appearance.Options.UseTextOptions = True
        Me.CareLabel21.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel21.Location = New System.Drawing.Point(314, 31)
        Me.CareLabel21.Name = "CareLabel21"
        Me.CareLabel21.Size = New System.Drawing.Size(58, 15)
        Me.CareLabel21.TabIndex = 4
        Me.CareLabel21.Text = "Nationality"
        Me.CareLabel21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxEthnicity
        '
        Me.cbxEthnicity.AllowBlank = False
        Me.cbxEthnicity.DataSource = Nothing
        Me.cbxEthnicity.DisplayMember = Nothing
        Me.cbxEthnicity.EnterMoveNextControl = True
        Me.cbxEthnicity.Location = New System.Drawing.Point(91, 56)
        Me.cbxEthnicity.Name = "cbxEthnicity"
        Me.cbxEthnicity.Properties.AccessibleName = "Keyworker"
        Me.cbxEthnicity.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxEthnicity.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxEthnicity.Properties.Appearance.Options.UseFont = True
        Me.cbxEthnicity.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxEthnicity.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxEthnicity.SelectedValue = Nothing
        Me.cbxEthnicity.Size = New System.Drawing.Size(214, 22)
        Me.cbxEthnicity.TabIndex = 12
        Me.cbxEthnicity.Tag = "AEB"
        Me.cbxEthnicity.ValueMember = Nothing
        '
        'cbxReligion
        '
        Me.cbxReligion.AllowBlank = False
        Me.cbxReligion.DataSource = Nothing
        Me.cbxReligion.DisplayMember = Nothing
        Me.cbxReligion.EnterMoveNextControl = True
        Me.cbxReligion.Location = New System.Drawing.Point(91, 28)
        Me.cbxReligion.Name = "cbxReligion"
        Me.cbxReligion.Properties.AccessibleName = "Group"
        Me.cbxReligion.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxReligion.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxReligion.Properties.Appearance.Options.UseFont = True
        Me.cbxReligion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxReligion.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxReligion.SelectedValue = Nothing
        Me.cbxReligion.Size = New System.Drawing.Size(214, 22)
        Me.cbxReligion.TabIndex = 11
        Me.cbxReligion.Tag = "AEB"
        Me.cbxReligion.ValueMember = Nothing
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel9.Appearance.Options.UseFont = True
        Me.CareLabel9.Appearance.Options.UseTextOptions = True
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(11, 59)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(46, 15)
        Me.CareLabel9.TabIndex = 2
        Me.CareLabel9.Text = "Ethnicity"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.Options.UseFont = True
        Me.CareLabel11.Appearance.Options.UseTextOptions = True
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(11, 31)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(43, 15)
        Me.CareLabel11.TabIndex = 0
        Me.CareLabel11.Text = "Religion"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl7
        '
        Me.GroupControl7.Controls.Add(Me.CareLabel45)
        Me.GroupControl7.Controls.Add(Me.CareLabel44)
        Me.GroupControl7.Controls.Add(Me.txtSchoolRef2)
        Me.GroupControl7.Controls.Add(Me.txtSchoolRef1)
        Me.GroupControl7.Controls.Add(Me.cbxSchool)
        Me.GroupControl7.Controls.Add(Me.CareLabel19)
        Me.GroupControl7.Location = New System.Drawing.Point(600, 361)
        Me.GroupControl7.Name = "GroupControl7"
        Me.GroupControl7.Size = New System.Drawing.Size(223, 115)
        Me.GroupControl7.TabIndex = 6
        Me.GroupControl7.Text = "School Details"
        '
        'CareLabel45
        '
        Me.CareLabel45.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel45.Appearance.Options.UseFont = True
        Me.CareLabel45.Appearance.Options.UseTextOptions = True
        Me.CareLabel45.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel45.Location = New System.Drawing.Point(8, 88)
        Me.CareLabel45.Name = "CareLabel45"
        Me.CareLabel45.Size = New System.Drawing.Size(26, 15)
        Me.CareLabel45.TabIndex = 4
        Me.CareLabel45.Text = "Ref 2"
        Me.CareLabel45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel44
        '
        Me.CareLabel44.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel44.Appearance.Options.UseFont = True
        Me.CareLabel44.Appearance.Options.UseTextOptions = True
        Me.CareLabel44.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel44.Location = New System.Drawing.Point(8, 60)
        Me.CareLabel44.Name = "CareLabel44"
        Me.CareLabel44.Size = New System.Drawing.Size(26, 15)
        Me.CareLabel44.TabIndex = 2
        Me.CareLabel44.Text = "Ref 1"
        Me.CareLabel44.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSchoolRef2
        '
        Me.txtSchoolRef2.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtSchoolRef2.EnterMoveNextControl = True
        Me.txtSchoolRef2.Location = New System.Drawing.Point(63, 85)
        Me.txtSchoolRef2.MaxLength = 30
        Me.txtSchoolRef2.Name = "txtSchoolRef2"
        Me.txtSchoolRef2.NumericAllowNegatives = False
        Me.txtSchoolRef2.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSchoolRef2.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSchoolRef2.Properties.AccessibleDescription = ""
        Me.txtSchoolRef2.Properties.AccessibleName = "Forename"
        Me.txtSchoolRef2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSchoolRef2.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSchoolRef2.Properties.Appearance.Options.UseFont = True
        Me.txtSchoolRef2.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSchoolRef2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSchoolRef2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSchoolRef2.Properties.MaxLength = 30
        Me.txtSchoolRef2.Size = New System.Drawing.Size(152, 22)
        Me.txtSchoolRef2.TabIndex = 24
        Me.txtSchoolRef2.Tag = "AEB"
        Me.txtSchoolRef2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSchoolRef2.ToolTipText = ""
        '
        'txtSchoolRef1
        '
        Me.txtSchoolRef1.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtSchoolRef1.EnterMoveNextControl = True
        Me.txtSchoolRef1.Location = New System.Drawing.Point(63, 57)
        Me.txtSchoolRef1.MaxLength = 30
        Me.txtSchoolRef1.Name = "txtSchoolRef1"
        Me.txtSchoolRef1.NumericAllowNegatives = False
        Me.txtSchoolRef1.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSchoolRef1.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSchoolRef1.Properties.AccessibleDescription = ""
        Me.txtSchoolRef1.Properties.AccessibleName = "Forename"
        Me.txtSchoolRef1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSchoolRef1.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSchoolRef1.Properties.Appearance.Options.UseFont = True
        Me.txtSchoolRef1.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSchoolRef1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSchoolRef1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSchoolRef1.Properties.MaxLength = 30
        Me.txtSchoolRef1.Size = New System.Drawing.Size(152, 22)
        Me.txtSchoolRef1.TabIndex = 23
        Me.txtSchoolRef1.Tag = "AEB"
        Me.txtSchoolRef1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSchoolRef1.ToolTipText = ""
        '
        'cbxSchool
        '
        Me.cbxSchool.AllowBlank = False
        Me.cbxSchool.DataSource = Nothing
        Me.cbxSchool.DisplayMember = Nothing
        Me.cbxSchool.EnterMoveNextControl = True
        Me.cbxSchool.Location = New System.Drawing.Point(63, 29)
        Me.cbxSchool.Name = "cbxSchool"
        Me.cbxSchool.Properties.AccessibleName = "Group"
        Me.cbxSchool.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSchool.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxSchool.Properties.Appearance.Options.UseFont = True
        Me.cbxSchool.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSchool.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSchool.SelectedValue = Nothing
        Me.cbxSchool.Size = New System.Drawing.Size(152, 22)
        Me.cbxSchool.TabIndex = 22
        Me.cbxSchool.Tag = "AEB"
        Me.cbxSchool.ValueMember = Nothing
        '
        'CareLabel19
        '
        Me.CareLabel19.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel19.Appearance.Options.UseFont = True
        Me.CareLabel19.Appearance.Options.UseTextOptions = True
        Me.CareLabel19.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel19.Location = New System.Drawing.Point(8, 32)
        Me.CareLabel19.Name = "CareLabel19"
        Me.CareLabel19.Size = New System.Drawing.Size(36, 15)
        Me.CareLabel19.TabIndex = 0
        Me.CareLabel19.Text = "School"
        Me.CareLabel19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtKnownAs)
        Me.GroupBox1.Controls.Add(Me.CareLabel40)
        Me.GroupBox1.Controls.Add(Me.lblLeaveDate)
        Me.GroupBox1.Controls.Add(Me.Label31)
        Me.GroupBox1.Controls.Add(Me.udtLeft)
        Me.GroupBox1.Controls.Add(Me.lblStartDate)
        Me.GroupBox1.Controls.Add(Me.lblClass)
        Me.GroupBox1.Controls.Add(Me.cbxClass)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.cbxGender)
        Me.GroupBox1.Controls.Add(Me.lblAge)
        Me.GroupBox1.Controls.Add(Me.cbxStatus)
        Me.GroupBox1.Controls.Add(Me.btnFamilyView)
        Me.GroupBox1.Controls.Add(Me.btnFamilySet)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.udtDOB)
        Me.GroupBox1.Controls.Add(Me.txtSurname)
        Me.GroupBox1.Controls.Add(Me.lblFamily)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.udtJoined)
        Me.GroupBox1.Controls.Add(Me.txtForename)
        Me.GroupBox1.Controls.Add(Me.txtFamily)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(9, 7)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(479, 256)
        Me.GroupBox1.TabIndex = 0
        '
        'txtKnownAs
        '
        Me.txtKnownAs.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtKnownAs.EnterMoveNextControl = True
        Me.txtKnownAs.Location = New System.Drawing.Point(311, 56)
        Me.txtKnownAs.MaxLength = 30
        Me.txtKnownAs.Name = "txtKnownAs"
        Me.txtKnownAs.NumericAllowNegatives = False
        Me.txtKnownAs.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtKnownAs.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtKnownAs.Properties.AccessibleDescription = ""
        Me.txtKnownAs.Properties.AccessibleName = "Known As"
        Me.txtKnownAs.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtKnownAs.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtKnownAs.Properties.Appearance.Options.UseFont = True
        Me.txtKnownAs.Properties.Appearance.Options.UseTextOptions = True
        Me.txtKnownAs.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtKnownAs.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtKnownAs.Properties.MaxLength = 30
        Me.txtKnownAs.Size = New System.Drawing.Size(158, 22)
        Me.txtKnownAs.TabIndex = 3
        Me.txtKnownAs.Tag = "AE"
        Me.txtKnownAs.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtKnownAs.ToolTipText = ""
        '
        'CareLabel40
        '
        Me.CareLabel40.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel40.Appearance.Options.UseFont = True
        Me.CareLabel40.Appearance.Options.UseTextOptions = True
        Me.CareLabel40.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel40.Location = New System.Drawing.Point(252, 59)
        Me.CareLabel40.Name = "CareLabel40"
        Me.CareLabel40.Size = New System.Drawing.Size(53, 15)
        Me.CareLabel40.TabIndex = 6
        Me.CareLabel40.Text = "Known As"
        Me.CareLabel40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLeaveDate
        '
        Me.lblLeaveDate.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveDate.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblLeaveDate.Appearance.Options.UseFont = True
        Me.lblLeaveDate.Appearance.Options.UseForeColor = True
        Me.lblLeaveDate.Appearance.Options.UseTextOptions = True
        Me.lblLeaveDate.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblLeaveDate.Location = New System.Drawing.Point(182, 226)
        Me.lblLeaveDate.Name = "lblLeaveDate"
        Me.lblLeaveDate.Size = New System.Drawing.Size(147, 15)
        Me.lblLeaveDate.TabIndex = 24
        Me.lblLeaveDate.Text = "1 year, 11 months, 21 days"
        Me.lblLeaveDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label31
        '
        Me.Label31.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label31.Appearance.Options.UseFont = True
        Me.Label31.Appearance.Options.UseTextOptions = True
        Me.Label31.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label31.Location = New System.Drawing.Point(11, 226)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(68, 15)
        Me.Label31.TabIndex = 22
        Me.Label31.Text = "Leaving Date"
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label31.ToolTip = "The last date the child will attend the Nursery. This date is used for invoicing " &
    "purposes."
        Me.Label31.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'udtLeft
        '
        Me.udtLeft.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.udtLeft.EnterMoveNextControl = True
        Me.udtLeft.Location = New System.Drawing.Point(91, 223)
        Me.udtLeft.Name = "udtLeft"
        Me.udtLeft.Properties.AccessibleName = "Leaving Date"
        Me.udtLeft.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.udtLeft.Properties.Appearance.Options.UseFont = True
        Me.udtLeft.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.udtLeft.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.udtLeft.Size = New System.Drawing.Size(85, 22)
        Me.udtLeft.TabIndex = 41
        Me.udtLeft.Tag = "EB"
        Me.udtLeft.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'lblStartDate
        '
        Me.lblStartDate.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblStartDate.Appearance.Options.UseFont = True
        Me.lblStartDate.Appearance.Options.UseForeColor = True
        Me.lblStartDate.Appearance.Options.UseTextOptions = True
        Me.lblStartDate.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblStartDate.Location = New System.Drawing.Point(182, 200)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(147, 15)
        Me.lblStartDate.TabIndex = 21
        Me.lblStartDate.Text = "1 year, 11 months, 21 days"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblClass
        '
        Me.lblClass.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblClass.Appearance.Options.UseFont = True
        Me.lblClass.Appearance.Options.UseTextOptions = True
        Me.lblClass.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblClass.Location = New System.Drawing.Point(235, 115)
        Me.lblClass.Name = "lblClass"
        Me.lblClass.Size = New System.Drawing.Size(70, 15)
        Me.lblClass.TabIndex = 12
        Me.lblClass.Text = "Classification"
        Me.lblClass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxClass
        '
        Me.cbxClass.AllowBlank = False
        Me.cbxClass.DataSource = Nothing
        Me.cbxClass.DisplayMember = Nothing
        Me.cbxClass.EnterMoveNextControl = True
        Me.cbxClass.Location = New System.Drawing.Point(311, 112)
        Me.cbxClass.Name = "cbxClass"
        Me.cbxClass.Properties.AccessibleName = "Classification"
        Me.cbxClass.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxClass.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxClass.Properties.Appearance.Options.UseFont = True
        Me.cbxClass.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxClass.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxClass.SelectedValue = Nothing
        Me.cbxClass.Size = New System.Drawing.Size(158, 22)
        Me.cbxClass.TabIndex = 6
        Me.cbxClass.Tag = "AEBM"
        Me.cbxClass.ValueMember = Nothing
        '
        'Label12
        '
        Me.Label12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label12.Appearance.Options.UseFont = True
        Me.Label12.Appearance.Options.UseTextOptions = True
        Me.Label12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label12.Location = New System.Drawing.Point(11, 115)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(38, 15)
        Me.Label12.TabIndex = 10
        Me.Label12.Text = "Gender"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxGender
        '
        Me.cbxGender.AllowBlank = False
        Me.cbxGender.DataSource = Nothing
        Me.cbxGender.DisplayMember = Nothing
        Me.cbxGender.EnterMoveNextControl = True
        Me.cbxGender.Location = New System.Drawing.Point(91, 112)
        Me.cbxGender.Name = "cbxGender"
        Me.cbxGender.Properties.AccessibleName = "Gender"
        Me.cbxGender.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxGender.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxGender.Properties.Appearance.Options.UseFont = True
        Me.cbxGender.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxGender.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxGender.SelectedValue = Nothing
        Me.cbxGender.Size = New System.Drawing.Size(127, 22)
        Me.cbxGender.TabIndex = 5
        Me.cbxGender.Tag = "AEBM"
        Me.cbxGender.ValueMember = Nothing
        '
        'lblAge
        '
        Me.lblAge.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAge.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblAge.Appearance.Options.UseFont = True
        Me.lblAge.Appearance.Options.UseForeColor = True
        Me.lblAge.Appearance.Options.UseTextOptions = True
        Me.lblAge.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblAge.Location = New System.Drawing.Point(182, 174)
        Me.lblAge.Name = "lblAge"
        Me.lblAge.Size = New System.Drawing.Size(147, 15)
        Me.lblAge.TabIndex = 18
        Me.lblAge.Text = "1 year, 11 months, 21 days"
        Me.lblAge.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxStatus
        '
        Me.cbxStatus.AllowBlank = False
        Me.cbxStatus.DataSource = Nothing
        Me.cbxStatus.DisplayMember = Nothing
        Me.cbxStatus.EnterMoveNextControl = True
        Me.cbxStatus.Location = New System.Drawing.Point(91, 140)
        Me.cbxStatus.Name = "cbxStatus"
        Me.cbxStatus.Properties.AccessibleName = "Status"
        Me.cbxStatus.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxStatus.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxStatus.Properties.Appearance.Options.UseFont = True
        Me.cbxStatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxStatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxStatus.SelectedValue = Nothing
        Me.cbxStatus.Size = New System.Drawing.Size(378, 22)
        Me.cbxStatus.TabIndex = 7
        Me.cbxStatus.Tag = "EBM"
        Me.cbxStatus.ValueMember = Nothing
        '
        'btnFamilyView
        '
        Me.btnFamilyView.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnFamilyView.Appearance.Options.UseFont = True
        Me.btnFamilyView.CausesValidation = False
        Me.btnFamilyView.Location = New System.Drawing.Point(419, 27)
        Me.btnFamilyView.Name = "btnFamilyView"
        Me.btnFamilyView.Size = New System.Drawing.Size(50, 23)
        Me.btnFamilyView.TabIndex = 3
        Me.btnFamilyView.Text = "View"
        '
        'btnFamilySet
        '
        Me.btnFamilySet.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnFamilySet.Appearance.Options.UseFont = True
        Me.btnFamilySet.CausesValidation = False
        Me.btnFamilySet.Location = New System.Drawing.Point(363, 27)
        Me.btnFamilySet.Name = "btnFamilySet"
        Me.btnFamilySet.Size = New System.Drawing.Size(50, 23)
        Me.btnFamilySet.TabIndex = 2
        Me.btnFamilySet.Tag = "AE"
        Me.btnFamilySet.Text = "Set"
        '
        'Label18
        '
        Me.Label18.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label18.Appearance.Options.UseFont = True
        Me.Label18.Appearance.Options.UseTextOptions = True
        Me.Label18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label18.Location = New System.Drawing.Point(11, 87)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(47, 15)
        Me.Label18.TabIndex = 8
        Me.Label18.Text = "Surname"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label16
        '
        Me.Label16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label16.Appearance.Options.UseFont = True
        Me.Label16.Appearance.Options.UseTextOptions = True
        Me.Label16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label16.Location = New System.Drawing.Point(11, 174)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(66, 15)
        Me.Label16.TabIndex = 16
        Me.Label16.Text = "Date of Birth"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'udtDOB
        '
        Me.udtDOB.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.udtDOB.EnterMoveNextControl = True
        Me.udtDOB.Location = New System.Drawing.Point(91, 171)
        Me.udtDOB.Name = "udtDOB"
        Me.udtDOB.Properties.AccessibleName = "Date of Birth"
        Me.udtDOB.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.udtDOB.Properties.Appearance.Options.UseFont = True
        Me.udtDOB.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.udtDOB.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.udtDOB.Size = New System.Drawing.Size(85, 22)
        Me.udtDOB.TabIndex = 8
        Me.udtDOB.Tag = "AEBM"
        Me.udtDOB.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'txtSurname
        '
        Me.txtSurname.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtSurname.EnterMoveNextControl = True
        Me.txtSurname.Location = New System.Drawing.Point(91, 84)
        Me.txtSurname.MaxLength = 30
        Me.txtSurname.Name = "txtSurname"
        Me.txtSurname.NumericAllowNegatives = False
        Me.txtSurname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSurname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSurname.Properties.AccessibleName = "Surname"
        Me.txtSurname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSurname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSurname.Properties.Appearance.Options.UseFont = True
        Me.txtSurname.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSurname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSurname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSurname.Properties.MaxLength = 30
        Me.txtSurname.Size = New System.Drawing.Size(378, 22)
        Me.txtSurname.TabIndex = 4
        Me.txtSurname.Tag = "AEBM"
        Me.txtSurname.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSurname.ToolTipText = ""
        '
        'lblFamily
        '
        Me.lblFamily.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblFamily.Appearance.Options.UseFont = True
        Me.lblFamily.Appearance.Options.UseTextOptions = True
        Me.lblFamily.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblFamily.Location = New System.Drawing.Point(11, 31)
        Me.lblFamily.Name = "lblFamily"
        Me.lblFamily.Size = New System.Drawing.Size(35, 15)
        Me.lblFamily.TabIndex = 0
        Me.lblFamily.Text = "Family"
        Me.lblFamily.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label4.Appearance.Options.UseFont = True
        Me.Label4.Appearance.Options.UseTextOptions = True
        Me.Label4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label4.Location = New System.Drawing.Point(11, 200)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 15)
        Me.Label4.TabIndex = 19
        Me.Label4.Text = "Start Date"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label4.ToolTip = "The date the child started in the Nursery. This date is used for invoicing purpos" &
    "es."
        Me.Label4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'udtJoined
        '
        Me.udtJoined.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.udtJoined.EnterMoveNextControl = True
        Me.udtJoined.Location = New System.Drawing.Point(91, 197)
        Me.udtJoined.Name = "udtJoined"
        Me.udtJoined.Properties.AccessibleName = "Joining Date"
        Me.udtJoined.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.udtJoined.Properties.Appearance.Options.UseFont = True
        Me.udtJoined.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.udtJoined.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.udtJoined.Size = New System.Drawing.Size(85, 22)
        Me.udtJoined.TabIndex = 9
        Me.udtJoined.Tag = "AEBM"
        Me.udtJoined.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'txtForename
        '
        Me.txtForename.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtForename.EnterMoveNextControl = True
        Me.txtForename.Location = New System.Drawing.Point(91, 56)
        Me.txtForename.MaxLength = 30
        Me.txtForename.Name = "txtForename"
        Me.txtForename.NumericAllowNegatives = False
        Me.txtForename.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtForename.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtForename.Properties.AccessibleDescription = ""
        Me.txtForename.Properties.AccessibleName = "Forename"
        Me.txtForename.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtForename.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtForename.Properties.Appearance.Options.UseFont = True
        Me.txtForename.Properties.Appearance.Options.UseTextOptions = True
        Me.txtForename.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtForename.Properties.MaxLength = 30
        Me.txtForename.Size = New System.Drawing.Size(155, 22)
        Me.txtForename.TabIndex = 2
        Me.txtForename.Tag = "AEBM"
        Me.txtForename.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtForename.ToolTipText = ""
        '
        'txtFamily
        '
        Me.txtFamily.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtFamily.EnterMoveNextControl = True
        Me.txtFamily.Location = New System.Drawing.Point(91, 28)
        Me.txtFamily.MaxLength = 0
        Me.txtFamily.Name = "txtFamily"
        Me.txtFamily.NumericAllowNegatives = False
        Me.txtFamily.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtFamily.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFamily.Properties.AccessibleName = "Family"
        Me.txtFamily.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFamily.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtFamily.Properties.Appearance.Options.UseFont = True
        Me.txtFamily.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFamily.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFamily.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFamily.Size = New System.Drawing.Size(266, 22)
        Me.txtFamily.TabIndex = 1
        Me.txtFamily.Tag = "RM"
        Me.txtFamily.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtFamily.ToolTipText = ""
        '
        'Label2
        '
        Me.Label2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label2.Appearance.Options.UseFont = True
        Me.Label2.Appearance.Options.UseTextOptions = True
        Me.Label2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label2.Location = New System.Drawing.Point(11, 59)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 15)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Forename"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label1.Appearance.Options.UseFont = True
        Me.Label1.Appearance.Options.UseTextOptions = True
        Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label1.Location = New System.Drawing.Point(12, 143)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 15)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Status"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabSessions
        '
        Me.tabSessions.Controls.Add(Me.tcSessions)
        Me.tabSessions.Controls.Add(Me.lblHolidays)
        Me.tabSessions.Controls.Add(Me.btnAdvAdd)
        Me.tabSessions.Controls.Add(Me.btnAdvEdit)
        Me.tabSessions.Controls.Add(Me.btnAdvRemove)
        Me.tabSessions.Name = "tabSessions"
        Me.tabSessions.Size = New System.Drawing.Size(832, 481)
        Me.tabSessions.Text = "Sessions && Fees"
        '
        'tcSessions
        '
        Me.tcSessions.Location = New System.Drawing.Point(9, 6)
        Me.tcSessions.Name = "tcSessions"
        Me.tcSessions.SelectedTabPage = Me.tabSessPatterns
        Me.tcSessions.Size = New System.Drawing.Size(813, 442)
        Me.tcSessions.TabIndex = 13
        Me.tcSessions.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabSessPatterns, Me.tabSessProfile, Me.tabSessAnnual, Me.tabSessMonthly, Me.tabSessHolidays, Me.tabSessOther, Me.tabSessDeposits, Me.tabSessDiscounts, Me.tabInvoices})
        '
        'tabSessPatterns
        '
        Me.tabSessPatterns.Controls.Add(Me.gbxSessions)
        Me.tabSessPatterns.Name = "tabSessPatterns"
        Me.tabSessPatterns.Size = New System.Drawing.Size(807, 414)
        Me.tabSessPatterns.Text = "Patterns"
        '
        'gbxSessions
        '
        Me.gbxSessions.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxSessions.Controls.Add(Me.radSessAll)
        Me.gbxSessions.Controls.Add(Me.radSessAdditional)
        Me.gbxSessions.Controls.Add(Me.radSessRecurring)
        Me.gbxSessions.Controls.Add(Me.radSessOverride)
        Me.gbxSessions.Controls.Add(Me.radSessSingle)
        Me.gbxSessions.Controls.Add(Me.radSessMulti)
        Me.gbxSessions.Controls.Add(Me.grdPatterns)
        Me.gbxSessions.Location = New System.Drawing.Point(3, 3)
        Me.gbxSessions.Name = "gbxSessions"
        Me.gbxSessions.ShowCaption = False
        Me.gbxSessions.Size = New System.Drawing.Size(801, 408)
        Me.gbxSessions.TabIndex = 0
        '
        'radSessAll
        '
        Me.radSessAll.EditValue = True
        Me.radSessAll.Location = New System.Drawing.Point(5, 5)
        Me.radSessAll.Name = "radSessAll"
        Me.radSessAll.Properties.AutoWidth = True
        Me.radSessAll.Properties.Caption = "All"
        Me.radSessAll.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radSessAll.Properties.RadioGroupIndex = 0
        Me.radSessAll.Size = New System.Drawing.Size(33, 19)
        Me.radSessAll.TabIndex = 0
        '
        'radSessAdditional
        '
        Me.radSessAdditional.Location = New System.Drawing.Point(276, 5)
        Me.radSessAdditional.Name = "radSessAdditional"
        Me.radSessAdditional.Properties.AutoWidth = True
        Me.radSessAdditional.Properties.Caption = "Additional Bookings"
        Me.radSessAdditional.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radSessAdditional.Properties.RadioGroupIndex = 0
        Me.radSessAdditional.Size = New System.Drawing.Size(114, 19)
        Me.radSessAdditional.TabIndex = 3
        Me.radSessAdditional.TabStop = False
        Me.radSessAdditional.ToolTip = resources.GetString("radSessAdditional.ToolTip")
        Me.radSessAdditional.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.radSessAdditional.ToolTipTitle = "Additional Bookings"
        '
        'radSessRecurring
        '
        Me.radSessRecurring.Location = New System.Drawing.Point(44, 5)
        Me.radSessRecurring.Name = "radSessRecurring"
        Me.radSessRecurring.Properties.AutoWidth = True
        Me.radSessRecurring.Properties.Caption = "Recurring Patterns"
        Me.radSessRecurring.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radSessRecurring.Properties.RadioGroupIndex = 0
        Me.radSessRecurring.Size = New System.Drawing.Size(112, 19)
        Me.radSessRecurring.TabIndex = 1
        Me.radSessRecurring.TabStop = False
        Me.radSessRecurring.ToolTip = "These patterns recur until another pattern supersedes it." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "If you wish to stop " &
    "a recurring pattern - create a blank pattern." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.radSessRecurring.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.radSessRecurring.ToolTipTitle = "Recurring Patterns"
        '
        'radSessOverride
        '
        Me.radSessOverride.Location = New System.Drawing.Point(162, 5)
        Me.radSessOverride.Name = "radSessOverride"
        Me.radSessOverride.Properties.AutoWidth = True
        Me.radSessOverride.Properties.Caption = "Override Patterns"
        Me.radSessOverride.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radSessOverride.Properties.RadioGroupIndex = 0
        Me.radSessOverride.Size = New System.Drawing.Size(108, 19)
        Me.radSessOverride.TabIndex = 2
        Me.radSessOverride.TabStop = False
        Me.radSessOverride.ToolTip = resources.GetString("radSessOverride.ToolTip")
        Me.radSessOverride.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.radSessOverride.ToolTipTitle = "Override Patterns"
        '
        'radSessSingle
        '
        Me.radSessSingle.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.radSessSingle.EditValue = True
        Me.radSessSingle.Location = New System.Drawing.Point(617, 5)
        Me.radSessSingle.Name = "radSessSingle"
        Me.radSessSingle.Properties.AutoWidth = True
        Me.radSessSingle.Properties.Caption = "Single Session"
        Me.radSessSingle.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radSessSingle.Properties.RadioGroupIndex = 1
        Me.radSessSingle.Size = New System.Drawing.Size(89, 19)
        Me.radSessSingle.TabIndex = 4
        Me.radSessSingle.ToolTip = "One Session per Day (used to be called Weekly Mode)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "e.g. Full Day Sessions" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.radSessSingle.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.radSessSingle.ToolTipTitle = "Single Session"
        '
        'radSessMulti
        '
        Me.radSessMulti.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.radSessMulti.Location = New System.Drawing.Point(712, 5)
        Me.radSessMulti.Name = "radSessMulti"
        Me.radSessMulti.Properties.AutoWidth = True
        Me.radSessMulti.Properties.Caption = "Multi-Session"
        Me.radSessMulti.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radSessMulti.Properties.RadioGroupIndex = 1
        Me.radSessMulti.Size = New System.Drawing.Size(84, 19)
        Me.radSessMulti.TabIndex = 5
        Me.radSessMulti.TabStop = False
        Me.radSessMulti.ToolTip = "Multiple Sessions per Day (used to be called Daily Mode)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "e.g. Breakfast Club & A" &
    "fterschool Club"
        Me.radSessMulti.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.radSessMulti.ToolTipTitle = "Multi-Session"
        '
        'grdPatterns
        '
        Me.grdPatterns.AllowBuildColumns = True
        Me.grdPatterns.AllowEdit = False
        Me.grdPatterns.AllowHorizontalScroll = False
        Me.grdPatterns.AllowMultiSelect = False
        Me.grdPatterns.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdPatterns.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdPatterns.Appearance.Options.UseFont = True
        Me.grdPatterns.AutoSizeByData = True
        Me.grdPatterns.DisableAutoSize = False
        Me.grdPatterns.DisableDataFormatting = False
        Me.grdPatterns.FocusedRowHandle = -2147483648
        Me.grdPatterns.HideFirstColumn = False
        Me.grdPatterns.Location = New System.Drawing.Point(5, 30)
        Me.grdPatterns.Name = "grdPatterns"
        Me.grdPatterns.PreviewColumn = ""
        Me.grdPatterns.QueryID = Nothing
        Me.grdPatterns.RowAutoHeight = False
        Me.grdPatterns.SearchAsYouType = True
        Me.grdPatterns.ShowAutoFilterRow = False
        Me.grdPatterns.ShowFindPanel = False
        Me.grdPatterns.ShowGroupByBox = False
        Me.grdPatterns.ShowLoadingPanel = False
        Me.grdPatterns.ShowNavigator = False
        Me.grdPatterns.Size = New System.Drawing.Size(791, 373)
        Me.grdPatterns.TabIndex = 6
        Me.grdPatterns.TabStop = False
        '
        'tabSessProfile
        '
        Me.tabSessProfile.Controls.Add(Me.GroupControl21)
        Me.tabSessProfile.Controls.Add(Me.GroupControl17)
        Me.tabSessProfile.Controls.Add(Me.GroupControl14)
        Me.tabSessProfile.Controls.Add(Me.GroupControl6)
        Me.tabSessProfile.Controls.Add(Me.GroupBox2)
        Me.tabSessProfile.Name = "tabSessProfile"
        Me.tabSessProfile.Size = New System.Drawing.Size(807, 414)
        Me.tabSessProfile.Text = "Invoice Profile"
        '
        'GroupControl21
        '
        Me.GroupControl21.Controls.Add(Me.txtVoucherSequence)
        Me.GroupControl21.Controls.Add(Me.txtVoucherProportion)
        Me.GroupControl21.Controls.Add(Me.lblSessVoucherValue)
        Me.GroupControl21.Controls.Add(Me.CareLabel10)
        Me.GroupControl21.Controls.Add(Me.CareLabel16)
        Me.GroupControl21.Controls.Add(Me.cbxVoucherMode)
        Me.GroupControl21.Location = New System.Drawing.Point(3, 198)
        Me.GroupControl21.Name = "GroupControl21"
        Me.GroupControl21.Size = New System.Drawing.Size(384, 114)
        Me.GroupControl21.TabIndex = 1
        Me.GroupControl21.Text = "Automatic Voucher Processing"
        '
        'txtVoucherSequence
        '
        Me.txtVoucherSequence.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtVoucherSequence.EnterMoveNextControl = True
        Me.txtVoucherSequence.Location = New System.Drawing.Point(110, 83)
        Me.txtVoucherSequence.MaxLength = 0
        Me.txtVoucherSequence.Name = "txtVoucherSequence"
        Me.txtVoucherSequence.NumericAllowNegatives = False
        Me.txtVoucherSequence.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtVoucherSequence.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtVoucherSequence.Properties.AccessibleName = "Family"
        Me.txtVoucherSequence.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtVoucherSequence.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtVoucherSequence.Properties.Appearance.Options.UseFont = True
        Me.txtVoucherSequence.Properties.Appearance.Options.UseTextOptions = True
        Me.txtVoucherSequence.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtVoucherSequence.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtVoucherSequence.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtVoucherSequence.Size = New System.Drawing.Size(112, 22)
        Me.txtVoucherSequence.TabIndex = 5
        Me.txtVoucherSequence.Tag = "AEB"
        Me.txtVoucherSequence.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtVoucherSequence.ToolTipText = ""
        '
        'txtVoucherProportion
        '
        Me.txtVoucherProportion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtVoucherProportion.EnterMoveNextControl = True
        Me.txtVoucherProportion.Location = New System.Drawing.Point(110, 55)
        Me.txtVoucherProportion.MaxLength = 0
        Me.txtVoucherProportion.Name = "txtVoucherProportion"
        Me.txtVoucherProportion.NumericAllowNegatives = False
        Me.txtVoucherProportion.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtVoucherProportion.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtVoucherProportion.Properties.AccessibleName = "Family"
        Me.txtVoucherProportion.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtVoucherProportion.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtVoucherProportion.Properties.Appearance.Options.UseFont = True
        Me.txtVoucherProportion.Properties.Appearance.Options.UseTextOptions = True
        Me.txtVoucherProportion.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtVoucherProportion.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtVoucherProportion.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtVoucherProportion.Size = New System.Drawing.Size(112, 22)
        Me.txtVoucherProportion.TabIndex = 3
        Me.txtVoucherProportion.Tag = "AEB"
        Me.txtVoucherProportion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtVoucherProportion.ToolTipText = ""
        '
        'lblSessVoucherValue
        '
        Me.lblSessVoucherValue.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSessVoucherValue.Appearance.Options.UseFont = True
        Me.lblSessVoucherValue.Appearance.Options.UseTextOptions = True
        Me.lblSessVoucherValue.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblSessVoucherValue.Location = New System.Drawing.Point(8, 58)
        Me.lblSessVoucherValue.Name = "lblSessVoucherValue"
        Me.lblSessVoucherValue.Size = New System.Drawing.Size(29, 15)
        Me.lblSessVoucherValue.TabIndex = 2
        Me.lblSessVoucherValue.Text = "Value"
        Me.lblSessVoucherValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel10
        '
        Me.CareLabel10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel10.Appearance.Options.UseFont = True
        Me.CareLabel10.Appearance.Options.UseTextOptions = True
        Me.CareLabel10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel10.Location = New System.Drawing.Point(8, 86)
        Me.CareLabel10.Name = "CareLabel10"
        Me.CareLabel10.Size = New System.Drawing.Size(51, 15)
        Me.CareLabel10.TabIndex = 4
        Me.CareLabel10.Text = "Sequence"
        Me.CareLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel16
        '
        Me.CareLabel16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel16.Appearance.Options.UseFont = True
        Me.CareLabel16.Appearance.Options.UseTextOptions = True
        Me.CareLabel16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel16.Location = New System.Drawing.Point(8, 30)
        Me.CareLabel16.Name = "CareLabel16"
        Me.CareLabel16.Size = New System.Drawing.Size(91, 15)
        Me.CareLabel16.TabIndex = 0
        Me.CareLabel16.Text = "Processing Mode"
        Me.CareLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxVoucherMode
        '
        Me.cbxVoucherMode.AllowBlank = False
        Me.cbxVoucherMode.DataSource = Nothing
        Me.cbxVoucherMode.DisplayMember = Nothing
        Me.cbxVoucherMode.EnterMoveNextControl = True
        Me.cbxVoucherMode.Location = New System.Drawing.Point(110, 27)
        Me.cbxVoucherMode.Name = "cbxVoucherMode"
        Me.cbxVoucherMode.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxVoucherMode.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxVoucherMode.Properties.Appearance.Options.UseFont = True
        Me.cbxVoucherMode.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxVoucherMode.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxVoucherMode.SelectedValue = Nothing
        Me.cbxVoucherMode.Size = New System.Drawing.Size(265, 22)
        Me.cbxVoucherMode.TabIndex = 1
        Me.cbxVoucherMode.Tag = "AEB"
        Me.cbxVoucherMode.ValueMember = Nothing
        '
        'GroupControl17
        '
        Me.GroupControl17.Controls.Add(Me.cbxTermType)
        Me.GroupControl17.Controls.Add(Me.CareLabel2)
        Me.GroupControl17.Controls.Add(Me.CareLabel20)
        Me.GroupControl17.Controls.Add(Me.chkTermTime)
        Me.GroupControl17.Location = New System.Drawing.Point(393, 3)
        Me.GroupControl17.Name = "GroupControl17"
        Me.GroupControl17.Size = New System.Drawing.Size(409, 57)
        Me.GroupControl17.TabIndex = 2
        Me.GroupControl17.Text = "Term && Term Time Only Flag"
        '
        'cbxTermType
        '
        Me.cbxTermType.AllowBlank = False
        Me.cbxTermType.DataSource = Nothing
        Me.cbxTermType.DisplayMember = Nothing
        Me.cbxTermType.EnterMoveNextControl = True
        Me.cbxTermType.Location = New System.Drawing.Point(230, 27)
        Me.cbxTermType.Name = "cbxTermType"
        Me.cbxTermType.Properties.AccessibleName = "Group"
        Me.cbxTermType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxTermType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxTermType.Properties.Appearance.Options.UseFont = True
        Me.cbxTermType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxTermType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxTermType.SelectedValue = Nothing
        Me.cbxTermType.Size = New System.Drawing.Size(171, 22)
        Me.cbxTermType.TabIndex = 3
        Me.cbxTermType.Tag = "AEB"
        Me.cbxTermType.ValueMember = Nothing
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel2.Appearance.Options.UseFont = True
        Me.CareLabel2.Appearance.Options.UseTextOptions = True
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(167, 30)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(57, 15)
        Me.CareLabel2.TabIndex = 2
        Me.CareLabel2.Text = "Term Type"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel20
        '
        Me.CareLabel20.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel20.Appearance.Options.UseFont = True
        Me.CareLabel20.Appearance.Options.UseTextOptions = True
        Me.CareLabel20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel20.Location = New System.Drawing.Point(8, 30)
        Me.CareLabel20.Name = "CareLabel20"
        Me.CareLabel20.Size = New System.Drawing.Size(86, 15)
        Me.CareLabel20.TabIndex = 0
        Me.CareLabel20.Text = "Term Time Only"
        Me.CareLabel20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel20.ToolTip = resources.GetString("CareLabel20.ToolTip")
        Me.CareLabel20.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.CareLabel20.ToolTipTitle = "Term Time Only"
        '
        'chkTermTime
        '
        Me.chkTermTime.EnterMoveNextControl = True
        Me.chkTermTime.Location = New System.Drawing.Point(106, 28)
        Me.chkTermTime.Name = "chkTermTime"
        Me.chkTermTime.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTermTime.Properties.Appearance.Options.UseFont = True
        Me.chkTermTime.Size = New System.Drawing.Size(20, 19)
        Me.chkTermTime.TabIndex = 1
        Me.chkTermTime.Tag = "AEB"
        '
        'GroupControl14
        '
        Me.GroupControl14.Controls.Add(Me.CareLabel37)
        Me.GroupControl14.Controls.Add(Me.txtFundRef)
        Me.GroupControl14.Controls.Add(Me.CareLabel36)
        Me.GroupControl14.Controls.Add(Me.cbxFundType)
        Me.GroupControl14.Location = New System.Drawing.Point(393, 132)
        Me.GroupControl14.Name = "GroupControl14"
        Me.GroupControl14.Size = New System.Drawing.Size(409, 60)
        Me.GroupControl14.TabIndex = 4
        Me.GroupControl14.Text = "Funding Options"
        '
        'CareLabel37
        '
        Me.CareLabel37.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel37.Appearance.Options.UseFont = True
        Me.CareLabel37.Appearance.Options.UseTextOptions = True
        Me.CareLabel37.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel37.Location = New System.Drawing.Point(239, 32)
        Me.CareLabel37.Name = "CareLabel37"
        Me.CareLabel37.Size = New System.Drawing.Size(17, 15)
        Me.CareLabel37.TabIndex = 2
        Me.CareLabel37.Text = "Ref"
        Me.CareLabel37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFundRef
        '
        Me.txtFundRef.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFundRef.EnterMoveNextControl = True
        Me.txtFundRef.Location = New System.Drawing.Point(262, 29)
        Me.txtFundRef.MaxLength = 0
        Me.txtFundRef.Name = "txtFundRef"
        Me.txtFundRef.NumericAllowNegatives = False
        Me.txtFundRef.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtFundRef.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFundRef.Properties.AccessibleName = "Family"
        Me.txtFundRef.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFundRef.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtFundRef.Properties.Appearance.Options.UseFont = True
        Me.txtFundRef.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFundRef.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFundRef.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFundRef.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFundRef.Size = New System.Drawing.Size(139, 22)
        Me.txtFundRef.TabIndex = 3
        Me.txtFundRef.Tag = "AEB"
        Me.txtFundRef.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtFundRef.ToolTipText = ""
        '
        'CareLabel36
        '
        Me.CareLabel36.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel36.Appearance.Options.UseFont = True
        Me.CareLabel36.Appearance.Options.UseTextOptions = True
        Me.CareLabel36.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel36.Location = New System.Drawing.Point(8, 32)
        Me.CareLabel36.Name = "CareLabel36"
        Me.CareLabel36.Size = New System.Drawing.Size(26, 15)
        Me.CareLabel36.TabIndex = 0
        Me.CareLabel36.Text = "Type"
        Me.CareLabel36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxFundType
        '
        Me.cbxFundType.AllowBlank = False
        Me.cbxFundType.DataSource = Nothing
        Me.cbxFundType.DisplayMember = Nothing
        Me.cbxFundType.EnterMoveNextControl = True
        Me.cbxFundType.Location = New System.Drawing.Point(61, 29)
        Me.cbxFundType.Name = "cbxFundType"
        Me.cbxFundType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxFundType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxFundType.Properties.Appearance.Options.UseFont = True
        Me.cbxFundType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxFundType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxFundType.SelectedValue = Nothing
        Me.cbxFundType.Size = New System.Drawing.Size(163, 22)
        Me.cbxFundType.TabIndex = 1
        Me.cbxFundType.Tag = "AEB"
        Me.cbxFundType.ValueMember = Nothing
        '
        'GroupControl6
        '
        Me.GroupControl6.Controls.Add(Me.CareLabel18)
        Me.GroupControl6.Controls.Add(Me.CareLabel15)
        Me.GroupControl6.Controls.Add(Me.cbxNLCode)
        Me.GroupControl6.Controls.Add(Me.cbxNLTracking)
        Me.GroupControl6.Location = New System.Drawing.Point(393, 66)
        Me.GroupControl6.Name = "GroupControl6"
        Me.GroupControl6.Size = New System.Drawing.Size(409, 60)
        Me.GroupControl6.TabIndex = 3
        Me.GroupControl6.Text = "Nominal Override"
        '
        'CareLabel18
        '
        Me.CareLabel18.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel18.Appearance.Options.UseFont = True
        Me.CareLabel18.Appearance.Options.UseTextOptions = True
        Me.CareLabel18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel18.Location = New System.Drawing.Point(182, 32)
        Me.CareLabel18.Name = "CareLabel18"
        Me.CareLabel18.Size = New System.Drawing.Size(64, 15)
        Me.CareLabel18.TabIndex = 2
        Me.CareLabel18.Text = "NL Tracking"
        Me.CareLabel18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel15
        '
        Me.CareLabel15.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel15.Appearance.Options.UseFont = True
        Me.CareLabel15.Appearance.Options.UseTextOptions = True
        Me.CareLabel15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel15.Location = New System.Drawing.Point(9, 32)
        Me.CareLabel15.Name = "CareLabel15"
        Me.CareLabel15.Size = New System.Drawing.Size(46, 15)
        Me.CareLabel15.TabIndex = 0
        Me.CareLabel15.Text = "NL Code"
        Me.CareLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxNLCode
        '
        Me.cbxNLCode.AllowBlank = False
        Me.cbxNLCode.DataSource = Nothing
        Me.cbxNLCode.DisplayMember = Nothing
        Me.cbxNLCode.EnterMoveNextControl = True
        Me.cbxNLCode.Location = New System.Drawing.Point(61, 29)
        Me.cbxNLCode.Name = "cbxNLCode"
        Me.cbxNLCode.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxNLCode.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxNLCode.Properties.Appearance.Options.UseFont = True
        Me.cbxNLCode.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxNLCode.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxNLCode.SelectedValue = Nothing
        Me.cbxNLCode.Size = New System.Drawing.Size(106, 22)
        Me.cbxNLCode.TabIndex = 1
        Me.cbxNLCode.Tag = "AEB"
        Me.cbxNLCode.ValueMember = Nothing
        '
        'cbxNLTracking
        '
        Me.cbxNLTracking.AllowBlank = False
        Me.cbxNLTracking.DataSource = Nothing
        Me.cbxNLTracking.DisplayMember = Nothing
        Me.cbxNLTracking.EnterMoveNextControl = True
        Me.cbxNLTracking.Location = New System.Drawing.Point(255, 29)
        Me.cbxNLTracking.Name = "cbxNLTracking"
        Me.cbxNLTracking.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxNLTracking.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxNLTracking.Properties.Appearance.Options.UseFont = True
        Me.cbxNLTracking.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxNLTracking.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxNLTracking.SelectedValue = Nothing
        Me.cbxNLTracking.Size = New System.Drawing.Size(146, 22)
        Me.cbxNLTracking.TabIndex = 3
        Me.cbxNLTracking.Tag = "AEB"
        Me.cbxNLTracking.ValueMember = Nothing
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.CareLabel13)
        Me.GroupBox2.Controls.Add(Me.txtPaymentRef)
        Me.GroupBox2.Controls.Add(Me.CareLabel35)
        Me.GroupBox2.Controls.Add(Me.cbxPaymentDue)
        Me.GroupBox2.Controls.Add(Me.CareLabel39)
        Me.GroupBox2.Controls.Add(Me.cbxPaymentFreq)
        Me.GroupBox2.Controls.Add(Me.CareLabel38)
        Me.GroupBox2.Controls.Add(Me.cbxPaymentMethod)
        Me.GroupBox2.Controls.Add(Me.Label30)
        Me.GroupBox2.Controls.Add(Me.cbxDiscount)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(384, 189)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.Text = "Invoicing Settings"
        '
        'CareLabel13
        '
        Me.CareLabel13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel13.Appearance.Options.UseFont = True
        Me.CareLabel13.Appearance.Options.UseTextOptions = True
        Me.CareLabel13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel13.Location = New System.Drawing.Point(8, 160)
        Me.CareLabel13.Name = "CareLabel13"
        Me.CareLabel13.Size = New System.Drawing.Size(52, 15)
        Me.CareLabel13.TabIndex = 9
        Me.CareLabel13.Text = "Reference"
        Me.CareLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPaymentRef
        '
        Me.txtPaymentRef.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPaymentRef.EnterMoveNextControl = True
        Me.txtPaymentRef.Location = New System.Drawing.Point(110, 157)
        Me.txtPaymentRef.MaxLength = 0
        Me.txtPaymentRef.Name = "txtPaymentRef"
        Me.txtPaymentRef.NumericAllowNegatives = False
        Me.txtPaymentRef.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtPaymentRef.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPaymentRef.Properties.AccessibleName = "Family"
        Me.txtPaymentRef.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPaymentRef.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtPaymentRef.Properties.Appearance.Options.UseFont = True
        Me.txtPaymentRef.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPaymentRef.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPaymentRef.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPaymentRef.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPaymentRef.Size = New System.Drawing.Size(265, 22)
        Me.txtPaymentRef.TabIndex = 8
        Me.txtPaymentRef.Tag = "AEB"
        Me.txtPaymentRef.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPaymentRef.ToolTipText = ""
        '
        'CareLabel35
        '
        Me.CareLabel35.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel35.Appearance.Options.UseFont = True
        Me.CareLabel35.Appearance.Options.UseTextOptions = True
        Me.CareLabel35.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel35.Location = New System.Drawing.Point(8, 67)
        Me.CareLabel35.Name = "CareLabel35"
        Me.CareLabel35.Size = New System.Drawing.Size(62, 15)
        Me.CareLabel35.TabIndex = 2
        Me.CareLabel35.Text = "Invoice Due"
        Me.CareLabel35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxPaymentDue
        '
        Me.cbxPaymentDue.AllowBlank = False
        Me.cbxPaymentDue.DataSource = Nothing
        Me.cbxPaymentDue.DisplayMember = Nothing
        Me.cbxPaymentDue.EnterMoveNextControl = True
        Me.cbxPaymentDue.Location = New System.Drawing.Point(110, 64)
        Me.cbxPaymentDue.Name = "cbxPaymentDue"
        Me.cbxPaymentDue.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxPaymentDue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxPaymentDue.Properties.Appearance.Options.UseFont = True
        Me.cbxPaymentDue.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxPaymentDue.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxPaymentDue.SelectedValue = Nothing
        Me.cbxPaymentDue.Size = New System.Drawing.Size(265, 22)
        Me.cbxPaymentDue.TabIndex = 3
        Me.cbxPaymentDue.Tag = "AEB"
        Me.cbxPaymentDue.ValueMember = Nothing
        '
        'CareLabel39
        '
        Me.CareLabel39.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel39.Appearance.Options.UseFont = True
        Me.CareLabel39.Appearance.Options.UseTextOptions = True
        Me.CareLabel39.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel39.Location = New System.Drawing.Point(8, 95)
        Me.CareLabel39.Name = "CareLabel39"
        Me.CareLabel39.Size = New System.Drawing.Size(96, 15)
        Me.CareLabel39.TabIndex = 4
        Me.CareLabel39.Text = "Invoice Frequency"
        Me.CareLabel39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxPaymentFreq
        '
        Me.cbxPaymentFreq.AllowBlank = False
        Me.cbxPaymentFreq.DataSource = Nothing
        Me.cbxPaymentFreq.DisplayMember = Nothing
        Me.cbxPaymentFreq.EnterMoveNextControl = True
        Me.cbxPaymentFreq.Location = New System.Drawing.Point(110, 92)
        Me.cbxPaymentFreq.Name = "cbxPaymentFreq"
        Me.cbxPaymentFreq.Properties.AccessibleName = "Group"
        Me.cbxPaymentFreq.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxPaymentFreq.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxPaymentFreq.Properties.Appearance.Options.UseFont = True
        Me.cbxPaymentFreq.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxPaymentFreq.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxPaymentFreq.SelectedValue = Nothing
        Me.cbxPaymentFreq.Size = New System.Drawing.Size(265, 22)
        Me.cbxPaymentFreq.TabIndex = 5
        Me.cbxPaymentFreq.Tag = "AEB"
        Me.cbxPaymentFreq.ValueMember = Nothing
        '
        'CareLabel38
        '
        Me.CareLabel38.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel38.Appearance.Options.UseFont = True
        Me.CareLabel38.Appearance.Options.UseTextOptions = True
        Me.CareLabel38.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel38.Location = New System.Drawing.Point(8, 132)
        Me.CareLabel38.Name = "CareLabel38"
        Me.CareLabel38.Size = New System.Drawing.Size(64, 15)
        Me.CareLabel38.TabIndex = 6
        Me.CareLabel38.Text = "Pay Method"
        Me.CareLabel38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxPaymentMethod
        '
        Me.cbxPaymentMethod.AllowBlank = False
        Me.cbxPaymentMethod.DataSource = Nothing
        Me.cbxPaymentMethod.DisplayMember = Nothing
        Me.cbxPaymentMethod.EnterMoveNextControl = True
        Me.cbxPaymentMethod.Location = New System.Drawing.Point(110, 129)
        Me.cbxPaymentMethod.Name = "cbxPaymentMethod"
        Me.cbxPaymentMethod.Properties.AccessibleName = "Group"
        Me.cbxPaymentMethod.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxPaymentMethod.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxPaymentMethod.Properties.Appearance.Options.UseFont = True
        Me.cbxPaymentMethod.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxPaymentMethod.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxPaymentMethod.SelectedValue = Nothing
        Me.cbxPaymentMethod.Size = New System.Drawing.Size(265, 22)
        Me.cbxPaymentMethod.TabIndex = 7
        Me.cbxPaymentMethod.Tag = "AEB"
        Me.cbxPaymentMethod.ValueMember = Nothing
        '
        'Label30
        '
        Me.Label30.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Appearance.Options.UseFont = True
        Me.Label30.Appearance.Options.UseTextOptions = True
        Me.Label30.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label30.Location = New System.Drawing.Point(8, 30)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(47, 15)
        Me.Label30.TabIndex = 0
        Me.Label30.Text = "Discount"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxDiscount
        '
        Me.cbxDiscount.AllowBlank = False
        Me.cbxDiscount.DataSource = Nothing
        Me.cbxDiscount.DisplayMember = Nothing
        Me.cbxDiscount.EnterMoveNextControl = True
        Me.cbxDiscount.Location = New System.Drawing.Point(110, 27)
        Me.cbxDiscount.Name = "cbxDiscount"
        Me.cbxDiscount.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxDiscount.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxDiscount.Properties.Appearance.Options.UseFont = True
        Me.cbxDiscount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxDiscount.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxDiscount.SelectedValue = Nothing
        Me.cbxDiscount.Size = New System.Drawing.Size(265, 22)
        Me.cbxDiscount.TabIndex = 1
        Me.cbxDiscount.Tag = "AEB"
        Me.cbxDiscount.ValueMember = Nothing
        '
        'tabSessAnnual
        '
        Me.tabSessAnnual.Controls.Add(Me.GroupControl4)
        Me.tabSessAnnual.Name = "tabSessAnnual"
        Me.tabSessAnnual.Size = New System.Drawing.Size(807, 414)
        Me.tabSessAnnual.Text = "Annual Calculations"
        '
        'GroupControl4
        '
        Me.GroupControl4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl4.Controls.Add(Me.radCalcAnnual)
        Me.GroupControl4.Controls.Add(Me.radCalcMonthly)
        Me.GroupControl4.Controls.Add(Me.radCalcWeekly)
        Me.GroupControl4.Controls.Add(Me.radAnnualPatterns)
        Me.GroupControl4.Controls.Add(Me.radAnnualManual)
        Me.GroupControl4.Controls.Add(Me.grdAnnual)
        Me.GroupControl4.Location = New System.Drawing.Point(3, 3)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.ShowCaption = False
        Me.GroupControl4.Size = New System.Drawing.Size(801, 408)
        Me.GroupControl4.TabIndex = 5
        '
        'radCalcAnnual
        '
        Me.radCalcAnnual.EditValue = True
        Me.radCalcAnnual.Location = New System.Drawing.Point(701, 5)
        Me.radCalcAnnual.Name = "radCalcAnnual"
        Me.radCalcAnnual.Properties.AutoWidth = True
        Me.radCalcAnnual.Properties.Caption = "Annual Amount"
        Me.radCalcAnnual.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radCalcAnnual.Properties.RadioGroupIndex = 1
        Me.radCalcAnnual.Size = New System.Drawing.Size(95, 19)
        Me.radCalcAnnual.TabIndex = 12
        '
        'radCalcMonthly
        '
        Me.radCalcMonthly.Location = New System.Drawing.Point(595, 5)
        Me.radCalcMonthly.Name = "radCalcMonthly"
        Me.radCalcMonthly.Properties.AutoWidth = True
        Me.radCalcMonthly.Properties.Caption = "Monthly Amount"
        Me.radCalcMonthly.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radCalcMonthly.Properties.RadioGroupIndex = 1
        Me.radCalcMonthly.Size = New System.Drawing.Size(100, 19)
        Me.radCalcMonthly.TabIndex = 10
        Me.radCalcMonthly.TabStop = False
        '
        'radCalcWeekly
        '
        Me.radCalcWeekly.Location = New System.Drawing.Point(492, 5)
        Me.radCalcWeekly.Name = "radCalcWeekly"
        Me.radCalcWeekly.Properties.AutoWidth = True
        Me.radCalcWeekly.Properties.Caption = "Weekly Amount"
        Me.radCalcWeekly.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radCalcWeekly.Properties.RadioGroupIndex = 1
        Me.radCalcWeekly.Size = New System.Drawing.Size(97, 19)
        Me.radCalcWeekly.TabIndex = 11
        Me.radCalcWeekly.TabStop = False
        Me.radCalcWeekly.Visible = False
        '
        'radAnnualPatterns
        '
        Me.radAnnualPatterns.EditValue = True
        Me.radAnnualPatterns.Location = New System.Drawing.Point(5, 5)
        Me.radAnnualPatterns.Name = "radAnnualPatterns"
        Me.radAnnualPatterns.Properties.AutoWidth = True
        Me.radAnnualPatterns.Properties.Caption = "Derived from Patterns"
        Me.radAnnualPatterns.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radAnnualPatterns.Properties.RadioGroupIndex = 0
        Me.radAnnualPatterns.Size = New System.Drawing.Size(128, 19)
        Me.radAnnualPatterns.TabIndex = 0
        '
        'radAnnualManual
        '
        Me.radAnnualManual.Location = New System.Drawing.Point(139, 5)
        Me.radAnnualManual.Name = "radAnnualManual"
        Me.radAnnualManual.Properties.AutoWidth = True
        Me.radAnnualManual.Properties.Caption = "Manual"
        Me.radAnnualManual.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radAnnualManual.Properties.RadioGroupIndex = 0
        Me.radAnnualManual.Size = New System.Drawing.Size(56, 19)
        Me.radAnnualManual.TabIndex = 1
        Me.radAnnualManual.TabStop = False
        '
        'grdAnnual
        '
        Me.grdAnnual.AllowBuildColumns = True
        Me.grdAnnual.AllowEdit = False
        Me.grdAnnual.AllowHorizontalScroll = False
        Me.grdAnnual.AllowMultiSelect = False
        Me.grdAnnual.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdAnnual.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdAnnual.Appearance.Options.UseFont = True
        Me.grdAnnual.AutoSizeByData = True
        Me.grdAnnual.DisableAutoSize = False
        Me.grdAnnual.DisableDataFormatting = False
        Me.grdAnnual.FocusedRowHandle = -2147483648
        Me.grdAnnual.HideFirstColumn = False
        Me.grdAnnual.Location = New System.Drawing.Point(5, 30)
        Me.grdAnnual.Name = "grdAnnual"
        Me.grdAnnual.PreviewColumn = ""
        Me.grdAnnual.QueryID = Nothing
        Me.grdAnnual.RowAutoHeight = False
        Me.grdAnnual.SearchAsYouType = True
        Me.grdAnnual.ShowAutoFilterRow = False
        Me.grdAnnual.ShowFindPanel = False
        Me.grdAnnual.ShowGroupByBox = False
        Me.grdAnnual.ShowLoadingPanel = False
        Me.grdAnnual.ShowNavigator = False
        Me.grdAnnual.Size = New System.Drawing.Size(791, 373)
        Me.grdAnnual.TabIndex = 7
        Me.grdAnnual.TabStop = False
        '
        'tabSessMonthly
        '
        Me.tabSessMonthly.Controls.Add(Me.grdSessMonthly)
        Me.tabSessMonthly.Name = "tabSessMonthly"
        Me.tabSessMonthly.Size = New System.Drawing.Size(807, 414)
        Me.tabSessMonthly.Text = "Monthly Overrides"
        '
        'grdSessMonthly
        '
        Me.grdSessMonthly.AllowBuildColumns = True
        Me.grdSessMonthly.AllowEdit = False
        Me.grdSessMonthly.AllowHorizontalScroll = False
        Me.grdSessMonthly.AllowMultiSelect = False
        Me.grdSessMonthly.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdSessMonthly.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdSessMonthly.Appearance.Options.UseFont = True
        Me.grdSessMonthly.AutoSizeByData = True
        Me.grdSessMonthly.DisableAutoSize = False
        Me.grdSessMonthly.DisableDataFormatting = False
        Me.grdSessMonthly.FocusedRowHandle = -2147483648
        Me.grdSessMonthly.HideFirstColumn = False
        Me.grdSessMonthly.Location = New System.Drawing.Point(3, 3)
        Me.grdSessMonthly.Name = "grdSessMonthly"
        Me.grdSessMonthly.PreviewColumn = ""
        Me.grdSessMonthly.QueryID = Nothing
        Me.grdSessMonthly.RowAutoHeight = False
        Me.grdSessMonthly.SearchAsYouType = True
        Me.grdSessMonthly.ShowAutoFilterRow = False
        Me.grdSessMonthly.ShowFindPanel = False
        Me.grdSessMonthly.ShowGroupByBox = False
        Me.grdSessMonthly.ShowLoadingPanel = False
        Me.grdSessMonthly.ShowNavigator = False
        Me.grdSessMonthly.Size = New System.Drawing.Size(801, 408)
        Me.grdSessMonthly.TabIndex = 10
        Me.grdSessMonthly.TabStop = False
        '
        'tabSessHolidays
        '
        Me.tabSessHolidays.Controls.Add(Me.CareFrame3)
        Me.tabSessHolidays.Name = "tabSessHolidays"
        Me.tabSessHolidays.Size = New System.Drawing.Size(807, 414)
        Me.tabSessHolidays.Text = "Absence && Holidays"
        '
        'CareFrame3
        '
        Me.CareFrame3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CareFrame3.Controls.Add(Me.radHolidaysAll)
        Me.CareFrame3.Controls.Add(Me.radHolidays)
        Me.CareFrame3.Controls.Add(Me.radAbsence)
        Me.CareFrame3.Controls.Add(Me.grdHolidays)
        Me.CareFrame3.Location = New System.Drawing.Point(3, 3)
        Me.CareFrame3.Name = "CareFrame3"
        Me.CareFrame3.ShowCaption = False
        Me.CareFrame3.Size = New System.Drawing.Size(801, 408)
        Me.CareFrame3.TabIndex = 6
        '
        'radHolidaysAll
        '
        Me.radHolidaysAll.EditValue = True
        Me.radHolidaysAll.Location = New System.Drawing.Point(5, 5)
        Me.radHolidaysAll.Name = "radHolidaysAll"
        Me.radHolidaysAll.Properties.AutoWidth = True
        Me.radHolidaysAll.Properties.Caption = "All"
        Me.radHolidaysAll.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radHolidaysAll.Properties.RadioGroupIndex = 0
        Me.radHolidaysAll.Size = New System.Drawing.Size(33, 19)
        Me.radHolidaysAll.TabIndex = 9
        '
        'radHolidays
        '
        Me.radHolidays.Location = New System.Drawing.Point(44, 5)
        Me.radHolidays.Name = "radHolidays"
        Me.radHolidays.Properties.AutoWidth = True
        Me.radHolidays.Properties.Caption = "Holidays"
        Me.radHolidays.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radHolidays.Properties.RadioGroupIndex = 0
        Me.radHolidays.Size = New System.Drawing.Size(62, 19)
        Me.radHolidays.TabIndex = 0
        Me.radHolidays.TabStop = False
        '
        'radAbsence
        '
        Me.radAbsence.Location = New System.Drawing.Point(112, 5)
        Me.radAbsence.Name = "radAbsence"
        Me.radAbsence.Properties.AutoWidth = True
        Me.radAbsence.Properties.Caption = "Absence"
        Me.radAbsence.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radAbsence.Properties.RadioGroupIndex = 0
        Me.radAbsence.Size = New System.Drawing.Size(63, 19)
        Me.radAbsence.TabIndex = 1
        Me.radAbsence.TabStop = False
        '
        'grdHolidays
        '
        Me.grdHolidays.AllowBuildColumns = True
        Me.grdHolidays.AllowEdit = False
        Me.grdHolidays.AllowHorizontalScroll = False
        Me.grdHolidays.AllowMultiSelect = False
        Me.grdHolidays.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdHolidays.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdHolidays.Appearance.Options.UseFont = True
        Me.grdHolidays.AutoSizeByData = True
        Me.grdHolidays.DisableAutoSize = False
        Me.grdHolidays.DisableDataFormatting = False
        Me.grdHolidays.FocusedRowHandle = -2147483648
        Me.grdHolidays.HideFirstColumn = False
        Me.grdHolidays.Location = New System.Drawing.Point(5, 30)
        Me.grdHolidays.Name = "grdHolidays"
        Me.grdHolidays.PreviewColumn = ""
        Me.grdHolidays.QueryID = Nothing
        Me.grdHolidays.RowAutoHeight = False
        Me.grdHolidays.SearchAsYouType = True
        Me.grdHolidays.ShowAutoFilterRow = False
        Me.grdHolidays.ShowFindPanel = False
        Me.grdHolidays.ShowGroupByBox = False
        Me.grdHolidays.ShowLoadingPanel = False
        Me.grdHolidays.ShowNavigator = False
        Me.grdHolidays.Size = New System.Drawing.Size(791, 373)
        Me.grdHolidays.TabIndex = 7
        Me.grdHolidays.TabStop = False
        '
        'tabSessOther
        '
        Me.tabSessOther.Controls.Add(Me.GroupControl8)
        Me.tabSessOther.Name = "tabSessOther"
        Me.tabSessOther.Size = New System.Drawing.Size(807, 414)
        Me.tabSessOther.Text = "Additional Fees"
        '
        'GroupControl8
        '
        Me.GroupControl8.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl8.Controls.Add(Me.radAddAll)
        Me.GroupControl8.Controls.Add(Me.radAddMonthly)
        Me.GroupControl8.Controls.Add(Me.radAddOneOff)
        Me.GroupControl8.Controls.Add(Me.radAddWeekly)
        Me.GroupControl8.Controls.Add(Me.grdAdditional)
        Me.GroupControl8.Location = New System.Drawing.Point(3, 3)
        Me.GroupControl8.Name = "GroupControl8"
        Me.GroupControl8.ShowCaption = False
        Me.GroupControl8.Size = New System.Drawing.Size(801, 408)
        Me.GroupControl8.TabIndex = 5
        '
        'radAddAll
        '
        Me.radAddAll.EditValue = True
        Me.radAddAll.Location = New System.Drawing.Point(5, 5)
        Me.radAddAll.Name = "radAddAll"
        Me.radAddAll.Properties.AutoWidth = True
        Me.radAddAll.Properties.Caption = "All"
        Me.radAddAll.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radAddAll.Properties.RadioGroupIndex = 0
        Me.radAddAll.Size = New System.Drawing.Size(33, 19)
        Me.radAddAll.TabIndex = 9
        '
        'radAddMonthly
        '
        Me.radAddMonthly.Location = New System.Drawing.Point(310, 5)
        Me.radAddMonthly.Name = "radAddMonthly"
        Me.radAddMonthly.Properties.AutoWidth = True
        Me.radAddMonthly.Properties.Caption = "Recurring Monthly Charges"
        Me.radAddMonthly.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radAddMonthly.Properties.RadioGroupIndex = 0
        Me.radAddMonthly.Size = New System.Drawing.Size(152, 19)
        Me.radAddMonthly.TabIndex = 8
        Me.radAddMonthly.TabStop = False
        Me.radAddMonthly.Visible = False
        '
        'radAddOneOff
        '
        Me.radAddOneOff.Location = New System.Drawing.Point(44, 5)
        Me.radAddOneOff.Name = "radAddOneOff"
        Me.radAddOneOff.Properties.AutoWidth = True
        Me.radAddOneOff.Properties.Caption = "One-Off Charges"
        Me.radAddOneOff.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radAddOneOff.Properties.RadioGroupIndex = 0
        Me.radAddOneOff.Size = New System.Drawing.Size(105, 19)
        Me.radAddOneOff.TabIndex = 0
        Me.radAddOneOff.TabStop = False
        '
        'radAddWeekly
        '
        Me.radAddWeekly.Location = New System.Drawing.Point(155, 5)
        Me.radAddWeekly.Name = "radAddWeekly"
        Me.radAddWeekly.Properties.AutoWidth = True
        Me.radAddWeekly.Properties.Caption = "Recurring Weekly Charges"
        Me.radAddWeekly.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radAddWeekly.Properties.RadioGroupIndex = 0
        Me.radAddWeekly.Size = New System.Drawing.Size(149, 19)
        Me.radAddWeekly.TabIndex = 1
        Me.radAddWeekly.TabStop = False
        Me.radAddWeekly.Visible = False
        '
        'grdAdditional
        '
        Me.grdAdditional.AllowBuildColumns = True
        Me.grdAdditional.AllowEdit = False
        Me.grdAdditional.AllowHorizontalScroll = False
        Me.grdAdditional.AllowMultiSelect = False
        Me.grdAdditional.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdAdditional.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdAdditional.Appearance.Options.UseFont = True
        Me.grdAdditional.AutoSizeByData = True
        Me.grdAdditional.DisableAutoSize = False
        Me.grdAdditional.DisableDataFormatting = False
        Me.grdAdditional.FocusedRowHandle = -2147483648
        Me.grdAdditional.HideFirstColumn = False
        Me.grdAdditional.Location = New System.Drawing.Point(5, 30)
        Me.grdAdditional.Name = "grdAdditional"
        Me.grdAdditional.PreviewColumn = ""
        Me.grdAdditional.QueryID = Nothing
        Me.grdAdditional.RowAutoHeight = False
        Me.grdAdditional.SearchAsYouType = True
        Me.grdAdditional.ShowAutoFilterRow = False
        Me.grdAdditional.ShowFindPanel = False
        Me.grdAdditional.ShowGroupByBox = False
        Me.grdAdditional.ShowLoadingPanel = False
        Me.grdAdditional.ShowNavigator = False
        Me.grdAdditional.Size = New System.Drawing.Size(791, 373)
        Me.grdAdditional.TabIndex = 7
        Me.grdAdditional.TabStop = False
        '
        'tabSessDeposits
        '
        Me.tabSessDeposits.Controls.Add(Me.CareFrame2)
        Me.tabSessDeposits.Name = "tabSessDeposits"
        Me.tabSessDeposits.Size = New System.Drawing.Size(807, 414)
        Me.tabSessDeposits.Text = "Deposits"
        '
        'CareFrame2
        '
        Me.CareFrame2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CareFrame2.Controls.Add(Me.lblDepositBalance)
        Me.CareFrame2.Controls.Add(Me.grdDeposits)
        Me.CareFrame2.Location = New System.Drawing.Point(3, 3)
        Me.CareFrame2.Name = "CareFrame2"
        Me.CareFrame2.ShowCaption = False
        Me.CareFrame2.Size = New System.Drawing.Size(801, 408)
        Me.CareFrame2.TabIndex = 6
        '
        'lblDepositBalance
        '
        Me.lblDepositBalance.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDepositBalance.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepositBalance.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblDepositBalance.Appearance.Options.UseFont = True
        Me.lblDepositBalance.Appearance.Options.UseForeColor = True
        Me.lblDepositBalance.Appearance.Options.UseTextOptions = True
        Me.lblDepositBalance.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblDepositBalance.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblDepositBalance.Location = New System.Drawing.Point(311, 9)
        Me.lblDepositBalance.Name = "lblDepositBalance"
        Me.lblDepositBalance.Size = New System.Drawing.Size(485, 15)
        Me.lblDepositBalance.TabIndex = 13
        Me.lblDepositBalance.Text = "Deposit Balance 0.00"
        Me.lblDepositBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grdDeposits
        '
        Me.grdDeposits.AllowBuildColumns = True
        Me.grdDeposits.AllowEdit = False
        Me.grdDeposits.AllowHorizontalScroll = False
        Me.grdDeposits.AllowMultiSelect = False
        Me.grdDeposits.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdDeposits.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdDeposits.Appearance.Options.UseFont = True
        Me.grdDeposits.AutoSizeByData = True
        Me.grdDeposits.DisableAutoSize = False
        Me.grdDeposits.DisableDataFormatting = False
        Me.grdDeposits.FocusedRowHandle = -2147483648
        Me.grdDeposits.HideFirstColumn = False
        Me.grdDeposits.Location = New System.Drawing.Point(5, 30)
        Me.grdDeposits.Name = "grdDeposits"
        Me.grdDeposits.PreviewColumn = ""
        Me.grdDeposits.QueryID = Nothing
        Me.grdDeposits.RowAutoHeight = False
        Me.grdDeposits.SearchAsYouType = True
        Me.grdDeposits.ShowAutoFilterRow = False
        Me.grdDeposits.ShowFindPanel = False
        Me.grdDeposits.ShowGroupByBox = False
        Me.grdDeposits.ShowLoadingPanel = False
        Me.grdDeposits.ShowNavigator = False
        Me.grdDeposits.Size = New System.Drawing.Size(791, 373)
        Me.grdDeposits.TabIndex = 7
        Me.grdDeposits.TabStop = False
        '
        'tabSessDiscounts
        '
        Me.tabSessDiscounts.Controls.Add(Me.grdDiscounts)
        Me.tabSessDiscounts.Name = "tabSessDiscounts"
        Me.tabSessDiscounts.PageVisible = False
        Me.tabSessDiscounts.Size = New System.Drawing.Size(807, 414)
        Me.tabSessDiscounts.Text = "Discounts"
        '
        'grdDiscounts
        '
        Me.grdDiscounts.AllowBuildColumns = True
        Me.grdDiscounts.AllowEdit = False
        Me.grdDiscounts.AllowHorizontalScroll = False
        Me.grdDiscounts.AllowMultiSelect = False
        Me.grdDiscounts.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdDiscounts.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdDiscounts.Appearance.Options.UseFont = True
        Me.grdDiscounts.AutoSizeByData = True
        Me.grdDiscounts.DisableAutoSize = False
        Me.grdDiscounts.DisableDataFormatting = False
        Me.grdDiscounts.FocusedRowHandle = -2147483648
        Me.grdDiscounts.HideFirstColumn = False
        Me.grdDiscounts.Location = New System.Drawing.Point(3, 3)
        Me.grdDiscounts.Name = "grdDiscounts"
        Me.grdDiscounts.PreviewColumn = ""
        Me.grdDiscounts.QueryID = Nothing
        Me.grdDiscounts.RowAutoHeight = False
        Me.grdDiscounts.SearchAsYouType = True
        Me.grdDiscounts.ShowAutoFilterRow = False
        Me.grdDiscounts.ShowFindPanel = False
        Me.grdDiscounts.ShowGroupByBox = False
        Me.grdDiscounts.ShowLoadingPanel = False
        Me.grdDiscounts.ShowNavigator = False
        Me.grdDiscounts.Size = New System.Drawing.Size(801, 408)
        Me.grdDiscounts.TabIndex = 10
        Me.grdDiscounts.TabStop = False
        '
        'tabInvoices
        '
        Me.tabInvoices.Controls.Add(Me.CareFrame1)
        Me.tabInvoices.Name = "tabInvoices"
        Me.tabInvoices.Size = New System.Drawing.Size(807, 414)
        Me.tabInvoices.Text = "Invoices && Credits"
        '
        'CareFrame1
        '
        Me.CareFrame1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CareFrame1.Controls.Add(Me.radInvoicesPosted)
        Me.CareFrame1.Controls.Add(Me.radNonPostedCredits)
        Me.CareFrame1.Controls.Add(Me.radInvoices)
        Me.CareFrame1.Controls.Add(Me.radPostedCredits)
        Me.CareFrame1.Controls.Add(Me.cgInvoices)
        Me.CareFrame1.Location = New System.Drawing.Point(3, 3)
        Me.CareFrame1.Name = "CareFrame1"
        Me.CareFrame1.ShowCaption = False
        Me.CareFrame1.Size = New System.Drawing.Size(801, 408)
        Me.CareFrame1.TabIndex = 2
        '
        'radInvoicesPosted
        '
        Me.radInvoicesPosted.EditValue = True
        Me.radInvoicesPosted.Location = New System.Drawing.Point(5, 5)
        Me.radInvoicesPosted.Name = "radInvoicesPosted"
        Me.radInvoicesPosted.Properties.AutoWidth = True
        Me.radInvoicesPosted.Properties.Caption = "Posted Invoices"
        Me.radInvoicesPosted.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radInvoicesPosted.Properties.RadioGroupIndex = 0
        Me.radInvoicesPosted.Size = New System.Drawing.Size(98, 19)
        Me.radInvoicesPosted.TabIndex = 0
        '
        'radNonPostedCredits
        '
        Me.radNonPostedCredits.Location = New System.Drawing.Point(334, 5)
        Me.radNonPostedCredits.Name = "radNonPostedCredits"
        Me.radNonPostedCredits.Properties.AutoWidth = True
        Me.radNonPostedCredits.Properties.Caption = "Non-Posted Credits"
        Me.radNonPostedCredits.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radNonPostedCredits.Properties.RadioGroupIndex = 0
        Me.radNonPostedCredits.Size = New System.Drawing.Size(115, 19)
        Me.radNonPostedCredits.TabIndex = 3
        Me.radNonPostedCredits.TabStop = False
        '
        'radInvoices
        '
        Me.radInvoices.Location = New System.Drawing.Point(109, 5)
        Me.radInvoices.Name = "radInvoices"
        Me.radInvoices.Properties.AutoWidth = True
        Me.radInvoices.Properties.Caption = "Non-Posted Invoices"
        Me.radInvoices.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radInvoices.Properties.RadioGroupIndex = 0
        Me.radInvoices.Size = New System.Drawing.Size(121, 19)
        Me.radInvoices.TabIndex = 1
        Me.radInvoices.TabStop = False
        '
        'radPostedCredits
        '
        Me.radPostedCredits.Location = New System.Drawing.Point(236, 5)
        Me.radPostedCredits.Name = "radPostedCredits"
        Me.radPostedCredits.Properties.AutoWidth = True
        Me.radPostedCredits.Properties.Caption = "Posted Credits"
        Me.radPostedCredits.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radPostedCredits.Properties.RadioGroupIndex = 0
        Me.radPostedCredits.Size = New System.Drawing.Size(92, 19)
        Me.radPostedCredits.TabIndex = 2
        Me.radPostedCredits.TabStop = False
        '
        'cgInvoices
        '
        Me.cgInvoices.AllowBuildColumns = True
        Me.cgInvoices.AllowEdit = False
        Me.cgInvoices.AllowHorizontalScroll = False
        Me.cgInvoices.AllowMultiSelect = False
        Me.cgInvoices.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgInvoices.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgInvoices.Appearance.Options.UseFont = True
        Me.cgInvoices.AutoSizeByData = True
        Me.cgInvoices.DisableAutoSize = False
        Me.cgInvoices.DisableDataFormatting = False
        Me.cgInvoices.FocusedRowHandle = -2147483648
        Me.cgInvoices.HideFirstColumn = False
        Me.cgInvoices.Location = New System.Drawing.Point(5, 30)
        Me.cgInvoices.Name = "cgInvoices"
        Me.cgInvoices.PreviewColumn = ""
        Me.cgInvoices.QueryID = Nothing
        Me.cgInvoices.RowAutoHeight = False
        Me.cgInvoices.SearchAsYouType = True
        Me.cgInvoices.ShowAutoFilterRow = False
        Me.cgInvoices.ShowFindPanel = False
        Me.cgInvoices.ShowGroupByBox = False
        Me.cgInvoices.ShowLoadingPanel = False
        Me.cgInvoices.ShowNavigator = False
        Me.cgInvoices.Size = New System.Drawing.Size(791, 373)
        Me.cgInvoices.TabIndex = 6
        Me.cgInvoices.TabStop = False
        '
        'lblHolidays
        '
        Me.lblHolidays.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblHolidays.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHolidays.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblHolidays.Appearance.Options.UseFont = True
        Me.lblHolidays.Appearance.Options.UseForeColor = True
        Me.lblHolidays.Appearance.Options.UseTextOptions = True
        Me.lblHolidays.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblHolidays.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblHolidays.Location = New System.Drawing.Point(337, 455)
        Me.lblHolidays.Name = "lblHolidays"
        Me.lblHolidays.Size = New System.Drawing.Size(485, 15)
        Me.lblHolidays.TabIndex = 12
        Me.lblHolidays.Text = "x holidays claimed this Calendar Year"
        Me.lblHolidays.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblHolidays.Visible = False
        '
        'btnAdvAdd
        '
        Me.btnAdvAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAdvAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnAdvAdd.Appearance.Options.UseFont = True
        Me.btnAdvAdd.Location = New System.Drawing.Point(9, 451)
        Me.btnAdvAdd.Name = "btnAdvAdd"
        Me.btnAdvAdd.Size = New System.Drawing.Size(75, 23)
        Me.btnAdvAdd.TabIndex = 9
        Me.btnAdvAdd.Text = "Add"
        '
        'btnAdvEdit
        '
        Me.btnAdvEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAdvEdit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnAdvEdit.Appearance.Options.UseFont = True
        Me.btnAdvEdit.Location = New System.Drawing.Point(90, 451)
        Me.btnAdvEdit.Name = "btnAdvEdit"
        Me.btnAdvEdit.Size = New System.Drawing.Size(75, 23)
        Me.btnAdvEdit.TabIndex = 10
        Me.btnAdvEdit.Text = "Edit"
        '
        'btnAdvRemove
        '
        Me.btnAdvRemove.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAdvRemove.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnAdvRemove.Appearance.Options.UseFont = True
        Me.btnAdvRemove.Location = New System.Drawing.Point(171, 451)
        Me.btnAdvRemove.Name = "btnAdvRemove"
        Me.btnAdvRemove.Size = New System.Drawing.Size(75, 23)
        Me.btnAdvRemove.TabIndex = 11
        Me.btnAdvRemove.Text = "Remove"
        '
        'tabCalendar
        '
        Me.tabCalendar.Controls.Add(Me.GroupControl5)
        Me.tabCalendar.Controls.Add(Me.yvBookings)
        Me.tabCalendar.Controls.Add(Me.cgBookings)
        Me.tabCalendar.Name = "tabCalendar"
        Me.tabCalendar.Size = New System.Drawing.Size(832, 481)
        Me.tabCalendar.Text = "Bookings && Register"
        '
        'GroupControl5
        '
        Me.GroupControl5.Controls.Add(Me.radBookCalendar)
        Me.GroupControl5.Controls.Add(Me.radBookRegister)
        Me.GroupControl5.Controls.Add(Me.radBookActual)
        Me.GroupControl5.Controls.Add(Me.btnBuildBookings)
        Me.GroupControl5.Location = New System.Drawing.Point(9, 9)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.ShowCaption = False
        Me.GroupControl5.Size = New System.Drawing.Size(814, 33)
        Me.GroupControl5.TabIndex = 0
        '
        'radBookCalendar
        '
        Me.radBookCalendar.EditValue = True
        Me.radBookCalendar.Location = New System.Drawing.Point(14, 7)
        Me.radBookCalendar.Name = "radBookCalendar"
        Me.radBookCalendar.Properties.AutoWidth = True
        Me.radBookCalendar.Properties.Caption = "Bookings Annual Calendar"
        Me.radBookCalendar.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radBookCalendar.Properties.RadioGroupIndex = 0
        Me.radBookCalendar.Size = New System.Drawing.Size(146, 19)
        Me.radBookCalendar.TabIndex = 0
        '
        'radBookRegister
        '
        Me.radBookRegister.Location = New System.Drawing.Point(269, 7)
        Me.radBookRegister.Name = "radBookRegister"
        Me.radBookRegister.Properties.AutoWidth = True
        Me.radBookRegister.Properties.Caption = "Register"
        Me.radBookRegister.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radBookRegister.Properties.RadioGroupIndex = 0
        Me.radBookRegister.Size = New System.Drawing.Size(62, 19)
        Me.radBookRegister.TabIndex = 2
        Me.radBookRegister.TabStop = False
        '
        'radBookActual
        '
        Me.radBookActual.Location = New System.Drawing.Point(166, 7)
        Me.radBookActual.Name = "radBookActual"
        Me.radBookActual.Properties.AutoWidth = True
        Me.radBookActual.Properties.Caption = "Actual Bookings"
        Me.radBookActual.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radBookActual.Properties.RadioGroupIndex = 0
        Me.radBookActual.Size = New System.Drawing.Size(97, 19)
        Me.radBookActual.TabIndex = 1
        Me.radBookActual.TabStop = False
        '
        'btnBuildBookings
        '
        Me.btnBuildBookings.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBuildBookings.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnBuildBookings.Appearance.Options.UseFont = True
        Me.btnBuildBookings.Location = New System.Drawing.Point(732, 4)
        Me.btnBuildBookings.Name = "btnBuildBookings"
        Me.btnBuildBookings.Size = New System.Drawing.Size(75, 23)
        Me.btnBuildBookings.TabIndex = 4
        Me.btnBuildBookings.Text = "Rebuild"
        Me.btnBuildBookings.Visible = False
        '
        'yvBookings
        '
        Me.yvBookings.CalendarFont = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.yvBookings.Location = New System.Drawing.Point(9, 48)
        Me.yvBookings.Margin = New System.Windows.Forms.Padding(4)
        Me.yvBookings.Name = "yvBookings"
        Me.yvBookings.Size = New System.Drawing.Size(814, 424)
        Me.yvBookings.TabIndex = 2
        '
        'cgBookings
        '
        Me.cgBookings.ButtonsEnabled = False
        Me.cgBookings.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgBookings.HideFirstColumn = False
        Me.cgBookings.Location = New System.Drawing.Point(9, 48)
        Me.cgBookings.Name = "cgBookings"
        Me.cgBookings.PreviewColumn = ""
        Me.cgBookings.Size = New System.Drawing.Size(814, 424)
        Me.cgBookings.TabIndex = 3
        '
        'tabHealth
        '
        Me.tabHealth.Controls.Add(Me.tcHealth)
        Me.tabHealth.Name = "tabHealth"
        Me.tabHealth.Size = New System.Drawing.Size(832, 481)
        Me.tabHealth.Text = "Allergies, Health && Consent"
        '
        'tcHealth
        '
        Me.tcHealth.Location = New System.Drawing.Point(9, 9)
        Me.tcHealth.Name = "tcHealth"
        Me.tcHealth.SelectedTabPage = Me.tabAllergyDiet
        Me.tcHealth.Size = New System.Drawing.Size(820, 469)
        Me.tcHealth.TabIndex = 0
        Me.tcHealth.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabAllergyDiet, Me.tabMedication, Me.tabMedContact, Me.tabSEND, Me.tabImms, Me.tabConsent, Me.tabAttributes, Me.tabRepeatingMedication})
        '
        'tabAllergyDiet
        '
        Me.tabAllergyDiet.Controls.Add(Me.GroupControl11)
        Me.tabAllergyDiet.Controls.Add(Me.GroupControl9)
        Me.tabAllergyDiet.Controls.Add(Me.GroupBox8)
        Me.tabAllergyDiet.Name = "tabAllergyDiet"
        Me.tabAllergyDiet.Size = New System.Drawing.Size(814, 441)
        Me.tabAllergyDiet.Text = "Allergies && Dietary Requirements"
        '
        'GroupControl11
        '
        Me.GroupControl11.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.GroupControl11.Controls.Add(Me.cgAllergyMatrix)
        Me.GroupControl11.Location = New System.Drawing.Point(427, 9)
        Me.GroupControl11.Name = "GroupControl11"
        Me.GroupControl11.Size = New System.Drawing.Size(378, 424)
        Me.GroupControl11.TabIndex = 2
        Me.GroupControl11.Text = "Allergy Matrix"
        '
        'cgAllergyMatrix
        '
        Me.cgAllergyMatrix.AllowBuildColumns = True
        Me.cgAllergyMatrix.AllowEdit = False
        Me.cgAllergyMatrix.AllowHorizontalScroll = False
        Me.cgAllergyMatrix.AllowMultiSelect = False
        Me.cgAllergyMatrix.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgAllergyMatrix.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgAllergyMatrix.Appearance.Options.UseFont = True
        Me.cgAllergyMatrix.AutoSizeByData = True
        Me.cgAllergyMatrix.DisableAutoSize = False
        Me.cgAllergyMatrix.DisableDataFormatting = False
        Me.cgAllergyMatrix.FocusedRowHandle = -2147483648
        Me.cgAllergyMatrix.HideFirstColumn = False
        Me.cgAllergyMatrix.Location = New System.Drawing.Point(9, 29)
        Me.cgAllergyMatrix.Name = "cgAllergyMatrix"
        Me.cgAllergyMatrix.PreviewColumn = ""
        Me.cgAllergyMatrix.QueryID = Nothing
        Me.cgAllergyMatrix.RowAutoHeight = False
        Me.cgAllergyMatrix.SearchAsYouType = True
        Me.cgAllergyMatrix.ShowAutoFilterRow = False
        Me.cgAllergyMatrix.ShowFindPanel = False
        Me.cgAllergyMatrix.ShowGroupByBox = False
        Me.cgAllergyMatrix.ShowLoadingPanel = False
        Me.cgAllergyMatrix.ShowNavigator = False
        Me.cgAllergyMatrix.Size = New System.Drawing.Size(361, 386)
        Me.cgAllergyMatrix.TabIndex = 0
        '
        'GroupControl9
        '
        Me.GroupControl9.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.GroupControl9.Controls.Add(Me.CareLabel3)
        Me.GroupControl9.Controls.Add(Me.cbxDietRestrict)
        Me.GroupControl9.Controls.Add(Me.txtDietNotes)
        Me.GroupControl9.Location = New System.Drawing.Point(9, 233)
        Me.GroupControl9.Name = "GroupControl9"
        Me.GroupControl9.Size = New System.Drawing.Size(412, 200)
        Me.GroupControl9.TabIndex = 1
        Me.GroupControl9.Text = "Dietary Requirements"
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel3.Appearance.Options.UseFont = True
        Me.CareLabel3.Appearance.Options.UseTextOptions = True
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(12, 32)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(61, 15)
        Me.CareLabel3.TabIndex = 0
        Me.CareLabel3.Text = "Restrictions"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxDietRestrict
        '
        Me.cbxDietRestrict.AllowBlank = False
        Me.cbxDietRestrict.DataSource = Nothing
        Me.cbxDietRestrict.DisplayMember = Nothing
        Me.cbxDietRestrict.EnterMoveNextControl = True
        Me.cbxDietRestrict.Location = New System.Drawing.Point(104, 29)
        Me.cbxDietRestrict.Name = "cbxDietRestrict"
        Me.cbxDietRestrict.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxDietRestrict.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxDietRestrict.Properties.Appearance.Options.UseFont = True
        Me.cbxDietRestrict.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxDietRestrict.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxDietRestrict.SelectedValue = Nothing
        Me.cbxDietRestrict.Size = New System.Drawing.Size(297, 22)
        Me.cbxDietRestrict.TabIndex = 1
        Me.cbxDietRestrict.Tag = "AEB"
        Me.cbxDietRestrict.ValueMember = Nothing
        '
        'txtDietNotes
        '
        Me.txtDietNotes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDietNotes.Location = New System.Drawing.Point(12, 57)
        Me.txtDietNotes.Name = "txtDietNotes"
        Me.txtDietNotes.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDietNotes.Properties.Appearance.Options.UseFont = True
        Me.txtDietNotes.Properties.MaxLength = 1024
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtDietNotes, True)
        Me.txtDietNotes.Size = New System.Drawing.Size(389, 134)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtDietNotes, OptionsSpelling4)
        Me.txtDietNotes.TabIndex = 2
        Me.txtDietNotes.Tag = "AE"
        '
        'GroupBox8
        '
        Me.GroupBox8.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.GroupBox8.Controls.Add(Me.CareLabel8)
        Me.GroupBox8.Controls.Add(Me.cbxAllergyGrade)
        Me.GroupBox8.Controls.Add(Me.txtMedAllergies)
        Me.GroupBox8.Location = New System.Drawing.Point(9, 9)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(412, 218)
        Me.GroupBox8.TabIndex = 0
        Me.GroupBox8.Text = "Allergies"
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel8.Appearance.Options.UseFont = True
        Me.CareLabel8.Appearance.Options.UseTextOptions = True
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(12, 32)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(71, 15)
        Me.CareLabel8.TabIndex = 0
        Me.CareLabel8.Text = "Allergy Grade"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxAllergyGrade
        '
        Me.cbxAllergyGrade.AllowBlank = False
        Me.cbxAllergyGrade.DataSource = Nothing
        Me.cbxAllergyGrade.DisplayMember = Nothing
        Me.cbxAllergyGrade.EnterMoveNextControl = True
        Me.cbxAllergyGrade.Location = New System.Drawing.Point(104, 29)
        Me.cbxAllergyGrade.Name = "cbxAllergyGrade"
        Me.cbxAllergyGrade.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxAllergyGrade.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxAllergyGrade.Properties.Appearance.Options.UseFont = True
        Me.cbxAllergyGrade.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxAllergyGrade.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxAllergyGrade.SelectedValue = Nothing
        Me.cbxAllergyGrade.Size = New System.Drawing.Size(297, 22)
        Me.cbxAllergyGrade.TabIndex = 1
        Me.cbxAllergyGrade.Tag = "AEB"
        Me.cbxAllergyGrade.ValueMember = Nothing
        '
        'txtMedAllergies
        '
        Me.txtMedAllergies.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMedAllergies.Location = New System.Drawing.Point(12, 57)
        Me.txtMedAllergies.Name = "txtMedAllergies"
        Me.txtMedAllergies.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMedAllergies.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtMedAllergies, True)
        Me.txtMedAllergies.Size = New System.Drawing.Size(389, 152)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtMedAllergies, OptionsSpelling5)
        Me.txtMedAllergies.TabIndex = 2
        Me.txtMedAllergies.Tag = "AE"
        '
        'tabMedication
        '
        Me.tabMedication.Controls.Add(Me.GroupBox6)
        Me.tabMedication.Controls.Add(Me.GroupBox9)
        Me.tabMedication.Name = "tabMedication"
        Me.tabMedication.Size = New System.Drawing.Size(814, 441)
        Me.tabMedication.Text = "Medication && Medical Notes"
        '
        'GroupBox6
        '
        Me.GroupBox6.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.GroupBox6.Controls.Add(Me.txtMedMedication)
        Me.GroupBox6.Location = New System.Drawing.Point(9, 9)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(797, 140)
        Me.GroupBox6.TabIndex = 0
        Me.GroupBox6.Text = "Medication"
        '
        'txtMedMedication
        '
        Me.txtMedMedication.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMedMedication.Location = New System.Drawing.Point(5, 23)
        Me.txtMedMedication.Name = "txtMedMedication"
        Me.txtMedMedication.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMedMedication.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtMedMedication, True)
        Me.txtMedMedication.Size = New System.Drawing.Size(787, 112)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtMedMedication, OptionsSpelling6)
        Me.txtMedMedication.TabIndex = 0
        Me.txtMedMedication.Tag = "AE"
        '
        'GroupBox9
        '
        Me.GroupBox9.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.GroupBox9.Controls.Add(Me.txtMedNotes)
        Me.GroupBox9.Location = New System.Drawing.Point(9, 155)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(797, 122)
        Me.GroupBox9.TabIndex = 1
        Me.GroupBox9.Text = "Other Medical Notes"
        '
        'txtMedNotes
        '
        Me.txtMedNotes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMedNotes.Location = New System.Drawing.Point(5, 23)
        Me.txtMedNotes.Name = "txtMedNotes"
        Me.txtMedNotes.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMedNotes.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtMedNotes, True)
        Me.txtMedNotes.Size = New System.Drawing.Size(787, 94)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtMedNotes, OptionsSpelling7)
        Me.txtMedNotes.TabIndex = 0
        Me.txtMedNotes.Tag = "AE"
        '
        'tabMedContact
        '
        Me.tabMedContact.Controls.Add(Me.GroupControl10)
        Me.tabMedContact.Controls.Add(Me.GroupBox7)
        Me.tabMedContact.Name = "tabMedContact"
        Me.tabMedContact.Size = New System.Drawing.Size(814, 441)
        Me.tabMedContact.Text = "Medical Contacts"
        '
        'GroupControl10
        '
        Me.GroupControl10.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.GroupControl10.Controls.Add(Me.txtHVTelephone)
        Me.GroupControl10.Controls.Add(Me.txtHVMobile)
        Me.GroupControl10.Controls.Add(Me.txtHVEmail)
        Me.GroupControl10.Controls.Add(Me.CareLabel23)
        Me.GroupControl10.Controls.Add(Me.txtHVName)
        Me.GroupControl10.Controls.Add(Me.CareLabel4)
        Me.GroupControl10.Controls.Add(Me.CareLabel5)
        Me.GroupControl10.Controls.Add(Me.CareLabel6)
        Me.GroupControl10.Location = New System.Drawing.Point(357, 9)
        Me.GroupControl10.Name = "GroupControl10"
        Me.GroupControl10.Size = New System.Drawing.Size(342, 144)
        Me.GroupControl10.TabIndex = 1
        Me.GroupControl10.Text = "Health Visitor"
        '
        'txtHVTelephone
        '
        Me.txtHVTelephone.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHVTelephone.Appearance.Options.UseFont = True
        Me.txtHVTelephone.IsMobile = False
        Me.txtHVTelephone.Location = New System.Drawing.Point(113, 57)
        Me.txtHVTelephone.MaxLength = 15
        Me.txtHVTelephone.MinimumSize = New System.Drawing.Size(0, 22)
        Me.txtHVTelephone.Name = "txtHVTelephone"
        Me.txtHVTelephone.ReadOnly = False
        Me.txtHVTelephone.Size = New System.Drawing.Size(218, 22)
        Me.txtHVTelephone.TabIndex = 3
        Me.txtHVTelephone.Tag = "AE"
        '
        'txtHVMobile
        '
        Me.txtHVMobile.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHVMobile.Appearance.Options.UseFont = True
        Me.txtHVMobile.IsMobile = True
        Me.txtHVMobile.Location = New System.Drawing.Point(113, 85)
        Me.txtHVMobile.MaxLength = 15
        Me.txtHVMobile.MinimumSize = New System.Drawing.Size(0, 22)
        Me.txtHVMobile.Name = "txtHVMobile"
        Me.txtHVMobile.ReadOnly = False
        Me.txtHVMobile.Size = New System.Drawing.Size(218, 22)
        Me.txtHVMobile.TabIndex = 5
        Me.txtHVMobile.Tag = "AE"
        '
        'txtHVEmail
        '
        Me.txtHVEmail.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHVEmail.Appearance.Options.UseFont = True
        Me.txtHVEmail.Location = New System.Drawing.Point(113, 113)
        Me.txtHVEmail.MaxLength = 100
        Me.txtHVEmail.Name = "txtHVEmail"
        Me.txtHVEmail.NoButton = False
        Me.txtHVEmail.NoColours = False
        Me.txtHVEmail.NoValidate = False
        Me.txtHVEmail.ReadOnly = False
        Me.txtHVEmail.Size = New System.Drawing.Size(218, 22)
        Me.txtHVEmail.TabIndex = 7
        Me.txtHVEmail.Tag = "AE"
        '
        'CareLabel23
        '
        Me.CareLabel23.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel23.Appearance.Options.UseFont = True
        Me.CareLabel23.Appearance.Options.UseTextOptions = True
        Me.CareLabel23.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel23.Location = New System.Drawing.Point(11, 114)
        Me.CareLabel23.Name = "CareLabel23"
        Me.CareLabel23.Size = New System.Drawing.Size(29, 15)
        Me.CareLabel23.TabIndex = 6
        Me.CareLabel23.Text = "Email"
        Me.CareLabel23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtHVName
        '
        Me.txtHVName.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtHVName.EnterMoveNextControl = True
        Me.txtHVName.Location = New System.Drawing.Point(113, 29)
        Me.txtHVName.MaxLength = 30
        Me.txtHVName.Name = "txtHVName"
        Me.txtHVName.NumericAllowNegatives = False
        Me.txtHVName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtHVName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtHVName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtHVName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHVName.Properties.Appearance.Options.UseFont = True
        Me.txtHVName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtHVName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtHVName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtHVName.Properties.MaxLength = 30
        Me.txtHVName.Size = New System.Drawing.Size(218, 22)
        Me.txtHVName.TabIndex = 1
        Me.txtHVName.Tag = "AE"
        Me.txtHVName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtHVName.ToolTipText = ""
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel4.Appearance.Options.UseFont = True
        Me.CareLabel4.Appearance.Options.UseTextOptions = True
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(11, 32)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel4.TabIndex = 0
        Me.CareLabel4.Text = "Name"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel5.Appearance.Options.UseFont = True
        Me.CareLabel5.Appearance.Options.UseTextOptions = True
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(11, 60)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(56, 15)
        Me.CareLabel5.TabIndex = 2
        Me.CareLabel5.Text = "Telephone"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel6.Appearance.Options.UseFont = True
        Me.CareLabel6.Appearance.Options.UseTextOptions = True
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(11, 88)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(37, 15)
        Me.CareLabel6.TabIndex = 4
        Me.CareLabel6.Text = "Mobile"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox7
        '
        Me.GroupBox7.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.GroupBox7.Controls.Add(Me.txtSurgTel)
        Me.GroupBox7.Controls.Add(Me.txtSurgDoctor)
        Me.GroupBox7.Controls.Add(Me.Label33)
        Me.GroupBox7.Controls.Add(Me.txtSurgName)
        Me.GroupBox7.Controls.Add(Me.Label34)
        Me.GroupBox7.Controls.Add(Me.Label32)
        Me.GroupBox7.Location = New System.Drawing.Point(9, 9)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(342, 117)
        Me.GroupBox7.TabIndex = 0
        Me.GroupBox7.Text = "Doctor Details"
        '
        'txtSurgTel
        '
        Me.txtSurgTel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSurgTel.Appearance.Options.UseFont = True
        Me.txtSurgTel.IsMobile = False
        Me.txtSurgTel.Location = New System.Drawing.Point(113, 85)
        Me.txtSurgTel.MaxLength = 15
        Me.txtSurgTel.MinimumSize = New System.Drawing.Size(0, 22)
        Me.txtSurgTel.Name = "txtSurgTel"
        Me.txtSurgTel.ReadOnly = False
        Me.txtSurgTel.Size = New System.Drawing.Size(218, 22)
        Me.txtSurgTel.TabIndex = 5
        Me.txtSurgTel.Tag = "AE"
        '
        'txtSurgDoctor
        '
        Me.txtSurgDoctor.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtSurgDoctor.EnterMoveNextControl = True
        Me.txtSurgDoctor.Location = New System.Drawing.Point(113, 29)
        Me.txtSurgDoctor.MaxLength = 30
        Me.txtSurgDoctor.Name = "txtSurgDoctor"
        Me.txtSurgDoctor.NumericAllowNegatives = False
        Me.txtSurgDoctor.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSurgDoctor.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSurgDoctor.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSurgDoctor.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSurgDoctor.Properties.Appearance.Options.UseFont = True
        Me.txtSurgDoctor.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSurgDoctor.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSurgDoctor.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSurgDoctor.Properties.MaxLength = 30
        Me.txtSurgDoctor.Size = New System.Drawing.Size(218, 22)
        Me.txtSurgDoctor.TabIndex = 1
        Me.txtSurgDoctor.Tag = "AE"
        Me.txtSurgDoctor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSurgDoctor.ToolTipText = ""
        '
        'Label33
        '
        Me.Label33.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.Appearance.Options.UseFont = True
        Me.Label33.Appearance.Options.UseTextOptions = True
        Me.Label33.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label33.Location = New System.Drawing.Point(11, 32)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(75, 15)
        Me.Label33.TabIndex = 0
        Me.Label33.Text = "Surgery Name"
        Me.Label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSurgName
        '
        Me.txtSurgName.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtSurgName.EnterMoveNextControl = True
        Me.txtSurgName.Location = New System.Drawing.Point(113, 57)
        Me.txtSurgName.MaxLength = 30
        Me.txtSurgName.Name = "txtSurgName"
        Me.txtSurgName.NumericAllowNegatives = False
        Me.txtSurgName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSurgName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSurgName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSurgName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSurgName.Properties.Appearance.Options.UseFont = True
        Me.txtSurgName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSurgName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSurgName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSurgName.Properties.MaxLength = 30
        Me.txtSurgName.Size = New System.Drawing.Size(218, 22)
        Me.txtSurgName.TabIndex = 3
        Me.txtSurgName.Tag = "AE"
        Me.txtSurgName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSurgName.ToolTipText = ""
        '
        'Label34
        '
        Me.Label34.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.Appearance.Options.UseFont = True
        Me.Label34.Appearance.Options.UseTextOptions = True
        Me.Label34.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label34.Location = New System.Drawing.Point(11, 60)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(71, 15)
        Me.Label34.TabIndex = 2
        Me.Label34.Text = "Doctor Name"
        Me.Label34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label32
        '
        Me.Label32.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Appearance.Options.UseFont = True
        Me.Label32.Appearance.Options.UseTextOptions = True
        Me.Label32.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label32.Location = New System.Drawing.Point(11, 88)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(56, 15)
        Me.Label32.TabIndex = 4
        Me.Label32.Text = "Telephone"
        Me.Label32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabSEND
        '
        Me.tabSEND.Controls.Add(Me.GroupControl12)
        Me.tabSEND.Name = "tabSEND"
        Me.tabSEND.Size = New System.Drawing.Size(814, 441)
        Me.tabSEND.Text = "SEND"
        '
        'GroupControl12
        '
        Me.GroupControl12.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.GroupControl12.Controls.Add(Me.chkSEN)
        Me.GroupControl12.Controls.Add(Me.CareLabel22)
        Me.GroupControl12.Controls.Add(Me.txtSENNotes)
        Me.GroupControl12.Location = New System.Drawing.Point(9, 9)
        Me.GroupControl12.Name = "GroupControl12"
        Me.GroupControl12.Size = New System.Drawing.Size(797, 425)
        Me.GroupControl12.TabIndex = 1
        Me.GroupControl12.Text = "Special Educational Needs and Disability (SEND)"
        '
        'chkSEN
        '
        Me.chkSEN.EnterMoveNextControl = True
        Me.chkSEN.Location = New System.Drawing.Point(47, 28)
        Me.chkSEN.Name = "chkSEN"
        Me.chkSEN.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSEN.Properties.Appearance.Options.UseFont = True
        Me.chkSEN.Size = New System.Drawing.Size(20, 19)
        Me.chkSEN.TabIndex = 2
        Me.chkSEN.Tag = "AEB"
        '
        'CareLabel22
        '
        Me.CareLabel22.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel22.Appearance.Options.UseFont = True
        Me.CareLabel22.Appearance.Options.UseTextOptions = True
        Me.CareLabel22.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel22.Location = New System.Drawing.Point(12, 30)
        Me.CareLabel22.Name = "CareLabel22"
        Me.CareLabel22.Size = New System.Drawing.Size(29, 15)
        Me.CareLabel22.TabIndex = 1
        Me.CareLabel22.Text = "SEND"
        Me.CareLabel22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSENNotes
        '
        Me.txtSENNotes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSENNotes.Location = New System.Drawing.Point(12, 51)
        Me.txtSENNotes.Name = "txtSENNotes"
        Me.txtSENNotes.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSENNotes.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtSENNotes, True)
        Me.txtSENNotes.Size = New System.Drawing.Size(774, 365)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtSENNotes, OptionsSpelling8)
        Me.txtSENNotes.TabIndex = 0
        Me.txtSENNotes.Tag = "AE"
        '
        'tabImms
        '
        Me.tabImms.Controls.Add(Me.GroupControl16)
        Me.tabImms.Name = "tabImms"
        Me.tabImms.Size = New System.Drawing.Size(814, 441)
        Me.tabImms.Text = "Immunisations"
        '
        'GroupControl16
        '
        Me.GroupControl16.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl16.Controls.Add(Me.gbxImm)
        Me.GroupControl16.Controls.Add(Me.cgImms)
        Me.GroupControl16.Location = New System.Drawing.Point(3, 3)
        Me.GroupControl16.Name = "GroupControl16"
        Me.GroupControl16.ShowCaption = False
        Me.GroupControl16.Size = New System.Drawing.Size(808, 435)
        Me.GroupControl16.TabIndex = 6
        '
        'gbxImm
        '
        Me.gbxImm.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.gbxImm.Controls.Add(Me.btnImmCancel)
        Me.gbxImm.Controls.Add(Me.btnImmOK)
        Me.gbxImm.Controls.Add(Me.cbxImm)
        Me.gbxImm.Controls.Add(Me.CareLabel30)
        Me.gbxImm.Controls.Add(Me.cdtImmDate)
        Me.gbxImm.Controls.Add(Me.CareLabel32)
        Me.gbxImm.Location = New System.Drawing.Point(220, 143)
        Me.gbxImm.Name = "gbxImm"
        Me.gbxImm.Size = New System.Drawing.Size(369, 126)
        Me.gbxImm.TabIndex = 3
        '
        'btnImmCancel
        '
        Me.btnImmCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnImmCancel.Appearance.Options.UseFont = True
        Me.btnImmCancel.CausesValidation = False
        Me.btnImmCancel.Location = New System.Drawing.Point(272, 95)
        Me.btnImmCancel.Name = "btnImmCancel"
        Me.btnImmCancel.Size = New System.Drawing.Size(85, 23)
        Me.btnImmCancel.TabIndex = 7
        Me.btnImmCancel.Text = "Cancel"
        '
        'btnImmOK
        '
        Me.btnImmOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnImmOK.Appearance.Options.UseFont = True
        Me.btnImmOK.CausesValidation = False
        Me.btnImmOK.Location = New System.Drawing.Point(181, 95)
        Me.btnImmOK.Name = "btnImmOK"
        Me.btnImmOK.Size = New System.Drawing.Size(85, 23)
        Me.btnImmOK.TabIndex = 6
        Me.btnImmOK.Tag = "AE"
        Me.btnImmOK.Text = "OK"
        '
        'cbxImm
        '
        Me.cbxImm.AllowBlank = False
        Me.cbxImm.DataSource = Nothing
        Me.cbxImm.DisplayMember = Nothing
        Me.cbxImm.EnterMoveNextControl = True
        Me.cbxImm.Location = New System.Drawing.Point(103, 29)
        Me.cbxImm.Name = "cbxImm"
        Me.cbxImm.Properties.AccessibleName = "Site"
        Me.cbxImm.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxImm.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxImm.Properties.Appearance.Options.UseFont = True
        Me.cbxImm.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxImm.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxImm.SelectedValue = Nothing
        Me.cbxImm.Size = New System.Drawing.Size(254, 22)
        Me.cbxImm.TabIndex = 1
        Me.cbxImm.Tag = "AE"
        Me.cbxImm.ValueMember = Nothing
        '
        'CareLabel30
        '
        Me.CareLabel30.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel30.Appearance.Options.UseFont = True
        Me.CareLabel30.Appearance.Options.UseTextOptions = True
        Me.CareLabel30.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel30.Location = New System.Drawing.Point(12, 32)
        Me.CareLabel30.Name = "CareLabel30"
        Me.CareLabel30.Size = New System.Drawing.Size(74, 15)
        Me.CareLabel30.TabIndex = 0
        Me.CareLabel30.Text = "Immunisation"
        Me.CareLabel30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel30.ToolTip = "The last date the child will attend the Nursery. This date is used for invoicing " &
    "purposes."
        Me.CareLabel30.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'cdtImmDate
        '
        Me.cdtImmDate.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtImmDate.EnterMoveNextControl = True
        Me.cdtImmDate.Location = New System.Drawing.Point(103, 57)
        Me.cdtImmDate.Name = "cdtImmDate"
        Me.cdtImmDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtImmDate.Properties.Appearance.Options.UseFont = True
        Me.cdtImmDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtImmDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtImmDate.Size = New System.Drawing.Size(85, 22)
        Me.cdtImmDate.TabIndex = 5
        Me.cdtImmDate.Tag = "AEB"
        Me.cdtImmDate.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'CareLabel32
        '
        Me.CareLabel32.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel32.Appearance.Options.UseFont = True
        Me.CareLabel32.Appearance.Options.UseTextOptions = True
        Me.CareLabel32.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel32.Location = New System.Drawing.Point(12, 60)
        Me.CareLabel32.Name = "CareLabel32"
        Me.CareLabel32.Size = New System.Drawing.Size(24, 15)
        Me.CareLabel32.TabIndex = 2
        Me.CareLabel32.Text = "Date"
        Me.CareLabel32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cgImms
        '
        Me.cgImms.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgImms.ButtonsEnabled = False
        Me.cgImms.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgImms.HideFirstColumn = False
        Me.cgImms.Location = New System.Drawing.Point(5, 5)
        Me.cgImms.Name = "cgImms"
        Me.cgImms.PreviewColumn = ""
        Me.cgImms.Size = New System.Drawing.Size(798, 425)
        Me.cgImms.TabIndex = 1
        '
        'tabConsent
        '
        Me.tabConsent.Controls.Add(Me.GroupControl15)
        Me.tabConsent.Name = "tabConsent"
        Me.tabConsent.Size = New System.Drawing.Size(814, 441)
        Me.tabConsent.Text = "Consent"
        '
        'GroupControl15
        '
        Me.GroupControl15.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl15.Controls.Add(Me.gbxConsent)
        Me.GroupControl15.Controls.Add(Me.cgConsent)
        Me.GroupControl15.Location = New System.Drawing.Point(3, 3)
        Me.GroupControl15.Name = "GroupControl15"
        Me.GroupControl15.ShowCaption = False
        Me.GroupControl15.Size = New System.Drawing.Size(808, 435)
        Me.GroupControl15.TabIndex = 6
        '
        'gbxConsent
        '
        Me.gbxConsent.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.gbxConsent.Controls.Add(Me.btnConsentClear)
        Me.gbxConsent.Controls.Add(Me.txtConsentItem)
        Me.gbxConsent.Controls.Add(Me.cbxConsentContact)
        Me.gbxConsent.Controls.Add(Me.CareLabel27)
        Me.gbxConsent.Controls.Add(Me.btnConsentCancel)
        Me.gbxConsent.Controls.Add(Me.btnConsentOK)
        Me.gbxConsent.Controls.Add(Me.CareLabel25)
        Me.gbxConsent.Controls.Add(Me.cdtConsentDate)
        Me.gbxConsent.Controls.Add(Me.CareLabel26)
        Me.gbxConsent.Location = New System.Drawing.Point(208, 139)
        Me.gbxConsent.Name = "gbxConsent"
        Me.gbxConsent.Size = New System.Drawing.Size(393, 157)
        Me.gbxConsent.TabIndex = 0
        '
        'btnConsentClear
        '
        Me.btnConsentClear.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnConsentClear.Appearance.Options.UseFont = True
        Me.btnConsentClear.CausesValidation = False
        Me.btnConsentClear.Location = New System.Drawing.Point(12, 123)
        Me.btnConsentClear.Name = "btnConsentClear"
        Me.btnConsentClear.Size = New System.Drawing.Size(85, 23)
        Me.btnConsentClear.TabIndex = 6
        Me.btnConsentClear.Tag = "AE"
        Me.btnConsentClear.Text = "Clear"
        '
        'txtConsentItem
        '
        Me.txtConsentItem.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtConsentItem.EnterMoveNextControl = True
        Me.txtConsentItem.Location = New System.Drawing.Point(125, 29)
        Me.txtConsentItem.MaxLength = 30
        Me.txtConsentItem.Name = "txtConsentItem"
        Me.txtConsentItem.NumericAllowNegatives = False
        Me.txtConsentItem.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtConsentItem.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtConsentItem.Properties.AccessibleName = "Surname"
        Me.txtConsentItem.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtConsentItem.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtConsentItem.Properties.Appearance.Options.UseFont = True
        Me.txtConsentItem.Properties.Appearance.Options.UseTextOptions = True
        Me.txtConsentItem.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtConsentItem.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtConsentItem.Properties.MaxLength = 30
        Me.txtConsentItem.Properties.ReadOnly = True
        Me.txtConsentItem.Size = New System.Drawing.Size(254, 22)
        Me.txtConsentItem.TabIndex = 1
        Me.txtConsentItem.Tag = ""
        Me.txtConsentItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtConsentItem.ToolTipText = ""
        '
        'cbxConsentContact
        '
        Me.cbxConsentContact.AllowBlank = False
        Me.cbxConsentContact.DataSource = Nothing
        Me.cbxConsentContact.DisplayMember = Nothing
        Me.cbxConsentContact.EnterMoveNextControl = True
        Me.cbxConsentContact.Location = New System.Drawing.Point(125, 85)
        Me.cbxConsentContact.Name = "cbxConsentContact"
        Me.cbxConsentContact.Properties.AccessibleName = "Site"
        Me.cbxConsentContact.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxConsentContact.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxConsentContact.Properties.Appearance.Options.UseFont = True
        Me.cbxConsentContact.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxConsentContact.SelectedValue = Nothing
        Me.cbxConsentContact.Size = New System.Drawing.Size(254, 22)
        Me.cbxConsentContact.TabIndex = 5
        Me.cbxConsentContact.Tag = "AE"
        Me.cbxConsentContact.ValueMember = Nothing
        '
        'CareLabel27
        '
        Me.CareLabel27.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel27.Appearance.Options.UseFont = True
        Me.CareLabel27.Appearance.Options.UseTextOptions = True
        Me.CareLabel27.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel27.Location = New System.Drawing.Point(12, 86)
        Me.CareLabel27.Name = "CareLabel27"
        Me.CareLabel27.Size = New System.Drawing.Size(46, 15)
        Me.CareLabel27.TabIndex = 4
        Me.CareLabel27.Text = "Given By"
        Me.CareLabel27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel27.ToolTip = "The last date the child will attend the Nursery. This date is used for invoicing " &
    "purposes."
        Me.CareLabel27.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'btnConsentCancel
        '
        Me.btnConsentCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnConsentCancel.Appearance.Options.UseFont = True
        Me.btnConsentCancel.CausesValidation = False
        Me.btnConsentCancel.Location = New System.Drawing.Point(294, 123)
        Me.btnConsentCancel.Name = "btnConsentCancel"
        Me.btnConsentCancel.Size = New System.Drawing.Size(85, 23)
        Me.btnConsentCancel.TabIndex = 8
        Me.btnConsentCancel.Text = "Cancel"
        '
        'btnConsentOK
        '
        Me.btnConsentOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnConsentOK.Appearance.Options.UseFont = True
        Me.btnConsentOK.CausesValidation = False
        Me.btnConsentOK.Location = New System.Drawing.Point(203, 123)
        Me.btnConsentOK.Name = "btnConsentOK"
        Me.btnConsentOK.Size = New System.Drawing.Size(85, 23)
        Me.btnConsentOK.TabIndex = 7
        Me.btnConsentOK.Tag = "AE"
        Me.btnConsentOK.Text = "OK"
        '
        'CareLabel25
        '
        Me.CareLabel25.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel25.Appearance.Options.UseFont = True
        Me.CareLabel25.Appearance.Options.UseTextOptions = True
        Me.CareLabel25.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel25.Location = New System.Drawing.Point(12, 32)
        Me.CareLabel25.Name = "CareLabel25"
        Me.CareLabel25.Size = New System.Drawing.Size(95, 15)
        Me.CareLabel25.TabIndex = 0
        Me.CareLabel25.Text = "Consent Given for"
        Me.CareLabel25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel25.ToolTip = "The last date the child will attend the Nursery. This date is used for invoicing " &
    "purposes."
        Me.CareLabel25.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'cdtConsentDate
        '
        Me.cdtConsentDate.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtConsentDate.EnterMoveNextControl = True
        Me.cdtConsentDate.Location = New System.Drawing.Point(125, 57)
        Me.cdtConsentDate.Name = "cdtConsentDate"
        Me.cdtConsentDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtConsentDate.Properties.Appearance.Options.UseFont = True
        Me.cdtConsentDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtConsentDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtConsentDate.Size = New System.Drawing.Size(85, 22)
        Me.cdtConsentDate.TabIndex = 3
        Me.cdtConsentDate.Tag = "AEB"
        Me.cdtConsentDate.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'CareLabel26
        '
        Me.CareLabel26.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel26.Appearance.Options.UseFont = True
        Me.CareLabel26.Appearance.Options.UseTextOptions = True
        Me.CareLabel26.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel26.Location = New System.Drawing.Point(12, 60)
        Me.CareLabel26.Name = "CareLabel26"
        Me.CareLabel26.Size = New System.Drawing.Size(24, 15)
        Me.CareLabel26.TabIndex = 2
        Me.CareLabel26.Text = "Date"
        Me.CareLabel26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cgConsent
        '
        Me.cgConsent.AllowBuildColumns = True
        Me.cgConsent.AllowEdit = False
        Me.cgConsent.AllowHorizontalScroll = False
        Me.cgConsent.AllowMultiSelect = False
        Me.cgConsent.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgConsent.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgConsent.Appearance.Options.UseFont = True
        Me.cgConsent.AutoSizeByData = True
        Me.cgConsent.DisableAutoSize = False
        Me.cgConsent.DisableDataFormatting = False
        Me.cgConsent.FocusedRowHandle = -2147483648
        Me.cgConsent.HideFirstColumn = False
        Me.cgConsent.Location = New System.Drawing.Point(5, 5)
        Me.cgConsent.Name = "cgConsent"
        Me.cgConsent.PreviewColumn = ""
        Me.cgConsent.QueryID = Nothing
        Me.cgConsent.RowAutoHeight = False
        Me.cgConsent.SearchAsYouType = True
        Me.cgConsent.ShowAutoFilterRow = False
        Me.cgConsent.ShowFindPanel = False
        Me.cgConsent.ShowGroupByBox = False
        Me.cgConsent.ShowLoadingPanel = False
        Me.cgConsent.ShowNavigator = False
        Me.cgConsent.Size = New System.Drawing.Size(798, 425)
        Me.cgConsent.TabIndex = 0
        '
        'tabAttributes
        '
        Me.tabAttributes.Controls.Add(Me.GroupControl13)
        Me.tabAttributes.Name = "tabAttributes"
        Me.tabAttributes.Size = New System.Drawing.Size(814, 441)
        Me.tabAttributes.Text = "Attributes"
        '
        'GroupControl13
        '
        Me.GroupControl13.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl13.Controls.Add(Me.clbAttributes)
        Me.GroupControl13.Location = New System.Drawing.Point(3, 3)
        Me.GroupControl13.Name = "GroupControl13"
        Me.GroupControl13.ShowCaption = False
        Me.GroupControl13.Size = New System.Drawing.Size(808, 435)
        Me.GroupControl13.TabIndex = 5
        '
        'clbAttributes
        '
        Me.clbAttributes.Location = New System.Drawing.Point(5, 5)
        Me.clbAttributes.MultiColumn = True
        Me.clbAttributes.Name = "clbAttributes"
        Me.clbAttributes.Size = New System.Drawing.Size(798, 425)
        Me.clbAttributes.TabIndex = 0
        '
        'tabRepeatingMedication
        '
        Me.tabRepeatingMedication.Controls.Add(Me.gbxRepeatMedication)
        Me.tabRepeatingMedication.Controls.Add(Me.cgRepeatingMedication)
        Me.tabRepeatingMedication.Name = "tabRepeatingMedication"
        Me.tabRepeatingMedication.Padding = New System.Windows.Forms.Padding(5)
        Me.tabRepeatingMedication.Size = New System.Drawing.Size(814, 441)
        Me.tabRepeatingMedication.Text = "Repeating Medication"
        '
        'gbxRepeatMedication
        '
        Me.gbxRepeatMedication.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.gbxRepeatMedication.Controls.Add(Me.txtManualTimeEntry)
        Me.gbxRepeatMedication.Controls.Add(Me.txtStartTime)
        Me.gbxRepeatMedication.Controls.Add(Me.btnManualTimeEntryRemove)
        Me.gbxRepeatMedication.Controls.Add(Me.btnManualTimeEntryAdd)
        Me.gbxRepeatMedication.Controls.Add(Me.chkAsRequired)
        Me.gbxRepeatMedication.Controls.Add(Me.CareLabel47)
        Me.gbxRepeatMedication.Controls.Add(Me.lblManualTimeEntries)
        Me.gbxRepeatMedication.Controls.Add(Me.CareLabel43)
        Me.gbxRepeatMedication.Controls.Add(Me.CareLabel33)
        Me.gbxRepeatMedication.Controls.Add(Me.btnRepeatingMedicationCancel)
        Me.gbxRepeatMedication.Controls.Add(Me.btnRepeatingMedicationOK)
        Me.gbxRepeatMedication.Controls.Add(Me.txtFrequency)
        Me.gbxRepeatMedication.Controls.Add(Me.CareLabel31)
        Me.gbxRepeatMedication.Controls.Add(Me.CareLabel29)
        Me.gbxRepeatMedication.Controls.Add(Me.cbxDosageRequired)
        Me.gbxRepeatMedication.Controls.Add(Me.CareLabel28)
        Me.gbxRepeatMedication.Controls.Add(Me.cbxMedicationRequired)
        Me.gbxRepeatMedication.Controls.Add(Me.CareLabel17)
        Me.gbxRepeatMedication.Controls.Add(Me.cbxNatureOfIllness)
        Me.gbxRepeatMedication.Location = New System.Drawing.Point(163, 56)
        Me.gbxRepeatMedication.Name = "gbxRepeatMedication"
        Me.gbxRepeatMedication.Padding = New System.Windows.Forms.Padding(10, 10, 3, 3)
        Me.gbxRepeatMedication.ShowCaption = False
        Me.gbxRepeatMedication.Size = New System.Drawing.Size(492, 270)
        Me.gbxRepeatMedication.TabIndex = 7
        '
        'txtManualTimeEntry
        '
        Me.txtManualTimeEntry.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtManualTimeEntry.EnterMoveNextControl = True
        Me.txtManualTimeEntry.Location = New System.Drawing.Point(146, 203)
        Me.txtManualTimeEntry.MaxLength = 8
        Me.txtManualTimeEntry.Name = "txtManualTimeEntry"
        Me.txtManualTimeEntry.NumericAllowNegatives = False
        Me.txtManualTimeEntry.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.Time
        Me.txtManualTimeEntry.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtManualTimeEntry.Properties.AccessibleName = "Start Time"
        Me.txtManualTimeEntry.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtManualTimeEntry.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtManualTimeEntry.Properties.Appearance.Options.UseFont = True
        Me.txtManualTimeEntry.Properties.Appearance.Options.UseTextOptions = True
        Me.txtManualTimeEntry.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtManualTimeEntry.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic
        Me.txtManualTimeEntry.Properties.Mask.EditMask = "(0?\d|1\d|2[0-3])\:[0-5]\d"
        Me.txtManualTimeEntry.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtManualTimeEntry.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtManualTimeEntry.Properties.MaxLength = 8
        Me.txtManualTimeEntry.Size = New System.Drawing.Size(75, 22)
        Me.txtManualTimeEntry.TabIndex = 21
        Me.txtManualTimeEntry.Tag = "AEM"
        Me.txtManualTimeEntry.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtManualTimeEntry.ToolTipText = ""
        '
        'txtStartTime
        '
        Me.txtStartTime.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtStartTime.EnterMoveNextControl = True
        Me.txtStartTime.Location = New System.Drawing.Point(146, 163)
        Me.txtStartTime.MaxLength = 8
        Me.txtStartTime.Name = "txtStartTime"
        Me.txtStartTime.NumericAllowNegatives = False
        Me.txtStartTime.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.Time
        Me.txtStartTime.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStartTime.Properties.AccessibleName = "Start Time"
        Me.txtStartTime.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtStartTime.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtStartTime.Properties.Appearance.Options.UseFont = True
        Me.txtStartTime.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStartTime.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtStartTime.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic
        Me.txtStartTime.Properties.Mask.EditMask = "(0?\d|1\d|2[0-3])\:[0-5]\d"
        Me.txtStartTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtStartTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtStartTime.Properties.MaxLength = 8
        Me.txtStartTime.Size = New System.Drawing.Size(75, 22)
        Me.txtStartTime.TabIndex = 20
        Me.txtStartTime.Tag = "AEM"
        Me.txtStartTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtStartTime.ToolTipText = ""
        '
        'btnManualTimeEntryRemove
        '
        Me.btnManualTimeEntryRemove.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnManualTimeEntryRemove.Appearance.Options.UseFont = True
        Me.btnManualTimeEntryRemove.CausesValidation = False
        Me.btnManualTimeEntryRemove.Location = New System.Drawing.Point(253, 203)
        Me.btnManualTimeEntryRemove.Name = "btnManualTimeEntryRemove"
        Me.btnManualTimeEntryRemove.Size = New System.Drawing.Size(20, 22)
        Me.btnManualTimeEntryRemove.TabIndex = 23
        Me.btnManualTimeEntryRemove.Tag = "AE"
        Me.btnManualTimeEntryRemove.Text = "-"
        '
        'btnManualTimeEntryAdd
        '
        Me.btnManualTimeEntryAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnManualTimeEntryAdd.Appearance.Options.UseFont = True
        Me.btnManualTimeEntryAdd.CausesValidation = False
        Me.btnManualTimeEntryAdd.Location = New System.Drawing.Point(227, 203)
        Me.btnManualTimeEntryAdd.Name = "btnManualTimeEntryAdd"
        Me.btnManualTimeEntryAdd.Size = New System.Drawing.Size(20, 22)
        Me.btnManualTimeEntryAdd.TabIndex = 22
        Me.btnManualTimeEntryAdd.Tag = "AE"
        Me.btnManualTimeEntryAdd.Text = "+"
        '
        'chkAsRequired
        '
        Me.chkAsRequired.EnterMoveNextControl = True
        Me.chkAsRequired.Location = New System.Drawing.Point(146, 109)
        Me.chkAsRequired.Name = "chkAsRequired"
        Me.chkAsRequired.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAsRequired.Properties.Appearance.Options.UseFont = True
        Me.chkAsRequired.Size = New System.Drawing.Size(20, 19)
        Me.chkAsRequired.TabIndex = 18
        Me.chkAsRequired.Tag = "AEB"
        '
        'CareLabel47
        '
        Me.CareLabel47.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel47.Appearance.Options.UseFont = True
        Me.CareLabel47.Appearance.Options.UseTextOptions = True
        Me.CareLabel47.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel47.Location = New System.Drawing.Point(15, 111)
        Me.CareLabel47.Name = "CareLabel47"
        Me.CareLabel47.Size = New System.Drawing.Size(63, 15)
        Me.CareLabel47.TabIndex = 31
        Me.CareLabel47.Text = "As Required"
        Me.CareLabel47.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblManualTimeEntries
        '
        Me.lblManualTimeEntries.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.lblManualTimeEntries.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblManualTimeEntries.Appearance.Options.UseFont = True
        Me.lblManualTimeEntries.Appearance.Options.UseForeColor = True
        Me.lblManualTimeEntries.Appearance.Options.UseTextOptions = True
        Me.lblManualTimeEntries.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblManualTimeEntries.Location = New System.Drawing.Point(280, 207)
        Me.lblManualTimeEntries.Name = "lblManualTimeEntries"
        Me.lblManualTimeEntries.Size = New System.Drawing.Size(113, 15)
        Me.lblManualTimeEntries.TabIndex = 30
        Me.lblManualTimeEntries.Text = "manual time entries"
        Me.lblManualTimeEntries.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel43
        '
        Me.CareLabel43.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel43.Appearance.Options.UseFont = True
        Me.CareLabel43.Appearance.Options.UseTextOptions = True
        Me.CareLabel43.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel43.Location = New System.Drawing.Point(15, 205)
        Me.CareLabel43.Name = "CareLabel43"
        Me.CareLabel43.Size = New System.Drawing.Size(100, 15)
        Me.CareLabel43.TabIndex = 29
        Me.CareLabel43.Text = "Manual Time Entry"
        Me.CareLabel43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel33
        '
        Me.CareLabel33.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel33.Appearance.Options.UseFont = True
        Me.CareLabel33.Appearance.Options.UseTextOptions = True
        Me.CareLabel33.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel33.Location = New System.Drawing.Point(15, 166)
        Me.CareLabel33.Name = "CareLabel33"
        Me.CareLabel33.Size = New System.Drawing.Size(54, 15)
        Me.CareLabel33.TabIndex = 26
        Me.CareLabel33.Text = "Start Time"
        Me.CareLabel33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnRepeatingMedicationCancel
        '
        Me.btnRepeatingMedicationCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnRepeatingMedicationCancel.Appearance.Options.UseFont = True
        Me.btnRepeatingMedicationCancel.CausesValidation = False
        Me.btnRepeatingMedicationCancel.Location = New System.Drawing.Point(399, 239)
        Me.btnRepeatingMedicationCancel.Name = "btnRepeatingMedicationCancel"
        Me.btnRepeatingMedicationCancel.Size = New System.Drawing.Size(85, 23)
        Me.btnRepeatingMedicationCancel.TabIndex = 25
        Me.btnRepeatingMedicationCancel.Text = "Cancel"
        '
        'btnRepeatingMedicationOK
        '
        Me.btnRepeatingMedicationOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnRepeatingMedicationOK.Appearance.Options.UseFont = True
        Me.btnRepeatingMedicationOK.CausesValidation = False
        Me.btnRepeatingMedicationOK.Location = New System.Drawing.Point(308, 239)
        Me.btnRepeatingMedicationOK.Name = "btnRepeatingMedicationOK"
        Me.btnRepeatingMedicationOK.Size = New System.Drawing.Size(85, 23)
        Me.btnRepeatingMedicationOK.TabIndex = 24
        Me.btnRepeatingMedicationOK.Tag = "AE"
        Me.btnRepeatingMedicationOK.Text = "OK"
        '
        'txtFrequency
        '
        Me.txtFrequency.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtFrequency.EnterMoveNextControl = True
        Me.txtFrequency.Location = New System.Drawing.Point(146, 134)
        Me.txtFrequency.MaxLength = 10
        Me.txtFrequency.Name = "txtFrequency"
        Me.txtFrequency.NumericAllowNegatives = False
        Me.txtFrequency.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtFrequency.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFrequency.Properties.AccessibleDescription = ""
        Me.txtFrequency.Properties.AccessibleName = "Forename"
        Me.txtFrequency.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFrequency.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtFrequency.Properties.Appearance.Options.UseFont = True
        Me.txtFrequency.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFrequency.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFrequency.Properties.Mask.EditMask = "##########;"
        Me.txtFrequency.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtFrequency.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFrequency.Properties.MaxLength = 10
        Me.txtFrequency.Size = New System.Drawing.Size(75, 22)
        Me.txtFrequency.TabIndex = 19
        Me.txtFrequency.Tag = "AEBM"
        Me.txtFrequency.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtFrequency.ToolTipText = ""
        '
        'CareLabel31
        '
        Me.CareLabel31.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel31.Appearance.Options.UseFont = True
        Me.CareLabel31.Appearance.Options.UseTextOptions = True
        Me.CareLabel31.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel31.Location = New System.Drawing.Point(15, 138)
        Me.CareLabel31.Name = "CareLabel31"
        Me.CareLabel31.Size = New System.Drawing.Size(109, 15)
        Me.CareLabel31.TabIndex = 20
        Me.CareLabel31.Text = "Frequency (in hours)"
        Me.CareLabel31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel29
        '
        Me.CareLabel29.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel29.Appearance.Options.UseFont = True
        Me.CareLabel29.Appearance.Options.UseTextOptions = True
        Me.CareLabel29.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel29.Location = New System.Drawing.Point(15, 74)
        Me.CareLabel29.Name = "CareLabel29"
        Me.CareLabel29.Size = New System.Drawing.Size(89, 15)
        Me.CareLabel29.TabIndex = 18
        Me.CareLabel29.Text = "Dosage Required"
        Me.CareLabel29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxDosageRequired
        '
        Me.cbxDosageRequired.AllowBlank = False
        Me.cbxDosageRequired.DataSource = Nothing
        Me.cbxDosageRequired.DisplayMember = Nothing
        Me.cbxDosageRequired.EnterMoveNextControl = True
        Me.cbxDosageRequired.Location = New System.Drawing.Point(146, 70)
        Me.cbxDosageRequired.Name = "cbxDosageRequired"
        Me.cbxDosageRequired.Properties.AccessibleName = "Classification"
        Me.cbxDosageRequired.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxDosageRequired.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxDosageRequired.Properties.Appearance.Options.UseFont = True
        Me.cbxDosageRequired.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxDosageRequired.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxDosageRequired.SelectedValue = Nothing
        Me.cbxDosageRequired.Size = New System.Drawing.Size(158, 22)
        Me.cbxDosageRequired.TabIndex = 17
        Me.cbxDosageRequired.Tag = "AEBM"
        Me.cbxDosageRequired.ValueMember = Nothing
        '
        'CareLabel28
        '
        Me.CareLabel28.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel28.Appearance.Options.UseFont = True
        Me.CareLabel28.Appearance.Options.UseTextOptions = True
        Me.CareLabel28.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel28.Location = New System.Drawing.Point(15, 46)
        Me.CareLabel28.Name = "CareLabel28"
        Me.CareLabel28.Size = New System.Drawing.Size(110, 15)
        Me.CareLabel28.TabIndex = 16
        Me.CareLabel28.Text = "Medication Required"
        Me.CareLabel28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxMedicationRequired
        '
        Me.cbxMedicationRequired.AllowBlank = False
        Me.cbxMedicationRequired.DataSource = Nothing
        Me.cbxMedicationRequired.DisplayMember = Nothing
        Me.cbxMedicationRequired.EnterMoveNextControl = True
        Me.cbxMedicationRequired.Location = New System.Drawing.Point(146, 42)
        Me.cbxMedicationRequired.Name = "cbxMedicationRequired"
        Me.cbxMedicationRequired.Properties.AccessibleName = "Classification"
        Me.cbxMedicationRequired.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxMedicationRequired.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxMedicationRequired.Properties.Appearance.Options.UseFont = True
        Me.cbxMedicationRequired.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxMedicationRequired.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxMedicationRequired.SelectedValue = Nothing
        Me.cbxMedicationRequired.Size = New System.Drawing.Size(158, 22)
        Me.cbxMedicationRequired.TabIndex = 16
        Me.cbxMedicationRequired.Tag = "AEBM"
        Me.cbxMedicationRequired.ValueMember = Nothing
        '
        'CareLabel17
        '
        Me.CareLabel17.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel17.Appearance.Options.UseFont = True
        Me.CareLabel17.Appearance.Options.UseTextOptions = True
        Me.CareLabel17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel17.Location = New System.Drawing.Point(15, 18)
        Me.CareLabel17.Name = "CareLabel17"
        Me.CareLabel17.Size = New System.Drawing.Size(85, 15)
        Me.CareLabel17.TabIndex = 14
        Me.CareLabel17.Text = "Nature of Illness"
        Me.CareLabel17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxNatureOfIllness
        '
        Me.cbxNatureOfIllness.AllowBlank = False
        Me.cbxNatureOfIllness.DataSource = Nothing
        Me.cbxNatureOfIllness.DisplayMember = Nothing
        Me.cbxNatureOfIllness.EnterMoveNextControl = True
        Me.cbxNatureOfIllness.Location = New System.Drawing.Point(146, 14)
        Me.cbxNatureOfIllness.Name = "cbxNatureOfIllness"
        Me.cbxNatureOfIllness.Properties.AccessibleName = "Classification"
        Me.cbxNatureOfIllness.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxNatureOfIllness.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxNatureOfIllness.Properties.Appearance.Options.UseFont = True
        Me.cbxNatureOfIllness.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxNatureOfIllness.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxNatureOfIllness.SelectedValue = Nothing
        Me.cbxNatureOfIllness.Size = New System.Drawing.Size(158, 22)
        Me.cbxNatureOfIllness.TabIndex = 15
        Me.cbxNatureOfIllness.Tag = "AEBM"
        Me.cbxNatureOfIllness.ValueMember = Nothing
        '
        'cgRepeatingMedication
        '
        Me.cgRepeatingMedication.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgRepeatingMedication.ButtonsEnabled = False
        Me.cgRepeatingMedication.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgRepeatingMedication.HideFirstColumn = False
        Me.cgRepeatingMedication.Location = New System.Drawing.Point(8, 8)
        Me.cgRepeatingMedication.Name = "cgRepeatingMedication"
        Me.cgRepeatingMedication.PreviewColumn = ""
        Me.cgRepeatingMedication.Size = New System.Drawing.Size(798, 425)
        Me.cgRepeatingMedication.TabIndex = 6
        '
        'tabMedical
        '
        Me.tabMedical.Controls.Add(Me.yvMedInc)
        Me.tabMedical.Controls.Add(Me.btnMedPrintAll)
        Me.tabMedical.Controls.Add(Me.btnDelete)
        Me.tabMedical.Controls.Add(Me.btnMedPrint)
        Me.tabMedical.Controls.Add(Me.grdMedical)
        Me.tabMedical.Controls.Add(Me.GroupControl2)
        Me.tabMedical.Name = "tabMedical"
        Me.tabMedical.Size = New System.Drawing.Size(832, 481)
        Me.tabMedical.Text = "Medicine && Incidents"
        '
        'yvMedInc
        '
        Me.yvMedInc.CalendarFont = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.yvMedInc.Location = New System.Drawing.Point(9, 46)
        Me.yvMedInc.Margin = New System.Windows.Forms.Padding(4)
        Me.yvMedInc.Name = "yvMedInc"
        Me.yvMedInc.Size = New System.Drawing.Size(815, 427)
        Me.yvMedInc.TabIndex = 7
        '
        'btnMedPrintAll
        '
        Me.btnMedPrintAll.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnMedPrintAll.Appearance.Options.UseFont = True
        Me.btnMedPrintAll.CausesValidation = False
        Me.btnMedPrintAll.Location = New System.Drawing.Point(381, 448)
        Me.btnMedPrintAll.Name = "btnMedPrintAll"
        Me.btnMedPrintAll.Size = New System.Drawing.Size(180, 25)
        Me.btnMedPrintAll.TabIndex = 6
        Me.btnMedPrintAll.Tag = ""
        Me.btnMedPrintAll.Text = "Print Entire Log"
        '
        'btnDelete
        '
        Me.btnDelete.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnDelete.Appearance.Options.UseFont = True
        Me.btnDelete.CausesValidation = False
        Me.btnDelete.Location = New System.Drawing.Point(195, 448)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(180, 25)
        Me.btnDelete.TabIndex = 5
        Me.btnDelete.Tag = ""
        Me.btnDelete.Text = "Delete selected Record"
        '
        'btnMedPrint
        '
        Me.btnMedPrint.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnMedPrint.Appearance.Options.UseFont = True
        Me.btnMedPrint.CausesValidation = False
        Me.btnMedPrint.Location = New System.Drawing.Point(9, 448)
        Me.btnMedPrint.Name = "btnMedPrint"
        Me.btnMedPrint.Size = New System.Drawing.Size(180, 25)
        Me.btnMedPrint.TabIndex = 4
        Me.btnMedPrint.Tag = ""
        Me.btnMedPrint.Text = "Print Medication Auth/Log"
        '
        'grdMedical
        '
        Me.grdMedical.AllowBuildColumns = True
        Me.grdMedical.AllowEdit = False
        Me.grdMedical.AllowHorizontalScroll = False
        Me.grdMedical.AllowMultiSelect = False
        Me.grdMedical.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdMedical.Appearance.Options.UseFont = True
        Me.grdMedical.AutoSizeByData = True
        Me.grdMedical.DisableAutoSize = False
        Me.grdMedical.DisableDataFormatting = False
        Me.grdMedical.FocusedRowHandle = -2147483648
        Me.grdMedical.HideFirstColumn = False
        Me.grdMedical.Location = New System.Drawing.Point(9, 46)
        Me.grdMedical.Name = "grdMedical"
        Me.grdMedical.PreviewColumn = ""
        Me.grdMedical.QueryID = Nothing
        Me.grdMedical.RowAutoHeight = False
        Me.grdMedical.SearchAsYouType = True
        Me.grdMedical.ShowAutoFilterRow = False
        Me.grdMedical.ShowFindPanel = False
        Me.grdMedical.ShowGroupByBox = False
        Me.grdMedical.ShowLoadingPanel = False
        Me.grdMedical.ShowNavigator = False
        Me.grdMedical.Size = New System.Drawing.Size(815, 396)
        Me.grdMedical.TabIndex = 2
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.radMedCalendar)
        Me.GroupControl2.Controls.Add(Me.radMedPriorIncidents)
        Me.GroupControl2.Controls.Add(Me.radMedIncidents)
        Me.GroupControl2.Controls.Add(Me.radMedAccidents)
        Me.GroupControl2.Controls.Add(Me.radMedGiven)
        Me.GroupControl2.Controls.Add(Me.radMedRequests)
        Me.GroupControl2.Location = New System.Drawing.Point(9, 7)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.ShowCaption = False
        Me.GroupControl2.Size = New System.Drawing.Size(815, 33)
        Me.GroupControl2.TabIndex = 1
        '
        'radMedCalendar
        '
        Me.radMedCalendar.CausesValidation = False
        Me.radMedCalendar.EditValue = True
        Me.radMedCalendar.Location = New System.Drawing.Point(16, 7)
        Me.radMedCalendar.Name = "radMedCalendar"
        Me.radMedCalendar.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.radMedCalendar.Properties.Appearance.Options.UseFont = True
        Me.radMedCalendar.Properties.Appearance.Options.UseTextOptions = True
        Me.radMedCalendar.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radMedCalendar.Properties.AutoWidth = True
        Me.radMedCalendar.Properties.Caption = "Calendar"
        Me.radMedCalendar.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radMedCalendar.Properties.RadioGroupIndex = 1
        Me.radMedCalendar.Size = New System.Drawing.Size(69, 19)
        Me.radMedCalendar.TabIndex = 6
        '
        'radMedPriorIncidents
        '
        Me.radMedPriorIncidents.CausesValidation = False
        Me.radMedPriorIncidents.Location = New System.Drawing.Point(661, 7)
        Me.radMedPriorIncidents.Name = "radMedPriorIncidents"
        Me.radMedPriorIncidents.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.radMedPriorIncidents.Properties.Appearance.Options.UseFont = True
        Me.radMedPriorIncidents.Properties.Appearance.Options.UseTextOptions = True
        Me.radMedPriorIncidents.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radMedPriorIncidents.Properties.AutoWidth = True
        Me.radMedPriorIncidents.Properties.Caption = "Prior Incidents"
        Me.radMedPriorIncidents.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radMedPriorIncidents.Properties.RadioGroupIndex = 1
        Me.radMedPriorIncidents.Size = New System.Drawing.Size(98, 19)
        Me.radMedPriorIncidents.TabIndex = 5
        Me.radMedPriorIncidents.TabStop = False
        '
        'radMedIncidents
        '
        Me.radMedIncidents.CausesValidation = False
        Me.radMedIncidents.Location = New System.Drawing.Point(585, 7)
        Me.radMedIncidents.Name = "radMedIncidents"
        Me.radMedIncidents.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.radMedIncidents.Properties.Appearance.Options.UseFont = True
        Me.radMedIncidents.Properties.Appearance.Options.UseTextOptions = True
        Me.radMedIncidents.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radMedIncidents.Properties.AutoWidth = True
        Me.radMedIncidents.Properties.Caption = "Incidents"
        Me.radMedIncidents.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radMedIncidents.Properties.RadioGroupIndex = 1
        Me.radMedIncidents.Size = New System.Drawing.Size(70, 19)
        Me.radMedIncidents.TabIndex = 4
        Me.radMedIncidents.TabStop = False
        '
        'radMedAccidents
        '
        Me.radMedAccidents.CausesValidation = False
        Me.radMedAccidents.Location = New System.Drawing.Point(505, 7)
        Me.radMedAccidents.Name = "radMedAccidents"
        Me.radMedAccidents.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.radMedAccidents.Properties.Appearance.Options.UseFont = True
        Me.radMedAccidents.Properties.Appearance.Options.UseTextOptions = True
        Me.radMedAccidents.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radMedAccidents.Properties.AutoWidth = True
        Me.radMedAccidents.Properties.Caption = "Accidents"
        Me.radMedAccidents.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radMedAccidents.Properties.RadioGroupIndex = 1
        Me.radMedAccidents.Size = New System.Drawing.Size(74, 19)
        Me.radMedAccidents.TabIndex = 3
        Me.radMedAccidents.TabStop = False
        '
        'radMedGiven
        '
        Me.radMedGiven.CausesValidation = False
        Me.radMedGiven.Location = New System.Drawing.Point(229, 7)
        Me.radMedGiven.Name = "radMedGiven"
        Me.radMedGiven.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.radMedGiven.Properties.Appearance.Options.UseFont = True
        Me.radMedGiven.Properties.Appearance.Options.UseTextOptions = True
        Me.radMedGiven.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radMedGiven.Properties.AutoWidth = True
        Me.radMedGiven.Properties.Caption = "Medication Given (without Prior Authorisation)"
        Me.radMedGiven.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radMedGiven.Properties.RadioGroupIndex = 1
        Me.radMedGiven.Size = New System.Drawing.Size(270, 19)
        Me.radMedGiven.TabIndex = 2
        Me.radMedGiven.TabStop = False
        '
        'radMedRequests
        '
        Me.radMedRequests.CausesValidation = False
        Me.radMedRequests.Location = New System.Drawing.Point(91, 7)
        Me.radMedRequests.Name = "radMedRequests"
        Me.radMedRequests.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.radMedRequests.Properties.Appearance.Options.UseFont = True
        Me.radMedRequests.Properties.Appearance.Options.UseTextOptions = True
        Me.radMedRequests.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radMedRequests.Properties.AutoWidth = True
        Me.radMedRequests.Properties.Caption = "Medication Requests"
        Me.radMedRequests.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radMedRequests.Properties.RadioGroupIndex = 1
        Me.radMedRequests.Size = New System.Drawing.Size(132, 19)
        Me.radMedRequests.TabIndex = 1
        Me.radMedRequests.TabStop = False
        '
        'tabReports
        '
        Me.tabReports.Controls.Add(Me.CareFrame4)
        Me.tabReports.Controls.Add(Me.GroupControl19)
        Me.tabReports.Controls.Add(Me.GroupBox10)
        Me.tabReports.Controls.Add(Me.GroupBox12)
        Me.tabReports.Controls.Add(Me.btnEmail)
        Me.tabReports.Controls.Add(Me.btnPrint)
        Me.tabReports.Controls.Add(Me.grdReports)
        Me.tabReports.Name = "tabReports"
        Me.tabReports.Size = New System.Drawing.Size(832, 481)
        Me.tabReports.Text = "Reports && Options"
        '
        'CareFrame4
        '
        Me.CareFrame4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.CareFrame4.Appearance.Options.UseFont = True
        Me.CareFrame4.Controls.Add(Me.btnPIN)
        Me.CareFrame4.Controls.Add(Me.txtPIN)
        Me.CareFrame4.Controls.Add(Me.CareLabel34)
        Me.CareFrame4.Location = New System.Drawing.Point(467, 7)
        Me.CareFrame4.Name = "CareFrame4"
        Me.CareFrame4.Size = New System.Drawing.Size(130, 90)
        Me.CareFrame4.TabIndex = 6
        Me.CareFrame4.Text = "Self-CheckIn"
        '
        'btnPIN
        '
        Me.btnPIN.Location = New System.Drawing.Point(5, 57)
        Me.btnPIN.Name = "btnPIN"
        Me.btnPIN.Size = New System.Drawing.Size(120, 25)
        Me.btnPIN.TabIndex = 25
        Me.btnPIN.Text = "Change PIN"
        '
        'txtPIN
        '
        Me.txtPIN.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtPIN.EnterMoveNextControl = True
        Me.txtPIN.Location = New System.Drawing.Point(30, 29)
        Me.txtPIN.MaxLength = 10
        Me.txtPIN.Name = "txtPIN"
        Me.txtPIN.NumericAllowNegatives = False
        Me.txtPIN.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtPIN.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPIN.Properties.AccessibleDescription = ""
        Me.txtPIN.Properties.AccessibleName = "Forename"
        Me.txtPIN.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPIN.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtPIN.Properties.Appearance.Options.UseFont = True
        Me.txtPIN.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPIN.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPIN.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPIN.Properties.MaxLength = 10
        Me.txtPIN.Properties.ReadOnly = True
        Me.txtPIN.Size = New System.Drawing.Size(95, 22)
        Me.txtPIN.TabIndex = 24
        Me.txtPIN.TabStop = False
        Me.txtPIN.Tag = ""
        Me.txtPIN.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPIN.ToolTipText = ""
        '
        'CareLabel34
        '
        Me.CareLabel34.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel34.Appearance.Options.UseFont = True
        Me.CareLabel34.Appearance.Options.UseTextOptions = True
        Me.CareLabel34.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel34.Location = New System.Drawing.Point(5, 32)
        Me.CareLabel34.Name = "CareLabel34"
        Me.CareLabel34.Size = New System.Drawing.Size(19, 15)
        Me.CareLabel34.TabIndex = 0
        Me.CareLabel34.Text = "PIN"
        Me.CareLabel34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl19
        '
        Me.GroupControl19.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl19.Appearance.Options.UseFont = True
        Me.GroupControl19.Controls.Add(Me.chkNappies)
        Me.GroupControl19.Controls.Add(Me.chkMilk)
        Me.GroupControl19.Controls.Add(Me.chkFoodDetail)
        Me.GroupControl19.Controls.Add(Me.CareLabel1)
        Me.GroupControl19.Controls.Add(Me.chkOffMenu)
        Me.GroupControl19.Controls.Add(Me.chkBabyFood)
        Me.GroupControl19.Controls.Add(Me.CareLabel12)
        Me.GroupControl19.Controls.Add(Me.Label35)
        Me.GroupControl19.Controls.Add(Me.Label25)
        Me.GroupControl19.Controls.Add(Me.Label26)
        Me.GroupControl19.Location = New System.Drawing.Point(238, 7)
        Me.GroupControl19.Name = "GroupControl19"
        Me.GroupControl19.Size = New System.Drawing.Size(223, 90)
        Me.GroupControl19.TabIndex = 1
        Me.GroupControl19.Text = "Touchscreen / Tablet Options"
        '
        'chkNappies
        '
        Me.chkNappies.EnterMoveNextControl = True
        Me.chkNappies.Location = New System.Drawing.Point(87, 23)
        Me.chkNappies.Name = "chkNappies"
        Me.chkNappies.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNappies.Properties.Appearance.Options.UseFont = True
        Me.chkNappies.Properties.Caption = ""
        Me.chkNappies.Size = New System.Drawing.Size(20, 19)
        Me.chkNappies.TabIndex = 1
        Me.chkNappies.Tag = "AEB"
        '
        'chkMilk
        '
        Me.chkMilk.EnterMoveNextControl = True
        Me.chkMilk.Location = New System.Drawing.Point(87, 45)
        Me.chkMilk.Name = "chkMilk"
        Me.chkMilk.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMilk.Properties.Appearance.Options.UseFont = True
        Me.chkMilk.Properties.Caption = ""
        Me.chkMilk.Size = New System.Drawing.Size(20, 19)
        Me.chkMilk.TabIndex = 3
        Me.chkMilk.Tag = "AEB"
        '
        'chkFoodDetail
        '
        Me.chkFoodDetail.EnterMoveNextControl = True
        Me.chkFoodDetail.Location = New System.Drawing.Point(87, 68)
        Me.chkFoodDetail.Name = "chkFoodDetail"
        Me.chkFoodDetail.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFoodDetail.Properties.Appearance.Options.UseFont = True
        Me.chkFoodDetail.Properties.Caption = ""
        Me.chkFoodDetail.Size = New System.Drawing.Size(20, 19)
        Me.chkFoodDetail.TabIndex = 5
        Me.chkFoodDetail.Tag = "AEB"
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel1.Appearance.Options.UseFont = True
        Me.CareLabel1.Appearance.Options.UseTextOptions = True
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(5, 70)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.CareLabel1.Size = New System.Drawing.Size(76, 15)
        Me.CareLabel1.TabIndex = 4
        Me.CareLabel1.Text = "Detailed Food"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkOffMenu
        '
        Me.chkOffMenu.EnterMoveNextControl = True
        Me.chkOffMenu.Location = New System.Drawing.Point(198, 45)
        Me.chkOffMenu.Name = "chkOffMenu"
        Me.chkOffMenu.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOffMenu.Properties.Appearance.Options.UseFont = True
        Me.chkOffMenu.Properties.Caption = ""
        Me.chkOffMenu.Size = New System.Drawing.Size(20, 19)
        Me.chkOffMenu.TabIndex = 9
        Me.chkOffMenu.Tag = "AEB"
        '
        'chkBabyFood
        '
        Me.chkBabyFood.EnterMoveNextControl = True
        Me.chkBabyFood.Location = New System.Drawing.Point(198, 23)
        Me.chkBabyFood.Name = "chkBabyFood"
        Me.chkBabyFood.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkBabyFood.Properties.Appearance.Options.UseFont = True
        Me.chkBabyFood.Properties.Caption = ""
        Me.chkBabyFood.Size = New System.Drawing.Size(20, 19)
        Me.chkBabyFood.TabIndex = 7
        Me.chkBabyFood.Tag = "AEB"
        '
        'CareLabel12
        '
        Me.CareLabel12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel12.Appearance.Options.UseFont = True
        Me.CareLabel12.Appearance.Options.UseTextOptions = True
        Me.CareLabel12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel12.Location = New System.Drawing.Point(129, 47)
        Me.CareLabel12.Name = "CareLabel12"
        Me.CareLabel12.Size = New System.Drawing.Size(63, 15)
        Me.CareLabel12.TabIndex = 8
        Me.CareLabel12.Text = "Blank Menu"
        Me.CareLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel12.ToolTip = "Do not display the standard menu on Touchscreens." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Food items will be added manua" &
    "lly." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.CareLabel12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.CareLabel12.ToolTipTitle = "Blank Menu"
        '
        'Label35
        '
        Me.Label35.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.Appearance.Options.UseFont = True
        Me.Label35.Appearance.Options.UseTextOptions = True
        Me.Label35.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label35.Location = New System.Drawing.Point(129, 23)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(61, 15)
        Me.Label35.TabIndex = 6
        Me.Label35.Text = "Baby Food?"
        Me.Label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label25
        '
        Me.Label25.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Appearance.Options.UseFont = True
        Me.Label25.Appearance.Options.UseTextOptions = True
        Me.Label25.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label25.Location = New System.Drawing.Point(5, 47)
        Me.Label25.Name = "Label25"
        Me.Label25.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.Label25.Size = New System.Drawing.Size(31, 15)
        Me.Label25.TabIndex = 2
        Me.Label25.Text = "Milk?"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label26
        '
        Me.Label26.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Appearance.Options.UseFont = True
        Me.Label26.Appearance.Options.UseTextOptions = True
        Me.Label26.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label26.Location = New System.Drawing.Point(5, 25)
        Me.Label26.Name = "Label26"
        Me.Label26.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.Label26.Size = New System.Drawing.Size(51, 15)
        Me.Label26.TabIndex = 0
        Me.Label26.Text = "Nappies?"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox10
        '
        Me.GroupBox10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupBox10.Appearance.Options.UseFont = True
        Me.GroupBox10.Controls.Add(Me.lblSleep)
        Me.GroupBox10.Controls.Add(Me.lblMedicine)
        Me.GroupBox10.Controls.Add(Me.chkSMSMilk)
        Me.GroupBox10.Controls.Add(Me.chkSMSSleep)
        Me.GroupBox10.Controls.Add(Me.lblObs)
        Me.GroupBox10.Controls.Add(Me.lblFood)
        Me.GroupBox10.Controls.Add(Me.lblMilk)
        Me.GroupBox10.Controls.Add(Me.chkSMSFood)
        Me.GroupBox10.Controls.Add(Me.chkSMSMedicine)
        Me.GroupBox10.Controls.Add(Me.lblNappy)
        Me.GroupBox10.Controls.Add(Me.chkSMSNappy)
        Me.GroupBox10.Controls.Add(Me.chkSMSObs)
        Me.GroupBox10.Location = New System.Drawing.Point(9, 7)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(223, 90)
        Me.GroupBox10.TabIndex = 0
        Me.GroupBox10.Text = "Realtime Communication"
        '
        'lblSleep
        '
        Me.lblSleep.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSleep.Appearance.Options.UseFont = True
        Me.lblSleep.Appearance.Options.UseTextOptions = True
        Me.lblSleep.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblSleep.Location = New System.Drawing.Point(5, 47)
        Me.lblSleep.Name = "lblSleep"
        Me.lblSleep.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.lblSleep.Size = New System.Drawing.Size(31, 15)
        Me.lblSleep.TabIndex = 2
        Me.lblSleep.Text = "Sleep"
        Me.lblSleep.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMedicine
        '
        Me.lblMedicine.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMedicine.Appearance.Options.UseFont = True
        Me.lblMedicine.Appearance.Options.UseTextOptions = True
        Me.lblMedicine.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblMedicine.Location = New System.Drawing.Point(5, 70)
        Me.lblMedicine.Name = "lblMedicine"
        Me.lblMedicine.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.lblMedicine.Size = New System.Drawing.Size(52, 15)
        Me.lblMedicine.TabIndex = 4
        Me.lblMedicine.Text = "Medicine"
        Me.lblMedicine.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkSMSMilk
        '
        Me.chkSMSMilk.EnterMoveNextControl = True
        Me.chkSMSMilk.Location = New System.Drawing.Point(66, 23)
        Me.chkSMSMilk.Name = "chkSMSMilk"
        Me.chkSMSMilk.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSMSMilk.Properties.Appearance.Options.UseFont = True
        Me.chkSMSMilk.Properties.Caption = ""
        Me.chkSMSMilk.Size = New System.Drawing.Size(20, 19)
        Me.chkSMSMilk.TabIndex = 1
        Me.chkSMSMilk.Tag = "AEB"
        '
        'chkSMSSleep
        '
        Me.chkSMSSleep.EnterMoveNextControl = True
        Me.chkSMSSleep.Location = New System.Drawing.Point(66, 45)
        Me.chkSMSSleep.Name = "chkSMSSleep"
        Me.chkSMSSleep.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSMSSleep.Properties.Appearance.Options.UseFont = True
        Me.chkSMSSleep.Properties.Caption = ""
        Me.chkSMSSleep.Size = New System.Drawing.Size(20, 19)
        Me.chkSMSSleep.TabIndex = 3
        Me.chkSMSSleep.Tag = "AEB"
        '
        'lblObs
        '
        Me.lblObs.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblObs.Appearance.Options.UseFont = True
        Me.lblObs.Appearance.Options.UseTextOptions = True
        Me.lblObs.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblObs.Location = New System.Drawing.Point(122, 70)
        Me.lblObs.Name = "lblObs"
        Me.lblObs.Size = New System.Drawing.Size(69, 15)
        Me.lblObs.TabIndex = 10
        Me.lblObs.Text = "Observations"
        Me.lblObs.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFood
        '
        Me.lblFood.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFood.Appearance.Options.UseFont = True
        Me.lblFood.Appearance.Options.UseTextOptions = True
        Me.lblFood.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblFood.Location = New System.Drawing.Point(122, 25)
        Me.lblFood.Name = "lblFood"
        Me.lblFood.Size = New System.Drawing.Size(27, 15)
        Me.lblFood.TabIndex = 6
        Me.lblFood.Text = "Food"
        Me.lblFood.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMilk
        '
        Me.lblMilk.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMilk.Appearance.Options.UseFont = True
        Me.lblMilk.Appearance.Options.UseTextOptions = True
        Me.lblMilk.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblMilk.Location = New System.Drawing.Point(5, 25)
        Me.lblMilk.Name = "lblMilk"
        Me.lblMilk.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.lblMilk.Size = New System.Drawing.Size(26, 15)
        Me.lblMilk.TabIndex = 0
        Me.lblMilk.Text = "Milk"
        Me.lblMilk.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkSMSFood
        '
        Me.chkSMSFood.EnterMoveNextControl = True
        Me.chkSMSFood.Location = New System.Drawing.Point(197, 23)
        Me.chkSMSFood.Name = "chkSMSFood"
        Me.chkSMSFood.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSMSFood.Properties.Appearance.Options.UseFont = True
        Me.chkSMSFood.Properties.Caption = ""
        Me.chkSMSFood.Size = New System.Drawing.Size(21, 19)
        Me.chkSMSFood.TabIndex = 7
        Me.chkSMSFood.Tag = "AEB"
        '
        'chkSMSMedicine
        '
        Me.chkSMSMedicine.EnterMoveNextControl = True
        Me.chkSMSMedicine.Location = New System.Drawing.Point(66, 68)
        Me.chkSMSMedicine.Name = "chkSMSMedicine"
        Me.chkSMSMedicine.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSMSMedicine.Properties.Appearance.Options.UseFont = True
        Me.chkSMSMedicine.Properties.Caption = ""
        Me.chkSMSMedicine.Size = New System.Drawing.Size(20, 19)
        Me.chkSMSMedicine.TabIndex = 5
        Me.chkSMSMedicine.Tag = "AEB"
        '
        'lblNappy
        '
        Me.lblNappy.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNappy.Appearance.Options.UseFont = True
        Me.lblNappy.Appearance.Options.UseTextOptions = True
        Me.lblNappy.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblNappy.Location = New System.Drawing.Point(122, 47)
        Me.lblNappy.Name = "lblNappy"
        Me.lblNappy.Size = New System.Drawing.Size(43, 15)
        Me.lblNappy.TabIndex = 8
        Me.lblNappy.Text = "Nappies"
        Me.lblNappy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkSMSNappy
        '
        Me.chkSMSNappy.EnterMoveNextControl = True
        Me.chkSMSNappy.Location = New System.Drawing.Point(197, 45)
        Me.chkSMSNappy.Name = "chkSMSNappy"
        Me.chkSMSNappy.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSMSNappy.Properties.Appearance.Options.UseFont = True
        Me.chkSMSNappy.Properties.Caption = ""
        Me.chkSMSNappy.Size = New System.Drawing.Size(21, 19)
        Me.chkSMSNappy.TabIndex = 9
        Me.chkSMSNappy.Tag = "AEB"
        '
        'chkSMSObs
        '
        Me.chkSMSObs.EnterMoveNextControl = True
        Me.chkSMSObs.Location = New System.Drawing.Point(197, 68)
        Me.chkSMSObs.Name = "chkSMSObs"
        Me.chkSMSObs.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSMSObs.Properties.Appearance.Options.UseFont = True
        Me.chkSMSObs.Properties.Caption = ""
        Me.chkSMSObs.Size = New System.Drawing.Size(21, 19)
        Me.chkSMSObs.TabIndex = 11
        Me.chkSMSObs.Tag = "AEB"
        '
        'GroupBox12
        '
        Me.GroupBox12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupBox12.Appearance.Options.UseFont = True
        Me.GroupBox12.Controls.Add(Me.Label42)
        Me.GroupBox12.Controls.Add(Me.cbxReportDetail)
        Me.GroupBox12.Controls.Add(Me.chkPaperReport)
        Me.GroupBox12.Location = New System.Drawing.Point(603, 7)
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.Size = New System.Drawing.Size(221, 90)
        Me.GroupBox12.TabIndex = 2
        Me.GroupBox12.Text = "Printed Daily Report"
        '
        'Label42
        '
        Me.Label42.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.Appearance.Options.UseFont = True
        Me.Label42.Appearance.Options.UseTextOptions = True
        Me.Label42.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label42.Location = New System.Drawing.Point(5, 32)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(42, 15)
        Me.Label42.TabIndex = 0
        Me.Label42.Text = "Enabled"
        Me.Label42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxReportDetail
        '
        Me.cbxReportDetail.AllowBlank = False
        Me.cbxReportDetail.DataSource = Nothing
        Me.cbxReportDetail.DisplayMember = Nothing
        Me.cbxReportDetail.EnterMoveNextControl = True
        Me.cbxReportDetail.Location = New System.Drawing.Point(79, 29)
        Me.cbxReportDetail.Name = "cbxReportDetail"
        Me.cbxReportDetail.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxReportDetail.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxReportDetail.Properties.Appearance.Options.UseFont = True
        Me.cbxReportDetail.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxReportDetail.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxReportDetail.SelectedValue = Nothing
        Me.cbxReportDetail.Size = New System.Drawing.Size(134, 22)
        Me.cbxReportDetail.TabIndex = 2
        Me.cbxReportDetail.Tag = "AEB"
        Me.cbxReportDetail.ValueMember = Nothing
        '
        'chkPaperReport
        '
        Me.chkPaperReport.EnterMoveNextControl = True
        Me.chkPaperReport.Location = New System.Drawing.Point(53, 30)
        Me.chkPaperReport.Name = "chkPaperReport"
        Me.chkPaperReport.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPaperReport.Properties.Appearance.Options.UseFont = True
        Me.chkPaperReport.Size = New System.Drawing.Size(20, 19)
        Me.chkPaperReport.TabIndex = 1
        Me.chkPaperReport.Tag = "AEB"
        '
        'btnEmail
        '
        Me.btnEmail.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnEmail.Appearance.Options.UseFont = True
        Me.btnEmail.Location = New System.Drawing.Point(165, 450)
        Me.btnEmail.Name = "btnEmail"
        Me.btnEmail.Size = New System.Drawing.Size(150, 23)
        Me.btnEmail.TabIndex = 5
        Me.btnEmail.Text = "Email selected Report"
        '
        'btnPrint
        '
        Me.btnPrint.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnPrint.Appearance.Options.UseFont = True
        Me.btnPrint.Location = New System.Drawing.Point(9, 450)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(150, 23)
        Me.btnPrint.TabIndex = 4
        Me.btnPrint.Text = "Print selected Report"
        '
        'grdReports
        '
        Me.grdReports.AllowBuildColumns = True
        Me.grdReports.AllowEdit = False
        Me.grdReports.AllowHorizontalScroll = False
        Me.grdReports.AllowMultiSelect = False
        Me.grdReports.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdReports.Appearance.Options.UseFont = True
        Me.grdReports.AutoSizeByData = True
        Me.grdReports.DisableAutoSize = False
        Me.grdReports.DisableDataFormatting = False
        Me.grdReports.FocusedRowHandle = -2147483648
        Me.grdReports.HideFirstColumn = False
        Me.grdReports.Location = New System.Drawing.Point(9, 103)
        Me.grdReports.Name = "grdReports"
        Me.grdReports.PreviewColumn = ""
        Me.grdReports.QueryID = Nothing
        Me.grdReports.RowAutoHeight = False
        Me.grdReports.SearchAsYouType = True
        Me.grdReports.ShowAutoFilterRow = False
        Me.grdReports.ShowFindPanel = False
        Me.grdReports.ShowGroupByBox = False
        Me.grdReports.ShowLoadingPanel = False
        Me.grdReports.ShowNavigator = False
        Me.grdReports.Size = New System.Drawing.Size(815, 341)
        Me.grdReports.TabIndex = 3
        '
        'tabAssessment
        '
        Me.tabAssessment.Controls.Add(Me.btnPrintProfile)
        Me.tabAssessment.Controls.Add(Me.GroupControl3)
        Me.tabAssessment.Controls.Add(Me.grdAssessment)
        Me.tabAssessment.Name = "tabAssessment"
        Me.tabAssessment.PageVisible = False
        Me.tabAssessment.Size = New System.Drawing.Size(832, 481)
        Me.tabAssessment.Text = "Observations"
        '
        'btnPrintProfile
        '
        Me.btnPrintProfile.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnPrintProfile.Appearance.Options.UseFont = True
        Me.btnPrintProfile.Location = New System.Drawing.Point(9, 451)
        Me.btnPrintProfile.Name = "btnPrintProfile"
        Me.btnPrintProfile.Size = New System.Drawing.Size(85, 23)
        Me.btnPrintProfile.TabIndex = 2
        Me.btnPrintProfile.Text = "Print Profile"
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.radCDAPNotAttained)
        Me.GroupControl3.Controls.Add(Me.radCDAPAttained)
        Me.GroupControl3.Controls.Add(Me.radCDAPAll)
        Me.GroupControl3.Location = New System.Drawing.Point(9, 7)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.ShowCaption = False
        Me.GroupControl3.Size = New System.Drawing.Size(815, 33)
        Me.GroupControl3.TabIndex = 0
        '
        'radCDAPNotAttained
        '
        Me.radCDAPNotAttained.CausesValidation = False
        Me.radCDAPNotAttained.Location = New System.Drawing.Point(164, 7)
        Me.radCDAPNotAttained.Name = "radCDAPNotAttained"
        Me.radCDAPNotAttained.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.radCDAPNotAttained.Properties.Appearance.Options.UseFont = True
        Me.radCDAPNotAttained.Properties.Appearance.Options.UseTextOptions = True
        Me.radCDAPNotAttained.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radCDAPNotAttained.Properties.AutoWidth = True
        Me.radCDAPNotAttained.Properties.Caption = "Not Attained"
        Me.radCDAPNotAttained.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radCDAPNotAttained.Properties.RadioGroupIndex = 1
        Me.radCDAPNotAttained.Size = New System.Drawing.Size(90, 19)
        Me.radCDAPNotAttained.TabIndex = 2
        Me.radCDAPNotAttained.TabStop = False
        '
        'radCDAPAttained
        '
        Me.radCDAPAttained.CausesValidation = False
        Me.radCDAPAttained.Location = New System.Drawing.Point(69, 7)
        Me.radCDAPAttained.Name = "radCDAPAttained"
        Me.radCDAPAttained.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.radCDAPAttained.Properties.Appearance.Options.UseFont = True
        Me.radCDAPAttained.Properties.Appearance.Options.UseTextOptions = True
        Me.radCDAPAttained.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radCDAPAttained.Properties.AutoWidth = True
        Me.radCDAPAttained.Properties.Caption = "Attained"
        Me.radCDAPAttained.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radCDAPAttained.Properties.RadioGroupIndex = 1
        Me.radCDAPAttained.Size = New System.Drawing.Size(67, 19)
        Me.radCDAPAttained.TabIndex = 1
        Me.radCDAPAttained.TabStop = False
        '
        'radCDAPAll
        '
        Me.radCDAPAll.CausesValidation = False
        Me.radCDAPAll.EditValue = True
        Me.radCDAPAll.Location = New System.Drawing.Point(10, 7)
        Me.radCDAPAll.Name = "radCDAPAll"
        Me.radCDAPAll.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.radCDAPAll.Properties.Appearance.Options.UseFont = True
        Me.radCDAPAll.Properties.Appearance.Options.UseTextOptions = True
        Me.radCDAPAll.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.radCDAPAll.Properties.AutoWidth = True
        Me.radCDAPAll.Properties.Caption = "All"
        Me.radCDAPAll.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radCDAPAll.Properties.RadioGroupIndex = 1
        Me.radCDAPAll.Size = New System.Drawing.Size(36, 19)
        Me.radCDAPAll.TabIndex = 0
        '
        'grdAssessment
        '
        Me.grdAssessment.AllowBuildColumns = True
        Me.grdAssessment.AllowEdit = False
        Me.grdAssessment.AllowHorizontalScroll = False
        Me.grdAssessment.AllowMultiSelect = False
        Me.grdAssessment.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdAssessment.Appearance.Options.UseFont = True
        Me.grdAssessment.AutoSizeByData = True
        Me.grdAssessment.DisableAutoSize = False
        Me.grdAssessment.DisableDataFormatting = False
        Me.grdAssessment.FocusedRowHandle = -2147483648
        Me.grdAssessment.HideFirstColumn = False
        Me.grdAssessment.Location = New System.Drawing.Point(9, 46)
        Me.grdAssessment.Name = "grdAssessment"
        Me.grdAssessment.PreviewColumn = ""
        Me.grdAssessment.QueryID = Nothing
        Me.grdAssessment.RowAutoHeight = False
        Me.grdAssessment.SearchAsYouType = True
        Me.grdAssessment.ShowAutoFilterRow = False
        Me.grdAssessment.ShowFindPanel = False
        Me.grdAssessment.ShowGroupByBox = False
        Me.grdAssessment.ShowLoadingPanel = False
        Me.grdAssessment.ShowNavigator = False
        Me.grdAssessment.Size = New System.Drawing.Size(815, 399)
        Me.grdAssessment.TabIndex = 1
        '
        'tabActivity
        '
        Me.tabActivity.Controls.Add(Me.grdActivity)
        Me.tabActivity.Name = "tabActivity"
        Me.tabActivity.Size = New System.Drawing.Size(832, 481)
        Me.tabActivity.Text = "Activity"
        '
        'grdActivity
        '
        Me.grdActivity.AllowBuildColumns = True
        Me.grdActivity.AllowEdit = False
        Me.grdActivity.AllowHorizontalScroll = False
        Me.grdActivity.AllowMultiSelect = False
        Me.grdActivity.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdActivity.Appearance.Options.UseFont = True
        Me.grdActivity.AutoSizeByData = True
        Me.grdActivity.DisableAutoSize = False
        Me.grdActivity.DisableDataFormatting = False
        Me.grdActivity.FocusedRowHandle = -2147483648
        Me.grdActivity.HideFirstColumn = False
        Me.grdActivity.Location = New System.Drawing.Point(6, 8)
        Me.grdActivity.Name = "grdActivity"
        Me.grdActivity.PreviewColumn = ""
        Me.grdActivity.QueryID = Nothing
        Me.grdActivity.RowAutoHeight = False
        Me.grdActivity.SearchAsYouType = True
        Me.grdActivity.ShowAutoFilterRow = False
        Me.grdActivity.ShowFindPanel = False
        Me.grdActivity.ShowGroupByBox = False
        Me.grdActivity.ShowLoadingPanel = False
        Me.grdActivity.ShowNavigator = False
        Me.grdActivity.Size = New System.Drawing.Size(818, 465)
        Me.grdActivity.TabIndex = 1
        '
        'ctxPopup
        '
        Me.ctxPopup.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuUpload, Me.mnuFullSize, Me.mnuDelete})
        Me.ctxPopup.Name = "ctxPopup"
        Me.ctxPopup.Size = New System.Drawing.Size(153, 70)
        '
        'mnuUpload
        '
        Me.mnuUpload.Name = "mnuUpload"
        Me.mnuUpload.Size = New System.Drawing.Size(152, 22)
        Me.mnuUpload.Text = "Upload Photo"
        '
        'mnuFullSize
        '
        Me.mnuFullSize.Name = "mnuFullSize"
        Me.mnuFullSize.Size = New System.Drawing.Size(152, 22)
        Me.mnuFullSize.Text = "View Full Size"
        '
        'mnuDelete
        '
        Me.mnuDelete.Name = "mnuDelete"
        Me.mnuDelete.Size = New System.Drawing.Size(152, 22)
        Me.mnuDelete.Text = "Remove Photo"
        '
        'btnLabelTablemats
        '
        Me.btnLabelTablemats.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnLabelTablemats.Appearance.Options.UseFont = True
        Me.btnLabelTablemats.CausesValidation = False
        Me.btnLabelTablemats.Location = New System.Drawing.Point(9, 4)
        Me.btnLabelTablemats.Name = "btnLabelTablemats"
        Me.btnLabelTablemats.Size = New System.Drawing.Size(180, 25)
        Me.btnLabelTablemats.TabIndex = 0
        Me.btnLabelTablemats.Tag = ""
        Me.btnLabelTablemats.Text = "Print Labels && Tablemats"
        '
        'bgwBookings
        '
        '
        'txtYellow
        '
        Me.txtYellow.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtYellow.Location = New System.Drawing.Point(500, 7)
        Me.txtYellow.Name = "txtYellow"
        Me.txtYellow.Properties.Appearance.BackColor = System.Drawing.Color.Khaki
        Me.txtYellow.Properties.Appearance.Options.UseBackColor = True
        Me.txtYellow.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtYellow, True)
        Me.txtYellow.Size = New System.Drawing.Size(31, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtYellow, OptionsSpelling3)
        Me.txtYellow.TabIndex = 15
        Me.txtYellow.TabStop = False
        Me.txtYellow.Visible = False
        '
        'txtRed
        '
        Me.txtRed.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtRed.Location = New System.Drawing.Point(537, 7)
        Me.txtRed.Name = "txtRed"
        Me.txtRed.Properties.Appearance.BackColor = System.Drawing.Color.LightCoral
        Me.txtRed.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRed.Properties.Appearance.Options.UseBackColor = True
        Me.txtRed.Properties.Appearance.Options.UseFont = True
        Me.txtRed.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtRed, True)
        Me.txtRed.Size = New System.Drawing.Size(31, 22)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtRed, OptionsSpelling2)
        Me.txtRed.TabIndex = 14
        Me.txtRed.TabStop = False
        Me.txtRed.Visible = False
        '
        'txtGreen
        '
        Me.txtGreen.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtGreen.Location = New System.Drawing.Point(463, 7)
        Me.txtGreen.Name = "txtGreen"
        Me.txtGreen.Properties.Appearance.BackColor = System.Drawing.Color.LightGreen
        Me.txtGreen.Properties.Appearance.Options.UseBackColor = True
        Me.txtGreen.Properties.ReadOnly = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtGreen, True)
        Me.txtGreen.Size = New System.Drawing.Size(31, 20)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtGreen, OptionsSpelling1)
        Me.txtGreen.TabIndex = 13
        Me.txtGreen.TabStop = False
        Me.txtGreen.Visible = False
        '
        'ssm
        '
        Me.ssm.ClosingDelay = 500
        '
        'btnPostPlacement
        '
        Me.btnPostPlacement.Location = New System.Drawing.Point(195, 4)
        Me.btnPostPlacement.Name = "btnPostPlacement"
        Me.btnPostPlacement.Size = New System.Drawing.Size(117, 25)
        Me.btnPostPlacement.TabIndex = 16
        Me.btnPostPlacement.Text = "Post Placement"
        '
        'frmChildren
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(858, 600)
        Me.Controls.Add(Me.tcMain)
        Me.MinimumSize = New System.Drawing.Size(544, 289)
        Me.Name = "frmChildren"
        Me.Text = "Children"
        Me.Controls.SetChildIndex(Me.tcMain, 0)
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.SchedulerStorage1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.GroupBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxKeyworker.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxGroup.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tcMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tcMain.ResumeLayout(False)
        Me.tabGeneral.ResumeLayout(False)
        CType(Me.GroupControl20, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl20.ResumeLayout(False)
        Me.GroupControl20.PerformLayout()
        CType(Me.cbxMoveMode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtMoveDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtMoveDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxMoveRoom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl18, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl18.ResumeLayout(False)
        CType(Me.txtTags.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.ccxNationality.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ccxLanguage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxEthnicity.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxReligion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl7.ResumeLayout(False)
        Me.GroupControl7.PerformLayout()
        CType(Me.txtSchoolRef2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSchoolRef1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSchool.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.txtKnownAs.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.udtLeft.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.udtLeft.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxClass.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxGender.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.udtDOB.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.udtDOB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.udtJoined.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.udtJoined.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtForename.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFamily.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabSessions.ResumeLayout(False)
        CType(Me.tcSessions, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tcSessions.ResumeLayout(False)
        Me.tabSessPatterns.ResumeLayout(False)
        CType(Me.gbxSessions, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxSessions.ResumeLayout(False)
        Me.gbxSessions.PerformLayout()
        CType(Me.radSessAll.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radSessAdditional.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radSessRecurring.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radSessOverride.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radSessSingle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radSessMulti.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabSessProfile.ResumeLayout(False)
        CType(Me.GroupControl21, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl21.ResumeLayout(False)
        Me.GroupControl21.PerformLayout()
        CType(Me.txtVoucherSequence.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtVoucherProportion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxVoucherMode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl17, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl17.ResumeLayout(False)
        Me.GroupControl17.PerformLayout()
        CType(Me.cbxTermType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkTermTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl14, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl14.ResumeLayout(False)
        Me.GroupControl14.PerformLayout()
        CType(Me.txtFundRef.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxFundType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl6.ResumeLayout(False)
        Me.GroupControl6.PerformLayout()
        CType(Me.cbxNLCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxNLTracking.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.txtPaymentRef.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxPaymentDue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxPaymentFreq.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxPaymentMethod.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxDiscount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabSessAnnual.ResumeLayout(False)
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.radCalcAnnual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radCalcMonthly.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radCalcWeekly.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radAnnualPatterns.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radAnnualManual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabSessMonthly.ResumeLayout(False)
        Me.tabSessHolidays.ResumeLayout(False)
        CType(Me.CareFrame3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CareFrame3.ResumeLayout(False)
        Me.CareFrame3.PerformLayout()
        CType(Me.radHolidaysAll.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radHolidays.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radAbsence.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabSessOther.ResumeLayout(False)
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl8.ResumeLayout(False)
        Me.GroupControl8.PerformLayout()
        CType(Me.radAddAll.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radAddMonthly.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radAddOneOff.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radAddWeekly.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabSessDeposits.ResumeLayout(False)
        CType(Me.CareFrame2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CareFrame2.ResumeLayout(False)
        Me.tabSessDiscounts.ResumeLayout(False)
        Me.tabInvoices.ResumeLayout(False)
        CType(Me.CareFrame1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CareFrame1.ResumeLayout(False)
        Me.CareFrame1.PerformLayout()
        CType(Me.radInvoicesPosted.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radNonPostedCredits.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radInvoices.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radPostedCredits.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabCalendar.ResumeLayout(False)
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.radBookCalendar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radBookRegister.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radBookActual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabHealth.ResumeLayout(False)
        CType(Me.tcHealth, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tcHealth.ResumeLayout(False)
        Me.tabAllergyDiet.ResumeLayout(False)
        CType(Me.GroupControl11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl11.ResumeLayout(False)
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl9.ResumeLayout(False)
        Me.GroupControl9.PerformLayout()
        CType(Me.cbxDietRestrict.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDietNotes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        CType(Me.cbxAllergyGrade.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMedAllergies.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabMedication.ResumeLayout(False)
        CType(Me.GroupBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        CType(Me.txtMedMedication.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox9.ResumeLayout(False)
        CType(Me.txtMedNotes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabMedContact.ResumeLayout(False)
        CType(Me.GroupControl10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl10.ResumeLayout(False)
        Me.GroupControl10.PerformLayout()
        CType(Me.txtHVName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        CType(Me.txtSurgDoctor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSurgName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabSEND.ResumeLayout(False)
        CType(Me.GroupControl12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl12.ResumeLayout(False)
        Me.GroupControl12.PerformLayout()
        CType(Me.chkSEN.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSENNotes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabImms.ResumeLayout(False)
        CType(Me.GroupControl16, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl16.ResumeLayout(False)
        CType(Me.gbxImm, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxImm.ResumeLayout(False)
        Me.gbxImm.PerformLayout()
        CType(Me.cbxImm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtImmDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtImmDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabConsent.ResumeLayout(False)
        CType(Me.GroupControl15, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl15.ResumeLayout(False)
        CType(Me.gbxConsent, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxConsent.ResumeLayout(False)
        Me.gbxConsent.PerformLayout()
        CType(Me.txtConsentItem.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxConsentContact.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtConsentDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtConsentDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabAttributes.ResumeLayout(False)
        CType(Me.GroupControl13, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl13.ResumeLayout(False)
        CType(Me.clbAttributes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabRepeatingMedication.ResumeLayout(False)
        CType(Me.gbxRepeatMedication, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxRepeatMedication.ResumeLayout(False)
        Me.gbxRepeatMedication.PerformLayout()
        CType(Me.txtManualTimeEntry.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStartTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAsRequired.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFrequency.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxDosageRequired.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxMedicationRequired.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxNatureOfIllness.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabMedical.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.radMedCalendar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radMedPriorIncidents.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radMedIncidents.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radMedAccidents.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radMedGiven.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radMedRequests.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabReports.ResumeLayout(False)
        CType(Me.CareFrame4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CareFrame4.ResumeLayout(False)
        Me.CareFrame4.PerformLayout()
        CType(Me.txtPIN.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl19, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl19.ResumeLayout(False)
        Me.GroupControl19.PerformLayout()
        CType(Me.chkNappies.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkMilk.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkFoodDetail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkOffMenu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkBabyFood.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox10.ResumeLayout(False)
        Me.GroupBox10.PerformLayout()
        CType(Me.chkSMSMilk.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSMSSleep.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSMSFood.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSMSMedicine.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSMSNappy.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSMSObs.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox12.ResumeLayout(False)
        Me.GroupBox12.PerformLayout()
        CType(Me.cbxReportDetail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkPaperReport.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabAssessment.ResumeLayout(False)
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.radCDAPNotAttained.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radCDAPAttained.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radCDAPAll.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabActivity.ResumeLayout(False)
        Me.ctxPopup.ResumeLayout(False)
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label13 As Care.Controls.CareLabel
    Friend WithEvents Label14 As Care.Controls.CareLabel
    Friend WithEvents cbxGroup As Care.Controls.CareComboBox
    Friend WithEvents cbxKeyworker As Care.Controls.CareComboBox
    Friend WithEvents OpenFileDialog As OpenFileDialog
    Friend WithEvents tcMain As Care.Controls.CareTab
    Friend WithEvents tabGeneral As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents Label12 As Care.Controls.CareLabel
    Friend WithEvents cbxGender As Care.Controls.CareComboBox
    Friend WithEvents lblAge As Care.Controls.CareLabel
    Friend WithEvents cbxStatus As Care.Controls.CareComboBox
    Friend WithEvents btnFamilyView As Care.Controls.CareButton
    Friend WithEvents btnFamilySet As Care.Controls.CareButton
    Friend WithEvents Label18 As Care.Controls.CareLabel
    Friend WithEvents Label16 As Care.Controls.CareLabel
    Private WithEvents udtDOB As Care.Controls.CareDateTime
    Friend WithEvents txtSurname As Care.Controls.CareTextBox
    Friend WithEvents lblFamily As Care.Controls.CareLabel
    Friend WithEvents Label4 As Care.Controls.CareLabel
    Private WithEvents udtJoined As Care.Controls.CareDateTime
    Friend WithEvents txtForename As Care.Controls.CareTextBox
    Friend WithEvents txtFamily As Care.Controls.CareTextBox
    Friend WithEvents Label2 As Care.Controls.CareLabel
    Friend WithEvents Label1 As Care.Controls.CareLabel
    Friend WithEvents tabHealth As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabSessions As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents btnAdvRemove As Care.Controls.CareButton
    Friend WithEvents btnAdvEdit As Care.Controls.CareButton
    Friend WithEvents btnAdvAdd As Care.Controls.CareButton
    Friend WithEvents tabReports As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents grdReports As Care.Controls.CareGrid
    Friend WithEvents btnEmail As Care.Controls.CareButton
    Friend WithEvents btnPrint As Care.Controls.CareButton
    Friend WithEvents ctxPopup As ContextMenuStrip
    Friend WithEvents mnuUpload As ToolStripMenuItem
    Friend WithEvents mnuFullSize As ToolStripMenuItem
    Friend WithEvents mnuDelete As ToolStripMenuItem
    Friend WithEvents tabMedical As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabActivity As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents grdActivity As Care.Controls.CareGrid
    Friend WithEvents grdMedical As Care.Controls.CareGrid
    Friend WithEvents radMedAccidents As Care.Controls.CareRadioButton
    Friend WithEvents radMedGiven As Care.Controls.CareRadioButton
    Friend WithEvents radMedRequests As Care.Controls.CareRadioButton
    Friend WithEvents tabAssessment As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents grdAssessment As Care.Controls.CareGrid
    Friend WithEvents radCDAPNotAttained As Care.Controls.CareRadioButton
    Friend WithEvents radCDAPAttained As Care.Controls.CareRadioButton
    Friend WithEvents radCDAPAll As Care.Controls.CareRadioButton
    Friend WithEvents btnPrintProfile As Care.Controls.CareButton
    Friend WithEvents tabCalendar As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents SchedulerStorage1 As DevExpress.XtraScheduler.SchedulerStorage
    Friend WithEvents PictureBox As Care.Controls.PhotoControl
    Friend WithEvents lblClass As Care.Controls.CareLabel
    Friend WithEvents cbxClass As Care.Controls.CareComboBox
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents lblHolidays As Care.Controls.CareLabel
    Friend WithEvents radBookRegister As Care.Controls.CareRadioButton
    Friend WithEvents radBookActual As Care.Controls.CareRadioButton
    Friend WithEvents btnBuildBookings As Care.Controls.CareButton
    Friend WithEvents btnLabelTablemats As Care.Controls.CareButton
    Friend WithEvents yvBookings As Nursery.YearView
    Friend WithEvents radBookCalendar As Care.Controls.CareRadioButton
    Friend WithEvents radMedPriorIncidents As Care.Controls.CareRadioButton
    Friend WithEvents radMedIncidents As Care.Controls.CareRadioButton
    Friend WithEvents btnMedPrint As Care.Controls.CareButton
    Friend WithEvents tcSessions As Care.Controls.CareTab
    Friend WithEvents tabSessPatterns As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabSessAnnual As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents radAnnualPatterns As Care.Controls.CareRadioButton
    Friend WithEvents radAnnualManual As Care.Controls.CareRadioButton
    Friend WithEvents grdAnnual As Care.Controls.CareGrid
    Friend WithEvents tabSessHolidays As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabSessOther As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents radAddAll As Care.Controls.CareRadioButton
    Friend WithEvents radAddMonthly As Care.Controls.CareRadioButton
    Friend WithEvents radAddOneOff As Care.Controls.CareRadioButton
    Friend WithEvents radAddWeekly As Care.Controls.CareRadioButton
    Friend WithEvents grdAdditional As Care.Controls.CareGrid
    Friend WithEvents tabSessDiscounts As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents grdDiscounts As Care.Controls.CareGrid
    Friend WithEvents cbxSchool As Care.Controls.CareComboBox
    Friend WithEvents CareLabel19 As Care.Controls.CareLabel
    Friend WithEvents tabSessMonthly As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents grdSessMonthly As Care.Controls.CareGrid
    Friend WithEvents bgwBookings As System.ComponentModel.BackgroundWorker
    Friend WithEvents radCalcAnnual As Care.Controls.CareRadioButton
    Friend WithEvents radCalcMonthly As Care.Controls.CareRadioButton
    Friend WithEvents radCalcWeekly As Care.Controls.CareRadioButton
    Friend WithEvents tcHealth As Care.Controls.CareTab
    Friend WithEvents tabAllergyDiet As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabMedContact As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabImms As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabConsent As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabAttributes As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents cbxDietRestrict As Care.Controls.CareComboBox
    Friend WithEvents txtDietNotes As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents cbxAllergyGrade As Care.Controls.CareComboBox
    Friend WithEvents txtMedAllergies As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents cbxEthnicity As Care.Controls.CareComboBox
    Friend WithEvents cbxReligion As Care.Controls.CareComboBox
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents CareLabel21 As Care.Controls.CareLabel
    Friend WithEvents lblStartDate As Care.Controls.CareLabel
    Friend WithEvents tabMedication As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents txtMedMedication As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtMedNotes As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtHVName As Care.Controls.CareTextBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents txtSurgDoctor As Care.Controls.CareTextBox
    Friend WithEvents Label33 As Care.Controls.CareLabel
    Friend WithEvents txtSurgName As Care.Controls.CareTextBox
    Friend WithEvents Label34 As Care.Controls.CareLabel
    Friend WithEvents Label32 As Care.Controls.CareLabel
    Friend WithEvents cgImms As Care.Controls.CareGridWithButtons
    Friend WithEvents clbAttributes As DevExpress.XtraEditors.CheckedListBoxControl
    Friend WithEvents lblLeaveDate As Care.Controls.CareLabel
    Friend WithEvents Label31 As Care.Controls.CareLabel
    Private WithEvents udtLeft As Care.Controls.CareDateTime
    Friend WithEvents cgAllergyMatrix As Care.Controls.CareGrid
    Friend WithEvents tabSEND As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents CareLabel22 As Care.Controls.CareLabel
    Friend WithEvents txtSENNotes As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents chkSEN As Care.Controls.CareCheckBox
    Friend WithEvents cgConsent As Care.Controls.CareGrid
    Friend WithEvents txtHVTelephone As Care.Shared.CareTelephoneNumber
    Friend WithEvents txtHVMobile As Care.Shared.CareTelephoneNumber
    Friend WithEvents txtHVEmail As Care.Shared.CareEmailAddress
    Friend WithEvents CareLabel23 As Care.Controls.CareLabel
    Friend WithEvents txtSurgTel As Care.Shared.CareTelephoneNumber
    Friend WithEvents btnImmCancel As Care.Controls.CareButton
    Friend WithEvents btnImmOK As Care.Controls.CareButton
    Friend WithEvents cbxImm As Care.Controls.CareComboBox
    Friend WithEvents CareLabel30 As Care.Controls.CareLabel
    Private WithEvents cdtImmDate As Care.Controls.CareDateTime
    Friend WithEvents CareLabel32 As Care.Controls.CareLabel
    Friend WithEvents btnConsentCancel As Care.Controls.CareButton
    Friend WithEvents btnConsentOK As Care.Controls.CareButton
    Friend WithEvents CareLabel25 As Care.Controls.CareLabel
    Private WithEvents cdtConsentDate As Care.Controls.CareDateTime
    Friend WithEvents CareLabel26 As Care.Controls.CareLabel
    Friend WithEvents cbxConsentContact As Care.Controls.CareComboBox
    Friend WithEvents CareLabel27 As Care.Controls.CareLabel
    Friend WithEvents txtConsentItem As Care.Controls.CareTextBox
    Friend WithEvents txtGreen As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRed As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtYellow As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btnDelete As Care.Controls.CareButton
    Friend WithEvents cgBookings As Care.Controls.CareGridWithButtons
    Friend WithEvents btnMedPrintAll As Care.Controls.CareButton
    Friend WithEvents txtTags As DevExpress.XtraEditors.TokenEdit
    Friend WithEvents txtKnownAs As Care.Controls.CareTextBox
    Friend WithEvents CareLabel40 As Care.Controls.CareLabel
    Friend WithEvents btnConsentClear As Care.Controls.CareButton
    Friend WithEvents CareLabel42 As Care.Controls.CareLabel
    Private WithEvents cdtMoveDate As Care.Controls.CareDateTime
    Friend WithEvents cbxMoveRoom As Care.Controls.CareComboBox
    Friend WithEvents CareLabel41 As Care.Controls.CareLabel
    Friend WithEvents txtSchoolRef2 As Care.Controls.CareTextBox
    Friend WithEvents txtSchoolRef1 As Care.Controls.CareTextBox
    Friend WithEvents Label42 As Care.Controls.CareLabel
    Friend WithEvents cbxReportDetail As Care.Controls.CareComboBox
    Friend WithEvents chkPaperReport As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel24 As Care.Controls.CareLabel
    Friend WithEvents cbxMoveMode As Care.Controls.CareComboBox
    Friend WithEvents CareLabel45 As Care.Controls.CareLabel
    Friend WithEvents CareLabel44 As Care.Controls.CareLabel
    Friend WithEvents lblMoveIn As Care.Controls.CareLabel
    Friend WithEvents ssm As DevExpress.XtraSplashScreen.SplashScreenManager
    Friend WithEvents tabSessProfile As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cbxTermType As Care.Controls.CareComboBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel20 As Care.Controls.CareLabel
    Friend WithEvents chkTermTime As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel37 As Care.Controls.CareLabel
    Friend WithEvents txtFundRef As Care.Controls.CareTextBox
    Friend WithEvents CareLabel36 As Care.Controls.CareLabel
    Friend WithEvents cbxFundType As Care.Controls.CareComboBox
    Friend WithEvents CareLabel18 As Care.Controls.CareLabel
    Friend WithEvents CareLabel15 As Care.Controls.CareLabel
    Friend WithEvents cbxNLCode As Care.Controls.CareComboBox
    Friend WithEvents cbxNLTracking As Care.Controls.CareComboBox
    Friend WithEvents txtPaymentRef As Care.Controls.CareTextBox
    Friend WithEvents CareLabel35 As Care.Controls.CareLabel
    Friend WithEvents cbxPaymentDue As Care.Controls.CareComboBox
    Friend WithEvents CareLabel39 As Care.Controls.CareLabel
    Friend WithEvents cbxPaymentFreq As Care.Controls.CareComboBox
    Friend WithEvents CareLabel38 As Care.Controls.CareLabel
    Friend WithEvents cbxPaymentMethod As Care.Controls.CareComboBox
    Friend WithEvents Label30 As Care.Controls.CareLabel
    Friend WithEvents cbxDiscount As Care.Controls.CareComboBox
    Friend WithEvents txtVoucherProportion As Care.Controls.CareTextBox
    Friend WithEvents lblSessVoucherValue As Care.Controls.CareLabel
    Friend WithEvents CareLabel10 As Care.Controls.CareLabel
    Friend WithEvents CareLabel16 As Care.Controls.CareLabel
    Friend WithEvents cbxVoucherMode As Care.Controls.CareComboBox
    Friend WithEvents radSessAll As Care.Controls.CareRadioButton
    Friend WithEvents radSessAdditional As Care.Controls.CareRadioButton
    Friend WithEvents radSessRecurring As Care.Controls.CareRadioButton
    Friend WithEvents radSessOverride As Care.Controls.CareRadioButton
    Friend WithEvents radSessSingle As Care.Controls.CareRadioButton
    Friend WithEvents radSessMulti As Care.Controls.CareRadioButton
    Friend WithEvents grdPatterns As Care.Controls.CareGrid
    Friend WithEvents txtVoucherSequence As Care.Controls.CareTextBox
    Friend WithEvents CareLabel13 As Care.Controls.CareLabel
    Friend WithEvents yvMedInc As YearView
    Friend WithEvents radMedCalendar As Care.Controls.CareRadioButton
    Friend WithEvents GroupBox4 As Care.Controls.CareFrame
    Friend WithEvents GroupBox3 As Care.Controls.CareFrame
    Friend WithEvents GroupBox1 As Care.Controls.CareFrame
    Friend WithEvents GroupControl2 As Care.Controls.CareFrame
    Friend WithEvents GroupControl3 As Care.Controls.CareFrame
    Friend WithEvents GroupControl5 As Care.Controls.CareFrame
    Friend WithEvents GroupControl4 As Care.Controls.CareFrame
    Friend WithEvents GroupControl8 As Care.Controls.CareFrame
    Friend WithEvents GroupControl7 As Care.Controls.CareFrame
    Friend WithEvents GroupControl16 As Care.Controls.CareFrame
    Friend WithEvents GroupControl15 As Care.Controls.CareFrame
    Friend WithEvents GroupControl13 As Care.Controls.CareFrame
    Friend WithEvents GroupControl9 As Care.Controls.CareFrame
    Friend WithEvents GroupBox8 As Care.Controls.CareFrame
    Friend WithEvents GroupControl1 As Care.Controls.CareFrame
    Friend WithEvents GroupBox6 As Care.Controls.CareFrame
    Friend WithEvents GroupBox9 As Care.Controls.CareFrame
    Friend WithEvents GroupControl10 As Care.Controls.CareFrame
    Friend WithEvents GroupBox7 As Care.Controls.CareFrame
    Friend WithEvents GroupControl11 As Care.Controls.CareFrame
    Friend WithEvents GroupControl12 As Care.Controls.CareFrame
    Friend WithEvents gbxImm As Care.Controls.CareFrame
    Friend WithEvents gbxConsent As Care.Controls.CareFrame
    Friend WithEvents GroupControl18 As Care.Controls.CareFrame
    Friend WithEvents GroupControl20 As Care.Controls.CareFrame
    Friend WithEvents GroupControl19 As Care.Controls.CareFrame
    Friend WithEvents GroupBox10 As Care.Controls.CareFrame
    Friend WithEvents GroupBox12 As Care.Controls.CareFrame
    Friend WithEvents GroupControl17 As Care.Controls.CareFrame
    Friend WithEvents GroupControl14 As Care.Controls.CareFrame
    Friend WithEvents GroupControl6 As Care.Controls.CareFrame
    Friend WithEvents GroupBox2 As Care.Controls.CareFrame
    Friend WithEvents GroupControl21 As Care.Controls.CareFrame
    Friend WithEvents gbxSessions As Care.Controls.CareFrame
    Friend WithEvents tabRepeatingMedication As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gbxRepeatMedication As Care.Controls.CareFrame
    Friend WithEvents txtManualTimeEntry As Care.Controls.CareTextBox
    Friend WithEvents txtStartTime As Care.Controls.CareTextBox
    Friend WithEvents btnManualTimeEntryRemove As Care.Controls.CareButton
    Friend WithEvents btnManualTimeEntryAdd As Care.Controls.CareButton
    Friend WithEvents chkAsRequired As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel47 As Care.Controls.CareLabel
    Friend WithEvents lblManualTimeEntries As Care.Controls.CareLabel
    Friend WithEvents CareLabel43 As Care.Controls.CareLabel
    Friend WithEvents CareLabel33 As Care.Controls.CareLabel
    Friend WithEvents btnRepeatingMedicationCancel As Care.Controls.CareButton
    Friend WithEvents btnRepeatingMedicationOK As Care.Controls.CareButton
    Friend WithEvents txtFrequency As Care.Controls.CareTextBox
    Friend WithEvents CareLabel31 As Care.Controls.CareLabel
    Friend WithEvents CareLabel29 As Care.Controls.CareLabel
    Friend WithEvents cbxDosageRequired As Care.Controls.CareComboBox
    Friend WithEvents CareLabel28 As Care.Controls.CareLabel
    Friend WithEvents cbxMedicationRequired As Care.Controls.CareComboBox
    Friend WithEvents CareLabel17 As Care.Controls.CareLabel
    Friend WithEvents cbxNatureOfIllness As Care.Controls.CareComboBox
    Friend WithEvents cgRepeatingMedication As Care.Controls.CareGridWithButtons
    Friend WithEvents ccxLanguage As Care.Controls.CareCheckedComboBox
    Friend WithEvents ccxNationality As Care.Controls.CareCheckedComboBox
    Friend WithEvents btnPostPlacement As Care.Controls.CareButton
    Friend WithEvents tabInvoices As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents CareFrame1 As Care.Controls.CareFrame
    Friend WithEvents radInvoicesPosted As Care.Controls.CareRadioButton
    Friend WithEvents radNonPostedCredits As Care.Controls.CareRadioButton
    Friend WithEvents radInvoices As Care.Controls.CareRadioButton
    Friend WithEvents radPostedCredits As Care.Controls.CareRadioButton
    Friend WithEvents cgInvoices As Care.Controls.CareGrid
    Friend WithEvents tabSessDeposits As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents CareFrame2 As Care.Controls.CareFrame
    Friend WithEvents lblDepositBalance As Care.Controls.CareLabel
    Friend WithEvents grdDeposits As Care.Controls.CareGrid
    Friend WithEvents CareFrame3 As Care.Controls.CareFrame
    Friend WithEvents radHolidaysAll As Care.Controls.CareRadioButton
    Friend WithEvents radHolidays As Care.Controls.CareRadioButton
    Friend WithEvents radAbsence As Care.Controls.CareRadioButton
    Friend WithEvents grdHolidays As Care.Controls.CareGrid
    Friend WithEvents chkFoodDetail As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents chkMilk As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel12 As Care.Controls.CareLabel
    Friend WithEvents Label26 As Care.Controls.CareLabel
    Friend WithEvents chkOffMenu As Care.Controls.CareCheckBox
    Friend WithEvents chkNappies As Care.Controls.CareCheckBox
    Friend WithEvents Label25 As Care.Controls.CareLabel
    Friend WithEvents Label35 As Care.Controls.CareLabel
    Friend WithEvents chkBabyFood As Care.Controls.CareCheckBox
    Friend WithEvents chkSMSMedicine As Care.Controls.CareCheckBox
    Friend WithEvents chkSMSObs As Care.Controls.CareCheckBox
    Friend WithEvents chkSMSMilk As Care.Controls.CareCheckBox
    Friend WithEvents chkSMSSleep As Care.Controls.CareCheckBox
    Friend WithEvents chkSMSFood As Care.Controls.CareCheckBox
    Friend WithEvents chkSMSNappy As Care.Controls.CareCheckBox
    Friend WithEvents lblMedicine As Care.Controls.CareLabel
    Friend WithEvents lblObs As Care.Controls.CareLabel
    Friend WithEvents lblSleep As Care.Controls.CareLabel
    Friend WithEvents lblMilk As Care.Controls.CareLabel
    Friend WithEvents lblNappy As Care.Controls.CareLabel
    Friend WithEvents lblFood As Care.Controls.CareLabel
    Friend WithEvents CareFrame4 As Care.Controls.CareFrame
    Friend WithEvents txtPIN As Care.Controls.CareTextBox
    Friend WithEvents CareLabel34 As Care.Controls.CareLabel
    Friend WithEvents btnPIN As Care.Controls.CareButton
End Class
