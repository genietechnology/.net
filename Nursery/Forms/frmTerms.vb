﻿Imports Care.Global
Imports Care.Data

Public Class frmTerms

    Dim m_Term As Business.Term = Nothing

    Private Sub frmTerms_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        With cbxTermType
            .PopulateFromListMaintenance(Care.Shared.ListHandler.ReturnListFromCache("Term Type"))
        End With

        Me.GridSQL = "select id, name as 'Name', term_type as 'Type', term_start as 'Term Starts', term_end as 'Term Ends'," & _
                     " datediff(wk, term_start, term_end) - isnull(datediff(wk, ht_start, ht_end),0) as 'Weeks'," & _
                     " ht_start as 'Half Term Starts', ht_end as 'Half Term Ends'," & _
                     " isnull(datediff(wk, ht_start, ht_end),0) as 'HT Weeks'" & _
                     " from Terms" & _
                     " order by term_start, term_type"
        gbx.Hide()

    End Sub

    Protected Overrides Sub SetBindings()
        'not used
    End Sub

    Protected Overrides Sub BindToID(ID As System.Guid, IsNew As Boolean)

        If IsNew Then
            m_Term = New Business.Term
        Else
            m_Term = Business.Term.RetreiveByID(ID)
        End If

        txtName.Text = m_Term._Name
        cbxTermType.Text = m_Term._TermType
        dtStarts.Value = m_Term._TermStart
        dtEnds.Value = m_Term._TermEnd
        dtHTStarts.Value = m_Term._HtStart
        dtHTEnds.Value = m_Term._HtEnd

    End Sub

    Protected Overrides Sub AfterAdd()
        cbxTermType.SelectedIndex = 0
    End Sub

    Protected Overrides Function BeforeCommitUpdate() As Boolean

        If txtName.Text = "" Then
            CareMessage("Please enter the Term Name.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If dtStarts.Text = "" Then
            CareMessage("Please enter the Term Start date.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If dtEnds.Text = "" Then
            CareMessage("Please enter the Term End date.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        Return True

    End Function

    Protected Overrides Sub CommitUpdate()

        m_Term._Name = txtName.Text
        m_Term._TermType = cbxTermType.Text
        m_Term._TermStart = dtStarts.Value
        m_Term._TermEnd = dtEnds.Value
        m_Term._HtStart = dtHTStarts.Value
        m_Term._HtEnd = dtHTEnds.Value

        m_Term.Store()

    End Sub

    Protected Overrides Sub CommitDelete(ByVal ID As Guid)

        Dim _SQL As String = "delete from Terms where ID = '" + ID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

    End Sub

End Class