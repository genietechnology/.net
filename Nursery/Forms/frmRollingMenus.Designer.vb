﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRollingMenus
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupBox1 = New Care.Controls.CareFrame()
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.cdtStart = New Care.Controls.CareDateTime(Me.components)
        Me.lblFamily = New Care.Controls.CareLabel(Me.components)
        Me.txtName = New Care.Controls.CareTextBox(Me.components)
        Me.btnRemove = New Care.Controls.CareButton(Me.components)
        Me.gbxWeeks = New Care.Controls.CareFrame()
        Me.grdWeeks = New Care.Controls.CareGrid()
        Me.btnAdd = New Care.Controls.CareButton(Me.components)
        Me.gbxDays = New Care.Controls.CareFrame()
        Me.grdDays = New Care.Controls.CareGrid()
        Me.gbxMeals = New Care.Controls.CareFrame()
        Me.btnDupTeaDessert = New Care.Controls.CareButton(Me.components)
        Me.btnDupLunchDessert = New Care.Controls.CareButton(Me.components)
        Me.btnDupTea = New Care.Controls.CareButton(Me.components)
        Me.btnDupLunch = New Care.Controls.CareButton(Me.components)
        Me.btnDupSnack = New Care.Controls.CareButton(Me.components)
        Me.btnDupBreakfast = New Care.Controls.CareButton(Me.components)
        Me.btnDayCancel = New Care.Controls.CareButton(Me.components)
        Me.btnDayOK = New Care.Controls.CareButton(Me.components)
        Me.btnClearTeaDessert = New Care.Controls.CareButton(Me.components)
        Me.btnSelectTeaDessert = New Care.Controls.CareButton(Me.components)
        Me.txtTeaDessert = New Care.Controls.CareTextBox(Me.components)
        Me.Label8 = New Care.Controls.CareLabel(Me.components)
        Me.btnClearLunchDessert = New Care.Controls.CareButton(Me.components)
        Me.btnSelectLunchDessert = New Care.Controls.CareButton(Me.components)
        Me.txtLunchDessert = New Care.Controls.CareTextBox(Me.components)
        Me.Label7 = New Care.Controls.CareLabel(Me.components)
        Me.btnClearTea = New Care.Controls.CareButton(Me.components)
        Me.btnSelectTea = New Care.Controls.CareButton(Me.components)
        Me.txtTea = New Care.Controls.CareTextBox(Me.components)
        Me.Label5 = New Care.Controls.CareLabel(Me.components)
        Me.btnClearLunch = New Care.Controls.CareButton(Me.components)
        Me.btnSelectLunch = New Care.Controls.CareButton(Me.components)
        Me.txtLunch = New Care.Controls.CareTextBox(Me.components)
        Me.Label4 = New Care.Controls.CareLabel(Me.components)
        Me.btnClearSnack = New Care.Controls.CareButton(Me.components)
        Me.btnSelectSnack = New Care.Controls.CareButton(Me.components)
        Me.txtSnack = New Care.Controls.CareTextBox(Me.components)
        Me.Label3 = New Care.Controls.CareLabel(Me.components)
        Me.btnClearBreakfast = New Care.Controls.CareButton(Me.components)
        Me.btnSelectBreakfast = New Care.Controls.CareButton(Me.components)
        Me.txtBreakfast = New Care.Controls.CareTextBox(Me.components)
        Me.Label1 = New Care.Controls.CareLabel(Me.components)
        Me.btnDupPMSnack = New Care.Controls.CareButton(Me.components)
        Me.btnClearPMSnack = New Care.Controls.CareButton(Me.components)
        Me.btnSelectPMSnack = New Care.Controls.CareButton(Me.components)
        Me.txtPMSnack = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.cdtStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxWeeks, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxWeeks.SuspendLayout()
        CType(Me.gbxDays, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxDays.SuspendLayout()
        CType(Me.gbxMeals, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxMeals.SuspendLayout()
        CType(Me.txtTeaDessert.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLunchDessert.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTea.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLunch.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSnack.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBreakfast.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPMSnack.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(762, 3)
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(671, 3)
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(0, 404)
        Me.Panel1.Size = New System.Drawing.Size(859, 36)
        Me.Panel1.TabIndex = 2
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CareLabel1)
        Me.GroupBox1.Controls.Add(Me.cdtStart)
        Me.GroupBox1.Controls.Add(Me.lblFamily)
        Me.GroupBox1.Controls.Add(Me.txtName)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 53)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(835, 56)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.Text = "Season Details"
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(514, 31)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(51, 15)
        Me.CareLabel1.TabIndex = 2
        Me.CareLabel1.Text = "Start Date"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtStart
        '
        Me.cdtStart.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtStart.EnterMoveNextControl = True
        Me.cdtStart.Location = New System.Drawing.Point(571, 28)
        Me.cdtStart.Name = "cdtStart"
        Me.cdtStart.Properties.AccessibleName = "Season Start Date"
        Me.cdtStart.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtStart.Properties.Appearance.Options.UseFont = True
        Me.cdtStart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtStart.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtStart.Size = New System.Drawing.Size(80, 22)
        Me.cdtStart.TabIndex = 3
        Me.cdtStart.Tag = "AEM"
        Me.cdtStart.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'lblFamily
        '
        Me.lblFamily.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblFamily.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblFamily.Location = New System.Drawing.Point(11, 31)
        Me.lblFamily.Name = "lblFamily"
        Me.lblFamily.Size = New System.Drawing.Size(32, 15)
        Me.lblFamily.TabIndex = 0
        Me.lblFamily.Text = "Name"
        Me.lblFamily.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.CharacterCasing = CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(72, 28)
        Me.txtName.MaxLength = 0
        Me.txtName.Name = "txtName"
        Me.txtName.NumericAllowNegatives = False
        Me.txtName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AccessibleName = "Season Name"
        Me.txtName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtName.Size = New System.Drawing.Size(418, 22)
        Me.txtName.TabIndex = 1
        Me.txtName.Tag = "AEM"
        Me.txtName.TextAlign = HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'btnRemove
        '
        Me.btnRemove.Anchor = AnchorStyles.Bottom
        Me.btnRemove.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnRemove.Appearance.Options.UseFont = True
        Me.btnRemove.CausesValidation = False
        Me.btnRemove.Location = New System.Drawing.Point(5, 255)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(55, 23)
        Me.btnRemove.TabIndex = 2
        Me.btnRemove.Tag = "E"
        Me.btnRemove.Text = "Remove"
        '
        'gbxWeeks
        '
        Me.gbxWeeks.Controls.Add(Me.grdWeeks)
        Me.gbxWeeks.Controls.Add(Me.btnAdd)
        Me.gbxWeeks.Controls.Add(Me.btnRemove)
        Me.gbxWeeks.Location = New System.Drawing.Point(12, 115)
        Me.gbxWeeks.Name = "gbxWeeks"
        Me.gbxWeeks.Size = New System.Drawing.Size(66, 283)
        Me.gbxWeeks.TabIndex = 1
        Me.gbxWeeks.Text = "Weeks"
        '
        'grdWeeks
        '
        Me.grdWeeks.AllowBuildColumns = True
        Me.grdWeeks.AllowEdit = False
        Me.grdWeeks.AllowHorizontalScroll = False
        Me.grdWeeks.AllowMultiSelect = False
        Me.grdWeeks.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.grdWeeks.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdWeeks.Appearance.Options.UseFont = True
        Me.grdWeeks.AutoSizeByData = True
        Me.grdWeeks.DisableAutoSize = False
        Me.grdWeeks.DisableDataFormatting = False
        Me.grdWeeks.FocusedRowHandle = -2147483648
        Me.grdWeeks.HideFirstColumn = False
        Me.grdWeeks.Location = New System.Drawing.Point(5, 24)
        Me.grdWeeks.Name = "grdWeeks"
        Me.grdWeeks.PreviewColumn = ""
        Me.grdWeeks.QueryID = Nothing
        Me.grdWeeks.RowAutoHeight = False
        Me.grdWeeks.SearchAsYouType = True
        Me.grdWeeks.ShowAutoFilterRow = False
        Me.grdWeeks.ShowFindPanel = False
        Me.grdWeeks.ShowGroupByBox = False
        Me.grdWeeks.ShowLoadingPanel = False
        Me.grdWeeks.ShowNavigator = False
        Me.grdWeeks.Size = New System.Drawing.Size(55, 196)
        Me.grdWeeks.TabIndex = 0
        Me.grdWeeks.TabStop = False
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = AnchorStyles.Bottom
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnAdd.Appearance.Options.UseFont = True
        Me.btnAdd.CausesValidation = False
        Me.btnAdd.Location = New System.Drawing.Point(5, 226)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(55, 23)
        Me.btnAdd.TabIndex = 1
        Me.btnAdd.Tag = "E"
        Me.btnAdd.Text = "Add"
        '
        'gbxDays
        '
        Me.gbxDays.Controls.Add(Me.gbxMeals)
        Me.gbxDays.Controls.Add(Me.grdDays)
        Me.gbxDays.Location = New System.Drawing.Point(84, 115)
        Me.gbxDays.Name = "gbxDays"
        Me.gbxDays.Size = New System.Drawing.Size(763, 283)
        Me.gbxDays.TabIndex = 7
        Me.gbxDays.Text = "Days (Double-Click to change)"
        '
        'grdDays
        '
        Me.grdDays.AllowBuildColumns = True
        Me.grdDays.AllowEdit = False
        Me.grdDays.AllowHorizontalScroll = False
        Me.grdDays.AllowMultiSelect = False
        Me.grdDays.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.grdDays.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdDays.Appearance.Options.UseFont = True
        Me.grdDays.AutoSizeByData = True
        Me.grdDays.DisableAutoSize = False
        Me.grdDays.DisableDataFormatting = False
        Me.grdDays.FocusedRowHandle = -2147483648
        Me.grdDays.HideFirstColumn = False
        Me.grdDays.Location = New System.Drawing.Point(5, 24)
        Me.grdDays.Name = "grdDays"
        Me.grdDays.PreviewColumn = ""
        Me.grdDays.QueryID = Nothing
        Me.grdDays.RowAutoHeight = False
        Me.grdDays.SearchAsYouType = True
        Me.grdDays.ShowAutoFilterRow = False
        Me.grdDays.ShowFindPanel = False
        Me.grdDays.ShowGroupByBox = False
        Me.grdDays.ShowLoadingPanel = False
        Me.grdDays.ShowNavigator = False
        Me.grdDays.Size = New System.Drawing.Size(753, 254)
        Me.grdDays.TabIndex = 9
        Me.grdDays.TabStop = False
        '
        'gbxMeals
        '
        Me.gbxMeals.Controls.Add(Me.btnDupPMSnack)
        Me.gbxMeals.Controls.Add(Me.btnClearPMSnack)
        Me.gbxMeals.Controls.Add(Me.btnSelectPMSnack)
        Me.gbxMeals.Controls.Add(Me.txtPMSnack)
        Me.gbxMeals.Controls.Add(Me.CareLabel2)
        Me.gbxMeals.Controls.Add(Me.btnDupTeaDessert)
        Me.gbxMeals.Controls.Add(Me.btnDupLunchDessert)
        Me.gbxMeals.Controls.Add(Me.btnDupTea)
        Me.gbxMeals.Controls.Add(Me.btnDupLunch)
        Me.gbxMeals.Controls.Add(Me.btnDupSnack)
        Me.gbxMeals.Controls.Add(Me.btnDupBreakfast)
        Me.gbxMeals.Controls.Add(Me.btnDayCancel)
        Me.gbxMeals.Controls.Add(Me.btnDayOK)
        Me.gbxMeals.Controls.Add(Me.btnClearTeaDessert)
        Me.gbxMeals.Controls.Add(Me.btnSelectTeaDessert)
        Me.gbxMeals.Controls.Add(Me.txtTeaDessert)
        Me.gbxMeals.Controls.Add(Me.Label8)
        Me.gbxMeals.Controls.Add(Me.btnClearLunchDessert)
        Me.gbxMeals.Controls.Add(Me.btnSelectLunchDessert)
        Me.gbxMeals.Controls.Add(Me.txtLunchDessert)
        Me.gbxMeals.Controls.Add(Me.Label7)
        Me.gbxMeals.Controls.Add(Me.btnClearTea)
        Me.gbxMeals.Controls.Add(Me.btnSelectTea)
        Me.gbxMeals.Controls.Add(Me.txtTea)
        Me.gbxMeals.Controls.Add(Me.Label5)
        Me.gbxMeals.Controls.Add(Me.btnClearLunch)
        Me.gbxMeals.Controls.Add(Me.btnSelectLunch)
        Me.gbxMeals.Controls.Add(Me.txtLunch)
        Me.gbxMeals.Controls.Add(Me.Label4)
        Me.gbxMeals.Controls.Add(Me.btnClearSnack)
        Me.gbxMeals.Controls.Add(Me.btnSelectSnack)
        Me.gbxMeals.Controls.Add(Me.txtSnack)
        Me.gbxMeals.Controls.Add(Me.Label3)
        Me.gbxMeals.Controls.Add(Me.btnClearBreakfast)
        Me.gbxMeals.Controls.Add(Me.btnSelectBreakfast)
        Me.gbxMeals.Controls.Add(Me.txtBreakfast)
        Me.gbxMeals.Controls.Add(Me.Label1)
        Me.gbxMeals.Location = New System.Drawing.Point(0, 0)
        Me.gbxMeals.Name = "gbxMeals"
        Me.gbxMeals.Size = New System.Drawing.Size(763, 283)
        Me.gbxMeals.TabIndex = 0
        '
        'btnDupTeaDessert
        '
        Me.btnDupTeaDessert.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnDupTeaDessert.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnDupTeaDessert.Appearance.Options.UseFont = True
        Me.btnDupTeaDessert.Location = New System.Drawing.Point(499, 208)
        Me.btnDupTeaDessert.Name = "btnDupTeaDessert"
        Me.btnDupTeaDessert.Size = New System.Drawing.Size(141, 24)
        Me.btnDupTeaDessert.TabIndex = 32
        Me.btnDupTeaDessert.Tag = "E"
        Me.btnDupTeaDessert.Text = "Duplicate the Week"
        '
        'btnDupLunchDessert
        '
        Me.btnDupLunchDessert.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnDupLunchDessert.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnDupLunchDessert.Appearance.Options.UseFont = True
        Me.btnDupLunchDessert.Location = New System.Drawing.Point(499, 118)
        Me.btnDupLunchDessert.Name = "btnDupLunchDessert"
        Me.btnDupLunchDessert.Size = New System.Drawing.Size(141, 24)
        Me.btnDupLunchDessert.TabIndex = 17
        Me.btnDupLunchDessert.Tag = "E"
        Me.btnDupLunchDessert.Text = "Duplicate the Week"
        '
        'btnDupTea
        '
        Me.btnDupTea.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnDupTea.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnDupTea.Appearance.Options.UseFont = True
        Me.btnDupTea.Location = New System.Drawing.Point(499, 178)
        Me.btnDupTea.Name = "btnDupTea"
        Me.btnDupTea.Size = New System.Drawing.Size(141, 24)
        Me.btnDupTea.TabIndex = 27
        Me.btnDupTea.Tag = "E"
        Me.btnDupTea.Text = "Duplicate the Week"
        '
        'btnDupLunch
        '
        Me.btnDupLunch.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnDupLunch.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnDupLunch.Appearance.Options.UseFont = True
        Me.btnDupLunch.Location = New System.Drawing.Point(499, 88)
        Me.btnDupLunch.Name = "btnDupLunch"
        Me.btnDupLunch.Size = New System.Drawing.Size(141, 24)
        Me.btnDupLunch.TabIndex = 12
        Me.btnDupLunch.Tag = "E"
        Me.btnDupLunch.Text = "Duplicate the Week"
        '
        'btnDupSnack
        '
        Me.btnDupSnack.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnDupSnack.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnDupSnack.Appearance.Options.UseFont = True
        Me.btnDupSnack.Location = New System.Drawing.Point(499, 58)
        Me.btnDupSnack.Name = "btnDupSnack"
        Me.btnDupSnack.Size = New System.Drawing.Size(141, 24)
        Me.btnDupSnack.TabIndex = 7
        Me.btnDupSnack.Tag = "E"
        Me.btnDupSnack.Text = "Duplicate the Week"
        '
        'btnDupBreakfast
        '
        Me.btnDupBreakfast.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnDupBreakfast.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnDupBreakfast.Appearance.Options.UseFont = True
        Me.btnDupBreakfast.Location = New System.Drawing.Point(499, 28)
        Me.btnDupBreakfast.Name = "btnDupBreakfast"
        Me.btnDupBreakfast.Size = New System.Drawing.Size(141, 24)
        Me.btnDupBreakfast.TabIndex = 2
        Me.btnDupBreakfast.Tag = "E"
        Me.btnDupBreakfast.Text = "Duplicate the Week"
        '
        'btnDayCancel
        '
        Me.btnDayCancel.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnDayCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnDayCancel.Appearance.Options.UseFont = True
        Me.btnDayCancel.Location = New System.Drawing.Point(699, 251)
        Me.btnDayCancel.Name = "btnDayCancel"
        Me.btnDayCancel.Size = New System.Drawing.Size(50, 24)
        Me.btnDayCancel.TabIndex = 36
        Me.btnDayCancel.Tag = "E"
        Me.btnDayCancel.Text = "Cancel"
        '
        'btnDayOK
        '
        Me.btnDayOK.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnDayOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnDayOK.Appearance.Options.UseFont = True
        Me.btnDayOK.Location = New System.Drawing.Point(646, 251)
        Me.btnDayOK.Name = "btnDayOK"
        Me.btnDayOK.Size = New System.Drawing.Size(50, 24)
        Me.btnDayOK.TabIndex = 35
        Me.btnDayOK.Tag = "E"
        Me.btnDayOK.Text = "OK"
        '
        'btnClearTeaDessert
        '
        Me.btnClearTeaDessert.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnClearTeaDessert.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClearTeaDessert.Appearance.Options.UseFont = True
        Me.btnClearTeaDessert.Location = New System.Drawing.Point(699, 208)
        Me.btnClearTeaDessert.Name = "btnClearTeaDessert"
        Me.btnClearTeaDessert.Size = New System.Drawing.Size(50, 24)
        Me.btnClearTeaDessert.TabIndex = 34
        Me.btnClearTeaDessert.Tag = "E"
        Me.btnClearTeaDessert.Text = "Clear"
        '
        'btnSelectTeaDessert
        '
        Me.btnSelectTeaDessert.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnSelectTeaDessert.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSelectTeaDessert.Appearance.Options.UseFont = True
        Me.btnSelectTeaDessert.Location = New System.Drawing.Point(646, 208)
        Me.btnSelectTeaDessert.Name = "btnSelectTeaDessert"
        Me.btnSelectTeaDessert.Size = New System.Drawing.Size(50, 24)
        Me.btnSelectTeaDessert.TabIndex = 33
        Me.btnSelectTeaDessert.Tag = "E"
        Me.btnSelectTeaDessert.Text = "Select"
        '
        'txtTeaDessert
        '
        Me.txtTeaDessert.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtTeaDessert.CharacterCasing = CharacterCasing.Normal
        Me.txtTeaDessert.EnterMoveNextControl = True
        Me.txtTeaDessert.Location = New System.Drawing.Point(75, 208)
        Me.txtTeaDessert.MaxLength = 0
        Me.txtTeaDessert.Name = "txtTeaDessert"
        Me.txtTeaDessert.NumericAllowNegatives = False
        Me.txtTeaDessert.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtTeaDessert.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTeaDessert.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtTeaDessert.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtTeaDessert.Properties.Appearance.Options.UseFont = True
        Me.txtTeaDessert.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTeaDessert.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtTeaDessert.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTeaDessert.Properties.ReadOnly = True
        Me.txtTeaDessert.Size = New System.Drawing.Size(418, 22)
        Me.txtTeaDessert.TabIndex = 31
        Me.txtTeaDessert.Tag = ""
        Me.txtTeaDessert.TextAlign = HorizontalAlignment.Left
        Me.txtTeaDessert.ToolTipText = ""
        '
        'Label8
        '
        Me.Label8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label8.Location = New System.Drawing.Point(9, 211)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(38, 15)
        Me.Label8.TabIndex = 30
        Me.Label8.Text = "Dessert"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnClearLunchDessert
        '
        Me.btnClearLunchDessert.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnClearLunchDessert.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClearLunchDessert.Appearance.Options.UseFont = True
        Me.btnClearLunchDessert.Location = New System.Drawing.Point(699, 118)
        Me.btnClearLunchDessert.Name = "btnClearLunchDessert"
        Me.btnClearLunchDessert.Size = New System.Drawing.Size(50, 24)
        Me.btnClearLunchDessert.TabIndex = 19
        Me.btnClearLunchDessert.Tag = "E"
        Me.btnClearLunchDessert.Text = "Clear"
        '
        'btnSelectLunchDessert
        '
        Me.btnSelectLunchDessert.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnSelectLunchDessert.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSelectLunchDessert.Appearance.Options.UseFont = True
        Me.btnSelectLunchDessert.Location = New System.Drawing.Point(646, 118)
        Me.btnSelectLunchDessert.Name = "btnSelectLunchDessert"
        Me.btnSelectLunchDessert.Size = New System.Drawing.Size(50, 24)
        Me.btnSelectLunchDessert.TabIndex = 18
        Me.btnSelectLunchDessert.Tag = "E"
        Me.btnSelectLunchDessert.Text = "Select"
        '
        'txtLunchDessert
        '
        Me.txtLunchDessert.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtLunchDessert.CharacterCasing = CharacterCasing.Normal
        Me.txtLunchDessert.EnterMoveNextControl = True
        Me.txtLunchDessert.Location = New System.Drawing.Point(75, 118)
        Me.txtLunchDessert.MaxLength = 0
        Me.txtLunchDessert.Name = "txtLunchDessert"
        Me.txtLunchDessert.NumericAllowNegatives = False
        Me.txtLunchDessert.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtLunchDessert.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLunchDessert.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtLunchDessert.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtLunchDessert.Properties.Appearance.Options.UseFont = True
        Me.txtLunchDessert.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLunchDessert.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtLunchDessert.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtLunchDessert.Properties.ReadOnly = True
        Me.txtLunchDessert.Size = New System.Drawing.Size(418, 22)
        Me.txtLunchDessert.TabIndex = 16
        Me.txtLunchDessert.Tag = ""
        Me.txtLunchDessert.TextAlign = HorizontalAlignment.Left
        Me.txtLunchDessert.ToolTipText = ""
        '
        'Label7
        '
        Me.Label7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label7.Location = New System.Drawing.Point(9, 121)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(38, 15)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "Dessert"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnClearTea
        '
        Me.btnClearTea.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnClearTea.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClearTea.Appearance.Options.UseFont = True
        Me.btnClearTea.Location = New System.Drawing.Point(699, 178)
        Me.btnClearTea.Name = "btnClearTea"
        Me.btnClearTea.Size = New System.Drawing.Size(50, 24)
        Me.btnClearTea.TabIndex = 29
        Me.btnClearTea.Tag = "E"
        Me.btnClearTea.Text = "Clear"
        '
        'btnSelectTea
        '
        Me.btnSelectTea.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnSelectTea.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSelectTea.Appearance.Options.UseFont = True
        Me.btnSelectTea.Location = New System.Drawing.Point(646, 178)
        Me.btnSelectTea.Name = "btnSelectTea"
        Me.btnSelectTea.Size = New System.Drawing.Size(50, 24)
        Me.btnSelectTea.TabIndex = 28
        Me.btnSelectTea.Tag = "E"
        Me.btnSelectTea.Text = "Select"
        '
        'txtTea
        '
        Me.txtTea.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtTea.CharacterCasing = CharacterCasing.Normal
        Me.txtTea.EnterMoveNextControl = True
        Me.txtTea.Location = New System.Drawing.Point(75, 178)
        Me.txtTea.MaxLength = 0
        Me.txtTea.Name = "txtTea"
        Me.txtTea.NumericAllowNegatives = False
        Me.txtTea.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtTea.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTea.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtTea.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtTea.Properties.Appearance.Options.UseFont = True
        Me.txtTea.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTea.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtTea.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTea.Properties.ReadOnly = True
        Me.txtTea.Size = New System.Drawing.Size(418, 22)
        Me.txtTea.TabIndex = 26
        Me.txtTea.Tag = ""
        Me.txtTea.TextAlign = HorizontalAlignment.Left
        Me.txtTea.ToolTipText = ""
        '
        'Label5
        '
        Me.Label5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label5.Location = New System.Drawing.Point(9, 181)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(19, 15)
        Me.Label5.TabIndex = 25
        Me.Label5.Text = "Tea"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnClearLunch
        '
        Me.btnClearLunch.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnClearLunch.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClearLunch.Appearance.Options.UseFont = True
        Me.btnClearLunch.Location = New System.Drawing.Point(699, 88)
        Me.btnClearLunch.Name = "btnClearLunch"
        Me.btnClearLunch.Size = New System.Drawing.Size(50, 24)
        Me.btnClearLunch.TabIndex = 14
        Me.btnClearLunch.Tag = "E"
        Me.btnClearLunch.Text = "Clear"
        '
        'btnSelectLunch
        '
        Me.btnSelectLunch.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnSelectLunch.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSelectLunch.Appearance.Options.UseFont = True
        Me.btnSelectLunch.Location = New System.Drawing.Point(646, 88)
        Me.btnSelectLunch.Name = "btnSelectLunch"
        Me.btnSelectLunch.Size = New System.Drawing.Size(50, 24)
        Me.btnSelectLunch.TabIndex = 13
        Me.btnSelectLunch.Tag = "E"
        Me.btnSelectLunch.Text = "Select"
        '
        'txtLunch
        '
        Me.txtLunch.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtLunch.CharacterCasing = CharacterCasing.Normal
        Me.txtLunch.EnterMoveNextControl = True
        Me.txtLunch.Location = New System.Drawing.Point(75, 88)
        Me.txtLunch.MaxLength = 0
        Me.txtLunch.Name = "txtLunch"
        Me.txtLunch.NumericAllowNegatives = False
        Me.txtLunch.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtLunch.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLunch.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtLunch.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtLunch.Properties.Appearance.Options.UseFont = True
        Me.txtLunch.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLunch.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtLunch.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtLunch.Properties.ReadOnly = True
        Me.txtLunch.Size = New System.Drawing.Size(418, 22)
        Me.txtLunch.TabIndex = 11
        Me.txtLunch.Tag = ""
        Me.txtLunch.TextAlign = HorizontalAlignment.Left
        Me.txtLunch.ToolTipText = ""
        '
        'Label4
        '
        Me.Label4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label4.Location = New System.Drawing.Point(9, 91)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(33, 15)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Lunch"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnClearSnack
        '
        Me.btnClearSnack.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnClearSnack.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClearSnack.Appearance.Options.UseFont = True
        Me.btnClearSnack.Location = New System.Drawing.Point(699, 58)
        Me.btnClearSnack.Name = "btnClearSnack"
        Me.btnClearSnack.Size = New System.Drawing.Size(50, 24)
        Me.btnClearSnack.TabIndex = 9
        Me.btnClearSnack.Tag = "E"
        Me.btnClearSnack.Text = "Clear"
        '
        'btnSelectSnack
        '
        Me.btnSelectSnack.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnSelectSnack.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSelectSnack.Appearance.Options.UseFont = True
        Me.btnSelectSnack.Location = New System.Drawing.Point(646, 58)
        Me.btnSelectSnack.Name = "btnSelectSnack"
        Me.btnSelectSnack.Size = New System.Drawing.Size(50, 24)
        Me.btnSelectSnack.TabIndex = 8
        Me.btnSelectSnack.Tag = "E"
        Me.btnSelectSnack.Text = "Select"
        '
        'txtSnack
        '
        Me.txtSnack.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtSnack.CharacterCasing = CharacterCasing.Normal
        Me.txtSnack.EnterMoveNextControl = True
        Me.txtSnack.Location = New System.Drawing.Point(75, 58)
        Me.txtSnack.MaxLength = 0
        Me.txtSnack.Name = "txtSnack"
        Me.txtSnack.NumericAllowNegatives = False
        Me.txtSnack.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSnack.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSnack.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSnack.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSnack.Properties.Appearance.Options.UseFont = True
        Me.txtSnack.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSnack.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSnack.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSnack.Properties.ReadOnly = True
        Me.txtSnack.Size = New System.Drawing.Size(418, 22)
        Me.txtSnack.TabIndex = 6
        Me.txtSnack.Tag = ""
        Me.txtSnack.TextAlign = HorizontalAlignment.Left
        Me.txtSnack.ToolTipText = ""
        '
        'Label3
        '
        Me.Label3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label3.Location = New System.Drawing.Point(9, 61)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 15)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "AM Snack"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnClearBreakfast
        '
        Me.btnClearBreakfast.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnClearBreakfast.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClearBreakfast.Appearance.Options.UseFont = True
        Me.btnClearBreakfast.Location = New System.Drawing.Point(699, 28)
        Me.btnClearBreakfast.Name = "btnClearBreakfast"
        Me.btnClearBreakfast.Size = New System.Drawing.Size(50, 24)
        Me.btnClearBreakfast.TabIndex = 4
        Me.btnClearBreakfast.Tag = "E"
        Me.btnClearBreakfast.Text = "Clear"
        '
        'btnSelectBreakfast
        '
        Me.btnSelectBreakfast.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnSelectBreakfast.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSelectBreakfast.Appearance.Options.UseFont = True
        Me.btnSelectBreakfast.Location = New System.Drawing.Point(646, 28)
        Me.btnSelectBreakfast.Name = "btnSelectBreakfast"
        Me.btnSelectBreakfast.Size = New System.Drawing.Size(50, 24)
        Me.btnSelectBreakfast.TabIndex = 3
        Me.btnSelectBreakfast.Tag = "E"
        Me.btnSelectBreakfast.Text = "Select"
        '
        'txtBreakfast
        '
        Me.txtBreakfast.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtBreakfast.CharacterCasing = CharacterCasing.Normal
        Me.txtBreakfast.EnterMoveNextControl = True
        Me.txtBreakfast.Location = New System.Drawing.Point(75, 28)
        Me.txtBreakfast.MaxLength = 0
        Me.txtBreakfast.Name = "txtBreakfast"
        Me.txtBreakfast.NumericAllowNegatives = False
        Me.txtBreakfast.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtBreakfast.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBreakfast.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtBreakfast.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtBreakfast.Properties.Appearance.Options.UseFont = True
        Me.txtBreakfast.Properties.Appearance.Options.UseTextOptions = True
        Me.txtBreakfast.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtBreakfast.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtBreakfast.Properties.ReadOnly = True
        Me.txtBreakfast.Size = New System.Drawing.Size(418, 22)
        Me.txtBreakfast.TabIndex = 1
        Me.txtBreakfast.Tag = ""
        Me.txtBreakfast.TextAlign = HorizontalAlignment.Left
        Me.txtBreakfast.ToolTipText = ""
        '
        'Label1
        '
        Me.Label1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label1.Location = New System.Drawing.Point(9, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Breakfast"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnDupPMSnack
        '
        Me.btnDupPMSnack.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnDupPMSnack.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnDupPMSnack.Appearance.Options.UseFont = True
        Me.btnDupPMSnack.Location = New System.Drawing.Point(499, 148)
        Me.btnDupPMSnack.Name = "btnDupPMSnack"
        Me.btnDupPMSnack.Size = New System.Drawing.Size(141, 24)
        Me.btnDupPMSnack.TabIndex = 22
        Me.btnDupPMSnack.Tag = "E"
        Me.btnDupPMSnack.Text = "Duplicate the Week"
        '
        'btnClearPMSnack
        '
        Me.btnClearPMSnack.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnClearPMSnack.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClearPMSnack.Appearance.Options.UseFont = True
        Me.btnClearPMSnack.Location = New System.Drawing.Point(699, 148)
        Me.btnClearPMSnack.Name = "btnClearPMSnack"
        Me.btnClearPMSnack.Size = New System.Drawing.Size(50, 24)
        Me.btnClearPMSnack.TabIndex = 24
        Me.btnClearPMSnack.Tag = "E"
        Me.btnClearPMSnack.Text = "Clear"
        '
        'btnSelectPMSnack
        '
        Me.btnSelectPMSnack.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnSelectPMSnack.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSelectPMSnack.Appearance.Options.UseFont = True
        Me.btnSelectPMSnack.Location = New System.Drawing.Point(646, 148)
        Me.btnSelectPMSnack.Name = "btnSelectPMSnack"
        Me.btnSelectPMSnack.Size = New System.Drawing.Size(50, 24)
        Me.btnSelectPMSnack.TabIndex = 23
        Me.btnSelectPMSnack.Tag = "E"
        Me.btnSelectPMSnack.Text = "Select"
        '
        'txtPMSnack
        '
        Me.txtPMSnack.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtPMSnack.CharacterCasing = CharacterCasing.Normal
        Me.txtPMSnack.EnterMoveNextControl = True
        Me.txtPMSnack.Location = New System.Drawing.Point(75, 148)
        Me.txtPMSnack.MaxLength = 0
        Me.txtPMSnack.Name = "txtPMSnack"
        Me.txtPMSnack.NumericAllowNegatives = False
        Me.txtPMSnack.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtPMSnack.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPMSnack.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPMSnack.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtPMSnack.Properties.Appearance.Options.UseFont = True
        Me.txtPMSnack.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPMSnack.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPMSnack.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPMSnack.Properties.ReadOnly = True
        Me.txtPMSnack.Size = New System.Drawing.Size(418, 22)
        Me.txtPMSnack.TabIndex = 21
        Me.txtPMSnack.Tag = ""
        Me.txtPMSnack.TextAlign = HorizontalAlignment.Left
        Me.txtPMSnack.ToolTipText = ""
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(9, 151)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(52, 15)
        Me.CareLabel2.TabIndex = 20
        Me.CareLabel2.Text = "PM Snack"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmRollingMenus
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(859, 440)
        Me.Controls.Add(Me.gbxDays)
        Me.Controls.Add(Me.gbxWeeks)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmRollingMenus"
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.gbxWeeks, 0)
        Me.Controls.SetChildIndex(Me.gbxDays, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.cdtStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxWeeks, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxWeeks.ResumeLayout(False)
        CType(Me.gbxDays, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxDays.ResumeLayout(False)
        CType(Me.gbxMeals, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxMeals.ResumeLayout(False)
        Me.gbxMeals.PerformLayout()
        CType(Me.txtTeaDessert.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLunchDessert.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTea.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLunch.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSnack.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBreakfast.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPMSnack.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As DevExpress.XtraEditors.GroupControl
    Private WithEvents cdtStart As Care.Controls.CareDateTime
    Friend WithEvents lblFamily As Care.Controls.CareLabel
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents btnRemove As Care.Controls.CareButton
    Friend WithEvents gbxWeeks As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnAdd As Care.Controls.CareButton
    Friend WithEvents gbxDays As DevExpress.XtraEditors.GroupControl
    Friend WithEvents gbxMeals As DevExpress.XtraEditors.GroupControl
    Friend WithEvents grdWeeks As Care.Controls.CareGrid
    Friend WithEvents grdDays As Care.Controls.CareGrid
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents btnClearTeaDessert As Care.Controls.CareButton
    Friend WithEvents btnSelectTeaDessert As Care.Controls.CareButton
    Friend WithEvents txtTeaDessert As Care.Controls.CareTextBox
    Friend WithEvents Label8 As Care.Controls.CareLabel
    Friend WithEvents btnClearLunchDessert As Care.Controls.CareButton
    Friend WithEvents btnSelectLunchDessert As Care.Controls.CareButton
    Friend WithEvents txtLunchDessert As Care.Controls.CareTextBox
    Friend WithEvents Label7 As Care.Controls.CareLabel
    Friend WithEvents btnClearTea As Care.Controls.CareButton
    Friend WithEvents btnSelectTea As Care.Controls.CareButton
    Friend WithEvents txtTea As Care.Controls.CareTextBox
    Friend WithEvents Label5 As Care.Controls.CareLabel
    Friend WithEvents btnClearLunch As Care.Controls.CareButton
    Friend WithEvents btnSelectLunch As Care.Controls.CareButton
    Friend WithEvents txtLunch As Care.Controls.CareTextBox
    Friend WithEvents Label4 As Care.Controls.CareLabel
    Friend WithEvents btnClearSnack As Care.Controls.CareButton
    Friend WithEvents btnSelectSnack As Care.Controls.CareButton
    Friend WithEvents txtSnack As Care.Controls.CareTextBox
    Friend WithEvents Label3 As Care.Controls.CareLabel
    Friend WithEvents btnClearBreakfast As Care.Controls.CareButton
    Friend WithEvents btnSelectBreakfast As Care.Controls.CareButton
    Friend WithEvents txtBreakfast As Care.Controls.CareTextBox
    Friend WithEvents Label1 As Care.Controls.CareLabel
    Friend WithEvents btnDayCancel As Care.Controls.CareButton
    Friend WithEvents btnDayOK As Care.Controls.CareButton
    Friend WithEvents btnDupTeaDessert As Care.Controls.CareButton
    Friend WithEvents btnDupLunchDessert As Care.Controls.CareButton
    Friend WithEvents btnDupTea As Care.Controls.CareButton
    Friend WithEvents btnDupLunch As Care.Controls.CareButton
    Friend WithEvents btnDupSnack As Care.Controls.CareButton
    Friend WithEvents btnDupBreakfast As Care.Controls.CareButton
    Friend WithEvents btnDupPMSnack As Care.Controls.CareButton
    Friend WithEvents btnClearPMSnack As Care.Controls.CareButton
    Friend WithEvents btnSelectPMSnack As Care.Controls.CareButton
    Friend WithEvents txtPMSnack As Care.Controls.CareTextBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel

End Class
