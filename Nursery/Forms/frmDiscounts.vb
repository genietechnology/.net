﻿Imports Care.Global

Public Class frmDiscounts

    Dim m_Discount As Business.Discount

    Private Sub frmTariff_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        cbxType.Items.Clear()
        cbxType.AddItem("Percentage")
        cbxType.AddItem("Fixed")

        Me.GridSQL = "select id, name as 'Description', discount_type as 'Type', discount_value as 'Value'" & _
                     " from Discounts" & _
                     " order by Description"

        gbx.Hide()

    End Sub

    Protected Overrides Sub FormatGrid()
        'ug.DisplayLayout.Bands(0).Columns(0).Hidden = True
        'ug.DisplayLayout.Bands(0).Columns("Value").Header.Appearance.TextHAlign = Infragistics.Win.HAlign.Right
        'ug.DisplayLayout.Bands(0).Columns("Value").Format = "£0.00"
        'ug.DisplayLayout.Bands(0).Columns("Value").CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
    End Sub

    Protected Overrides Sub SetBindings()

        m_Discount = New Business.Discount
        bs.DataSource = m_Discount

        txtDescription.DataBindings.Add("Text", bs, "_Name")
        cbxType.DataBindings.Add("Text", bs, "_DiscountType")
        txtValue.DataBindings.Add("Text", bs, "_DiscountValue", True)

    End Sub

    Protected Overrides Sub BindToID(ID As System.Guid, IsNew As Boolean)
        m_Discount = Business.Discount.RetreiveByID(ID)
        bs.DataSource = m_Discount
    End Sub

    Protected Overrides Sub CommitUpdate()
        'm_Discount._Name = txtDescription.Text
        'm_Discount._DiscountType = cbxType.Text
        m_Discount._DiscountValue = Decimal.Parse(txtValue.Text)
        m_Discount.Store()
    End Sub

    Protected Overrides Sub CommitDelete(ID As System.Guid)
        Business.Discount.DeleteRecord(ID)
    End Sub

    Protected Overrides Sub AfterAdd()
        cbxType.Text = "Percentage"
    End Sub

End Class
