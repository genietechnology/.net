﻿

Imports Care.Data
Imports Care.Global
Imports Care.Shared
Imports System.Windows.Forms

Public Class frmInvoiceBatchCreate

    Private m_BatchID As Guid? = Nothing
    Private m_IsNew As Boolean
    Private m_Eng As New Business.InvoicingEngine

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_IsNew = True

    End Sub

    Public Sub New(ByVal BatchID As Guid)

        ' This call is required by the designer.
        InitializeComponent()

        m_BatchID = BatchID
        m_IsNew = False

    End Sub

    Private Sub frmInvoiceBatchCreate_Load(sender As Object, e As EventArgs) Handles Me.Load

        btnCreate.Enabled = False

        PopulateLists()

        If m_IsNew Then

            'get the next invoice batch number
            txtNextBatchNo.Text = (Invoicing.LastBatchNo + 1).ToString
            txtNextInvoiceNo.Text = (Invoicing.LastInvoiceNo + 1).ToString

            cbxTariff.BackColor = Session.ChangeColour
            cbxFundedType.BackColor = Session.ChangeColour
            cbxTags.BackColor = Session.ChangeColour

            Me.Text = "Create New Invoice Batch"
            btnCreate.Text = "Create Batch"

        Else

            DisplayBatch()

            cbxTariff.Enabled = False
            cbxFundedType.Enabled = False
            cbxTags.Enabled = False

            btnCreate.Text = "OK"

        End If

        btnCreate.Enabled = True

    End Sub

    Private Sub DisplayBatch()

        Dim _b As Business.InvoiceBatch = Business.InvoiceBatch.RetreiveByID(m_BatchID.Value)

        With _b

            Me.Text = "Amend Invoice Batch: " + ._BatchNo.ToString

            cbxPeriod.Text = ._BatchPeriod

            cbxSite.Text = ._SiteName
            cbxClassification.Text = ._BatchClass
            cbxFrequency.Text = ._BatchFreq

            cdtFirst.Value = ._DateFirst
            cdtLast.Value = ._DateLast
            cdtInvoiceDate.Value = ._BatchDate
            cbxDuplicate.SelectedIndex = ._DuplicateMode

            cbxAdditional.SelectedIndex = ._AdditionalSessions
            cdtAddlFrom.Value = ._AdditionalFrom
            cdtAddlTo.Value = ._AdditionalTo

            cbxGeneration.SelectedIndex = ._GenerationMode
            cbxLayout.Text = ._BatchLayout

            cbxPaymentMethod.Text = ._PaymentMethod
            cbxRoom.Text = ._BatchGroup
            cbxTariff.Text = ._TariffFilter
            cbxFundedType.Text = ._FundingType
            cbxTags.Text = ._TagFilter

            cbxXCheck.SelectedIndex = ._Xcheck
            cdtXCheckFrom.Value = ._XcheckFrom
            cdtXCheckTo.Value = ._XcheckTo
            txtXCheckThresh.EditValue = ._XcheckThresh
            txtXCheckPerHour.EditValue = ._XcheckPerHour
            chkXCheckAdditional.Checked = ._XcheckAdditional

            txtComments.Text = _b._Comments

        End With

    End Sub

    Private Sub frmInvoiceBatchCreate_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        If m_IsNew Then

            MyControls.SetControls(ControlHandler.Mode.Add, Me.Controls, ControlHandler.DebugMode.None)
            cbxTariff.BackColor = Session.ChangeColour

            cbxPeriod.SelectedIndex = 0 'custom period
            cbxXCheck.SelectedIndex = 0 'none
            cbxLayout.SelectedIndex = 0 'first layout
            cbxAdditional.SelectedIndex = 1 'included
            cbxDuplicate.SelectedIndex = 0 'included
            cbxGeneration.SelectedIndex = 0 'standard

            If cbxSite.ItemCount = 1 Then cbxSite.SelectedIndex = 0
            If cbxClassification.ItemCount = 1 Then cbxClassification.SelectedIndex = 0
            If cbxFrequency.ItemCount = 1 Then cbxFrequency.SelectedIndex = 0

            cbxSite.Focus()

        Else
            MyControls.SetControls(ControlHandler.Mode.Edit, Me.Controls, ControlHandler.DebugMode.None)
            txtComments.Focus()
        End If

    End Sub

    Private Sub PopulateLists()

        '***************************************************************************************************************************************

        With cbxPeriod
            .AddItem("Custom Period")
            .AddItem("Empty Batch")
            .AddItem("Opening Balances")
            .AddItem("Annual Recalculation")
        End With

        '***************************************************************************************************************************************

        If ParameterHandler.ReturnBoolean("USECLASS") Then
            With cbxClassification
                .AllowBlank = False
                .PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Classification"))
                .Tag = "A"
            End With
        Else
            cbxClassification.Tag = ""
            cbxClassification.Enabled = False
        End If

        cbxSite.PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)
        cbxFrequency.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Invoice Frequency"))

        cbxPaymentMethod.AllowBlank = True
        cbxPaymentMethod.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Payment Method"))

        ControlHandler.PopulateCheckedListBox(cbxFundedType, "Funding Type")
        ControlHandler.PopulateCheckedListBox(cbxTags, GetAllTags)

        '***************************************************************************************************************************************

        With cbxDuplicate
            .AddItem("Include")
            .AddItem("Exclude All Invoices")
            .AddItem("Exclude Only Posted Invoices")
        End With

        With cbxXCheck
            .AddItem("None")
            .AddItem("Register vs Invoiced")
            .AddItem("Register vs Bookings")
        End With

        cbxLayout.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Invoice Layouts"))

        With cbxAdditional
            .AddItem("Excluded")
            .AddItem("Included")
            .AddItem("Only")
        End With

        With cbxGeneration
            .AddItem("Standard")
            .AddItem("Summarised (by Weekday)")
            .AddItem("Summarised (KLN)")
        End With

        '***************************************************************************************************************************************

    End Sub

    Private Function GetAllTags() As List(Of String)

        Dim _Tags As New List(Of String)

        Dim _SQL As String = "select tags from Children where status <> 'Left' and len(tags) > 0"
        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            'loop through each child
            For Each _DR As DataRow In _DT.Rows

                'fetch the tags and split into list of tags
                Dim _ChildTags As List(Of String) = _DR.Item("tags").ToString.Split(CChar(",")).ToList

                'loop through the tags and append to tag list where they are not already in the main list
                For Each _Tag In _ChildTags

                    If Not InList(_Tags, _Tag) Then
                        _Tags.Add(_Tag.Trim)
                    End If

                Next

            Next

        End If

        Return _Tags

    End Function

    Private Function InList(ByRef Tags As List(Of String), ByVal CheckTag As String) As Boolean

        For Each _Tag In Tags
            If _Tag.Trim.ToUpper = CheckTag.Trim.ToUpper Then
                Return True
            End If
        Next

        Return False

    End Function

    Private Sub SetDateControl(ByRef DateControl As Care.Controls.CareDateTime, ByVal Enabled As Boolean)

        DateControl.Enabled = True
        DateControl.ReadOnly = Not Enabled
        DateControl.TabStop = Enabled
        DateControl.Value = Nothing

        If Enabled Then
            DateControl.BackColor = Session.ChangeColour
        Else
            DateControl.ResetBackColor()
        End If

    End Sub

    Private Sub SetTextBoxControl(ByRef TextBoxControl As Care.Controls.CareTextBox, ByVal Enabled As Boolean)

        TextBoxControl.Enabled = True
        TextBoxControl.ReadOnly = Not Enabled
        TextBoxControl.TabStop = Enabled

        If Enabled Then
            TextBoxControl.BackColor = Session.ChangeColour
        Else
            TextBoxControl.Text = ""
            TextBoxControl.ResetBackColor()
        End If

    End Sub

    Private Sub SetCheckBoxControl(ByRef CheckBoxControl As Care.Controls.CareCheckBox, ByVal Enabled As Boolean)

        CheckBoxControl.Enabled = Enabled
        CheckBoxControl.TabStop = Enabled

        If Enabled Then
            CheckBoxControl.BackColor = Session.ChangeColour
        Else
            CheckBoxControl.Checked = False
            CheckBoxControl.ResetBackColor()
        End If

    End Sub

    Private Function CreateBatch() As Boolean

        Dim _OK As Boolean = False
        Dim _XCheckThresh As Decimal = 0
        Dim _XCheckPerHour As Decimal = 0

        If txtXCheckThresh.Text = "" Then
            _XCheckThresh = ParameterHandler.ReturnDecimal("DEFXCHECKTHRESH")
        Else
            _XCheckThresh = ValueHandler.ConvertDecimal(txtXCheckThresh.Text)
        End If

        If txtXCheckPerHour.Text = "" Then
            _XCheckPerHour = ParameterHandler.ReturnDecimal("DEFXCHECKPERHOUR")
        Else
            _XCheckPerHour = ValueHandler.ConvertDecimal(txtXCheckPerHour.Text)
        End If

        'save the batch header information
        Dim _BatchID As Guid = Guid.NewGuid
        If SaveBatchHeader(_BatchID) Then

            'generate the invoices
            _OK = m_Eng.Run(_BatchID)

            Me.Tag = _BatchID

        End If

        Return _OK

    End Function

    Private Function ValidateBatch() As Boolean

        If cbxPeriod.SelectedIndex < 0 Then
            CareMessage("Please select a batch type.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If cbxSite.SelectedIndex < 0 Then
            CareMessage("Please select a site.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If cbxPeriod.Text = "Empty Batch" Or cbxPeriod.Text = "Annual Recalculation" Then
            'we don't bother with an invoice date for these batches
        Else

            If ParameterHandler.ReturnBoolean("USECLASS") Then
                If cbxClassification.SelectedIndex < 0 Then
                    CareMessage("Please select a Classification.", MessageBoxIcon.Exclamation, "Mandatory Field")
                    Return False
                End If
            End If

            If cbxFrequency.SelectedIndex < 0 Then
                CareMessage("Please select an invoice frequency.", MessageBoxIcon.Exclamation, "Mandatory Field")
                Return False
            End If

            'ensure the first and last dates are entered and make sense
            If cbxPeriod.Text = "Custom Period" Then

                If cdtFirst.Text = "" Then
                    CareMessage("Please enter the first date of the invoice period.", MessageBoxIcon.Exclamation, "Mandatory Field")
                    Return False
                End If

                If cdtLast.Text = "" Then
                    CareMessage("Please enter the last date of the invoice period.", MessageBoxIcon.Exclamation, "Mandatory Field")
                    Return False
                End If

                If cdtFirst.Value > cdtLast.Value Then
                    CareMessage("Please provide a valid invoice date range.", MessageBoxIcon.Exclamation, "Date range check")
                    Return False
                End If

            End If

            'check the invoice date has been entered
            If cdtInvoiceDate.Text = "" Then
                CareMessage("Please enter the invoice date. This is date that will be used on all Invoices.", MessageBoxIcon.Exclamation, "Mandatory Field")
                Return False
            End If

            If cbxAdditional.SelectedIndex > 0 Then

                If cdtAddlFrom.Text = "" AndAlso cdtAddlTo.Text = "" Then

                    If CareMessage("You have not specified the period for additional sessions. Shall we default to the invoice period?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Additional Sessions") = DialogResult.Yes Then
                        cdtAddlFrom.Value = cdtFirst.Value
                        cdtAddlTo.Value = cdtLast.Value
                        Application.DoEvents()
                    Else
                        Return False
                    End If

                Else

                    If cdtAddlFrom.Text = "" Then
                        CareMessage("Please enter the first date of the additional session period.", MessageBoxIcon.Exclamation, "Mandatory Field")
                        Return False
                    End If

                    If cdtAddlTo.Text = "" Then
                        CareMessage("Please enter the last date of the additional session period.", MessageBoxIcon.Exclamation, "Mandatory Field")
                        Return False
                    End If

                End If

                If cdtAddlFrom.Value > cdtAddlTo.Value Then
                    CareMessage("Please provide a valid additional session date range.", MessageBoxIcon.Exclamation, "Date range check")
                    Return False
                End If

            End If

        End If

        If cbxGeneration.SelectedIndex < 0 Then
            CareMessage("Please select an invoice generation mode.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If cbxLayout.SelectedIndex < 0 Then
            CareMessage("Please select an invoice layout.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        Return True

    End Function

    Private Function SaveBatchHeader(ByVal BatchID As Guid) As Boolean

        Dim _B As New Business.InvoiceBatch
        With _B

            ._ID = BatchID
            ._BatchNo = Invoicing.LastBatchNo + 1
            ._BatchStatus = "Open"
            ._InvFirst = Invoicing.LastInvoiceNo + 1

            ._BatchPeriod = cbxPeriod.Text

            'standard filters
            ._BatchClass = cbxClassification.Text
            ._SiteId = New Guid(cbxSite.SelectedValue.ToString)
            ._SiteName = cbxSite.Text
            ._BatchFreq = cbxFrequency.Text
            ._PaymentMethod = cbxPaymentMethod.Text

            'additional filters
            ._BatchGroup = cbxRoom.Text
            ._TariffFilter = cbxTariff.EditValue.ToString
            ._FundingType = cbxFundedType.Text
            ._TagFilter = cbxTags.EditValue.ToString

            'invoice dates
            ._DateFirst = cdtFirst.DateTime
            ._DateLast = cdtLast.DateTime
            ._BatchDate = cdtInvoiceDate.DateTime
            ._DuplicateMode = ValueHandler.ConvertByte(cbxDuplicate.SelectedIndex)

            'register cross-check
            ._Xcheck = cbxXCheck.SelectedIndex
            If ._Xcheck > 0 Then
                ._XcheckFrom = cdtXCheckFrom.DateTime
                ._XcheckTo = cdtXCheckTo.DateTime
                ._XcheckThresh = ValueHandler.ConvertDecimal(txtXCheckThresh.Text)
                ._XcheckPerHour = ValueHandler.ConvertDecimal(txtXCheckPerHour.Text)
                ._XcheckAdditional = chkXCheckAdditional.Checked
            Else
                ._XcheckFrom = Nothing
                ._XcheckTo = Nothing
                ._XcheckThresh = 0
                ._XcheckPerHour = 0
                ._XcheckAdditional = False
            End If

            'layout and generation
            ._BatchLayout = cbxLayout.Text
            ._GenerationMode = ValueHandler.ConvertByte(cbxGeneration.SelectedIndex)

            'addtional sessions
            ._AdditionalSessions = ValueHandler.ConvertByte(cbxAdditional.SelectedIndex)
            If cbxAdditional.SelectedIndex > 0 Then
                ._AdditionalFrom = cdtAddlFrom.Value
                ._AdditionalTo = cdtAddlTo.Value
            Else
                ._AdditionalFrom = Nothing
                ._AdditionalTo = Nothing
            End If

            ._Comments = txtComments.Text

            ._UserId = Session.CurrentUser.ID
            ._Stamp = Now

            .Store()

        End With

        Return True

    End Function

    Private Function ReturnAdditionalEnum() As Invoicing.EnumAdditionalSessionMode
        If cbxAdditional.SelectedIndex = 1 Then Return Invoicing.EnumAdditionalSessionMode.Included
        If cbxAdditional.SelectedIndex = 2 Then Return Invoicing.EnumAdditionalSessionMode.Only
        Return Invoicing.EnumAdditionalSessionMode.Excluded
    End Function

    Private Function ReturnDuplicateEnum() As Invoicing.EnumDuplicateMode
        If cbxDuplicate.SelectedIndex = 1 Then Return Invoicing.EnumDuplicateMode.ExcludeAllInvoices
        If cbxDuplicate.SelectedIndex = 2 Then Return Invoicing.EnumDuplicateMode.ExcludeOnlyPostedInvoices
        Return Invoicing.EnumDuplicateMode.Include
    End Function

#Region "Controls"

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Tag = Nothing
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnCreate_Click(sender As Object, e As EventArgs) Handles btnCreate.Click

        Dim _OK As Boolean = False

        btnCreate.Enabled = False
        btnCancel.Enabled = False

        Session.CursorWaiting()

        If m_IsNew Then
            If ValidateBatch() Then
                Session.WaitFormShow()
                If CreateBatch() Then _OK = True
                Session.WaitFormClose()
            End If
        Else
            UpdateHeader()
            _OK = True
        End If

        Session.CursorDefault()

        btnCreate.Enabled = True
        btnCancel.Enabled = True

        If _OK Then
            Me.DialogResult = DialogResult.OK
            Me.Close()
        End If

    End Sub

    Private Sub UpdateHeader()

        Dim _b As Business.InvoiceBatch = Business.InvoiceBatch.RetreiveByID(m_BatchID.Value)

        'if we have changed the invoice layout, we need to update each invoice aswell
        If cbxLayout.Text <> _b._BatchLayout Then

            Dim _SQL As String = "update Invoices set invoice_layout = '" + cbxLayout.Text + "' where batch_id = '" + m_BatchID.Value.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)

            _b._BatchLayout = cbxLayout.Text

        End If

        _b._Comments = txtComments.Text
        _b.Store()

    End Sub

    Private Sub cbxPeriod_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxPeriod.SelectedIndexChanged

        Dim _SetPeriod As Boolean = False
        Dim _ChangeInvoicePeriod As Boolean = True
        Dim _ChangeInvoiceDate As Boolean = True

        Select Case cbxPeriod.Text

            Case "Opening Balances"
                _ChangeInvoicePeriod = False
                _ChangeInvoiceDate = True

            Case "Custom Period"
                _ChangeInvoicePeriod = True
                _ChangeInvoiceDate = True

            Case "Empty Batch", "Annual Recalculation"
                _ChangeInvoicePeriod = False
                _ChangeInvoiceDate = False

        End Select

        SetDateControl(cdtFirst, _ChangeInvoicePeriod)
        SetDateControl(cdtLast, _ChangeInvoicePeriod)
        SetDateControl(cdtInvoiceDate, _ChangeInvoiceDate)

    End Sub

    Private Sub cbxXCheck_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxXCheck.SelectedIndexChanged

        If cbxXCheck.SelectedIndex > 0 Then
            SetDateControl(cdtXCheckFrom, True)
            SetDateControl(cdtXCheckTo, True)
            SetTextBoxControl(txtXCheckThresh, True)
            SetTextBoxControl(txtXCheckPerHour, True)
            SetCheckBoxControl(chkXCheckAdditional, True)
        Else
            SetDateControl(cdtXCheckFrom, False)
            SetDateControl(cdtXCheckTo, False)
            SetTextBoxControl(txtXCheckThresh, False)
            SetTextBoxControl(txtXCheckPerHour, False)
            SetCheckBoxControl(chkXCheckAdditional, False)
        End If

    End Sub

#End Region

    Private Sub cbxSite_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSite.SelectedIndexChanged
        If cbxSite.Text = "" Then
            cbxRoom.Clear()
            cbxRoom.SelectedIndex = -1
            cbxTariff.DeselectAll()
            cbxTariff.Properties.Items.Clear()
        Else
            cbxRoom.PopulateWithSQL(Session.ConnectionString, Business.RoomRatios.RetreiveSiteRoomSQL(cbxSite.SelectedValue.ToString))
            ControlHandler.PopulateCheckedListBox(cbxTariff, Session.ConnectionString, "select id, name from Tariffs where site_id = '" + cbxSite.SelectedValue.ToString + "' order by name")
        End If
    End Sub

    Private Sub cbxAdditional_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxAdditional.SelectedIndexChanged

        If cbxAdditional.SelectedIndex = 0 Then

            cdtAddlFrom.Enabled = False
            cdtAddlFrom.ResetBackColor()
            cdtAddlFrom.Text = ""

            cdtAddlTo.Enabled = False
            cdtAddlTo.ResetBackColor()
            cdtAddlTo.Text = ""

        Else

            cdtAddlFrom.Enabled = True
            cdtAddlFrom.BackColor = Session.ChangeColour

            cdtAddlTo.Enabled = True
            cdtAddlTo.BackColor = Session.ChangeColour

        End If

    End Sub

End Class
