﻿Imports Care.Global
Imports System.Windows.Forms

Public Class frmParentReports

    Private Sub frmParentReports_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        udtDate.Value = Date.Today

        With cbxMode
            .AddItem("Print Paper Reports Only", "P")
            .AddItem("Send Emails Only", "E")
            .AddItem("Print Paper Reports and Send Emails", "B")
            .SelectedIndex = 0
        End With

        With cbxSite
            .PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)
        End With

    End Sub

    Private Sub btnRun_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRun.Click

        If udtDate.Value Is Nothing Then Exit Sub
        If cbxSite.Text = "" Then Exit Sub

        btnRun.Enabled = False
        btnClose.Enabled = False
        Cursor.Current = Cursors.WaitCursor
        Application.DoEvents()

        Dim _SiteID As String = ""
        If cbxSite.Text <> "" Then _SiteID = cbxSite.SelectedValue.ToString

        Dim _RoomID As String = ""
        If cbxRoom.Text <> "" Then _RoomID = cbxRoom.SelectedValue.ToString

        Dim _RunMode As Reports.DailyReport.EnumRunMode = Reports.DailyReport.EnumRunMode.Both
        If cbxMode.SelectedValue.ToString = "P" Then _RunMode = Reports.DailyReport.EnumRunMode.Print
        If cbxMode.SelectedValue.ToString = "E" Then _RunMode = Reports.DailyReport.EnumRunMode.Email

        Dim _DailyReport As New Reports.DailyReport(_RunMode, udtDate.DateTime, New Guid(_SiteID), , , True, , _RoomID)
        _DailyReport.Run()

        btnRun.Enabled = True
        btnClose.Enabled = True
        Cursor.Current = Cursors.Default
        Application.DoEvents()

    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub cbxSite_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSite.SelectedIndexChanged
        If cbxSite.Text = "" Then
            cbxRoom.Clear()
            cbxRoom.SelectedIndex = -1
        Else
            cbxRoom.PopulateWithSQL(Session.ConnectionString, Business.RoomRatios.RetreiveSiteRoomSQL(cbxSite.SelectedValue.ToString))
        End If
    End Sub
End Class
