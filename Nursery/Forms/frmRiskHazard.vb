﻿Imports Care.Global
Imports Care.Shared

Public Class frmRiskHazard

    Private m_IsNew As Boolean = True
    Private m_RiskID As Guid
    Private m_ActivityID As Guid?
    Private m_Room As New Business.SiteRoom

    Public Sub New(ByVal RiskID As Guid, ByVal ActivityID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_RiskID = RiskID
        m_ActivityID = ActivityID

        If ActivityID.HasValue Then
            m_IsNew = False
        Else
            Me.Text = "Create New Risk Activity Item"
        End If

    End Sub

    Private Sub frmSiteRoom_Load(sender As Object, e As EventArgs) Handles Me.Load

        SetupLists()

        txtActivity.BackColor = Session.ChangeColour

        cbxHazards.BackColor = Session.ChangeColour
        cbxRisks.BackColor = Session.ChangeColour
        cbxPeople.BackColor = Session.ChangeColour

        txtHazards.BackColor = Session.ChangeColour
        txtRisks.BackColor = Session.ChangeColour
        txtPeople.BackColor = Session.ChangeColour

        txtControlMeasures.BackColor = Session.ChangeColour

        txtActivity.Focus()

        If Not m_IsNew Then DisplayRecord()

    End Sub

    Private Sub frmRiskHazard_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        txtActivity.Focus()
    End Sub

    Private Sub SetupLists()

        Dim _Hazards As DataRowCollection = ListHandler.ReturnItems("Risk Assessment Hazard")
        For Each _H As DataRow In _Hazards
            cbxHazards.Properties.Items.Add(_H.Item("name").ToString)
        Next

        Dim _Who As DataRowCollection = ListHandler.ReturnItems("Risk Assessment Who")
        For Each _W As DataRow In _Who
            cbxPeople.Properties.Items.Add(_W.Item("name").ToString)
        Next

    End Sub

    Private Sub DisplayRecord()

        Dim _C As Business.RiskActivities = Business.RiskActivities.RetreiveByID(m_ActivityID.Value)
        With _C

            Me.Text = "Edit Risk Activity Item: " + _C._Activity

            txtActivity.Text = ._Activity
            txtHazards.Text = ._HazardDesc
            txtRisks.Text = ._RiskDesc
            txtPeople.Text = ._PeopleDesc
            txtControlMeasures.Text = ._Controls

            rcUncontrolled.Likelihood = ._UcLikely
            rcUncontrolled.Severity = ._UcSeverity

            rcResidual.Likelihood = ._cLikely
            rcResidual.Severity = ._cSeverity

        End With

        _C = Nothing

    End Sub

    Private Function ValidateEntry() As Boolean

        If txtActivity.Text = "" Then
            CareMessage("Please enter the title for the Activity.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If txtHazards.Text = "" Then
            CareMessage("Please enter what the hazards are.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If txtRisks.Text = "" Then
            CareMessage("Please enter how they might be harmed.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If txtPeople.Text = "" Then
            CareMessage("Please enter who might be harmed.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If txtControlMeasures.Text = "" Then
            CareMessage("Please enter your control measures.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        Return True

    End Function

    Private Sub SaveAndExit()

        If Not ValidateEntry() Then Exit Sub

        Dim _C As Business.RiskActivities = Nothing

        If m_IsNew Then
            _C = New Business.RiskActivities
            _C._ID = Guid.NewGuid
            _C._RiskId = m_RiskID
        Else
            _C = Business.RiskActivities.RetreiveByID(m_ActivityID.Value)
        End If

        With _C
            ._Activity = txtActivity.Text
            ._HazardDesc = txtHazards.Text
            ._RiskDesc = txtRisks.Text
            ._PeopleDesc = txtPeople.Text
            ._Controls = txtControlMeasures.Text
            ._UcLikely = rcUncontrolled.Likelihood
            ._UcSeverity = rcUncontrolled.Severity
            ._UcRating = rcUncontrolled.Rating
            ._cLikely = rcResidual.Likelihood
            ._cSeverity = rcResidual.Severity
            ._cRating = rcResidual.Rating
            .Store()
        End With

        _C = Nothing

        Me.DialogResult = DialogResult.OK
        Me.Close()

    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        SaveAndExit()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub cbxHazards_Validated(sender As Object, e As EventArgs) Handles cbxHazards.Validated
        If txtHazards.Text = "" Then txtHazards.Text = cbxHazards.Text
    End Sub

    Private Sub cbxPeople_Validated(sender As Object, e As EventArgs) Handles cbxPeople.Validated
        If txtPeople.Text = "" Then txtPeople.Text = cbxPeople.Text
    End Sub

    Private Sub cbxRisks_Validated(sender As Object, e As EventArgs) Handles cbxRisks.Validated
        If txtRisks.Text = "" Then txtRisks.Text = cbxRisks.Text
    End Sub

End Class

