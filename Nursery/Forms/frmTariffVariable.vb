﻿Imports Care.Global
Imports Care.Data

Public Class frmTariffVariable

    Private m_TariffID As Guid? = Nothing
    Private m_TariffName As String = ""
    Private m_ID As Guid? = Nothing

    Public Sub New(ByVal TariffID As Guid, ByVal TariffName As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_TariffID = TariffID
        m_TariffName = TariffName

    End Sub

    Private Sub frmTariffVariable_Load(sender As Object, e As EventArgs) Handles Me.Load

        Me.Text = "Variable Rates for " + m_TariffName

        txtStartTime.BackColor = Session.ChangeColour
        txtEndTime.BackColor = Session.ChangeColour
        txtUnits.BackColor = Session.ChangeColour
        txtRate.BackColor = Session.ChangeColour

        PopulateGrid()

        gbxItem.Hide()

    End Sub

    Private Sub PopulateGrid()

        Dim _SQL As String = ""
        _SQL += "select id, start_time as 'Start Time', end_time as 'End Time',"
        _SQL += " units as 'Interval', rate as 'Rate'"
        _SQL += " from TariffVariable"
        _SQL += " where tariff_id = '" + m_TariffID.Value.ToString + "'"
        _SQL += " order by start_time"

        cgVariable.HideFirstColumn = True
        cgVariable.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Function Save() As Boolean

        If Not Valid Then Return False

        Dim _V As New Business.TariffVariable
        If m_ID.HasValue Then
            _V = Business.TariffVariable.RetreiveByID(m_ID.Value)
        Else
            _V._TariffId = m_TariffID
        End If

        With _V
            ._StartTime = CleanData.ReturnTimeSpan(txtStartTime.EditValue)
            ._EndTime = CleanData.ReturnTimeSpan(txtEndTime.EditValue)
            ._Units = CleanData.ReturnInteger(txtUnits.EditValue)
            ._Rate = CleanData.ReturnDecimal(txtRate.EditValue)
            .Store()
        End With

        Return True

    End Function

    Private Function ValidateTimes() As Boolean

        'return is hooked to e.cancel, so false is actually good!

        Dim _from As TimeSpan? = ConverttoTimeSpan(txtStartTime.Text)
        Dim _to As TimeSpan? = ConverttoTimeSpan(txtEndTime.Text)

        If _from.HasValue AndAlso _to.HasValue Then
            If _from.Value >= _to.Value Then
                txtStartTime.BackColor = Drawing.Color.LightCoral
                txtEndTime.BackColor = Drawing.Color.LightCoral
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If

    End Function

    Private Function ConverttoTimeSpan(ByVal TextIn As Object) As TimeSpan?

        If TextIn Is Nothing Then Return Nothing
        If TextIn.ToString = "" Then Return Nothing

        Try
            Return TimeSpan.Parse(TextIn.ToString)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Private Function Valid() As Boolean




        Return True

    End Function

    Private Sub txtStartTime_InvalidValue(sender As Object, e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles txtStartTime.InvalidValue
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub

    Private Sub txtStartTime_Validating(sender As Object, e As ComponentModel.CancelEventArgs) Handles txtStartTime.Validating
        e.Cancel = ValidateTimes()
    End Sub

    Private Sub txtEndTime_InvalidValue(sender As Object, e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles txtEndTime.InvalidValue
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub

    Private Sub txtEndTime_Validating(sender As Object, e As ComponentModel.CancelEventArgs) Handles txtEndTime.Validating
        e.Cancel = ValidateTimes()
    End Sub

    Private Sub cgVariable_AddClick(sender As Object, e As EventArgs) Handles cgVariable.AddClick

        m_ID = Nothing

        txtStartTime.Text = ""
        txtEndTime.Text = ""
        txtUnits.Text = ""
        txtRate.Text = ""

        gbxItem.Show()
        txtStartTime.Focus()

    End Sub

    Private Sub cgVariable_EditClick(sender As Object, e As EventArgs) Handles cgVariable.EditClick

        txtStartTime.EditValue = CleanData.ReturnTimeSpan(cgVariable.CurrentRow.Item("Start Time"))
        txtEndTime.EditValue = CleanData.ReturnTimeSpan(cgVariable.CurrentRow.Item("End Time"))
        txtUnits.EditValue = CleanData.ReturnInteger(cgVariable.CurrentRow.Item("Interval"))
        txtRate.EditValue = CleanData.ReturnDecimal(cgVariable.CurrentRow.Item("Rate"))

        gbxItem.Show()
        txtStartTime.Focus()

    End Sub

    Private Sub cgVariable_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles cgVariable.FocusedRowChanged
        m_ID = Nothing
        If cgVariable.CurrentRow IsNot Nothing Then
            If cgVariable.CurrentRow.Item("id") IsNot Nothing Then
                m_ID = New Guid(cgVariable.CurrentRow.Item("id").ToString)
            End If
        End If
    End Sub

    Private Sub cgVariable_RemoveClick(sender As Object, e As EventArgs) Handles cgVariable.RemoveClick
        If CareMessage("Are you sure you want to delete this rate?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Delete Rate") = DialogResult.Yes Then
            Dim _SQL As String = "delete from TariffVariable where ID = '" + m_ID.Value.ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)
            PopulateGrid()
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        gbxItem.Hide()
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        If Save Then
            cgVariable.RePopulate()
            gbxItem.Hide()
        End If
    End Sub

End Class
