﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTariffVariable
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.cgVariable = New Care.Controls.CareGridWithButtons()
        Me.gbxItem = New Care.Controls.CareFrame()
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        Me.btnOK = New Care.Controls.CareButton(Me.components)
        Me.txtRate = New Care.Controls.CareTextBox(Me.components)
        Me.txtUnits = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel15 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel16 = New Care.Controls.CareLabel(Me.components)
        Me.txtEndTime = New Care.Controls.CareTextBox(Me.components)
        Me.txtStartTime = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        CType(Me.gbxItem, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxItem.SuspendLayout()
        CType(Me.txtRate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUnits.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEndTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStartTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'cgVariable
        '
        Me.cgVariable.ButtonsEnabled = True
        Me.cgVariable.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgVariable.HideFirstColumn = False
        Me.cgVariable.Location = New System.Drawing.Point(12, 12)
        Me.cgVariable.Name = "cgVariable"
        Me.cgVariable.Size = New System.Drawing.Size(671, 407)
        Me.cgVariable.TabIndex = 0
        '
        'gbxItem
        '
        Me.gbxItem.Controls.Add(Me.btnCancel)
        Me.gbxItem.Controls.Add(Me.btnOK)
        Me.gbxItem.Controls.Add(Me.txtRate)
        Me.gbxItem.Controls.Add(Me.txtUnits)
        Me.gbxItem.Controls.Add(Me.CareLabel15)
        Me.gbxItem.Controls.Add(Me.CareLabel16)
        Me.gbxItem.Controls.Add(Me.txtEndTime)
        Me.gbxItem.Controls.Add(Me.txtStartTime)
        Me.gbxItem.Controls.Add(Me.CareLabel4)
        Me.gbxItem.Controls.Add(Me.CareLabel8)
        Me.gbxItem.Location = New System.Drawing.Point(234, 128)
        Me.gbxItem.Name = "gbxItem"
        Me.gbxItem.ShowCaption = False
        Me.gbxItem.Size = New System.Drawing.Size(224, 163)
        Me.gbxItem.TabIndex = 1
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(136, 132)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 9
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(55, 132)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 8
        Me.btnOK.Text = "OK"
        '
        'txtRate
        '
        Me.txtRate.CharacterCasing = CharacterCasing.Normal
        Me.txtRate.EnterMoveNextControl = True
        Me.txtRate.Location = New System.Drawing.Point(136, 97)
        Me.txtRate.MaxLength = 14
        Me.txtRate.Name = "txtRate"
        Me.txtRate.NumericAllowNegatives = False
        Me.txtRate.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtRate.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRate.Properties.AccessibleName = "Rate"
        Me.txtRate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRate.Properties.Appearance.Options.UseFont = True
        Me.txtRate.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRate.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtRate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtRate.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRate.Properties.MaxLength = 14
        Me.txtRate.ReadOnly = False
        Me.txtRate.Size = New System.Drawing.Size(75, 20)
        Me.txtRate.TabIndex = 7
        Me.txtRate.Tag = "AE"
        Me.txtRate.TextAlign = HorizontalAlignment.Left
        Me.txtRate.ToolTipText = ""
        '
        'txtUnits
        '
        Me.txtUnits.CharacterCasing = CharacterCasing.Normal
        Me.txtUnits.EnterMoveNextControl = True
        Me.txtUnits.Location = New System.Drawing.Point(136, 69)
        Me.txtUnits.MaxLength = 10
        Me.txtUnits.Name = "txtUnits"
        Me.txtUnits.NumericAllowNegatives = False
        Me.txtUnits.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtUnits.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtUnits.Properties.AccessibleName = "Charge Interval"
        Me.txtUnits.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtUnits.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtUnits.Properties.Appearance.Options.UseFont = True
        Me.txtUnits.Properties.Appearance.Options.UseTextOptions = True
        Me.txtUnits.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtUnits.Properties.Mask.EditMask = "##########;"
        Me.txtUnits.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtUnits.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtUnits.Properties.MaxLength = 10
        Me.txtUnits.ReadOnly = False
        Me.txtUnits.Size = New System.Drawing.Size(75, 20)
        Me.txtUnits.TabIndex = 5
        Me.txtUnits.Tag = "AE"
        Me.txtUnits.TextAlign = HorizontalAlignment.Left
        Me.txtUnits.ToolTipText = ""
        '
        'CareLabel15
        '
        Me.CareLabel15.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel15.Location = New System.Drawing.Point(12, 72)
        Me.CareLabel15.Name = "CareLabel15"
        Me.CareLabel15.Size = New System.Drawing.Size(117, 15)
        Me.CareLabel15.TabIndex = 4
        Me.CareLabel15.Text = "Charge Interval (mins)"
        Me.CareLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel16
        '
        Me.CareLabel16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel16.Location = New System.Drawing.Point(12, 100)
        Me.CareLabel16.Name = "CareLabel16"
        Me.CareLabel16.Size = New System.Drawing.Size(85, 15)
        Me.CareLabel16.TabIndex = 6
        Me.CareLabel16.Text = "Rate per Interval"
        Me.CareLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEndTime
        '
        Me.txtEndTime.CharacterCasing = CharacterCasing.Normal
        Me.txtEndTime.EnterMoveNextControl = True
        Me.txtEndTime.Location = New System.Drawing.Point(136, 41)
        Me.txtEndTime.MaxLength = 8
        Me.txtEndTime.Name = "txtEndTime"
        Me.txtEndTime.NumericAllowNegatives = False
        Me.txtEndTime.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.Time
        Me.txtEndTime.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtEndTime.Properties.AccessibleName = "End Time"
        Me.txtEndTime.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtEndTime.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtEndTime.Properties.Appearance.Options.UseFont = True
        Me.txtEndTime.Properties.Appearance.Options.UseTextOptions = True
        Me.txtEndTime.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtEndTime.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic
        Me.txtEndTime.Properties.Mask.EditMask = "(0?\d|1\d|2[0-3])\:[0-5]\d"
        Me.txtEndTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtEndTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtEndTime.Properties.MaxLength = 8
        Me.txtEndTime.ReadOnly = False
        Me.txtEndTime.Size = New System.Drawing.Size(75, 20)
        Me.txtEndTime.TabIndex = 3
        Me.txtEndTime.Tag = "AE"
        Me.txtEndTime.TextAlign = HorizontalAlignment.Left
        Me.txtEndTime.ToolTipText = ""
        '
        'txtStartTime
        '
        Me.txtStartTime.CharacterCasing = CharacterCasing.Normal
        Me.txtStartTime.EnterMoveNextControl = True
        Me.txtStartTime.Location = New System.Drawing.Point(136, 12)
        Me.txtStartTime.MaxLength = 8
        Me.txtStartTime.Name = "txtStartTime"
        Me.txtStartTime.NumericAllowNegatives = False
        Me.txtStartTime.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.Time
        Me.txtStartTime.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStartTime.Properties.AccessibleName = "Start Time"
        Me.txtStartTime.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtStartTime.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtStartTime.Properties.Appearance.Options.UseFont = True
        Me.txtStartTime.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStartTime.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtStartTime.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic
        Me.txtStartTime.Properties.Mask.EditMask = "(0?\d|1\d|2[0-3])\:[0-5]\d"
        Me.txtStartTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txtStartTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtStartTime.Properties.MaxLength = 8
        Me.txtStartTime.ReadOnly = False
        Me.txtStartTime.Size = New System.Drawing.Size(75, 20)
        Me.txtStartTime.TabIndex = 1
        Me.txtStartTime.Tag = "AE"
        Me.txtStartTime.TextAlign = HorizontalAlignment.Left
        Me.txtStartTime.ToolTipText = ""
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(11, 15)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(54, 15)
        Me.CareLabel4.TabIndex = 0
        Me.CareLabel4.Text = "Start Time"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(11, 44)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(50, 15)
        Me.CareLabel8.TabIndex = 2
        Me.CareLabel8.Text = "End Time"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmTariffVariable
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(695, 425)
        Me.Controls.Add(Me.gbxItem)
        Me.Controls.Add(Me.cgVariable)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTariffVariable"
        Me.StartPosition = FormStartPosition.CenterScreen
        CType(Me.gbxItem, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxItem.ResumeLayout(False)
        Me.gbxItem.PerformLayout()
        CType(Me.txtRate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUnits.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEndTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStartTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cgVariable As Care.Controls.CareGridWithButtons
    Friend WithEvents gbxItem As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents txtRate As Care.Controls.CareTextBox
    Friend WithEvents txtUnits As Care.Controls.CareTextBox
    Friend WithEvents CareLabel15 As Care.Controls.CareLabel
    Friend WithEvents CareLabel16 As Care.Controls.CareLabel
    Friend WithEvents txtEndTime As Care.Controls.CareTextBox
    Friend WithEvents txtStartTime As Care.Controls.CareTextBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel

End Class
