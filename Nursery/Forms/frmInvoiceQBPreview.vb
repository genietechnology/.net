﻿

Imports Care.Global
Imports Care.Data

Public Class frmInvoiceQBPreview

    Private m_BatchID As Guid?
    Private m_InvoiceData As New List(Of InvoiceData)

    Public Sub New(ByVal BatchID As Guid)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_BatchID = BatchID

    End Sub

    Private Class InvoiceData
        Property BatchNoAndInvoiceNo As String
        Property MonthYear As String
        Property SiteName As String
        Property FinancialsID As String
        Property FamilyName As String
        Property InvoiceDate As Date?
        Property InvoiceDue As Date?
        Property InvoicePeriodFrom As Date?
        Property InvoicePeriodTo As Date?
        Property ChildName As String
        Property ChildName2 As String
        Property AddressBlock As String
        Property LineDesc As String
        Property LineNLCode As String
        Property LineValue As Decimal
    End Class

    Private Sub frmInvoiceQBPreview_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim _SQL As String = ""

        _SQL += "select b.batch_no, b.site_name, s.nl_code, s.nl_tracking,"
        _SQL += " i.financials_id, i.family_name, i.family_account_no, i.invoice_no, i.invoice_date, i.invoice_due_date, i.invoice_period, i.invoice_period_end,"
        _SQL += " i.child_name, i.address, i.invoice_sub, i.discount_name, i.invoice_discount, i.invoice_total"
        _SQL += " from invoices i"
        _SQL += " left join InvoiceBatch b on b.ID = i.batch_id"
        _SQL += " left join Sites s on s.ID = b.site_id"
        _SQL += " where batch_id = '" + m_BatchID.ToString + "'"
        _SQL += " order by i.invoice_no"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                Dim _ID As InvoiceData = ReturnStaticInvoiceData(_DR)

                Dim _Discount As Decimal = ValueHandler.ConvertDecimal(_DR.Item("invoice_discount"))
                Dim _Total As Decimal = ValueHandler.ConvertDecimal(_DR.Item("invoice_total"))

                With _ID
                    _ID.LineDesc = ReturnLineDesc(_ID)
                    _ID.LineValue = _Total
                End With

                'so we just create one invoice data record
                m_InvoiceData.Add(_ID)

                If _Discount <> 0 Then

                    Dim _DID As InvoiceData = ReturnStaticInvoiceData(_DR)

                    _DID.LineDesc = "Discount (" + _DR.Item("discount_name").ToString + ")"
                    _DID.LineNLCode = "NLDISCOUNT"
                    _DID.LineValue = _Discount

                    m_InvoiceData.Add(_DID)

                End If

            Next

            cgData.Populate(m_InvoiceData)

        End If

    End Sub

    Private Function ReturnLineDesc(ByVal _ID As InvoiceData) As String
        Dim _Return As String = ""
        _Return += "Site: " + _ID.SiteName
        _Return += vbCr
        _Return += "Child: " + _ID.ChildName
        _Return += vbCr
        _Return += "Period: " + Format(_ID.InvoicePeriodFrom, "dd/MM/yyyy").ToString + " to " + Format(_ID.InvoicePeriodTo, "dd/MM/yyyy").ToString
        Return _Return
    End Function

    Private Function ReturnStaticInvoiceData(ByVal _DR As DataRow) As InvoiceData

        Dim _ID As New InvoiceData
        With _ID
            .BatchNoAndInvoiceNo = _DR.Item("batch_no").ToString + "-" + _DR.Item("invoice_no").ToString
            .SiteName = _DR.Item("site_name").ToString
            .FinancialsID = _DR.Item("financials_id").ToString
            .FamilyName = _DR.Item("family_name").ToString
            .InvoiceDate = ValueHandler.ConvertDate(_DR.Item("invoice_date"))
            .InvoiceDue = ValueHandler.ConvertDate(_DR.Item("invoice_due_date"))
            .InvoicePeriodFrom = ValueHandler.ConvertDate(_DR.Item("invoice_period"))
            .InvoicePeriodTo = ValueHandler.ConvertDate(_DR.Item("invoice_period_end"))
            .ChildName = _DR.Item("child_name").ToString
            .ChildName2 = .ChildName
            .AddressBlock = _DR.Item("address").ToString
            .LineDesc = ""
            .LineNLCode = _DR.Item("nl_code").ToString
            .LineValue = 0
            .MonthYear = DateAndTime.MonthName(.InvoicePeriodFrom.Value.Month, False) + " " + .InvoicePeriodFrom.Value.Year.ToString
        End With

        Return _ID

    End Function

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class
