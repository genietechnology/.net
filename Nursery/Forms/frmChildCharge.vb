﻿Imports Care.Global

Public Class frmChildCharge

    Private m_ChildID As Guid
    Private m_ChargeID As Guid?
    Private m_IsNew As Boolean = True

    Public Sub New(ByVal ChildID As Guid, ByVal ChargeID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        m_ChildID = ChildID

        If ChargeID.HasValue Then
            m_IsNew = False
            m_ChargeID = ChargeID
        Else
            m_IsNew = True
            m_ChargeID = Nothing
        End If

    End Sub

    Private Sub frmChildCharge_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cdtActionDate.BackColor = Session.ChangeColour
        txtDescription.BackColor = Session.ChangeColour
        txtReference.BackColor = Session.ChangeColour
        txtValue.BackColor = Session.ChangeColour

        If m_ChargeID.HasValue Then
            Me.Text = "Edit Charge"
            DisplayRecord()
        Else
            Me.Text = "Create New Charge"
        End If

    End Sub

    Private Sub frmSession_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        cdtActionDate.Focus()
    End Sub

    Private Sub DisplayRecord()

        Dim _C As Business.ChildCharge = Business.ChildCharge.RetreiveByID(m_ChargeID.Value)
        With _C
            cdtActionDate.Value = ._ActionDate
            txtDescription.Text = ._Description
            txtReference.Text = ._Reference
            txtValue.Text = ValueHandler.MoneyAsText(_C._Value)
        End With

        _C = Nothing

    End Sub

    Private Function ValidateCharge() As Boolean

        If cdtActionDate.Text = "" Then Return False
        If cdtActionDate.Value Is Nothing Then Return False
        If Not cdtActionDate.Value.HasValue Then Return False

        If txtDescription.Text = "" Then Return False
        If txtValue.Text = "" Then Return False

        Return True

    End Function

    Private Sub SaveAndExit()

        If Not ValidateCharge() Then Exit Sub

        Dim _C As Business.ChildCharge = Nothing

        If m_IsNew Then
            _C = New Business.ChildCharge
            _C._ID = Guid.NewGuid
            _C._ChildId = m_ChildID
        Else
            _C = Business.ChildCharge.RetreiveByID(m_ChargeID.Value)
        End If

        With _C
            ._ActionDate = cdtActionDate.Value
            ._Description = txtDescription.Text
            ._Reference = txtReference.Text
            ._Value = ValueHandler.ConvertDecimal(txtValue.Text)
            .Store()
        End With

        _C = Nothing

        Me.DialogResult = DialogResult.OK
        Me.Close()

    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        SaveAndExit()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub
End Class
