﻿Imports System.Windows.Forms
Imports Care.Global
Imports Care.Shared

Public Class frmInvoiceAdd

    Private m_InvoiceBatch As Business.InvoiceBatch
    Private m_ChildID As String = ""

    Public Sub New(ByVal InvoiceHeader As Business.InvoiceBatch, ByVal ChildID As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_InvoiceBatch = InvoiceHeader
        m_ChildID = ChildID

    End Sub

    Private Sub frmInvoiceAdd_Load(sender As Object, e As EventArgs) Handles Me.Load

        cbxLayout.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Invoice Layouts"))

        With cbxAdditional
            .AddItem("Excluded")
            .AddItem("Included")
            .AddItem("Only")
        End With

        With cbxGeneration
            .AddItem("Standard")
            .AddItem("Summarised (by Weekday)")
            .AddItem("Summarised (KLN)")
        End With

        If m_InvoiceBatch._BatchPeriod = "Empty Batch" Then
            cbxAdditional.SelectedIndex = 0
            cbxGeneration.SelectedIndex = 0
            cbxLayout.SelectedIndex = 0
        Else

            cdtPeriodStart.Value = m_InvoiceBatch._DateFirst
            cdtPeriodEnd.Value = m_InvoiceBatch._DateLast
            cdtInvoiceDate.Value = m_InvoiceBatch._BatchDate

            cbxAdditional.SelectedIndex = ValueHandler.ConvertInteger(m_InvoiceBatch._AdditionalSessions)
            cdtAddlFrom.Value = m_InvoiceBatch._AdditionalFrom
            cdtAddlTo.Value = m_InvoiceBatch._AdditionalTo

            cbxGeneration.SelectedIndex = ValueHandler.ConvertInteger(m_InvoiceBatch._GenerationMode)
            cbxLayout.Text = m_InvoiceBatch._BatchLayout

        End If

        btnSessions.Enabled = False
        btnOK.Enabled = False

        Dim _C As Business.Child = Business.Child.RetreiveByID(New Guid(m_ChildID))
        If _C IsNot Nothing Then

            txtChild.Text = _C._Fullname

            If _C._Dob.HasValue Then
                txtDOB.Text = Format(_C._Dob, "dd/MM/yyyy")
                lblAge.Text = ValueHandler.DateDifferenceAsText(_C._Dob.Value, Today, True, True, False)
            Else
                txtDOB.Text = Format(_C._Dob, "dd/MM/yyyy")
            End If

            txtRoom.Text = _C._GroupName

            If _C._Started.HasValue Then txtStart.Text = _C._Started.ToString
            If _C._DateLeft.HasValue Then txtLeave.Text = _C._DateLeft.ToString

            _C = Nothing

            btnSessions.Enabled = True
            btnOK.Enabled = True

        End If

    End Sub

    Private Sub frmInvoiceAdd_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        MyControls.SetControls(ControlHandler.Mode.Edit, Me.Controls, ControlHandler.DebugMode.None)
    End Sub

    Private Function CheckFields() As Boolean

        If cdtPeriodStart.Text = "" Then
            CareMessage("Please enter the start date of the Invoice Period.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If cdtPeriodEnd.Text = "" Then
            CareMessage("Please enter the end date of the Invoice Period.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If cdtPeriodEnd.Value.Value < cdtPeriodStart.Value.Value Then
            CareMessage("Invalid Invoice Period.", MessageBoxIcon.Exclamation, "Invoice Period Check")
            Return False
        End If

        If cdtInvoiceDate.Text = "" Then
            CareMessage("Please enter the Invoice Date.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If cbxAdditional.SelectedIndex > 0 Then

            If cdtAddlFrom.Text = "" AndAlso cdtAddlTo.Text = "" Then

                If CareMessage("You have not specified the period for additional sessions. Shall we default to the invoice period?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Additional Sessions") = DialogResult.Yes Then
                    cdtAddlFrom.Value = cdtPeriodStart.Value
                    cdtAddlTo.Value = cdtPeriodEnd.Value
                    Application.DoEvents()
                Else
                    Return False
                End If

            Else

                If cdtAddlFrom.Text = "" Then
                    CareMessage("Please enter the first date of the additional session period.", MessageBoxIcon.Exclamation, "Mandatory Field")
                    Return False
                End If

                If cdtAddlTo.Text = "" Then
                    CareMessage("Please enter the last date of the additional session period.", MessageBoxIcon.Exclamation, "Mandatory Field")
                    Return False
                End If

            End If

            If cdtAddlFrom.Value > cdtAddlTo.Value Then
                CareMessage("Please provide a valid additional session date range.", MessageBoxIcon.Exclamation, "Date range check")
                Return False
            End If

        End If

        If cbxGeneration.SelectedIndex < 0 Then
            CareMessage("Please select an invoice generation mode.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If cbxLayout.SelectedIndex < 0 Then
            CareMessage("Please select an invoice layout.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        Return True

    End Function

    Private Function CreateInvoice() As Boolean

        Dim _InvoiceNumber As Integer = ParameterHandler.ReturnInteger("NEXTINV", False)

        Dim _BatchLayout As String = cbxLayout.Text
        Dim _GenerationMode As Byte = ValueHandler.ConvertByte(cbxGeneration.SelectedIndex)
        Dim _TariffFilter As String = ""

        Dim _AdditionalSessions As Byte = ValueHandler.ConvertByte(cbxAdditional.SelectedIndex)
        Dim _AdditionalFrom As Date? = Nothing
        Dim _AdditionalTo As Date? = Nothing

        If cbxAdditional.SelectedIndex > 0 Then
            _AdditionalFrom = cdtAddlFrom.Value
            _AdditionalTo = cdtAddlTo.Value
        End If

        Dim _NewInvoice As Guid? = Invoicing.GenerateInvoice(m_InvoiceBatch._ID.Value, m_InvoiceBatch._BatchNo, New Guid(m_ChildID),
                                                             cdtInvoiceDate.Value.Value, cdtPeriodStart.Value.Value, cdtPeriodEnd.Value.Value,
                                                             _InvoiceNumber, False, _BatchLayout, CType(_AdditionalSessions, Invoicing.EnumAdditionalSessionMode), Invoicing.EnumDuplicateMode.Include, _TariffFilter,
                                                              CType(_GenerationMode, Invoicing.EnumGenerationMode), _AdditionalFrom, _AdditionalTo)
        If _NewInvoice IsNot Nothing Then
            ParameterHandler.ReturnInteger("NEXTINV", True)
        Else
            CareMessage("No Invoice was generated. Check Sessions exist for Invoice Period.", MessageBoxIcon.Exclamation, "Create Manual Invoice")
            Return False
        End If

        Return True

    End Function

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click

        If Not CheckFields() Then Exit Sub

        btnOK.Enabled = False
        btnCancel.Enabled = False
        Session.CursorWaiting()

        If CreateInvoice() Then
            btnOK.Enabled = False
            btnCancel.Enabled = True
            Session.CursorDefault()
            Me.DialogResult = DialogResult.OK
        Else
            btnOK.Enabled = True
            btnCancel.Enabled = True
            Session.CursorDefault()
        End If

    End Sub

    Private Sub btnSessions_Click(sender As Object, e As EventArgs) Handles btnSessions.Click
        Business.Child.DrillDown(New Guid(m_ChildID), True)
    End Sub

End Class
