﻿Imports Care.Global
Imports Care.Data

Public Class frmChangePIN

    Dim m_Child As Business.Child

    Public Sub New(ByRef Child As Business.Child)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_Child = Child

    End Sub

    Private Sub frmChangePassword_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        txtPIN.BackColor = Session.ChangeColour
    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnChange_Click(sender As System.Object, e As System.EventArgs) Handles btnChange.Click

        If txtPIN.Text = "" Then
            CareMessage("Please enter your new PIN.", MessageBoxIcon.Exclamation, "Change PIN")
            txtPIN.Focus()
            Exit Sub
        End If

        If txtPIN.Text.Length <> 4 Then
            CareMessage("PIN must be 4 characters.", MessageBoxIcon.Exclamation, "Change PIN")
            txtPIN.Text = ""
            txtPIN.Focus()
            Exit Sub
        End If

        If PINExists(txtPIN.Text) Then
            CareMessage("This PIN is already in use. Please enter a different PIN.", MessageBoxIcon.Exclamation, "Change PIN")
            txtPIN.Text = ""
            txtPIN.Focus()
            Exit Sub
        End If

        m_Child._PIN = txtPIN.Text
        m_Child.Store()

        CareMessage("PIN changed successfully.", MessageBoxIcon.Information, "Change PIN")

        Me.DialogResult = DialogResult.OK
        Me.Close()

    End Sub

    Private Function PINExists(PIN As String) As Boolean

        Dim _sql As String = "select top 1 * from children where ID <> '" + m_Child._ID.ToString + "' AND pin = '" & PIN & "'"
        Dim _table As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _sql)
        If _table IsNot Nothing AndAlso _table.Rows.Count > 0 Then
            Return True
        End If

        Return False

    End Function

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click

        If CareMessage("Are you sure you want to reset this child's PIN to be blank?",
                       MessageBoxIcon.Question, MessageBoxButtons.YesNo, "Reset PIN") = DialogResult.Yes Then

            m_Child._PIN = ""
            m_Child.Store()
            CareMessage("PIN reset successfully.", MessageBoxIcon.Information, "Reset PIN")

        End If

    End Sub

End Class
