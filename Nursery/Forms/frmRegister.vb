﻿Imports Care.Global
Imports System.Drawing
Imports System.Windows.Forms

Public Class frmRegister

    Private m_BookingDate As String = Format(Date.Today, "yyyy-MM-dd")
    Private m_DayID As String = ""

    Private Sub frmRegister_Load(sender As Object, e As EventArgs) Handles Me.Load
        gbxHistorical.Enabled = False
    End Sub

    Private Sub frmRegister_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        RefreshGrid()
        Application.DoEvents()
    End Sub

    Private Sub RefreshChildren()

        Dim _SQL As String = ""
        Dim _Out As String = "(select top 1 last_stamp from RegisterSummary out where out.date = r.date and out.person_id = r.person_id and out.in_out = 'O' order by last_stamp desc)"

        _SQL = "select person_id, person_name as 'Name', r.location as 'Location', first_stamp as 'In', " + _Out + " as 'Out'," &
               " CAST(duration AS MONEY) / 60 as 'Hours'" &
               " from RegisterSummary r" &
               " left join Children c on c.id = r.person_id" &
               " left join Groups on groups.id = c.group_id" &
               " where r.date = " & m_BookingDate &
               " and r.person_type = 'C'"

        If radRealtime.Checked Then

            If radCheckedIn.Checked Then
                _SQL += " and r.in_out = 'I'"
            End If

            If radCheckedOut.Checked Then
                _SQL += " and r.in_out = 'O'"
            End If

        End If

        _SQL += " order by Groups.sequence, c.Surname, c.Forename"

        cgChildren.HideFirstColumn = True
        cgChildren.Populate(Session.ConnectionString, _SQL)

        If radAbsent.Checked Or radHistAbsence.Checked Then
            cgChildren.Columns("Date/Time").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            cgChildren.Columns("Date/Time").DisplayFormat.FormatString = "HH:mm:ss"
        Else
            cgChildren.Columns("In").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            cgChildren.Columns("In").DisplayFormat.FormatString = "HH:mm:ss"
            cgChildren.Columns("Out").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            cgChildren.Columns("Out").DisplayFormat.FormatString = "HH:mm:ss"
        End If

    End Sub

    Private Sub RefreshAbsence()

        Dim _SQL As String = ""

        _SQL += "select c.id, c.fullname as 'Name', description as 'Reason', a.value_1 as 'Reported by', staff_name as 'Recorded by', stamp as 'Date/Time'"
        _SQL += " from Activity a"
        _SQL += " left join Children c on c.id = a.key_id"
        _SQL += " left join Day d on d.ID = a.day_id"
        _SQL += " where date = " + m_BookingDate
        _SQL += " and a.type = 'ABSENCE'"

        cgChildren.HideFirstColumn = True
        cgChildren.Populate(Session.ConnectionString, _SQL)

        cgChildren.Columns("Date/Time").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        cgChildren.Columns("Date/Time").DisplayFormat.FormatString = "HH:mm:ss"

    End Sub

    Private Sub RefreshVisitors()

        Dim _SQL As String = ""

        Dim _Out As String = "(select top 1 last_stamp from RegisterSummary out where out.date = r.date and out.person_id = r.person_id and out.in_out = 'O' order by last_stamp desc)"

        _SQL = "select person_id, person_name as 'Name', location as 'Visiting', first_stamp as 'In', " + _Out + " as 'Out'," & _
               " CAST(duration AS MONEY) / 60 as 'Hours'" & _
               " from RegisterSummary r" & _
               " where date = " & m_BookingDate & _
               " and person_type = 'V'"

        cgChildren.HideFirstColumn = True
        cgChildren.Populate(Session.ConnectionString, _SQL)

        cgChildren.Columns("In").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        cgChildren.Columns("In").DisplayFormat.FormatString = "HH:mm:ss"

        cgChildren.Columns("Out").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        cgChildren.Columns("Out").DisplayFormat.FormatString = "HH:mm:ss"

    End Sub

    Private Sub RefreshStaff()

        Dim _SQL As String

        Dim _Out As String = "(select top 1 last_stamp from RegisterSummary out where out.date = r.date and out.person_id = r.person_id and out.in_out = 'O' order by last_stamp desc)"

        _SQL = "select person_id, person_name as 'Name', r.location as 'Location', first_stamp as 'In', " + _Out + " as 'Out'," &
               " CAST(duration AS MONEY) / 60 as 'Hours'" &
               " from RegisterSummary r" &
               " left join Staff s on s.id = r.person_id" &
               " left join Groups on groups.id = s.group_id" &
               " where r.date = " & m_BookingDate &
               " and r.person_type = 'S'" &
               " order by Groups.sequence, s.Surname, s.Forename"

        cgStaff.HideFirstColumn = True
        cgStaff.Populate(Session.ConnectionString, _SQL)

        cgStaff.Columns("In").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        cgStaff.Columns("In").DisplayFormat.FormatString = "HH:mm:ss"

        cgStaff.Columns("Out").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        cgStaff.Columns("Out").DisplayFormat.FormatString = "HH:mm:ss"

    End Sub

    Private Sub RefreshGrid()

        If radRealtime.Checked Then
            m_BookingDate = ValueHandler.SQLDate(Today, True)
        Else
            If cdtDate.Text = "" Then
                CareMessage("Please enter or select the date you wish to view registers for.", MessageBoxIcon.Exclamation, "Refresh Grid")
                Exit Sub
            End If
            m_BookingDate = ValueHandler.SQLDate(cdtDate.Value.Value, True)
        End If

        Cursor.Current = Cursors.WaitCursor

        If radVisitors.Checked OrElse radHistVisitors.Checked Then
            RefreshVisitors()
        Else
            If radAbsent.Checked OrElse radHistAbsence.Checked Then
                RefreshAbsence()
            Else
                RefreshChildren()
                RefreshStaff()
            End If
        End If

        Cursor.Current = Cursors.Default

    End Sub

    Private Function ReturnColour(ByVal CellValue As String) As Color
        If CellValue = "F" Then Return txtGreen.BackColor
        If CellValue = "M" Then Return txtBlue.BackColor
        If CellValue = "A" Then Return txtBlue.BackColor
        If CellValue = "S" Then Return txtYellow.BackColor
        Return txtRed.BackColor
    End Function

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub cgChildren_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles cgChildren.RowStyle

        'If e.RowHandle < 0 Then Exit Sub

        'If radRealtime.Checked Then
        '    If cgChildren.UnderlyingGridView.GetRowCellDisplayText(e.RowHandle, "In").ToString = "" Then
        '        e.Appearance.BackColor = txtRed.BackColor
        '    Else
        '        If cgChildren.UnderlyingGridView.GetRowCellDisplayText(e.RowHandle, "Out").ToString = "" Then
        '            e.Appearance.BackColor = txtGreen.BackColor
        '        Else
        '            If CDate(cgChildren.UnderlyingGridView.GetRowCellDisplayText(e.RowHandle, "Out")) >= cgChildren.UnderlyingGridView.GetRowCellDisplayText(e.RowHandle, "In") Then
        '                e.Appearance.BackColor = txtRed.BackColor
        '            Else
        '                e.Appearance.BackColor = txtGreen.BackColor
        '            End If
        '        End If
        '    End If
        'Else
        '    e.Appearance.Reset()
        'End If

    End Sub

    Private Sub cgChildren_GridDoubleClick(sender As System.Object, e As System.EventArgs) Handles cgChildren.GridDoubleClick

        If cgChildren.RecordCount <= 0 Then Exit Sub

        If cgChildren.CurrentRow(0).ToString <> "" Then
            Dim _ID As New Guid(cgChildren.CurrentRow(0).ToString)
            Business.Child.DrillDown(_ID)
        End If

    End Sub

    Private Sub cgStaff_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles cgStaff.RowStyle

        'If e.RowHandle < 0 Then Exit Sub

        'If radRealtime.Checked Then
        '    If cgStaff.UnderlyingGridView.GetRowCellDisplayText(e.RowHandle, "In").ToString = "" Then
        '        e.Appearance.BackColor = txtRed.BackColor
        '    Else
        '        If cgStaff.UnderlyingGridView.GetRowCellDisplayText(e.RowHandle, "Out").ToString = "" Then
        '            e.Appearance.BackColor = txtGreen.BackColor
        '        Else
        '            If CDate(cgStaff.UnderlyingGridView.GetRowCellDisplayText(e.RowHandle, "Out")) >= cgStaff.UnderlyingGridView.GetRowCellDisplayText(e.RowHandle, "In") Then
        '                e.Appearance.BackColor = txtRed.BackColor
        '            Else
        '                e.Appearance.BackColor = txtGreen.BackColor
        '            End If
        '        End If
        '    End If
        'Else
        '    e.Appearance.Reset()
        'End If

    End Sub

    Private Sub cgStaff_GridDoubleClick(sender As Object, e As EventArgs) Handles cgStaff.GridDoubleClick

        If cgStaff.RecordCount <= 0 Then Exit Sub

        If cgStaff.CurrentRow(0).ToString <> "" Then
            Dim _ID As New Guid(cgStaff.CurrentRow(0).ToString)
            Business.Staff.DrillDown(_ID)
        End If

    End Sub

    Private Sub radRealtime_CheckedChanged(sender As Object, e As EventArgs) Handles radRealtime.CheckedChanged
        If radRealtime.Checked Then
            cdtDate.Text = ""
            gbxRealtime.Enabled = True
            gbxHistorical.Enabled = False
            RefreshGrid()
        End If
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        RefreshGrid()
    End Sub

    Private Sub radHistorical_CheckedChanged(sender As Object, e As EventArgs) Handles radHistorical.CheckedChanged
        If radHistorical.Checked Then
            gbxHistorical.Enabled = True
            gbxRealtime.Enabled = False
            cdtDate.Focus()
        End If
    End Sub

    Private Sub radCheckedIn_CheckedChanged(sender As Object, e As EventArgs) Handles radCheckedIn.CheckedChanged
        If radCheckedIn.Checked Then RefreshGrid()
    End Sub

    Private Sub radCheckedOut_CheckedChanged(sender As Object, e As EventArgs) Handles radCheckedOut.CheckedChanged
        If radCheckedOut.Checked Then RefreshGrid()
    End Sub

    Private Sub radAbsent_CheckedChanged(sender As Object, e As EventArgs) Handles radAbsent.CheckedChanged
        If radAbsent.Checked Then RefreshGrid()
    End Sub

    Private Sub radHistRegister_CheckedChanged(sender As Object, e As EventArgs) Handles radHistRegister.CheckedChanged
        If radHistRegister.Checked Then RefreshGrid()
    End Sub

    Private Sub radHistAbsence_CheckedChanged(sender As Object, e As EventArgs) Handles radHistAbsence.CheckedChanged
        If radHistAbsence.Checked Then RefreshGrid()
    End Sub

    Private Sub radHistVisitors_CheckedChanged(sender As Object, e As EventArgs) Handles radHistVisitors.CheckedChanged
        If radHistVisitors.Checked Then RefreshGrid()
    End Sub

    Private Sub radVisitors_CheckedChanged(sender As Object, e As EventArgs) Handles radVisitors.CheckedChanged
        If radVisitors.Checked Then RefreshGrid()
    End Sub
End Class
