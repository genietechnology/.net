﻿

Imports Care.Global

Public Class frmInvoiceLine

    Private m_Invoice As Business.Invoice
    Private m_LineID As Guid?
    Private m_Child As Business.Child = Nothing
    Private m_NewLine As Boolean = False

    Public Sub New(ByVal InvoiceObject As Business.Invoice, ByVal LineID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_Invoice = InvoiceObject
        m_LineID = LineID

        If Not LineID.HasValue Then m_NewLine = True

    End Sub

    Private Sub frmInvoiceLine_Load(sender As Object, e As EventArgs) Handles Me.Load

        lblDayName.Text = ""

        lblRef1.Hide()
        lblRef2.Hide()
        lblRef3.Hide()

        With cbxNLCode
            .AllowBlank = True
            .PopulateFromListMaintenance(Care.Shared.ListHandler.ReturnListFromCache("NL Codes"))
        End With

        With cbxNLTracking
            .AllowBlank = True
            .PopulateFromListMaintenance(Care.Shared.ListHandler.ReturnListFromCache("NL Tracking"))
        End With

        MyControls.SetControls(Care.Shared.ControlHandler.Mode.Add, Me.Controls, Care.Shared.ControlHandler.DebugMode.None)

        DisplayRecord()

    End Sub

    Private Sub DisplayRecord()

        Dim _Title As String = ""

        If m_NewLine Then
            _Title = "Add New"
        Else
            _Title = "Edit"
        End If

        _Title += " Line on Invoice: " + m_Invoice._InvoiceNo.ToString

        If m_Invoice._ChildId.HasValue Then
            m_Child = Business.Child.RetreiveByID(m_Invoice._ChildId.Value)
            _Title += " (" + m_Invoice._ChildName + ")"
        End If

        Me.Text = _Title

        If Not m_NewLine Then

            Dim _Line As Business.InvoiceLine = Business.InvoiceLine.RetreiveByID(m_LineID.Value)

            txtLineNo.Text = _Line._LineNo.ToString

            cdtLineActionDate.Value = _Line._ActionDate.Value
            lblDayName.Text = _Line._ActionDate.Value.DayOfWeek.ToString

            txtLineDescription.Text = _Line._Description

            cbxNLCode.Text = _Line._NlCode
            cbxNLTracking.Text = _Line._NlTracking

            If _Line._Discountable > 0 Then
                chkDiscount.Checked = True
            Else
                chkDiscount.Checked = False
            End If

            txtLineValue.Text = Format(_Line._Value, "0.00")

            lblRef1.Text = _Line._Ref1
            lblRef2.Text = _Line._Ref2
            lblRef3.Text = _Line._Ref3

        End If

    End Sub

    Private Function SaveLine() As Boolean

        If Not MyControls.Validate(Me.Controls) Then Return False

        'if tracking is on, check we have selected an item
        If FinanceShared.TrackingCategory <> "" Then
            If cbxNLTracking.Text = "" Then
                CareMessage("Please select a NL Tracking item.", MessageBoxIcon.Exclamation, "Mandatory Field")
                Return False
            End If
        End If

        Dim _Line As Business.InvoiceLine

        If m_NewLine Then
            _Line = New Business.InvoiceLine
            _Line._InvoiceId = m_Invoice._ID
            _Line._Section = "2"
            _Line._SummaryColumn = "X"
        Else
            _Line = Business.InvoiceLine.RetreiveByID(m_LineID.Value)
        End If

        _Line._LineNo = Integer.Parse(txtLineNo.Text)
        _Line._ActionDate = cdtLineActionDate.Value
        _Line._Description = txtLineDescription.Text

        _Line._NlCode = cbxNLCode.Text
        _Line._NlTracking = cbxNLTracking.Text

        _Line._Value = Decimal.Parse(txtLineValue.Text)

        If chkDiscount.Checked Then
            _Line._Discountable = _Line._Value
        Else
            _Line._Discountable = 0
        End If

        _Line.Store()
        _Line = Nothing

        Return True

    End Function

    Private Sub btnLineOK_Click(sender As Object, e As EventArgs) Handles btnLineOK.Click
        If SaveLine() Then
            Me.DialogResult = DialogResult.OK
            Me.Close()
        End If
    End Sub

    Private Sub btnLineCancel_Click(sender As Object, e As EventArgs) Handles btnLineCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub cdtLineActionDate_Validating(sender As Object, e As ComponentModel.CancelEventArgs) Handles cdtLineActionDate.Validating
        If cdtLineActionDate.Value.HasValue Then
            lblDayName.Text = cdtLineActionDate.Value.Value.DayOfWeek.ToString
        Else
            lblDayName.Text = ""
        End If
    End Sub

    Private Sub btnDefaultNL_Click(sender As Object, e As EventArgs) Handles btnDefaultNL.Click
        cbxNLCode.Text = Invoicing.ReturnNLCode(m_Child, ReturnNLCodeFromTariff(lblRef3.Text))
        cbxNLTracking.Text = Invoicing.ReturnNLTracking(m_Child, ReturnNLTrackingFromTariff(lblRef3.Text))
    End Sub

    Private Function ReturnNLCodeFromTariff(ByVal TariffID As String) As String

        If TariffID = "" Then Return ""
        Dim _Return As String = ""

        Dim _ID As Guid = Nothing
        If Guid.TryParse(TariffID, _ID) Then
            Dim _T As Business.Tariff = Business.Tariff.RetreiveByID(_ID)
            If _T IsNot Nothing Then
                _Return = _T._NlCode
            End If
        End If

        Return _Return

    End Function

    Private Function ReturnNLTrackingFromTariff(ByVal TariffID As String) As String

        If TariffID = "" Then Return ""
        Dim _Return As String = ""

        Dim _ID As Guid = Nothing
        If Guid.TryParse(TariffID, _ID) Then
            Dim _T As Business.Tariff = Business.Tariff.RetreiveByID(New Guid(TariffID))
            If _T IsNot Nothing Then
                _Return = _T._NlTracking
            End If
        End If

        Return _Return

    End Function

    Private Sub gbxLine_DoubleClick(sender As Object, e As EventArgs) Handles gbxLine.DoubleClick
        lblRef1.Show()
        lblRef2.Show()
        lblRef3.Show()
    End Sub

End Class