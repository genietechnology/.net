﻿Imports Care.Global

Public Class frmStaffTrain

    Private m_StaffID As Guid
    Private m_RecordID As Guid?
    Private m_IsNew As Boolean = True

    Public Sub New(ByVal StaffID As Guid, ByVal RecordID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        m_StaffID = StaffID

        If RecordID.HasValue Then
            m_IsNew = False
            m_RecordID = RecordID
        Else
            m_IsNew = True
            m_RecordID = Nothing
        End If

    End Sub

    Private Sub frmChildCharge_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cdtFrom.BackColor = Session.ChangeColour
        cdtTo.BackColor = Session.ChangeColour
        txtCourse.BackColor = Session.ChangeColour
        txtVenue.BackColor = Session.ChangeColour
        txtCost.BackColor = Session.ChangeColour

        If m_RecordID.HasValue Then
            Me.Text = "Edit Training Record"
            DisplayRecord()
        Else
            Me.Text = "Create New Training Record"
        End If

    End Sub

    Private Sub frmSession_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        cdtFrom.Focus()
    End Sub

    Private Sub DisplayRecord()

        Dim _C As Business.StaffTrain = Business.StaffTrain.RetreiveByID(m_RecordID.Value)
        With _C
            cdtFrom.Value = ._TrainFrom
            cdtTo.Value = ._TrainTo
            txtCourse.Text = ._TrainCourse
            txtVenue.Text = ._TrainVenue
            txtCost.Text = ValueHandler.MoneyAsText(_C._TrainCost)
        End With

        _C = Nothing

    End Sub

    Private Function ValidateEntry() As Boolean
        Return MyControls.Validate(Me.Controls)
    End Function

    Private Sub SaveAndExit()

        If Not ValidateEntry() Then Exit Sub

        Dim _C As Business.StaffTrain = Nothing

        If m_IsNew Then
            _C = New Business.StaffTrain
            _C._ID = Guid.NewGuid
            _C._StaffId = m_StaffID
        Else
            _C = Business.StaffTrain.RetreiveByID(m_RecordID.Value)
        End If

        With _C
            ._TrainFrom = cdtFrom.Value
            ._TrainTo = cdtTo.Value
            ._TrainCourse = txtCourse.Text
            ._TrainVenue = txtVenue.Text
            ._TrainCost = ValueHandler.ConvertDecimal(txtCost.Text)
            .Store()
        End With

        _C = Nothing

        Me.DialogResult = DialogResult.OK
        Me.Close()

    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        SaveAndExit()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub
End Class
