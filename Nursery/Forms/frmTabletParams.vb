﻿

Imports Care.Global
Imports Care.Data

Public Class frmTabletParams

    Dim m_TabletParam As Business.TabletParam

    Private Sub frmTabletParams_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        cbxType.AddItem("String")
        cbxType.AddItem("Boolean")

        Dim _SQL As String = ""

        _SQL += "select id, device as 'Endpoint', parent as 'Parent', param_id as 'Param ID',"
        _SQL += " param_desc as 'Description', param_type as 'Type', param_value as 'Value'"
        _SQL += " from TabletParams"
        _SQL += " order by parent, param_id"

        Me.GridSQL = _SQL

        gbx.Hide()
        chkValue.Hide()

    End Sub

    Protected Overrides Function BeforeAdd() As Boolean
        Clear()
        Return True
    End Function

    Private Sub Clear()
        radScopeMaster.Checked = True
        txtParent.Text = ""
        txtParamID.Text = ""
        txtDescription.Text = ""
        cbxType.Text = "String"
        txtValue.Text = ""
    End Sub

    Private Sub SetValueControls()
        If cbxType.Text = "Boolean" Then
            chkValue.Show()
            txtValue.Hide()
        Else
            chkValue.Hide()
            txtValue.Show()
        End If
    End Sub

    Protected Overrides Sub AfterAdd()
        SetEndpointControls()
        SetValueControls()
        txtParent.Focus()
    End Sub

    Protected Overrides Sub AfterEdit()

        If m_TabletParam._Device = "MASTER" Then
            radScopeMaster.Checked = True
        Else
            radScopeSpecific.Checked = True
        End If

        SetEndpointControls()

        txtEndpointName.Text = m_TabletParam._Device
        txtParent.Text = m_TabletParam._Parent
        txtParamID.Text = m_TabletParam._ParamId
        txtDescription.Text = m_TabletParam._ParamDesc
        cbxType.Text = m_TabletParam._ParamType

        If m_TabletParam._ParamType = "Boolean" Then

            Dim _Checked As Boolean = False
            cbxType.Text = "Boolean"

            If m_TabletParam._ParamValue.ToUpper = "Y" Then _Checked = True
            If m_TabletParam._ParamValue.ToUpper = "YES" Then _Checked = True
            If m_TabletParam._ParamValue.ToUpper = "T" Then _Checked = True
            If m_TabletParam._ParamValue.ToUpper = "TRUE" Then _Checked = True

            chkValue.Checked = _Checked
            chkValue.Focus()

        Else
            cbxType.Text = "String"
            txtValue.Text = m_TabletParam._ParamValue
            txtValue.Focus()
        End If

    End Sub

    Protected Overrides Sub BindToID(ID As System.Guid, IsNew As Boolean)
        If IsNew Then
            m_TabletParam = New Business.TabletParam
        Else
            m_TabletParam = Business.TabletParam.RetreiveByID(ID)
        End If
    End Sub

    Protected Overrides Sub CommitUpdate()

        If radScopeMaster.Checked Then
            m_TabletParam._Device = "MASTER"
        Else
            m_TabletParam._Device = txtEndpointName.Text
        End If

        m_TabletParam._Parent = txtParent.Text
        m_TabletParam._ParamId = txtParamID.Text
        m_TabletParam._ParamDesc = txtDescription.Text

        If cbxType.Text = "Boolean" Then
            m_TabletParam._ParamType = "Boolean"
            m_TabletParam._ParamValue = "FALSE"
            If chkValue.Checked Then m_TabletParam._ParamValue = "TRUE"
        Else
            m_TabletParam._ParamType = "String"
            m_TabletParam._ParamValue = txtValue.Text
        End If

        m_TabletParam.Store()

    End Sub

    Protected Overrides Sub CommitDelete(ByVal ID As Guid)

        Dim _SQL As String = "delete from TabletParams where ID = '" + ID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

    End Sub

    Private Sub SetEndpointControls()
        If radScopeMaster.Checked Then
            txtEndpointName.Text = "MASTER"
            txtEndpointName.ResetBackColor()
            txtEndpointName.ReadOnly = True

        Else
            txtEndpointName.Text = ""
            txtEndpointName.BackColor = Session.ChangeColour
            txtEndpointName.ReadOnly = False
        End If
    End Sub

    Private Sub radScopeMaster_CheckedChanged(sender As Object, e As EventArgs) Handles radScopeMaster.CheckedChanged
        If radScopeMaster.Checked Then SetEndpointControls()
    End Sub

    Private Sub radScopeSpecific_CheckedChanged(sender As Object, e As EventArgs) Handles radScopeSpecific.CheckedChanged
        If radScopeSpecific.Checked Then
            SetEndpointControls()
            txtEndpointName.Focus()
        End If
    End Sub

    Private Sub cbxType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxType.SelectedIndexChanged
        SetValueControls()
    End Sub
End Class