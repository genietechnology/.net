﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRecruit
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupBox1 = New Care.Controls.CareFrame(Me.components)
        Me.cdtDOB = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel26 = New Care.Controls.CareLabel(Me.components)
        Me.lblAge = New Care.Controls.CareLabel(Me.components)
        Me.Label5 = New Care.Controls.CareLabel(Me.components)
        Me.txtForename = New Care.Controls.CareTextBox(Me.components)
        Me.Label3 = New Care.Controls.CareLabel(Me.components)
        Me.txtSurname = New Care.Controls.CareTextBox(Me.components)
        Me.txtFullname = New Care.Controls.CareTextBox(Me.components)
        Me.Label1 = New Care.Controls.CareLabel(Me.components)
        Me.cdtIDWhen = New Care.Controls.CareDateTime(Me.components)
        Me.GroupBox5 = New Care.Controls.CareFrame(Me.components)
        Me.txtTelHome = New Care.[Shared].CareTelephoneNumber()
        Me.txtTelMobile = New Care.[Shared].CareTelephoneNumber()
        Me.txtEmail = New Care.[Shared].CareEmailAddress()
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.txtAddress = New Care.Address.CareAddress()
        Me.Label25 = New Care.Controls.CareLabel(Me.components)
        Me.Label23 = New Care.Controls.CareLabel(Me.components)
        Me.Label24 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl13 = New Care.Controls.CareFrame(Me.components)
        Me.cbxStage = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel10 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl1 = New Care.Controls.CareFrame(Me.components)
        Me.txtJobTitle = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.txtEmployer = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.txtSalary = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.txtHours = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl2 = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.cbxQualLevel = New Care.Controls.CareComboBox(Me.components)
        Me.GroupControl3 = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel22 = New Care.Controls.CareLabel(Me.components)
        Me.cdtDBSIssued = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel21 = New Care.Controls.CareLabel(Me.components)
        Me.txtRef2What = New Care.Controls.CareTextBox(Me.components)
        Me.txtRef1What = New Care.Controls.CareTextBox(Me.components)
        Me.cbxRef2Who = New Care.Controls.CareComboBox(Me.components)
        Me.cbxRef1Who = New Care.Controls.CareComboBox(Me.components)
        Me.CareCheckBox7 = New Care.Controls.CareCheckBox(Me.components)
        Me.CareCheckBox8 = New Care.Controls.CareCheckBox(Me.components)
        Me.cdtRef2When = New Care.Controls.CareDateTime(Me.components)
        Me.cdtRef1When = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel19 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel20 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel18 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel17 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel16 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel15 = New Care.Controls.CareLabel(Me.components)
        Me.txtRUKWhat = New Care.Controls.CareTextBox(Me.components)
        Me.txtQualWhat = New Care.Controls.CareTextBox(Me.components)
        Me.txtDBSWhat = New Care.Controls.CareTextBox(Me.components)
        Me.txtIDWhat = New Care.Controls.CareTextBox(Me.components)
        Me.cbxOverseasWho = New Care.Controls.CareComboBox(Me.components)
        Me.cbxRUKWho = New Care.Controls.CareComboBox(Me.components)
        Me.cbxDisWho = New Care.Controls.CareComboBox(Me.components)
        Me.cbxQualWho = New Care.Controls.CareComboBox(Me.components)
        Me.cbxBarrWho = New Care.Controls.CareComboBox(Me.components)
        Me.cbxDBSWho = New Care.Controls.CareComboBox(Me.components)
        Me.chkOverseas = New Care.Controls.CareCheckBox(Me.components)
        Me.CareCheckBox5 = New Care.Controls.CareCheckBox(Me.components)
        Me.CareCheckBox4 = New Care.Controls.CareCheckBox(Me.components)
        Me.chkQual = New Care.Controls.CareCheckBox(Me.components)
        Me.CareCheckBox2 = New Care.Controls.CareCheckBox(Me.components)
        Me.cdtOverseasWhen = New Care.Controls.CareDateTime(Me.components)
        Me.cdtRUKWhen = New Care.Controls.CareDateTime(Me.components)
        Me.cdtDisWhen = New Care.Controls.CareDateTime(Me.components)
        Me.cdtQualWhen = New Care.Controls.CareDateTime(Me.components)
        Me.cdtBarrWhen = New Care.Controls.CareDateTime(Me.components)
        Me.cdtDBSWhen = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel13 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel12 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.CareCheckBox1 = New Care.Controls.CareCheckBox(Me.components)
        Me.chkKeyworker = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.cbxIDWho = New Care.Controls.CareComboBox(Me.components)
        Me.btnNotes = New Care.Controls.CareButton(Me.components)
        Me.btnDocuments = New Care.Controls.CareButton(Me.components)
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        Me.btnOK = New Care.Controls.CareButton(Me.components)
        Me.btnEmployee = New Care.Controls.CareButton(Me.components)
        Me.cbxDBSCheckType = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel23 = New Care.Controls.CareLabel(Me.components)
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.cdtDOB.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDOB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtForename.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFullname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtIDWhen.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtIDWhen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        CType(Me.GroupControl13, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl13.SuspendLayout()
        CType(Me.cbxStage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtJobTitle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEmployer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSalary.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtHours.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.cbxQualLevel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.cdtDBSIssued.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDBSIssued.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRef2What.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRef1What.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxRef2Who.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxRef1Who.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CareCheckBox7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CareCheckBox8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtRef2When.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtRef2When.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtRef1When.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtRef1When.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRUKWhat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtQualWhat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDBSWhat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtIDWhat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxOverseasWho.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxRUKWho.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxDisWho.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxQualWho.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxBarrWho.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxDBSWho.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkOverseas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CareCheckBox5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CareCheckBox4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkQual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CareCheckBox2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtOverseasWhen.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtOverseasWhen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtRUKWhen.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtRUKWhen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDisWhen.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDisWhen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtQualWhen.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtQualWhen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtBarrWhen.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtBarrWhen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDBSWhen.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDBSWhen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CareCheckBox1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkKeyworker.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxIDWho.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxDBSCheckType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cdtDOB)
        Me.GroupBox1.Controls.Add(Me.CareLabel26)
        Me.GroupBox1.Controls.Add(Me.lblAge)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtForename)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtSurname)
        Me.GroupBox1.Controls.Add(Me.txtFullname)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 9)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(477, 114)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.Text = "Personal Details"
        '
        'cdtDOB
        '
        Me.cdtDOB.EditValue = New Date(2012, 6, 30, 0, 0, 0, 0)
        Me.cdtDOB.EnterMoveNextControl = True
        Me.cdtDOB.Location = New System.Drawing.Point(68, 84)
        Me.cdtDOB.Name = "cdtDOB"
        Me.cdtDOB.Properties.AccessibleName = "Date of Birth"
        Me.cdtDOB.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cdtDOB.Properties.Appearance.Options.UseFont = True
        Me.cdtDOB.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDOB.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtDOB.Size = New System.Drawing.Size(85, 22)
        Me.cdtDOB.TabIndex = 7
        Me.cdtDOB.Tag = "AEM"
        Me.cdtDOB.Value = New Date(2012, 6, 30, 0, 0, 0, 0)
        '
        'CareLabel26
        '
        Me.CareLabel26.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel26.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel26.Location = New System.Drawing.Point(249, 59)
        Me.CareLabel26.Name = "CareLabel26"
        Me.CareLabel26.Size = New System.Drawing.Size(47, 15)
        Me.CareLabel26.TabIndex = 4
        Me.CareLabel26.Text = "Surname"
        Me.CareLabel26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAge
        '
        Me.lblAge.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAge.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblAge.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblAge.Location = New System.Drawing.Point(159, 87)
        Me.lblAge.Name = "lblAge"
        Me.lblAge.Size = New System.Drawing.Size(147, 15)
        Me.lblAge.TabIndex = 8
        Me.lblAge.Text = "1 year, 11 months, 21 days"
        Me.lblAge.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label5.Location = New System.Drawing.Point(8, 87)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(24, 15)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "DOB"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtForename
        '
        Me.txtForename.CharacterCasing = CharacterCasing.Normal
        Me.txtForename.EnterMoveNextControl = True
        Me.txtForename.Location = New System.Drawing.Point(68, 56)
        Me.txtForename.MaxLength = 30
        Me.txtForename.Name = "txtForename"
        Me.txtForename.NumericAllowNegatives = False
        Me.txtForename.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtForename.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtForename.Properties.AccessibleName = "Forename"
        Me.txtForename.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtForename.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtForename.Properties.Appearance.Options.UseFont = True
        Me.txtForename.Properties.Appearance.Options.UseTextOptions = True
        Me.txtForename.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtForename.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtForename.Properties.MaxLength = 30
        Me.txtForename.Size = New System.Drawing.Size(170, 22)
        Me.txtForename.TabIndex = 3
        Me.txtForename.Tag = "AEM"
        Me.txtForename.TextAlign = HorizontalAlignment.Left
        Me.txtForename.ToolTipText = ""
        '
        'Label3
        '
        Me.Label3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label3.Location = New System.Drawing.Point(8, 59)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 15)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Forename"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSurname
        '
        Me.txtSurname.CharacterCasing = CharacterCasing.Normal
        Me.txtSurname.EnterMoveNextControl = True
        Me.txtSurname.Location = New System.Drawing.Point(302, 56)
        Me.txtSurname.MaxLength = 30
        Me.txtSurname.Name = "txtSurname"
        Me.txtSurname.NumericAllowNegatives = False
        Me.txtSurname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSurname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSurname.Properties.AccessibleName = "Surname"
        Me.txtSurname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSurname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSurname.Properties.Appearance.Options.UseFont = True
        Me.txtSurname.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSurname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSurname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSurname.Properties.MaxLength = 30
        Me.txtSurname.Size = New System.Drawing.Size(167, 22)
        Me.txtSurname.TabIndex = 5
        Me.txtSurname.Tag = "AEM"
        Me.txtSurname.TextAlign = HorizontalAlignment.Left
        Me.txtSurname.ToolTipText = ""
        '
        'txtFullname
        '
        Me.txtFullname.CharacterCasing = CharacterCasing.Normal
        Me.txtFullname.EnterMoveNextControl = True
        Me.txtFullname.Location = New System.Drawing.Point(68, 28)
        Me.txtFullname.MaxLength = 60
        Me.txtFullname.Name = "txtFullname"
        Me.txtFullname.NumericAllowNegatives = False
        Me.txtFullname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtFullname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFullname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFullname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFullname.Properties.Appearance.Options.UseFont = True
        Me.txtFullname.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFullname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFullname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFullname.Properties.MaxLength = 60
        Me.txtFullname.Properties.ReadOnly = True
        Me.txtFullname.Size = New System.Drawing.Size(401, 22)
        Me.txtFullname.TabIndex = 1
        Me.txtFullname.Tag = ""
        Me.txtFullname.TextAlign = HorizontalAlignment.Left
        Me.txtFullname.ToolTipText = ""
        '
        'Label1
        '
        Me.Label1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label1.Location = New System.Drawing.Point(8, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Fullname"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtIDWhen
        '
        Me.cdtIDWhen.EditValue = New Date(2012, 6, 30, 0, 0, 0, 0)
        Me.cdtIDWhen.EnterMoveNextControl = True
        Me.cdtIDWhen.Location = New System.Drawing.Point(222, 50)
        Me.cdtIDWhen.Name = "cdtIDWhen"
        Me.cdtIDWhen.Properties.AccessibleName = "Date of Birth"
        Me.cdtIDWhen.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cdtIDWhen.Properties.Appearance.Options.UseFont = True
        Me.cdtIDWhen.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtIDWhen.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtIDWhen.Size = New System.Drawing.Size(85, 22)
        Me.cdtIDWhen.TabIndex = 6
        Me.cdtIDWhen.Tag = "AE"
        Me.cdtIDWhen.Value = New Date(2012, 6, 30, 0, 0, 0, 0)
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.txtTelHome)
        Me.GroupBox5.Controls.Add(Me.txtTelMobile)
        Me.GroupBox5.Controls.Add(Me.txtEmail)
        Me.GroupBox5.Controls.Add(Me.CareLabel1)
        Me.GroupBox5.Controls.Add(Me.txtAddress)
        Me.GroupBox5.Controls.Add(Me.Label25)
        Me.GroupBox5.Controls.Add(Me.Label23)
        Me.GroupBox5.Controls.Add(Me.Label24)
        Me.GroupBox5.Location = New System.Drawing.Point(495, 72)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(338, 234)
        Me.GroupBox5.TabIndex = 4
        Me.GroupBox5.Text = "Address and Contact Numbers"
        '
        'txtTelHome
        '
        Me.txtTelHome.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTelHome.Appearance.Options.UseFont = True
        Me.txtTelHome.IsMobile = False
        Me.txtTelHome.Location = New System.Drawing.Point(114, 147)
        Me.txtTelHome.MaxLength = 15
        Me.txtTelHome.MinimumSize = New System.Drawing.Size(0, 22)
        Me.txtTelHome.Name = "txtTelHome"
        Me.txtTelHome.ReadOnly = False
        Me.txtTelHome.Size = New System.Drawing.Size(213, 22)
        Me.txtTelHome.TabIndex = 3
        Me.txtTelHome.Tag = "AE"
        '
        'txtTelMobile
        '
        Me.txtTelMobile.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTelMobile.Appearance.Options.UseFont = True
        Me.txtTelMobile.IsMobile = True
        Me.txtTelMobile.Location = New System.Drawing.Point(114, 176)
        Me.txtTelMobile.MaxLength = 15
        Me.txtTelMobile.MinimumSize = New System.Drawing.Size(0, 22)
        Me.txtTelMobile.Name = "txtTelMobile"
        Me.txtTelMobile.ReadOnly = False
        Me.txtTelMobile.Size = New System.Drawing.Size(213, 22)
        Me.txtTelMobile.TabIndex = 5
        Me.txtTelMobile.Tag = "AE"
        '
        'txtEmail
        '
        Me.txtEmail.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail.Appearance.Options.UseFont = True
        Me.txtEmail.Location = New System.Drawing.Point(114, 204)
        Me.txtEmail.MaxLength = 250
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.NoButton = False
        Me.txtEmail.NoColours = False
        Me.txtEmail.NoValidate = False
        Me.txtEmail.ReadOnly = False
        Me.txtEmail.Size = New System.Drawing.Size(213, 22)
        Me.txtEmail.TabIndex = 7
        Me.txtEmail.Tag = "AE"
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(8, 207)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(74, 15)
        Me.CareLabel1.TabIndex = 6
        Me.CareLabel1.Text = "Email Address"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAddress
        '
        Me.txtAddress.Address = ""
        Me.txtAddress.AddressBlock = ""
        Me.txtAddress.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.Appearance.Options.UseFont = True
        Me.txtAddress.County = ""
        Me.txtAddress.DisplayMode = Care.Address.CareAddress.EnumDisplayMode.SingleControl
        Me.txtAddress.Location = New System.Drawing.Point(114, 28)
        Me.txtAddress.MaxLength = 500
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.PostCode = ""
        Me.txtAddress.ReadOnly = False
        Me.txtAddress.Size = New System.Drawing.Size(213, 113)
        Me.txtAddress.TabIndex = 1
        Me.txtAddress.Tag = "AE"
        Me.txtAddress.Town = ""
        '
        'Label25
        '
        Me.Label25.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label25.Location = New System.Drawing.Point(8, 150)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(52, 15)
        Me.Label25.TabIndex = 2
        Me.Label25.Text = "Home Tel"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label23
        '
        Me.Label23.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label23.Location = New System.Drawing.Point(8, 179)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(56, 15)
        Me.Label23.TabIndex = 4
        Me.Label23.Text = "Mobile Tel"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label24
        '
        Me.Label24.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label24.Location = New System.Drawing.Point(8, 28)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(42, 15)
        Me.Label24.TabIndex = 0
        Me.Label24.Text = "Address"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl13
        '
        Me.GroupControl13.Controls.Add(Me.cbxStage)
        Me.GroupControl13.Controls.Add(Me.CareLabel10)
        Me.GroupControl13.Location = New System.Drawing.Point(495, 9)
        Me.GroupControl13.Name = "GroupControl13"
        Me.GroupControl13.Size = New System.Drawing.Size(338, 57)
        Me.GroupControl13.TabIndex = 3
        Me.GroupControl13.Text = "Current Stage"
        '
        'cbxStage
        '
        Me.cbxStage.AllowBlank = False
        Me.cbxStage.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxStage.DataSource = Nothing
        Me.cbxStage.DisplayMember = Nothing
        Me.cbxStage.EnterMoveNextControl = True
        Me.cbxStage.Location = New System.Drawing.Point(46, 27)
        Me.cbxStage.Name = "cbxStage"
        Me.cbxStage.Properties.AccessibleName = "Qualification Level"
        Me.cbxStage.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxStage.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxStage.Properties.Appearance.Options.UseFont = True
        Me.cbxStage.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxStage.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxStage.SelectedValue = Nothing
        Me.cbxStage.Size = New System.Drawing.Size(281, 22)
        Me.cbxStage.TabIndex = 1
        Me.cbxStage.Tag = "AE"
        Me.cbxStage.ValueMember = Nothing
        '
        'CareLabel10
        '
        Me.CareLabel10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel10.Location = New System.Drawing.Point(11, 30)
        Me.CareLabel10.Name = "CareLabel10"
        Me.CareLabel10.Size = New System.Drawing.Size(29, 15)
        Me.CareLabel10.TabIndex = 0
        Me.CareLabel10.Text = "Stage"
        Me.CareLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.txtJobTitle)
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Controls.Add(Me.txtEmployer)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.txtSalary)
        Me.GroupControl1.Controls.Add(Me.CareLabel5)
        Me.GroupControl1.Controls.Add(Me.txtHours)
        Me.GroupControl1.Controls.Add(Me.CareLabel6)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 129)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(477, 114)
        Me.GroupControl1.TabIndex = 1
        Me.GroupControl1.Text = "Current / Previous Employment"
        '
        'txtJobTitle
        '
        Me.txtJobTitle.CharacterCasing = CharacterCasing.Normal
        Me.txtJobTitle.EnterMoveNextControl = True
        Me.txtJobTitle.Location = New System.Drawing.Point(64, 56)
        Me.txtJobTitle.MaxLength = 100
        Me.txtJobTitle.Name = "txtJobTitle"
        Me.txtJobTitle.NumericAllowNegatives = False
        Me.txtJobTitle.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtJobTitle.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtJobTitle.Properties.AccessibleName = "Forename"
        Me.txtJobTitle.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtJobTitle.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobTitle.Properties.Appearance.Options.UseFont = True
        Me.txtJobTitle.Properties.Appearance.Options.UseTextOptions = True
        Me.txtJobTitle.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtJobTitle.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtJobTitle.Properties.MaxLength = 100
        Me.txtJobTitle.Size = New System.Drawing.Size(405, 22)
        Me.txtJobTitle.TabIndex = 3
        Me.txtJobTitle.Tag = "AE"
        Me.txtJobTitle.TextAlign = HorizontalAlignment.Left
        Me.txtJobTitle.ToolTipText = ""
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(8, 59)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(44, 15)
        Me.CareLabel3.TabIndex = 2
        Me.CareLabel3.Text = "Job Title"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmployer
        '
        Me.txtEmployer.CharacterCasing = CharacterCasing.Normal
        Me.txtEmployer.EnterMoveNextControl = True
        Me.txtEmployer.Location = New System.Drawing.Point(64, 28)
        Me.txtEmployer.MaxLength = 30
        Me.txtEmployer.Name = "txtEmployer"
        Me.txtEmployer.NumericAllowNegatives = False
        Me.txtEmployer.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtEmployer.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtEmployer.Properties.AccessibleName = "Forename"
        Me.txtEmployer.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtEmployer.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployer.Properties.Appearance.Options.UseFont = True
        Me.txtEmployer.Properties.Appearance.Options.UseTextOptions = True
        Me.txtEmployer.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtEmployer.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtEmployer.Properties.MaxLength = 30
        Me.txtEmployer.Size = New System.Drawing.Size(405, 22)
        Me.txtEmployer.TabIndex = 1
        Me.txtEmployer.Tag = "AE"
        Me.txtEmployer.TextAlign = HorizontalAlignment.Left
        Me.txtEmployer.ToolTipText = ""
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(280, 87)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(94, 15)
        Me.CareLabel2.TabIndex = 6
        Me.CareLabel2.Text = "Contracted Hours"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSalary
        '
        Me.txtSalary.CharacterCasing = CharacterCasing.Normal
        Me.txtSalary.EnterMoveNextControl = True
        Me.txtSalary.Location = New System.Drawing.Point(64, 84)
        Me.txtSalary.MaxLength = 10
        Me.txtSalary.Name = "txtSalary"
        Me.txtSalary.NumericAllowNegatives = False
        Me.txtSalary.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtSalary.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSalary.Properties.AccessibleName = "Forename"
        Me.txtSalary.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSalary.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSalary.Properties.Appearance.Options.UseFont = True
        Me.txtSalary.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSalary.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSalary.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtSalary.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtSalary.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSalary.Properties.MaxLength = 10
        Me.txtSalary.Size = New System.Drawing.Size(89, 22)
        Me.txtSalary.TabIndex = 5
        Me.txtSalary.Tag = "AE"
        Me.txtSalary.TextAlign = HorizontalAlignment.Left
        Me.txtSalary.ToolTipText = ""
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(8, 87)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(31, 15)
        Me.CareLabel5.TabIndex = 4
        Me.CareLabel5.Text = "Salary"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtHours
        '
        Me.txtHours.CharacterCasing = CharacterCasing.Normal
        Me.txtHours.EnterMoveNextControl = True
        Me.txtHours.Location = New System.Drawing.Point(380, 84)
        Me.txtHours.MaxLength = 5
        Me.txtHours.Name = "txtHours"
        Me.txtHours.NumericAllowNegatives = False
        Me.txtHours.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtHours.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtHours.Properties.AccessibleName = "Surname"
        Me.txtHours.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtHours.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHours.Properties.Appearance.Options.UseFont = True
        Me.txtHours.Properties.Appearance.Options.UseTextOptions = True
        Me.txtHours.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtHours.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtHours.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtHours.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtHours.Properties.MaxLength = 5
        Me.txtHours.Size = New System.Drawing.Size(89, 22)
        Me.txtHours.TabIndex = 7
        Me.txtHours.Tag = "AE"
        Me.txtHours.TextAlign = HorizontalAlignment.Left
        Me.txtHours.ToolTipText = ""
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(8, 31)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(50, 15)
        Me.CareLabel6.TabIndex = 0
        Me.CareLabel6.Text = "Employer"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.CareLabel7)
        Me.GroupControl2.Controls.Add(Me.cbxQualLevel)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 249)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(477, 57)
        Me.GroupControl2.TabIndex = 2
        Me.GroupControl2.Text = "Current Qualification Level"
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(11, 30)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(27, 15)
        Me.CareLabel7.TabIndex = 0
        Me.CareLabel7.Text = "Level"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxQualLevel
        '
        Me.cbxQualLevel.AllowBlank = False
        Me.cbxQualLevel.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxQualLevel.DataSource = Nothing
        Me.cbxQualLevel.DisplayMember = Nothing
        Me.cbxQualLevel.EnterMoveNextControl = True
        Me.cbxQualLevel.Location = New System.Drawing.Point(64, 27)
        Me.cbxQualLevel.Name = "cbxQualLevel"
        Me.cbxQualLevel.Properties.AccessibleName = "Qualification Level"
        Me.cbxQualLevel.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxQualLevel.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxQualLevel.Properties.Appearance.Options.UseFont = True
        Me.cbxQualLevel.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxQualLevel.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxQualLevel.SelectedValue = Nothing
        Me.cbxQualLevel.Size = New System.Drawing.Size(404, 22)
        Me.cbxQualLevel.TabIndex = 1
        Me.cbxQualLevel.Tag = "AEM"
        Me.cbxQualLevel.ValueMember = Nothing
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.CareLabel23)
        Me.GroupControl3.Controls.Add(Me.cbxDBSCheckType)
        Me.GroupControl3.Controls.Add(Me.CareLabel22)
        Me.GroupControl3.Controls.Add(Me.cdtDBSIssued)
        Me.GroupControl3.Controls.Add(Me.CareLabel21)
        Me.GroupControl3.Controls.Add(Me.txtRef2What)
        Me.GroupControl3.Controls.Add(Me.txtRef1What)
        Me.GroupControl3.Controls.Add(Me.cbxRef2Who)
        Me.GroupControl3.Controls.Add(Me.cbxRef1Who)
        Me.GroupControl3.Controls.Add(Me.CareCheckBox7)
        Me.GroupControl3.Controls.Add(Me.CareCheckBox8)
        Me.GroupControl3.Controls.Add(Me.cdtRef2When)
        Me.GroupControl3.Controls.Add(Me.cdtRef1When)
        Me.GroupControl3.Controls.Add(Me.CareLabel19)
        Me.GroupControl3.Controls.Add(Me.CareLabel20)
        Me.GroupControl3.Controls.Add(Me.CareLabel18)
        Me.GroupControl3.Controls.Add(Me.CareLabel17)
        Me.GroupControl3.Controls.Add(Me.CareLabel16)
        Me.GroupControl3.Controls.Add(Me.CareLabel15)
        Me.GroupControl3.Controls.Add(Me.txtRUKWhat)
        Me.GroupControl3.Controls.Add(Me.txtQualWhat)
        Me.GroupControl3.Controls.Add(Me.txtDBSWhat)
        Me.GroupControl3.Controls.Add(Me.txtIDWhat)
        Me.GroupControl3.Controls.Add(Me.cbxOverseasWho)
        Me.GroupControl3.Controls.Add(Me.cbxRUKWho)
        Me.GroupControl3.Controls.Add(Me.cbxDisWho)
        Me.GroupControl3.Controls.Add(Me.cbxQualWho)
        Me.GroupControl3.Controls.Add(Me.cbxBarrWho)
        Me.GroupControl3.Controls.Add(Me.cbxDBSWho)
        Me.GroupControl3.Controls.Add(Me.chkOverseas)
        Me.GroupControl3.Controls.Add(Me.CareCheckBox5)
        Me.GroupControl3.Controls.Add(Me.CareCheckBox4)
        Me.GroupControl3.Controls.Add(Me.chkQual)
        Me.GroupControl3.Controls.Add(Me.CareCheckBox2)
        Me.GroupControl3.Controls.Add(Me.cdtOverseasWhen)
        Me.GroupControl3.Controls.Add(Me.cdtRUKWhen)
        Me.GroupControl3.Controls.Add(Me.cdtDisWhen)
        Me.GroupControl3.Controls.Add(Me.cdtQualWhen)
        Me.GroupControl3.Controls.Add(Me.cdtBarrWhen)
        Me.GroupControl3.Controls.Add(Me.cdtDBSWhen)
        Me.GroupControl3.Controls.Add(Me.CareLabel14)
        Me.GroupControl3.Controls.Add(Me.CareLabel13)
        Me.GroupControl3.Controls.Add(Me.CareLabel12)
        Me.GroupControl3.Controls.Add(Me.CareLabel11)
        Me.GroupControl3.Controls.Add(Me.CareLabel9)
        Me.GroupControl3.Controls.Add(Me.CareLabel8)
        Me.GroupControl3.Controls.Add(Me.CareCheckBox1)
        Me.GroupControl3.Controls.Add(Me.chkKeyworker)
        Me.GroupControl3.Controls.Add(Me.CareLabel4)
        Me.GroupControl3.Controls.Add(Me.cdtIDWhen)
        Me.GroupControl3.Controls.Add(Me.cbxIDWho)
        Me.GroupControl3.Location = New System.Drawing.Point(12, 312)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(821, 332)
        Me.GroupControl3.TabIndex = 5
        Me.GroupControl3.Text = "Safer Recruitment Checks"
        '
        'CareLabel22
        '
        Me.CareLabel22.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel22.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel22.Location = New System.Drawing.Point(505, 109)
        Me.CareLabel22.Name = "CareLabel22"
        Me.CareLabel22.Size = New System.Drawing.Size(60, 15)
        Me.CareLabel22.TabIndex = 17
        Me.CareLabel22.Text = "Date Issued"
        Me.CareLabel22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtDBSIssued
        '
        Me.cdtDBSIssued.EditValue = New Date(2012, 6, 30, 0, 0, 0, 0)
        Me.cdtDBSIssued.EnterMoveNextControl = True
        Me.cdtDBSIssued.Location = New System.Drawing.Point(577, 106)
        Me.cdtDBSIssued.Name = "cdtDBSIssued"
        Me.cdtDBSIssued.Properties.AccessibleName = "Date of Birth"
        Me.cdtDBSIssued.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cdtDBSIssued.Properties.Appearance.Options.UseFont = True
        Me.cdtDBSIssued.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDBSIssued.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtDBSIssued.Size = New System.Drawing.Size(85, 22)
        Me.cdtDBSIssued.TabIndex = 18
        Me.cdtDBSIssued.Tag = "AE"
        Me.cdtDBSIssued.Value = New Date(2012, 6, 30, 0, 0, 0, 0)
        '
        'CareLabel21
        '
        Me.CareLabel21.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel21.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel21.Location = New System.Drawing.Point(504, 81)
        Me.CareLabel21.Name = "CareLabel21"
        Me.CareLabel21.Size = New System.Drawing.Size(62, 15)
        Me.CareLabel21.TabIndex = 13
        Me.CareLabel21.Text = "Check Type"
        Me.CareLabel21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRef2What
        '
        Me.txtRef2What.CharacterCasing = CharacterCasing.Normal
        Me.txtRef2What.EnterMoveNextControl = True
        Me.txtRef2What.Location = New System.Drawing.Point(504, 302)
        Me.txtRef2What.MaxLength = 30
        Me.txtRef2What.Name = "txtRef2What"
        Me.txtRef2What.NumericAllowNegatives = False
        Me.txtRef2What.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRef2What.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRef2What.Properties.AccessibleName = "Surname"
        Me.txtRef2What.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRef2What.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRef2What.Properties.Appearance.Options.UseFont = True
        Me.txtRef2What.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRef2What.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRef2What.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRef2What.Properties.MaxLength = 30
        Me.txtRef2What.Size = New System.Drawing.Size(306, 22)
        Me.txtRef2What.TabIndex = 50
        Me.txtRef2What.Tag = "AE"
        Me.txtRef2What.TextAlign = HorizontalAlignment.Left
        Me.txtRef2What.ToolTipText = ""
        '
        'txtRef1What
        '
        Me.txtRef1What.CharacterCasing = CharacterCasing.Normal
        Me.txtRef1What.EnterMoveNextControl = True
        Me.txtRef1What.Location = New System.Drawing.Point(504, 274)
        Me.txtRef1What.MaxLength = 30
        Me.txtRef1What.Name = "txtRef1What"
        Me.txtRef1What.NumericAllowNegatives = False
        Me.txtRef1What.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRef1What.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRef1What.Properties.AccessibleName = "Surname"
        Me.txtRef1What.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRef1What.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRef1What.Properties.Appearance.Options.UseFont = True
        Me.txtRef1What.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRef1What.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRef1What.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRef1What.Properties.MaxLength = 30
        Me.txtRef1What.Size = New System.Drawing.Size(306, 22)
        Me.txtRef1What.TabIndex = 45
        Me.txtRef1What.Tag = "AE"
        Me.txtRef1What.TextAlign = HorizontalAlignment.Left
        Me.txtRef1What.ToolTipText = ""
        '
        'cbxRef2Who
        '
        Me.cbxRef2Who.AllowBlank = False
        Me.cbxRef2Who.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxRef2Who.DataSource = Nothing
        Me.cbxRef2Who.DisplayMember = Nothing
        Me.cbxRef2Who.EnterMoveNextControl = True
        Me.cbxRef2Who.Location = New System.Drawing.Point(317, 302)
        Me.cbxRef2Who.Name = "cbxRef2Who"
        Me.cbxRef2Who.Properties.AccessibleName = "Qualification Level"
        Me.cbxRef2Who.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxRef2Who.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxRef2Who.Properties.Appearance.Options.UseFont = True
        Me.cbxRef2Who.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxRef2Who.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxRef2Who.SelectedValue = Nothing
        Me.cbxRef2Who.Size = New System.Drawing.Size(181, 22)
        Me.cbxRef2Who.TabIndex = 49
        Me.cbxRef2Who.Tag = "AE"
        Me.cbxRef2Who.ValueMember = Nothing
        '
        'cbxRef1Who
        '
        Me.cbxRef1Who.AllowBlank = False
        Me.cbxRef1Who.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxRef1Who.DataSource = Nothing
        Me.cbxRef1Who.DisplayMember = Nothing
        Me.cbxRef1Who.EnterMoveNextControl = True
        Me.cbxRef1Who.Location = New System.Drawing.Point(317, 274)
        Me.cbxRef1Who.Name = "cbxRef1Who"
        Me.cbxRef1Who.Properties.AccessibleName = "Qualification Level"
        Me.cbxRef1Who.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxRef1Who.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxRef1Who.Properties.Appearance.Options.UseFont = True
        Me.cbxRef1Who.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxRef1Who.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxRef1Who.SelectedValue = Nothing
        Me.cbxRef1Who.Size = New System.Drawing.Size(181, 22)
        Me.cbxRef1Who.TabIndex = 44
        Me.cbxRef1Who.Tag = "AE"
        Me.cbxRef1Who.ValueMember = Nothing
        '
        'CareCheckBox7
        '
        Me.CareCheckBox7.EditValue = True
        Me.CareCheckBox7.EnterMoveNextControl = True
        Me.CareCheckBox7.Location = New System.Drawing.Point(186, 303)
        Me.CareCheckBox7.Name = "CareCheckBox7"
        Me.CareCheckBox7.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareCheckBox7.Properties.Appearance.Options.UseFont = True
        Me.CareCheckBox7.Properties.Caption = ""
        Me.CareCheckBox7.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareCheckBox7.Properties.ReadOnly = True
        Me.CareCheckBox7.Size = New System.Drawing.Size(24, 19)
        Me.CareCheckBox7.TabIndex = 47
        Me.CareCheckBox7.Tag = ""
        '
        'CareCheckBox8
        '
        Me.CareCheckBox8.EditValue = True
        Me.CareCheckBox8.EnterMoveNextControl = True
        Me.CareCheckBox8.Location = New System.Drawing.Point(186, 275)
        Me.CareCheckBox8.Name = "CareCheckBox8"
        Me.CareCheckBox8.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareCheckBox8.Properties.Appearance.Options.UseFont = True
        Me.CareCheckBox8.Properties.Caption = ""
        Me.CareCheckBox8.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareCheckBox8.Properties.ReadOnly = True
        Me.CareCheckBox8.Size = New System.Drawing.Size(24, 19)
        Me.CareCheckBox8.TabIndex = 42
        Me.CareCheckBox8.Tag = ""
        '
        'cdtRef2When
        '
        Me.cdtRef2When.EditValue = New Date(2012, 6, 30, 0, 0, 0, 0)
        Me.cdtRef2When.EnterMoveNextControl = True
        Me.cdtRef2When.Location = New System.Drawing.Point(222, 302)
        Me.cdtRef2When.Name = "cdtRef2When"
        Me.cdtRef2When.Properties.AccessibleName = "Date of Birth"
        Me.cdtRef2When.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cdtRef2When.Properties.Appearance.Options.UseFont = True
        Me.cdtRef2When.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtRef2When.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtRef2When.Size = New System.Drawing.Size(85, 22)
        Me.cdtRef2When.TabIndex = 48
        Me.cdtRef2When.Tag = "AE"
        Me.cdtRef2When.Value = New Date(2012, 6, 30, 0, 0, 0, 0)
        '
        'cdtRef1When
        '
        Me.cdtRef1When.EditValue = New Date(2012, 6, 30, 0, 0, 0, 0)
        Me.cdtRef1When.EnterMoveNextControl = True
        Me.cdtRef1When.Location = New System.Drawing.Point(222, 274)
        Me.cdtRef1When.Name = "cdtRef1When"
        Me.cdtRef1When.Properties.AccessibleName = "Date of Birth"
        Me.cdtRef1When.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cdtRef1When.Properties.Appearance.Options.UseFont = True
        Me.cdtRef1When.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtRef1When.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtRef1When.Size = New System.Drawing.Size(85, 22)
        Me.cdtRef1When.TabIndex = 43
        Me.cdtRef1When.Tag = "AE"
        Me.cdtRef1When.Value = New Date(2012, 6, 30, 0, 0, 0, 0)
        '
        'CareLabel19
        '
        Me.CareLabel19.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel19.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel19.Location = New System.Drawing.Point(11, 277)
        Me.CareLabel19.Name = "CareLabel19"
        Me.CareLabel19.Size = New System.Drawing.Size(61, 15)
        Me.CareLabel19.TabIndex = 41
        Me.CareLabel19.Text = "Reference 1"
        Me.CareLabel19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel20
        '
        Me.CareLabel20.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel20.Location = New System.Drawing.Point(11, 305)
        Me.CareLabel20.Name = "CareLabel20"
        Me.CareLabel20.Size = New System.Drawing.Size(61, 15)
        Me.CareLabel20.TabIndex = 46
        Me.CareLabel20.Text = "Reference 2"
        Me.CareLabel20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel18
        '
        Me.CareLabel18.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel18.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.CareLabel18.Location = New System.Drawing.Point(504, 29)
        Me.CareLabel18.Name = "CareLabel18"
        Me.CareLabel18.Size = New System.Drawing.Size(306, 15)
        Me.CareLabel18.TabIndex = 3
        Me.CareLabel18.Text = "What was Evidenced / Checked"
        Me.CareLabel18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CareLabel17
        '
        Me.CareLabel17.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel17.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.CareLabel17.Location = New System.Drawing.Point(317, 28)
        Me.CareLabel17.Name = "CareLabel17"
        Me.CareLabel17.Size = New System.Drawing.Size(181, 16)
        Me.CareLabel17.TabIndex = 2
        Me.CareLabel17.Text = "Who Checked"
        Me.CareLabel17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CareLabel16
        '
        Me.CareLabel16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel16.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.CareLabel16.Location = New System.Drawing.Point(222, 28)
        Me.CareLabel16.Name = "CareLabel16"
        Me.CareLabel16.Size = New System.Drawing.Size(85, 16)
        Me.CareLabel16.TabIndex = 1
        Me.CareLabel16.Text = "Checked"
        Me.CareLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CareLabel15
        '
        Me.CareLabel15.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.CareLabel15.Location = New System.Drawing.Point(174, 28)
        Me.CareLabel15.Name = "CareLabel15"
        Me.CareLabel15.Size = New System.Drawing.Size(47, 15)
        Me.CareLabel15.TabIndex = 0
        Me.CareLabel15.Text = "Reqd"
        Me.CareLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtRUKWhat
        '
        Me.txtRUKWhat.CharacterCasing = CharacterCasing.Normal
        Me.txtRUKWhat.EnterMoveNextControl = True
        Me.txtRUKWhat.Location = New System.Drawing.Point(504, 218)
        Me.txtRUKWhat.MaxLength = 30
        Me.txtRUKWhat.Name = "txtRUKWhat"
        Me.txtRUKWhat.NumericAllowNegatives = False
        Me.txtRUKWhat.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRUKWhat.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRUKWhat.Properties.AccessibleName = "Surname"
        Me.txtRUKWhat.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRUKWhat.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRUKWhat.Properties.Appearance.Options.UseFont = True
        Me.txtRUKWhat.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRUKWhat.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRUKWhat.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRUKWhat.Properties.MaxLength = 30
        Me.txtRUKWhat.Size = New System.Drawing.Size(306, 22)
        Me.txtRUKWhat.TabIndex = 36
        Me.txtRUKWhat.Tag = "AE"
        Me.txtRUKWhat.TextAlign = HorizontalAlignment.Left
        Me.txtRUKWhat.ToolTipText = ""
        '
        'txtQualWhat
        '
        Me.txtQualWhat.CharacterCasing = CharacterCasing.Normal
        Me.txtQualWhat.EnterMoveNextControl = True
        Me.txtQualWhat.Location = New System.Drawing.Point(504, 162)
        Me.txtQualWhat.MaxLength = 30
        Me.txtQualWhat.Name = "txtQualWhat"
        Me.txtQualWhat.NumericAllowNegatives = False
        Me.txtQualWhat.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtQualWhat.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtQualWhat.Properties.AccessibleName = "Surname"
        Me.txtQualWhat.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtQualWhat.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtQualWhat.Properties.Appearance.Options.UseFont = True
        Me.txtQualWhat.Properties.Appearance.Options.UseTextOptions = True
        Me.txtQualWhat.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtQualWhat.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtQualWhat.Properties.MaxLength = 30
        Me.txtQualWhat.Size = New System.Drawing.Size(306, 22)
        Me.txtQualWhat.TabIndex = 27
        Me.txtQualWhat.Tag = "AE"
        Me.txtQualWhat.TextAlign = HorizontalAlignment.Left
        Me.txtQualWhat.ToolTipText = ""
        '
        'txtDBSWhat
        '
        Me.txtDBSWhat.CharacterCasing = CharacterCasing.Normal
        Me.txtDBSWhat.EnterMoveNextControl = True
        Me.txtDBSWhat.Location = New System.Drawing.Point(317, 106)
        Me.txtDBSWhat.MaxLength = 30
        Me.txtDBSWhat.Name = "txtDBSWhat"
        Me.txtDBSWhat.NumericAllowNegatives = False
        Me.txtDBSWhat.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtDBSWhat.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDBSWhat.Properties.AccessibleName = "Surname"
        Me.txtDBSWhat.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtDBSWhat.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDBSWhat.Properties.Appearance.Options.UseFont = True
        Me.txtDBSWhat.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDBSWhat.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtDBSWhat.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDBSWhat.Properties.MaxLength = 30
        Me.txtDBSWhat.Size = New System.Drawing.Size(181, 22)
        Me.txtDBSWhat.TabIndex = 16
        Me.txtDBSWhat.Tag = "AE"
        Me.txtDBSWhat.TextAlign = HorizontalAlignment.Left
        Me.txtDBSWhat.ToolTipText = ""
        '
        'txtIDWhat
        '
        Me.txtIDWhat.CharacterCasing = CharacterCasing.Normal
        Me.txtIDWhat.EnterMoveNextControl = True
        Me.txtIDWhat.Location = New System.Drawing.Point(504, 50)
        Me.txtIDWhat.MaxLength = 30
        Me.txtIDWhat.Name = "txtIDWhat"
        Me.txtIDWhat.NumericAllowNegatives = False
        Me.txtIDWhat.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtIDWhat.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtIDWhat.Properties.AccessibleName = "Surname"
        Me.txtIDWhat.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtIDWhat.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIDWhat.Properties.Appearance.Options.UseFont = True
        Me.txtIDWhat.Properties.Appearance.Options.UseTextOptions = True
        Me.txtIDWhat.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtIDWhat.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtIDWhat.Properties.MaxLength = 30
        Me.txtIDWhat.Size = New System.Drawing.Size(306, 22)
        Me.txtIDWhat.TabIndex = 8
        Me.txtIDWhat.Tag = "AE"
        Me.txtIDWhat.TextAlign = HorizontalAlignment.Left
        Me.txtIDWhat.ToolTipText = ""
        '
        'cbxOverseasWho
        '
        Me.cbxOverseasWho.AllowBlank = False
        Me.cbxOverseasWho.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxOverseasWho.DataSource = Nothing
        Me.cbxOverseasWho.DisplayMember = Nothing
        Me.cbxOverseasWho.EnterMoveNextControl = True
        Me.cbxOverseasWho.Location = New System.Drawing.Point(317, 246)
        Me.cbxOverseasWho.Name = "cbxOverseasWho"
        Me.cbxOverseasWho.Properties.AccessibleName = "Qualification Level"
        Me.cbxOverseasWho.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxOverseasWho.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxOverseasWho.Properties.Appearance.Options.UseFont = True
        Me.cbxOverseasWho.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxOverseasWho.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxOverseasWho.SelectedValue = Nothing
        Me.cbxOverseasWho.Size = New System.Drawing.Size(181, 22)
        Me.cbxOverseasWho.TabIndex = 40
        Me.cbxOverseasWho.Tag = "AE"
        Me.cbxOverseasWho.ValueMember = Nothing
        '
        'cbxRUKWho
        '
        Me.cbxRUKWho.AllowBlank = False
        Me.cbxRUKWho.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxRUKWho.DataSource = Nothing
        Me.cbxRUKWho.DisplayMember = Nothing
        Me.cbxRUKWho.EnterMoveNextControl = True
        Me.cbxRUKWho.Location = New System.Drawing.Point(317, 218)
        Me.cbxRUKWho.Name = "cbxRUKWho"
        Me.cbxRUKWho.Properties.AccessibleName = "Qualification Level"
        Me.cbxRUKWho.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxRUKWho.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxRUKWho.Properties.Appearance.Options.UseFont = True
        Me.cbxRUKWho.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxRUKWho.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxRUKWho.SelectedValue = Nothing
        Me.cbxRUKWho.Size = New System.Drawing.Size(181, 22)
        Me.cbxRUKWho.TabIndex = 35
        Me.cbxRUKWho.Tag = "AE"
        Me.cbxRUKWho.ValueMember = Nothing
        '
        'cbxDisWho
        '
        Me.cbxDisWho.AllowBlank = False
        Me.cbxDisWho.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxDisWho.DataSource = Nothing
        Me.cbxDisWho.DisplayMember = Nothing
        Me.cbxDisWho.EnterMoveNextControl = True
        Me.cbxDisWho.Location = New System.Drawing.Point(317, 190)
        Me.cbxDisWho.Name = "cbxDisWho"
        Me.cbxDisWho.Properties.AccessibleName = "Qualification Level"
        Me.cbxDisWho.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxDisWho.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxDisWho.Properties.Appearance.Options.UseFont = True
        Me.cbxDisWho.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxDisWho.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxDisWho.SelectedValue = Nothing
        Me.cbxDisWho.Size = New System.Drawing.Size(181, 22)
        Me.cbxDisWho.TabIndex = 31
        Me.cbxDisWho.Tag = "AE"
        Me.cbxDisWho.ValueMember = Nothing
        '
        'cbxQualWho
        '
        Me.cbxQualWho.AllowBlank = False
        Me.cbxQualWho.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxQualWho.DataSource = Nothing
        Me.cbxQualWho.DisplayMember = Nothing
        Me.cbxQualWho.EnterMoveNextControl = True
        Me.cbxQualWho.Location = New System.Drawing.Point(317, 162)
        Me.cbxQualWho.Name = "cbxQualWho"
        Me.cbxQualWho.Properties.AccessibleName = "Qualification Level"
        Me.cbxQualWho.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxQualWho.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxQualWho.Properties.Appearance.Options.UseFont = True
        Me.cbxQualWho.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxQualWho.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxQualWho.SelectedValue = Nothing
        Me.cbxQualWho.Size = New System.Drawing.Size(181, 22)
        Me.cbxQualWho.TabIndex = 26
        Me.cbxQualWho.Tag = "AE"
        Me.cbxQualWho.ValueMember = Nothing
        '
        'cbxBarrWho
        '
        Me.cbxBarrWho.AllowBlank = False
        Me.cbxBarrWho.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxBarrWho.DataSource = Nothing
        Me.cbxBarrWho.DisplayMember = Nothing
        Me.cbxBarrWho.EnterMoveNextControl = True
        Me.cbxBarrWho.Location = New System.Drawing.Point(317, 134)
        Me.cbxBarrWho.Name = "cbxBarrWho"
        Me.cbxBarrWho.Properties.AccessibleName = "Qualification Level"
        Me.cbxBarrWho.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxBarrWho.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxBarrWho.Properties.Appearance.Options.UseFont = True
        Me.cbxBarrWho.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxBarrWho.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxBarrWho.SelectedValue = Nothing
        Me.cbxBarrWho.Size = New System.Drawing.Size(181, 22)
        Me.cbxBarrWho.TabIndex = 22
        Me.cbxBarrWho.Tag = "AE"
        Me.cbxBarrWho.ValueMember = Nothing
        '
        'cbxDBSWho
        '
        Me.cbxDBSWho.AllowBlank = False
        Me.cbxDBSWho.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxDBSWho.DataSource = Nothing
        Me.cbxDBSWho.DisplayMember = Nothing
        Me.cbxDBSWho.EnterMoveNextControl = True
        Me.cbxDBSWho.Location = New System.Drawing.Point(317, 78)
        Me.cbxDBSWho.Name = "cbxDBSWho"
        Me.cbxDBSWho.Properties.AccessibleName = "Qualification Level"
        Me.cbxDBSWho.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxDBSWho.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxDBSWho.Properties.Appearance.Options.UseFont = True
        Me.cbxDBSWho.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxDBSWho.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxDBSWho.SelectedValue = Nothing
        Me.cbxDBSWho.Size = New System.Drawing.Size(181, 22)
        Me.cbxDBSWho.TabIndex = 12
        Me.cbxDBSWho.Tag = "AE"
        Me.cbxDBSWho.ValueMember = Nothing
        '
        'chkOverseas
        '
        Me.chkOverseas.EnterMoveNextControl = True
        Me.chkOverseas.Location = New System.Drawing.Point(186, 247)
        Me.chkOverseas.Name = "chkOverseas"
        Me.chkOverseas.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOverseas.Properties.Appearance.Options.UseFont = True
        Me.chkOverseas.Properties.Caption = ""
        Me.chkOverseas.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkOverseas.Size = New System.Drawing.Size(24, 19)
        Me.chkOverseas.TabIndex = 38
        Me.chkOverseas.Tag = "AE"
        '
        'CareCheckBox5
        '
        Me.CareCheckBox5.EditValue = True
        Me.CareCheckBox5.EnterMoveNextControl = True
        Me.CareCheckBox5.Location = New System.Drawing.Point(186, 219)
        Me.CareCheckBox5.Name = "CareCheckBox5"
        Me.CareCheckBox5.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareCheckBox5.Properties.Appearance.Options.UseFont = True
        Me.CareCheckBox5.Properties.Caption = ""
        Me.CareCheckBox5.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareCheckBox5.Properties.ReadOnly = True
        Me.CareCheckBox5.Size = New System.Drawing.Size(24, 19)
        Me.CareCheckBox5.TabIndex = 33
        Me.CareCheckBox5.Tag = ""
        '
        'CareCheckBox4
        '
        Me.CareCheckBox4.EditValue = True
        Me.CareCheckBox4.EnterMoveNextControl = True
        Me.CareCheckBox4.Location = New System.Drawing.Point(186, 191)
        Me.CareCheckBox4.Name = "CareCheckBox4"
        Me.CareCheckBox4.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareCheckBox4.Properties.Appearance.Options.UseFont = True
        Me.CareCheckBox4.Properties.Caption = ""
        Me.CareCheckBox4.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareCheckBox4.Properties.ReadOnly = True
        Me.CareCheckBox4.Size = New System.Drawing.Size(24, 19)
        Me.CareCheckBox4.TabIndex = 29
        Me.CareCheckBox4.Tag = ""
        '
        'chkQual
        '
        Me.chkQual.EnterMoveNextControl = True
        Me.chkQual.Location = New System.Drawing.Point(186, 163)
        Me.chkQual.Name = "chkQual"
        Me.chkQual.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkQual.Properties.Appearance.Options.UseFont = True
        Me.chkQual.Properties.Caption = ""
        Me.chkQual.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkQual.Size = New System.Drawing.Size(24, 19)
        Me.chkQual.TabIndex = 24
        Me.chkQual.Tag = "AE"
        '
        'CareCheckBox2
        '
        Me.CareCheckBox2.EditValue = True
        Me.CareCheckBox2.EnterMoveNextControl = True
        Me.CareCheckBox2.Location = New System.Drawing.Point(186, 135)
        Me.CareCheckBox2.Name = "CareCheckBox2"
        Me.CareCheckBox2.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareCheckBox2.Properties.Appearance.Options.UseFont = True
        Me.CareCheckBox2.Properties.Caption = ""
        Me.CareCheckBox2.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareCheckBox2.Properties.ReadOnly = True
        Me.CareCheckBox2.Size = New System.Drawing.Size(24, 19)
        Me.CareCheckBox2.TabIndex = 20
        Me.CareCheckBox2.Tag = ""
        '
        'cdtOverseasWhen
        '
        Me.cdtOverseasWhen.EditValue = New Date(2012, 6, 30, 0, 0, 0, 0)
        Me.cdtOverseasWhen.EnterMoveNextControl = True
        Me.cdtOverseasWhen.Location = New System.Drawing.Point(222, 246)
        Me.cdtOverseasWhen.Name = "cdtOverseasWhen"
        Me.cdtOverseasWhen.Properties.AccessibleName = "Date of Birth"
        Me.cdtOverseasWhen.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cdtOverseasWhen.Properties.Appearance.Options.UseFont = True
        Me.cdtOverseasWhen.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtOverseasWhen.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtOverseasWhen.Size = New System.Drawing.Size(85, 22)
        Me.cdtOverseasWhen.TabIndex = 39
        Me.cdtOverseasWhen.Tag = "AE"
        Me.cdtOverseasWhen.Value = New Date(2012, 6, 30, 0, 0, 0, 0)
        '
        'cdtRUKWhen
        '
        Me.cdtRUKWhen.EditValue = New Date(2012, 6, 30, 0, 0, 0, 0)
        Me.cdtRUKWhen.EnterMoveNextControl = True
        Me.cdtRUKWhen.Location = New System.Drawing.Point(222, 218)
        Me.cdtRUKWhen.Name = "cdtRUKWhen"
        Me.cdtRUKWhen.Properties.AccessibleName = "Date of Birth"
        Me.cdtRUKWhen.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cdtRUKWhen.Properties.Appearance.Options.UseFont = True
        Me.cdtRUKWhen.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtRUKWhen.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtRUKWhen.Size = New System.Drawing.Size(85, 22)
        Me.cdtRUKWhen.TabIndex = 34
        Me.cdtRUKWhen.Tag = "AE"
        Me.cdtRUKWhen.Value = New Date(2012, 6, 30, 0, 0, 0, 0)
        '
        'cdtDisWhen
        '
        Me.cdtDisWhen.EditValue = New Date(2012, 6, 30, 0, 0, 0, 0)
        Me.cdtDisWhen.EnterMoveNextControl = True
        Me.cdtDisWhen.Location = New System.Drawing.Point(222, 190)
        Me.cdtDisWhen.Name = "cdtDisWhen"
        Me.cdtDisWhen.Properties.AccessibleName = "Date of Birth"
        Me.cdtDisWhen.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cdtDisWhen.Properties.Appearance.Options.UseFont = True
        Me.cdtDisWhen.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDisWhen.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtDisWhen.Size = New System.Drawing.Size(85, 22)
        Me.cdtDisWhen.TabIndex = 30
        Me.cdtDisWhen.Tag = "AE"
        Me.cdtDisWhen.Value = New Date(2012, 6, 30, 0, 0, 0, 0)
        '
        'cdtQualWhen
        '
        Me.cdtQualWhen.EditValue = New Date(2012, 6, 30, 0, 0, 0, 0)
        Me.cdtQualWhen.EnterMoveNextControl = True
        Me.cdtQualWhen.Location = New System.Drawing.Point(222, 162)
        Me.cdtQualWhen.Name = "cdtQualWhen"
        Me.cdtQualWhen.Properties.AccessibleName = "Date of Birth"
        Me.cdtQualWhen.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cdtQualWhen.Properties.Appearance.Options.UseFont = True
        Me.cdtQualWhen.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtQualWhen.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtQualWhen.Size = New System.Drawing.Size(85, 22)
        Me.cdtQualWhen.TabIndex = 25
        Me.cdtQualWhen.Tag = "AE"
        Me.cdtQualWhen.Value = New Date(2012, 6, 30, 0, 0, 0, 0)
        '
        'cdtBarrWhen
        '
        Me.cdtBarrWhen.EditValue = New Date(2012, 6, 30, 0, 0, 0, 0)
        Me.cdtBarrWhen.EnterMoveNextControl = True
        Me.cdtBarrWhen.Location = New System.Drawing.Point(222, 134)
        Me.cdtBarrWhen.Name = "cdtBarrWhen"
        Me.cdtBarrWhen.Properties.AccessibleName = "Date of Birth"
        Me.cdtBarrWhen.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cdtBarrWhen.Properties.Appearance.Options.UseFont = True
        Me.cdtBarrWhen.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtBarrWhen.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtBarrWhen.Size = New System.Drawing.Size(85, 22)
        Me.cdtBarrWhen.TabIndex = 21
        Me.cdtBarrWhen.Tag = "AE"
        Me.cdtBarrWhen.Value = New Date(2012, 6, 30, 0, 0, 0, 0)
        '
        'cdtDBSWhen
        '
        Me.cdtDBSWhen.EditValue = New Date(2012, 6, 30, 0, 0, 0, 0)
        Me.cdtDBSWhen.EnterMoveNextControl = True
        Me.cdtDBSWhen.Location = New System.Drawing.Point(222, 78)
        Me.cdtDBSWhen.Name = "cdtDBSWhen"
        Me.cdtDBSWhen.Properties.AccessibleName = "Date of Birth"
        Me.cdtDBSWhen.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cdtDBSWhen.Properties.Appearance.Options.UseFont = True
        Me.cdtDBSWhen.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDBSWhen.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtDBSWhen.Size = New System.Drawing.Size(85, 22)
        Me.cdtDBSWhen.TabIndex = 11
        Me.cdtDBSWhen.Tag = "AE"
        Me.cdtDBSWhen.Value = New Date(2012, 6, 30, 0, 0, 0, 0)
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(11, 193)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(135, 15)
        Me.CareLabel14.TabIndex = 28
        Me.CareLabel14.Text = "Childcare Disqualification"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel13
        '
        Me.CareLabel13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel13.Location = New System.Drawing.Point(11, 221)
        Me.CareLabel13.Name = "CareLabel13"
        Me.CareLabel13.Size = New System.Drawing.Size(104, 15)
        Me.CareLabel13.TabIndex = 32
        Me.CareLabel13.Text = "Right to Work in UK"
        Me.CareLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel12
        '
        Me.CareLabel12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel12.Location = New System.Drawing.Point(11, 249)
        Me.CareLabel12.Name = "CareLabel12"
        Me.CareLabel12.Size = New System.Drawing.Size(47, 15)
        Me.CareLabel12.TabIndex = 37
        Me.CareLabel12.Text = "Overseas"
        Me.CareLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(11, 81)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(21, 15)
        Me.CareLabel11.TabIndex = 9
        Me.CareLabel11.Text = "DBS"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(11, 165)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(73, 15)
        Me.CareLabel9.TabIndex = 23
        Me.CareLabel9.Text = "Qualifications"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(11, 137)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(83, 15)
        Me.CareLabel8.TabIndex = 19
        Me.CareLabel8.Text = "ISA / Barred List"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareCheckBox1
        '
        Me.CareCheckBox1.EditValue = True
        Me.CareCheckBox1.EnterMoveNextControl = True
        Me.CareCheckBox1.Location = New System.Drawing.Point(186, 79)
        Me.CareCheckBox1.Name = "CareCheckBox1"
        Me.CareCheckBox1.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareCheckBox1.Properties.Appearance.Options.UseFont = True
        Me.CareCheckBox1.Properties.Caption = ""
        Me.CareCheckBox1.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareCheckBox1.Properties.ReadOnly = True
        Me.CareCheckBox1.Size = New System.Drawing.Size(24, 19)
        Me.CareCheckBox1.TabIndex = 10
        Me.CareCheckBox1.Tag = ""
        '
        'chkKeyworker
        '
        Me.chkKeyworker.EditValue = True
        Me.chkKeyworker.EnterMoveNextControl = True
        Me.chkKeyworker.Location = New System.Drawing.Point(186, 51)
        Me.chkKeyworker.Name = "chkKeyworker"
        Me.chkKeyworker.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkKeyworker.Properties.Appearance.Options.UseFont = True
        Me.chkKeyworker.Properties.Caption = ""
        Me.chkKeyworker.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkKeyworker.Properties.ReadOnly = True
        Me.chkKeyworker.Size = New System.Drawing.Size(24, 19)
        Me.chkKeyworker.TabIndex = 5
        Me.chkKeyworker.Tag = ""
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(11, 53)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(70, 15)
        Me.CareLabel4.TabIndex = 4
        Me.CareLabel4.Text = "Identification"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxIDWho
        '
        Me.cbxIDWho.AllowBlank = False
        Me.cbxIDWho.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxIDWho.DataSource = Nothing
        Me.cbxIDWho.DisplayMember = Nothing
        Me.cbxIDWho.EnterMoveNextControl = True
        Me.cbxIDWho.Location = New System.Drawing.Point(317, 50)
        Me.cbxIDWho.Name = "cbxIDWho"
        Me.cbxIDWho.Properties.AccessibleName = "Qualification Level"
        Me.cbxIDWho.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxIDWho.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxIDWho.Properties.Appearance.Options.UseFont = True
        Me.cbxIDWho.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxIDWho.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxIDWho.SelectedValue = Nothing
        Me.cbxIDWho.Size = New System.Drawing.Size(181, 22)
        Me.cbxIDWho.TabIndex = 7
        Me.cbxIDWho.Tag = "AE"
        Me.cbxIDWho.ValueMember = Nothing
        '
        'btnNotes
        '
        Me.btnNotes.Location = New System.Drawing.Point(12, 650)
        Me.btnNotes.Name = "btnNotes"
        Me.btnNotes.Size = New System.Drawing.Size(120, 25)
        Me.btnNotes.TabIndex = 6
        Me.btnNotes.Text = "Notes"
        '
        'btnDocuments
        '
        Me.btnDocuments.Location = New System.Drawing.Point(138, 650)
        Me.btnDocuments.Name = "btnDocuments"
        Me.btnDocuments.Size = New System.Drawing.Size(120, 25)
        Me.btnDocuments.TabIndex = 7
        Me.btnDocuments.Text = "Documents"
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(748, 650)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 25)
        Me.btnCancel.TabIndex = 10
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(657, 650)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(85, 25)
        Me.btnOK.TabIndex = 9
        Me.btnOK.Text = "OK"
        '
        'btnEmployee
        '
        Me.btnEmployee.Location = New System.Drawing.Point(264, 650)
        Me.btnEmployee.Name = "btnEmployee"
        Me.btnEmployee.Size = New System.Drawing.Size(120, 25)
        Me.btnEmployee.TabIndex = 8
        Me.btnEmployee.Text = "Convert to Employee"
        '
        'cbxDBSCheckType
        '
        Me.cbxDBSCheckType.AllowBlank = False
        Me.cbxDBSCheckType.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxDBSCheckType.DataSource = Nothing
        Me.cbxDBSCheckType.DisplayMember = Nothing
        Me.cbxDBSCheckType.EnterMoveNextControl = True
        Me.cbxDBSCheckType.Location = New System.Drawing.Point(577, 78)
        Me.cbxDBSCheckType.Name = "cbxDBSCheckType"
        Me.cbxDBSCheckType.Properties.AccessibleName = "Qualification Level"
        Me.cbxDBSCheckType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxDBSCheckType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxDBSCheckType.Properties.Appearance.Options.UseFont = True
        Me.cbxDBSCheckType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxDBSCheckType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxDBSCheckType.SelectedValue = Nothing
        Me.cbxDBSCheckType.Size = New System.Drawing.Size(233, 22)
        Me.cbxDBSCheckType.TabIndex = 14
        Me.cbxDBSCheckType.Tag = "AE"
        Me.cbxDBSCheckType.ValueMember = Nothing
        '
        'CareLabel23
        '
        Me.CareLabel23.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel23.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel23.Location = New System.Drawing.Point(290, 109)
        Me.CareLabel23.Name = "CareLabel23"
        Me.CareLabel23.Size = New System.Drawing.Size(17, 15)
        Me.CareLabel23.TabIndex = 15
        Me.CareLabel23.Text = "Ref"
        Me.CareLabel23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmRecruit
        '
        Me.AcceptButton = Me.btnOK
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(843, 682)
        Me.Controls.Add(Me.btnEmployee)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnDocuments)
        Me.Controls.Add(Me.btnNotes)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.GroupControl13)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmRecruit"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "frmRecruit"
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.cdtDOB.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDOB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtForename.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFullname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtIDWhen.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtIDWhen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.GroupControl13, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl13.ResumeLayout(False)
        Me.GroupControl13.PerformLayout()
        CType(Me.cbxStage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtJobTitle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEmployer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSalary.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtHours.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.cbxQualLevel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.cdtDBSIssued.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDBSIssued.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRef2What.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRef1What.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxRef2Who.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxRef1Who.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CareCheckBox7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CareCheckBox8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtRef2When.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtRef2When.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtRef1When.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtRef1When.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRUKWhat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtQualWhat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDBSWhat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtIDWhat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxOverseasWho.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxRUKWho.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxDisWho.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxQualWho.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxBarrWho.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxDBSWho.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkOverseas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CareCheckBox5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CareCheckBox4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkQual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CareCheckBox2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtOverseasWhen.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtOverseasWhen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtRUKWhen.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtRUKWhen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDisWhen.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDisWhen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtQualWhen.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtQualWhen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtBarrWhen.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtBarrWhen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDBSWhen.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDBSWhen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CareCheckBox1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkKeyworker.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxIDWho.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxDBSCheckType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CareLabel26 As Care.Controls.CareLabel
    Friend WithEvents lblAge As Care.Controls.CareLabel
    Friend WithEvents cdtIDWhen As Care.Controls.CareDateTime
    Friend WithEvents Label5 As Care.Controls.CareLabel
    Friend WithEvents txtForename As Care.Controls.CareTextBox
    Friend WithEvents Label3 As Care.Controls.CareLabel
    Friend WithEvents txtSurname As Care.Controls.CareTextBox
    Friend WithEvents txtFullname As Care.Controls.CareTextBox
    Friend WithEvents Label1 As Care.Controls.CareLabel
    Friend WithEvents txtTelHome As Care.Shared.CareTelephoneNumber
    Friend WithEvents txtTelMobile As Care.Shared.CareTelephoneNumber
    Friend WithEvents txtEmail As Care.Shared.CareEmailAddress
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents txtAddress As Care.Address.CareAddress
    Friend WithEvents Label25 As Care.Controls.CareLabel
    Friend WithEvents Label23 As Care.Controls.CareLabel
    Friend WithEvents Label24 As Care.Controls.CareLabel
    Friend WithEvents CareLabel10 As Care.Controls.CareLabel
    Friend WithEvents txtJobTitle As Care.Controls.CareTextBox
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents txtEmployer As Care.Controls.CareTextBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents txtSalary As Care.Controls.CareTextBox
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents txtHours As Care.Controls.CareTextBox
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents cbxQualLevel As Care.Controls.CareComboBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents cbxIDWho As Care.Controls.CareComboBox
    Friend WithEvents chkOverseas As Care.Controls.CareCheckBox
    Friend WithEvents CareCheckBox5 As Care.Controls.CareCheckBox
    Friend WithEvents CareCheckBox4 As Care.Controls.CareCheckBox
    Friend WithEvents chkQual As Care.Controls.CareCheckBox
    Friend WithEvents CareCheckBox2 As Care.Controls.CareCheckBox
    Friend WithEvents cdtOverseasWhen As Care.Controls.CareDateTime
    Friend WithEvents cdtRUKWhen As Care.Controls.CareDateTime
    Friend WithEvents cdtDisWhen As Care.Controls.CareDateTime
    Friend WithEvents cdtQualWhen As Care.Controls.CareDateTime
    Friend WithEvents cdtBarrWhen As Care.Controls.CareDateTime
    Friend WithEvents cdtDBSWhen As Care.Controls.CareDateTime
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents CareLabel13 As Care.Controls.CareLabel
    Friend WithEvents CareLabel12 As Care.Controls.CareLabel
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents CareCheckBox1 As Care.Controls.CareCheckBox
    Friend WithEvents chkKeyworker As Care.Controls.CareCheckBox
    Friend WithEvents cdtDOB As Care.Controls.CareDateTime
    Friend WithEvents txtRef2What As Care.Controls.CareTextBox
    Friend WithEvents txtRef1What As Care.Controls.CareTextBox
    Friend WithEvents cbxRef2Who As Care.Controls.CareComboBox
    Friend WithEvents cbxRef1Who As Care.Controls.CareComboBox
    Friend WithEvents CareCheckBox7 As Care.Controls.CareCheckBox
    Friend WithEvents CareCheckBox8 As Care.Controls.CareCheckBox
    Friend WithEvents cdtRef2When As Care.Controls.CareDateTime
    Friend WithEvents cdtRef1When As Care.Controls.CareDateTime
    Friend WithEvents CareLabel19 As Care.Controls.CareLabel
    Friend WithEvents CareLabel20 As Care.Controls.CareLabel
    Friend WithEvents CareLabel18 As Care.Controls.CareLabel
    Friend WithEvents CareLabel17 As Care.Controls.CareLabel
    Friend WithEvents CareLabel16 As Care.Controls.CareLabel
    Friend WithEvents CareLabel15 As Care.Controls.CareLabel
    Friend WithEvents txtRUKWhat As Care.Controls.CareTextBox
    Friend WithEvents txtQualWhat As Care.Controls.CareTextBox
    Friend WithEvents txtDBSWhat As Care.Controls.CareTextBox
    Friend WithEvents txtIDWhat As Care.Controls.CareTextBox
    Friend WithEvents cbxOverseasWho As Care.Controls.CareComboBox
    Friend WithEvents cbxRUKWho As Care.Controls.CareComboBox
    Friend WithEvents cbxDisWho As Care.Controls.CareComboBox
    Friend WithEvents cbxQualWho As Care.Controls.CareComboBox
    Friend WithEvents cbxBarrWho As Care.Controls.CareComboBox
    Friend WithEvents cbxDBSWho As Care.Controls.CareComboBox
    Friend WithEvents btnNotes As Care.Controls.CareButton
    Friend WithEvents btnDocuments As Care.Controls.CareButton
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents btnEmployee As Care.Controls.CareButton
    Friend WithEvents CareLabel21 As Care.Controls.CareLabel
    Friend WithEvents CareLabel22 As Care.Controls.CareLabel
    Friend WithEvents cdtDBSIssued As Care.Controls.CareDateTime
    Friend WithEvents cbxStage As Care.Controls.CareComboBox
    Friend WithEvents CareLabel23 As Care.Controls.CareLabel
    Friend WithEvents cbxDBSCheckType As Care.Controls.CareComboBox
    Friend WithEvents GroupBox1 As Care.Controls.CareFrame
    Friend WithEvents GroupBox5 As Care.Controls.CareFrame
    Friend WithEvents GroupControl13 As Care.Controls.CareFrame
    Friend WithEvents GroupControl1 As Care.Controls.CareFrame
    Friend WithEvents GroupControl2 As Care.Controls.CareFrame
    Friend WithEvents GroupControl3 As Care.Controls.CareFrame
End Class
