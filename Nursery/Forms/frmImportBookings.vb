﻿
Imports Care.Global
Imports Care.Data
Imports DevExpress.Spreadsheet

Public Class frmImportBookings

    Private m_Records As New List(Of ImportRecord)

    Private m_Failed As Integer = 0
    Private m_Passed As Integer = 0

    Private m_Importing As Boolean = False
    Private m_ImportPassed As Integer = 0
    Private m_ImportFailed As Integer = 0

    Private m_FamilyID As Guid?
    Private m_ChildID As Guid?
    Private m_Address As String
    Private m_PostCode As String

    Private Sub frmDataImport_Load(sender As Object, e As EventArgs) Handles Me.Load

        ofd.Filter = "XLSX Files|*.xlsx"
        ofd.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.Desktop

    End Sub

    Private Sub Preview()

        m_Records.Clear()
        m_Passed = 0
        m_Failed = 0

        Dim _wb As IWorkbook = sc.Document
        Dim _ws As Worksheet = _wb.Worksheets(0)

        Dim _Row As Integer = 5
        Dim _FirstCell As Cell = _ws.Columns("B")(_Row)

        While _FirstCell.DisplayText <> ""

            Dim _ChildSurname As Cell = _ws.Columns("B")(_Row)
            Dim _ChildForename As Cell = _ws.Columns("C")(_Row)

            Dim _WK1ThuAM As Cell = _ws.Columns("G")(_Row)
            Dim _WK1ThuPM As Cell = _ws.Columns("H")(_Row)
            Dim _WK1FriAM As Cell = _ws.Columns("I")(_Row)
            Dim _WK1FriPM As Cell = _ws.Columns("J")(_Row)

            Dim _WK2MonAM As Cell = _ws.Columns("K")(_Row)
            Dim _WK2MonPM As Cell = _ws.Columns("L")(_Row)
            Dim _WK2TueAM As Cell = _ws.Columns("M")(_Row)
            Dim _WK2TuePM As Cell = _ws.Columns("N")(_Row)
            Dim _WK2WedAM As Cell = _ws.Columns("O")(_Row)
            Dim _WK2WedPM As Cell = _ws.Columns("P")(_Row)
            Dim _WK2ThuAM As Cell = _ws.Columns("Q")(_Row)
            Dim _WK2ThuPM As Cell = _ws.Columns("R")(_Row)
            Dim _WK2FriAM As Cell = _ws.Columns("S")(_Row)
            Dim _WK2FriPM As Cell = _ws.Columns("T")(_Row)

            Dim _WK3MonAM As Cell = _ws.Columns("U")(_Row)
            Dim _WK3MonPM As Cell = _ws.Columns("V")(_Row)
            Dim _WK3TueAM As Cell = _ws.Columns("W")(_Row)
            Dim _WK3TuePM As Cell = _ws.Columns("X")(_Row)
            Dim _WK3WedAM As Cell = _ws.Columns("Y")(_Row)
            Dim _WK3WedPM As Cell = _ws.Columns("Z")(_Row)
            Dim _WK3ThuAM As Cell = _ws.Columns("AA")(_Row)
            Dim _WK3ThuPM As Cell = _ws.Columns("AB")(_Row)
            Dim _WK3FriAM As Cell = _ws.Columns("AC")(_Row)
            Dim _WK3FriPM As Cell = _ws.Columns("AD")(_Row)

            Dim _WK4MonAM As Cell = _ws.Columns("AE")(_Row)
            Dim _WK4MonPM As Cell = _ws.Columns("AF")(_Row)
            Dim _WK4TueAM As Cell = _ws.Columns("AG")(_Row)
            Dim _WK4TuePM As Cell = _ws.Columns("AH")(_Row)
            Dim _WK4WedAM As Cell = _ws.Columns("AI")(_Row)
            Dim _WK4WedPM As Cell = _ws.Columns("AJ")(_Row)
            Dim _WK4ThuAM As Cell = _ws.Columns("AK")(_Row)
            Dim _WK4ThuPM As Cell = _ws.Columns("AL")(_Row)
            Dim _WK4FriAM As Cell = _ws.Columns("AM")(_Row)
            Dim _WK4FriPM As Cell = _ws.Columns("AN")(_Row)

            Dim _WK5MonAM As Cell = _ws.Columns("AO")(_Row)
            Dim _WK5MonPM As Cell = _ws.Columns("AP")(_Row)
            Dim _WK5TueAM As Cell = _ws.Columns("AQ")(_Row)
            Dim _WK5TuePM As Cell = _ws.Columns("AR")(_Row)
            Dim _WK5WedAM As Cell = _ws.Columns("AS")(_Row)
            Dim _WK5WedPM As Cell = _ws.Columns("AT")(_Row)
            Dim _WK5ThuAM As Cell = _ws.Columns("AU")(_Row)
            Dim _WK5ThuPM As Cell = _ws.Columns("AV")(_Row)
            Dim _WK5FriAM As Cell = _ws.Columns("AW")(_Row)
            Dim _WK5FriPM As Cell = _ws.Columns("AX")(_Row)

            Dim _WK6MonAM As Cell = _ws.Columns("AY")(_Row)
            Dim _WK6MonPM As Cell = _ws.Columns("AZ")(_Row)
            Dim _WK6TueAM As Cell = _ws.Columns("BA")(_Row)
            Dim _WK6TuePM As Cell = _ws.Columns("BB")(_Row)
            Dim _WK6WedAM As Cell = _ws.Columns("BC")(_Row)
            Dim _WK6WedPM As Cell = _ws.Columns("BD")(_Row)
            Dim _WK6ThuAM As Cell = _ws.Columns("BE")(_Row)
            Dim _WK6ThuPM As Cell = _ws.Columns("BF")(_Row)
            Dim _WK6FriAM As Cell = _ws.Columns("BG")(_Row)
            Dim _WK6FriPM As Cell = _ws.Columns("BH")(_Row)

            Dim _WK7MonAM As Cell = _ws.Columns("BI")(_Row)
            Dim _WK7MonPM As Cell = _ws.Columns("BJ")(_Row)
            Dim _WK7TueAM As Cell = _ws.Columns("BK")(_Row)
            Dim _WK7TuePM As Cell = _ws.Columns("BL")(_Row)
            Dim _WK7WedAM As Cell = _ws.Columns("BM")(_Row)
            Dim _WK7WedPM As Cell = _ws.Columns("BN")(_Row)
            Dim _WK7ThuAM As Cell = _ws.Columns("BO")(_Row)
            Dim _WK7ThuPM As Cell = _ws.Columns("BP")(_Row)
            Dim _WK7FriAM As Cell = _ws.Columns("BQ")(_Row)
            Dim _WK7FriPM As Cell = _ws.Columns("BR")(_Row)

            Dim _Time As Cell = _ws.Columns("BW")(_Row)

            Dim _Import As Boolean = False

            Dim _r As New ImportRecord
            With _r

                .Errors = 0

                .ChildSurname = ReturnString(_ChildSurname)
                .ChildForename = ReturnString(_ChildForename)

                If .ChildSurname <> "" AndAlso .ChildForename <> "" Then _Import = True

                If _Import Then

                    .WK1Thu = ReturnTariff(_WK1ThuAM, _WK1ThuPM, _Time, .Errors)
                    .WK1Fri = ReturnTariff(_WK1FriAM, _WK1FriPM, _Time, .Errors)

                    .WK2Mon = ReturnTariff(_WK2MonAM, _WK2MonPM, _Time, .Errors)
                    .WK2Tue = ReturnTariff(_WK2TueAM, _WK2TuePM, _Time, .Errors)
                    .WK2Wed = ReturnTariff(_WK2WedAM, _WK2WedPM, _Time, .Errors)
                    .WK2Thu = ReturnTariff(_WK2ThuAM, _WK2ThuPM, _Time, .Errors)
                    .WK2Fri = ReturnTariff(_WK2FriAM, _WK2FriPM, _Time, .Errors)

                    .WK3Mon = ReturnTariff(_WK3MonAM, _WK3MonPM, _Time, .Errors)
                    .WK3Tue = ReturnTariff(_WK3TueAM, _WK3TuePM, _Time, .Errors)
                    .WK3Wed = ReturnTariff(_WK3WedAM, _WK3WedPM, _Time, .Errors)
                    .WK3Thu = ReturnTariff(_WK3ThuAM, _WK3ThuPM, _Time, .Errors)
                    .WK3Fri = ReturnTariff(_WK3FriAM, _WK3FriPM, _Time, .Errors)

                    .WK4Mon = ReturnTariff(_WK4MonAM, _WK4MonPM, _Time, .Errors)
                    .WK4Tue = ReturnTariff(_WK4TueAM, _WK4TuePM, _Time, .Errors)
                    .WK4Wed = ReturnTariff(_WK4WedAM, _WK4WedPM, _Time, .Errors)
                    .WK4Thu = ReturnTariff(_WK4ThuAM, _WK4ThuPM, _Time, .Errors)
                    .WK4Fri = ReturnTariff(_WK4FriAM, _WK4FriPM, _Time, .Errors)

                    .WK5Mon = ReturnTariff(_WK5MonAM, _WK5MonPM, _Time, .Errors)
                    .WK5Tue = ReturnTariff(_WK5TueAM, _WK5TuePM, _Time, .Errors)
                    .WK5Wed = ReturnTariff(_WK5WedAM, _WK5WedPM, _Time, .Errors)
                    .WK5Thu = ReturnTariff(_WK5ThuAM, _WK5ThuPM, _Time, .Errors)
                    .WK5Fri = ReturnTariff(_WK5FriAM, _WK5FriPM, _Time, .Errors)

                    .WK6Mon = ReturnTariff(_WK6MonAM, _WK6MonPM, _Time, .Errors)
                    .WK6Tue = ReturnTariff(_WK6TueAM, _WK6TuePM, _Time, .Errors)
                    .WK6Wed = ReturnTariff(_WK6WedAM, _WK6WedPM, _Time, .Errors)
                    .WK6Thu = ReturnTariff(_WK6ThuAM, _WK6ThuPM, _Time, .Errors)
                    .WK6Fri = ReturnTariff(_WK6FriAM, _WK6FriPM, _Time, .Errors)

                    .WK7Mon = ReturnTariff(_WK7MonAM, _WK7MonPM, _Time, .Errors)
                    .WK7Tue = ReturnTariff(_WK7TueAM, _WK7TuePM, _Time, .Errors)
                    .WK7Wed = ReturnTariff(_WK7WedAM, _WK7WedPM, _Time, .Errors)
                    .WK7Thu = ReturnTariff(_WK7ThuAM, _WK7ThuPM, _Time, .Errors)
                    .WK7Fri = ReturnTariff(_WK7FriAM, _WK7FriPM, _Time, .Errors)

                    .Valid = ValidateRecord(_r)

                    If .Valid Then
                        m_Passed += 1
                    Else
                        m_Failed += 1
                    End If

                    m_Records.Add(_r)

                End If

            End With

            _Row += 1
            _FirstCell = _ws.Columns("A")(_Row)

        End While

    End Sub

    Private Function ReturnTariff(ByVal AM As Cell, ByVal PM As Cell, ByVal Time As Cell, ByRef Errors As Integer) As String

        Dim _AM As String = AM.Value.ToString
        Dim _PM As String = PM.Value.ToString
        Dim _Time As String = Time.Value.ToString

        If _AM = "" Then _AM = "0"
        If _PM = "" Then _PM = "0"

        If _AM = "0" AndAlso _PM = "0" Then Return ""

        Dim _Tariff As String = ""

        If _AM = "1" Then
            _Tariff = ReturnTariff(AM.FillColor.ToArgb, _Time)
        Else
            If _PM = "1" Then
                _Tariff = ReturnTariff(PM.FillColor.ToArgb, _Time)
            End If
        End If

        Select Case _Tariff

            Case ""
                Errors += 1
                Return "NO-TARIFF"

            Case "FD"
                If _AM = "1" AndAlso _PM = "1" Then
                    Return "FD"
                Else
                    Errors += 1
                    Return "FD ?"
                End If

            Case "AH"
                If _AM = "1" AndAlso _PM = "1" Then
                    Return "AH"
                Else
                    Errors += 1
                    Return "AH ?"
                End If

            Case "HD"
                If _AM = "1" AndAlso _PM = "1" Then
                    'should be one or the other
                    Errors += 1
                    Return "HD ?"
                Else
                    If _AM = "1" Then
                        Return "AM"
                    Else
                        Return "PM"
                    End If
                End If

            Case "COLOUR-ERROR"
                Errors += 1
                Return "COLOUR-ERROR"

            Case Else
                'you choose, check it starts with square bracket...
                If _Tariff = "[]" Then
                    Errors += 1
                    Return "NOTIMES"
                Else
                    If _Tariff.StartsWith("[") Then
                        Return _Tariff
                    Else
                        Errors += 1
                    End If
                End If

        End Select

        Errors += 1
        Return "ERROR"

    End Function

    Private Function ReturnTariff(ByVal RGB As Integer, ByVal TimeIn As String) As String
        If RGB = -740989 Then Return "[" + TimeIn + "]"
        If RGB = -6437914 Then Return "AH"
        If RGB = -5647986 Then Return "HD"
        If RGB = -256 Then Return "FD"
        Return "COLOUR-ERROR"
    End Function

    Private Function ReturnOverrideValue(Of T)(ByVal SpreadsheetValue As T, ByVal OverrideValue As T) As T
        If SpreadsheetValue.ToString = "" Then
            Return OverrideValue
        Else
            Return SpreadsheetValue
        End If
    End Function

    Private Function ValidateRecord(ByRef RecordIn As ImportRecord) As Boolean

        'find the child
        Dim _ID As String = FindChild(RecordIn.ChildForename, RecordIn.ChildSurname)

        If _ID = "" Then
            RecordIn.Errors += 1
        Else
            RecordIn.ChildID = _ID
        End If

        If RecordIn.Errors = 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Function FindChild(ByVal Forename As String, ByVal Surname As String) As String

        Dim _ID As String = ""
        Dim _SQL As String = "select ID from Children where status <> 'Left' and forename = '" + Forename + "' and surname = '" + Surname + "'"
        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)

        If _DT IsNot Nothing Then

            If _DT.Rows.Count = 1 Then
                _ID = _DT.Rows(0).Item(0).ToString
            End If

            _DT.Dispose()
            _DT = Nothing

        End If

        Return _ID

    End Function

    Private Sub Wiz_CancelClick(sender As Object, e As ComponentModel.CancelEventArgs) Handles Wiz.CancelClick
        If m_Importing Then
            e.Cancel = True
        Else
            If Wiz.SelectedPage.Name = "wpWelcome" Then
                Me.Close()
            Else
                If CareMessage("Are you sure you want to Exit the Import Wizard?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Exit") = DialogResult.Yes Then
                    Me.Close()
                End If
            End If
        End If
    End Sub

    Private Sub Wiz_NextClick(sender As Object, e As DevExpress.XtraWizard.WizardCommandButtonClickEventArgs) Handles Wiz.NextClick

        Select Case e.Page.Name

            Case "wpWelcome"
                If Not SelectFile() Then e.Handled = True

            Case "wpSpreadsheet"
                PreviewImport()

            Case "wpPreview"
                'goto preview page

        End Select

    End Sub

    Private Sub Import()

        m_Importing = True
        Session.SetupProgressBar("Importing Data...", m_Records.Count)

        m_ImportPassed = 0
        m_ImportFailed = 0

        For Each _r As ImportRecord In m_Records

            If _r.Valid Then

                Dim _WC As Date? = Nothing

                If _r.WK1Thu <> "" OrElse _r.WK1Fri <> "" Then
                    _WC = DateSerial(2016, 7, 18)
                    CreateBooking(_r.ChildID, _WC, "", "", "", _r.WK1Thu, _r.WK1Fri)
                End If

                If _r.WK2Mon <> "" OrElse _r.WK2Tue <> "" OrElse _r.WK2Wed <> "" OrElse _r.WK2Thu <> "" OrElse _r.WK2Fri <> "" Then
                    _WC = DateSerial(2016, 7, 25)
                    CreateBooking(_r.ChildID, _WC, _r.WK2Mon, _r.WK2Tue, _r.WK2Wed, _r.WK2Thu, _r.WK2Fri)
                End If

                If _r.WK3Mon <> "" OrElse _r.WK3Tue <> "" OrElse _r.WK3Wed <> "" OrElse _r.WK3Thu <> "" OrElse _r.WK3Fri <> "" Then
                    _WC = DateSerial(2016, 8, 1)
                    CreateBooking(_r.ChildID, _WC, _r.WK3Mon, _r.WK3Tue, _r.WK3Wed, _r.WK3Thu, _r.WK3Fri)
                End If

                If _r.WK4Mon <> "" OrElse _r.WK4Tue <> "" OrElse _r.WK4Wed <> "" OrElse _r.WK4Thu <> "" OrElse _r.WK4Fri <> "" Then
                    _WC = DateSerial(2016, 8, 8)
                    CreateBooking(_r.ChildID, _WC, _r.WK4Mon, _r.WK4Tue, _r.WK4Wed, _r.WK4Thu, _r.WK4Fri)
                End If

                If _r.WK5Mon <> "" OrElse _r.WK5Tue <> "" OrElse _r.WK5Wed <> "" OrElse _r.WK5Thu <> "" OrElse _r.WK5Fri <> "" Then
                    _WC = DateSerial(2016, 8, 15)
                    CreateBooking(_r.ChildID, _WC, _r.WK5Mon, _r.WK5Tue, _r.WK5Wed, _r.WK5Thu, _r.WK5Fri)
                End If

                If _r.WK6Mon <> "" OrElse _r.WK6Tue <> "" OrElse _r.WK6Wed <> "" OrElse _r.WK6Thu <> "" OrElse _r.WK6Fri <> "" Then
                    _WC = DateSerial(2016, 8, 22)
                    CreateBooking(_r.ChildID, _WC, _r.WK6Mon, _r.WK6Tue, _r.WK6Wed, _r.WK6Thu, _r.WK6Fri)
                End If

                If _r.WK7Mon <> "" OrElse _r.WK7Tue <> "" OrElse _r.WK7Wed <> "" OrElse _r.WK7Thu <> "" OrElse _r.WK7Fri <> "" Then
                    _WC = DateSerial(2016, 8, 29)
                    CreateBooking(_r.ChildID, _WC, _r.WK7Mon, _r.WK7Tue, _r.WK7Wed, _r.WK7Thu, _r.WK7Fri)
                End If

            End If

            Session.StepProgressBar()

        Next

        m_Importing = False

    End Sub

    Private Sub CreateBooking(ByVal ChildID As String, ByVal WC As Date?, ByVal Mon As String, ByVal Tue As String, ByVal Wed As String, ByVal Thu As String, ByVal Fri As String)

        Dim _T As Business.Tariff = Nothing
        Dim _B As New Business.ChildAdvanceSession
        With _B

            ._ChildId = New Guid(ChildID)
            ._DateFrom = WC

            ._Mode = "Weekly"
            ._PatternType = "Override"
            ._Comments = "Imported from Excel " + Now.ToString

            SetDay(Mon, _B._Monday, _B._MondayDesc, _B._Monday1, _B._Monday2, _B._MondayBoid, _B._MondayBo)
            SetDay(Tue, _B._Tuesday, _B._TuesdayDesc, _B._Tuesday1, _B._Tuesday2, _B._TuesdayBoid, _B._TuesdayBo)
            SetDay(Wed, _B._Wednesday, _B._WednesdayDesc, _B._Wednesday1, _B._Wednesday2, _B._WednesdayBoid, _B._WednesdayBo)
            SetDay(Thu, _B._Thursday, _B._ThursdayDesc, _B._Thursday1, _B._Thursday2, _B._ThursdayBoid, _B._ThursdayBo)
            SetDay(Fri, _B._Friday, _B._FridayDesc, _B._Friday1, _B._Friday2, _B._FridayBoid, _B._FridayBo)

            .Store()

        End With

    End Sub

    Private Sub SetDay(ByVal ImportValue As String, ByRef TariffID As Guid?, ByRef TariffDesc As String, ByRef Value1 As Decimal, ByRef Value2 As Decimal, ByRef BoltOnIDs As String, ByRef BoldOns As String)

        If ImportValue = "" Then Exit Sub
        If ImportValue = "NOTIMES" Then Exit Sub

        'check if we are dealing with times
        If ImportValue.StartsWith("[") Then

            'strip brackets
            ImportValue = ImportValue.Replace("[", "")
            ImportValue = ImportValue.Replace("]", "")

            'check for dash, split the from and to
            If ImportValue.Contains("-") Then

                Dim _Dash As Integer = ImportValue.IndexOf("-")
                Dim _From As String = ImportValue.Substring(0, _Dash)
                Dim _To As String = ImportValue.Substring(_Dash + 1)

                Dim _FromDecimal As Decimal = ReturnTimeSpan(_From)
                Dim _ToDecimal As Decimal = ReturnTimeSpan(_To)

                TariffID = New Guid("7C71B858-86D0-4900-860A-6FBEE19F2ED2")
                TariffDesc = "Holiday Club You Choose"
                Value1 = _FromDecimal
                Value2 = _ToDecimal
                BoltOnIDs = ""
                BoldOns = ""

            End If

        Else

            Select Case ImportValue

                Case "FD"
                    TariffID = New Guid("FAA9A549-E0AB-498D-97BC-C83595A5D75B")
                    TariffDesc = "Holiday Club Full Day"

                Case "AM"
                    TariffID = New Guid("293643A6-EA20-4B8A-B8F0-E118EBCC596B")
                    TariffDesc = "Holiday Club AM"

                Case "PM"
                    TariffID = New Guid("3E82B927-5E37-43C9-8D1B-7585731E5503")
                    TariffDesc = "Holiday Club PM"

                Case "AH"
                    TariffID = New Guid("265B2AD3-3A41-4EAF-8696-0198BF5C8ABE")
                    TariffDesc = "Holiday Club Activity Hours"

                Case Else
                    Stop

            End Select

            Value1 = 0
            Value2 = 0
            BoltOnIDs = ""
            BoldOns = ""

        End If

    End Sub

    Private Function ReturnTimeSpan(ByVal TimeIn As String) As Decimal

        'strip all spaces, dots, colons from time
        TimeIn = TimeIn.Replace(" ", "")
        TimeIn = TimeIn.Replace(":", "")
        TimeIn = TimeIn.Replace(".", "")

        'look for shorter times, i.e. 2:30
        If TimeIn.Length = 3 Then

            Dim _Hours As String = TimeIn.First.ToString
            Dim _Mins As String = TimeIn.Substring(1)

            'check the first character, if its 1-6 then the user
            'has entered 2:30 instead of 14:30
            If _Hours = "1" Then _Hours = "13"
            If _Hours = "2" Then _Hours = "14"
            If _Hours = "3" Then _Hours = "15"
            If _Hours = "4" Then _Hours = "16"
            If _Hours = "5" Then _Hours = "17"
            If _Hours = "6" Then _Hours = "18"

            TimeIn = _Hours + _Mins

        End If

        Try
            Return ValueHandler.ConvertDecimal(TimeIn)

        Catch ex As Exception
            Return 0
        End Try

    End Function

    Private Sub Memo(ByVal TextIn As String)
        memProgress.Text.Insert(0, TextIn)
        memProgress.Refresh()
    End Sub

    Private Function PreviewImport() As Boolean

        Preview()

        If m_Records.Count > 0 Then

            cgRecords.Populate(m_Records)
            FormatColumns()

            If m_Failed = 0 Then
                CareMessage("Valid Data :-)" + vbCrLf + vbCrLf + "Ready to Import!", MessageBoxIcon.Information, "Data Validation")
                Return True
            Else
                CareMessage(m_Failed.ToString + " record(s) failed validation :-(" + vbCrLf + vbCrLf + m_Passed.ToString + " record(s) passed validation :-)" + vbCrLf + vbCrLf + "Please review and correct accordingly.", MessageBoxIcon.Error, "Data Validation")
            End If

        Else
            CareMessage("No records available for Preview :-(" + vbCrLf + vbCrLf + "Please ensure you are using the Nursery Genie Import Spreadsheet.", MessageBoxIcon.Error, "Data Validation")
        End If

        Return False

    End Function

    Private Sub FormatColumns()

        'make all the columns invisible
        'For Each _c As DevExpress.XtraGrid.Columns.GridColumn In cgRecords.Columns
        '    _c.Visible = False
        'Next

        'show the ones we want (in descending order)

        'cgRecords.Columns("C4Surname").Visible = True
        'cgRecords.Columns("C4Forename").Visible = True

        'cgRecords.Columns("C3Surname").Visible = True
        'cgRecords.Columns("C3Forename").Visible = True

        'cgRecords.Columns("C2Surname").Visible = True
        'cgRecords.Columns("C2Forename").Visible = True

        'cgRecords.Columns("C1Surname").Visible = True
        'cgRecords.Columns("C1Forename").Visible = True

        'cgRecords.Columns("Address").Visible = True
        'cgRecords.Columns("PostCode").Visible = True

        'cgRecords.Columns("Discount").Visible = True
        'cgRecords.Columns("TermTime").Visible = True

        'cgRecords.Columns("Friday").Visible = True
        'cgRecords.Columns("Thursday").Visible = True
        'cgRecords.Columns("Wednesday").Visible = True
        'cgRecords.Columns("Tuesday").Visible = True
        'cgRecords.Columns("Monday").Visible = True

        'cgRecords.Columns("LeavingDate").Visible = True
        'cgRecords.Columns("StartDate").Visible = True

        'cgRecords.Columns("PostCode").Visible = True
        'cgRecords.Columns("Address").Visible = True

        'cgRecords.Columns("Keyworker").Visible = True
        'cgRecords.Columns("Group").Visible = True

        'cgRecords.Columns("ChildGender").Visible = True
        'cgRecords.Columns("ChildDOB").Visible = True
        'cgRecords.Columns("ChildSurname").Visible = True
        'cgRecords.Columns("ChildForename").Visible = True

        'cgRecords.Columns("Valid").Visible = True

        cgRecords.AutoSizeColumns()

    End Sub

    Private Function SelectFile() As Boolean
        If ofd.ShowDialog = DialogResult.OK Then
            sc.LoadDocument(ofd.FileName)
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub Wiz_SelectedPageChanged(sender As Object, e As DevExpress.XtraWizard.WizardPageChangedEventArgs) Handles Wiz.SelectedPageChanged

        Select Case e.Page.Name

            Case "wpProgress"
                Import()

        End Select

    End Sub

    Private Sub Wiz_SelectedPageChanging(sender As Object, e As DevExpress.XtraWizard.WizardPageChangingEventArgs) Handles Wiz.SelectedPageChanging

        If m_Importing Then
            e.Cancel = True
            Exit Sub
        End If

        If e.Direction = DevExpress.XtraWizard.Direction.Forward Then
            If e.PrevPage.Name = "wpPreview" Then
                If m_Failed > 0 Then
                    If CareMessage(m_Failed.ToString + " records will not be imported - Are you sure you wish to Continue?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Import Data") = DialogResult.Yes Then

                    Else
                        e.Cancel = True
                    End If
                End If
            End If
        End If

    End Sub

#Region "Import Data"

    Private Function SaveSessions(ByVal DataIn As ImportRecord) As Boolean

        'Dim _S As New Business.ChildAdvanceSession
        'With _S

        '    ._ChildId = m_ChildID
        '    ._DateFrom = ValueHandler.NearestDate(DataIn.StartDate, DayOfWeek.Monday, ValueHandler.EnumDirection.Backwards)
        '    ._Mode = "Weekly"

        '    If DataIn.Monday <> "" Then
        '        Dim _Monday As New IDandDescription("Tariffs", "name", DataIn.Monday)
        '        If _Monday.Found Then
        '            ._Monday = _Monday.ID
        '            ._MondayDesc = _Monday.Description
        '        End If
        '    End If

        '    If DataIn.Tuesday <> "" Then
        '        Dim _Tuesday As New IDandDescription("Tariffs", "name", DataIn.Tuesday)
        '        If _Tuesday.Found Then
        '            ._Tuesday = _Tuesday.ID
        '            ._TuesdayDesc = _Tuesday.Description
        '        End If
        '    End If

        '    If DataIn.Wednesday <> "" Then
        '        Dim _Wednesday As New IDandDescription("Tariffs", "name", DataIn.Wednesday)
        '        If _Wednesday.Found Then
        '            ._Wednesday = _Wednesday.ID
        '            ._WednesdayDesc = _Wednesday.Description
        '        End If
        '    End If

        '    If DataIn.Thursday <> "" Then
        '        Dim _Thursday As New IDandDescription("Tariffs", "name", DataIn.Thursday)
        '        If _Thursday.Found Then
        '            ._Thursday = _Thursday.ID
        '            ._ThursdayDesc = _Thursday.Description
        '        End If
        '    End If

        '    If DataIn.Friday <> "" Then
        '        Dim _Friday As New IDandDescription("Tariffs", "name", DataIn.Friday)
        '        If _Friday.Found Then
        '            ._Friday = _Friday.ID
        '            ._FridayDesc = _Friday.Description
        '        End If
        '    End If

        '    ._Activated = False
        '    .Store()

        'End With

        Return True

    End Function

    Private Function SaveFamily(ByVal DataIn As ImportRecord) As Boolean

        'Memo("Creating Family " + DataIn.C1Surname + "...")

        'Dim _F As New Business.Family
        'With _F

        '    ._Surname = DataIn.C1Surname
        '    ._LetterName = DataIn.C1Forename + " " + DataIn.C1Surname
        '    ._eInvoicing = False
        '    ._Address = DataIn.Address
        '    ._Postcode = DataIn.PostCode

        '    ._OpeningBalance = 0
        '    ._Balance = 0

        '    .Store()

        '    m_FamilyID = _F._ID

        'End With

        '_F = Nothing

        Return True

    End Function

    Private Function SaveContacts(ByVal DataIn As ImportRecord) As Boolean

        'If Not CreateContactRecord(True, False, DataIn.C1Forename, DataIn.C1Surname, DataIn.C1Relationship, _
        '                    DataIn.C1Mobile, False, DataIn.C1Landline, DataIn.C1Email, DataIn.C1Secret) Then Return False

        'If DataIn.C2Surname <> "" Then
        '    If Not CreateContactRecord(False, True, DataIn.C2Forename, DataIn.C2Surname, DataIn.C2Relationship, _
        '                        DataIn.C2Mobile, False, DataIn.C2Landline, DataIn.C2Email, DataIn.C2Secret) Then Return False
        'End If

        'If DataIn.C3Surname <> "" Then
        '    If Not CreateContactRecord(False, True, DataIn.C3Forename, DataIn.C3Surname, DataIn.C3Relationship, _
        '                        DataIn.C3Mobile, False, DataIn.C3Landline, DataIn.C3Email, DataIn.C3Secret) Then Return False
        'End If

        'If DataIn.C4Surname <> "" Then
        '    If Not CreateContactRecord(False, True, DataIn.C4Forename, DataIn.C4Surname, DataIn.C4Relationship, _
        '                        DataIn.C4Mobile, False, DataIn.C4Landline, DataIn.C4Email, DataIn.C4Secret) Then Return False
        'End If

        Return True

    End Function

    Private Function SaveChild(ByVal DataIn As ImportRecord) As Boolean

        'Dim _C As New Business.Child
        'With _C

        '    ._FamilyId = m_FamilyID
        '    ._FamilyName = DataIn.C1Surname

        '    ._Status = "On Waiting List"

        '    ._Forename = DataIn.ChildForename
        '    ._Surname = DataIn.ChildSurname
        '    ._Fullname = ._Forename + " " + ._Surname

        '    ._Dob = DataIn.ChildDOB
        '    ._Gender = DataIn.ChildGender

        '    ._Started = DataIn.StartDate
        '    ._GroupStarted = ._Started

        '    Dim _Group As New IDandDescription("Groups", "name", DataIn.Group)
        '    If _Group.Found Then
        '        ._GroupId = _Group.ID
        '        ._GroupName = _Group.Description
        '    End If

        '    Dim _Keyworker As New IDandDescription("Staff", "fullname", DataIn.Keyworker)
        '    If _Keyworker.Found Then
        '        ._KeyworkerId = _Keyworker.ID
        '        ._KeyworkerName = _Keyworker.Description
        '    End If

        '    Dim _Discount As New IDandDescription("Discounts", "name", DataIn.Discount)
        '    If _Discount.Found Then
        '        ._Discount = _Discount.ID
        '    End If

        '    ._TermOnly = DataIn.TermTime

        '    ._MedAllergies = DataIn.Alergies
        '    ._MedMedication = DataIn.MedicalNotes
        '    ._MedNotes = DataIn.OtherNotes

        '    ._ConsCalpol = DataIn.ConCalpol
        '    ._ConsOffsite = DataIn.ConOffsite
        '    ._ConsOutdoor = DataIn.ConOutdoors
        '    ._ConsPhoto = DataIn.ConPhotos
        '    ._ConsPlasters = DataIn.ConPlasters
        '    ._ConsSuncream = DataIn.ConSuncream

        '    ._SurgDoc = DataIn.GPName
        '    ._SurgAdd1 = DataIn.GPAddress
        '    ._SurgTel = DataIn.GPTel

        '    .Store()

        '    m_ChildID = ._ID

        'End With

        Return True

    End Function

    Private Function CreateContactRecord(ByVal Primary As Boolean, ByVal Emergency As Boolean, ByVal Forename As String, ByVal Surname As String, ByVal Rel As String, _
                                         ByVal Mobile As String, ByVal SMS As Boolean, ByVal Landline As String, ByVal Email As String, ByVal Password As String) As Boolean

        Dim _C As New Business.Contact
        With _C

            ._FamilyId = m_FamilyID

            ._Surname = Surname
            ._Forename = Forename
            ._Fullname = Forename + " " + Surname
            ._Relationship = Rel

            If Primary Then
                ._PrimaryCont = True
                ._EmerCont = True
                ._Address = m_Address
                ._Postcode = m_PostCode
            Else
                ._PrimaryCont = False
                ._EmerCont = Emergency
            End If

            ._TelHome = Landline
            ._TelMobile = Mobile
            ._Sms = SMS
            ._Email = Email

            ._Password = Password

            .Store()

        End With

        _C = Nothing

        Return True

    End Function
#End Region

#Region "Grid Events"

    Private Sub cgRecords_RowCellStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs) Handles cgRecords.RowCellStyle

        If e.CellValue IsNot Nothing Then
            If e.CellValue.ToString.Contains("?") Then
                e.Appearance.BackColor = Drawing.Color.Khaki
            Else
                If e.CellValue.ToString.Contains("ERROR") Then
                    e.Appearance.BackColor = Drawing.Color.LightCoral
                Else
                    If e.CellValue.ToString.Contains("NOTIMES") Then
                        e.Appearance.BackColor = Drawing.Color.LightCoral
                    Else
                        e.Appearance.Reset()
                    End If
                End If
            End If
        End If

    End Sub

    Private Sub cgRecords_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles cgRecords.RowStyle

        Dim _ChildID As Object = cgRecords.GetRowCellValue(e.RowHandle, "ChildID")

        If _ChildID Is Nothing Then
            e.Appearance.BackColor = Drawing.Color.LightGray
        Else

            If _ChildID.ToString = "" Then
                e.Appearance.BackColor = Drawing.Color.LightGray
            Else
                If ValueHandler.ConvertBoolean(cgRecords.GetRowCellValue(e.RowHandle, "Valid")) Then
                    e.Appearance.BackColor = Drawing.Color.LightGreen
                Else
                    e.Appearance.Reset()
                End If
            End If

        End If

    End Sub

#End Region

#Region "Data Helpers"

    Private Function ReturnString(ByVal ValueIn As Cell) As String
        If ValueIn.Value.IsText Then
            Return ValueIn.Value.TextValue
        Else
            Return ""
        End If
    End Function

    Private Function ReturnDate(ByVal ValueIn As Cell) As Date?
        If ValueIn.Value.IsDateTime Then
            Return ValueIn.Value.DateTimeValue
        Else
            Return Nothing
        End If
    End Function

    Private Function ReturnBoolean(ByVal ValueIn As Cell) As Boolean

        If ValueIn.Value.IsBoolean Then
            Return ValueIn.Value.BooleanValue
        Else

            If ValueIn.Value.IsText Then

                Select Case ValueIn.Value.TextValue.ToUpper

                    Case "Y", "T", "1", "YES", "TRUE"
                        Return True

                    Case Else
                        Return False

                End Select

            Else
                Return False
            End If
        End If

    End Function

#End Region

#Region "Private Classes"

    Private Class IDandDescription

        Public Sub New(ByVal Table As String, ByVal DescriptionField As String, ByRef Description As String)

            If Description = "" Then
                Me.Found = False
                Me.ID = Nothing
                Description = "!"
                Me.Description = Description
            Else

                Description = Description.Trim()

                Dim _SQL As String = "select id, " + DescriptionField + " from " + Table + " where " + DescriptionField + " like '%" + Description + "%'"
                Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)

                If _DR Is Nothing Then
                    Me.Found = False
                    Me.ID = Nothing
                    Description = "! " + Description
                    Me.Description = Description
                Else
                    Me.Found = True
                    Me.ID = New Guid(_DR.Item("id").ToString)
                    Description = _DR.Item(DescriptionField).ToString
                    Me.Description = Description
                End If

            End If

        End Sub

        Property Found As Boolean
        Property ID As Guid?
        Property Description As String

    End Class

    Private Class ImportRecord

        Property Valid As Boolean
        Property Errors As Integer

        Property ChildID As String
        Property ChildForename As String
        Property ChildSurname As String

        Property Time As String

        Property WK1Thu As String
        Property WK1Fri As String

        Property WK2Mon As String
        Property WK2Tue As String
        Property WK2Wed As String
        Property WK2Thu As String
        Property WK2Fri As String

        Property WK3Mon As String
        Property WK3Tue As String
        Property WK3Wed As String
        Property WK3Thu As String
        Property WK3Fri As String

        Property WK4Mon As String
        Property WK4Tue As String
        Property WK4Wed As String
        Property WK4Thu As String
        Property WK4Fri As String

        Property WK5Mon As String
        Property WK5Tue As String
        Property WK5Wed As String
        Property WK5Thu As String
        Property WK5Fri As String

        Property WK6Mon As String
        Property WK6Tue As String
        Property WK6Wed As String
        Property WK6Thu As String
        Property WK6Fri As String

        Property WK7Mon As String
        Property WK7Tue As String
        Property WK7Wed As String
        Property WK7Thu As String
        Property WK7Fri As String

    End Class

#End Region

    Private Sub cgRecords_GridDoubleClick(sender As Object, e As EventArgs) Handles cgRecords.GridDoubleClick

    End Sub
End Class