﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInvoiceAdd
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInvoiceAdd))
        Me.Panel1 = New DevExpress.XtraEditors.PanelControl()
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        Me.btnOK = New Care.Controls.CareButton(Me.components)
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.lblAge = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.txtLeave = New Care.Controls.CareTextBox(Me.components)
        Me.txtStart = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.txtDOB = New Care.Controls.CareTextBox(Me.components)
        Me.txtRoom = New Care.Controls.CareTextBox(Me.components)
        Me.btnSessions = New Care.Controls.CareButton(Me.components)
        Me.txtChild = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl2 = New Care.Controls.CareFrame()
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.cdtPeriodEnd = New Care.Controls.CareDateTime(Me.components)
        Me.cdtPeriodStart = New Care.Controls.CareDateTime(Me.components)
        Me.cdtInvoiceDate = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl9 = New Care.Controls.CareFrame()
        Me.cdtAddlTo = New Care.Controls.CareDateTime(Me.components)
        Me.cbxAdditional = New Care.Controls.CareComboBox(Me.components)
        Me.cdtAddlFrom = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel25 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel26 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl4 = New Care.Controls.CareFrame()
        Me.cbxGeneration = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel17 = New Care.Controls.CareLabel(Me.components)
        Me.cbxLayout = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtLeave.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDOB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRoom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtChild.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.cdtPeriodEnd.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtPeriodEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtPeriodStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtPeriodStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtInvoiceDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtInvoiceDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl9.SuspendLayout()
        CType(Me.cdtAddlTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtAddlTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxAdditional.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtAddlFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtAddlFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.cbxGeneration.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxLayout.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.Panel1.Controls.Add(Me.CareLabel3)
        Me.Panel1.Controls.Add(Me.btnCancel)
        Me.Panel1.Controls.Add(Me.btnOK)
        Me.Panel1.Dock = DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 449)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(586, 36)
        Me.Panel1.TabIndex = 4
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel3.Appearance.Image = CType(resources.GetObject("CareLabel3.Appearance.Image"), System.Drawing.Image)
        Me.CareLabel3.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.CareLabel3.Location = New System.Drawing.Point(206, 5)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(182, 21)
        Me.CareLabel3.TabIndex = 0
        Me.CareLabel3.Text = "Manual Invoice Calculation"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel3.ToolTip = resources.GetString("CareLabel3.ToolTip")
        Me.CareLabel3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.[Error]
        Me.CareLabel3.ToolTipTitle = "Manual Invoice Calculation"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.CausesValidation = False
        Me.btnCancel.Cursor = Cursors.IBeam
        Me.btnCancel.DialogResult = DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(489, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 25)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Tag = "Y"
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(398, 3)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(85, 25)
        Me.btnOK.TabIndex = 1
        Me.btnOK.Text = "OK"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.lblAge)
        Me.GroupControl1.Controls.Add(Me.CareLabel6)
        Me.GroupControl1.Controls.Add(Me.CareLabel5)
        Me.GroupControl1.Controls.Add(Me.txtLeave)
        Me.GroupControl1.Controls.Add(Me.txtStart)
        Me.GroupControl1.Controls.Add(Me.CareLabel4)
        Me.GroupControl1.Controls.Add(Me.txtDOB)
        Me.GroupControl1.Controls.Add(Me.txtRoom)
        Me.GroupControl1.Controls.Add(Me.btnSessions)
        Me.GroupControl1.Controls.Add(Me.txtChild)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(561, 144)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Selected Child"
        '
        'lblAge
        '
        Me.lblAge.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAge.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblAge.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblAge.Location = New System.Drawing.Point(253, 88)
        Me.lblAge.Name = "lblAge"
        Me.lblAge.Size = New System.Drawing.Size(147, 15)
        Me.lblAge.TabIndex = 7
        Me.lblAge.Text = "1 year, 11 months, 21 days"
        Me.lblAge.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(11, 116)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(130, 15)
        Me.CareLabel6.TabIndex = 8
        Me.CareLabel6.Text = "Start Date / Leaving Date"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(11, 88)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(24, 15)
        Me.CareLabel5.TabIndex = 5
        Me.CareLabel5.Text = "DOB"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLeave
        '
        Me.txtLeave.CharacterCasing = CharacterCasing.Normal
        Me.txtLeave.EnterMoveNextControl = True
        Me.txtLeave.Location = New System.Drawing.Point(253, 113)
        Me.txtLeave.MaxLength = 0
        Me.txtLeave.Name = "txtLeave"
        Me.txtLeave.NumericAllowNegatives = False
        Me.txtLeave.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtLeave.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLeave.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtLeave.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.txtLeave.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtLeave.Properties.Appearance.Options.UseBackColor = True
        Me.txtLeave.Properties.Appearance.Options.UseFont = True
        Me.txtLeave.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLeave.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtLeave.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtLeave.Properties.ReadOnly = True
        Me.txtLeave.Size = New System.Drawing.Size(100, 22)
        Me.txtLeave.TabIndex = 10
        Me.txtLeave.TabStop = False
        Me.txtLeave.Tag = "R"
        Me.txtLeave.TextAlign = HorizontalAlignment.Left
        Me.txtLeave.ToolTipText = ""
        '
        'txtStart
        '
        Me.txtStart.CharacterCasing = CharacterCasing.Normal
        Me.txtStart.EnterMoveNextControl = True
        Me.txtStart.Location = New System.Drawing.Point(147, 113)
        Me.txtStart.MaxLength = 0
        Me.txtStart.Name = "txtStart"
        Me.txtStart.NumericAllowNegatives = False
        Me.txtStart.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtStart.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStart.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtStart.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.txtStart.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtStart.Properties.Appearance.Options.UseBackColor = True
        Me.txtStart.Properties.Appearance.Options.UseFont = True
        Me.txtStart.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStart.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtStart.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtStart.Properties.ReadOnly = True
        Me.txtStart.Size = New System.Drawing.Size(100, 22)
        Me.txtStart.TabIndex = 9
        Me.txtStart.TabStop = False
        Me.txtStart.Tag = "R"
        Me.txtStart.TextAlign = HorizontalAlignment.Left
        Me.txtStart.ToolTipText = ""
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(11, 60)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel4.TabIndex = 3
        Me.CareLabel4.Text = "Room"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDOB
        '
        Me.txtDOB.CharacterCasing = CharacterCasing.Normal
        Me.txtDOB.EnterMoveNextControl = True
        Me.txtDOB.Location = New System.Drawing.Point(147, 85)
        Me.txtDOB.MaxLength = 0
        Me.txtDOB.Name = "txtDOB"
        Me.txtDOB.NumericAllowNegatives = False
        Me.txtDOB.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtDOB.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDOB.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtDOB.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.txtDOB.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtDOB.Properties.Appearance.Options.UseBackColor = True
        Me.txtDOB.Properties.Appearance.Options.UseFont = True
        Me.txtDOB.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDOB.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtDOB.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDOB.Properties.ReadOnly = True
        Me.txtDOB.Size = New System.Drawing.Size(100, 22)
        Me.txtDOB.TabIndex = 6
        Me.txtDOB.TabStop = False
        Me.txtDOB.Tag = "R"
        Me.txtDOB.TextAlign = HorizontalAlignment.Left
        Me.txtDOB.ToolTipText = ""
        '
        'txtRoom
        '
        Me.txtRoom.CharacterCasing = CharacterCasing.Normal
        Me.txtRoom.EnterMoveNextControl = True
        Me.txtRoom.Location = New System.Drawing.Point(147, 57)
        Me.txtRoom.MaxLength = 0
        Me.txtRoom.Name = "txtRoom"
        Me.txtRoom.NumericAllowNegatives = False
        Me.txtRoom.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRoom.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRoom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRoom.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.txtRoom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRoom.Properties.Appearance.Options.UseBackColor = True
        Me.txtRoom.Properties.Appearance.Options.UseFont = True
        Me.txtRoom.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRoom.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRoom.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRoom.Properties.ReadOnly = True
        Me.txtRoom.Size = New System.Drawing.Size(296, 22)
        Me.txtRoom.TabIndex = 4
        Me.txtRoom.TabStop = False
        Me.txtRoom.Tag = "R"
        Me.txtRoom.TextAlign = HorizontalAlignment.Left
        Me.txtRoom.ToolTipText = ""
        '
        'btnSessions
        '
        Me.btnSessions.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnSessions.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSessions.Appearance.Options.UseFont = True
        Me.btnSessions.Location = New System.Drawing.Point(449, 28)
        Me.btnSessions.Name = "btnSessions"
        Me.btnSessions.Size = New System.Drawing.Size(100, 25)
        Me.btnSessions.TabIndex = 2
        Me.btnSessions.TabStop = False
        Me.btnSessions.Text = "View Sessions"
        '
        'txtChild
        '
        Me.txtChild.CharacterCasing = CharacterCasing.Normal
        Me.txtChild.EnterMoveNextControl = True
        Me.txtChild.Location = New System.Drawing.Point(147, 29)
        Me.txtChild.MaxLength = 0
        Me.txtChild.Name = "txtChild"
        Me.txtChild.NumericAllowNegatives = False
        Me.txtChild.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtChild.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtChild.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtChild.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.txtChild.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtChild.Properties.Appearance.Options.UseBackColor = True
        Me.txtChild.Properties.Appearance.Options.UseFont = True
        Me.txtChild.Properties.Appearance.Options.UseTextOptions = True
        Me.txtChild.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtChild.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtChild.Properties.ReadOnly = True
        Me.txtChild.Size = New System.Drawing.Size(296, 22)
        Me.txtChild.TabIndex = 1
        Me.txtChild.TabStop = False
        Me.txtChild.Tag = "R"
        Me.txtChild.TextAlign = HorizontalAlignment.Left
        Me.txtChild.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(11, 33)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(63, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Child Name"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.CareLabel2)
        Me.GroupControl2.Controls.Add(Me.cdtPeriodEnd)
        Me.GroupControl2.Controls.Add(Me.cdtPeriodStart)
        Me.GroupControl2.Controls.Add(Me.cdtInvoiceDate)
        Me.GroupControl2.Controls.Add(Me.CareLabel8)
        Me.GroupControl2.Controls.Add(Me.CareLabel9)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 162)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(561, 94)
        Me.GroupControl2.TabIndex = 1
        Me.GroupControl2.Text = "Invoice Configuration"
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(318, 34)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(125, 15)
        Me.CareLabel2.TabIndex = 2
        Me.CareLabel2.Text = "Invoice Period End Date"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtPeriodEnd
        '
        Me.cdtPeriodEnd.EditValue = Nothing
        Me.cdtPeriodEnd.EnterMoveNextControl = True
        Me.cdtPeriodEnd.Location = New System.Drawing.Point(449, 31)
        Me.cdtPeriodEnd.Name = "cdtPeriodEnd"
        Me.cdtPeriodEnd.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtPeriodEnd.Properties.Appearance.Options.UseFont = True
        Me.cdtPeriodEnd.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtPeriodEnd.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtPeriodEnd.Size = New System.Drawing.Size(100, 22)
        Me.cdtPeriodEnd.TabIndex = 3
        Me.cdtPeriodEnd.Tag = "E"
        Me.cdtPeriodEnd.Value = Nothing
        '
        'cdtPeriodStart
        '
        Me.cdtPeriodStart.EditValue = Nothing
        Me.cdtPeriodStart.EnterMoveNextControl = True
        Me.cdtPeriodStart.Location = New System.Drawing.Point(147, 31)
        Me.cdtPeriodStart.Name = "cdtPeriodStart"
        Me.cdtPeriodStart.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtPeriodStart.Properties.Appearance.Options.UseFont = True
        Me.cdtPeriodStart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtPeriodStart.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtPeriodStart.Size = New System.Drawing.Size(100, 22)
        Me.cdtPeriodStart.TabIndex = 1
        Me.cdtPeriodStart.Tag = "E"
        Me.cdtPeriodStart.Value = Nothing
        '
        'cdtInvoiceDate
        '
        Me.cdtInvoiceDate.EditValue = Nothing
        Me.cdtInvoiceDate.EnterMoveNextControl = True
        Me.cdtInvoiceDate.Location = New System.Drawing.Point(147, 61)
        Me.cdtInvoiceDate.Name = "cdtInvoiceDate"
        Me.cdtInvoiceDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtInvoiceDate.Properties.Appearance.Options.UseFont = True
        Me.cdtInvoiceDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtInvoiceDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtInvoiceDate.Size = New System.Drawing.Size(100, 22)
        Me.cdtInvoiceDate.TabIndex = 5
        Me.cdtInvoiceDate.Tag = "E"
        Me.cdtInvoiceDate.Value = Nothing
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(11, 36)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(129, 15)
        Me.CareLabel8.TabIndex = 0
        Me.CareLabel8.Text = "Invoice Period Start Date"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(11, 64)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(65, 15)
        Me.CareLabel9.TabIndex = 4
        Me.CareLabel9.Text = "Invoice Date"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl9
        '
        Me.GroupControl9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl9.Appearance.Options.UseFont = True
        Me.GroupControl9.Controls.Add(Me.cdtAddlTo)
        Me.GroupControl9.Controls.Add(Me.cbxAdditional)
        Me.GroupControl9.Controls.Add(Me.cdtAddlFrom)
        Me.GroupControl9.Controls.Add(Me.CareLabel25)
        Me.GroupControl9.Controls.Add(Me.CareLabel26)
        Me.GroupControl9.Location = New System.Drawing.Point(12, 262)
        Me.GroupControl9.Name = "GroupControl9"
        Me.GroupControl9.Size = New System.Drawing.Size(561, 87)
        Me.GroupControl9.TabIndex = 2
        Me.GroupControl9.Text = "Additional Sessions"
        '
        'cdtAddlTo
        '
        Me.cdtAddlTo.EditValue = Nothing
        Me.cdtAddlTo.EnterMoveNextControl = True
        Me.cdtAddlTo.Location = New System.Drawing.Point(253, 56)
        Me.cdtAddlTo.Name = "cdtAddlTo"
        Me.cdtAddlTo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtAddlTo.Properties.Appearance.Options.UseFont = True
        Me.cdtAddlTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtAddlTo.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtAddlTo.Size = New System.Drawing.Size(100, 22)
        Me.cdtAddlTo.TabIndex = 4
        Me.cdtAddlTo.Tag = "E"
        Me.cdtAddlTo.Value = Nothing
        '
        'cbxAdditional
        '
        Me.cbxAdditional.AllowBlank = False
        Me.cbxAdditional.DataSource = Nothing
        Me.cbxAdditional.DisplayMember = Nothing
        Me.cbxAdditional.EnterMoveNextControl = True
        Me.cbxAdditional.Location = New System.Drawing.Point(147, 30)
        Me.cbxAdditional.Name = "cbxAdditional"
        Me.cbxAdditional.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxAdditional.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxAdditional.Properties.Appearance.Options.UseFont = True
        Me.cbxAdditional.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxAdditional.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxAdditional.SelectedValue = Nothing
        Me.cbxAdditional.Size = New System.Drawing.Size(402, 22)
        Me.cbxAdditional.TabIndex = 1
        Me.cbxAdditional.Tag = "E"
        Me.cbxAdditional.ValueMember = Nothing
        '
        'cdtAddlFrom
        '
        Me.cdtAddlFrom.EditValue = Nothing
        Me.cdtAddlFrom.EnterMoveNextControl = True
        Me.cdtAddlFrom.Location = New System.Drawing.Point(147, 56)
        Me.cdtAddlFrom.Name = "cdtAddlFrom"
        Me.cdtAddlFrom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtAddlFrom.Properties.Appearance.Options.UseFont = True
        Me.cdtAddlFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtAddlFrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtAddlFrom.Size = New System.Drawing.Size(100, 22)
        Me.cdtAddlFrom.TabIndex = 3
        Me.cdtAddlFrom.Tag = "E"
        Me.cdtAddlFrom.Value = Nothing
        '
        'CareLabel25
        '
        Me.CareLabel25.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel25.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel25.Location = New System.Drawing.Point(11, 59)
        Me.CareLabel25.Name = "CareLabel25"
        Me.CareLabel25.Size = New System.Drawing.Size(92, 15)
        Me.CareLabel25.TabIndex = 2
        Me.CareLabel25.Text = "Additional Period"
        Me.CareLabel25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel26
        '
        Me.CareLabel26.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel26.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel26.Location = New System.Drawing.Point(11, 33)
        Me.CareLabel26.Name = "CareLabel26"
        Me.CareLabel26.Size = New System.Drawing.Size(102, 15)
        Me.CareLabel26.TabIndex = 0
        Me.CareLabel26.Text = "Additional Sessions"
        Me.CareLabel26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl4
        '
        Me.GroupControl4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl4.Appearance.Options.UseFont = True
        Me.GroupControl4.Controls.Add(Me.cbxGeneration)
        Me.GroupControl4.Controls.Add(Me.CareLabel17)
        Me.GroupControl4.Controls.Add(Me.cbxLayout)
        Me.GroupControl4.Controls.Add(Me.CareLabel14)
        Me.GroupControl4.Location = New System.Drawing.Point(12, 355)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(561, 89)
        Me.GroupControl4.TabIndex = 3
        Me.GroupControl4.Text = "Generation and Layout"
        '
        'cbxGeneration
        '
        Me.cbxGeneration.AllowBlank = False
        Me.cbxGeneration.DataSource = Nothing
        Me.cbxGeneration.DisplayMember = Nothing
        Me.cbxGeneration.EnterMoveNextControl = True
        Me.cbxGeneration.Location = New System.Drawing.Point(147, 30)
        Me.cbxGeneration.Name = "cbxGeneration"
        Me.cbxGeneration.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxGeneration.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxGeneration.Properties.Appearance.Options.UseFont = True
        Me.cbxGeneration.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxGeneration.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxGeneration.SelectedValue = Nothing
        Me.cbxGeneration.Size = New System.Drawing.Size(402, 22)
        Me.cbxGeneration.TabIndex = 1
        Me.cbxGeneration.Tag = "E"
        Me.cbxGeneration.ValueMember = Nothing
        '
        'CareLabel17
        '
        Me.CareLabel17.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel17.Location = New System.Drawing.Point(11, 61)
        Me.CareLabel17.Name = "CareLabel17"
        Me.CareLabel17.Size = New System.Drawing.Size(98, 15)
        Me.CareLabel17.TabIndex = 2
        Me.CareLabel17.Text = "Invoice Layout File"
        Me.CareLabel17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxLayout
        '
        Me.cbxLayout.AllowBlank = False
        Me.cbxLayout.DataSource = Nothing
        Me.cbxLayout.DisplayMember = Nothing
        Me.cbxLayout.EnterMoveNextControl = True
        Me.cbxLayout.Location = New System.Drawing.Point(147, 58)
        Me.cbxLayout.Name = "cbxLayout"
        Me.cbxLayout.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxLayout.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxLayout.Properties.Appearance.Options.UseFont = True
        Me.cbxLayout.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxLayout.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxLayout.SelectedValue = Nothing
        Me.cbxLayout.Size = New System.Drawing.Size(402, 22)
        Me.cbxLayout.TabIndex = 3
        Me.cbxLayout.Tag = "E"
        Me.cbxLayout.ValueMember = Nothing
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(11, 33)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(99, 15)
        Me.CareLabel14.TabIndex = 0
        Me.CareLabel14.Text = "Invoice Generation"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmInvoiceAdd
        '
        Me.AcceptButton = Me.btnOK
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(586, 485)
        Me.Controls.Add(Me.GroupControl9)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.Panel1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmInvoiceAdd"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "Add Invoice to Batch"
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtLeave.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDOB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRoom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtChild.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.cdtPeriodEnd.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtPeriodEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtPeriodStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtPeriodStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtInvoiceDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtInvoiceDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl9.ResumeLayout(False)
        Me.GroupControl9.PerformLayout()
        CType(Me.cdtAddlTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtAddlTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxAdditional.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtAddlFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtAddlFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.cbxGeneration.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxLayout.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents Panel1 As DevExpress.XtraEditors.PanelControl
    Private WithEvents btnCancel As Care.Controls.CareButton
    Private WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Protected WithEvents btnSessions As Care.Controls.CareButton
    Friend WithEvents txtChild As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents cdtPeriodEnd As Care.Controls.CareDateTime
    Friend WithEvents cdtPeriodStart As Care.Controls.CareDateTime
    Friend WithEvents cdtInvoiceDate As Care.Controls.CareDateTime
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents GroupControl9 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cdtAddlTo As Care.Controls.CareDateTime
    Friend WithEvents cbxAdditional As Care.Controls.CareComboBox
    Friend WithEvents cdtAddlFrom As Care.Controls.CareDateTime
    Friend WithEvents CareLabel25 As Care.Controls.CareLabel
    Friend WithEvents CareLabel26 As Care.Controls.CareLabel
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cbxGeneration As Care.Controls.CareComboBox
    Friend WithEvents CareLabel17 As Care.Controls.CareLabel
    Friend WithEvents cbxLayout As Care.Controls.CareComboBox
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents txtLeave As Care.Controls.CareTextBox
    Friend WithEvents txtStart As Care.Controls.CareTextBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents txtDOB As Care.Controls.CareTextBox
    Friend WithEvents txtRoom As Care.Controls.CareTextBox
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents lblAge As Care.Controls.CareLabel
End Class
