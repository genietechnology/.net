﻿Imports Care.Global

Public Class frmStaffAbsence

    Private m_StaffID As Guid
    Private m_RecordID As Guid?
    Private m_IsNew As Boolean = True

    Public Sub New(ByVal StaffID As Guid, ByVal RecordID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        m_StaffID = StaffID

        If RecordID.HasValue Then
            m_IsNew = False
            m_RecordID = RecordID
        Else
            m_IsNew = True
            m_RecordID = Nothing
        End If

    End Sub

    Private Sub frmChildCharge_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cbxType.PopulateFromListMaintenance(Care.Shared.ListHandler.ReturnListFromCache("Absence Type"))
        cbxReason.PopulateFromListMaintenance(Care.Shared.ListHandler.ReturnListFromCache("Absence Reason"))

        cdtFrom.BackColor = Session.ChangeColour
        cdtTo.BackColor = Session.ChangeColour
        cbxType.BackColor = Session.ChangeColour
        cbxReason.BackColor = Session.ChangeColour
        txtCost.BackColor = Session.ChangeColour
        txtNotes.BackColor = Session.ChangeColour

        If m_RecordID.HasValue Then
            Me.Text = "Edit Absence Record"
            DisplayRecord()
        Else
            Me.Text = "Create New Absence Record"
        End If

    End Sub

    Private Sub frmSession_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        cdtFrom.Focus()
    End Sub

    Private Sub DisplayRecord()

        Dim _C As Business.StaffAbsence = Business.StaffAbsence.RetreiveByID(m_RecordID.Value)
        With _C
            cdtFrom.Value = ._AbsFrom
            cdtTo.Value = ._AbsTo
            cbxType.Text = ._AbsType
            cbxReason.Text = ._AbsReason
            txtCost.Text = ValueHandler.MoneyAsText(_C._AbsCost)
            txtNotes.Text = ._AbsNotes
        End With

        _C = Nothing

    End Sub

    Private Function ValidateEntry() As Boolean
        Return MyControls.Validate(Me.Controls)
    End Function

    Private Sub SaveAndExit()

        If Not ValidateEntry() Then Exit Sub

        Dim _C As Business.StaffAbsence = Nothing

        If m_IsNew Then
            _C = New Business.StaffAbsence
            _C._ID = Guid.NewGuid
            _C._StaffId = m_StaffID
        Else
            _C = Business.StaffAbsence.RetreiveByID(m_RecordID.Value)
        End If

        With _C
            ._AbsFrom = cdtFrom.Value
            ._AbsTo = cdtTo.Value
            ._AbsType = cbxType.Text
            ._AbsReason = cbxReason.Text
            ._AbsCost = ValueHandler.ConvertDecimal(txtCost.Text)
            ._AbsNotes = txtNotes.Text
            .Store()
        End With

        _C = Nothing

        Me.DialogResult = DialogResult.OK
        Me.Close()

    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        SaveAndExit()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub
End Class
