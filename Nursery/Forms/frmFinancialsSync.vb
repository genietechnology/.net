﻿

Imports System.Windows.Forms
Imports Care.Global
Imports Care.Data
Imports Care.Shared

Public Class frmFinancialsSync

    Private m_Xero As Boolean = False
    Private m_System As String = ""
    Private m_DefaultNameFormat As String = ""
    Private m_Records As New List(Of SyncRecord)

    Private Class SyncRecord
        Public Property SiteID As Guid
        Public Property OurAccountNumber As String
        Public Property ExternalID As String
        Public Property Linked As Boolean
        Public Property ContactName As String
        Public Property Address As String
        Public Property Email As String
        Public Property Status As String
        Public Property ErrorText As String
        Public Property Family As Business.Family
        Public Property Entity As Care.Financials.Entity
    End Class

    Private Sub frmFinancialsSync_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If cbxNameFormat.Text <> m_DefaultNameFormat Then
            ParameterHandler.SetString("FINSYNCNAME", cbxNameFormat.Text)
        End If
    End Sub

    Private Sub frmFinancialsSync_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Session.CursorWaiting()

        m_System = Session.FinancialSystem.ToUpper

        cbxOption.AddItem("Synchronise New Contacts Only")
        cbxOption.AddItem("Synchronise New and Existing Contacts")

        cbxNameFormat.AddItem("Use Addressee from Family Record")
        cbxNameFormat.AddItem("Primary Contact Surname, Primary Contact Forename")
        cbxNameFormat.AddItem("Primary Contact Forename, Primary Contact Surname")

        cbxSite.PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)
        cbxSite.ReadOnly = True

        If m_System = "XERO" Then
            m_Xero = True
            cbxOption.AddItem("Link Existing Xero Accounts")
            cbxOption.AddItem("Blank Account Numbers")
            cbxNameFormat.AddItem("Primary Contact Surname, Forename, #Account No")
        End If

        cbxOption.SelectedIndex = 0
        cbxNameFormat.SelectedIndex = 0

        m_DefaultNameFormat = ParameterHandler.ReturnString("FINSYNCNAME")
        If m_DefaultNameFormat = "" Then m_DefaultNameFormat = "Primary Contact Surname, Primary Contact Forename"
        cbxNameFormat.Text = m_DefaultNameFormat

        btnRefresh.Enabled = False
        btnRun.Enabled = False

    End Sub

    Private Sub frmFinancialsSync_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        BuildRecords()
        Session.CursorDefault()
    End Sub

    Private Function CheckAccountNumbers() As Boolean

        Dim _Families As List(Of Business.Family) = Business.Family.RetrieveFamiliesWithoutAccountNos
        If _Families IsNot Nothing Then
            If _Families.Count > 0 Then
                Return False
            Else
                Return True
            End If
            _Families = Nothing
        Else
            Return True
        End If

    End Function

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click
        Run()
    End Sub

    Private Sub BuildRecords()

        btnRefresh.Enabled = False

        m_Records.Clear()

        Dim _Families As List(Of Business.Family) = Nothing

        If radAll.Checked Then
            _Families = Business.Family.RetrieveCurrentFamilies
        Else
            If cbxSite.Text <> "" Then
                _Families = Business.Family.RetrieveCurrentFamilies(New Guid(cbxSite.SelectedValue.ToString))
            End If
        End If

        If _Families IsNot Nothing Then

            Session.SetupProgressBar("Building Synchronise Package...", _Families.Count)

            For Each _f As Business.Family In _Families

                Dim _E As Care.Financials.Entity = ReturnEntity(_f)

                Dim _R As New SyncRecord
                With _R

                    .SiteID = _E.SiteID
                    .OurAccountNumber = _E.OurAccountNumber

                    .Linked = False
                    .ExternalID = _E.ExternalID
                    If .ExternalID <> "" Then .Linked = True

                    .ContactName = _E.ContactName
                    .Address = _f._Address

                    If _E.Email Is Nothing Then
                        .Email = ""
                    Else
                        .Email = _E.Email.ToLower
                    End If

                    .Entity = _E
                    .Family = _f
                    .ErrorText = ""

                    If .ContactName = "" Then
                        .Status = "Excluded"
                    Else
                        If _f._ExclFinancials Then
                            .Status = "Excluded"
                        Else
                            .Status = "Ready to Sync"
                        End If
                    End If

                End With

                m_Records.Add(_R)
                Session.StepProgressBar()

            Next

            Session.HideProgressBar()

        End If

        cgRecords.Populate(m_Records)
        cgRecords.PreviewColumn = "ErrorText"

        cgRecords.Columns("SiteID").Visible = False
        cgRecords.Columns("ExternalID").Visible = False
        cgRecords.Columns("Entity").Visible = False
        cgRecords.Columns("Family").Visible = False
        cgRecords.Columns("ErrorText").Visible = False

        If m_Records.Count > 0 Then btnRun.Enabled = True

        btnRefresh.Enabled = True

    End Sub

    Private Function ReturnEntity(ByRef _f As Business.Family) As Care.Financials.Entity

        Dim _E As New Care.Financials.Entity
        With _E

            .SiteID = _f._SiteId.Value
            .ExternalID = _f._FinancialsId
            .OurAccountNumber = _f._AccountNo

            'split the address into parts
            If _f._Address <> "" Then

                Dim _i As Integer = 1
                Dim _Lines As String() = _f._Address.Split(CChar(vbCrLf))
                For Each _Line As String In _Lines
                    If _Line <> "" Then
                        If _i = 1 Then .Address1 = _Line
                        If _i = 2 Then .Address2 = _Line
                        If _i = 3 Then .Address3 = _Line
                        If _i = 4 Then .Address4 = _Line
                        If _i = 5 Then .Address5 = _Line
                        _i += 1
                    End If
                Next

            End If

            Dim _C As Business.Contact = Business.Contact.RetrievePrimaryContact(_f._ID.ToString)
            If _C IsNot Nothing Then

                Select Case cbxNameFormat.Text

                    Case "Use Addressee from Family Record"
                        .ContactName = _f._LetterName

                    Case "Primary Contact Surname, Primary Contact Forename"
                        .ContactName = _C._Surname.Trim + ", " + _C._Forename.Trim

                    Case "Primary Contact Forename, Primary Contact Surname"
                        .ContactName = _C._Forename.Trim + " " + _C._Surname.Trim

                    Case "Primary Contact Surname, Forename, #Account No"
                        .ContactName = _C._Surname.Trim + ", " + _C._Forename.Trim + ", #" + _f._AccountNo

                End Select

                .Forename = _C._Forename.Trim
                .Surname = _C._Surname.Trim
                .Phone = _C._TelHome
                .Mobile = _C._TelMobile

                If _C._Email IsNot Nothing Then
                    .Email = _C._Email.ToLower
                Else
                    .Email = ""
                End If

                .Fax = _f._SiteName

            Else
                .ContactName = _f._LetterName
            End If

        End With

        Return _E

    End Function

    Private Sub Run()

        If cbxOption.Text = "Blank Account Numbers" Then
            ResetAccountNumbers()
            Exit Sub
        End If

        If m_System = "SAGE" Then
            If Not CheckAccountNumbers() Then
                CareMessage("There are Family records without Account Numbers. Account numbers must be present in order to Synchronise with Sage.", MessageBoxIcon.Warning, "Account Number Check")
                Exit Sub
            End If
        End If

        If CareMessage("Are you sure you want to Continue?", MessageBoxIcon.Question, MessageBoxButtons.YesNo, "Run") = DialogResult.No Then
            Exit Sub
        End If

        btnRefresh.Enabled = False
        btnRun.Enabled = False
        btnClose.Enabled = False

        If radAll.Checked Then
            For Each _s As Business.Site In Business.Site.RetreiveAll
                SyncSite(_s._ID.Value, _s._Name)
            Next
        Else
            SyncSite(New Guid(cbxSite.SelectedValue.ToString), cbxSite.Text)
        End If

        btnRefresh.Enabled = True
        btnRun.Enabled = True
        btnClose.Enabled = True

        Session.HideProgressBar()
        Session.CursorDefault()

    End Sub

    Private Sub SyncSite(ByVal SiteID As Guid, ByVal SiteName As String)

        Dim _Engine As New Care.Financials.Engine(FinanceShared.ReturnFinancialsIntegration(SiteID))

        Session.SetupProgressBar($"Synchronising {SiteName}...", m_Records.Count)
        Session.CursorWaiting()

        For Each _R As SyncRecord In m_Records

            If _R.Status = "Ready to Sync" AndAlso _R.SiteID = SiteID Then

                Select Case cbxOption.Text

                    Case "Synchronise New Contacts Only"
                        If Not _R.Linked Then
                            Dim _Response As Care.Financials.Response = _Engine.CreateEntity(_R.Entity)
                            If _Response.Success Then
                                _R.Family._FinancialsId = _R.Entity.ExternalID
                                _R.Family.Store()
                                _R.Status = "OK"
                                _R.ExternalID = _R.Entity.ExternalID
                            Else
                                _R.Status = "Error"
                                _R.ErrorText = _R.Entity.ExceptionText
                            End If
                        Else
                            _R.Status = "Skipped"
                        End If

                    Case "Synchronise New and Existing Contacts"

                        If _R.Linked Then
                            Dim _Response As Care.Financials.Response = _Engine.UpdateEntity(_R.Entity)
                            If _Response.Success Then
                                _R.Status = "OK"
                            Else
                                _R.Status = "Error"
                                _R.ErrorText = _R.Entity.ExceptionText
                            End If
                        Else
                            Dim _Response As Care.Financials.Response = _Engine.CreateEntity(_R.Entity)
                            If _Response.Success Then
                                _R.Family._FinancialsId = _R.Entity.ExternalID
                                _R.Family.Store()
                                _R.Status = "OK"
                                _R.ExternalID = _R.Entity.ExternalID
                            Else
                                _R.Status = "Error"
                                _R.ErrorText = _R.Entity.ExceptionText
                            End If
                        End If

                    Case "Link Existing Xero Accounts"
                        If Not _R.Linked Then
                            Dim _Response As Care.Financials.Response = _Engine.ReturnContact(_R.ContactName, _R.Email, _R.Entity)
                            If _Response.Success Then
                                _R.Family._FinancialsId = _R.Entity.ExternalID
                                _R.Family.Store()
                                _R.Status = "OK"
                                _R.ExternalID = _R.Entity.ExternalID
                            Else
                                _R.Status = "Not Found"
                            End If
                        Else
                            _R.Status = "Skipped"
                        End If

                End Select

                If m_Xero AndAlso _R.Status <> "Skipped" Then
                    Threading.Thread.Sleep(1100)
                End If

            End If

            cgRecords.UnderlyingGridControl.RefreshDataSource()
            Session.StepProgressBar()
            Application.DoEvents()

        Next

        _Engine.Disconnect()
        Threading.Thread.Sleep(3000)

    End Sub

    Private Sub ResetAccountNumbers()

        If CareMessage("Are you sure you want to Continue?", MessageBoxIcon.Question, MessageBoxButtons.YesNo, "Run") = DialogResult.No Then
            Exit Sub
        End If

        btnRun.Enabled = False
        btnClose.Enabled = False
        Session.CursorWaiting()

        'FinanceShared.ResetAccountNumbers()

        btnRun.Enabled = True
        btnClose.Enabled = True
        Session.CursorDefault()

    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        BuildRecords()
    End Sub

    Private Sub cgRecords_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles cgRecords.RowStyle

        If e IsNot Nothing And e.RowHandle >= 0 Then

            Select Case cgRecords.GetRowCellValue(e.RowHandle, "Status").ToString

                Case "Excluded", "Not Found"
                    e.Appearance.BackColor = txtOrange.BackColor

                Case "Skipped"
                    e.Appearance.BackColor = txtBlue.BackColor

                Case "Error"
                    e.Appearance.BackColor = txtRed.BackColor

                Case "OK"
                    e.Appearance.BackColor = txtGreen.BackColor

            End Select

        End If

    End Sub

    Private Sub cgRecords_GridDoubleClick(sender As Object, e As EventArgs) Handles cgRecords.GridDoubleClick

        If cgRecords Is Nothing Then Exit Sub
        If cgRecords.RecordCount <= 0 Then Exit Sub
        If cgRecords.RowIndex < 0 Then Exit Sub

        Dim _SRO As Object = cgRecords.UnderlyingGridView.GetFocusedRow
        If _SRO IsNot Nothing Then
            Dim _SR As SyncRecord = CType(_SRO, SyncRecord)
            If _SR.Family IsNot Nothing AndAlso _SR.Family._ID.HasValue Then
                Business.Family.DrillDown(_SR.Family._ID.Value)
            End If
        End If

    End Sub

    Private Sub radAll_CheckedChanged(sender As Object, e As EventArgs) Handles radAll.CheckedChanged
        If radAll.Checked Then
            cbxSite.SelectedIndex = -1
            cbxSite.ReadOnly = True
            BuildRecords()
        End If
    End Sub

    Private Sub radSpecific_CheckedChanged(sender As Object, e As EventArgs) Handles radSpecific.CheckedChanged
        If radSpecific.Checked Then
            cbxSite.ReadOnly = False
            cbxSite.SelectedIndex = 0
        End If
    End Sub

    Private Sub cbxSite_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSite.SelectedIndexChanged
        If cbxSite.SelectedIndex >= 0 Then
            BuildRecords()
        End If
    End Sub
End Class