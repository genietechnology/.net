﻿Imports Care.Global

Public Class frmLeadManagement

    Private m_RecordID As Guid? = Nothing

    Private Sub frmLeadManagement_Load(sender As Object, e As EventArgs) Handles Me.Load

        With cbxSite
            .PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)
            .SelectedIndex = 0
        End With

        cbxStage.AllowBlank = True
        cbxStage.PopulateWithSQL(Session.ConnectionString, "select name from LeadStages order by pcnt_complete, name")
        cbxStage.SelectedIndex = 0

        RefreshGrid()

    End Sub

    Private Sub RefreshGrid()

        Dim _SQL As String = ""
        _SQL += "select ID, date_entered as 'Entered', stage as 'Stage', stage_start as 'Stage Started', found_us as 'Found Us',"

        If radStandard.Checked Then
            _SQL += " via as 'Via', rating as 'Rating', comm_method as 'Comm. Method', comm_marketing as 'Marketing',"
        End If

        If radContact.Checked Then
            _SQL += " contact_fullname as 'Contact', contact_forename as 'Contact Forename', contact_surname as 'Contact Surname',"
            _SQL += " contact_relationship as 'Relationship', contact_home as 'Contact Home', contact_mobile as 'Mobile', contact_email as 'Contact Email',"
            _SQL += " child_fullname as 'Child', child_forename as 'Child Forename', child_surname as 'Child Surname',"
        Else
            _SQL += " contact_fullname as 'Contact',"
            _SQL += " contact_relationship as 'Relationship', contact_mobile as 'Mobile',"
            _SQL += " child_fullname as 'Child',"
        End If

        _SQL += " child_dob as 'DOB', child_gender as 'Gender',"

        If chkShowDays.Checked Then
            _SQL += " child_monday as 'Mon', child_tuesday as 'Tue', child_wednesday as 'Wed', child_thursday as 'Thu', child_friday as 'Fri', child_saturday as 'Sat', child_sunday as 'Sun',"
        End If

        _SQL += " child_starting as 'Starting',"

        If radDate.Checked Then
            _SQL += " date_first_contacted as '1st Contact', date_pack_sent as 'Pack Sent', date_viewing as 'Viewing',"
            _SQL += " date_quoted as 'Quote Sent', date_listed as 'On List',"
        End If

        _SQL += " date_last_contacted as 'Last Contact'"
        _SQL += " from Leads"

        _SQL += " where closed = 0"

        If cbxSite.SelectedIndex >= 0 Then
            _SQL += " and site_name = '" + cbxSite.Text + "'"
        End If

        If cbxStage.SelectedIndex > 0 Then
            _SQL += " and stage = '" + cbxStage.Text + "'"
        End If

        _SQL += " order by date_entered desc"

        cgLeads.HideFirstColumn = True
        cgLeads.Populate(Session.ConnectionString, _SQL)

        If cgLeads.RecordCount > 0 Then
            If radDate.Checked Then FormatDates()
            btnViewLead.Enabled = True
            btnProgressLead.Enabled = True
        Else
            btnViewLead.Enabled = False
            btnProgressLead.Enabled = False
        End If

    End Sub

    Private Sub FormatDates()
        cgLeads.Columns("1st Contact").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        cgLeads.Columns("1st Contact").DisplayFormat.FormatString = "dd/MM/yy HH:mm"
        cgLeads.Columns("Pack Sent").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        cgLeads.Columns("Pack Sent").DisplayFormat.FormatString = "dd/MM/yy HH:mm"
        cgLeads.Columns("Viewing").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        cgLeads.Columns("Viewing").DisplayFormat.FormatString = "dd/MM/yy HH:mm"
        cgLeads.Columns("Quote Sent").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        cgLeads.Columns("Quote Sent").DisplayFormat.FormatString = "dd/MM/yy HH:mm"
        cgLeads.Columns("On List").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        cgLeads.Columns("On List").DisplayFormat.FormatString = "dd/MM/yy HH:mm"
    End Sub

    Private Sub ViewLead()

        Session.CursorWaiting()

        Dim _LeadID As Guid = New Guid(cgLeads.CurrentRow.Item("id").ToString)
        Dim _frmLead As New frmLead(_LeadID)
        _frmLead.ShowDialog()

        If _frmLead.DialogResult = DialogResult.OK Then
            RefreshGrid()
        End If

        _frmLead.Dispose()

    End Sub

    Private Sub CreateLead()

        Session.CursorWaiting()

        Dim _frmLead As New frmLead
        _frmLead.ShowDialog()

        If _frmLead.DialogResult = DialogResult.OK Then
            RefreshGrid()
        End If

        _frmLead.Dispose()

    End Sub

    Private Sub ProgressLead()

        If cgLeads Is Nothing Then Exit Sub
        If cgLeads.RecordCount <= 0 Then Exit Sub

        Dim _LeadID As Guid = New Guid(cgLeads.CurrentRow.Item("id").ToString)

        Dim _frm As New frmLeadChangeStage(_LeadID)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            RefreshGrid()
        End If

        _frm.Dispose()
        _frm = Nothing

    End Sub

    Private Sub ConvertLead()

        If cgLeads Is Nothing Then Exit Sub
        If cgLeads.RecordCount <= 0 Then Exit Sub

        Dim _LeadID As Guid = New Guid(cgLeads.CurrentRow.Item("id").ToString)

        Dim _frm As New frmLeadConvert(_LeadID)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            RefreshGrid()
        End If

        _frm.Dispose()
        _frm = Nothing

    End Sub

#Region "Control Events"

    Private Sub cgLeads_GridDoubleClick(sender As Object, e As EventArgs) Handles cgLeads.GridDoubleClick
        ViewLead()
    End Sub

    Private Sub btnNewLead_Click(sender As Object, e As EventArgs) Handles btnNewLead.Click
        CreateLead()
    End Sub

    Private Sub btnViewLead_Click(sender As Object, e As EventArgs) Handles btnViewLead.Click
        ViewLead()
    End Sub

    Private Sub btnProgressLead_Click(sender As Object, e As EventArgs) Handles btnProgressLead.Click
        ProgressLead()
    End Sub

    Private Sub cbxStage_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxStage.SelectedIndexChanged
        RefreshGrid()
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub radStandard_CheckedChanged(sender As Object, e As EventArgs) Handles radStandard.CheckedChanged
        If radStandard.Checked Then RefreshGrid()
    End Sub

    Private Sub radContact_CheckedChanged(sender As Object, e As EventArgs) Handles radContact.CheckedChanged
        If radContact.Checked Then RefreshGrid()
    End Sub

    Private Sub radDate_CheckedChanged(sender As Object, e As EventArgs) Handles radDate.CheckedChanged
        If radDate.Checked Then RefreshGrid()
    End Sub

    Private Sub chkShowDays_CheckedChanged(sender As Object, e As EventArgs) Handles chkShowDays.CheckedChanged
        RefreshGrid()
    End Sub

    Private Sub btnConvert_Click(sender As Object, e As EventArgs) Handles btnConvert.Click
        ConvertLead()
    End Sub

    Private Sub cbxSite_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSite.SelectedIndexChanged
        RefreshGrid()
    End Sub

#End Region

End Class
