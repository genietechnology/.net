﻿Imports Care.Global
Imports Care.Data
Imports DevExpress.XtraCharts
Imports System.Windows.Forms
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraCharts.Printing

Public Class frmCharts

    Private m_ChartPrinter As ChartPrinter

    Private Sub frmCharts_Load(sender As Object, e As EventArgs) Handles Me.Load

        cbxStatus.AddItem("Daily Occupancy")
        cbxStatus.AddItem("Turnover by Period")
        cbxStatus.AddItem("Tariff Usage")
        cbxStatus.AddItem("Touchscreen Usage")
        cbxStatus.AddItem("Accident Hotspots")
        cbxStatus.AddItem("Accidents by Staff")
        cbxStatus.AddItem("Accidents by Children")
        cbxStatus.AddItem("Leads by Contact Method")
        cbxStatus.AddItem("Leads by Source")
        cbxStatus.AddItem("Children by Nationality")
        cbxStatus.AddItem("Children by Languages Spoken")
        cbxStatus.AddItem("Children by Ethnicity")

    End Sub

    Private Sub frmCharts_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        Application.DoEvents()

        Session.CursorWaiting()

        Session.SetProgressMessage("Initialising Spaces...")
        InitialiseSpaces()
        Session.SetProgressMessage("Build Complete", 3)

        cbxStatus.SelectedIndex = 0

        Session.CursorDefault()

    End Sub

    Private Sub InitialiseSpaces()

        DAL.ExecuteSQL(Session.ConnectionString, "truncate table Spaces")

        Dim _Eng As New SpaceEngine
        _Eng.InitialiseSpaces()

    End Sub

    Private Sub PopulateChart()

        rc.SelectedRange.Reset()

        cht.DataSource = Nothing
        cht.Series.Clear()
        cht.Legend.Visibility = DevExpress.Utils.DefaultBoolean.False

        Select Case cbxStatus.Text

            Case "Daily Occupancy"
                CreateOccupancyChart()

            Case "Turnover by Period"
                CreateTurnOverChart()

            Case "Tariff Usage"
                TariffUsage()

            Case "Touchscreen Usage"
                TouchscreenUsage()

            Case "Accident Hotspots"
                AccidentHotspots()

            Case "Accidents by Staff"
                AccidentsByStaff()

            Case "Accidents by Children"
                AccidentsByChildren()

            Case "Leads by Contact Method"
                LeadsByContactMethod()

            Case "Leads by Source"
                LeadsBySource()

            Case "Children by Nationality"
                Nationality()

            Case "Children by Languages Spoken"
                Language()

            Case "Children by Ethnicity"
                Ethnicity()

        End Select

    End Sub

    Private Sub AccidentHotspots()

        Dim _SQL As String = ""

        _SQL += "select location as 'Location', count(*) as 'Count' from Incidents"
        _SQL += " where stamp between " + ValueHandler.SQLDate(Today.AddMonths(-10), True) + " and " + ValueHandler.SQLDate(Today.AddMonths(-6), True)
        _SQL += " and incident_type = 'Accident'"
        _SQL += " group by location"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then
            AddSeries("Location", ScaleType.Auto, "Count", "Count", ViewType.Bar)
            cht.DataSource = _DT
        End If

        CType(cht.Diagram, XYDiagram).AxisY.Label.TextPattern = "{A:n0}"

    End Sub

    Private Sub AccidentsByStaff()

        Dim _SQL As String = ""

        _SQL += "select staff_name as 'Staff', count(*) as 'Count' from Incidents"
        _SQL += " where stamp between " + ValueHandler.SQLDate(Today.AddMonths(-10), True) + " and " + ValueHandler.SQLDate(Today.AddMonths(-6), True)
        _SQL += " and incident_type = 'Accident'"
        _SQL += " group by staff_name"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then
            AddSeries("Staff", ScaleType.Auto, "Count", "Count", ViewType.Bar)
            cht.DataSource = _DT
        End If

        CType(cht.Diagram, XYDiagram).AxisY.Label.TextPattern = "{A:n0}"

    End Sub

    Private Sub AccidentsByChildren()

        Dim _SQL As String = ""

        _SQL += "select child_name as 'Child', count(*) as 'Count' from Incidents"
        _SQL += " where stamp between " + ValueHandler.SQLDate(Today.AddMonths(-10), True) + " and " + ValueHandler.SQLDate(Today.AddMonths(-6), True)
        _SQL += " and incident_type = 'Accident'"
        _SQL += " group by child_name"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then
            AddSeries("Child", ScaleType.Auto, "Count", "Count", ViewType.Bar)
            cht.DataSource = _DT
        End If

        CType(cht.Diagram, XYDiagram).AxisY.Label.TextPattern = "{A:n0}"

    End Sub

    Private Sub LeadsByContactMethod()

        Dim _SQL As String = ""

        _SQL += "select via 'Contact Method', count(*) as 'Count' from Leads"
        _SQL += " where date_entered between " + ValueHandler.SQLDate(Today.AddMonths(-10), True) + " and " + ValueHandler.SQLDate(Today.AddMonths(-6), True)
        _SQL += " group by via"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then
            AddSeries("Contact Method", ScaleType.Auto, "Count", "Count", ViewType.Doughnut)
            cht.DataSource = _DT
        End If

    End Sub

    Private Sub LeadsBySource()

        Dim _SQL As String = ""

        _SQL += "select found_us 'Source', count(*) as 'Count' from Leads"
        _SQL += " where date_entered between " + ValueHandler.SQLDate(Today.AddMonths(-10), True) + " and " + ValueHandler.SQLDate(Today.AddMonths(-6), True)
        _SQL += " group by found_us"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then
            AddSeries("Source", ScaleType.Auto, "Count", "Count", ViewType.Doughnut)
            cht.DataSource = _DT
        End If

    End Sub

    Private Sub TouchscreenUsage()

        Dim _SQL As String = ""

        _SQL += "select staff_name as 'Staff', count(*) as 'Entries' from Activity"
        _SQL += " left join Day on Day.ID = Activity.day_id"
        _SQL += " where staff_name <> ''"
        _SQL += " and Day.date between " + ValueHandler.SQLDate(Today.AddMonths(-3), True) + " and " + ValueHandler.SQLDate(Today, True)
        _SQL += " group by staff_name"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then
            AddSeries("Staff", ScaleType.Auto, "Entries", "Entries", ViewType.Bar)
            cht.DataSource = _DT
        End If

        CType(cht.Diagram, XYDiagram).AxisY.Label.TextPattern = "{A:n0}"

    End Sub

    Private Sub CreateTurnOverChart()

        Dim _SQL As String = ""

        _SQL += "select LEFT(CONVERT(varchar, invoice_date, 112),6) as 'Period',"
        _SQL += " sum(value_funded) as 'Funded', sum(value_boltons) as 'BoltOns', sum(value_gross) as 'Total'"
        _SQL += " from InvoiceDays"
        _SQL += " group by LEFT(CONVERT(varchar, invoice_date, 112),6)"
        _SQL += " order by LEFT(CONVERT(varchar, invoice_date, 112),6)"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            AddSeries("Period", ScaleType.Auto, "Total", "Total", ViewType.StackedBar)
            AddSeries("Period", ScaleType.Auto, "BoltOns", "BoltOns", ViewType.StackedBar)
            AddSeries("Period", ScaleType.Auto, "Funded", "Funded", ViewType.StackedBar)

            cht.DataSource = _DT

            CType(cht.Diagram, XYDiagram).AxisY.Label.TextPattern = "{A:C2}"

        End If

    End Sub

    Private Sub TariffUsage()

        Dim _SQL As String = ""

        _SQL += "select tariff_name as 'Tariff', count(*) as 'Sessions' from Bookings"
        _SQL += " where booking_date between " + ValueHandler.SQLDate(Today, True) + " and " + ValueHandler.SQLDate(Today.AddMonths(1), True)
        _SQL += " group by tariff_name"
        _SQL += " order by tariff_name"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then
            AddSeries("Tariff", ScaleType.Auto, "Tariff", "Sessions", ViewType.Doughnut)
            cht.DataSource = _DT
        End If

    End Sub

    Private Sub CreateOccupancyChart()

        Dim _SQL As String = ""

        _SQL += "select space_date as 'Date', (sum(capacity) - sum(bk)) as 'Space', sum(bk) as 'Bookings' from Spaces"
        _SQL += " where space_date between " + ValueHandler.SQLDate(Today, True) + " and " + ValueHandler.SQLDate(Today.AddMonths(1), True)
        _SQL += " group by space_date"
        _SQL += " order by space_date"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            AddSeries("Date", ScaleType.DateTime, "Bookings", "Bookings", ViewType.FullStackedBar)
            AddSeries("Date", ScaleType.DateTime, "Space", "Space", ViewType.FullStackedBar)

            cht.DataSource = _DT

            CType(cht.Diagram, XYDiagram).AxisY.Label.TextPattern = "{A:P2}"

        End If

    End Sub

    Private Sub Nationality()

        Dim sql As String = ""
        sql = String.Concat("select i.name as 'Nationality', (select count(*) from children c where charindex(i.name,c.nationality) > 0) as 'Count'",
                        " from AppListItems i",
                        " where i.list_id = (select id From AppLists l Where l.name = 'Nationality')",
                        " group by i.name")

        Dim table As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, sql)
        If table IsNot Nothing Then
            AddSeries("Nationality", ScaleType.Auto, "Count", "Count", ViewType.Doughnut)
            cht.DataSource = table
        End If

    End Sub

    Private Sub Language()

        Dim sql As String = ""
        sql = String.Concat("select i.name as 'Language', (select count(*) from children c where charindex(i.name,c.language) > 0) as 'Count'",
                        " from AppListItems i",
                        " where i.list_id = (select id From AppLists l Where l.name = 'Language')",
                        " group by i.name")

        Dim table As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, sql)
        If table IsNot Nothing Then
            AddSeries("Language", ScaleType.Auto, "Count", "Count", ViewType.Doughnut)
            cht.DataSource = table
        End If

    End Sub

    Private Sub Ethnicity()

        Dim sql As String = ""
        sql = String.Concat("select ethnicity as 'Ethnicity', count(*) as 'Count'",
                            " from Children",
                            " group by ethnicity")

        Dim table As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, sql)
        If table IsNot Nothing Then
            AddSeries("Ethnicity", ScaleType.Auto, "Count", "Count", ViewType.Doughnut)
            cht.DataSource = table
        End If

    End Sub

    Private Sub AddSeries(ByVal ArgumentField As String, ByVal ArgumentScaleType As ScaleType, ByVal SeriesName As String, ByVal SeriesField As String, ByVal ViewType As ViewType)

        Dim _S As New Series(SeriesName, ViewType)

        _S.ArgumentScaleType = ArgumentScaleType
        _S.ArgumentDataMember = ArgumentField

        _S.ValueScaleType = ScaleType.Numerical
        _S.ValueDataMembers.AddRange(New String() {SeriesField})

        If ViewType = DevExpress.XtraCharts.ViewType.Doughnut Then
            _S.Label.TextPattern = "{A}: {VP:p2}"
            CType(_S.Label, DoughnutSeriesLabel).Position = PieSeriesLabelPosition.TwoColumns
        End If

        cht.Series.Add(_S)

    End Sub

    Private Sub cbxStatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxStatus.SelectedIndexChanged
        PopulateChart()
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click


        Dim l As New Link(New PrintingSystem())
        l.Landscape = True
        l.PaperKind = System.Drawing.Printing.PaperKind.A4

        Dim _PHF As PageHeaderFooter = TryCast(l.PageHeaderFooter, PageHeaderFooter)
        _PHF.Header.Font = New System.Drawing.Font("Verdana", 14)
        _PHF.Header.Content.AddRange({"", cbxStatus.Text, ""})
        _PHF.Header.LineAlignment = BrickAlignment.Center

        _PHF.Footer.Content.Add(Format(Now, "dd/MM/yyyy HH:mm:ss"))
        _PHF.Footer.LineAlignment = BrickAlignment.Center

        m_ChartPrinter = New ChartPrinter(cht)
        m_ChartPrinter.Initialize(l.PrintingSystem, l)
        m_ChartPrinter.SizeMode = PrintSizeMode.Zoom

        AddHandler l.CreateDetailArea, AddressOf CreateDetailArea

        l.ShowRibbonPreviewDialog(DevExpress.LookAndFeel.UserLookAndFeel.Default)
        m_ChartPrinter.Release()


    End Sub

    Private Sub CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        m_ChartPrinter.CreateDetail(e.Graph)
    End Sub

End Class