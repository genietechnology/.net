﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTodayDrillDown
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.cgRecords = New Care.Controls.CareGrid()
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'cgRecords
        '
        Me.cgRecords.AllowBuildColumns = True
        Me.cgRecords.AllowEdit = False
        Me.cgRecords.AllowHorizontalScroll = False
        Me.cgRecords.AllowMultiSelect = False
        Me.cgRecords.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgRecords.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgRecords.Appearance.Options.UseFont = True
        Me.cgRecords.AutoSizeByData = True
        Me.cgRecords.DisableAutoSize = False
        Me.cgRecords.DisableDataFormatting = False
        Me.cgRecords.FocusedRowHandle = -2147483648
        Me.cgRecords.HideFirstColumn = True
        Me.cgRecords.Location = New System.Drawing.Point(12, 12)
        Me.cgRecords.Name = "cgRecords"
        Me.cgRecords.PreviewColumn = ""
        Me.cgRecords.QueryID = Nothing
        Me.cgRecords.RowAutoHeight = False
        Me.cgRecords.SearchAsYouType = True
        Me.cgRecords.ShowAutoFilterRow = False
        Me.cgRecords.ShowFindPanel = False
        Me.cgRecords.ShowGroupByBox = False
        Me.cgRecords.ShowLoadingPanel = False
        Me.cgRecords.ShowNavigator = True
        Me.cgRecords.Size = New System.Drawing.Size(830, 354)
        Me.cgRecords.TabIndex = 0
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.DialogResult = DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(754, 374)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(88, 24)
        Me.btnClose.TabIndex = 6
        Me.btnClose.Tag = ""
        Me.btnClose.Text = "Close"
        '
        'frmTodayDrillDown
        '
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(854, 408)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.cgRecords)
        Me.FormBorderStyle = FormBorderStyle.Sizable
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(750, 350)
        Me.Name = "frmTodayDrillDown"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = ""
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cgRecords As Care.Controls.CareGrid
    Friend WithEvents btnClose As Care.Controls.CareButton

End Class
