﻿

Imports System.Windows.Forms
Imports Care.Global
Imports Care.Data
Imports DevExpress.XtraGrid.Views.BandedGrid

Public Class frmRosterBuild

    Public ReadOnly Property RotaID As Guid
        Get
            Return m_RotaID
        End Get
    End Property

    Private m_MemoEdit As New DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit

    '************************************************************
    Private m_Staff As List(Of Business.Staff)
    Private m_StaffAbsence As List(Of Business.StaffAbsence)
    Private m_StaffPatterns As List(Of Business.StaffPrefHour)
    Private m_Ratios As List(Of Business.RoomRatios)
    Private m_Calendar As List(Of Business.Calendar)
    Private m_Children As List(Of Business.Child)
    Private m_Bookings As List(Of Business.Booking)
    '************************************************************

    Private m_RotaID As Guid

    'Private m_Slots As New List(Of Business.RotaSlot)
    Private m_Slots As New List(Of Business.RotaSlot)

    Private m_SlotStaff As New List(Of Business.RotaSlotStaff)
    Private m_RotaStaff As New List(Of Business.RotaStaff)

    Private m_SlotChildren As New List(Of Business.RotaSlotStaff)
    Private m_RotaChildren As New List(Of Business.RotaChildren)

    Private Sub frmRosterBuild_Load(sender As Object, e As EventArgs) Handles Me.Load
        cbxSite.PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)
        cdtWC.Value = ValueHandler.NearestDate(Today, DayOfWeek.Monday, ValueHandler.EnumDirection.Backwards)
    End Sub

    Private Sub CreateRoster()

        Dim siteID As Guid = New Guid(cbxSite.SelectedValue.ToString)
        Dim weekCommencing As Date = cdtWC.Value.Value
        m_RotaID = Guid.NewGuid

        BuildRoster(siteID, weekCommencing)

        '************************************************************************************************************************

        Session.SetProgressMessage("Updating Rota Record...")

        Dim _R As New Business.Rota
        With _R
            ._ID = m_RotaID
            ._SiteId = SiteID
            ._SiteName = cbxSite.Text
            ._Wc = WeekCommencing
            ._Status = "Planned"
            ._CreateUser = Session.CurrentUser.UserCode
            ._CreateStamp = Now
            .Store()
        End With

        '************************************************************************************************************************

        Session.SetProgressMessage("Database Update Complete.", 3)

    End Sub

    Public Sub UpdateRoster(RotaID As Guid, SiteID As Guid, WeekCommencing As Date)

        m_RotaID = RotaID
        cdtWC.Value = WeekCommencing

        BuildRoster(SiteID, WeekCommencing)
        Session.SetProgressMessage("Database Update Complete.", 3)

    End Sub

    Private Sub BuildRoster(SiteID As Guid, WeekCommencing As Date)

        Dim _Sunday As Date = WeekCommencing.AddDays(6)
        Dim site As Business.Site = Business.Site.RetreiveByID(SiteID)

        '************************************************************************************************************************

        Session.SetProgressMessage("Caching Data for number crunching...")

        'static data to save round trips to the DB
        '************************************************************************************************************************
        m_Staff = Business.Staff.RetrieveLiveStaffBySite(SiteID)
        m_StaffAbsence = Business.StaffAbsence.RetreiveAll
        m_StaffPatterns = Business.StaffPrefHour.RetreiveAll
        m_Ratios = Business.RoomRatios.RetreiveBySite(SiteID)
        m_Calendar = Business.Calendar.RetreiveAll
        m_Children = Business.Child.RetrieveLiveAndWaitingChildrenBySiteName(site._Name)
        m_Bookings = Business.Booking.RetreiveBySiteDateRange(SiteID, WeekCommencing, _Sunday)
        '************************************************************************************************************************

        Session.SetProgressMessage("Building Slot Summary...")
        BuildSlotSummary(SiteID, WeekCommencing, _Sunday)

        Session.SetProgressMessage("Allocating Staff to Slots...")
        AutoAllocate(WeekCommencing)

        Session.HideProgressBar()

        '************************************************************************************************************************

        Session.SetProgressMessage("Creating Staff Summary...")
        CreateStaffSummary()

        Session.SetProgressMessage("Creating Child Summary...")
        CreateChildSummary()

        '************************************************************************************************************************

        Session.SetProgressMessage("Updating Rota Staff Summary...")
        Business.RotaStaff.SaveAll(m_RotaStaff)

        Session.SetProgressMessage("Updating Rota Children Summary...")
        Business.RotaChildren.SaveAll(m_RotaChildren)

        Session.SetProgressMessage("Updating Rota Slots...")
        Business.RotaSlot.SaveAll(m_Slots)

        Session.SetProgressMessage("Updating Rota Slot Allocation...")
        Business.RotaSlotStaff.SaveAll(m_SlotStaff)

    End Sub

    Private Function FixTime(ByVal TimeSpanIn As TimeSpan) As TimeSpan

        Select Case TimeSpanIn.Minutes

            Case 1, 16, 31, 46
                Return New TimeSpan(TimeSpanIn.Hours, TimeSpanIn.Minutes - 1, 0)

            Case 59
                Return New TimeSpan(TimeSpanIn.Hours + 1, 0, 0)

            Case 14, 29, 44
                Return New TimeSpan(TimeSpanIn.Hours, TimeSpanIn.Minutes + 1, 0)

            Case Else
                Return TimeSpanIn

        End Select

    End Function

    Private Sub CreateStaffSummary()

        'loop through all the staff
        For Each _Staff In m_Staff

            'process each day of the week
            Dim _WC As Date = cdtWC.Value.Value
            Dim _Sunday As Date = _WC.AddDays(6)

            Dim _Date As Date = _WC
            While _Date <= _Sunday

                Dim _Start As TimeSpan? = Nothing
                Dim _End As TimeSpan? = Nothing

                'get their start and end time of each day
                Dim _X = From _s As Business.RotaSlotStaff In m_SlotStaff
                         Where _s._RssDate = _Date And _s._RssStaffId = _Staff._ID
                         Order By _s._RssStart

                If _X IsNot Nothing AndAlso _X.Count > 0 Then
                    _Start = _X.First._RssStart
                End If

                Dim _Y = From _s As Business.RotaSlotStaff In m_SlotStaff
                         Where _s._RssDate = _Date And _s._RssStaffId = _Staff._ID
                         Order By _s._RssFinish Descending

                If _Y IsNot Nothing AndAlso _Y.Count > 0 Then
                    _End = _Y.First._RssFinish
                End If

                'calculate durations and costs per day
                If _Start.HasValue AndAlso _End.HasValue Then

                    Dim _RotaStaff As New Business.RotaStaff
                    With _RotaStaff

                        ._RsRotaId = m_RotaID
                        ._RsStatus = "Planned"
                        ._RsDate = _Date
                        ._RsRatioId = _X.First._RssRatio
                        ._RsRoom = _X.First._RssRoom

                        ._RsStart = FixTime(_Start)
                        ._RsFinish = FixTime(_End)

                        Dim _Duration As New TimeSpan(_End.Value.Ticks - _Start.Value.Ticks)
                        ._RsHours = ValueHandler.ConvertDecimal(_Duration.TotalHours)
                        ._RsCost = ._RsHours * _Staff._SalaryHour
                        ._RsStaffRate = _Staff._SalaryHour

                        ._RsStaffId = _Staff._ID
                        ._RsStaffName = _Staff._Fullname
                        If Not IsNothing(_Staff._Dob) Then ._RsStaffAge = CInt(Math.Floor(ValueHandler.ReturnExactMonths(_Staff._Dob.Value, Today) \ 12))
                        ._RsStaffLevel = _Staff._QualLevel
                        ._RsStaffPaed = _Staff._FaPaed
                        ._RsStaffFaid = _Staff._Fa
                        ._RsStaffSenco = _Staff._Senco

                    End With

                    m_RotaStaff.Add(_RotaStaff)

                End If

                _Date = _Date.AddDays(1)

            End While

        Next

    End Sub

    Private Sub CreateChildSummary()

        'loop through all the children
        For Each _Child In m_Children

            'process each day of the week
            Dim _WC As Date = cdtWC.Value.Value
            Dim _Sunday As Date = _WC.AddDays(6)

            Dim _Date As Date = _WC
            While _Date <= _Sunday

                'get their start and end time of each day
                Dim _Q = From _b As Business.Booking In m_Bookings
                         Where _b._BookingDate = _Date _
                             And _b._ChildId = _Child._ID _
                             And _b._BookingStatus = "Booked"
                         Order By _b._BookingFrom

                If _Q IsNot Nothing AndAlso _Q.Count = 1 Then

                    Dim _B As Business.Booking = _Q.First

                    Dim _RC As New Business.RotaChildren
                    With _RC
                        ._RcRotaId = m_RotaID
                        ._RcDate = _Date
                        ._RcRatioId = _B._RatioId
                        ._RcRoom = Business.RoomRatios.RetreiveRoomNameFromRatioID(_B._RatioId)
                        ._RcStart = _B._BookingFrom
                        ._RcFinish = _B._BookingTo
                        ._RcHours = _B._Hours
                        ._RcChildId = _B._ChildId
                        ._RcChildName = _B._ChildName
                        ._RcChildDob = _B._ChildDob
                        ._RcChildAge = _B._ChildAge
                    End With

                    m_RotaChildren.Add(_RC)

                End If

                _Date = _Date.AddDays(1)

            End While

        Next

    End Sub

    Private Sub BuildSlotSummary(ByVal SiteID As Guid, ByVal StartDate As Date, ByVal EndDate As Date)

        m_Slots.Clear()
        m_SlotStaff.Clear()

        Dim _SQL As String = ""

        _SQL += "select count(b.id) as 'Count', bs.booking_date, bs.ratio_id, bs.slot_id, bs.slot_start, bs.slot_end from BookingSlots bs"
        _SQL += " left join Bookings b on b.ID = bs.booking_id"
        _SQL += " left join SiteRoomRatios rr on rr.ID = b.ratio_id"
        _SQL += " left join SiteRooms r on r.ID = rr.room_id"
        _SQL += " left join Calendar c on c.date = bs.booking_date"
        _SQL += " where b.site_id = '" + SiteID.ToString + "'"
        _SQL += " and bs.booking_date between " + ValueHandler.SQLDate(StartDate, True) + " and " + ValueHandler.SQLDate(EndDate, True)
        _SQL += " and b.booking_status = 'Booked'"
        _SQL += " group by bs.booking_date, bs.ratio_id, bs.slot_id, bs.slot_start, bs.slot_end"
        _SQL += " order by booking_date, ratio_id, slot_start"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            Session.SetupProgressBar("Building Slots...", _DT.Rows.Count)

            For Each _DR As DataRow In _DT.Rows

                Dim _SS As New Business.RotaSlot

                _SS._ID = Guid.NewGuid
                _SS._RotaId = m_RotaID
                _SS._SlotID = ValueHandler.ConvertGUID(_DR.Item("slot_id"))

                _SS._SlotDate = ValueHandler.ConvertDate(_DR.Item("booking_date"))
                _SS._SlotStart = FixTime(Business.TimeSlot.GetTimeSpan(_DR.Item("slot_start")).Value)
                _SS._SlotFinish = FixTime(Business.TimeSlot.GetTimeSpan(_DR.Item("slot_end")).Value)

                _SS._SlotRatio = ValueHandler.ConvertGUID(_DR.Item("ratio_id"))
                _SS._SlotRoom = ReturnRoomName(_SS._SlotRatio.Value)

                _SS._StaffRatio = ReturnStaffRatio(_SS._SlotRatio.ToString)

                If IsOpen(_SS._SlotDate) Then
                    _SS._SlotOpen = True
                    _SS._ChildCount = ValueHandler.ConvertInteger(_DR.Item("count"))
                    _SS._StaffRequired = ReturnStaffRequired(_SS._ChildCount, _SS._StaffRatio)
                Else
                    _SS._SlotOpen = False
                    _SS._ChildCount = 0
                    _SS._StaffRequired = 0
                End If

                _SS._StaffRostered = 0
                _SS._StaffShort = 0
                _SS._StaffSpare = 0

                m_Slots.Add(_SS)
                Session.StepProgressBar()

            Next

            _DT.Dispose()
            _DT = Nothing

        End If

    End Sub

    Private Function IsOpen(ByVal DateIn As Date?) As Boolean

        If DateIn.HasValue Then

            Dim _Q As IEnumerable(Of Business.Calendar) = From _C As Business.Calendar In m_Calendar _
                                                          Where _C._Date = DateIn.Value

            If _Q IsNot Nothing AndAlso _Q.Count = 1 Then
                If _Q.First._Closed = False Then
                    Return True
                Else
                    Return False
                End If
            End If

        End If

        Return False

    End Function

    Private Function FixTime(ByVal TimeSpanIn As TimeSpan?) As TimeSpan

        Select Case TimeSpanIn.Value.Minutes

            Case 1, 16, 31, 46
                Return New TimeSpan(TimeSpanIn.Value.Hours, TimeSpanIn.Value.Minutes - 1, 0)

            Case 59
                Return New TimeSpan(TimeSpanIn.Value.Hours + 1, 0, 0)

            Case 14, 29, 44
                Return New TimeSpan(TimeSpanIn.Value.Hours, TimeSpanIn.Value.Minutes + 1, 0)

            Case Else
                Return TimeSpanIn.Value

        End Select

    End Function

    Private Sub AutoAllocate(ByVal WC As Date)

        Dim minimumLevel As Integer = Care.Shared.ParameterHandler.ReturnInteger("MINQUALLEVEL", False)

        Session.SetupProgressBar("Allocating Staff...", m_Slots.Count)

        For Each _Slot In m_Slots

            _Slot._StaffRosteredList = ""
            _Slot._StaffSpareList = ""

            If _Slot._ChildCount > 0 Then

                If _Slot._SlotOpen Then

                    Dim _Level As Integer = 0
                    Dim _StaffList As List(Of StaffItem) = ReturnStaff(WC, _Slot._SlotDate.Value, _Slot._SlotStart, _Slot._SlotFinish, _Slot._SlotRatio)

                    If _StaffList.Count > 0 Then

                        _StaffList = _StaffList.OrderByDescending(Function(x) x.InPreviousSlot).ToList

                        'allocate the staff
                        For Each _Staff In _StaffList

                            'if no staff have been rostered, we need a level 3 or above
                            'otherwise, we need level 2 or above
                            If _Slot._StaffRostered = 0 Then
                                _Level = 3
                            Else
                                _Level = minimumLevel
                            End If

                            If _Staff.OK AndAlso _Staff.StaffRecord._QualLevel >= _Level Then

                                'yep, we still need staff
                                If _Slot._StaffRequired > _Slot._StaffRostered Then

                                    Dim _SlotStaff As New Business.RotaSlotStaff

                                    _SlotStaff._RssRotaId = m_RotaID
                                    _SlotStaff._RssSlotId = _Slot._ID
                                    _SlotStaff._RssStatus = "Planned"

                                    _SlotStaff._RssDate = _Slot._SlotDate
                                    _SlotStaff._RssStart = FixTime(_Slot._SlotStart.Value)
                                    _SlotStaff._RssFinish = FixTime(_Slot._SlotFinish.Value)

                                    _SlotStaff._RssRatio = _Slot._SlotRatio
                                    _SlotStaff._RssRoom = _Slot._SlotRoom

                                    Dim _Diff As TimeSpan = _SlotStaff._RssFinish.Value - _SlotStaff._RssStart.Value
                                    _SlotStaff._RssHours = ValueHandler.ConvertDecimal(_Diff.TotalHours)
                                    _SlotStaff._RssCost = _SlotStaff._RssHours * _Staff.StaffRecord._SalaryHour

                                    _SlotStaff._RssStaffId = _Staff.StaffID
                                    _SlotStaff._RssStaffName = _Staff.StaffName

                                    _SlotStaff._RssStaffLevel = _Staff.StaffRecord._QualLevel
                                    _SlotStaff._RssStaffRate = _Staff.StaffRecord._SalaryHour
                                    _SlotStaff._RssStaffPaed = _Staff.StaffRecord._FaPaed
                                    _SlotStaff._RssStaffFaid = _Staff.StaffRecord._Fa
                                    _SlotStaff._RssStaffSenco = _Staff.StaffRecord._Senco

                                    m_SlotStaff.Add(_SlotStaff)

                                    If _Slot._StaffRosteredList = "" Then
                                        _Slot._StaffRosteredList += _Staff.StaffName
                                    Else
                                        _Slot._StaffRosteredList += vbCrLf + _Staff.StaffName
                                    End If

                                    _Slot._StaffRostered += 1

                                Else

                                    'spare staff
                                    If _Slot._StaffSpareList = "" Then
                                        _Slot._StaffSpareList += _Staff.StaffName
                                    Else
                                        _Slot._StaffSpareList += vbCrLf + _Staff.StaffName
                                    End If

                                    _Slot._StaffSpare += 1

                                End If

                            End If

                        Next

                    End If

                    _Slot._StaffShort = _Slot._StaffRequired - _Slot._StaffRostered

                Else
                    'closed
                End If

            Else
                'child count zero
            End If

            Session.StepProgressBar()

        Next

    End Sub

    Private Class StaffItem
        Property StaffID As Guid
        Property StaffName As String
        Property StaffAge As String
        Property OK As Boolean
        Property Qualified As Boolean
        Property AbsenceOK As Boolean
        Property PatternOK As Boolean
        Property StartOK As Boolean
        Property EndOK As Boolean
        Property PrefStart As TimeSpan?
        Property PrefEnd As TimeSpan?
        Property StaffRecord As Business.Staff
        Property InPreviousSlot As Boolean
    End Class

    Private Function ReturnStaff(ByVal WC As Date, ByVal DateReq As Date, TimeStart As TimeSpan?, TimeEnd As TimeSpan?, ByVal RatioID As Guid?) As List(Of StaffItem)

        Dim _Return As New List(Of StaffItem)
        Dim _RoomID As Guid? = ReturnRoomID(RatioID.Value)

        If _RoomID.HasValue Then

            'fetch all the staff for the specified room and qualification level
            Dim _Q As IEnumerable(Of Business.Staff) = From _S As Business.Staff In m_Staff
                                                       Where _S._GroupId = _RoomID
                                                       Order By _S._QualLevel Descending

            If _Q IsNot Nothing AndAlso _Q.Count > 0 Then

                For Each _S In _Q

                    Dim _StaffItem As New StaffItem

                    _StaffItem.StaffRecord = _S
                    _StaffItem.StaffID = _S._ID.Value
                    _StaffItem.StaffName = _S._Fullname

                    If _S._Dob.HasValue Then
                        _StaffItem.StaffAge = ValueHandler.DateDifferenceAsTextYM(_S._Dob.Value, DateReq)
                    Else
                        _StaffItem.StaffAge = ""
                    End If

                    _StaffItem.OK = False
                    _StaffItem.Qualified = False
                    _StaffItem.AbsenceOK = False
                    _StaffItem.PatternOK = False
                    _StaffItem.StartOK = False
                    _StaffItem.EndOK = False

                    'check for holidays or absence booked
                    If CheckAbsence(_S._ID.Value, DateReq) Then
                        _StaffItem.AbsenceOK = True
                        'check times are within the staff working pattern
                        If CheckWorkingPattern(_S._ID.Value, WC, DateReq, TimeStart.Value, TimeEnd.Value, _StaffItem.StartOK, _StaffItem.EndOK, _StaffItem.PrefStart, _StaffItem.PrefEnd) Then
                            _StaffItem.PatternOK = True
                            _StaffItem.OK = True
                        End If
                    End If

                    'this code will add a boolean flag to each staff member to check if they were booked into the previous slot
                    'this is so we can make sure to use staff who are already booked in on that day where possible
                    _StaffItem.InPreviousSlot = False
                    If m_SlotStaff.Count > 0 Then

                        Dim _lastIndex As Integer = m_SlotStaff.Count - 1
                        Dim _previousSlotStart As TimeSpan = m_SlotStaff(_lastIndex)._RssStart.Value
                        Dim _AlreadyBook As IEnumerable(Of Business.RotaSlotStaff) = From _staff As Business.RotaSlotStaff In m_SlotStaff
                                                                                     Where _staff._RssRatio = RatioID _
                                                                                         And _staff._RssDate = DateReq _
                                                                                         And _staff._RssStart = _previousSlotStart _
                                                                                         And _staff._RssStaffId = _StaffItem.StaffID

                        If _AlreadyBook IsNot Nothing AndAlso _AlreadyBook.Count > 0 Then
                            _StaffItem.InPreviousSlot = True
                        End If

                    End If

                    _Return.Add(_StaffItem)

                Next

            Else
                'no suitable staff
            End If

        Else
            'room id not found
        End If

        Return _Return

    End Function

    Private Function CheckAbsence(ByVal StaffID As Guid, ByVal CheckDate As Date) As Boolean

        Dim _Q As IEnumerable(Of Business.StaffAbsence) = From _a As Business.StaffAbsence In m_StaffAbsence _
                                                          Where _a._StaffId = StaffID _
                                                          And CheckDate >= _a._AbsFrom And CheckDate <= _a._AbsTo

        If _Q IsNot Nothing AndAlso _Q.Count > 0 Then
            'absence found
            Return False
        Else
            Return True
        End If

    End Function

    Private Function CheckWorkingPattern(ByVal StaffID As Guid, ByVal WC As Date, ByVal CheckDate As Date, StartTime As TimeSpan?, EndTime As TimeSpan?, _
                                         ByRef StartOK As Boolean, ByRef EndOK As Boolean, ByRef PrefStart As TimeSpan?, ByRef PrefEnd As TimeSpan?) As Boolean

        Dim _Q As IEnumerable(Of Business.StaffPrefHour) = From _p As Business.StaffPrefHour In m_StaffPatterns _
                                                  Where _p._StaffId = StaffID _
                                                  And _p._Wc <= WC _
                                                  Order By _p._Wc Descending

        If _Q IsNot Nothing AndAlso _Q.Count > 0 Then

            'we have a working pattern
            Dim _P As Business.StaffPrefHour = _Q.First

            'check this member of staff works on this day
            Select Case CheckDate.DayOfWeek

                Case DayOfWeek.Monday
                    PrefStart = _P._MonStart
                    PrefEnd = _P._MonFinish
                    Return TimesOK(_P._MonStart, _P._MonFinish, StartTime.Value, EndTime.Value, StartOK, EndOK)

                Case DayOfWeek.Tuesday
                    PrefStart = _P._TueStart
                    PrefEnd = _P._TueFinish
                    Return TimesOK(_P._TueStart, _P._TueFinish, StartTime.Value, EndTime.Value, StartOK, EndOK)

                Case DayOfWeek.Wednesday
                    PrefStart = _P._WedStart
                    PrefEnd = _P._WedFinish
                    Return TimesOK(_P._WedStart, _P._WedFinish, StartTime.Value, EndTime.Value, StartOK, EndOK)

                Case DayOfWeek.Thursday
                    PrefStart = _P._ThuStart
                    PrefEnd = _P._ThuFinish
                    Return TimesOK(_P._ThuStart, _P._ThuFinish, StartTime.Value, EndTime.Value, StartOK, EndOK)

                Case DayOfWeek.Friday
                    PrefStart = _P._FriStart
                    PrefEnd = _P._FriFinish
                    Return TimesOK(_P._FriStart, _P._FriFinish, StartTime.Value, EndTime.Value, StartOK, EndOK)

                Case DayOfWeek.Saturday
                    PrefStart = _P._SatStart
                    PrefEnd = _P._SatFinish
                    Return TimesOK(_P._SatStart, _P._SatFinish, StartTime.Value, EndTime.Value, StartOK, EndOK)

                Case DayOfWeek.Sunday
                    PrefStart = _P._SunStart
                    PrefEnd = _P._SunFinish
                    Return TimesOK(_P._SunStart, _P._SunFinish, StartTime.Value, EndTime.Value, StartOK, EndOK)

            End Select

            Return False

        Else
            'no working patterns found
            Return False
        End If

    End Function

    Private Function TimesOK(ByVal PatternFrom As TimeSpan?, ByVal PatternTo As TimeSpan?, ByVal SlotFrom As TimeSpan, ByVal SlotTo As TimeSpan, _
                             ByRef StartOK As Boolean, ByRef EndOK As Boolean) As Boolean

        If PatternFrom Is Nothing Then Return False
        If PatternTo Is Nothing Then Return False

        'slot is before start time
        If SlotFrom < PatternFrom Then
            StartOK = False
            Return False
        Else
            StartOK = True
        End If

        'slot is after finish time
        If SlotFrom > PatternTo OrElse SlotTo > PatternTo Then
            EndOK = False
            Return False
        Else
            EndOK = True
        End If

        Return True

    End Function

    Private Function ReturnStaffRequired(ByVal ChildCount As Integer, ByVal Ratio As Integer) As Integer

        If ChildCount = 0 Then Return 0
        If Ratio = 0 Then Return 0

        Dim _Required As Double = ChildCount / Ratio

        If _Required > 0 And _Required < 1 Then
            Return 1
        Else
            Dim _Count As Integer = CInt(Math.Ceiling(_Required))
            Return _Count
        End If

    End Function

    Private Function ReturnStaffAllocated(ByVal BookingDate As Date, ByVal RatioID As Guid?, ByVal TimeSlotID As Guid?) As String

        If m_SlotStaff.Count = 0 Then Return ""


        Return "x"
    End Function

    Private Function ReturnStaffRatio(ByVal RatioID As String) As Integer

        Dim _Q As IEnumerable(Of Business.RoomRatios) = From _RR As Business.RoomRatios In m_Ratios _
                                              Where _RR.RatioID = New Guid(RatioID)

        If _Q IsNot Nothing AndAlso _Q.Count > 0 Then
            Return _Q.First.Ratio
        Else
            Return 0
        End If

    End Function

    Private Function ReturnRoomID(ByVal RatioID As Guid) As Guid?

        Dim _Q As IEnumerable(Of Business.RoomRatios) = From _RR As Business.RoomRatios In m_Ratios _
                                              Where _RR.RatioID = RatioID

        If _Q IsNot Nothing AndAlso _Q.Count > 0 Then
            Return _Q.First.RoomID
        Else
            Return Nothing
        End If

    End Function

    Private Function ReturnRoomName(ByVal RatioID As Guid) As String

        Dim _Q As IEnumerable(Of Business.RoomRatios) = From _RR As Business.RoomRatios In m_Ratios _
                                              Where _RR.RatioID = RatioID

        If _Q IsNot Nothing AndAlso _Q.Count > 0 Then
            Return _Q.First.RoomName
        Else
            Return Nothing
        End If

    End Function

    Private Sub AddColumn(ByVal DateIn As Date, ByVal Name As String, StringType As String, ByRef DT As DataTable, ByRef BV As BandedGridView, ByRef BandOwner As GridBand)

        DT.Columns.Add(Name, Type.GetType(StringType))

        Dim _BC As BandedGridColumn = BV.Columns.AddField(Name)
        _BC.Caption = DateIn.DayOfWeek.ToString
        _BC.Visible = True
        _BC.OptionsColumn.AllowEdit = False
        _BC.OwnerBand = BandOwner

        BV.Columns.Add(_BC)

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click

        If cbxSite.Text = "" Then
            CareMessage("Please select a Site.", MessageBoxIcon.Exclamation, "Build Staff Roster")
            Exit Sub
        End If

        If cdtWC.Text = "" OrElse cdtWC.Value.HasValue = False Then
            CareMessage("Please enter a Week Commencing Date.", MessageBoxIcon.Exclamation, "Build Staff Roster")
            Exit Sub
        Else
            If cdtWC.Value.Value.DayOfWeek <> DayOfWeek.Monday Then
                CareMessage("Please a valid Week Commencing Date (must be a Monday).", MessageBoxIcon.Exclamation, "Build Staff Roster")
                Exit Sub
            End If
        End If

        Dim siteID As Guid = New Guid(cbxSite.SelectedValue.ToString)
        Dim weekCommencing As Date = cdtWC.Value.Value
        If RotaExists(siteID, weekCommencing) Then
            CareMessage("A Rota already exists for the selected Site and Week.", MessageBoxIcon.Exclamation, "Build Staff Roster")
            Exit Sub
        End If

        SetCMDs(False)
        Session.CursorWaiting()
        Application.DoEvents()

        CreateRoster()

        SetCMDs(True)
        Session.CursorDefault()

        Me.DialogResult = DialogResult.OK
        Me.Close()

    End Sub

    Private Sub SetCMDs(ByVal Enabled As Boolean)
        btnRun.Enabled = Enabled
        btnCancel.Enabled = Enabled
    End Sub

    Private Function RotaExists(SiteID As Guid, WeekCommencing As Date) As Boolean

        Dim sql As String = String.Concat("select * from rotas",
                                            " where site_id = '", SiteID, "'",
                                            " and wc = '", ValueHandler.SQLDate(WeekCommencing), "'")

        Dim row As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, sql)
        If row IsNot Nothing Then Return True

        Return False

    End Function

End Class