﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSchools
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.XtraTabControl1 = New Care.Controls.CareTab(Me.components)
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupBox4 = New Care.Controls.CareFrame()
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.txtAddress = New Care.Address.CareAddress()
        Me.txtTel = New Care.[Shared].CareTelephoneNumber()
        Me.Label14 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox2 = New Care.Controls.CareFrame()
        Me.txtCollectionFees = New Care.Controls.CareTextBox(Me.components)
        Me.txtDropOffFees = New Care.Controls.CareTextBox(Me.components)
        Me.Label8 = New Care.Controls.CareLabel(Me.components)
        Me.Label9 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox1 = New Care.Controls.CareFrame()
        Me.txtName = New Care.Controls.CareTextBox(Me.components)
        Me.Label17 = New Care.Controls.CareLabel(Me.components)
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.cgInsetDays = New Care.Controls.CareGridWithButtons()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.GroupBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.GroupBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.txtCollectionFees.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDropOffFees.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(348, 3)
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(257, 3)
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(0, 409)
        Me.Panel1.Size = New System.Drawing.Size(445, 36)
        Me.Panel1.TabIndex = 1
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XtraTabControl1.Appearance.Options.UseFont = True
        Me.XtraTabControl1.AppearancePage.Header.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.XtraTabControl1.AppearancePage.Header.Options.UseFont = True
        Me.XtraTabControl1.AppearancePage.HeaderActive.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.XtraTabControl1.AppearancePage.HeaderActive.Options.UseFont = True
        Me.XtraTabControl1.AppearancePage.HeaderDisabled.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.XtraTabControl1.AppearancePage.HeaderDisabled.Options.UseFont = True
        Me.XtraTabControl1.AppearancePage.HeaderHotTracked.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.XtraTabControl1.AppearancePage.HeaderHotTracked.Options.UseFont = True
        Me.XtraTabControl1.AppearancePage.PageClient.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.XtraTabControl1.AppearancePage.PageClient.Options.UseFont = True
        Me.XtraTabControl1.Location = New System.Drawing.Point(8, 55)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(429, 352)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.GroupBox4)
        Me.XtraTabPage1.Controls.Add(Me.GroupBox2)
        Me.XtraTabPage1.Controls.Add(Me.GroupBox1)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(423, 322)
        Me.XtraTabPage1.Text = "School Details"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.CareLabel1)
        Me.GroupBox4.Controls.Add(Me.txtAddress)
        Me.GroupBox4.Controls.Add(Me.txtTel)
        Me.GroupBox4.Controls.Add(Me.Label14)
        Me.GroupBox4.Location = New System.Drawing.Point(10, 58)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.ShowCaption = False
        Me.GroupBox4.Size = New System.Drawing.Size(404, 157)
        Me.GroupBox4.TabIndex = 1
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(8, 130)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(56, 15)
        Me.CareLabel1.TabIndex = 2
        Me.CareLabel1.Text = "Telephone"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAddress
        '
        Me.txtAddress.Address = ""
        Me.txtAddress.AddressBlock = ""
        Me.txtAddress.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.Appearance.Options.UseFont = True
        Me.txtAddress.County = ""
        Me.txtAddress.DisplayMode = Care.Address.CareAddress.EnumDisplayMode.SingleControl
        Me.txtAddress.Location = New System.Drawing.Point(113, 10)
        Me.txtAddress.MaxLength = 500
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.PostCode = ""
        Me.txtAddress.ReadOnly = False
        Me.txtAddress.Size = New System.Drawing.Size(275, 110)
        Me.txtAddress.TabIndex = 1
        Me.txtAddress.Tag = "AE"
        Me.txtAddress.Town = ""
        '
        'txtTel
        '
        Me.txtTel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTel.Appearance.Options.UseFont = True
        Me.txtTel.IsMobile = False
        Me.txtTel.Location = New System.Drawing.Point(113, 126)
        Me.txtTel.MaxLength = 15
        Me.txtTel.MinimumSize = New System.Drawing.Size(0, 22)
        Me.txtTel.Name = "txtTel"
        Me.txtTel.ReadOnly = False
        Me.txtTel.Size = New System.Drawing.Size(275, 22)
        Me.txtTel.TabIndex = 3
        Me.txtTel.Tag = "AE"
        '
        'Label14
        '
        Me.Label14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label14.Location = New System.Drawing.Point(8, 10)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(42, 15)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Address"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox2
        '
        Me.GroupBox2.AppearanceCaption.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.AppearanceCaption.Options.UseFont = True
        Me.GroupBox2.Controls.Add(Me.txtCollectionFees)
        Me.GroupBox2.Controls.Add(Me.txtDropOffFees)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Location = New System.Drawing.Point(10, 221)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(404, 94)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.Text = "Charges"
        '
        'txtCollectionFees
        '
        Me.txtCollectionFees.CharacterCasing = CharacterCasing.Upper
        Me.txtCollectionFees.EnterMoveNextControl = True
        Me.txtCollectionFees.Location = New System.Drawing.Point(113, 62)
        Me.txtCollectionFees.MaxLength = 14
        Me.txtCollectionFees.Name = "txtCollectionFees"
        Me.txtCollectionFees.NumericAllowNegatives = False
        Me.txtCollectionFees.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtCollectionFees.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCollectionFees.Properties.AccessibleName = "Secret Word"
        Me.txtCollectionFees.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtCollectionFees.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCollectionFees.Properties.Appearance.Options.UseFont = True
        Me.txtCollectionFees.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCollectionFees.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtCollectionFees.Properties.CharacterCasing = CharacterCasing.Upper
        Me.txtCollectionFees.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtCollectionFees.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtCollectionFees.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtCollectionFees.Properties.MaxLength = 14
        Me.txtCollectionFees.ReadOnly = False
        Me.txtCollectionFees.Size = New System.Drawing.Size(90, 20)
        Me.txtCollectionFees.TabIndex = 3
        Me.txtCollectionFees.Tag = "AE"
        Me.txtCollectionFees.TextAlign = HorizontalAlignment.Left
        Me.txtCollectionFees.ToolTipText = ""
        '
        'txtDropOffFees
        '
        Me.txtDropOffFees.CharacterCasing = CharacterCasing.Normal
        Me.txtDropOffFees.EnterMoveNextControl = True
        Me.txtDropOffFees.Location = New System.Drawing.Point(113, 34)
        Me.txtDropOffFees.MaxLength = 14
        Me.txtDropOffFees.Name = "txtDropOffFees"
        Me.txtDropOffFees.NumericAllowNegatives = False
        Me.txtDropOffFees.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtDropOffFees.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDropOffFees.Properties.AccessibleName = "Secret Word"
        Me.txtDropOffFees.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtDropOffFees.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDropOffFees.Properties.Appearance.Options.UseFont = True
        Me.txtDropOffFees.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDropOffFees.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtDropOffFees.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtDropOffFees.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtDropOffFees.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDropOffFees.Properties.MaxLength = 14
        Me.txtDropOffFees.ReadOnly = False
        Me.txtDropOffFees.Size = New System.Drawing.Size(90, 20)
        Me.txtDropOffFees.TabIndex = 1
        Me.txtDropOffFees.Tag = "AE"
        Me.txtDropOffFees.TextAlign = HorizontalAlignment.Left
        Me.txtDropOffFees.ToolTipText = ""
        '
        'Label8
        '
        Me.Label8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label8.Location = New System.Drawing.Point(8, 64)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(80, 15)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "Collection Fees"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label9.Location = New System.Drawing.Point(8, 37)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(72, 15)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Drop Off Fees"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtName)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Location = New System.Drawing.Point(10, 10)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.ShowCaption = False
        Me.GroupBox1.Size = New System.Drawing.Size(404, 42)
        Me.GroupBox1.TabIndex = 0
        '
        'txtName
        '
        Me.txtName.CharacterCasing = CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(113, 11)
        Me.txtName.MaxLength = 40
        Me.txtName.Name = "txtName"
        Me.txtName.NumericAllowNegatives = False
        Me.txtName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AccessibleName = "School Name"
        Me.txtName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtName.Properties.MaxLength = 40
        Me.txtName.ReadOnly = False
        Me.txtName.Size = New System.Drawing.Size(275, 20)
        Me.txtName.TabIndex = 1
        Me.txtName.Tag = "AEM"
        Me.txtName.TextAlign = HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'Label17
        '
        Me.Label17.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label17.Location = New System.Drawing.Point(8, 14)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(32, 15)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Name"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.cgInsetDays)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.PageVisible = False
        Me.XtraTabPage2.Size = New System.Drawing.Size(423, 322)
        Me.XtraTabPage2.Text = "Inset Days"
        '
        'cgInsetDays
        '
        Me.cgInsetDays.ButtonsEnabled = False
        Me.cgInsetDays.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgInsetDays.HideFirstColumn = False
        Me.cgInsetDays.Location = New System.Drawing.Point(7, 7)
        Me.cgInsetDays.Name = "cgInsetDays"
        Me.cgInsetDays.Size = New System.Drawing.Size(409, 316)
        Me.cgInsetDays.TabIndex = 0
        '
        'frmSchools
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(445, 445)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Name = "frmSchools"
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.XtraTabControl1, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.GroupBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.GroupBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.txtCollectionFees.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDropOffFees.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents XtraTabControl1 As Care.Controls.CareTab
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupBox4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtAddress As Care.Address.CareAddress
    Friend WithEvents txtTel As Care.Shared.CareTelephoneNumber
    Friend WithEvents Label14 As Care.Controls.CareLabel
    Friend WithEvents GroupBox2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtCollectionFees As Care.Controls.CareTextBox
    Friend WithEvents txtDropOffFees As Care.Controls.CareTextBox
    Friend WithEvents Label8 As Care.Controls.CareLabel
    Friend WithEvents Label9 As Care.Controls.CareLabel
    Friend WithEvents GroupBox1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents Label17 As Care.Controls.CareLabel
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents cgInsetDays As Care.Controls.CareGridWithButtons

End Class
