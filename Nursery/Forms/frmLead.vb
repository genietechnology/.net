﻿Imports Care.Global
Imports Care.Data
Imports System.Windows.Forms
Imports Care.Shared

Public Class frmLead

    Private m_Mode As String = ""
    Private m_SelectedLead As Guid? = Nothing
    Private m_Lead As Business.Lead = Nothing
    Private m_RefreshRequired As Boolean = False

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub New(ByVal LeadID As Guid?)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_SelectedLead = LeadID

    End Sub

    Private Sub frmLead_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If m_RefreshRequired Then Me.DialogResult = DialogResult.OK
    End Sub

    Private Sub frmLead_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Session.CursorWaiting()

        lblEntered.Text = ""
        lblAge.Text = ""
        lblStart.Text = ""

        PopulateLists()

        CareTab1.DisableTabs()
        btnAvailEmail.Enabled = False

        If m_SelectedLead.HasValue Then
            DisplayRecord()
            btnOK.Enabled = False
        Else
            m_Mode = "ADD"
            btnEdit.Enabled = False
            btnProgress.Enabled = False
            btnDeleteLead.Enabled = False
            btnOK.Enabled = True
            MyControls.SetControls(Care.Shared.ControlHandler.Mode.Add, Me.Controls)
        End If

        Session.CursorDefault()

    End Sub

    Private Sub PopulateLists()

        With cbxRating
            .AddItem("Cold")
            .AddItem("Warm")
            .AddItem("Hot")
        End With

        With cbxChildGender
            .AddItem("Unknown", "U")
            .AddItem("Male", "M")
            .AddItem("Female", "F")
        End With

        cbxFoundUs.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Lead Source"))
        cbxVia.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Lead Via"))
        cbxSite.PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)
        cbxCommMethod.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Communication Preferences"))

    End Sub

    Private Sub cdtEntered_TextChanged(sender As Object, e As EventArgs) Handles cdtEntered.TextChanged
        lblEntered.Text = ""
    End Sub

    Private Sub cdtEntered_Validated(sender As Object, e As EventArgs) Handles cdtEntered.Validated
        If cdtEntered.Value.HasValue Then
            lblEntered.Text = ValueHandler.DateDifferenceAsText(cdtEntered.Value.Value, Today, False, True, True)
        End If
    End Sub

    Private Sub CommitUpdate()

        Dim _Lead As Business.Lead = Nothing
        If m_Mode = "ADD" Then
            _Lead = New Business.Lead
            _Lead._Stage = "Entered"
        Else
            _Lead = m_Lead
        End If

        With _Lead

            ._ContactForename = txtContactForename.Text
            ._ContactSurname = txtContactSurname.Text
            ._ContactRelationship = txtContactRelationship.Text
            ._ContactFullname = txtContactForename.Text + " " + txtContactSurname.Text

            ._ContactAddress = txtAddressFull.Address
            ._ContactPostcode = txtAddressFull.PostCode

            ._ContactHome = txtTel.Text
            ._ContactMobile = txtMobile.Text
            ._ContactEmail = txtEmail.Text

            ._ContactNotes = memContactNotes.Text

            ._SiteId = New Guid(cbxSite.SelectedValue.ToString)
            ._SiteName = cbxSite.Text

            ._DateEntered = cdtEntered.Value
            ._FoundUs = cbxFoundUs.Text
            ._Via = cbxVia.Text
            ._Rating = cbxRating.Text

            ._ChildForename = txtChildForename.Text
            ._ChildSurname = txtChildSurname.Text
            ._ChildFullname = txtChildForename.Text + " " + txtChildSurname.Text
            ._ChildGender = cbxChildGender.Text
            ._ChildDob = cdtDOB.Value

            ._ChildStarting = cdtExpectedStart.Value
            If cbxExpectedRoom.SelectedValue IsNot Nothing Then
                ._ChildRoom = New Guid(cbxExpectedRoom.SelectedValue.ToString)
            Else
                ._ChildRoom = Nothing
            End If

            ._ChildMonday = chkMonday.Checked
            ._ChildTuesday = chkTuesday.Checked
            ._ChildWednesday = chkWednesday.Checked
            ._ChildThursday = chkThursday.Checked
            ._ChildFriday = chkFriday.Checked

            ._ChildNotes = memChildNotes.Text

            If m_Mode = "ADD" Then
                Dim _FirstStage As Business.LeadStage = Business.LeadStage.RetreiveFirstStage()
                If _FirstStage IsNot Nothing Then
                    ._StageId = _FirstStage._ID
                    ._Stage = _FirstStage._Name
                    ._StagePcnt = _FirstStage._PcntComplete
                    ._StageStart = Now
                End If
            End If

            ._CommMethod = cbxCommMethod.Text
            ._CommMarketing = chkMarketing.Checked

            .Store()

            If m_Mode = "ADD" Then
                m_SelectedLead = _Lead._ID
                DisplayRecord()
            End If

        End With


    End Sub

    Private Sub DisplayRecord()

        m_Lead = Business.Lead.RetreiveByID(m_SelectedLead.Value)

        Me.Text = "Manage Lead - " + m_Lead._ContactFullname + " | " + m_Lead._ChildFullname

        '////////////////////////////////////////////////////////////////////////////////////////////////////////

        With m_Lead

            txtContactForename.Text = ._ContactForename
            txtContactSurname.Text = ._ContactSurname
            txtContactRelationship.Text = ._ContactRelationship

            txtAddressFull.Address = ._ContactAddress
            txtAddressFull.PostCode = ._ContactPostcode

            txtTel.Text = ._ContactHome
            txtMobile.Text = ._ContactMobile
            txtEmail.Text = ._ContactEmail

            memContactNotes.Text = ._ContactNotes

            cbxSite.Text = ._SiteName
            cdtEntered.Value = ._DateEntered
            txtStage.Text = ._Stage
            cbxFoundUs.Text = ._FoundUs
            cbxVia.Text = ._Via
            cbxRating.Text = ._Rating

            txtChildForename.Text = ._ChildForename
            txtChildSurname.Text = ._ChildSurname
            cbxChildGender.SelectedValue = ._ChildGender
            cdtDOB.Value = ._ChildDob

            cdtExpectedStart.Value = ._ChildStarting
            cbxExpectedRoom.SelectedValue = ._ChildRoom

            chkMonday.Checked = ._ChildMonday
            chkTuesday.Checked = ._ChildTuesday
            chkWednesday.Checked = ._ChildWednesday
            chkThursday.Checked = ._ChildThursday
            chkFriday.Checked = ._ChildFriday

            memChildNotes.Text = ._ChildNotes

            cbxCommMethod.Text = ._CommMethod
            chkMarketing.Checked = ._CommMarketing

        End With

        '////////////////////////////////////////////////////////////////////////////////////////////////////////

        CareTab1.EnableTabs()
        CareTab1.FirstPage()

        btnEdit.Enabled = True
        btnOK.Enabled = False

        MyControls.SetControls(Care.Shared.ControlHandler.Mode.Locked, Me.Controls)

    End Sub

    Private Sub CareTab1_SelectedPageChanging(sender As Object, e As DevExpress.XtraTab.TabPageChangingEventArgs) Handles CareTab1.SelectedPageChanging

        btnAvailEmail.Enabled = False

        'always ensure the CRM control is setup
        CRM.CRMLinkID = m_SelectedLead
        CRM.CRMContactName = txtContactForename.Text + " " + txtContactSurname.Text
        CRM.CRMContactMobile = txtMobile.Text
        CRM.CRMContactEmail = txtEmail.Text

        Select Case e.Page.Name

            Case "tabAvailability"
                If BuildYearView() Then btnAvailEmail.Enabled = True

            Case "tabActivity"
                CRM.Populate()

        End Select

    End Sub

    Private Function BuildYearView() As Boolean

        If Not cdtExpectedStart.Value.HasValue Then Return False
        Dim _From As Date = cdtExpectedStart.Value.Value

        If cbxExpectedRoom.SelectedValue Is Nothing Then Return False
        If cbxExpectedRoom.SelectedValue.ToString = "" Then Return False

        Dim _Capacity As Integer = ReturnCapacity()

        Dim m_AvailabiltySummary As New List(Of AvailabilitySummary)

        Dim _AM As String = " select count(am) from Bookings b where b.booking_date = c.date and b.am = 1"
        _AM += " and b.ratio_id = '" & cbxExpectedRoom.SelectedValue.ToString & "'"

        Dim _PM As String = " select count(pm) from Bookings b where b.booking_date = c.date and b.pm = 1"
        _PM += " and b.ratio_id = '" & cbxExpectedRoom.SelectedValue.ToString & "'"

        Dim _SQL As String = ""

        _SQL += "select c.date as 'date',"
        _SQL += " (" & _AM & ") as 'am',"
        _SQL += " (" & _PM & ") as 'pm'"
        _SQL += " from Calendar c"
        _SQL += " where c.date between '" & ValueHandler.SQLDate(_From) & "' and '" & ValueHandler.SQLDate(_From.AddMonths(6)) & "'"
        _SQL += " order by c.date"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows
                Dim _S As New AvailabilitySummary
                _S.BookingDate = ValueHandler.ConvertDate(_DR.Item("date")).Value
                _S.AM = ValueHandler.ConvertInteger(_DR.Item("am"))
                _S.PM = ValueHandler.ConvertInteger(_DR.Item("pm"))
                _S.Capacity = _Capacity
                _S.VacanciesAM = _S.Capacity - _S.AM
                _S.VacanciesPM = _S.Capacity - _S.PM
                m_AvailabiltySummary.Add(_S)
            Next

            YearView1.Populate(m_AvailabiltySummary, _From, False)
            Return True

        End If

        Return False

    End Function

    Private Function ReturnCapacity() As Integer

        Dim _SQL As String = ""

        _SQL += "select isnull(sum(capacity),0) as 'capacity' from SiteRoomRatios"
        _SQL += " where ID = '" & cbxExpectedRoom.SelectedValue.ToString & "'"

        Return ValueHandler.ConvertInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))

    End Function

    Private Sub YearView1_DoubleClick(sender As Object, e As YearView.YearViewDoubleClickArgs) Handles YearView1.DoubleClick
        If cbxExpectedRoom.Text = "" Then Exit Sub
        Bookings.BookingsDrillDown(e.HitDate.Value, New Guid(cbxSite.SelectedValue.ToString), New Guid(cbxExpectedRoom.SelectedValue.ToString), "Availability Drilldown")
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click

        Dim _msg As String = ""

        Select Case m_Mode

            Case "ADD"
                _msg = "Are you sure you want to cancel creating this record?"

            Case "EDIT"
                _msg = "Are you sure you want to cancel amending this record?"

            Case Else
                Me.Close()
                Exit Sub

        End Select

        If CareMessage(_msg, MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Exit Form") = MsgBoxResult.Yes Then

            If m_Mode = "EDIT" Then
                m_Mode = ""
                MyControls.SetControls(ControlHandler.Mode.Locked, Me.Controls)
                DisplayRecord()
                Me.DialogResult = DialogResult.None
            Else
                m_Mode = ""
                Me.Close()
            End If

        End If

    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        If MyControls.Validate(Me.Controls) Then
            CommitUpdate()
            m_Mode = ""
            MyControls.SetControls(Care.Shared.ControlHandler.Mode.Locked, Me.Controls)
            btnEdit.Enabled = True
            btnOK.Enabled = False
            m_RefreshRequired = True
        End If
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        m_Mode = "EDIT"
        MyControls.SetControls(Care.Shared.ControlHandler.Mode.Edit, Me.Controls)
        btnEdit.Enabled = False
        btnOK.Enabled = True
        txtContactForename.Focus()
    End Sub

    Private Sub btnAvailEmail_Click(sender As Object, e As EventArgs) Handles btnAvailEmail.Click

        If txtEmail.Text = "" Then Exit Sub
        If txtContactForename.Text = "" Then Exit Sub
        If txtContactSurname.Text = "" Then Exit Sub

        btnAvailEmail.Enabled = False
        Session.CursorWaiting()

        Dim _PDFFile As String = Session.TempFolder + "Availabilty-" + txtContactForename.Text + txtContactSurname.Text + ".pdf"
        YearView1.ExportToPDF(_PDFFile, "Availability Calendar for " + txtContactForename.Text + " " + txtContactSurname.Text)

        If IO.File.Exists(_PDFFile) Then

            Dim _Body As String = ""
            _Body += "Dear " + txtContactForename.Text + "," + vbCrLf + vbCrLf
            _Body += "Please find the attached Availability Calendar."

            If EmailHandler.SendEmailWithAttachment(Nothing, txtEmail.Text, "Availability Calendar", _Body, _PDFFile) = 0 Then
                CRM.LogActivity(Care.Shared.CRM.EnumActivityType.Email, "Availability Calendar Sent", "")
            End If

        End If

        btnAvailEmail.Enabled = True
        Session.CursorDefault()

    End Sub

    Private Sub frmLead_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        If m_Mode = "ADD" Then
            cbxChildGender.SelectedIndex = 0
            txtContactForename.Focus()
        End If
    End Sub

    Private Sub btnProgress_Click(sender As Object, e As EventArgs) Handles btnProgress.Click

        Dim _frm As New frmLeadChangeStage(m_SelectedLead.Value)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            Me.DialogResult = DialogResult.OK
            Me.Close()
        End If

        _frm.Dispose()
        _frm = Nothing

    End Sub

    Private Sub btnDeleteLead_Click(sender As Object, e As EventArgs) Handles btnDeleteLead.Click

        If CareMessage("Are you sure you want to Delete this Lead?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Delete Lead") = DialogResult.Yes Then
            If ParameterHandler.ManagerAuthorised Then

                Dim _SQL As String = ""

                _SQL = "delete from Leads where ID = '" + m_Lead._ID.ToString + "'"
                DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                _SQL = "delete from ActivityCRM where link_id = '" + m_Lead._ID.ToString + "'"
                DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                Me.DialogResult = DialogResult.OK
                Me.Close()

            End If
        End If

    End Sub

    Private Sub cbxSite_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSite.SelectedIndexChanged
        If cbxSite.Text = "" Then
            cbxExpectedRoom.Clear()
            cbxExpectedRoom.SelectedIndex = -1
        Else
            cbxExpectedRoom.PopulateWithSQL(Session.ConnectionString, Business.RoomRatios.RetreiveSiteRoomRatioSQL(cbxSite.SelectedValue.ToString))
        End If
    End Sub
End Class
