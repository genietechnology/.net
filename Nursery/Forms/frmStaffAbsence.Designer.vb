﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStaffAbsence
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.gbxWeekly = New Care.Controls.CareFrame()
        Me.cbxType = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.cdtTo = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.txtCost = New Care.Controls.CareTextBox(Me.components)
        Me.cdtFrom = New Care.Controls.CareDateTime(Me.components)
        Me.btnOK = New Care.Controls.CareButton(Me.components)
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        Me.cbxReason = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.txtNotes = New DevExpress.XtraEditors.MemoEdit()
        CType(Me.gbxWeekly, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxWeekly.SuspendLayout()
        CType(Me.cbxType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCost.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxReason.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtNotes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'gbxWeekly
        '
        Me.gbxWeekly.Controls.Add(Me.CareLabel3)
        Me.gbxWeekly.Controls.Add(Me.cbxReason)
        Me.gbxWeekly.Controls.Add(Me.cbxType)
        Me.gbxWeekly.Controls.Add(Me.CareLabel2)
        Me.gbxWeekly.Controls.Add(Me.cdtTo)
        Me.gbxWeekly.Controls.Add(Me.CareLabel1)
        Me.gbxWeekly.Controls.Add(Me.CareLabel14)
        Me.gbxWeekly.Controls.Add(Me.CareLabel11)
        Me.gbxWeekly.Controls.Add(Me.txtCost)
        Me.gbxWeekly.Controls.Add(Me.cdtFrom)
        Me.gbxWeekly.Location = New System.Drawing.Point(12, 12)
        Me.gbxWeekly.Name = "gbxWeekly"
        Me.gbxWeekly.Size = New System.Drawing.Size(392, 145)
        Me.gbxWeekly.TabIndex = 0
        Me.gbxWeekly.Text = "Absence Details"
        '
        'cbxType
        '
        Me.cbxType.AllowBlank = False
        Me.cbxType.DataSource = Nothing
        Me.cbxType.DisplayMember = Nothing
        Me.cbxType.EnterMoveNextControl = True
        Me.cbxType.Location = New System.Drawing.Point(113, 57)
        Me.cbxType.Name = "cbxType"
        Me.cbxType.Properties.AccessibleName = "Absence Type"
        Me.cbxType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxType.Properties.Appearance.Options.UseFont = True
        Me.cbxType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxType.SelectedValue = Nothing
        Me.cbxType.Size = New System.Drawing.Size(270, 22)
        Me.cbxType.TabIndex = 5
        Me.cbxType.Tag = "AEM"
        Me.cbxType.ValueMember = Nothing
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(204, 32)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(11, 15)
        Me.CareLabel2.TabIndex = 2
        Me.CareLabel2.Text = "to"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtTo
        '
        Me.cdtTo.EditValue = Nothing
        Me.cdtTo.EnterMoveNextControl = True
        Me.cdtTo.Location = New System.Drawing.Point(221, 29)
        Me.cdtTo.Name = "cdtTo"
        Me.cdtTo.Properties.AccessibleName = "Absence To Date"
        Me.cdtTo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtTo.Properties.Appearance.Options.UseFont = True
        Me.cdtTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtTo.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtTo.Size = New System.Drawing.Size(85, 22)
        Me.cdtTo.TabIndex = 3
        Me.cdtTo.Tag = "AEM"
        Me.cdtTo.Value = Nothing
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(11, 60)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(74, 15)
        Me.CareLabel1.TabIndex = 4
        Me.CareLabel1.Text = "Absence Type"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(11, 116)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(24, 15)
        Me.CareLabel14.TabIndex = 8
        Me.CareLabel14.Text = "Cost"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(11, 32)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(55, 15)
        Me.CareLabel11.TabIndex = 0
        Me.CareLabel11.Text = "From Date"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCost
        '
        Me.txtCost.CharacterCasing = CharacterCasing.Normal
        Me.txtCost.EnterMoveNextControl = True
        Me.txtCost.Location = New System.Drawing.Point(113, 113)
        Me.txtCost.MaxLength = 14
        Me.txtCost.Name = "txtCost"
        Me.txtCost.NumericAllowNegatives = True
        Me.txtCost.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtCost.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCost.Properties.AccessibleName = "Cost"
        Me.txtCost.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtCost.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtCost.Properties.Appearance.Options.UseFont = True
        Me.txtCost.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtCost.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txtCost.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtCost.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtCost.Properties.MaxLength = 14
        Me.txtCost.Size = New System.Drawing.Size(85, 22)
        Me.txtCost.TabIndex = 9
        Me.txtCost.Tag = "AE"
        Me.txtCost.TextAlign = HorizontalAlignment.Left
        Me.txtCost.ToolTipText = ""
        '
        'cdtFrom
        '
        Me.cdtFrom.EditValue = Nothing
        Me.cdtFrom.EnterMoveNextControl = True
        Me.cdtFrom.Location = New System.Drawing.Point(113, 29)
        Me.cdtFrom.Name = "cdtFrom"
        Me.cdtFrom.Properties.AccessibleDescription = ""
        Me.cdtFrom.Properties.AccessibleName = "Absence From Date"
        Me.cdtFrom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtFrom.Properties.Appearance.Options.UseFont = True
        Me.cdtFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtFrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtFrom.Size = New System.Drawing.Size(85, 22)
        Me.cdtFrom.TabIndex = 1
        Me.cdtFrom.Tag = "AEM"
        Me.cdtFrom.Value = Nothing
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(228, 478)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(85, 25)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.CausesValidation = False
        Me.btnCancel.DialogResult = DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(319, 478)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 25)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        '
        'cbxReason
        '
        Me.cbxReason.AllowBlank = False
        Me.cbxReason.DataSource = Nothing
        Me.cbxReason.DisplayMember = Nothing
        Me.cbxReason.EnterMoveNextControl = True
        Me.cbxReason.Location = New System.Drawing.Point(113, 85)
        Me.cbxReason.Name = "cbxReason"
        Me.cbxReason.Properties.AccessibleName = "Absence Type"
        Me.cbxReason.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxReason.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxReason.Properties.Appearance.Options.UseFont = True
        Me.cbxReason.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxReason.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxReason.SelectedValue = Nothing
        Me.cbxReason.Size = New System.Drawing.Size(270, 22)
        Me.cbxReason.TabIndex = 7
        Me.cbxReason.Tag = "AE"
        Me.cbxReason.ValueMember = Nothing
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(11, 88)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(38, 15)
        Me.CareLabel3.TabIndex = 6
        Me.CareLabel3.Text = "Reason"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.txtNotes)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 163)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(392, 309)
        Me.GroupControl1.TabIndex = 1
        Me.GroupControl1.Text = "Absence Details"
        '
        'txtNotes
        '
        Me.txtNotes.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtNotes.Location = New System.Drawing.Point(11, 26)
        Me.txtNotes.Name = "txtNotes"
        Me.txtNotes.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNotes.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtNotes, True)
        Me.txtNotes.Size = New System.Drawing.Size(370, 275)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtNotes, OptionsSpelling1)
        Me.txtNotes.TabIndex = 0
        Me.txtNotes.Tag = ""
        '
        'frmStaffAbsence
        '
        Me.AcceptButton = Me.btnOK
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(414, 511)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.gbxWeekly)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmStaffAbsence"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "Absence Record"
        CType(Me.gbxWeekly, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxWeekly.ResumeLayout(False)
        Me.gbxWeekly.PerformLayout()
        CType(Me.cbxType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCost.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxReason.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.txtNotes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxWeekly As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents txtCost As Care.Controls.CareTextBox
    Friend WithEvents cdtFrom As Care.Controls.CareDateTime
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents cdtTo As Care.Controls.CareDateTime
    Friend WithEvents cbxType As Care.Controls.CareComboBox
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents cbxReason As Care.Controls.CareComboBox
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtNotes As DevExpress.XtraEditors.MemoEdit

End Class
