﻿
Public Class frmTescoOrder

    Dim _TAPI As New TescoAPI

    Private Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click

        If _TAPI.Login() Then
            lblStatus.Text = _TAPI.SessionKey
        End If

    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Dim _SearchText As String = InputBox("Search:")
        Dim _Products As List(Of TescoAPI.Product) = _TAPI.Search(_SearchText)
        cgResults.Populate(_Products)
    End Sub
End Class

