﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmToday
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling2 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.timRefresh = New Timer(Me.components)
        Me.btnDayPatterns = New Care.Controls.CareButton(Me.components)
        Me.panTop = New DevExpress.XtraEditors.PanelControl()
        Me.txtSite = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.txtDate = New Care.Controls.CareTextBox(Me.components)
        Me.Label2 = New Care.Controls.CareLabel(Me.components)
        Me.panMain = New DevExpress.XtraEditors.PanelControl()
        Me.tlpMain = New TableLayoutPanel()
        Me.dshPayments = New Care.Controls.DashboardLabel()
        Me.dshTasks = New Care.Controls.DashboardLabel()
        Me.dshChecklists = New Care.Controls.DashboardLabel()
        Me.dshStaffExpected = New Care.Controls.DashboardLabel()
        Me.dshPrior = New Care.Controls.DashboardLabel()
        Me.dshAbsences = New Care.Controls.DashboardLabel()
        Me.dshViewings = New Care.Controls.DashboardLabel()
        Me.dshBirthdays = New Care.Controls.DashboardLabel()
        Me.dshLeavers = New Care.Controls.DashboardLabel()
        Me.dshAccidents = New Care.Controls.DashboardLabel()
        Me.dshStaffIn = New Care.Controls.DashboardLabel()
        Me.dshChildrenIn = New Care.Controls.DashboardLabel()
        Me.dshChildrenExpected = New Care.Controls.DashboardLabel()
        Me.gbxActivity = New Care.Controls.CareFrame()
        Me.txtActivity = New DevExpress.XtraEditors.MemoEdit()
        Me.gbxMessage = New Care.Controls.CareFrame()
        Me.txtNarr = New DevExpress.XtraEditors.MemoEdit()
        Me.gbxTop = New Care.Controls.CareFrame()
        Me.btnClearSnackPM = New Care.Controls.CareButton(Me.components)
        Me.btnSelectSnackPM = New Care.Controls.CareButton(Me.components)
        Me.txtSnackPM = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.btnClearTeaDessert = New Care.Controls.CareButton(Me.components)
        Me.btnSelectTeaDessert = New Care.Controls.CareButton(Me.components)
        Me.txtTeaDessert = New Care.Controls.CareTextBox(Me.components)
        Me.Label8 = New Care.Controls.CareLabel(Me.components)
        Me.btnClearLunchDessert = New Care.Controls.CareButton(Me.components)
        Me.btnSelectLunchDessert = New Care.Controls.CareButton(Me.components)
        Me.txtLunchDessert = New Care.Controls.CareTextBox(Me.components)
        Me.Label7 = New Care.Controls.CareLabel(Me.components)
        Me.btnClearTea = New Care.Controls.CareButton(Me.components)
        Me.btnSelectTea = New Care.Controls.CareButton(Me.components)
        Me.txtTea = New Care.Controls.CareTextBox(Me.components)
        Me.Label5 = New Care.Controls.CareLabel(Me.components)
        Me.btnClearLunch = New Care.Controls.CareButton(Me.components)
        Me.btnSelectLunch = New Care.Controls.CareButton(Me.components)
        Me.txtLunch = New Care.Controls.CareTextBox(Me.components)
        Me.Label4 = New Care.Controls.CareLabel(Me.components)
        Me.btnClearSnack = New Care.Controls.CareButton(Me.components)
        Me.btnSelectSnack = New Care.Controls.CareButton(Me.components)
        Me.txtSnack = New Care.Controls.CareTextBox(Me.components)
        Me.Label3 = New Care.Controls.CareLabel(Me.components)
        Me.btnClearBreakfast = New Care.Controls.CareButton(Me.components)
        Me.btnSelectBreakfast = New Care.Controls.CareButton(Me.components)
        Me.txtBreakfast = New Care.Controls.CareTextBox(Me.components)
        Me.Label1 = New Care.Controls.CareLabel(Me.components)
        Me.gbxGrid = New Care.Controls.CareFrame()
        Me.grdGroups = New Care.Controls.CareGrid()
        Me.dshStarters = New Care.Controls.DashboardLabel()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.panTop, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panTop.SuspendLayout()
        CType(Me.txtSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.panMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panMain.SuspendLayout()
        Me.tlpMain.SuspendLayout()
        CType(Me.gbxActivity, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxActivity.SuspendLayout()
        CType(Me.txtActivity.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxMessage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxMessage.SuspendLayout()
        CType(Me.txtNarr.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxTop, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxTop.SuspendLayout()
        CType(Me.txtSnackPM.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTeaDessert.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLunchDessert.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTea.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLunch.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSnack.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBreakfast.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxGrid.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(821, 5)
        Me.btnCancel.TabIndex = 3
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(730, 5)
        Me.btnOK.TabIndex = 2
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnDayPatterns)
        Me.Panel1.Location = New System.Drawing.Point(0, 657)
        Me.Panel1.Size = New System.Drawing.Size(914, 35)
        Me.Panel1.TabIndex = 1
        Me.Panel1.Controls.SetChildIndex(Me.btnDayPatterns, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnOK, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnCancel, 0)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'timRefresh
        '
        Me.timRefresh.Interval = 1000
        '
        'btnDayPatterns
        '
        Me.btnDayPatterns.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnDayPatterns.Appearance.Options.UseFont = True
        Me.btnDayPatterns.Location = New System.Drawing.Point(12, 5)
        Me.btnDayPatterns.Name = "btnDayPatterns"
        Me.btnDayPatterns.Size = New System.Drawing.Size(123, 25)
        Me.btnDayPatterns.TabIndex = 1
        Me.btnDayPatterns.Tag = ""
        Me.btnDayPatterns.Text = "Rebuild Day Patterns"
        '
        'panTop
        '
        Me.panTop.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.panTop.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.panTop.Controls.Add(Me.txtSite)
        Me.panTop.Controls.Add(Me.CareLabel2)
        Me.panTop.Controls.Add(Me.txtDate)
        Me.panTop.Controls.Add(Me.Label2)
        Me.panTop.Location = New System.Drawing.Point(7, 52)
        Me.panTop.Name = "panTop"
        Me.panTop.Size = New System.Drawing.Size(899, 37)
        Me.panTop.TabIndex = 0
        '
        'txtSite
        '
        Me.txtSite.CharacterCasing = CharacterCasing.Normal
        Me.txtSite.EnterMoveNextControl = True
        Me.txtSite.Location = New System.Drawing.Point(388, 8)
        Me.txtSite.MaxLength = 0
        Me.txtSite.Name = "txtSite"
        Me.txtSite.NumericAllowNegatives = False
        Me.txtSite.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSite.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSite.Properties.Appearance.Options.UseFont = True
        Me.txtSite.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSite.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSite.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSite.Size = New System.Drawing.Size(252, 22)
        Me.txtSite.TabIndex = 5
        Me.txtSite.Tag = "R"
        Me.txtSite.TextAlign = HorizontalAlignment.Left
        Me.txtSite.ToolTipText = ""
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(363, 11)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(19, 15)
        Me.CareLabel2.TabIndex = 2
        Me.CareLabel2.Text = "Site"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDate
        '
        Me.txtDate.CharacterCasing = CharacterCasing.Normal
        Me.txtDate.EnterMoveNextControl = True
        Me.txtDate.Location = New System.Drawing.Point(69, 8)
        Me.txtDate.MaxLength = 0
        Me.txtDate.Name = "txtDate"
        Me.txtDate.NumericAllowNegatives = False
        Me.txtDate.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtDate.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtDate.Properties.Appearance.Options.UseFont = True
        Me.txtDate.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtDate.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDate.Size = New System.Drawing.Size(279, 22)
        Me.txtDate.TabIndex = 1
        Me.txtDate.Tag = "R"
        Me.txtDate.TextAlign = HorizontalAlignment.Left
        Me.txtDate.ToolTipText = ""
        '
        'Label2
        '
        Me.Label2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label2.Location = New System.Drawing.Point(15, 11)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(24, 15)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Date"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'panMain
        '
        Me.panMain.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.panMain.Controls.Add(Me.tlpMain)
        Me.panMain.Location = New System.Drawing.Point(7, 92)
        Me.panMain.Name = "panMain"
        Me.panMain.Size = New System.Drawing.Size(899, 559)
        Me.panMain.TabIndex = 2
        '
        'tlpMain
        '
        Me.tlpMain.ColumnCount = 6
        Me.tlpMain.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 16.66667!))
        Me.tlpMain.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 16.66667!))
        Me.tlpMain.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 16.66667!))
        Me.tlpMain.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 16.66667!))
        Me.tlpMain.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 16.66667!))
        Me.tlpMain.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 16.66667!))
        Me.tlpMain.Controls.Add(Me.dshPayments, 5, 2)
        Me.tlpMain.Controls.Add(Me.dshTasks, 5, 4)
        Me.tlpMain.Controls.Add(Me.dshChecklists, 4, 4)
        Me.tlpMain.Controls.Add(Me.dshStaffExpected, 4, 1)
        Me.tlpMain.Controls.Add(Me.dshPrior, 5, 3)
        Me.tlpMain.Controls.Add(Me.dshAbsences, 4, 2)
        Me.tlpMain.Controls.Add(Me.dshViewings, 3, 4)
        Me.tlpMain.Controls.Add(Me.dshBirthdays, 2, 4)
        Me.tlpMain.Controls.Add(Me.dshLeavers, 1, 4)
        Me.tlpMain.Controls.Add(Me.dshAccidents, 4, 3)
        Me.tlpMain.Controls.Add(Me.dshStaffIn, 5, 1)
        Me.tlpMain.Controls.Add(Me.dshChildrenIn, 5, 0)
        Me.tlpMain.Controls.Add(Me.dshChildrenExpected, 4, 0)
        Me.tlpMain.Controls.Add(Me.gbxActivity, 2, 2)
        Me.tlpMain.Controls.Add(Me.gbxMessage, 2, 0)
        Me.tlpMain.Controls.Add(Me.gbxTop, 0, 0)
        Me.tlpMain.Controls.Add(Me.gbxGrid, 0, 2)
        Me.tlpMain.Controls.Add(Me.dshStarters, 0, 4)
        Me.tlpMain.Dock = DockStyle.Fill
        Me.tlpMain.Location = New System.Drawing.Point(2, 2)
        Me.tlpMain.Name = "tlpMain"
        Me.tlpMain.RowCount = 5
        Me.tlpMain.RowStyles.Add(New RowStyle(SizeType.Percent, 20.0!))
        Me.tlpMain.RowStyles.Add(New RowStyle(SizeType.Percent, 20.0!))
        Me.tlpMain.RowStyles.Add(New RowStyle(SizeType.Percent, 20.0!))
        Me.tlpMain.RowStyles.Add(New RowStyle(SizeType.Percent, 20.0!))
        Me.tlpMain.RowStyles.Add(New RowStyle(SizeType.Percent, 20.0!))
        Me.tlpMain.RowStyles.Add(New RowStyle(SizeType.Absolute, 20.0!))
        Me.tlpMain.Size = New System.Drawing.Size(895, 555)
        Me.tlpMain.TabIndex = 2
        '
        'dshPayments
        '
        Me.dshPayments.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.dshPayments.Dock = DockStyle.Fill
        Me.dshPayments.LabelBottom = "Today"
        Me.dshPayments.LabelMiddle = "55"
        Me.dshPayments.LabelTop = "Payments"
        Me.dshPayments.Location = New System.Drawing.Point(748, 225)
        Me.dshPayments.Name = "dshPayments"
        Me.dshPayments.Size = New System.Drawing.Size(144, 105)
        Me.dshPayments.TabIndex = 19
        '
        'dshTasks
        '
        Me.dshTasks.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.dshTasks.Dock = DockStyle.Fill
        Me.dshTasks.LabelBottom = "This Week"
        Me.dshTasks.LabelMiddle = "55"
        Me.dshTasks.LabelTop = "Tasks Due"
        Me.dshTasks.Location = New System.Drawing.Point(748, 447)
        Me.dshTasks.Name = "dshTasks"
        Me.dshTasks.Size = New System.Drawing.Size(144, 105)
        Me.dshTasks.TabIndex = 18
        '
        'dshChecklists
        '
        Me.dshChecklists.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.dshChecklists.Dock = DockStyle.Fill
        Me.dshChecklists.LabelBottom = "Incomplete"
        Me.dshChecklists.LabelMiddle = "55"
        Me.dshChecklists.LabelTop = "Checklists"
        Me.dshChecklists.Location = New System.Drawing.Point(599, 447)
        Me.dshChecklists.Name = "dshChecklists"
        Me.dshChecklists.Size = New System.Drawing.Size(143, 105)
        Me.dshChecklists.TabIndex = 16
        '
        'dshStaffExpected
        '
        Me.dshStaffExpected.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.dshStaffExpected.Dock = DockStyle.Fill
        Me.dshStaffExpected.LabelBottom = "Expected Today"
        Me.dshStaffExpected.LabelMiddle = "55"
        Me.dshStaffExpected.LabelTop = "Staff"
        Me.dshStaffExpected.Location = New System.Drawing.Point(599, 114)
        Me.dshStaffExpected.Name = "dshStaffExpected"
        Me.dshStaffExpected.Size = New System.Drawing.Size(143, 105)
        Me.dshStaffExpected.TabIndex = 14
        '
        'dshPrior
        '
        Me.dshPrior.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.dshPrior.Dock = DockStyle.Fill
        Me.dshPrior.LabelBottom = "Today"
        Me.dshPrior.LabelMiddle = "55"
        Me.dshPrior.LabelTop = "Incidents Offsite"
        Me.dshPrior.Location = New System.Drawing.Point(748, 336)
        Me.dshPrior.Name = "dshPrior"
        Me.dshPrior.Size = New System.Drawing.Size(144, 105)
        Me.dshPrior.TabIndex = 13
        '
        'dshAbsences
        '
        Me.dshAbsences.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.dshAbsences.Dock = DockStyle.Fill
        Me.dshAbsences.LabelBottom = "Today"
        Me.dshAbsences.LabelMiddle = "55"
        Me.dshAbsences.LabelTop = "Child Absences"
        Me.dshAbsences.Location = New System.Drawing.Point(599, 225)
        Me.dshAbsences.Name = "dshAbsences"
        Me.dshAbsences.Size = New System.Drawing.Size(143, 105)
        Me.dshAbsences.TabIndex = 8
        '
        'dshViewings
        '
        Me.dshViewings.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.dshViewings.Dock = DockStyle.Fill
        Me.dshViewings.LabelBottom = "This Week"
        Me.dshViewings.LabelMiddle = "55"
        Me.dshViewings.LabelTop = "Viewings"
        Me.dshViewings.Location = New System.Drawing.Point(450, 447)
        Me.dshViewings.Name = "dshViewings"
        Me.dshViewings.Size = New System.Drawing.Size(143, 105)
        Me.dshViewings.TabIndex = 7
        '
        'dshBirthdays
        '
        Me.dshBirthdays.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.dshBirthdays.Dock = DockStyle.Fill
        Me.dshBirthdays.LabelBottom = "This Week"
        Me.dshBirthdays.LabelMiddle = "55"
        Me.dshBirthdays.LabelTop = "Birthdays"
        Me.dshBirthdays.Location = New System.Drawing.Point(301, 447)
        Me.dshBirthdays.Name = "dshBirthdays"
        Me.dshBirthdays.Size = New System.Drawing.Size(143, 105)
        Me.dshBirthdays.TabIndex = 6
        '
        'dshLeavers
        '
        Me.dshLeavers.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.dshLeavers.Dock = DockStyle.Fill
        Me.dshLeavers.LabelBottom = "This Week"
        Me.dshLeavers.LabelMiddle = "55"
        Me.dshLeavers.LabelTop = "Leavers"
        Me.dshLeavers.Location = New System.Drawing.Point(152, 447)
        Me.dshLeavers.Name = "dshLeavers"
        Me.dshLeavers.Size = New System.Drawing.Size(143, 105)
        Me.dshLeavers.TabIndex = 5
        '
        'dshAccidents
        '
        Me.dshAccidents.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.dshAccidents.Dock = DockStyle.Fill
        Me.dshAccidents.LabelBottom = "Today"
        Me.dshAccidents.LabelMiddle = "55"
        Me.dshAccidents.LabelTop = "Incidents Onsite"
        Me.dshAccidents.Location = New System.Drawing.Point(599, 336)
        Me.dshAccidents.Name = "dshAccidents"
        Me.dshAccidents.Size = New System.Drawing.Size(143, 105)
        Me.dshAccidents.TabIndex = 9
        '
        'dshStaffIn
        '
        Me.dshStaffIn.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.dshStaffIn.Dock = DockStyle.Fill
        Me.dshStaffIn.LabelBottom = "Checked In"
        Me.dshStaffIn.LabelMiddle = "55"
        Me.dshStaffIn.LabelTop = "Staff"
        Me.dshStaffIn.Location = New System.Drawing.Point(748, 114)
        Me.dshStaffIn.Name = "dshStaffIn"
        Me.dshStaffIn.Size = New System.Drawing.Size(144, 105)
        Me.dshStaffIn.TabIndex = 10
        '
        'dshChildrenIn
        '
        Me.dshChildrenIn.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.dshChildrenIn.Dock = DockStyle.Fill
        Me.dshChildrenIn.LabelBottom = "Checked In"
        Me.dshChildrenIn.LabelMiddle = "55"
        Me.dshChildrenIn.LabelTop = "Children"
        Me.dshChildrenIn.Location = New System.Drawing.Point(748, 3)
        Me.dshChildrenIn.Name = "dshChildrenIn"
        Me.dshChildrenIn.Size = New System.Drawing.Size(144, 105)
        Me.dshChildrenIn.TabIndex = 11
        '
        'dshChildrenExpected
        '
        Me.dshChildrenExpected.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.dshChildrenExpected.Dock = DockStyle.Fill
        Me.dshChildrenExpected.LabelBottom = "Expected Today"
        Me.dshChildrenExpected.LabelMiddle = "55"
        Me.dshChildrenExpected.LabelTop = "Children"
        Me.dshChildrenExpected.Location = New System.Drawing.Point(599, 3)
        Me.dshChildrenExpected.Name = "dshChildrenExpected"
        Me.dshChildrenExpected.Size = New System.Drawing.Size(143, 105)
        Me.dshChildrenExpected.TabIndex = 12
        '
        'gbxActivity
        '
        Me.tlpMain.SetColumnSpan(Me.gbxActivity, 2)
        Me.gbxActivity.Controls.Add(Me.txtActivity)
        Me.gbxActivity.Dock = DockStyle.Fill
        Me.gbxActivity.Location = New System.Drawing.Point(301, 225)
        Me.gbxActivity.Name = "gbxActivity"
        Me.tlpMain.SetRowSpan(Me.gbxActivity, 2)
        Me.gbxActivity.Size = New System.Drawing.Size(292, 216)
        Me.gbxActivity.TabIndex = 3
        '
        'txtActivity
        '
        Me.txtActivity.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtActivity.Location = New System.Drawing.Point(10, 29)
        Me.txtActivity.Name = "txtActivity"
        Me.txtActivity.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtActivity.Properties.Appearance.Options.UseFont = True
        Me.txtActivity.Properties.Appearance.Options.UseTextOptions = True
        Me.txtActivity.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtActivity, True)
        Me.txtActivity.Size = New System.Drawing.Size(272, 178)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtActivity, OptionsSpelling1)
        Me.txtActivity.TabIndex = 0
        Me.txtActivity.Tag = "E"
        '
        'gbxMessage
        '
        Me.tlpMain.SetColumnSpan(Me.gbxMessage, 2)
        Me.gbxMessage.Controls.Add(Me.txtNarr)
        Me.gbxMessage.Dock = DockStyle.Fill
        Me.gbxMessage.Location = New System.Drawing.Point(301, 3)
        Me.gbxMessage.Name = "gbxMessage"
        Me.tlpMain.SetRowSpan(Me.gbxMessage, 2)
        Me.gbxMessage.Size = New System.Drawing.Size(292, 216)
        Me.gbxMessage.TabIndex = 1
        Me.gbxMessage.Text = "Message (This message is displayed on every Daily Report)"
        '
        'txtNarr
        '
        Me.txtNarr.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtNarr.Location = New System.Drawing.Point(10, 29)
        Me.txtNarr.Name = "txtNarr"
        Me.txtNarr.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNarr.Properties.Appearance.Options.UseFont = True
        Me.txtNarr.Properties.Appearance.Options.UseTextOptions = True
        Me.txtNarr.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtNarr, True)
        Me.txtNarr.Size = New System.Drawing.Size(272, 178)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtNarr, OptionsSpelling2)
        Me.txtNarr.TabIndex = 0
        Me.txtNarr.Tag = "E"
        '
        'gbxTop
        '
        Me.tlpMain.SetColumnSpan(Me.gbxTop, 2)
        Me.gbxTop.Controls.Add(Me.btnClearSnackPM)
        Me.gbxTop.Controls.Add(Me.btnSelectSnackPM)
        Me.gbxTop.Controls.Add(Me.txtSnackPM)
        Me.gbxTop.Controls.Add(Me.CareLabel1)
        Me.gbxTop.Controls.Add(Me.btnClearTeaDessert)
        Me.gbxTop.Controls.Add(Me.btnSelectTeaDessert)
        Me.gbxTop.Controls.Add(Me.txtTeaDessert)
        Me.gbxTop.Controls.Add(Me.Label8)
        Me.gbxTop.Controls.Add(Me.btnClearLunchDessert)
        Me.gbxTop.Controls.Add(Me.btnSelectLunchDessert)
        Me.gbxTop.Controls.Add(Me.txtLunchDessert)
        Me.gbxTop.Controls.Add(Me.Label7)
        Me.gbxTop.Controls.Add(Me.btnClearTea)
        Me.gbxTop.Controls.Add(Me.btnSelectTea)
        Me.gbxTop.Controls.Add(Me.txtTea)
        Me.gbxTop.Controls.Add(Me.Label5)
        Me.gbxTop.Controls.Add(Me.btnClearLunch)
        Me.gbxTop.Controls.Add(Me.btnSelectLunch)
        Me.gbxTop.Controls.Add(Me.txtLunch)
        Me.gbxTop.Controls.Add(Me.Label4)
        Me.gbxTop.Controls.Add(Me.btnClearSnack)
        Me.gbxTop.Controls.Add(Me.btnSelectSnack)
        Me.gbxTop.Controls.Add(Me.txtSnack)
        Me.gbxTop.Controls.Add(Me.Label3)
        Me.gbxTop.Controls.Add(Me.btnClearBreakfast)
        Me.gbxTop.Controls.Add(Me.btnSelectBreakfast)
        Me.gbxTop.Controls.Add(Me.txtBreakfast)
        Me.gbxTop.Controls.Add(Me.Label1)
        Me.gbxTop.Dock = DockStyle.Fill
        Me.gbxTop.Location = New System.Drawing.Point(3, 3)
        Me.gbxTop.Name = "gbxTop"
        Me.tlpMain.SetRowSpan(Me.gbxTop, 2)
        Me.gbxTop.ShowCaption = False
        Me.gbxTop.Size = New System.Drawing.Size(292, 216)
        Me.gbxTop.TabIndex = 0
        Me.gbxTop.Text = "2"
        '
        'btnClearSnackPM
        '
        Me.btnClearSnackPM.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnClearSnackPM.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClearSnackPM.Appearance.Options.UseFont = True
        Me.btnClearSnackPM.Location = New System.Drawing.Point(233, 127)
        Me.btnClearSnackPM.Name = "btnClearSnackPM"
        Me.btnClearSnackPM.Size = New System.Drawing.Size(50, 22)
        Me.btnClearSnackPM.TabIndex = 19
        Me.btnClearSnackPM.Tag = "E"
        Me.btnClearSnackPM.Text = "Clear"
        '
        'btnSelectSnackPM
        '
        Me.btnSelectSnackPM.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnSelectSnackPM.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSelectSnackPM.Appearance.Options.UseFont = True
        Me.btnSelectSnackPM.Location = New System.Drawing.Point(180, 127)
        Me.btnSelectSnackPM.Name = "btnSelectSnackPM"
        Me.btnSelectSnackPM.Size = New System.Drawing.Size(50, 22)
        Me.btnSelectSnackPM.TabIndex = 18
        Me.btnSelectSnackPM.Tag = "E"
        Me.btnSelectSnackPM.Text = "Select"
        '
        'txtSnackPM
        '
        Me.txtSnackPM.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtSnackPM.CharacterCasing = CharacterCasing.Normal
        Me.txtSnackPM.EnterMoveNextControl = True
        Me.txtSnackPM.Location = New System.Drawing.Point(64, 127)
        Me.txtSnackPM.MaxLength = 0
        Me.txtSnackPM.Name = "txtSnackPM"
        Me.txtSnackPM.NumericAllowNegatives = False
        Me.txtSnackPM.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSnackPM.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSnackPM.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSnackPM.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSnackPM.Properties.Appearance.Options.UseFont = True
        Me.txtSnackPM.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSnackPM.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSnackPM.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSnackPM.Size = New System.Drawing.Size(110, 20)
        Me.txtSnackPM.TabIndex = 17
        Me.txtSnackPM.Tag = "R"
        Me.txtSnackPM.TextAlign = HorizontalAlignment.Left
        Me.txtSnackPM.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(10, 131)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(52, 15)
        Me.CareLabel1.TabIndex = 16
        Me.CareLabel1.Text = "PM Snack"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnClearTeaDessert
        '
        Me.btnClearTeaDessert.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnClearTeaDessert.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClearTeaDessert.Appearance.Options.UseFont = True
        Me.btnClearTeaDessert.Location = New System.Drawing.Point(233, 183)
        Me.btnClearTeaDessert.Name = "btnClearTeaDessert"
        Me.btnClearTeaDessert.Size = New System.Drawing.Size(50, 22)
        Me.btnClearTeaDessert.TabIndex = 27
        Me.btnClearTeaDessert.Tag = "E"
        Me.btnClearTeaDessert.Text = "Clear"
        '
        'btnSelectTeaDessert
        '
        Me.btnSelectTeaDessert.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnSelectTeaDessert.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSelectTeaDessert.Appearance.Options.UseFont = True
        Me.btnSelectTeaDessert.Location = New System.Drawing.Point(180, 183)
        Me.btnSelectTeaDessert.Name = "btnSelectTeaDessert"
        Me.btnSelectTeaDessert.Size = New System.Drawing.Size(50, 22)
        Me.btnSelectTeaDessert.TabIndex = 26
        Me.btnSelectTeaDessert.Tag = "E"
        Me.btnSelectTeaDessert.Text = "Select"
        '
        'txtTeaDessert
        '
        Me.txtTeaDessert.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtTeaDessert.CharacterCasing = CharacterCasing.Normal
        Me.txtTeaDessert.EnterMoveNextControl = True
        Me.txtTeaDessert.Location = New System.Drawing.Point(64, 183)
        Me.txtTeaDessert.MaxLength = 0
        Me.txtTeaDessert.Name = "txtTeaDessert"
        Me.txtTeaDessert.NumericAllowNegatives = False
        Me.txtTeaDessert.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtTeaDessert.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTeaDessert.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtTeaDessert.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtTeaDessert.Properties.Appearance.Options.UseFont = True
        Me.txtTeaDessert.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTeaDessert.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtTeaDessert.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTeaDessert.Size = New System.Drawing.Size(110, 20)
        Me.txtTeaDessert.TabIndex = 25
        Me.txtTeaDessert.Tag = "R"
        Me.txtTeaDessert.TextAlign = HorizontalAlignment.Left
        Me.txtTeaDessert.ToolTipText = ""
        '
        'Label8
        '
        Me.Label8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label8.Location = New System.Drawing.Point(8, 186)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(38, 15)
        Me.Label8.TabIndex = 24
        Me.Label8.Text = "Dessert"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnClearLunchDessert
        '
        Me.btnClearLunchDessert.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnClearLunchDessert.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClearLunchDessert.Appearance.Options.UseFont = True
        Me.btnClearLunchDessert.Location = New System.Drawing.Point(233, 99)
        Me.btnClearLunchDessert.Name = "btnClearLunchDessert"
        Me.btnClearLunchDessert.Size = New System.Drawing.Size(50, 22)
        Me.btnClearLunchDessert.TabIndex = 15
        Me.btnClearLunchDessert.Tag = "E"
        Me.btnClearLunchDessert.Text = "Clear"
        '
        'btnSelectLunchDessert
        '
        Me.btnSelectLunchDessert.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnSelectLunchDessert.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSelectLunchDessert.Appearance.Options.UseFont = True
        Me.btnSelectLunchDessert.Location = New System.Drawing.Point(180, 99)
        Me.btnSelectLunchDessert.Name = "btnSelectLunchDessert"
        Me.btnSelectLunchDessert.Size = New System.Drawing.Size(50, 22)
        Me.btnSelectLunchDessert.TabIndex = 14
        Me.btnSelectLunchDessert.Tag = "E"
        Me.btnSelectLunchDessert.Text = "Select"
        '
        'txtLunchDessert
        '
        Me.txtLunchDessert.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtLunchDessert.CharacterCasing = CharacterCasing.Normal
        Me.txtLunchDessert.EnterMoveNextControl = True
        Me.txtLunchDessert.Location = New System.Drawing.Point(64, 99)
        Me.txtLunchDessert.MaxLength = 0
        Me.txtLunchDessert.Name = "txtLunchDessert"
        Me.txtLunchDessert.NumericAllowNegatives = False
        Me.txtLunchDessert.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtLunchDessert.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLunchDessert.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtLunchDessert.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtLunchDessert.Properties.Appearance.Options.UseFont = True
        Me.txtLunchDessert.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLunchDessert.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtLunchDessert.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtLunchDessert.Size = New System.Drawing.Size(110, 20)
        Me.txtLunchDessert.TabIndex = 13
        Me.txtLunchDessert.Tag = "R"
        Me.txtLunchDessert.TextAlign = HorizontalAlignment.Left
        Me.txtLunchDessert.ToolTipText = ""
        '
        'Label7
        '
        Me.Label7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label7.Location = New System.Drawing.Point(10, 103)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(38, 15)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Dessert"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnClearTea
        '
        Me.btnClearTea.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnClearTea.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClearTea.Appearance.Options.UseFont = True
        Me.btnClearTea.Location = New System.Drawing.Point(233, 155)
        Me.btnClearTea.Name = "btnClearTea"
        Me.btnClearTea.Size = New System.Drawing.Size(50, 22)
        Me.btnClearTea.TabIndex = 23
        Me.btnClearTea.Tag = "E"
        Me.btnClearTea.Text = "Clear"
        '
        'btnSelectTea
        '
        Me.btnSelectTea.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnSelectTea.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSelectTea.Appearance.Options.UseFont = True
        Me.btnSelectTea.Location = New System.Drawing.Point(180, 155)
        Me.btnSelectTea.Name = "btnSelectTea"
        Me.btnSelectTea.Size = New System.Drawing.Size(50, 22)
        Me.btnSelectTea.TabIndex = 22
        Me.btnSelectTea.Tag = "E"
        Me.btnSelectTea.Text = "Select"
        '
        'txtTea
        '
        Me.txtTea.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtTea.CharacterCasing = CharacterCasing.Normal
        Me.txtTea.EnterMoveNextControl = True
        Me.txtTea.Location = New System.Drawing.Point(64, 155)
        Me.txtTea.MaxLength = 0
        Me.txtTea.Name = "txtTea"
        Me.txtTea.NumericAllowNegatives = False
        Me.txtTea.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtTea.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTea.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtTea.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtTea.Properties.Appearance.Options.UseFont = True
        Me.txtTea.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTea.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtTea.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTea.Size = New System.Drawing.Size(110, 20)
        Me.txtTea.TabIndex = 21
        Me.txtTea.Tag = "R"
        Me.txtTea.TextAlign = HorizontalAlignment.Left
        Me.txtTea.ToolTipText = ""
        '
        'Label5
        '
        Me.Label5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label5.Location = New System.Drawing.Point(10, 158)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(19, 15)
        Me.Label5.TabIndex = 20
        Me.Label5.Text = "Tea"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnClearLunch
        '
        Me.btnClearLunch.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnClearLunch.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClearLunch.Appearance.Options.UseFont = True
        Me.btnClearLunch.Location = New System.Drawing.Point(233, 71)
        Me.btnClearLunch.Name = "btnClearLunch"
        Me.btnClearLunch.Size = New System.Drawing.Size(50, 22)
        Me.btnClearLunch.TabIndex = 11
        Me.btnClearLunch.Tag = "E"
        Me.btnClearLunch.Text = "Clear"
        '
        'btnSelectLunch
        '
        Me.btnSelectLunch.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnSelectLunch.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSelectLunch.Appearance.Options.UseFont = True
        Me.btnSelectLunch.Location = New System.Drawing.Point(180, 71)
        Me.btnSelectLunch.Name = "btnSelectLunch"
        Me.btnSelectLunch.Size = New System.Drawing.Size(50, 22)
        Me.btnSelectLunch.TabIndex = 10
        Me.btnSelectLunch.Tag = "E"
        Me.btnSelectLunch.Text = "Select"
        '
        'txtLunch
        '
        Me.txtLunch.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtLunch.CharacterCasing = CharacterCasing.Normal
        Me.txtLunch.EnterMoveNextControl = True
        Me.txtLunch.Location = New System.Drawing.Point(64, 71)
        Me.txtLunch.MaxLength = 0
        Me.txtLunch.Name = "txtLunch"
        Me.txtLunch.NumericAllowNegatives = False
        Me.txtLunch.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtLunch.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLunch.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtLunch.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtLunch.Properties.Appearance.Options.UseFont = True
        Me.txtLunch.Properties.Appearance.Options.UseTextOptions = True
        Me.txtLunch.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtLunch.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtLunch.Size = New System.Drawing.Size(110, 20)
        Me.txtLunch.TabIndex = 9
        Me.txtLunch.Tag = "R"
        Me.txtLunch.TextAlign = HorizontalAlignment.Left
        Me.txtLunch.ToolTipText = ""
        '
        'Label4
        '
        Me.Label4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label4.Location = New System.Drawing.Point(10, 75)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(33, 15)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Lunch"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnClearSnack
        '
        Me.btnClearSnack.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnClearSnack.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClearSnack.Appearance.Options.UseFont = True
        Me.btnClearSnack.Location = New System.Drawing.Point(233, 43)
        Me.btnClearSnack.Name = "btnClearSnack"
        Me.btnClearSnack.Size = New System.Drawing.Size(50, 22)
        Me.btnClearSnack.TabIndex = 7
        Me.btnClearSnack.Tag = "E"
        Me.btnClearSnack.Text = "Clear"
        '
        'btnSelectSnack
        '
        Me.btnSelectSnack.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnSelectSnack.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSelectSnack.Appearance.Options.UseFont = True
        Me.btnSelectSnack.Location = New System.Drawing.Point(180, 43)
        Me.btnSelectSnack.Name = "btnSelectSnack"
        Me.btnSelectSnack.Size = New System.Drawing.Size(50, 22)
        Me.btnSelectSnack.TabIndex = 6
        Me.btnSelectSnack.Tag = "E"
        Me.btnSelectSnack.Text = "Select"
        '
        'txtSnack
        '
        Me.txtSnack.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtSnack.CharacterCasing = CharacterCasing.Normal
        Me.txtSnack.EnterMoveNextControl = True
        Me.txtSnack.Location = New System.Drawing.Point(64, 43)
        Me.txtSnack.MaxLength = 0
        Me.txtSnack.Name = "txtSnack"
        Me.txtSnack.NumericAllowNegatives = False
        Me.txtSnack.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSnack.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSnack.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSnack.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSnack.Properties.Appearance.Options.UseFont = True
        Me.txtSnack.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSnack.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSnack.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSnack.Size = New System.Drawing.Size(110, 20)
        Me.txtSnack.TabIndex = 5
        Me.txtSnack.Tag = "R"
        Me.txtSnack.TextAlign = HorizontalAlignment.Left
        Me.txtSnack.ToolTipText = ""
        '
        'Label3
        '
        Me.Label3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label3.Location = New System.Drawing.Point(10, 46)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(31, 15)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Snack"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnClearBreakfast
        '
        Me.btnClearBreakfast.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnClearBreakfast.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClearBreakfast.Appearance.Options.UseFont = True
        Me.btnClearBreakfast.Location = New System.Drawing.Point(233, 15)
        Me.btnClearBreakfast.Name = "btnClearBreakfast"
        Me.btnClearBreakfast.Size = New System.Drawing.Size(50, 22)
        Me.btnClearBreakfast.TabIndex = 3
        Me.btnClearBreakfast.Tag = "E"
        Me.btnClearBreakfast.Text = "Clear"
        '
        'btnSelectBreakfast
        '
        Me.btnSelectBreakfast.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnSelectBreakfast.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSelectBreakfast.Appearance.Options.UseFont = True
        Me.btnSelectBreakfast.Location = New System.Drawing.Point(180, 15)
        Me.btnSelectBreakfast.Name = "btnSelectBreakfast"
        Me.btnSelectBreakfast.Size = New System.Drawing.Size(50, 22)
        Me.btnSelectBreakfast.TabIndex = 2
        Me.btnSelectBreakfast.Tag = "E"
        Me.btnSelectBreakfast.Text = "Select"
        '
        'txtBreakfast
        '
        Me.txtBreakfast.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtBreakfast.CharacterCasing = CharacterCasing.Normal
        Me.txtBreakfast.EnterMoveNextControl = True
        Me.txtBreakfast.Location = New System.Drawing.Point(64, 15)
        Me.txtBreakfast.MaxLength = 0
        Me.txtBreakfast.Name = "txtBreakfast"
        Me.txtBreakfast.NumericAllowNegatives = False
        Me.txtBreakfast.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtBreakfast.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBreakfast.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtBreakfast.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtBreakfast.Properties.Appearance.Options.UseFont = True
        Me.txtBreakfast.Properties.Appearance.Options.UseTextOptions = True
        Me.txtBreakfast.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtBreakfast.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtBreakfast.Size = New System.Drawing.Size(110, 20)
        Me.txtBreakfast.TabIndex = 1
        Me.txtBreakfast.Tag = "R"
        Me.txtBreakfast.TextAlign = HorizontalAlignment.Left
        Me.txtBreakfast.ToolTipText = ""
        '
        'Label1
        '
        Me.Label1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label1.Location = New System.Drawing.Point(10, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Breakfast"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbxGrid
        '
        Me.tlpMain.SetColumnSpan(Me.gbxGrid, 2)
        Me.gbxGrid.Controls.Add(Me.grdGroups)
        Me.gbxGrid.Dock = DockStyle.Fill
        Me.gbxGrid.Location = New System.Drawing.Point(3, 225)
        Me.gbxGrid.Name = "gbxGrid"
        Me.tlpMain.SetRowSpan(Me.gbxGrid, 2)
        Me.gbxGrid.Size = New System.Drawing.Size(292, 216)
        Me.gbxGrid.TabIndex = 2
        Me.gbxGrid.Text = "Select a Group to amend today's activities"
        '
        'grdGroups
        '
        Me.grdGroups.AllowBuildColumns = True
        Me.grdGroups.AllowEdit = False
        Me.grdGroups.AllowHorizontalScroll = False
        Me.grdGroups.AllowMultiSelect = False
        Me.grdGroups.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.grdGroups.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdGroups.Appearance.Options.UseFont = True
        Me.grdGroups.AutoSizeByData = True
        Me.grdGroups.DisableAutoSize = False
        Me.grdGroups.DisableDataFormatting = False
        Me.grdGroups.FocusedRowHandle = -2147483648
        Me.grdGroups.HideFirstColumn = False
        Me.grdGroups.Location = New System.Drawing.Point(8, 28)
        Me.grdGroups.Name = "grdGroups"
        Me.grdGroups.PreviewColumn = ""
        Me.grdGroups.QueryID = Nothing
        Me.grdGroups.RowAutoHeight = False
        Me.grdGroups.SearchAsYouType = True
        Me.grdGroups.ShowAutoFilterRow = False
        Me.grdGroups.ShowFindPanel = False
        Me.grdGroups.ShowGroupByBox = False
        Me.grdGroups.ShowLoadingPanel = False
        Me.grdGroups.ShowNavigator = False
        Me.grdGroups.Size = New System.Drawing.Size(275, 179)
        Me.grdGroups.TabIndex = 0
        '
        'dshStarters
        '
        Me.dshStarters.Colour = Care.Controls.DashboardLabel.EnumColourCode.Neutral
        Me.dshStarters.Dock = DockStyle.Fill
        Me.dshStarters.LabelBottom = "This Week"
        Me.dshStarters.LabelMiddle = "55"
        Me.dshStarters.LabelTop = "Starters"
        Me.dshStarters.Location = New System.Drawing.Point(3, 447)
        Me.dshStarters.Name = "dshStarters"
        Me.dshStarters.Size = New System.Drawing.Size(143, 105)
        Me.dshStarters.TabIndex = 4
        '
        'frmToday
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(914, 692)
        Me.Controls.Add(Me.panTop)
        Me.Controls.Add(Me.panMain)
        Me.FormBorderStyle = FormBorderStyle.Sizable
        Me.LoadMaximised = True
        Me.MaximizeBox = True
        Me.MinimumSize = New System.Drawing.Size(930, 730)
        Me.Name = "frmToday"
        Me.WindowState = FormWindowState.Maximized
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.panMain, 0)
        Me.Controls.SetChildIndex(Me.panTop, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.panTop, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panTop.ResumeLayout(False)
        Me.panTop.PerformLayout()
        CType(Me.txtSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.panMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panMain.ResumeLayout(False)
        Me.tlpMain.ResumeLayout(False)
        CType(Me.gbxActivity, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxActivity.ResumeLayout(False)
        CType(Me.txtActivity.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxMessage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxMessage.ResumeLayout(False)
        CType(Me.txtNarr.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxTop, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxTop.ResumeLayout(False)
        Me.gbxTop.PerformLayout()
        CType(Me.txtSnackPM.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTeaDessert.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLunchDessert.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTea.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLunch.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSnack.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBreakfast.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxGrid.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents timRefresh As Timer
    Friend WithEvents btnDayPatterns As Care.Controls.CareButton
    Friend WithEvents panTop As DevExpress.XtraEditors.PanelControl
    Friend WithEvents panMain As DevExpress.XtraEditors.PanelControl
    Friend WithEvents tlpMain As TableLayoutPanel
    Friend WithEvents dshAbsences As Care.Controls.DashboardLabel
    Friend WithEvents dshViewings As Care.Controls.DashboardLabel
    Friend WithEvents dshBirthdays As Care.Controls.DashboardLabel
    Friend WithEvents dshLeavers As Care.Controls.DashboardLabel
    Friend WithEvents dshAccidents As Care.Controls.DashboardLabel
    Friend WithEvents dshStaffIn As Care.Controls.DashboardLabel
    Friend WithEvents dshChildrenIn As Care.Controls.DashboardLabel
    Friend WithEvents dshChildrenExpected As Care.Controls.DashboardLabel
    Friend WithEvents gbxActivity As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtActivity As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents gbxMessage As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtNarr As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents gbxTop As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnClearSnackPM As Care.Controls.CareButton
    Friend WithEvents btnSelectSnackPM As Care.Controls.CareButton
    Friend WithEvents txtSnackPM As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents btnClearTeaDessert As Care.Controls.CareButton
    Friend WithEvents btnSelectTeaDessert As Care.Controls.CareButton
    Friend WithEvents txtTeaDessert As Care.Controls.CareTextBox
    Friend WithEvents Label8 As Care.Controls.CareLabel
    Friend WithEvents btnClearLunchDessert As Care.Controls.CareButton
    Friend WithEvents btnSelectLunchDessert As Care.Controls.CareButton
    Friend WithEvents txtLunchDessert As Care.Controls.CareTextBox
    Friend WithEvents Label7 As Care.Controls.CareLabel
    Friend WithEvents btnClearTea As Care.Controls.CareButton
    Friend WithEvents btnSelectTea As Care.Controls.CareButton
    Friend WithEvents txtTea As Care.Controls.CareTextBox
    Friend WithEvents Label5 As Care.Controls.CareLabel
    Friend WithEvents btnClearLunch As Care.Controls.CareButton
    Friend WithEvents btnSelectLunch As Care.Controls.CareButton
    Friend WithEvents txtLunch As Care.Controls.CareTextBox
    Friend WithEvents Label4 As Care.Controls.CareLabel
    Friend WithEvents btnClearSnack As Care.Controls.CareButton
    Friend WithEvents btnSelectSnack As Care.Controls.CareButton
    Friend WithEvents txtSnack As Care.Controls.CareTextBox
    Friend WithEvents Label3 As Care.Controls.CareLabel
    Friend WithEvents Label2 As Care.Controls.CareLabel
    Friend WithEvents txtDate As Care.Controls.CareTextBox
    Friend WithEvents btnClearBreakfast As Care.Controls.CareButton
    Friend WithEvents btnSelectBreakfast As Care.Controls.CareButton
    Friend WithEvents txtBreakfast As Care.Controls.CareTextBox
    Friend WithEvents Label1 As Care.Controls.CareLabel
    Friend WithEvents gbxGrid As DevExpress.XtraEditors.GroupControl
    Friend WithEvents grdGroups As Care.Controls.CareGrid
    Friend WithEvents dshStarters As Care.Controls.DashboardLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents txtSite As Care.Controls.CareTextBox
    Friend WithEvents dshPrior As Care.Controls.DashboardLabel
    Friend WithEvents dshStaffExpected As Care.Controls.DashboardLabel
    Friend WithEvents dshPayments As Care.Controls.DashboardLabel
    Friend WithEvents dshTasks As Care.Controls.DashboardLabel
    Friend WithEvents dshChecklists As Care.Controls.DashboardLabel
End Class
