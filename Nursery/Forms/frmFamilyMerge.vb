﻿Imports Care.Global
Imports Care.Data

Public Class frmFamilyMerge

    Private Enum EnumControlSet
        Source
        Target
    End Enum

    Private m_SourceFamilyID As Guid? = Nothing
    Private m_TargetFamilyID As Guid? = Nothing

    Public Sub New(ByVal FamilyID As Guid)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_SourceFamilyID = FamilyID

    End Sub

    Private Sub frmFamilyMerge_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DisplayFamily(EnumControlSet.Source, m_SourceFamilyID)
    End Sub

    Private Sub DisplayFamily(ByVal ControlSet As EnumControlSet, ByVal FamilyID As Guid?)

        Dim _SQL As String = ""
        Dim _ChildSQL As String = ""
        Dim _ContactSQL As String = ""
        Dim _LetterName As String = ""
        Dim _FinancialsLinked As String = ""
        Dim _Surname As String = ""

        _SQL = "select letter_name, surname, excl_financials, financials_id from Family where ID = '" + FamilyID.Value.ToString + "'"
        Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)

        If _DR IsNot Nothing Then

            _LetterName = _DR.Item("letter_name").ToString
            _Surname = _DR.Item("surname").ToString

            If ValueHandler.ConvertBoolean(_DR.Item("excl_financials")) Then
                _FinancialsLinked = "No"
            Else
                If _DR.Item("financials_id").ToString <> "" Then
                    _FinancialsLinked = "Yes"
                Else
                    _FinancialsLinked = "No"
                End If
            End If

        End If

        _ChildSQL = "select ID, fullname as 'Name', dob as 'DOB' from Children where family_id = '" + FamilyID.Value.ToString + "'"

        _ContactSQL += "select ID, fullname as 'Name', relationship as 'Relationship',"
        _ContactSQL += " CAST(CASE WHEN (len(rtrim(tel_home)) > 0) THEN 1 ELSE 0 END AS BIT) as 'Home Tel',"
        _ContactSQL += " CAST(CASE WHEN (len(rtrim(tel_mobile)) > 0) THEN 1 ELSE 0 END AS BIT) as 'Mobile Tel',"
        _ContactSQL += " CAST(CASE WHEN (len(rtrim(email)) > 0) THEN 1 ELSE 0 END AS BIT) as 'Email'"
        _ContactSQL += " from Contacts where family_id = '" + FamilyID.Value.ToString + "'"

        If ControlSet = EnumControlSet.Source Then

            txtSourceAddressee.Text = _LetterName
            txtSourceSurname.Text = _Surname
            txtSourceFinancials.Text = _FinancialsLinked

            grdSourceChildren.HideFirstColumn = True
            grdSourceChildren.Populate(Session.ConnectionString, _ChildSQL)

            grdSourceContacts.HideFirstColumn = True
            grdSourceContacts.Populate(Session.ConnectionString, _ContactSQL)

        Else

            txtTargetAddressee.Text = _LetterName
            txtTargetSurname.Text = _Surname
            txtTargetFinancials.Text = _FinancialsLinked

            grdTargetChildren.HideFirstColumn = True
            grdTargetChildren.Populate(Session.ConnectionString, _ChildSQL)

            grdTargetContacts.HideFirstColumn = True
            grdTargetContacts.Populate(Session.ConnectionString, _ContactSQL)

        End If

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnMerge_Click(sender As Object, e As EventArgs) Handles btnMerge.Click

        If Not m_TargetFamilyID.HasValue Then
            CareMessage("Please select a Target Family.", MessageBoxIcon.Exclamation, "Merge Family")
            Exit Sub
        End If

        Dim _Response As String = InputBox("Please type MERGE to continue.", "Merge Customer").ToUpper

        If _Response = "MERGE" OrElse _Response = "MERGE!" Then

            Dim _SkipCheck As Boolean = False
            If _Response.EndsWith("!") Then _SkipCheck = True

            If _SkipCheck Then
                If CareMessage("Are you sure you want to force a Merge? This will merge any invoices that have been generated or posted.", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Force Merge") = DialogResult.No Then
                    Exit Sub
                End If
            Else
                If Not ChecksOK() Then Exit Sub
            End If

            DoMerge()

            CareMessage("Merge Completed Successfully. Click OK to open the Merged Family record.", MessageBoxIcon.Information, "Merge Family")

            Me.DialogResult = DialogResult.OK
            Me.Tag = m_TargetFamilyID.Value.ToString
            Me.Close()

        End If

    End Sub

    Private Function ChecksOK() As Boolean

        'check family has not had invoices posted
        Dim _SQL As String = ""
        _SQL += "select COUNT(*) from Invoices"
        _SQL += " where family_id = '" + m_SourceFamilyID.Value.ToString + "'"
        _SQL += " and len(financials_doc_id) > 0 and len(financials_id) > 0"

        If ValueHandler.ConvertInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL)) > 0 Then
            CareMessage("Merge Cancelled. The source family has invoices posted in your accounting system.", MessageBoxIcon.Exclamation, "Merge Family")
            Return False
        End If

        'warn if family has invoices generated in open batches
        _SQL = ""
        _SQL += "select COUNT(*) from Invoices i"
        _SQL += " left join InvoiceBatch b on b.ID = i.batch_id"
        _SQL += " where i.family_id = '" + m_SourceFamilyID.Value.ToString + "'"
        _SQL += " and b.batch_status = 'Open'"
        _SQL += " and b.batch_period not in ('Annual Invoice', 'Annual Recalculation')"

        If ValueHandler.ConvertInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL)) > 0 Then
            CareMessage("Merge Cancelled. The source family has invoices present in unposted batches.", MessageBoxIcon.Exclamation, "Merge Family")
            Return False
        End If

        'warn if family has financials linked
        If txtSourceFinancials.Text = "Yes" Then
            CareMessage("The source family has been linked to customer in your accounting system. Please check the customer is linked correctly following the Merge.", MessageBoxIcon.Exclamation, "Merge Family")
            Return False
        End If

        Return True

    End Function

    Private Sub DoMerge()

        Dim _SQL As String = ""

        If radChildrenCopy.Checked Then
            _SQL = ""
            _SQL = "update Children set family_id = '" + m_TargetFamilyID.Value.ToString + "'"
            _SQL += ", family_name = '" + txtTargetSurname.Text + "'"
            _SQL += " where family_id = '" + m_SourceFamilyID.Value.ToString + "'"
        Else
            _SQL = ""
            _SQL = "delete from Children"
            _SQL += " where family_id = '" + m_SourceFamilyID.Value.ToString + "'"
        End If

        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        '********************************************************************************************************************

        If radContactCopy.Checked Then
            _SQL = ""
            _SQL = "update Contacts set family_id = '" + m_TargetFamilyID.Value.ToString + "'"
            _SQL += " where family_id = '" + m_SourceFamilyID.Value.ToString + "'"
        Else
            _SQL = ""
            _SQL = "delete from Contacts"
            _SQL += " where family_id = '" + m_SourceFamilyID.Value.ToString + "'"
        End If

        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        '********************************************************************************************************************

        _SQL = ""
        _SQL = "update Invoices set family_id = '" + m_TargetFamilyID.Value.ToString + "'"
        _SQL += " where family_id = '" + m_SourceFamilyID.Value.ToString + "'"

        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        '********************************************************************************************************************

        _SQL = "delete from Family"
        _SQL += " where ID = '" + m_SourceFamilyID.Value.ToString + "'"

        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        '********************************************************************************************************************

    End Sub

    Private Sub btnFind_Click(sender As Object, e As EventArgs) Handles btnFind.Click

        Dim _ID As String = Business.Family.FindFamily(Me, True)
        If _ID = "" Then Exit Sub

        If m_SourceFamilyID = New Guid(_ID) Then
            CareMessage("You cannot select the same Target and Source Families.", MessageBoxIcon.Exclamation, "Find Target Family")
            Exit Sub
        End If

        m_TargetFamilyID = New Guid(_ID)
        DisplayFamily(EnumControlSet.Target, m_TargetFamilyID)

    End Sub

    Private Sub grdSourceChildren_GridDoubleClick(sender As Object, e As EventArgs) Handles grdSourceChildren.GridDoubleClick
        If grdSourceChildren.RecordCount < 1 Then Exit Sub
        If grdSourceChildren.CurrentRow(0).ToString <> "" Then
            Dim _ID As New Guid(grdSourceChildren.CurrentRow(0).ToString)
            Business.Child.DrillDown(_ID)
        End If
    End Sub

    Private Sub grdSourceContacts_GridDoubleClick(sender As Object, e As EventArgs) Handles grdSourceContacts.GridDoubleClick
        If grdSourceContacts.RecordCount < 1 Then Exit Sub
        If grdSourceContacts.CurrentRow(0).ToString <> "" Then
            Dim _ID As New Guid(grdSourceContacts.CurrentRow(0).ToString)
            Business.Contact.ViewContact(_ID)
        End If
    End Sub

    Private Sub grdTargetChildren_GridDoubleClick(sender As Object, e As EventArgs) Handles grdTargetChildren.GridDoubleClick
        If grdTargetChildren.RecordCount < 1 Then Exit Sub
        If grdTargetChildren.CurrentRow(0).ToString <> "" Then
            Dim _ID As New Guid(grdTargetChildren.CurrentRow(0).ToString)
            Business.Child.DrillDown(_ID)
        End If
    End Sub

    Private Sub grdTargetContacts_GridDoubleClick(sender As Object, e As EventArgs) Handles grdTargetContacts.GridDoubleClick
        If grdTargetContacts.RecordCount < 1 Then Exit Sub
        If grdTargetContacts.CurrentRow(0).ToString <> "" Then
            Dim _ID As New Guid(grdTargetContacts.CurrentRow(0).ToString)
            Business.Contact.ViewContact(_ID)
        End If
    End Sub
End Class
