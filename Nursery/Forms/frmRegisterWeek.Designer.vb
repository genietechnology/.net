﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRegisterWeek
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.cgRegister = New Care.Controls.CareGrid()
        Me.gbxHistorical = New Care.Controls.CareFrame(Me.components)
        Me.Label13 = New Care.Controls.CareLabel(Me.components)
        Me.cdtWC = New Care.Controls.CareDateTime(Me.components)
        Me.btnRefresh = New Care.Controls.CareButton(Me.components)
        Me.btnHoursByWeek = New Care.Controls.CareButton(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.cbxSite = New Care.Controls.CareComboBox(Me.components)
        CType(Me.gbxHistorical, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxHistorical.SuspendLayout()
        CType(Me.cdtWC.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtWC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'cgRegister
        '
        Me.cgRegister.AllowBuildColumns = True
        Me.cgRegister.AllowEdit = False
        Me.cgRegister.AllowHorizontalScroll = False
        Me.cgRegister.AllowMultiSelect = False
        Me.cgRegister.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgRegister.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgRegister.Appearance.Options.UseFont = True
        Me.cgRegister.AutoSizeByData = True
        Me.cgRegister.DisableAutoSize = False
        Me.cgRegister.DisableDataFormatting = False
        Me.cgRegister.FocusedRowHandle = -2147483648
        Me.cgRegister.HideFirstColumn = False
        Me.cgRegister.Location = New System.Drawing.Point(12, 56)
        Me.cgRegister.Name = "cgRegister"
        Me.cgRegister.PreviewColumn = ""
        Me.cgRegister.QueryID = Nothing
        Me.cgRegister.RowAutoHeight = False
        Me.cgRegister.SearchAsYouType = False
        Me.cgRegister.ShowAutoFilterRow = False
        Me.cgRegister.ShowFindPanel = False
        Me.cgRegister.ShowGroupByBox = False
        Me.cgRegister.ShowLoadingPanel = False
        Me.cgRegister.ShowNavigator = False
        Me.cgRegister.Size = New System.Drawing.Size(861, 493)
        Me.cgRegister.TabIndex = 1
        '
        'gbxHistorical
        '
        Me.gbxHistorical.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.gbxHistorical.Controls.Add(Me.CareLabel3)
        Me.gbxHistorical.Controls.Add(Me.cbxSite)
        Me.gbxHistorical.Controls.Add(Me.btnHoursByWeek)
        Me.gbxHistorical.Controls.Add(Me.Label13)
        Me.gbxHistorical.Controls.Add(Me.cdtWC)
        Me.gbxHistorical.Controls.Add(Me.btnRefresh)
        Me.gbxHistorical.Location = New System.Drawing.Point(12, 12)
        Me.gbxHistorical.Name = "gbxHistorical"
        Me.gbxHistorical.ShowCaption = False
        Me.gbxHistorical.Size = New System.Drawing.Size(861, 38)
        Me.gbxHistorical.TabIndex = 0
        '
        'Label13
        '
        Me.Label13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label13.Location = New System.Drawing.Point(12, 12)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(132, 15)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "Week Commencing Date"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtWC
        '
        Me.cdtWC.EditValue = Nothing
        Me.cdtWC.EnterMoveNextControl = True
        Me.cdtWC.Location = New System.Drawing.Point(150, 9)
        Me.cdtWC.Name = "cdtWC"
        Me.cdtWC.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtWC.Properties.Appearance.Options.UseFont = True
        Me.cdtWC.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtWC.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtWC.Size = New System.Drawing.Size(97, 22)
        Me.cdtWC.TabIndex = 1
        Me.cdtWC.Value = Nothing
        '
        'btnRefresh
        '
        Me.btnRefresh.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefresh.Appearance.Options.UseFont = True
        Me.btnRefresh.Location = New System.Drawing.Point(520, 8)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(80, 22)
        Me.btnRefresh.TabIndex = 4
        Me.btnRefresh.Tag = ""
        Me.btnRefresh.Text = "Refresh"
        '
        'btnHoursByWeek
        '
        Me.btnHoursByWeek.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHoursByWeek.Appearance.Options.UseFont = True
        Me.btnHoursByWeek.Location = New System.Drawing.Point(606, 8)
        Me.btnHoursByWeek.Name = "btnHoursByWeek"
        Me.btnHoursByWeek.Size = New System.Drawing.Size(142, 22)
        Me.btnHoursByWeek.TabIndex = 5
        Me.btnHoursByWeek.Tag = ""
        Me.btnHoursByWeek.Text = "Print Hours by Week"
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(263, 12)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(19, 15)
        Me.CareLabel3.TabIndex = 2
        Me.CareLabel3.Text = "Site"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(288, 9)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AccessibleName = "Group"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(226, 22)
        Me.cbxSite.TabIndex = 3
        Me.cbxSite.Tag = ""
        Me.cbxSite.ValueMember = Nothing
        '
        'frmRegisterWeek
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(884, 561)
        Me.Controls.Add(Me.cgRegister)
        Me.Controls.Add(Me.gbxHistorical)
        Me.LoadMaximised = True
        Me.Name = "frmRegisterWeek"
        Me.Text = "frmRegisterWeek"
        Me.WindowState = FormWindowState.Maximized
        CType(Me.gbxHistorical, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxHistorical.ResumeLayout(False)
        Me.gbxHistorical.PerformLayout()
        CType(Me.cdtWC.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtWC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents cgRegister As Care.Controls.CareGrid
    Friend WithEvents gbxHistorical As Care.Controls.CareFrame
    Friend WithEvents cdtWC As Care.Controls.CareDateTime
    Friend WithEvents btnRefresh As Care.Controls.CareButton
    Friend WithEvents Label13 As Care.Controls.CareLabel
    Friend WithEvents btnHoursByWeek As Care.Controls.CareButton
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
End Class
