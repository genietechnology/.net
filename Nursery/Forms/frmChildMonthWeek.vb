﻿Imports Care.Global

Public Class frmChildMonthWeek

    Private m_ChildID As Guid
    Private m_ID As Guid?
    Private m_Frequency As EnumFrequency
    Private m_IsNew As Boolean = True

    Public Enum EnumFrequency
        Monthly
        Weekly
    End Enum

    Public Sub New(ByVal ChildID As Guid, ByVal RecordID As Guid?, ByVal Frequency As EnumFrequency)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_ChildID = ChildID

        If RecordID.HasValue Then
            m_IsNew = False
            m_ID = RecordID
        Else
            m_IsNew = True
            m_ID = Nothing
        End If

        m_Frequency = Frequency

    End Sub

    Private Sub frmChildHoliday_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cbxDays.AddItem("1 day")
        cbxDays.AddItem("2 days")
        cbxDays.AddItem("3 days")
        cbxDays.AddItem("4 days")
        cbxDays.AddItem("5 days")
        cbxDays.AddItem("6 days")
        cbxDays.AddItem("7 days")

        cbxMode.AddItem("Use DOB")
        cbxMode.AddItem("Specific Band")

        Dim _SQL As String = "select id, description from TariffAgeBands order by min_age"
        cbxAgeBand.PopulateWithSQL(Session.ConnectionString, _SQL)

        Dim _C As Business.Child = Business.Child.RetreiveByID(m_ChildID)
        If _C IsNot Nothing Then
            _SQL = "select id, name from Tariffs where site_id = '" + _C._SiteId.ToString + "' and type = 'Month Matrix' and archived = 0 order by name"
            cbxTariff.PopulateWithSQL(Session.ConnectionString, _SQL)
        End If

        cdtStartDate.BackColor = Session.ChangeColour
        cbxDays.BackColor = Session.ChangeColour
        cbxTariff.BackColor = Session.ChangeColour
        cbxMode.BackColor = Session.ChangeColour

        If m_ID.HasValue Then
            Me.Text = "Edit " + m_Frequency.ToString + " Override"
            DisplayRecord()
        Else
            Me.Text = "Create New " + m_Frequency.ToString + " Override"
            cbxMode.SelectedIndex = 0
        End If

    End Sub

    Private Sub DisplayRecord()

        Dim _H As Business.ChildWeekMonth = Business.ChildWeekMonth.RetreiveByID(m_ID.Value)
        With _H

            cdtStartDate.Value = ._DateFrom
            cbxDays.SelectedIndex = ._DaysPerWeek - 1

            cbxMode.Text = ._AgeMode
            cbxTariff.SelectedValue = ._TariffId

            If cbxMode.SelectedIndex = 1 Then
                cbxAgeBand.SelectedValue = ._AgeId
            End If

        End With

        _H = Nothing

    End Sub

    Private Function ValidateCharge() As Boolean

        If cdtStartDate.Text = "" Then Return False
        If cdtStartDate.Value Is Nothing Then Return False
        If Not cdtStartDate.Value.HasValue Then Return False

        If cbxMode.SelectedIndex < 0 Then Return False
        If cbxTariff.SelectedIndex < 0 Then Return False
        If cbxDays.SelectedIndex < 0 Then Return False

        If cbxMode.SelectedIndex = 1 AndAlso cbxAgeBand.SelectedIndex < 0 Then Return False

        Return True

    End Function

    Private Sub SaveAndExit()

        If Not ValidateCharge() Then Exit Sub

        Dim _WM As Business.ChildWeekMonth = Nothing

        If m_IsNew Then
            _WM = New Business.ChildWeekMonth
            _WM._ID = Guid.NewGuid
            _WM._ChildId = m_ChildID
            _WM._Frequency = m_Frequency.ToString
        Else
            _WM = Business.ChildWeekMonth.RetreiveByID(m_ID.Value)
        End If

        With _WM

            ._DateFrom = cdtStartDate.Value
            ._DaysPerWeek = cbxDays.SelectedIndex + 1
            ._TariffId = New Guid(cbxTariff.SelectedValue.ToString)
            ._AgeMode = cbxMode.Text

            If cbxMode.SelectedIndex = 0 Then
                ._AgeId = Nothing
            Else
                ._AgeId = New Guid(cbxAgeBand.SelectedValue.ToString)
            End If

            .Store()

        End With

        _WM = Nothing

        Me.DialogResult = DialogResult.OK
        Me.Close()

    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        SaveAndExit()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub cbxMode_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxMode.SelectedIndexChanged
        If cbxMode.SelectedIndex = 0 Then
            cbxAgeBand.SelectedIndex = -1
            cbxAgeBand.ResetBackColor()
            cbxAgeBand.ReadOnly = True
        Else
            cbxAgeBand.BackColor = Session.ChangeColour
            cbxAgeBand.ReadOnly = False
        End If
    End Sub
End Class
