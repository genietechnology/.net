﻿

Imports Care.Global
Imports Care.Data
Imports System.Windows.Forms

Public Class frmAnnualCalcs

    Private Sub frmAnnualCalculations_Load(sender As Object, e As EventArgs) Handles Me.Load

        With cbxSite
            .PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)
            .SelectedIndex = 0
        End With

        gbxNew.Visible = False

        cbxGeneration.AddItem("Children with existing Annual Calculations")
        cbxGeneration.AddItem("All Current Children")
        cbxGeneration.SelectedIndex = 0

    End Sub

    Private Sub DisplayGrid()

        Dim _SQL As String = ""

        _SQL += "select a.id, a.annual_invoice, a.child_id, c.fullname, c.group_name, date_from, date_to, date_invoiced, date_due,"
        _SQL += " annual_total, split_type, split_total"
        _SQL += " from ChildAnnual a"
        _SQL += " left join Children c on c.ID = a.child_id"
        _SQL += " where c.status = 'Current'"
        _SQL += " and a.active = 1"

        _SQL += " and c.site_name = '" + cbxSite.Text + "'"

        _SQL += " order by c.surname_forename"

        cgAnnualInvoices.Populate(Session.ConnectionString, _SQL)

        cgAnnualInvoices.Columns("id").Visible = False
        cgAnnualInvoices.Columns("annual_invoice").Visible = False
        cgAnnualInvoices.Columns("child_id").Visible = False
        cgAnnualInvoices.Columns("fullname").Caption = "Name"
        cgAnnualInvoices.Columns("group_name").Caption = "Room"
        cgAnnualInvoices.Columns("date_from").Caption = "Date From"
        cgAnnualInvoices.Columns("date_to").Caption = "Date To"
        cgAnnualInvoices.Columns("date_invoiced").Caption = "Invoice Date"
        cgAnnualInvoices.Columns("date_due").Caption = "Invoice Due"
        cgAnnualInvoices.Columns("annual_total").Caption = "Annual Value"
        cgAnnualInvoices.Columns("split_type").Caption = "Split"
        cgAnnualInvoices.Columns("split_total").Caption = "Split Value"

    End Sub

    Private Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        gbxNew.Show()
        cdtDateFrom.Focus()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        gbxNew.Hide()
    End Sub

    Private Sub Run(ByVal ChildrenToProcess As List(Of Business.Child),
                    ByVal FromDate As Date, ByVal ToDate As Date,
                    ByVal InvoiceDate As Date, ByVal DueDate As Date, ByVal SplitMonths As Integer)

        Session.SetupProgressBar("Creating Annual Calculations", ChildrenToProcess.Count)

        For Each _C In ChildrenToProcess

            If _C._Dob.HasValue Then

                Session.StepProgressBar("Processing " + _C._Fullname)

                Dim _LastDate As Date = ToDate
                Dim _SplitMonths As Integer = SplitMonths

                'check if the child is due to leave
                If _C._DateLeft.HasValue Then
                    If _C._DateLeft < _LastDate Then

                        'get the month before the leaving date
                        Dim _LastMonthDate As Date = DateAdd(DateInterval.Month, -1, _C._DateLeft.Value)

                        'work out the last day of that month
                        _LastDate = DateSerial(_LastMonthDate.Year, _LastMonthDate.Month, DateTime.DaysInMonth(_LastMonthDate.Year, _LastMonthDate.Month))

                        'work out the number of months from the start date
                        _SplitMonths = CInt(DateDiff(DateInterval.Month, FromDate, _LastDate)) + 1

                    End If
                End If

                'split months must be at least 1 (might be leaving in first month, therefore 0)
                If _SplitMonths > 0 Then
                    'create calculation using april to leave date
                    AnnualCalculation(_C._ID.Value, InvoiceDate, DueDate, FromDate, _LastDate, _SplitMonths)
                End If

            Else
                'blank dob
            End If

        Next

        Session.SetProgressMessage("Process Complete", 5)

    End Sub

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click

        If ValidateEntry() Then

            Session.CursorWaiting()
            SetCMDs(False)
            Application.DoEvents()

            Dim _Children As List(Of Business.Child) = Nothing

            If cbxGeneration.Text = "Children with existing Annual Calculations" Then

                _Children = New List(Of Business.Child)

                Dim _AllChildren As List(Of Business.Child) = Business.Child.RetrieveLiveChildrenBySite(New Guid(cbxSite.SelectedValue.ToString))
                For Each _C In _AllChildren
                    If HasActiveAnnualCalculation(_C._ID.ToString) Then
                        _Children.Add(_C)
                    End If
                Next

            Else
                _Children = Business.Child.RetrieveLiveChildrenBySite(New Guid(cbxSite.SelectedValue.ToString))
            End If

            Dim _Months As Integer = ValueHandler.ConvertInteger(txtMonths.Text)

            If _Children IsNot Nothing AndAlso _Children.Count > 0 Then
                Run(_Children, cdtDateFrom.Value.Value, cdtDateTo.Value.Value, cdtInvoiceDate.Value.Value, cdtInvoiceDue.Value.Value, _Months)
            End If

            DisplayGrid()

            Session.CursorDefault()
            SetCMDs(True)
            Application.DoEvents()

            gbxNew.Hide()

        End If

    End Sub

    Private Sub SetCMDs(ByVal Enabled As Boolean)
        btnRun.Enabled = Enabled
        btnCancel.Enabled = Enabled
        btnNew.Enabled = Enabled
        btnCalc.Enabled = Enabled
        btnInvoice.Enabled = Enabled
        btnSend.Enabled = Enabled
    End Sub

    Private Function HasActiveAnnualCalculation(ByVal ChildID As String) As Boolean

        Dim _SQL As String = "select count(*) from ChildAnnual where child_id = '" + ChildID + "' and active = 1"

        Dim _Count As Integer = ValueHandler.ConvertInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))
        If _Count > 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Sub AnnualCalculation(ByVal ChildID As Guid, ByVal InvoiceDate As Date, ByVal InvoiceDue As Date,
                                  ByVal FromDate As Date, ByVal ToDate As Date, ByVal SplitMonths As Integer)

        Dim _A As New Business.ChildAnnual
        With _A

            ._ID = Guid.NewGuid
            ._ChildId = ChildID

            Dim _BatchNo As Integer = Invoicing.LastBatchNo + 1
            Dim _InvoiceNo As Integer = Invoicing.LastInvoiceNo + 1
            Dim _SplitType As String = SplitMonths.ToString + " months"
            If SplitMonths = 1 Then _SplitType = "1 month"

            If .Calculate(_A._ID.Value, _BatchNo, _InvoiceNo, InvoiceDate, InvoiceDue, FromDate, ToDate, _SplitType, SplitMonths) Then
                _A.CreateBatchHeader(_A._ID.Value, _BatchNo, "Annual Invoice", InvoiceDate, FromDate, ToDate, _InvoiceNo, _InvoiceNo)
            End If

        End With

    End Sub

    Private Function ValidateEntry() As Boolean

        If Not cdtDateFrom.Value.HasValue Then Return False
        If cdtDateFrom.Value = Date.MinValue Then Return False

        If Not cdtDateTo.Value.HasValue Then Return False
        If cdtDateTo.Value = Date.MinValue Then Return False

        If Not cdtInvoiceDate.Value.HasValue Then Return False
        If cdtInvoiceDate.Value = Date.MinValue Then Return False

        If Not cdtInvoiceDue.Value.HasValue Then Return False
        If cdtInvoiceDue.Value = Date.MinValue Then Return False

        If cdtDateFrom.Value > cdtDateTo.Value Then Return False

        If txtMonths.Text = "" Then Return False

        Return True

    End Function

    Private Sub CalculateSplit()

        txtMonths.Text = ""

        If cdtDateFrom.Value.HasValue AndAlso cdtDateTo.Value.HasValue Then
            Dim _Months As Long = DateDiff(DateInterval.Month, cdtDateFrom.Value.Value, cdtDateTo.Value.Value) + 1
            txtMonths.Text = _Months.ToString
        End If

    End Sub

    Private Sub cdtDateFrom_Validated(sender As Object, e As EventArgs) Handles cdtDateFrom.Validated
        CalculateSplit()
    End Sub

    Private Sub cdtDateTo_Validated(sender As Object, e As EventArgs) Handles cdtDateTo.Validated
        CalculateSplit()
    End Sub

    Private Sub cbxSite_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSite.SelectedIndexChanged
        DisplayGrid()
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnInvoice_Click(sender As Object, e As EventArgs) Handles btnInvoice.Click
        ViewInvoice()
    End Sub

    Private Function GetID() As Guid?
        If cgAnnualInvoices Is Nothing Then Return Nothing
        If cgAnnualInvoices.RecordCount < 1 Then Return Nothing
        If cgAnnualInvoices.CurrentRow("id").ToString = "" Then Return Nothing
        Return New Guid(cgAnnualInvoices.CurrentRow("id").ToString)
    End Function

    Private Function GetChildID() As Guid?
        If cgAnnualInvoices Is Nothing Then Return Nothing
        If cgAnnualInvoices.RecordCount < 1 Then Return Nothing
        If cgAnnualInvoices.CurrentRow("child_id").ToString = "" Then Return Nothing
        Return New Guid(cgAnnualInvoices.CurrentRow("child_id").ToString)
    End Function

    Private Function GetInvoiceID() As Guid?
        If cgAnnualInvoices Is Nothing Then Return Nothing
        If cgAnnualInvoices.RecordCount < 1 Then Return Nothing
        If cgAnnualInvoices.CurrentRow("annual_invoice").ToString = "" Then Return Nothing
        Return New Guid(cgAnnualInvoices.CurrentRow("annual_invoice").ToString)
    End Function

    Private Sub ViewInvoice()
        Dim _ID As Guid? = GetInvoiceID()
        If _ID.HasValue Then
            Invoicing.ViewInvoice(_ID.Value)
        End If
    End Sub

    Private Sub cgAnnualInvoices_GridDoubleClick(sender As Object, e As EventArgs) Handles cgAnnualInvoices.GridDoubleClick
        ViewInvoice()
    End Sub

    Private Sub btnCalc_Click(sender As Object, e As EventArgs) Handles btnCalc.Click

        Dim _ChildID As Guid? = GetChildID()
        Dim _CalculationID As Guid? = GetID()

        If _ChildID.HasValue AndAlso _CalculationID.HasValue Then
            Dim _frm As New frmChildAnnual(_ChildID.Value, _CalculationID.Value)
            _frm.ShowDialog()
            If _frm.DialogResult = DialogResult.OK Then
                DisplayGrid()
            End If
        End If

    End Sub

    Private Sub btnSend_Click(sender As Object, e As EventArgs) Handles btnSend.Click

        Dim _SelectedRows As Integer() = cgAnnualInvoices.UnderlyingGridView.GetSelectedRows()
        If _SelectedRows.Length > 0 AndAlso _SelectedRows.Length >= 1 Then

            Dim _Invoices As New List(Of Guid)

            For Each _RowIndex As Integer In _SelectedRows
                Dim _DR As DataRow = cgAnnualInvoices.GetRow(_RowIndex)
                If _DR IsNot Nothing Then
                    Dim _InvoiceID As Guid = New Guid(_DR.Item("annual_invoice").ToString)
                    _Invoices.Add(_InvoiceID)
                End If
            Next

            Invoicing.SendInvoices(_Invoices, False)

        End If

    End Sub

End Class
