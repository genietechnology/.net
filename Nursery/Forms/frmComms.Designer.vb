﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmComms
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmComms))
        Me.GroupControl2 = New Care.Controls.CareFrame()
        Me.cbxSite = New Care.Controls.CareComboBox()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.cbxType = New Care.Controls.CareComboBox()
        Me.GroupBox12 = New Care.Controls.CareFrame()
        Me.cbxContact = New Care.Controls.CareComboBox()
        Me.btnField = New DevExpress.XtraEditors.DropDownButton()
        Me.popFields = New DevExpress.XtraBars.PopupMenu()
        Me.mnuContactForename = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuContactFullname = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuChildForename = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuChildKnownAs = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuChildFullname = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuHisHer = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuHeShe = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuStaffForename = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuStaffFullname = New DevExpress.XtraBars.BarButtonItem()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.mnuAccountNo = New DevExpress.XtraBars.BarButtonItem()
        Me.gbxMerge = New Care.Controls.CareFrame()
        Me.gbxAttach = New Care.Controls.CareFrame()
        Me.tokAttachments = New DevExpress.XtraEditors.TokenEdit()
        Me.btnAttach = New Care.Controls.CareButton()
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.GroupControl5 = New Care.Controls.CareFrame()
        Me.grdRecipients = New Care.Controls.CareGrid()
        Me.btnClose = New Care.Controls.CareButton()
        Me.txtBody = New Care.Controls.CareRichText()
        Me.gbxSubject = New Care.Controls.CareFrame()
        Me.txtBCC = New Care.[Shared].CareEmailAddress()
        Me.txtCC = New Care.[Shared].CareEmailAddress()
        Me.CareLabel3 = New Care.Controls.CareLabel()
        Me.CareLabel2 = New Care.Controls.CareLabel()
        Me.cbxSender = New Care.Controls.CareComboBox()
        Me.CareLabel1 = New Care.Controls.CareLabel()
        Me.Label16 = New Care.Controls.CareLabel()
        Me.txtSubject = New Care.Controls.CareTextBox()
        Me.btnSend = New Care.Controls.CareButton()
        Me.GroupControl8 = New Care.Controls.CareFrame()
        Me.cbxTariff = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.btnCancel = New Care.Controls.CareButton()
        Me.GroupControl10 = New Care.Controls.CareFrame()
        Me.cbxRooms = New Care.Controls.CareComboBox()
        Me.GroupControl4 = New Care.Controls.CareFrame()
        Me.cbxBoltOns = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.cbxBookingRange = New Care.Controls.CareFrame()
        Me.cbxDates = New Care.Controls.CareComboBox()
        Me.cdtFrom = New Care.Controls.CareDateTime()
        Me.cdtTo = New Care.Controls.CareDateTime()
        Me.btnRefresh = New Care.Controls.CareButton()
        Me.GroupControl12 = New Care.Controls.CareFrame()
        Me.cbxTimeSlots = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.btnSendTest = New Care.Controls.CareButton()
        Me.lblSMS = New Care.Controls.CareLabel()
        Me.CareFrame1 = New Care.Controls.CareFrame()
        Me.cbxMarketing = New Care.Controls.CareComboBox()
        Me.CareFrame2 = New Care.Controls.CareFrame()
        Me.cbxPersonalised = New Care.Controls.CareComboBox()
        Me.CareFrame3 = New Care.Controls.CareFrame()
        Me.txtRef2 = New Care.Controls.CareTextBox()
        Me.txtRef1 = New Care.Controls.CareTextBox()
        Me.CareFrame4 = New Care.Controls.CareFrame()
        Me.cbxTags = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.CareFrame6 = New Care.Controls.CareFrame()
        Me.cbxSchool = New Care.Controls.CareComboBox()
        Me.CareFrame5 = New Care.Controls.CareFrame()
        Me.cbxDuplicates = New Care.Controls.CareComboBox()
        Me.btnSelectAll = New Care.Controls.CareButton()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.cbxType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox12.SuspendLayout()
        CType(Me.cbxContact.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.popFields, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxMerge, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxMerge.SuspendLayout()
        CType(Me.gbxAttach, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxAttach.SuspendLayout()
        CType(Me.tokAttachments.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.gbxSubject, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxSubject.SuspendLayout()
        CType(Me.cbxSender.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSubject.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl8.SuspendLayout()
        CType(Me.cbxTariff.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl10.SuspendLayout()
        CType(Me.cbxRooms.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.cbxBoltOns.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxBookingRange, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cbxBookingRange.SuspendLayout()
        CType(Me.cbxDates.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl12.SuspendLayout()
        CType(Me.cbxTimeSlots.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CareFrame1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CareFrame1.SuspendLayout()
        CType(Me.cbxMarketing.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CareFrame2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CareFrame2.SuspendLayout()
        CType(Me.cbxPersonalised.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CareFrame3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CareFrame3.SuspendLayout()
        CType(Me.txtRef2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRef1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CareFrame4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CareFrame4.SuspendLayout()
        CType(Me.cbxTags.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CareFrame6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CareFrame6.SuspendLayout()
        CType(Me.cbxSchool.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CareFrame5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CareFrame5.SuspendLayout()
        CType(Me.cbxDuplicates.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl2
        '
        Me.GroupControl2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl2.Appearance.Options.UseFont = True
        Me.GroupControl2.Controls.Add(Me.cbxSite)
        Me.GroupControl2.Location = New System.Drawing.Point(170, 8)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(180, 54)
        Me.GroupControl2.TabIndex = 1
        Me.GroupControl2.Text = "Site"
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(5, 26)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AccessibleName = "Gender"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(170, 20)
        Me.cbxSite.TabIndex = 0
        Me.cbxSite.Tag = ""
        Me.cbxSite.ValueMember = Nothing
        '
        'GroupControl1
        '
        Me.GroupControl1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl1.Appearance.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.cbxType)
        Me.GroupControl1.Location = New System.Drawing.Point(8, 68)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(156, 54)
        Me.GroupControl1.TabIndex = 4
        Me.GroupControl1.Text = "Type"
        '
        'cbxType
        '
        Me.cbxType.AllowBlank = False
        Me.cbxType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxType.DataSource = Nothing
        Me.cbxType.DisplayMember = Nothing
        Me.cbxType.EnterMoveNextControl = True
        Me.cbxType.Location = New System.Drawing.Point(5, 26)
        Me.cbxType.Name = "cbxType"
        Me.cbxType.Properties.AccessibleName = "Gender"
        Me.cbxType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxType.Properties.Appearance.Options.UseFont = True
        Me.cbxType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxType.SelectedValue = Nothing
        Me.cbxType.Size = New System.Drawing.Size(146, 20)
        Me.cbxType.TabIndex = 0
        Me.cbxType.Tag = ""
        Me.cbxType.ValueMember = Nothing
        '
        'GroupBox12
        '
        Me.GroupBox12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupBox12.Appearance.Options.UseFont = True
        Me.GroupBox12.Controls.Add(Me.cbxContact)
        Me.GroupBox12.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.Size = New System.Drawing.Size(156, 54)
        Me.GroupBox12.TabIndex = 0
        Me.GroupBox12.Text = "Contact Selection"
        '
        'cbxContact
        '
        Me.cbxContact.AllowBlank = False
        Me.cbxContact.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxContact.DataSource = Nothing
        Me.cbxContact.DisplayMember = Nothing
        Me.cbxContact.EnterMoveNextControl = True
        Me.cbxContact.Location = New System.Drawing.Point(5, 26)
        Me.cbxContact.Name = "cbxContact"
        Me.cbxContact.Properties.AccessibleName = "Gender"
        Me.cbxContact.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxContact.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxContact.Properties.Appearance.Options.UseFont = True
        Me.cbxContact.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxContact.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxContact.SelectedValue = Nothing
        Me.cbxContact.Size = New System.Drawing.Size(146, 20)
        Me.cbxContact.TabIndex = 0
        Me.cbxContact.Tag = ""
        Me.cbxContact.ValueMember = Nothing
        '
        'btnField
        '
        Me.btnField.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnField.DropDownControl = Me.popFields
        Me.btnField.Location = New System.Drawing.Point(6, 25)
        Me.btnField.Name = "btnField"
        Me.btnField.Size = New System.Drawing.Size(169, 23)
        Me.btnField.TabIndex = 0
        Me.btnField.Text = "Add Field"
        '
        'popFields
        '
        Me.popFields.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuContactForename), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuContactFullname), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuChildForename), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuChildKnownAs), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuChildFullname), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHisHer), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHeShe), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuStaffForename), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuStaffFullname)})
        Me.popFields.Manager = Me.BarManager1
        Me.popFields.Name = "popFields"
        '
        'mnuContactForename
        '
        Me.mnuContactForename.Caption = "Contact Forename"
        Me.mnuContactForename.Id = 0
        Me.mnuContactForename.Name = "mnuContactForename"
        '
        'mnuContactFullname
        '
        Me.mnuContactFullname.Caption = "Contact Fullname"
        Me.mnuContactFullname.Id = 1
        Me.mnuContactFullname.Name = "mnuContactFullname"
        '
        'mnuChildForename
        '
        Me.mnuChildForename.Caption = "Child Forename"
        Me.mnuChildForename.Id = 3
        Me.mnuChildForename.Name = "mnuChildForename"
        '
        'mnuChildKnownAs
        '
        Me.mnuChildKnownAs.Caption = "Child Known As"
        Me.mnuChildKnownAs.Id = 7
        Me.mnuChildKnownAs.Name = "mnuChildKnownAs"
        '
        'mnuChildFullname
        '
        Me.mnuChildFullname.Caption = "Child Fullname"
        Me.mnuChildFullname.Id = 4
        Me.mnuChildFullname.Name = "mnuChildFullname"
        '
        'mnuHisHer
        '
        Me.mnuHisHer.Caption = "his/her"
        Me.mnuHisHer.Id = 5
        Me.mnuHisHer.Name = "mnuHisHer"
        '
        'mnuHeShe
        '
        Me.mnuHeShe.Caption = "he/she"
        Me.mnuHeShe.Id = 6
        Me.mnuHeShe.Name = "mnuHeShe"
        '
        'mnuStaffForename
        '
        Me.mnuStaffForename.Caption = "Staff Forename"
        Me.mnuStaffForename.Id = 8
        Me.mnuStaffForename.Name = "mnuStaffForename"
        '
        'mnuStaffFullname
        '
        Me.mnuStaffFullname.Caption = "Staff Fullname"
        Me.mnuStaffFullname.Id = 9
        Me.mnuStaffFullname.Name = "mnuStaffFullname"
        '
        'BarManager1
        '
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.mnuContactForename, Me.mnuContactFullname, Me.mnuAccountNo, Me.mnuChildForename, Me.mnuChildFullname, Me.mnuHisHer, Me.mnuHeShe, Me.mnuChildKnownAs, Me.mnuStaffForename, Me.mnuStaffFullname})
        Me.BarManager1.MaxItemId = 10
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Manager = Me.BarManager1
        Me.barDockControlTop.Size = New System.Drawing.Size(1009, 0)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 661)
        Me.barDockControlBottom.Manager = Me.BarManager1
        Me.barDockControlBottom.Size = New System.Drawing.Size(1009, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlLeft.Manager = Me.BarManager1
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 661)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(1009, 0)
        Me.barDockControlRight.Manager = Me.BarManager1
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 661)
        '
        'mnuAccountNo
        '
        Me.mnuAccountNo.Caption = "Account Number"
        Me.mnuAccountNo.Id = 2
        Me.mnuAccountNo.Name = "mnuAccountNo"
        '
        'gbxMerge
        '
        Me.gbxMerge.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.gbxMerge.Appearance.Options.UseFont = True
        Me.gbxMerge.Controls.Add(Me.btnField)
        Me.gbxMerge.Location = New System.Drawing.Point(819, 128)
        Me.gbxMerge.Name = "gbxMerge"
        Me.gbxMerge.Size = New System.Drawing.Size(180, 54)
        Me.gbxMerge.TabIndex = 15
        Me.gbxMerge.Text = "Merge Fields"
        '
        'gbxAttach
        '
        Me.gbxAttach.Controls.Add(Me.tokAttachments)
        Me.gbxAttach.Controls.Add(Me.btnAttach)
        Me.gbxAttach.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.gbxAttach.Location = New System.Drawing.Point(0, 375)
        Me.gbxAttach.Name = "gbxAttach"
        Me.gbxAttach.Size = New System.Drawing.Size(458, 56)
        Me.gbxAttach.TabIndex = 2
        Me.gbxAttach.Text = "Attachments"
        '
        'tokAttachments
        '
        Me.tokAttachments.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tokAttachments.Location = New System.Drawing.Point(5, 27)
        Me.tokAttachments.MenuManager = Me.BarManager1
        Me.tokAttachments.Name = "tokAttachments"
        Me.tokAttachments.Properties.MaxExpandLines = 1
        Me.tokAttachments.Properties.Separators.AddRange(New String() {","})
        Me.tokAttachments.Size = New System.Drawing.Size(360, 20)
        Me.tokAttachments.TabIndex = 0
        '
        'btnAttach
        '
        Me.btnAttach.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAttach.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnAttach.Appearance.Options.UseFont = True
        Me.btnAttach.Location = New System.Drawing.Point(371, 24)
        Me.btnAttach.Name = "btnAttach"
        Me.btnAttach.Size = New System.Drawing.Size(75, 25)
        Me.btnAttach.TabIndex = 1
        Me.btnAttach.Tag = ""
        Me.btnAttach.Text = "Attach"
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1
        Me.SplitContainerControl1.Location = New System.Drawing.Point(8, 188)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl5)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.txtBody)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.gbxSubject)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.gbxAttach)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(992, 431)
        Me.SplitContainerControl1.SplitterPosition = 529
        Me.SplitContainerControl1.TabIndex = 16
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'GroupControl5
        '
        Me.GroupControl5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl5.Appearance.Options.UseFont = True
        Me.GroupControl5.Controls.Add(Me.grdRecipients)
        Me.GroupControl5.Controls.Add(Me.btnClose)
        Me.GroupControl5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl5.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(529, 431)
        Me.GroupControl5.TabIndex = 0
        Me.GroupControl5.Text = "Recipients"
        '
        'grdRecipients
        '
        Me.grdRecipients.AllowBuildColumns = True
        Me.grdRecipients.AllowEdit = True
        Me.grdRecipients.AllowHorizontalScroll = False
        Me.grdRecipients.AllowMultiSelect = False
        Me.grdRecipients.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdRecipients.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdRecipients.Appearance.Options.UseFont = True
        Me.grdRecipients.AutoSizeByData = True
        Me.grdRecipients.DisableAutoSize = False
        Me.grdRecipients.DisableDataFormatting = False
        Me.grdRecipients.FocusedRowHandle = -2147483648
        Me.grdRecipients.HideFirstColumn = False
        Me.grdRecipients.Location = New System.Drawing.Point(5, 24)
        Me.grdRecipients.Name = "grdRecipients"
        Me.grdRecipients.PreviewColumn = ""
        Me.grdRecipients.QueryID = Nothing
        Me.grdRecipients.RowAutoHeight = False
        Me.grdRecipients.SearchAsYouType = True
        Me.grdRecipients.ShowAutoFilterRow = False
        Me.grdRecipients.ShowFindPanel = True
        Me.grdRecipients.ShowGroupByBox = False
        Me.grdRecipients.ShowLoadingPanel = True
        Me.grdRecipients.ShowNavigator = True
        Me.grdRecipients.Size = New System.Drawing.Size(519, 402)
        Me.grdRecipients.TabIndex = 0
        Me.grdRecipients.TabStop = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(454, 406)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 25)
        Me.btnClose.TabIndex = 9
        Me.btnClose.Tag = ""
        Me.btnClose.Text = "Close"
        Me.btnClose.ToolTip = "Performing a reset will take the Search back to the defaults. This includes any c" &
    "olumn movements, as well as the size and location of this form."
        Me.btnClose.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.btnClose.ToolTipTitle = "Reset"
        '
        'txtBody
        '
        Me.txtBody.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBody.HTMLText = resources.GetString("txtBody.HTMLText")
        Me.txtBody.Location = New System.Drawing.Point(0, 135)
        Me.txtBody.Name = "txtBody"
        Me.txtBody.RTFText = resources.GetString("txtBody.RTFText")
        Me.txtBody.Size = New System.Drawing.Size(458, 240)
        Me.txtBody.TabIndex = 1
        '
        'gbxSubject
        '
        Me.gbxSubject.Controls.Add(Me.txtBCC)
        Me.gbxSubject.Controls.Add(Me.txtCC)
        Me.gbxSubject.Controls.Add(Me.CareLabel3)
        Me.gbxSubject.Controls.Add(Me.CareLabel2)
        Me.gbxSubject.Controls.Add(Me.cbxSender)
        Me.gbxSubject.Controls.Add(Me.CareLabel1)
        Me.gbxSubject.Controls.Add(Me.Label16)
        Me.gbxSubject.Controls.Add(Me.txtSubject)
        Me.gbxSubject.Dock = System.Windows.Forms.DockStyle.Top
        Me.gbxSubject.Location = New System.Drawing.Point(0, 0)
        Me.gbxSubject.Name = "gbxSubject"
        Me.gbxSubject.Size = New System.Drawing.Size(458, 135)
        Me.gbxSubject.TabIndex = 0
        Me.gbxSubject.Text = "Sender and Subject"
        '
        'txtBCC
        '
        Me.txtBCC.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtBCC.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBCC.Appearance.Options.UseFont = True
        Me.txtBCC.Location = New System.Drawing.Point(53, 106)
        Me.txtBCC.MaxLength = 0
        Me.txtBCC.Name = "txtBCC"
        Me.txtBCC.NoButton = True
        Me.txtBCC.NoColours = False
        Me.txtBCC.NoValidate = True
        Me.txtBCC.ReadOnly = False
        Me.txtBCC.Size = New System.Drawing.Size(400, 22)
        Me.txtBCC.TabIndex = 7
        '
        'txtCC
        '
        Me.txtCC.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCC.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCC.Appearance.Options.UseFont = True
        Me.txtCC.Location = New System.Drawing.Point(53, 78)
        Me.txtCC.MaxLength = 0
        Me.txtCC.Name = "txtCC"
        Me.txtCC.NoButton = True
        Me.txtCC.NoColours = False
        Me.txtCC.NoValidate = True
        Me.txtCC.ReadOnly = False
        Me.txtCC.Size = New System.Drawing.Size(400, 22)
        Me.txtCC.TabIndex = 5
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.Options.UseFont = True
        Me.CareLabel3.Appearance.Options.UseTextOptions = True
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(8, 109)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(23, 15)
        Me.CareLabel3.TabIndex = 6
        Me.CareLabel3.Text = "BCC"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.Options.UseFont = True
        Me.CareLabel2.Appearance.Options.UseTextOptions = True
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(8, 81)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(16, 15)
        Me.CareLabel2.TabIndex = 4
        Me.CareLabel2.Text = "CC"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxSender
        '
        Me.cbxSender.AllowBlank = False
        Me.cbxSender.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxSender.DataSource = Nothing
        Me.cbxSender.DisplayMember = Nothing
        Me.cbxSender.EnterMoveNextControl = True
        Me.cbxSender.Location = New System.Drawing.Point(53, 25)
        Me.cbxSender.Name = "cbxSender"
        Me.cbxSender.Properties.AccessibleName = "Gender"
        Me.cbxSender.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSender.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSender.Properties.Appearance.Options.UseFont = True
        Me.cbxSender.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSender.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSender.SelectedValue = Nothing
        Me.cbxSender.Size = New System.Drawing.Size(400, 20)
        Me.cbxSender.TabIndex = 1
        Me.cbxSender.Tag = ""
        Me.cbxSender.ValueMember = Nothing
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.Options.UseFont = True
        Me.CareLabel1.Appearance.Options.UseTextOptions = True
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(8, 28)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(36, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Sender"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label16
        '
        Me.Label16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label16.Appearance.Options.UseFont = True
        Me.Label16.Appearance.Options.UseTextOptions = True
        Me.Label16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label16.Location = New System.Drawing.Point(8, 55)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(39, 15)
        Me.Label16.TabIndex = 2
        Me.Label16.Text = "Subject"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSubject
        '
        Me.txtSubject.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSubject.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtSubject.EnterMoveNextControl = True
        Me.txtSubject.Location = New System.Drawing.Point(53, 52)
        Me.txtSubject.MaxLength = 0
        Me.txtSubject.Name = "txtSubject"
        Me.txtSubject.NumericAllowNegatives = False
        Me.txtSubject.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSubject.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSubject.Properties.AccessibleName = "Contact Forename"
        Me.txtSubject.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSubject.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSubject.Properties.Appearance.Options.UseFont = True
        Me.txtSubject.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSubject.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSubject.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSubject.Size = New System.Drawing.Size(400, 20)
        Me.txtSubject.TabIndex = 3
        Me.txtSubject.Tag = ""
        Me.txtSubject.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSubject.ToolTipText = ""
        '
        'btnSend
        '
        Me.btnSend.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSend.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSend.Appearance.Options.UseFont = True
        Me.btnSend.Location = New System.Drawing.Point(109, 628)
        Me.btnSend.Name = "btnSend"
        Me.btnSend.Size = New System.Drawing.Size(95, 25)
        Me.btnSend.TabIndex = 18
        Me.btnSend.Tag = ""
        Me.btnSend.Text = "Send Now"
        '
        'GroupControl8
        '
        Me.GroupControl8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl8.Appearance.Options.UseFont = True
        Me.GroupControl8.Controls.Add(Me.cbxTariff)
        Me.GroupControl8.Location = New System.Drawing.Point(542, 68)
        Me.GroupControl8.Name = "GroupControl8"
        Me.GroupControl8.Size = New System.Drawing.Size(271, 54)
        Me.GroupControl8.TabIndex = 7
        Me.GroupControl8.Text = "Tariff"
        '
        'cbxTariff
        '
        Me.cbxTariff.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxTariff.Location = New System.Drawing.Point(5, 27)
        Me.cbxTariff.Name = "cbxTariff"
        Me.cbxTariff.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxTariff.Size = New System.Drawing.Size(261, 20)
        Me.cbxTariff.TabIndex = 0
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(905, 628)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(95, 25)
        Me.btnCancel.TabIndex = 0
        Me.btnCancel.Tag = ""
        Me.btnCancel.Text = "Close"
        '
        'GroupControl10
        '
        Me.GroupControl10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl10.Appearance.Options.UseFont = True
        Me.GroupControl10.Controls.Add(Me.cbxRooms)
        Me.GroupControl10.Location = New System.Drawing.Point(356, 8)
        Me.GroupControl10.Name = "GroupControl10"
        Me.GroupControl10.Size = New System.Drawing.Size(180, 54)
        Me.GroupControl10.TabIndex = 2
        Me.GroupControl10.Text = "Room"
        '
        'cbxRooms
        '
        Me.cbxRooms.AllowBlank = False
        Me.cbxRooms.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxRooms.DataSource = Nothing
        Me.cbxRooms.DisplayMember = Nothing
        Me.cbxRooms.EnterMoveNextControl = True
        Me.cbxRooms.Location = New System.Drawing.Point(5, 26)
        Me.cbxRooms.Name = "cbxRooms"
        Me.cbxRooms.Properties.AccessibleName = "Gender"
        Me.cbxRooms.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxRooms.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxRooms.Properties.Appearance.Options.UseFont = True
        Me.cbxRooms.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxRooms.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxRooms.SelectedValue = Nothing
        Me.cbxRooms.Size = New System.Drawing.Size(170, 20)
        Me.cbxRooms.TabIndex = 0
        Me.cbxRooms.Tag = ""
        Me.cbxRooms.ValueMember = Nothing
        '
        'GroupControl4
        '
        Me.GroupControl4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl4.Appearance.Options.UseFont = True
        Me.GroupControl4.Controls.Add(Me.cbxBoltOns)
        Me.GroupControl4.Location = New System.Drawing.Point(819, 68)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(180, 54)
        Me.GroupControl4.TabIndex = 8
        Me.GroupControl4.Text = "Bolt-Ons"
        '
        'cbxBoltOns
        '
        Me.cbxBoltOns.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxBoltOns.Location = New System.Drawing.Point(5, 27)
        Me.cbxBoltOns.Name = "cbxBoltOns"
        Me.cbxBoltOns.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxBoltOns.Size = New System.Drawing.Size(170, 20)
        Me.cbxBoltOns.TabIndex = 0
        '
        'cbxBookingRange
        '
        Me.cbxBookingRange.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.cbxBookingRange.Appearance.Options.UseFont = True
        Me.cbxBookingRange.Controls.Add(Me.cbxDates)
        Me.cbxBookingRange.Controls.Add(Me.cdtFrom)
        Me.cbxBookingRange.Controls.Add(Me.cdtTo)
        Me.cbxBookingRange.Controls.Add(Me.btnRefresh)
        Me.cbxBookingRange.Location = New System.Drawing.Point(542, 8)
        Me.cbxBookingRange.Name = "cbxBookingRange"
        Me.cbxBookingRange.Size = New System.Drawing.Size(457, 54)
        Me.cbxBookingRange.TabIndex = 3
        Me.cbxBookingRange.Text = "Booking Date Range"
        '
        'cbxDates
        '
        Me.cbxDates.AllowBlank = False
        Me.cbxDates.DataSource = Nothing
        Me.cbxDates.DisplayMember = Nothing
        Me.cbxDates.EnterMoveNextControl = True
        Me.cbxDates.Location = New System.Drawing.Point(5, 25)
        Me.cbxDates.Name = "cbxDates"
        Me.cbxDates.Properties.AccessibleName = "Gender"
        Me.cbxDates.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxDates.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxDates.Properties.Appearance.Options.UseFont = True
        Me.cbxDates.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxDates.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxDates.SelectedValue = Nothing
        Me.cbxDates.Size = New System.Drawing.Size(209, 22)
        Me.cbxDates.TabIndex = 0
        Me.cbxDates.Tag = ""
        Me.cbxDates.ValueMember = Nothing
        '
        'cdtFrom
        '
        Me.cdtFrom.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtFrom.EnterMoveNextControl = True
        Me.cdtFrom.Location = New System.Drawing.Point(220, 25)
        Me.cdtFrom.Name = "cdtFrom"
        Me.cdtFrom.Properties.AccessibleName = "Date of Birth"
        Me.cdtFrom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtFrom.Properties.Appearance.Options.UseFont = True
        Me.cdtFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtFrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtFrom.Size = New System.Drawing.Size(85, 22)
        Me.cdtFrom.TabIndex = 1
        Me.cdtFrom.Tag = ""
        Me.cdtFrom.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'cdtTo
        '
        Me.cdtTo.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtTo.EnterMoveNextControl = True
        Me.cdtTo.Location = New System.Drawing.Point(311, 25)
        Me.cdtTo.Name = "cdtTo"
        Me.cdtTo.Properties.AccessibleName = "Date of Birth"
        Me.cdtTo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtTo.Properties.Appearance.Options.UseFont = True
        Me.cdtTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtTo.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtTo.Size = New System.Drawing.Size(85, 22)
        Me.cdtTo.TabIndex = 2
        Me.cdtTo.Tag = ""
        Me.cdtTo.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'btnRefresh
        '
        Me.btnRefresh.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRefresh.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnRefresh.Appearance.Options.UseFont = True
        Me.btnRefresh.Location = New System.Drawing.Point(402, 23)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(50, 25)
        Me.btnRefresh.TabIndex = 3
        Me.btnRefresh.Tag = ""
        Me.btnRefresh.Text = "Refresh"
        '
        'GroupControl12
        '
        Me.GroupControl12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl12.Appearance.Options.UseFont = True
        Me.GroupControl12.Controls.Add(Me.cbxTimeSlots)
        Me.GroupControl12.Location = New System.Drawing.Point(356, 68)
        Me.GroupControl12.Name = "GroupControl12"
        Me.GroupControl12.Size = New System.Drawing.Size(180, 54)
        Me.GroupControl12.TabIndex = 6
        Me.GroupControl12.Text = "Time Slot"
        '
        'cbxTimeSlots
        '
        Me.cbxTimeSlots.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxTimeSlots.Location = New System.Drawing.Point(5, 27)
        Me.cbxTimeSlots.Name = "cbxTimeSlots"
        Me.cbxTimeSlots.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxTimeSlots.Size = New System.Drawing.Size(170, 20)
        Me.cbxTimeSlots.TabIndex = 0
        '
        'btnSendTest
        '
        Me.btnSendTest.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSendTest.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSendTest.Appearance.Options.UseFont = True
        Me.btnSendTest.Location = New System.Drawing.Point(210, 628)
        Me.btnSendTest.Name = "btnSendTest"
        Me.btnSendTest.Size = New System.Drawing.Size(95, 25)
        Me.btnSendTest.TabIndex = 19
        Me.btnSendTest.Tag = ""
        Me.btnSendTest.Text = "Send Test"
        Me.btnSendTest.ToolTip = resources.GetString("btnSendTest.ToolTip")
        Me.btnSendTest.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.btnSendTest.ToolTipTitle = "Send Test"
        '
        'lblSMS
        '
        Me.lblSMS.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSMS.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblSMS.Appearance.Options.UseFont = True
        Me.lblSMS.Appearance.Options.UseTextOptions = True
        Me.lblSMS.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblSMS.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblSMS.Location = New System.Drawing.Point(735, 633)
        Me.lblSMS.Name = "lblSMS"
        Me.lblSMS.Size = New System.Drawing.Size(152, 15)
        Me.lblSMS.TabIndex = 20
        Me.lblSMS.Text = "SMS Count"
        Me.lblSMS.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CareFrame1
        '
        Me.CareFrame1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.CareFrame1.Appearance.Options.UseFont = True
        Me.CareFrame1.Controls.Add(Me.cbxMarketing)
        Me.CareFrame1.Location = New System.Drawing.Point(542, 128)
        Me.CareFrame1.Name = "CareFrame1"
        Me.CareFrame1.Size = New System.Drawing.Size(88, 54)
        Me.CareFrame1.TabIndex = 12
        Me.CareFrame1.Text = "Marketing?"
        '
        'cbxMarketing
        '
        Me.cbxMarketing.AllowBlank = False
        Me.cbxMarketing.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxMarketing.DataSource = Nothing
        Me.cbxMarketing.DisplayMember = Nothing
        Me.cbxMarketing.EnterMoveNextControl = True
        Me.cbxMarketing.Location = New System.Drawing.Point(5, 24)
        Me.cbxMarketing.Name = "cbxMarketing"
        Me.cbxMarketing.Properties.AccessibleName = "Gender"
        Me.cbxMarketing.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxMarketing.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxMarketing.Properties.Appearance.Options.UseFont = True
        Me.cbxMarketing.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxMarketing.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxMarketing.SelectedValue = Nothing
        Me.cbxMarketing.Size = New System.Drawing.Size(78, 20)
        Me.cbxMarketing.TabIndex = 0
        Me.cbxMarketing.Tag = ""
        Me.cbxMarketing.ToolTip = resources.GetString("cbxMarketing.ToolTip")
        Me.cbxMarketing.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.cbxMarketing.ToolTipTitle = "Marketing?"
        Me.cbxMarketing.ValueMember = Nothing
        '
        'CareFrame2
        '
        Me.CareFrame2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.CareFrame2.Appearance.Options.UseFont = True
        Me.CareFrame2.Controls.Add(Me.cbxPersonalised)
        Me.CareFrame2.Location = New System.Drawing.Point(636, 128)
        Me.CareFrame2.Name = "CareFrame2"
        Me.CareFrame2.Size = New System.Drawing.Size(88, 54)
        Me.CareFrame2.TabIndex = 13
        Me.CareFrame2.Text = "Personalised?"
        '
        'cbxPersonalised
        '
        Me.cbxPersonalised.AllowBlank = False
        Me.cbxPersonalised.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxPersonalised.DataSource = Nothing
        Me.cbxPersonalised.DisplayMember = Nothing
        Me.cbxPersonalised.EnterMoveNextControl = True
        Me.cbxPersonalised.Location = New System.Drawing.Point(5, 24)
        Me.cbxPersonalised.Name = "cbxPersonalised"
        Me.cbxPersonalised.Properties.AccessibleName = "Gender"
        Me.cbxPersonalised.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxPersonalised.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxPersonalised.Properties.Appearance.Options.UseFont = True
        Me.cbxPersonalised.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxPersonalised.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxPersonalised.SelectedValue = Nothing
        Me.cbxPersonalised.Size = New System.Drawing.Size(78, 20)
        Me.cbxPersonalised.TabIndex = 0
        Me.cbxPersonalised.Tag = ""
        Me.cbxPersonalised.ToolTip = resources.GetString("cbxPersonalised.ToolTip")
        Me.cbxPersonalised.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.cbxPersonalised.ToolTipTitle = "Personalised?"
        Me.cbxPersonalised.ValueMember = Nothing
        '
        'CareFrame3
        '
        Me.CareFrame3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.CareFrame3.Appearance.Options.UseFont = True
        Me.CareFrame3.Controls.Add(Me.txtRef2)
        Me.CareFrame3.Controls.Add(Me.txtRef1)
        Me.CareFrame3.Location = New System.Drawing.Point(170, 128)
        Me.CareFrame3.Name = "CareFrame3"
        Me.CareFrame3.Size = New System.Drawing.Size(366, 54)
        Me.CareFrame3.TabIndex = 11
        Me.CareFrame3.Text = "References"
        '
        'txtRef2
        '
        Me.txtRef2.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtRef2.EnterMoveNextControl = True
        Me.txtRef2.Location = New System.Drawing.Point(191, 24)
        Me.txtRef2.MaxLength = 0
        Me.txtRef2.Name = "txtRef2"
        Me.txtRef2.NumericAllowNegatives = False
        Me.txtRef2.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRef2.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRef2.Properties.AccessibleName = "Contact Forename"
        Me.txtRef2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRef2.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRef2.Properties.Appearance.Options.UseFont = True
        Me.txtRef2.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRef2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRef2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRef2.Size = New System.Drawing.Size(170, 22)
        Me.txtRef2.TabIndex = 5
        Me.txtRef2.Tag = ""
        Me.txtRef2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRef2.ToolTipText = ""
        '
        'txtRef1
        '
        Me.txtRef1.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtRef1.EnterMoveNextControl = True
        Me.txtRef1.Location = New System.Drawing.Point(5, 24)
        Me.txtRef1.MaxLength = 0
        Me.txtRef1.Name = "txtRef1"
        Me.txtRef1.NumericAllowNegatives = False
        Me.txtRef1.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRef1.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRef1.Properties.AccessibleName = "Contact Forename"
        Me.txtRef1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRef1.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRef1.Properties.Appearance.Options.UseFont = True
        Me.txtRef1.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRef1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRef1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRef1.Size = New System.Drawing.Size(170, 22)
        Me.txtRef1.TabIndex = 4
        Me.txtRef1.Tag = ""
        Me.txtRef1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRef1.ToolTipText = ""
        '
        'CareFrame4
        '
        Me.CareFrame4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.CareFrame4.Appearance.Options.UseFont = True
        Me.CareFrame4.Controls.Add(Me.cbxTags)
        Me.CareFrame4.Location = New System.Drawing.Point(8, 128)
        Me.CareFrame4.Name = "CareFrame4"
        Me.CareFrame4.Size = New System.Drawing.Size(156, 54)
        Me.CareFrame4.TabIndex = 9
        Me.CareFrame4.Text = "Tags"
        '
        'cbxTags
        '
        Me.cbxTags.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxTags.Location = New System.Drawing.Point(5, 25)
        Me.cbxTags.Name = "cbxTags"
        Me.cbxTags.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxTags.Size = New System.Drawing.Size(146, 20)
        Me.cbxTags.TabIndex = 11
        '
        'CareFrame6
        '
        Me.CareFrame6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.CareFrame6.Appearance.Options.UseFont = True
        Me.CareFrame6.Controls.Add(Me.cbxSchool)
        Me.CareFrame6.Location = New System.Drawing.Point(170, 68)
        Me.CareFrame6.Name = "CareFrame6"
        Me.CareFrame6.Size = New System.Drawing.Size(180, 54)
        Me.CareFrame6.TabIndex = 5
        Me.CareFrame6.Text = "School"
        '
        'cbxSchool
        '
        Me.cbxSchool.AllowBlank = False
        Me.cbxSchool.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxSchool.DataSource = Nothing
        Me.cbxSchool.DisplayMember = Nothing
        Me.cbxSchool.EnterMoveNextControl = True
        Me.cbxSchool.Location = New System.Drawing.Point(5, 26)
        Me.cbxSchool.Name = "cbxSchool"
        Me.cbxSchool.Properties.AccessibleName = "Gender"
        Me.cbxSchool.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSchool.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSchool.Properties.Appearance.Options.UseFont = True
        Me.cbxSchool.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSchool.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSchool.SelectedValue = Nothing
        Me.cbxSchool.Size = New System.Drawing.Size(170, 20)
        Me.cbxSchool.TabIndex = 0
        Me.cbxSchool.Tag = ""
        Me.cbxSchool.ValueMember = Nothing
        '
        'CareFrame5
        '
        Me.CareFrame5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.CareFrame5.Appearance.Options.UseFont = True
        Me.CareFrame5.Controls.Add(Me.cbxDuplicates)
        Me.CareFrame5.Location = New System.Drawing.Point(730, 128)
        Me.CareFrame5.Name = "CareFrame5"
        Me.CareFrame5.Size = New System.Drawing.Size(83, 54)
        Me.CareFrame5.TabIndex = 14
        Me.CareFrame5.Text = "Duplicates?"
        '
        'cbxDuplicates
        '
        Me.cbxDuplicates.AllowBlank = False
        Me.cbxDuplicates.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxDuplicates.DataSource = Nothing
        Me.cbxDuplicates.DisplayMember = Nothing
        Me.cbxDuplicates.EnterMoveNextControl = True
        Me.cbxDuplicates.Location = New System.Drawing.Point(5, 24)
        Me.cbxDuplicates.Name = "cbxDuplicates"
        Me.cbxDuplicates.Properties.AccessibleName = "Gender"
        Me.cbxDuplicates.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxDuplicates.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxDuplicates.Properties.Appearance.Options.UseFont = True
        Me.cbxDuplicates.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxDuplicates.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxDuplicates.SelectedValue = Nothing
        Me.cbxDuplicates.Size = New System.Drawing.Size(73, 20)
        Me.cbxDuplicates.TabIndex = 0
        Me.cbxDuplicates.Tag = ""
        Me.cbxDuplicates.ToolTip = "Duplicates are turned off by default." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "We suggest duplicates are only turned on" &
    " if you are sending a personalised message where you want to Merge fields."
        Me.cbxDuplicates.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.cbxDuplicates.ToolTipTitle = "Duplicates?"
        Me.cbxDuplicates.ValueMember = Nothing
        '
        'btnSelectAll
        '
        Me.btnSelectAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSelectAll.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSelectAll.Appearance.Options.UseFont = True
        Me.btnSelectAll.Location = New System.Drawing.Point(8, 628)
        Me.btnSelectAll.Name = "btnSelectAll"
        Me.btnSelectAll.Size = New System.Drawing.Size(95, 25)
        Me.btnSelectAll.TabIndex = 17
        Me.btnSelectAll.Tag = ""
        Me.btnSelectAll.Text = "Select All"
        '
        'frmComms
        '
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(1009, 661)
        Me.Controls.Add(Me.btnSelectAll)
        Me.Controls.Add(Me.CareFrame5)
        Me.Controls.Add(Me.CareFrame3)
        Me.Controls.Add(Me.CareFrame6)
        Me.Controls.Add(Me.CareFrame4)
        Me.Controls.Add(Me.CareFrame2)
        Me.Controls.Add(Me.CareFrame1)
        Me.Controls.Add(Me.lblSMS)
        Me.Controls.Add(Me.btnSendTest)
        Me.Controls.Add(Me.GroupControl12)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.cbxBookingRange)
        Me.Controls.Add(Me.GroupControl10)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.GroupControl8)
        Me.Controls.Add(Me.btnSend)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Controls.Add(Me.gbxMerge)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupBox12)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.MinimumSize = New System.Drawing.Size(1024, 700)
        Me.Name = "frmComms"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.cbxType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox12.ResumeLayout(False)
        CType(Me.cbxContact.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.popFields, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxMerge, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxMerge.ResumeLayout(False)
        CType(Me.gbxAttach, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxAttach.ResumeLayout(False)
        CType(Me.tokAttachments.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        CType(Me.gbxSubject, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxSubject.ResumeLayout(False)
        Me.gbxSubject.PerformLayout()
        CType(Me.cbxSender.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSubject.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl8.ResumeLayout(False)
        CType(Me.cbxTariff.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl10.ResumeLayout(False)
        CType(Me.cbxRooms.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        CType(Me.cbxBoltOns.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxBookingRange, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cbxBookingRange.ResumeLayout(False)
        CType(Me.cbxDates.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl12.ResumeLayout(False)
        CType(Me.cbxTimeSlots.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CareFrame1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CareFrame1.ResumeLayout(False)
        CType(Me.cbxMarketing.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CareFrame2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CareFrame2.ResumeLayout(False)
        CType(Me.cbxPersonalised.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CareFrame3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CareFrame3.ResumeLayout(False)
        CType(Me.txtRef2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRef1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CareFrame4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CareFrame4.ResumeLayout(False)
        CType(Me.cbxTags.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CareFrame6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CareFrame6.ResumeLayout(False)
        CType(Me.cbxSchool.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CareFrame5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CareFrame5.ResumeLayout(False)
        CType(Me.cbxDuplicates.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbxType As Care.Controls.CareComboBox
    Friend WithEvents cbxContact As Care.Controls.CareComboBox
    Friend WithEvents btnField As DevExpress.XtraEditors.DropDownButton
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents grdRecipients As Care.Controls.CareGrid
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents txtBody As Care.Controls.CareRichText
    Friend WithEvents txtSubject As Care.Controls.CareTextBox
    Friend WithEvents btnSend As Care.Controls.CareButton
    Friend WithEvents btnAttach As Care.Controls.CareButton
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents popFields As DevExpress.XtraBars.PopupMenu
    Friend WithEvents mnuContactForename As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuContactFullname As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuAccountNo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuChildForename As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuChildKnownAs As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuChildFullname As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuHisHer As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuHeShe As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
    Friend WithEvents cbxRooms As Care.Controls.CareComboBox
    Friend WithEvents cbxTariff As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents cbxTimeSlots As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents cbxBoltOns As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents btnRefresh As Care.Controls.CareButton
    Friend WithEvents tokAttachments As DevExpress.XtraEditors.TokenEdit
    Private WithEvents cdtFrom As Care.Controls.CareDateTime
    Private WithEvents cdtTo As Care.Controls.CareDateTime
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents Label16 As Care.Controls.CareLabel
    Friend WithEvents cbxSender As Care.Controls.CareComboBox
    Friend WithEvents btnSendTest As Care.Controls.CareButton
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents txtBCC As Care.Shared.CareEmailAddress
    Friend WithEvents txtCC As Care.Shared.CareEmailAddress
    Friend WithEvents lblSMS As Care.Controls.CareLabel
    Friend WithEvents GroupControl2 As Care.Controls.CareFrame
    Friend WithEvents GroupControl1 As Care.Controls.CareFrame
    Friend WithEvents GroupBox12 As Care.Controls.CareFrame
    Friend WithEvents gbxMerge As Care.Controls.CareFrame
    Friend WithEvents gbxAttach As Care.Controls.CareFrame
    Friend WithEvents GroupControl5 As Care.Controls.CareFrame
    Friend WithEvents gbxSubject As Care.Controls.CareFrame
    Friend WithEvents GroupControl8 As Care.Controls.CareFrame
    Friend WithEvents GroupControl10 As Care.Controls.CareFrame
    Friend WithEvents GroupControl4 As Care.Controls.CareFrame
    Friend WithEvents cbxBookingRange As Care.Controls.CareFrame
    Friend WithEvents GroupControl12 As Care.Controls.CareFrame
    Friend WithEvents CareFrame1 As Care.Controls.CareFrame
    Friend WithEvents cbxMarketing As Care.Controls.CareComboBox
    Friend WithEvents CareFrame2 As Care.Controls.CareFrame
    Friend WithEvents cbxPersonalised As Care.Controls.CareComboBox
    Friend WithEvents mnuStaffForename As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuStaffFullname As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents CareFrame3 As Care.Controls.CareFrame
    Friend WithEvents CareFrame6 As Care.Controls.CareFrame
    Friend WithEvents cbxSchool As Care.Controls.CareComboBox
    Friend WithEvents CareFrame4 As Care.Controls.CareFrame
    Friend WithEvents txtRef2 As Care.Controls.CareTextBox
    Friend WithEvents txtRef1 As Care.Controls.CareTextBox
    Friend WithEvents cbxDates As Care.Controls.CareComboBox
    Friend WithEvents CareFrame5 As Care.Controls.CareFrame
    Friend WithEvents cbxDuplicates As Care.Controls.CareComboBox
    Friend WithEvents cbxTags As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents btnSelectAll As Care.Controls.CareButton
End Class
