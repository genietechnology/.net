﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCDAPDefinition
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling2 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling3 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling4 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling5 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.GroupBox8 = New Care.Controls.CareFrame()
        Me.txtDescription = New DevExpress.XtraEditors.MemoEdit()
        Me.GroupBox1 = New Care.Controls.CareFrame()
        Me.CareLabel3 = New Care.Controls.CareLabel()
        Me.cbxLetter = New Care.Controls.CareComboBox()
        Me.CareLabel2 = New Care.Controls.CareLabel()
        Me.txtName = New Care.Controls.CareTextBox()
        Me.CareLabel1 = New Care.Controls.CareLabel()
        Me.cbxStep = New Care.Controls.CareComboBox()
        Me.cbxArea = New Care.Controls.CareComboBox()
        Me.Label3 = New Care.Controls.CareLabel()
        Me.Label1 = New Care.Controls.CareLabel()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.txtHow = New DevExpress.XtraEditors.MemoEdit()
        Me.GroupControl2 = New Care.Controls.CareFrame()
        Me.txtExamples = New DevExpress.XtraEditors.MemoEdit()
        Me.GroupControl3 = New Care.Controls.CareFrame()
        Me.txtProgOutcomes = New DevExpress.XtraEditors.MemoEdit()
        Me.GroupControl4 = New Care.Controls.CareFrame()
        Me.txtCreativity = New DevExpress.XtraEditors.MemoEdit()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.GroupBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox8.SuspendLayout()
        CType(Me.txtDescription.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.cbxLetter.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxStep.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxArea.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtHow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.txtExamples.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.txtProgOutcomes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.txtCreativity.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(597, 3)
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(506, 3)
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(0, 576)
        Me.Panel1.Size = New System.Drawing.Size(694, 36)
        Me.Panel1.TabIndex = 6
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.txtDescription)
        Me.GroupBox8.Location = New System.Drawing.Point(12, 151)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(672, 100)
        Me.GroupBox8.TabIndex = 1
        Me.GroupBox8.Text = "Description"
        '
        'txtDescription
        '
        Me.txtDescription.Location = New System.Drawing.Point(12, 28)
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtDescription, True)
        Me.txtDescription.Size = New System.Drawing.Size(648, 63)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtDescription, OptionsSpelling1)
        Me.txtDescription.TabIndex = 0
        Me.txtDescription.Tag = "AE"
        Me.txtDescription.UseOptimizedRendering = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CareLabel3)
        Me.GroupBox1.Controls.Add(Me.cbxLetter)
        Me.GroupBox1.Controls.Add(Me.CareLabel2)
        Me.GroupBox1.Controls.Add(Me.txtName)
        Me.GroupBox1.Controls.Add(Me.CareLabel1)
        Me.GroupBox1.Controls.Add(Me.cbxStep)
        Me.GroupBox1.Controls.Add(Me.cbxArea)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 53)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(672, 92)
        Me.GroupBox1.TabIndex = 0
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(565, 33)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(30, 15)
        Me.CareLabel3.TabIndex = 25
        Me.CareLabel3.Text = "Letter"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxLetter
        '
        Me.cbxLetter.AllowBlank = False
        Me.cbxLetter.DataSource = Nothing
        Me.cbxLetter.DisplayMember = Nothing
        Me.cbxLetter.EnterMoveNextControl = True
        Me.cbxLetter.Location = New System.Drawing.Point(601, 30)
        Me.cbxLetter.Name = "cbxLetter"
        Me.cbxLetter.Properties.AccessibleName = "Letter"
        Me.cbxLetter.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxLetter.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxLetter.Properties.Appearance.Options.UseFont = True
        Me.cbxLetter.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxLetter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxLetter.ReadOnly = False
        Me.cbxLetter.SelectedValue = Nothing
        Me.cbxLetter.Size = New System.Drawing.Size(55, 20)
        Me.cbxLetter.TabIndex = 2
        Me.cbxLetter.Tag = "AEBM"
        Me.cbxLetter.ValueMember = Nothing
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(13, 61)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel2.TabIndex = 23
        Me.CareLabel2.Text = "Name"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.CharacterCasing = CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(126, 58)
        Me.txtName.MaxLength = 100
        Me.txtName.Name = "txtName"
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AccessibleName = "Name"
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.MaxLength = 100
        Me.txtName.ReadOnly = False
        Me.txtName.Size = New System.Drawing.Size(530, 20)
        Me.txtName.TabIndex = 3
        Me.txtName.Tag = "AEBM"
        Me.txtName.TextAlign = HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(455, 33)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(23, 15)
        Me.CareLabel1.TabIndex = 21
        Me.CareLabel1.Text = "Step"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxStep
        '
        Me.cbxStep.AllowBlank = False
        Me.cbxStep.DataSource = Nothing
        Me.cbxStep.DisplayMember = Nothing
        Me.cbxStep.EnterMoveNextControl = True
        Me.cbxStep.Location = New System.Drawing.Point(484, 30)
        Me.cbxStep.Name = "cbxStep"
        Me.cbxStep.Properties.AccessibleName = "Step"
        Me.cbxStep.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxStep.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxStep.Properties.Appearance.Options.UseFont = True
        Me.cbxStep.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxStep.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxStep.ReadOnly = False
        Me.cbxStep.SelectedValue = Nothing
        Me.cbxStep.Size = New System.Drawing.Size(55, 20)
        Me.cbxStep.TabIndex = 1
        Me.cbxStep.Tag = "AEBM"
        Me.cbxStep.ValueMember = Nothing
        '
        'cbxArea
        '
        Me.cbxArea.AllowBlank = False
        Me.cbxArea.DataSource = Nothing
        Me.cbxArea.DisplayMember = Nothing
        Me.cbxArea.EnterMoveNextControl = True
        Me.cbxArea.Location = New System.Drawing.Point(126, 30)
        Me.cbxArea.Name = "cbxArea"
        Me.cbxArea.Properties.AccessibleName = "Developmental Area"
        Me.cbxArea.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxArea.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxArea.Properties.Appearance.Options.UseFont = True
        Me.cbxArea.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxArea.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxArea.ReadOnly = False
        Me.cbxArea.SelectedValue = Nothing
        Me.cbxArea.Size = New System.Drawing.Size(312, 20)
        Me.cbxArea.TabIndex = 0
        Me.cbxArea.Tag = "AEBM"
        Me.cbxArea.ValueMember = Nothing
        '
        'Label3
        '
        Me.Label3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label3.Location = New System.Drawing.Point(6, 131)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(0, 15)
        Me.Label3.TabIndex = 4
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label1.Location = New System.Drawing.Point(13, 33)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(107, 15)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Developmental Area"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.txtHow)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 257)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(672, 100)
        Me.GroupControl1.TabIndex = 2
        Me.GroupControl1.Text = "How, where and when to assess"
        '
        'txtHow
        '
        Me.txtHow.Location = New System.Drawing.Point(13, 28)
        Me.txtHow.Name = "txtHow"
        Me.txtHow.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtHow.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtHow, True)
        Me.txtHow.Size = New System.Drawing.Size(647, 63)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtHow, OptionsSpelling2)
        Me.txtHow.TabIndex = 0
        Me.txtHow.Tag = "AE"
        Me.txtHow.UseOptimizedRendering = True
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.txtExamples)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 363)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(320, 207)
        Me.GroupControl2.TabIndex = 3
        Me.GroupControl2.Text = "Examples"
        '
        'txtExamples
        '
        Me.txtExamples.Location = New System.Drawing.Point(13, 28)
        Me.txtExamples.Name = "txtExamples"
        Me.txtExamples.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtExamples.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtExamples, True)
        Me.txtExamples.Size = New System.Drawing.Size(297, 172)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtExamples, OptionsSpelling3)
        Me.txtExamples.TabIndex = 0
        Me.txtExamples.Tag = "AE"
        Me.txtExamples.UseOptimizedRendering = True
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.txtProgOutcomes)
        Me.GroupControl3.Location = New System.Drawing.Point(338, 363)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(346, 120)
        Me.GroupControl3.TabIndex = 4
        Me.GroupControl3.Text = "Progress towards outcomes"
        '
        'txtProgOutcomes
        '
        Me.txtProgOutcomes.Location = New System.Drawing.Point(12, 28)
        Me.txtProgOutcomes.Name = "txtProgOutcomes"
        Me.txtProgOutcomes.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtProgOutcomes.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtProgOutcomes, True)
        Me.txtProgOutcomes.Size = New System.Drawing.Size(322, 85)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtProgOutcomes, OptionsSpelling4)
        Me.txtProgOutcomes.TabIndex = 0
        Me.txtProgOutcomes.Tag = "AE"
        Me.txtProgOutcomes.UseOptimizedRendering = True
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.txtCreativity)
        Me.GroupControl4.Location = New System.Drawing.Point(338, 489)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(346, 81)
        Me.GroupControl4.TabIndex = 5
        Me.GroupControl4.Text = "Creativity"
        '
        'txtCreativity
        '
        Me.txtCreativity.Location = New System.Drawing.Point(12, 28)
        Me.txtCreativity.Name = "txtCreativity"
        Me.txtCreativity.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtCreativity.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtCreativity, True)
        Me.txtCreativity.Size = New System.Drawing.Size(322, 46)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtCreativity, OptionsSpelling5)
        Me.txtCreativity.TabIndex = 0
        Me.txtCreativity.Tag = "AE"
        Me.txtCreativity.UseOptimizedRendering = True
        '
        'frmCDAPDefinition
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(694, 612)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox8)
        Me.Name = "frmCDAPDefinition"
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.GroupBox8, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        Me.Controls.SetChildIndex(Me.GroupControl2, 0)
        Me.Controls.SetChildIndex(Me.GroupControl3, 0)
        Me.Controls.SetChildIndex(Me.GroupControl4, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.GroupBox8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox8.ResumeLayout(False)
        CType(Me.txtDescription.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.cbxLetter.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxStep.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxArea.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.txtHow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.txtExamples.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.txtProgOutcomes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        CType(Me.txtCreativity.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox8 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtDescription As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupBox1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents cbxStep As Care.Controls.CareComboBox
    Friend WithEvents cbxArea As Care.Controls.CareComboBox
    Friend WithEvents Label3 As Care.Controls.CareLabel
    Friend WithEvents Label1 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents cbxLetter As Care.Controls.CareComboBox
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtHow As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtExamples As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtProgOutcomes As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtCreativity As DevExpress.XtraEditors.MemoEdit

End Class
