﻿

Imports Care.Global
Imports Care.Data
Imports DevExpress.XtraPivotGrid
Imports System.Windows.Forms

Public Class frmFCOccupancy

    Private Sub frmForecasting_Load(sender As Object, e As EventArgs) Handles Me.Load

        cbxOutputFormat.AddItem("Monthly View")
        cbxOutputFormat.AddItem("Weekly View")
        cbxOutputFormat.SelectedIndex = 0

        cbxSite.PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)
        cbxSite.SelectedIndex = 0

        Dim _From As Date = DateSerial(Today.Year, Today.Month, 1)
        Dim _To As Date = DateAdd(DateInterval.Month, 12, _From)

        cdtFrom.Value = _From
        cdtTo.Value = _To

        Dim _SQL As String = ""
        _SQL += "select top 20 last_run as 'Last Run',"
        _SQL += " CASE WHEN last_result = 0 THEN 'Successful' ELSE 'Failed' END as 'Status'"
        _SQL += " from AppTasks t"
        _SQL += " where command = 'RebuildOccupancy'"
        _SQL += " order by last_run desc"

        grdTasks.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click

        If cbxSite.SelectedIndex < 0 Then Exit Sub

        Dim _Mess As String = ""
        If cbxSite.Items.Count > 1 Then
            _Mess = "This will rebuild the Occupancy Reporting Data for ALL Sites in a Multi-Site Environment and could take a while. Are you sure you wish to Continue?"
        Else
            _Mess = "This will rebuild the Occupancy Reporting Data and could take a while. Are you sure you wish to Continue?"
        End If

        If CareMessage(_Mess, MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Rebuild Occupancy Data") = DialogResult.Yes Then

            SetCMDs(False)
            Session.CursorWaiting()

            Dim _OC As New OccupancyEngine
            _OC.RebuildOccupancy(chkExclWL.Checked, chkExclHolidays.Checked)

            SetCMDs(True)
            Session.CursorDefault()
        End If

    End Sub

    Private Sub SetCMDs(ByVal Enabled As Boolean)
        btnRefresh.Enabled = Enabled
        btnPrint.Enabled = Enabled
        btnClose.Enabled = Enabled
    End Sub

    Private Function ValidateDateRange() As Boolean

        Dim _From As Date? = ValueHandler.ConvertDate(cdtFrom.Value)
        Dim _To As Date? = ValueHandler.ConvertDate(cdtTo.Value)

        If _From.HasValue AndAlso _To.HasValue Then
            If _From.Value > _To.Value Then
                Return False
            Else
                Return True
            End If
        Else
            Return False
        End If

    End Function

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click

        If Not ValidateDateRange() Then
            CareMessage("Please enter a valid date range.", MessageBoxIcon.Exclamation, "Date Range Validation")
            Exit Sub
        End If

        SetCMDs(False)
        Session.CursorWaiting()

        Dim _SQL As String = ""

        _SQL += "select period_from, period_to, s.name as 'site_name', o.room_name, room_capacity, total_capacity, total_headcount,"
        _SQL += " occ, fte, ft, pt, hours, funded, revenue,"

        If cbxOutputFormat.SelectedIndex = 0 Then
            _SQL += " mondays, tuesdays, wednesdays, thursdays, fridays, total_days,"
        End If

        _SQL += " mon_am, mon_pm as 'mon_pm', mon_hc as 'mon_headcount', mon_occ,"
        _SQL += " tue_am, tue_pm as 'tue_pm', tue_hc as 'tue_headcount', tue_occ,"
        _SQL += " wed_am, wed_pm as 'wed_pm', wed_hc as 'wed_headcount', wed_occ,"
        _SQL += " thu_am, thu_pm as 'thu_pm', thu_hc as 'thu_headcount', thu_occ,"
        _SQL += " fri_am, fri_pm as 'fri_pm', fri_hc as 'fri_headcount', fri_occ"

        If cbxOutputFormat.SelectedIndex = 0 Then
            _SQL += " from OccMonths o"
        Else
            _SQL += " from OccWeeks o"
        End If

        _SQL += " left join SiteRoomRatios rr on rr.ID = o.room_id"
        _SQL += " left join SiteRooms r on r.ID = rr.room_id"
        _SQL += " left join Sites s on s.ID = r.site_id"
        _SQL += " where r.site_id = '" + cbxSite.SelectedValue.ToString + "'"
        _SQL += " and period_from between " + ValueHandler.SQLDate(cdtFrom.Value.Value, True) + " and " + ValueHandler.SQLDate(cdtTo.Value.Value, True)

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)

        If cbxOutputFormat.SelectedIndex = 0 Then
            If btnPrint.ShiftDown Then
                Care.Shared.ReportHandler.DesignReport("ForecastOccMonth.repx", Session.ConnectionString, _SQL)
            Else
                Care.Shared.ReportHandler.RunReport("ForecastOccMonth.repx", Session.ConnectionString, _SQL)
            End If
        Else
            If btnPrint.ShiftDown Then
                Care.Shared.ReportHandler.DesignReport("ForecastOccWeek.repx", Session.ConnectionString, _SQL)
            Else
                Care.Shared.ReportHandler.RunReport("ForecastOccWeek.repx", Session.ConnectionString, _SQL)
            End If
        End If

        SetCMDs(True)
        Session.CursorDefault()

    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

End Class
