﻿Imports Care.Global
Imports Care.Data

Public Class frmTariffAge

    Dim m_AgeBand As Business.TariffAgeBand

    Private Sub frmRooms_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.GridSQL = "select id, description as 'Name', min_age as 'Min Age', max_age as 'Max Age'" & _
                     " from TariffAgeBands" & _
                     " order by min_age"

        gbx.Hide()

    End Sub

    Protected Overrides Sub SetBindings()

        m_AgeBand = New Business.TariffAgeBand
        bs.DataSource = m_AgeBand

        txtDescription.DataBindings.Add("Text", bs, "_Description")
        txtMinAge.DataBindings.Add("Text", bs, "_MinAge")
        txtMaxAge.DataBindings.Add("Text", bs, "_MaxAge")

    End Sub

    Protected Overrides Sub BindToID(ID As System.Guid, IsNew As Boolean)
        m_AgeBand = Business.TariffAgeBand.RetreiveByID(ID)
        bs.DataSource = m_AgeBand
    End Sub

    Protected Overrides Sub CommitUpdate()
        m_AgeBand.Store()
    End Sub

    Protected Overrides Sub CommitDelete(ByVal ID As Guid)

        Dim _SQL As String = "delete from TariffAgeBands where ID = '" + ID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

    End Sub

End Class