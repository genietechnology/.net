﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmStaff
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.tabActivity = New DevExpress.XtraTab.XtraTabPage()
        Me.CrmActivity1 = New Care.[Shared].CRMActivity()
        Me.tabTimesheets = New DevExpress.XtraTab.XtraTabPage()
        Me.gbxPayroll = New Care.Controls.CareFrame(Me.components)
        Me.btnPaySlip = New Care.Controls.CareButton(Me.components)
        Me.btnPayEmployee = New Care.Controls.CareButton(Me.components)
        Me.txtPayrollID = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.txtNINO = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.txtTaxCode = New Care.Controls.CareTextBox(Me.components)
        Me.txtNICat = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.gbxAttendence = New Care.Controls.CareFrame(Me.components)
        Me.cgTimesheets = New Care.Controls.CareGrid()
        Me.GroupControl2 = New Care.Controls.CareFrame(Me.components)
        Me.lblSalaryPer = New Care.Controls.CareLabel(Me.components)
        Me.txtSalaryPerHour = New Care.Controls.CareTextBox(Me.components)
        Me.txtContractedAnnual = New Care.Controls.CareTextBox(Me.components)
        Me.txtContracted4Week = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.txtFTEAnnual = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel13 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel12 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.txtContractedWeek = New Care.Controls.CareTextBox(Me.components)
        Me.txtFTEWeek = New Care.Controls.CareTextBox(Me.components)
        Me.txtHoursContracted = New Care.Controls.CareTextBox(Me.components)
        Me.cbxSalaryUnit = New Care.Controls.CareComboBox(Me.components)
        Me.txtFTE4Week = New Care.Controls.CareTextBox(Me.components)
        Me.txtSalary = New Care.Controls.CareTextBox(Me.components)
        Me.Label18 = New Care.Controls.CareLabel(Me.components)
        Me.txtHoursFTE = New Care.Controls.CareTextBox(Me.components)
        Me.Label4 = New Care.Controls.CareLabel(Me.components)
        Me.tabAbsence = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl17 = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel41 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel39 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel38 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel37 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel36 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel34 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel33 = New Care.Controls.CareLabel(Me.components)
        Me.txtStatSickness = New Care.Controls.CareTextBox(Me.components)
        Me.txtStatTotal = New Care.Controls.CareTextBox(Me.components)
        Me.txtStatHols = New Care.Controls.CareTextBox(Me.components)
        Me.txtStatOther = New Care.Controls.CareTextBox(Me.components)
        Me.lblDaysSince = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl16 = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel32 = New Care.Controls.CareLabel(Me.components)
        Me.txtHolsEntitlement = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel31 = New Care.Controls.CareLabel(Me.components)
        Me.cbxHolsFrom = New Care.Controls.CareComboBox(Me.components)
        Me.GroupControl15 = New Care.Controls.CareFrame(Me.components)
        Me.lblBradfordScore = New Care.Controls.CareLabel(Me.components)
        Me.txtStatHolsLeft = New Care.Controls.CareTextBox(Me.components)
        Me.txtStatBS = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel35 = New Care.Controls.CareLabel(Me.components)
        Me.cgAbs = New Care.Controls.CareGridWithButtons()
        Me.GroupControl7 = New Care.Controls.CareFrame(Me.components)
        Me.chkAbsenceNotes = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel18 = New Care.Controls.CareLabel(Me.components)
        Me.cbxAbsenceType = New Care.Controls.CareComboBox(Me.components)
        Me.tabApps = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl5 = New Care.Controls.CareFrame(Me.components)
        Me.cgDis = New Care.Controls.CareGridWithButtons()
        Me.GroupControl6 = New Care.Controls.CareFrame(Me.components)
        Me.cgAppraisals = New Care.Controls.CareGridWithButtons()
        Me.tabQualsTrain = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl4 = New Care.Controls.CareFrame(Me.components)
        Me.cgQuals = New Care.Controls.CareGridWithButtons()
        Me.GroupControl3 = New Care.Controls.CareFrame(Me.components)
        Me.cgTraining = New Care.Controls.CareGridWithButtons()
        Me.tabCar = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupBox5 = New Care.Controls.CareFrame(Me.components)
        Me.txtTelHome = New Care.[Shared].CareTelephoneNumber()
        Me.txtTelMobile = New Care.[Shared].CareTelephoneNumber()
        Me.txtEmail = New Care.[Shared].CareEmailAddress()
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.txtAddress = New Care.Address.CareAddress()
        Me.Label25 = New Care.Controls.CareLabel(Me.components)
        Me.Label23 = New Care.Controls.CareLabel(Me.components)
        Me.Label24 = New Care.Controls.CareLabel(Me.components)
        Me.gbxPaxton = New Care.Controls.CareFrame(Me.components)
        Me.cbxPaxtonDepartment = New Care.Controls.CareComboBox(Me.components)
        Me.btnPaxtonLink = New Care.Controls.CareButton(Me.components)
        Me.btnPaxtonDisable = New Care.Controls.CareButton(Me.components)
        Me.cbxPaxtonAccessLevel = New Care.Controls.CareComboBox(Me.components)
        Me.btnPaxtonUpdate = New Care.Controls.CareButton(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.txtPaxtonUserID = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox4 = New Care.Controls.CareFrame(Me.components)
        Me.chkCar = New Care.Controls.CareCheckBox(Me.components)
        Me.chkDriver = New Care.Controls.CareCheckBox(Me.components)
        Me.Label11 = New Care.Controls.CareLabel(Me.components)
        Me.Label17 = New Care.Controls.CareLabel(Me.components)
        Me.udtInsCheck = New Care.Controls.CareDateTime(Me.components)
        Me.txtMakeModel = New Care.Controls.CareTextBox(Me.components)
        Me.Label20 = New Care.Controls.CareLabel(Me.components)
        Me.txtReg = New Care.Controls.CareTextBox(Me.components)
        Me.Label21 = New Care.Controls.CareLabel(Me.components)
        Me.Label22 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox6 = New Care.Controls.CareFrame(Me.components)
        Me.txtICEMobile = New Care.Controls.CareTextBox(Me.components)
        Me.Label27 = New Care.Controls.CareLabel(Me.components)
        Me.txtICEHome = New Care.Controls.CareTextBox(Me.components)
        Me.Label30 = New Care.Controls.CareLabel(Me.components)
        Me.txtICERel = New Care.Controls.CareTextBox(Me.components)
        Me.Label31 = New Care.Controls.CareLabel(Me.components)
        Me.txtICEName = New Care.Controls.CareTextBox(Me.components)
        Me.Label32 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox2 = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel42 = New Care.Controls.CareLabel(Me.components)
        Me.txtDBSPosition = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel40 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel30 = New Care.Controls.CareLabel(Me.components)
        Me.cbxDBSCheckType = New Care.Controls.CareComboBox(Me.components)
        Me.cdtDBSIssued = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel17 = New Care.Controls.CareLabel(Me.components)
        Me.txtCRBRef = New Care.Controls.CareTextBox(Me.components)
        Me.udtCRBDue = New Care.Controls.CareDateTime(Me.components)
        Me.udtCRBLast = New Care.Controls.CareDateTime(Me.components)
        Me.Label7 = New Care.Controls.CareLabel(Me.components)
        Me.Label8 = New Care.Controls.CareLabel(Me.components)
        Me.tabPersonal = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl13 = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel10 = New Care.Controls.CareLabel(Me.components)
        Me.cbxQualLevel = New Care.Controls.CareComboBox(Me.components)
        Me.GroupControl12 = New Care.Controls.CareFrame(Me.components)
        Me.lblYearsLeft = New Care.Controls.CareLabel(Me.components)
        Me.lblYearsService = New Care.Controls.CareLabel(Me.components)
        Me.udtLeft = New Care.Controls.CareDateTime(Me.components)
        Me.Label13 = New Care.Controls.CareLabel(Me.components)
        Me.udtStarted = New Care.Controls.CareDateTime(Me.components)
        Me.Label12 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl9 = New Care.Controls.CareFrame(Me.components)
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.chkENCO = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel24 = New Care.Controls.CareLabel(Me.components)
        Me.chkSENCO = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel19 = New Care.Controls.CareLabel(Me.components)
        Me.chkHygiene = New Care.Controls.CareCheckBox(Me.components)
        Me.chkSafe = New Care.Controls.CareCheckBox(Me.components)
        Me.lblMedicine = New Care.Controls.CareLabel(Me.components)
        Me.lblObs = New Care.Controls.CareLabel(Me.components)
        Me.lblSleep = New Care.Controls.CareLabel(Me.components)
        Me.lblMilk = New Care.Controls.CareLabel(Me.components)
        Me.lblNappy = New Care.Controls.CareLabel(Me.components)
        Me.chkAidWork = New Care.Controls.CareCheckBox(Me.components)
        Me.chkFS = New Care.Controls.CareCheckBox(Me.components)
        Me.lblFood = New Care.Controls.CareLabel(Me.components)
        Me.chkPaeds = New Care.Controls.CareCheckBox(Me.components)
        Me.chkFSAid = New Care.Controls.CareCheckBox(Me.components)
        Me.GroupControl10 = New Care.Controls.CareFrame(Me.components)
        Me.ccxLanguages = New Care.Controls.CareCheckedComboBox(Me.components)
        Me.ccxNationality = New Care.Controls.CareCheckedComboBox(Me.components)
        Me.CareLabel20 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel21 = New Care.Controls.CareLabel(Me.components)
        Me.cbxEthnicity = New Care.Controls.CareComboBox(Me.components)
        Me.cbxReligion = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel22 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel23 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl8 = New Care.Controls.CareFrame(Me.components)
        Me.btnPIN = New Care.Controls.CareButton(Me.components)
        Me.txtTouchPIN = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel16 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox1 = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel26 = New Care.Controls.CareLabel(Me.components)
        Me.lblAge = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel15 = New Care.Controls.CareLabel(Me.components)
        Me.cbxContractType = New Care.Controls.CareComboBox(Me.components)
        Me.cbxStatus = New Care.Controls.CareComboBox(Me.components)
        Me.Label10 = New Care.Controls.CareLabel(Me.components)
        Me.udtDOB = New Care.Controls.CareDateTime(Me.components)
        Me.Label5 = New Care.Controls.CareLabel(Me.components)
        Me.txtForename = New Care.Controls.CareTextBox(Me.components)
        Me.Label3 = New Care.Controls.CareLabel(Me.components)
        Me.txtSurname = New Care.Controls.CareTextBox(Me.components)
        Me.txtFullname = New Care.Controls.CareTextBox(Me.components)
        Me.Label1 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox3 = New Care.Controls.CareFrame(Me.components)
        Me.cbxManager = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel29 = New Care.Controls.CareLabel(Me.components)
        Me.chkLineManager = New Care.Controls.CareCheckBox(Me.components)
        Me.Label9 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel28 = New Care.Controls.CareLabel(Me.components)
        Me.cbxDepartment = New Care.Controls.CareComboBox(Me.components)
        Me.chkKeyworker = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel27 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel25 = New Care.Controls.CareLabel(Me.components)
        Me.cbxSite = New Care.Controls.CareComboBox(Me.components)
        Me.cbxGroup = New Care.Controls.CareComboBox(Me.components)
        Me.Label15 = New Care.Controls.CareLabel(Me.components)
        Me.txtJobTitle = New Care.Controls.CareTextBox(Me.components)
        Me.Label16 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl1 = New Care.Controls.CareFrame(Me.components)
        Me.PictureBox = New Care.Controls.PhotoControl()
        Me.tabMain = New Care.Controls.CareTab(Me.components)
        Me.tabChildren = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl11 = New Care.Controls.CareFrame(Me.components)
        Me.cgChildren = New Care.Controls.CareGrid()
        Me.tabPlanner = New DevExpress.XtraTab.XtraTabPage()
        Me.YearView1 = New Nursery.YearView()
        Me.tabShifts = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl14 = New Care.Controls.CareFrame(Me.components)
        Me.radShiftPreferences = New Care.Controls.CareRadioButton(Me.components)
        Me.radShiftActual = New Care.Controls.CareRadioButton(Me.components)
        Me.cgShifts = New Care.Controls.CareGridWithButtons()
        Me.SchedulerStorage1 = New DevExpress.XtraScheduler.SchedulerStorage(Me.components)
        Me.ctxPopup = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuUpload = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFullSize = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDelete = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpenFileDialog = New System.Windows.Forms.OpenFileDialog()
        Me.btnRecruit = New Care.Controls.CareButton(Me.components)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.tabActivity.SuspendLayout()
        Me.tabTimesheets.SuspendLayout()
        CType(Me.gbxPayroll, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxPayroll.SuspendLayout()
        CType(Me.txtPayrollID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNINO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTaxCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNICat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxAttendence, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxAttendence.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.txtSalaryPerHour.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtContractedAnnual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtContracted4Week.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFTEAnnual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtContractedWeek.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFTEWeek.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtHoursContracted.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSalaryUnit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFTE4Week.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSalary.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtHoursFTE.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabAbsence.SuspendLayout()
        CType(Me.GroupControl17, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl17.SuspendLayout()
        CType(Me.txtStatSickness.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStatTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStatHols.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStatOther.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl16, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl16.SuspendLayout()
        CType(Me.txtHolsEntitlement.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxHolsFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl15, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl15.SuspendLayout()
        CType(Me.txtStatHolsLeft.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStatBS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl7.SuspendLayout()
        CType(Me.chkAbsenceNotes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxAbsenceType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabApps.SuspendLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl6.SuspendLayout()
        Me.tabQualsTrain.SuspendLayout()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        Me.tabCar.SuspendLayout()
        CType(Me.GroupBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        CType(Me.gbxPaxton, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxPaxton.SuspendLayout()
        CType(Me.cbxPaxtonDepartment.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxPaxtonAccessLevel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPaxtonUserID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.chkCar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkDriver.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.udtInsCheck.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.udtInsCheck.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMakeModel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtReg.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        CType(Me.txtICEMobile.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtICEHome.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtICERel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtICEName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.txtDBSPosition.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxDBSCheckType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDBSIssued.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDBSIssued.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCRBRef.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.udtCRBDue.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.udtCRBDue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.udtCRBLast.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.udtCRBLast.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabPersonal.SuspendLayout()
        CType(Me.GroupControl13, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl13.SuspendLayout()
        CType(Me.cbxQualLevel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl12.SuspendLayout()
        CType(Me.udtLeft.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.udtLeft.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.udtStarted.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.udtStarted.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl9.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        CType(Me.chkENCO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSENCO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkHygiene.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSafe.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAidWork.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkFS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkPaeds.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkFSAid.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl10.SuspendLayout()
        CType(Me.ccxLanguages.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ccxNationality.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxEthnicity.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxReligion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl8.SuspendLayout()
        CType(Me.txtTouchPIN.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.cbxContractType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.udtDOB.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.udtDOB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtForename.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFullname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.cbxManager.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkLineManager.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxDepartment.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkKeyworker.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxGroup.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJobTitle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.tabMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabMain.SuspendLayout()
        Me.tabChildren.SuspendLayout()
        CType(Me.GroupControl11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl11.SuspendLayout()
        Me.tabPlanner.SuspendLayout()
        Me.tabShifts.SuspendLayout()
        CType(Me.GroupControl14, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl14.SuspendLayout()
        CType(Me.radShiftPreferences.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radShiftActual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SchedulerStorage1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ctxPopup.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(738, 5)
        Me.btnCancel.TabIndex = 3
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(647, 5)
        Me.btnOK.TabIndex = 2
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnRecruit)
        Me.Panel1.Location = New System.Drawing.Point(0, 570)
        Me.Panel1.Size = New System.Drawing.Size(833, 35)
        Me.Panel1.TabIndex = 1
        Me.Panel1.Controls.SetChildIndex(Me.btnOK, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnCancel, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnRecruit, 0)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'tabActivity
        '
        Me.tabActivity.Controls.Add(Me.CrmActivity1)
        Me.tabActivity.Name = "tabActivity"
        Me.tabActivity.Size = New System.Drawing.Size(807, 487)
        Me.tabActivity.Text = "Activity"
        '
        'CrmActivity1
        '
        Me.CrmActivity1.CRMContactEmail = Nothing
        Me.CrmActivity1.CRMContactID = Nothing
        Me.CrmActivity1.CRMContactMobile = Nothing
        Me.CrmActivity1.CRMContactName = Nothing
        Me.CrmActivity1.CRMLinkID = Nothing
        Me.CrmActivity1.CRMLinkType = Care.[Shared].CRM.EnumLinkType.Staff
        Me.CrmActivity1.Location = New System.Drawing.Point(8, 7)
        Me.CrmActivity1.Name = "CrmActivity1"
        Me.CrmActivity1.Size = New System.Drawing.Size(794, 472)
        Me.CrmActivity1.TabIndex = 0
        '
        'tabTimesheets
        '
        Me.tabTimesheets.Controls.Add(Me.gbxPayroll)
        Me.tabTimesheets.Controls.Add(Me.gbxAttendence)
        Me.tabTimesheets.Controls.Add(Me.GroupControl2)
        Me.tabTimesheets.Name = "tabTimesheets"
        Me.tabTimesheets.Size = New System.Drawing.Size(807, 487)
        Me.tabTimesheets.Text = "Register && Pay"
        '
        'gbxPayroll
        '
        Me.gbxPayroll.Controls.Add(Me.btnPaySlip)
        Me.gbxPayroll.Controls.Add(Me.btnPayEmployee)
        Me.gbxPayroll.Controls.Add(Me.txtPayrollID)
        Me.gbxPayroll.Controls.Add(Me.CareLabel4)
        Me.gbxPayroll.Controls.Add(Me.txtNINO)
        Me.gbxPayroll.Controls.Add(Me.CareLabel5)
        Me.gbxPayroll.Controls.Add(Me.CareLabel6)
        Me.gbxPayroll.Controls.Add(Me.txtTaxCode)
        Me.gbxPayroll.Controls.Add(Me.txtNICat)
        Me.gbxPayroll.Controls.Add(Me.CareLabel7)
        Me.gbxPayroll.Location = New System.Drawing.Point(576, 9)
        Me.gbxPayroll.Name = "gbxPayroll"
        Me.gbxPayroll.Size = New System.Drawing.Size(222, 186)
        Me.gbxPayroll.TabIndex = 0
        Me.gbxPayroll.Text = "Payroll Details"
        '
        'btnPaySlip
        '
        Me.btnPaySlip.Location = New System.Drawing.Point(112, 148)
        Me.btnPaySlip.Name = "btnPaySlip"
        Me.btnPaySlip.Size = New System.Drawing.Size(95, 23)
        Me.btnPaySlip.TabIndex = 9
        Me.btnPaySlip.Text = "View Payslip"
        '
        'btnPayEmployee
        '
        Me.btnPayEmployee.Location = New System.Drawing.Point(11, 148)
        Me.btnPayEmployee.Name = "btnPayEmployee"
        Me.btnPayEmployee.Size = New System.Drawing.Size(95, 23)
        Me.btnPayEmployee.TabIndex = 8
        Me.btnPayEmployee.Text = "View Employee"
        '
        'txtPayrollID
        '
        Me.txtPayrollID.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtPayrollID.EnterMoveNextControl = True
        Me.txtPayrollID.Location = New System.Drawing.Point(87, 28)
        Me.txtPayrollID.MaxLength = 36
        Me.txtPayrollID.Name = "txtPayrollID"
        Me.txtPayrollID.NumericAllowNegatives = False
        Me.txtPayrollID.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtPayrollID.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPayrollID.Properties.AccessibleName = "Forename"
        Me.txtPayrollID.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPayrollID.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPayrollID.Properties.Appearance.Options.UseFont = True
        Me.txtPayrollID.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPayrollID.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPayrollID.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPayrollID.Properties.MaxLength = 36
        Me.txtPayrollID.Size = New System.Drawing.Size(124, 22)
        Me.txtPayrollID.TabIndex = 1
        Me.txtPayrollID.Tag = "AE"
        Me.txtPayrollID.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPayrollID.ToolTipText = ""
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel4.Appearance.Options.UseFont = True
        Me.CareLabel4.Appearance.Options.UseTextOptions = True
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(11, 31)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(50, 15)
        Me.CareLabel4.TabIndex = 0
        Me.CareLabel4.Text = "Payroll ID"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNINO
        '
        Me.txtNINO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNINO.EnterMoveNextControl = True
        Me.txtNINO.Location = New System.Drawing.Point(87, 112)
        Me.txtNINO.MaxLength = 9
        Me.txtNINO.Name = "txtNINO"
        Me.txtNINO.NumericAllowNegatives = False
        Me.txtNINO.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtNINO.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtNINO.Properties.AccessibleName = "Forename"
        Me.txtNINO.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtNINO.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNINO.Properties.Appearance.Options.UseFont = True
        Me.txtNINO.Properties.Appearance.Options.UseTextOptions = True
        Me.txtNINO.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtNINO.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNINO.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtNINO.Properties.MaxLength = 9
        Me.txtNINO.Size = New System.Drawing.Size(124, 22)
        Me.txtNINO.TabIndex = 7
        Me.txtNINO.Tag = "AE"
        Me.txtNINO.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNINO.ToolTipText = ""
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel5.Appearance.Options.UseFont = True
        Me.CareLabel5.Appearance.Options.UseTextOptions = True
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(11, 87)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(49, 15)
        Me.CareLabel5.TabIndex = 4
        Me.CareLabel5.Text = "Tax Code"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel6.Appearance.Options.UseFont = True
        Me.CareLabel6.Appearance.Options.UseTextOptions = True
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(11, 115)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(59, 15)
        Me.CareLabel6.TabIndex = 6
        Me.CareLabel6.Text = "NI Number"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTaxCode
        '
        Me.txtTaxCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTaxCode.EnterMoveNextControl = True
        Me.txtTaxCode.Location = New System.Drawing.Point(87, 84)
        Me.txtTaxCode.MaxLength = 6
        Me.txtTaxCode.Name = "txtTaxCode"
        Me.txtTaxCode.NumericAllowNegatives = False
        Me.txtTaxCode.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtTaxCode.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTaxCode.Properties.AccessibleName = "National Insurance Number"
        Me.txtTaxCode.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtTaxCode.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTaxCode.Properties.Appearance.Options.UseFont = True
        Me.txtTaxCode.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTaxCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtTaxCode.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTaxCode.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTaxCode.Properties.MaxLength = 6
        Me.txtTaxCode.Size = New System.Drawing.Size(124, 22)
        Me.txtTaxCode.TabIndex = 5
        Me.txtTaxCode.Tag = "AE"
        Me.txtTaxCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTaxCode.ToolTipText = ""
        '
        'txtNICat
        '
        Me.txtNICat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNICat.EnterMoveNextControl = True
        Me.txtNICat.Location = New System.Drawing.Point(87, 56)
        Me.txtNICat.MaxLength = 1
        Me.txtNICat.Name = "txtNICat"
        Me.txtNICat.NumericAllowNegatives = False
        Me.txtNICat.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtNICat.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtNICat.Properties.AccessibleName = "National Insurance Number"
        Me.txtNICat.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtNICat.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNICat.Properties.Appearance.Options.UseFont = True
        Me.txtNICat.Properties.Appearance.Options.UseTextOptions = True
        Me.txtNICat.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtNICat.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNICat.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtNICat.Properties.MaxLength = 1
        Me.txtNICat.Size = New System.Drawing.Size(30, 22)
        Me.txtNICat.TabIndex = 3
        Me.txtNICat.Tag = "AE"
        Me.txtNICat.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNICat.ToolTipText = ""
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel7.Appearance.Options.UseFont = True
        Me.CareLabel7.Appearance.Options.UseTextOptions = True
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(11, 59)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(63, 15)
        Me.CareLabel7.TabIndex = 2
        Me.CareLabel7.Text = "NI Category"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbxAttendence
        '
        Me.gbxAttendence.Controls.Add(Me.cgTimesheets)
        Me.gbxAttendence.Location = New System.Drawing.Point(11, 203)
        Me.gbxAttendence.Name = "gbxAttendence"
        Me.gbxAttendence.Size = New System.Drawing.Size(787, 276)
        Me.gbxAttendence.TabIndex = 1
        Me.gbxAttendence.Text = "Attendance"
        '
        'cgTimesheets
        '
        Me.cgTimesheets.AllowBuildColumns = True
        Me.cgTimesheets.AllowEdit = False
        Me.cgTimesheets.AllowHorizontalScroll = False
        Me.cgTimesheets.AllowMultiSelect = False
        Me.cgTimesheets.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgTimesheets.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgTimesheets.Appearance.Options.UseFont = True
        Me.cgTimesheets.AutoSizeByData = True
        Me.cgTimesheets.DisableAutoSize = False
        Me.cgTimesheets.DisableDataFormatting = False
        Me.cgTimesheets.FocusedRowHandle = -2147483648
        Me.cgTimesheets.HideFirstColumn = False
        Me.cgTimesheets.Location = New System.Drawing.Point(11, 30)
        Me.cgTimesheets.Name = "cgTimesheets"
        Me.cgTimesheets.PreviewColumn = ""
        Me.cgTimesheets.QueryID = Nothing
        Me.cgTimesheets.RowAutoHeight = False
        Me.cgTimesheets.SearchAsYouType = True
        Me.cgTimesheets.ShowAutoFilterRow = False
        Me.cgTimesheets.ShowFindPanel = False
        Me.cgTimesheets.ShowGroupByBox = False
        Me.cgTimesheets.ShowLoadingPanel = False
        Me.cgTimesheets.ShowNavigator = False
        Me.cgTimesheets.Size = New System.Drawing.Size(765, 236)
        Me.cgTimesheets.TabIndex = 0
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.lblSalaryPer)
        Me.GroupControl2.Controls.Add(Me.txtSalaryPerHour)
        Me.GroupControl2.Controls.Add(Me.txtContractedAnnual)
        Me.GroupControl2.Controls.Add(Me.txtContracted4Week)
        Me.GroupControl2.Controls.Add(Me.CareLabel14)
        Me.GroupControl2.Controls.Add(Me.txtFTEAnnual)
        Me.GroupControl2.Controls.Add(Me.CareLabel13)
        Me.GroupControl2.Controls.Add(Me.CareLabel12)
        Me.GroupControl2.Controls.Add(Me.CareLabel3)
        Me.GroupControl2.Controls.Add(Me.CareLabel2)
        Me.GroupControl2.Controls.Add(Me.txtContractedWeek)
        Me.GroupControl2.Controls.Add(Me.txtFTEWeek)
        Me.GroupControl2.Controls.Add(Me.txtHoursContracted)
        Me.GroupControl2.Controls.Add(Me.cbxSalaryUnit)
        Me.GroupControl2.Controls.Add(Me.txtFTE4Week)
        Me.GroupControl2.Controls.Add(Me.txtSalary)
        Me.GroupControl2.Controls.Add(Me.Label18)
        Me.GroupControl2.Controls.Add(Me.txtHoursFTE)
        Me.GroupControl2.Controls.Add(Me.Label4)
        Me.GroupControl2.Location = New System.Drawing.Point(11, 9)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(559, 186)
        Me.GroupControl2.TabIndex = 0
        Me.GroupControl2.Text = "Salary Details"
        '
        'lblSalaryPer
        '
        Me.lblSalaryPer.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSalaryPer.Appearance.Options.UseFont = True
        Me.lblSalaryPer.Appearance.Options.UseTextOptions = True
        Me.lblSalaryPer.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblSalaryPer.Location = New System.Drawing.Point(11, 57)
        Me.lblSalaryPer.Name = "lblSalaryPer"
        Me.lblSalaryPer.Size = New System.Drawing.Size(130, 15)
        Me.lblSalaryPer.TabIndex = 3
        Me.lblSalaryPer.Text = "Salary (per hour/annum)"
        Me.lblSalaryPer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSalaryPerHour
        '
        Me.txtSalaryPerHour.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSalaryPerHour.EnterMoveNextControl = True
        Me.txtSalaryPerHour.Location = New System.Drawing.Point(163, 54)
        Me.txtSalaryPerHour.MaxLength = 14
        Me.txtSalaryPerHour.Name = "txtSalaryPerHour"
        Me.txtSalaryPerHour.NumericAllowNegatives = False
        Me.txtSalaryPerHour.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtSalaryPerHour.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSalaryPerHour.Properties.AccessibleName = "National Insurance Number"
        Me.txtSalaryPerHour.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSalaryPerHour.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSalaryPerHour.Properties.Appearance.Options.UseFont = True
        Me.txtSalaryPerHour.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSalaryPerHour.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtSalaryPerHour.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSalaryPerHour.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtSalaryPerHour.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtSalaryPerHour.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSalaryPerHour.Properties.MaxLength = 14
        Me.txtSalaryPerHour.Properties.ReadOnly = True
        Me.txtSalaryPerHour.Size = New System.Drawing.Size(100, 22)
        Me.txtSalaryPerHour.TabIndex = 4
        Me.txtSalaryPerHour.Tag = "2DP"
        Me.txtSalaryPerHour.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSalaryPerHour.ToolTipText = ""
        '
        'txtContractedAnnual
        '
        Me.txtContractedAnnual.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtContractedAnnual.EnterMoveNextControl = True
        Me.txtContractedAnnual.Location = New System.Drawing.Point(447, 153)
        Me.txtContractedAnnual.MaxLength = 14
        Me.txtContractedAnnual.Name = "txtContractedAnnual"
        Me.txtContractedAnnual.NumericAllowNegatives = False
        Me.txtContractedAnnual.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtContractedAnnual.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtContractedAnnual.Properties.AccessibleName = "Forename"
        Me.txtContractedAnnual.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtContractedAnnual.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContractedAnnual.Properties.Appearance.Options.UseFont = True
        Me.txtContractedAnnual.Properties.Appearance.Options.UseTextOptions = True
        Me.txtContractedAnnual.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtContractedAnnual.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtContractedAnnual.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtContractedAnnual.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtContractedAnnual.Properties.MaxLength = 14
        Me.txtContractedAnnual.Properties.ReadOnly = True
        Me.txtContractedAnnual.Size = New System.Drawing.Size(100, 22)
        Me.txtContractedAnnual.TabIndex = 18
        Me.txtContractedAnnual.Tag = "2DP"
        Me.txtContractedAnnual.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtContractedAnnual.ToolTipText = ""
        '
        'txtContracted4Week
        '
        Me.txtContracted4Week.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtContracted4Week.EnterMoveNextControl = True
        Me.txtContracted4Week.Location = New System.Drawing.Point(341, 153)
        Me.txtContracted4Week.MaxLength = 14
        Me.txtContracted4Week.Name = "txtContracted4Week"
        Me.txtContracted4Week.NumericAllowNegatives = False
        Me.txtContracted4Week.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtContracted4Week.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtContracted4Week.Properties.AccessibleName = "Forename"
        Me.txtContracted4Week.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtContracted4Week.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContracted4Week.Properties.Appearance.Options.UseFont = True
        Me.txtContracted4Week.Properties.Appearance.Options.UseTextOptions = True
        Me.txtContracted4Week.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtContracted4Week.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtContracted4Week.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtContracted4Week.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtContracted4Week.Properties.MaxLength = 14
        Me.txtContracted4Week.Properties.ReadOnly = True
        Me.txtContracted4Week.Size = New System.Drawing.Size(100, 22)
        Me.txtContracted4Week.TabIndex = 17
        Me.txtContracted4Week.Tag = "2DP"
        Me.txtContracted4Week.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtContracted4Week.ToolTipText = ""
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel14.Appearance.Options.UseFont = True
        Me.CareLabel14.Appearance.Options.UseTextOptions = True
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.CareLabel14.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.CareLabel14.Location = New System.Drawing.Point(447, 104)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(100, 15)
        Me.CareLabel14.TabIndex = 8
        Me.CareLabel14.Text = "Salary (Annual)"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtFTEAnnual
        '
        Me.txtFTEAnnual.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtFTEAnnual.EnterMoveNextControl = True
        Me.txtFTEAnnual.Location = New System.Drawing.Point(447, 125)
        Me.txtFTEAnnual.MaxLength = 14
        Me.txtFTEAnnual.Name = "txtFTEAnnual"
        Me.txtFTEAnnual.NumericAllowNegatives = False
        Me.txtFTEAnnual.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtFTEAnnual.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFTEAnnual.Properties.AccessibleName = "Forename"
        Me.txtFTEAnnual.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFTEAnnual.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFTEAnnual.Properties.Appearance.Options.UseFont = True
        Me.txtFTEAnnual.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFTEAnnual.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtFTEAnnual.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtFTEAnnual.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtFTEAnnual.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFTEAnnual.Properties.MaxLength = 14
        Me.txtFTEAnnual.Properties.ReadOnly = True
        Me.txtFTEAnnual.Size = New System.Drawing.Size(100, 22)
        Me.txtFTEAnnual.TabIndex = 13
        Me.txtFTEAnnual.Tag = "2DP"
        Me.txtFTEAnnual.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtFTEAnnual.ToolTipText = ""
        '
        'CareLabel13
        '
        Me.CareLabel13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel13.Appearance.Options.UseFont = True
        Me.CareLabel13.Appearance.Options.UseTextOptions = True
        Me.CareLabel13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.CareLabel13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.CareLabel13.Location = New System.Drawing.Point(341, 104)
        Me.CareLabel13.Name = "CareLabel13"
        Me.CareLabel13.Size = New System.Drawing.Size(100, 15)
        Me.CareLabel13.TabIndex = 7
        Me.CareLabel13.Text = "Salary (4 Weeks)"
        Me.CareLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CareLabel12
        '
        Me.CareLabel12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel12.Appearance.Options.UseFont = True
        Me.CareLabel12.Appearance.Options.UseTextOptions = True
        Me.CareLabel12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.CareLabel12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.CareLabel12.Location = New System.Drawing.Point(235, 104)
        Me.CareLabel12.Name = "CareLabel12"
        Me.CareLabel12.Size = New System.Drawing.Size(100, 15)
        Me.CareLabel12.TabIndex = 6
        Me.CareLabel12.Text = "Salary (Week)"
        Me.CareLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel3.Appearance.Options.UseFont = True
        Me.CareLabel3.Appearance.Options.UseTextOptions = True
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(11, 156)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(59, 15)
        Me.CareLabel3.TabIndex = 14
        Me.CareLabel3.Text = "Contracted"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel2.Appearance.Options.UseFont = True
        Me.CareLabel2.Appearance.Options.UseTextOptions = True
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.CareLabel2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.CareLabel2.Location = New System.Drawing.Point(152, 104)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(77, 15)
        Me.CareLabel2.TabIndex = 5
        Me.CareLabel2.Text = "Hours (Week)"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtContractedWeek
        '
        Me.txtContractedWeek.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtContractedWeek.EnterMoveNextControl = True
        Me.txtContractedWeek.Location = New System.Drawing.Point(235, 153)
        Me.txtContractedWeek.MaxLength = 14
        Me.txtContractedWeek.Name = "txtContractedWeek"
        Me.txtContractedWeek.NumericAllowNegatives = False
        Me.txtContractedWeek.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtContractedWeek.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtContractedWeek.Properties.AccessibleName = "National Insurance Number"
        Me.txtContractedWeek.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtContractedWeek.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContractedWeek.Properties.Appearance.Options.UseFont = True
        Me.txtContractedWeek.Properties.Appearance.Options.UseTextOptions = True
        Me.txtContractedWeek.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtContractedWeek.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtContractedWeek.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtContractedWeek.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtContractedWeek.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtContractedWeek.Properties.MaxLength = 14
        Me.txtContractedWeek.Properties.ReadOnly = True
        Me.txtContractedWeek.Size = New System.Drawing.Size(100, 22)
        Me.txtContractedWeek.TabIndex = 16
        Me.txtContractedWeek.Tag = "2DP"
        Me.txtContractedWeek.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtContractedWeek.ToolTipText = ""
        '
        'txtFTEWeek
        '
        Me.txtFTEWeek.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFTEWeek.EnterMoveNextControl = True
        Me.txtFTEWeek.Location = New System.Drawing.Point(235, 125)
        Me.txtFTEWeek.MaxLength = 14
        Me.txtFTEWeek.Name = "txtFTEWeek"
        Me.txtFTEWeek.NumericAllowNegatives = False
        Me.txtFTEWeek.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtFTEWeek.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFTEWeek.Properties.AccessibleName = "National Insurance Number"
        Me.txtFTEWeek.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFTEWeek.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFTEWeek.Properties.Appearance.Options.UseFont = True
        Me.txtFTEWeek.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFTEWeek.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtFTEWeek.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFTEWeek.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtFTEWeek.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtFTEWeek.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFTEWeek.Properties.MaxLength = 14
        Me.txtFTEWeek.Properties.ReadOnly = True
        Me.txtFTEWeek.Size = New System.Drawing.Size(100, 22)
        Me.txtFTEWeek.TabIndex = 11
        Me.txtFTEWeek.Tag = "2DP"
        Me.txtFTEWeek.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtFTEWeek.ToolTipText = ""
        '
        'txtHoursContracted
        '
        Me.txtHoursContracted.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtHoursContracted.EnterMoveNextControl = True
        Me.txtHoursContracted.Location = New System.Drawing.Point(163, 153)
        Me.txtHoursContracted.MaxLength = 14
        Me.txtHoursContracted.Name = "txtHoursContracted"
        Me.txtHoursContracted.NumericAllowNegatives = False
        Me.txtHoursContracted.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtHoursContracted.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtHoursContracted.Properties.AccessibleName = "National Insurance Number"
        Me.txtHoursContracted.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtHoursContracted.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHoursContracted.Properties.Appearance.Options.UseFont = True
        Me.txtHoursContracted.Properties.Appearance.Options.UseTextOptions = True
        Me.txtHoursContracted.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtHoursContracted.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtHoursContracted.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtHoursContracted.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtHoursContracted.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtHoursContracted.Properties.MaxLength = 14
        Me.txtHoursContracted.Size = New System.Drawing.Size(66, 22)
        Me.txtHoursContracted.TabIndex = 15
        Me.txtHoursContracted.Tag = "AE2DP"
        Me.txtHoursContracted.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtHoursContracted.ToolTipText = ""
        '
        'cbxSalaryUnit
        '
        Me.cbxSalaryUnit.AllowBlank = False
        Me.cbxSalaryUnit.DataSource = Nothing
        Me.cbxSalaryUnit.DisplayMember = Nothing
        Me.cbxSalaryUnit.EnterMoveNextControl = True
        Me.cbxSalaryUnit.Location = New System.Drawing.Point(269, 28)
        Me.cbxSalaryUnit.Name = "cbxSalaryUnit"
        Me.cbxSalaryUnit.Properties.AccessibleName = "Status"
        Me.cbxSalaryUnit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSalaryUnit.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSalaryUnit.Properties.Appearance.Options.UseFont = True
        Me.cbxSalaryUnit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSalaryUnit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSalaryUnit.SelectedValue = Nothing
        Me.cbxSalaryUnit.Size = New System.Drawing.Size(172, 22)
        Me.cbxSalaryUnit.TabIndex = 2
        Me.cbxSalaryUnit.Tag = "AE"
        Me.cbxSalaryUnit.ValueMember = Nothing
        '
        'txtFTE4Week
        '
        Me.txtFTE4Week.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtFTE4Week.EnterMoveNextControl = True
        Me.txtFTE4Week.Location = New System.Drawing.Point(341, 125)
        Me.txtFTE4Week.MaxLength = 14
        Me.txtFTE4Week.Name = "txtFTE4Week"
        Me.txtFTE4Week.NumericAllowNegatives = False
        Me.txtFTE4Week.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtFTE4Week.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFTE4Week.Properties.AccessibleName = "Forename"
        Me.txtFTE4Week.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFTE4Week.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFTE4Week.Properties.Appearance.Options.UseFont = True
        Me.txtFTE4Week.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFTE4Week.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtFTE4Week.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtFTE4Week.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtFTE4Week.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFTE4Week.Properties.MaxLength = 14
        Me.txtFTE4Week.Properties.ReadOnly = True
        Me.txtFTE4Week.Size = New System.Drawing.Size(100, 22)
        Me.txtFTE4Week.TabIndex = 12
        Me.txtFTE4Week.Tag = "2DP"
        Me.txtFTE4Week.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtFTE4Week.ToolTipText = ""
        '
        'txtSalary
        '
        Me.txtSalary.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtSalary.EnterMoveNextControl = True
        Me.txtSalary.Location = New System.Drawing.Point(163, 28)
        Me.txtSalary.MaxLength = 14
        Me.txtSalary.Name = "txtSalary"
        Me.txtSalary.NumericAllowNegatives = False
        Me.txtSalary.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtSalary.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSalary.Properties.AccessibleName = "Forename"
        Me.txtSalary.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSalary.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSalary.Properties.Appearance.Options.UseFont = True
        Me.txtSalary.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSalary.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtSalary.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtSalary.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtSalary.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSalary.Properties.MaxLength = 14
        Me.txtSalary.Size = New System.Drawing.Size(100, 22)
        Me.txtSalary.TabIndex = 1
        Me.txtSalary.Tag = "AE2DP"
        Me.txtSalary.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSalary.ToolTipText = ""
        '
        'Label18
        '
        Me.Label18.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Appearance.Options.UseFont = True
        Me.Label18.Appearance.Options.UseTextOptions = True
        Me.Label18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label18.Location = New System.Drawing.Point(11, 128)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(137, 15)
        Me.Label18.TabIndex = 9
        Me.Label18.Text = "Full Time Equivalent (FTE)"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtHoursFTE
        '
        Me.txtHoursFTE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtHoursFTE.EnterMoveNextControl = True
        Me.txtHoursFTE.Location = New System.Drawing.Point(163, 125)
        Me.txtHoursFTE.MaxLength = 14
        Me.txtHoursFTE.Name = "txtHoursFTE"
        Me.txtHoursFTE.NumericAllowNegatives = False
        Me.txtHoursFTE.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtHoursFTE.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtHoursFTE.Properties.AccessibleName = "National Insurance Number"
        Me.txtHoursFTE.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtHoursFTE.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHoursFTE.Properties.Appearance.Options.UseFont = True
        Me.txtHoursFTE.Properties.Appearance.Options.UseTextOptions = True
        Me.txtHoursFTE.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtHoursFTE.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtHoursFTE.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtHoursFTE.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtHoursFTE.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtHoursFTE.Properties.MaxLength = 14
        Me.txtHoursFTE.Size = New System.Drawing.Size(66, 22)
        Me.txtHoursFTE.TabIndex = 10
        Me.txtHoursFTE.Tag = "AE2DP"
        Me.txtHoursFTE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtHoursFTE.ToolTipText = ""
        '
        'Label4
        '
        Me.Label4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Appearance.Options.UseFont = True
        Me.Label4.Appearance.Options.UseTextOptions = True
        Me.Label4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label4.Location = New System.Drawing.Point(11, 31)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(31, 15)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Salary"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabAbsence
        '
        Me.tabAbsence.Controls.Add(Me.GroupControl17)
        Me.tabAbsence.Controls.Add(Me.GroupControl16)
        Me.tabAbsence.Controls.Add(Me.GroupControl15)
        Me.tabAbsence.Controls.Add(Me.cgAbs)
        Me.tabAbsence.Controls.Add(Me.GroupControl7)
        Me.tabAbsence.Name = "tabAbsence"
        Me.tabAbsence.Size = New System.Drawing.Size(807, 487)
        Me.tabAbsence.Text = "Absence && Holidays"
        '
        'GroupControl17
        '
        Me.GroupControl17.Controls.Add(Me.CareLabel41)
        Me.GroupControl17.Controls.Add(Me.CareLabel39)
        Me.GroupControl17.Controls.Add(Me.CareLabel38)
        Me.GroupControl17.Controls.Add(Me.CareLabel37)
        Me.GroupControl17.Controls.Add(Me.CareLabel36)
        Me.GroupControl17.Controls.Add(Me.CareLabel34)
        Me.GroupControl17.Controls.Add(Me.CareLabel33)
        Me.GroupControl17.Controls.Add(Me.txtStatSickness)
        Me.GroupControl17.Controls.Add(Me.txtStatTotal)
        Me.GroupControl17.Controls.Add(Me.txtStatHols)
        Me.GroupControl17.Controls.Add(Me.txtStatOther)
        Me.GroupControl17.Controls.Add(Me.lblDaysSince)
        Me.GroupControl17.Location = New System.Drawing.Point(332, 413)
        Me.GroupControl17.Name = "GroupControl17"
        Me.GroupControl17.ShowCaption = False
        Me.GroupControl17.Size = New System.Drawing.Size(464, 67)
        Me.GroupControl17.TabIndex = 4
        Me.GroupControl17.Text = "GroupControl17"
        '
        'CareLabel41
        '
        Me.CareLabel41.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel41.Appearance.Options.UseFont = True
        Me.CareLabel41.Appearance.Options.UseTextOptions = True
        Me.CareLabel41.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel41.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.CareLabel41.Location = New System.Drawing.Point(393, 17)
        Me.CareLabel41.Name = "CareLabel41"
        Me.CareLabel41.Size = New System.Drawing.Size(62, 15)
        Me.CareLabel41.TabIndex = 4
        Me.CareLabel41.Text = "TOTAL"
        Me.CareLabel41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CareLabel39
        '
        Me.CareLabel39.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel39.Appearance.Options.UseFont = True
        Me.CareLabel39.Appearance.Options.UseTextOptions = True
        Me.CareLabel39.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel39.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.CareLabel39.Location = New System.Drawing.Point(311, 17)
        Me.CareLabel39.Name = "CareLabel39"
        Me.CareLabel39.Size = New System.Drawing.Size(62, 15)
        Me.CareLabel39.TabIndex = 3
        Me.CareLabel39.Text = "Other"
        Me.CareLabel39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CareLabel38
        '
        Me.CareLabel38.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel38.Appearance.Options.UseFont = True
        Me.CareLabel38.Appearance.Options.UseTextOptions = True
        Me.CareLabel38.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel38.Location = New System.Drawing.Point(379, 41)
        Me.CareLabel38.Name = "CareLabel38"
        Me.CareLabel38.Size = New System.Drawing.Size(8, 15)
        Me.CareLabel38.TabIndex = 8
        Me.CareLabel38.Text = "="
        Me.CareLabel38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel37
        '
        Me.CareLabel37.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel37.Appearance.Options.UseFont = True
        Me.CareLabel37.Appearance.Options.UseTextOptions = True
        Me.CareLabel37.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel37.Location = New System.Drawing.Point(297, 41)
        Me.CareLabel37.Name = "CareLabel37"
        Me.CareLabel37.Size = New System.Drawing.Size(8, 15)
        Me.CareLabel37.TabIndex = 7
        Me.CareLabel37.Text = "+"
        Me.CareLabel37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel36
        '
        Me.CareLabel36.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel36.Appearance.Options.UseFont = True
        Me.CareLabel36.Appearance.Options.UseTextOptions = True
        Me.CareLabel36.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel36.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.CareLabel36.Location = New System.Drawing.Point(229, 17)
        Me.CareLabel36.Name = "CareLabel36"
        Me.CareLabel36.Size = New System.Drawing.Size(62, 15)
        Me.CareLabel36.TabIndex = 2
        Me.CareLabel36.Text = "Holidays"
        Me.CareLabel36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CareLabel34
        '
        Me.CareLabel34.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel34.Appearance.Options.UseFont = True
        Me.CareLabel34.Appearance.Options.UseTextOptions = True
        Me.CareLabel34.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel34.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.CareLabel34.Location = New System.Drawing.Point(147, 17)
        Me.CareLabel34.Name = "CareLabel34"
        Me.CareLabel34.Size = New System.Drawing.Size(62, 15)
        Me.CareLabel34.TabIndex = 1
        Me.CareLabel34.Text = "Sickness"
        Me.CareLabel34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CareLabel33
        '
        Me.CareLabel33.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel33.Appearance.Options.UseFont = True
        Me.CareLabel33.Appearance.Options.UseTextOptions = True
        Me.CareLabel33.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel33.Location = New System.Drawing.Point(215, 41)
        Me.CareLabel33.Name = "CareLabel33"
        Me.CareLabel33.Size = New System.Drawing.Size(8, 15)
        Me.CareLabel33.TabIndex = 6
        Me.CareLabel33.Text = "+"
        Me.CareLabel33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtStatSickness
        '
        Me.txtStatSickness.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtStatSickness.EnterMoveNextControl = True
        Me.txtStatSickness.Location = New System.Drawing.Point(147, 38)
        Me.txtStatSickness.MaxLength = 14
        Me.txtStatSickness.Name = "txtStatSickness"
        Me.txtStatSickness.NumericAllowNegatives = False
        Me.txtStatSickness.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtStatSickness.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStatSickness.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtStatSickness.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStatSickness.Properties.Appearance.Options.UseFont = True
        Me.txtStatSickness.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStatSickness.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtStatSickness.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtStatSickness.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtStatSickness.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtStatSickness.Properties.MaxLength = 14
        Me.txtStatSickness.Properties.ReadOnly = True
        Me.txtStatSickness.Size = New System.Drawing.Size(62, 22)
        Me.txtStatSickness.TabIndex = 5
        Me.txtStatSickness.Tag = ""
        Me.txtStatSickness.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtStatSickness.ToolTipText = ""
        '
        'txtStatTotal
        '
        Me.txtStatTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtStatTotal.EnterMoveNextControl = True
        Me.txtStatTotal.Location = New System.Drawing.Point(393, 38)
        Me.txtStatTotal.MaxLength = 14
        Me.txtStatTotal.Name = "txtStatTotal"
        Me.txtStatTotal.NumericAllowNegatives = False
        Me.txtStatTotal.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtStatTotal.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStatTotal.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtStatTotal.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStatTotal.Properties.Appearance.Options.UseFont = True
        Me.txtStatTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStatTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtStatTotal.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtStatTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtStatTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtStatTotal.Properties.MaxLength = 14
        Me.txtStatTotal.Properties.ReadOnly = True
        Me.txtStatTotal.Size = New System.Drawing.Size(62, 22)
        Me.txtStatTotal.TabIndex = 17
        Me.txtStatTotal.Tag = ""
        Me.txtStatTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtStatTotal.ToolTipText = ""
        '
        'txtStatHols
        '
        Me.txtStatHols.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtStatHols.EnterMoveNextControl = True
        Me.txtStatHols.Location = New System.Drawing.Point(229, 38)
        Me.txtStatHols.MaxLength = 14
        Me.txtStatHols.Name = "txtStatHols"
        Me.txtStatHols.NumericAllowNegatives = False
        Me.txtStatHols.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtStatHols.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStatHols.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtStatHols.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStatHols.Properties.Appearance.Options.UseFont = True
        Me.txtStatHols.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStatHols.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtStatHols.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtStatHols.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtStatHols.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtStatHols.Properties.MaxLength = 14
        Me.txtStatHols.Properties.ReadOnly = True
        Me.txtStatHols.Size = New System.Drawing.Size(62, 22)
        Me.txtStatHols.TabIndex = 15
        Me.txtStatHols.Tag = ""
        Me.txtStatHols.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtStatHols.ToolTipText = ""
        '
        'txtStatOther
        '
        Me.txtStatOther.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtStatOther.EnterMoveNextControl = True
        Me.txtStatOther.Location = New System.Drawing.Point(311, 38)
        Me.txtStatOther.MaxLength = 14
        Me.txtStatOther.Name = "txtStatOther"
        Me.txtStatOther.NumericAllowNegatives = False
        Me.txtStatOther.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtStatOther.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStatOther.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtStatOther.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStatOther.Properties.Appearance.Options.UseFont = True
        Me.txtStatOther.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStatOther.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtStatOther.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtStatOther.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtStatOther.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtStatOther.Properties.MaxLength = 14
        Me.txtStatOther.Properties.ReadOnly = True
        Me.txtStatOther.Size = New System.Drawing.Size(62, 22)
        Me.txtStatOther.TabIndex = 12
        Me.txtStatOther.Tag = ""
        Me.txtStatOther.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtStatOther.ToolTipText = ""
        '
        'lblDaysSince
        '
        Me.lblDaysSince.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDaysSince.Appearance.Options.UseFont = True
        Me.lblDaysSince.Appearance.Options.UseTextOptions = True
        Me.lblDaysSince.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblDaysSince.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblDaysSince.Location = New System.Drawing.Point(23, 39)
        Me.lblDaysSince.Name = "lblDaysSince"
        Me.lblDaysSince.Size = New System.Drawing.Size(118, 19)
        Me.lblDaysSince.TabIndex = 0
        Me.lblDaysSince.Text = "Days since January"
        Me.lblDaysSince.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupControl16
        '
        Me.GroupControl16.Controls.Add(Me.CareLabel32)
        Me.GroupControl16.Controls.Add(Me.txtHolsEntitlement)
        Me.GroupControl16.Controls.Add(Me.CareLabel31)
        Me.GroupControl16.Controls.Add(Me.cbxHolsFrom)
        Me.GroupControl16.Location = New System.Drawing.Point(385, 9)
        Me.GroupControl16.Name = "GroupControl16"
        Me.GroupControl16.ShowCaption = False
        Me.GroupControl16.Size = New System.Drawing.Size(411, 39)
        Me.GroupControl16.TabIndex = 1
        Me.GroupControl16.Text = "GroupControl16"
        '
        'CareLabel32
        '
        Me.CareLabel32.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel32.Appearance.Options.UseFont = True
        Me.CareLabel32.Appearance.Options.UseTextOptions = True
        Me.CareLabel32.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel32.Location = New System.Drawing.Point(228, 13)
        Me.CareLabel32.Name = "CareLabel32"
        Me.CareLabel32.Size = New System.Drawing.Size(105, 15)
        Me.CareLabel32.TabIndex = 2
        Me.CareLabel32.Text = "Holiday Entitlement"
        Me.CareLabel32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtHolsEntitlement
        '
        Me.txtHolsEntitlement.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtHolsEntitlement.EnterMoveNextControl = True
        Me.txtHolsEntitlement.Location = New System.Drawing.Point(339, 10)
        Me.txtHolsEntitlement.MaxLength = 14
        Me.txtHolsEntitlement.Name = "txtHolsEntitlement"
        Me.txtHolsEntitlement.NumericAllowNegatives = False
        Me.txtHolsEntitlement.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtHolsEntitlement.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtHolsEntitlement.Properties.AccessibleName = ""
        Me.txtHolsEntitlement.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtHolsEntitlement.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHolsEntitlement.Properties.Appearance.Options.UseFont = True
        Me.txtHolsEntitlement.Properties.Appearance.Options.UseTextOptions = True
        Me.txtHolsEntitlement.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtHolsEntitlement.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtHolsEntitlement.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtHolsEntitlement.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtHolsEntitlement.Properties.MaxLength = 14
        Me.txtHolsEntitlement.Size = New System.Drawing.Size(62, 22)
        Me.txtHolsEntitlement.TabIndex = 3
        Me.txtHolsEntitlement.Tag = "AE"
        Me.txtHolsEntitlement.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtHolsEntitlement.ToolTipText = ""
        '
        'CareLabel31
        '
        Me.CareLabel31.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel31.Appearance.Options.UseFont = True
        Me.CareLabel31.Appearance.Options.UseTextOptions = True
        Me.CareLabel31.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel31.Location = New System.Drawing.Point(10, 13)
        Me.CareLabel31.Name = "CareLabel31"
        Me.CareLabel31.Size = New System.Drawing.Size(73, 15)
        Me.CareLabel31.TabIndex = 0
        Me.CareLabel31.Text = "Holidays Start"
        Me.CareLabel31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxHolsFrom
        '
        Me.cbxHolsFrom.AllowBlank = False
        Me.cbxHolsFrom.DataSource = Nothing
        Me.cbxHolsFrom.DisplayMember = Nothing
        Me.cbxHolsFrom.EnterMoveNextControl = True
        Me.cbxHolsFrom.Location = New System.Drawing.Point(94, 10)
        Me.cbxHolsFrom.Name = "cbxHolsFrom"
        Me.cbxHolsFrom.Properties.AccessibleName = "Absence Type"
        Me.cbxHolsFrom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxHolsFrom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxHolsFrom.Properties.Appearance.Options.UseFont = True
        Me.cbxHolsFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxHolsFrom.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxHolsFrom.SelectedValue = Nothing
        Me.cbxHolsFrom.Size = New System.Drawing.Size(116, 22)
        Me.cbxHolsFrom.TabIndex = 1
        Me.cbxHolsFrom.Tag = "AE"
        Me.cbxHolsFrom.ValueMember = Nothing
        '
        'GroupControl15
        '
        Me.GroupControl15.Controls.Add(Me.lblBradfordScore)
        Me.GroupControl15.Controls.Add(Me.txtStatHolsLeft)
        Me.GroupControl15.Controls.Add(Me.txtStatBS)
        Me.GroupControl15.Controls.Add(Me.CareLabel35)
        Me.GroupControl15.Location = New System.Drawing.Point(11, 441)
        Me.GroupControl15.Name = "GroupControl15"
        Me.GroupControl15.ShowCaption = False
        Me.GroupControl15.Size = New System.Drawing.Size(315, 39)
        Me.GroupControl15.TabIndex = 3
        Me.GroupControl15.Text = "GroupControl15"
        '
        'lblBradfordScore
        '
        Me.lblBradfordScore.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBradfordScore.Appearance.Options.UseFont = True
        Me.lblBradfordScore.Appearance.Options.UseTextOptions = True
        Me.lblBradfordScore.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblBradfordScore.Location = New System.Drawing.Point(10, 13)
        Me.lblBradfordScore.Name = "lblBradfordScore"
        Me.lblBradfordScore.Size = New System.Drawing.Size(78, 15)
        Me.lblBradfordScore.TabIndex = 0
        Me.lblBradfordScore.Text = "Bradford Score"
        Me.lblBradfordScore.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtStatHolsLeft
        '
        Me.txtStatHolsLeft.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtStatHolsLeft.EnterMoveNextControl = True
        Me.txtStatHolsLeft.Location = New System.Drawing.Point(242, 10)
        Me.txtStatHolsLeft.MaxLength = 14
        Me.txtStatHolsLeft.Name = "txtStatHolsLeft"
        Me.txtStatHolsLeft.NumericAllowNegatives = True
        Me.txtStatHolsLeft.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtStatHolsLeft.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStatHolsLeft.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtStatHolsLeft.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStatHolsLeft.Properties.Appearance.Options.UseFont = True
        Me.txtStatHolsLeft.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStatHolsLeft.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtStatHolsLeft.Properties.Mask.EditMask = "###,###,##0.00"
        Me.txtStatHolsLeft.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtStatHolsLeft.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtStatHolsLeft.Properties.MaxLength = 14
        Me.txtStatHolsLeft.Properties.ReadOnly = True
        Me.txtStatHolsLeft.Size = New System.Drawing.Size(62, 22)
        Me.txtStatHolsLeft.TabIndex = 3
        Me.txtStatHolsLeft.Tag = ""
        Me.txtStatHolsLeft.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtStatHolsLeft.ToolTipText = ""
        '
        'txtStatBS
        '
        Me.txtStatBS.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtStatBS.EnterMoveNextControl = True
        Me.txtStatBS.Location = New System.Drawing.Point(94, 10)
        Me.txtStatBS.MaxLength = 60
        Me.txtStatBS.Name = "txtStatBS"
        Me.txtStatBS.NumericAllowNegatives = False
        Me.txtStatBS.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtStatBS.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStatBS.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtStatBS.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStatBS.Properties.Appearance.Options.UseFont = True
        Me.txtStatBS.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStatBS.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtStatBS.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtStatBS.Properties.MaxLength = 60
        Me.txtStatBS.Properties.ReadOnly = True
        Me.txtStatBS.Size = New System.Drawing.Size(62, 22)
        Me.txtStatBS.TabIndex = 1
        Me.txtStatBS.Tag = ""
        Me.txtStatBS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtStatBS.ToolTipText = ""
        '
        'CareLabel35
        '
        Me.CareLabel35.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel35.Appearance.Options.UseFont = True
        Me.CareLabel35.Appearance.Options.UseTextOptions = True
        Me.CareLabel35.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel35.Location = New System.Drawing.Point(167, 13)
        Me.CareLabel35.Name = "CareLabel35"
        Me.CareLabel35.Size = New System.Drawing.Size(69, 15)
        Me.CareLabel35.TabIndex = 2
        Me.CareLabel35.Text = "Holidays Left"
        Me.CareLabel35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cgAbs
        '
        Me.cgAbs.ButtonsEnabled = False
        Me.cgAbs.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgAbs.HideFirstColumn = False
        Me.cgAbs.Location = New System.Drawing.Point(11, 54)
        Me.cgAbs.Name = "cgAbs"
        Me.cgAbs.PreviewColumn = ""
        Me.cgAbs.Size = New System.Drawing.Size(785, 381)
        Me.cgAbs.TabIndex = 2
        '
        'GroupControl7
        '
        Me.GroupControl7.Controls.Add(Me.chkAbsenceNotes)
        Me.GroupControl7.Controls.Add(Me.CareLabel18)
        Me.GroupControl7.Controls.Add(Me.cbxAbsenceType)
        Me.GroupControl7.Location = New System.Drawing.Point(11, 9)
        Me.GroupControl7.Name = "GroupControl7"
        Me.GroupControl7.ShowCaption = False
        Me.GroupControl7.Size = New System.Drawing.Size(368, 39)
        Me.GroupControl7.TabIndex = 0
        Me.GroupControl7.Text = "GroupControl7"
        '
        'chkAbsenceNotes
        '
        Me.chkAbsenceNotes.EnterMoveNextControl = True
        Me.chkAbsenceNotes.Location = New System.Drawing.Point(277, 11)
        Me.chkAbsenceNotes.Name = "chkAbsenceNotes"
        Me.chkAbsenceNotes.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAbsenceNotes.Properties.Appearance.Options.UseFont = True
        Me.chkAbsenceNotes.Properties.Caption = "Show Notes"
        Me.chkAbsenceNotes.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.chkAbsenceNotes.Size = New System.Drawing.Size(86, 19)
        Me.chkAbsenceNotes.TabIndex = 2
        Me.chkAbsenceNotes.Tag = ""
        '
        'CareLabel18
        '
        Me.CareLabel18.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel18.Appearance.Options.UseFont = True
        Me.CareLabel18.Appearance.Options.UseTextOptions = True
        Me.CareLabel18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel18.Location = New System.Drawing.Point(10, 13)
        Me.CareLabel18.Name = "CareLabel18"
        Me.CareLabel18.Size = New System.Drawing.Size(74, 15)
        Me.CareLabel18.TabIndex = 0
        Me.CareLabel18.Text = "Absence Type"
        Me.CareLabel18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxAbsenceType
        '
        Me.cbxAbsenceType.AllowBlank = False
        Me.cbxAbsenceType.DataSource = Nothing
        Me.cbxAbsenceType.DisplayMember = Nothing
        Me.cbxAbsenceType.EnterMoveNextControl = True
        Me.cbxAbsenceType.Location = New System.Drawing.Point(94, 10)
        Me.cbxAbsenceType.Name = "cbxAbsenceType"
        Me.cbxAbsenceType.Properties.AccessibleName = "Absence Type"
        Me.cbxAbsenceType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxAbsenceType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxAbsenceType.Properties.Appearance.Options.UseFont = True
        Me.cbxAbsenceType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxAbsenceType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxAbsenceType.SelectedValue = Nothing
        Me.cbxAbsenceType.Size = New System.Drawing.Size(158, 22)
        Me.cbxAbsenceType.TabIndex = 1
        Me.cbxAbsenceType.Tag = ""
        Me.cbxAbsenceType.ValueMember = Nothing
        '
        'tabApps
        '
        Me.tabApps.Controls.Add(Me.GroupControl5)
        Me.tabApps.Controls.Add(Me.GroupControl6)
        Me.tabApps.Name = "tabApps"
        Me.tabApps.Size = New System.Drawing.Size(807, 487)
        Me.tabApps.Text = "Performance"
        '
        'GroupControl5
        '
        Me.GroupControl5.Controls.Add(Me.cgDis)
        Me.GroupControl5.Location = New System.Drawing.Point(11, 243)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(785, 236)
        Me.GroupControl5.TabIndex = 3
        Me.GroupControl5.Text = "Disciplinary Actions"
        '
        'cgDis
        '
        Me.cgDis.ButtonsEnabled = False
        Me.cgDis.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgDis.HideFirstColumn = False
        Me.cgDis.Location = New System.Drawing.Point(6, 26)
        Me.cgDis.Name = "cgDis"
        Me.cgDis.PreviewColumn = ""
        Me.cgDis.Size = New System.Drawing.Size(774, 204)
        Me.cgDis.TabIndex = 1
        '
        'GroupControl6
        '
        Me.GroupControl6.Controls.Add(Me.cgAppraisals)
        Me.GroupControl6.Location = New System.Drawing.Point(11, 9)
        Me.GroupControl6.Name = "GroupControl6"
        Me.GroupControl6.Size = New System.Drawing.Size(785, 228)
        Me.GroupControl6.TabIndex = 2
        Me.GroupControl6.Text = "Appraisals"
        '
        'cgAppraisals
        '
        Me.cgAppraisals.ButtonsEnabled = False
        Me.cgAppraisals.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgAppraisals.HideFirstColumn = False
        Me.cgAppraisals.Location = New System.Drawing.Point(6, 26)
        Me.cgAppraisals.Name = "cgAppraisals"
        Me.cgAppraisals.PreviewColumn = ""
        Me.cgAppraisals.Size = New System.Drawing.Size(774, 196)
        Me.cgAppraisals.TabIndex = 0
        '
        'tabQualsTrain
        '
        Me.tabQualsTrain.Controls.Add(Me.GroupControl4)
        Me.tabQualsTrain.Controls.Add(Me.GroupControl3)
        Me.tabQualsTrain.Name = "tabQualsTrain"
        Me.tabQualsTrain.Size = New System.Drawing.Size(807, 487)
        Me.tabQualsTrain.Text = "Personal Development"
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.cgQuals)
        Me.GroupControl4.Location = New System.Drawing.Point(11, 251)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(785, 228)
        Me.GroupControl4.TabIndex = 1
        Me.GroupControl4.Text = "Qualifications"
        '
        'cgQuals
        '
        Me.cgQuals.ButtonsEnabled = False
        Me.cgQuals.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgQuals.HideFirstColumn = False
        Me.cgQuals.Location = New System.Drawing.Point(6, 26)
        Me.cgQuals.Name = "cgQuals"
        Me.cgQuals.PreviewColumn = ""
        Me.cgQuals.Size = New System.Drawing.Size(774, 197)
        Me.cgQuals.TabIndex = 1
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.cgTraining)
        Me.GroupControl3.Location = New System.Drawing.Point(11, 9)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(785, 236)
        Me.GroupControl3.TabIndex = 0
        Me.GroupControl3.Text = "Training History"
        '
        'cgTraining
        '
        Me.cgTraining.ButtonsEnabled = False
        Me.cgTraining.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgTraining.HideFirstColumn = False
        Me.cgTraining.Location = New System.Drawing.Point(6, 26)
        Me.cgTraining.Name = "cgTraining"
        Me.cgTraining.PreviewColumn = ""
        Me.cgTraining.Size = New System.Drawing.Size(774, 204)
        Me.cgTraining.TabIndex = 1
        '
        'tabCar
        '
        Me.tabCar.Controls.Add(Me.GroupBox5)
        Me.tabCar.Controls.Add(Me.gbxPaxton)
        Me.tabCar.Controls.Add(Me.GroupBox4)
        Me.tabCar.Controls.Add(Me.GroupBox6)
        Me.tabCar.Controls.Add(Me.GroupBox2)
        Me.tabCar.Name = "tabCar"
        Me.tabCar.Size = New System.Drawing.Size(807, 487)
        Me.tabCar.Text = "DBS && Access Control"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.txtTelHome)
        Me.GroupBox5.Controls.Add(Me.txtTelMobile)
        Me.GroupBox5.Controls.Add(Me.txtEmail)
        Me.GroupBox5.Controls.Add(Me.CareLabel1)
        Me.GroupBox5.Controls.Add(Me.txtAddress)
        Me.GroupBox5.Controls.Add(Me.Label25)
        Me.GroupBox5.Controls.Add(Me.Label23)
        Me.GroupBox5.Controls.Add(Me.Label24)
        Me.GroupBox5.Location = New System.Drawing.Point(453, 9)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(338, 225)
        Me.GroupBox5.TabIndex = 3
        Me.GroupBox5.Text = "Address && Contact Numbers"
        '
        'txtTelHome
        '
        Me.txtTelHome.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTelHome.Appearance.Options.UseFont = True
        Me.txtTelHome.IsMobile = False
        Me.txtTelHome.Location = New System.Drawing.Point(114, 136)
        Me.txtTelHome.MaxLength = 15
        Me.txtTelHome.MinimumSize = New System.Drawing.Size(0, 22)
        Me.txtTelHome.Name = "txtTelHome"
        Me.txtTelHome.ReadOnly = False
        Me.txtTelHome.Size = New System.Drawing.Size(213, 22)
        Me.txtTelHome.TabIndex = 1
        Me.txtTelHome.Tag = "AE"
        '
        'txtTelMobile
        '
        Me.txtTelMobile.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTelMobile.Appearance.Options.UseFont = True
        Me.txtTelMobile.IsMobile = True
        Me.txtTelMobile.Location = New System.Drawing.Point(114, 165)
        Me.txtTelMobile.MaxLength = 15
        Me.txtTelMobile.MinimumSize = New System.Drawing.Size(0, 22)
        Me.txtTelMobile.Name = "txtTelMobile"
        Me.txtTelMobile.ReadOnly = False
        Me.txtTelMobile.Size = New System.Drawing.Size(213, 22)
        Me.txtTelMobile.TabIndex = 2
        Me.txtTelMobile.Tag = "AE"
        '
        'txtEmail
        '
        Me.txtEmail.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail.Appearance.Options.UseFont = True
        Me.txtEmail.Location = New System.Drawing.Point(114, 193)
        Me.txtEmail.MaxLength = 250
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.NoButton = False
        Me.txtEmail.NoColours = False
        Me.txtEmail.NoValidate = False
        Me.txtEmail.ReadOnly = False
        Me.txtEmail.Size = New System.Drawing.Size(213, 22)
        Me.txtEmail.TabIndex = 3
        Me.txtEmail.Tag = "AE"
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel1.Appearance.Options.UseFont = True
        Me.CareLabel1.Appearance.Options.UseTextOptions = True
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(8, 196)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(74, 15)
        Me.CareLabel1.TabIndex = 14
        Me.CareLabel1.Text = "Email Address"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAddress
        '
        Me.txtAddress.Address = ""
        Me.txtAddress.AddressBlock = ""
        Me.txtAddress.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.Appearance.Options.UseFont = True
        Me.txtAddress.County = ""
        Me.txtAddress.DisplayMode = Care.Address.CareAddress.EnumDisplayMode.SingleControl
        Me.txtAddress.Location = New System.Drawing.Point(114, 28)
        Me.txtAddress.MaxLength = 500
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.PostCode = ""
        Me.txtAddress.ReadOnly = False
        Me.txtAddress.Size = New System.Drawing.Size(213, 94)
        Me.txtAddress.TabIndex = 0
        Me.txtAddress.Tag = "AE"
        Me.txtAddress.Town = ""
        '
        'Label25
        '
        Me.Label25.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Appearance.Options.UseFont = True
        Me.Label25.Appearance.Options.UseTextOptions = True
        Me.Label25.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label25.Location = New System.Drawing.Point(8, 139)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(52, 15)
        Me.Label25.TabIndex = 12
        Me.Label25.Text = "Home Tel"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label23
        '
        Me.Label23.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Appearance.Options.UseFont = True
        Me.Label23.Appearance.Options.UseTextOptions = True
        Me.Label23.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label23.Location = New System.Drawing.Point(8, 168)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(56, 15)
        Me.Label23.TabIndex = 10
        Me.Label23.Text = "Mobile Tel"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label24
        '
        Me.Label24.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Appearance.Options.UseFont = True
        Me.Label24.Appearance.Options.UseTextOptions = True
        Me.Label24.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label24.Location = New System.Drawing.Point(8, 28)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(42, 15)
        Me.Label24.TabIndex = 0
        Me.Label24.Text = "Address"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbxPaxton
        '
        Me.gbxPaxton.Controls.Add(Me.cbxPaxtonDepartment)
        Me.gbxPaxton.Controls.Add(Me.btnPaxtonLink)
        Me.gbxPaxton.Controls.Add(Me.btnPaxtonDisable)
        Me.gbxPaxton.Controls.Add(Me.cbxPaxtonAccessLevel)
        Me.gbxPaxton.Controls.Add(Me.btnPaxtonUpdate)
        Me.gbxPaxton.Controls.Add(Me.CareLabel8)
        Me.gbxPaxton.Controls.Add(Me.CareLabel9)
        Me.gbxPaxton.Controls.Add(Me.txtPaxtonUserID)
        Me.gbxPaxton.Controls.Add(Me.CareLabel11)
        Me.gbxPaxton.Location = New System.Drawing.Point(453, 240)
        Me.gbxPaxton.Name = "gbxPaxton"
        Me.gbxPaxton.Size = New System.Drawing.Size(338, 178)
        Me.gbxPaxton.TabIndex = 4
        Me.gbxPaxton.Text = "Paxton Access Control"
        '
        'cbxPaxtonDepartment
        '
        Me.cbxPaxtonDepartment.AllowBlank = False
        Me.cbxPaxtonDepartment.DataSource = Nothing
        Me.cbxPaxtonDepartment.DisplayMember = Nothing
        Me.cbxPaxtonDepartment.EnterMoveNextControl = True
        Me.cbxPaxtonDepartment.Location = New System.Drawing.Point(114, 84)
        Me.cbxPaxtonDepartment.Name = "cbxPaxtonDepartment"
        Me.cbxPaxtonDepartment.Properties.AccessibleName = "Status"
        Me.cbxPaxtonDepartment.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxPaxtonDepartment.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxPaxtonDepartment.Properties.Appearance.Options.UseFont = True
        Me.cbxPaxtonDepartment.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxPaxtonDepartment.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxPaxtonDepartment.SelectedValue = Nothing
        Me.cbxPaxtonDepartment.Size = New System.Drawing.Size(213, 22)
        Me.cbxPaxtonDepartment.TabIndex = 6
        Me.cbxPaxtonDepartment.Tag = "AE"
        Me.cbxPaxtonDepartment.ValueMember = Nothing
        '
        'btnPaxtonLink
        '
        Me.btnPaxtonLink.Location = New System.Drawing.Point(196, 28)
        Me.btnPaxtonLink.Name = "btnPaxtonLink"
        Me.btnPaxtonLink.Size = New System.Drawing.Size(132, 21)
        Me.btnPaxtonLink.TabIndex = 2
        Me.btnPaxtonLink.Text = "Find Paxton User"
        '
        'btnPaxtonDisable
        '
        Me.btnPaxtonDisable.Location = New System.Drawing.Point(60, 147)
        Me.btnPaxtonDisable.Name = "btnPaxtonDisable"
        Me.btnPaxtonDisable.Size = New System.Drawing.Size(131, 23)
        Me.btnPaxtonDisable.TabIndex = 7
        Me.btnPaxtonDisable.Text = "Disable User"
        '
        'cbxPaxtonAccessLevel
        '
        Me.cbxPaxtonAccessLevel.AllowBlank = False
        Me.cbxPaxtonAccessLevel.DataSource = Nothing
        Me.cbxPaxtonAccessLevel.DisplayMember = Nothing
        Me.cbxPaxtonAccessLevel.EnterMoveNextControl = True
        Me.cbxPaxtonAccessLevel.Location = New System.Drawing.Point(114, 56)
        Me.cbxPaxtonAccessLevel.Name = "cbxPaxtonAccessLevel"
        Me.cbxPaxtonAccessLevel.Properties.AccessibleName = "Status"
        Me.cbxPaxtonAccessLevel.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxPaxtonAccessLevel.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxPaxtonAccessLevel.Properties.Appearance.Options.UseFont = True
        Me.cbxPaxtonAccessLevel.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxPaxtonAccessLevel.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxPaxtonAccessLevel.SelectedValue = Nothing
        Me.cbxPaxtonAccessLevel.Size = New System.Drawing.Size(214, 22)
        Me.cbxPaxtonAccessLevel.TabIndex = 4
        Me.cbxPaxtonAccessLevel.Tag = "AE"
        Me.cbxPaxtonAccessLevel.ValueMember = Nothing
        '
        'btnPaxtonUpdate
        '
        Me.btnPaxtonUpdate.Location = New System.Drawing.Point(197, 147)
        Me.btnPaxtonUpdate.Name = "btnPaxtonUpdate"
        Me.btnPaxtonUpdate.Size = New System.Drawing.Size(131, 23)
        Me.btnPaxtonUpdate.TabIndex = 8
        Me.btnPaxtonUpdate.Text = "Update Paxton"
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel8.Appearance.Options.UseFont = True
        Me.CareLabel8.Appearance.Options.UseTextOptions = True
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(11, 59)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(66, 15)
        Me.CareLabel8.TabIndex = 3
        Me.CareLabel8.Text = "Access Level"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel9.Appearance.Options.UseFont = True
        Me.CareLabel9.Appearance.Options.UseTextOptions = True
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(11, 87)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(63, 15)
        Me.CareLabel9.TabIndex = 5
        Me.CareLabel9.Text = "Department"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPaxtonUserID
        '
        Me.txtPaxtonUserID.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtPaxtonUserID.EnterMoveNextControl = True
        Me.txtPaxtonUserID.Location = New System.Drawing.Point(114, 28)
        Me.txtPaxtonUserID.MaxLength = 4
        Me.txtPaxtonUserID.Name = "txtPaxtonUserID"
        Me.txtPaxtonUserID.NumericAllowNegatives = False
        Me.txtPaxtonUserID.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtPaxtonUserID.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPaxtonUserID.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPaxtonUserID.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPaxtonUserID.Properties.Appearance.Options.UseFont = True
        Me.txtPaxtonUserID.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPaxtonUserID.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPaxtonUserID.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPaxtonUserID.Properties.MaxLength = 4
        Me.txtPaxtonUserID.Properties.ReadOnly = True
        Me.txtPaxtonUserID.Size = New System.Drawing.Size(75, 22)
        Me.txtPaxtonUserID.TabIndex = 1
        Me.txtPaxtonUserID.Tag = ""
        Me.txtPaxtonUserID.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPaxtonUserID.ToolTipText = ""
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel11.Appearance.Options.UseFont = True
        Me.CareLabel11.Appearance.Options.UseTextOptions = True
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(11, 31)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(37, 15)
        Me.CareLabel11.TabIndex = 0
        Me.CareLabel11.Text = "User ID"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.chkCar)
        Me.GroupBox4.Controls.Add(Me.chkDriver)
        Me.GroupBox4.Controls.Add(Me.Label11)
        Me.GroupBox4.Controls.Add(Me.Label17)
        Me.GroupBox4.Controls.Add(Me.udtInsCheck)
        Me.GroupBox4.Controls.Add(Me.txtMakeModel)
        Me.GroupBox4.Controls.Add(Me.Label20)
        Me.GroupBox4.Controls.Add(Me.txtReg)
        Me.GroupBox4.Controls.Add(Me.Label21)
        Me.GroupBox4.Controls.Add(Me.Label22)
        Me.GroupBox4.Location = New System.Drawing.Point(11, 187)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(436, 136)
        Me.GroupBox4.TabIndex = 1
        Me.GroupBox4.Text = "Driving && Vehicle Details"
        '
        'chkCar
        '
        Me.chkCar.EnterMoveNextControl = True
        Me.chkCar.Location = New System.Drawing.Point(398, 27)
        Me.chkCar.Name = "chkCar"
        Me.chkCar.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCar.Properties.Appearance.Options.UseFont = True
        Me.chkCar.Size = New System.Drawing.Size(20, 19)
        Me.chkCar.TabIndex = 1
        Me.chkCar.Tag = "AE"
        '
        'chkDriver
        '
        Me.chkDriver.EnterMoveNextControl = True
        Me.chkDriver.Location = New System.Drawing.Point(111, 27)
        Me.chkDriver.Name = "chkDriver"
        Me.chkDriver.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDriver.Properties.Appearance.Options.UseFont = True
        Me.chkDriver.Size = New System.Drawing.Size(20, 19)
        Me.chkDriver.TabIndex = 0
        Me.chkDriver.Tag = "AE"
        '
        'Label11
        '
        Me.Label11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Appearance.Options.UseFont = True
        Me.Label11.Appearance.Options.UseTextOptions = True
        Me.Label11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label11.Location = New System.Drawing.Point(336, 29)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(56, 15)
        Me.Label11.TabIndex = 14
        Me.Label11.Text = "Car Owner"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label17
        '
        Me.Label17.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Appearance.Options.UseFont = True
        Me.Label17.Appearance.Options.UseTextOptions = True
        Me.Label17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label17.Location = New System.Drawing.Point(8, 111)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(87, 15)
        Me.Label17.TabIndex = 12
        Me.Label17.Text = "Insurance Check"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'udtInsCheck
        '
        Me.udtInsCheck.EditValue = New Date(2012, 6, 30, 0, 0, 0, 0)
        Me.udtInsCheck.EnterMoveNextControl = True
        Me.udtInsCheck.Location = New System.Drawing.Point(114, 108)
        Me.udtInsCheck.Name = "udtInsCheck"
        Me.udtInsCheck.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.udtInsCheck.Properties.Appearance.Options.UseFont = True
        Me.udtInsCheck.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.udtInsCheck.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.udtInsCheck.Size = New System.Drawing.Size(110, 22)
        Me.udtInsCheck.TabIndex = 4
        Me.udtInsCheck.Tag = "AE"
        Me.udtInsCheck.Value = New Date(2012, 6, 30, 0, 0, 0, 0)
        '
        'txtMakeModel
        '
        Me.txtMakeModel.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtMakeModel.EnterMoveNextControl = True
        Me.txtMakeModel.Location = New System.Drawing.Point(114, 80)
        Me.txtMakeModel.MaxLength = 60
        Me.txtMakeModel.Name = "txtMakeModel"
        Me.txtMakeModel.NumericAllowNegatives = False
        Me.txtMakeModel.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtMakeModel.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtMakeModel.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtMakeModel.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMakeModel.Properties.Appearance.Options.UseFont = True
        Me.txtMakeModel.Properties.Appearance.Options.UseTextOptions = True
        Me.txtMakeModel.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtMakeModel.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtMakeModel.Properties.MaxLength = 60
        Me.txtMakeModel.Size = New System.Drawing.Size(304, 22)
        Me.txtMakeModel.TabIndex = 3
        Me.txtMakeModel.Tag = "AET"
        Me.txtMakeModel.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtMakeModel.ToolTipText = ""
        '
        'Label20
        '
        Me.Label20.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Appearance.Options.UseFont = True
        Me.Label20.Appearance.Options.UseTextOptions = True
        Me.Label20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label20.Location = New System.Drawing.Point(8, 83)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(74, 15)
        Me.Label20.TabIndex = 4
        Me.Label20.Text = "Make / Model"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtReg
        '
        Me.txtReg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReg.EnterMoveNextControl = True
        Me.txtReg.Location = New System.Drawing.Point(113, 52)
        Me.txtReg.MaxLength = 10
        Me.txtReg.Name = "txtReg"
        Me.txtReg.NumericAllowNegatives = False
        Me.txtReg.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtReg.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtReg.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtReg.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReg.Properties.Appearance.Options.UseFont = True
        Me.txtReg.Properties.Appearance.Options.UseTextOptions = True
        Me.txtReg.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtReg.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReg.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtReg.Properties.MaxLength = 10
        Me.txtReg.Size = New System.Drawing.Size(99, 22)
        Me.txtReg.TabIndex = 2
        Me.txtReg.Tag = "AE"
        Me.txtReg.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtReg.ToolTipText = ""
        '
        'Label21
        '
        Me.Label21.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Appearance.Options.UseFont = True
        Me.Label21.Appearance.Options.UseTextOptions = True
        Me.Label21.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label21.Location = New System.Drawing.Point(8, 55)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(63, 15)
        Me.Label21.TabIndex = 2
        Me.Label21.Text = "Registration"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label22
        '
        Me.Label22.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Appearance.Options.UseFont = True
        Me.Label22.Appearance.Options.UseTextOptions = True
        Me.Label22.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label22.Location = New System.Drawing.Point(8, 27)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(52, 15)
        Me.Label22.TabIndex = 0
        Me.Label22.Text = "Car Driver"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.txtICEMobile)
        Me.GroupBox6.Controls.Add(Me.Label27)
        Me.GroupBox6.Controls.Add(Me.txtICEHome)
        Me.GroupBox6.Controls.Add(Me.Label30)
        Me.GroupBox6.Controls.Add(Me.txtICERel)
        Me.GroupBox6.Controls.Add(Me.Label31)
        Me.GroupBox6.Controls.Add(Me.txtICEName)
        Me.GroupBox6.Controls.Add(Me.Label32)
        Me.GroupBox6.Location = New System.Drawing.Point(11, 329)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(436, 145)
        Me.GroupBox6.TabIndex = 2
        Me.GroupBox6.Text = "In Case of Emergency"
        '
        'txtICEMobile
        '
        Me.txtICEMobile.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtICEMobile.EnterMoveNextControl = True
        Me.txtICEMobile.Location = New System.Drawing.Point(113, 114)
        Me.txtICEMobile.MaxLength = 15
        Me.txtICEMobile.Name = "txtICEMobile"
        Me.txtICEMobile.NumericAllowNegatives = False
        Me.txtICEMobile.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtICEMobile.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtICEMobile.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtICEMobile.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtICEMobile.Properties.Appearance.Options.UseFont = True
        Me.txtICEMobile.Properties.Appearance.Options.UseTextOptions = True
        Me.txtICEMobile.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtICEMobile.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtICEMobile.Properties.MaxLength = 15
        Me.txtICEMobile.Size = New System.Drawing.Size(304, 22)
        Me.txtICEMobile.TabIndex = 3
        Me.txtICEMobile.Tag = "AEN"
        Me.txtICEMobile.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtICEMobile.ToolTipText = ""
        '
        'Label27
        '
        Me.Label27.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Appearance.Options.UseFont = True
        Me.Label27.Appearance.Options.UseTextOptions = True
        Me.Label27.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label27.Location = New System.Drawing.Point(8, 117)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(56, 15)
        Me.Label27.TabIndex = 12
        Me.Label27.Text = "Mobile Tel"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtICEHome
        '
        Me.txtICEHome.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtICEHome.EnterMoveNextControl = True
        Me.txtICEHome.Location = New System.Drawing.Point(113, 86)
        Me.txtICEHome.MaxLength = 15
        Me.txtICEHome.Name = "txtICEHome"
        Me.txtICEHome.NumericAllowNegatives = False
        Me.txtICEHome.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtICEHome.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtICEHome.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtICEHome.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtICEHome.Properties.Appearance.Options.UseFont = True
        Me.txtICEHome.Properties.Appearance.Options.UseTextOptions = True
        Me.txtICEHome.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtICEHome.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtICEHome.Properties.MaxLength = 15
        Me.txtICEHome.Size = New System.Drawing.Size(304, 22)
        Me.txtICEHome.TabIndex = 2
        Me.txtICEHome.Tag = "AEN"
        Me.txtICEHome.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtICEHome.ToolTipText = ""
        '
        'Label30
        '
        Me.Label30.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Appearance.Options.UseFont = True
        Me.Label30.Appearance.Options.UseTextOptions = True
        Me.Label30.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label30.Location = New System.Drawing.Point(8, 89)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(52, 15)
        Me.Label30.TabIndex = 4
        Me.Label30.Text = "Home Tel"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtICERel
        '
        Me.txtICERel.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtICERel.EnterMoveNextControl = True
        Me.txtICERel.Location = New System.Drawing.Point(113, 58)
        Me.txtICERel.MaxLength = 30
        Me.txtICERel.Name = "txtICERel"
        Me.txtICERel.NumericAllowNegatives = False
        Me.txtICERel.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtICERel.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtICERel.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtICERel.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtICERel.Properties.Appearance.Options.UseFont = True
        Me.txtICERel.Properties.Appearance.Options.UseTextOptions = True
        Me.txtICERel.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtICERel.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtICERel.Properties.MaxLength = 30
        Me.txtICERel.Size = New System.Drawing.Size(304, 22)
        Me.txtICERel.TabIndex = 1
        Me.txtICERel.Tag = "AET"
        Me.txtICERel.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtICERel.ToolTipText = ""
        '
        'Label31
        '
        Me.Label31.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Appearance.Options.UseFont = True
        Me.Label31.Appearance.Options.UseTextOptions = True
        Me.Label31.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label31.Location = New System.Drawing.Point(8, 61)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(65, 15)
        Me.Label31.TabIndex = 2
        Me.Label31.Text = "Relationship"
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtICEName
        '
        Me.txtICEName.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtICEName.EnterMoveNextControl = True
        Me.txtICEName.Location = New System.Drawing.Point(114, 30)
        Me.txtICEName.MaxLength = 30
        Me.txtICEName.Name = "txtICEName"
        Me.txtICEName.NumericAllowNegatives = False
        Me.txtICEName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtICEName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtICEName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtICEName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtICEName.Properties.Appearance.Options.UseFont = True
        Me.txtICEName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtICEName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtICEName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtICEName.Properties.MaxLength = 30
        Me.txtICEName.Size = New System.Drawing.Size(304, 22)
        Me.txtICEName.TabIndex = 0
        Me.txtICEName.Tag = "AET"
        Me.txtICEName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtICEName.ToolTipText = ""
        '
        'Label32
        '
        Me.Label32.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Appearance.Options.UseFont = True
        Me.Label32.Appearance.Options.UseTextOptions = True
        Me.Label32.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label32.Location = New System.Drawing.Point(8, 33)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(32, 15)
        Me.Label32.TabIndex = 0
        Me.Label32.Text = "Name"
        Me.Label32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.CareLabel42)
        Me.GroupBox2.Controls.Add(Me.txtDBSPosition)
        Me.GroupBox2.Controls.Add(Me.CareLabel40)
        Me.GroupBox2.Controls.Add(Me.CareLabel30)
        Me.GroupBox2.Controls.Add(Me.cbxDBSCheckType)
        Me.GroupBox2.Controls.Add(Me.cdtDBSIssued)
        Me.GroupBox2.Controls.Add(Me.CareLabel17)
        Me.GroupBox2.Controls.Add(Me.txtCRBRef)
        Me.GroupBox2.Controls.Add(Me.udtCRBDue)
        Me.GroupBox2.Controls.Add(Me.udtCRBLast)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Location = New System.Drawing.Point(11, 9)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(436, 172)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.Text = "DBS Checks"
        '
        'CareLabel42
        '
        Me.CareLabel42.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel42.Appearance.Options.UseFont = True
        Me.CareLabel42.Appearance.Options.UseTextOptions = True
        Me.CareLabel42.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel42.Location = New System.Drawing.Point(8, 60)
        Me.CareLabel42.Name = "CareLabel42"
        Me.CareLabel42.Size = New System.Drawing.Size(60, 15)
        Me.CareLabel42.TabIndex = 4
        Me.CareLabel42.Text = "Date Issued"
        Me.CareLabel42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDBSPosition
        '
        Me.txtDBSPosition.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtDBSPosition.EnterMoveNextControl = True
        Me.txtDBSPosition.Location = New System.Drawing.Point(114, 141)
        Me.txtDBSPosition.MaxLength = 100
        Me.txtDBSPosition.Name = "txtDBSPosition"
        Me.txtDBSPosition.NumericAllowNegatives = False
        Me.txtDBSPosition.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtDBSPosition.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDBSPosition.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtDBSPosition.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDBSPosition.Properties.Appearance.Options.UseFont = True
        Me.txtDBSPosition.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDBSPosition.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtDBSPosition.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDBSPosition.Properties.MaxLength = 100
        Me.txtDBSPosition.Size = New System.Drawing.Size(303, 22)
        Me.txtDBSPosition.TabIndex = 11
        Me.txtDBSPosition.Tag = "AE"
        Me.txtDBSPosition.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDBSPosition.ToolTipText = ""
        '
        'CareLabel40
        '
        Me.CareLabel40.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel40.Appearance.Options.UseFont = True
        Me.CareLabel40.Appearance.Options.UseTextOptions = True
        Me.CareLabel40.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel40.Location = New System.Drawing.Point(9, 144)
        Me.CareLabel40.Name = "CareLabel40"
        Me.CareLabel40.Size = New System.Drawing.Size(98, 15)
        Me.CareLabel40.TabIndex = 10
        Me.CareLabel40.Text = "Position requested"
        Me.CareLabel40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel40.ToolTip = "The position for which the certificate was requested"
        '
        'CareLabel30
        '
        Me.CareLabel30.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel30.Appearance.Options.UseFont = True
        Me.CareLabel30.Appearance.Options.UseTextOptions = True
        Me.CareLabel30.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel30.Location = New System.Drawing.Point(8, 116)
        Me.CareLabel30.Name = "CareLabel30"
        Me.CareLabel30.Size = New System.Drawing.Size(99, 15)
        Me.CareLabel30.TabIndex = 8
        Me.CareLabel30.Text = "Reference Number"
        Me.CareLabel30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxDBSCheckType
        '
        Me.cbxDBSCheckType.AllowBlank = False
        Me.cbxDBSCheckType.DataSource = Nothing
        Me.cbxDBSCheckType.DisplayMember = Nothing
        Me.cbxDBSCheckType.EnterMoveNextControl = True
        Me.cbxDBSCheckType.Location = New System.Drawing.Point(114, 85)
        Me.cbxDBSCheckType.Name = "cbxDBSCheckType"
        Me.cbxDBSCheckType.Properties.AccessibleName = "Group"
        Me.cbxDBSCheckType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxDBSCheckType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxDBSCheckType.Properties.Appearance.Options.UseFont = True
        Me.cbxDBSCheckType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxDBSCheckType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxDBSCheckType.SelectedValue = Nothing
        Me.cbxDBSCheckType.Size = New System.Drawing.Size(303, 22)
        Me.cbxDBSCheckType.TabIndex = 7
        Me.cbxDBSCheckType.Tag = "AE"
        Me.cbxDBSCheckType.ValueMember = Nothing
        '
        'cdtDBSIssued
        '
        Me.cdtDBSIssued.EditValue = New Date(2012, 6, 30, 0, 0, 0, 0)
        Me.cdtDBSIssued.EnterMoveNextControl = True
        Me.cdtDBSIssued.Location = New System.Drawing.Point(114, 57)
        Me.cdtDBSIssued.Name = "cdtDBSIssued"
        Me.cdtDBSIssued.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cdtDBSIssued.Properties.Appearance.Options.UseFont = True
        Me.cdtDBSIssued.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDBSIssued.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtDBSIssued.Size = New System.Drawing.Size(110, 22)
        Me.cdtDBSIssued.TabIndex = 5
        Me.cdtDBSIssued.Tag = "AE"
        Me.cdtDBSIssued.Value = New Date(2012, 6, 30, 0, 0, 0, 0)
        '
        'CareLabel17
        '
        Me.CareLabel17.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel17.Appearance.Options.UseFont = True
        Me.CareLabel17.Appearance.Options.UseTextOptions = True
        Me.CareLabel17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel17.Location = New System.Drawing.Point(8, 88)
        Me.CareLabel17.Name = "CareLabel17"
        Me.CareLabel17.Size = New System.Drawing.Size(86, 15)
        Me.CareLabel17.TabIndex = 6
        Me.CareLabel17.Text = "DBS Check Type"
        Me.CareLabel17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCRBRef
        '
        Me.txtCRBRef.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCRBRef.EnterMoveNextControl = True
        Me.txtCRBRef.Location = New System.Drawing.Point(114, 113)
        Me.txtCRBRef.MaxLength = 20
        Me.txtCRBRef.Name = "txtCRBRef"
        Me.txtCRBRef.NumericAllowNegatives = False
        Me.txtCRBRef.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtCRBRef.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtCRBRef.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtCRBRef.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCRBRef.Properties.Appearance.Options.UseFont = True
        Me.txtCRBRef.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCRBRef.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtCRBRef.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCRBRef.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtCRBRef.Properties.MaxLength = 20
        Me.txtCRBRef.Size = New System.Drawing.Size(303, 22)
        Me.txtCRBRef.TabIndex = 9
        Me.txtCRBRef.Tag = "AE"
        Me.txtCRBRef.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCRBRef.ToolTipText = ""
        '
        'udtCRBDue
        '
        Me.udtCRBDue.EditValue = New Date(2012, 6, 30, 0, 0, 0, 0)
        Me.udtCRBDue.EnterMoveNextControl = True
        Me.udtCRBDue.Location = New System.Drawing.Point(308, 29)
        Me.udtCRBDue.Name = "udtCRBDue"
        Me.udtCRBDue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.udtCRBDue.Properties.Appearance.Options.UseFont = True
        Me.udtCRBDue.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.udtCRBDue.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.udtCRBDue.Size = New System.Drawing.Size(110, 22)
        Me.udtCRBDue.TabIndex = 3
        Me.udtCRBDue.Tag = "AE"
        Me.udtCRBDue.Value = New Date(2012, 6, 30, 0, 0, 0, 0)
        '
        'udtCRBLast
        '
        Me.udtCRBLast.EditValue = New Date(2012, 6, 30, 0, 0, 0, 0)
        Me.udtCRBLast.EnterMoveNextControl = True
        Me.udtCRBLast.Location = New System.Drawing.Point(114, 29)
        Me.udtCRBLast.Name = "udtCRBLast"
        Me.udtCRBLast.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.udtCRBLast.Properties.Appearance.Options.UseFont = True
        Me.udtCRBLast.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.udtCRBLast.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.udtCRBLast.Size = New System.Drawing.Size(110, 22)
        Me.udtCRBLast.TabIndex = 1
        Me.udtCRBLast.Tag = "AE"
        Me.udtCRBLast.Value = New Date(2012, 6, 30, 0, 0, 0, 0)
        '
        'Label7
        '
        Me.Label7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Appearance.Options.UseFont = True
        Me.Label7.Appearance.Options.UseTextOptions = True
        Me.Label7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label7.Location = New System.Drawing.Point(254, 32)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(48, 15)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "Next Due"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Appearance.Options.UseFont = True
        Me.Label8.Appearance.Options.UseTextOptions = True
        Me.Label8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label8.Location = New System.Drawing.Point(8, 32)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(70, 15)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Last Checked"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabPersonal
        '
        Me.tabPersonal.Controls.Add(Me.GroupControl13)
        Me.tabPersonal.Controls.Add(Me.GroupControl12)
        Me.tabPersonal.Controls.Add(Me.GroupControl9)
        Me.tabPersonal.Controls.Add(Me.GroupControl10)
        Me.tabPersonal.Controls.Add(Me.GroupControl8)
        Me.tabPersonal.Controls.Add(Me.GroupBox1)
        Me.tabPersonal.Controls.Add(Me.GroupBox3)
        Me.tabPersonal.Controls.Add(Me.GroupControl1)
        Me.tabPersonal.Name = "tabPersonal"
        Me.tabPersonal.Size = New System.Drawing.Size(807, 487)
        Me.tabPersonal.Text = "Personal"
        '
        'GroupControl13
        '
        Me.GroupControl13.Controls.Add(Me.CareLabel10)
        Me.GroupControl13.Controls.Add(Me.cbxQualLevel)
        Me.GroupControl13.Location = New System.Drawing.Point(494, 220)
        Me.GroupControl13.Name = "GroupControl13"
        Me.GroupControl13.Size = New System.Drawing.Size(303, 57)
        Me.GroupControl13.TabIndex = 5
        Me.GroupControl13.Text = "Current Qualification Level"
        '
        'CareLabel10
        '
        Me.CareLabel10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel10.Appearance.Options.UseFont = True
        Me.CareLabel10.Appearance.Options.UseTextOptions = True
        Me.CareLabel10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel10.Location = New System.Drawing.Point(11, 30)
        Me.CareLabel10.Name = "CareLabel10"
        Me.CareLabel10.Size = New System.Drawing.Size(27, 15)
        Me.CareLabel10.TabIndex = 0
        Me.CareLabel10.Text = "Level"
        Me.CareLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxQualLevel
        '
        Me.cbxQualLevel.AllowBlank = False
        Me.cbxQualLevel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxQualLevel.DataSource = Nothing
        Me.cbxQualLevel.DisplayMember = Nothing
        Me.cbxQualLevel.EnterMoveNextControl = True
        Me.cbxQualLevel.Location = New System.Drawing.Point(55, 27)
        Me.cbxQualLevel.Name = "cbxQualLevel"
        Me.cbxQualLevel.Properties.AccessibleName = "Qualification Level"
        Me.cbxQualLevel.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxQualLevel.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxQualLevel.Properties.Appearance.Options.UseFont = True
        Me.cbxQualLevel.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxQualLevel.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxQualLevel.SelectedValue = Nothing
        Me.cbxQualLevel.Size = New System.Drawing.Size(239, 20)
        Me.cbxQualLevel.TabIndex = 1
        Me.cbxQualLevel.Tag = "AEM"
        Me.cbxQualLevel.ValueMember = Nothing
        '
        'GroupControl12
        '
        Me.GroupControl12.Controls.Add(Me.lblYearsLeft)
        Me.GroupControl12.Controls.Add(Me.lblYearsService)
        Me.GroupControl12.Controls.Add(Me.udtLeft)
        Me.GroupControl12.Controls.Add(Me.Label13)
        Me.GroupControl12.Controls.Add(Me.udtStarted)
        Me.GroupControl12.Controls.Add(Me.Label12)
        Me.GroupControl12.Location = New System.Drawing.Point(11, 302)
        Me.GroupControl12.Name = "GroupControl12"
        Me.GroupControl12.Size = New System.Drawing.Size(477, 85)
        Me.GroupControl12.TabIndex = 2
        Me.GroupControl12.Text = "Keyworker && Dates"
        '
        'lblYearsLeft
        '
        Me.lblYearsLeft.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYearsLeft.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblYearsLeft.Appearance.Options.UseFont = True
        Me.lblYearsLeft.Appearance.Options.UseForeColor = True
        Me.lblYearsLeft.Appearance.Options.UseTextOptions = True
        Me.lblYearsLeft.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblYearsLeft.Location = New System.Drawing.Point(167, 58)
        Me.lblYearsLeft.Name = "lblYearsLeft"
        Me.lblYearsLeft.Size = New System.Drawing.Size(147, 15)
        Me.lblYearsLeft.TabIndex = 5
        Me.lblYearsLeft.Text = "1 year, 11 months, 21 days"
        Me.lblYearsLeft.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblYearsService
        '
        Me.lblYearsService.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYearsService.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblYearsService.Appearance.Options.UseFont = True
        Me.lblYearsService.Appearance.Options.UseForeColor = True
        Me.lblYearsService.Appearance.Options.UseTextOptions = True
        Me.lblYearsService.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblYearsService.Location = New System.Drawing.Point(167, 30)
        Me.lblYearsService.Name = "lblYearsService"
        Me.lblYearsService.Size = New System.Drawing.Size(147, 15)
        Me.lblYearsService.TabIndex = 2
        Me.lblYearsService.Text = "1 year, 11 months, 21 days"
        Me.lblYearsService.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'udtLeft
        '
        Me.udtLeft.EditValue = New Date(2012, 6, 30, 0, 0, 0, 0)
        Me.udtLeft.EnterMoveNextControl = True
        Me.udtLeft.Location = New System.Drawing.Point(68, 55)
        Me.udtLeft.Name = "udtLeft"
        Me.udtLeft.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.udtLeft.Properties.Appearance.Options.UseFont = True
        Me.udtLeft.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.udtLeft.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.udtLeft.Size = New System.Drawing.Size(93, 22)
        Me.udtLeft.TabIndex = 4
        Me.udtLeft.Tag = "AE"
        Me.udtLeft.Value = New Date(2012, 6, 30, 0, 0, 0, 0)
        '
        'Label13
        '
        Me.Label13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Appearance.Options.UseFont = True
        Me.Label13.Appearance.Options.UseTextOptions = True
        Me.Label13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label13.Location = New System.Drawing.Point(8, 30)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(37, 15)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "Started"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'udtStarted
        '
        Me.udtStarted.EditValue = New Date(2012, 6, 30, 0, 0, 0, 0)
        Me.udtStarted.EnterMoveNextControl = True
        Me.udtStarted.Location = New System.Drawing.Point(68, 27)
        Me.udtStarted.Name = "udtStarted"
        Me.udtStarted.Properties.AccessibleName = "Date Started"
        Me.udtStarted.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.udtStarted.Properties.Appearance.Options.UseFont = True
        Me.udtStarted.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.udtStarted.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.udtStarted.Size = New System.Drawing.Size(93, 22)
        Me.udtStarted.TabIndex = 1
        Me.udtStarted.Tag = "AE"
        Me.udtStarted.Value = New Date(2012, 6, 30, 0, 0, 0, 0)
        '
        'Label12
        '
        Me.Label12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Appearance.Options.UseFont = True
        Me.Label12.Appearance.Options.UseTextOptions = True
        Me.Label12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label12.Location = New System.Drawing.Point(8, 58)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(20, 15)
        Me.Label12.TabIndex = 3
        Me.Label12.Text = "Left"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl9
        '
        Me.GroupControl9.Controls.Add(Me.TableLayoutPanel2)
        Me.GroupControl9.Location = New System.Drawing.Point(494, 283)
        Me.GroupControl9.Name = "GroupControl9"
        Me.GroupControl9.Size = New System.Drawing.Size(303, 104)
        Me.GroupControl9.TabIndex = 6
        Me.GroupControl9.Text = "Key Attributes && Qualifications"
        Me.GroupControl9.UseDisabledStatePainter = False
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 4
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.7876!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.212404!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.7876!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.212404!))
        Me.TableLayoutPanel2.Controls.Add(Me.chkENCO, 3, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.CareLabel24, 2, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.chkSENCO, 1, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.CareLabel19, 0, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.chkHygiene, 1, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.chkSafe, 3, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.lblMedicine, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.lblObs, 2, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.lblSleep, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.lblMilk, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.lblNappy, 2, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.chkAidWork, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.chkFS, 1, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.lblFood, 2, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.chkPaeds, 3, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.chkFSAid, 3, 1)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(2, 20)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 4
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(299, 82)
        Me.TableLayoutPanel2.TabIndex = 1
        '
        'chkENCO
        '
        Me.chkENCO.Dock = System.Windows.Forms.DockStyle.Fill
        Me.chkENCO.EnterMoveNextControl = True
        Me.chkENCO.Location = New System.Drawing.Point(278, 63)
        Me.chkENCO.Name = "chkENCO"
        Me.chkENCO.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkENCO.Properties.Appearance.Options.UseFont = True
        Me.chkENCO.Properties.Caption = ""
        Me.chkENCO.Size = New System.Drawing.Size(18, 16)
        Me.chkENCO.TabIndex = 15
        Me.chkENCO.Tag = "AEB"
        '
        'CareLabel24
        '
        Me.CareLabel24.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel24.Appearance.Options.UseFont = True
        Me.CareLabel24.Appearance.Options.UseTextOptions = True
        Me.CareLabel24.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel24.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CareLabel24.Location = New System.Drawing.Point(151, 63)
        Me.CareLabel24.Name = "CareLabel24"
        Me.CareLabel24.Size = New System.Drawing.Size(121, 16)
        Me.CareLabel24.TabIndex = 14
        Me.CareLabel24.Text = "ENCO"
        Me.CareLabel24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkSENCO
        '
        Me.chkSENCO.Dock = System.Windows.Forms.DockStyle.Fill
        Me.chkSENCO.EnterMoveNextControl = True
        Me.chkSENCO.Location = New System.Drawing.Point(130, 63)
        Me.chkSENCO.Name = "chkSENCO"
        Me.chkSENCO.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSENCO.Properties.Appearance.Options.UseFont = True
        Me.chkSENCO.Properties.Caption = ""
        Me.chkSENCO.Size = New System.Drawing.Size(15, 16)
        Me.chkSENCO.TabIndex = 13
        Me.chkSENCO.Tag = "AEB"
        '
        'CareLabel19
        '
        Me.CareLabel19.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel19.Appearance.Options.UseFont = True
        Me.CareLabel19.Appearance.Options.UseTextOptions = True
        Me.CareLabel19.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel19.Location = New System.Drawing.Point(3, 63)
        Me.CareLabel19.Name = "CareLabel19"
        Me.CareLabel19.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.CareLabel19.Size = New System.Drawing.Size(41, 15)
        Me.CareLabel19.TabIndex = 12
        Me.CareLabel19.Text = "SENCO"
        Me.CareLabel19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkHygiene
        '
        Me.chkHygiene.Dock = System.Windows.Forms.DockStyle.Fill
        Me.chkHygiene.EnterMoveNextControl = True
        Me.chkHygiene.Location = New System.Drawing.Point(130, 43)
        Me.chkHygiene.Name = "chkHygiene"
        Me.chkHygiene.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkHygiene.Properties.Appearance.Options.UseFont = True
        Me.chkHygiene.Properties.Caption = ""
        Me.chkHygiene.Size = New System.Drawing.Size(15, 14)
        Me.chkHygiene.TabIndex = 9
        Me.chkHygiene.Tag = "AEB"
        '
        'chkSafe
        '
        Me.chkSafe.Dock = System.Windows.Forms.DockStyle.Fill
        Me.chkSafe.EnterMoveNextControl = True
        Me.chkSafe.Location = New System.Drawing.Point(278, 43)
        Me.chkSafe.Name = "chkSafe"
        Me.chkSafe.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSafe.Properties.Appearance.Options.UseFont = True
        Me.chkSafe.Properties.Caption = ""
        Me.chkSafe.Size = New System.Drawing.Size(18, 14)
        Me.chkSafe.TabIndex = 11
        Me.chkSafe.Tag = "AEB"
        '
        'lblMedicine
        '
        Me.lblMedicine.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMedicine.Appearance.Options.UseFont = True
        Me.lblMedicine.Appearance.Options.UseTextOptions = True
        Me.lblMedicine.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblMedicine.Location = New System.Drawing.Point(3, 43)
        Me.lblMedicine.Name = "lblMedicine"
        Me.lblMedicine.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.lblMedicine.Size = New System.Drawing.Size(77, 15)
        Me.lblMedicine.TabIndex = 8
        Me.lblMedicine.Text = "Food Hygiene"
        Me.lblMedicine.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblObs
        '
        Me.lblObs.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblObs.Appearance.Options.UseFont = True
        Me.lblObs.Appearance.Options.UseTextOptions = True
        Me.lblObs.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblObs.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblObs.Location = New System.Drawing.Point(151, 43)
        Me.lblObs.Name = "lblObs"
        Me.lblObs.Size = New System.Drawing.Size(121, 14)
        Me.lblObs.TabIndex = 10
        Me.lblObs.Text = "Safeguarding"
        Me.lblObs.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSleep
        '
        Me.lblSleep.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSleep.Appearance.Options.UseFont = True
        Me.lblSleep.Appearance.Options.UseTextOptions = True
        Me.lblSleep.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblSleep.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblSleep.Location = New System.Drawing.Point(3, 23)
        Me.lblSleep.Name = "lblSleep"
        Me.lblSleep.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.lblSleep.Size = New System.Drawing.Size(121, 14)
        Me.lblSleep.TabIndex = 4
        Me.lblSleep.Text = "Forest School Trained"
        Me.lblSleep.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMilk
        '
        Me.lblMilk.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMilk.Appearance.Options.UseFont = True
        Me.lblMilk.Appearance.Options.UseTextOptions = True
        Me.lblMilk.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblMilk.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblMilk.Location = New System.Drawing.Point(3, 3)
        Me.lblMilk.Name = "lblMilk"
        Me.lblMilk.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.lblMilk.Size = New System.Drawing.Size(121, 14)
        Me.lblMilk.TabIndex = 0
        Me.lblMilk.Text = "First Aid @ Work"
        Me.lblMilk.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNappy
        '
        Me.lblNappy.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNappy.Appearance.Options.UseFont = True
        Me.lblNappy.Appearance.Options.UseTextOptions = True
        Me.lblNappy.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblNappy.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblNappy.Location = New System.Drawing.Point(151, 23)
        Me.lblNappy.Name = "lblNappy"
        Me.lblNappy.Size = New System.Drawing.Size(121, 14)
        Me.lblNappy.TabIndex = 6
        Me.lblNappy.Text = "Forest School First Aid"
        Me.lblNappy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkAidWork
        '
        Me.chkAidWork.Dock = System.Windows.Forms.DockStyle.Fill
        Me.chkAidWork.EnterMoveNextControl = True
        Me.chkAidWork.Location = New System.Drawing.Point(130, 3)
        Me.chkAidWork.Name = "chkAidWork"
        Me.chkAidWork.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAidWork.Properties.Appearance.Options.UseFont = True
        Me.chkAidWork.Properties.Caption = ""
        Me.chkAidWork.Size = New System.Drawing.Size(15, 14)
        Me.chkAidWork.TabIndex = 1
        Me.chkAidWork.Tag = "AEB"
        '
        'chkFS
        '
        Me.chkFS.Dock = System.Windows.Forms.DockStyle.Fill
        Me.chkFS.EnterMoveNextControl = True
        Me.chkFS.Location = New System.Drawing.Point(130, 23)
        Me.chkFS.Name = "chkFS"
        Me.chkFS.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFS.Properties.Appearance.Options.UseFont = True
        Me.chkFS.Properties.Caption = ""
        Me.chkFS.Size = New System.Drawing.Size(15, 14)
        Me.chkFS.TabIndex = 5
        Me.chkFS.Tag = "AEB"
        '
        'lblFood
        '
        Me.lblFood.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFood.Appearance.Options.UseFont = True
        Me.lblFood.Appearance.Options.UseTextOptions = True
        Me.lblFood.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblFood.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblFood.Location = New System.Drawing.Point(151, 3)
        Me.lblFood.Name = "lblFood"
        Me.lblFood.Size = New System.Drawing.Size(121, 14)
        Me.lblFood.TabIndex = 2
        Me.lblFood.Text = "Paediatric First Aid"
        Me.lblFood.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkPaeds
        '
        Me.chkPaeds.Dock = System.Windows.Forms.DockStyle.Fill
        Me.chkPaeds.EnterMoveNextControl = True
        Me.chkPaeds.Location = New System.Drawing.Point(278, 3)
        Me.chkPaeds.Name = "chkPaeds"
        Me.chkPaeds.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPaeds.Properties.Appearance.Options.UseFont = True
        Me.chkPaeds.Properties.Caption = ""
        Me.chkPaeds.Size = New System.Drawing.Size(18, 14)
        Me.chkPaeds.TabIndex = 3
        Me.chkPaeds.Tag = "AEB"
        '
        'chkFSAid
        '
        Me.chkFSAid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.chkFSAid.EnterMoveNextControl = True
        Me.chkFSAid.Location = New System.Drawing.Point(278, 23)
        Me.chkFSAid.Name = "chkFSAid"
        Me.chkFSAid.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFSAid.Properties.Appearance.Options.UseFont = True
        Me.chkFSAid.Properties.Caption = ""
        Me.chkFSAid.Size = New System.Drawing.Size(18, 14)
        Me.chkFSAid.TabIndex = 7
        Me.chkFSAid.Tag = "AEB"
        '
        'GroupControl10
        '
        Me.GroupControl10.Controls.Add(Me.ccxLanguages)
        Me.GroupControl10.Controls.Add(Me.ccxNationality)
        Me.GroupControl10.Controls.Add(Me.CareLabel20)
        Me.GroupControl10.Controls.Add(Me.CareLabel21)
        Me.GroupControl10.Controls.Add(Me.cbxEthnicity)
        Me.GroupControl10.Controls.Add(Me.cbxReligion)
        Me.GroupControl10.Controls.Add(Me.CareLabel22)
        Me.GroupControl10.Controls.Add(Me.CareLabel23)
        Me.GroupControl10.Location = New System.Drawing.Point(11, 393)
        Me.GroupControl10.Name = "GroupControl10"
        Me.GroupControl10.Size = New System.Drawing.Size(585, 86)
        Me.GroupControl10.TabIndex = 3
        Me.GroupControl10.Text = "Other Details"
        '
        'ccxLanguages
        '
        Me.ccxLanguages.DataSource = Nothing
        Me.ccxLanguages.DisplayMember = Nothing
        Me.ccxLanguages.EditValue = ""
        Me.ccxLanguages.EnterMoveNextControl = True
        Me.ccxLanguages.Location = New System.Drawing.Point(394, 56)
        Me.ccxLanguages.Name = "ccxLanguages"
        Me.ccxLanguages.Properties.AccessibleName = "Langauge"
        Me.ccxLanguages.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.ccxLanguages.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.ccxLanguages.Properties.Appearance.Options.UseFont = True
        Me.ccxLanguages.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ccxLanguages.SelectedItems = Nothing
        Me.ccxLanguages.Size = New System.Drawing.Size(181, 22)
        Me.ccxLanguages.TabIndex = 7
        Me.ccxLanguages.Tag = "AE"
        Me.ccxLanguages.ValueMember = Nothing
        '
        'ccxNationality
        '
        Me.ccxNationality.DataSource = Nothing
        Me.ccxNationality.DisplayMember = Nothing
        Me.ccxNationality.EditValue = ""
        Me.ccxNationality.EnterMoveNextControl = True
        Me.ccxNationality.Location = New System.Drawing.Point(394, 28)
        Me.ccxNationality.Name = "ccxNationality"
        Me.ccxNationality.Properties.AccessibleName = "Nationality"
        Me.ccxNationality.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.ccxNationality.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.ccxNationality.Properties.Appearance.Options.UseFont = True
        Me.ccxNationality.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ccxNationality.SelectedItems = Nothing
        Me.ccxNationality.Size = New System.Drawing.Size(181, 22)
        Me.ccxNationality.TabIndex = 3
        Me.ccxNationality.Tag = "AE"
        Me.ccxNationality.ValueMember = Nothing
        '
        'CareLabel20
        '
        Me.CareLabel20.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel20.Appearance.Options.UseFont = True
        Me.CareLabel20.Appearance.Options.UseTextOptions = True
        Me.CareLabel20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel20.Location = New System.Drawing.Point(314, 59)
        Me.CareLabel20.Name = "CareLabel20"
        Me.CareLabel20.Size = New System.Drawing.Size(65, 15)
        Me.CareLabel20.TabIndex = 6
        Me.CareLabel20.Text = "Language(s)"
        Me.CareLabel20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel21
        '
        Me.CareLabel21.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel21.Appearance.Options.UseFont = True
        Me.CareLabel21.Appearance.Options.UseTextOptions = True
        Me.CareLabel21.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel21.Location = New System.Drawing.Point(314, 31)
        Me.CareLabel21.Name = "CareLabel21"
        Me.CareLabel21.Size = New System.Drawing.Size(58, 15)
        Me.CareLabel21.TabIndex = 2
        Me.CareLabel21.Text = "Nationality"
        Me.CareLabel21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxEthnicity
        '
        Me.cbxEthnicity.AllowBlank = False
        Me.cbxEthnicity.DataSource = Nothing
        Me.cbxEthnicity.DisplayMember = Nothing
        Me.cbxEthnicity.EnterMoveNextControl = True
        Me.cbxEthnicity.Location = New System.Drawing.Point(68, 56)
        Me.cbxEthnicity.Name = "cbxEthnicity"
        Me.cbxEthnicity.Properties.AccessibleName = "Ethnicity"
        Me.cbxEthnicity.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxEthnicity.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxEthnicity.Properties.Appearance.Options.UseFont = True
        Me.cbxEthnicity.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxEthnicity.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxEthnicity.SelectedValue = Nothing
        Me.cbxEthnicity.Size = New System.Drawing.Size(237, 22)
        Me.cbxEthnicity.TabIndex = 5
        Me.cbxEthnicity.Tag = "AE"
        Me.cbxEthnicity.ValueMember = Nothing
        '
        'cbxReligion
        '
        Me.cbxReligion.AllowBlank = False
        Me.cbxReligion.DataSource = Nothing
        Me.cbxReligion.DisplayMember = Nothing
        Me.cbxReligion.EnterMoveNextControl = True
        Me.cbxReligion.Location = New System.Drawing.Point(68, 28)
        Me.cbxReligion.Name = "cbxReligion"
        Me.cbxReligion.Properties.AccessibleName = "Religion"
        Me.cbxReligion.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxReligion.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxReligion.Properties.Appearance.Options.UseFont = True
        Me.cbxReligion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxReligion.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxReligion.SelectedValue = Nothing
        Me.cbxReligion.Size = New System.Drawing.Size(237, 22)
        Me.cbxReligion.TabIndex = 1
        Me.cbxReligion.Tag = "AE"
        Me.cbxReligion.ValueMember = Nothing
        '
        'CareLabel22
        '
        Me.CareLabel22.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel22.Appearance.Options.UseFont = True
        Me.CareLabel22.Appearance.Options.UseTextOptions = True
        Me.CareLabel22.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel22.Location = New System.Drawing.Point(8, 59)
        Me.CareLabel22.Name = "CareLabel22"
        Me.CareLabel22.Size = New System.Drawing.Size(46, 15)
        Me.CareLabel22.TabIndex = 4
        Me.CareLabel22.Text = "Ethnicity"
        Me.CareLabel22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel23
        '
        Me.CareLabel23.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel23.Appearance.Options.UseFont = True
        Me.CareLabel23.Appearance.Options.UseTextOptions = True
        Me.CareLabel23.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel23.Location = New System.Drawing.Point(8, 31)
        Me.CareLabel23.Name = "CareLabel23"
        Me.CareLabel23.Size = New System.Drawing.Size(43, 15)
        Me.CareLabel23.TabIndex = 0
        Me.CareLabel23.Text = "Religion"
        Me.CareLabel23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl8
        '
        Me.GroupControl8.Controls.Add(Me.btnPIN)
        Me.GroupControl8.Controls.Add(Me.txtTouchPIN)
        Me.GroupControl8.Controls.Add(Me.CareLabel16)
        Me.GroupControl8.Location = New System.Drawing.Point(602, 393)
        Me.GroupControl8.Name = "GroupControl8"
        Me.GroupControl8.Size = New System.Drawing.Size(195, 86)
        Me.GroupControl8.TabIndex = 7
        Me.GroupControl8.Text = "Tablet and Touchscreen Security"
        '
        'btnPIN
        '
        Me.btnPIN.Location = New System.Drawing.Point(12, 56)
        Me.btnPIN.Name = "btnPIN"
        Me.btnPIN.Size = New System.Drawing.Size(173, 21)
        Me.btnPIN.TabIndex = 2
        Me.btnPIN.Text = "Set PIN from Birthday"
        '
        'txtTouchPIN
        '
        Me.txtTouchPIN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTouchPIN.EnterMoveNextControl = True
        Me.txtTouchPIN.Location = New System.Drawing.Point(106, 28)
        Me.txtTouchPIN.MaxLength = 5
        Me.txtTouchPIN.Name = "txtTouchPIN"
        Me.txtTouchPIN.NumericAllowNegatives = False
        Me.txtTouchPIN.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtTouchPIN.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTouchPIN.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtTouchPIN.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTouchPIN.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtTouchPIN.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTouchPIN.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTouchPIN.Properties.MaxLength = 5
        Me.txtTouchPIN.Size = New System.Drawing.Size(79, 20)
        Me.txtTouchPIN.TabIndex = 1
        Me.txtTouchPIN.Tag = "AE"
        Me.txtTouchPIN.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTouchPIN.ToolTipText = ""
        '
        'CareLabel16
        '
        Me.CareLabel16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel16.Appearance.Options.UseFont = True
        Me.CareLabel16.Appearance.Options.UseTextOptions = True
        Me.CareLabel16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel16.Location = New System.Drawing.Point(10, 31)
        Me.CareLabel16.Name = "CareLabel16"
        Me.CareLabel16.Size = New System.Drawing.Size(90, 15)
        Me.CareLabel16.TabIndex = 0
        Me.CareLabel16.Text = "Touchscreen PIN"
        Me.CareLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CareLabel26)
        Me.GroupBox1.Controls.Add(Me.lblAge)
        Me.GroupBox1.Controls.Add(Me.CareLabel15)
        Me.GroupBox1.Controls.Add(Me.cbxContractType)
        Me.GroupBox1.Controls.Add(Me.cbxStatus)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.udtDOB)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtForename)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtSurname)
        Me.GroupBox1.Controls.Add(Me.txtFullname)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(11, 7)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(477, 141)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.Text = "Personal Details"
        '
        'CareLabel26
        '
        Me.CareLabel26.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel26.Appearance.Options.UseFont = True
        Me.CareLabel26.Appearance.Options.UseTextOptions = True
        Me.CareLabel26.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel26.Location = New System.Drawing.Point(249, 59)
        Me.CareLabel26.Name = "CareLabel26"
        Me.CareLabel26.Size = New System.Drawing.Size(47, 15)
        Me.CareLabel26.TabIndex = 4
        Me.CareLabel26.Text = "Surname"
        Me.CareLabel26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAge
        '
        Me.lblAge.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAge.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblAge.Appearance.Options.UseFont = True
        Me.lblAge.Appearance.Options.UseForeColor = True
        Me.lblAge.Appearance.Options.UseTextOptions = True
        Me.lblAge.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblAge.Location = New System.Drawing.Point(207, 115)
        Me.lblAge.Name = "lblAge"
        Me.lblAge.Size = New System.Drawing.Size(147, 15)
        Me.lblAge.TabIndex = 12
        Me.lblAge.Text = "1 year, 11 months, 21 days"
        Me.lblAge.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel15
        '
        Me.CareLabel15.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel15.Appearance.Options.UseFont = True
        Me.CareLabel15.Appearance.Options.UseTextOptions = True
        Me.CareLabel15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel15.Location = New System.Drawing.Point(221, 87)
        Me.CareLabel15.Name = "CareLabel15"
        Me.CareLabel15.Size = New System.Drawing.Size(75, 15)
        Me.CareLabel15.TabIndex = 8
        Me.CareLabel15.Text = "Contract Type"
        Me.CareLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxContractType
        '
        Me.cbxContractType.AllowBlank = False
        Me.cbxContractType.DataSource = Nothing
        Me.cbxContractType.DisplayMember = Nothing
        Me.cbxContractType.EnterMoveNextControl = True
        Me.cbxContractType.Location = New System.Drawing.Point(302, 84)
        Me.cbxContractType.Name = "cbxContractType"
        Me.cbxContractType.Properties.AccessibleName = "Contract Type"
        Me.cbxContractType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxContractType.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxContractType.Properties.Appearance.Options.UseFont = True
        Me.cbxContractType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxContractType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxContractType.SelectedValue = Nothing
        Me.cbxContractType.Size = New System.Drawing.Size(167, 22)
        Me.cbxContractType.TabIndex = 9
        Me.cbxContractType.Tag = "AEM"
        Me.cbxContractType.ValueMember = Nothing
        '
        'cbxStatus
        '
        Me.cbxStatus.AllowBlank = False
        Me.cbxStatus.DataSource = Nothing
        Me.cbxStatus.DisplayMember = Nothing
        Me.cbxStatus.EnterMoveNextControl = True
        Me.cbxStatus.Location = New System.Drawing.Point(68, 84)
        Me.cbxStatus.Name = "cbxStatus"
        Me.cbxStatus.Properties.AccessibleName = "Status"
        Me.cbxStatus.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxStatus.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxStatus.Properties.Appearance.Options.UseFont = True
        Me.cbxStatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxStatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxStatus.SelectedValue = Nothing
        Me.cbxStatus.Size = New System.Drawing.Size(133, 22)
        Me.cbxStatus.TabIndex = 7
        Me.cbxStatus.Tag = "AEM"
        Me.cbxStatus.ValueMember = Nothing
        '
        'Label10
        '
        Me.Label10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Appearance.Options.UseFont = True
        Me.Label10.Appearance.Options.UseTextOptions = True
        Me.Label10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label10.Location = New System.Drawing.Point(8, 87)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(32, 15)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "Status"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'udtDOB
        '
        Me.udtDOB.EditValue = New Date(2012, 6, 30, 0, 0, 0, 0)
        Me.udtDOB.EnterMoveNextControl = True
        Me.udtDOB.Location = New System.Drawing.Point(68, 112)
        Me.udtDOB.Name = "udtDOB"
        Me.udtDOB.Properties.AccessibleName = "Date of Birth"
        Me.udtDOB.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.udtDOB.Properties.Appearance.Options.UseFont = True
        Me.udtDOB.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.udtDOB.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.udtDOB.Size = New System.Drawing.Size(133, 22)
        Me.udtDOB.TabIndex = 11
        Me.udtDOB.Tag = "AE"
        Me.udtDOB.Value = New Date(2012, 6, 30, 0, 0, 0, 0)
        '
        'Label5
        '
        Me.Label5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Appearance.Options.UseFont = True
        Me.Label5.Appearance.Options.UseTextOptions = True
        Me.Label5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label5.Location = New System.Drawing.Point(8, 115)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(24, 15)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "DOB"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtForename
        '
        Me.txtForename.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtForename.EnterMoveNextControl = True
        Me.txtForename.Location = New System.Drawing.Point(68, 56)
        Me.txtForename.MaxLength = 30
        Me.txtForename.Name = "txtForename"
        Me.txtForename.NumericAllowNegatives = False
        Me.txtForename.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtForename.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtForename.Properties.AccessibleName = "Forename"
        Me.txtForename.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtForename.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtForename.Properties.Appearance.Options.UseFont = True
        Me.txtForename.Properties.Appearance.Options.UseTextOptions = True
        Me.txtForename.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtForename.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtForename.Properties.MaxLength = 30
        Me.txtForename.Size = New System.Drawing.Size(170, 22)
        Me.txtForename.TabIndex = 3
        Me.txtForename.Tag = "AETM"
        Me.txtForename.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtForename.ToolTipText = ""
        '
        'Label3
        '
        Me.Label3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Appearance.Options.UseFont = True
        Me.Label3.Appearance.Options.UseTextOptions = True
        Me.Label3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label3.Location = New System.Drawing.Point(8, 59)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 15)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Forename"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSurname
        '
        Me.txtSurname.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtSurname.EnterMoveNextControl = True
        Me.txtSurname.Location = New System.Drawing.Point(302, 56)
        Me.txtSurname.MaxLength = 30
        Me.txtSurname.Name = "txtSurname"
        Me.txtSurname.NumericAllowNegatives = False
        Me.txtSurname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSurname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSurname.Properties.AccessibleName = "Surname"
        Me.txtSurname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSurname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSurname.Properties.Appearance.Options.UseFont = True
        Me.txtSurname.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSurname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSurname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSurname.Properties.MaxLength = 30
        Me.txtSurname.Size = New System.Drawing.Size(167, 22)
        Me.txtSurname.TabIndex = 5
        Me.txtSurname.Tag = "AETM"
        Me.txtSurname.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSurname.ToolTipText = ""
        '
        'txtFullname
        '
        Me.txtFullname.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtFullname.EnterMoveNextControl = True
        Me.txtFullname.Location = New System.Drawing.Point(68, 28)
        Me.txtFullname.MaxLength = 60
        Me.txtFullname.Name = "txtFullname"
        Me.txtFullname.NumericAllowNegatives = False
        Me.txtFullname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtFullname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFullname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFullname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFullname.Properties.Appearance.Options.UseFont = True
        Me.txtFullname.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFullname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFullname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFullname.Properties.MaxLength = 60
        Me.txtFullname.Properties.ReadOnly = True
        Me.txtFullname.Size = New System.Drawing.Size(401, 22)
        Me.txtFullname.TabIndex = 1
        Me.txtFullname.Tag = ""
        Me.txtFullname.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtFullname.ToolTipText = ""
        '
        'Label1
        '
        Me.Label1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Appearance.Options.UseFont = True
        Me.Label1.Appearance.Options.UseTextOptions = True
        Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label1.Location = New System.Drawing.Point(8, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Fullname"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cbxManager)
        Me.GroupBox3.Controls.Add(Me.CareLabel29)
        Me.GroupBox3.Controls.Add(Me.chkLineManager)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.CareLabel28)
        Me.GroupBox3.Controls.Add(Me.cbxDepartment)
        Me.GroupBox3.Controls.Add(Me.chkKeyworker)
        Me.GroupBox3.Controls.Add(Me.CareLabel27)
        Me.GroupBox3.Controls.Add(Me.CareLabel25)
        Me.GroupBox3.Controls.Add(Me.cbxSite)
        Me.GroupBox3.Controls.Add(Me.cbxGroup)
        Me.GroupBox3.Controls.Add(Me.Label15)
        Me.GroupBox3.Controls.Add(Me.txtJobTitle)
        Me.GroupBox3.Controls.Add(Me.Label16)
        Me.GroupBox3.Location = New System.Drawing.Point(11, 154)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(477, 142)
        Me.GroupBox3.TabIndex = 1
        Me.GroupBox3.Text = "Site, Room && Job Title"
        '
        'cbxManager
        '
        Me.cbxManager.AllowBlank = False
        Me.cbxManager.DataSource = Nothing
        Me.cbxManager.DisplayMember = Nothing
        Me.cbxManager.EnterMoveNextControl = True
        Me.cbxManager.Location = New System.Drawing.Point(68, 111)
        Me.cbxManager.Name = "cbxManager"
        Me.cbxManager.Properties.AccessibleName = "Group"
        Me.cbxManager.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxManager.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxManager.Properties.Appearance.Options.UseFont = True
        Me.cbxManager.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxManager.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxManager.SelectedValue = Nothing
        Me.cbxManager.Size = New System.Drawing.Size(286, 22)
        Me.cbxManager.TabIndex = 11
        Me.cbxManager.Tag = "AE"
        Me.cbxManager.ValueMember = Nothing
        '
        'CareLabel29
        '
        Me.CareLabel29.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel29.Appearance.Options.UseFont = True
        Me.CareLabel29.Appearance.Options.UseTextOptions = True
        Me.CareLabel29.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel29.Location = New System.Drawing.Point(360, 114)
        Me.CareLabel29.Name = "CareLabel29"
        Me.CareLabel29.Size = New System.Drawing.Size(83, 15)
        Me.CareLabel29.TabIndex = 12
        Me.CareLabel29.Text = "Is Line Manager"
        Me.CareLabel29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkLineManager
        '
        Me.chkLineManager.EnterMoveNextControl = True
        Me.chkLineManager.Location = New System.Drawing.Point(449, 112)
        Me.chkLineManager.Name = "chkLineManager"
        Me.chkLineManager.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLineManager.Properties.Appearance.Options.UseFont = True
        Me.chkLineManager.Size = New System.Drawing.Size(20, 19)
        Me.chkLineManager.TabIndex = 13
        Me.chkLineManager.Tag = "AE"
        '
        'Label9
        '
        Me.Label9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Appearance.Options.UseFont = True
        Me.Label9.Appearance.Options.UseTextOptions = True
        Me.Label9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label9.Location = New System.Drawing.Point(374, 86)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(69, 15)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "Is Key Person"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel28
        '
        Me.CareLabel28.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel28.Appearance.Options.UseFont = True
        Me.CareLabel28.Appearance.Options.UseTextOptions = True
        Me.CareLabel28.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel28.Location = New System.Drawing.Point(8, 114)
        Me.CareLabel28.Name = "CareLabel28"
        Me.CareLabel28.Size = New System.Drawing.Size(54, 15)
        Me.CareLabel28.TabIndex = 10
        Me.CareLabel28.Text = "Reports to"
        Me.CareLabel28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxDepartment
        '
        Me.cbxDepartment.AllowBlank = False
        Me.cbxDepartment.DataSource = Nothing
        Me.cbxDepartment.DisplayMember = Nothing
        Me.cbxDepartment.EnterMoveNextControl = True
        Me.cbxDepartment.Location = New System.Drawing.Point(317, 55)
        Me.cbxDepartment.Name = "cbxDepartment"
        Me.cbxDepartment.Properties.AccessibleName = "Group"
        Me.cbxDepartment.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxDepartment.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxDepartment.Properties.Appearance.Options.UseFont = True
        Me.cbxDepartment.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxDepartment.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxDepartment.SelectedValue = Nothing
        Me.cbxDepartment.Size = New System.Drawing.Size(152, 22)
        Me.cbxDepartment.TabIndex = 5
        Me.cbxDepartment.Tag = "AE"
        Me.cbxDepartment.ValueMember = Nothing
        '
        'chkKeyworker
        '
        Me.chkKeyworker.EnterMoveNextControl = True
        Me.chkKeyworker.Location = New System.Drawing.Point(449, 84)
        Me.chkKeyworker.Name = "chkKeyworker"
        Me.chkKeyworker.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkKeyworker.Properties.Appearance.Options.UseFont = True
        Me.chkKeyworker.Size = New System.Drawing.Size(20, 19)
        Me.chkKeyworker.TabIndex = 9
        Me.chkKeyworker.Tag = "AE"
        '
        'CareLabel27
        '
        Me.CareLabel27.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel27.Appearance.Options.UseFont = True
        Me.CareLabel27.Appearance.Options.UseTextOptions = True
        Me.CareLabel27.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel27.Location = New System.Drawing.Point(244, 58)
        Me.CareLabel27.Name = "CareLabel27"
        Me.CareLabel27.Size = New System.Drawing.Size(67, 15)
        Me.CareLabel27.TabIndex = 4
        Me.CareLabel27.Text = "Deptartment"
        Me.CareLabel27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel25
        '
        Me.CareLabel25.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel25.Appearance.Options.UseFont = True
        Me.CareLabel25.Appearance.Options.UseTextOptions = True
        Me.CareLabel25.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel25.Location = New System.Drawing.Point(8, 30)
        Me.CareLabel25.Name = "CareLabel25"
        Me.CareLabel25.Size = New System.Drawing.Size(19, 15)
        Me.CareLabel25.TabIndex = 0
        Me.CareLabel25.Text = "Site"
        Me.CareLabel25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(68, 27)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AccessibleName = "Site"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(401, 22)
        Me.cbxSite.TabIndex = 1
        Me.cbxSite.Tag = "AEM"
        Me.cbxSite.ValueMember = Nothing
        '
        'cbxGroup
        '
        Me.cbxGroup.AllowBlank = False
        Me.cbxGroup.DataSource = Nothing
        Me.cbxGroup.DisplayMember = Nothing
        Me.cbxGroup.EnterMoveNextControl = True
        Me.cbxGroup.Location = New System.Drawing.Point(68, 55)
        Me.cbxGroup.Name = "cbxGroup"
        Me.cbxGroup.Properties.AccessibleName = "Group"
        Me.cbxGroup.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxGroup.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxGroup.Properties.Appearance.Options.UseFont = True
        Me.cbxGroup.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxGroup.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxGroup.SelectedValue = Nothing
        Me.cbxGroup.Size = New System.Drawing.Size(170, 22)
        Me.cbxGroup.TabIndex = 3
        Me.cbxGroup.Tag = "AE"
        Me.cbxGroup.ValueMember = Nothing
        '
        'Label15
        '
        Me.Label15.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Appearance.Options.UseFont = True
        Me.Label15.Appearance.Options.UseTextOptions = True
        Me.Label15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label15.Location = New System.Drawing.Point(8, 58)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(32, 15)
        Me.Label15.TabIndex = 2
        Me.Label15.Text = "Room"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJobTitle
        '
        Me.txtJobTitle.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtJobTitle.EnterMoveNextControl = True
        Me.txtJobTitle.Location = New System.Drawing.Point(68, 83)
        Me.txtJobTitle.MaxLength = 100
        Me.txtJobTitle.Name = "txtJobTitle"
        Me.txtJobTitle.NumericAllowNegatives = False
        Me.txtJobTitle.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtJobTitle.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtJobTitle.Properties.AccessibleName = "Job Title"
        Me.txtJobTitle.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtJobTitle.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobTitle.Properties.Appearance.Options.UseFont = True
        Me.txtJobTitle.Properties.Appearance.Options.UseTextOptions = True
        Me.txtJobTitle.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtJobTitle.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtJobTitle.Properties.MaxLength = 100
        Me.txtJobTitle.Size = New System.Drawing.Size(286, 22)
        Me.txtJobTitle.TabIndex = 7
        Me.txtJobTitle.Tag = "AETM"
        Me.txtJobTitle.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtJobTitle.ToolTipText = ""
        '
        'Label16
        '
        Me.Label16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Appearance.Options.UseFont = True
        Me.Label16.Appearance.Options.UseTextOptions = True
        Me.Label16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label16.Location = New System.Drawing.Point(8, 86)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(44, 15)
        Me.Label16.TabIndex = 6
        Me.Label16.Text = "Job Title"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.PictureBox)
        Me.GroupControl1.Location = New System.Drawing.Point(494, 7)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(303, 207)
        Me.GroupControl1.TabIndex = 4
        Me.GroupControl1.Text = "Photo (Right Click to Amend Photo)"
        Me.GroupControl1.UseDisabledStatePainter = False
        '
        'PictureBox
        '
        Me.PictureBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBox.DocumentID = Nothing
        Me.PictureBox.Image = Nothing
        Me.PictureBox.Location = New System.Drawing.Point(2, 20)
        Me.PictureBox.Name = "PictureBox"
        Me.PictureBox.Padding = New System.Windows.Forms.Padding(1, 1, 0, 0)
        Me.PictureBox.Size = New System.Drawing.Size(299, 185)
        Me.PictureBox.TabIndex = 0
        '
        'tabMain
        '
        Me.tabMain.Location = New System.Drawing.Point(12, 55)
        Me.tabMain.Name = "tabMain"
        Me.tabMain.SelectedTabPage = Me.tabPersonal
        Me.tabMain.Size = New System.Drawing.Size(813, 515)
        Me.tabMain.TabIndex = 0
        Me.tabMain.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabPersonal, Me.tabCar, Me.tabChildren, Me.tabQualsTrain, Me.tabApps, Me.tabAbsence, Me.tabPlanner, Me.tabShifts, Me.tabTimesheets, Me.tabActivity})
        '
        'tabChildren
        '
        Me.tabChildren.Controls.Add(Me.GroupControl11)
        Me.tabChildren.Name = "tabChildren"
        Me.tabChildren.Size = New System.Drawing.Size(807, 487)
        Me.tabChildren.Text = "Children"
        '
        'GroupControl11
        '
        Me.GroupControl11.Controls.Add(Me.cgChildren)
        Me.GroupControl11.Location = New System.Drawing.Point(10, 8)
        Me.GroupControl11.Name = "GroupControl11"
        Me.GroupControl11.ShowCaption = False
        Me.GroupControl11.Size = New System.Drawing.Size(787, 472)
        Me.GroupControl11.TabIndex = 3
        Me.GroupControl11.Text = "Attendence"
        '
        'cgChildren
        '
        Me.cgChildren.AllowBuildColumns = True
        Me.cgChildren.AllowEdit = False
        Me.cgChildren.AllowHorizontalScroll = False
        Me.cgChildren.AllowMultiSelect = False
        Me.cgChildren.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cgChildren.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgChildren.Appearance.Options.UseFont = True
        Me.cgChildren.AutoSizeByData = True
        Me.cgChildren.DisableAutoSize = False
        Me.cgChildren.DisableDataFormatting = False
        Me.cgChildren.FocusedRowHandle = -2147483648
        Me.cgChildren.HideFirstColumn = False
        Me.cgChildren.Location = New System.Drawing.Point(11, 11)
        Me.cgChildren.Name = "cgChildren"
        Me.cgChildren.PreviewColumn = ""
        Me.cgChildren.QueryID = Nothing
        Me.cgChildren.RowAutoHeight = False
        Me.cgChildren.SearchAsYouType = True
        Me.cgChildren.ShowAutoFilterRow = False
        Me.cgChildren.ShowFindPanel = False
        Me.cgChildren.ShowGroupByBox = False
        Me.cgChildren.ShowLoadingPanel = False
        Me.cgChildren.ShowNavigator = False
        Me.cgChildren.Size = New System.Drawing.Size(765, 452)
        Me.cgChildren.TabIndex = 0
        '
        'tabPlanner
        '
        Me.tabPlanner.Controls.Add(Me.YearView1)
        Me.tabPlanner.Name = "tabPlanner"
        Me.tabPlanner.Size = New System.Drawing.Size(807, 487)
        Me.tabPlanner.Text = "Planner"
        '
        'YearView1
        '
        Me.YearView1.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.YearView1.Location = New System.Drawing.Point(3, 3)
        Me.YearView1.Name = "YearView1"
        Me.YearView1.Size = New System.Drawing.Size(801, 481)
        Me.YearView1.TabIndex = 0
        '
        'tabShifts
        '
        Me.tabShifts.Controls.Add(Me.GroupControl14)
        Me.tabShifts.Controls.Add(Me.cgShifts)
        Me.tabShifts.Name = "tabShifts"
        Me.tabShifts.Size = New System.Drawing.Size(807, 487)
        Me.tabShifts.Text = "Shifts"
        '
        'GroupControl14
        '
        Me.GroupControl14.Controls.Add(Me.radShiftPreferences)
        Me.GroupControl14.Controls.Add(Me.radShiftActual)
        Me.GroupControl14.Location = New System.Drawing.Point(11, 9)
        Me.GroupControl14.Name = "GroupControl14"
        Me.GroupControl14.ShowCaption = False
        Me.GroupControl14.Size = New System.Drawing.Size(785, 39)
        Me.GroupControl14.TabIndex = 4
        Me.GroupControl14.Text = "GroupControl14"
        '
        'radShiftPreferences
        '
        Me.radShiftPreferences.EditValue = True
        Me.radShiftPreferences.Location = New System.Drawing.Point(9, 10)
        Me.radShiftPreferences.Name = "radShiftPreferences"
        Me.radShiftPreferences.Properties.AutoWidth = True
        Me.radShiftPreferences.Properties.Caption = "Shift Preferences"
        Me.radShiftPreferences.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radShiftPreferences.Properties.RadioGroupIndex = 0
        Me.radShiftPreferences.Size = New System.Drawing.Size(105, 19)
        Me.radShiftPreferences.TabIndex = 2
        '
        'radShiftActual
        '
        Me.radShiftActual.Location = New System.Drawing.Point(120, 10)
        Me.radShiftActual.Name = "radShiftActual"
        Me.radShiftActual.Properties.AutoWidth = True
        Me.radShiftActual.Properties.Caption = "Actual Shifts"
        Me.radShiftActual.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radShiftActual.Properties.RadioGroupIndex = 0
        Me.radShiftActual.Size = New System.Drawing.Size(82, 19)
        Me.radShiftActual.TabIndex = 3
        Me.radShiftActual.TabStop = False
        Me.radShiftActual.Visible = False
        '
        'cgShifts
        '
        Me.cgShifts.ButtonsEnabled = False
        Me.cgShifts.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgShifts.HideFirstColumn = False
        Me.cgShifts.Location = New System.Drawing.Point(11, 54)
        Me.cgShifts.Name = "cgShifts"
        Me.cgShifts.PreviewColumn = ""
        Me.cgShifts.Size = New System.Drawing.Size(785, 426)
        Me.cgShifts.TabIndex = 0
        '
        'ctxPopup
        '
        Me.ctxPopup.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuUpload, Me.mnuFullSize, Me.mnuDelete})
        Me.ctxPopup.Name = "ctxPopup"
        Me.ctxPopup.Size = New System.Drawing.Size(153, 70)
        '
        'mnuUpload
        '
        Me.mnuUpload.Name = "mnuUpload"
        Me.mnuUpload.Size = New System.Drawing.Size(152, 22)
        Me.mnuUpload.Text = "Upload Photo"
        '
        'mnuFullSize
        '
        Me.mnuFullSize.Name = "mnuFullSize"
        Me.mnuFullSize.Size = New System.Drawing.Size(152, 22)
        Me.mnuFullSize.Text = "View Full Size"
        '
        'mnuDelete
        '
        Me.mnuDelete.Name = "mnuDelete"
        Me.mnuDelete.Size = New System.Drawing.Size(152, 22)
        Me.mnuDelete.Text = "Remove Photo"
        '
        'btnRecruit
        '
        Me.btnRecruit.Location = New System.Drawing.Point(12, 5)
        Me.btnRecruit.Name = "btnRecruit"
        Me.btnRecruit.Size = New System.Drawing.Size(173, 25)
        Me.btnRecruit.TabIndex = 0
        Me.btnRecruit.Text = "Enter Recruitment Detail"
        '
        'frmStaff
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(833, 605)
        Me.Controls.Add(Me.tabMain)
        Me.MinimumSize = New System.Drawing.Size(544, 289)
        Me.Name = "frmStaff"
        Me.Text = "Staff"
        Me.Controls.SetChildIndex(Me.tabMain, 0)
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.tabActivity.ResumeLayout(False)
        Me.tabTimesheets.ResumeLayout(False)
        CType(Me.gbxPayroll, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxPayroll.ResumeLayout(False)
        Me.gbxPayroll.PerformLayout()
        CType(Me.txtPayrollID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNINO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTaxCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNICat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxAttendence, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxAttendence.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.txtSalaryPerHour.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtContractedAnnual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtContracted4Week.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFTEAnnual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtContractedWeek.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFTEWeek.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtHoursContracted.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSalaryUnit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFTE4Week.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSalary.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtHoursFTE.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabAbsence.ResumeLayout(False)
        CType(Me.GroupControl17, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl17.ResumeLayout(False)
        Me.GroupControl17.PerformLayout()
        CType(Me.txtStatSickness.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStatTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStatHols.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStatOther.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl16, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl16.ResumeLayout(False)
        Me.GroupControl16.PerformLayout()
        CType(Me.txtHolsEntitlement.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxHolsFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl15, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl15.ResumeLayout(False)
        Me.GroupControl15.PerformLayout()
        CType(Me.txtStatHolsLeft.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStatBS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl7.ResumeLayout(False)
        Me.GroupControl7.PerformLayout()
        CType(Me.chkAbsenceNotes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxAbsenceType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabApps.ResumeLayout(False)
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl6.ResumeLayout(False)
        Me.tabQualsTrain.ResumeLayout(False)
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.tabCar.ResumeLayout(False)
        CType(Me.GroupBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.gbxPaxton, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxPaxton.ResumeLayout(False)
        Me.gbxPaxton.PerformLayout()
        CType(Me.cbxPaxtonDepartment.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxPaxtonAccessLevel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPaxtonUserID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.chkCar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkDriver.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.udtInsCheck.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.udtInsCheck.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMakeModel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtReg.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        CType(Me.txtICEMobile.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtICEHome.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtICERel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtICEName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.txtDBSPosition.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxDBSCheckType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDBSIssued.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDBSIssued.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCRBRef.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.udtCRBDue.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.udtCRBDue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.udtCRBLast.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.udtCRBLast.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabPersonal.ResumeLayout(False)
        CType(Me.GroupControl13, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl13.ResumeLayout(False)
        Me.GroupControl13.PerformLayout()
        CType(Me.cbxQualLevel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl12.ResumeLayout(False)
        Me.GroupControl12.PerformLayout()
        CType(Me.udtLeft.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.udtLeft.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.udtStarted.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.udtStarted.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl9.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        CType(Me.chkENCO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSENCO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkHygiene.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSafe.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAidWork.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkFS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkPaeds.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkFSAid.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl10.ResumeLayout(False)
        Me.GroupControl10.PerformLayout()
        CType(Me.ccxLanguages.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ccxNationality.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxEthnicity.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxReligion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl8.ResumeLayout(False)
        Me.GroupControl8.PerformLayout()
        CType(Me.txtTouchPIN.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.cbxContractType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.udtDOB.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.udtDOB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtForename.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFullname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.cbxManager.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkLineManager.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxDepartment.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkKeyworker.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxGroup.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJobTitle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.tabMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabMain.ResumeLayout(False)
        Me.tabChildren.ResumeLayout(False)
        CType(Me.GroupControl11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl11.ResumeLayout(False)
        Me.tabPlanner.ResumeLayout(False)
        Me.tabShifts.ResumeLayout(False)
        CType(Me.GroupControl14, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl14.ResumeLayout(False)
        Me.GroupControl14.PerformLayout()
        CType(Me.radShiftPreferences.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radShiftActual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SchedulerStorage1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ctxPopup.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tabActivity As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabTimesheets As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabAbsence As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabApps As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabQualsTrain As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tabCar As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents chkCar As Care.Controls.CareCheckBox
    Friend WithEvents chkDriver As Care.Controls.CareCheckBox
    Friend WithEvents Label11 As Care.Controls.CareLabel
    Friend WithEvents Label17 As Care.Controls.CareLabel
    Friend WithEvents udtInsCheck As Care.Controls.CareDateTime
    Friend WithEvents txtMakeModel As Care.Controls.CareTextBox
    Friend WithEvents Label20 As Care.Controls.CareLabel
    Friend WithEvents txtReg As Care.Controls.CareTextBox
    Friend WithEvents Label21 As Care.Controls.CareLabel
    Friend WithEvents Label22 As Care.Controls.CareLabel
    Friend WithEvents txtICEMobile As Care.Controls.CareTextBox
    Friend WithEvents Label27 As Care.Controls.CareLabel
    Friend WithEvents txtICEHome As Care.Controls.CareTextBox
    Friend WithEvents Label30 As Care.Controls.CareLabel
    Friend WithEvents txtICERel As Care.Controls.CareTextBox
    Friend WithEvents Label31 As Care.Controls.CareLabel
    Friend WithEvents txtICEName As Care.Controls.CareTextBox
    Friend WithEvents Label32 As Care.Controls.CareLabel
    Friend WithEvents udtCRBDue As Care.Controls.CareDateTime
    Friend WithEvents udtCRBLast As Care.Controls.CareDateTime
    Friend WithEvents Label7 As Care.Controls.CareLabel
    Friend WithEvents Label8 As Care.Controls.CareLabel
    Friend WithEvents tabPersonal As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cbxStatus As Care.Controls.CareComboBox
    Friend WithEvents Label10 As Care.Controls.CareLabel
    Friend WithEvents udtDOB As Care.Controls.CareDateTime
    Friend WithEvents Label5 As Care.Controls.CareLabel
    Friend WithEvents txtForename As Care.Controls.CareTextBox
    Friend WithEvents Label3 As Care.Controls.CareLabel
    Friend WithEvents txtSurname As Care.Controls.CareTextBox
    Friend WithEvents txtFullname As Care.Controls.CareTextBox
    Friend WithEvents Label1 As Care.Controls.CareLabel
    Friend WithEvents udtLeft As Care.Controls.CareDateTime
    Friend WithEvents cbxGroup As Care.Controls.CareComboBox
    Friend WithEvents chkKeyworker As Care.Controls.CareCheckBox
    Friend WithEvents Label9 As Care.Controls.CareLabel
    Friend WithEvents Label12 As Care.Controls.CareLabel
    Friend WithEvents udtStarted As Care.Controls.CareDateTime
    Friend WithEvents Label13 As Care.Controls.CareLabel
    Friend WithEvents Label15 As Care.Controls.CareLabel
    Friend WithEvents txtJobTitle As Care.Controls.CareTextBox
    Friend WithEvents Label16 As Care.Controls.CareLabel
    Friend WithEvents tabMain As Care.Controls.CareTab
    Friend WithEvents cgTimesheets As Care.Controls.CareGrid
    Friend WithEvents txtSalary As Care.Controls.CareTextBox
    Friend WithEvents Label18 As Care.Controls.CareLabel
    Friend WithEvents txtHoursFTE As Care.Controls.CareTextBox
    Friend WithEvents Label4 As Care.Controls.CareLabel
    Friend WithEvents txtPayrollID As Care.Controls.CareTextBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents txtNINO As Care.Controls.CareTextBox
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents txtTaxCode As Care.Controls.CareTextBox
    Friend WithEvents txtNICat As Care.Controls.CareTextBox
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents txtFTE4Week As Care.Controls.CareTextBox
    Friend WithEvents cbxContractType As Care.Controls.CareComboBox
    Friend WithEvents txtContractedWeek As Care.Controls.CareTextBox
    Friend WithEvents txtFTEWeek As Care.Controls.CareTextBox
    Friend WithEvents txtHoursContracted As Care.Controls.CareTextBox
    Friend WithEvents cbxSalaryUnit As Care.Controls.CareComboBox
    Friend WithEvents txtContractedAnnual As Care.Controls.CareTextBox
    Friend WithEvents txtContracted4Week As Care.Controls.CareTextBox
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents txtFTEAnnual As Care.Controls.CareTextBox
    Friend WithEvents CareLabel13 As Care.Controls.CareLabel
    Friend WithEvents CareLabel12 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents btnPaySlip As Care.Controls.CareButton
    Friend WithEvents btnPayEmployee As Care.Controls.CareButton
    Friend WithEvents cbxPaxtonAccessLevel As Care.Controls.CareComboBox
    Friend WithEvents btnPaxtonUpdate As Care.Controls.CareButton
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents txtPaxtonUserID As Care.Controls.CareTextBox
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents CareLabel15 As Care.Controls.CareLabel
    Friend WithEvents btnPaxtonLink As Care.Controls.CareButton
    Friend WithEvents btnPaxtonDisable As Care.Controls.CareButton
    Friend WithEvents SchedulerStorage1 As DevExpress.XtraScheduler.SchedulerStorage
    Friend WithEvents cgDis As Care.Controls.CareGridWithButtons
    Friend WithEvents cgAppraisals As Care.Controls.CareGridWithButtons
    Friend WithEvents cgQuals As Care.Controls.CareGridWithButtons
    Friend WithEvents cgTraining As Care.Controls.CareGridWithButtons
    Friend WithEvents cgAbs As Care.Controls.CareGridWithButtons
    Friend WithEvents ctxPopup As ContextMenuStrip
    Friend WithEvents mnuUpload As ToolStripMenuItem
    Friend WithEvents mnuFullSize As ToolStripMenuItem
    Friend WithEvents mnuDelete As ToolStripMenuItem
    Friend WithEvents OpenFileDialog As OpenFileDialog
    Friend WithEvents lblSalaryPer As Care.Controls.CareLabel
    Friend WithEvents txtSalaryPerHour As Care.Controls.CareTextBox
    Friend WithEvents PictureBox As Care.Controls.PhotoControl
    Friend WithEvents lblAge As Care.Controls.CareLabel
    Friend WithEvents txtTouchPIN As Care.Controls.CareTextBox
    Friend WithEvents CareLabel16 As Care.Controls.CareLabel
    Friend WithEvents tabPlanner As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents CrmActivity1 As Care.Shared.CRMActivity
    Friend WithEvents YearView1 As Nursery.YearView
    Friend WithEvents CareLabel17 As Care.Controls.CareLabel
    Friend WithEvents txtCRBRef As Care.Controls.CareTextBox
    Friend WithEvents CareLabel18 As Care.Controls.CareLabel
    Friend WithEvents cbxAbsenceType As Care.Controls.CareComboBox
    Friend WithEvents txtTelHome As Care.Shared.CareTelephoneNumber
    Friend WithEvents txtTelMobile As Care.Shared.CareTelephoneNumber
    Friend WithEvents txtEmail As Care.Shared.CareEmailAddress
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents txtAddress As Care.Address.CareAddress
    Friend WithEvents Label25 As Care.Controls.CareLabel
    Friend WithEvents Label23 As Care.Controls.CareLabel
    Friend WithEvents Label24 As Care.Controls.CareLabel
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents CareLabel19 As Care.Controls.CareLabel
    Friend WithEvents chkHygiene As Care.Controls.CareCheckBox
    Friend WithEvents chkSafe As Care.Controls.CareCheckBox
    Friend WithEvents lblMedicine As Care.Controls.CareLabel
    Friend WithEvents lblObs As Care.Controls.CareLabel
    Friend WithEvents lblSleep As Care.Controls.CareLabel
    Friend WithEvents lblMilk As Care.Controls.CareLabel
    Friend WithEvents lblNappy As Care.Controls.CareLabel
    Friend WithEvents chkAidWork As Care.Controls.CareCheckBox
    Friend WithEvents chkFS As Care.Controls.CareCheckBox
    Friend WithEvents lblFood As Care.Controls.CareLabel
    Friend WithEvents chkPaeds As Care.Controls.CareCheckBox
    Friend WithEvents chkFSAid As Care.Controls.CareCheckBox
    Friend WithEvents ccxLanguages As Care.Controls.CareCheckedComboBox
    Friend WithEvents ccxNationality As Care.Controls.CareCheckedComboBox
    Friend WithEvents CareLabel20 As Care.Controls.CareLabel
    Friend WithEvents CareLabel21 As Care.Controls.CareLabel
    Friend WithEvents cbxEthnicity As Care.Controls.CareComboBox
    Friend WithEvents cbxReligion As Care.Controls.CareComboBox
    Friend WithEvents CareLabel22 As Care.Controls.CareLabel
    Friend WithEvents CareLabel23 As Care.Controls.CareLabel
    Friend WithEvents chkENCO As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel24 As Care.Controls.CareLabel
    Friend WithEvents chkSENCO As Care.Controls.CareCheckBox
    Friend WithEvents tabChildren As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cgChildren As Care.Controls.CareGrid
    Friend WithEvents cbxPaxtonDepartment As Care.Controls.CareComboBox
    Friend WithEvents cbxQualLevel As Care.Controls.CareComboBox
    Friend WithEvents CareLabel10 As Care.Controls.CareLabel
    Friend WithEvents lblYearsLeft As Care.Controls.CareLabel
    Friend WithEvents lblYearsService As Care.Controls.CareLabel
    Friend WithEvents CareLabel25 As Care.Controls.CareLabel
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
    Friend WithEvents tabShifts As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents btnPIN As Care.Controls.CareButton
    Friend WithEvents cgShifts As Care.Controls.CareGridWithButtons
    Friend WithEvents radShiftPreferences As Care.Controls.CareRadioButton
    Friend WithEvents radShiftActual As Care.Controls.CareRadioButton
    Friend WithEvents CareLabel26 As Care.Controls.CareLabel
    Friend WithEvents cbxManager As Care.Controls.CareComboBox
    Friend WithEvents CareLabel29 As Care.Controls.CareLabel
    Friend WithEvents chkLineManager As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel28 As Care.Controls.CareLabel
    Friend WithEvents cbxDepartment As Care.Controls.CareComboBox
    Friend WithEvents CareLabel27 As Care.Controls.CareLabel
    Friend WithEvents btnRecruit As Care.Controls.CareButton
    Friend WithEvents CareLabel41 As Care.Controls.CareLabel
    Friend WithEvents CareLabel39 As Care.Controls.CareLabel
    Friend WithEvents CareLabel38 As Care.Controls.CareLabel
    Friend WithEvents CareLabel37 As Care.Controls.CareLabel
    Friend WithEvents CareLabel36 As Care.Controls.CareLabel
    Friend WithEvents CareLabel34 As Care.Controls.CareLabel
    Friend WithEvents CareLabel33 As Care.Controls.CareLabel
    Friend WithEvents txtStatSickness As Care.Controls.CareTextBox
    Friend WithEvents txtStatTotal As Care.Controls.CareTextBox
    Friend WithEvents txtStatHols As Care.Controls.CareTextBox
    Friend WithEvents txtStatOther As Care.Controls.CareTextBox
    Friend WithEvents lblDaysSince As Care.Controls.CareLabel
    Friend WithEvents CareLabel32 As Care.Controls.CareLabel
    Friend WithEvents txtHolsEntitlement As Care.Controls.CareTextBox
    Friend WithEvents CareLabel31 As Care.Controls.CareLabel
    Friend WithEvents cbxHolsFrom As Care.Controls.CareComboBox
    Friend WithEvents lblBradfordScore As Care.Controls.CareLabel
    Friend WithEvents txtStatHolsLeft As Care.Controls.CareTextBox
    Friend WithEvents txtStatBS As Care.Controls.CareTextBox
    Friend WithEvents CareLabel35 As Care.Controls.CareLabel
    Friend WithEvents chkAbsenceNotes As Care.Controls.CareCheckBox
    Friend WithEvents GroupBox4 As Care.Controls.CareFrame
    Friend WithEvents GroupBox6 As Care.Controls.CareFrame
    Friend WithEvents GroupBox2 As Care.Controls.CareFrame
    Friend WithEvents GroupControl1 As Care.Controls.CareFrame
    Friend WithEvents GroupBox1 As Care.Controls.CareFrame
    Friend WithEvents GroupBox3 As Care.Controls.CareFrame
    Friend WithEvents GroupControl2 As Care.Controls.CareFrame
    Friend WithEvents gbxPayroll As Care.Controls.CareFrame
    Friend WithEvents gbxPaxton As Care.Controls.CareFrame
    Friend WithEvents gbxAttendence As Care.Controls.CareFrame
    Friend WithEvents GroupControl5 As Care.Controls.CareFrame
    Friend WithEvents GroupControl6 As Care.Controls.CareFrame
    Friend WithEvents GroupControl4 As Care.Controls.CareFrame
    Friend WithEvents GroupControl3 As Care.Controls.CareFrame
    Friend WithEvents GroupControl7 As Care.Controls.CareFrame
    Friend WithEvents GroupControl8 As Care.Controls.CareFrame
    Friend WithEvents GroupBox5 As Care.Controls.CareFrame
    Friend WithEvents GroupControl9 As Care.Controls.CareFrame
    Friend WithEvents GroupControl10 As Care.Controls.CareFrame
    Friend WithEvents GroupControl11 As Care.Controls.CareFrame
    Friend WithEvents GroupControl13 As Care.Controls.CareFrame
    Friend WithEvents GroupControl12 As Care.Controls.CareFrame
    Friend WithEvents GroupControl14 As Care.Controls.CareFrame
    Friend WithEvents GroupControl17 As Care.Controls.CareFrame
    Friend WithEvents GroupControl16 As Care.Controls.CareFrame
    Friend WithEvents GroupControl15 As Care.Controls.CareFrame
    Friend WithEvents CareLabel42 As Care.Controls.CareLabel
    Friend WithEvents txtDBSPosition As Care.Controls.CareTextBox
    Friend WithEvents CareLabel40 As Care.Controls.CareLabel
    Friend WithEvents CareLabel30 As Care.Controls.CareLabel
    Friend WithEvents cbxDBSCheckType As Care.Controls.CareComboBox
    Friend WithEvents cdtDBSIssued As Care.Controls.CareDateTime
End Class
