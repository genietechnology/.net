﻿

Imports Care.Global

Public Class frmTodayDrillDown

    Public Enum EnumFormToLoad
        None
        Child
        Staff
        AutoSelect
        Lead
    End Enum

    Private m_SQL As String = ""
    Private m_FormToLoad As EnumFormToLoad

    Private Sub frmTodayDrillDown_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Session.CursorWaiting()

        cgRecords.Populate(Session.ConnectionString, m_SQL)

        For Each c As DevExpress.XtraGrid.Columns.GridColumn In cgRecords.Columns

            Select Case c.FieldName

                Case "Viewing Date"
                    c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
                    c.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm"

                Case "Checked-In", "Date/Time"
                    c.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
                    c.DisplayFormat.FormatString = "HH:mm"
            End Select

        Next

        Session.CursorDefault()

    End Sub

    Public Sub New(ByVal SQL As String, ByVal FormCaption As String, ByVal FormToLoad As EnumFormToLoad)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.Text = FormCaption

        m_SQL = SQL
        m_FormToLoad = FormToLoad

    End Sub

    Private Sub cgRecords_GridDoubleClick(sender As Object, e As EventArgs) Handles cgRecords.GridDoubleClick

        If m_FormToLoad = EnumFormToLoad.None Then Exit Sub

        Dim _ID As String = cgRecords.CurrentRow.Item(0).ToString
        If _ID <> "" Then

            Select Case m_FormToLoad

                Case EnumFormToLoad.Child
                    Business.Child.DrillDown(New Guid(_ID))

                Case EnumFormToLoad.Staff
                    Business.Staff.DrillDown(New Guid(_ID))

                Case EnumFormToLoad.AutoSelect
                    If cgRecords.CurrentRow.Item("Type").ToString = "Child" Then
                        Business.Child.DrillDown(New Guid(_ID))
                    Else
                        Business.Staff.DrillDown(New Guid(_ID))
                    End If

                Case EnumFormToLoad.Lead
                    Dim _frmLead As New frmLead(New Guid(_ID))
                    _frmLead.ShowDialog()

                    _frmLead.Dispose()
                    _frmLead = Nothing

            End Select

        End If

    End Sub
End Class
