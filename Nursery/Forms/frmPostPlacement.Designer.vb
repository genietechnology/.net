﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPostPlacement
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnOK = New Care.Controls.CareButton(Me.components)
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        Me.GroupBox1 = New Care.Controls.CareFrame(Me.components)
        Me.txtPlacement = New Care.Controls.CareTextBox(Me.components)
        Me.txtPlacementAgency = New Care.Controls.CareTextBox(Me.components)
        Me.txtPlacementReferee = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.Label13 = New Care.Controls.CareLabel(Me.components)
        Me.Label14 = New Care.Controls.CareLabel(Me.components)
        Me.btnFamilyView = New Care.Controls.CareButton(Me.components)
        Me.btnFamilySet = New Care.Controls.CareButton(Me.components)
        Me.Label18 = New Care.Controls.CareLabel(Me.components)
        Me.txtSurname = New Care.Controls.CareTextBox(Me.components)
        Me.lblFamily = New Care.Controls.CareLabel(Me.components)
        Me.txtForename = New Care.Controls.CareTextBox(Me.components)
        Me.txtFamily = New Care.Controls.CareTextBox(Me.components)
        Me.Label2 = New Care.Controls.CareLabel(Me.components)
        Me.btnClear = New Care.Controls.CareButton(Me.components)
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtPlacement.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPlacementAgency.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPlacementReferee.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtForename.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFamily.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'btnOK
        '
        Me.btnOK.DialogResult = DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(368, 210)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(85, 25)
        Me.btnOK.TabIndex = 9
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(459, 210)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 25)
        Me.btnCancel.TabIndex = 10
        Me.btnCancel.Text = "Cancel"
        '
        'GroupBox1
        '
        Me.GroupBox1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.GroupBox1.Controls.Add(Me.btnClear)
        Me.GroupBox1.Controls.Add(Me.txtPlacement)
        Me.GroupBox1.Controls.Add(Me.txtPlacementAgency)
        Me.GroupBox1.Controls.Add(Me.txtPlacementReferee)
        Me.GroupBox1.Controls.Add(Me.CareLabel14)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.btnFamilyView)
        Me.GroupBox1.Controls.Add(Me.btnFamilySet)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.txtSurname)
        Me.GroupBox1.Controls.Add(Me.lblFamily)
        Me.GroupBox1.Controls.Add(Me.txtForename)
        Me.GroupBox1.Controls.Add(Me.txtFamily)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(532, 192)
        Me.GroupBox1.TabIndex = 13
        '
        'txtPlacement
        '
        Me.txtPlacement.CharacterCasing = CharacterCasing.Normal
        Me.txtPlacement.EnterMoveNextControl = True
        Me.txtPlacement.Location = New System.Drawing.Point(89, 98)
        Me.txtPlacement.MaxLength = 30
        Me.txtPlacement.Name = "txtPlacement"
        Me.txtPlacement.NumericAllowNegatives = False
        Me.txtPlacement.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtPlacement.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPlacement.Properties.AccessibleDescription = ""
        Me.txtPlacement.Properties.AccessibleName = "Forename"
        Me.txtPlacement.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPlacement.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtPlacement.Properties.Appearance.Options.UseFont = True
        Me.txtPlacement.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPlacement.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPlacement.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPlacement.Properties.MaxLength = 30
        Me.txtPlacement.Size = New System.Drawing.Size(199, 22)
        Me.txtPlacement.TabIndex = 6
        Me.txtPlacement.Tag = "AEBM"
        Me.txtPlacement.TextAlign = HorizontalAlignment.Left
        Me.txtPlacement.ToolTipText = ""
        '
        'txtPlacementAgency
        '
        Me.txtPlacementAgency.CharacterCasing = CharacterCasing.Normal
        Me.txtPlacementAgency.EnterMoveNextControl = True
        Me.txtPlacementAgency.Location = New System.Drawing.Point(89, 154)
        Me.txtPlacementAgency.MaxLength = 30
        Me.txtPlacementAgency.Name = "txtPlacementAgency"
        Me.txtPlacementAgency.NumericAllowNegatives = False
        Me.txtPlacementAgency.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtPlacementAgency.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPlacementAgency.Properties.AccessibleDescription = ""
        Me.txtPlacementAgency.Properties.AccessibleName = "Forename"
        Me.txtPlacementAgency.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPlacementAgency.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtPlacementAgency.Properties.Appearance.Options.UseFont = True
        Me.txtPlacementAgency.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPlacementAgency.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPlacementAgency.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPlacementAgency.Properties.MaxLength = 30
        Me.txtPlacementAgency.Size = New System.Drawing.Size(199, 22)
        Me.txtPlacementAgency.TabIndex = 8
        Me.txtPlacementAgency.Tag = "AEBM"
        Me.txtPlacementAgency.TextAlign = HorizontalAlignment.Left
        Me.txtPlacementAgency.ToolTipText = ""
        '
        'txtPlacementReferee
        '
        Me.txtPlacementReferee.CharacterCasing = CharacterCasing.Normal
        Me.txtPlacementReferee.EnterMoveNextControl = True
        Me.txtPlacementReferee.Location = New System.Drawing.Point(89, 126)
        Me.txtPlacementReferee.MaxLength = 30
        Me.txtPlacementReferee.Name = "txtPlacementReferee"
        Me.txtPlacementReferee.NumericAllowNegatives = False
        Me.txtPlacementReferee.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtPlacementReferee.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPlacementReferee.Properties.AccessibleDescription = ""
        Me.txtPlacementReferee.Properties.AccessibleName = "Forename"
        Me.txtPlacementReferee.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtPlacementReferee.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtPlacementReferee.Properties.Appearance.Options.UseFont = True
        Me.txtPlacementReferee.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPlacementReferee.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtPlacementReferee.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPlacementReferee.Properties.MaxLength = 30
        Me.txtPlacementReferee.Size = New System.Drawing.Size(199, 22)
        Me.txtPlacementReferee.TabIndex = 7
        Me.txtPlacementReferee.Tag = "AEBM"
        Me.txtPlacementReferee.TextAlign = HorizontalAlignment.Left
        Me.txtPlacementReferee.ToolTipText = ""
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(10, 129)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(39, 15)
        Me.CareLabel14.TabIndex = 9
        Me.CareLabel14.Text = "Referee"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel14.ToolTip = "The last date the child will attend the Nursery. This date is used for invoicing " &
    "purposes."
        Me.CareLabel14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'Label13
        '
        Me.Label13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label13.Location = New System.Drawing.Point(9, 101)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(56, 15)
        Me.Label13.TabIndex = 11
        Me.Label13.Text = "Placement"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label14
        '
        Me.Label14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label14.Location = New System.Drawing.Point(9, 157)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(40, 15)
        Me.Label14.TabIndex = 10
        Me.Label14.Text = "Agency"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnFamilyView
        '
        Me.btnFamilyView.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnFamilyView.Appearance.Options.UseFont = True
        Me.btnFamilyView.CausesValidation = False
        Me.btnFamilyView.Location = New System.Drawing.Point(417, 13)
        Me.btnFamilyView.Name = "btnFamilyView"
        Me.btnFamilyView.Size = New System.Drawing.Size(50, 23)
        Me.btnFamilyView.TabIndex = 3
        Me.btnFamilyView.Text = "View"
        '
        'btnFamilySet
        '
        Me.btnFamilySet.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnFamilySet.Appearance.Options.UseFont = True
        Me.btnFamilySet.CausesValidation = False
        Me.btnFamilySet.Location = New System.Drawing.Point(361, 13)
        Me.btnFamilySet.Name = "btnFamilySet"
        Me.btnFamilySet.Size = New System.Drawing.Size(50, 23)
        Me.btnFamilySet.TabIndex = 2
        Me.btnFamilySet.Tag = "AE"
        Me.btnFamilySet.Text = "Set"
        '
        'Label18
        '
        Me.Label18.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label18.Location = New System.Drawing.Point(9, 73)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(47, 15)
        Me.Label18.TabIndex = 8
        Me.Label18.Text = "Surname"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSurname
        '
        Me.txtSurname.CharacterCasing = CharacterCasing.Normal
        Me.txtSurname.EnterMoveNextControl = True
        Me.txtSurname.Location = New System.Drawing.Point(89, 70)
        Me.txtSurname.MaxLength = 30
        Me.txtSurname.Name = "txtSurname"
        Me.txtSurname.NumericAllowNegatives = False
        Me.txtSurname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtSurname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSurname.Properties.AccessibleName = "Surname"
        Me.txtSurname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtSurname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtSurname.Properties.Appearance.Options.UseFont = True
        Me.txtSurname.Properties.Appearance.Options.UseTextOptions = True
        Me.txtSurname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtSurname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtSurname.Properties.MaxLength = 30
        Me.txtSurname.Size = New System.Drawing.Size(378, 22)
        Me.txtSurname.TabIndex = 5
        Me.txtSurname.Tag = "AEBM"
        Me.txtSurname.TextAlign = HorizontalAlignment.Left
        Me.txtSurname.ToolTipText = ""
        '
        'lblFamily
        '
        Me.lblFamily.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblFamily.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblFamily.Location = New System.Drawing.Point(9, 17)
        Me.lblFamily.Name = "lblFamily"
        Me.lblFamily.Size = New System.Drawing.Size(35, 15)
        Me.lblFamily.TabIndex = 0
        Me.lblFamily.Text = "Family"
        Me.lblFamily.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtForename
        '
        Me.txtForename.CharacterCasing = CharacterCasing.Normal
        Me.txtForename.EnterMoveNextControl = True
        Me.txtForename.Location = New System.Drawing.Point(89, 42)
        Me.txtForename.MaxLength = 30
        Me.txtForename.Name = "txtForename"
        Me.txtForename.NumericAllowNegatives = False
        Me.txtForename.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtForename.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtForename.Properties.AccessibleDescription = ""
        Me.txtForename.Properties.AccessibleName = "Forename"
        Me.txtForename.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtForename.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtForename.Properties.Appearance.Options.UseFont = True
        Me.txtForename.Properties.Appearance.Options.UseTextOptions = True
        Me.txtForename.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtForename.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtForename.Properties.MaxLength = 30
        Me.txtForename.Size = New System.Drawing.Size(127, 22)
        Me.txtForename.TabIndex = 4
        Me.txtForename.Tag = "AEBM"
        Me.txtForename.TextAlign = HorizontalAlignment.Left
        Me.txtForename.ToolTipText = ""
        '
        'txtFamily
        '
        Me.txtFamily.CharacterCasing = CharacterCasing.Normal
        Me.txtFamily.EnterMoveNextControl = True
        Me.txtFamily.Location = New System.Drawing.Point(89, 14)
        Me.txtFamily.MaxLength = 0
        Me.txtFamily.Name = "txtFamily"
        Me.txtFamily.NumericAllowNegatives = False
        Me.txtFamily.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtFamily.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFamily.Properties.AccessibleName = "Family"
        Me.txtFamily.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFamily.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtFamily.Properties.Appearance.Options.UseFont = True
        Me.txtFamily.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFamily.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFamily.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFamily.Size = New System.Drawing.Size(266, 22)
        Me.txtFamily.TabIndex = 1
        Me.txtFamily.Tag = "RM"
        Me.txtFamily.TextAlign = HorizontalAlignment.Left
        Me.txtFamily.ToolTipText = ""
        '
        'Label2
        '
        Me.Label2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label2.Location = New System.Drawing.Point(9, 45)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 15)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Forename"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnClear
        '
        Me.btnClear.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnClear.Appearance.Options.UseFont = True
        Me.btnClear.CausesValidation = False
        Me.btnClear.Location = New System.Drawing.Point(473, 13)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(50, 23)
        Me.btnClear.TabIndex = 12
        Me.btnClear.Text = "Clear"
        '
        'frmPostPlacement
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(552, 243)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Name = "frmPostPlacement"
        Me.Text = "Post Placement Details"
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.txtPlacement.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPlacementAgency.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPlacementReferee.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtForename.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFamily.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents GroupBox1 As Care.Controls.CareFrame
    Friend WithEvents btnFamilyView As Care.Controls.CareButton
    Friend WithEvents btnFamilySet As Care.Controls.CareButton
    Friend WithEvents Label18 As Care.Controls.CareLabel
    Friend WithEvents txtSurname As Care.Controls.CareTextBox
    Friend WithEvents lblFamily As Care.Controls.CareLabel
    Friend WithEvents txtForename As Care.Controls.CareTextBox
    Friend WithEvents txtFamily As Care.Controls.CareTextBox
    Friend WithEvents Label2 As Care.Controls.CareLabel
    Friend WithEvents txtPlacement As Care.Controls.CareTextBox
    Friend WithEvents txtPlacementAgency As Care.Controls.CareTextBox
    Friend WithEvents txtPlacementReferee As Care.Controls.CareTextBox
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents Label13 As Care.Controls.CareLabel
    Friend WithEvents Label14 As Care.Controls.CareLabel
    Friend WithEvents btnClear As Care.Controls.CareButton
End Class
