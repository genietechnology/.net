﻿Imports Care.Global

Public Class frmPaymentMethods

    Dim m_PayMeth As Business.PaymentMethod

    Private Sub frmTransactionItem_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.GridSQL = "select id, name as 'Name' from PaymentMethods" & _
                     " order by name"

        gbx.Hide()

    End Sub

    Protected Overrides Sub FormatGrid()
        'ug.DisplayLayout.Bands(0).Columns("id").Hidden = True
    End Sub

    Protected Overrides Sub SetBindings()

        m_PayMeth = New Business.PaymentMethod
        bs.DataSource = m_PayMeth

        txtName.DataBindings.Add("Text", bs, "_name")

    End Sub

    Protected Overrides Sub CommitUpdate()
        Business.PaymentMethod.SavePaymentMethod(CType(bs.Item(bs.Position), Business.PaymentMethod))
    End Sub

    Protected Overrides Sub BindToID(ID As System.Guid, IsNew As Boolean)
        m_PayMeth = Business.PaymentMethod.RetreiveByID(ID)
        bs.DataSource = m_PayMeth
    End Sub

End Class
