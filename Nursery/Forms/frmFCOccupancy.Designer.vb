﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFCOccupancy
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl1 = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.chkExclHolidays = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.chkExclWL = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.cbxSite = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.cbxOutputFormat = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.cdtTo = New Care.Controls.CareDateTime(Me.components)
        Me.cdtFrom = New Care.Controls.CareDateTime(Me.components)
        Me.btnPrint = New Care.Controls.CareButton(Me.components)
        Me.btnRefresh = New Care.Controls.CareButton(Me.components)
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.CareFrame1 = New Care.Controls.CareFrame(Me.components)
        Me.grdTasks = New Care.Controls.CareGrid()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.chkExclHolidays.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkExclWL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxOutputFormat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CareFrame1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CareFrame1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.CareLabel6)
        Me.GroupControl1.Controls.Add(Me.chkExclHolidays)
        Me.GroupControl1.Controls.Add(Me.CareLabel5)
        Me.GroupControl1.Controls.Add(Me.chkExclWL)
        Me.GroupControl1.Controls.Add(Me.CareLabel4)
        Me.GroupControl1.Controls.Add(Me.cbxSite)
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Controls.Add(Me.cbxOutputFormat)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Controls.Add(Me.cdtTo)
        Me.GroupControl1.Controls.Add(Me.cdtFrom)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 10)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(333, 163)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Forecasting Range"
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(10, 140)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(136, 15)
        Me.CareLabel6.TabIndex = 10
        Me.CareLabel6.Text = "Exclude Holiday Bookings"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkExclHolidays
        '
        Me.chkExclHolidays.EnterMoveNextControl = True
        Me.chkExclHolidays.Location = New System.Drawing.Point(181, 138)
        Me.chkExclHolidays.Name = "chkExclHolidays"
        Me.chkExclHolidays.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkExclHolidays.Properties.Appearance.Options.UseFont = True
        Me.chkExclHolidays.Properties.Caption = ""
        Me.chkExclHolidays.Size = New System.Drawing.Size(19, 19)
        Me.chkExclHolidays.TabIndex = 11
        Me.chkExclHolidays.Tag = ""
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(10, 115)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(153, 15)
        Me.CareLabel5.TabIndex = 8
        Me.CareLabel5.Text = "Exclude Waiting List Children"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkExclWL
        '
        Me.chkExclWL.EnterMoveNextControl = True
        Me.chkExclWL.Location = New System.Drawing.Point(181, 113)
        Me.chkExclWL.Name = "chkExclWL"
        Me.chkExclWL.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkExclWL.Properties.Appearance.Options.UseFont = True
        Me.chkExclWL.Properties.Caption = ""
        Me.chkExclWL.Size = New System.Drawing.Size(19, 19)
        Me.chkExclWL.TabIndex = 9
        Me.chkExclWL.Tag = ""
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(10, 32)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(19, 15)
        Me.CareLabel4.TabIndex = 0
        Me.CareLabel4.Text = "Site"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(100, 29)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AccessibleName = "Site"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(223, 22)
        Me.cbxSite.TabIndex = 1
        Me.cbxSite.Tag = "AEM"
        Me.cbxSite.ValueMember = Nothing
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(10, 88)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(79, 15)
        Me.CareLabel3.TabIndex = 6
        Me.CareLabel3.Text = "Output Format"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxOutputFormat
        '
        Me.cbxOutputFormat.AllowBlank = False
        Me.cbxOutputFormat.DataSource = Nothing
        Me.cbxOutputFormat.DisplayMember = Nothing
        Me.cbxOutputFormat.EnterMoveNextControl = True
        Me.cbxOutputFormat.Location = New System.Drawing.Point(100, 85)
        Me.cbxOutputFormat.Name = "cbxOutputFormat"
        Me.cbxOutputFormat.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxOutputFormat.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxOutputFormat.Properties.Appearance.Options.UseFont = True
        Me.cbxOutputFormat.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxOutputFormat.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxOutputFormat.SelectedValue = Nothing
        Me.cbxOutputFormat.Size = New System.Drawing.Size(223, 22)
        Me.cbxOutputFormat.TabIndex = 7
        Me.cbxOutputFormat.Tag = ""
        Me.cbxOutputFormat.ValueMember = Nothing
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(10, 60)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(71, 15)
        Me.CareLabel2.TabIndex = 2
        Me.CareLabel2.Text = "Report Range"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(206, 60)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(11, 15)
        Me.CareLabel1.TabIndex = 4
        Me.CareLabel1.Text = "to"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtTo
        '
        Me.cdtTo.EditValue = Nothing
        Me.cdtTo.EnterMoveNextControl = True
        Me.cdtTo.Location = New System.Drawing.Point(223, 57)
        Me.cdtTo.Name = "cdtTo"
        Me.cdtTo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtTo.Properties.Appearance.Options.UseFont = True
        Me.cdtTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtTo.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtTo.Size = New System.Drawing.Size(100, 22)
        Me.cdtTo.TabIndex = 5
        Me.cdtTo.Value = Nothing
        '
        'cdtFrom
        '
        Me.cdtFrom.EditValue = Nothing
        Me.cdtFrom.EnterMoveNextControl = True
        Me.cdtFrom.Location = New System.Drawing.Point(100, 57)
        Me.cdtFrom.Name = "cdtFrom"
        Me.cdtFrom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtFrom.Properties.Appearance.Options.UseFont = True
        Me.cdtFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtFrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtFrom.Size = New System.Drawing.Size(100, 22)
        Me.cdtFrom.TabIndex = 3
        Me.cdtFrom.Value = Nothing
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnPrint.Location = New System.Drawing.Point(542, 180)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(75, 23)
        Me.btnPrint.TabIndex = 2
        Me.btnPrint.Text = "Print"
        '
        'btnRefresh
        '
        Me.btnRefresh.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnRefresh.Location = New System.Drawing.Point(12, 180)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(171, 23)
        Me.btnRefresh.TabIndex = 1
        Me.btnRefresh.Text = "Build Occupancy Data"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnClose.DialogResult = DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(623, 180)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "Close"
        '
        'CareFrame1
        '
        Me.CareFrame1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.CareFrame1.Controls.Add(Me.grdTasks)
        Me.CareFrame1.Location = New System.Drawing.Point(351, 10)
        Me.CareFrame1.Name = "CareFrame1"
        Me.CareFrame1.Size = New System.Drawing.Size(347, 163)
        Me.CareFrame1.TabIndex = 8
        Me.CareFrame1.Text = "Scheduled Tasks"
        '
        'grdTasks
        '
        Me.grdTasks.AllowBuildColumns = True
        Me.grdTasks.AllowEdit = False
        Me.grdTasks.AllowHorizontalScroll = False
        Me.grdTasks.AllowMultiSelect = False
        Me.grdTasks.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.grdTasks.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdTasks.Appearance.Options.UseFont = True
        Me.grdTasks.AutoSizeByData = True
        Me.grdTasks.DisableAutoSize = False
        Me.grdTasks.DisableDataFormatting = False
        Me.grdTasks.FocusedRowHandle = -2147483648
        Me.grdTasks.HideFirstColumn = False
        Me.grdTasks.Location = New System.Drawing.Point(5, 23)
        Me.grdTasks.Name = "grdTasks"
        Me.grdTasks.PreviewColumn = ""
        Me.grdTasks.QueryID = Nothing
        Me.grdTasks.RowAutoHeight = False
        Me.grdTasks.SearchAsYouType = True
        Me.grdTasks.ShowAutoFilterRow = False
        Me.grdTasks.ShowFindPanel = False
        Me.grdTasks.ShowGroupByBox = False
        Me.grdTasks.ShowLoadingPanel = False
        Me.grdTasks.ShowNavigator = False
        Me.grdTasks.Size = New System.Drawing.Size(337, 135)
        Me.grdTasks.TabIndex = 7
        '
        'frmFCOccupancy
        '
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(710, 210)
        Me.Controls.Add(Me.CareFrame1)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.btnRefresh)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFCOccupancy"
        Me.Text = ""
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.chkExclHolidays.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkExclWL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxOutputFormat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CareFrame1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CareFrame1.ResumeLayout(False)
        Me.CareFrame1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnRefresh As Care.Controls.CareButton
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents cdtTo As Care.Controls.CareDateTime
    Friend WithEvents cdtFrom As Care.Controls.CareDateTime
    Friend WithEvents btnPrint As Care.Controls.CareButton
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents cbxOutputFormat As Care.Controls.CareComboBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents chkExclHolidays As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents chkExclWL As Care.Controls.CareCheckBox
    Friend WithEvents GroupControl1 As Care.Controls.CareFrame
    Friend WithEvents CareFrame1 As Care.Controls.CareFrame
    Friend WithEvents grdTasks As Care.Controls.CareGrid
End Class
