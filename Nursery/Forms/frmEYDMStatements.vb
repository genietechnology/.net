﻿Imports Care.Global
Imports Care.Data

Public Class frmEYDMStatements

    Private Sub frmEYDMStatements_Load(sender As Object, e As EventArgs) Handles Me.Load
        DisplayGrid()
        cgStatements.ButtonsEnabled = True
    End Sub

    Private Sub cgStatements_AddClick(sender As Object, e As EventArgs) Handles cgStatements.AddClick
        ShowItemForm(Nothing)
    End Sub

    Private Sub cgStatements_EditClick(sender As Object, e As EventArgs) Handles cgStatements.EditClick
        ShowItemForm(New Guid(cgStatements.CurrentRow("ID").ToString))
    End Sub

    Private Sub cgStatements_RemoveClick(sender As Object, e As EventArgs) Handles cgStatements.RemoveClick
        If CareMessage("Are you sure you want to delete this statement?", MessageBoxIcon.Warning, MessageBoxButtons.YesNo, "Confirm delete") = DialogResult.Yes Then
            Dim _SQL As String = "delete from EYStatements where ID = '" + cgStatements.CurrentRow("ID").ToString + "'"
            DAL.ExecuteSQL(Session.ConnectionString, _SQL)
            cgStatements.RePopulate()
        End If
    End Sub

    Private Sub ShowItemForm(ByVal RecordID As Guid?)

        Dim _frm As New frmEYDMItem(RecordID)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            cgStatements.RePopulate()
        End If

    End Sub

    Private Sub DisplayGrid()

        Dim _SQL As String = ""

        _SQL += "select s.ID, a.name as 'Area', a.aspect as 'Aspect', ab.name as 'Age Band', s.statement as 'Statement'"
        _SQL += " from EYStatements s"
        _SQL += " left join EYAreas a on a.ID = s.area_id"
        _SQL += " left join EYAgeBands ab on ab.ID = s.age_id"
        _SQL += " order by a.display_seq, ab.months_from, s.display_seq"

        cgStatements.HideFirstColumn = True
        cgStatements.Populate(Session.ConnectionString, _SQL)

    End Sub

End Class