﻿Imports Care.Global

Public Class frmChildHoliday

    Private m_ChildID As Guid
    Private m_HolidayID As Guid?
    Private m_IsNew As Boolean = True

    Public Sub New(ByVal ChildID As Guid, ByVal HolidayID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        m_ChildID = ChildID

        If HolidayID.HasValue Then
            m_IsNew = False
            m_HolidayID = HolidayID
        Else
            m_IsNew = True
            m_HolidayID = Nothing
        End If

    End Sub

    Private Sub frmChildHoliday_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cdtStartDate.BackColor = Session.ChangeColour
        chkFromHalf.BackColor = Session.ChangeColour

        cdtEndDate.BackColor = Session.ChangeColour
        chkToHalf.BackColor = Session.ChangeColour

        txtDiscount.BackColor = Session.ChangeColour
        txtNotes.BackColor = Session.ChangeColour

        If m_HolidayID.HasValue Then
            Me.Text = "Edit Session Holiday"
            DisplayRecord()
        Else
            Me.Text = "Create New Session Holiday"
            txtDiscount.Text = "0"
        End If

    End Sub

    Private Sub DisplayRecord()

        Dim _H As Business.ChildHoliday = Business.ChildHoliday.RetreiveByID(m_HolidayID.Value)
        With _H
            cdtStartDate.Value = ._FromDate
            chkFromHalf.Checked = ._FromHalf
            cdtEndDate.Value = ._ToDate
            chkToHalf.Checked = ._ToHalf
            txtDiscount.Text = ValueHandler.MoneyAsText(_H._Discount)
            txtNotes.Text = ._Notes
        End With

        _H = Nothing

    End Sub

    Private Function ValidateCharge() As Boolean

        If cdtStartDate.Text = "" Then Return False
        If cdtStartDate.Value Is Nothing Then Return False
        If Not cdtStartDate.Value.HasValue Then Return False

        If cdtEndDate.Text = "" Then Return False
        If cdtEndDate.Value Is Nothing Then Return False
        If Not cdtEndDate.Value.HasValue Then Return False

        If txtDiscount.Text = "" Then Return False
        If Care.Data.CleanData.ReturnDecimal(txtDiscount.EditValue) > 100 Then Return False

        If cdtStartDate.Value.Value > cdtEndDate.Value.Value Then Return False

        Return True

    End Function

    Private Sub SaveAndExit()

        If Not ValidateCharge() Then Exit Sub

        Dim _H As Business.ChildHoliday = Nothing

        If m_IsNew Then
            _H = New Business.ChildHoliday
            _H._ID = Guid.NewGuid
            _H._ChildId = m_ChildID
        Else
            _H = Business.ChildHoliday.RetreiveByID(m_HolidayID.Value)
        End If

        With _H
            ._FromDate = cdtStartDate.Value
            ._FromHalf = chkFromHalf.Checked
            ._ToDate = cdtEndDate.Value
            ._ToHalf = chkToHalf.Checked
            ._Discount = ValueHandler.ConvertDecimal(txtDiscount.EditValue)
            ._Notes = txtNotes.Text
            .Store()
        End With

        _H = Nothing

        Me.DialogResult = DialogResult.OK
        Me.Close()

    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        SaveAndExit()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

End Class
