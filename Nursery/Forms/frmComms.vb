﻿Imports Care.Global
Imports Care.Shared
Imports Care.Data
Imports System.IO
Imports DevExpress.XtraBars

Public Class frmComms

    Private Enum EnumControl
        Subject
        Body
    End Enum

    Private m_Loading As Boolean = True
    Private m_Control As EnumControl = EnumControl.Subject
    Private m_ControlPos As Integer = 0

    Private m_SenderName As String = ""
    Private m_SenderEmail As String = ""
    Private m_Receipients As New List(Of Recipient)
    Private m_SelectAll As Boolean = False

    Private Sub frmComms_Load(sender As Object, e As EventArgs) Handles Me.Load

        cbxType.AddItem("Email")
        If ParameterHandler.ReturnBoolean("SMS") Then cbxType.AddItem("SMS")

        cbxType.SelectedIndex = 0

        cbxDates.AddItem("Today")
        cbxDates.AddItem("Tomorrow")
        cbxDates.AddItem("Yesterday")
        cbxDates.AddItem("This week")
        cbxDates.AddItem("Next week")
        cbxDates.AddItem("Last week")
        cbxDates.AddItem("This month")
        cbxDates.AddItem("Custom range")
        cbxDates.SelectedIndex = 0

        cbxContact.AddItem("Primary Contacts Only")
        cbxContact.AddItem("Parents Only")
        cbxContact.AddItem("All Contacts")
        cbxContact.AddItem("Staff Only")
        cbxContact.AddItem("Leads Only")
        cbxContact.AddItem("Leavers Only")
        cbxContact.SelectedIndex = 0

        cbxDuplicates.AddItem("Yes")
        cbxDuplicates.AddItem("No")
        cbxDuplicates.SelectedIndex = 1

        cbxMarketing.AddItem("Yes")
        cbxMarketing.AddItem("No")
        cbxMarketing.SelectedIndex = 1

        cbxPersonalised.AddItem("Yes")
        cbxPersonalised.AddItem("No")
        cbxPersonalised.SelectedIndex = 1

        cbxSite.PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)
        cbxSite.AddItem("All Sites")
        cbxSite.SelectedIndex = cbxSite.ItemCount - 1

        With cbxSchool
            .AllowBlank = True
            .PopulateWithSQL(Session.ConnectionString, "select id, name from Schools order by name")
        End With

        ControlHandler.PopulateCheckedListBox(cbxTimeSlots, Session.ConnectionString, "select id, name from Timeslots order by start_time")
        ControlHandler.PopulateCheckedListBox(cbxBoltOns, Session.ConnectionString, "select id, name from TariffBoltOns order by name")
        ControlHandler.PopulateCheckedListBox(cbxTags, GetAllTags)
        PopulateTariffs()

        lblSMS.Visible = False

        m_Loading = False
        PopulateRecipients()
        LoadAutoSignature()

    End Sub

    Private Sub PopulateTariffs()
        If cbxSite.Text = "All Sites" Then
            ControlHandler.PopulateCheckedListBox(cbxTariff, Session.ConnectionString, "select distinct name from Tariffs where archived <> 1 and len(isnull(name,'')) > 0 order by name")
        Else
            ControlHandler.PopulateCheckedListBox(cbxTariff, Session.ConnectionString, $"select name from Tariffs where site_name = '{cbxSite.Text}' and archived <> 1 and len(isnull(name,'')) > 0 order by name")
        End If
    End Sub

    Private Sub LoadAutoSignature()
        Dim _u As Care.Shared.Business.User = Care.Shared.Business.User.RetreiveByID(Session.CurrentUser.ID)
        txtBody.HTMLText = _u._Signature
        _u = Nothing
    End Sub

    Private Sub PopulateRecipients()

        If m_Loading Then Exit Sub

        Cursor.Current = Cursors.WaitCursor

        cbxBookingRange.Text = "Booking Date Range"
        PopulateMergeFields()

        Select Case cbxContact.Text

            Case "Staff Only"
                PopulateStaff()

            Case "Leads Only"
                PopulateLeads()

            Case "Leavers Only"
                PopulateLeavers()

            Case Else
                PopulateContactsOfChildren()

        End Select

        Cursor.Current = Cursors.Default

    End Sub

    Private Sub SetDatesFromPresets(ByRef SQLFrom As String, ByRef SQLTo As String, ByVal PresetText As String)

        Select Case PresetText

            Case "Today"
                SQLFrom = ValueHandler.SQLDate(Today, True)
                SQLTo = ValueHandler.SQLDate(Today, True)
            Case "Tomorrow"
                SQLFrom = ValueHandler.SQLDate(Today.AddDays(1), True)
                SQLTo = ValueHandler.SQLDate(Today.AddDays(1), True)
            Case "Yesterday"
                SQLFrom = ValueHandler.SQLDate(Today.AddDays(-1), True)
                SQLTo = ValueHandler.SQLDate(Today.AddDays(-1), True)
            Case "This week"
                SQLFrom = ValueHandler.SQLDate(ValueHandler.NearestDate(Today, DayOfWeek.Monday, ValueHandler.EnumDirection.Backwards), True)
                SQLTo = ValueHandler.SQLDate(ValueHandler.NearestDate(Today, DayOfWeek.Sunday, ValueHandler.EnumDirection.Forwards), True)
            Case "Next week"
                SQLFrom = ValueHandler.SQLDate(ValueHandler.NearestDate(Today.AddDays(7), DayOfWeek.Monday, ValueHandler.EnumDirection.Backwards), True)
                SQLTo = ValueHandler.SQLDate(ValueHandler.NearestDate(Today.AddDays(7), DayOfWeek.Sunday, ValueHandler.EnumDirection.Forwards), True)
            Case "Last week"
                SQLFrom = ValueHandler.SQLDate(ValueHandler.NearestDate(Today.AddDays(-7), DayOfWeek.Monday, ValueHandler.EnumDirection.Backwards), True)
                SQLTo = ValueHandler.SQLDate(ValueHandler.NearestDate(Today.AddDays(-7), DayOfWeek.Sunday, ValueHandler.EnumDirection.Forwards), True)
            Case "This month"
                SQLFrom = ValueHandler.SQLDate(DateSerial(Today.Year, Today.Month, 1), True)
                SQLTo = ValueHandler.SQLDate(DateSerial(Today.Year, Today.Month, Date.DaysInMonth(Today.Year, Today.Month)), True)

        End Select

    End Sub

    Private Sub PopulateContactsOfChildren()

        Dim _SQLFrom As String = ""
        Dim _SQLTo As String = ""

        If cbxDates.Text = "Custom range" Then
            If cdtFrom.Text = "" Then Exit Sub
            If cdtTo.Text = "" Then Exit Sub
            _SQLFrom = ValueHandler.SQLDate(cdtFrom.Value.Value, True)
            _SQLTo = ValueHandler.SQLDate(cdtTo.Value.Value, True)
        Else
            SetDatesFromPresets(_SQLFrom, _SQLTo, cbxDates.Text)
        End If

        Dim _SQL As String = ""
        _SQL += "select distinct child_id from Bookings b"
        _SQL += " left join Children c on c.ID = b.child_id"
        _SQL += " left join Schools s on s.id = c.school"
        _SQL += " where booking_date between " + _SQLFrom + " and " + _SQLTo
        _SQL += " and b.site_id not in ("
        _SQL += "select site_id from SiteExcludedUsers where user_id = '" + Session.CurrentUser.ID.ToString + "'"
        _SQL += ")"
        _SQL += vbCrLf

        If cbxSite.Text <> "" AndAlso cbxSite.Text <> "All Sites" Then
            _SQL += " and b.site_id = '" & cbxSite.SelectedValue.ToString & "'"
            _SQL += vbCrLf
        End If

        If cbxRooms.Text <> "" Then
            _SQL += " and c.group_name = '" & cbxRooms.Text & "'"
            _SQL += vbCrLf
        End If

        If cbxSchool.Text <> "" Then
            _SQL += " and s.name = '" & cbxSchool.Text & "'"
            _SQL += vbCrLf
        End If

        If txtRef1.Text <> "" Then
            _SQL += " and c.school_ref_1 = '" & txtRef1.Text & "'"
            _SQL += vbCrLf
        End If

        If txtRef2.Text <> "" Then
            _SQL += " and c.school_ref_2 = '" & txtRef2.Text & "'"
            _SQL += vbCrLf
        End If

        Dim _SelectedTags As String = ReturnSelectedItems(cbxTags)
        If _SelectedTags <> "" Then
            _SQL += " and c.tags in (" + _SelectedTags + ")"
        End If

        _SQL += FilterTariffs()
        _SQL += FilterTimeSlots()
        _SQL += FilterBoltOns()

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            Dim _IDs As String = ""
            For Each _DR As DataRow In _DT.Rows
                If _IDs = "" Then
                    _IDs += "'" + _DR.Item(0).ToString() + "'"
                Else
                    _IDs += ", '" + _DR.Item(0).ToString() + "'"
                End If
            Next

            If _IDs <> "" Then
                BuildRecipients(_IDs)
            Else
                grdRecipients.Clear()
            End If

            _DT.Dispose()
            _DT = Nothing

        End If

    End Sub

    Private Sub PopulateStaff()

        Dim _SQL As String = ""
        Dim _TargetField As String = "email"
        If cbxType.Text = "SMS" Then _TargetField = "tel_mobile"

        _SQL += $"select ID, forename, fullname, site_name, group_name, {_TargetField} as 'Target'"
        _SQL += " from Staff"
        _SQL += " where status = 'C'"
        _SQL += " and len(" + _TargetField + ") > 0"
        _SQL += " and site_id not in (select site_id from SiteExcludedUsers where user_id = '" + Session.CurrentUser.ID.ToString + "')"
        _SQL += vbCrLf

        If cbxSite.Text <> "" AndAlso cbxSite.Text <> "All Sites" Then
            _SQL += " and site_id = '" & cbxSite.SelectedValue.ToString & "'"
            _SQL += vbCrLf
        End If

        If cbxRooms.Text <> "" Then
            _SQL += " and group_name = '" & cbxRooms.Text & "'"
            _SQL += vbCrLf
        End If

        _SQL += " order by site_name, forename, surname"

        m_Receipients.Clear()

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                If Not InList(_DR.Item("Target").ToString) Then

                    Dim _r As New Recipient
                    _r.Site = _DR.Item("site_name").ToString.Trim
                    _r.Room = _DR.Item("group_name").ToString.Trim
                    _r.Child = ""
                    _r.PersonName = _DR.Item("fullname").ToString.Trim
                    _r.Target = _DR.Item("Target").ToString.Trim.ToLower
                    _r.ChildID = ""
                    _r.FamilyID = ""
                    _r.ChildForename = ""
                    _r.ChildKnownAs = ""
                    _r.ChildGender = ""
                    _r.PersonID = _DR.Item("ID").ToString
                    _r.PersonForename = _DR.Item("forename").ToString.Trim
                    _r.Status = ""
                    _r.Send = m_SelectAll

                    m_Receipients.Add(_r)

                End If

            Next

            grdRecipients.Populate(m_Receipients)
            FormatGrid()

        End If

    End Sub

    Private Sub PopulateLeads()

        Dim _SQL As String = ""
        Dim _TargetField As String = "l.contact_email"
        If cbxType.Text = "SMS" Then _TargetField = "l.contact_mobile"

        _SQL += $"select l.site_name, r.room_name, l.contact_fullname, l.child_fullname, {_TargetField} as 'Target', l.ID, l.contact_forename, l.child_forename, l.child_gender"
        _SQL += " from Leads l"
        _SQL += " left join SiteRoomRatios rr on rr.ID = l.child_room"
        _SQL += " left join SiteRooms r on r.ID = rr.room_id"
        _SQL += " where l.closed = 0"
        _SQL += " and len(" + _TargetField + ") > 0"
        _SQL += " and l.site_id not in (select site_id from SiteExcludedUsers where user_id = '" + Session.CurrentUser.ID.ToString + "')"
        _SQL += vbCrLf

        If cbxSite.Text <> "" AndAlso cbxSite.Text <> "All Sites" Then
            _SQL += " and l.site_id = '" & cbxSite.SelectedValue.ToString & "'"
            _SQL += vbCrLf
        End If

        If cbxRooms.Text <> "" Then
            _SQL += " and r.room_name = '" & cbxRooms.Text & "'"
            _SQL += vbCrLf
        End If

        If cbxMarketing.Text = "Yes" Then
            _SQL += " and l.comm_marketing = 1"
            _SQL += vbCrLf
        End If

        _SQL += " order by site_name, contact_forename, contact_surname"

        m_Receipients.Clear()

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                If Not InList(_DR.Item("Target").ToString) Then

                    Dim _r As New Recipient
                    _r.Site = _DR.Item("site_name").ToString.Trim
                    _r.Room = _DR.Item("room_name").ToString.Trim
                    _r.Child = _DR.Item("child_fullname").ToString.Trim
                    _r.PersonName = _DR.Item("contact_fullname").ToString.Trim
                    _r.Target = _DR.Item("Target").ToString.Trim.ToLower
                    _r.ChildID = ""
                    _r.FamilyID = ""
                    _r.ChildForename = _DR.Item("child_forename").ToString.Trim
                    _r.ChildKnownAs = ""
                    _r.ChildGender = _DR.Item("child_gender").ToString
                    _r.PersonID = _DR.Item("ID").ToString
                    _r.PersonForename = _DR.Item("contact_forename").ToString.Trim
                    _r.Status = ""
                    _r.Send = m_SelectAll

                    m_Receipients.Add(_r)

                End If

            Next

            grdRecipients.Populate(m_Receipients)
            FormatGrid()

        End If

    End Sub

    Private Sub PopulateLeavers()

        cbxBookingRange.Text = "Leaving Date Range"

        Dim _SQLFrom As String = ""
        Dim _SQLTo As String = ""

        If cbxDates.Text = "Custom range" Then
            If cdtFrom.Text = "" Then Exit Sub
            If cdtTo.Text = "" Then Exit Sub
            _SQLFrom = ValueHandler.SQLDate(cdtFrom.Value.Value, True)
            _SQLTo = ValueHandler.SQLDate(cdtTo.Value.Value, True)
        Else
            SetDatesFromPresets(_SQLFrom, _SQLTo, cbxDates.Text)
        End If

        Dim _SQL As String = ""
        Dim _Target As String = ""

        _SQL = "select k.site_name as 'Site', k.group_name as 'Room', k.fullname as 'Child', c.fullname as 'Name',"

        If cbxType.Text = "Email" Then
            _Target = "c.email"
            _SQL += _Target & " as 'Target',"
        Else
            _Target = "c.tel_mobile"
            _SQL += _Target & " as 'Target',"
        End If

        _SQL += " k.ID as 'ChildID', k.family_id as 'FamilyID', k.forename as 'ChildForename', k.knownas as 'ChildKnownAs', k.gender as 'ChildGender',"
        _SQL += " c.ID as 'ContactID', c.forename as 'ContactForename'"
        _SQL += " from Children k"
        _SQL += " left join Contacts c on c.Family_ID = k.family_id "
        _SQL += " where k.status = 'Left'"
        _SQL += $" and len({_Target}) > 0"
        _SQL += $" and date_left between {_SQLFrom} and {_SQLTo}"

        If cbxSite.Text <> "" AndAlso cbxSite.Text <> "All Sites" Then
            _SQL += " And site_id = '" & cbxSite.SelectedValue.ToString & "'"
        End If

        If cbxMarketing.Text = "Yes" Then
            _SQL += " and c.comm_marketing = 1"
        End If

        _SQL += " order by k.site_name, k.forename, k.surname"

        m_Receipients.Clear()

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                If Not InList(_DR.Item("Target").ToString) Then

                    Dim _r As New Recipient
                    _r.Site = _DR.Item("Site").ToString.Trim
                    _r.Room = _DR.Item("Room").ToString.Trim
                    _r.Child = _DR.Item("Child").ToString.Trim
                    _r.PersonName = _DR.Item("Name").ToString.Trim
                    _r.Target = _DR.Item("Target").ToString.Trim.ToLower
                    _r.ChildID = _DR.Item("ChildID").ToString
                    _r.FamilyID = _DR.Item("FamilyID").ToString
                    _r.ChildForename = _DR.Item("ChildForename").ToString.Trim
                    _r.ChildKnownAs = _DR.Item("ChildKnownas").ToString.Trim
                    _r.ChildGender = _DR.Item("ChildGender").ToString
                    _r.PersonID = _DR.Item("ContactID").ToString
                    _r.PersonForename = _DR.Item("ContactForename").ToString.Trim
                    _r.Status = ""
                    _r.Send = m_SelectAll

                    m_Receipients.Add(_r)

                End If

            Next

            grdRecipients.Populate(m_Receipients)
            FormatGrid()

        End If

    End Sub

    Private Sub BuildRecipients(ByVal ChildIDClause As String)

        Dim _SQL As String = ""
        Dim _TargetField As String = "c.email"
        If cbxType.Text = "SMS" Then _TargetField = "c.tel_mobile"

        _SQL += "select k.site_name as 'Site', k.group_name as 'Room', k.fullname as 'Child', c.fullname as 'Name',"
        _SQL += _TargetField + " as 'Target',"
        _SQL += " k.ID as 'ChildID', k.family_id as 'FamilyID', k.forename as 'ChildForename', k.knownas as 'ChildKnownAs', k.gender as 'ChildGender',"
        _SQL += " c.ID as 'ContactID', c.forename as 'ContactForename'"
        _SQL += " from Children k"
        _SQL += " left join Contacts c on c.Family_ID = k.family_id "
        _SQL += " where k.ID in (" + ChildIDClause + ")"
        _SQL += " and len(" + _TargetField + ") > 0"

        If cbxContact.Text = "Primary Contacts Only" Then
            _SQL += " and c.primary_cont = 1"
        Else
            If cbxContact.Text = "Parents Only" Then
                _SQL += " and c.parent = 1"
            End If
        End If

        If cbxMarketing.Text = "Yes" Then
            _SQL += " and c.comm_marketing = 1"
        End If

        _SQL += " order by k.site_name, k.forename, k.surname"

        m_Receipients.Clear()

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows

                If Not InList(_DR.Item("Target").ToString) Then

                    Dim _r As New Recipient
                    _r.Site = _DR.Item("Site").ToString.Trim
                    _r.Room = _DR.Item("Room").ToString.Trim
                    _r.Child = _DR.Item("Child").ToString.Trim
                    _r.PersonName = _DR.Item("Name").ToString.Trim
                    _r.Target = _DR.Item("Target").ToString.Trim.ToLower
                    _r.ChildID = _DR.Item("ChildID").ToString
                    _r.FamilyID = _DR.Item("FamilyID").ToString
                    _r.ChildForename = _DR.Item("ChildForename").ToString.Trim
                    _r.ChildKnownAs = _DR.Item("ChildKnownas").ToString.Trim
                    _r.ChildGender = _DR.Item("ChildGender").ToString
                    _r.PersonID = _DR.Item("ContactID").ToString
                    _r.PersonForename = _DR.Item("ContactForename").ToString.Trim
                    _r.Status = ""
                    _r.Send = m_SelectAll

                    m_Receipients.Add(_r)

                End If

            Next

            grdRecipients.Populate(m_Receipients)
            FormatGrid()

        End If

    End Sub

    Private Sub FormatGrid()

        Select Case cbxContact.Text

            Case "Staff Only"
                grdRecipients.Columns("Child").Visible = False
                grdRecipients.Columns("PersonName").Caption = "Staff"

            Case Else
                grdRecipients.Columns("Child").Visible = True
                grdRecipients.Columns("PersonName").Caption = "Contact"

        End Select

        If cbxType.Text = "Email" Then
            grdRecipients.Columns("Target").Caption = "Email"
        Else
            grdRecipients.Columns("Target").Caption = "Mobile"
        End If

        If cbxSite.Text <> "All Sites" Then
            grdRecipients.Columns("Site").Visible = False
        End If

        If cbxRooms.Text <> "" Then
            grdRecipients.Columns("Room").Visible = False
        End If

        grdRecipients.Columns("ChildID").Visible = False
        grdRecipients.Columns("FamilyID").Visible = False
        grdRecipients.Columns("ChildForename").Visible = False
        grdRecipients.Columns("ChildKnownAs").Visible = False
        grdRecipients.Columns("ChildGender").Visible = False

        grdRecipients.Columns("PersonID").Visible = False
        grdRecipients.Columns("PersonForename").Visible = False
        grdRecipients.Columns("Status").Visible = False

        For Each _c As DevExpress.XtraGrid.Columns.GridColumn In grdRecipients.Columns
            If _c.FieldName <> "Send" Then
                _c.OptionsColumn.AllowEdit = False
            End If
        Next

    End Sub

    Private Function InList(ByVal Target As String) As Boolean
        If cbxDuplicates.Text = "Yes" Then
            Return False
        Else
            For Each _r In m_Receipients
                If _r.Target.Trim.ToLower = Target.Trim.ToLower Then
                    Return True
                End If
            Next
            Return False
        End If
    End Function

    Private Function FilterTariffs() As String
        If cbxTariff.Text <> "" Then
            Dim _Clauses As String = ""
            Dim _Split As String() = cbxTariff.Text.Split(CChar(","))
            For Each _Value In _Split
                If _Clauses = "" Then
                    _Clauses += "b.tariff_name like '%" + _Value.Trim + "%'"
                Else
                    _Clauses += " OR b.tariff_name like '%" + _Value.Trim + "%'"
                End If
            Next
            Dim _Return As String = " AND (" + _Clauses + ")"
            Return _Return
        Else
            Return ""
        End If
    End Function

    Private Function FilterTimeSlots() As String
        If cbxTimeSlots.Text <> "" Then
            Dim _Clauses As String = ""
            Dim _Split As String() = cbxTimeSlots.Text.Split(CChar(","))
            For Each _Value In _Split
                If _Clauses = "" Then
                    _Clauses += "b.timeslots like '%" + _Value.Trim + "%'"
                Else
                    _Clauses += " OR b.timeslots like '%" + _Value.Trim + "%'"
                End If
            Next
            Dim _Return As String = " AND (" + _Clauses + ")"
            Return _Return
        Else
            Return ""
        End If
    End Function

    Private Function FilterBoltOns() As String
        If cbxBoltOns.Text <> "" Then
            Dim _Clauses As String = ""
            Dim _Split As String() = cbxBoltOns.Text.Split(CChar(","))
            For Each _Value In _Split
                If _Clauses = "" Then
                    _Clauses += "b.bolt_ons like '%" + _Value.Trim + "%'"
                Else
                    _Clauses += " OR b.bolt_ons like '%" + _Value.Trim + "%'"
                End If
            Next
            Dim _Return As String = " AND (" + _Clauses + ")"
            Return _Return
        Else
            Return ""
        End If
    End Function

    Private Sub InsertText(ByVal ItemText As String)

        Dim _FieldName As String = ItemText.Replace(" ", "")
        _FieldName = "{" + _FieldName + "}"

        If m_Control = EnumControl.Body Then
            txtBody.InsertTextAtCaret(_FieldName)
        Else
            'txtSubject.InsertTextAtCaret(_FieldName)
        End If

    End Sub

    Private Sub txtSubject_LostFocus(sender As Object, e As EventArgs) Handles txtSubject.LostFocus
        m_Control = EnumControl.Subject
    End Sub

    Private Sub txtBody_LostFocus(sender As Object, e As EventArgs) Handles txtBody.ControlLostFocus
        m_Control = EnumControl.Body
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        PopulateRecipients()
    End Sub

    Private Sub btnAttach_Click(sender As Object, e As EventArgs) Handles btnAttach.Click

        Dim _OFD As New OpenFileDialog
        _OFD.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.Desktop
        _OFD.Multiselect = True

        If _OFD.ShowDialog = DialogResult.OK Then

            Dim _Files As String = ""
            tokAttachments.Properties.Tokens.Clear()

            For Each _File In _OFD.FileNames

                Dim _fi As New IO.FileInfo(_File)

                If _Files = "" Then
                    _Files += _fi.FullName
                Else
                    _Files += ", " + _fi.FullName
                End If

                tokAttachments.Properties.Tokens.Add(New DevExpress.XtraEditors.TokenEditToken(_fi.Name, _fi.FullName))

            Next

            tokAttachments.EditValue = _Files

        End If

    End Sub

    Private Sub tokAttachments_CustomDrawTokenGlyph(sender As Object, e As DevExpress.XtraEditors.TokenEditCustomDrawTokenGlyphEventArgs) Handles tokAttachments.CustomDrawTokenGlyph
        e.Graphics.DrawImage(My.Resources.Paperclip, e.GlyphBounds)
        e.Handled = True
    End Sub

    Private Sub PopulateSenders()

        If cbxSite.Text = "" Then
            cbxSender.Clear()
            cbxSender.SelectedIndex = -1
            m_SenderName = ""
            m_SenderEmail = ""
        Else

            cbxSender.Clear()

            If cbxSite.Text = "All Sites" Then
                For Each site As Business.Site In Business.Site.RetreiveAll
                    If site IsNot Nothing Then

                        If site._eGenAddress <> "" AndAlso site._eGenFrom <> "" Then
                            cbxSender.AddItem("General - " + site._eGenFrom, site._eGenAddress)
                        End If

                        If site._eReportAddress <> "" AndAlso site._eReportAddress <> "" Then
                            cbxSender.AddItem("Reports - " + site._eReportFrom, site._eReportAddress)
                        End If

                        If site._eInvAddress <> "" AndAlso site._eInvFrom <> "" Then
                            cbxSender.AddItem("Finance - " + site._eInvFrom, site._eInvAddress)
                        End If

                    End If
                Next
            Else

                Dim _S As Business.Site = Business.Site.RetreiveByID(New Guid(cbxSite.SelectedValue.ToString))
                If _S IsNot Nothing Then

                    If _S._eGenAddress <> "" AndAlso _S._eGenFrom <> "" Then
                        cbxSender.AddItem("General - " + _S._eGenFrom, _S._eGenAddress)
                    End If

                    If _S._eReportAddress <> "" AndAlso _S._eReportAddress <> "" Then
                        cbxSender.AddItem("Reports - " + _S._eReportFrom, _S._eReportAddress)
                    End If

                    If _S._eInvAddress <> "" AndAlso _S._eInvFrom <> "" Then
                        cbxSender.AddItem("Finance - " + _S._eInvFrom, _S._eInvAddress)
                    End If

                End If
            End If
        End If

    End Sub

    Private Sub PopulateRooms()

        Dim _SQL As String = ""

        _SQL = "select ID, room_name from SiteRooms"
        _SQL += " where site_id = '" + cbxSite.SelectedValue.ToString + "'"
        _SQL += " order by room_sequence"

        If Not String.IsNullOrWhiteSpace(_SQL) Then
            cbxRooms.AllowBlank = True
            cbxRooms.PopulateWithSQL(Session.ConnectionString, _SQL)
        End If

    End Sub

    Private Function IsValidateMessage() As Boolean

        If cbxPersonalised.Text = "No" Then
            If txtSubject.Text.Contains("{") AndAlso txtSubject.Text.Contains("}") Then
                CareMessage("You cannot use Merge Fields in Non-Personalised messages!", MessageBoxIcon.Exclamation, "Send")
                Return False
            End If
            If txtBody.Text.Contains("{") AndAlso txtBody.Text.Contains("}") Then
                CareMessage("You cannot use Merge Fields in Non-Personalised messages!", MessageBoxIcon.Exclamation, "Send")
                Return False
            End If
        End If

        If cbxType.Text = "Email" Then

            If cbxSender.Text = "" Then
                CareMessage("Please select a sender address", MessageBoxIcon.Exclamation, "Send Email")
                Return False
            End If

            If txtSubject.Text = "" Then
                CareMessage("Please enter a subject", MessageBoxIcon.Exclamation, "Send Email")
                Return False
            End If

            If txtBody.Text = "" Then
                CareMessage("Please enter the email body", MessageBoxIcon.Exclamation, "Send Email")
                Return False
            End If

        Else
            If txtBody.Text = "" Then
                CareMessage("Please the message text.", MessageBoxIcon.Exclamation, "Send SMS")
                Return False
            End If
        End If

        Return True

    End Function

    Private Sub btnSend_Click(sender As Object, e As EventArgs) Handles btnSend.Click

        If Not IsValidateMessage() Then Exit Sub

        Dim _ToSend As Integer = SendCount()

        If _ToSend > 0 Then

            If CareMessage("Are you sure you want to " + cbxType.Text + " the " + _ToSend.ToString + " selected record(s)?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Change Records") = DialogResult.Yes Then

                Session.WaitFormShow()

                SetCMDs(False)
                Session.CursorWaiting()

                If cbxType.Text = "Email" Then
                    SendAsEmail()
                Else
                    SendAsSMS()
                End If

                SetCMDs(True)

                Session.HideProgressBar()
                Session.CursorDefault()
                Session.WaitFormClose()

            End If

        Else
            CareMessage("Please select at least one recipient.", MessageBoxIcon.Exclamation, "Send " + cbxType.Text)
        End If

    End Sub

    Private Function CheckAttachments(ByVal Attachments As String) As Boolean
        If Attachments = "" Then Return True
        Dim _List As List(Of String) = Attachments.Split(CType("|", Char())).ToList
        For Each _a As String In _List
            If Not FileAccessible(_a) Then
                Return False
            End If
        Next
        Return True
    End Function

    Private Function FileAccessible(ByVal FilePath As String) As Boolean
        If FilePath = "" Then Return True
        If File.Exists(FilePath) Then
            Dim _fs As FileStream = Nothing
            Try
                _fs = File.Open(FilePath, FileMode.Open, FileAccess.ReadWrite, FileShare.None)
                _fs.Close()
            Catch ex As Exception
                Return False
            End Try
            Return True
        Else
            Return False
        End If
    End Function

    Private Function SendCount() As Integer
        Dim _i As Integer = 0
        For Each _r In m_Receipients
            If _r.Send Then _i += 1
        Next
        Return _i
    End Function

    Private Sub SendAsEmail()

        Dim _subject As String = txtSubject.Text
        Dim _body As String = txtBody.HTMLText
        Dim _attachments As String = ProcessAttachments()

        'check attachments are accessible
        If Not CheckAttachments(_attachments) Then
            CareMessage("Error sending email. Please check the file you have attached is not open.", MessageBoxIcon.Exclamation, "Attachment Check")
            Exit Sub
        End If

        Dim _CC As New List(Of String)
        If txtCC.Text <> "" Then
            If txtCC.Text.Contains(",") Then
                _CC = New List(Of String)(Split(txtCC.Text, ","))
            Else
                If txtCC.Text.Contains(";") Then
                    _CC = New List(Of String)(Split(txtCC.Text, ";"))
                Else
                    _CC.Add(txtCC.Text)
                End If
            End If
        End If

        Dim _BCC As New List(Of String)
        If txtBCC.Text <> "" Then
            If txtBCC.Text.Contains(",") Then
                _BCC = New List(Of String)(Split(txtBCC.Text, ","))
            Else
                If txtBCC.Text.Contains(";") Then
                    _BCC = New List(Of String)(Split(txtBCC.Text, ";"))
                Else
                    _BCC.Add(txtBCC.Text)
                End If
            End If
        End If

        If cbxPersonalised.Text = "Yes" Then
            Session.SetupProgressBar("Sending emails...", m_Receipients.Count)
        Else
            Session.SetupProgressBar("Processing recipients...", m_Receipients.Count)
        End If

        For Each _r In m_Receipients

            If _r.Send Then

                'if personalised, we send as we go
                If cbxPersonalised.Text = "Yes" Then

                    Dim _to As String = _r.Target.Trim.ToLower
                    _subject = MergePlaceHolders(txtSubject.Text, _r)
                    _body = MergePlaceHolders(txtBody.HTMLText, _r)

                    Dim _Result As Integer = 0
                    If _attachments = "" Then
                        _Result = EmailHandler.SendHTMLEmail(ReturnSender, _to, _subject, _body, _CC, _BCC)
                    Else
                        _Result = EmailHandler.SendHTMLEmailWithAttachment(ReturnSender, _to, _subject, _body, _attachments, _CC, _BCC)
                    End If

                    'create activity record
                    CreateActivityRecord(_r, _subject, _body, _Result)

                Else
                    _BCC.Add(_r.Target)
                End If

            End If

            Session.StepProgressBar()

        Next

        If cbxPersonalised.Text = "No" Then

            Session.SetProgressMessage("Sending single BCC email...")

            'when using BCC we cannot send an email without a "to" address, so we set the To to be the sender
            Dim _Result As Integer = 0

            If _attachments = "" Then
                _Result = EmailHandler.SendHTMLEmail(ReturnSender, ReturnSender.Address, _subject, _body, _CC, _BCC)
            Else
                _Result = EmailHandler.SendHTMLEmailWithAttachment(ReturnSender, ReturnSender.Address, _subject, _body, _attachments, _CC, _BCC)
            End If

            If _Result = 0 Then
                Session.SetProgressMessage("Email Sent Successfully.", 5)
            Else
                Session.SetProgressMessage("Email Failed.", 5)
            End If

            CreateActivityRecords(_Result)

        End If

    End Sub

    Private Sub CreateActivityRecord(ByVal Recipient As Recipient, ByVal Subject As String, ByVal Body As String, ByVal Result As Integer)

        Try

            If Result = 0 Then
                Subject += ": Send Successful"
            Else
                Subject += ": Send Failed"
            End If

            Dim _LinkType As CRM.EnumLinkType = CRM.EnumLinkType.Family
            Dim _LinkID As Guid? = Nothing
            Dim _ContactID As Guid? = Nothing
            Dim _ContactName As String = Recipient.PersonName

            Select Case cbxContact.Text

                Case "Staff Only"
                    _LinkType = CRM.EnumLinkType.Staff
                    _LinkID = New Guid(Recipient.PersonID)
                    _ContactID = Nothing

                Case "Leads Only"
                    _LinkType = CRM.EnumLinkType.Lead
                    _LinkID = New Guid(Recipient.PersonID)
                    _ContactID = Nothing

                Case Else
                    _LinkID = New Guid(Recipient.FamilyID)
                    _ContactID = New Guid(Recipient.PersonID)

            End Select

            If _LinkID.HasValue Then
                CRM.CreateActivityRecord(_LinkID.Value, _LinkType, CRM.EnumActivityType.Email, _ContactID, _ContactName, Subject, Body, True)
            End If

        Catch ex As Exception
            ErrorHandler.LogExceptionToDatabase(ex, False)
        End Try

    End Sub

    Private Sub SendAsSMS()
        Session.SetupProgressBar("Sending SMS...", m_Receipients.Count)
        For Each _r In m_Receipients
            If _r.Send Then
                Dim _Result As Integer = 0
                If Not SMSHandler.SendSMS(_r.Target.Trim, txtBody.Text) Then _Result = -1
                CreateActivityRecord(_r, $"SMS to {_r.Target.Trim}", txtBody.Text, _Result)
            End If
            Session.StepProgressBar()
        Next
    End Sub

    Private Sub CreateActivityRecords(ByVal Results As Integer)
        Session.SetupProgressBar("Creating activity records...", m_Receipients.Count)
        For Each _r In m_Receipients
            If _r.Send Then
                CreateActivityRecord(_r, txtSubject.Text, txtBody.HTMLText, Results)
            End If
            Session.StepProgressBar()
        Next
    End Sub

    Private Function ReturnSender() As Net.Mail.MailAddress
        If m_SenderName <> "" AndAlso m_SenderEmail <> "" Then
            Return New Net.Mail.MailAddress(m_SenderEmail, m_SenderName)
        Else
            Return Nothing
        End If
    End Function

    Private Function ProcessAttachments() As String

        Dim _Return As String = ""

        If tokAttachments.HasTokens Then
            For Each _t As DevExpress.XtraEditors.TokenEditToken In tokAttachments.Properties.Tokens
                If _Return = "" Then
                    _Return += _t.Value.ToString
                Else
                    _Return += "|" + _t.Value.ToString
                End If
            Next
        End If

        Return _Return

    End Function

    Private Sub SetCMDs(ByVal Enabled As Boolean)
        btnSend.Enabled = Enabled
        btnCancel.Enabled = Enabled
        btnSelectAll.Enabled = Enabled
    End Sub

    Private Function MergePlaceHolders(ByVal TemplateIn As String, ByRef Recipient As Recipient) As String

        Dim _Return As String = TemplateIn

        Try

            Select Case cbxContact.Text

                Case "Staff Only"
                    _Return = _Return.Replace("{StaffForename}", Recipient.PersonForename)
                    _Return = _Return.Replace("{StaffFullname}", Recipient.PersonName)

                Case "Leads Only"

                    'merge contact fields
                    _Return = _Return.Replace("{ContactForename}", Recipient.PersonForename)
                    _Return = _Return.Replace("{ContactFullname}", Recipient.PersonName)

                    'merge child fields
                    Dim _Forename As String = Recipient.ChildForename

                    _Return = _Return.Replace("{ChildFullname}", Recipient.Child)
                    _Return = _Return.Replace("{ChildForename}", Recipient.ChildForename)

                    If Recipient.ChildGender = "M" Then
                        _Return = _Return.Replace("{he/she}", "he")
                        _Return = _Return.Replace("{his/her}", "his")
                    Else
                        _Return = _Return.Replace("{he/she}", "she")
                        _Return = _Return.Replace("{his/her}", "her")
                    End If

                Case Else

                    'merge contact fields
                    _Return = _Return.Replace("{ContactForename}", Recipient.PersonForename)
                    _Return = _Return.Replace("{ContactFullname}", Recipient.PersonName)

                    'merge child fields
                    Dim _Forename As String = Recipient.Child
                    Dim _ActualKnownAs As String = Recipient.ChildKnownAs

                    Dim _KnownAs As String = _Forename
                    If _ActualKnownAs <> "" Then _KnownAs = _ActualKnownAs

                    _Return = _Return.Replace("{ChildFullname}", Recipient.Child)
                    _Return = _Return.Replace("{ChildForename}", Recipient.ChildForename)
                    _Return = _Return.Replace("{ChildKnownAs}", Recipient.ChildKnownAs)

                    If Recipient.ChildGender = "M" Then
                        _Return = _Return.Replace("{he/she}", "he")
                        _Return = _Return.Replace("{his/her}", "his")
                    Else
                        _Return = _Return.Replace("{he/she}", "she")
                        _Return = _Return.Replace("{his/her}", "her")
                    End If

            End Select

        Catch ex As Exception
            ErrorHandler.LogExceptionToDatabase(ex, False)
        End Try

        Return _Return

    End Function

    Private Sub btnSendTest_Click(sender As Object, e As EventArgs) Handles btnSendTest.Click

        CareMessage(txtBody.HTMLText)

        If cbxType.Text = "SMS" Then
            CareMessage("It is not possible to Test SMS Communications at present.")
            Exit Sub
        End If

        If Not IsValidateMessage() Then Exit Sub

        Session.WaitFormShow()

        SetCMDs(False)
        Session.CursorWaiting()

        Dim _Body As String = txtBody.HTMLText

        If cbxType.Text = "Email" Then

            Dim _Attachments As String = ProcessAttachments()

            'check attachments are accessible
            If Not CheckAttachments(_Attachments) Then
                CareMessage("Error sending email. Please check the file you have attached is not open.", MessageBoxIcon.Exclamation, "Attachment Check")
                Exit Sub
            End If

            Dim _SendOK As Integer = 0

            Dim _cc As New List(Of String)
            If txtCC.Text.Contains(",") Then
                _cc = New List(Of String)(Split(txtCC.Text, ","))

            ElseIf txtCC.Text.Contains(";") Then
                _cc = New List(Of String)(Split(txtCC.Text, ";"))

            Else
                _cc.Add(txtCC.Text)
            End If

            Dim _bcc As New List(Of String)
            If txtBCC.Text.Contains(",") Then
                _bcc = New List(Of String)(Split(txtBCC.Text, ","))

            ElseIf txtBCC.Text.Contains(";") Then
                _bcc = New List(Of String)(Split(txtBCC.Text, ";"))
            Else
                _bcc.Add(txtBCC.Text)

            End If

            If _Attachments = "" Then
                _SendOK = EmailHandler.SendHTMLEmail(ReturnSender, ReturnSender.Address, txtSubject.Text, _Body, _cc, _bcc)
            Else
                _SendOK = EmailHandler.SendHTMLEmailWithAttachment(ReturnSender, ReturnSender.Address, txtSubject.Text, _Body, _Attachments, _cc, _bcc)
            End If

        End If

        SetCMDs(True)

        Session.HideProgressBar()
        Session.CursorDefault()
        Session.WaitFormClose()

    End Sub

    Private Sub SMSCount()

        If cbxType.Text = "SMS" Then

            Dim count As Integer = txtBody.Text.Count
            If count > 160 Then

                lblSMS.Visible = True

                Dim messages As Integer = 0
                messages = CInt(Math.Floor(count / 160))

                Dim remainder As Double = count - (messages * 160)
                lblSMS.Text = String.Concat(remainder, "/160 +", messages, " message(s)")

            ElseIf count >= 150 Then

                lblSMS.Visible = True
                lblSMS.Text = String.Concat(count, "/160")

            Else
                lblSMS.Visible = False
            End If

        End If

    End Sub

    Private Sub txtBody_TextChanged(sender As Object, e As EventArgs) Handles txtBody.TextChanged
        SMSCount()
    End Sub

    Private Sub PopulateMergeFields()

        Select Case cbxContact.Text

            Case "Staff Only"
                mnuAccountNo.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
                mnuChildForename.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
                mnuChildFullname.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
                mnuChildKnownAs.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
                mnuContactForename.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
                mnuContactFullname.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
                mnuHeShe.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
                mnuHisHer.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
                mnuStaffForename.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                mnuStaffFullname.Visibility = DevExpress.XtraBars.BarItemVisibility.Always

            Case "Leads Only"
                mnuAccountNo.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
                mnuChildForename.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                mnuChildFullname.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                mnuChildKnownAs.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
                mnuContactForename.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                mnuContactFullname.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                mnuHeShe.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                mnuHisHer.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                mnuStaffForename.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
                mnuStaffFullname.Visibility = DevExpress.XtraBars.BarItemVisibility.Never

            Case Else
                mnuAccountNo.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                mnuChildForename.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                mnuChildFullname.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                mnuChildKnownAs.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                mnuContactForename.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                mnuContactFullname.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                mnuHeShe.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                mnuHisHer.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                mnuStaffForename.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
                mnuStaffFullname.Visibility = DevExpress.XtraBars.BarItemVisibility.Never

        End Select

    End Sub

    Private Function GetAllTags() As List(Of String)

        Dim _Tags As New List(Of String)

        Dim _SQL As String = "select tags from Children where status <> 'Left' and len(tags) > 0"
        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            'loop through each child
            For Each _DR As DataRow In _DT.Rows

                'fetch the tags and split into list of tags
                Dim _ChildTags As List(Of String) = _DR.Item("tags").ToString.Split(CChar(",")).ToList

                'loop through the tags and append to tag list where they are not already in the main list
                For Each _Tag In _ChildTags
                    If Not InList(_Tags, _Tag) Then
                        _Tags.Add(_Tag.Trim)
                    End If
                Next
            Next
        End If

        Return _Tags

    End Function

    Private Function InList(ByRef Tags As List(Of String), ByVal CheckTag As String) As Boolean
        For Each _Tag In Tags
            If _Tag.Trim.ToUpper = CheckTag.Trim.ToUpper Then
                Return True
            End If
        Next
        Return False
    End Function

    Private Sub cbxTags_EditValueChanged(sender As Object, e As EventArgs) Handles cbxTags.EditValueChanged
        If Not m_Loading Then PopulateRecipients()
    End Sub

    Private Sub cbxSender_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSender.SelectedIndexChanged
        If cbxSender.Text <> "" Then
            m_SenderName = cbxSender.Text.Substring(cbxSender.Text.IndexOf("-") + 2)
            m_SenderEmail = cbxSender.SelectedValue.ToString
        Else
            m_SenderName = ""
            m_SenderEmail = ""
        End If
    End Sub

    Private Sub cbxMarketing_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxMarketing.SelectedIndexChanged
        If Not m_Loading Then
            PopulateRecipients()
        End If
    End Sub

    Private Sub cbxPersonalised_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxPersonalised.SelectedIndexChanged
        If cbxPersonalised.Text = "Yes" Then
            PopulateMergeFields()
            gbxMerge.Enabled = True
            btnField.Enabled = True
        Else
            gbxMerge.Enabled = False
            btnField.Enabled = False
        End If
    End Sub

    Private Function ReturnSelectedItems(ByRef List As DevExpress.XtraEditors.CheckedComboBoxEdit) As String

        Dim _Return As String = ""
        Dim _Separator As String = ""

        For Each _Item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In List.Properties.Items
            If _Item.CheckState = CheckState.Checked Then
                If _Return <> "" Then _Separator = ","
                Dim _Description As String = _Item.Description
                If _Description = "" Then _Description = _Item.Value.ToString
                _Return += _Separator + "'" + _Description + "'"
            End If
        Next

        Return _Return

    End Function

    Private Sub btnSelectAll_Click(sender As Object, e As EventArgs) Handles btnSelectAll.Click
        If m_SelectAll Then
            m_SelectAll = False
            btnSelectAll.Text = "Select All"
        Else
            m_SelectAll = True
            btnSelectAll.Text = "De-select All"
        End If
        PopulateRecipients()
    End Sub

#Region "Filter Controls"

    Private Sub cbxPreset_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxContact.SelectedIndexChanged
        PopulateRecipients()
    End Sub

    Private Sub cbxType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxType.SelectedIndexChanged

        If cbxType.Text = "Email" Then

            gbxSubject.Enabled = True
            gbxAttach.Enabled = True
            lblSMS.Visible = False
            LoadAutoSignature()
            PopulateSenders()

            cbxPersonalised.Enabled = True

        Else

            txtSubject.Text = ""
            tokAttachments.Properties.Tokens.Clear()
            tokAttachments.EditValue = ""
            gbxSubject.Enabled = False
            gbxAttach.Enabled = False
            txtBody.Text = ""
            SMSCount()

            cbxPersonalised.Text = "No"
            cbxPersonalised.Enabled = False

        End If

        PopulateRecipients()

    End Sub

    Private Sub cbxSite_SelectedIndexChanged(sender As Object, e As EventArgs)
        PopulateRecipients()
    End Sub

    Private Sub cbxTariff_EditValueChanged(sender As Object, e As EventArgs) Handles cbxTariff.EditValueChanged
        PopulateRecipients()
    End Sub

    Private Sub cbxRooms_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxRooms.SelectedIndexChanged
        PopulateRecipients()
    End Sub

    Private Sub cbxTimeSlots_EditValueChanged(sender As Object, e As EventArgs) Handles cbxTimeSlots.EditValueChanged
        PopulateRecipients()
    End Sub

    Private Sub cbxBoltOns_EditValueChanged(sender As Object, e As EventArgs) Handles cbxBoltOns.EditValueChanged
        PopulateRecipients()
    End Sub

    Private Sub cbxSites_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSite.SelectedIndexChanged
        If cbxSite.Text = "All Sites" Then
            cbxRooms.Clear()
            cbxRooms.Text = ""
            cbxRooms.Enabled = False
        Else
            cbxRooms.Enabled = True
            PopulateRooms()
        End If
        PopulateRecipients()
        PopulateSenders()
    End Sub

    Private Sub cbxSchool_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSchool.SelectedIndexChanged
        PopulateRecipients()
    End Sub

    Private Sub txtRef1_Validated(sender As Object, e As EventArgs) Handles txtRef1.LostFocus
        PopulateRecipients()
    End Sub

    Private Sub txtRef2_Validated(sender As Object, e As EventArgs) Handles txtRef2.LostFocus
        PopulateRecipients()
    End Sub

    Private Sub cbxDuplicates_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxDuplicates.SelectedIndexChanged
        PopulateRecipients()
    End Sub

    Private Sub cbxDates_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxDates.SelectedIndexChanged
        If cbxDates.Text = "Custom range" Then
            cdtFrom.Enabled = True
            cdtTo.Enabled = True
        Else
            cdtFrom.Enabled = False
            cdtFrom.Text = ""
            cdtTo.Enabled = False
            cdtTo.Text = ""
            PopulateRecipients()
        End If
    End Sub

#End Region

#Region "Mail Merge Inserts"

    Private Sub mnuChildForename_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuChildForename.ItemClick
        InsertText(e.Item.Caption)
    End Sub

    Private Sub mnuChildFullname_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuChildFullname.ItemClick
        InsertText(e.Item.Caption)
    End Sub

    Private Sub mnuAccountNo_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuAccountNo.ItemClick
        InsertText(e.Item.Caption)
    End Sub

    Private Sub mnuChildKnownAs_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuChildKnownAs.ItemClick
        InsertText(e.Item.Caption)
    End Sub

    Private Sub mnuContactForename_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuContactForename.ItemClick
        InsertText(e.Item.Caption)
    End Sub

    Private Sub mnuContactFullname_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuContactFullname.ItemClick
        InsertText(e.Item.Caption)
    End Sub

    Private Sub mnuHeShe_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuHeShe.ItemClick
        InsertText(e.Item.Caption)
    End Sub

    Private Sub mnuHisHer_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuHisHer.ItemClick
        InsertText(e.Item.Caption)
    End Sub

    Private Sub mnuStaffForename_ItemClick(sender As Object, e As ItemClickEventArgs) Handles mnuStaffForename.ItemClick
        InsertText(e.Item.Caption)
    End Sub

    Private Sub mnuStaffFullname_ItemClick(sender As Object, e As ItemClickEventArgs) Handles mnuStaffFullname.ItemClick
        InsertText(e.Item.Caption)
    End Sub

#End Region

    Private Class Recipient

        Property Site As String
        Property Room As String
        Property Child As String
        Property PersonName As String
        Property Target As String
        Property ChildID As String
        Property FamilyID As String
        Property ChildForename As String
        Property ChildKnownAs As String
        Property ChildGender As String
        Property PersonID As String
        Property PersonForename As String
        Property Status As String
        Property Send As Boolean

    End Class

End Class

