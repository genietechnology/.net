﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCheckLists
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.tabMain = New Care.Controls.CareTab(Me.components)
        Me.tabSummary = New DevExpress.XtraTab.XtraTabPage()
        Me.panItem = New DevExpress.XtraEditors.PanelControl()
        Me.btnItemSaveAdd = New Care.Controls.CareButton(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.txtItemSequence = New Care.Controls.CareTextBox(Me.components)
        Me.btnItemOK = New Care.Controls.CareButton(Me.components)
        Me.btnItemCancel = New Care.Controls.CareButton(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.cbxItemResponse = New Care.Controls.CareComboBox(Me.components)
        Me.txtItemText = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.cgItems = New Care.Controls.CareGridWithButtons()
        Me.panMain = New DevExpress.XtraEditors.PanelControl()
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.cbxWeekday = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.cbxSite = New Care.Controls.CareComboBox(Me.components)
        Me.cbxFreq = New Care.Controls.CareComboBox(Me.components)
        Me.chkActive = New Care.Controls.CareCheckBox(Me.components)
        Me.txtName = New Care.Controls.CareTextBox(Me.components)
        Me.Label25 = New Care.Controls.CareLabel(Me.components)
        Me.Label26 = New Care.Controls.CareLabel(Me.components)
        Me.Label27 = New Care.Controls.CareLabel(Me.components)
        Me.tabResults = New DevExpress.XtraTab.XtraTabPage()
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.cgHeader = New Care.Controls.CareGrid()
        Me.cgAnswers = New Care.Controls.CareGrid()
        Me.GroupControl2 = New Care.Controls.CareFrame(Me.components)
        Me.btnPrintSelected = New Care.Controls.CareButton(Me.components)
        Me.btnBlankCheckList = New Care.Controls.CareButton(Me.components)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.tabMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabMain.SuspendLayout()
        Me.tabSummary.SuspendLayout()
        CType(Me.panItem, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panItem.SuspendLayout()
        CType(Me.txtItemSequence.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxItemResponse.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtItemText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.panMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panMain.SuspendLayout()
        CType(Me.cbxWeekday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxFreq.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkActive.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabResults.SuspendLayout()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(705, 5)
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(614, 5)
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnBlankCheckList)
        Me.Panel1.Location = New System.Drawing.Point(0, 565)
        Me.Panel1.Size = New System.Drawing.Size(802, 36)
        Me.Panel1.TabIndex = 1
        Me.Panel1.Controls.SetChildIndex(Me.btnOK, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnCancel, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnBlankCheckList, 0)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'tabMain
        '
        Me.tabMain.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.tabMain.Location = New System.Drawing.Point(9, 53)
        Me.tabMain.Name = "tabMain"
        Me.tabMain.SelectedTabPage = Me.tabSummary
        Me.tabMain.Size = New System.Drawing.Size(781, 511)
        Me.tabMain.TabIndex = 0
        Me.tabMain.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabSummary, Me.tabResults})
        '
        'tabSummary
        '
        Me.tabSummary.Controls.Add(Me.panItem)
        Me.tabSummary.Controls.Add(Me.cgItems)
        Me.tabSummary.Controls.Add(Me.panMain)
        Me.tabSummary.Name = "tabSummary"
        Me.tabSummary.Size = New System.Drawing.Size(775, 483)
        Me.tabSummary.Text = "List Definition"
        '
        'panItem
        '
        Me.panItem.Anchor = AnchorStyles.None
        Me.panItem.Controls.Add(Me.btnItemSaveAdd)
        Me.panItem.Controls.Add(Me.CareLabel6)
        Me.panItem.Controls.Add(Me.txtItemSequence)
        Me.panItem.Controls.Add(Me.btnItemOK)
        Me.panItem.Controls.Add(Me.btnItemCancel)
        Me.panItem.Controls.Add(Me.CareLabel4)
        Me.panItem.Controls.Add(Me.cbxItemResponse)
        Me.panItem.Controls.Add(Me.txtItemText)
        Me.panItem.Controls.Add(Me.CareLabel5)
        Me.panItem.Location = New System.Drawing.Point(44, 227)
        Me.panItem.Name = "panItem"
        Me.panItem.Size = New System.Drawing.Size(686, 96)
        Me.panItem.TabIndex = 2
        '
        'btnItemSaveAdd
        '
        Me.btnItemSaveAdd.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnItemSaveAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnItemSaveAdd.Appearance.Options.UseFont = True
        Me.btnItemSaveAdd.Location = New System.Drawing.Point(407, 63)
        Me.btnItemSaveAdd.Name = "btnItemSaveAdd"
        Me.btnItemSaveAdd.Size = New System.Drawing.Size(85, 25)
        Me.btnItemSaveAdd.TabIndex = 8
        Me.btnItemSaveAdd.Text = "Save && Add"
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(10, 68)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(51, 15)
        Me.CareLabel6.TabIndex = 4
        Me.CareLabel6.Text = "Sequence"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtItemSequence
        '
        Me.txtItemSequence.CharacterCasing = CharacterCasing.Normal
        Me.txtItemSequence.EnterMoveNextControl = True
        Me.txtItemSequence.Location = New System.Drawing.Point(99, 65)
        Me.txtItemSequence.MaxLength = 10
        Me.txtItemSequence.Name = "txtItemSequence"
        Me.txtItemSequence.NumericAllowNegatives = False
        Me.txtItemSequence.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtItemSequence.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtItemSequence.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtItemSequence.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtItemSequence.Properties.Appearance.Options.UseFont = True
        Me.txtItemSequence.Properties.Appearance.Options.UseTextOptions = True
        Me.txtItemSequence.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtItemSequence.Properties.Mask.EditMask = "##########;"
        Me.txtItemSequence.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtItemSequence.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtItemSequence.Properties.MaxLength = 10
        Me.txtItemSequence.Size = New System.Drawing.Size(50, 22)
        Me.txtItemSequence.TabIndex = 5
        Me.txtItemSequence.Tag = ""
        Me.txtItemSequence.TextAlign = HorizontalAlignment.Left
        Me.txtItemSequence.ToolTipText = ""
        '
        'btnItemOK
        '
        Me.btnItemOK.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnItemOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnItemOK.Appearance.Options.UseFont = True
        Me.btnItemOK.Location = New System.Drawing.Point(498, 63)
        Me.btnItemOK.Name = "btnItemOK"
        Me.btnItemOK.Size = New System.Drawing.Size(85, 25)
        Me.btnItemOK.TabIndex = 6
        Me.btnItemOK.Text = "Save && Close"
        '
        'btnItemCancel
        '
        Me.btnItemCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnItemCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnItemCancel.Appearance.Options.UseFont = True
        Me.btnItemCancel.CausesValidation = False
        Me.btnItemCancel.DialogResult = DialogResult.Cancel
        Me.btnItemCancel.Location = New System.Drawing.Point(589, 63)
        Me.btnItemCancel.Name = "btnItemCancel"
        Me.btnItemCancel.Size = New System.Drawing.Size(85, 25)
        Me.btnItemCancel.TabIndex = 7
        Me.btnItemCancel.Text = "Cancel"
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(10, 40)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(79, 15)
        Me.CareLabel4.TabIndex = 2
        Me.CareLabel4.Text = "Response Type"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxItemResponse
        '
        Me.cbxItemResponse.AllowBlank = False
        Me.cbxItemResponse.DataSource = Nothing
        Me.cbxItemResponse.DisplayMember = Nothing
        Me.cbxItemResponse.EnterMoveNextControl = True
        Me.cbxItemResponse.Location = New System.Drawing.Point(99, 37)
        Me.cbxItemResponse.Name = "cbxItemResponse"
        Me.cbxItemResponse.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxItemResponse.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxItemResponse.Properties.Appearance.Options.UseFont = True
        Me.cbxItemResponse.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxItemResponse.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxItemResponse.SelectedValue = Nothing
        Me.cbxItemResponse.Size = New System.Drawing.Size(273, 22)
        Me.cbxItemResponse.TabIndex = 3
        Me.cbxItemResponse.Tag = ""
        Me.cbxItemResponse.ValueMember = Nothing
        '
        'txtItemText
        '
        Me.txtItemText.CharacterCasing = CharacterCasing.Normal
        Me.txtItemText.EnterMoveNextControl = True
        Me.txtItemText.Location = New System.Drawing.Point(99, 9)
        Me.txtItemText.MaxLength = 100
        Me.txtItemText.Name = "txtItemText"
        Me.txtItemText.NumericAllowNegatives = False
        Me.txtItemText.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtItemText.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtItemText.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtItemText.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtItemText.Properties.Appearance.Options.UseFont = True
        Me.txtItemText.Properties.Appearance.Options.UseTextOptions = True
        Me.txtItemText.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtItemText.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtItemText.Properties.MaxLength = 100
        Me.txtItemText.Size = New System.Drawing.Size(575, 22)
        Me.txtItemText.TabIndex = 1
        Me.txtItemText.Tag = ""
        Me.txtItemText.TextAlign = HorizontalAlignment.Left
        Me.txtItemText.ToolTipText = ""
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(10, 12)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(49, 15)
        Me.CareLabel5.TabIndex = 0
        Me.CareLabel5.Text = "Item Text"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cgItems
        '
        Me.cgItems.ButtonsEnabled = False
        Me.cgItems.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgItems.HideFirstColumn = False
        Me.cgItems.Location = New System.Drawing.Point(8, 109)
        Me.cgItems.Name = "cgItems"
        Me.cgItems.PreviewColumn = ""
        Me.cgItems.Size = New System.Drawing.Size(760, 367)
        Me.cgItems.TabIndex = 1
        '
        'panMain
        '
        Me.panMain.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.panMain.Controls.Add(Me.CareLabel3)
        Me.panMain.Controls.Add(Me.cbxWeekday)
        Me.panMain.Controls.Add(Me.CareLabel2)
        Me.panMain.Controls.Add(Me.cbxSite)
        Me.panMain.Controls.Add(Me.cbxFreq)
        Me.panMain.Controls.Add(Me.chkActive)
        Me.panMain.Controls.Add(Me.txtName)
        Me.panMain.Controls.Add(Me.Label25)
        Me.panMain.Controls.Add(Me.Label26)
        Me.panMain.Controls.Add(Me.Label27)
        Me.panMain.Location = New System.Drawing.Point(8, 7)
        Me.panMain.Name = "panMain"
        Me.panMain.Size = New System.Drawing.Size(760, 95)
        Me.panMain.TabIndex = 0
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(388, 40)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(48, 15)
        Me.CareLabel3.TabIndex = 11
        Me.CareLabel3.Text = "Weekday"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxWeekday
        '
        Me.cbxWeekday.AllowBlank = False
        Me.cbxWeekday.DataSource = Nothing
        Me.cbxWeekday.DisplayMember = Nothing
        Me.cbxWeekday.EnterMoveNextControl = True
        Me.cbxWeekday.Location = New System.Drawing.Point(442, 37)
        Me.cbxWeekday.Name = "cbxWeekday"
        Me.cbxWeekday.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxWeekday.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxWeekday.Properties.Appearance.Options.UseFont = True
        Me.cbxWeekday.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxWeekday.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxWeekday.SelectedValue = Nothing
        Me.cbxWeekday.Size = New System.Drawing.Size(296, 22)
        Me.cbxWeekday.TabIndex = 10
        Me.cbxWeekday.Tag = "AE"
        Me.cbxWeekday.ValueMember = Nothing
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(10, 68)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(55, 15)
        Me.CareLabel2.TabIndex = 8
        Me.CareLabel2.Text = "Frequency"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(71, 37)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(301, 22)
        Me.cbxSite.TabIndex = 7
        Me.cbxSite.Tag = "AE"
        Me.cbxSite.ValueMember = Nothing
        '
        'cbxFreq
        '
        Me.cbxFreq.AllowBlank = False
        Me.cbxFreq.DataSource = Nothing
        Me.cbxFreq.DisplayMember = Nothing
        Me.cbxFreq.EnterMoveNextControl = True
        Me.cbxFreq.Location = New System.Drawing.Point(71, 65)
        Me.cbxFreq.Name = "cbxFreq"
        Me.cbxFreq.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxFreq.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxFreq.Properties.Appearance.Options.UseFont = True
        Me.cbxFreq.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxFreq.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxFreq.SelectedValue = Nothing
        Me.cbxFreq.Size = New System.Drawing.Size(301, 22)
        Me.cbxFreq.TabIndex = 9
        Me.cbxFreq.Tag = "AE"
        Me.cbxFreq.ValueMember = Nothing
        '
        'chkActive
        '
        Me.chkActive.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.chkActive.EnterMoveNextControl = True
        Me.chkActive.Location = New System.Drawing.Point(442, 66)
        Me.chkActive.Name = "chkActive"
        Me.chkActive.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkActive.Properties.Appearance.Options.UseFont = True
        Me.chkActive.Size = New System.Drawing.Size(20, 19)
        Me.chkActive.TabIndex = 3
        Me.chkActive.Tag = "AE"
        '
        'txtName
        '
        Me.txtName.CharacterCasing = CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(71, 9)
        Me.txtName.MaxLength = 100
        Me.txtName.Name = "txtName"
        Me.txtName.NumericAllowNegatives = False
        Me.txtName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtName.Properties.MaxLength = 100
        Me.txtName.Size = New System.Drawing.Size(667, 22)
        Me.txtName.TabIndex = 1
        Me.txtName.Tag = "AE"
        Me.txtName.TextAlign = HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'Label25
        '
        Me.Label25.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label25.Location = New System.Drawing.Point(10, 12)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(32, 15)
        Me.Label25.TabIndex = 0
        Me.Label25.Text = "Name"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label26
        '
        Me.Label26.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label26.Location = New System.Drawing.Point(10, 40)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(19, 15)
        Me.Label26.TabIndex = 6
        Me.Label26.Text = "Site"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label27
        '
        Me.Label27.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.Label27.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label27.Location = New System.Drawing.Point(388, 68)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(33, 15)
        Me.Label27.TabIndex = 2
        Me.Label27.Text = "Active"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabResults
        '
        Me.tabResults.Controls.Add(Me.SplitContainerControl1)
        Me.tabResults.Name = "tabResults"
        Me.tabResults.Size = New System.Drawing.Size(836, 483)
        Me.tabResults.Text = "Results"
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.SplitContainerControl1.Location = New System.Drawing.Point(8, 8)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.cgHeader)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.cgAnswers)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.GroupControl2)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(819, 467)
        Me.SplitContainerControl1.SplitterPosition = 242
        Me.SplitContainerControl1.TabIndex = 14
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'cgHeader
        '
        Me.cgHeader.AllowBuildColumns = True
        Me.cgHeader.AllowEdit = False
        Me.cgHeader.AllowHorizontalScroll = False
        Me.cgHeader.AllowMultiSelect = False
        Me.cgHeader.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgHeader.Appearance.Options.UseFont = True
        Me.cgHeader.AutoSizeByData = True
        Me.cgHeader.DisableAutoSize = False
        Me.cgHeader.DisableDataFormatting = False
        Me.cgHeader.Dock = DockStyle.Fill
        Me.cgHeader.FocusedRowHandle = -2147483648
        Me.cgHeader.HideFirstColumn = False
        Me.cgHeader.Location = New System.Drawing.Point(0, 0)
        Me.cgHeader.Name = "cgHeader"
        Me.cgHeader.PreviewColumn = ""
        Me.cgHeader.QueryID = Nothing
        Me.cgHeader.RowAutoHeight = False
        Me.cgHeader.SearchAsYouType = True
        Me.cgHeader.ShowAutoFilterRow = False
        Me.cgHeader.ShowFindPanel = False
        Me.cgHeader.ShowGroupByBox = False
        Me.cgHeader.ShowLoadingPanel = False
        Me.cgHeader.ShowNavigator = False
        Me.cgHeader.Size = New System.Drawing.Size(242, 467)
        Me.cgHeader.TabIndex = 14
        '
        'cgAnswers
        '
        Me.cgAnswers.AllowBuildColumns = True
        Me.cgAnswers.AllowEdit = False
        Me.cgAnswers.AllowHorizontalScroll = False
        Me.cgAnswers.AllowMultiSelect = False
        Me.cgAnswers.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgAnswers.Appearance.Options.UseFont = True
        Me.cgAnswers.AutoSizeByData = True
        Me.cgAnswers.DisableAutoSize = False
        Me.cgAnswers.DisableDataFormatting = False
        Me.cgAnswers.Dock = DockStyle.Fill
        Me.cgAnswers.FocusedRowHandle = -2147483648
        Me.cgAnswers.HideFirstColumn = False
        Me.cgAnswers.Location = New System.Drawing.Point(0, 0)
        Me.cgAnswers.Name = "cgAnswers"
        Me.cgAnswers.PreviewColumn = ""
        Me.cgAnswers.QueryID = Nothing
        Me.cgAnswers.RowAutoHeight = False
        Me.cgAnswers.SearchAsYouType = True
        Me.cgAnswers.ShowAutoFilterRow = False
        Me.cgAnswers.ShowFindPanel = False
        Me.cgAnswers.ShowGroupByBox = False
        Me.cgAnswers.ShowLoadingPanel = False
        Me.cgAnswers.ShowNavigator = False
        Me.cgAnswers.Size = New System.Drawing.Size(572, 432)
        Me.cgAnswers.TabIndex = 15
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.btnPrintSelected)
        Me.GroupControl2.Dock = DockStyle.Bottom
        Me.GroupControl2.Location = New System.Drawing.Point(0, 432)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.ShowCaption = False
        Me.GroupControl2.Size = New System.Drawing.Size(572, 35)
        Me.GroupControl2.TabIndex = 18
        Me.GroupControl2.Text = "GroupControl2"
        '
        'btnPrintSelected
        '
        Me.btnPrintSelected.Location = New System.Drawing.Point(5, 5)
        Me.btnPrintSelected.Name = "btnPrintSelected"
        Me.btnPrintSelected.Size = New System.Drawing.Size(175, 25)
        Me.btnPrintSelected.TabIndex = 8
        Me.btnPrintSelected.Text = "Print selected Checklist"
        '
        'btnBlankCheckList
        '
        Me.btnBlankCheckList.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnBlankCheckList.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnBlankCheckList.Appearance.Options.UseFont = True
        Me.btnBlankCheckList.Location = New System.Drawing.Point(12, 5)
        Me.btnBlankCheckList.Name = "btnBlankCheckList"
        Me.btnBlankCheckList.Size = New System.Drawing.Size(152, 25)
        Me.btnBlankCheckList.TabIndex = 9
        Me.btnBlankCheckList.Text = "Print Blank Checklist"
        '
        'frmCheckLists
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(802, 601)
        Me.Controls.Add(Me.tabMain)
        Me.Name = "frmCheckLists"
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.tabMain, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.tabMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabMain.ResumeLayout(False)
        Me.tabMain.PerformLayout()
        Me.tabSummary.ResumeLayout(False)
        CType(Me.panItem, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panItem.ResumeLayout(False)
        Me.panItem.PerformLayout()
        CType(Me.txtItemSequence.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxItemResponse.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtItemText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.panMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panMain.ResumeLayout(False)
        Me.panMain.PerformLayout()
        CType(Me.cbxWeekday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxFreq.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkActive.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabResults.ResumeLayout(False)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents tabMain As Care.Controls.CareTab
    Friend WithEvents tabSummary As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents panMain As DevExpress.XtraEditors.PanelControl
    Friend WithEvents tabResults As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents cgHeader As Care.Controls.CareGrid
    Friend WithEvents cgAnswers As Care.Controls.CareGrid
    Friend WithEvents btnPrintSelected As Care.Controls.CareButton
    Friend WithEvents cgItems As Care.Controls.CareGridWithButtons
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
    Friend WithEvents chkActive As Care.Controls.CareCheckBox
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents Label25 As Care.Controls.CareLabel
    Friend WithEvents Label26 As Care.Controls.CareLabel
    Friend WithEvents Label27 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents cbxFreq As Care.Controls.CareComboBox
    Friend WithEvents panItem As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnItemOK As Care.Controls.CareButton
    Friend WithEvents btnItemCancel As Care.Controls.CareButton
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents cbxItemResponse As Care.Controls.CareComboBox
    Friend WithEvents txtItemText As Care.Controls.CareTextBox
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents cbxWeekday As Care.Controls.CareComboBox
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents txtItemSequence As Care.Controls.CareTextBox
    Friend WithEvents btnBlankCheckList As Care.Controls.CareButton
    Friend WithEvents btnItemSaveAdd As Care.Controls.CareButton
    Friend WithEvents GroupControl2 As Care.Controls.CareFrame
End Class
