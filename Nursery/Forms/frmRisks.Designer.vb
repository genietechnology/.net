﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRisks
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.tabMain = New Care.Controls.CareTab(Me.components)
        Me.tabRisk = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl1 = New Care.Controls.CareFrame(Me.components)
        Me.cgHazards = New Care.Controls.CareGridWithButtons()
        Me.GroupControl5 = New Care.Controls.CareFrame(Me.components)
        Me.cbxFreq = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.GroupBox1 = New Care.Controls.CareFrame(Me.components)
        Me.cbxStatus = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.txtRevision = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.cbxArea = New Care.Controls.CareComboBox(Me.components)
        Me.txtTitle = New Care.Controls.CareTextBox(Me.components)
        Me.Label1 = New Care.Controls.CareLabel(Me.components)
        Me.cbxSite = New Care.Controls.CareComboBox(Me.components)
        Me.tabHistory = New DevExpress.XtraTab.XtraTabPage()
        Me.cgHistory = New Care.Controls.CareGridWithButtons()
        Me.tabEquipment = New DevExpress.XtraTab.XtraTabPage()
        Me.cgEquipment = New Care.Controls.CareGridWithButtons()
        Me.tabIncidents = New DevExpress.XtraTab.XtraTabPage()
        Me.cgIncidents = New Care.Controls.CareGrid()
        Me.btnPrint = New Care.Controls.CareButton(Me.components)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.tabMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabMain.SuspendLayout()
        Me.tabRisk.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.cbxFreq.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.cbxStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRevision.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxArea.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTitle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabHistory.SuspendLayout()
        Me.tabEquipment.SuspendLayout()
        Me.tabIncidents.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(761, 3)
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(670, 3)
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnPrint)
        Me.Panel1.Location = New System.Drawing.Point(0, 564)
        Me.Panel1.Size = New System.Drawing.Size(858, 36)
        Me.Panel1.Controls.SetChildIndex(Me.btnOK, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnCancel, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnPrint, 0)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'tabMain
        '
        Me.tabMain.Location = New System.Drawing.Point(8, 53)
        Me.tabMain.Name = "tabMain"
        Me.tabMain.SelectedTabPage = Me.tabRisk
        Me.tabMain.Size = New System.Drawing.Size(842, 511)
        Me.tabMain.TabIndex = 5
        Me.tabMain.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabRisk, Me.tabHistory, Me.tabEquipment, Me.tabIncidents})
        '
        'tabRisk
        '
        Me.tabRisk.Controls.Add(Me.GroupControl1)
        Me.tabRisk.Controls.Add(Me.GroupControl5)
        Me.tabRisk.Controls.Add(Me.GroupBox1)
        Me.tabRisk.Name = "tabRisk"
        Me.tabRisk.Size = New System.Drawing.Size(836, 483)
        Me.tabRisk.Text = "Risk Assessment Details"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.cgHazards)
        Me.GroupControl1.Location = New System.Drawing.Point(7, 152)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(823, 325)
        Me.GroupControl1.TabIndex = 4
        Me.GroupControl1.Text = "Activities, Hazards and Control Measures"
        '
        'cgHazards
        '
        Me.cgHazards.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgHazards.ButtonsEnabled = False
        Me.cgHazards.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgHazards.HideFirstColumn = False
        Me.cgHazards.Location = New System.Drawing.Point(8, 26)
        Me.cgHazards.Name = "cgHazards"
        Me.cgHazards.PreviewColumn = ""
        Me.cgHazards.Size = New System.Drawing.Size(806, 292)
        Me.cgHazards.TabIndex = 7
        '
        'GroupControl5
        '
        Me.GroupControl5.Controls.Add(Me.cbxFreq)
        Me.GroupControl5.Controls.Add(Me.CareLabel8)
        Me.GroupControl5.Location = New System.Drawing.Point(7, 83)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(823, 63)
        Me.GroupControl5.TabIndex = 3
        Me.GroupControl5.Text = "Assessment Frequency"
        '
        'cbxFreq
        '
        Me.cbxFreq.AllowBlank = False
        Me.cbxFreq.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxFreq.DataSource = Nothing
        Me.cbxFreq.DisplayMember = Nothing
        Me.cbxFreq.EnterMoveNextControl = True
        Me.cbxFreq.Location = New System.Drawing.Point(69, 30)
        Me.cbxFreq.Name = "cbxFreq"
        Me.cbxFreq.Properties.AccessibleName = "Tariff Type"
        Me.cbxFreq.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxFreq.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxFreq.Properties.Appearance.Options.UseFont = True
        Me.cbxFreq.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxFreq.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxFreq.SelectedValue = Nothing
        Me.cbxFreq.Size = New System.Drawing.Size(218, 20)
        Me.cbxFreq.TabIndex = 3
        Me.cbxFreq.Tag = "AE"
        Me.cbxFreq.ValueMember = Nothing
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(8, 33)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(55, 15)
        Me.CareLabel8.TabIndex = 0
        Me.CareLabel8.Text = "Frequency"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cbxStatus)
        Me.GroupBox1.Controls.Add(Me.CareLabel4)
        Me.GroupBox1.Controls.Add(Me.txtRevision)
        Me.GroupBox1.Controls.Add(Me.CareLabel3)
        Me.GroupBox1.Controls.Add(Me.CareLabel2)
        Me.GroupBox1.Controls.Add(Me.CareLabel1)
        Me.GroupBox1.Controls.Add(Me.cbxArea)
        Me.GroupBox1.Controls.Add(Me.txtTitle)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cbxSite)
        Me.GroupBox1.Location = New System.Drawing.Point(7, 7)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.ShowCaption = False
        Me.GroupBox1.Size = New System.Drawing.Size(823, 70)
        Me.GroupBox1.TabIndex = 0
        '
        'cbxStatus
        '
        Me.cbxStatus.AllowBlank = False
        Me.cbxStatus.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxStatus.DataSource = Nothing
        Me.cbxStatus.DisplayMember = Nothing
        Me.cbxStatus.EnterMoveNextControl = True
        Me.cbxStatus.Location = New System.Drawing.Point(705, 10)
        Me.cbxStatus.Name = "cbxStatus"
        Me.cbxStatus.Properties.AccessibleName = "Tariff Type"
        Me.cbxStatus.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxStatus.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxStatus.Properties.Appearance.Options.UseFont = True
        Me.cbxStatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxStatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxStatus.SelectedValue = Nothing
        Me.cbxStatus.Size = New System.Drawing.Size(110, 20)
        Me.cbxStatus.TabIndex = 8
        Me.cbxStatus.Tag = "AE"
        Me.cbxStatus.ValueMember = Nothing
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(655, 41)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(44, 15)
        Me.CareLabel4.TabIndex = 7
        Me.CareLabel4.Text = "Revision"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRevision
        '
        Me.txtRevision.CharacterCasing = CharacterCasing.Normal
        Me.txtRevision.EnterMoveNextControl = True
        Me.txtRevision.Location = New System.Drawing.Point(705, 38)
        Me.txtRevision.MaxLength = 250
        Me.txtRevision.Name = "txtRevision"
        Me.txtRevision.NumericAllowNegatives = False
        Me.txtRevision.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRevision.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRevision.Properties.AccessibleName = "Site Name"
        Me.txtRevision.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRevision.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRevision.Properties.Appearance.Options.UseFont = True
        Me.txtRevision.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRevision.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRevision.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRevision.Properties.MaxLength = 250
        Me.txtRevision.Size = New System.Drawing.Size(110, 22)
        Me.txtRevision.TabIndex = 6
        Me.txtRevision.Tag = "AE"
        Me.txtRevision.TextAlign = HorizontalAlignment.Left
        Me.txtRevision.ToolTipText = ""
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(667, 13)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel3.TabIndex = 5
        Me.CareLabel3.Text = "Status"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(8, 41)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(19, 15)
        Me.CareLabel2.TabIndex = 4
        Me.CareLabel2.Text = "Site"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(308, 41)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(24, 15)
        Me.CareLabel1.TabIndex = 3
        Me.CareLabel1.Text = "Area"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxArea
        '
        Me.cbxArea.AllowBlank = False
        Me.cbxArea.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxArea.DataSource = Nothing
        Me.cbxArea.DisplayMember = Nothing
        Me.cbxArea.EnterMoveNextControl = True
        Me.cbxArea.Location = New System.Drawing.Point(338, 38)
        Me.cbxArea.Name = "cbxArea"
        Me.cbxArea.Properties.AccessibleName = "Tariff Type"
        Me.cbxArea.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxArea.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxArea.Properties.Appearance.Options.UseFont = True
        Me.cbxArea.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxArea.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxArea.SelectedValue = Nothing
        Me.cbxArea.Size = New System.Drawing.Size(306, 20)
        Me.cbxArea.TabIndex = 2
        Me.cbxArea.Tag = "AE"
        Me.cbxArea.ValueMember = Nothing
        '
        'txtTitle
        '
        Me.txtTitle.CharacterCasing = CharacterCasing.Normal
        Me.txtTitle.EnterMoveNextControl = True
        Me.txtTitle.Location = New System.Drawing.Point(37, 10)
        Me.txtTitle.MaxLength = 40
        Me.txtTitle.Name = "txtTitle"
        Me.txtTitle.NumericAllowNegatives = False
        Me.txtTitle.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtTitle.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtTitle.Properties.AccessibleName = "Site Name"
        Me.txtTitle.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtTitle.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtTitle.Properties.Appearance.Options.UseFont = True
        Me.txtTitle.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTitle.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtTitle.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTitle.Properties.MaxLength = 40
        Me.txtTitle.Size = New System.Drawing.Size(607, 22)
        Me.txtTitle.TabIndex = 1
        Me.txtTitle.Tag = "AEBM"
        Me.txtTitle.TextAlign = HorizontalAlignment.Left
        Me.txtTitle.ToolTipText = ""
        '
        'Label1
        '
        Me.Label1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label1.Location = New System.Drawing.Point(8, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(23, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Title"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(37, 38)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AccessibleName = "Tariff Type"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(250, 20)
        Me.cbxSite.TabIndex = 1
        Me.cbxSite.Tag = "AE"
        Me.cbxSite.ValueMember = Nothing
        '
        'tabHistory
        '
        Me.tabHistory.Controls.Add(Me.cgHistory)
        Me.tabHistory.Name = "tabHistory"
        Me.tabHistory.Size = New System.Drawing.Size(836, 483)
        Me.tabHistory.Text = "Assessment History"
        '
        'cgHistory
        '
        Me.cgHistory.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgHistory.ButtonsEnabled = False
        Me.cgHistory.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgHistory.HideFirstColumn = False
        Me.cgHistory.Location = New System.Drawing.Point(8, 7)
        Me.cgHistory.Name = "cgHistory"
        Me.cgHistory.PreviewColumn = ""
        Me.cgHistory.Size = New System.Drawing.Size(820, 470)
        Me.cgHistory.TabIndex = 6
        '
        'tabEquipment
        '
        Me.tabEquipment.Controls.Add(Me.cgEquipment)
        Me.tabEquipment.Name = "tabEquipment"
        Me.tabEquipment.Size = New System.Drawing.Size(836, 483)
        Me.tabEquipment.Text = "Associated Equipment"
        '
        'cgEquipment
        '
        Me.cgEquipment.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgEquipment.ButtonsEnabled = False
        Me.cgEquipment.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgEquipment.HideFirstColumn = False
        Me.cgEquipment.Location = New System.Drawing.Point(8, 7)
        Me.cgEquipment.Name = "cgEquipment"
        Me.cgEquipment.PreviewColumn = ""
        Me.cgEquipment.Size = New System.Drawing.Size(820, 470)
        Me.cgEquipment.TabIndex = 7
        '
        'tabIncidents
        '
        Me.tabIncidents.Controls.Add(Me.cgIncidents)
        Me.tabIncidents.Name = "tabIncidents"
        Me.tabIncidents.Size = New System.Drawing.Size(836, 483)
        Me.tabIncidents.Text = "Associated Incidents"
        '
        'cgIncidents
        '
        Me.cgIncidents.AllowBuildColumns = True
        Me.cgIncidents.AllowEdit = False
        Me.cgIncidents.AllowHorizontalScroll = False
        Me.cgIncidents.AllowMultiSelect = False
        Me.cgIncidents.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgIncidents.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgIncidents.Appearance.Options.UseFont = True
        Me.cgIncidents.AutoSizeByData = True
        Me.cgIncidents.DisableAutoSize = False
        Me.cgIncidents.DisableDataFormatting = False
        Me.cgIncidents.FocusedRowHandle = -2147483648
        Me.cgIncidents.HideFirstColumn = False
        Me.cgIncidents.Location = New System.Drawing.Point(8, 7)
        Me.cgIncidents.Name = "cgIncidents"
        Me.cgIncidents.PreviewColumn = ""
        Me.cgIncidents.QueryID = Nothing
        Me.cgIncidents.RowAutoHeight = False
        Me.cgIncidents.SearchAsYouType = True
        Me.cgIncidents.ShowAutoFilterRow = False
        Me.cgIncidents.ShowFindPanel = False
        Me.cgIncidents.ShowGroupByBox = False
        Me.cgIncidents.ShowLoadingPanel = False
        Me.cgIncidents.ShowNavigator = False
        Me.cgIncidents.Size = New System.Drawing.Size(820, 469)
        Me.cgIncidents.TabIndex = 1
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnPrint.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnPrint.Appearance.Options.UseFont = True
        Me.btnPrint.Location = New System.Drawing.Point(8, 3)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(160, 25)
        Me.btnPrint.TabIndex = 2
        Me.btnPrint.Text = "Print Assessment"
        '
        'frmRisks
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(858, 600)
        Me.Controls.Add(Me.tabMain)
        Me.Name = "frmRisks"
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.tabMain, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.tabMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabMain.ResumeLayout(False)
        Me.tabMain.PerformLayout()
        Me.tabRisk.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.cbxFreq.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.cbxStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRevision.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxArea.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTitle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabHistory.ResumeLayout(False)
        Me.tabEquipment.ResumeLayout(False)
        Me.tabIncidents.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tabMain As Care.Controls.CareTab
    Friend WithEvents tabRisk As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
    Friend WithEvents txtTitle As Care.Controls.CareTextBox
    Friend WithEvents Label1 As Care.Controls.CareLabel
    Friend WithEvents tabHistory As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cgHazards As Care.Controls.CareGridWithButtons
    Friend WithEvents cbxFreq As Care.Controls.CareComboBox
    Friend WithEvents cbxStatus As Care.Controls.CareComboBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents txtRevision As Care.Controls.CareTextBox
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents cbxArea As Care.Controls.CareComboBox
    Friend WithEvents cgHistory As Care.Controls.CareGridWithButtons
    Friend WithEvents tabIncidents As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cgIncidents As Care.Controls.CareGrid
    Friend WithEvents tabEquipment As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cgEquipment As Care.Controls.CareGridWithButtons
    Friend WithEvents btnPrint As Care.Controls.CareButton
    Friend WithEvents GroupControl5 As Care.Controls.CareFrame
    Friend WithEvents GroupBox1 As Care.Controls.CareFrame
    Friend WithEvents GroupControl1 As Care.Controls.CareFrame
End Class
