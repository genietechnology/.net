﻿Imports Care.Data
Imports Care.Global

Public Class frmCDAPAreas

    Dim m_Area As Business.CDAPArea

    Private Sub frmCDAPAreas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.GridSQL = "select id, name as 'Area Name', seq as 'Display Sequence'" & _
                     " from CDAPAreas" & _
                     " order by seq"

        gbx.Hide()

    End Sub

    Protected Overrides Sub SetBindings()

        m_Area = New Business.CDAPArea
        bs.DataSource = m_Area

        txtName.DataBindings.Add("Text", bs, "_Name")
        txtSeq.DataBindings.Add("Text", bs, "_Seq")

    End Sub

    Protected Overrides Sub BindToID(ID As System.Guid, IsNew As Boolean)
        m_Area = Business.CDAPArea.RetreiveByID(ID)
        bs.DataSource = m_Area
    End Sub

    Protected Overrides Sub CommitUpdate()
        Business.CDAPArea.SaveRecord(CType(bs.Item(bs.Position), Business.CDAPArea))
    End Sub

    Protected Overrides Sub CommitDelete(ID As System.Guid)
        Dim _SQL As String = "delete from CDAPAreas where id = '" + ID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)
    End Sub

    Protected Overrides Sub AfterAdd()
        MyBase.AfterAdd()
        txtName.Focus()
    End Sub

    Protected Overrides Sub AfterEdit()
        MyBase.AfterEdit()
        txtName.Focus()
    End Sub

End Class
