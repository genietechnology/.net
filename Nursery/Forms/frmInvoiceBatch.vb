﻿

Imports Care.Global
Imports Care.Shared
Imports Care.Data

Public Class frmInvoiceBatch

    Private m_BatchID As Guid? = Nothing
    Private m_BatchHeader As Business.InvoiceBatch = Nothing
    Private m_PrintPassword As Boolean = False
    Private m_PasswordEntered As Boolean = False

    Public Sub New()
        ' This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub

    Public Sub New(ByVal BatchID As Guid, ByVal Caption As String)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        m_BatchID = BatchID
        DisplayRecord()

        Me.Text = Caption
        Me.MinimizeBox = False
        Me.MaximizeBox = False
        Me.WindowState = FormWindowState.Normal
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.ToolbarMode = ToolbarEnum.EditOnly

    End Sub

    Private Sub frmInvoiceBatch_Load(sender As Object, e As EventArgs) Handles Me.Load

        m_PrintPassword = ParameterHandler.ReturnBoolean("FINPRINTMGRPASS")

        cdtGlobalInvoiceDate.Enabled = True
        cdtGlobalInvoiceDue.Enabled = True
        gbxGlobalChanges.Hide()
        gbxPrint.Hide()

        SetCMDs()
        Me.DeleteMessage = "Are you sure you want to Abandon this batch?"

    End Sub

    Protected Overrides Sub AfterAdd()

        m_BatchID = Nothing
        Clear()

        Dim _Inv As New frmInvoiceBatchCreate
        _Inv.ShowDialog()

        If _Inv.DialogResult = DialogResult.OK Then
            m_BatchID = CType(_Inv.Tag, Guid)
            DisplayRecord()
        Else
            Me.ToolbarMode = ToolbarEnum.FindandNew
        End If

        ToolAccept()

    End Sub

    Private Sub Clear()
        Me.RecordPopulated = False
        txtBatchNo.Text = ""
        txtStatus.Text = ""
        txtComments.Text = ""
        cgbInvoices.Clear()
        SetCMDs()
    End Sub

    Private Sub DisplayInvoices()

        Dim _SQL As String = ""
        _SQL += "select i.ID, i.family_account_no as 'Account No', i.invoice_no as 'Invoice No', i.invoice_status as 'Status',"
        _SQL += " i.invoice_date as 'Invoice Date', i.invoice_due_date as 'Invoice Due',"
        _SQL += " i.invoice_period as 'Invoice From', i.invoice_period_end as 'Invoice To',"
        _SQL += " i.family_name as 'Addressee', f.surname as 'Family Surname', i.child_name as 'Child', c.dob as 'DOB',"
        _SQL += " i.changed_by, i.invoice_annualised as 'Annualised',"

        If Not chkHideNL.Checked Then
            _SQL += " c.nl_code as 'NL Code', c.nl_tracking as 'NL Tracking',"
        End If

        _SQL += " c.payment_method as 'Method', c.payment_ref as 'Ref.',"

        If Not chkHideXCheck.Checked Then
            _SQL += " i.xcheck_count as 'XCheck Count', i.xcheck_amount as 'XCheck Value',"
        End If

        If Not chkHideDDInfo.Checked Then
            _SQL += " i.voucher_proportion as 'V. Prop', i.voucher_pool as 'V. Pool', i.voucher_amount as 'V. Applied',"
        End If

        If Not chkHideDeposits.Checked Then
            _SQL += " (select isnull(sum(dep_value),0) from ChildDeposits where child_id = i.child_id and dep_date between i.invoice_period and i.invoice_period_end) as 'Deposit Movement',"
        End If

        _SQL += " i.invoice_sub as 'Sub Total', i.invoice_discount as 'Discount', i.invoice_total as 'Total'"
        _SQL += " from Invoices i"
        _SQL += " left join Children c on c.ID = i.Child_ID"
        _SQL += " left join Family f on f.ID = i.family_id"
        _SQL += " where i.batch_id = '" + m_BatchID.Value.ToString + "'"

        If radManual.Checked Then
            _SQL += " and i.changed_by is not null"
        End If

        If radXCheck.Checked Then
            _SQL += " and i.xcheck_count > 0"
            chkHideXCheck.Checked = False
        End If

        _SQL += " order by i.invoice_no"

        cgbInvoices.HideFirstColumn = True
        cgbInvoices.Populate(Session.ConnectionString, _SQL)

        cgbInvoices.Columns("changed_by").Visible = False

    End Sub

    Private Sub SetItemCMDs(ByVal Enabled As Boolean)
        btnAmendInvoice.Enabled = Enabled
        btnDeleteInvoice.Enabled = Enabled
    End Sub

    Protected Overrides Function BeforeDelete() As Boolean
        If txtStatus.Text <> "Open" Then
            CareMessage("Only Open batches can be Abandoned.", MessageBoxIcon.Exclamation, "Abandon Batch")
            Return False
        Else
            Me.DeleteMessage = "Are you sure you wish to Abandon this Invoice Batch?"
            Return ParameterHandler.ManagerAuthorised
        End If
    End Function

    Protected Overrides Sub CommitDelete()

        m_BatchHeader._BatchStatus = "Abandoned"
        m_BatchHeader.Store()

        Clear()
        Me.ToolbarMode = ToolbarEnum.FindandNew

    End Sub

    Protected Overrides Function BeforeEdit() As Boolean
        If txtStatus.Text <> "Open" Then
            CareMessage("Only Open batches can be Abandoned.", MessageBoxIcon.Exclamation, "Edit Batch")
            Return False
        Else
            Return True
        End If
    End Function

    Protected Overrides Sub AfterEdit()

        Dim _Inv As New frmInvoiceBatchCreate(m_BatchID.Value)
        _Inv.ShowDialog()

        ToolAccept()
        Me.ToolbarMode = ToolbarEnum.Standard

    End Sub

    Protected Overrides Sub CommitUpdate()
        'stub for creating new batch etc
    End Sub

    Protected Overrides Sub SetBindings()
        'stub
    End Sub

    Protected Overrides Sub FindRecord()

        Dim _Find As New GenericFind
        With _Find

            .Caption = "Find Invoice Batch"
            .ParentForm = ParentForm
            .PopulateOnLoad = True
            .ConnectionString = Session.ConnectionString

            .GridSQL = "select batch_no as 'Batch No', batch_status as 'Status'," &
                       " batch_period as 'Type', site_name as 'Site', batch_class as 'Class.', batch_freq as 'Freq.'," &
                       " payment_method as 'Pay. Method', batch_group as 'Room', batch_layout as 'Layout'," &
                       " date_first as 'Inv. From', date_last as 'Inv. To'," &
                       " (select count(*) from Invoices where Invoices.batch_id = InvoiceBatch.ID) 'Items', comments" &
                       " from InvoiceBatch"

            .GridWhereClause = " and batch_period not like '%Annual%' and batch_status <> 'Abandoned'"
            .GridOrderBy = "order by batch_no desc"
            .ReturnField = "ID"
            .FormWidth = 1000
            .PreviewField = "comments"
            .Show()

            If .ReturnValue <> "" Then
                m_BatchID = New Guid(.ReturnValue)
                DisplayRecord()
            End If

        End With

    End Sub

    Private Sub DisplayRecord()

        Session.CursorWaiting()

        'fetch the header record
        m_BatchHeader = Business.InvoiceBatch.RetreiveByID(m_BatchID.Value)

        DisplayInvoices()

        Me.ToolbarMode = ToolbarEnum.Standard
        Me.RecordPopulated = True

        txtBatchNo.Text = m_BatchHeader._BatchNo.ToString
        txtStatus.Text = m_BatchHeader._BatchStatus
        txtComments.Text = m_BatchHeader._Comments

        SetCMDs()

        Session.CursorDefault()

    End Sub

    Private Sub SetCMDs()

        If m_BatchHeader Is Nothing Then
            btnAddInvoice.Enabled = False
            btnPrintEmail.Enabled = False
            btnPost.Enabled = False
            btnPrintOptions.Enabled = False
            btnCrossCheck.Enabled = False
            btnGlobalChanges.Enabled = False
            SetItemCMDs(False)
        Else

            'confirmed isnt used anymore, but we need to keep it as some people might have confirmed batches in their system
            If m_BatchHeader._BatchStatus = "Open" Or m_BatchHeader._BatchStatus = "Confirmed" Then
                btnAddInvoice.Enabled = True
                If cgbInvoices.RecordCount > 0 Then
                    btnPrintEmail.Enabled = True
                    btnPost.Enabled = True
                    btnGlobalChanges.Enabled = True
                    SetItemCMDs(True)
                Else
                    btnPrintEmail.Enabled = False
                    btnPost.Enabled = False
                    btnGlobalChanges.Enabled = False
                    SetItemCMDs(False)
                End If
            Else
                btnAddInvoice.Enabled = False
                btnPost.Enabled = False
                btnPrintEmail.Enabled = False
                btnGlobalChanges.Enabled = False
                SetItemCMDs(False)
            End If

            If cgbInvoices.RecordCount > 0 Then
                btnPrintOptions.Enabled = True
                btnCrossCheck.Enabled = True
            Else
                btnPrintOptions.Enabled = False
                btnCrossCheck.Enabled = False
            End If

        End If

    End Sub

    Protected Overrides Sub SetBindingsByID(ID As Guid)
        'stub
    End Sub

    Private Sub btnPrintSummary_Click(sender As Object, e As EventArgs) Handles btnPrintOptions.Click
        If m_PrintPassword Then
            If m_PasswordEntered Then
                gbxPrint.Show()
            Else
                If ParameterHandler.ManagerAuthorised Then
                    m_PasswordEntered = True
                    gbxPrint.Show()
                End If
            End If
        Else
            gbxPrint.Show()
        End If
    End Sub

    Private Sub btnPrintEmail_Click(sender As Object, e As EventArgs) Handles btnPrintEmail.Click

        btnPrintEmail.Enabled = False
        Session.CursorWaiting()

        're-apply invoice numbers in case someone has deleted invoices etc
        Invoicing.ReApplyInvoiceNumbers(m_BatchID.Value, True)

        Dim _Invoices As List(Of Guid) = ReturnInvoiceList()
        If _Invoices.Count > 0 Then
            If Invoicing.SendInvoices(_Invoices, False) Then
                Session.CursorWaiting()
                DisplayRecord()
            End If
        End If

        btnPrintEmail.Enabled = True

    End Sub

    Private Function ReturnInvoiceList() As List(Of Guid)

        Dim _Invoices As New List(Of Guid)

        Dim _SQL As String = "select ID from Invoices where batch_id = '" + m_BatchID.Value.ToString + "'"
        Dim _DT As DataTable = Care.Data.DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            For Each _DR As DataRow In _DT.Rows
                Dim _ID As Guid = New Guid(_DR.Item("ID").ToString)
                _Invoices.Add(_ID)
            Next

            _DT.Dispose()
            _DT = Nothing

        End If

        Return _Invoices

    End Function

    Private Sub btnAmendInvoice_Click(sender As Object, e As EventArgs) Handles btnAmendInvoice.Click
        AmendInvoice()
    End Sub

    Private Sub cgbInvoices_GridDoubleClick(sender As Object, e As EventArgs) Handles cgbInvoices.GridDoubleClick
        AmendInvoice()
    End Sub

    Private Sub AmendInvoice()

        If cgbInvoices.RecordCount < 1 Then Exit Sub
        Dim _ID As New Guid(cgbInvoices.CurrentRow.Item("id").ToString)

        If btnAmendInvoice.Enabled Then

            cgbInvoices.PersistRowPosition()
            If Invoicing.AmendInvoice(_ID) Then
                DisplayInvoices()
            End If
            cgbInvoices.RestoreRowPosition()

        Else
            Invoicing.ViewInvoice(_ID)
        End If

    End Sub

    Private Sub btnPost_Click(sender As Object, e As EventArgs) Handles btnPost.Click

        If Not PostCheck() Then Exit Sub

        If Session.FinancialsIntegrated AndAlso Session.FinancialSystem.ToUpper = "QUICKBOOKS" Then

            Dim _frm As New frmInvoiceQBPreview(m_BatchID.Value)
            _frm.Text = "Quickbooks Data Export Data"
            _frm.ShowDialog()

            Clear()
            Me.ToolbarMode = ToolbarEnum.FindandNew

        Else
            If CareMessage("Are you sure you want to Post this Invoice Batch?", MessageBoxIcon.Question, MessageBoxButtons.YesNo, "Post Invoices") = DialogResult.Yes Then
                btnPost.Enabled = False
                Invoicing.PostBatch(m_BatchID.Value, m_BatchHeader._SiteId.Value)
                DisplayRecord()
                btnPost.Enabled = True
            End If
        End If

    End Sub

    Private Function PostCheck() As Boolean

        If m_BatchHeader._BatchPeriod.ToUpper.Contains("ANNUAL") Then Return False
        If m_BatchHeader._BatchPeriod = "Opening Balances" Then Return True

        'if we are integrated with a financials system, we need to ensure all invoices have been printed or emailed
        If Session.FinancialsIntegrated Then

            If Not Invoicing.BatchReadyToPost(m_BatchID.Value) Then

                Dim _Mess As String = "One or more invoices have not yet been printed or emailed. Are you sure you want to Continue?"
                If CareMessage(_Mess, MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Post Check") = DialogResult.Yes Then
                    If InputBox("Please type POST to continue.", "Force Post").ToUpper <> "POST" Then
                        Return False
                    End If
                Else
                    Return False
                End If

            End If

        End If

        Return True

    End Function


    Private Sub btnDeleteInvoice_Click(sender As Object, e As EventArgs) Handles btnDeleteInvoice.Click

        If cgbInvoices.RecordCount < 1 Then Exit Sub

        If CareMessage("Are you sure you want to Delete this Invoice?", MessageBoxIcon.Warning, MessageBoxButtons.YesNo, "Delete Invoice") = DialogResult.Yes Then
            Dim _ID As String = cgbInvoices.CurrentRow.Item("id").ToString
            Invoicing.DeleteInvoice(_ID)
            Invoicing.ReApplyInvoiceNumbers(m_BatchID.Value, True)
            DisplayRecord()
        End If

    End Sub

    Private Sub btnAddInvoice_Click(sender As Object, e As EventArgs) Handles btnAddInvoice.Click

        Dim _ChildID As String = Business.Child.FindChild(Me)

        If _ChildID <> "" Then

            'check if this child is already in this batch
            If ChildInBatch(_ChildID) Then
                If CareMessage("This Child is already in this Invoice Batch. Are you sure you want to Add them again?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Add Child") = DialogResult.No Then
                    Exit Sub
                End If
            End If

            Dim _frm As New frmInvoiceAdd(m_BatchHeader, _ChildID)
            _frm.ShowDialog()

            If _frm.DialogResult = DialogResult.OK Then
                DisplayRecord()
                SetCMDs()
            End If

            _frm.Dispose()
            _frm = Nothing

        End If

    End Sub

    Private Function ChildInBatch(ByVal ChildID As String) As Boolean

        Dim _SQL As String = ""
        _SQL += "select * from Invoices"
        _SQL += " where batch_id = '" + m_BatchID.Value.ToString + "'"
        _SQL += " and child_id = '" + ChildID + "'"

        Dim _DR As DataRow = DAL.GetRowfromSQL(Session.ConnectionString, _SQL)
        If _DR IsNot Nothing Then
            _DR = Nothing
            Return True
        Else
            Return False
        End If

    End Function

    Private Sub btnCrossCheck_Click(sender As Object, e As EventArgs) Handles btnCrossCheck.Click

        Dim _frm As New frmInvoiceCrossCheck(m_BatchHeader)
        _frm.MdiParent = Me.MdiParent
        _frm.WindowState = FormWindowState.Maximized
        _frm.Show()

    End Sub

    Private Sub GroupControl1_DoubleClick(sender As Object, e As EventArgs) Handles GroupControl1.DoubleClick

        If InputBox("Enter password for the override options:", "Override Batch") <> "care122126" Then Exit Sub

        Dim _Mess As String = ""
        _Mess += "1. Copy Financial ID from Family Record to Invoices" + vbCrLf
        _Mess += "2. Mark all Invoices as Printed" + vbCrLf
        _Mess += "3. Recalculate entire batch" + vbCrLf
        _Mess += "4. Copy Account Number from Family Record to Invoices" + vbCrLf
        _Mess += "5. Re-Apply NL Codes and Tracking" + vbCrLf
        _Mess += "6. Mark Batch as Posted" + vbCrLf

        If CareMessage("Available Functions:" + vbCrLf + vbCrLf + _Mess + vbCrLf + "Click Yes to continue", MessageBoxIcon.Information, MessageBoxButtons.YesNo, "Functions Menu") = DialogResult.No Then Exit Sub
        RunOverride(InputBox("Enter function number:", "Enter Function Number"))
        DisplayRecord()

    End Sub

    Private Sub RunOverride(ByVal FunctionNumber As String)

        If FunctionNumber = "" Then Exit Sub

        Dim _SQL As String = ""
        _SQL = "select i.ID, f.financials_id, f.account_no from Invoices i"
        _SQL += " left join Family f on f.ID = i.family_id"
        _SQL += " where i.batch_id = '" + m_BatchID.Value.ToString + "'"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            Session.SetupProgressBar("Processing...", _DT.Rows.Count)
            For Each _DR As DataRow In _DT.Rows

                Dim _I As Business.Invoice = Business.Invoice.RetreiveByID(New Guid(_DR.Item("ID").ToString))
                If _I IsNot Nothing Then

                    Select Case FunctionNumber

                        Case "1"
                            _I._FinancialsId = _DR.Item("financials_id").ToString
                            _I.Store()

                        Case "2"
                            _I._InvoiceStatus = "Printed"
                            _I.Store()

                        Case "3"
                            Invoicing.RecalculateInvoice(New Guid(_DR.Item("ID").ToString))

                        Case "4"
                            _I._FamilyAccountNo = _DR.Item("account_no").ToString
                            _I.Store()

                        Case "5"
                            Invoicing.ReApplyNLCodes(_I)

                        Case "6"
                            _I._InvoiceStatus = "Posted"
                            _I.Store()

                    End Select

                End If

                Session.StepProgressBar()

            Next

            If FunctionNumber = "6" Then

                m_BatchHeader._BatchStatus = "Posted"
                m_BatchHeader.Store()

                Clear()
                Me.ToolbarMode = ToolbarEnum.FindandNew

            End If

            Session.HideProgressBar()
            CareMessage("Process Complete.", MessageBoxIcon.Information, "Run Function")

        End If

    End Sub

    Private Sub chkHideNL_CheckedChanged(sender As Object, e As EventArgs) Handles chkHideNL.CheckedChanged
        DisplayInvoices()
    End Sub

    Private Sub chkHideXCheck_CheckedChanged(sender As Object, e As EventArgs) Handles chkHideXCheck.CheckedChanged
        DisplayInvoices()
    End Sub

    Private Sub radAll_CheckedChanged(sender As Object, e As EventArgs) Handles radAll.CheckedChanged
        DisplayInvoices()
    End Sub

    Private Sub radXCheck_CheckedChanged(sender As Object, e As EventArgs) Handles radXCheck.CheckedChanged
        DisplayInvoices()
    End Sub

    Private Sub radManual_CheckedChanged(sender As Object, e As EventArgs) Handles radManual.CheckedChanged
        DisplayInvoices()
    End Sub

    Private Sub cgbInvoices_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles cgbInvoices.RowStyle

        If e.RowHandle < 0 Then Exit Sub

        If cgbInvoices.UnderlyingGridView.GetRowCellDisplayText(e.RowHandle, "changed_by").ToString <> "" Then
            e.Appearance.BackColor = txtYellow.BackColor
        Else
            If ValueHandler.ConvertBoolean(cgbInvoices.UnderlyingGridView.GetRowCellValue(e.RowHandle, "Annualised")) = True Then
                e.Appearance.BackColor = txtBlue.BackColor
            Else
                If ValueHandler.ConvertInteger(cgbInvoices.UnderlyingGridView.GetRowCellValue(e.RowHandle, "XCheck Count")) > 0 Then
                    e.Appearance.BackColor = txtRed.BackColor
                End If
            End If
        End If

    End Sub

    Private Sub btnGlobalChanges_Click(sender As Object, e As EventArgs) Handles btnGlobalChanges.Click

        gbxGlobalChanges.Enabled = True
        btnGlobalRun.Enabled = True
        btnGlobalCancel.Enabled = True

        cdtGlobalInvoiceDate.Enabled = True
        cdtGlobalInvoiceDate.ReadOnly = False
        cdtGlobalInvoiceDate.Value = m_BatchHeader._BatchDate

        cdtGlobalInvoiceDue.Enabled = True
        cdtGlobalInvoiceDue.ReadOnly = False
        cdtGlobalInvoiceDue.Text = ""

        gbxGlobalChanges.Show()
        cdtGlobalInvoiceDate.Focus()

    End Sub

    Private Sub btnGlobalRun_Click(sender As Object, e As EventArgs) Handles btnGlobalRun.Click

        If cdtGlobalInvoiceDate.Text = "" Then
            CareMessage("Please enter the new Invoice date.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Exit Sub
        End If

        If cdtGlobalInvoiceDue.Text <> "" Then
            If CareMessage("You have set an Invoice Due date. This will be appied across all Invoices regardless of the Invoice Due settings on the Family Record. Is this intentional?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Change Invoice Due") = DialogResult.No Then
                Exit Sub
            End If
        End If

        If CareMessage("Are you sure you want to apply the change to this entire Invoice Batch?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Global Change") = DialogResult.Yes Then

            gbxGlobalChanges.Enabled = False
            Session.CursorWaiting()

            Dim _SQL As String = ""
            _SQL = "select i.ID from Invoices i"
            _SQL += " where i.batch_id = '" + m_BatchID.Value.ToString + "'"

            Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
            If _DT IsNot Nothing Then

                Session.SetupProgressBar("Processing...", _DT.Rows.Count)
                For Each _DR As DataRow In _DT.Rows

                    Dim _I As Business.Invoice = Business.Invoice.RetreiveByID(New Guid(_DR.Item("ID").ToString))
                    If _I IsNot Nothing Then

                        _I._InvoiceDate = cdtGlobalInvoiceDate.Value

                        If cdtGlobalInvoiceDue.Text = "" Then
                            _I._InvoiceDueDate = Invoicing.ReturnInvoiceDueDate(_I._InvoiceDue, _I._InvoiceDate.Value, _I._InvoicePeriod)
                        Else
                            _I._InvoiceDueDate = cdtGlobalInvoiceDue.Value
                        End If

                        _I.Store()

                    End If

                    Session.StepProgressBar()

                Next

                m_BatchHeader._BatchDate = cdtGlobalInvoiceDate.Value
                m_BatchHeader.Store()

                Session.HideProgressBar()

                DisplayInvoices()

                gbxGlobalChanges.Enabled = True
                Session.CursorDefault()

                CareMessage("Process Complete.", MessageBoxIcon.Information, "Global Changes")

            End If

            gbxGlobalChanges.Hide()

        End If

    End Sub

    Private Sub btnGlobalCancel_Click(sender As Object, e As EventArgs) Handles btnGlobalCancel.Click
        gbxGlobalChanges.Hide()
    End Sub

    Private Sub txtStatus_DoubleClick(sender As Object, e As EventArgs) Handles txtStatus.DoubleClick
        If txtStatus.Text = "Posted" Then
            If InputBox("Enter password to Reset Batch back to Open:", "Override Batch") = "care122126" Then
                m_BatchHeader._BatchStatus = "Open"
                m_BatchHeader.Store()
                DisplayRecord()
            End If
        End If
    End Sub

    Private Sub chkHideDDInfo_CheckedChanged(sender As Object, e As EventArgs) Handles chkHideDDInfo.CheckedChanged
        DisplayInvoices()
    End Sub

    Private Sub btnPrintClose_Click(sender As Object, e As EventArgs) Handles btnPrintClose.Click
        gbxPrint.Hide()
    End Sub

    Private Sub btnPrintInvoices_Click(sender As Object, e As EventArgs) Handles btnPrintInvoices.Click

        PrintCMDs(False)
        Session.CursorApplicationStarting()

        Dim _Invoices As List(Of Guid) = ReturnInvoiceList()
        If _Invoices.Count > 0 Then

            Dim _Errs As String = ""
            Dim _r As New Reports.InvoiceRun
            _r.RunInvoices(Reports.InvoiceRun.EnumOutputMode.ForcePrint, _Invoices, "", _Errs, "", "")
            _r = Nothing

        End If

        PrintCMDs(True)
        Session.CursorDefault()

    End Sub

    Private Sub btnPrintBatch_Click(sender As Object, e As EventArgs) Handles btnPrintBatch.Click

        PrintCMDs(False)
        Session.CursorApplicationStarting()

        Invoicing.PrintInvoiceSummary(m_BatchID.Value, btnPrintOptions.ShiftDown)

        PrintCMDs(True)
        Session.CursorDefault()

    End Sub

    Private Sub PrintCMDs(ByVal Enabled As Boolean)
        btnPrintBatch.Enabled = Enabled
        btnPrintInvoices.Enabled = Enabled
        btnPrintClose.Enabled = Enabled
    End Sub

    Private Sub chkHideDeposits_CheckedChanged(sender As Object, e As EventArgs) Handles chkHideDeposits.CheckedChanged
        DisplayInvoices()
    End Sub
End Class
