﻿

Imports Care.Global

Public Class frmChildCalc

    Private m_IsNew As Boolean = True
    Private m_ChildID As Guid? = Nothing
    Private m_CalculationID As Guid? = Nothing
    Private m_CalculationText As String = ""

#Region "Constuctors"

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub New(ByVal ChildID As Guid, ByVal CalculationID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_ChildID = ChildID
        m_CalculationID = CalculationID
        If CalculationID.HasValue Then m_IsNew = False

    End Sub

#End Region

    Private Sub frmChildCalc_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cdtDateFrom.BackColor = Session.ChangeColour
        chkActive.BackColor = Session.ChangeColour

        mcAnnual.BackColor = Session.ChangeColour
        mcFunded.BackColor = Session.ChangeColour

        cbxInvoiceSplit.BackColor = Session.ChangeColour
        cbxInvoiceSplit.AddItem("month")

        For _i = 2 To 12
            cbxInvoiceSplit.AddItem(_i.ToString + " months")
        Next

        For _i = 5 To 53
            cbxInvoiceSplit.AddItem(_i.ToString + " weeks")
        Next

        cbxInvoiceSplit.SelectedIndex = 0

        SetupList(cbxMc2)
        SetupList(cbxMc3)
        SetupList(cbxMc4)
        SetupList(cbxMc5)

        If m_IsNew Then
            Me.Text = "Create New Manual Calculation"
            chkActive.Checked = True
        Else
            Me.Text = "Edit Manual Calculation"
            DisplayRecord()
        End If

    End Sub

    Private Sub SetupList(ByRef ComboIn As Care.Controls.CareComboBox)
        ComboIn.AllowBlank = False
        ComboIn.BackColor = Session.ChangeColour
        ComboIn.AddItem("None")
        ComboIn.AddItem("Add (+)")
        ComboIn.AddItem("Subtract (-)")
        ComboIn.SelectedIndex = 0
    End Sub

    Private Sub DisplayRecord()

        Dim _Calc As Business.ChildCalculation = Business.ChildCalculation.RetreiveByID(m_CalculationID.Value)
        cdtDateFrom.Value = _Calc._DateFrom
        cbxInvoiceSplit.Text = _Calc._SplitType
        chkActive.Checked = _Calc._Active
        m_CalculationText = _Calc._InvoiceText

        '**********************************************************

        With mcAnnual
            .Units = _Calc._Units
            .Multiplier = _Calc._Multiplier
            .Rate = _Calc._Rate
            .Per = _Calc._Per
            .Weeks = _Calc._Weeks
            .Recalculate()
        End With

        '**********************************************************

        cbxMc2.SelectedIndex = ReturnIndexFromSign(_Calc._C2Sign)
        With mc2
            .Units = _Calc._C2Units
            .Multiplier = _Calc._C2Multiplier
            .Rate = _Calc._C2Rate
            .Per = _Calc._C2Per
            .Weeks = _Calc._C2Weeks
            .Recalculate()
        End With

        '**********************************************************

        cbxMc3.SelectedIndex = ReturnIndexFromSign(_Calc._C3Sign)
        With mc3
            .Units = _Calc._C3Units
            .Multiplier = _Calc._C3Multiplier
            .Rate = _Calc._C3Rate
            .Per = _Calc._C3Per
            .Weeks = _Calc._C3Weeks
            .Recalculate()
        End With

        '**********************************************************

        cbxMc4.SelectedIndex = ReturnIndexFromSign(_Calc._C4Sign)
        With mc4
            .Units = _Calc._C4Units
            .Multiplier = _Calc._C4Multiplier
            .Rate = _Calc._C4Rate
            .Per = _Calc._C4Per
            .Weeks = _Calc._C4Weeks
            .Recalculate()
        End With

        '**********************************************************

        cbxMc5.SelectedIndex = ReturnIndexFromSign(_Calc._C5Sign)
        With mc5
            .Units = _Calc._C5Units
            .Multiplier = _Calc._C5Multiplier
            .Rate = _Calc._C5Rate
            .Per = _Calc._C5Per
            .Weeks = _Calc._C5Weeks
            .Recalculate()
        End With

        '**********************************************************

        With mcFunded
            .Units = _Calc._FundUnits
            .Multiplier = _Calc._FundMultiplier
            .Rate = _Calc._FundRate
            .Per = _Calc._FundPer
            .Weeks = _Calc._FundWeeks
            .Recalculate()
        End With

        '**********************************************************

        CalculateScreenTotal()

        '**********************************************************

        _Calc = Nothing

    End Sub

    Private Function Commit() As Boolean

        CalculateScreenTotal()

        Dim _Calc As Business.ChildCalculation = Nothing

        If m_IsNew Then
            _Calc = New Business.ChildCalculation
            _Calc._Active = True
        Else
            _Calc = Business.ChildCalculation.RetreiveByID(m_CalculationID.Value)
            _Calc._Active = chkActive.Checked
        End If

        With _Calc

            ._ChildId = m_ChildID
            ._DateFrom = cdtDateFrom.Value
            ._SplitType = cbxInvoiceSplit.Text

            '*******************************************

            ._Units = mcAnnual.Units
            ._Multiplier = mcAnnual.Multiplier
            ._Rate = mcAnnual.Rate
            ._Per = mcAnnual.Per
            ._Weeks = mcAnnual.Weeks

            ._CalcDay = mcAnnual.CalculatedDay
            ._CalcWeek = mcAnnual.CalculatedWeek
            ._CalcMonth = mcAnnual.CalculatedMonth
            ._CalcAnnum = mcAnnual.CalculatedAnnum

            '*******************************************

            ._C2Sign = ReturnSign(cbxMc2)
            ._C2Units = mc2.Units
            ._C2Multiplier = mc2.Multiplier
            ._C2Rate = mc2.Rate
            ._C2Per = mc2.Per
            ._C2Weeks = mc2.Weeks

            ._C2CalcDay = mc2.CalculatedDay
            ._C2CalcWeek = mc2.CalculatedWeek
            ._C2CalcMonth = mc2.CalculatedMonth
            ._C2CalcAnnum = mc2.CalculatedAnnum

            '*******************************************

            ._C3Sign = ReturnSign(cbxMc3)
            ._C3Units = mc3.Units
            ._C3Multiplier = mc3.Multiplier
            ._C3Rate = mc3.Rate
            ._C3Per = mc3.Per
            ._C3Weeks = mc3.Weeks

            ._C3CalcDay = mc3.CalculatedDay
            ._C3CalcWeek = mc3.CalculatedWeek
            ._C3CalcMonth = mc3.CalculatedMonth
            ._C3CalcAnnum = mc3.CalculatedAnnum

            '*******************************************

            ._C4Sign = ReturnSign(cbxMc4)
            ._C4Units = mc4.Units
            ._C4Multiplier = mc4.Multiplier
            ._C4Rate = mc4.Rate
            ._C4Per = mc4.Per
            ._C4Weeks = mc4.Weeks

            ._C4CalcDay = mc4.CalculatedDay
            ._C4CalcWeek = mc4.CalculatedWeek
            ._C4CalcMonth = mc4.CalculatedMonth
            ._C4CalcAnnum = mc4.CalculatedAnnum

            '*******************************************

            ._C5Sign = ReturnSign(cbxMc5)
            ._C5Units = mc5.Units
            ._C5Multiplier = mc5.Multiplier
            ._C5Rate = mc5.Rate
            ._C5Per = mc5.Per
            ._C5Weeks = mc5.Weeks

            ._C5CalcDay = mc5.CalculatedDay
            ._C5CalcWeek = mc5.CalculatedWeek
            ._C5CalcMonth = mc5.CalculatedMonth
            ._C5CalcAnnum = mc5.CalculatedAnnum

            '*******************************************

            ._FundUnits = mcFunded.Units
            ._FundMultiplier = mcFunded.Multiplier
            ._FundRate = mcFunded.Rate
            ._FundPer = mcFunded.Per
            ._FundWeeks = mcFunded.Weeks

            ._FundCalcDay = mcFunded.CalculatedDay
            ._FundCalcWeek = mcFunded.CalculatedWeek
            ._FundCalcMonth = mcFunded.CalculatedMonth
            ._FundCalcAnnum = mcFunded.CalculatedAnnum

            '*******************************************

            .CalculateTotals(Invoicing.ReturnSplitInto(cbxInvoiceSplit.Text))
            .BuildInvoiceText(Invoicing.ReturnSplitInto(cbxInvoiceSplit.Text))

            m_CalculationText = _Calc._InvoiceText

            .Store()

        End With

        Return True

    End Function

    Private Function ReturnSign(ByVal CareComboIn As Care.Controls.CareComboBox) As String
        If CareComboIn.SelectedIndex = 1 Then Return "+"
        If CareComboIn.SelectedIndex = 2 Then Return "-"
        Return ""
    End Function

    Private Function ReturnIndexFromSign(ByVal Sign As String) As Integer
        If Sign = "+" Then Return 1
        If Sign = "-" Then Return 2
        Return 0
    End Function

    Private Sub CalculateScreenTotal()

        Dim _Annual As Decimal = mcAnnual.CalculatedAnnum
        Dim _SubAnnual As Decimal = _Annual

        _SubAnnual += FetchCalculated(cbxMc2.SelectedIndex, mc2.CalculatedAnnum)
        _SubAnnual += FetchCalculated(cbxMc3.SelectedIndex, mc3.CalculatedAnnum)
        _SubAnnual += FetchCalculated(cbxMc4.SelectedIndex, mc4.CalculatedAnnum)
        _SubAnnual += FetchCalculated(cbxMc5.SelectedIndex, mc5.CalculatedAnnum)

        Dim _SubMonthly As Decimal = _SubAnnual / 12
        Dim _SubWeekly As Decimal = _SubAnnual / 52
        Dim _SubSplit As Decimal = _SubAnnual / Invoicing.ReturnSplitInto(cbxInvoiceSplit.Text)

        txtSubTotalAnnum.Text = ValueHandler.MoneyAsText(_SubAnnual)
        txtSubTotalMonth.Text = ValueHandler.MoneyAsText(_SubMonthly)
        txtSubTotalWeek.Text = ValueHandler.MoneyAsText(_SubWeekly)
        txtSubTotalPerSplit.Text = ValueHandler.MoneyAsText(_SubSplit)

        Dim _FundedAnnual As Decimal = mcFunded.CalculatedAnnum

        Dim _TotalAnnual As Decimal = _SubAnnual - _FundedAnnual
        Dim _TotalMonthly As Decimal = (_SubAnnual - _FundedAnnual) / 12
        Dim _TotalWeekly As Decimal = (_SubAnnual - _FundedAnnual) / 52
        Dim _TotalSplit As Decimal = (_SubAnnual - _FundedAnnual) / Invoicing.ReturnSplitInto(cbxInvoiceSplit.Text)

        txtTotalAnnum.Text = ValueHandler.MoneyAsText(_TotalAnnual)
        txtTotalMonth.Text = ValueHandler.MoneyAsText(_TotalMonthly)
        txtTotalWeek.Text = ValueHandler.MoneyAsText(_TotalWeekly)
        txtTotalPerSplit.Text = ValueHandler.MoneyAsText(_TotalSplit)

    End Sub

    Private Function FetchCalculated(ByVal SelectedIndex As Integer, ByVal CalculatedIn As Decimal) As Decimal

        Dim _Return As Decimal = 0
        Select Case SelectedIndex

            'none
            Case 0
                'stub

                'add
            Case 1
                _Return = CalculatedIn

                'subtract
            Case 2
                _Return = CalculatedIn * -1

        End Select

        Return _Return

    End Function

    Private Function BuildLine(ByRef CalculationControl As ManualCalculation) As String
        Dim _Return As String = ""
        _Return = CalculationControl.Units.ToString + " " + CalculationControl.Multiplier + " x £" + ValueHandler.MoneyAsText(CalculationControl.Rate) + " per " + CalculationControl.Per + " x " + CalculationControl.Weeks.ToString + " weeks = £" + ValueHandler.MoneyAsText(CalculationControl.CalculatedAnnum) + " per annum"
        Return _Return
    End Function

    Private Sub ListChanged(ByRef ControlIn As Object)
        If TypeOf ControlIn Is Care.Controls.CareComboBox Then
            Dim _Combo As Care.Controls.CareComboBox = CType(ControlIn, Care.Controls.CareComboBox)
            If _Combo.Name = "cbxMc2" Then SetMCControl(mc2, _Combo.SelectedIndex)
            If _Combo.Name = "cbxMc3" Then SetMCControl(mc3, _Combo.SelectedIndex)
            If _Combo.Name = "cbxMc4" Then SetMCControl(mc4, _Combo.SelectedIndex)
            If _Combo.Name = "cbxMc5" Then SetMCControl(mc5, _Combo.SelectedIndex)
        End If
    End Sub

    Private Sub SetMCControl(ByRef MCIn As ManualCalculation, ByVal SelectedIndex As Integer)

        Select Case SelectedIndex

            Case 0
                MCIn.Clear()
                MCIn.Enabled = False
                MCIn.ResetBackColor()

            Case 1
                MCIn.Enabled = True
                MCIn.BackColor = Session.ChangeColour
                MCIn.CalcForeColor = Drawing.Color.Black

            Case 2
                MCIn.Enabled = True
                MCIn.BackColor = Session.ChangeColour
                MCIn.CalcForeColor = Drawing.Color.Red

        End Select

    End Sub

#Region "Control Events"

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        If Commit() Then
            Me.DialogResult = DialogResult.OK
            Me.Close()
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub List_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxMc2.SelectedIndexChanged, cbxMc3.SelectedIndexChanged, cbxMc4.SelectedIndexChanged, cbxMc5.SelectedIndexChanged
        ListChanged(sender)
    End Sub

    Private Sub mcAnnual_ManualCalculationValidated(sender As Object, e As EventArgs) Handles mcAnnual.ManualCalculationValidated
        CalculateScreenTotal()
    End Sub

    Private Sub mc2_Cleared(sender As Object, e As EventArgs) Handles mc2.Cleared
        cbxMc2.SelectedIndex = 0
    End Sub

    Private Sub mc2_ManualCalculationValidated(sender As Object, e As EventArgs) Handles mc2.ManualCalculationValidated
        CalculateScreenTotal()
    End Sub

    Private Sub mc3_Cleared(sender As Object, e As EventArgs) Handles mc3.Cleared
        cbxMc3.SelectedIndex = 0
    End Sub

    Private Sub mc3_ManualCalculationValidated(sender As Object, e As EventArgs) Handles mc3.ManualCalculationValidated
        CalculateScreenTotal()
    End Sub

    Private Sub mc4_Cleared(sender As Object, e As EventArgs) Handles mc4.Cleared
        cbxMc4.SelectedIndex = 0
    End Sub

    Private Sub mc4_ManualCalculationValidated(sender As Object, e As EventArgs) Handles mc4.ManualCalculationValidated
        CalculateScreenTotal()
    End Sub

    Private Sub mc5_Cleared(sender As Object, e As EventArgs) Handles mc5.Cleared
        cbxMc5.SelectedIndex = 0
    End Sub

    Private Sub mc5_ManualCalculationValidated(sender As Object, e As EventArgs) Handles mc5.ManualCalculationValidated
        CalculateScreenTotal()
    End Sub

    Private Sub mcFunded_ManualCalculationValidated(sender As Object, e As EventArgs) Handles mcFunded.ManualCalculationValidated
        CalculateScreenTotal()
    End Sub

    Private Sub cbxInvoiceSplit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxInvoiceSplit.SelectedIndexChanged
        CalculateScreenTotal()
    End Sub

#End Region

    Private Sub btnViewText_Click(sender As Object, e As EventArgs) Handles btnViewText.Click
        CareMessage(m_CalculationText, MessageBoxIcon.Information, "Calculation Text")
    End Sub
End Class