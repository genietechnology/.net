﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUtils
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Button1 = New Care.Controls.CareButton(Me.components)
        Me.Button2 = New Care.Controls.CareButton(Me.components)
        Me.CareButton1 = New Care.Controls.CareButton(Me.components)
        Me.CareButton3 = New Care.Controls.CareButton(Me.components)
        Me.CareButton4 = New Care.Controls.CareButton(Me.components)
        Me.CareButton5 = New Care.Controls.CareButton(Me.components)
        Me.CareButton2 = New Care.Controls.CareButton(Me.components)
        Me.CareButton6 = New Care.Controls.CareButton(Me.components)
        Me.CareButton7 = New Care.Controls.CareButton(Me.components)
        Me.CareButton8 = New Care.Controls.CareButton(Me.components)
        Me.CareButton9 = New Care.Controls.CareButton(Me.components)
        Me.CareButton10 = New Care.Controls.CareButton(Me.components)
        Me.CareButton11 = New Care.Controls.CareButton(Me.components)
        Me.CareButton12 = New Care.Controls.CareButton(Me.components)
        Me.CareButton13 = New Care.Controls.CareButton(Me.components)
        Me.CareButton14 = New Care.Controls.CareButton(Me.components)
        Me.CareButton15 = New Care.Controls.CareButton(Me.components)
        Me.CareButton16 = New Care.Controls.CareButton(Me.components)
        Me.CareButton17 = New Care.Controls.CareButton(Me.components)
        Me.CareButton18 = New Care.Controls.CareButton(Me.components)
        Me.CareButton19 = New Care.Controls.CareButton(Me.components)
        Me.CareButton20 = New Care.Controls.CareButton(Me.components)
        Me.CareButton21 = New Care.Controls.CareButton(Me.components)
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'Button1
        '
        Me.Button1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Button1.Appearance.Options.UseFont = True
        Me.Button1.Location = New System.Drawing.Point(12, 12)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(316, 23)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Fix Invoice Addresses"
        '
        'Button2
        '
        Me.Button2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Button2.Appearance.Options.UseFont = True
        Me.Button2.Location = New System.Drawing.Point(12, 41)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(316, 23)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "Fix Contact Addresses"
        '
        'CareButton1
        '
        Me.CareButton1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareButton1.Appearance.Options.UseFont = True
        Me.CareButton1.Location = New System.Drawing.Point(12, 70)
        Me.CareButton1.Name = "CareButton1"
        Me.CareButton1.Size = New System.Drawing.Size(316, 23)
        Me.CareButton1.TabIndex = 2
        Me.CareButton1.Text = "Fix Staff Addresses"
        '
        'CareButton3
        '
        Me.CareButton3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareButton3.Appearance.Options.UseFont = True
        Me.CareButton3.Location = New System.Drawing.Point(334, 41)
        Me.CareButton3.Name = "CareButton3"
        Me.CareButton3.Size = New System.Drawing.Size(316, 23)
        Me.CareButton3.TabIndex = 4
        Me.CareButton3.Text = "Add Drinks to Lunch"
        '
        'CareButton4
        '
        Me.CareButton4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareButton4.Appearance.Options.UseFont = True
        Me.CareButton4.Location = New System.Drawing.Point(334, 69)
        Me.CareButton4.Name = "CareButton4"
        Me.CareButton4.Size = New System.Drawing.Size(316, 23)
        Me.CareButton4.TabIndex = 5
        Me.CareButton4.Text = "Add Drinks to Tea"
        '
        'CareButton5
        '
        Me.CareButton5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareButton5.Appearance.Options.UseFont = True
        Me.CareButton5.Location = New System.Drawing.Point(12, 108)
        Me.CareButton5.Name = "CareButton5"
        Me.CareButton5.Size = New System.Drawing.Size(316, 23)
        Me.CareButton5.TabIndex = 6
        Me.CareButton5.Text = "Fix Toilet records"
        '
        'CareButton2
        '
        Me.CareButton2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareButton2.Appearance.Options.UseFont = True
        Me.CareButton2.Location = New System.Drawing.Point(334, 12)
        Me.CareButton2.Name = "CareButton2"
        Me.CareButton2.Size = New System.Drawing.Size(316, 23)
        Me.CareButton2.TabIndex = 7
        Me.CareButton2.Text = "Add Drinks to Snack"
        '
        'CareButton6
        '
        Me.CareButton6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareButton6.Appearance.Options.UseFont = True
        Me.CareButton6.Location = New System.Drawing.Point(12, 137)
        Me.CareButton6.Name = "CareButton6"
        Me.CareButton6.Size = New System.Drawing.Size(316, 23)
        Me.CareButton6.TabIndex = 8
        Me.CareButton6.Text = "Fix Child Sessions"
        '
        'CareButton7
        '
        Me.CareButton7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareButton7.Appearance.Options.UseFont = True
        Me.CareButton7.Location = New System.Drawing.Point(12, 166)
        Me.CareButton7.Name = "CareButton7"
        Me.CareButton7.Size = New System.Drawing.Size(316, 23)
        Me.CareButton7.TabIndex = 9
        Me.CareButton7.Text = "Import from External Database"
        '
        'CareButton8
        '
        Me.CareButton8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareButton8.Appearance.Options.UseFont = True
        Me.CareButton8.Location = New System.Drawing.Point(334, 108)
        Me.CareButton8.Name = "CareButton8"
        Me.CareButton8.Size = New System.Drawing.Size(316, 23)
        Me.CareButton8.TabIndex = 10
        Me.CareButton8.Text = "Rebuild Annual Calculation Text"
        '
        'CareButton9
        '
        Me.CareButton9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareButton9.Appearance.Options.UseFont = True
        Me.CareButton9.Location = New System.Drawing.Point(334, 137)
        Me.CareButton9.Name = "CareButton9"
        Me.CareButton9.Size = New System.Drawing.Size(316, 23)
        Me.CareButton9.TabIndex = 11
        Me.CareButton9.Text = "Setup DOBs so they match rooms"
        '
        'CareButton10
        '
        Me.CareButton10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareButton10.Appearance.Options.UseFont = True
        Me.CareButton10.Location = New System.Drawing.Point(12, 195)
        Me.CareButton10.Name = "CareButton10"
        Me.CareButton10.Size = New System.Drawing.Size(316, 23)
        Me.CareButton10.TabIndex = 12
        Me.CareButton10.Text = "Start of Day"
        '
        'CareButton11
        '
        Me.CareButton11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareButton11.Appearance.Options.UseFont = True
        Me.CareButton11.Location = New System.Drawing.Point(334, 166)
        Me.CareButton11.Name = "CareButton11"
        Me.CareButton11.Size = New System.Drawing.Size(316, 23)
        Me.CareButton11.TabIndex = 13
        Me.CareButton11.Text = "Create Register Summary from Register Detail"
        '
        'CareButton12
        '
        Me.CareButton12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareButton12.Appearance.Options.UseFont = True
        Me.CareButton12.Location = New System.Drawing.Point(334, 195)
        Me.CareButton12.Name = "CareButton12"
        Me.CareButton12.Size = New System.Drawing.Size(316, 23)
        Me.CareButton12.TabIndex = 14
        Me.CareButton12.Text = "Migrate Child Consent Options"
        '
        'CareButton13
        '
        Me.CareButton13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareButton13.Appearance.Options.UseFont = True
        Me.CareButton13.Location = New System.Drawing.Point(12, 234)
        Me.CareButton13.Name = "CareButton13"
        Me.CareButton13.Size = New System.Drawing.Size(316, 23)
        Me.CareButton13.TabIndex = 15
        Me.CareButton13.Text = "Fix Additional Calculation Flag on all Invoices"
        '
        'CareButton14
        '
        Me.CareButton14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareButton14.Appearance.Options.UseFont = True
        Me.CareButton14.Location = New System.Drawing.Point(12, 263)
        Me.CareButton14.Name = "CareButton14"
        Me.CareButton14.Size = New System.Drawing.Size(316, 23)
        Me.CareButton14.TabIndex = 16
        Me.CareButton14.Text = "Change Group ID on Activity Text Table"
        '
        'CareButton15
        '
        Me.CareButton15.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareButton15.Appearance.Options.UseFont = True
        Me.CareButton15.Location = New System.Drawing.Point(334, 234)
        Me.CareButton15.Name = "CareButton15"
        Me.CareButton15.Size = New System.Drawing.Size(316, 23)
        Me.CareButton15.TabIndex = 17
        Me.CareButton15.Text = "Division by Zero Error"
        '
        'CareButton16
        '
        Me.CareButton16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareButton16.Appearance.Options.UseFont = True
        Me.CareButton16.Location = New System.Drawing.Point(12, 350)
        Me.CareButton16.Name = "CareButton16"
        Me.CareButton16.Size = New System.Drawing.Size(638, 23)
        Me.CareButton16.TabIndex = 18
        Me.CareButton16.Text = "Generate Annual Calculations from April 2018 to March 2019, breaking on Birthdays" &
    " (Capellas)"
        '
        'CareButton17
        '
        Me.CareButton17.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareButton17.Appearance.Options.UseFont = True
        Me.CareButton17.Location = New System.Drawing.Point(12, 292)
        Me.CareButton17.Name = "CareButton17"
        Me.CareButton17.Size = New System.Drawing.Size(316, 23)
        Me.CareButton17.TabIndex = 19
        Me.CareButton17.Text = "Increase Fees by x%"
        '
        'CareButton18
        '
        Me.CareButton18.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareButton18.Appearance.Options.UseFont = True
        Me.CareButton18.Location = New System.Drawing.Point(12, 321)
        Me.CareButton18.Name = "CareButton18"
        Me.CareButton18.Size = New System.Drawing.Size(316, 23)
        Me.CareButton18.TabIndex = 20
        Me.CareButton18.Text = "Tick all Consent"
        '
        'CareButton19
        '
        Me.CareButton19.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareButton19.Appearance.Options.UseFont = True
        Me.CareButton19.Location = New System.Drawing.Point(334, 263)
        Me.CareButton19.Name = "CareButton19"
        Me.CareButton19.Size = New System.Drawing.Size(316, 23)
        Me.CareButton19.TabIndex = 21
        Me.CareButton19.Text = "Create Monthly Matrix Overrides from Recurring Patterns"
        '
        'CareButton20
        '
        Me.CareButton20.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareButton20.Appearance.Options.UseFont = True
        Me.CareButton20.Location = New System.Drawing.Point(334, 292)
        Me.CareButton20.Name = "CareButton20"
        Me.CareButton20.Size = New System.Drawing.Size(316, 23)
        Me.CareButton20.TabIndex = 22
        Me.CareButton20.Text = "Get Financials Balances"
        '
        'CareButton21
        '
        Me.CareButton21.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareButton21.Appearance.Options.UseFont = True
        Me.CareButton21.Location = New System.Drawing.Point(12, 379)
        Me.CareButton21.Name = "CareButton21"
        Me.CareButton21.Size = New System.Drawing.Size(316, 23)
        Me.CareButton21.TabIndex = 23
        Me.CareButton21.Text = "Create Thumbnails from Photos"
        '
        'frmUtils
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(665, 436)
        Me.Controls.Add(Me.CareButton21)
        Me.Controls.Add(Me.CareButton20)
        Me.Controls.Add(Me.CareButton19)
        Me.Controls.Add(Me.CareButton18)
        Me.Controls.Add(Me.CareButton17)
        Me.Controls.Add(Me.CareButton16)
        Me.Controls.Add(Me.CareButton15)
        Me.Controls.Add(Me.CareButton14)
        Me.Controls.Add(Me.CareButton13)
        Me.Controls.Add(Me.CareButton12)
        Me.Controls.Add(Me.CareButton11)
        Me.Controls.Add(Me.CareButton10)
        Me.Controls.Add(Me.CareButton9)
        Me.Controls.Add(Me.CareButton8)
        Me.Controls.Add(Me.CareButton7)
        Me.Controls.Add(Me.CareButton6)
        Me.Controls.Add(Me.CareButton2)
        Me.Controls.Add(Me.CareButton5)
        Me.Controls.Add(Me.CareButton4)
        Me.Controls.Add(Me.CareButton3)
        Me.Controls.Add(Me.CareButton1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Name = "frmUtils"
        Me.Text = "frmUtils"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button1 As Care.Controls.CareButton
    Friend WithEvents Button2 As Care.Controls.CareButton
    Friend WithEvents CareButton1 As Care.Controls.CareButton
    Friend WithEvents CareButton3 As Care.Controls.CareButton
    Friend WithEvents CareButton4 As Care.Controls.CareButton
    Friend WithEvents CareButton5 As Care.Controls.CareButton
    Friend WithEvents CareButton2 As Care.Controls.CareButton
    Friend WithEvents CareButton6 As Care.Controls.CareButton
    Friend WithEvents CareButton7 As Care.Controls.CareButton
    Friend WithEvents CareButton8 As Care.Controls.CareButton
    Friend WithEvents CareButton9 As Care.Controls.CareButton
    Friend WithEvents CareButton10 As Care.Controls.CareButton
    Friend WithEvents CareButton11 As Care.Controls.CareButton
    Friend WithEvents CareButton12 As Care.Controls.CareButton
    Friend WithEvents CareButton13 As Care.Controls.CareButton
    Friend WithEvents CareButton14 As Care.Controls.CareButton
    Friend WithEvents CareButton15 As Care.Controls.CareButton
    Friend WithEvents CareButton16 As Care.Controls.CareButton
    Friend WithEvents CareButton17 As Care.Controls.CareButton
    Friend WithEvents CareButton18 As Care.Controls.CareButton
    Friend WithEvents CareButton19 As Care.Controls.CareButton
    Friend WithEvents CareButton20 As Care.Controls.CareButton
    Friend WithEvents CareButton21 As Care.Controls.CareButton
End Class
