﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChildMonthWeek
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxWeekly = New Care.Controls.CareFrame()
        Me.cbxTariff = New Care.Controls.CareComboBox()
        Me.cbxDays = New Care.Controls.CareComboBox()
        Me.CareLabel1 = New Care.Controls.CareLabel()
        Me.cbxAgeBand = New Care.Controls.CareComboBox()
        Me.cbxMode = New Care.Controls.CareComboBox()
        Me.CareLabel14 = New Care.Controls.CareLabel()
        Me.CareLabel11 = New Care.Controls.CareLabel()
        Me.cdtStartDate = New Care.Controls.CareDateTime()
        Me.btnOK = New Care.Controls.CareButton()
        Me.btnCancel = New Care.Controls.CareButton()
        CType(Me.gbxWeekly, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxWeekly.SuspendLayout()
        CType(Me.cbxTariff.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxDays.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxAgeBand.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxMode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtStartDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtStartDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'gbxWeekly
        '
        Me.gbxWeekly.Controls.Add(Me.cbxTariff)
        Me.gbxWeekly.Controls.Add(Me.cbxDays)
        Me.gbxWeekly.Controls.Add(Me.CareLabel1)
        Me.gbxWeekly.Controls.Add(Me.cbxAgeBand)
        Me.gbxWeekly.Controls.Add(Me.cbxMode)
        Me.gbxWeekly.Controls.Add(Me.CareLabel14)
        Me.gbxWeekly.Controls.Add(Me.CareLabel11)
        Me.gbxWeekly.Controls.Add(Me.cdtStartDate)
        Me.gbxWeekly.Location = New System.Drawing.Point(12, 12)
        Me.gbxWeekly.Name = "gbxWeekly"
        Me.gbxWeekly.Size = New System.Drawing.Size(486, 112)
        Me.gbxWeekly.TabIndex = 0
        Me.gbxWeekly.Text = "Override Details"
        '
        'cbxTariff
        '
        Me.cbxTariff.AllowBlank = False
        Me.cbxTariff.DataSource = Nothing
        Me.cbxTariff.DisplayMember = Nothing
        Me.cbxTariff.EnterMoveNextControl = True
        Me.cbxTariff.Location = New System.Drawing.Point(230, 55)
        Me.cbxTariff.Name = "cbxTariff"
        Me.cbxTariff.Properties.AccessibleName = "Group"
        Me.cbxTariff.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxTariff.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxTariff.Properties.Appearance.Options.UseFont = True
        Me.cbxTariff.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxTariff.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxTariff.SelectedValue = Nothing
        Me.cbxTariff.Size = New System.Drawing.Size(251, 22)
        Me.cbxTariff.TabIndex = 4
        Me.cbxTariff.Tag = ""
        Me.cbxTariff.ValueMember = Nothing
        '
        'cbxDays
        '
        Me.cbxDays.AllowBlank = False
        Me.cbxDays.DataSource = Nothing
        Me.cbxDays.DisplayMember = Nothing
        Me.cbxDays.EnterMoveNextControl = True
        Me.cbxDays.Location = New System.Drawing.Point(124, 55)
        Me.cbxDays.Name = "cbxDays"
        Me.cbxDays.Properties.AccessibleName = "Group"
        Me.cbxDays.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxDays.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxDays.Properties.Appearance.Options.UseFont = True
        Me.cbxDays.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxDays.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxDays.SelectedValue = Nothing
        Me.cbxDays.Size = New System.Drawing.Size(100, 22)
        Me.cbxDays.TabIndex = 3
        Me.cbxDays.Tag = ""
        Me.cbxDays.ValueMember = Nothing
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(14, 58)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(77, 15)
        Me.CareLabel1.TabIndex = 2
        Me.CareLabel1.Text = "Days per Week"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxAgeBand
        '
        Me.cbxAgeBand.AllowBlank = False
        Me.cbxAgeBand.DataSource = Nothing
        Me.cbxAgeBand.DisplayMember = Nothing
        Me.cbxAgeBand.EnterMoveNextControl = True
        Me.cbxAgeBand.Location = New System.Drawing.Point(230, 81)
        Me.cbxAgeBand.Name = "cbxAgeBand"
        Me.cbxAgeBand.Properties.AccessibleName = "Group"
        Me.cbxAgeBand.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxAgeBand.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxAgeBand.Properties.Appearance.Options.UseFont = True
        Me.cbxAgeBand.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxAgeBand.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxAgeBand.SelectedValue = Nothing
        Me.cbxAgeBand.Size = New System.Drawing.Size(251, 22)
        Me.cbxAgeBand.TabIndex = 7
        Me.cbxAgeBand.Tag = ""
        Me.cbxAgeBand.ValueMember = Nothing
        '
        'cbxMode
        '
        Me.cbxMode.AllowBlank = False
        Me.cbxMode.DataSource = Nothing
        Me.cbxMode.DisplayMember = Nothing
        Me.cbxMode.EnterMoveNextControl = True
        Me.cbxMode.Location = New System.Drawing.Point(124, 81)
        Me.cbxMode.Name = "cbxMode"
        Me.cbxMode.Properties.AccessibleName = "Group"
        Me.cbxMode.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxMode.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxMode.Properties.Appearance.Options.UseFont = True
        Me.cbxMode.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxMode.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxMode.SelectedValue = Nothing
        Me.cbxMode.Size = New System.Drawing.Size(100, 22)
        Me.cbxMode.TabIndex = 6
        Me.cbxMode.Tag = ""
        Me.cbxMode.ValueMember = Nothing
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(14, 84)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(85, 15)
        Me.CareLabel14.TabIndex = 5
        Me.CareLabel14.Text = "Age Band Mode"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(14, 32)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(64, 15)
        Me.CareLabel11.TabIndex = 0
        Me.CareLabel11.Text = "Active From"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtStartDate
        '
        Me.cdtStartDate.EditValue = Nothing
        Me.cdtStartDate.EnterMoveNextControl = True
        Me.cdtStartDate.Location = New System.Drawing.Point(124, 29)
        Me.cdtStartDate.Name = "cdtStartDate"
        Me.cdtStartDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtStartDate.Properties.Appearance.Options.UseFont = True
        Me.cdtStartDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtStartDate.Size = New System.Drawing.Size(100, 22)
        Me.cdtStartDate.TabIndex = 1
        Me.cdtStartDate.Tag = ""
        Me.cdtStartDate.Value = Nothing
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(322, 131)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(85, 25)
        Me.btnOK.TabIndex = 1
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.CausesValidation = False
        Me.btnCancel.DialogResult = DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(413, 131)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 25)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "Cancel"
        '
        'frmChildMonthWeek
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(510, 164)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.gbxWeekly)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmChildMonthWeek"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = ""
        CType(Me.gbxWeekly, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxWeekly.ResumeLayout(False)
        Me.gbxWeekly.PerformLayout()
        CType(Me.cbxTariff.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxDays.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxAgeBand.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxMode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtStartDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtStartDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxWeekly As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents cdtStartDate As Care.Controls.CareDateTime
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents cbxMode As Care.Controls.CareComboBox
    Friend WithEvents cbxAgeBand As Care.Controls.CareComboBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents cbxDays As Care.Controls.CareComboBox
    Friend WithEvents cbxTariff As Care.Controls.CareComboBox

End Class
