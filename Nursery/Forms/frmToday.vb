﻿

Imports Care.Global
Imports Care.Data
Imports Care.Shared
Imports System.Windows.Forms

Public Class frmToday

    Private m_ID As Guid? = Nothing
    Private m_SiteID As Guid? = Nothing
    Private m_ActivityID As String

    Private m_Date As Date?
    Private m_WeekStarts As Date
    Private m_WeekEnds As Date
    Private m_SQLThisWeek As String = ""
    Private WithEvents m_StartOfDay As New Business.StartOfDay

    Private Sub frmToday_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ToggleTiles(False)
    End Sub

    Private Sub frmToday_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        Application.DoEvents()

        Session.WaitFormShow()
        StartupProcess()
        Session.WaitFormClose()

    End Sub

    Private Sub ToggleTiles(ByVal Visible As Boolean)

        'right side, per row - top down
        dshChildrenExpected.Visible = Visible
        dshChildrenIn.Visible = Visible
        dshStaffExpected.Visible = Visible
        dshStaffIn.Visible = Visible
        dshAbsences.Visible = Visible
        dshPayments.Visible = Visible
        dshAccidents.Visible = Visible
        dshPrior.Visible = Visible

        'bottom row, left to right
        dshStarters.Visible = Visible
        dshLeavers.Visible = Visible
        dshBirthdays.Visible = Visible
        dshViewings.Visible = Visible
        dshChecklists.Visible = Visible
        dshTasks.Visible = Visible

        If Visible Then
            tlpMain.ColumnCount = 6
            tlpMain.RowCount = 5
        Else
            tlpMain.ColumnCount = 4
            tlpMain.RowCount = 4
        End If

    End Sub

    Private Sub CheckVersion()

        If ParameterHandler.CheckExists("LASTVERSION") Then

            Dim _Last As String = ParameterHandler.ReturnString("LASTVERSION")

            If _Last = "" Then
                LaunchPostUpgrade(Nothing)
                ParameterHandler.SetString("LASTVERSION", My.Application.Info.Version.ToString)
            Else

                Dim _LastVersion As New Version(_Last)
                Dim _Current As Version = My.Application.Info.Version

                If My.Application.Info.Version > New Version(_Last) Then
                    LaunchPostUpgrade(_LastVersion)
                    ParameterHandler.SetString("LASTVERSION", My.Application.Info.Version.ToString)
                End If

            End If

        Else
            LaunchPostUpgrade(Nothing)
            ParameterHandler.SetString("LASTVERSION", My.Application.Info.Version.ToString)
        End If

    End Sub

    Private Sub LaunchPostUpgrade(ByVal LastVersion As Version)

        Dim _frm As New frmPostUpgrade(LastVersion)
        _frm.ShowDialog()

        _frm.Dispose()

    End Sub

    Private Sub StartupProcess()

        Session.SetProgressMessage("Checking Version...")
        CheckVersion()

        Session.SetProgressMessage("Processing Start of Day...")
        m_StartOfDay.Run()

        Session.SetProgressMessage("Generating Account Numbers...")
        GenerateAccountNumbers()

        Session.SetProgressMessage("Display Statistics...")
        DisplayStats()

        Session.SetProgressMessage("Setup Timer...")

        timRefresh.Interval = 10 * 1000
        timRefresh.Start()

        Session.SetProgressMessage("Startup Process Complete.", 3)

    End Sub

#Region "Overrides"

    Protected Overrides Sub SetBindings()
        'stub
    End Sub

    Protected Overrides Sub AfterEdit()
        m_ActivityID = grdGroups.CurrentRow("ID").ToString
        grdGroups.Enabled = False
        timRefresh.Stop()
    End Sub

    Protected Overrides Function BeforeDelete() As Boolean
        CareMessage("You do not have permission to Delete a Day record.", MessageBoxIcon.Exclamation, "Delete Record")
        Return False
    End Function

    Protected Overrides Function BeforeCommitUpdate() As Boolean

        If Me.Mode = "EDIT" Then

            Dim _Day As Business.Day = Business.Day.RetreiveByID(m_ID.Value)

            If _Day._BreakfastName <> "" AndAlso _Day._BreakfastName <> txtBreakfast.Text Then
                If MealPrompt("Breakfast") Then
                    DeleteMealActivity(_Day._BreakfastId.Value, "B")
                Else
                    Return False
                End If
            End If

            If _Day._SnackName <> "" AndAlso _Day._SnackName <> txtSnack.Text Then
                If MealPrompt("Morning Snack") Then
                    DeleteMealActivity(_Day._SnackId.Value, "S")
                Else
                    Return False
                End If
            End If

            If _Day._LunchName <> "" AndAlso _Day._LunchName <> txtLunch.Text Then
                If MealPrompt("Lunch") Then
                    DeleteMealActivity(_Day._LunchId.Value, "L")
                Else
                    Return False
                End If
            End If

            If _Day._LunchDesName <> "" AndAlso _Day._LunchDesName <> txtLunchDessert.Text Then
                If MealPrompt("Lunch Dessert") Then
                    DeleteMealActivity(_Day._LunchDesId.Value, "LD")
                Else
                    Return False
                End If
            End If

            If _Day._SnackPmName <> "" AndAlso _Day._SnackPmName <> txtSnackPM.Text Then
                If MealPrompt("Afternoon Snack") Then
                    DeleteMealActivity(_Day._SnackPmId.Value, "SP")
                Else
                    Return False
                End If
            End If

            If _Day._TeaName <> "" AndAlso _Day._TeaName <> txtTea.Text Then
                If MealPrompt("Tea") Then
                    DeleteMealActivity(_Day._TeaId.Value, "T")
                Else
                    Return False
                End If
            End If

            If _Day._TeaDesName <> "" AndAlso _Day._TeaDesName <> txtTeaDessert.Text Then
                If MealPrompt("Tea Dessert") Then
                    DeleteMealActivity(_Day._TeaDesId.Value, "TD")
                Else
                    Return False
                End If
            End If

        End If

        Return True

    End Function

    Private Function MealPrompt(ByVal MealName As String) As Boolean
        Dim _Mess As String = "You have changed the " + MealName + ". This will delete any data captured for this Meal." + vbCrLf + vbCrLf + "Are you sure you wish to Continue?"
        If CareMessage(_Mess, MessageBoxIcon.Warning, MessageBoxButtons.YesNo, "Meal Changed") = DialogResult.Yes Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub DeleteMealActivity(ByVal MealID As Guid, ByVal MealType As String)

        Dim _SQL As String = ""
        _SQL += "delete from FoodRegister"
        _SQL += " where day_id = '" + m_ID.ToString + "'"
        _SQL += " and meal_id = '" + MealID.ToString + "'"
        _SQL += " and meal_type = '" + MealType + "'"

        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

    End Sub

    Protected Overrides Function BeforeAdd() As Boolean

        Dim _frm As New frmTodayNew
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then

            m_ID = New Guid(_frm.DayID)
            DisplayRecord()
            DisplayStats()

            _frm.Dispose()
            _frm = Nothing

            Session.CursorDefault()
            ToolEdit()

        End If

        Return False

    End Function

    Protected Overrides Sub FindRecord()

        Dim _ID As String = Business.Day.FindDay(Me)
        If _ID <> "" Then

            Me.RecordPopulated = True
            Me.ToolbarMode = ToolbarEnum.Standard

            m_ID = New Guid(_ID)
            Me.RecordID = m_ID

            DisplayRecord()
            DisplayStats()

        End If

    End Sub

    Protected Overrides Sub CommitUpdate()

        Dim _Day As Business.Day = Business.Day.RetreiveByID(m_ID.Value)

        If txtBreakfast.AccessibleName <> "" Then
            _Day._BreakfastId = New Guid(txtBreakfast.AccessibleName)
            _Day._BreakfastName = txtBreakfast.Text
        Else
            _Day._BreakfastId = Nothing
            _Day._BreakfastName = ""
        End If

        If txtSnack.AccessibleName <> "" Then
            _Day._SnackId = New Guid(txtSnack.AccessibleName)
            _Day._SnackName = txtSnack.Text
        Else
            _Day._SnackId = Nothing
            _Day._SnackName = ""
        End If

        If txtLunch.AccessibleName <> "" Then
            _Day._LunchId = New Guid(txtLunch.AccessibleName)
            _Day._LunchName = txtLunch.Text
        Else
            _Day._LunchId = Nothing
            _Day._LunchName = ""
        End If

        If txtLunchDessert.AccessibleName <> "" Then
            _Day._LunchDesId = New Guid(txtLunchDessert.AccessibleName)
            _Day._LunchDesName = txtLunchDessert.Text
        Else
            _Day._LunchDesId = Nothing
            _Day._LunchDesName = ""
        End If

        If txtSnackPM.AccessibleName <> "" Then
            _Day._SnackPmId = New Guid(txtSnackPM.AccessibleName)
            _Day._SnackPmName = txtSnackPM.Text
        Else
            _Day._SnackPmId = Nothing
            _Day._SnackPmName = ""
        End If

        If txtTea.AccessibleName <> "" Then
            _Day._TeaId = New Guid(txtTea.AccessibleName)
            _Day._TeaName = txtTea.Text
        Else
            _Day._TeaId = Nothing
            _Day._TeaName = ""
        End If

        If txtTeaDessert.AccessibleName <> "" Then
            _Day._TeaDesId = New Guid(txtTeaDessert.AccessibleName)
            _Day._TeaDesName = txtTeaDessert.Text
        Else
            _Day._TeaDesId = Nothing
            _Day._TeaDesName = ""
        End If

        _Day._Message = txtNarr.Text

        _Day.Store()

        CommitActivity()

        _Day = Nothing

    End Sub

    Protected Overrides Sub AfterAcceptChanges()
        ToolbarMode = ToolbarEnum.Standard
        grdGroups.Enabled = True
        RefreshGrid()
        timRefresh.Start()
    End Sub

    Protected Overrides Sub AfterCancelChanges()
        ToolbarMode = ToolbarEnum.Standard
        grdGroups.Enabled = True
        timRefresh.Start()
    End Sub

#End Region

    Private Sub GenerateAccountNumbers()
        Dim _Families As List(Of Business.Family) = Business.Family.RetrieveFamiliesWithoutAccountNos
        If _Families.Count > 0 Then
            Session.SetupProgressBar("Generating Account Numbers...", _Families.Count)
            For Each _F As Business.Family In _Families
                _F.Store()
                Session.StepProgressBar()
            Next
            Session.SetProgressMessage("Account Numbers Generated OK.", 3)
        End If
    End Sub

    Private Sub DisplayRecord()

        Dim _Day As Business.Day = Business.Day.RetreiveByID(m_ID.Value)
        If _Day IsNot Nothing Then

            m_SiteID = _Day._SiteId
            txtSite.Text = _Day._SiteName

            If _Day._Date.HasValue Then

                txtDate.AccessibleName = _Day._Date.ToString
                txtDate.Text = Format(_Day._Date, "dddd d MMMM yyyy")

                m_Date = _Day._Date.Value
                m_WeekStarts = ValueHandler.NearestDate(_Day._Date.Value, DayOfWeek.Monday, ValueHandler.EnumDirection.Backwards)
                m_WeekEnds = m_WeekStarts.AddDays(6)
                m_SQLThisWeek = "'" + ValueHandler.SQLDate(m_WeekStarts) + "' and '" + ValueHandler.SQLDate(m_WeekEnds) + "'"

            Else
                txtDate.Text = ""
            End If

            If _Day._BreakfastId.HasValue Then
                txtBreakfast.AccessibleName = _Day._BreakfastId.Value.ToString
                txtBreakfast.Text = _Day._BreakfastName
            Else
                txtBreakfast.AccessibleName = ""
                txtBreakfast.Text = ""
            End If

            If _Day._SnackId.HasValue Then
                txtSnack.AccessibleName = _Day._SnackId.Value.ToString
                txtSnack.Text = _Day._SnackName
            Else
                txtSnack.AccessibleName = ""
                txtSnack.Text = ""
            End If

            If _Day._LunchId.HasValue Then
                txtLunch.AccessibleName = _Day._LunchId.Value.ToString
                txtLunch.Text = _Day._LunchName
            Else
                txtLunch.AccessibleName = ""
                txtLunch.Text = ""
            End If

            If _Day._LunchDesId.HasValue Then
                txtLunchDessert.AccessibleName = _Day._LunchDesId.Value.ToString
                txtLunchDessert.Text = _Day._LunchDesName
            Else
                txtLunchDessert.AccessibleName = ""
                txtLunchDessert.Text = ""
            End If

            If _Day._SnackPmId.HasValue Then
                txtSnackPM.AccessibleName = _Day._SnackPmId.Value.ToString
                txtSnackPM.Text = _Day._SnackPmName
            Else
                txtSnackPM.AccessibleName = ""
                txtSnackPM.Text = ""
            End If

            If _Day._TeaId.HasValue Then
                txtTea.AccessibleName = _Day._TeaId.Value.ToString
                txtTea.Text = _Day._TeaName
            Else
                txtTea.AccessibleName = ""
                txtTea.Text = ""
            End If

            If _Day._TeaDesId.HasValue Then
                txtTeaDessert.AccessibleName = _Day._TeaDesId.Value.ToString
                txtTeaDessert.Text = _Day._TeaDesName
            Else
                txtTeaDessert.AccessibleName = ""
                txtTeaDessert.Text = ""
            End If

            txtNarr.Text = _Day._Message

            RefreshGrid()

            _Day = Nothing

        End If

    End Sub

    Private Sub RefreshGrid()

        If Not m_ID.HasValue Then Exit Sub

        Dim _SQL As String = ""
        _SQL += "select a.ID, r.room_name as 'Room', a.activity_text from DayActivity a"
        _SQL += " left join SiteRooms r on r.ID = a.group_id"
        _SQL += " where a.day_id = '" + m_ID.ToString + "'"
        _SQL += " order by r.room_sequence"

        grdGroups.Populate(Session.ConnectionString, _SQL)

        grdGroups.Columns("ID").Visible = False
        grdGroups.Columns("activity_text").Visible = False

        grdGroups.Enabled = True

        grdGroups.MoveFirst()
        DisplayActivityText()

    End Sub

    Private Sub DisplayActivityText()
        If grdGroups.CurrentRow IsNot Nothing Then
            gbxActivity.Text = "Activities for " + grdGroups.CurrentRow("Room").ToString
            txtActivity.Text = grdGroups.CurrentRow("activity_text").ToString
        End If
    End Sub

    Private Sub CommitActivity()

        If Me.Mode = "ADD" Then Exit Sub

        grdGroups.Enabled = True

        Dim _ID As Guid = Nothing
        Dim _DayAct As Business.DayActivity = Business.DayActivity.RetreiveByID(New Guid(m_ActivityID))
        If _DayAct IsNot Nothing Then
            _DayAct._ActivityText = txtActivity.Text
            _DayAct.Store()
            _DayAct = Nothing
        Else
            CareMessage("DayActivity record not found!", MessageBoxIcon.Exclamation, "CommitActivity")
        End If

    End Sub

    Private Sub DisplayStats()

        If Not m_Date.HasValue Then Exit Sub
        If Not m_SiteID.HasValue Then Exit Sub

        ToggleTiles(True)

        Dim _SQL As String = ""
        Dim _Colour As Care.Controls.DashboardLabel.EnumColourCode

        'children expected
        '*********************************************************************************************************************************

        _SQL = ""
        _SQL += "select count(*) from Bookings b"
        _SQL += " left join Children c on c.ID = b.child_id"
        _SQL += " left join Day d on d.date = b.booking_date and d.site_id = b.site_id"
        _SQL += " where booking_date = " + ValueHandler.SQLDate(m_Date.Value, True)
        _SQL += " and c.site_id = '" + m_SiteID.ToString + "'"
        _SQL += " and b.booking_status <> 'Holiday'"
        _SQL += " and b.child_id not in (select act.key_id from Activity act where act.day_id = d.ID and act.key_id = b.child_id and act.type = 'ABSENCE')"
        _SQL += " and b.child_id not in (select rs.person_id from RegisterSummary rs where rs.date = b.booking_date and rs.person_id = b.child_id)"

        Dim _ChildrenDue As Decimal = ReturnSQLCount(_SQL)
        _Colour = Care.Controls.DashboardLabel.EnumColourCode.Red
        If _ChildrenDue > 0 Then _Colour = Care.Controls.DashboardLabel.EnumColourCode.Green
        dshChildrenExpected.SetValue(_ChildrenDue, Care.Controls.DashboardLabel.EnumMiddleFormat.WholeNumber, _Colour)

        'children in
        '*********************************************************************************************************************************
        Dim _ChildrenIn As Decimal = ReturnRegisterCount("C")
        _Colour = Care.Controls.DashboardLabel.EnumColourCode.Red
        If _ChildrenIn > 0 Then _Colour = Care.Controls.DashboardLabel.EnumColourCode.Green
        dshChildrenIn.SetValue(_ChildrenIn, Care.Controls.DashboardLabel.EnumMiddleFormat.WholeNumber, _Colour)

        'staff expected
        '*********************************************************************************************************************************

        _SQL = ""
        _SQL += "select count(*) from RotaStaff"
        _SQL += " left Join Rotas on Rotas.ID = RotaStaff.rs_rota_id"
        _SQL += " where rs_date = " + ValueHandler.SQLDate(m_Date.Value, True)
        _SQL += " and rs_status = 'Committed'"
        _SQL += " and site_id = '" + m_SiteID.ToString + "'"

        Dim _StaffExpected As Decimal = ReturnSQLCount(_SQL)
        _Colour = Care.Controls.DashboardLabel.EnumColourCode.Red
        If _StaffExpected > 0 Then _Colour = Care.Controls.DashboardLabel.EnumColourCode.Green
        dshStaffExpected.SetValue(_StaffExpected, Care.Controls.DashboardLabel.EnumMiddleFormat.WholeNumber, _Colour)

        'staff in
        '*********************************************************************************************************************************
        Dim _StaffIn As Decimal = ReturnRegisterCount("S")
        _Colour = Care.Controls.DashboardLabel.EnumColourCode.Red
        If _StaffIn > 0 Then _Colour = Care.Controls.DashboardLabel.EnumColourCode.Green
        dshStaffIn.SetValue(_StaffIn, Care.Controls.DashboardLabel.EnumMiddleFormat.WholeNumber, _Colour)

        'absences
        '*********************************************************************************************************************************

        _SQL = ""
        _SQL += "select count(*) from Activity a"
        _SQL += " left join Day d on d.ID = a.day_id"
        _SQL += " where d.date = " + ValueHandler.SQLDate(m_Date.Value, True)
        _SQL += " and a.type = 'ABSENCE'"
        _SQL += " and d.site_id = '" + m_SiteID.ToString + "'"

        Dim _Absences As Decimal = ReturnSQLCount(_SQL)
        _Colour = Care.Controls.DashboardLabel.EnumColourCode.Green
        If _Absences > 0 Then _Colour = Care.Controls.DashboardLabel.EnumColourCode.Red

        dshAbsences.SetValue(_Absences, Care.Controls.DashboardLabel.EnumMiddleFormat.WholeNumber, _Colour)

        'payments
        '*********************************************************************************************************************************

        _SQL = ""
        _SQL += "select count(*) from Activity a"
        _SQL += " left join Day d on d.ID = a.day_id"
        _SQL += " where d.date = " + ValueHandler.SQLDate(m_Date.Value, True)
        _SQL += " and a.type = 'PAYMENT'"
        _SQL += " and d.site_id = '" + m_SiteID.ToString + "'"

        Dim _Payments As Decimal = ReturnSQLCount(_SQL)
        _Colour = Care.Controls.DashboardLabel.EnumColourCode.Green
        If _Payments > 0 Then _Colour = Care.Controls.DashboardLabel.EnumColourCode.Amber

        dshPayments.SetValue(_Payments, Care.Controls.DashboardLabel.EnumMiddleFormat.WholeNumber, _Colour)

        'accidents
        '*********************************************************************************************************************************

        _SQL = ""
        _SQL += "select count(*) from Incidents i"
        _SQL += " left join Day d on d.ID = i.day_id"
        _SQL += " where d.date = " + ValueHandler.SQLDate(m_Date.Value, True)
        _SQL += " and i.incident_type = 'Accident'"
        _SQL += " and d.site_id = '" + m_SiteID.ToString + "'"

        Dim _Accidents As Decimal = ReturnSQLCount(_SQL)
        _Colour = Care.Controls.DashboardLabel.EnumColourCode.Green
        If _Accidents > 0 Then _Colour = Care.Controls.DashboardLabel.EnumColourCode.Red
        dshAccidents.SetValue(_Accidents, Care.Controls.DashboardLabel.EnumMiddleFormat.WholeNumber, _Colour)

        'prior-injuries
        '*********************************************************************************************************************************

        _SQL = ""
        _SQL += "select count(*) from Incidents i"
        _SQL += " left join Day d on d.ID = i.day_id"
        _SQL += " where d.date = " + ValueHandler.SQLDate(m_Date.Value, True)
        _SQL += " and i.incident_type = 'Prior Injury'"
        _SQL += " and d.site_id = '" + m_SiteID.ToString + "'"

        Dim _Prior As Decimal = ReturnSQLCount(_SQL)
        _Colour = Care.Controls.DashboardLabel.EnumColourCode.Green
        If _Prior > 0 Then _Colour = Care.Controls.DashboardLabel.EnumColourCode.Red
        dshPrior.SetValue(_Prior, Care.Controls.DashboardLabel.EnumMiddleFormat.WholeNumber, _Colour)

        'starters
        '*********************************************************************************************************************************
        _SQL = "select count(*) from Children where started between " + m_SQLThisWeek + " and site_id = '" + m_SiteID.ToString + "'"

        Dim _Starters As Decimal = ReturnSQLCount(_SQL)
        _Colour = Care.Controls.DashboardLabel.EnumColourCode.Green
        If _Starters > 0 Then _Colour = Care.Controls.DashboardLabel.EnumColourCode.Amber
        dshStarters.SetValue(_Starters, Care.Controls.DashboardLabel.EnumMiddleFormat.WholeNumber, _Colour)

        'leavers
        '*********************************************************************************************************************************
        _SQL = "select count(*) from Children where date_left between " + m_SQLThisWeek + " and site_id = '" + m_SiteID.ToString + "'"

        Dim _Leavers As Decimal = ReturnSQLCount(_SQL)
        _Colour = Care.Controls.DashboardLabel.EnumColourCode.Green
        If _Leavers > 0 Then _Colour = Care.Controls.DashboardLabel.EnumColourCode.Amber
        dshLeavers.SetValue(_Leavers, Care.Controls.DashboardLabel.EnumMiddleFormat.WholeNumber, _Colour)

        'birthdays
        '*********************************************************************************************************************************
        _SQL = "SELECT count(*) "
        _SQL += " FROM Children"
        _SQL += " WHERE DatePart(Week, DateAdd(Year, DatePart(Year, " + ValueHandler.SQLDate(m_Date.Value, True) + ") - DatePart(Year, DOB), DOB)) = DatePart(Week, " + ValueHandler.SQLDate(m_Date.Value, True) + ")"
        _SQL += " AND (DATE_LEFT IS NULL OR DATE_LEFT > " + ValueHandler.SQLDate(m_Date.Value, True) + ")"
        _SQL += " AND SITE_ID = '" + m_SiteID.ToString + "'"
        _SQL += " AND status = 'Current'"

        Dim _ChildBirthdays As Decimal = ReturnSQLCount(_SQL)

        _SQL = "SELECT count(*) "
        _SQL += " FROM Staff"
        _SQL += " WHERE DatePart(Week, DateAdd(Year, DatePart(Year, " + ValueHandler.SQLDate(m_Date.Value, True) + ") - DatePart(Year, DOB), DOB)) = DatePart(Week, " + ValueHandler.SQLDate(m_Date.Value, True) + ")"
        _SQL += " AND (DATE_LEFT IS NULL OR DATE_LEFT > " + ValueHandler.SQLDate(m_Date.Value, True) + ")"
        _SQL += " AND SITE_ID = '" + m_SiteID.ToString + "'"

        Dim _StaffBirthdays As Decimal = ReturnSQLCount(_SQL)
        Dim _Birthdays As Decimal = _ChildBirthdays + _StaffBirthdays

        _Colour = Care.Controls.DashboardLabel.EnumColourCode.Green
        If _Birthdays > 0 Then _Colour = Care.Controls.DashboardLabel.EnumColourCode.Amber
        dshBirthdays.SetValue(_Birthdays, Care.Controls.DashboardLabel.EnumMiddleFormat.WholeNumber, _Colour)

        'viewings
        '*********************************************************************************************************************************
        _SQL = "select count(*) from Leads where date_viewing between " + m_SQLThisWeek + " and site_id = '" + m_SiteID.ToString + "'"
        Dim _Viewings As Decimal = ReturnSQLCount(_SQL)
        _Colour = Care.Controls.DashboardLabel.EnumColourCode.Amber
        If _Viewings > 0 Then _Colour = Care.Controls.DashboardLabel.EnumColourCode.Green
        dshViewings.SetValue(_Viewings, Care.Controls.DashboardLabel.EnumMiddleFormat.WholeNumber, Care.Controls.DashboardLabel.EnumColourCode.Green)

        'checklists
        '*********************************************************************************************************************************
        dshChecklists.SetValue(0, Care.Controls.DashboardLabel.EnumMiddleFormat.WholeNumber, Care.Controls.DashboardLabel.EnumColourCode.Green)

        'tasks due
        '*********************************************************************************************************************************
        dshTasks.SetValue(0, Care.Controls.DashboardLabel.EnumMiddleFormat.WholeNumber, Care.Controls.DashboardLabel.EnumColourCode.Green)

        '*********************************************************************************************************************************

    End Sub

    Private Function ReturnSQLCount(ByVal SQL As String) As Decimal

        Dim _o As Object = DAL.ReturnScalar(Session.ConnectionString, SQL)
        If _o IsNot Nothing Then
            Return ValueHandler.ConvertDecimal(_o)
        Else
            Return 0
        End If

    End Function

    Private Function ReturnRegisterCount(ByVal TypeIn As String) As Decimal

        Dim _SQL As String = ""

        Select Case TypeIn

            Case "C"
                _SQL += "select count(*) from RegisterSummary r"
                _SQL += " left join Children c on c.ID = r.person_id"
                _SQL += " where r.date = " + ValueHandler.SQLDate(m_Date.Value, True)
                _SQL += " And r.person_type = 'C'"
                _SQL += " and r.in_out = 'I'"
                _SQL += " and c.site_id = '" + m_SiteID.ToString + "'"

            Case "S"
                _SQL += "select count(*) from RegisterSummary r"
                _SQL += " left join Staff s on s.ID = r.person_id"
                _SQL += " where r.date = " + ValueHandler.SQLDate(m_Date.Value, True)
                _SQL += " and r.person_type = 'S'"
                _SQL += " and r.in_out = 'I'"
                _SQL += " and s.site_id = '" + m_SiteID.ToString + "'"

        End Select

        Return ReturnSQLCount(_SQL)

    End Function

#Region "Control Events"

    Private Sub grdGroups_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles grdGroups.FocusedRowChanged
        DisplayActivityText()
    End Sub

    Private Sub btnSelectBreakfastSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectBreakfast.Click
        Dim _ReturnValue As String = Business.Meal.FindMeal(Me, Business.Meal.EnumMealType.Breakfast)
        If _ReturnValue <> "" Then
            Dim _Food As Business.Meal = Business.Meal.RetreiveByID(New Guid(_ReturnValue))
            If MealWarnings(_Food) Then Exit Sub
            txtBreakfast.AccessibleName = _ReturnValue
            txtBreakfast.Text = _Food._Name
            _Food = Nothing
        End If
    End Sub

    Private Sub btnClearBreakfast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearBreakfast.Click
        If CareMessage("Clear breakfast details?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Clear") = MsgBoxResult.Yes Then
            txtBreakfast.AccessibleName = ""
            txtBreakfast.Text = ""
        End If
    End Sub

    Private Sub btnSelectSnack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectSnack.Click
        Dim _ReturnValue As String = Business.Meal.FindMeal(Me, Business.Meal.EnumMealType.Snack)
        If _ReturnValue <> "" Then
            Dim _Food As Business.Meal = Business.Meal.RetreiveByID(New Guid(_ReturnValue))
            If MealWarnings(_Food) Then Exit Sub
            txtSnack.AccessibleName = _ReturnValue
            txtSnack.Text = _Food._Name
            _Food = Nothing
        End If
    End Sub

    Private Sub btnSelectLunch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectLunch.Click
        Dim _ReturnValue As String = Business.Meal.FindMeal(Me, Business.Meal.EnumMealType.Lunch)
        If _ReturnValue <> "" Then
            Dim _Food As Business.Meal = Business.Meal.RetreiveByID(New Guid(_ReturnValue))
            If MealWarnings(_Food) Then Exit Sub
            txtLunch.AccessibleName = _ReturnValue
            txtLunch.Text = _Food._Name
            _Food = Nothing
        End If
    End Sub

    Private Sub btnSelectTea_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectTea.Click
        Dim _ReturnValue As String = Business.Meal.FindMeal(Me, Business.Meal.EnumMealType.Tea)
        If _ReturnValue <> "" Then
            Dim _Food As Business.Meal = Business.Meal.RetreiveByID(New Guid(_ReturnValue))
            If MealWarnings(_Food) Then Exit Sub
            txtTea.AccessibleName = _ReturnValue
            txtTea.Text = _Food._Name
            _Food = Nothing
        End If
    End Sub

    Private Sub btnClearSnack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearSnack.Click
        If CareMessage("Clear snack details?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Clear") = MsgBoxResult.Yes Then
            txtSnack.AccessibleName = ""
            txtSnack.Text = ""
        End If
    End Sub

    Private Sub btnClearLunch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearLunch.Click
        If CareMessage("Clear lunch details?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Clear") = MsgBoxResult.Yes Then
            txtLunch.AccessibleName = ""
            txtLunch.Text = ""
        End If
    End Sub

    Private Sub btnClearTea_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearTea.Click
        If CareMessage("Clear tea details?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Clear") = MsgBoxResult.Yes Then
            txtTea.AccessibleName = ""
            txtTea.Text = ""
        End If
    End Sub

    Private Sub btnClearLunchDessert_Click(sender As System.Object, e As System.EventArgs) Handles btnClearLunchDessert.Click
        If CareMessage("Clear lunch dessert details?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Clear") = MsgBoxResult.Yes Then
            txtLunchDessert.AccessibleName = ""
            txtLunchDessert.Text = ""
        End If
    End Sub

    Private Sub btnClearTeaDessert_Click(sender As System.Object, e As System.EventArgs) Handles btnClearTeaDessert.Click
        If CareMessage("Clear tea dessert details?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Clear") = MsgBoxResult.Yes Then
            txtTeaDessert.AccessibleName = ""
            txtTeaDessert.Text = ""
        End If
    End Sub

    Private Sub btnSelectLunchDessert_Click(sender As System.Object, e As System.EventArgs) Handles btnSelectLunchDessert.Click
        Dim _ReturnValue As String = Business.Meal.FindMeal(Me, Business.Meal.EnumMealType.Dessert)
        If _ReturnValue <> "" Then
            Dim _Food As Business.Meal = Business.Meal.RetreiveByID(New Guid(_ReturnValue))
            If MealWarnings(_Food) Then Exit Sub
            txtLunchDessert.AccessibleName = _ReturnValue
            txtLunchDessert.Text = _Food._Name
            _Food = Nothing
        End If
    End Sub

    Private Sub btnSelectTeaDessert_Click(sender As System.Object, e As System.EventArgs) Handles btnSelectTeaDessert.Click
        Dim _ReturnValue As String = Business.Meal.FindMeal(Me, Business.Meal.EnumMealType.Dessert)
        If _ReturnValue <> "" Then
            Dim _Food As Business.Meal = Business.Meal.RetreiveByID(New Guid(_ReturnValue))
            If MealWarnings(_Food) Then Exit Sub
            txtTeaDessert.AccessibleName = _ReturnValue
            txtTeaDessert.Text = _Food._Name
            _Food = Nothing
        End If
    End Sub

    Private Sub btnSelectSnackPM_Click(sender As Object, e As EventArgs) Handles btnSelectSnackPM.Click
        Dim _ReturnValue As String = Business.Meal.FindMeal(Me, Business.Meal.EnumMealType.Snack)
        If _ReturnValue <> "" Then
            Dim _Food As Business.Meal = Business.Meal.RetreiveByID(New Guid(_ReturnValue))
            If MealWarnings(_Food) Then Exit Sub
            txtSnackPM.AccessibleName = _ReturnValue
            txtSnackPM.Text = _Food._Name
            _Food = Nothing
        End If
    End Sub

    Private Sub btnClearSnackPM_Click(sender As Object, e As EventArgs) Handles btnClearSnackPM.Click
        If CareMessage("Clear snack details?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Clear") = MsgBoxResult.Yes Then
            txtSnackPM.AccessibleName = ""
            txtSnackPM.Text = ""
        End If
    End Sub

#End Region

    Private Function MealWarnings(ByVal Meal As Business.Meal) As Boolean

        Dim _Return As Boolean = False
        Dim _WarningCount As Integer = 0
        Dim _Children As String = ""
        Dim _DayName As String = Today.DayOfWeek.ToString.ToLower

        'fetch the components for this meal and check the ingredients against the alergies
        Dim _Ingredients As String = GetIngredientsForMeal(Meal._ID.Value)

        If _Ingredients = "" Then Return False

        'fetch all the children due in today with alergies
        Dim _SQL As String = "select fullname, group_name, med_allergies from Children" & _
                             " where " + _DayName + " Is Not null" & _
                             " and status = 'Current'" & _
                             " and len(med_allergies) > 0"

        Dim _DTChildren As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DTChildren IsNot Nothing Then

            For Each _DRChild As DataRow In _DTChildren.Rows

                Dim _Name As String = _DRChild.Item("fullname").ToString
                Dim _Group As String = _DRChild.Item("group_name").ToString
                Dim _Allergies As String = Replace(_DRChild.Item("med_allergies").ToString, vbCrLf, "")

                If _Ingredients.ToUpper.Contains(_Allergies.ToUpper) Then
                    _WarningCount += 1
                    _Children += _Name + " (" + _Group + ") is allergic to " + _Allergies + vbCrLf
                End If

            Next

            If _WarningCount > 0 Then
                Dim _Mess As String = "Ingredients Vs Allergy Check:" + vbCrLf + vbCrLf + _Children + vbCrLf + "Are you sure you wish to Continue?"
                If CareMessage(_Mess, MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Allergy Warning") = DialogResult.Yes Then
                    _Return = False
                Else
                    _Return = True
                End If
            End If

        End If

        _DTChildren.Dispose()
        _DTChildren = Nothing

        Return _Return

    End Function

    Private Function GetIngredientsForMeal(MealID As Guid) As String

        Dim _Return As String = ""
        Dim _SQL As String = "select Food.name, Food.ingredients from MealComponents" & _
                             " left join Food on Food.ID = MealComponents.food_id" & _
                             " where meal_id = '" & MealID.ToString & "'" & _
                             " and ingredients is not null and len(ingredients) > 0"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then
            For Each _DR As DataRow In _DT.Rows
                _Return += _DR.Item("ingredients").ToString + " "
            Next
        End If

        _DT.Dispose()
        _DT = Nothing

        Return _Return

    End Function

    Private Sub timRefresh_Tick(sender As Object, e As EventArgs) Handles timRefresh.Tick
        DisplayStats()
    End Sub

    Private Sub DashboardDrillDown(ByVal SQL As String, ByVal FormCaption As String, ByVal FormToLoad As frmTodayDrillDown.EnumFormToLoad)

        Session.CursorWaiting()

        Dim _Title As String = FormCaption
        If m_Date = Today Then
            _Title += " today"
        Else
            _Title += " " + Format(m_Date, "dddd dd MMMM yyyy").ToString
        End If

        Dim _frm As New frmTodayDrillDown(SQL, _Title, FormToLoad)
        _frm.ShowDialog()

        _frm.Dispose()
        _frm = Nothing

    End Sub

#Region "Dashboard Clicks"

    Private Sub dshChildrenExpected_DoubleClick(sender As Object, e As Care.Controls.DashboardLabel.DashboardEventArgs) Handles dshChildrenExpected.DoubleClick

        If e.Value <= 0 Then Exit Sub

        Dim _SQL As String = ""

        _SQL += "select c.id, c.group_name as 'Room', c.fullname as 'Child Name', p.fullname as 'Primary Contact', p.tel_mobile as 'Mobile', b.booking_from as 'From', b.booking_to as 'To', b.tariff_name as 'Session'"
        _SQL += " from Bookings b"
        _SQL += " left join Children c on c.ID = b.child_id"
        _SQL += " left join Day d on d.date = b.booking_date and d.site_id = b.site_id"
        _SQL += " left join Groups g on g.id = c.group_id"
        _SQL += " left join Contacts p on p.family_id = c.family_id and p.primary_cont = 1"
        _SQL += " where booking_date = " + ValueHandler.SQLDate(m_Date.Value, True)
        _SQL += " and b.child_id not in (select act.key_id from Activity act where act.day_id = d.ID and act.key_id = b.child_id and act.type = 'ABSENCE')"
        _SQL += " and b.child_id not in (select rs.person_id from RegisterSummary rs where rs.date = b.booking_date and rs.person_id = b.child_id)"
        _SQL += " and b.booking_status <> 'Holiday'"
        _SQL += " and c.site_id = '" + m_SiteID.ToString + "'"
        _SQL += " order by b.booking_from, g.sequence, c.fullname"

        DashboardDrillDown(_SQL, "Children expected", frmTodayDrillDown.EnumFormToLoad.Child)

    End Sub

    Private Sub dshViewings_DoubleClick(sender As Object, e As Care.Controls.DashboardLabel.DashboardEventArgs) Handles dshViewings.DoubleClick

        If e.Value <= 0 Then Exit Sub

        Dim _SQL As String = ""

        _SQL += "select ID, date_viewing as 'Viewing Date', contact_fullname as 'Contact', contact_relationship as 'Relationship', contact_mobile as 'Mobile',"
        _SQL += " child_fullname as 'Child', child_dob as 'DOB' from Leads"
        _SQL += " where date_viewing between " + m_SQLThisWeek
        _SQL += " and site_id = '" + m_SiteID.ToString + "'"

        DashboardDrillDown(_SQL, "Viewings this week", frmTodayDrillDown.EnumFormToLoad.Lead)

    End Sub

    Private Sub dshStarters_DoubleClick(sender As Object, e As Care.Controls.DashboardLabel.DashboardEventArgs) Handles dshStarters.DoubleClick
        If e.Value <= 0 Then Exit Sub
        Dim _SQL As String = "select id, fullname as 'Name', group_name as 'Room', dob as 'Date of Birth', started as 'Start Date' from Children where started between " + m_SQLThisWeek
        _SQL += " and site_id = '" + m_SiteID.ToString + "'"
        DashboardDrillDown(_SQL, "Starters for this week", frmTodayDrillDown.EnumFormToLoad.Child)
    End Sub

    Private Sub dshLeavers_DoubleClick(sender As Object, e As Care.Controls.DashboardLabel.DashboardEventArgs) Handles dshLeavers.DoubleClick
        If e.Value <= 0 Then Exit Sub
        Dim _SQL As String = "select id, fullname as 'Name', group_name as 'Room', dob as 'Date of Birth', date_left as 'Leave Date' from Children where date_left between " + m_SQLThisWeek
        _SQL += " and site_id = '" + m_SiteID.ToString + "'"
        DashboardDrillDown(_SQL, "Leavers for this week", frmTodayDrillDown.EnumFormToLoad.Child)
    End Sub

    Private Sub dshBirthdays_DoubleClick(sender As Object, e As Care.Controls.DashboardLabel.DashboardEventArgs) Handles dshBirthdays.DoubleClick

        If e.Value <= 0 Then Exit Sub

        Dim _SQL As String = ""
        _SQL += "select id, 'Child' as 'Type', fullname as 'Name', group_name as 'Group', dob as 'DOB', datediff(year, dob, " + ValueHandler.SQLDate(m_Date.Value, True) + ") as 'Age' from Children"
        _SQL += " where DatePart(Week, DateAdd(Year, DatePart(Year, " + ValueHandler.SQLDate(m_Date.Value, True) + ") - DatePart(Year, DOB), DOB)) = DatePart(Week, " + ValueHandler.SQLDate(m_Date.Value, True) + ")"
        _SQL += " AND (DATE_LEFT IS NULL OR DATE_LEFT > " + ValueHandler.SQLDate(m_Date.Value, True) + ")"
        _SQL += " and Children.status = 'Current'"
        _SQL += " and Children.site_id = '" + m_SiteID.ToString + "'"
        _SQL += " UNION"
        _SQL += " select id, 'Staff' as 'Type', fullname as 'Name', group_name as 'Group', dob as 'DOB', datediff(year, dob, " + ValueHandler.SQLDate(m_Date.Value, True) + ") as 'Age' from staff"
        _SQL += " where DatePart(Week, DateAdd(Year, DatePart(Year, " + ValueHandler.SQLDate(m_Date.Value, True) + ") - DatePart(Year, DOB), DOB)) = DatePart(Week, " + ValueHandler.SQLDate(m_Date.Value, True) + ")"
        _SQL += " AND (DATE_LEFT IS NULL OR DATE_LEFT > " + ValueHandler.SQLDate(m_Date.Value, True) + ")"
        _SQL += " and Staff.site_id = '" + m_SiteID.ToString + "'"

        DashboardDrillDown(_SQL, "Birthdays for week", frmTodayDrillDown.EnumFormToLoad.AutoSelect)

    End Sub

    Private Sub dshStaffIn_DoubleClick(sender As Object, e As Care.Controls.DashboardLabel.DashboardEventArgs) Handles dshStaffIn.DoubleClick

        If e.Value <= 0 Then Exit Sub

        Dim _SQL As String = ""

        _SQL += "select s.id, s.fullname as 'Name', r.location as 'Current Location', r.last_stamp as 'Checked-In'"
        _SQL += " from RegisterSummary r"
        _SQL += " left join Staff s on s.id = r.person_ID"
        _SQL += " where date = " + ValueHandler.SQLDate(m_Date.Value, True)
        _SQL += " and person_type = 'S'"
        _SQL += " and in_out = 'I'"
        _SQL += " and s.site_id = '" + m_SiteID.ToString + "'"
        _SQL += " order by r.last_stamp"

        DashboardDrillDown(_SQL, "Staff checked in", frmTodayDrillDown.EnumFormToLoad.Staff)

    End Sub

    Private Sub dshChildrenIn_DoubleClick(sender As Object, e As Care.Controls.DashboardLabel.DashboardEventArgs) Handles dshChildrenIn.DoubleClick

        If e.Value <= 0 Then Exit Sub

        Dim _SQL As String = ""

        _SQL += "select c.id, c.fullname as 'Name', c.group_name as 'Normal Room', r.location as 'Current Location', r.last_stamp as 'Checked-In'"
        _SQL += " from RegisterSummary r"
        _SQL += " left join Children c on c.id = r.person_ID"
        _SQL += " where date = " + ValueHandler.SQLDate(m_Date.Value, True)
        _SQL += " and person_type = 'C'"
        _SQL += " and in_out = 'I'"
        _SQL += " and c.site_id = '" + m_SiteID.ToString + "'"
        _SQL += " order by r.last_stamp"

        DashboardDrillDown(_SQL, "Children checked in", frmTodayDrillDown.EnumFormToLoad.Child)

    End Sub

    Private Sub dshAccidents_DoubleClick(sender As Object, e As Care.Controls.DashboardLabel.DashboardEventArgs) Handles dshAccidents.DoubleClick

        If e.Value <= 0 Then Exit Sub

        Dim _SQL As String = ""

        _SQL += "select c.id, c.group_name as 'Room', c.fullname as 'Name', i.incident_type as 'Type', i.injury as 'Injury', i.stamp as 'Date/Time'"
        _SQL += " from Incidents i"
        _SQL += " left join Children c on c.id = i.child_id"
        _SQL += " left join Day d on d.ID = i.day_id and d.site_id = c.site_id"
        _SQL += " where d.date = " + ValueHandler.SQLDate(m_Date.Value, True)
        _SQL += " and c.site_id = '" + m_SiteID.ToString + "'"
        _SQL += " and i.incident_type = 'Accident'"
        _SQL += " order by i.stamp desc"

        DashboardDrillDown(_SQL, "Accidents Recorded", frmTodayDrillDown.EnumFormToLoad.Child)

    End Sub

    Private Sub dshAbsences_DoubleClick(sender As Object, e As Care.Controls.DashboardLabel.DashboardEventArgs) Handles dshAbsences.DoubleClick

        If e.Value <= 0 Then Exit Sub

        Dim _SQL As String = ""

        _SQL += "select c.id, c.group_name as 'Room', c.fullname as 'Name', description as 'Reason', a.value_1 as 'Reported by', staff_name as 'Recorded by', stamp as 'Date/Time'"
        _SQL += " from Activity a"
        _SQL += " left join Children c on c.id = a.key_id"
        _SQL += " left join Day d on d.ID = a.day_id and d.site_id = c.site_id"
        _SQL += " left join SiteRooms g on g.id = c.group_id"
        _SQL += " where date = " + ValueHandler.SQLDate(m_Date.Value, True)
        _SQL += " and a.type = 'ABSENCE'"
        _SQL += " and c.site_id = '" + m_SiteID.ToString + "'"
        _SQL += " order by g.room_sequence, c.fullname"

        DashboardDrillDown(_SQL, "Children reported Absent", frmTodayDrillDown.EnumFormToLoad.Child)

    End Sub

#End Region

    Private Sub btnDayPatterns_Click(sender As Object, e As EventArgs) Handles btnDayPatterns.Click
        If CareMessage("Are you sure you want to rebuild day patterns for Today?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Rebuild Patterns") = DialogResult.Yes Then
            Business.Child.BuildDayPatternsForEveryone(Today)
        End If
    End Sub

    Private Sub m_StartOfDay_Progress(sender As Object, Progress As Business.ProgressEvent) Handles m_StartOfDay.Progress
        Session.SetProgressMessage(Progress.ProgressText + "...")
    End Sub

    Private Sub dshPrior_DoubleClick(sender As Object, e As Care.Controls.DashboardLabel.DashboardEventArgs) Handles dshPrior.DoubleClick

        If e.Value <= 0 Then Exit Sub

        Dim _SQL As String = ""

        _SQL += "select c.id, c.group_name as 'Room', c.fullname as 'Name', i.incident_type as 'Type', i.injury as 'Injury', i.stamp as 'Date/Time'"
        _SQL += " from Incidents i"
        _SQL += " left join Children c on c.id = i.child_id"
        _SQL += " left join Day d on d.ID = i.day_id and d.site_id = c.site_id"
        _SQL += " where d.date = " + ValueHandler.SQLDate(m_Date.Value, True)
        _SQL += " and c.site_id = '" + m_SiteID.ToString + "'"
        _SQL += " and i.incident_type = 'Prior Injury'"
        _SQL += " order by i.stamp desc"

        DashboardDrillDown(_SQL, "Prior-Injuries Reported", frmTodayDrillDown.EnumFormToLoad.Child)

    End Sub

    Private Sub dshPayments_DoubleClick(sender As Object, e As Care.Controls.DashboardLabel.DashboardEventArgs) Handles dshPayments.DoubleClick

        If e.Value <= 0 Then Exit Sub

        Dim _SQL As String = ""

        _SQL += "select a.id, key_name As 'Contact', value_2 as 'Payment Method', cast(value_1 as money) as 'Payment', staff_name as 'Taken By', stamp as 'Date/Time'"
        _SQL += " from Activity a"
        _SQL += " left join Day d on d.ID = a.day_id"
        _SQL += " where d.date = " + ValueHandler.SQLDate(m_Date.Value, True)
        _SQL += " and d.site_id = '" + m_SiteID.ToString + "'"
        _SQL += " and a.type = 'Payment'"
        _SQL += " order by a.stamp desc"

        DashboardDrillDown(_SQL, "Payments Processed", frmTodayDrillDown.EnumFormToLoad.None)

    End Sub

End Class

