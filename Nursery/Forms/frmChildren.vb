﻿Imports System.Drawing
Imports Care.Global
Imports Care.Shared
Imports Care.Data
Imports System.Windows.Forms

Public Class frmChildren

    Dim m_Child As Business.Child
    Dim m_DrillDownChildID As Guid = Nothing
    Dim m_DrillDown As Boolean = False
    Dim m_DrillDownSessions As Boolean = False
    Dim m_JustCreated As Boolean = False
    Dim m_FormTitle As String = ""
    Dim m_RebuildBookings As Boolean = False

    Private m_ClassificationEnabled As Boolean = False
    Private m_Bookings As New List(Of Business.Booking)
    Private RecordCounts As New RecordCountObject

    Private m_AllergyMatrix As New List(Of AllergyMatrix)
    Private m_SchoolLeaveMonths As Integer = 0
    Private m_CopyTariff As Business.ChildAdvanceSession = Nothing
    Private m_BuildModal As Boolean = False

#Region "Constructor"

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub New(ByVal ChildID As Guid, Optional Sessions As Boolean = False)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        m_DrillDownChildID = ChildID
        m_DrillDown = True
        m_DrillDownSessions = Sessions

    End Sub

#End Region

    Private Sub frmChildren_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If bgwBookings.IsBusy Then
            e.Cancel = True
        End If
    End Sub

    Private Sub frmChildren_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        m_FormTitle = Me.Text
        lblAge.Text = ""
        lblStartDate.Text = ""
        lblLeaveDate.Text = ""
        lblMoveIn.Text = ""
        btnFamilyView.Enabled = False
        btnLabelTablemats.Enabled = False
        btnPostPlacement.Enabled = False

        m_ClassificationEnabled = ParameterHandler.ReturnBoolean("USECLASS")
        lblClass.Visible = m_ClassificationEnabled
        cbxClass.Visible = m_ClassificationEnabled

        btnPostPlacement.Visible = ParameterHandler.ReturnBoolean("POSTPLACEMENT")

        m_BuildModal = ParameterHandler.ReturnBoolean("BOOKINGSMODAL")
        m_SchoolLeaveMonths = ParameterHandler.ReturnInteger("SCHOOLMONTHS", False)

        Session.SetProgressMessage("Populating Lists...")

        PopulateLists()

        Session.HideProgressBar()

        tcMain.DisableTabs()
        ToggleSessionButtons(False)

        SchedulerStorage1.Appointments.Mappings.Start = "BookingStart"
        SchedulerStorage1.Appointments.Mappings.End = "BookingEnd"
        SchedulerStorage1.Appointments.Mappings.Subject = "Subject"
        SchedulerStorage1.Appointments.Mappings.Location = "Location"
        SchedulerStorage1.Appointments.Mappings.Description = "Notes"
        SchedulerStorage1.Appointments.Mappings.Label = "Label"

        If Not m_DrillDownChildID = Nothing Then
            DisplayRecord(m_DrillDownChildID)
        End If

        txtTags.ReadOnly = True

        gbxImm.Hide()
        gbxConsent.Hide()
        gbxRepeatMedication.Hide()

        Session.CursorDefault()

    End Sub

    Private Sub frmChildren_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        If m_DrillDown Then
            ToolbarMode = ToolbarEnum.EditOnly
            If m_DrillDownSessions Then tcMain.SelectedTabPage = tabSessions
        End If
    End Sub

#Region "Overrides"

    Protected Overrides Function BeforeCommitUpdate() As Boolean

        If Mode <> "EDIT" Then Return True

        Dim _Rebuild As Boolean = False

        'FETCH THE CURRENT DB VERSION OF THE CHILD RECORD BEFORE THE UPDATE
        Dim _DBChild As Business.Child = Business.Child.RetreiveByID(m_Child._ID.Value)
        If _DBChild IsNot Nothing Then

            'check if we have changed any data that could cause us to rebuild the bookings
            Select Case tcMain.SelectedTabPage.Name

                Case "tabGeneral"

                    'is rebuild required?
                    _Rebuild = RebuildRequired(_DBChild)

                    If cbxMoveMode.Text = "Manual" Then
                        If cdtMoveDate.Text = "" Then
                            CareMessage("Please enter a Room Move date.", MessageBoxIcon.Exclamation, "Mandatory Field")
                            Return False
                        End If
                        If cbxMoveRoom.Text = "" Then
                            CareMessage("Please select the Room the Child will move to.", MessageBoxIcon.Exclamation, "Mandatory Field")
                            Return False
                        End If
                    End If

                    If _DBChild._Started <> udtJoined.Value Then
                        'start date has changed, so we might need to change the session start date
                        ChangeSessionStartDate(udtJoined.Value.Value)
                    End If

                Case "tabSessions"

                    If tcSessions.SelectedTabPage.Name = "tabSessProfile" Then

                        If _DBChild._TermOnly <> chkTermTime.Checked Then
                            _Rebuild = True
                        End If

                        If _DBChild._TermType <> cbxTermType.Text Then
                            _Rebuild = True
                        End If

                    End If

            End Select

        End If

        'Set the rebuild bookings flag, the flag might be true already, so we only change it if _Rebuild is True.
        If _Rebuild = True Then m_RebuildBookings = True

        Return True

    End Function

    Private Sub ChangeSessionStartDate(ByVal NewStartDate As Date)

        Dim _SQL As String = "select ID from ChildAdvanceSessions where child_id = '" + m_Child._ID.Value.ToString + "'"
        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)

        If _DT IsNot Nothing Then

            If _DT.Rows.Count = 1 Then

                Dim _Session As Business.ChildAdvanceSession = Business.ChildAdvanceSession.RetreiveByID(New Guid(_DT.Rows(0).Item("ID").ToString))
                If _Session IsNot Nothing Then

                    'if there is only one session defined, we will prompt to automatically change the w/c date
                    Dim _NewWC As Date = ValueHandler.NearestDate(NewStartDate, DayOfWeek.Monday, ValueHandler.EnumDirection.Backwards)

                    If _NewWC <> _Session._DateFrom.Value Then
                        If CareMessage("You have changed the Start date. Do you want to automatically update the Session date?", MessageBoxIcon.Question, MessageBoxButtons.YesNo, "Update Session Dates?") = DialogResult.Yes Then
                            _Session._DateFrom = _NewWC
                            _Session.Store()
                        End If
                    End If

                End If

            End If

        End If

    End Sub

    Private Function RebuildRequired(ByRef DBChild As Business.Child) As Boolean

        If DBChild._Status <> cbxStatus.Text Then Return True
        If DBChild._Dob <> udtDOB.Value Then Return True
        If DBChild._Started <> udtJoined.Value Then Return True

        If udtLeft.Value.HasValue Then
            If DBChild._DateLeft.HasValue Then
                If DBChild._DateLeft <> udtLeft.Value Then Return True
            Else
                Return True
            End If
        Else
            If DBChild._DateLeft.HasValue Then Return True
        End If

        If DBChild._MoveMode <> cbxMoveMode.Text Then Return True

        If DBChild._MoveDate.HasValue <> cdtMoveDate.Value.HasValue Then Return True
        If DBChild._MoveDate.HasValue AndAlso cdtMoveDate.Value.HasValue Then
            If DBChild._MoveDate <> cdtMoveDate.DateTime Then
                Return True
            End If
        End If

        Dim _MoveRatioID As String = ""
        If cbxMoveRoom.SelectedValue IsNot Nothing Then _MoveRatioID = cbxMoveRoom.SelectedValue.ToString
        If DBChild._MoveRatioId.ToString <> _MoveRatioID Then Return True

        Return False

    End Function

    Protected Overrides Function BeforeDelete() As Boolean

        If InputBox("Type 'Delete' to Continue", "Delete Child").ToUpper = "DELETE" Then

            Dim _SQL As String = ""

            _SQL = "select count(*) from Activity where key_id = '" + m_Child._ID.Value.ToString + "'"
            Dim _Activity As Integer = CleanData.ReturnInteger(DAL.ReturnScalar(Session.ConnectionString, _SQL))

            If _Activity > 0 Then
                CareMessage("Unable to Delete Child when activity has been generated.", MessageBoxIcon.Exclamation, "Delete Child")
                Return False
            End If

            Return True

        Else
            Return False
        End If

    End Function

    Protected Overrides Sub CommitDelete()
        m_Child.Delete()
    End Sub

    Protected Overrides Sub FindRecord()

        Dim _ReturnValue As String = Business.Child.FindChild(Me)
        If _ReturnValue <> "" Then
            DisplayRecord(New Guid(_ReturnValue))
        End If

    End Sub

    Protected Overrides Sub AfterAcceptChanges()
        MyBase.AfterAcceptChanges()
        If m_JustCreated Then
            tcMain.SelectedTabPage = tabSessions
            ToolEdit()
            AddItem()
            m_JustCreated = False
        End If
        SetToolbar(tcMain.SelectedTabPage.Name)
    End Sub

    Protected Overrides Sub AfterCommitUpdate()

        If Mode = "ADD" Then
            m_JustCreated = True
        Else
            m_JustCreated = False
        End If

        btnFamilyView.Enabled = True
        btnLabelTablemats.Enabled = True
        btnPostPlacement.Enabled = True
        ToggleSessionButtons(False)
        PictureBox.Enabled = False

        btnPIN.Enabled = False

        If m_RebuildBookings Then
            RebuildBookings()
        End If

        cgAllergyMatrix.AllowEdit = False
        cgBookings.ButtonsEnabled = False

        txtTags.ReadOnly = True
        txtTags.ResetBackColor()

    End Sub

    Protected Overrides Sub AfterCancelChanges()

        If m_Child._FamilyId.HasValue Then
            btnFamilyView.Enabled = True
            btnLabelTablemats.Enabled = True
            btnPostPlacement.Enabled = True
        Else
            btnFamilyView.Enabled = False
            btnLabelTablemats.Enabled = False
            btnPostPlacement.Enabled = False
        End If

        btnPIN.Enabled = False

        ToggleSessionButtons(False)
        PictureBox.Enabled = False

        cgImms.ButtonsEnabled = False
        cgAllergyMatrix.AllowEdit = False
        cgBookings.ButtonsEnabled = False
        cgRepeatingMedication.ButtonsEnabled = False

        txtTags.ReadOnly = True
        txtTags.ResetBackColor()

        'if the rebuild bookings flag is set, we rebuild even if the user has clicked cancel
        If m_RebuildBookings Then
            RebuildBookings()
        End If

    End Sub

    Protected Overrides Sub CommitUpdate()

        Dim _Child As Business.Child = CType(bs.Item(bs.Position), Business.Child)

        Select Case tcMain.SelectedTabPage.Name

            Case "tabGeneral"
                CommitGeneral(_Child)

            Case "tabSessions"
                If tcSessions.SelectedTabPage.Name = "tabSessProfile" Then CommitSessions(_Child)

            Case "tabHealth"
                CommitHealth(_Child)

        End Select

        _Child.Store()

        _Child = Nothing

    End Sub

    Protected Overrides Sub SetBindings()

        m_Child = New Business.Child
        bs.DataSource = m_Child

        'general tab
        '/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        udtJoined.DataBindings.Add("Value", bs, "_Started", True)
        udtLeft.DataBindings.Add("Value", bs, "_DateLeft", True)

        txtForename.DataBindings.Add("Text", bs, "_Forename")
        txtKnownAs.DataBindings.Add("Text", bs, "_KnownAs")

        udtDOB.DataBindings.Add("Value", bs, "_DOB", True)
        cbxGender.DataBindings.Add("SelectedValue", bs, "_Gender")

        'udtJoinedGroup.DataBindings.Add("Value", bs, "_GroupStarted", True)

        ccxNationality.DataBindings.Add("Text", bs, "_Nationality")
        cbxEthnicity.DataBindings.Add("Text", bs, "_Ethnicity")
        cbxReligion.DataBindings.Add("Text", bs, "_Religion")
        ccxLanguage.DataBindings.Add("Text", bs, "_Language")

        txtSchoolRef1.DataBindings.Add("Text", bs, "_SchoolRef1")
        txtSchoolRef2.DataBindings.Add("Text", bs, "_SchoolRef2")

        'allergies, health and consent
        '/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        txtMedAllergies.DataBindings.Add("Text", bs, "_MedAllergies")
        txtMedMedication.DataBindings.Add("Text", bs, "_MedMedication")
        txtMedNotes.DataBindings.Add("Text", bs, "_MedNotes")

        cbxDietRestrict.DataBindings.Add("Text", bs, "_DietRestrict")
        txtDietNotes.DataBindings.Add("Text", bs, "_DietNotes")

        txtSurgName.DataBindings.Add("Text", bs, "_SurgName")
        txtSurgDoctor.DataBindings.Add("Text", bs, "_SurgDoc")

        txtHVName.DataBindings.Add("Text", bs, "_HvName")

        chkSEN.DataBindings.Add("Checked", bs, "_Sen")
        txtSENNotes.DataBindings.Add("Text", bs, "_SenNotes")

        'daily reports
        '/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        chkNappies.DataBindings.Add("Checked", bs, "_Nappies")
        chkMilk.DataBindings.Add("Checked", bs, "_Milk")
        chkBabyFood.DataBindings.Add("Checked", bs, "_BabyFood")
        chkOffMenu.DataBindings.Add("Checked", bs, "_OffMenu")
        chkFoodDetail.DataBindings.Add("Checked", bs, "_FoodDetail")

        chkSMSFood.DataBindings.Add("Checked", bs, "_smsfood")
        chkSMSMilk.DataBindings.Add("Checked", bs, "_smsmilk")
        chkSMSNappy.DataBindings.Add("Checked", bs, "_smstoilet")
        chkSMSSleep.DataBindings.Add("Checked", bs, "_smssleep")
        chkSMSObs.DataBindings.Add("Checked", bs, "_smsobs")
        chkSMSMedicine.DataBindings.Add("Checked", bs, "_smsmedical")

        chkPaperReport.DataBindings.Add("Checked", bs, "_PaperReport")
        cbxReportDetail.DataBindings.Add("SelectedValue", bs, "_ReportDetail")

    End Sub

    Protected Overrides Sub AfterAdd()

        MyBase.RecordID = Nothing
        MyBase.RecordPopulated = False
        m_Child = New Business.Child
        bs.DataSource = m_Child

        btnFamilyView.Enabled = False
        btnLabelTablemats.Enabled = False
        btnPostPlacement.Enabled = False

        PictureBox.Clear()

        lblAge.Text = ""
        lblFamily.Tag = ""

        cbxMoveMode.Text = "Auto"
        lblMoveIn.Text = ""

        txtFamily.Text = ""
        Me.Text = m_FormTitle + " - New Record"

        cbxStatus.SelectedValue = "On Waiting List"

        ToggleSessionButtons(True)
        SetFamily()

        txtTags.ReadOnly = False
        txtTags.BackColor = Session.ChangeColour

        txtForename.Focus()

    End Sub

    Protected Overrides Sub AfterEdit()

        Select Case tcMain.SelectedTabPage.Name

            Case "tabGeneral"

                btnFamilyView.Enabled = False
                btnLabelTablemats.Enabled = False
                btnPostPlacement.Enabled = False
                PictureBox.Enabled = True

                txtTags.ReadOnly = False
                txtTags.BackColor = Session.ChangeColour

                SetFutureMoves()

            Case "tabSessions"
                ToggleSessionButtons(True)

            Case "tabHealth"

                Select Case tcHealth.SelectedTabPage.Name

                    Case "tabAllergyDiet"
                        If cbxAllergyGrade.SelectedIndex < 0 Then cbxAllergyGrade.SelectedIndex = 0
                        cgAllergyMatrix.AllowEdit = True

                    Case "tabImms"
                        cgImms.ButtonsEnabled = True

                    Case "tabRepeatingMedication"
                        cgRepeatingMedication.ButtonsEnabled = True

                End Select

            Case "tabReports"
                btnPIN.Enabled = True

        End Select

    End Sub

#End Region

    Private Sub tcMain_Selecting(sender As Object, e As DevExpress.XtraTab.TabPageCancelEventArgs) Handles tcMain.Selecting

        If m_Child Is Nothing Then Exit Sub
        Select Case e.Page.Name

            Case "tabGeneral"

            Case "tabSessions"
                HandleSessionsTab()

            Case "tabCalendar"
                DisplayBookings()

            Case "tabHealth"
                HandleHealthTab()

            Case "tabMedical"
                DisplayMedicineAndIncidents()

            Case "tabReports"
                DisplayReports()

            Case "tabAssessment"
                DisplayAssessment()

            Case "tabActivity"
                DisplayActivity()

        End Select

        SetToolbar(e.Page.Name)

    End Sub

    Private Sub SetToolbar(ByVal TabName As String)

        Select Case TabName

            Case "tabGeneral"
                Me.ToolbarMode = ToolbarEnum.Standard

            Case "tabSessions"
                Me.ToolbarMode = ToolbarEnum.FindandEdit

            Case "tabCalendar"
                Me.ToolbarMode = ToolbarEnum.FindOnly

            Case "tabHealth"
                Me.ToolbarMode = ToolbarEnum.FindandEdit

            Case "tabMedical"
                Me.ToolbarMode = ToolbarEnum.FindOnly

            Case "tabReports"
                Me.ToolbarMode = ToolbarEnum.FindandEdit

            Case "tabAssessment"
                Me.ToolbarMode = ToolbarEnum.FindOnly

            Case "tabActivity"
                Me.ToolbarMode = ToolbarEnum.FindOnly

        End Select

    End Sub

    Private Sub DisplayRecord(ByVal ChildID As Guid)

        Session.CursorWaiting()

        MyBase.RecordID = ChildID
        MyBase.RecordPopulated = True

        m_Child = Business.Child.RetreiveByID(ChildID)
        bs.DataSource = m_Child

        txtFamily.Text = m_Child._FamilyName

        lblFamily.Tag = m_Child._FamilyId
        If m_Child._FamilyId.HasValue Then
            btnFamilyView.Enabled = True
            btnLabelTablemats.Enabled = True
            btnPostPlacement.Enabled = True
        Else
            btnFamilyView.Enabled = False
            btnLabelTablemats.Enabled = False
            btnPostPlacement.Enabled = False
        End If

        txtSurname.Text = m_Child._Surname

        cbxStatus.SelectedValue = m_Child._Status.ToString
        cbxClass.Text = m_Child._Class.ToString

        txtTags.EditValue = m_Child._Tags

        cbxSite.SelectedValue = m_Child._SiteId.ToString
        Application.DoEvents()

        cbxGroup.SelectedValue = m_Child._GroupId.ToString
        cbxKeyworker.SelectedValue = m_Child._KeyworkerId.ToString

        cbxMoveMode.Text = m_Child._MoveMode
        If m_Child._MoveMode = "Manual" Then
            cdtMoveDate.Value = m_Child._MoveDate
            cbxMoveRoom.SelectedValue = m_Child._MoveRatioId
        Else
            cdtMoveDate.Text = ""
            cbxMoveRoom.SelectedIndex = -1
        End If

        cbxSchool.SelectedValue = m_Child._School.ToString

        DisplayDateLabels()

        '////////////////////////////////////////////////////////////////////////////////////////////////////////
        Me.Text = BuildFormTitle()
        '////////////////////////////////////////////////////////////////////////////////////////////////////////

        ToggleSessionButtons(False)
        tcMain.EnableTabs()
        tcMain.FirstPage()

        btnLabelTablemats.Enabled = True
        btnPIN.Enabled = False

        '////////////////////////////////////////////////////////////////////////////////////////////////////////

        m_Bookings.Clear()

        '////////////////////////////////////////////////////////////////////////////////////////////////////////

        cbxConsentContact.PopulateWithSQL(Session.ConnectionString, "select fullname from Contacts where family_id = '" + m_Child._FamilyId.Value.ToString + "' order by primary_cont desc, fullname")

        '////////////////////////////////////////////////////////////////////////////////////////////////////////
        'display photo last as there is sometimes a bit of a delay

        PictureBox.Enabled = False
        If m_Child._Photo.HasValue Then
            PictureBox.DocumentID = m_Child._Photo
            PictureBox.Image = DocumentHandler.GetImageByID(m_Child._Photo)
        Else
            PictureBox.DocumentID = Nothing
            PictureBox.Clear()
        End If

        '////////////////////////////////////////////////////////////////////////////////////////////////////////

        Session.CursorDefault()

    End Sub

    Private Function BuildFormTitle() As String

        Dim _Title As String = ""
        _Title += m_FormTitle + " - " + txtForename.Text + " " + txtSurname.Text + ", " + cbxGroup.Text

        If m_Child._AllergyRating <> "" AndAlso m_Child._AllergyRating <> "None" Then
            _Title += " | " + m_Child._AllergyRating + " Allergies"
        End If

        If m_Child._TermOnly Then
            _Title += " | Term-Time Only, " + m_Child._TermType
        Else
            _Title += " | " + m_Child._TermType
        End If

        Return _Title

    End Function

    Private Sub CommitHealth(ByRef _Child As Business.Child)

        Select Case tcHealth.SelectedTabPage.Name

            Case "tabAllergyDiet"

                If cbxAllergyGrade.SelectedIndex < 0 Then
                    _Child._AllergyRating = "None"
                Else
                    _Child._AllergyRating = cbxAllergyGrade.Text
                End If

                CommitAllergyMatrix()

            Case "tabMedContact"

                _Child._SurgTel = txtSurgTel.Text

                _Child._HvTel = txtHVTelephone.Text
                _Child._HvMobile = txtHVMobile.Text
                _Child._HvEmail = txtHVEmail.Text

            Case "tabSEND"

                If chkSEN.Checked Then
                    _Child._Sen = True
                    _Child._SenNotes = txtSENNotes.Text
                Else
                    _Child._Sen = False
                    _Child._SenNotes = ""
                End If

            Case "tabImms"
                cgImms.ButtonsEnabled = False

            Case "tabAttributes"
                CommitAttributes()

            Case "tabRepeatingMedication"
                cgRepeatingMedication.ButtonsEnabled = False

        End Select

    End Sub

    Private Sub CommitAllergyMatrix()

        DeleteAttibutes("Allergies")

        cgAllergyMatrix.Store()
        For Each _A As AllergyMatrix In m_AllergyMatrix
            If _A.Selected Then
                SaveAttribute("Allergies", _A.Description)
            End If
        Next

    End Sub

    Private Sub DeleteAttibutes(ByVal Category As String)
        Dim _SQL As String = "delete from ChildAttrib where child_id = '" + m_Child._ID.ToString + "' and category = '" + Category + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)
    End Sub

    Private Sub SaveAttribute(ByVal Category As String, ByVal Attribute As String)
        Dim _A As New Business.ChildAttrib
        _A._ChildId = m_Child._ID
        _A._Category = Category
        _A._Attribute = Attribute
        _A.Store()
    End Sub

    Private Sub CommitAttributes()



    End Sub

    Private Sub CommitSessions(ByRef _Child As Business.Child)

        If cbxDiscount.SelectedValue Is Nothing Then
            _Child._Discount = Nothing
        Else
            _Child._Discount = New Guid(cbxDiscount.SelectedValue.ToString)
        End If

        _Child._InvoiceDue = cbxPaymentDue.Text
        _Child._InvoiceFreq = cbxPaymentFreq.Text
        _Child._PaymentMethod = cbxPaymentMethod.Text
        _Child._PaymentRef = txtPaymentRef.Text

        _Child._TermType = cbxTermType.Text
        _Child._TermOnly = chkTermTime.Checked

        _Child._NlCode = cbxNLCode.Text
        _Child._NlTracking = cbxNLTracking.Text

        _Child._FundingType = cbxFundType.Text
        _Child._FundingRef = txtFundRef.Text

        _Child._VoucherMode = cbxVoucherMode.Text
        If _Child._VoucherMode = "" Then _Child._VoucherMode = "Disabled"

        If _Child._VoucherMode = "Disabled" Then
            _Child._VoucherProportion = 0
            _Child._VoucherSeq = 0
        Else

            If _Child._VoucherMode.Contains("Fixed") Then
                _Child._VoucherProportion = ValueHandler.ConvertDecimal(txtVoucherProportion.Text)
            Else
                _Child._VoucherProportion = 0
            End If

            _Child._VoucherSeq = ValueHandler.ConvertInteger(txtVoucherSequence.Text)

        End If

    End Sub

    Private Sub CommitGeneral(ByRef _Child As Business.Child)

        If Mode = "ADD" Then
            _Child._Status = "On Waiting List"
            If Not _Child._GroupStarted.HasValue Then
                _Child._GroupStarted = _Child._Started
            End If
            _Child._InvoiceFreq = "Monthly"
        Else
            _Child._Status = cbxStatus.Text
        End If

        _Child._Surname = txtSurname.Text

        If m_ClassificationEnabled Then
            _Child._Class = cbxClass.Text
        Else
            _Child._Class = Nothing
        End If

        _Child._SiteId = New Guid(cbxSite.SelectedValue.ToString)
        _Child._SiteName = cbxSite.Text
        _Child._GroupId = New Guid(cbxGroup.SelectedValue.ToString)
        _Child._GroupName = cbxGroup.Text
        _Child._KeyworkerId = New Guid(cbxKeyworker.SelectedValue.ToString)
        _Child._KeyworkerName = cbxKeyworker.Text

        If cbxSchool.Text = "" Then
            _Child._School = Nothing
        Else
            _Child._School = New Guid(cbxSchool.SelectedValue.ToString)
        End If

        If txtTags.EditValue Is Nothing Then
            _Child._Tags = ""
        Else
            _Child._Tags = txtTags.EditValue.ToString
        End If

        _Child._MoveMode = cbxMoveMode.Text
        If cbxMoveMode.Text = "Manual" Then
            _Child._MoveDate = cdtMoveDate.Value
            _Child._MoveRatioId = New Guid(cbxMoveRoom.SelectedValue.ToString)
        Else
            _Child._MoveDate = Nothing
            _Child._MoveRatioId = Nothing
        End If

        If lblFamily.Tag IsNot Nothing AndAlso lblFamily.Tag.ToString <> "" Then
            _Child._FamilyId = New Guid(lblFamily.Tag.ToString)
            _Child._FamilyName = txtFamily.Text
        Else
            _Child._FamilyId = Nothing
            _Child._FamilyName = ""
        End If

        'calculate funding and end dates
        _Child._CalcLeaveDate = Bookings.ReturnSchoolLeaveDate(m_Child._Dob, m_SchoolLeaveMonths)

        '/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        DocumentHandler.UpdatePhoto(_Child._Photo, _Child._ID, PictureBox, _Child._Fullname)

    End Sub

    Private Sub PopulateLists()

        Try

            With cbxStatus
                .PopulateStart()
                .AddItem("On Waiting List")
                .AddItem("Current")
                .AddItem("On Hold")
                .AddItem("Left")
                .PopulateEnd()
            End With

            With cbxGender
                .PopulateStart()
                .AddItem("Male", "M")
                .AddItem("Female", "F")
                .AddItem("Unknown", "U")
                .PopulateEnd()
            End With

            With cbxMoveMode
                .PopulateStart()
                .AddItem("Disabled")
                .AddItem("Auto")
                .AddItem("Manual")
                .PopulateEnd()
            End With

            If m_ClassificationEnabled Then
                With cbxClass
                    .AllowBlank = True
                    .PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Classification"))
                End With
            End If

            Session.SetProgressMessage("Populating Religion, Language, Nationality and Ethnicity...")

            cbxReligion.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Religion"))
            ccxLanguage.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Language"))
            ccxNationality.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Nationality"))
            cbxEthnicity.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Ethnicity"))

            Session.SetProgressMessage("Populating Sites, Groups and Keyworkers...")

            With cbxSite
                .PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)
            End With

            With cbxKeyworker
                .DataSource = DAL.GetDataTablefromSQL(Session.ConnectionString, "select id, fullname from staff where status = 'C' and keyworker = 1 order by fullname").Rows
                .ValueMember = "ID"
                .DisplayMember = "fullname"
            End With

            With cbxReportDetail
                .PopulateStart()
                .AddItem("Basic Report", "B")
                .AddItem("Standard Report", "S")
                .AddItem("High Detail Report", "H")
                .PopulateEnd()
            End With

            '//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            Session.SetProgressMessage("Populating Discounts, Term Types and Schools...")

            With cbxDiscount
                .AllowBlank = True
                .DataSource = DAL.GetDataTablefromSQL(Session.ConnectionString, "select id, name from Discounts order by discount_value, name").Rows
                .ValueMember = "ID"
                .DisplayMember = "name"
            End With

            With cbxPaymentDue
                .PopulateStart()
                .AddItem("x days from invoice date")
                .AddItem("1st of the invoiced month")
                .AddItem("1st of the following invoiced month")
                .AddItem("28th of the invoiced month")
                .AddItem("28th of the following invoiced month")
                .AddItem("Last day of the invoiced month")
                .AddItem("Last day of the following invoiced month")
                .AddItem("Last day of the previous invoiced month")
                .PopulateEnd()
            End With

            cbxPaymentFreq.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Invoice Frequency"))
            cbxPaymentMethod.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Payment Method"))

            cbxTermType.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Term Type"))
            cbxFundType.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Funding Type"))

            With cbxSchool
                .AllowBlank = True
                .PopulateWithSQL(Session.ConnectionString, "select id, name from Schools order by name")
            End With

            Session.SetProgressMessage("Populating NL Codes and Tracking...")

            With cbxNLCode
                .AllowBlank = True
                .PopulateFromListMaintenance(ListHandler.ReturnListFromCache("NL Codes"))
            End With

            With cbxNLTracking
                .AllowBlank = True
                .PopulateFromListMaintenance(ListHandler.ReturnListFromCache("NL Tracking"))
            End With

            With cbxVoucherMode
                .PopulateStart()
                .AddItem("Disabled")
                .AddItem("Fixed Percentage")
                .AddItem("Fixed Value")
                .PopulateEnd()
            End With

            '//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            Session.SetProgressMessage("Populating Allergy Grades, Diet Restrictions and Immunisations...")

            With cbxAllergyGrade
                .PopulateStart()
                .AddItem("None")
                .AddItem("Moderate")
                .AddItem("Serious")
                .PopulateEnd()
            End With

            With cbxDietRestrict
                .AllowBlank = True
                .PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Diet Restrictions"))
            End With

            '//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            cbxImm.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Immunisation"))

            '//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            cbxNatureOfIllness.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Nature of Illness"))
            cbxDosageRequired.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Dosage"))
            cbxMedicationRequired.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Medicine"))

            '//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        Catch ex As Exception
            ErrorHandler.LogExceptionToDatabase(ex, False)
        End Try

    End Sub

#Region "General Tab"

    Private Sub SetFamily()

        Dim _ReturnValue As String = Business.Family.FindFamily(Me, True)

        If _ReturnValue <> "" Then

            lblFamily.Tag = _ReturnValue

            Dim _Family As Business.Family = Business.Family.RetreiveByID(New Guid(lblFamily.Tag.ToString))
            m_Child._FamilyName = _Family._Surname
            txtFamily.Text = _Family._Surname

            m_Child._Surname = _Family._Surname
            txtSurname.Text = _Family._Surname

            _Family = Nothing

        End If

    End Sub

    Private Sub btnFamilySet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFamilySet.Click
        SetFamily()
    End Sub

    Private Sub btnFamilyView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFamilyView.Click
        If Me.ReadOnlyPermissions Then
            CareMessage("You do not have permission to drill down.", MessageBoxIcon.Exclamation, "Permissions Check")
        Else
            If lblFamily.Tag.ToString <> "" Then
                btnFamilyView.Enabled = False
                Session.CursorWaiting()
                Business.Family.DrillDown(New Guid(lblFamily.Tag.ToString))
                btnFamilyView.Enabled = True
            End If
        End If
    End Sub

    Private Sub udtDOB_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles udtDOB.LostFocus
        DisplayDateLabels()
    End Sub

    Private Sub udtDOB_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles udtDOB.Validating
        If Not udtDOB.Value.HasValue Then Exit Sub
        If Mode = "ADD" Then Business.Child.CheckForDuplicates(udtDOB.Value.Value)
    End Sub

    Private Sub DisplayDateLabels()

        lblAge.Text = ""

        lblLeaveDate.Text = ""
        lblLeaveDate.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        lblLeaveDate.ToolTipTitle = ""
        lblLeaveDate.ToolTip = ""

        If udtDOB.Value Is Nothing Then Exit Sub
        lblAge.Text = ValueHandler.AgeYearsMonths(m_Child.AgeInMonths)

        If udtLeft.Value.HasValue Then
            lblLeaveDate.Text = ValueHandler.DateDifferenceAsText(udtDOB.Value.Value, udtLeft.Value.Value, True, True, False)
            lblLeaveDate.ToolTipTitle = "Age at Leaving Date"
            lblLeaveDate.ToolTip = "The age of the child when leaving according to their leaving date."
        Else
            'no leave date, so we display how long until they leave
            Dim _LeaveDate As Date? = Bookings.ReturnSchoolLeaveDate(ValueHandler.ConvertDate(udtDOB.Value), m_SchoolLeaveMonths)
            If _LeaveDate.HasValue Then
                lblLeaveDate.Text = Format(_LeaveDate.Value, "dd/MM/yyyy") + " - " + ValueHandler.DateDifferenceAsText(Today, _LeaveDate.Value, True, True, False)
                lblLeaveDate.ToolTipTitle = "Time left"
                lblLeaveDate.ToolTip = "Time until child leaves based upon School Leave Estimation (" + m_SchoolLeaveMonths.ToString + " months)"
            End If
        End If

    End Sub

#End Region

#Region "Sessions Tab"

    Private Sub ShowDialogForm(ByVal RecordID As Guid?)

        If Mode = "" Then Exit Sub
        If m_Child Is Nothing Then Exit Sub

        Dim _frm As Form = Nothing
        Select Case tcSessions.SelectedTabPage.Name

            Case "tabSessPatterns"
                Dim _Waiting As Boolean = False
                If m_Child._Status = "On Waiting List" Then _Waiting = True
                _frm = New frmBookingPattern(m_Child._SiteId.Value, m_Child._ID.Value, _Waiting, RecordID, ReturnPatternType, radSessSingle.Checked, m_CopyTariff)

            Case "tabSessAnnual"
                If radAnnualPatterns.Checked Then _frm = New frmChildAnnual(m_Child._ID.Value, RecordID)
                If radAnnualManual.Checked Then _frm = New frmChildCalc(m_Child._ID.Value, RecordID)

            Case "tabSessMonthly"
                _frm = New frmChildMonthWeek(m_Child._ID.Value, RecordID, frmChildMonthWeek.EnumFrequency.Monthly)

            Case "tabSessHolidays"
                If radHolidaysAll.Checked Then
                    CareMessage("Please select either Holidays or Absence", MessageBoxIcon.Exclamation, "Holiday or Absence?")
                Else
                    If radHolidays.Checked Then
                        _frm = New frmChildHoliday(m_Child._ID.Value, RecordID)
                    Else
                        _frm = New frmChildAbsence(m_Child._ID.Value, RecordID)
                    End If
                End If

            Case "tabSessOther"
                _frm = New frmChildCharge(m_Child._ID.Value, RecordID)

            Case "tabSessDeposits"
                _frm = New frmChildDeposit(m_Child._ID.Value, RecordID)

            Case "tabInvoices"
                _frm = New frmInvoiceCreditPrompt(m_Child._ID.Value)

        End Select

        If _frm IsNot Nothing Then

            _frm.ShowDialog()

            If _frm.DialogResult = DialogResult.OK Then
                If tcSessions.SelectedTabPage.Name = "tabSessPatterns" OrElse (tcSessions.SelectedTabPage.Name = "tabSessHolidays" AndAlso radHolidays.Checked) Then
                    m_RebuildBookings = True
                End If
                HandleSessionsTab()
            End If

            If TypeOf (_frm) Is frmBookingPattern Then
                Dim _BP As frmBookingPattern = CType(_frm, frmBookingPattern)
                m_CopyTariff = _BP.CopyTariff
            End If

            _frm.Dispose()
            _frm = Nothing

        End If

    End Sub

    Private Sub AddItem()
        ShowDialogForm(Nothing)
    End Sub

    Private Sub RemoveItem()

        If Mode = "" Then Exit Sub

        'column 0 - always ID
        'column 1 - always date

        Dim _ID As Guid? = Nothing
        Dim _Mess As String = "Are you sure you want to Delete this Pattern?"

        Select Case tcSessions.SelectedTabPage.Name

            Case "tabSessPatterns"
                _ID = ReturnDialogID(grdPatterns)

            Case "tabSessAnnual"
                _ID = ReturnDialogID(grdAnnual)
                _Mess = "Are you sure you want to Delete this Annual Calculation?"

            Case "tabSessMonthly"
                _ID = ReturnDialogID(grdSessMonthly)
                _Mess = "Are you sure you want to Delete this Monthly Override?"

            Case "tabSessHolidays"

                _ID = ReturnDialogID(grdHolidays)

                If radHolidaysAll.Checked Then
                    CareMessage("Please select either Holidays or Absence before clicking Remove", MessageBoxIcon.Exclamation, "Remove Record")
                Else
                    If radHolidays.Checked Then
                        _Mess = "Are you sure you want to Delete this Holiday?"
                    Else
                        _Mess = "Are you sure you want to Delete this Absence Record?"
                    End If
                End If

            Case "tabSessOther"
                _ID = ReturnDialogID(grdAdditional)
                _Mess = "Are you sure you want to Delete this Charge?"

            Case "tabSessDeposits"
                _ID = ReturnDialogID(grdDeposits)
                _Mess = "Are you sure you want to Delete this Deposit Transaction?"

        End Select

        If _ID.HasValue Then

            If CareMessage(_Mess, MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Delete") = DialogResult.Yes Then

                Select Case tcSessions.SelectedTabPage.Name

                    Case "tabSessPatterns"
                        Business.ChildAdvanceSession.DeleteSession(_ID.Value)

                    Case "tabSessAnnual"
                        If radAnnualPatterns.Checked Then
                            Business.ChildAnnual.DeleteRecord(_ID.Value)
                        Else
                            Business.ChildCalculation.DeleteRecord(_ID.Value)
                        End If

                    Case "tabSessMonthly"
                        Business.ChildWeekMonth.DeleteRecord(_ID.Value)

                    Case "tabSessHolidays"
                        If radHolidays.Checked Then
                            Business.ChildHoliday.DeleteRecord(_ID.Value)
                        Else
                            Business.ChildAbsence.DeleteRecord(_ID.Value)
                        End If

                    Case "tabSessOther"
                        Business.ChildCharge.DeleteRecord(_ID.Value)

                    Case "tabSessDeposits"
                        Business.ChildDeposit.DeleteRecord(_ID.Value)

                End Select

                HandleSessionsTab()

            End If

        End If

    End Sub

    Private Sub EditItem()

        If Mode = "" Then Exit Sub
        Dim _ID As Guid? = Nothing

        Select Case tcSessions.SelectedTabPage.Name

            Case "tabSessPatterns"
                _ID = ReturnDialogID(grdPatterns)

            Case "tabSessAnnual"
                _ID = ReturnDialogID(grdAnnual)

            Case "tabSessMonthly"
                _ID = ReturnDialogID(grdSessMonthly)

            Case "tabSessHolidays"
                _ID = ReturnDialogID(grdHolidays)

            Case "tabSessOther"
                _ID = ReturnDialogID(grdAdditional)

            Case "tabSessDeposits"
                _ID = ReturnDialogID(grdDeposits)

        End Select

        If _ID IsNot Nothing Then
            ShowDialogForm(_ID)
        End If

    End Sub

    Private Function ReturnDialogID(ByRef GridIn As Care.Controls.CareGrid) As Guid?

        If GridIn Is Nothing Then Return Nothing
        If GridIn.RecordCount < 1 Then Return Nothing
        If GridIn.CurrentRow(0).ToString = "" Then Return Nothing

        Return New Guid(GridIn.CurrentRow(0).ToString)

    End Function

    Private Function ReturnDialogID(ByRef GridIn As Care.Controls.CareGridWithButtons) As Guid?

        If GridIn Is Nothing Then Return Nothing
        If GridIn.RecordCount < 1 Then Return Nothing
        If GridIn.CurrentRow(0).ToString = "" Then Return Nothing

        Return New Guid(GridIn.CurrentRow(0).ToString)

    End Function

    Private Function ReturnPatternType() As frmBookingPattern.EnumPatternType
        If radSessAdditional.Checked Then Return frmBookingPattern.EnumPatternType.Additional
        If radSessOverride.Checked Then Return frmBookingPattern.EnumPatternType.Override
        Return frmBookingPattern.EnumPatternType.Recurring
    End Function

    Private Sub ToggleSessionButtons(ByVal Enabled As Boolean)

        If Me.Mode = "" Then Enabled = False

        If Enabled = False Then
            btnAdvAdd.Enabled = False
            btnAdvEdit.Enabled = False
            btnAdvRemove.Enabled = False
        Else

            btnAdvAdd.Enabled = True

            Dim _RecordCount As Integer = 0
            Select Case tcSessions.SelectedTabPage.Name

                Case "tabSessPatterns"
                    _RecordCount = grdPatterns.RecordCount

                Case "tabSessAnnual"
                    _RecordCount = grdAnnual.RecordCount

                Case "tabSessMonthly"
                    _RecordCount = grdSessMonthly.RecordCount

                Case "tabSessHolidays"
                    _RecordCount = grdHolidays.RecordCount

                Case "tabSessDeposits"
                    _RecordCount = grdDeposits.RecordCount

                Case "tabSessOther"
                    _RecordCount = grdAdditional.RecordCount

            End Select

            If _RecordCount > 0 Then
                btnAdvEdit.Enabled = True
                btnAdvRemove.Enabled = True
            Else
                btnAdvEdit.Enabled = False
                btnAdvRemove.Enabled = False
            End If

        End If

    End Sub

    Private Sub DisplayDeposits()

        lblDepositBalance.Text = "Deposit Balance: " + ValueHandler.MoneyAsText(Business.Child.ReturnDepositBalance(m_Child._ID.Value))

        Dim _SQL As String = ""
        _SQL = "select id, dep_date as 'Date', dep_type as 'Transaction Type', dep_ref as 'Ref', dep_post as 'Post to Financials?', dep_value as 'Value'" &
               " from ChildDeposits" &
               " where child_id = '" & m_Child._ID.ToString & "'" &
               " order by dep_date desc"

        grdDeposits.Populate(Session.ConnectionString, _SQL)
        grdDeposits.Columns("id").Visible = False

        grdDeposits.AutoSizeColumns()

    End Sub

    Private Sub DisplayCharges()

        Dim _SQL As String = ""

        _SQL = "select id, action_date as 'Date', description as 'Charge', reference as 'Ref', value as 'Amount'" &
               " from ChildCharges" &
               " where child_id = '" & m_Child._ID.ToString & "'" &
               " order by action_date desc"

        grdAdditional.Populate(Session.ConnectionString, _SQL)
        grdAdditional.Columns("id").Visible = False

        grdAdditional.AutoSizeColumns()

    End Sub

    Private Sub DisplayHolidays()

        Dim _SQL As String = ""

        If radHolidaysAll.Checked Then

            _SQL += "select id, 'Holiday' as 'Type', from_date as 'From', to_date as 'To',"
            _SQL += " DATEDIFF(day, from_date, to_date) + 1 as 'Days'"
            _SQL += " from ChildHolidays"
            _SQL += " where child_id = '" & m_Child._ID.ToString & "'"

            _SQL += " UNION"

            _SQL += " select id, 'Absence' as 'Type', abs_from as 'From', abs_to as 'To',"
            _SQL += " DATEDIFF(day, abs_from, abs_to) + 1 as 'Days'"
            _SQL += " from ChildAbsence"
            _SQL += " where child_id = '" & m_Child._ID.ToString & "'"

            _SQL += " order by [From] desc"

        Else
            If radHolidays.Checked Then
                _SQL += "select id, from_date as 'Holiday Start Date', from_half 'Start Date Half Day', to_date as 'Holiday End Date', to_half as 'End Date Half Day',"
                _SQL += " DATEDIFF(WEEK, from_date, to_date) as 'Weeks', DATEDIFF(day, from_date, to_date) + 1 as 'Days',"
                _SQL += " discount as 'Discount %', notes"
                _SQL += " from ChildHolidays"
                _SQL += " where child_id = '" & m_Child._ID.ToString & "'"
                _SQL += " order by from_date desc"
            Else
                _SQL += "select id, abs_from as 'Absence From', abs_to as 'Absence To',"
                _SQL += " DATEDIFF(day, abs_from, abs_to) + 1 as 'Days',"
                _SQL += " abs_reason as 'Reason', abs_source as 'Source', abs_staff_name as 'Recorded by', abs_stamp as 'Recorded On', abs_notes"
                _SQL += " from ChildAbsence"
                _SQL += " where child_id = '" & m_Child._ID.ToString & "'"
                _SQL += " order by abs_from desc"
            End If
        End If

        grdHolidays.Populate(Session.ConnectionString, _SQL)
        grdHolidays.Columns("id").Visible = False

        If radHolidays.Checked Then
            grdHolidays.Columns("notes").Visible = False
            grdHolidays.PreviewColumn = "notes"
        Else
            If radAbsence.Checked Then
                grdHolidays.Columns("abs_notes").Visible = False
                grdHolidays.PreviewColumn = "abs_notes"
            End If
        End If

        grdHolidays.AutoSizeColumns()

    End Sub

    Private Sub DisplayAnnualPatterns()

        Dim _SQL As String = ""

        _SQL = "select id, active as 'Active', date_from as 'Date From', date_to as 'Date To', date_invoiced as 'Invoiced'," &
               " split_type as 'Split Type', split_total as 'Split Amount', annual_total as 'Annual Amount'" &
               " from ChildAnnual" &
               " where child_id = '" & m_Child._ID.ToString & "'" &
               " order by date_from desc"

        grdAnnual.Populate(Session.ConnectionString, _SQL)
        grdAnnual.Columns("id").Visible = False

        grdAnnual.AutoSizeColumns()

    End Sub

    Private Sub DisplaySessionsGrid()

        grdPatterns.Clear()

        Dim _SQL As String = ""

        _SQL = "select count(*) from ChildAdvanceSessions where child_id = '" + m_Child._ID.ToString + "' and mode = 'Daily'"
        Dim _HasMulti As Boolean = RecordCounts.HasRecords(_SQL)
        radSessMulti.HighlightToggle(_HasMulti)

        _SQL = "select count(*) from ChildAdvanceSessions where child_id = '" + m_Child._ID.ToString + "' and mode = 'Weekly'"
        Dim _HasSingle As Boolean = RecordCounts.HasRecords(_SQL)
        radSessSingle.HighlightToggle(_HasSingle)

        _SQL = ""

        If radSessMulti.Checked Then

            _SQL += "select id, substring(booking_status, 1, 1) as ' ', date_from as 'From', datename(weekday,date_from) as 'Weekday',"
            _SQL += " pattern_type as 'Type', recurring_scope,"

            If radSessAll.Checked OrElse radSessRecurring.Checked Then _SQL += " recurring_scope_desc as 'Scope',"

            _SQL += " monday_desc as 'Session 1',"
            _SQL += " (select colour from Tariffs where ID = monday) as '1',"
            _SQL += " tuesday_desc  as 'Session 2',"
            _SQL += " (select colour from Tariffs where ID = tuesday) as '2',"
            _SQL += " wednesday_desc as 'Session 3',"
            _SQL += " (select colour from Tariffs where ID = wednesday) as '3',"
            _SQL += " thursday_desc as 'Session 4',"
            _SQL += " (select colour from Tariffs where ID = thursday) as '4',"
            _SQL += " friday_desc as 'Session 5',"
            _SQL += " (select colour from Tariffs where ID = friday) as '5',"
            _SQL += " monday_fund, tuesday_fund, wednesday_fund, thursday_fund, friday_fund,"
            _SQL += " monday_bo, tuesday_bo, wednesday_bo, thursday_bo, friday_bo,"
            _SQL += " comments"
            _SQL += " from ChildAdvanceSessions"
            _SQL += " where child_id = '" & m_Child._ID.ToString & "'"
            _SQL += " and mode = 'Daily'"

        Else

            _SQL += "select id, substring(booking_status, 1, 1) as ' ', date_from as 'W/C',"
            _SQL += " pattern_type as 'Type', recurring_scope,"

            If radSessAll.Checked OrElse radSessRecurring.Checked Then _SQL += " recurring_scope_desc as 'Scope',"

            _SQL += " monday_desc as 'Monday',"
            _SQL += " (select colour from Tariffs where ID = monday) as 'colour_monday',"
            _SQL += " tuesday_desc  as 'Tuesday',"
            _SQL += " (select colour from Tariffs where ID = tuesday) as 'colour_tuesday',"
            _SQL += " wednesday_desc as 'Wednesday',"
            _SQL += " (select colour from Tariffs where ID = wednesday) as 'colour_wednesday',"
            _SQL += " thursday_desc as 'Thursday',"
            _SQL += " (select colour from Tariffs where ID = thursday) as 'colour_thursday',"
            _SQL += " friday_desc as 'Friday',"
            _SQL += " (select colour from Tariffs where ID = friday) as 'colour_friday',"
            _SQL += " monday_fund, tuesday_fund, wednesday_fund, thursday_fund, friday_fund,"
            _SQL += " monday_bo, tuesday_bo, wednesday_bo, thursday_bo, friday_bo,"
            _SQL += " comments"
            _SQL += " from ChildAdvanceSessions"
            _SQL += " where child_id = '" & m_Child._ID.ToString & "'"
            _SQL += " and mode = 'Weekly'"

        End If

        If radSessRecurring.Checked Then _SQL += " and pattern_type = 'Recurring'"
        If radSessAdditional.Checked Then _SQL += " and pattern_type = 'Additional'"
        If radSessOverride.Checked Then _SQL += " and pattern_type = 'Override'"

        _SQL += " order by date_from desc"

        grdPatterns.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub btnAdvAdd_Click(sender As Object, e As System.EventArgs) Handles btnAdvAdd.Click
        AddItem()
    End Sub

    Private Sub btnAdvEdit_Click(sender As Object, e As System.EventArgs) Handles btnAdvEdit.Click
        EditItem()
    End Sub

    Private Sub btnAdvRemove_Click(sender As Object, e As System.EventArgs) Handles btnAdvRemove.Click
        RemoveItem()
    End Sub

    Private Sub grdPatterns_AfterPopulate(sender As Object, e As EventArgs) Handles grdPatterns.AfterPopulate

        grdPatterns.Columns("id").Visible = False
        grdPatterns.Columns("recurring_scope").Visible = False

        grdPatterns.Columns("monday_fund").Visible = False
        grdPatterns.Columns("tuesday_fund").Visible = False
        grdPatterns.Columns("wednesday_fund").Visible = False
        grdPatterns.Columns("thursday_fund").Visible = False
        grdPatterns.Columns("friday_fund").Visible = False

        grdPatterns.Columns("monday_bo").Visible = False
        grdPatterns.Columns("tuesday_bo").Visible = False
        grdPatterns.Columns("wednesday_bo").Visible = False
        grdPatterns.Columns("thursday_bo").Visible = False
        grdPatterns.Columns("friday_bo").Visible = False

        If radSessSingle.Checked Then
            grdPatterns.Columns("colour_monday").Visible = False
            grdPatterns.Columns("colour_tuesday").Visible = False
            grdPatterns.Columns("colour_wednesday").Visible = False
            grdPatterns.Columns("colour_thursday").Visible = False
            grdPatterns.Columns("colour_friday").Visible = False
        Else
            If radSessMulti.Checked Then
                grdPatterns.Columns("1").Visible = False
                grdPatterns.Columns("2").Visible = False
                grdPatterns.Columns("3").Visible = False
                grdPatterns.Columns("4").Visible = False
                grdPatterns.Columns("5").Visible = False
            End If
        End If

        grdPatterns.Columns("comments").Visible = False
        grdPatterns.PreviewColumn = "comments"

        grdPatterns.AutoSizeColumns()
        ToggleSessionButtons(True)

    End Sub

    Private Sub grdPatterns_RowCellStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs) Handles grdPatterns.RowCellStyle

        Dim _Colour As Drawing.Color = Nothing
        If radSessMulti.Checked Then
            If e.Column.FieldName.EndsWith("1") AndAlso ReturnTariffColour(e, _Colour) Then e.Appearance.BackColor = _Colour
            If e.Column.FieldName.EndsWith("2") AndAlso ReturnTariffColour(e, _Colour) Then e.Appearance.BackColor = _Colour
            If e.Column.FieldName.EndsWith("3") AndAlso ReturnTariffColour(e, _Colour) Then e.Appearance.BackColor = _Colour
            If e.Column.FieldName.EndsWith("4") AndAlso ReturnTariffColour(e, _Colour) Then e.Appearance.BackColor = _Colour
            If e.Column.FieldName.EndsWith("5") AndAlso ReturnTariffColour(e, _Colour) Then e.Appearance.BackColor = _Colour
        Else
            If e.Column.FieldName.ToLower = "monday" AndAlso ReturnTariffColour(e, _Colour) Then e.Appearance.BackColor = _Colour
            If e.Column.FieldName.ToLower = "tuesday" AndAlso ReturnTariffColour(e, _Colour) Then e.Appearance.BackColor = _Colour
            If e.Column.FieldName.ToLower = "wednesday" AndAlso ReturnTariffColour(e, _Colour) Then e.Appearance.BackColor = _Colour
            If e.Column.FieldName.ToLower = "thursday" AndAlso ReturnTariffColour(e, _Colour) Then e.Appearance.BackColor = _Colour
            If e.Column.FieldName.ToLower = "friday" AndAlso ReturnTariffColour(e, _Colour) Then e.Appearance.BackColor = _Colour
        End If

    End Sub

    Private Function ReturnTariffColour(ByVal e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs, ByRef ReturnColour As Drawing.Color) As Boolean

        Dim _Return As Boolean = False
        ReturnColour = Nothing

        Try

            Dim _ColumnName As String = ""
            If radSessMulti.Checked Then
                _ColumnName = e.Column.FieldName.Last.ToString
            Else
                _ColumnName = "colour_" + e.Column.FieldName.ToLower
            End If

            Dim _ColourString As String = grdPatterns.UnderlyingGridView.GetRowCellDisplayText(e.RowHandle, _ColumnName)

            If _ColourString <> "" Then
                ReturnColour = Drawing.Color.FromName(_ColourString)
                _Return = True
            End If

        Catch ex As Exception
            'error trap
        End Try

        Return _Return

    End Function

    Private Sub DisplayInvoices()

        Dim _SQL As String = ""

        If radInvoices.Checked OrElse radInvoicesPosted.Checked Then

            _SQL += "select i.id, invoice_ref as 'Ref', invoice_status as 'Status', invoice_date as 'Date', invoice_due_date as 'Due', invoice_period as 'Period From', invoice_period_end as 'Period To',"
            _SQL += " invoice_sub as 'Sub', invoice_discount as 'Discount', invoice_total as 'Total' from Invoices i"
            _SQL += " left join InvoiceBatch b on b.ID = i.batch_id"
            _SQL += " where child_id = '" + m_Child._ID.ToString + "'"
            _SQL += " and b.batch_period not in ('Annual Invoice', 'Annual Segments')"
            _SQL += " and b.batch_status <> 'Abandoned'"

            If radInvoicesPosted.Checked Then
                _SQL += " and i.invoice_status = 'Posted'"
            Else
                _SQL += " and i.invoice_status <> 'Posted'"
            End If

            _SQL += " order by b.batch_no desc, i.invoice_no desc"

        Else

            _SQL += "select c.id, credit_ref as 'Ref', credit_status as 'Status', credit_date as 'Date', credit_due as 'Due', credit_total as 'Total' from Credits c"
            _SQL += " left join CreditBatch b on b.ID = c.batch_id"
            _SQL += " where child_id = '" + m_Child._ID.ToString + "'"
            _SQL += " and b.batch_status <> 'Abandoned'"

            If radPostedCredits.Checked Then
                _SQL += " and c.credit_status = 'Posted'"
            Else
                _SQL += " and c.credit_status <> 'Posted'"
            End If

            _SQL += " order by b.batch_no desc, c.credit_no desc"

        End If

        cgInvoices.Populate(Session.ConnectionString, _SQL)
        cgInvoices.Columns("id").Visible = False

        cgInvoices.AutoSizeColumns()

    End Sub

#End Region

#Region "Bookings and Register Tab"

    Private Sub PopulateBookings()
        m_Bookings.Clear()
        m_Bookings = Business.Booking.RetreiveByChildID(m_Child._ID.Value)
    End Sub

    Private Sub DisplayBookings()

        Me.Cursor = Cursors.WaitCursor

        Me.ToolbarMode = ToolbarEnum.FindOnly

        If radBookCalendar.Checked Then

            yvBookings.Show()
            cgBookings.Hide()

            If m_Bookings.Count = 0 Then PopulateBookings()

            Dim _YearViewStartDate As Date = DateSerial(Today.Year, Today.Month, 1)

            'if the child has not started yet, we default the view to their start date
            If m_Child._Started.HasValue Then
                If m_Child._Started.Value > Today Then
                    _YearViewStartDate = DateSerial(m_Child._Started.Value.Year, m_Child._Started.Value.Month, 1)
                End If
            End If

            yvBookings.Populate(m_Bookings, _YearViewStartDate)

        Else

            If radBookActual.Checked Then
                DisplayBookingsGrid()
            Else
                DisplayRegister()
            End If

            cgBookings.Show()
            yvBookings.Hide()

        End If

        If radBookActual.Checked Then
            btnBuildBookings.Visible = True
        Else
            btnBuildBookings.Visible = False
        End If

        Me.Cursor = Cursors.Default

    End Sub

    Private Sub DisplayBookingsGrid()

        Dim _SQL As String = ""

        _SQL += "select b.id, b.booking_status as 'Status', b.booking_date as 'Date',"
        _SQL += " b.times as 'Times', hours as 'Hrs', funded_hours as 'Funded Hrs',"
        _SQL += " r.room_name as 'Room', b.tariff_name as 'Tariff', b.tariff_rate as 'Rate', t.colour as '!colour'"
        _SQL += " from Bookings b"
        _SQL += " left join Tariffs t on t.ID = b.tariff_id"
        _SQL += " left join SiteRoomRatios rr on rr.ID = b.ratio_id"
        _SQL += " left join SiteRooms r on r.ID = rr.room_id"
        _SQL += " where child_id = '" & m_Child._ID.ToString & "'"
        _SQL += " order by b.booking_date"

        cgBookings.Populate(Session.ConnectionString, _SQL)
        cgBookings.Columns("id").Visible = False

    End Sub

    Private Sub DisplayRegister()

        Dim _SQL As String = Business.Register.ReturnChildRegister(m_Child._ID.Value)
        cgBookings.Populate(Session.ConnectionString, _SQL)

        cgBookings.Columns("In").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        cgBookings.Columns("In").DisplayFormat.FormatString = "HH:mm"

        cgBookings.Columns("Out").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        cgBookings.Columns("Out").DisplayFormat.FormatString = "HH:mm"

    End Sub

    Private Sub radBookActual_CheckedChanged(sender As Object, e As EventArgs) Handles radBookActual.CheckedChanged
        DisplayBookings()
    End Sub

    Private Sub radBookRegister_CheckedChanged(sender As Object, e As EventArgs) Handles radBookRegister.CheckedChanged
        DisplayBookings()
    End Sub

    Private Sub btnBuildBookings_Click(sender As Object, e As EventArgs) Handles btnBuildBookings.Click

        If radBookActual.Checked Then

            If CareMessage("Are you sure you want to Rebuild the bookings data?", MessageBoxIcon.Question, MessageBoxButtons.YesNo, "Confirm Rebuid") = DialogResult.Yes Then

                btnBuildBookings.Enabled = False
                Session.CursorWaiting()

                m_Child.BuildBookings()
                PopulateBookings()

            End If

            DisplayBookings()

            btnBuildBookings.Enabled = True
            Session.CursorDefault()

        End If

    End Sub

    Private Sub radBookCalendar_CheckedChanged(sender As Object, e As EventArgs) Handles radBookCalendar.CheckedChanged
        DisplayBookings()
    End Sub

#End Region

#Region "Assessment Tab"

    Private Sub DisplayAssessment()

        Dim _SQL As String = ""

        _SQL = "select CDAPDefinitions.ID as 'definition_id' , '(' + cast(CDAPAreas.Seq as VarChar(3)) + ') ' + CDAPAreas.Name as 'Area'," &
               " CDAPDefinitions.step as 'Step', CDAPDefinitions.letter as 'Letter', CDAPDefinitions.name as 'Assessment'," &
               AssessmentSubQuery("id", "id") + "," &
               AssessmentSubQuery("date", "Date Attained") + "," &
               AssessmentSubQuery("staff_name", "Staff") &
               " from CDAPDefinitions" &
               " left join CDAPAreas on CDAPAreas.name = CDAPDefinitions.area"

        If radCDAPAttained.Checked Then
            _SQL += " where " + AssessmentSubQuery("date", "") + " is not null"
        End If

        If radCDAPNotAttained.Checked Then
            _SQL += " where " + AssessmentSubQuery("date", "") + " is null"
        End If

        _SQL += " order by CDAPAreas.seq, step, letter"

        grdAssessment.HideFirstColumn = True
        grdAssessment.Populate(Session.ConnectionString, _SQL)

        grdAssessment.UnderlyingGridView.Columns("id").Visible = False
        grdAssessment.UnderlyingGridView.Columns("Area").GroupIndex = 0
        grdAssessment.UnderlyingGridView.Columns("Step").GroupIndex = 1

        grdAssessment.AutoSizeColumns()

    End Sub

    Private Function AssessmentSubQuery(ByVal FieldName As String, ByVal FieldAlias As String) As String

        Dim _Return As String = ""

        _Return = "(select " + FieldName + " from ChildCDAP where ChildCDAP.child_id = '" + m_Child._ID.ToString + "'" &
                  " and ChildCDAP.cdap_id = CDAPDefinitions.ID)"

        If FieldAlias <> "" Then
            _Return += " as '" + FieldAlias + "'"
        End If

        Return _Return

    End Function

    Private Sub radCDAPAll_CheckedChanged(sender As Object, e As EventArgs) Handles radCDAPAll.CheckedChanged
        DisplayAssessment()
    End Sub

    Private Sub radCDAPAttained_CheckedChanged(sender As Object, e As EventArgs) Handles radCDAPAttained.CheckedChanged
        DisplayAssessment()
    End Sub

    Private Sub radCDAPNotAttained_CheckedChanged(sender As Object, e As EventArgs) Handles radCDAPNotAttained.CheckedChanged
        DisplayAssessment()
    End Sub

    Private Sub btnPrintProfile_Click(sender As Object, e As EventArgs) Handles btnPrintProfile.Click

        Dim _SQL As String = "select Children.fullname, Children.keyworker_name, CDAPDefinitions.name, CDAPDefinitions.description," &
                     " ChildCDAP.date, ChildCDAP.comments, ChildCDAP.staff_name," &
                     " '" + Session.ReportFolder + "' + ChildCDAP.photo_ref as 'photo_path'" &
                     " from ChildCDAP" &
                     " left join CDAPDefinitions on CDAPDefinitions.ID = ChildCDAP.cdap_id" &
                     " left join Children on Children.ID = ChildCDAP.child_id"

        Dim _RH As New ReportHandler
        _RH.DXReport = True
        _RH.ReportFile = "ChildCDAP.repx"
        _RH.ReportSQL = _SQL
        _RH.RunReport(True)

    End Sub

    Private Sub grdAssessment_GridDoubleClick(sender As Object, e As EventArgs) Handles grdAssessment.GridDoubleClick

        If grdAssessment.CurrentRow Is Nothing Then Exit Sub
        Dim _ID As String = grdAssessment.CurrentRow.Item("id").ToString
        If _ID <> "" Then
            CDAP.DisplayCDAPDetail(New Guid(_ID))
        End If

    End Sub

#End Region

#Region "Medical Tab"

    Private Sub DisplayMedicineAndIncidents()
        btnMedPrintAll.Hide()

        If radMedCalendar.Checked Then
            yvMedInc.Show()
            DisplayMedicationCalendar()
        Else
            yvMedInc.Hide()
            If radMedRequests.Checked Then DisplayMedicineRequests()
            If radMedGiven.Checked Then DisplayMedicineLog()
            If radMedAccidents.Checked Then DisplayAccidents()
            If radMedIncidents.Checked Then DisplayIncidents()
            If radMedPriorIncidents.Checked Then DisplayPriorIncidents()
        End If

    End Sub

    Private Sub DisplayMedicationCalendar()

        yvMedInc.Clear()

        Dim _MedIncData As New List(Of Business.MedicineIncidents)
        Dim _SevenMonthsAgo As Date = Today.AddMonths(-7)
        _SevenMonthsAgo = DateSerial(_SevenMonthsAgo.Year, _SevenMonthsAgo.Month, 1)

        Dim _SQL As String = ""

        'fetch the incidents
        _SQL += "select incident_type, injury, stamp from Incidents"
        _SQL += " where child_id = '" + m_Child._ID.ToString + "'"
        _SQL += " and stamp >= '" + Format(_SevenMonthsAgo, "yyyy-MM-dd 00:00:00.000") + "'"
        _SQL += " order by stamp"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then
            For Each _DR As DataRow In _DT.Rows
                Dim _MI As New Business.MedicineIncidents
                _MI.ActionDate = ValueHandler.ConvertDateOmitTime(_DR.Item("stamp")).Value
                _MI.ItemType = _DR.Item("incident_type").ToString
                _MI.Tooltip = _DR.Item("injury").ToString
                _MedIncData.Add(_MI)
            Next
        End If

        'fetch the medication
        _SQL = ""
        _SQL += "select due, auth_id, illness, medicine from MedicineLog"
        _SQL += " where child_id = '" + m_Child._ID.ToString + "'"
        _SQL += " and due >= '" + Format(_SevenMonthsAgo, "yyyy-MM-dd 00:00:00.000") + "'"
        _SQL += " order by due"

        _DT = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then
            For Each _DR As DataRow In _DT.Rows

                Dim _MI As New Business.MedicineIncidents
                _MI.ActionDate = ValueHandler.ConvertDateOmitTime(_DR.Item("due")).Value

                Dim _AuthID As String = _DR.Item("auth_id").ToString
                If _AuthID <> "" Then
                    _MI.ItemType = "Prior Authorised Medication"
                Else
                    _MI.ItemType = "Ad-Hoc Medication"
                End If

                _MI.Tooltip = _DR.Item("illness").ToString + " - " + _DR.Item("medicine").ToString

                _MedIncData.Add(_MI)

            Next
        End If

        'fetch the medication
        _SQL = ""
        _SQL += "select abs_from, abs_to, datediff(DAY, abs_from, abs_to) + 1 as 'duration', abs_reason, abs_notes from ChildAbsence"
        _SQL += " where child_id = '" + m_Child._ID.ToString + "'"
        _SQL += " and abs_from >= '" + Format(_SevenMonthsAgo, "yyyy-MM-dd 00:00:00.000") + "'"
        _SQL += " order by abs_from"

        _DT = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then
            For Each _DR As DataRow In _DT.Rows

                If ValueHandler.ConvertInteger(_DR.Item("duration")) > 1 Then
                    Dim _Day As Date = ValueHandler.ConvertDate(_DR.Item("abs_from")).Value
                    While _Day <= ValueHandler.ConvertDate(_DR.Item("abs_to")).Value
                        Dim _MI As New Business.MedicineIncidents
                        _MI.ActionDate = _Day
                        _MI.ItemType = "Absence"
                        _MI.Tooltip = _DR.Item("abs_reason").ToString + " - " + _DR.Item("abs_notes").ToString
                        _MedIncData.Add(_MI)
                        _Day = _Day.AddDays(1)
                    End While
                Else

                    Dim _MI As New Business.MedicineIncidents
                    _MI.ActionDate = ValueHandler.ConvertDateOmitTime(_DR.Item("abs_from")).Value
                    _MI.ItemType = "Absence"
                    _MI.Tooltip = _DR.Item("abs_reason").ToString + " - " + _DR.Item("abs_notes").ToString

                    _MedIncData.Add(_MI)

                End If

            Next
        End If

        yvMedInc.Populate(_MedIncData, _SevenMonthsAgo)

    End Sub

    Private Sub DisplayMedicineLog()

        btnMedPrintAll.Show()
        btnMedPrint.Text = "Print selected Log"

        Dim _SQL As String = "Select id, actual as 'Actual Time', illness as 'Illness', dosage as 'Dosage'," &
                             " medicine as 'Medicine', staff_name as 'Staff', witness_name as 'Witness'" &
                             " from MedicineLog" &
                             " where child_id = '" & m_Child._ID.Value.ToString & "'" &
                             " and auth_id is null" &
                             " and actual is not null" &
                             " order by actual desc"

        grdMedical.Populate(Session.ConnectionString, _SQL)

        grdMedical.Columns(0).Visible = False
        grdMedical.Columns("Actual Time").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        grdMedical.Columns("Actual Time").DisplayFormat.FormatString = "dd/MM/yyyy HH:mm"

        grdMedical.AutoSizeColumns()

    End Sub

    Private Sub DisplayMedicineRequests()

        btnMedPrint.Text = "Print selected Request"

        Dim _SQL As String = "select id, illness as 'Illness', medicine as 'Medicine', dosage as 'Dosage'," &
                             " last_given as 'Last Given', frequency as 'Frequency'," &
                             " contact_name as 'Contact Name', staff_name as 'Staff', stamp as 'Stamp'" &
                             " from MedicineAuth" &
                             " where child_id = '" & m_Child._ID.Value.ToString & "'" &
                             " order by stamp desc"

        grdMedical.Populate(Session.ConnectionString, _SQL)

        grdMedical.Columns(0).Visible = False
        grdMedical.Columns("Stamp").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        grdMedical.Columns("Stamp").DisplayFormat.FormatString = "dd/MM/yyyy HH:mm"

        grdMedical.AutoSizeColumns()

    End Sub

    Private Sub DisplayAccidents()
        DisplayIncidentsGrid("Accident")
    End Sub

    Private Sub DisplayPriorIncidents()
        DisplayIncidentsGrid("Prior Injury")
    End Sub

    Private Sub DisplayIncidents()
        DisplayIncidentsGrid("Incident")
    End Sub

    Private Sub DisplayIncidentsGrid(ByVal IncidentType As String)

        btnMedPrint.Enabled = True
        btnMedPrint.Text = "Print Incident Report"

        Dim _SQL As String = ""
        _SQL += "select id, details as 'Details', location as 'location', injury as 'Injury Suffered',"
        _SQL += " treatment as 'Treatment Given', action as 'Action Taken',"
        _SQL += " staff_name as 'Staff', stamp as 'Stamp'"
        _SQL += " from Incidents"
        _SQL += " where child_id = '" & m_Child._ID.Value.ToString & "'"
        _SQL += " and incident_type = '" & IncidentType & "'"
        _SQL += " order by stamp desc"

        grdMedical.Populate(Session.ConnectionString, _SQL)

        grdMedical.Columns(0).Visible = False
        grdMedical.Columns("Stamp").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        grdMedical.Columns("Stamp").DisplayFormat.FormatString = "dd/MM/yyyy HH:mm"

        grdMedical.AutoSizeColumns()

    End Sub

    Private Sub MedicalPrint(ByVal DesignReport As Boolean)

        If grdMedical Is Nothing Then Exit Sub
        If grdMedical.RecordCount < 1 Then Exit Sub

        If grdMedical.CurrentRow(0).ToString <> "" Then

            btnMedPrint.Enabled = False

            Dim _ID As String = grdMedical.CurrentRow(0).ToString

            If radMedRequests.Checked Or radMedGiven.Checked Then

                Dim _Mode As Medical.EnumLogOrRequest = Medical.EnumLogOrRequest.Log
                If radMedRequests.Checked Then _Mode = Medical.EnumLogOrRequest.Request

                If DesignReport Then
                    Medical.DesignReport(_Mode, _ID)
                Else
                    Medical.PrintReport(_Mode, _ID)
                End If

            Else
                If DesignReport Then
                    ReportHandler.RepositoryDesignReport("Incident Report", " and i.ID = '" + _ID + "'")
                Else
                    ReportHandler.RepositoryReportPreview("Incident Report", " and i.ID = '" + _ID + "'")
                End If
            End If

            btnMedPrint.Enabled = True

        End If

    End Sub

    Private Sub grdMedical_GridDoubleClick(sender As Object, e As EventArgs) Handles grdMedical.GridDoubleClick
        MedicalPrint(False)
    End Sub

    Private Sub radMedRequests_Click(sender As Object, e As EventArgs) Handles radMedRequests.CheckedChanged
        DisplayMedicineAndIncidents()
    End Sub

    Private Sub radMedGiven_Click(sender As Object, e As EventArgs) Handles radMedGiven.CheckedChanged
        DisplayMedicineAndIncidents()
    End Sub

    Private Sub radAccidents_Click(sender As Object, e As EventArgs) Handles radMedAccidents.CheckedChanged
        DisplayMedicineAndIncidents()
    End Sub

    Private Sub radMedIncidents_CheckedChanged(sender As Object, e As EventArgs) Handles radMedIncidents.CheckedChanged
        DisplayMedicineAndIncidents()
    End Sub

    Private Sub radMedPriorIncidents_CheckedChanged(sender As Object, e As EventArgs) Handles radMedPriorIncidents.CheckedChanged
        DisplayMedicineAndIncidents()
    End Sub

    Private Sub btnMedPrint_Click(sender As Object, e As EventArgs) Handles btnMedPrint.Click
        MedicalPrint(btnMedPrint.ShiftDown)
    End Sub

#End Region

#Region "Daily Reports Tab"

    Private Sub DisplayReports()

        Dim _SQL As String = ""

        _SQL += "select r.day_id, r.date as 'Report Date', datename(WEEKDAY, r.date) as 'Day', r.first_stamp as 'Arrived', r.last_stamp as 'Left',"
        _SQL += " (select count(*) from Incidents i where i.day_id = r.day_id and i.child_id = r.person_id) as 'Incidents',"
        _SQL += " (select count(*) from MedicineAuth ma where ma.day_id = r.day_id and ma.child_id = r.person_id) as 'Medicine Requests',"
        _SQL += " (select count(*) from MedicineLog ml where ml.day_id = r.day_id and ml.child_id = r.person_id and ml.actual is not null) as 'Medicine Logged'"
        _SQL += " from RegisterSummary r"
        _SQL += " where r.person_id = '" & m_Child._ID.ToString & "'"
        _SQL += " order by r.date desc"

        grdReports.HideFirstColumn = True
        grdReports.Populate(Session.ConnectionString, _SQL)

        grdReports.Columns("Arrived").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        grdReports.Columns("Arrived").DisplayFormat.FormatString = "HH:mm:ss"
        grdReports.Columns("Left").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        grdReports.Columns("Left").DisplayFormat.FormatString = "HH:mm:ss"

        grdReports.AutoSizeColumns()

    End Sub

    Private Sub grdReports_GridDoubleClick(sender As System.Object, e As System.EventArgs) Handles grdReports.GridDoubleClick
        ShowReport()
    End Sub

    Private Sub grdReports_RowCellStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs) Handles grdReports.RowCellStyle

        Dim _Colour As Drawing.Color = Nothing

        Select Case e.Column.FieldName

            Case "Incidents"
                Dim _Count As Integer = ValueHandler.ConvertInteger(e.CellValue)
                If _Count > 0 Then e.Appearance.BackColor = txtRed.BackColor

            Case "Medicine Requests", "Medicine Logged"
                Dim _Count As Integer = ValueHandler.ConvertInteger(e.CellValue)
                If _Count > 0 Then e.Appearance.BackColor = txtYellow.BackColor

        End Select

    End Sub

    Private Function SelectedDate() As Date?

        If grdReports Is Nothing Then Return Nothing
        If grdReports.RecordCount < 1 Then Return Nothing
        If grdReports.CurrentRow("Report Date").ToString = "" Then Return Nothing

        Return ValueHandler.ConvertDate(grdReports.CurrentRow("Report Date"))

    End Function

    Private Sub ShowReport()

        Dim _ReportDate As Date? = SelectedDate()
        If _ReportDate.HasValue Then

            Session.CursorWaiting()

            Dim _Day As Business.Day = Business.Day.RetreiveByDateAndSite(_ReportDate.Value, m_Child._SiteId.Value)
            If _Day IsNot Nothing Then
                Dim _DailyReport As New Reports.DailyReport(Reports.DailyReport.EnumRunMode.ForcedPrint, _ReportDate.Value, m_Child._SiteId.Value, "", m_Child._ID.ToString, True)
                _DailyReport.Run()
            End If

            Session.CursorDefault()

        End If

    End Sub

    Private Sub btnPrint_Click(sender As System.Object, e As System.EventArgs) Handles btnPrint.Click

        btnPrint.Enabled = False

        If btnPrint.ShiftDown Then

            Session.CursorWaiting()

            Dim _EmptyData As New List(Of DailyReportData)
            ReportHandler.DesignReport("XtraDailyReport.repx", _EmptyData)

            Session.CursorDefault()

        Else
            ShowReport()
        End If

        btnPrint.Enabled = True

    End Sub

    Private Sub btnEmail_Click(sender As System.Object, e As System.EventArgs) Handles btnEmail.Click

        btnEmail.Enabled = False
        Me.Cursor = Cursors.WaitCursor

        Dim _ReportDate As Date? = SelectedDate()
        If _ReportDate.HasValue Then
            Dim _DailyReport As New Reports.DailyReport(Reports.DailyReport.EnumRunMode.ForcedEmail, _ReportDate.Value, m_Child._SiteId.Value, "", m_Child._ID.ToString, True)
            _DailyReport.Run()
        End If

        btnEmail.Enabled = True
        Me.Cursor = Cursors.Default

    End Sub

#End Region

#Region "Activity Tab"

    Private Sub DisplayActivity()

        Dim _SQL As String = "select description as 'Description', value_2 as 'Ref 2', value_3 as 'Ref 3'," &
                             " staff_name as 'Staff', stamp as 'Stamp'" &
                             " from Activity" &
                             " where key_id = '" & m_Child._ID.Value.ToString & "'" &
                             " order by stamp desc"

        grdActivity.Populate(Session.ConnectionString, _SQL)

        grdActivity.Columns("Stamp").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        grdActivity.Columns("Stamp").DisplayFormat.FormatString = "dd/MM/yyyy HH:mm"

        grdActivity.AutoSizeColumns()

    End Sub

#End Region

#Region "Calendar Stuff"

    Private Class CalendarData

        Public Property BookingStart As Date
        Public Property BookingEnd As Date
        Public Property Subject As String
        Public Property Location As String
        Public Property AllDay As Boolean
        Public Property Notes As String
        Public Property Label As String

    End Class

    Private Sub SchedulerControl1_AppointmentViewInfoCustomizing(sender As Object, e As DevExpress.XtraScheduler.AppointmentViewInfoCustomizingEventArgs)
        Try
            e.ViewInfo.Appearance.BackColor = Drawing.Color.FromName(e.ViewInfo.Appointment.Location)
        Catch ex As Exception

        End Try
    End Sub

#End Region

    Private Sub btnLabelTablemats_Click(sender As Object, e As EventArgs) Handles btnLabelTablemats.Click

        Dim _SQL As String = ""
        _SQL += "select c.id, forename, surname, fullname, dob, gender, group_name, keyworker_name,"
        _SQL += " off_menu, diet_restrict, diet_notes, allergy_rating, med_allergies, med_medication, med_notes, Data"
        _SQL += " from Children c"
        _SQL += " left join AppDocs d on d.id = c.photo"
        _SQL += " where c.ID = '" + m_Child._ID.Value.ToString + "'"

        If btnPrint.ShiftDown Then
            btnPrint.Enabled = False
            ReportHandler.DesignReport("ChildLabels.repx", Session.ConnectionString, _SQL)
            btnPrint.Enabled = True
        Else
            btnPrint.Enabled = False
            ReportHandler.RunReport("ChildLabels.repx", Session.ConnectionString, _SQL)
            btnPrint.Enabled = True
        End If

    End Sub

    Private Sub tcSessions_SelectedPageChanged(sender As Object, e As DevExpress.XtraTab.TabPageChangedEventArgs) Handles tcSessions.SelectedPageChanged
        HandleSessionsTab()
    End Sub

    Private Sub HandleSessionsTab()

        If m_Child Is Nothing Then Exit Sub

        tcSessions.ClearHighlightedPages()
        Me.RecordCounts.Populate(m_Child._ID.Value)

        If Me.RecordCounts.ChildAnnual Then tcSessions.HighlightPage("tabSessAnnual")
        If Me.RecordCounts.ChildCalculation Then tcSessions.HighlightPage("tabSessAnnual")
        If Me.RecordCounts.ChildWeekMonth Then tcSessions.HighlightPage("tabSessMonthly")
        If Me.RecordCounts.ChildHolidays Then tcSessions.HighlightPage("tabSessHolidays")
        If Me.RecordCounts.ChildCharges Then tcSessions.HighlightPage("tabSessOther")
        If Me.RecordCounts.ChildDeposits Then tcSessions.HighlightPage("tabSessDeposits")

        Select Case tcSessions.SelectedTabPage.Name

            Case "tabSessPatterns"
                DisplaySessionsGrid()

            Case "tabSessProfile"
                DisplayInvoiceProfile()

            Case "tabSessAnnual"
                DisplayAnnualCalculations()

            Case "tabSessMonthly"
                DisplayMonthlyCalculations()

            Case "tabSessHolidays"
                DisplayHolidays()

            Case "tabSessOther"
                DisplayCharges()

            Case "tabSessDeposits"
                DisplayDeposits()

            Case "tabInvoices"
                DisplayInvoices()

        End Select

    End Sub

    Private Sub DisplayInvoiceProfile()

        cbxDiscount.SelectedValue = m_Child._Discount.ToString
        cbxPaymentDue.Text = m_Child._InvoiceDue
        cbxPaymentFreq.Text = m_Child._InvoiceFreq
        cbxPaymentMethod.Text = m_Child._PaymentMethod
        txtPaymentRef.Text = m_Child._PaymentRef

        cbxTermType.Text = m_Child._TermType
        chkTermTime.Checked = m_Child._TermOnly

        cbxFundType.Text = m_Child._FundingType
        txtFundRef.Text = m_Child._FundingRef

        cbxNLCode.Text = m_Child._NlCode
        cbxNLTracking.Text = m_Child._NlTracking

        cbxVoucherMode.Text = m_Child._VoucherMode

        If m_Child._VoucherMode = "Fixed Percentage" OrElse m_Child._VoucherMode = "Fixed Value" Then
            txtVoucherProportion.Text = ValueHandler.MoneyAsText(m_Child._VoucherProportion)
        Else
            txtVoucherProportion.Text = ""
        End If

        txtVoucherSequence.Text = m_Child._VoucherSeq.ToString

    End Sub

    Private Sub radSessRecurring_CheckedChanged(sender As Object, e As EventArgs) Handles radSessRecurring.CheckedChanged
        If radSessRecurring.Checked Then DisplaySessionsGrid()
    End Sub

    Private Sub radSessAll_CheckedChanged(sender As Object, e As EventArgs) Handles radSessAll.CheckedChanged
        If radSessAll.Checked Then DisplaySessionsGrid()
    End Sub

    Private Sub radSessOverride_CheckedChanged(sender As Object, e As EventArgs) Handles radSessOverride.CheckedChanged
        If radSessOverride.Checked Then DisplaySessionsGrid()
    End Sub

    Private Sub radSessAdditional_CheckedChanged(sender As Object, e As EventArgs) Handles radSessAdditional.CheckedChanged
        If radSessAdditional.Checked Then DisplaySessionsGrid()
    End Sub

    Private Sub radSessSingle_CheckedChanged(sender As Object, e As EventArgs) Handles radSessSingle.CheckedChanged
        If radSessSingle.Checked Then DisplaySessionsGrid()
    End Sub

    Private Sub radSessMulti_CheckedChanged(sender As Object, e As EventArgs) Handles radSessMulti.CheckedChanged
        If radSessMulti.Checked Then DisplaySessionsGrid()
    End Sub

    Private Sub grdPatterns_GridDoubleClick(sender As Object, e As EventArgs) Handles grdPatterns.GridDoubleClick
        EditItem()
    End Sub

    Private Sub radAnnualManual_CheckedChanged(sender As Object, e As EventArgs) Handles radAnnualManual.CheckedChanged
        DisplayAnnualCalculations()
    End Sub

    Private Sub radAnnualPatterns_CheckedChanged(sender As Object, e As EventArgs) Handles radAnnualPatterns.CheckedChanged
        DisplayAnnualCalculations()
    End Sub

    Private Sub DisplayAnnualCalculations()

        radAnnualManual.HighlightToggle(RecordCounts.ChildAnnualManual)
        radAnnualPatterns.HighlightToggle(RecordCounts.ChildAnnualPattern)

        If radAnnualManual.Checked Then DisplayAnnualManual()
        If radAnnualPatterns.Checked Then DisplayAnnualPatterns()

        radCalcAnnual.Visible = radAnnualManual.Checked
        radCalcMonthly.Visible = radAnnualManual.Checked
        radCalcWeekly.Visible = False

    End Sub

    Private Sub DisplayAnnualManual()

        Dim _SQL As String = ""

        _SQL += "select id, date_from as 'Date From', active,"

        If radCalcAnnual.Checked Then _SQL += " total_calc_annum as 'Full per Annum', fund_calc_annum as 'Less Funding per Annum', (total_calc_annum-fund_calc_annum) as ' = Total per Annum'"
        If radCalcMonthly.Checked Then _SQL += " total_calc_month as 'Full per Month', fund_calc_month as 'Less Funding per Month', (total_calc_month-fund_calc_month) as ' = Total per Month'"

        _SQL += " from ChildCalculation"
        _SQL += " where child_id = '" & m_Child._ID.ToString & "'"
        _SQL += " order by date_from desc"

        grdAnnual.Populate(Session.ConnectionString, _SQL)
        grdAnnual.Columns("id").Visible = False
        grdAnnual.Columns("active").Visible = False

        grdAnnual.AutoSizeColumns()

    End Sub

    Private Sub grdAnnual_GridDoubleClick(sender As Object, e As EventArgs) Handles grdAnnual.GridDoubleClick
        EditItem()
    End Sub

    Private Sub grdHolidays_GridDoubleClick(sender As Object, e As EventArgs) Handles grdHolidays.GridDoubleClick
        EditItem()
    End Sub

    Private Sub grdAdditional_GridDoubleClick(sender As Object, e As EventArgs) Handles grdAdditional.GridDoubleClick
        EditItem()
    End Sub

    Private Sub grdDiscounts_GridDoubleClick(sender As Object, e As EventArgs) Handles grdDiscounts.GridDoubleClick
        EditItem()
    End Sub

    Private Sub DisplayMonthlyCalculations()

        Dim _SQL As String = ""

        _SQL += "select cwm.id, cwm.date_from as 'Date From',"
        _SQL += " cwm.days_per_week as 'Days per Week', t.name as 'Tariff', cwm.age_mode as 'Age Mode', b.description as 'Specific Band'"
        _SQL += " from ChildWeekMonth cwm"
        _SQL += " left join TariffAgeBands b on b.ID = cwm.age_id"
        _SQL += " left join Tariffs t on t.ID = cwm.tariff_id"
        _SQL += " where cwm.child_id = '" & m_Child._ID.ToString & "'"
        _SQL += " and cwm.frequency = 'Monthly'"
        _SQL += " order by cwm.date_from desc"

        grdSessMonthly.Populate(Session.ConnectionString, _SQL)
        grdSessMonthly.Columns("id").Visible = False

        grdSessMonthly.AutoSizeColumns()

    End Sub

    Private Sub grdSessMonthly_GridDoubleClick(sender As Object, e As EventArgs) Handles grdSessMonthly.GridDoubleClick
        EditItem()
    End Sub

    Private Sub RebuildBookings()

        If m_BuildModal Then

            ssm.ShowWaitForm()

            m_Child.BuildBookings()
            m_Child.BuildDayPatterns(Today)

            m_Bookings.Clear()
            PopulateBookings()
            DisplayBookings()

            ssm.CloseWaitForm()

        Else
            If Not bgwBookings.IsBusy Then
                Session.SetProgressMessage("Building Bookings...")
                bgwBookings.RunWorkerAsync()
            End If
        End If

        'reset the rebuild bookings flag
        m_RebuildBookings = False

    End Sub

    Private Sub bgwBookings_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgwBookings.DoWork
        m_Child.BuildBookings()
    End Sub

    Private Sub bgwBookings_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwBookings.RunWorkerCompleted

        Session.SetProgressMessage("Building Day Patterns...")
        m_Child.BuildDayPatterns(Today)

        m_Bookings.Clear()

        Session.SetProgressMessage("Caching Bookings Data")
        PopulateBookings()

        If tcMain.SelectedTabPage.Name = "tabCalendar" Then
            Session.SetProgressMessage("Loading Booking Pattern")
            DisplayBookings()
        End If

        Session.SetProgressMessage("Build Bookings Complete", 3)

    End Sub

    Private Sub radCalcAnnual_CheckedChanged(sender As Object, e As EventArgs) Handles radCalcAnnual.CheckedChanged
        DisplayAnnualManual()
    End Sub

    Private Sub radCalcMonthly_CheckedChanged(sender As Object, e As EventArgs) Handles radCalcMonthly.CheckedChanged
        DisplayAnnualManual()
    End Sub

    Private Sub radCalcWeekly_CheckedChanged(sender As Object, e As EventArgs) Handles radCalcWeekly.CheckedChanged
        DisplayAnnualManual()
    End Sub

    Public Class RecordCountObject

        Public Property ChildAnnual As Boolean
        Public Property ChildCalculation As Boolean
        Public Property ChildWeekMonth As Boolean
        Public Property ChildHolidays As Boolean
        Public Property ChildCharges As Boolean
        Public Property ChildDeposits As Boolean

        Public Property ChildAnnualPattern As Boolean
        Public Property ChildAnnualManual As Boolean

        Public Sub Populate(ByVal ChildID As Guid)

            Dim _ID As String = "'" + ChildID.ToString + "'"
            Dim _SQL As String = ""

            _SQL = "select count(*) from ChildAnnual where child_id = " + _ID
            ChildAnnual = HasRecords(_SQL)

            _SQL = "select count(*) from ChildCalculation where child_id = " + _ID
            ChildCalculation = HasRecords(_SQL)

            _SQL = "select count(*) from ChildWeekMonth where child_id = " + _ID
            ChildWeekMonth = HasRecords(_SQL)

            _SQL = "select count(*) from ChildHolidays where child_id = " + _ID
            ChildHolidays = HasRecords(_SQL)

            If ChildHolidays = False Then
                _SQL = "select count(*) from ChildAbsence where child_id = " + _ID
                ChildHolidays = HasRecords(_SQL)
            End If

            _SQL = "select count(*) from ChildCharges where child_id = " + _ID
            ChildCharges = HasRecords(_SQL)

            _SQL = "select count(*) from ChildDeposits where child_id = " + _ID
            ChildDeposits = HasRecords(_SQL)

            '****************************************************************************************************************************************

            _SQL = "select count(*) from ChildAnnual where child_id = " + _ID
            ChildAnnualPattern = HasRecords(_SQL)

            _SQL = "select count(*) from ChildCalculation where child_id = " + _ID
            ChildAnnualManual = HasRecords(_SQL)

            '****************************************************************************************************************************************

        End Sub

        Public Function HasRecords(ByVal SQL As String) As Boolean

            Dim _Count As Object = DAL.ReturnScalar(Session.ConnectionString, SQL)
            If _Count IsNot Nothing Then
                Dim i As Integer = ValueHandler.ConvertInteger(_Count)
                If i > 0 Then Return True
                Return False
            Else
                Return False
            End If

        End Function

    End Class

    Private Sub HandleHealthTab()

        If m_Child Is Nothing Then Exit Sub

        Select Case tcHealth.SelectedTabPage.Name

            Case "tabAllergyDiet"

                If m_Child._AllergyRating.ToString = "" Then
                    cbxAllergyGrade.SelectedValue = "None"
                Else
                    cbxAllergyGrade.SelectedValue = m_Child._AllergyRating
                End If

                DisplayAllergyMatrix()

            Case "tabMedication"
                DisplayAnnualCalculations()

            Case "tabMedContact"
                txtSurgTel.Text = m_Child._SurgTel

                txtHVTelephone.Text = m_Child._HvTel
                txtHVMobile.Text = m_Child._HvMobile
                txtHVEmail.Text = m_Child._HvEmail

            Case "tabSEND"
                'handled in bindings

            Case "tabImms"
                DisplayImmunisations()

            Case "tabConsent"
                DisplayConsent()

            Case "tabAttributes"
                DisplayAttributes()

            Case "tabRepeatingMedication"
                DisplayRepeatingMedication()

        End Select

    End Sub

    Private Sub DisplayAllergyMatrix()

        m_AllergyMatrix.Clear()

        Dim _SQL As String = ""
        _SQL += "select AppListItems.name, cast(iif(ChildAttrib.attribute = AppListItems.name, 1, 0) as bit) as 'checked' from AppListItems"
        _SQL += " left join AppLists on AppLists.ID = AppListItems.list_id"
        _SQL += " left join ChildAttrib on ChildAttrib.category = 'Allergies'"
        _SQL += " and ChildAttrib.child_id = '" + m_Child._ID.Value.ToString + "'"
        _SQL += " and ChildAttrib.attribute = AppListItems.name"
        _SQL += " where AppLists.name = 'Allergy Matrix'"
        _SQL += " order by AppListItems.seq, AppListItems.name"

        Dim _DT As DataTable = Care.Data.DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then
            For Each _DR As DataRow In _DT.Rows
                m_AllergyMatrix.Add(New AllergyMatrix(_DR.Item("name").ToString, ValueHandler.ConvertBoolean(_DR.Item("checked"))))
            Next
        End If

        cgAllergyMatrix.Populate(m_AllergyMatrix)

        cgAllergyMatrix.Columns("Description").Caption = "Allergy"
        cgAllergyMatrix.Columns("Description").OptionsColumn.AllowEdit = False

        cgAllergyMatrix.Columns("Selected").Caption = " "

    End Sub

    Private Sub DisplayConsent()

        Dim _SQL As String = ""

        _SQL += "select c.ID as 'ID', i.ref_1, i.name, isnull(c.given, 0) as 'given', c.consent_date, c.consent_person, i.seq from AppListItems i"
        _SQL += " left join AppLists l on l.ID = i.list_id"
        _SQL += " left join ChildConsent c on c.consent = i.name and c.child_id = '" + RecordID.ToString + "'"
        _SQL += " where l.name = 'Consent'"
        _SQL += " order by i.ref_1, i.name"

        cgConsent.Populate(Session.ConnectionString, _SQL)

        cgConsent.Columns("ID").Visible = False
        cgConsent.Columns("ref_1").Caption = "Category"
        cgConsent.Columns("name").Caption = "Consent"
        cgConsent.Columns("given").Caption = "Given"
        cgConsent.Columns("consent_date").Caption = "Date Given"
        cgConsent.Columns("consent_person").Caption = "Given By"
        cgConsent.Columns("seq").Visible = False

        cgConsent.Columns("ref_1").Group()
        cgConsent.Columns("seq").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        cgConsent.ExpandAll()

    End Sub

    Private Sub DisplayImmunisations()

        Dim _SQL As String = ""

        _SQL += "select ID, imm_date, immunisation from ChildImms"
        _SQL += " where child_id = '" + RecordID.ToString + "'"
        _SQL += " order by imm_date, immunisation"

        cgImms.HideFirstColumn = True
        cgImms.Populate(Session.ConnectionString, _SQL)

        cgImms.Columns("imm_date").Caption = "Immunisation Date"
        cgImms.Columns("immunisation").Caption = "Immunisation"

    End Sub

    Private Sub DisplayRepeatingMedication()

        Dim sql As String = ""

        sql = String.Concat("select id, illness, medicine, dosage, last_given, ",
                            " frequency, dosages_due ",
                            " from medicineauth",
                            " where day_id is null ",
                            " and child_id = '", m_Child._ID, "'")
        '" and child_id = '", RecordID.ToString, "'")

        With cgRepeatingMedication

            .HideFirstColumn = True
            .Populate(Session.ConnectionString, sql)

            .Columns("illness").Caption = "Illness"
            .Columns("medicine").Caption = "Medicine"
            .Columns("dosage").Caption = "Dosage"
            .Columns("last_given").Caption = "Start Time"
            .Columns("last_given").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            .Columns("last_given").DisplayFormat.FormatString = "HH:mm"
            '.Columns("end_time").Caption = "End Time"
            .Columns("frequency").Caption = "Frequency (hours)"
            .Columns("dosages_due").Caption = "Dosages Due"

        End With

    End Sub

    Private Sub DisplayAttributes()

    End Sub

    Private Sub tcHealth_SelectedPageChanged(sender As Object, e As DevExpress.XtraTab.TabPageChangedEventArgs) Handles tcHealth.SelectedPageChanged
        HandleHealthTab()
    End Sub

    Private Sub cgImms_AddClick(sender As Object, e As EventArgs) Handles cgImms.AddClick
        ShowImmunisation("")
    End Sub

    Private Sub cgImms_EditClick(sender As Object, e As EventArgs) Handles cgImms.EditClick
        Dim _ID As Guid? = ReturnDialogID(cgImms)
        If _ID.HasValue Then
            ShowImmunisation(_ID.Value.ToString)
        End If
    End Sub

    Private Sub cgImms_RemoveClick(sender As Object, e As EventArgs) Handles cgImms.RemoveClick
        If CareMessage("Are you sure you want to remove this Immunisation?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Remove Immunisation") = DialogResult.Yes Then
            Dim _ID As Guid? = ReturnDialogID(cgImms)
            If _ID.HasValue Then
                DAL.ExecuteSQL(Session.ConnectionString, "delete from ChildImms where ID = '" + _ID.Value.ToString + "'")
                DisplayImmunisations()
            End If
        End If
    End Sub

    Private Sub ShowImmunisation(ByVal ID As String)

        gbxImm.Tag = ID

        If ID <> "" Then
            cdtImmDate.Value = ValueHandler.ConvertDate(cgImms.CurrentRow("imm_date"))
            cbxImm.Text = cgImms.CurrentRow("immunisation").ToString
        Else
            cdtImmDate.Text = ""
            cbxImm.SelectedIndex = -1
        End If

        gbxImm.Show()

    End Sub

    Private Sub cgRepeatingMedication_AddClick(sender As Object, e As EventArgs) Handles cgRepeatingMedication.AddClick
        ShowRepeatMedication("")
    End Sub

    Private Sub cgRepeatingMedication_EditClick(sender As Object, e As EventArgs) Handles cgRepeatingMedication.EditClick
        Dim id As Guid? = ReturnDialogID(cgRepeatingMedication)
        If id.HasValue Then
            ShowRepeatMedication(id.Value.ToString)
        End If
    End Sub

    Private Sub cgRepeatingMedication_RemoveClick(sender As Object, e As EventArgs) Handles cgRepeatingMedication.RemoveClick
        If CareMessage("Are you sure you want to remove this Repeating Medication?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Remove Repeating Medication") = DialogResult.Yes Then
            Dim id As Guid? = ReturnDialogID(cgRepeatingMedication)
            If id.HasValue Then
                DAL.ExecuteSQL(Session.ConnectionString, "delete from medicineauth where ID = '" + id.Value.ToString + "'")
                DisplayRepeatingMedication()
            End If
        End If
    End Sub

    Private Sub ShowRepeatMedication(ByVal ID As String)

        gbxRepeatMedication.Tag = ID

        Dim grid As Care.Controls.CareGridWithButtons = cgRepeatingMedication

        If ID <> "" Then

            cbxNatureOfIllness.Text = grid.CurrentRow("illness").ToString
            cbxMedicationRequired.Text = grid.CurrentRow("medicine").ToString
            cbxDosageRequired.Text = grid.CurrentRow("dosage").ToString

            chkAsRequired.Checked = DirectCast(IIf(IsNothing(grid.CurrentRow("dosages_due")), True, False), Boolean)

            txtFrequency.Text = grid.CurrentRow("frequency").ToString
            txtStartTime.EditValue = CDate(grid.CurrentRow("last_given").ToString).ToShortTimeString

            txtManualTimeEntry.EditValue = Nothing
            If chkAsRequired.CheckState = CheckState.Unchecked Then
                lblManualTimeEntries.Text = grid.CurrentRow("dosages_due").ToString
            Else
                lblManualTimeEntries.Text = ""
            End If

        Else

            cbxNatureOfIllness.SelectedIndex = -1
            cbxMedicationRequired.SelectedIndex = -1
            cbxDosageRequired.SelectedIndex = -1

            chkAsRequired.Checked = False

            txtFrequency.Text = "0"
            txtStartTime.EditValue = Nothing

            txtManualTimeEntry.EditValue = Nothing
            lblManualTimeEntries.Text = ""

        End If

        txtManualTimeEntry.EnterMoveNextControl = False
        gbxRepeatMedication.Show()

    End Sub

    Private Sub cgConsent_GridDoubleClick(sender As Object, e As EventArgs) Handles cgConsent.GridDoubleClick

        If Mode <> "EDIT" Then Exit Sub

        txtConsentItem.Text = cgConsent.CurrentRow("name").ToString

        Dim _ID As Guid? = ReturnDialogID(cgConsent)
        If _ID.HasValue Then
            gbxConsent.Tag = _ID.Value.ToString
            cdtConsentDate.Value = ValueHandler.ConvertDate(cgConsent.CurrentRow("consent_date"))
            cbxConsentContact.Text = cgConsent.CurrentRow("consent_person").ToString
        Else
            gbxConsent.Tag = ""
            cdtConsentDate.Text = ""
            cbxConsentContact.SelectedIndex = -1
        End If

        gbxConsent.Show()
        cdtConsentDate.Focus()

    End Sub

    Private Sub btnConsentOK_Click(sender As Object, e As EventArgs) Handles btnConsentOK.Click

        Dim _C As New Business.ChildConsent
        If gbxConsent.Tag.ToString <> "" Then
            _C = Business.ChildConsent.RetreiveByID(New Guid(gbxConsent.Tag.ToString))
        Else
            _C._ChildId = m_Child._ID
            _C._Consent = txtConsentItem.Text
            _C._Given = True
        End If

        _C._ConsentDate = cdtConsentDate.Value
        _C._ConsentPerson = cbxConsentContact.Text
        _C.Store()

        DisplayConsent()

        gbxConsent.Hide()

    End Sub

    Private Sub btnConsentCancel_Click(sender As Object, e As EventArgs) Handles btnConsentCancel.Click
        gbxConsent.Hide()
    End Sub

    Private Sub btnImmOK_Click(sender As Object, e As EventArgs) Handles btnImmOK.Click

        Dim _I As New Business.ChildImm
        If gbxImm.Tag.ToString <> "" Then
            _I = Business.ChildImm.RetreiveByID(New Guid(gbxImm.Tag.ToString))
        Else
            _I._ChildId = m_Child._ID
        End If

        _I._ImmDate = cdtImmDate.Value
        _I._Immunisation = cbxImm.Text
        _I.Store()

        DisplayImmunisations()

        gbxImm.Hide()

    End Sub

    Private Sub btnImmCancel_Click(sender As Object, e As EventArgs) Handles btnImmCancel.Click
        gbxImm.Hide()
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click

        If grdMedical Is Nothing Then Exit Sub
        If grdMedical.RecordCount < 1 Then Exit Sub
        If grdMedical.CurrentRow(0).ToString = "" Then Exit Sub

        btnDelete.Enabled = False

        Dim _Mess As String = ""
        If radMedRequests.Checked Or radMedGiven.Checked Then
            _Mess = "Are you sure you want to Delete this Medication record?"
        Else
            _Mess = "Are you sure you want to Delete this Incident record?"
        End If

        If CareMessage(_Mess, MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Delete Record") = DialogResult.Yes Then

            If ParameterHandler.ManagerAuthorised Then

                Dim _SQL As String = ""
                Dim _ID As String = grdMedical.CurrentRow(0).ToString

                If radMedRequests.Checked Then

                    _SQL = "delete from MedicineAuth where ID = '" + _ID + "'"
                    DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                    _SQL = "delete from MedicineLog where auth_id = '" + _ID + "'"
                    DAL.ExecuteSQL(Session.ConnectionString, _SQL)

                Else
                    If radMedGiven.Checked Then
                        _SQL = "delete from MedicineLog where ID = '" + _ID + "'"
                        DAL.ExecuteSQL(Session.ConnectionString, _SQL)
                    Else
                        _SQL = "delete from Incidents where ID = '" + _ID + "'"
                        DAL.ExecuteSQL(Session.ConnectionString, _SQL)
                    End If
                End If

                DisplayMedicineAndIncidents()

            Else
                'auth failed
            End If
        Else
            'selected no
        End If

        btnDelete.Enabled = True

    End Sub

    Private Sub grdPatterns_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles grdPatterns.RowStyle
        If e.RowHandle >= 0 Then

            If grdPatterns.GetRowCellValue(e.RowHandle, "Type").ToString = "Recurring" Then

                Select Case grdPatterns.GetRowCellValue(e.RowHandle, "recurring_scope").ToString

                    Case "0"
                        'disabled
                        e.Appearance.Font = New Font(e.Appearance.Font, FontStyle.Strikeout)

                    Case "1"
                        'enabled all the time
                        e.Appearance.Font = New Font(e.Appearance.Font, FontStyle.Regular)

                    Case Else
                        'e.Appearance.Font = New Font(e.Appearance.Font, FontStyle.Italic)
                        e.Appearance.Font = New Font(e.Appearance.Font, FontStyle.Regular)

                End Select

            Else
                e.Appearance.Font = New Font(e.Appearance.Font, FontStyle.Regular)
            End If

        End If
    End Sub

    Private Sub radBookFutureRooms_CheckedChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub grdAnnual_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles grdAnnual.RowStyle

        If e.RowHandle >= 0 Then
            If radAnnualManual.Checked Then
                If grdAnnual.GetRowCellValue(e.RowHandle, "active").ToString = "False" Then
                    e.Appearance.Font = New Font(e.Appearance.Font, FontStyle.Strikeout)
                Else
                    e.Appearance.Font = New Font(e.Appearance.Font, FontStyle.Regular)
                End If
            Else
                e.Appearance.Font = New Font(e.Appearance.Font, FontStyle.Regular)
            End If
        End If

    End Sub

    Private Sub btnMedPrintAll_Click(sender As Object, e As EventArgs) Handles btnMedPrintAll.Click
        If btnMedPrintAll.ShiftDown Then
            Medical.DesignReport(Medical.EnumLogOrRequest.LogAll, m_Child._ID.ToString)
        Else
            Medical.PrintReport(Medical.EnumLogOrRequest.LogAll, m_Child._ID.ToString)
        End If
    End Sub

    Private Sub txtTags_ValidateToken(sender As Object, e As DevExpress.XtraEditors.TokenEditValidateTokenEventArgs) Handles txtTags.ValidateToken
        e.IsValid = True
    End Sub

    Private Sub btnConsentClear_Click(sender As Object, e As EventArgs) Handles btnConsentClear.Click

        If CareMessage("Are you sure you want to Clear this consent item?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Clear") = DialogResult.Yes Then

            Dim _C As New Business.ChildConsent
            If gbxConsent.Tag.ToString <> "" Then
                Business.ChildConsent.DeleteRecord(New Guid(gbxConsent.Tag.ToString))
            End If

            DisplayConsent()

            gbxConsent.Hide()

        End If

    End Sub

    Private Sub cbxSite_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSite.SelectedIndexChanged
        If cbxSite.Text = "" Then
            cbxGroup.Clear()
            cbxGroup.SelectedIndex = -1
        Else
            cbxGroup.PopulateWithSQL(Session.ConnectionString, Business.RoomRatios.RetreiveSiteRoomSQL(cbxSite.SelectedValue.ToString))
            cbxMoveRoom.PopulateWithSQL(Session.ConnectionString, Business.RoomRatios.RetreiveSiteRoomRatioSQL(cbxSite.SelectedValue.ToString))
        End If
    End Sub

    Private Sub cbxMoveMode_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxMoveMode.SelectedIndexChanged
        SetFutureMoves()
    End Sub

    Private Sub SetFutureMoves()

        If MyBase.Mode = "" Then Exit Sub

        If cbxMoveMode.Text = "Manual" Then

            cdtMoveDate.Enabled = True
            cdtMoveDate.BackColor = Session.ChangeColour
            cdtMoveDate.TabStop = True

            cbxMoveRoom.Enabled = True
            cbxMoveRoom.ReadOnly = False
            cbxMoveRoom.BackColor = Session.ChangeColour
            cbxMoveRoom.TabStop = True

        Else

            cdtMoveDate.Text = ""
            cdtMoveDate.Enabled = False
            cdtMoveDate.ResetBackColor()
            cdtMoveDate.TabStop = False

            cbxMoveRoom.SelectedIndex = -1
            cbxMoveRoom.Enabled = False
            cbxMoveRoom.ResetBackColor()
            cdtMoveDate.TabStop = False

        End If

    End Sub

    Private Sub cdtMoveDate_Validated(sender As Object, e As EventArgs) Handles cdtMoveDate.Validated

    End Sub

    Private Sub cdtMoveDate_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles cdtMoveDate.Validating

    End Sub

    Private Sub lblAge_Click(sender As Object, e As EventArgs) Handles lblAge.Click

    End Sub

    Private Sub lblAge_DoubleClick(sender As Object, e As EventArgs) Handles lblAge.DoubleClick
        Dim _Years As Integer = ValueHandler.ConvertInteger(InputBox("Enter Year"))
        Dim _Month As Integer = ValueHandler.ConvertInteger(InputBox("Enter Month"))
        Dim _Day As Integer = ValueHandler.ConvertInteger(InputBox("Enter Day"))
        Dim _Date As Date = DateSerial(_Years, _Month, _Day)
        Dim _Months As Long = ValueHandler.ReturnExactMonths(udtDOB.DateTime, _Date)
        CareMessage("Exact Months on " + _Date.ToString + ": " + _Months.ToString)
    End Sub

    Private Sub chkAsRequired_CheckedChanged(sender As Object, e As EventArgs) Handles chkAsRequired.CheckedChanged

        Dim enabled As Boolean = Not (chkAsRequired.Checked)

        If chkAsRequired.Checked Then

            txtFrequency.Text = ""
            txtFrequency.ReadOnly = True
            txtFrequency.ResetBackColor()
            txtStartTime.Text = ""
            txtStartTime.ReadOnly = True
            txtStartTime.ResetBackColor()
            txtManualTimeEntry.Text = ""
            txtManualTimeEntry.ReadOnly = True
            txtManualTimeEntry.ResetBackColor()
            btnManualTimeEntryAdd.Enabled = False
            btnManualTimeEntryRemove.Enabled = False

        Else

            txtFrequency.ReadOnly = False
            txtFrequency.BackColor = Session.ChangeColour
            txtStartTime.ReadOnly = False
            txtStartTime.BackColor = Session.ChangeColour
            txtManualTimeEntry.ReadOnly = False
            txtManualTimeEntry.BackColor = Session.ChangeColour
            btnManualTimeEntryAdd.Enabled = True
            btnManualTimeEntryRemove.Enabled = True

        End If

    End Sub

    Private Sub btnRepeatingMedicationOK_Click(sender As Object, e As EventArgs) Handles btnRepeatingMedicationOK.Click

        If cbxNatureOfIllness.Text = "" Then
            CareMessage("Please select Nature of Illness", MessageBoxIcon.Exclamation, "Mandatory Field")
            Exit Sub
        End If

        If cbxMedicationRequired.Text = "" Then
            CareMessage("Please select Medication Required", MessageBoxIcon.Exclamation, "Mandatory Field")
            Exit Sub
        End If

        If cbxDosageRequired.Text = "" Then
            CareMessage("Please select Dosage Required", MessageBoxIcon.Exclamation, "Mandatory Field")
            Exit Sub
        End If

        If chkAsRequired.Checked = False Then

            If lblManualTimeEntries.Text = "" Then
                CareMessage("No times entered.", MessageBoxIcon.Exclamation, "Mandatory Field")
                Exit Sub
            End If

            If txtStartTime.Text = "" Then
                CareMessage("Please enter a Start Time", MessageBoxIcon.Exclamation, "Mandatory Field")
                Exit Sub
            End If

        End If

        Dim medicineAuth As New Business.MedicineAuth
        If gbxRepeatMedication.Tag.ToString <> "" Then
            medicineAuth = Business.MedicineAuth.RetreiveByID(New Guid(gbxRepeatMedication.Tag.ToString))
        Else
            medicineAuth._ChildId = m_Child._ID
            medicineAuth._ChildName = m_Child._Fullname
        End If

        medicineAuth._Illness = cbxNatureOfIllness.Text
        medicineAuth._Medicine = cbxMedicationRequired.Text
        medicineAuth._Dosage = cbxDosageRequired.Text
        medicineAuth._Frequency = txtFrequency.Text

        If chkAsRequired.Checked Then
            medicineAuth._LastGiven = Nothing
            medicineAuth._Frequency = Nothing
            medicineAuth._DosagesDue = Nothing
        Else
            medicineAuth._LastGiven = Date.Parse("1900-01-01 " & txtStartTime.EditValue.ToString)
            medicineAuth._Frequency = txtFrequency.Text
            medicineAuth._DosagesDue = lblManualTimeEntries.Text
        End If

        medicineAuth.Store()

        DisplayRepeatingMedication()
        gbxRepeatMedication.Hide()

    End Sub

    Private Function ReturnDosagesDue(startTime As Date, frequency As Double) As String

        If txtFrequency.Text = "" OrElse txtStartTime.Text = "" Then Return ""

        Dim time As Date = startTime
        Dim times As String = ""

        While time < CDate("23:59")
            times &= DirectCast(IIf(times = "", time.ToShortTimeString, ", " & time.ToShortTimeString), String)
            time = time.AddHours(frequency)

        End While

        Return times

    End Function

    Private Sub btnRepeatingMedicationCancel_Click(sender As Object, e As EventArgs) Handles btnRepeatingMedicationCancel.Click
        gbxRepeatMedication.Hide()
    End Sub

    Private Sub TimeChanged(sender As Object, e As EventArgs) Handles txtFrequency.TextChanged, txtStartTime.TextChanged

        lblManualTimeEntries.Text = ""

        If txtStartTime.Text = "" Or Not IsDate(txtStartTime.Text) Then Exit Sub
        If txtFrequency.Text = "" Or txtFrequency.Text = "0" Then Exit Sub

        If gbxRepeatMedication.Visible Then lblManualTimeEntries.Text = ReturnDosagesDue(CDate(txtStartTime.EditValue.ToString), CDbl(txtFrequency.Text))

    End Sub

    Private Sub btnManualTimeEntryAdd_Click(sender As Object, e As EventArgs) Handles btnManualTimeEntryAdd.Click
        If IsDate(txtManualTimeEntry.EditValue) Then lblManualTimeEntries.Text &= DirectCast(IIf(lblManualTimeEntries.Text = "", txtManualTimeEntry.EditValue.ToString, ", " & txtManualTimeEntry.EditValue.ToString), String)
    End Sub

    Private Sub btnManualTimeEntryRemove_Click(sender As Object, e As EventArgs) Handles btnManualTimeEntryRemove.Click
        Dim i As Integer = InStrRev(lblManualTimeEntries.Text, ",") - 1
        If i > 0 Then
            lblManualTimeEntries.Text = Mid(lblManualTimeEntries.Text, 1, i)
        Else
            lblManualTimeEntries.Text = ""
        End If

    End Sub

    Private Sub btnPostPlacement_Click(sender As Object, e As EventArgs) Handles btnPostPlacement.Click

        Dim postPlacement As New frmPostPlacement(m_Child)
        postPlacement.ShowDialog()

        If postPlacement.DialogResult = DialogResult.OK Then
            If Not m_Child.IsNew Then m_Child.Store()
        End If

    End Sub

    Private Sub radInvoicesPosted_CheckedChanged(sender As Object, e As EventArgs) Handles radInvoicesPosted.CheckedChanged
        If radInvoicesPosted.Checked Then DisplayInvoices()
    End Sub

    Private Sub radInvoices_CheckedChanged(sender As Object, e As EventArgs) Handles radInvoices.CheckedChanged
        If radInvoices.Checked Then DisplayInvoices()
    End Sub

    Private Sub radPostedCredits_CheckedChanged(sender As Object, e As EventArgs) Handles radPostedCredits.CheckedChanged
        If radPostedCredits.Checked Then DisplayInvoices()
    End Sub

    Private Sub radNonPostedCredits_CheckedChanged(sender As Object, e As EventArgs) Handles radNonPostedCredits.CheckedChanged
        If radNonPostedCredits.Checked Then DisplayInvoices()
    End Sub

    Private Sub cgInvoices_GridDoubleClick(sender As Object, e As EventArgs) Handles cgInvoices.GridDoubleClick

        Dim _ID As Guid? = ReturnDialogID(cgInvoices)
        If _ID.HasValue Then
            If radInvoices.Checked OrElse radInvoicesPosted.Checked Then
                Invoicing.ViewInvoice(_ID.Value)
            Else
                Credits.ViewCredit(_ID.Value)
            End If
        End If

    End Sub

    Private Sub grdDeposits_GridDoubleClick(sender As Object, e As EventArgs) Handles grdDeposits.GridDoubleClick
        EditItem()
    End Sub

    Private Sub radHolidaysAll_CheckedChanged(sender As Object, e As EventArgs) Handles radHolidaysAll.CheckedChanged
        If radHolidaysAll.Checked Then DisplayHolidays()
    End Sub

    Private Sub radHolidays_CheckedChanged(sender As Object, e As EventArgs) Handles radHolidays.CheckedChanged
        If radHolidays.Checked Then DisplayHolidays()
    End Sub

    Private Sub radAbsence_CheckedChanged(sender As Object, e As EventArgs) Handles radAbsence.CheckedChanged
        If radAbsence.Checked Then DisplayHolidays()
    End Sub

    Private Sub btnPIN_Click(sender As Object, e As EventArgs) Handles btnPIN.Click

        Dim _frm As New frmChangePIN(m_Child)
        _frm.ShowDialog()

        txtPIN.Text = m_Child._Pin

        _frm.Dispose()
        _frm = Nothing

    End Sub
End Class
