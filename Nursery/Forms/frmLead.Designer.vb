﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLead
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Dim OptionsSpelling2 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.CareTab1 = New Care.Controls.CareTab(Me.components)
        Me.tabLead = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl4 = New Care.Controls.CareFrame()
        Me.memContactNotes = New DevExpress.XtraEditors.MemoEdit()
        Me.GroupControl3 = New Care.Controls.CareFrame()
        Me.TableLayoutPanel1 = New TableLayoutPanel()
        Me.CareLabel17 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel16 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel12 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.chkMonday = New Care.Controls.CareCheckBox(Me.components)
        Me.chkTuesday = New Care.Controls.CareCheckBox(Me.components)
        Me.chkWednesday = New Care.Controls.CareCheckBox(Me.components)
        Me.chkThursday = New Care.Controls.CareCheckBox(Me.components)
        Me.chkFriday = New Care.Controls.CareCheckBox(Me.components)
        Me.CareLabel10 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.cbxExpectedRoom = New Care.Controls.CareComboBox(Me.components)
        Me.lblStart = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.cdtExpectedStart = New Care.Controls.CareDateTime(Me.components)
        Me.GroupControl6 = New Care.Controls.CareFrame()
        Me.Label12 = New Care.Controls.CareLabel(Me.components)
        Me.cbxChildGender = New Care.Controls.CareComboBox(Me.components)
        Me.lblAge = New Care.Controls.CareLabel(Me.components)
        Me.Label16 = New Care.Controls.CareLabel(Me.components)
        Me.cdtDOB = New Care.Controls.CareDateTime(Me.components)
        Me.txtChildSurname = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel23 = New Care.Controls.CareLabel(Me.components)
        Me.txtChildForename = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel24 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl2 = New Care.Controls.CareFrame()
        Me.cbxSite = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.cbxVia = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.cbxFoundUs = New Care.Controls.CareComboBox(Me.components)
        Me.cbxRating = New Care.Controls.CareComboBox(Me.components)
        Me.lblEntered = New Care.Controls.CareLabel(Me.components)
        Me.cdtEntered = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel13 = New Care.Controls.CareLabel(Me.components)
        Me.txtStage = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel15 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.CareLabel18 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.chkMarketing = New Care.Controls.CareCheckBox(Me.components)
        Me.cbxCommMethod = New Care.Controls.CareComboBox(Me.components)
        Me.txtTel = New Care.[Shared].CareTelephoneNumber()
        Me.txtMobile = New Care.[Shared].CareTelephoneNumber()
        Me.txtEmail = New Care.[Shared].CareEmailAddress()
        Me.Label13 = New Care.Controls.CareLabel(Me.components)
        Me.Label8 = New Care.Controls.CareLabel(Me.components)
        Me.Label9 = New Care.Controls.CareLabel(Me.components)
        Me.txtAddressFull = New Care.Address.CareAddress()
        Me.Label2 = New Care.Controls.CareLabel(Me.components)
        Me.txtContactRelationship = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.txtContactSurname = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.txtContactForename = New Care.Controls.CareTextBox(Me.components)
        Me.tabNotes = New DevExpress.XtraTab.XtraTabPage()
        Me.memChildNotes = New DevExpress.XtraEditors.MemoEdit()
        Me.tabAvailability = New DevExpress.XtraTab.XtraTabPage()
        Me.YearView1 = New Nursery.YearView()
        Me.tabActivity = New DevExpress.XtraTab.XtraTabPage()
        Me.CRM = New Care.[Shared].CRMActivity()
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        Me.btnEdit = New Care.Controls.CareButton(Me.components)
        Me.btnOK = New Care.Controls.CareButton(Me.components)
        Me.btnProgress = New Care.Controls.CareButton(Me.components)
        Me.btnAvailEmail = New Care.Controls.CareButton(Me.components)
        Me.btnDeleteLead = New Care.Controls.CareButton(Me.components)
        CType(Me.CareTab1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CareTab1.SuspendLayout()
        Me.tabLead.SuspendLayout()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.memContactNotes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.chkMonday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkTuesday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkWednesday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkThursday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkFriday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxExpectedRoom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtExpectedStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtExpectedStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl6.SuspendLayout()
        CType(Me.cbxChildGender.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDOB.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtDOB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtChildSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtChildForename.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxVia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxFoundUs.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxRating.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtEntered.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtEntered.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.chkMarketing.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxCommMethod.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtContactRelationship.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtContactSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtContactForename.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabNotes.SuspendLayout()
        CType(Me.memChildNotes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabAvailability.SuspendLayout()
        Me.tabActivity.SuspendLayout()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'CareTab1
        '
        Me.CareTab1.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.CareTab1.Location = New System.Drawing.Point(12, 8)
        Me.CareTab1.Name = "CareTab1"
        Me.CareTab1.SelectedTabPage = Me.tabLead
        Me.CareTab1.Size = New System.Drawing.Size(726, 538)
        Me.CareTab1.TabIndex = 0
        Me.CareTab1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabLead, Me.tabNotes, Me.tabAvailability, Me.tabActivity})
        '
        'tabLead
        '
        Me.tabLead.Controls.Add(Me.GroupControl4)
        Me.tabLead.Controls.Add(Me.GroupControl3)
        Me.tabLead.Controls.Add(Me.GroupControl6)
        Me.tabLead.Controls.Add(Me.GroupControl2)
        Me.tabLead.Controls.Add(Me.GroupControl1)
        Me.tabLead.Name = "tabLead"
        Me.tabLead.Size = New System.Drawing.Size(720, 510)
        Me.tabLead.Text = "Lead Details"
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.memContactNotes)
        Me.GroupControl4.Location = New System.Drawing.Point(11, 377)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(345, 125)
        Me.GroupControl4.TabIndex = 1
        Me.GroupControl4.Text = "Contact Notes"
        '
        'memContactNotes
        '
        Me.memContactNotes.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.memContactNotes.Location = New System.Drawing.Point(10, 28)
        Me.memContactNotes.Name = "memContactNotes"
        Me.memContactNotes.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.memContactNotes.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.memContactNotes, True)
        Me.memContactNotes.Size = New System.Drawing.Size(324, 89)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.memContactNotes, OptionsSpelling1)
        Me.memContactNotes.TabIndex = 0
        Me.memContactNotes.Tag = "AEB"
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.TableLayoutPanel1)
        Me.GroupControl3.Controls.Add(Me.CareLabel3)
        Me.GroupControl3.Controls.Add(Me.cbxExpectedRoom)
        Me.GroupControl3.Controls.Add(Me.lblStart)
        Me.GroupControl3.Controls.Add(Me.CareLabel8)
        Me.GroupControl3.Controls.Add(Me.cdtExpectedStart)
        Me.GroupControl3.Location = New System.Drawing.Point(366, 357)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(345, 145)
        Me.GroupControl3.TabIndex = 4
        Me.GroupControl3.Text = "Booking Preferences"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 5
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.CareLabel17, 4, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.CareLabel16, 3, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.CareLabel12, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.CareLabel11, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.chkMonday, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.chkTuesday, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.chkWednesday, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.chkThursday, 3, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.chkFriday, 4, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.CareLabel10, 0, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(5, 87)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New RowStyle(SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New RowStyle(SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(335, 50)
        Me.TableLayoutPanel1.TabIndex = 5
        '
        'CareLabel17
        '
        Me.CareLabel17.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel17.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.CareLabel17.Dock = DockStyle.Fill
        Me.CareLabel17.Location = New System.Drawing.Point(271, 3)
        Me.CareLabel17.Name = "CareLabel17"
        Me.CareLabel17.Size = New System.Drawing.Size(61, 19)
        Me.CareLabel17.TabIndex = 9
        Me.CareLabel17.Text = "Fri"
        Me.CareLabel17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CareLabel16
        '
        Me.CareLabel16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel16.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.CareLabel16.Dock = DockStyle.Fill
        Me.CareLabel16.Location = New System.Drawing.Point(204, 3)
        Me.CareLabel16.Name = "CareLabel16"
        Me.CareLabel16.Size = New System.Drawing.Size(61, 19)
        Me.CareLabel16.TabIndex = 8
        Me.CareLabel16.Text = "Thurs"
        Me.CareLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CareLabel12
        '
        Me.CareLabel12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.CareLabel12.Dock = DockStyle.Fill
        Me.CareLabel12.Location = New System.Drawing.Point(137, 3)
        Me.CareLabel12.Name = "CareLabel12"
        Me.CareLabel12.Size = New System.Drawing.Size(61, 19)
        Me.CareLabel12.TabIndex = 7
        Me.CareLabel12.Text = "Wed"
        Me.CareLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.CareLabel11.Dock = DockStyle.Fill
        Me.CareLabel11.Location = New System.Drawing.Point(70, 3)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(61, 19)
        Me.CareLabel11.TabIndex = 6
        Me.CareLabel11.Text = "Tues"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkMonday
        '
        Me.chkMonday.Dock = DockStyle.Fill
        Me.chkMonday.EnterMoveNextControl = True
        Me.chkMonday.Location = New System.Drawing.Point(3, 28)
        Me.chkMonday.Name = "chkMonday"
        Me.chkMonday.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkMonday.Properties.Appearance.Options.UseFont = True
        Me.chkMonday.Properties.AutoHeight = False
        Me.chkMonday.Properties.Caption = ""
        Me.chkMonday.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkMonday.Size = New System.Drawing.Size(61, 19)
        Me.chkMonday.TabIndex = 0
        Me.chkMonday.Tag = "AE"
        '
        'chkTuesday
        '
        Me.chkTuesday.Dock = DockStyle.Fill
        Me.chkTuesday.EnterMoveNextControl = True
        Me.chkTuesday.Location = New System.Drawing.Point(70, 28)
        Me.chkTuesday.Name = "chkTuesday"
        Me.chkTuesday.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkTuesday.Properties.Appearance.Options.UseFont = True
        Me.chkTuesday.Properties.AutoHeight = False
        Me.chkTuesday.Properties.Caption = ""
        Me.chkTuesday.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkTuesday.Size = New System.Drawing.Size(61, 19)
        Me.chkTuesday.TabIndex = 1
        Me.chkTuesday.Tag = "AE"
        '
        'chkWednesday
        '
        Me.chkWednesday.Dock = DockStyle.Fill
        Me.chkWednesday.EnterMoveNextControl = True
        Me.chkWednesday.Location = New System.Drawing.Point(137, 28)
        Me.chkWednesday.Name = "chkWednesday"
        Me.chkWednesday.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkWednesday.Properties.Appearance.Options.UseFont = True
        Me.chkWednesday.Properties.AutoHeight = False
        Me.chkWednesday.Properties.Caption = ""
        Me.chkWednesday.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkWednesday.Size = New System.Drawing.Size(61, 19)
        Me.chkWednesday.TabIndex = 2
        Me.chkWednesday.Tag = "AE"
        '
        'chkThursday
        '
        Me.chkThursday.Dock = DockStyle.Fill
        Me.chkThursday.EnterMoveNextControl = True
        Me.chkThursday.Location = New System.Drawing.Point(204, 28)
        Me.chkThursday.Name = "chkThursday"
        Me.chkThursday.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkThursday.Properties.Appearance.Options.UseFont = True
        Me.chkThursday.Properties.AutoHeight = False
        Me.chkThursday.Properties.Caption = ""
        Me.chkThursday.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkThursday.Size = New System.Drawing.Size(61, 19)
        Me.chkThursday.TabIndex = 3
        Me.chkThursday.Tag = "AE"
        '
        'chkFriday
        '
        Me.chkFriday.Dock = DockStyle.Fill
        Me.chkFriday.EnterMoveNextControl = True
        Me.chkFriday.Location = New System.Drawing.Point(271, 28)
        Me.chkFriday.Name = "chkFriday"
        Me.chkFriday.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkFriday.Properties.Appearance.Options.UseFont = True
        Me.chkFriday.Properties.AutoHeight = False
        Me.chkFriday.Properties.Caption = ""
        Me.chkFriday.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.chkFriday.Size = New System.Drawing.Size(61, 19)
        Me.chkFriday.TabIndex = 4
        Me.chkFriday.Tag = "AE"
        '
        'CareLabel10
        '
        Me.CareLabel10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.CareLabel10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.CareLabel10.Dock = DockStyle.Fill
        Me.CareLabel10.Location = New System.Drawing.Point(3, 3)
        Me.CareLabel10.Name = "CareLabel10"
        Me.CareLabel10.Size = New System.Drawing.Size(61, 19)
        Me.CareLabel10.TabIndex = 5
        Me.CareLabel10.Text = "Mon"
        Me.CareLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(10, 60)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(82, 15)
        Me.CareLabel3.TabIndex = 3
        Me.CareLabel3.Text = "Expected Room"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxExpectedRoom
        '
        Me.cbxExpectedRoom.AllowBlank = False
        Me.cbxExpectedRoom.DataSource = Nothing
        Me.cbxExpectedRoom.DisplayMember = Nothing
        Me.cbxExpectedRoom.EnterMoveNextControl = True
        Me.cbxExpectedRoom.Location = New System.Drawing.Point(100, 57)
        Me.cbxExpectedRoom.Name = "cbxExpectedRoom"
        Me.cbxExpectedRoom.Properties.AccessibleName = "Gender"
        Me.cbxExpectedRoom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxExpectedRoom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxExpectedRoom.Properties.Appearance.Options.UseFont = True
        Me.cbxExpectedRoom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxExpectedRoom.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxExpectedRoom.SelectedValue = Nothing
        Me.cbxExpectedRoom.Size = New System.Drawing.Size(234, 22)
        Me.cbxExpectedRoom.TabIndex = 4
        Me.cbxExpectedRoom.Tag = "AEB"
        Me.cbxExpectedRoom.ValueMember = Nothing
        '
        'lblStart
        '
        Me.lblStart.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStart.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblStart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblStart.Location = New System.Drawing.Point(210, 32)
        Me.lblStart.Name = "lblStart"
        Me.lblStart.Size = New System.Drawing.Size(99, 15)
        Me.lblStart.TabIndex = 2
        Me.lblStart.Text = "1 year, 11 months"
        Me.lblStart.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(10, 32)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(74, 15)
        Me.CareLabel8.TabIndex = 0
        Me.CareLabel8.Text = "Expected Start"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtExpectedStart
        '
        Me.cdtExpectedStart.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtExpectedStart.EnterMoveNextControl = True
        Me.cdtExpectedStart.Location = New System.Drawing.Point(100, 29)
        Me.cdtExpectedStart.Name = "cdtExpectedStart"
        Me.cdtExpectedStart.Properties.AccessibleName = "Expected Start Date"
        Me.cdtExpectedStart.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtExpectedStart.Properties.Appearance.Options.UseFont = True
        Me.cdtExpectedStart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtExpectedStart.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtExpectedStart.Size = New System.Drawing.Size(104, 22)
        Me.cdtExpectedStart.TabIndex = 1
        Me.cdtExpectedStart.Tag = "AEM"
        Me.cdtExpectedStart.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'GroupControl6
        '
        Me.GroupControl6.Controls.Add(Me.Label12)
        Me.GroupControl6.Controls.Add(Me.cbxChildGender)
        Me.GroupControl6.Controls.Add(Me.lblAge)
        Me.GroupControl6.Controls.Add(Me.Label16)
        Me.GroupControl6.Controls.Add(Me.cdtDOB)
        Me.GroupControl6.Controls.Add(Me.txtChildSurname)
        Me.GroupControl6.Controls.Add(Me.CareLabel23)
        Me.GroupControl6.Controls.Add(Me.txtChildForename)
        Me.GroupControl6.Controls.Add(Me.CareLabel24)
        Me.GroupControl6.Location = New System.Drawing.Point(366, 211)
        Me.GroupControl6.Name = "GroupControl6"
        Me.GroupControl6.Size = New System.Drawing.Size(345, 140)
        Me.GroupControl6.TabIndex = 3
        Me.GroupControl6.Text = "Child Details"
        '
        'Label12
        '
        Me.Label12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label12.Location = New System.Drawing.Point(10, 86)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(38, 15)
        Me.Label12.TabIndex = 4
        Me.Label12.Text = "Gender"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxChildGender
        '
        Me.cbxChildGender.AllowBlank = False
        Me.cbxChildGender.DataSource = Nothing
        Me.cbxChildGender.DisplayMember = Nothing
        Me.cbxChildGender.EnterMoveNextControl = True
        Me.cbxChildGender.Location = New System.Drawing.Point(100, 83)
        Me.cbxChildGender.Name = "cbxChildGender"
        Me.cbxChildGender.Properties.AccessibleName = "Gender"
        Me.cbxChildGender.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxChildGender.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxChildGender.Properties.Appearance.Options.UseFont = True
        Me.cbxChildGender.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxChildGender.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxChildGender.SelectedValue = Nothing
        Me.cbxChildGender.Size = New System.Drawing.Size(104, 22)
        Me.cbxChildGender.TabIndex = 5
        Me.cbxChildGender.Tag = "AEM"
        Me.cbxChildGender.ValueMember = Nothing
        '
        'lblAge
        '
        Me.lblAge.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAge.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblAge.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblAge.Location = New System.Drawing.Point(210, 114)
        Me.lblAge.Name = "lblAge"
        Me.lblAge.Size = New System.Drawing.Size(99, 15)
        Me.lblAge.TabIndex = 8
        Me.lblAge.Text = "1 year, 11 months"
        Me.lblAge.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label16
        '
        Me.Label16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label16.Location = New System.Drawing.Point(10, 114)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(66, 15)
        Me.Label16.TabIndex = 6
        Me.Label16.Text = "Date of Birth"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtDOB
        '
        Me.cdtDOB.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtDOB.EnterMoveNextControl = True
        Me.cdtDOB.Location = New System.Drawing.Point(100, 111)
        Me.cdtDOB.Name = "cdtDOB"
        Me.cdtDOB.Properties.AccessibleName = "Date of Birth"
        Me.cdtDOB.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtDOB.Properties.Appearance.Options.UseFont = True
        Me.cdtDOB.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtDOB.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtDOB.Size = New System.Drawing.Size(104, 22)
        Me.cdtDOB.TabIndex = 7
        Me.cdtDOB.Tag = "AEM"
        Me.cdtDOB.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'txtChildSurname
        '
        Me.txtChildSurname.CharacterCasing = CharacterCasing.Normal
        Me.txtChildSurname.EnterMoveNextControl = True
        Me.txtChildSurname.Location = New System.Drawing.Point(100, 57)
        Me.txtChildSurname.MaxLength = 30
        Me.txtChildSurname.Name = "txtChildSurname"
        Me.txtChildSurname.NumericAllowNegatives = False
        Me.txtChildSurname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtChildSurname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtChildSurname.Properties.AccessibleName = "Child Surname"
        Me.txtChildSurname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtChildSurname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtChildSurname.Properties.Appearance.Options.UseFont = True
        Me.txtChildSurname.Properties.Appearance.Options.UseTextOptions = True
        Me.txtChildSurname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtChildSurname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtChildSurname.Properties.MaxLength = 30
        Me.txtChildSurname.Size = New System.Drawing.Size(235, 22)
        Me.txtChildSurname.TabIndex = 3
        Me.txtChildSurname.Tag = "AEM"
        Me.txtChildSurname.TextAlign = HorizontalAlignment.Left
        Me.txtChildSurname.ToolTipText = ""
        '
        'CareLabel23
        '
        Me.CareLabel23.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel23.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel23.Location = New System.Drawing.Point(10, 60)
        Me.CareLabel23.Name = "CareLabel23"
        Me.CareLabel23.Size = New System.Drawing.Size(47, 15)
        Me.CareLabel23.TabIndex = 2
        Me.CareLabel23.Text = "Surname"
        Me.CareLabel23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtChildForename
        '
        Me.txtChildForename.CharacterCasing = CharacterCasing.Normal
        Me.txtChildForename.EnterMoveNextControl = True
        Me.txtChildForename.Location = New System.Drawing.Point(100, 29)
        Me.txtChildForename.MaxLength = 30
        Me.txtChildForename.Name = "txtChildForename"
        Me.txtChildForename.NumericAllowNegatives = False
        Me.txtChildForename.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtChildForename.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtChildForename.Properties.AccessibleName = "Child Forename"
        Me.txtChildForename.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtChildForename.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtChildForename.Properties.Appearance.Options.UseFont = True
        Me.txtChildForename.Properties.Appearance.Options.UseTextOptions = True
        Me.txtChildForename.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtChildForename.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtChildForename.Properties.MaxLength = 30
        Me.txtChildForename.Size = New System.Drawing.Size(235, 22)
        Me.txtChildForename.TabIndex = 1
        Me.txtChildForename.Tag = "AEM"
        Me.txtChildForename.TextAlign = HorizontalAlignment.Left
        Me.txtChildForename.ToolTipText = ""
        '
        'CareLabel24
        '
        Me.CareLabel24.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel24.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel24.Location = New System.Drawing.Point(10, 32)
        Me.CareLabel24.Name = "CareLabel24"
        Me.CareLabel24.Size = New System.Drawing.Size(53, 15)
        Me.CareLabel24.TabIndex = 0
        Me.CareLabel24.Text = "Forename"
        Me.CareLabel24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.cbxSite)
        Me.GroupControl2.Controls.Add(Me.CareLabel6)
        Me.GroupControl2.Controls.Add(Me.CareLabel5)
        Me.GroupControl2.Controls.Add(Me.cbxVia)
        Me.GroupControl2.Controls.Add(Me.CareLabel9)
        Me.GroupControl2.Controls.Add(Me.cbxFoundUs)
        Me.GroupControl2.Controls.Add(Me.cbxRating)
        Me.GroupControl2.Controls.Add(Me.lblEntered)
        Me.GroupControl2.Controls.Add(Me.cdtEntered)
        Me.GroupControl2.Controls.Add(Me.CareLabel13)
        Me.GroupControl2.Controls.Add(Me.txtStage)
        Me.GroupControl2.Controls.Add(Me.CareLabel14)
        Me.GroupControl2.Controls.Add(Me.CareLabel15)
        Me.GroupControl2.Location = New System.Drawing.Point(366, 8)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(344, 197)
        Me.GroupControl2.TabIndex = 2
        Me.GroupControl2.Text = "Lead Details"
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(100, 27)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AccessibleName = "Site"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(234, 22)
        Me.cbxSite.TabIndex = 1
        Me.cbxSite.Tag = "AEM"
        Me.cbxSite.ValueMember = Nothing
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(10, 30)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(19, 15)
        Me.CareLabel6.TabIndex = 0
        Me.CareLabel6.Text = "Site"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(10, 142)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(16, 15)
        Me.CareLabel5.TabIndex = 9
        Me.CareLabel5.Text = "Via"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxVia
        '
        Me.cbxVia.AllowBlank = False
        Me.cbxVia.DataSource = Nothing
        Me.cbxVia.DisplayMember = Nothing
        Me.cbxVia.EnterMoveNextControl = True
        Me.cbxVia.Location = New System.Drawing.Point(100, 139)
        Me.cbxVia.Name = "cbxVia"
        Me.cbxVia.Properties.AccessibleName = "Gender"
        Me.cbxVia.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxVia.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxVia.Properties.Appearance.Options.UseFont = True
        Me.cbxVia.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxVia.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxVia.SelectedValue = Nothing
        Me.cbxVia.Size = New System.Drawing.Size(234, 22)
        Me.cbxVia.TabIndex = 10
        Me.cbxVia.Tag = "AE"
        Me.cbxVia.ValueMember = Nothing
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(10, 114)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(50, 15)
        Me.CareLabel9.TabIndex = 7
        Me.CareLabel9.Text = "Found Us"
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxFoundUs
        '
        Me.cbxFoundUs.AllowBlank = False
        Me.cbxFoundUs.DataSource = Nothing
        Me.cbxFoundUs.DisplayMember = Nothing
        Me.cbxFoundUs.EnterMoveNextControl = True
        Me.cbxFoundUs.Location = New System.Drawing.Point(100, 111)
        Me.cbxFoundUs.Name = "cbxFoundUs"
        Me.cbxFoundUs.Properties.AccessibleName = "Gender"
        Me.cbxFoundUs.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxFoundUs.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxFoundUs.Properties.Appearance.Options.UseFont = True
        Me.cbxFoundUs.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxFoundUs.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxFoundUs.SelectedValue = Nothing
        Me.cbxFoundUs.Size = New System.Drawing.Size(234, 22)
        Me.cbxFoundUs.TabIndex = 8
        Me.cbxFoundUs.Tag = "AE"
        Me.cbxFoundUs.ValueMember = Nothing
        '
        'cbxRating
        '
        Me.cbxRating.AllowBlank = False
        Me.cbxRating.DataSource = Nothing
        Me.cbxRating.DisplayMember = Nothing
        Me.cbxRating.EnterMoveNextControl = True
        Me.cbxRating.Location = New System.Drawing.Point(100, 167)
        Me.cbxRating.Name = "cbxRating"
        Me.cbxRating.Properties.AccessibleName = "Gender"
        Me.cbxRating.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxRating.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxRating.Properties.Appearance.Options.UseFont = True
        Me.cbxRating.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxRating.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxRating.SelectedValue = Nothing
        Me.cbxRating.Size = New System.Drawing.Size(234, 22)
        Me.cbxRating.TabIndex = 12
        Me.cbxRating.Tag = "AE"
        Me.cbxRating.ValueMember = Nothing
        '
        'lblEntered
        '
        Me.lblEntered.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEntered.Appearance.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.lblEntered.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblEntered.Location = New System.Drawing.Point(210, 60)
        Me.lblEntered.Name = "lblEntered"
        Me.lblEntered.Size = New System.Drawing.Size(99, 15)
        Me.lblEntered.TabIndex = 4
        Me.lblEntered.Text = "1 year, 11 months"
        Me.lblEntered.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtEntered
        '
        Me.cdtEntered.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtEntered.EnterMoveNextControl = True
        Me.cdtEntered.Location = New System.Drawing.Point(100, 55)
        Me.cdtEntered.Name = "cdtEntered"
        Me.cdtEntered.Properties.AccessibleName = "Date of Birth"
        Me.cdtEntered.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtEntered.Properties.Appearance.Options.UseFont = True
        Me.cdtEntered.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtEntered.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtEntered.Size = New System.Drawing.Size(104, 22)
        Me.cdtEntered.TabIndex = 3
        Me.cdtEntered.Tag = "AEBM"
        Me.cdtEntered.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'CareLabel13
        '
        Me.CareLabel13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel13.Location = New System.Drawing.Point(10, 170)
        Me.CareLabel13.Name = "CareLabel13"
        Me.CareLabel13.Size = New System.Drawing.Size(34, 15)
        Me.CareLabel13.TabIndex = 11
        Me.CareLabel13.Text = "Rating"
        Me.CareLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtStage
        '
        Me.txtStage.CharacterCasing = CharacterCasing.Normal
        Me.txtStage.EnterMoveNextControl = True
        Me.txtStage.Location = New System.Drawing.Point(100, 83)
        Me.txtStage.MaxLength = 0
        Me.txtStage.Name = "txtStage"
        Me.txtStage.NumericAllowNegatives = False
        Me.txtStage.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtStage.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStage.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtStage.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtStage.Properties.Appearance.Options.UseFont = True
        Me.txtStage.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStage.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtStage.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtStage.Properties.ReadOnly = True
        Me.txtStage.Size = New System.Drawing.Size(234, 22)
        Me.txtStage.TabIndex = 6
        Me.txtStage.Tag = ""
        Me.txtStage.TextAlign = HorizontalAlignment.Left
        Me.txtStage.ToolTipText = ""
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(10, 86)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(29, 15)
        Me.CareLabel14.TabIndex = 5
        Me.CareLabel14.Text = "Stage"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel15
        '
        Me.CareLabel15.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel15.Location = New System.Drawing.Point(10, 60)
        Me.CareLabel15.Name = "CareLabel15"
        Me.CareLabel15.Size = New System.Drawing.Size(67, 15)
        Me.CareLabel15.TabIndex = 2
        Me.CareLabel15.Text = "Date Entered"
        Me.CareLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.CareLabel18)
        Me.GroupControl1.Controls.Add(Me.CareLabel7)
        Me.GroupControl1.Controls.Add(Me.chkMarketing)
        Me.GroupControl1.Controls.Add(Me.cbxCommMethod)
        Me.GroupControl1.Controls.Add(Me.txtTel)
        Me.GroupControl1.Controls.Add(Me.txtMobile)
        Me.GroupControl1.Controls.Add(Me.txtEmail)
        Me.GroupControl1.Controls.Add(Me.Label13)
        Me.GroupControl1.Controls.Add(Me.Label8)
        Me.GroupControl1.Controls.Add(Me.Label9)
        Me.GroupControl1.Controls.Add(Me.txtAddressFull)
        Me.GroupControl1.Controls.Add(Me.Label2)
        Me.GroupControl1.Controls.Add(Me.txtContactRelationship)
        Me.GroupControl1.Controls.Add(Me.CareLabel4)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Controls.Add(Me.txtContactSurname)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.txtContactForename)
        Me.GroupControl1.Location = New System.Drawing.Point(11, 8)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(344, 363)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Contact Details"
        '
        'CareLabel18
        '
        Me.CareLabel18.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel18.Location = New System.Drawing.Point(10, 336)
        Me.CareLabel18.Name = "CareLabel18"
        Me.CareLabel18.Size = New System.Drawing.Size(59, 15)
        Me.CareLabel18.TabIndex = 16
        Me.CareLabel18.Text = "Marketing?"
        Me.CareLabel18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(10, 309)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(72, 15)
        Me.CareLabel7.TabIndex = 14
        Me.CareLabel7.Text = "Comm. Prefs."
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkMarketing
        '
        Me.chkMarketing.EnterMoveNextControl = True
        Me.chkMarketing.Location = New System.Drawing.Point(100, 334)
        Me.chkMarketing.Name = "chkMarketing"
        Me.chkMarketing.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMarketing.Properties.Appearance.Options.UseFont = True
        Me.chkMarketing.Size = New System.Drawing.Size(20, 19)
        Me.chkMarketing.TabIndex = 17
        Me.chkMarketing.Tag = "AE"
        '
        'cbxCommMethod
        '
        Me.cbxCommMethod.AllowBlank = False
        Me.cbxCommMethod.DataSource = Nothing
        Me.cbxCommMethod.DisplayMember = Nothing
        Me.cbxCommMethod.EnterMoveNextControl = True
        Me.cbxCommMethod.Location = New System.Drawing.Point(100, 306)
        Me.cbxCommMethod.Name = "cbxCommMethod"
        Me.cbxCommMethod.Properties.AccessibleName = "Communication Preferences"
        Me.cbxCommMethod.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxCommMethod.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxCommMethod.Properties.Appearance.Options.UseFont = True
        Me.cbxCommMethod.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxCommMethod.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxCommMethod.SelectedValue = Nothing
        Me.cbxCommMethod.Size = New System.Drawing.Size(234, 22)
        Me.cbxCommMethod.TabIndex = 15
        Me.cbxCommMethod.Tag = "AEM"
        Me.cbxCommMethod.ValueMember = Nothing
        '
        'txtTel
        '
        Me.txtTel.AccessibleName = "Contact Telephone Number"
        Me.txtTel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTel.Appearance.Options.UseFont = True
        Me.txtTel.IsMobile = True
        Me.txtTel.Location = New System.Drawing.Point(100, 222)
        Me.txtTel.MaxLength = 15
        Me.txtTel.MinimumSize = New System.Drawing.Size(0, 22)
        Me.txtTel.Name = "txtTel"
        Me.txtTel.ReadOnly = False
        Me.txtTel.Size = New System.Drawing.Size(234, 22)
        Me.txtTel.TabIndex = 9
        Me.txtTel.Tag = "AE"
        '
        'txtMobile
        '
        Me.txtMobile.AccessibleName = "Contact Mobile Number"
        Me.txtMobile.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMobile.Appearance.Options.UseFont = True
        Me.txtMobile.IsMobile = True
        Me.txtMobile.Location = New System.Drawing.Point(100, 250)
        Me.txtMobile.MaxLength = 15
        Me.txtMobile.MinimumSize = New System.Drawing.Size(0, 22)
        Me.txtMobile.Name = "txtMobile"
        Me.txtMobile.ReadOnly = False
        Me.txtMobile.Size = New System.Drawing.Size(234, 22)
        Me.txtMobile.TabIndex = 11
        Me.txtMobile.Tag = "AE"
        '
        'txtEmail
        '
        Me.txtEmail.AccessibleName = "Contact Email"
        Me.txtEmail.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail.Appearance.Options.UseFont = True
        Me.txtEmail.Location = New System.Drawing.Point(100, 278)
        Me.txtEmail.MaxLength = 100
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.NoButton = False
        Me.txtEmail.NoColours = False
        Me.txtEmail.NoValidate = False
        Me.txtEmail.ReadOnly = False
        Me.txtEmail.Size = New System.Drawing.Size(234, 22)
        Me.txtEmail.TabIndex = 13
        Me.txtEmail.Tag = "AE"
        '
        'Label13
        '
        Me.Label13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label13.Location = New System.Drawing.Point(10, 282)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(29, 15)
        Me.Label13.TabIndex = 12
        Me.Label13.Text = "Email"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label8.Location = New System.Drawing.Point(10, 254)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(37, 15)
        Me.Label8.TabIndex = 10
        Me.Label8.Text = "Mobile"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label9.Location = New System.Drawing.Point(10, 226)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(56, 15)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "Telephone"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAddressFull
        '
        Me.txtAddressFull.AccessibleName = "Address"
        Me.txtAddressFull.Address = ""
        Me.txtAddressFull.AddressBlock = ""
        Me.txtAddressFull.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddressFull.Appearance.Options.UseFont = True
        Me.txtAddressFull.County = ""
        Me.txtAddressFull.DisplayMode = Care.Address.CareAddress.EnumDisplayMode.SingleControl
        Me.txtAddressFull.Location = New System.Drawing.Point(100, 121)
        Me.txtAddressFull.MaxLength = 500
        Me.txtAddressFull.Name = "txtAddressFull"
        Me.txtAddressFull.PostCode = ""
        Me.txtAddressFull.ReadOnly = False
        Me.txtAddressFull.Size = New System.Drawing.Size(234, 95)
        Me.txtAddressFull.TabIndex = 7
        Me.txtAddressFull.Tag = "AE"
        Me.txtAddressFull.Town = ""
        '
        'Label2
        '
        Me.Label2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label2.Location = New System.Drawing.Point(10, 126)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 15)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Address"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtContactRelationship
        '
        Me.txtContactRelationship.CharacterCasing = CharacterCasing.Normal
        Me.txtContactRelationship.EnterMoveNextControl = True
        Me.txtContactRelationship.Location = New System.Drawing.Point(100, 81)
        Me.txtContactRelationship.MaxLength = 30
        Me.txtContactRelationship.Name = "txtContactRelationship"
        Me.txtContactRelationship.NumericAllowNegatives = False
        Me.txtContactRelationship.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtContactRelationship.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtContactRelationship.Properties.AccessibleName = "Relationship"
        Me.txtContactRelationship.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtContactRelationship.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtContactRelationship.Properties.Appearance.Options.UseFont = True
        Me.txtContactRelationship.Properties.Appearance.Options.UseTextOptions = True
        Me.txtContactRelationship.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtContactRelationship.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtContactRelationship.Properties.MaxLength = 30
        Me.txtContactRelationship.Size = New System.Drawing.Size(234, 22)
        Me.txtContactRelationship.TabIndex = 5
        Me.txtContactRelationship.Tag = "AEM"
        Me.txtContactRelationship.TextAlign = HorizontalAlignment.Left
        Me.txtContactRelationship.ToolTipText = ""
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(10, 84)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(65, 15)
        Me.CareLabel4.TabIndex = 4
        Me.CareLabel4.Text = "Relationship"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(10, 32)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(53, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Forename"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtContactSurname
        '
        Me.txtContactSurname.CharacterCasing = CharacterCasing.Normal
        Me.txtContactSurname.EnterMoveNextControl = True
        Me.txtContactSurname.Location = New System.Drawing.Point(100, 55)
        Me.txtContactSurname.MaxLength = 30
        Me.txtContactSurname.Name = "txtContactSurname"
        Me.txtContactSurname.NumericAllowNegatives = False
        Me.txtContactSurname.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtContactSurname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtContactSurname.Properties.AccessibleName = "Contact Surname"
        Me.txtContactSurname.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtContactSurname.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtContactSurname.Properties.Appearance.Options.UseFont = True
        Me.txtContactSurname.Properties.Appearance.Options.UseTextOptions = True
        Me.txtContactSurname.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtContactSurname.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtContactSurname.Properties.MaxLength = 30
        Me.txtContactSurname.Size = New System.Drawing.Size(234, 22)
        Me.txtContactSurname.TabIndex = 3
        Me.txtContactSurname.Tag = "AEM"
        Me.txtContactSurname.TextAlign = HorizontalAlignment.Left
        Me.txtContactSurname.ToolTipText = ""
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(10, 58)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(47, 15)
        Me.CareLabel2.TabIndex = 2
        Me.CareLabel2.Text = "Surname"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtContactForename
        '
        Me.txtContactForename.CharacterCasing = CharacterCasing.Normal
        Me.txtContactForename.EnterMoveNextControl = True
        Me.txtContactForename.Location = New System.Drawing.Point(100, 27)
        Me.txtContactForename.MaxLength = 30
        Me.txtContactForename.Name = "txtContactForename"
        Me.txtContactForename.NumericAllowNegatives = False
        Me.txtContactForename.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtContactForename.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtContactForename.Properties.AccessibleName = "Contact Forename"
        Me.txtContactForename.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtContactForename.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtContactForename.Properties.Appearance.Options.UseFont = True
        Me.txtContactForename.Properties.Appearance.Options.UseTextOptions = True
        Me.txtContactForename.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtContactForename.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtContactForename.Properties.MaxLength = 30
        Me.txtContactForename.Size = New System.Drawing.Size(234, 22)
        Me.txtContactForename.TabIndex = 1
        Me.txtContactForename.Tag = "AEM"
        Me.txtContactForename.TextAlign = HorizontalAlignment.Left
        Me.txtContactForename.ToolTipText = ""
        '
        'tabNotes
        '
        Me.tabNotes.Controls.Add(Me.memChildNotes)
        Me.tabNotes.Name = "tabNotes"
        Me.tabNotes.Size = New System.Drawing.Size(720, 510)
        Me.tabNotes.Text = "Child Notes"
        '
        'memChildNotes
        '
        Me.memChildNotes.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.memChildNotes.Location = New System.Drawing.Point(10, 9)
        Me.memChildNotes.Name = "memChildNotes"
        Me.memChildNotes.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.memChildNotes.Properties.Appearance.Options.UseFont = True
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.memChildNotes, True)
        Me.memChildNotes.Size = New System.Drawing.Size(701, 492)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.memChildNotes, OptionsSpelling2)
        Me.memChildNotes.TabIndex = 0
        Me.memChildNotes.Tag = "AEB"
        '
        'tabAvailability
        '
        Me.tabAvailability.Controls.Add(Me.YearView1)
        Me.tabAvailability.Name = "tabAvailability"
        Me.tabAvailability.Size = New System.Drawing.Size(720, 510)
        Me.tabAvailability.Text = "Availability"
        '
        'YearView1
        '
        Me.YearView1.CalendarFont = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.YearView1.Dock = DockStyle.Fill
        Me.YearView1.Location = New System.Drawing.Point(0, 0)
        Me.YearView1.Name = "YearView1"
        Me.YearView1.Size = New System.Drawing.Size(720, 510)
        Me.YearView1.TabIndex = 0
        '
        'tabActivity
        '
        Me.tabActivity.Controls.Add(Me.CRM)
        Me.tabActivity.Name = "tabActivity"
        Me.tabActivity.Size = New System.Drawing.Size(720, 510)
        Me.tabActivity.Text = "Activity"
        '
        'CRM
        '
        Me.CRM.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.CRM.CRMContactEmail = Nothing
        Me.CRM.CRMContactID = Nothing
        Me.CRM.CRMContactMobile = Nothing
        Me.CRM.CRMContactName = Nothing
        Me.CRM.CRMLinkID = Nothing
        Me.CRM.CRMLinkType = Care.[Shared].CRM.EnumLinkType.Lead
        Me.CRM.Location = New System.Drawing.Point(8, 7)
        Me.CRM.Name = "CRM"
        Me.CRM.Size = New System.Drawing.Size(707, 495)
        Me.CRM.TabIndex = 0
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.DialogResult = DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(648, 552)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 23)
        Me.btnCancel.TabIndex = 6
        Me.btnCancel.Text = "Cancel"
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnEdit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnEdit.Appearance.Options.UseFont = True
        Me.btnEdit.Location = New System.Drawing.Point(12, 552)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(100, 23)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Text = "Edit Lead"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(557, 552)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(85, 23)
        Me.btnOK.TabIndex = 5
        Me.btnOK.Text = "OK"
        '
        'btnProgress
        '
        Me.btnProgress.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnProgress.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnProgress.Appearance.Options.UseFont = True
        Me.btnProgress.Location = New System.Drawing.Point(118, 552)
        Me.btnProgress.Name = "btnProgress"
        Me.btnProgress.Size = New System.Drawing.Size(100, 23)
        Me.btnProgress.TabIndex = 2
        Me.btnProgress.Text = "Progress Lead"
        '
        'btnAvailEmail
        '
        Me.btnAvailEmail.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnAvailEmail.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnAvailEmail.Appearance.Options.UseFont = True
        Me.btnAvailEmail.Location = New System.Drawing.Point(330, 552)
        Me.btnAvailEmail.Name = "btnAvailEmail"
        Me.btnAvailEmail.Size = New System.Drawing.Size(140, 23)
        Me.btnAvailEmail.TabIndex = 4
        Me.btnAvailEmail.Text = "Send Availability Email"
        '
        'btnDeleteLead
        '
        Me.btnDeleteLead.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnDeleteLead.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnDeleteLead.Appearance.Options.UseFont = True
        Me.btnDeleteLead.Location = New System.Drawing.Point(224, 552)
        Me.btnDeleteLead.Name = "btnDeleteLead"
        Me.btnDeleteLead.Size = New System.Drawing.Size(100, 23)
        Me.btnDeleteLead.TabIndex = 3
        Me.btnDeleteLead.Text = "Delete Lead"
        '
        'frmLead
        '
        Me.AcceptButton = Me.btnOK
        Me.Appearance.Options.UseFont = True
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(745, 584)
        Me.Controls.Add(Me.btnDeleteLead)
        Me.Controls.Add(Me.btnAvailEmail)
        Me.Controls.Add(Me.btnProgress)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.CareTab1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLead"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = ""
        CType(Me.CareTab1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CareTab1.ResumeLayout(False)
        Me.tabLead.ResumeLayout(False)
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        CType(Me.memContactNotes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.chkMonday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkTuesday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkWednesday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkThursday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkFriday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxExpectedRoom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtExpectedStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtExpectedStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl6.ResumeLayout(False)
        Me.GroupControl6.PerformLayout()
        CType(Me.cbxChildGender.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDOB.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtDOB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtChildSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtChildForename.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxVia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxFoundUs.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxRating.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtEntered.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtEntered.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.chkMarketing.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxCommMethod.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtContactRelationship.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtContactSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtContactForename.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabNotes.ResumeLayout(False)
        CType(Me.memChildNotes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabAvailability.ResumeLayout(False)
        Me.tabActivity.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CareTab1 As Care.Controls.CareTab
    Friend WithEvents tabLead As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl6 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtChildSurname As Care.Controls.CareTextBox
    Friend WithEvents CareLabel23 As Care.Controls.CareLabel
    Friend WithEvents txtChildForename As Care.Controls.CareTextBox
    Friend WithEvents CareLabel24 As Care.Controls.CareLabel
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel13 As Care.Controls.CareLabel
    Friend WithEvents txtStage As Care.Controls.CareTextBox
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents CareLabel15 As Care.Controls.CareLabel
    Friend WithEvents tabAvailability As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtContactRelationship As Care.Controls.CareTextBox
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents txtContactSurname As Care.Controls.CareTextBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents txtContactForename As Care.Controls.CareTextBox
    Friend WithEvents txtAddressFull As Care.Address.CareAddress
    Friend WithEvents Label2 As Care.Controls.CareLabel
    Friend WithEvents Label12 As Care.Controls.CareLabel
    Friend WithEvents cbxChildGender As Care.Controls.CareComboBox
    Friend WithEvents lblAge As Care.Controls.CareLabel
    Friend WithEvents Label16 As Care.Controls.CareLabel
    Private WithEvents cdtDOB As Care.Controls.CareDateTime
    Private WithEvents cdtEntered As Care.Controls.CareDateTime
    Friend WithEvents txtTel As Care.Shared.CareTelephoneNumber
    Friend WithEvents txtMobile As Care.Shared.CareTelephoneNumber
    Friend WithEvents txtEmail As Care.Shared.CareEmailAddress
    Friend WithEvents Label13 As Care.Controls.CareLabel
    Friend WithEvents Label8 As Care.Controls.CareLabel
    Friend WithEvents Label9 As Care.Controls.CareLabel
    Friend WithEvents tabActivity As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents lblEntered As Care.Controls.CareLabel
    Friend WithEvents cbxRating As Care.Controls.CareComboBox
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents cbxFoundUs As Care.Controls.CareComboBox
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents CareLabel17 As Care.Controls.CareLabel
    Friend WithEvents CareLabel16 As Care.Controls.CareLabel
    Friend WithEvents CareLabel12 As Care.Controls.CareLabel
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents chkMonday As Care.Controls.CareCheckBox
    Friend WithEvents chkTuesday As Care.Controls.CareCheckBox
    Friend WithEvents chkWednesday As Care.Controls.CareCheckBox
    Friend WithEvents chkThursday As Care.Controls.CareCheckBox
    Friend WithEvents chkFriday As Care.Controls.CareCheckBox
    Friend WithEvents CareLabel10 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents cbxExpectedRoom As Care.Controls.CareComboBox
    Friend WithEvents lblStart As Care.Controls.CareLabel
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Private WithEvents cdtExpectedStart As Care.Controls.CareDateTime
    Friend WithEvents YearView1 As Nursery.YearView
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents btnEdit As Care.Controls.CareButton
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents memContactNotes As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents cbxVia As Care.Controls.CareComboBox
    Friend WithEvents tabNotes As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents memChildNotes As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents btnProgress As Care.Controls.CareButton
    Friend WithEvents CRM As Care.Shared.CRMActivity
    Friend WithEvents btnAvailEmail As Care.Controls.CareButton
    Friend WithEvents btnDeleteLead As Care.Controls.CareButton
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents cbxCommMethod As Care.Controls.CareComboBox
    Friend WithEvents CareLabel18 As Care.Controls.CareLabel
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents chkMarketing As Care.Controls.CareCheckBox
End Class
