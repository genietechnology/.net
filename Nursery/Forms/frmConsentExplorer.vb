﻿Imports Care.Global
Imports Care.Data

Public Class frmConsentExplorer

    Private m_Consent As New List(Of AllergyMatrix)

    Private Sub frmConsentExplorer_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        With cbxSite
            .PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)
            .SelectedIndex = 0
        End With

        DisplayConsent()

    End Sub

    Private Sub DisplayConsent()

        m_Consent.Clear()

        Dim _SQL As String = ""
        _SQL += "select AppListItems.name from AppListItems"
        _SQL += " left join AppLists on AppLists.ID = AppListItems.list_id"
        _SQL += " where AppLists.name = 'Consent'"
        _SQL += " order by AppListItems.seq, AppListItems.name"

        Dim _DT As DataTable = Care.Data.DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then
            For Each _DR As DataRow In _DT.Rows
                m_Consent.Add(New AllergyMatrix(_DR.Item("name").ToString, False))
            Next
        End If

        grdConsent.Populate(m_Consent)

        grdConsent.Columns("Description").Caption = "Consent"
        grdConsent.Columns("Description").OptionsColumn.AllowEdit = False
        grdConsent.Columns("Selected").Caption = " "

    End Sub

    Private Sub RefreshChildren()

        If cbxSite.Text = "" Then
            CareMessage("Please select a Site.", MessageBoxIcon.Exclamation, "Refresh")
            Exit Sub
        End If

        Dim _ConsentList As String = ""

        For Each _c In m_Consent
            If _c.Selected Then
                If _ConsentList = "" Then
                    _ConsentList = $"'{_c.Description}'"
                Else
                    _ConsentList += $", '{_c.Description}'"
                End If
            End If
        Next

        If _ConsentList = "" Then
            CareMessage("Please select at least one consent item.", MessageBoxIcon.Exclamation, "Refresh")
            Exit Sub
        End If

        Dim _SQL As String = ""
        Dim _InNotIn As String = "in"
        If radWithout.Checked Then _InNotIn = "not in"


        _SQL += "select c.id, c.fullname as 'Name', c.dob as 'DOB', c.keyworker_name as 'Key Person', c.group_name as 'Room', c.med_allergies as 'Allergies'," + vbCrLf
        _SQL += " c.med_medication As 'Medication', c.med_notes as 'Medical Notes', c.diet_restrict as 'Dietary Restrictions', c.diet_notes as 'Dietary Notes'" + vbCrLf
        _SQL += " from Children c" + vbCrLf
        _SQL += " where status = 'Current'" + vbCrLf
        _SQL += $" and c.site_id = '{cbxSite.SelectedValue.ToString}'" + vbCrLf
        _SQL += $" and c.id {_InNotIn} (select cc.child_id from ChildConsent cc where consent in ({_ConsentList}))" + vbCrLf

        grdChildren.HideFirstColumn = True
        grdChildren.Populate(Session.ConnectionString, _SQL)
        grdChildren.AutoSizeColumns()

    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        RefreshChildren()
    End Sub

    Private Sub grdChildren_GridDoubleClick(sender As Object, e As EventArgs) Handles grdChildren.GridDoubleClick

        If grdChildren.RecordCount < 1 Then Exit Sub

        If grdChildren.CurrentRow(0).ToString <> "" Then
            Session.CursorWaiting()
            Dim _ID As New Guid(grdChildren.CurrentRow("id").ToString)
            Business.Child.DrillDown(_ID)
        End If

    End Sub

End Class