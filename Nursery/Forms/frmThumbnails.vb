﻿Imports Care.Global
Imports Care.Data
Imports Care.Shared

Public Class frmThumbnails

    Private Sub Run()

        Dim _ThumbnailSize As Integer = 325

        Dim _SQL As String = ""
        _SQL += "select ID from AppDocs"
        _SQL += " where tags <> 'original' and tags <> 'thumbnail'"
        _SQL += " and (file_ext in ('jpg','jpeg','png','tiff','bmp') or subject like '%photo%')"
        _SQL += " order by stamp desc"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            Session.SetupProgressBar("Processing Thumbnails...", _DT.Rows.Count)
            For Each _DR As DataRow In _DT.Rows

                Dim _ID As Guid = New Guid(_DR.Item("ID").ToString)

                'fetch the document
                Dim _SourceDoc As Care.Shared.Business.Document = Care.Shared.Business.Document.RetreiveByID(Session.ConnectionString, _ID)
                If _SourceDoc IsNot Nothing Then

                    Dim _SourceImage As System.Drawing.Image = Care.Shared.DocumentHandler.GetImagefromByteArray(_SourceDoc._Data)
                    If _SourceImage IsNot Nothing Then
                        If _SourceImage.Size.Height > _ThumbnailSize OrElse _SourceImage.Size.Width > _ThumbnailSize Then

                            'create a new document based upon the original
                            DocumentHandler.InsertDocument(_SourceDoc._KeyId, Care.Shared.DocumentHandler.DocumentType.Application, _SourceDoc._Data, "Original " + _SourceDoc._Subject, "original")

                            'overwrite it with the thumbnail
                            _SourceDoc._Data = Care.Shared.DocumentHandler.ReturnThumbnail(_SourceDoc._Data, _ThumbnailSize)
                            _SourceDoc._FileSize = _SourceDoc._Data.Length
                            _SourceDoc._Tags = "thumbnail"

                            'save document
                            Care.Shared.Business.Document.SaveRecord(_SourceDoc)

                        End If
                    End If
                End If
                Session.StepProgressBar()
            Next

            Session.SetProgressMessage("Process Complete.", 5)

        End If

        _DT.Dispose()
        _DT = Nothing

    End Sub

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click
        Run()
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class