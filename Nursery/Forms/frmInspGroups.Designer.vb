﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInspGroups
    Inherits Care.Shared.frmGridMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtMonthsMin = New Care.Controls.CareTextBox()
        Me.txtMonthsMax = New Care.Controls.CareTextBox()
        Me.CareLabel2 = New Care.Controls.CareLabel()
        Me.CareLabel1 = New Care.Controls.CareLabel()
        Me.txtChildCapacity = New Care.Controls.CareTextBox()
        Me.Label2 = New Care.Controls.CareLabel()
        Me.txtName = New Care.Controls.CareTextBox()
        Me.Label1 = New Care.Controls.CareLabel()
        Me.CareLabel3 = New Care.Controls.CareLabel()
        Me.txtStaffRatio = New Care.Controls.CareTextBox()
        Me.CareLabel4 = New Care.Controls.CareLabel()
        Me.gbx.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.txtMonthsMin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMonthsMax.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtChildCapacity.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStaffRatio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbx
        '
        Me.gbx.Controls.Add(Me.CareLabel4)
        Me.gbx.Controls.Add(Me.CareLabel3)
        Me.gbx.Controls.Add(Me.txtStaffRatio)
        Me.gbx.Controls.Add(Me.txtMonthsMin)
        Me.gbx.Controls.Add(Me.txtMonthsMax)
        Me.gbx.Controls.Add(Me.CareLabel2)
        Me.gbx.Controls.Add(Me.CareLabel1)
        Me.gbx.Controls.Add(Me.txtChildCapacity)
        Me.gbx.Controls.Add(Me.Label2)
        Me.gbx.Controls.Add(Me.txtName)
        Me.gbx.Controls.Add(Me.Label1)
        Me.gbx.Size = New System.Drawing.Size(383, 192)
        Me.gbx.Controls.SetChildIndex(Me.Panel2, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label1, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtName, 0)
        Me.gbx.Controls.SetChildIndex(Me.Label2, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtChildCapacity, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel1, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel2, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtMonthsMax, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtMonthsMin, 0)
        Me.gbx.Controls.SetChildIndex(Me.txtStaffRatio, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel3, 0)
        Me.gbx.Controls.SetChildIndex(Me.CareLabel4, 0)
        '
        'Panel2
        '
        Me.Panel2.Location = New System.Drawing.Point(3, 161)
        Me.Panel2.Size = New System.Drawing.Size(377, 28)
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(192, 0)
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(283, 0)
        '
        'Grid
        '
        Me.Grid.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Grid.Appearance.Options.UseFont = True
        Me.Grid.Size = New System.Drawing.Size(419, 349)
        '
        'Panel1
        '
        Me.Panel1.Size = New System.Drawing.Size(438, 35)
        '
        'btnDelete
        '
        Me.btnDelete.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Appearance.Options.UseFont = True
        '
        'btnEdit
        '
        Me.btnEdit.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.Appearance.Options.UseFont = True
        '
        'btnAdd
        '
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.Appearance.Options.UseFont = True
        '
        'btnClose
        '
        Me.btnClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Appearance.Options.UseFont = True
        Me.btnClose.Location = New System.Drawing.Point(344, 5)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'txtMonthsMin
        '
        Me.txtMonthsMin.CharacterCasing = CharacterCasing.Normal
        Me.txtMonthsMin.EnterMoveNextControl = True
        Me.txtMonthsMin.Location = New System.Drawing.Point(127, 49)
        Me.txtMonthsMin.MaxLength = 0
        Me.txtMonthsMin.Name = "txtMonthsMin"
        Me.txtMonthsMin.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtMonthsMin.Properties.AccessibleName = "Display Sequence"
        Me.txtMonthsMin.Properties.Appearance.Options.UseTextOptions = True
        Me.txtMonthsMin.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtMonthsMin.ReadOnly = False
        Me.txtMonthsMin.Size = New System.Drawing.Size(56, 20)
        Me.txtMonthsMin.TabIndex = 10
        Me.txtMonthsMin.Tag = "AEN"
        Me.txtMonthsMin.TextAlign = HorizontalAlignment.Left
        Me.txtMonthsMin.ToolTipText = ""
        '
        'txtMonthsMax
        '
        Me.txtMonthsMax.CharacterCasing = CharacterCasing.Normal
        Me.txtMonthsMax.EnterMoveNextControl = True
        Me.txtMonthsMax.Location = New System.Drawing.Point(127, 75)
        Me.txtMonthsMax.MaxLength = 0
        Me.txtMonthsMax.Name = "txtMonthsMax"
        Me.txtMonthsMax.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtMonthsMax.Properties.AccessibleName = "Display Sequence"
        Me.txtMonthsMax.Properties.Appearance.Options.UseTextOptions = True
        Me.txtMonthsMax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtMonthsMax.ReadOnly = False
        Me.txtMonthsMax.Size = New System.Drawing.Size(56, 20)
        Me.txtMonthsMax.TabIndex = 11
        Me.txtMonthsMax.Tag = "AEN"
        Me.txtMonthsMax.TextAlign = HorizontalAlignment.Left
        Me.txtMonthsMax.ToolTipText = ""
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(10, 103)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(77, 15)
        Me.CareLabel2.TabIndex = 16
        Me.CareLabel2.Text = "Child Capacity"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(10, 77)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(78, 15)
        Me.CareLabel1.TabIndex = 15
        Me.CareLabel1.Text = "Maximum Age"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtChildCapacity
        '
        Me.txtChildCapacity.CharacterCasing = CharacterCasing.Normal
        Me.txtChildCapacity.EnterMoveNextControl = True
        Me.txtChildCapacity.Location = New System.Drawing.Point(127, 101)
        Me.txtChildCapacity.MaxLength = 0
        Me.txtChildCapacity.Name = "txtChildCapacity"
        Me.txtChildCapacity.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtChildCapacity.Properties.AccessibleName = "Display Sequence"
        Me.txtChildCapacity.Properties.Appearance.Options.UseTextOptions = True
        Me.txtChildCapacity.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtChildCapacity.ReadOnly = False
        Me.txtChildCapacity.Size = New System.Drawing.Size(56, 20)
        Me.txtChildCapacity.TabIndex = 13
        Me.txtChildCapacity.Tag = "AEN"
        Me.txtChildCapacity.TextAlign = HorizontalAlignment.Left
        Me.txtChildCapacity.ToolTipText = ""
        '
        'Label2
        '
        Me.Label2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label2.Location = New System.Drawing.Point(10, 51)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(77, 15)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Minimum Age"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.CharacterCasing = CharacterCasing.Normal
        Me.txtName.EnterMoveNextControl = True
        Me.txtName.Location = New System.Drawing.Point(127, 21)
        Me.txtName.MaxLength = 0
        Me.txtName.Name = "txtName"
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.Properties.AccessibleName = "Group Name"
        Me.txtName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtName.Properties.Appearance.Options.UseFont = True
        Me.txtName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtName.ReadOnly = False
        Me.txtName.Size = New System.Drawing.Size(244, 22)
        Me.txtName.TabIndex = 9
        Me.txtName.Tag = "AEM"
        Me.txtName.TextAlign = HorizontalAlignment.Left
        Me.txtName.ToolTipText = ""
        '
        'Label1
        '
        Me.Label1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label1.Location = New System.Drawing.Point(10, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 15)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "Name"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(10, 130)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(99, 15)
        Me.CareLabel3.TabIndex = 18
        Me.CareLabel3.Text = "Child to Staff Ratio"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtStaffRatio
        '
        Me.txtStaffRatio.CharacterCasing = CharacterCasing.Normal
        Me.txtStaffRatio.EnterMoveNextControl = True
        Me.txtStaffRatio.Location = New System.Drawing.Point(127, 128)
        Me.txtStaffRatio.MaxLength = 0
        Me.txtStaffRatio.Name = "txtStaffRatio"
        Me.txtStaffRatio.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStaffRatio.Properties.AccessibleName = "Display Sequence"
        Me.txtStaffRatio.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStaffRatio.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtStaffRatio.ReadOnly = False
        Me.txtStaffRatio.Size = New System.Drawing.Size(56, 20)
        Me.txtStaffRatio.TabIndex = 17
        Me.txtStaffRatio.Tag = "AEN"
        Me.txtStaffRatio.TextAlign = HorizontalAlignment.Left
        Me.txtStaffRatio.ToolTipText = ""
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(189, 130)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(12, 15)
        Me.CareLabel4.TabIndex = 19
        Me.CareLabel4.Text = ": 1"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmInspGroups
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(438, 402)
        Me.Name = "frmInspGroups"
        Me.gbx.ResumeLayout(False)
        Me.gbx.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.txtMonthsMin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMonthsMax.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtChildCapacity.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStaffRatio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents txtStaffRatio As Care.Controls.CareTextBox
    Friend WithEvents txtMonthsMin As Care.Controls.CareTextBox
    Friend WithEvents txtMonthsMax As Care.Controls.CareTextBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents txtChildCapacity As Care.Controls.CareTextBox
    Friend WithEvents Label2 As Care.Controls.CareLabel
    Friend WithEvents txtName As Care.Controls.CareTextBox
    Friend WithEvents Label1 As Care.Controls.CareLabel

End Class
