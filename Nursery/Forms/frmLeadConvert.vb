﻿

Imports System.Windows.Forms
Imports Care.Global
Imports Care.Data
Imports Care.Shared

Public Class frmLeadConvert

    Private m_LeadID As Guid? = Nothing
    Private m_Lead As Business.Lead = Nothing

    Public Sub New(ByVal LeadID As Guid)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_LeadID = LeadID

    End Sub

    Private Sub frmLeadConvert_Load(sender As Object, e As EventArgs) Handles Me.Load

        With cbxChildGender
            .AddItem("Male", "M")
            .AddItem("Female", "F")
        End With

        cbxSite.PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)
        cbxKeyworker.PopulateWithSQL(Session.ConnectionString, "select id, fullname from staff where keyworker = 1 order by fullname")

        MyControls.SetControls(Care.Shared.ControlHandler.Mode.Add, Me.Controls)
        DisplayRecord()

        cbxSite.Text = ParameterHandler.ReturnString("DEFSITE")

    End Sub

    Private Sub DisplayRecord()

        m_Lead = Business.Lead.RetreiveByID(m_LeadID.Value)
        With m_Lead

            txtContactForename.Text = ._ContactForename
            txtContactSurname.Text = ._ContactSurname
            txtContactRelationship.Text = ._ContactRelationship

            txtAddressFull.Address = ._ContactAddress
            txtAddressFull.PostCode = ._ContactPostcode

            txtTel.Text = ._ContactHome
            txtMobile.Text = ._ContactMobile
            txtEmail.Text = ._ContactEmail

            If txtEmail.Text <> "" Then
                chkEmailInvoices.Checked = True
                chkEmailReports.Checked = True
            End If

            txtChildForename.Text = ._ChildForename
            txtChildSurname.Text = ._ChildSurname
            cbxChildGender.SelectedValue = ._ChildGender
            cdtDOB.Value = ._ChildDob

            cdtStart.Value = ._ChildStarting
            cbxSite.Text = ._SiteName

        End With

    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click

        If MyControls.Validate(Me.Controls) Then
            If CareMessage("Are you sure you want to Convert this Lead to Family?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Convert Lead") = DialogResult.Yes Then

                btnOK.Enabled = False
                btnCancel.Enabled = False
                Session.CursorWaiting()

                Dim _F As New Business.Family
                With _F
                    ._ID = Guid.NewGuid()
                    ._Surname = txtContactSurname.Text
                    ._LetterName = txtContactForename.Text + " " + txtContactSurname.Text
                    ._Address = txtAddressFull.Text
                    ._eInvoicing = chkEmailInvoices.Checked
                    ._SiteId = New Guid(cbxSite.SelectedValue.ToString)
                    ._SiteName = cbxSite.Text
                    .Store()
                End With

                Dim _Pri As New Business.Contact
                With _Pri
                    ._FamilyId = _F._ID
                    ._Surname = txtContactSurname.Text
                    ._Forename = txtContactForename.Text
                    ._Fullname = txtContactForename.Text + " " + txtContactSurname.Text
                    ._Relationship = txtContactRelationship.Text
                    ._PrimaryCont = True
                    ._EmerCont = True
                    ._Address = txtAddressFull.Text
                    ._TelHome = txtTel.Text
                    ._TelMobile = txtMobile.Text
                    ._Email = txtEmail.Text
                    ._Report = chkEmailReports.Checked
                    ._ReportDetail = "S"
                    ._CommMethod = m_Lead._CommMethod
                    ._CommMarketing = m_Lead._CommMarketing
                    .Store()
                End With

                Dim _C As New Business.Child
                With _C
                    ._Status = "On Waiting List"
                    ._Forename = txtChildForename.Text
                    ._Surname = txtChildSurname.Text
                    ._Fullname = txtChildForename.Text + " " + txtChildSurname.Text
                    ._Dob = cdtDOB.Value
                    ._Gender = cbxChildGender.SelectedValue.ToString()
                    ._FamilyId = _F._ID
                    ._FamilyName = _F._Surname
                    ._Started = cdtStart.Value
                    ._KeyworkerId = New Guid(cbxKeyworker.SelectedValue.ToString)
                    ._KeyworkerName = cbxKeyworker.Text
                    ._GroupId = New Guid(cbxRoom.SelectedValue.ToString)
                    ._GroupName = cbxRoom.Text
                    ._ReportDetail = "S"
                    ._SiteId = New Guid(cbxSite.SelectedValue.ToString)
                    ._SiteName = cbxSite.Text
                    ._LeadId = m_LeadID
                    .Store()
                End With

                m_Lead._Closed = True
                m_Lead._ClosedDate = Today
                m_Lead.Store()

                CRM.CreateActivityRecord(m_Lead._ID.Value, CRM.EnumLinkType.Lead, CRM.EnumActivityType.Note, Nothing, m_Lead._ContactFullname, "Lead Converted", "")

                Session.CursorDefault()

                CareMessage("Lead Converted Successfully. Returning to Lead Management.", MessageBoxIcon.Information, "Convert Lead")

                Me.DialogResult = DialogResult.OK
                Me.Close()

            End If
        End If

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub cbxSite_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSite.SelectedIndexChanged
        PopulateRooms
    End Sub

    Private Sub PopulateRooms()
        If cbxSite.Text = "" Then
            cbxRoom.Clear()
            cbxRoom.SelectedIndex = -1
        Else
            cbxRoom.PopulateWithSQL(Session.ConnectionString, Business.RoomRatios.RetreiveSiteRoomSQL(cbxSite.SelectedValue.ToString))
        End If
    End Sub

End Class
