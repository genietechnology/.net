﻿Imports Care.Global
Imports System.Windows.Forms
Imports Care.Data

Public Class frmChildAnnual

    Private m_ChildID As Guid
    Private m_AnnualBatchID As Guid?

    Private m_AnnualRecord As Business.ChildAnnual = Nothing
    Private m_IsNew As Boolean = True
    Private m_Recalculated As Boolean = False
    Private m_ChildDOB As Date? = Nothing
    Private m_ChildName As String = ""

    Public Sub New(ByVal ChildID As Guid, ByVal AnnualID As Guid?)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        m_ChildID = ChildID

        If AnnualID.HasValue Then
            m_IsNew = False
            m_AnnualBatchID = AnnualID
        Else
            m_IsNew = True
            m_AnnualBatchID = Nothing
        End If

    End Sub

    Private Sub frmChildAnnual_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cbxSplitType.AddItem("Monthly")

        cbxSplitType.AddItem("1 month")
        cbxSplitType.AddItem("2 months")
        cbxSplitType.AddItem("3 months")
        cbxSplitType.AddItem("4 months")
        cbxSplitType.AddItem("5 months")
        cbxSplitType.AddItem("6 months")
        cbxSplitType.AddItem("7 months")
        cbxSplitType.AddItem("8 months")
        cbxSplitType.AddItem("9 months")
        cbxSplitType.AddItem("10 months")
        cbxSplitType.AddItem("11 months")
        cbxSplitType.AddItem("12 months")

        If m_AnnualBatchID.HasValue Then
            Me.Text = "Edit Annual Calculation"
            m_AnnualRecord = Business.ChildAnnual.RetreiveByID(m_AnnualBatchID.Value)
            DisplayRecord()
        Else
            Me.Text = "Create New Annual Calculation"
            m_AnnualRecord = New Business.ChildAnnual
            btnViewInvoice.Enabled = False
            btnViewBatch.Enabled = False
            btnDeactivate.Enabled = False
            cbxSplitType.SelectedIndex = 0
        End If

        Dim _C As Business.Child = Business.Child.RetreiveByID(m_ChildID)
        If _C IsNot Nothing Then

            txtName.Text = _C._Fullname
            m_ChildName = _C._Fullname

            If _C._Dob.HasValue Then
                m_ChildDOB = _C._Dob
                txtDOB.Text = Format(_C._Dob, "dd/MM/yyyy")
                lblAge.Text = ValueHandler.DateDifferenceAsText(_C._Dob.Value, Today, True, True, False)
            Else
                lblAge.Hide()
            End If

        End If

    End Sub

    Private Sub frmChildAnnual_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        If m_IsNew Then
            MyControls.SetControls(Care.Shared.ControlHandler.Mode.Add, Me.Controls, Care.Shared.ControlHandler.DebugMode.None)
        Else
            MyControls.SetControls(Care.Shared.ControlHandler.Mode.Edit, Me.Controls, Care.Shared.ControlHandler.DebugMode.None)
        End If
    End Sub

    Private Sub DisplayRecord()

        With m_AnnualRecord

            cdtDateFrom.Value = ._DateFrom
            cdtDateTo.Value = ._DateTo
            cdtInvoiceDate.Value = ._DateInvoiced
            cdtInvoiceDue.Value = ._DateDue

            cbxSplitType.SelectedValue = ._SplitType

            txtAnnualTotal.Text = ValueHandler.MoneyAsText(._AnnualTotal)
            txtSplitTotal.Text = ValueHandler.MoneyAsText(._SplitTotal)

            btnViewInvoice.Tag = ._AnnualInvoice

            btnViewInvoice.Enabled = True
            btnViewBatch.Enabled = True

            If m_AnnualRecord._Active Then
                btnDeactivate.Text = "Deactivate"
            Else
                btnDeactivate.Text = "Reactivate"
            End If

        End With

    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click

        If m_Recalculated Then
            Me.DialogResult = DialogResult.OK
        Else
            Me.DialogResult = DialogResult.Cancel
        End If

        Me.Close()

    End Sub

    Private Sub btnRecalculate_Click(sender As Object, e As EventArgs) Handles btnRecalculate.Click

        If Not ValidateEntry() Then Exit Sub
        If cbxSplitType.SelectedIndex < 0 Then cbxSplitType.SelectedIndex = 0

        btnRecalculate.Enabled = False
        Session.CursorWaiting()

        RecalculateAverage()

        btnRecalculate.Enabled = True
        Session.CursorDefault()

    End Sub

    Private Function ValidateEntry() As Boolean

        If Not cdtDateFrom.Value.HasValue Then Return False
        If cdtDateFrom.Value = Date.MinValue Then Return False

        If Not cdtDateTo.Value.HasValue Then Return False
        If cdtDateTo.Value = Date.MinValue Then Return False

        If Not cdtInvoiceDate.Value.HasValue Then Return False
        If cdtInvoiceDate.Value = Date.MinValue Then Return False

        If Not cdtInvoiceDue.Value.HasValue Then Return False
        If cdtInvoiceDue.Value = Date.MinValue Then Return False

        If cdtDateFrom.Value > cdtDateTo.Value Then Return False

        Return True

    End Function

    Private Sub RecalculateAverage()

        If Not ValidateEntry() Then Exit Sub

        btnViewInvoice.Tag = Nothing

        If m_IsNew Then
            m_AnnualRecord._ChildId = m_ChildID
            m_AnnualBatchID = m_AnnualRecord._ID
        Else
            Business.ChildAnnual.DeleteAnnualData(m_AnnualBatchID.Value)
        End If

        Dim _BatchNo As Integer = Invoicing.LastBatchNo + 1
        Dim _InvoiceNo As Integer = Invoicing.LastInvoiceNo + 1

        If m_AnnualRecord.Calculate(m_AnnualBatchID.Value, _BatchNo, _InvoiceNo, cdtInvoiceDate.DateTime, cdtInvoiceDue.DateTime,
                                    cdtDateFrom.DateTime, cdtDateTo.DateTime, cbxSplitType.Text, CInt(txtSplitSegments.Text)) Then

            m_AnnualRecord.CreateBatchHeader(m_AnnualBatchID.Value, _BatchNo, "Annual Invoice", cdtInvoiceDate.Value.Value, cdtDateFrom.Value.Value, cdtDateTo.Value.Value, _InvoiceNo, _InvoiceNo)

            btnViewInvoice.Tag = m_AnnualRecord._AnnualInvoice
            txtSplitTotal.Text = ValueHandler.MoneyAsText(m_AnnualRecord._SplitTotal)
            txtAnnualTotal.Text = ValueHandler.MoneyAsText(m_AnnualRecord._AnnualTotal)

            btnViewInvoice.Enabled = True
            btnViewBatch.Enabled = True

            m_Recalculated = True

        End If

    End Sub

    Private Sub btnViewInvoice_Click(sender As Object, e As EventArgs) Handles btnViewInvoice.Click
        If btnViewInvoice.Tag Is Nothing Then Exit Sub
        Dim _InvoiceID As Guid? = New Guid(btnViewInvoice.Tag.ToString)
        If _InvoiceID.HasValue Then Invoicing.ViewInvoice(_InvoiceID.Value)
    End Sub

    Private Sub cbxSplitType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSplitType.SelectedIndexChanged
        If cbxSplitType.Text = "Monthly" Then txtSplitSegments.Text = "12"
        If cbxSplitType.Text = "1 month" Then txtSplitSegments.Text = "1"
        If cbxSplitType.Text = "2 months" Then txtSplitSegments.Text = "2"
        If cbxSplitType.Text = "3 months" Then txtSplitSegments.Text = "3"
        If cbxSplitType.Text = "4 months" Then txtSplitSegments.Text = "4"
        If cbxSplitType.Text = "5 months" Then txtSplitSegments.Text = "5"
        If cbxSplitType.Text = "6 months" Then txtSplitSegments.Text = "6"
        If cbxSplitType.Text = "7 months" Then txtSplitSegments.Text = "7"
        If cbxSplitType.Text = "8 months" Then txtSplitSegments.Text = "8"
        If cbxSplitType.Text = "9 months" Then txtSplitSegments.Text = "9"
        If cbxSplitType.Text = "10 months" Then txtSplitSegments.Text = "10"
        If cbxSplitType.Text = "11 months" Then txtSplitSegments.Text = "11"
        If cbxSplitType.Text = "12 months" Then txtSplitSegments.Text = "12"
        SetToDate()
    End Sub

    Private Sub btnViewBatch_Click(sender As Object, e As EventArgs) Handles btnViewBatch.Click

        If m_AnnualRecord._SplitBatch.HasValue Then

            Dim _frm As New frmInvoiceBatch(m_AnnualRecord._SplitBatch.Value, "Invoice Split Breakdown")
            _frm.LoadMaximised = False
            _frm.ShowDialog()

            _frm.Dispose()
            _frm = Nothing

        End If

    End Sub

    Private Sub cdtDateFrom_Validated(sender As Object, e As EventArgs) Handles cdtDateFrom.Validated
        SetToDate()
    End Sub

    Private Sub SetToDate()
        If cdtDateFrom.Text <> "" Then
            cdtDateTo.DateTime = DateAdd(DateInterval.Month, CDbl(txtSplitSegments.Text), cdtDateFrom.DateTime).AddDays(-1)
        End If
    End Sub

    Private Sub btnDeactivate_Click(sender As Object, e As EventArgs) Handles btnDeactivate.Click

        If CareMessage("Are you sure you want to " + btnDeactivate.Text + " this Annual Calculation?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Change Calculation Status") = DialogResult.Yes Then

            If btnDeactivate.Text = "Deactivate" Then
                m_AnnualRecord._Active = False
            Else
                m_AnnualRecord._Active = True
            End If

            m_AnnualRecord.Store()

            Me.DialogResult = DialogResult.OK
            Me.Close()

        End If

    End Sub
End Class
