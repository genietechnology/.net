﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCalendarMaint
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.grdDays = New Care.Controls.CareGrid()
        Me.btnRebuild = New Care.Controls.CareButton()
        Me.btnAmend = New Care.Controls.CareButton()
        Me.btnClose = New Care.Controls.CareButton()
        Me.GroupBox1 = New Care.Controls.CareFrame()
        Me.btnRefresh = New Care.Controls.CareButton()
        Me.txtYear = New Care.Controls.CareTextBox()
        Me.Label1 = New Care.Controls.CareLabel()
        Me.gbxAmend = New Care.Controls.CareFrame()
        Me.btnCancel = New Care.Controls.CareButton()
        Me.btnOK = New Care.Controls.CareButton()
        Me.CareLabel4 = New Care.Controls.CareLabel()
        Me.CareLabel2 = New Care.Controls.CareLabel()
        Me.chkNoCharge = New Care.Controls.CareCheckBox()
        Me.txtHoliday = New Care.Controls.CareTextBox()
        Me.txtDayOfWeek = New Care.Controls.CareTextBox()
        Me.txtDate = New Care.Controls.CareTextBox()
        Me.CareLabel1 = New Care.Controls.CareLabel()
        Me.CareLabel6 = New Care.Controls.CareLabel()
        Me.chkClosed = New Care.Controls.CareCheckBox()
        Me.txtHolidayName = New Care.Controls.CareTextBox()
        Me.CareLabel3 = New Care.Controls.CareLabel()
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxAmend, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxAmend.SuspendLayout()
        CType(Me.chkNoCharge.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtHoliday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDayOfWeek.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkClosed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtHolidayName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'grdDays
        '
        Me.grdDays.AllowBuildColumns = True
        Me.grdDays.AllowHorizontalScroll = False
        Me.grdDays.AllowMultiSelect = False
        Me.grdDays.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdDays.Appearance.Options.UseFont = True
        Me.grdDays.AutoSizeByData = True
        Me.grdDays.HideFirstColumn = False
        Me.grdDays.Location = New System.Drawing.Point(12, 60)
        Me.grdDays.Name = "grdDays"
        Me.grdDays.QueryID = Nothing
        Me.grdDays.SearchAsYouType = True
        Me.grdDays.ShowAutoFilterRow = False
        Me.grdDays.ShowFindPanel = False
        Me.grdDays.ShowGroupByBox = False
        Me.grdDays.ShowNavigator = False
        Me.grdDays.Size = New System.Drawing.Size(650, 425)
        Me.grdDays.TabIndex = 1
        '
        'btnRebuild
        '
        Me.btnRebuild.Location = New System.Drawing.Point(12, 494)
        Me.btnRebuild.Name = "btnRebuild"
        Me.btnRebuild.Size = New System.Drawing.Size(75, 23)
        Me.btnRebuild.TabIndex = 2
        Me.btnRebuild.Text = "Rebuild Year"
        '
        'btnAmend
        '
        Me.btnAmend.Location = New System.Drawing.Point(93, 494)
        Me.btnAmend.Name = "btnAmend"
        Me.btnAmend.Size = New System.Drawing.Size(75, 23)
        Me.btnAmend.TabIndex = 3
        Me.btnAmend.Text = "Amend"
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(587, 494)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Text = "Close"
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.btnRefresh)
        Me.GroupBox1.Controls.Add(Me.txtYear)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 9)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.ShowCaption = False
        Me.GroupBox1.Size = New System.Drawing.Size(650, 42)
        Me.GroupBox1.TabIndex = 0
        '
        'btnRefresh
        '
        Me.btnRefresh.Location = New System.Drawing.Point(138, 11)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(75, 23)
        Me.btnRefresh.TabIndex = 2
        Me.btnRefresh.Text = "Refresh"
        '
        'txtYear
        '
        Me.txtYear.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtYear.CharacterCasing = CharacterCasing.Normal
        Me.txtYear.EnterMoveNextControl = True
        Me.txtYear.Location = New System.Drawing.Point(57, 12)
        Me.txtYear.MaxLength = 4
        Me.txtYear.Name = "txtYear"
        Me.txtYear.NumericAllowNegatives = True
        Me.txtYear.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtYear.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtYear.Properties.AccessibleName = "Name"
        Me.txtYear.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtYear.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtYear.Properties.Appearance.Options.UseFont = True
        Me.txtYear.Properties.Appearance.Options.UseTextOptions = True
        Me.txtYear.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtYear.Properties.Mask.EditMask = "##########"
        Me.txtYear.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtYear.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtYear.Properties.MaxLength = 4
        Me.txtYear.ReadOnly = False
        Me.txtYear.Size = New System.Drawing.Size(75, 20)
        Me.txtYear.TabIndex = 1
        Me.txtYear.Tag = ""
        Me.txtYear.TextAlign = HorizontalAlignment.Left
        Me.txtYear.ToolTipText = ""
        '
        'Label1
        '
        Me.Label1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label1.Location = New System.Drawing.Point(8, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(23, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Year"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbxAmend
        '
        Me.gbxAmend.Controls.Add(Me.btnCancel)
        Me.gbxAmend.Controls.Add(Me.btnOK)
        Me.gbxAmend.Controls.Add(Me.CareLabel4)
        Me.gbxAmend.Controls.Add(Me.CareLabel2)
        Me.gbxAmend.Controls.Add(Me.chkNoCharge)
        Me.gbxAmend.Controls.Add(Me.txtHoliday)
        Me.gbxAmend.Controls.Add(Me.txtDayOfWeek)
        Me.gbxAmend.Controls.Add(Me.txtDate)
        Me.gbxAmend.Controls.Add(Me.CareLabel1)
        Me.gbxAmend.Controls.Add(Me.CareLabel6)
        Me.gbxAmend.Controls.Add(Me.chkClosed)
        Me.gbxAmend.Controls.Add(Me.txtHolidayName)
        Me.gbxAmend.Controls.Add(Me.CareLabel3)
        Me.gbxAmend.Location = New System.Drawing.Point(167, 160)
        Me.gbxAmend.Name = "gbxAmend"
        Me.gbxAmend.Size = New System.Drawing.Size(341, 204)
        Me.gbxAmend.TabIndex = 5
        Me.gbxAmend.Text = "Amend Calendar Entry"
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(256, 172)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 12
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(175, 172)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 11
        Me.btnOK.Text = "OK"
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(12, 87)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(76, 15)
        Me.CareLabel4.TabIndex = 5
        Me.CareLabel4.Text = "Holiday Name"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(12, 143)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(83, 15)
        Me.CareLabel2.TabIndex = 9
        Me.CareLabel2.Text = "Not Chargeable"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkNoCharge
        '
        Me.chkNoCharge.EnterMoveNextControl = True
        Me.chkNoCharge.Location = New System.Drawing.Point(104, 141)
        Me.chkNoCharge.Name = "chkNoCharge"
        Me.chkNoCharge.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNoCharge.Properties.Appearance.Options.UseFont = True
        Me.chkNoCharge.Size = New System.Drawing.Size(20, 19)
        Me.chkNoCharge.TabIndex = 10
        Me.chkNoCharge.Tag = ""
        '
        'txtHoliday
        '
        Me.txtHoliday.CharacterCasing = CharacterCasing.Normal
        Me.txtHoliday.EnterMoveNextControl = True
        Me.txtHoliday.Location = New System.Drawing.Point(106, 56)
        Me.txtHoliday.MaxLength = 0
        Me.txtHoliday.Name = "txtHoliday"
        Me.txtHoliday.NumericAllowNegatives = False
        Me.txtHoliday.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtHoliday.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtHoliday.Properties.AccessibleDescription = ""
        Me.txtHoliday.Properties.AccessibleName = "Forename"
        Me.txtHoliday.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtHoliday.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtHoliday.Properties.Appearance.Options.UseFont = True
        Me.txtHoliday.Properties.Appearance.Options.UseTextOptions = True
        Me.txtHoliday.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtHoliday.Properties.ReadOnly = True
        Me.txtHoliday.ReadOnly = True
        Me.txtHoliday.Size = New System.Drawing.Size(91, 20)
        Me.txtHoliday.TabIndex = 4
        Me.txtHoliday.Tag = ""
        Me.txtHoliday.TextAlign = HorizontalAlignment.Left
        Me.txtHoliday.ToolTipText = ""
        '
        'txtDayOfWeek
        '
        Me.txtDayOfWeek.CharacterCasing = CharacterCasing.Normal
        Me.txtDayOfWeek.EnterMoveNextControl = True
        Me.txtDayOfWeek.Location = New System.Drawing.Point(203, 28)
        Me.txtDayOfWeek.MaxLength = 0
        Me.txtDayOfWeek.Name = "txtDayOfWeek"
        Me.txtDayOfWeek.NumericAllowNegatives = False
        Me.txtDayOfWeek.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtDayOfWeek.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDayOfWeek.Properties.AccessibleDescription = ""
        Me.txtDayOfWeek.Properties.AccessibleName = "Forename"
        Me.txtDayOfWeek.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtDayOfWeek.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtDayOfWeek.Properties.Appearance.Options.UseFont = True
        Me.txtDayOfWeek.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDayOfWeek.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtDayOfWeek.Properties.ReadOnly = True
        Me.txtDayOfWeek.ReadOnly = True
        Me.txtDayOfWeek.Size = New System.Drawing.Size(128, 20)
        Me.txtDayOfWeek.TabIndex = 2
        Me.txtDayOfWeek.Tag = ""
        Me.txtDayOfWeek.TextAlign = HorizontalAlignment.Left
        Me.txtDayOfWeek.ToolTipText = ""
        '
        'txtDate
        '
        Me.txtDate.CharacterCasing = CharacterCasing.Normal
        Me.txtDate.EnterMoveNextControl = True
        Me.txtDate.Location = New System.Drawing.Point(106, 28)
        Me.txtDate.MaxLength = 0
        Me.txtDate.Name = "txtDate"
        Me.txtDate.NumericAllowNegatives = False
        Me.txtDate.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtDate.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtDate.Properties.AccessibleDescription = ""
        Me.txtDate.Properties.AccessibleName = "Forename"
        Me.txtDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtDate.Properties.Appearance.Options.UseFont = True
        Me.txtDate.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtDate.Properties.ReadOnly = True
        Me.txtDate.ReadOnly = True
        Me.txtDate.Size = New System.Drawing.Size(91, 20)
        Me.txtDate.TabIndex = 1
        Me.txtDate.Tag = ""
        Me.txtDate.TextAlign = HorizontalAlignment.Left
        Me.txtDate.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(12, 31)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(24, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Date"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(12, 115)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(36, 15)
        Me.CareLabel6.TabIndex = 7
        Me.CareLabel6.Text = "Closed"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkClosed
        '
        Me.chkClosed.EnterMoveNextControl = True
        Me.chkClosed.Location = New System.Drawing.Point(104, 113)
        Me.chkClosed.Name = "chkClosed"
        Me.chkClosed.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkClosed.Properties.Appearance.Options.UseFont = True
        Me.chkClosed.Size = New System.Drawing.Size(20, 19)
        Me.chkClosed.TabIndex = 8
        Me.chkClosed.Tag = ""
        '
        'txtHolidayName
        '
        Me.txtHolidayName.CharacterCasing = CharacterCasing.Normal
        Me.txtHolidayName.EnterMoveNextControl = True
        Me.txtHolidayName.Location = New System.Drawing.Point(106, 84)
        Me.txtHolidayName.MaxLength = 30
        Me.txtHolidayName.Name = "txtHolidayName"
        Me.txtHolidayName.NumericAllowNegatives = False
        Me.txtHolidayName.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtHolidayName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtHolidayName.Properties.AccessibleDescription = ""
        Me.txtHolidayName.Properties.AccessibleName = "Forename"
        Me.txtHolidayName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtHolidayName.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtHolidayName.Properties.Appearance.Options.UseFont = True
        Me.txtHolidayName.Properties.Appearance.Options.UseTextOptions = True
        Me.txtHolidayName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtHolidayName.Properties.MaxLength = 30
        Me.txtHolidayName.ReadOnly = False
        Me.txtHolidayName.Size = New System.Drawing.Size(225, 20)
        Me.txtHolidayName.TabIndex = 6
        Me.txtHolidayName.Tag = ""
        Me.txtHolidayName.TextAlign = HorizontalAlignment.Left
        Me.txtHolidayName.ToolTipText = ""
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(12, 59)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(70, 15)
        Me.CareLabel3.TabIndex = 3
        Me.CareLabel3.Text = "Bank Holiday"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmCalendarMaint
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(674, 525)
        Me.Controls.Add(Me.gbxAmend)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnAmend)
        Me.Controls.Add(Me.btnRebuild)
        Me.Controls.Add(Me.grdDays)
        Me.Name = "frmCalendarMaint"
        CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.txtYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxAmend, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxAmend.ResumeLayout(False)
        Me.gbxAmend.PerformLayout()
        CType(Me.chkNoCharge.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtHoliday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDayOfWeek.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkClosed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtHolidayName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grdDays As Care.Controls.CareGrid
    Friend WithEvents btnRebuild As Care.Controls.CareButton
    Friend WithEvents btnAmend As Care.Controls.CareButton
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents GroupBox1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnRefresh As Care.Controls.CareButton
    Friend WithEvents txtYear As Care.Controls.CareTextBox
    Friend WithEvents Label1 As Care.Controls.CareLabel
    Friend WithEvents gbxAmend As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtDate As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents chkClosed As Care.Controls.CareCheckBox
    Friend WithEvents txtHolidayName As Care.Controls.CareTextBox
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents btnOK As Care.Controls.CareButton
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents chkNoCharge As Care.Controls.CareCheckBox
    Friend WithEvents txtHoliday As Care.Controls.CareTextBox
    Friend WithEvents txtDayOfWeek As Care.Controls.CareTextBox
    Friend WithEvents btnCancel As Care.Controls.CareButton

End Class
