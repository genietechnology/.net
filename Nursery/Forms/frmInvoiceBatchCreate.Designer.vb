﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInvoiceBatchCreate
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
        Me.GroupControl1 = New Care.Controls.CareFrame()
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.cbxPeriod = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.txtNextInvoiceNo = New Care.Controls.CareTextBox(Me.components)
        Me.txtNextBatchNo = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel16 = New Care.Controls.CareLabel(Me.components)
        Me.cbxSite = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel14 = New Care.Controls.CareLabel(Me.components)
        Me.cbxLayout = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel15 = New Care.Controls.CareLabel(Me.components)
        Me.cbxRoom = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel7 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel6 = New Care.Controls.CareLabel(Me.components)
        Me.cbxClassification = New Care.Controls.CareComboBox(Me.components)
        Me.cbxFrequency = New Care.Controls.CareComboBox(Me.components)
        Me.cdtLast = New Care.Controls.CareDateTime(Me.components)
        Me.cdtFirst = New Care.Controls.CareDateTime(Me.components)
        Me.cdtInvoiceDate = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.btnCancel = New Care.Controls.CareButton(Me.components)
        Me.btnCreate = New Care.Controls.CareButton(Me.components)
        Me.GroupControl2 = New Care.Controls.CareFrame()
        Me.CareLabel20 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel11 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel10 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel9 = New Care.Controls.CareLabel(Me.components)
        Me.txtXCheckPerHour = New Care.Controls.CareTextBox(Me.components)
        Me.txtXCheckThresh = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel8 = New Care.Controls.CareLabel(Me.components)
        Me.cbxXCheck = New Care.Controls.CareComboBox(Me.components)
        Me.cdtXCheckTo = New Care.Controls.CareDateTime(Me.components)
        Me.chkXCheckAdditional = New Care.Controls.CareCheckBox(Me.components)
        Me.cdtXCheckFrom = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel12 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel13 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl3 = New Care.Controls.CareFrame()
        Me.cbxPaymentMethod = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel21 = New Care.Controls.CareLabel(Me.components)
        Me.cbxFundedType = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.CareLabel23 = New Care.Controls.CareLabel(Me.components)
        Me.cbxTags = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.CareLabel22 = New Care.Controls.CareLabel(Me.components)
        Me.cbxTariff = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.CareLabel18 = New Care.Controls.CareLabel(Me.components)
        Me.cbxAdditional = New Care.Controls.CareComboBox(Me.components)
        Me.GroupControl4 = New Care.Controls.CareFrame()
        Me.cbxGeneration = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel17 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl5 = New Care.Controls.CareFrame()
        Me.cbxDuplicate = New Care.Controls.CareComboBox(Me.components)
        Me.CareLabel19 = New Care.Controls.CareLabel(Me.components)
        Me.GroupControl6 = New Care.Controls.CareFrame()
        Me.GroupControl7 = New Care.Controls.CareFrame()
        Me.txtComments = New DevExpress.XtraEditors.MemoEdit()
        Me.GroupControl8 = New Care.Controls.CareFrame()
        Me.GroupControl9 = New Care.Controls.CareFrame()
        Me.cdtAddlTo = New Care.Controls.CareDateTime(Me.components)
        Me.cdtAddlFrom = New Care.Controls.CareDateTime(Me.components)
        Me.CareLabel25 = New Care.Controls.CareLabel(Me.components)
        Me.CareLabel26 = New Care.Controls.CareLabel(Me.components)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.cbxPeriod.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNextInvoiceNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNextBatchNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxLayout.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxRoom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxClassification.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxFrequency.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtLast.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtLast.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtFirst.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtFirst.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtInvoiceDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtInvoiceDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.txtXCheckPerHour.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtXCheckThresh.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxXCheck.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtXCheckTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtXCheckTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkXCheckAdditional.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtXCheckFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtXCheckFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.cbxPaymentMethod.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxFundedType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxTags.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxTariff.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxAdditional.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.cbxGeneration.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.cbxDuplicate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl6.SuspendLayout()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl7.SuspendLayout()
        CType(Me.txtComments.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl8.SuspendLayout()
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl9.SuspendLayout()
        CType(Me.cdtAddlTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtAddlTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtAddlFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtAddlFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl1
        '
        Me.GroupControl1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl1.Appearance.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.CareLabel4)
        Me.GroupControl1.Controls.Add(Me.cbxPeriod)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(366, 57)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Batch Type"
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(11, 31)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(59, 15)
        Me.CareLabel4.TabIndex = 0
        Me.CareLabel4.Text = "Batch Type"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxPeriod
        '
        Me.cbxPeriod.AllowBlank = False
        Me.cbxPeriod.DataSource = Nothing
        Me.cbxPeriod.DisplayMember = Nothing
        Me.cbxPeriod.EnterMoveNextControl = True
        Me.cbxPeriod.Location = New System.Drawing.Point(147, 28)
        Me.cbxPeriod.Name = "cbxPeriod"
        Me.cbxPeriod.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxPeriod.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxPeriod.Properties.Appearance.Options.UseFont = True
        Me.cbxPeriod.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxPeriod.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxPeriod.SelectedValue = Nothing
        Me.cbxPeriod.Size = New System.Drawing.Size(206, 22)
        Me.cbxPeriod.TabIndex = 1
        Me.cbxPeriod.Tag = "A"
        Me.cbxPeriod.ValueMember = Nothing
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(206, 31)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(43, 15)
        Me.CareLabel5.TabIndex = 2
        Me.CareLabel5.Text = "Next Inv"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNextInvoiceNo
        '
        Me.txtNextInvoiceNo.CharacterCasing = CharacterCasing.Normal
        Me.txtNextInvoiceNo.EnterMoveNextControl = True
        Me.txtNextInvoiceNo.Location = New System.Drawing.Point(253, 28)
        Me.txtNextInvoiceNo.MaxLength = 0
        Me.txtNextInvoiceNo.Name = "txtNextInvoiceNo"
        Me.txtNextInvoiceNo.NumericAllowNegatives = False
        Me.txtNextInvoiceNo.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtNextInvoiceNo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtNextInvoiceNo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtNextInvoiceNo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtNextInvoiceNo.Properties.Appearance.Options.UseFont = True
        Me.txtNextInvoiceNo.Properties.Appearance.Options.UseTextOptions = True
        Me.txtNextInvoiceNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtNextInvoiceNo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtNextInvoiceNo.Properties.ReadOnly = True
        Me.txtNextInvoiceNo.Size = New System.Drawing.Size(100, 22)
        Me.txtNextInvoiceNo.TabIndex = 3
        Me.txtNextInvoiceNo.TabStop = False
        Me.txtNextInvoiceNo.TextAlign = HorizontalAlignment.Left
        Me.txtNextInvoiceNo.ToolTipText = ""
        '
        'txtNextBatchNo
        '
        Me.txtNextBatchNo.CharacterCasing = CharacterCasing.Normal
        Me.txtNextBatchNo.EnterMoveNextControl = True
        Me.txtNextBatchNo.Location = New System.Drawing.Point(147, 28)
        Me.txtNextBatchNo.MaxLength = 0
        Me.txtNextBatchNo.Name = "txtNextBatchNo"
        Me.txtNextBatchNo.NumericAllowNegatives = False
        Me.txtNextBatchNo.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtNextBatchNo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtNextBatchNo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtNextBatchNo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtNextBatchNo.Properties.Appearance.Options.UseFont = True
        Me.txtNextBatchNo.Properties.Appearance.Options.UseTextOptions = True
        Me.txtNextBatchNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtNextBatchNo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtNextBatchNo.Properties.ReadOnly = True
        Me.txtNextBatchNo.Size = New System.Drawing.Size(48, 22)
        Me.txtNextBatchNo.TabIndex = 1
        Me.txtNextBatchNo.TabStop = False
        Me.txtNextBatchNo.TextAlign = HorizontalAlignment.Left
        Me.txtNextBatchNo.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(11, 31)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(104, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Next Batch Number"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel16
        '
        Me.CareLabel16.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel16.Location = New System.Drawing.Point(11, 31)
        Me.CareLabel16.Name = "CareLabel16"
        Me.CareLabel16.Size = New System.Drawing.Size(19, 15)
        Me.CareLabel16.TabIndex = 0
        Me.CareLabel16.Text = "Site"
        Me.CareLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(147, 28)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(206, 22)
        Me.cbxSite.TabIndex = 1
        Me.cbxSite.Tag = "A"
        Me.cbxSite.ValueMember = Nothing
        '
        'CareLabel14
        '
        Me.CareLabel14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel14.Location = New System.Drawing.Point(11, 33)
        Me.CareLabel14.Name = "CareLabel14"
        Me.CareLabel14.Size = New System.Drawing.Size(99, 15)
        Me.CareLabel14.TabIndex = 0
        Me.CareLabel14.Text = "Invoice Generation"
        Me.CareLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxLayout
        '
        Me.cbxLayout.AllowBlank = False
        Me.cbxLayout.DataSource = Nothing
        Me.cbxLayout.DisplayMember = Nothing
        Me.cbxLayout.EnterMoveNextControl = True
        Me.cbxLayout.Location = New System.Drawing.Point(147, 58)
        Me.cbxLayout.Name = "cbxLayout"
        Me.cbxLayout.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxLayout.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxLayout.Properties.Appearance.Options.UseFont = True
        Me.cbxLayout.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxLayout.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxLayout.SelectedValue = Nothing
        Me.cbxLayout.Size = New System.Drawing.Size(206, 22)
        Me.cbxLayout.TabIndex = 3
        Me.cbxLayout.Tag = "AE"
        Me.cbxLayout.ValueMember = Nothing
        '
        'CareLabel15
        '
        Me.CareLabel15.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel15.Location = New System.Drawing.Point(11, 60)
        Me.CareLabel15.Name = "CareLabel15"
        Me.CareLabel15.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel15.TabIndex = 2
        Me.CareLabel15.Text = "Room"
        Me.CareLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxRoom
        '
        Me.cbxRoom.AllowBlank = False
        Me.cbxRoom.DataSource = Nothing
        Me.cbxRoom.DisplayMember = Nothing
        Me.cbxRoom.EnterMoveNextControl = True
        Me.cbxRoom.Location = New System.Drawing.Point(145, 57)
        Me.cbxRoom.Name = "cbxRoom"
        Me.cbxRoom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxRoom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxRoom.Properties.Appearance.Options.UseFont = True
        Me.cbxRoom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxRoom.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxRoom.SelectedValue = Nothing
        Me.cbxRoom.Size = New System.Drawing.Size(206, 22)
        Me.cbxRoom.TabIndex = 3
        Me.cbxRoom.Tag = "A"
        Me.cbxRoom.ValueMember = Nothing
        '
        'CareLabel7
        '
        Me.CareLabel7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel7.Location = New System.Drawing.Point(11, 88)
        Me.CareLabel7.Name = "CareLabel7"
        Me.CareLabel7.Size = New System.Drawing.Size(96, 15)
        Me.CareLabel7.TabIndex = 4
        Me.CareLabel7.Text = "Invoice Frequency"
        Me.CareLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel6
        '
        Me.CareLabel6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel6.Location = New System.Drawing.Point(11, 60)
        Me.CareLabel6.Name = "CareLabel6"
        Me.CareLabel6.Size = New System.Drawing.Size(70, 15)
        Me.CareLabel6.TabIndex = 2
        Me.CareLabel6.Text = "Classification"
        Me.CareLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxClassification
        '
        Me.cbxClassification.AllowBlank = False
        Me.cbxClassification.DataSource = Nothing
        Me.cbxClassification.DisplayMember = Nothing
        Me.cbxClassification.EnterMoveNextControl = True
        Me.cbxClassification.Location = New System.Drawing.Point(147, 56)
        Me.cbxClassification.Name = "cbxClassification"
        Me.cbxClassification.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxClassification.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxClassification.Properties.Appearance.Options.UseFont = True
        Me.cbxClassification.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxClassification.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxClassification.SelectedValue = Nothing
        Me.cbxClassification.Size = New System.Drawing.Size(206, 22)
        Me.cbxClassification.TabIndex = 3
        Me.cbxClassification.Tag = "A"
        Me.cbxClassification.ValueMember = Nothing
        '
        'cbxFrequency
        '
        Me.cbxFrequency.AllowBlank = False
        Me.cbxFrequency.DataSource = Nothing
        Me.cbxFrequency.DisplayMember = Nothing
        Me.cbxFrequency.EnterMoveNextControl = True
        Me.cbxFrequency.Location = New System.Drawing.Point(147, 84)
        Me.cbxFrequency.Name = "cbxFrequency"
        Me.cbxFrequency.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxFrequency.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxFrequency.Properties.Appearance.Options.UseFont = True
        Me.cbxFrequency.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxFrequency.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxFrequency.SelectedValue = Nothing
        Me.cbxFrequency.Size = New System.Drawing.Size(206, 22)
        Me.cbxFrequency.TabIndex = 5
        Me.cbxFrequency.Tag = "A"
        Me.cbxFrequency.ValueMember = Nothing
        '
        'cdtLast
        '
        Me.cdtLast.EditValue = Nothing
        Me.cdtLast.EnterMoveNextControl = True
        Me.cdtLast.Location = New System.Drawing.Point(253, 30)
        Me.cdtLast.Name = "cdtLast"
        Me.cdtLast.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtLast.Properties.Appearance.Options.UseFont = True
        Me.cdtLast.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtLast.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtLast.Size = New System.Drawing.Size(100, 22)
        Me.cdtLast.TabIndex = 2
        Me.cdtLast.Tag = ""
        Me.cdtLast.Value = Nothing
        '
        'cdtFirst
        '
        Me.cdtFirst.EditValue = Nothing
        Me.cdtFirst.EnterMoveNextControl = True
        Me.cdtFirst.Location = New System.Drawing.Point(147, 30)
        Me.cdtFirst.Name = "cdtFirst"
        Me.cdtFirst.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtFirst.Properties.Appearance.Options.UseFont = True
        Me.cdtFirst.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtFirst.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtFirst.Size = New System.Drawing.Size(100, 22)
        Me.cdtFirst.TabIndex = 1
        Me.cdtFirst.Tag = ""
        Me.cdtFirst.Value = Nothing
        '
        'cdtInvoiceDate
        '
        Me.cdtInvoiceDate.EditValue = Nothing
        Me.cdtInvoiceDate.EnterMoveNextControl = True
        Me.cdtInvoiceDate.Location = New System.Drawing.Point(147, 56)
        Me.cdtInvoiceDate.Name = "cdtInvoiceDate"
        Me.cdtInvoiceDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtInvoiceDate.Properties.Appearance.Options.UseFont = True
        Me.cdtInvoiceDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtInvoiceDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtInvoiceDate.Size = New System.Drawing.Size(100, 22)
        Me.cdtInvoiceDate.TabIndex = 4
        Me.cdtInvoiceDate.Tag = "A"
        Me.cdtInvoiceDate.Value = Nothing
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(11, 33)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(116, 15)
        Me.CareLabel3.TabIndex = 0
        Me.CareLabel3.Text = "First/Last Invoice Date"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(11, 59)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(65, 15)
        Me.CareLabel2.TabIndex = 3
        Me.CareLabel2.Text = "Invoice Date"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.CausesValidation = False
        Me.btnCancel.DialogResult = DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(665, 609)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 25)
        Me.btnCancel.TabIndex = 10
        Me.btnCancel.Tag = ""
        Me.btnCancel.Text = "Cancel"
        '
        'btnCreate
        '
        Me.btnCreate.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnCreate.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCreate.Appearance.Options.UseFont = True
        Me.btnCreate.CausesValidation = False
        Me.btnCreate.Location = New System.Drawing.Point(574, 609)
        Me.btnCreate.Name = "btnCreate"
        Me.btnCreate.Size = New System.Drawing.Size(85, 25)
        Me.btnCreate.TabIndex = 9
        Me.btnCreate.Tag = ""
        Me.btnCreate.Text = "Create Batch"
        '
        'GroupControl2
        '
        Me.GroupControl2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl2.Appearance.Options.UseFont = True
        Me.GroupControl2.Controls.Add(Me.CareLabel20)
        Me.GroupControl2.Controls.Add(Me.CareLabel11)
        Me.GroupControl2.Controls.Add(Me.CareLabel10)
        Me.GroupControl2.Controls.Add(Me.CareLabel9)
        Me.GroupControl2.Controls.Add(Me.txtXCheckPerHour)
        Me.GroupControl2.Controls.Add(Me.txtXCheckThresh)
        Me.GroupControl2.Controls.Add(Me.CareLabel8)
        Me.GroupControl2.Controls.Add(Me.cbxXCheck)
        Me.GroupControl2.Controls.Add(Me.cdtXCheckTo)
        Me.GroupControl2.Controls.Add(Me.chkXCheckAdditional)
        Me.GroupControl2.Controls.Add(Me.cdtXCheckFrom)
        Me.GroupControl2.Controls.Add(Me.CareLabel12)
        Me.GroupControl2.Controls.Add(Me.CareLabel13)
        Me.GroupControl2.Location = New System.Drawing.Point(384, 247)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(366, 250)
        Me.GroupControl2.TabIndex = 7
        Me.GroupControl2.Text = "Register Cross-Check"
        '
        'CareLabel20
        '
        Me.CareLabel20.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel20.Location = New System.Drawing.Point(11, 115)
        Me.CareLabel20.Name = "CareLabel20"
        Me.CareLabel20.Size = New System.Drawing.Size(91, 15)
        Me.CareLabel20.TabIndex = 8
        Me.CareLabel20.Text = "Cross Check Rate"
        Me.CareLabel20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel11
        '
        Me.CareLabel11.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel11.Location = New System.Drawing.Point(253, 116)
        Me.CareLabel11.Name = "CareLabel11"
        Me.CareLabel11.Size = New System.Drawing.Size(45, 15)
        Me.CareLabel11.TabIndex = 10
        Me.CareLabel11.Text = "per hour"
        Me.CareLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel10
        '
        Me.CareLabel10.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel10.Location = New System.Drawing.Point(253, 87)
        Me.CareLabel10.Name = "CareLabel10"
        Me.CareLabel10.Size = New System.Drawing.Size(34, 15)
        Me.CareLabel10.TabIndex = 7
        Me.CareLabel10.Text = "(mins)"
        Me.CareLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel9
        '
        Me.CareLabel9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel9.Location = New System.Drawing.Point(11, 152)
        Me.CareLabel9.Name = "CareLabel9"
        Me.CareLabel9.Size = New System.Drawing.Size(187, 15)
        Me.CareLabel9.TabIndex = 11
        Me.CareLabel9.Text = "Generate just the additional hours..."
        Me.CareLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtXCheckPerHour
        '
        Me.txtXCheckPerHour.CharacterCasing = CharacterCasing.Normal
        Me.txtXCheckPerHour.EnterMoveNextControl = True
        Me.txtXCheckPerHour.Location = New System.Drawing.Point(147, 112)
        Me.txtXCheckPerHour.MaxLength = 14
        Me.txtXCheckPerHour.Name = "txtXCheckPerHour"
        Me.txtXCheckPerHour.NumericAllowNegatives = False
        Me.txtXCheckPerHour.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.TwoDecimalPlaces
        Me.txtXCheckPerHour.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtXCheckPerHour.Properties.AccessibleName = "Family"
        Me.txtXCheckPerHour.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtXCheckPerHour.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtXCheckPerHour.Properties.Appearance.Options.UseFont = True
        Me.txtXCheckPerHour.Properties.Appearance.Options.UseTextOptions = True
        Me.txtXCheckPerHour.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtXCheckPerHour.Properties.Mask.EditMask = "###,###,##0.00;"
        Me.txtXCheckPerHour.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtXCheckPerHour.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtXCheckPerHour.Properties.MaxLength = 14
        Me.txtXCheckPerHour.Size = New System.Drawing.Size(100, 22)
        Me.txtXCheckPerHour.TabIndex = 9
        Me.txtXCheckPerHour.Tag = ""
        Me.txtXCheckPerHour.TextAlign = HorizontalAlignment.Left
        Me.txtXCheckPerHour.ToolTipText = ""
        '
        'txtXCheckThresh
        '
        Me.txtXCheckThresh.CharacterCasing = CharacterCasing.Normal
        Me.txtXCheckThresh.EnterMoveNextControl = True
        Me.txtXCheckThresh.Location = New System.Drawing.Point(147, 84)
        Me.txtXCheckThresh.MaxLength = 3
        Me.txtXCheckThresh.Name = "txtXCheckThresh"
        Me.txtXCheckThresh.NumericAllowNegatives = False
        Me.txtXCheckThresh.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.NumbersOnly
        Me.txtXCheckThresh.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtXCheckThresh.Properties.AccessibleName = "Family"
        Me.txtXCheckThresh.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtXCheckThresh.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtXCheckThresh.Properties.Appearance.Options.UseFont = True
        Me.txtXCheckThresh.Properties.Appearance.Options.UseTextOptions = True
        Me.txtXCheckThresh.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtXCheckThresh.Properties.Mask.EditMask = "##########;"
        Me.txtXCheckThresh.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtXCheckThresh.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtXCheckThresh.Properties.MaxLength = 3
        Me.txtXCheckThresh.Size = New System.Drawing.Size(100, 22)
        Me.txtXCheckThresh.TabIndex = 6
        Me.txtXCheckThresh.Tag = ""
        Me.txtXCheckThresh.TextAlign = HorizontalAlignment.Left
        Me.txtXCheckThresh.ToolTipText = ""
        '
        'CareLabel8
        '
        Me.CareLabel8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel8.Location = New System.Drawing.Point(11, 87)
        Me.CareLabel8.Name = "CareLabel8"
        Me.CareLabel8.Size = New System.Drawing.Size(121, 15)
        Me.CareLabel8.TabIndex = 5
        Me.CareLabel8.Text = "Cross Check Threshold"
        Me.CareLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxXCheck
        '
        Me.cbxXCheck.AllowBlank = False
        Me.cbxXCheck.DataSource = Nothing
        Me.cbxXCheck.DisplayMember = Nothing
        Me.cbxXCheck.EnterMoveNextControl = True
        Me.cbxXCheck.Location = New System.Drawing.Point(147, 28)
        Me.cbxXCheck.Name = "cbxXCheck"
        Me.cbxXCheck.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxXCheck.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxXCheck.Properties.Appearance.Options.UseFont = True
        Me.cbxXCheck.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxXCheck.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxXCheck.SelectedValue = Nothing
        Me.cbxXCheck.Size = New System.Drawing.Size(206, 22)
        Me.cbxXCheck.TabIndex = 1
        Me.cbxXCheck.Tag = "A"
        Me.cbxXCheck.ValueMember = Nothing
        '
        'cdtXCheckTo
        '
        Me.cdtXCheckTo.EditValue = Nothing
        Me.cdtXCheckTo.EnterMoveNextControl = True
        Me.cdtXCheckTo.Location = New System.Drawing.Point(253, 56)
        Me.cdtXCheckTo.Name = "cdtXCheckTo"
        Me.cdtXCheckTo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtXCheckTo.Properties.Appearance.Options.UseFont = True
        Me.cdtXCheckTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtXCheckTo.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtXCheckTo.Size = New System.Drawing.Size(100, 22)
        Me.cdtXCheckTo.TabIndex = 4
        Me.cdtXCheckTo.Tag = ""
        Me.cdtXCheckTo.Value = Nothing
        '
        'chkXCheckAdditional
        '
        Me.chkXCheckAdditional.EnterMoveNextControl = True
        Me.chkXCheckAdditional.Location = New System.Drawing.Point(204, 150)
        Me.chkXCheckAdditional.Name = "chkXCheckAdditional"
        Me.chkXCheckAdditional.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkXCheckAdditional.Properties.Appearance.Options.UseFont = True
        Me.chkXCheckAdditional.Size = New System.Drawing.Size(20, 19)
        Me.chkXCheckAdditional.TabIndex = 12
        Me.chkXCheckAdditional.Tag = ""
        '
        'cdtXCheckFrom
        '
        Me.cdtXCheckFrom.EditValue = Nothing
        Me.cdtXCheckFrom.EnterMoveNextControl = True
        Me.cdtXCheckFrom.Location = New System.Drawing.Point(147, 56)
        Me.cdtXCheckFrom.Name = "cdtXCheckFrom"
        Me.cdtXCheckFrom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtXCheckFrom.Properties.Appearance.Options.UseFont = True
        Me.cdtXCheckFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtXCheckFrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtXCheckFrom.Size = New System.Drawing.Size(100, 22)
        Me.cdtXCheckFrom.TabIndex = 3
        Me.cdtXCheckFrom.Tag = ""
        Me.cdtXCheckFrom.Value = Nothing
        '
        'CareLabel12
        '
        Me.CareLabel12.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel12.Location = New System.Drawing.Point(11, 31)
        Me.CareLabel12.Name = "CareLabel12"
        Me.CareLabel12.Size = New System.Drawing.Size(110, 15)
        Me.CareLabel12.TabIndex = 0
        Me.CareLabel12.Text = "Register Cross Check"
        Me.CareLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel13
        '
        Me.CareLabel13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel13.Location = New System.Drawing.Point(11, 59)
        Me.CareLabel13.Name = "CareLabel13"
        Me.CareLabel13.Size = New System.Drawing.Size(70, 15)
        Me.CareLabel13.TabIndex = 2
        Me.CareLabel13.Text = "Check Period"
        Me.CareLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl3
        '
        Me.GroupControl3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl3.Appearance.Options.UseFont = True
        Me.GroupControl3.Controls.Add(Me.CareLabel16)
        Me.GroupControl3.Controls.Add(Me.cbxClassification)
        Me.GroupControl3.Controls.Add(Me.cbxSite)
        Me.GroupControl3.Controls.Add(Me.cbxFrequency)
        Me.GroupControl3.Controls.Add(Me.CareLabel6)
        Me.GroupControl3.Controls.Add(Me.CareLabel7)
        Me.GroupControl3.Location = New System.Drawing.Point(12, 75)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(366, 117)
        Me.GroupControl3.TabIndex = 1
        Me.GroupControl3.Text = "Standard Filters"
        '
        'cbxPaymentMethod
        '
        Me.cbxPaymentMethod.AllowBlank = False
        Me.cbxPaymentMethod.DataSource = Nothing
        Me.cbxPaymentMethod.DisplayMember = Nothing
        Me.cbxPaymentMethod.EnterMoveNextControl = True
        Me.cbxPaymentMethod.Location = New System.Drawing.Point(145, 29)
        Me.cbxPaymentMethod.Name = "cbxPaymentMethod"
        Me.cbxPaymentMethod.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxPaymentMethod.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxPaymentMethod.Properties.Appearance.Options.UseFont = True
        Me.cbxPaymentMethod.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxPaymentMethod.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxPaymentMethod.SelectedValue = Nothing
        Me.cbxPaymentMethod.Size = New System.Drawing.Size(206, 22)
        Me.cbxPaymentMethod.TabIndex = 1
        Me.cbxPaymentMethod.Tag = "A"
        Me.cbxPaymentMethod.ValueMember = Nothing
        '
        'CareLabel21
        '
        Me.CareLabel21.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel21.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel21.Location = New System.Drawing.Point(11, 32)
        Me.CareLabel21.Name = "CareLabel21"
        Me.CareLabel21.Size = New System.Drawing.Size(92, 15)
        Me.CareLabel21.TabIndex = 0
        Me.CareLabel21.Text = "Payment Method"
        Me.CareLabel21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxFundedType
        '
        Me.cbxFundedType.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxFundedType.Location = New System.Drawing.Point(145, 111)
        Me.cbxFundedType.Name = "cbxFundedType"
        Me.cbxFundedType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxFundedType.Size = New System.Drawing.Size(206, 20)
        Me.cbxFundedType.TabIndex = 7
        '
        'CareLabel23
        '
        Me.CareLabel23.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel23.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel23.Location = New System.Drawing.Point(11, 113)
        Me.CareLabel23.Name = "CareLabel23"
        Me.CareLabel23.Size = New System.Drawing.Size(73, 15)
        Me.CareLabel23.TabIndex = 6
        Me.CareLabel23.Text = "Funding Type"
        Me.CareLabel23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxTags
        '
        Me.cbxTags.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxTags.Location = New System.Drawing.Point(145, 137)
        Me.cbxTags.Name = "cbxTags"
        Me.cbxTags.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxTags.Size = New System.Drawing.Size(206, 20)
        Me.cbxTags.TabIndex = 9
        '
        'CareLabel22
        '
        Me.CareLabel22.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel22.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel22.Location = New System.Drawing.Point(11, 139)
        Me.CareLabel22.Name = "CareLabel22"
        Me.CareLabel22.Size = New System.Drawing.Size(25, 15)
        Me.CareLabel22.TabIndex = 8
        Me.CareLabel22.Text = "Tags"
        Me.CareLabel22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxTariff
        '
        Me.cbxTariff.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cbxTariff.Location = New System.Drawing.Point(145, 85)
        Me.cbxTariff.Name = "cbxTariff"
        Me.cbxTariff.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxTariff.Size = New System.Drawing.Size(206, 20)
        Me.cbxTariff.TabIndex = 5
        '
        'CareLabel18
        '
        Me.CareLabel18.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel18.Location = New System.Drawing.Point(11, 87)
        Me.CareLabel18.Name = "CareLabel18"
        Me.CareLabel18.Size = New System.Drawing.Size(28, 15)
        Me.CareLabel18.TabIndex = 4
        Me.CareLabel18.Text = "Tariff"
        Me.CareLabel18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxAdditional
        '
        Me.cbxAdditional.AllowBlank = False
        Me.cbxAdditional.DataSource = Nothing
        Me.cbxAdditional.DisplayMember = Nothing
        Me.cbxAdditional.EnterMoveNextControl = True
        Me.cbxAdditional.Location = New System.Drawing.Point(147, 30)
        Me.cbxAdditional.Name = "cbxAdditional"
        Me.cbxAdditional.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxAdditional.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxAdditional.Properties.Appearance.Options.UseFont = True
        Me.cbxAdditional.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxAdditional.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxAdditional.SelectedValue = Nothing
        Me.cbxAdditional.Size = New System.Drawing.Size(206, 22)
        Me.cbxAdditional.TabIndex = 1
        Me.cbxAdditional.Tag = "A"
        Me.cbxAdditional.ValueMember = Nothing
        '
        'GroupControl4
        '
        Me.GroupControl4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl4.Appearance.Options.UseFont = True
        Me.GroupControl4.Controls.Add(Me.cbxGeneration)
        Me.GroupControl4.Controls.Add(Me.CareLabel17)
        Me.GroupControl4.Controls.Add(Me.cbxLayout)
        Me.GroupControl4.Controls.Add(Me.CareLabel14)
        Me.GroupControl4.Location = New System.Drawing.Point(12, 408)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(366, 89)
        Me.GroupControl4.TabIndex = 4
        Me.GroupControl4.Text = "Generation and Layout"
        '
        'cbxGeneration
        '
        Me.cbxGeneration.AllowBlank = False
        Me.cbxGeneration.DataSource = Nothing
        Me.cbxGeneration.DisplayMember = Nothing
        Me.cbxGeneration.EnterMoveNextControl = True
        Me.cbxGeneration.Location = New System.Drawing.Point(147, 30)
        Me.cbxGeneration.Name = "cbxGeneration"
        Me.cbxGeneration.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxGeneration.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxGeneration.Properties.Appearance.Options.UseFont = True
        Me.cbxGeneration.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxGeneration.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxGeneration.SelectedValue = Nothing
        Me.cbxGeneration.Size = New System.Drawing.Size(206, 22)
        Me.cbxGeneration.TabIndex = 1
        Me.cbxGeneration.Tag = "A"
        Me.cbxGeneration.ValueMember = Nothing
        '
        'CareLabel17
        '
        Me.CareLabel17.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel17.Location = New System.Drawing.Point(11, 61)
        Me.CareLabel17.Name = "CareLabel17"
        Me.CareLabel17.Size = New System.Drawing.Size(98, 15)
        Me.CareLabel17.TabIndex = 2
        Me.CareLabel17.Text = "Invoice Layout File"
        Me.CareLabel17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl5
        '
        Me.GroupControl5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl5.Appearance.Options.UseFont = True
        Me.GroupControl5.Controls.Add(Me.cbxDuplicate)
        Me.GroupControl5.Controls.Add(Me.CareLabel19)
        Me.GroupControl5.Controls.Add(Me.cdtLast)
        Me.GroupControl5.Controls.Add(Me.cdtFirst)
        Me.GroupControl5.Controls.Add(Me.CareLabel2)
        Me.GroupControl5.Controls.Add(Me.CareLabel3)
        Me.GroupControl5.Controls.Add(Me.cdtInvoiceDate)
        Me.GroupControl5.Location = New System.Drawing.Point(12, 198)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(366, 111)
        Me.GroupControl5.TabIndex = 2
        Me.GroupControl5.Text = "Invoice Dates"
        '
        'cbxDuplicate
        '
        Me.cbxDuplicate.AllowBlank = False
        Me.cbxDuplicate.DataSource = Nothing
        Me.cbxDuplicate.DisplayMember = Nothing
        Me.cbxDuplicate.EnterMoveNextControl = True
        Me.cbxDuplicate.Location = New System.Drawing.Point(147, 82)
        Me.cbxDuplicate.Name = "cbxDuplicate"
        Me.cbxDuplicate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxDuplicate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxDuplicate.Properties.Appearance.Options.UseFont = True
        Me.cbxDuplicate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxDuplicate.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxDuplicate.SelectedValue = Nothing
        Me.cbxDuplicate.Size = New System.Drawing.Size(206, 22)
        Me.cbxDuplicate.TabIndex = 6
        Me.cbxDuplicate.Tag = "A"
        Me.cbxDuplicate.ValueMember = Nothing
        '
        'CareLabel19
        '
        Me.CareLabel19.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel19.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel19.Location = New System.Drawing.Point(11, 85)
        Me.CareLabel19.Name = "CareLabel19"
        Me.CareLabel19.Size = New System.Drawing.Size(82, 15)
        Me.CareLabel19.TabIndex = 5
        Me.CareLabel19.Text = "Duplicate Dates"
        Me.CareLabel19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl6
        '
        Me.GroupControl6.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl6.Appearance.Options.UseFont = True
        Me.GroupControl6.Controls.Add(Me.CareLabel1)
        Me.GroupControl6.Controls.Add(Me.CareLabel5)
        Me.GroupControl6.Controls.Add(Me.txtNextBatchNo)
        Me.GroupControl6.Controls.Add(Me.txtNextInvoiceNo)
        Me.GroupControl6.Location = New System.Drawing.Point(384, 12)
        Me.GroupControl6.Name = "GroupControl6"
        Me.GroupControl6.Size = New System.Drawing.Size(366, 57)
        Me.GroupControl6.TabIndex = 5
        Me.GroupControl6.Text = "Batch && Invoice Numbers"
        '
        'GroupControl7
        '
        Me.GroupControl7.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl7.Appearance.Options.UseFont = True
        Me.GroupControl7.Controls.Add(Me.txtComments)
        Me.GroupControl7.Location = New System.Drawing.Point(12, 503)
        Me.GroupControl7.Name = "GroupControl7"
        Me.GroupControl7.Size = New System.Drawing.Size(738, 99)
        Me.GroupControl7.TabIndex = 8
        Me.GroupControl7.Text = "Comments"
        '
        'txtComments
        '
        Me.txtComments.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtComments.Location = New System.Drawing.Point(11, 26)
        Me.txtComments.Name = "txtComments"
        Me.txtComments.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtComments.Properties.Appearance.Options.UseFont = True
        Me.txtComments.Properties.Appearance.Options.UseTextOptions = True
        Me.txtComments.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.SpellChecker1.SetShowSpellCheckMenu(Me.txtComments, True)
        Me.txtComments.Size = New System.Drawing.Size(714, 66)
        Me.SpellChecker1.SetSpellCheckerOptions(Me.txtComments, OptionsSpelling1)
        Me.txtComments.TabIndex = 0
        Me.txtComments.Tag = "AE"
        '
        'GroupControl8
        '
        Me.GroupControl8.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl8.Appearance.Options.UseFont = True
        Me.GroupControl8.Controls.Add(Me.cbxPaymentMethod)
        Me.GroupControl8.Controls.Add(Me.CareLabel21)
        Me.GroupControl8.Controls.Add(Me.cbxFundedType)
        Me.GroupControl8.Controls.Add(Me.cbxRoom)
        Me.GroupControl8.Controls.Add(Me.CareLabel23)
        Me.GroupControl8.Controls.Add(Me.CareLabel15)
        Me.GroupControl8.Controls.Add(Me.CareLabel18)
        Me.GroupControl8.Controls.Add(Me.cbxTags)
        Me.GroupControl8.Controls.Add(Me.cbxTariff)
        Me.GroupControl8.Controls.Add(Me.CareLabel22)
        Me.GroupControl8.Location = New System.Drawing.Point(384, 75)
        Me.GroupControl8.Name = "GroupControl8"
        Me.GroupControl8.Size = New System.Drawing.Size(366, 166)
        Me.GroupControl8.TabIndex = 6
        Me.GroupControl8.Text = "Additional Filters (optional)"
        '
        'GroupControl9
        '
        Me.GroupControl9.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl9.Appearance.Options.UseFont = True
        Me.GroupControl9.Controls.Add(Me.cdtAddlTo)
        Me.GroupControl9.Controls.Add(Me.cbxAdditional)
        Me.GroupControl9.Controls.Add(Me.cdtAddlFrom)
        Me.GroupControl9.Controls.Add(Me.CareLabel25)
        Me.GroupControl9.Controls.Add(Me.CareLabel26)
        Me.GroupControl9.Location = New System.Drawing.Point(12, 315)
        Me.GroupControl9.Name = "GroupControl9"
        Me.GroupControl9.Size = New System.Drawing.Size(366, 87)
        Me.GroupControl9.TabIndex = 3
        Me.GroupControl9.Text = "Additional Sessions"
        '
        'cdtAddlTo
        '
        Me.cdtAddlTo.EditValue = Nothing
        Me.cdtAddlTo.EnterMoveNextControl = True
        Me.cdtAddlTo.Location = New System.Drawing.Point(253, 56)
        Me.cdtAddlTo.Name = "cdtAddlTo"
        Me.cdtAddlTo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtAddlTo.Properties.Appearance.Options.UseFont = True
        Me.cdtAddlTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtAddlTo.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtAddlTo.Size = New System.Drawing.Size(100, 22)
        Me.cdtAddlTo.TabIndex = 4
        Me.cdtAddlTo.Tag = ""
        Me.cdtAddlTo.Value = Nothing
        '
        'cdtAddlFrom
        '
        Me.cdtAddlFrom.EditValue = Nothing
        Me.cdtAddlFrom.EnterMoveNextControl = True
        Me.cdtAddlFrom.Location = New System.Drawing.Point(147, 56)
        Me.cdtAddlFrom.Name = "cdtAddlFrom"
        Me.cdtAddlFrom.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtAddlFrom.Properties.Appearance.Options.UseFont = True
        Me.cdtAddlFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtAddlFrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtAddlFrom.Size = New System.Drawing.Size(100, 22)
        Me.cdtAddlFrom.TabIndex = 3
        Me.cdtAddlFrom.Tag = ""
        Me.cdtAddlFrom.Value = Nothing
        '
        'CareLabel25
        '
        Me.CareLabel25.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel25.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel25.Location = New System.Drawing.Point(11, 59)
        Me.CareLabel25.Name = "CareLabel25"
        Me.CareLabel25.Size = New System.Drawing.Size(92, 15)
        Me.CareLabel25.TabIndex = 2
        Me.CareLabel25.Text = "Additional Period"
        Me.CareLabel25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CareLabel26
        '
        Me.CareLabel26.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel26.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel26.Location = New System.Drawing.Point(11, 33)
        Me.CareLabel26.Name = "CareLabel26"
        Me.CareLabel26.Size = New System.Drawing.Size(102, 15)
        Me.CareLabel26.TabIndex = 0
        Me.CareLabel26.Text = "Additional Sessions"
        Me.CareLabel26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmInvoiceBatchCreate
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(761, 641)
        Me.Controls.Add(Me.GroupControl9)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl8)
        Me.Controls.Add(Me.GroupControl7)
        Me.Controls.Add(Me.GroupControl6)
        Me.Controls.Add(Me.GroupControl5)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.btnCreate)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.GroupControl1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmInvoiceBatchCreate"
        Me.StartPosition = FormStartPosition.CenterScreen
        Me.Text = "Create New Invoice Batch"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.cbxPeriod.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNextInvoiceNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNextBatchNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxLayout.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxRoom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxClassification.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxFrequency.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtLast.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtLast.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtFirst.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtFirst.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtInvoiceDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtInvoiceDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.txtXCheckPerHour.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtXCheckThresh.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxXCheck.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtXCheckTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtXCheckTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkXCheckAdditional.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtXCheckFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtXCheckFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.cbxPaymentMethod.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxFundedType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxTags.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxTariff.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxAdditional.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.cbxGeneration.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.cbxDuplicate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl6.ResumeLayout(False)
        Me.GroupControl6.PerformLayout()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl7.ResumeLayout(False)
        CType(Me.txtComments.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl8.ResumeLayout(False)
        Me.GroupControl8.PerformLayout()
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl9.ResumeLayout(False)
        Me.GroupControl9.PerformLayout()
        CType(Me.cdtAddlTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtAddlTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtAddlFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtAddlFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents txtNextInvoiceNo As Care.Controls.CareTextBox
    Friend WithEvents cdtInvoiceDate As Care.Controls.CareDateTime
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents cbxPeriod As Care.Controls.CareComboBox
    Friend WithEvents txtNextBatchNo As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents btnCancel As Care.Controls.CareButton
    Friend WithEvents btnCreate As Care.Controls.CareButton
    Friend WithEvents cdtLast As Care.Controls.CareDateTime
    Friend WithEvents cdtFirst As Care.Controls.CareDateTime
    Friend WithEvents cbxClassification As Care.Controls.CareComboBox
    Friend WithEvents cbxFrequency As Care.Controls.CareComboBox
    Friend WithEvents CareLabel7 As Care.Controls.CareLabel
    Friend WithEvents CareLabel6 As Care.Controls.CareLabel
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel12 As Care.Controls.CareLabel
    Friend WithEvents CareLabel13 As Care.Controls.CareLabel
    Friend WithEvents cdtXCheckTo As Care.Controls.CareDateTime
    Friend WithEvents chkXCheckAdditional As Care.Controls.CareCheckBox
    Friend WithEvents cdtXCheckFrom As Care.Controls.CareDateTime
    Friend WithEvents cbxXCheck As Care.Controls.CareComboBox
    Friend WithEvents CareLabel8 As Care.Controls.CareLabel
    Friend WithEvents txtXCheckThresh As Care.Controls.CareTextBox
    Friend WithEvents CareLabel9 As Care.Controls.CareLabel
    Friend WithEvents txtXCheckPerHour As Care.Controls.CareTextBox
    Friend WithEvents CareLabel11 As Care.Controls.CareLabel
    Friend WithEvents CareLabel10 As Care.Controls.CareLabel
    Friend WithEvents CareLabel14 As Care.Controls.CareLabel
    Friend WithEvents cbxLayout As Care.Controls.CareComboBox
    Friend WithEvents CareLabel15 As Care.Controls.CareLabel
    Friend WithEvents cbxRoom As Care.Controls.CareComboBox
    Friend WithEvents CareLabel16 As Care.Controls.CareLabel
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cbxAdditional As Care.Controls.CareComboBox
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl6 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel18 As Care.Controls.CareLabel
    Friend WithEvents cbxDuplicate As Care.Controls.CareComboBox
    Friend WithEvents CareLabel19 As Care.Controls.CareLabel
    Friend WithEvents CareLabel20 As Care.Controls.CareLabel
    Friend WithEvents cbxTariff As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents cbxTags As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents CareLabel22 As Care.Controls.CareLabel
    Friend WithEvents CareLabel21 As Care.Controls.CareLabel
    Friend WithEvents cbxFundedType As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents CareLabel23 As Care.Controls.CareLabel
    Friend WithEvents cbxPaymentMethod As Care.Controls.CareComboBox
    Friend WithEvents GroupControl7 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtComments As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupControl8 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cbxGeneration As Care.Controls.CareComboBox
    Friend WithEvents CareLabel17 As Care.Controls.CareLabel
    Friend WithEvents GroupControl9 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cdtAddlTo As Care.Controls.CareDateTime
    Friend WithEvents cdtAddlFrom As Care.Controls.CareDateTime
    Friend WithEvents CareLabel25 As Care.Controls.CareLabel
    Friend WithEvents CareLabel26 As Care.Controls.CareLabel

End Class
