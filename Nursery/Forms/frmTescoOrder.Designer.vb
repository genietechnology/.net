﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTescoOrder
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnLogin = New Care.Controls.CareButton(Me.components)
        Me.cgResults = New Care.Controls.CareGrid()
        Me.btnSearch = New Care.Controls.CareButton(Me.components)
        Me.lblStatus = New DevExpress.XtraEditors.LabelControl()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'btnLogin
        '
        Me.btnLogin.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnLogin.Appearance.Options.UseFont = True
        Me.btnLogin.CausesValidation = False
        Me.btnLogin.Location = New System.Drawing.Point(12, 12)
        Me.btnLogin.Name = "btnLogin"
        Me.btnLogin.Size = New System.Drawing.Size(50, 23)
        Me.btnLogin.TabIndex = 4
        Me.btnLogin.Text = "Login"
        '
        'cgResults
        '
        Me.cgResults.AllowBuildColumns = True
        Me.cgResults.AllowHorizontalScroll = False
        Me.cgResults.AllowMultiSelect = False
        Me.cgResults.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgResults.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgResults.Appearance.Options.UseFont = True
        Me.cgResults.AutoSizeByData = True
        Me.cgResults.HideFirstColumn = False
        Me.cgResults.Location = New System.Drawing.Point(12, 41)
        Me.cgResults.Name = "cgResults"
        Me.cgResults.QueryID = Nothing
        Me.cgResults.SearchAsYouType = True
        Me.cgResults.ShowFindPanel = False
        Me.cgResults.ShowGroupByBox = True
        Me.cgResults.ShowNavigator = False
        Me.cgResults.Size = New System.Drawing.Size(636, 413)
        Me.cgResults.TabIndex = 5
        '
        'btnSearch
        '
        Me.btnSearch.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnSearch.Appearance.Options.UseFont = True
        Me.btnSearch.CausesValidation = False
        Me.btnSearch.Location = New System.Drawing.Point(68, 12)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(50, 23)
        Me.btnSearch.TabIndex = 6
        Me.btnSearch.Text = "Search"
        '
        'lblStatus
        '
        Me.lblStatus.Location = New System.Drawing.Point(124, 17)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(31, 13)
        Me.lblStatus.TabIndex = 7
        Me.lblStatus.Text = "Ready"
        '
        'frmTescoOrder
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(660, 466)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.cgResults)
        Me.Controls.Add(Me.btnLogin)
        Me.FormBorderStyle = FormBorderStyle.Sizable
        Me.Name = "frmTescoOrder"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnLogin As Care.Controls.CareButton
    Friend WithEvents cgResults As Care.Controls.CareGrid
    Friend WithEvents btnSearch As Care.Controls.CareButton
    Friend WithEvents lblStatus As DevExpress.XtraEditors.LabelControl

End Class
