﻿

Imports Care.Global
Imports Care.Data
Imports Care.Shared

Public Class frmReportMilk

    Private m_ReportData As New List(Of MilkReport)

    Private Sub frmReportMilk_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        txtCostPerUnit.Text = "0.50"
        txtMLPerChild.Text = "189"
        txtMLPerUnit.Text = "568"

    End Sub

    Private Sub Run()

        If cdtFrom.Text = "" Then Exit Sub
        If cdtTo.Text = "" Then Exit Sub

        If txtMLPerChild.Text = "" Then Exit Sub
        If txtMLPerUnit.Text = "" Then Exit Sub
        If txtCostPerUnit.Text = "" Then Exit Sub

        Dim _SQL As String = ""
        _SQL += "select booking_date, count(*) as 'headcount' from Bookings"
        _SQL += " where booking_date between " + ValueHandler.SQLDate(cdtFrom.Value.Value, True)
        _SQL += " and " + ValueHandler.SQLDate(cdtTo.Value.Value, True)
        _SQL += " and child_age <= 60"
        _SQL += " and hours >= 2"
        _SQL += " group by booking_date"
        _SQL += " order by booking_date"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            Session.SetupProgressBar("Processing", _DT.Rows.Count)
            m_ReportData.Clear()

            For Each _DR As DataRow In _DT.Rows

                Dim _BD As Date = ValueHandler.ConvertDate(_DR.Item("booking_date")).Value
                Dim _HeadCount As Integer = ValueHandler.ConvertInteger(_DR.Item("headcount"))

                Dim _R As New MilkReport
                With _R

                    .FromDate = cdtFrom.Value.Value
                    .ToDate = cdtTo.Value.Value

                    .ClaimDate = _BD
                    .ClaimYear = Year(_BD)
                    .ClaimMonth = MonthName(Month(_BD))
                    .YearMonth = .ClaimYear.ToString + Format(Month(_BD), "00")

                    .MLPerUnit = ValueHandler.ConvertInteger(txtMLPerUnit.Text)
                    .MLPerHead = ValueHandler.ConvertInteger(txtMLPerChild.Text)
                    .CostPerUnit = ValueHandler.ConvertDecimal(txtCostPerUnit.Text)

                    .HeadCount = _HeadCount
                    .UsedMLs = _HeadCount * .MLPerHead
                    .UsedUnits = .UsedMLs / .MLPerUnit
                    .UsedCost = .UsedUnits * .CostPerUnit

                End With

                m_ReportData.Add(_R)

                Session.StepProgressBar()

            Next

            Session.SetProgressMessage("Launching Report...", 1)

            ReportHandler.RunReport(Of MilkReport)("MilkReport.repx", m_ReportData)

        End If

    End Sub

    Public Class MilkReport
        Property FromDate As Date
        Property ToDate As Date
        Property YearMonth As String
        Property ClaimDate As Date
        Property ClaimYear As Integer
        Property ClaimMonth As String
        Property HeadCount As Integer
        Property MLPerHead As Integer
        Property MLPerUnit As Integer
        Property UsedMLs As Integer
        Property UsedUnits As Double
        Property UsedCost As Double
        Property CostPerUnit As Double
    End Class

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click
        If btnRun.ShiftDown Then
            ReportHandler.DesignReport(Of MilkReport)("MilkReport.repx", m_ReportData)
        Else
            Run()
        End If
    End Sub

End Class