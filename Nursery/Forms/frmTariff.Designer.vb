﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTariff
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl1 = New Care.Controls.CareFrame(Me.components)
        Me.chkUsage = New Care.Controls.CareCheckBox(Me.components)
        Me.chkShowColours = New Care.Controls.CareCheckBox(Me.components)
        Me.Label17 = New Care.Controls.CareLabel(Me.components)
        Me.cbxSite = New Care.Controls.CareComboBox(Me.components)
        Me.radArchived = New Care.Controls.CareRadioButton(Me.components)
        Me.radFuture = New Care.Controls.CareRadioButton(Me.components)
        Me.radCurrent = New Care.Controls.CareRadioButton(Me.components)
        Me.cgTariffs = New Care.Controls.CareGrid()
        Me.btnAdd = New Care.Controls.CareButton(Me.components)
        Me.btnEdit = New Care.Controls.CareButton(Me.components)
        Me.btnRemove = New Care.Controls.CareButton(Me.components)
        Me.btnUsage = New Care.Controls.CareButton(Me.components)
        Me.btnClose = New Care.Controls.CareButton(Me.components)
        Me.btnFuture = New Care.Controls.CareButton(Me.components)
        Me.CareButton1 = New Care.Controls.CareButton(Me.components)
        Me.btnGlobalPriceChange = New Care.Controls.CareButton(Me.components)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.chkUsage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkShowColours.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radArchived.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radFuture.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radCurrent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.chkUsage)
        Me.GroupControl1.Controls.Add(Me.chkShowColours)
        Me.GroupControl1.Controls.Add(Me.Label17)
        Me.GroupControl1.Controls.Add(Me.cbxSite)
        Me.GroupControl1.Controls.Add(Me.radArchived)
        Me.GroupControl1.Controls.Add(Me.radFuture)
        Me.GroupControl1.Controls.Add(Me.radCurrent)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.ShowCaption = False
        Me.GroupControl1.Size = New System.Drawing.Size(809, 40)
        Me.GroupControl1.TabIndex = 0
        '
        'chkUsage
        '
        Me.chkUsage.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.chkUsage.EnterMoveNextControl = True
        Me.chkUsage.Location = New System.Drawing.Point(600, 11)
        Me.chkUsage.Name = "chkUsage"
        Me.chkUsage.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkUsage.Properties.Appearance.Options.UseFont = True
        Me.chkUsage.Properties.Appearance.Options.UseTextOptions = True
        Me.chkUsage.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.chkUsage.Properties.Caption = "Show Usage"
        Me.chkUsage.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.chkUsage.Size = New System.Drawing.Size(94, 19)
        Me.chkUsage.TabIndex = 5
        Me.chkUsage.TabStop = False
        '
        'chkShowColours
        '
        Me.chkShowColours.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.chkShowColours.EditValue = True
        Me.chkShowColours.EnterMoveNextControl = True
        Me.chkShowColours.Location = New System.Drawing.Point(700, 11)
        Me.chkShowColours.Name = "chkShowColours"
        Me.chkShowColours.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkShowColours.Properties.Appearance.Options.UseFont = True
        Me.chkShowColours.Properties.Appearance.Options.UseTextOptions = True
        Me.chkShowColours.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.chkShowColours.Properties.Caption = "Show Colours"
        Me.chkShowColours.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.chkShowColours.Size = New System.Drawing.Size(101, 19)
        Me.chkShowColours.TabIndex = 6
        Me.chkShowColours.TabStop = False
        '
        'Label17
        '
        Me.Label17.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label17.Location = New System.Drawing.Point(10, 13)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(19, 15)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Site"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(38, 10)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AccessibleName = "Group"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(186, 22)
        Me.cbxSite.TabIndex = 1
        Me.cbxSite.Tag = ""
        Me.cbxSite.ValueMember = Nothing
        '
        'radArchived
        '
        Me.radArchived.Location = New System.Drawing.Point(433, 11)
        Me.radArchived.Name = "radArchived"
        Me.radArchived.Properties.AutoWidth = True
        Me.radArchived.Properties.Caption = "Archived Tariffs"
        Me.radArchived.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radArchived.Properties.RadioGroupIndex = 1
        Me.radArchived.Size = New System.Drawing.Size(98, 19)
        Me.radArchived.TabIndex = 4
        Me.radArchived.TabStop = False
        '
        'radFuture
        '
        Me.radFuture.Location = New System.Drawing.Point(339, 11)
        Me.radFuture.Name = "radFuture"
        Me.radFuture.Properties.AutoWidth = True
        Me.radFuture.Properties.Caption = "Future Tariffs"
        Me.radFuture.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radFuture.Properties.RadioGroupIndex = 1
        Me.radFuture.Size = New System.Drawing.Size(88, 19)
        Me.radFuture.TabIndex = 3
        Me.radFuture.TabStop = False
        '
        'radCurrent
        '
        Me.radCurrent.EditValue = True
        Me.radCurrent.Location = New System.Drawing.Point(240, 11)
        Me.radCurrent.Name = "radCurrent"
        Me.radCurrent.Properties.AutoWidth = True
        Me.radCurrent.Properties.Caption = "Current Tariffs"
        Me.radCurrent.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radCurrent.Properties.RadioGroupIndex = 1
        Me.radCurrent.Size = New System.Drawing.Size(93, 19)
        Me.radCurrent.TabIndex = 2
        '
        'cgTariffs
        '
        Me.cgTariffs.AllowBuildColumns = True
        Me.cgTariffs.AllowEdit = False
        Me.cgTariffs.AllowHorizontalScroll = False
        Me.cgTariffs.AllowMultiSelect = False
        Me.cgTariffs.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgTariffs.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgTariffs.Appearance.Options.UseFont = True
        Me.cgTariffs.AutoSizeByData = True
        Me.cgTariffs.DisableAutoSize = False
        Me.cgTariffs.DisableDataFormatting = False
        Me.cgTariffs.FocusedRowHandle = -2147483648
        Me.cgTariffs.HideFirstColumn = False
        Me.cgTariffs.Location = New System.Drawing.Point(12, 58)
        Me.cgTariffs.Name = "cgTariffs"
        Me.cgTariffs.PreviewColumn = ""
        Me.cgTariffs.QueryID = Nothing
        Me.cgTariffs.RowAutoHeight = False
        Me.cgTariffs.SearchAsYouType = True
        Me.cgTariffs.ShowAutoFilterRow = False
        Me.cgTariffs.ShowFindPanel = True
        Me.cgTariffs.ShowGroupByBox = False
        Me.cgTariffs.ShowLoadingPanel = False
        Me.cgTariffs.ShowNavigator = True
        Me.cgTariffs.Size = New System.Drawing.Size(809, 356)
        Me.cgTariffs.TabIndex = 1
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnAdd.Location = New System.Drawing.Point(12, 420)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(60, 23)
        Me.btnAdd.TabIndex = 2
        Me.btnAdd.Text = "Add"
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnEdit.Location = New System.Drawing.Point(78, 420)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(60, 23)
        Me.btnEdit.TabIndex = 3
        Me.btnEdit.Text = "Edit"
        '
        'btnRemove
        '
        Me.btnRemove.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnRemove.Location = New System.Drawing.Point(144, 420)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(60, 23)
        Me.btnRemove.TabIndex = 4
        Me.btnRemove.Text = "Delete"
        '
        'btnUsage
        '
        Me.btnUsage.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnUsage.Location = New System.Drawing.Point(210, 420)
        Me.btnUsage.Name = "btnUsage"
        Me.btnUsage.Size = New System.Drawing.Size(120, 23)
        Me.btnUsage.TabIndex = 5
        Me.btnUsage.Text = "View Usage"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnClose.Location = New System.Drawing.Point(725, 420)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(96, 23)
        Me.btnClose.TabIndex = 9
        Me.btnClose.Text = "Close"
        '
        'btnFuture
        '
        Me.btnFuture.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnFuture.Location = New System.Drawing.Point(462, 420)
        Me.btnFuture.Name = "btnFuture"
        Me.btnFuture.Size = New System.Drawing.Size(120, 23)
        Me.btnFuture.TabIndex = 7
        Me.btnFuture.Text = "Future Price Change"
        Me.btnFuture.Visible = False
        '
        'CareButton1
        '
        Me.CareButton1.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.CareButton1.Location = New System.Drawing.Point(588, 420)
        Me.CareButton1.Name = "CareButton1"
        Me.CareButton1.Size = New System.Drawing.Size(120, 23)
        Me.CareButton1.TabIndex = 8
        Me.CareButton1.Text = "Find && Replace"
        Me.CareButton1.Visible = False
        '
        'btnGlobalPriceChange
        '
        Me.btnGlobalPriceChange.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
        Me.btnGlobalPriceChange.Location = New System.Drawing.Point(336, 420)
        Me.btnGlobalPriceChange.Name = "btnGlobalPriceChange"
        Me.btnGlobalPriceChange.Size = New System.Drawing.Size(120, 23)
        Me.btnGlobalPriceChange.TabIndex = 6
        Me.btnGlobalPriceChange.Text = "Global Price Change"
        '
        'frmTariff
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(833, 449)
        Me.Controls.Add(Me.btnGlobalPriceChange)
        Me.Controls.Add(Me.CareButton1)
        Me.Controls.Add(Me.btnFuture)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnUsage)
        Me.Controls.Add(Me.btnRemove)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.cgTariffs)
        Me.Controls.Add(Me.GroupControl1)
        Me.FormBorderStyle = FormBorderStyle.Sizable
        Me.MinimumSize = New System.Drawing.Size(849, 488)
        Me.Name = "frmTariff"
        Me.WindowState = FormWindowState.Maximized
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.chkUsage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkShowColours.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radArchived.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radFuture.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radCurrent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents radArchived As Care.Controls.CareRadioButton
    Friend WithEvents radFuture As Care.Controls.CareRadioButton
    Friend WithEvents radCurrent As Care.Controls.CareRadioButton
    Friend WithEvents cgTariffs As Care.Controls.CareGrid
    Friend WithEvents btnAdd As Care.Controls.CareButton
    Friend WithEvents btnEdit As Care.Controls.CareButton
    Friend WithEvents btnRemove As Care.Controls.CareButton
    Friend WithEvents btnUsage As Care.Controls.CareButton
    Friend WithEvents btnClose As Care.Controls.CareButton
    Friend WithEvents btnFuture As Care.Controls.CareButton
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
    Friend WithEvents chkShowColours As Care.Controls.CareCheckBox
    Friend WithEvents chkUsage As Care.Controls.CareCheckBox
    Friend WithEvents Label17 As Care.Controls.CareLabel
    Friend WithEvents CareButton1 As Care.Controls.CareButton
    Friend WithEvents GroupControl1 As Care.Controls.CareFrame
    Friend WithEvents btnGlobalPriceChange As Care.Controls.CareButton
End Class
