﻿

Imports Care.Global
Imports Care.Shared

Public Class frmBoltOns

    Dim m_BoltOn As Business.TariffBoltOn

    Private Sub frmTariff_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        cbxCategory.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Bolt-On Categories"))
        cbxNLCode.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("NL Codes"))
        cbxNLTracking.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("NL Tracking"))

        cbxScope.PopulateStart()
        cbxScope.AddItem("Disabled")
        cbxScope.AddItem("Yes, All the time")
        cbxScope.AddItem("Yes, During Term Time Only")
        cbxScope.AddItem("Yes, During Term Holidays Only")
        cbxScope.PopulateEnd()

        cbxSummaryColumn.PopulateStart()
        cbxSummaryColumn.AddItem("1")
        cbxSummaryColumn.AddItem("2")
        cbxSummaryColumn.AddItem("3")
        cbxSummaryColumn.AddItem("4")
        cbxSummaryColumn.AddItem("5")
        cbxSummaryColumn.AddItem("6")
        cbxSummaryColumn.AddItem("7")
        cbxSummaryColumn.AddItem("8")
        cbxSummaryColumn.AddItem("9")
        cbxSummaryColumn.PopulateEnd()

        Dim _SQL As String = ""
        _SQL += "select id, category as 'Category', name as 'Name', recurring_scope_desc as 'Scope',"
        _SQL += " charge_closed as 'Chg. Closed', charge_holiday as 'Chg. Hols.', discount as 'Discounts',"
        _SQL += " nl_code as 'NL Code', nl_tracking as 'NL Tracking', summary_column as 'Column', price as 'Value'"
        _SQL += " from TariffBoltOns"
        _SQL += " order by category, name"

        Me.GridSQL = _SQL

        gbx.Hide()

    End Sub

    Protected Overrides Sub SetBindings()

        m_BoltOn = New Business.TariffBoltOn
        bs.DataSource = m_BoltOn

        cbxCategory.DataBindings.Add("Text", bs, "_Category")
        txtName.DataBindings.Add("Text", bs, "_Name")
        txtValue.DataBindings.Add("Text", bs, "_Price", True)
        chkDiscounts.DataBindings.Add("Checked", bs, "_Discount", True)
        chkChargeClosed.DataBindings.Add("Checked", bs, "_ChargeClosed")
        chkChargeHolidays.DataBindings.Add("Checked", bs, "_ChargeHoliday")
        cbxScope.DataBindings.Add("Text", bs, "_RecurringScopeDesc")
        cbxSummaryColumn.DataBindings.Add("Text", bs, "_SummaryColumn")
        cbxNLCode.DataBindings.Add("Text", bs, "_NLCode")
        cbxNLTracking.DataBindings.Add("Text", bs, "_NLTracking")

    End Sub

    Protected Overrides Sub BindToID(ID As System.Guid, IsNew As Boolean)

        If IsNew Then
            m_BoltOn = New Business.TariffBoltOn
        Else
            m_BoltOn = Business.TariffBoltOn.RetreiveByID(ID)
        End If

        bs.DataSource = m_BoltOn

    End Sub

    Protected Overrides Sub CommitUpdate()
        m_BoltOn._RecurringScope = ValueHandler.ConvertByte(cbxScope.SelectedIndex)
        m_BoltOn.Store()
    End Sub

    Protected Overrides Sub CommitDelete(ID As System.Guid)
        Business.TariffBoltOn.DeleteRecord(ID)
    End Sub

End Class
