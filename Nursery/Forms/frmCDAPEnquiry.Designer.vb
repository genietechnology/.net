﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCDAPEnquiry
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.cgSteps = New Care.Controls.CareGrid()
        Me.cgCDAP = New Care.Controls.CareGrid()
        Me.txtRed = New Care.Controls.CareTextBox(Me.components)
        Me.txtYellow = New Care.Controls.CareTextBox(Me.components)
        Me.txtBlue = New Care.Controls.CareTextBox(Me.components)
        Me.txtGreen = New Care.Controls.CareTextBox(Me.components)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBlue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.SplitContainerControl1.Location = New System.Drawing.Point(12, 12)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.cgSteps)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.cgCDAP)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(848, 217)
        Me.SplitContainerControl1.SplitterPosition = 212
        Me.SplitContainerControl1.TabIndex = 11
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'cgSteps
        '
        Me.cgSteps.AllowBuildColumns = True
        Me.cgSteps.AllowHorizontalScroll = False
        Me.cgSteps.AllowMultiSelect = False
        Me.cgSteps.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgSteps.Appearance.Options.UseFont = True
        Me.cgSteps.AutoSizeByData = True
        Me.cgSteps.Dock = DockStyle.Fill
        Me.cgSteps.HideFirstColumn = False
        Me.cgSteps.Location = New System.Drawing.Point(0, 0)
        Me.cgSteps.Name = "cgSteps"
        Me.cgSteps.QueryID = Nothing
        Me.cgSteps.SearchAsYouType = True
        Me.cgSteps.ShowFindPanel = False
        Me.cgSteps.ShowGroupByBox = False
        Me.cgSteps.ShowNavigator = False
        Me.cgSteps.Size = New System.Drawing.Size(212, 217)
        Me.cgSteps.TabIndex = 3
        '
        'cgCDAP
        '
        Me.cgCDAP.AllowBuildColumns = True
        Me.cgCDAP.AllowHorizontalScroll = False
        Me.cgCDAP.AllowMultiSelect = False
        Me.cgCDAP.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgCDAP.Appearance.Options.UseFont = True
        Me.cgCDAP.AutoSizeByData = True
        Me.cgCDAP.Dock = DockStyle.Fill
        Me.cgCDAP.HideFirstColumn = False
        Me.cgCDAP.Location = New System.Drawing.Point(0, 0)
        Me.cgCDAP.Name = "cgCDAP"
        Me.cgCDAP.QueryID = Nothing
        Me.cgCDAP.SearchAsYouType = True
        Me.cgCDAP.ShowFindPanel = False
        Me.cgCDAP.ShowGroupByBox = False
        Me.cgCDAP.ShowNavigator = False
        Me.cgCDAP.Size = New System.Drawing.Size(631, 217)
        Me.cgCDAP.TabIndex = 1
        '
        'txtRed
        '
        Me.txtRed.CharacterCasing = CharacterCasing.Normal
        Me.txtRed.EnterMoveNextControl = True
        Me.txtRed.Location = New System.Drawing.Point(0, 220)
        Me.txtRed.MaxLength = 0
        Me.txtRed.Name = "txtRed"
        Me.txtRed.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRed.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtRed.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRed.Properties.Appearance.Options.UseBackColor = True
        Me.txtRed.Properties.Appearance.Options.UseFont = True
        Me.txtRed.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRed.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRed.ReadOnly = False
        Me.txtRed.Size = New System.Drawing.Size(25, 20)
        Me.txtRed.TabIndex = 12
        Me.txtRed.TextAlign = HorizontalAlignment.Left
        Me.txtRed.ToolTipText = ""
        Me.txtRed.Visible = False
        '
        'txtYellow
        '
        Me.txtYellow.CharacterCasing = CharacterCasing.Normal
        Me.txtYellow.EnterMoveNextControl = True
        Me.txtYellow.Location = New System.Drawing.Point(91, 220)
        Me.txtYellow.MaxLength = 0
        Me.txtYellow.Name = "txtYellow"
        Me.txtYellow.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtYellow.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtYellow.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtYellow.Properties.Appearance.Options.UseBackColor = True
        Me.txtYellow.Properties.Appearance.Options.UseFont = True
        Me.txtYellow.Properties.Appearance.Options.UseTextOptions = True
        Me.txtYellow.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtYellow.ReadOnly = False
        Me.txtYellow.Size = New System.Drawing.Size(25, 20)
        Me.txtYellow.TabIndex = 15
        Me.txtYellow.TextAlign = HorizontalAlignment.Left
        Me.txtYellow.ToolTipText = ""
        Me.txtYellow.Visible = False
        '
        'txtBlue
        '
        Me.txtBlue.CharacterCasing = CharacterCasing.Normal
        Me.txtBlue.EnterMoveNextControl = True
        Me.txtBlue.Location = New System.Drawing.Point(60, 220)
        Me.txtBlue.MaxLength = 0
        Me.txtBlue.Name = "txtBlue"
        Me.txtBlue.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBlue.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBlue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtBlue.Properties.Appearance.Options.UseBackColor = True
        Me.txtBlue.Properties.Appearance.Options.UseFont = True
        Me.txtBlue.Properties.Appearance.Options.UseTextOptions = True
        Me.txtBlue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtBlue.ReadOnly = False
        Me.txtBlue.Size = New System.Drawing.Size(25, 20)
        Me.txtBlue.TabIndex = 14
        Me.txtBlue.TextAlign = HorizontalAlignment.Left
        Me.txtBlue.ToolTipText = ""
        Me.txtBlue.Visible = False
        '
        'txtGreen
        '
        Me.txtGreen.CharacterCasing = CharacterCasing.Normal
        Me.txtGreen.EnterMoveNextControl = True
        Me.txtGreen.Location = New System.Drawing.Point(29, 220)
        Me.txtGreen.MaxLength = 0
        Me.txtGreen.Name = "txtGreen"
        Me.txtGreen.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtGreen.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtGreen.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtGreen.Properties.Appearance.Options.UseBackColor = True
        Me.txtGreen.Properties.Appearance.Options.UseFont = True
        Me.txtGreen.Properties.Appearance.Options.UseTextOptions = True
        Me.txtGreen.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtGreen.ReadOnly = False
        Me.txtGreen.Size = New System.Drawing.Size(25, 20)
        Me.txtGreen.TabIndex = 13
        Me.txtGreen.TextAlign = HorizontalAlignment.Left
        Me.txtGreen.ToolTipText = ""
        Me.txtGreen.Visible = False
        '
        'frmCDAPEnquiry
        '
        Me.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(872, 241)
        Me.Controls.Add(Me.txtRed)
        Me.Controls.Add(Me.txtYellow)
        Me.Controls.Add(Me.txtBlue)
        Me.Controls.Add(Me.txtGreen)
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.FormBorderStyle = FormBorderStyle.Sizable
        Me.Name = "frmCDAPEnquiry"
        Me.Text = "CDAP Enquiry"
        Me.WindowState = FormWindowState.Maximized
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBlue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents cgSteps As Care.Controls.CareGrid
    Friend WithEvents cgCDAP As Care.Controls.CareGrid
    Friend WithEvents txtRed As Care.Controls.CareTextBox
    Friend WithEvents txtYellow As Care.Controls.CareTextBox
    Friend WithEvents txtBlue As Care.Controls.CareTextBox
    Friend WithEvents txtGreen As Care.Controls.CareTextBox

End Class
