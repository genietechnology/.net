﻿Imports System.Windows.Forms
Imports Care.Global
Imports Care.Shared
Imports Care.Data

Public Class frmFinancialsPost

    Private m_BatchID As Guid? = Nothing
    Private m_BatchType As EnumBatchType
    Private m_SiteID As Guid = Nothing

    Private m_PostingDocs As New List(Of PostingDoc)

    Private m_Processing As Boolean = False
    Private m_Eng As Care.Financials.Engine = Nothing
    Private m_Success As Integer = 0
    Private m_Warnings As Integer = 0
    Private m_Errors As Integer = 0

    Public Enum EnumBatchType
        Invoices
        Credits
    End Enum

    Public Sub New(ByVal BatchID As Guid, ByVal BatchType As EnumBatchType, ByVal SiteID As Guid)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_BatchID = BatchID
        m_BatchType = BatchType
        m_SiteID = SiteID

    End Sub

    Private Sub frmFinancialsPost_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        PopulateAndPost()
    End Sub

    Private Sub frmFinancialsPost_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Session.FinancialsIntegrated Then
            m_Eng = New Care.Financials.Engine(FinanceShared.ReturnFinancialsIntegration(m_SiteID))
        End If
        btnPrint.Enabled = False
        btnRetry.Enabled = False
    End Sub

    Private Sub frmFinancialsPost_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If e.CloseReason = CloseReason.UserClosing Then
            If m_Processing Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub PopulateAndPost()

        btnPrint.Enabled = False
        btnRetry.Enabled = False
        Session.CursorWaiting()

        m_Processing = True

        If m_BatchType = EnumBatchType.Invoices Then
            PopulateInvoices()
        Else
            PopulateCredits()
        End If

        cgDocs.Populate(m_PostingDocs)

        cgDocs.Columns("DocID").Visible = False
        cgDocs.Columns("DocRef").Caption = "Reference"
        cgDocs.Columns("DocSubject").Caption = "Subject"
        cgDocs.Columns("DocStatus").Caption = "Status"
        cgDocs.Columns("CanPost").Caption = "Can Post?"
        cgDocs.Columns("Status").Caption = "Status"
        cgDocs.Columns("ErrorText").Caption = "Error"
        cgDocs.Columns("Doc").Visible = False
        cgDocs.Columns("Ex").Caption = "Ex"

        cgDocs.Refresh()

        Post()

        m_Processing = False

        btnPrint.Enabled = True
        Session.CursorDefault()

        If m_Errors > 0 OrElse m_Warnings > 0 Then
            CareMessage("Batch Posting Failed. Please review the issues and try again.", MessageBoxIcon.Warning, "Post Batch")
        Else
            CareMessage("Batch Posted Successfully. Click OK to Exit.", MessageBoxIcon.Information, "Post Batch")
            Me.DialogResult = DialogResult.OK
            Me.Close()
        End If

    End Sub

    Private Sub PopulateCredits()

        Dim _SQL As String = ""
        _SQL += "select ID, credit_ref, child_name, credit_status from Credits"
        _SQL += " where batch_id = '" + m_BatchID.ToString + "'"
        _SQL += " and credit_status <> 'Posted'"
        _SQL += " order by credit_no"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then
            For Each _DR As DataRow In _DT.Rows
                Dim _PD As New PostingDoc(EnumBatchType.Credits, _DR.Item("ID").ToString, _DR.Item("credit_ref").ToString, _DR.Item("child_name").ToString, _DR.Item("credit_status").ToString)
                m_PostingDocs.Add(_PD)
            Next
        End If

        _DT.Dispose()
        _DT = Nothing

    End Sub

    Private Sub PopulateInvoices()

        Dim _SQL As String = ""
        _SQL += "select ID, invoice_ref, child_name, invoice_status from Invoices"
        _SQL += " where batch_id = '" + m_BatchID.ToString + "'"
        _SQL += " and invoice_status <> 'Posted'"
        _SQL += " order by invoice_no"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then
            For Each _DR As DataRow In _DT.Rows
                Dim _PD As New PostingDoc(EnumBatchType.Invoices, _DR.Item("ID").ToString, _DR.Item("invoice_ref").ToString, _DR.Item("child_name").ToString, _DR.Item("invoice_status").ToString)
                m_PostingDocs.Add(_PD)
            Next
        End If

        _DT.Dispose()
        _DT = Nothing

    End Sub

    Private Sub Post()

        m_Success = 0
        m_Warnings = 0
        m_Errors = 0

        For Each _PD As PostingDoc In m_PostingDocs

            If Session.FinancialsIntegrated Then

                If _PD.CanPost Then

                    _PD.Status = "Posting..."
                    cgDocs.UnderlyingGridControl.RefreshDataSource()
                    Application.DoEvents()

                    If Session.FinancialsIntegrated Then

                        Dim _Response As Care.Financials.Response = m_Eng.CreateDocument(_PD.Doc)
                        If _Response.HasErrors Then
                            _PD.Status = "Error"
                            _PD.ErrorText = _Response.ErrorText
                            _PD.Ex = _Response.Exception
                            m_Errors += 1
                        Else
                            _PD.Status = "Posted"
                            If m_BatchType = EnumBatchType.Invoices Then
                                Invoicing.UpdateInvoiceStatus(_PD.DocID.ToString, "Posted", _PD.Doc.ExternalID)
                            Else
                                Credits.UpdateCreditStatus(_PD.DocID.ToString, "Posted", _PD.Doc.ExternalID)
                            End If
                            m_Success += 1
                        End If

                        If Session.FinancialSystem.ToUpper = "XERO" Then Threading.Thread.Sleep(1100)
                        If Session.FinancialSystem.ToUpper = "SAGE" Then Threading.Thread.Sleep(500)

                    End If

                Else
                    _PD.ErrorText = _PD.Status
                    _PD.Status = "Skipped"
                    m_Warnings += 1
                End If

            Else
                'not linked to financials system, so we just mark the document as posted
                _PD.Status = "Posted"
                If m_BatchType = EnumBatchType.Invoices Then
                    Invoicing.UpdateInvoiceStatus(_PD.DocID.ToString, "Posted")
                Else
                    Credits.UpdateCreditStatus(_PD.DocID.ToString, "Posted")
                End If
                m_Success += 1
            End If

            cgDocs.UnderlyingGridControl.RefreshDataSource()
            cgDocs.AutoSizeColumns()

            txtSuccess.Text = m_Success.ToString
            txtWarnings.Text = m_Warnings.ToString
            txtErrors.Text = m_Errors.ToString
            txtRemaining.Text = (m_PostingDocs.Count - (m_Success + m_Warnings + m_Errors)).ToString

            Application.DoEvents()

        Next

        If m_Errors > 0 OrElse m_Warnings > 0 Then
            btnRetry.Enabled = True
            Application.DoEvents()
        Else
            UpdateHeaderAsPosted()
        End If

    End Sub

    Private Sub UpdateHeaderAsPosted()
        If m_BatchType = EnumBatchType.Invoices Then
            Dim _h As Business.InvoiceBatch = Business.InvoiceBatch.RetreiveByID(m_BatchID.Value)
            _h._BatchStatus = "Posted"
            _h.Store()
        Else
            Dim _h As Business.CreditBatch = Business.CreditBatch.RetreiveByID(m_BatchID.Value)
            _h._BatchStatus = "Posted"
            _h.Store()
        End If
    End Sub

    Private Sub cgDocs_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles cgDocs.RowStyle

        If e IsNot Nothing And e.RowHandle >= 0 Then

            Select Case cgDocs.GetRowCellValue(e.RowHandle, "Status").ToString

                Case "Skipped"
                    e.Appearance.BackColor = txtOrange.BackColor

                Case "Posting..."
                    e.Appearance.BackColor = txtYellow.BackColor

                Case "Posted"
                    e.Appearance.BackColor = txtGreen.BackColor

                Case "Error"
                    e.Appearance.BackColor = txtRed.BackColor

            End Select

        End If

    End Sub

    Private Sub Run()

        'Dim _Errs As Integer = 0
        'Dim _SQL As String = "select ID, invoice_status from Invoices where batch_id = '" + InvoiceBatchID.ToString + "' order by invoice_no"
        'Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        'If _DT IsNot Nothing Then

        '    Session.SetupProgressBar("Posting Batch...", _DT.Rows.Count)


        '    Dim _Batch As Business.InvoiceBatch = Business.InvoiceBatch.RetreiveByID(InvoiceBatchID)
        '    For Each _DR As DataRow In _DT.Rows

        '        Dim _ExtDocumentCreated As Boolean = False

        '        If Session.FinancialsIntegrated Then

        '            If _DR.Item("invoice_status").ToString = "Posted" Then
        '                'invoice has already been posted, we do not want to post again!
        '            Else

        '                Dim _ID As Guid = New Guid(_DR.Item("ID").ToString)
        '                Dim _Doc As Care.Financials.Document = Nothing

        '                If CreateFinancialsDocumentFromInvoiceID(_ID, _Doc) = 0 Then

        '                    Dim _Response As Care.Financials.Response = _Eng.CreateDocument(_Doc)

        '                    _ExtDocumentCreated = Not _Response.HasErrors

        '                    If _ExtDocumentCreated Then
        '                        _ExternalID = _Doc.ExternalID
        '                    Else
        '                        _Errs += 1
        '                        _ExternalID = Nothing
        '                    End If

        '                    If Session.FinancialSystem.ToUpper = "XERO" Then
        '                        Threading.Thread.Sleep(1100)
        '                    End If

        '                Else
        '                    'create document failed, we class this as an error
        '                    _Errs += 1
        '                End If

        '            End If

        '        Else
        '            'pretend the document was created as we are not integrated...
        '            _ExtDocumentCreated = True
        '        End If

        '        If _ExtDocumentCreated Then
        '            UpdateInvoiceStatus(_DR.Item("ID").ToString, "Posted", _ExternalID)
        '        End If

        '        Session.StepProgressBar()

        '    Next

        '    'disconnect from Sage
        '    If Session.FinancialsIntegrated Then
        '        _Eng.Disconnect()
        '    End If

        '    If _Errs = 0 Then
        '        _Batch._BatchStatus = "Posted"
        '        _Batch.Store()
        '    End If

        '    _Batch = Nothing

        '    Session.SetProgressMessage("Batch Post Complete.", 10)

        'End If

        '_DT.Dispose()
        '_DT = Nothing

        'If _Errs = 0 Then
        '    Return True
        'Else
        '    Return False
        'End If
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click

        btnPrint.Enabled = False
        Session.CursorWaiting()

        If btnPrint.ShiftDown Then
            ReportHandler.DesignReport(Of PostingDoc)("BatchPostAudit.repx", m_PostingDocs)
        Else
            ReportHandler.RunReport(Of PostingDoc)("BatchPostAudit.repx", m_PostingDocs)
        End If

        btnPrint.Enabled = True
        Session.CursorDefault()

    End Sub

#Region "Local Class"

    Private Class PostingDoc

        Public Sub New(ByVal DocType As EnumBatchType, ByVal DocID As String, ByVal DocRef As String, ByVal DocSubject As String, ByVal DocStatus As String)

            Me.DocID = New Guid(DocID)
            Me.DocRef = DocRef
            Me.DocSubject = DocSubject
            Me.DocStatus = DocStatus

            Dim _Result As Integer = -99
            If DocType = EnumBatchType.Invoices Then
                _Result = Invoicing.CreateFinancialsDocumentFromInvoiceID(Me.DocID, Me.Doc)
            Else
                _Result = Credits.CreateFinancialsDocumentFromCreditID(Me.DocID, Me.Doc)
            End If

            Select Case _Result
                Case 0 : Me.Status = "OK"
                Case -1 : Me.Status = "Could not load Document using ID"
                Case -2 : Me.Status = "Missing Customer Record"
                Case -3 : Me.Status = "Missing Nominal Code"
                Case -4 : Me.Status = "Missing Nominal Tracking"
                Case Else : Me.Status = "Unhandled Result"
            End Select

            If _Result = 0 Then Me.CanPost = True

        End Sub

        Property DocID As Guid
        Property DocRef As String
        Property DocSubject As String
        Property DocStatus As String
        Property CanPost As Boolean
        Property Status As String
        Property ErrorText As String
        Property Doc As Care.Financials.Document
        Property Ex As Exception

    End Class

    Private Sub btnRetry_Click(sender As Object, e As EventArgs) Handles btnRetry.Click
        m_PostingDocs.Clear()
        cgDocs.Clear()
        PopulateAndPost()
    End Sub

    Private Sub frmFinancialsPost_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        If Session.FinancialsIntegrated Then
            m_Eng.Disconnect()
        End If
    End Sub

#End Region

End Class