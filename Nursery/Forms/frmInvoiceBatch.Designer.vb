﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInvoiceBatch
    Inherits Care.Shared.frmBaseMaintenance

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnPrintOptions = New Care.Controls.CareButton(Me.components)
        Me.GroupControl1 = New Care.Controls.CareFrame(Me.components)
        Me.txtComments = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel3 = New Care.Controls.CareLabel(Me.components)
        Me.btnGlobalChanges = New Care.Controls.CareButton(Me.components)
        Me.btnPost = New Care.Controls.CareButton(Me.components)
        Me.btnPrintEmail = New Care.Controls.CareButton(Me.components)
        Me.txtStatus = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.txtBatchNo = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.chkHideNL = New Care.Controls.CareCheckBox(Me.components)
        Me.cgbInvoices = New Care.Controls.CareGrid()
        Me.btnAmendInvoice = New Care.Controls.CareButton(Me.components)
        Me.btnDeleteInvoice = New Care.Controls.CareButton(Me.components)
        Me.btnAddInvoice = New Care.Controls.CareButton(Me.components)
        Me.btnCrossCheck = New Care.Controls.CareButton(Me.components)
        Me.GroupControl2 = New Care.Controls.CareFrame(Me.components)
        Me.chkHideDDInfo = New Care.Controls.CareCheckBox(Me.components)
        Me.radManual = New Care.Controls.CareRadioButton(Me.components)
        Me.radXCheck = New Care.Controls.CareRadioButton(Me.components)
        Me.radAll = New Care.Controls.CareRadioButton(Me.components)
        Me.chkHideXCheck = New Care.Controls.CareCheckBox(Me.components)
        Me.txtRed = New Care.Controls.CareTextBox(Me.components)
        Me.txtYellow = New Care.Controls.CareTextBox(Me.components)
        Me.txtBlue = New Care.Controls.CareTextBox(Me.components)
        Me.txtGreen = New Care.Controls.CareTextBox(Me.components)
        Me.gbxGlobalChanges = New Care.Controls.CareFrame(Me.components)
        Me.CareLabel5 = New Care.Controls.CareLabel(Me.components)
        Me.cdtGlobalInvoiceDate = New Care.Controls.CareDateTime(Me.components)
        Me.cdtGlobalInvoiceDue = New Care.Controls.CareDateTime(Me.components)
        Me.btnGlobalCancel = New Care.Controls.CareButton(Me.components)
        Me.btnGlobalRun = New Care.Controls.CareButton(Me.components)
        Me.CareLabel4 = New Care.Controls.CareLabel(Me.components)
        Me.gbxPrint = New Care.Controls.CareFrame(Me.components)
        Me.btnPrintBatch = New Care.Controls.CareButton(Me.components)
        Me.btnPrintClose = New Care.Controls.CareButton(Me.components)
        Me.btnPrintInvoices = New Care.Controls.CareButton(Me.components)
        Me.chkHideDeposits = New Care.Controls.CareCheckBox(Me.components)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtComments.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBatchNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkHideNL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.chkHideDDInfo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radManual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radXCheck.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radAll.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkHideXCheck.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBlue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxGlobalChanges, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxGlobalChanges.SuspendLayout()
        CType(Me.cdtGlobalInvoiceDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtGlobalInvoiceDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtGlobalInvoiceDue.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtGlobalInvoiceDue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gbxPrint, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxPrint.SuspendLayout()
        CType(Me.chkHideDeposits.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.Location = New System.Drawing.Point(857, 3)
        Me.btnCancel.TabIndex = 6
        Me.btnCancel.Text = "Close"
        '
        'btnOK
        '
        Me.btnOK.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Appearance.Options.UseFont = True
        Me.btnOK.Location = New System.Drawing.Point(766, 3)
        Me.btnOK.TabIndex = 5
        Me.btnOK.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnCrossCheck)
        Me.Panel1.Controls.Add(Me.btnAddInvoice)
        Me.Panel1.Controls.Add(Me.btnDeleteInvoice)
        Me.Panel1.Controls.Add(Me.btnAmendInvoice)
        Me.Panel1.Controls.Add(Me.btnPrintOptions)
        Me.Panel1.Location = New System.Drawing.Point(0, 488)
        Me.Panel1.Size = New System.Drawing.Size(954, 36)
        Me.Panel1.TabIndex = 4
        Me.Panel1.Controls.SetChildIndex(Me.btnOK, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnCancel, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnPrintOptions, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnAmendInvoice, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnDeleteInvoice, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnAddInvoice, 0)
        Me.Panel1.Controls.SetChildIndex(Me.btnCrossCheck, 0)
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        '
        'btnPrintOptions
        '
        Me.btnPrintOptions.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnPrintOptions.Appearance.Options.UseFont = True
        Me.btnPrintOptions.CausesValidation = False
        Me.btnPrintOptions.Location = New System.Drawing.Point(465, 3)
        Me.btnPrintOptions.Name = "btnPrintOptions"
        Me.btnPrintOptions.Size = New System.Drawing.Size(145, 25)
        Me.btnPrintOptions.TabIndex = 3
        Me.btnPrintOptions.Tag = ""
        Me.btnPrintOptions.Text = "Print Options..."
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.txtComments)
        Me.GroupControl1.Controls.Add(Me.CareLabel3)
        Me.GroupControl1.Controls.Add(Me.btnGlobalChanges)
        Me.GroupControl1.Controls.Add(Me.btnPost)
        Me.GroupControl1.Controls.Add(Me.btnPrintEmail)
        Me.GroupControl1.Controls.Add(Me.txtStatus)
        Me.GroupControl1.Controls.Add(Me.CareLabel2)
        Me.GroupControl1.Controls.Add(Me.txtBatchNo)
        Me.GroupControl1.Controls.Add(Me.CareLabel1)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 53)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(930, 87)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Batch Details"
        '
        'txtComments
        '
        Me.txtComments.CharacterCasing = CharacterCasing.Normal
        Me.txtComments.EnterMoveNextControl = True
        Me.txtComments.Location = New System.Drawing.Point(95, 57)
        Me.txtComments.MaxLength = 0
        Me.txtComments.Name = "txtComments"
        Me.txtComments.NumericAllowNegatives = False
        Me.txtComments.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtComments.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtComments.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtComments.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.txtComments.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtComments.Properties.Appearance.Options.UseBackColor = True
        Me.txtComments.Properties.Appearance.Options.UseFont = True
        Me.txtComments.Properties.Appearance.Options.UseTextOptions = True
        Me.txtComments.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtComments.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtComments.Properties.ReadOnly = True
        Me.txtComments.Size = New System.Drawing.Size(352, 22)
        Me.txtComments.TabIndex = 8
        Me.txtComments.TabStop = False
        Me.txtComments.TextAlign = HorizontalAlignment.Left
        Me.txtComments.ToolTipText = ""
        '
        'CareLabel3
        '
        Me.CareLabel3.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel3.Location = New System.Drawing.Point(12, 60)
        Me.CareLabel3.Name = "CareLabel3"
        Me.CareLabel3.Size = New System.Drawing.Size(59, 15)
        Me.CareLabel3.TabIndex = 7
        Me.CareLabel3.Text = "Comments"
        Me.CareLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnGlobalChanges
        '
        Me.btnGlobalChanges.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnGlobalChanges.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnGlobalChanges.Appearance.Options.UseFont = True
        Me.btnGlobalChanges.CausesValidation = False
        Me.btnGlobalChanges.Location = New System.Drawing.Point(488, 27)
        Me.btnGlobalChanges.Name = "btnGlobalChanges"
        Me.btnGlobalChanges.Size = New System.Drawing.Size(140, 25)
        Me.btnGlobalChanges.TabIndex = 4
        Me.btnGlobalChanges.Tag = ""
        Me.btnGlobalChanges.Text = "Global Changes"
        '
        'btnPost
        '
        Me.btnPost.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnPost.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnPost.Appearance.Options.UseFont = True
        Me.btnPost.CausesValidation = False
        Me.btnPost.Location = New System.Drawing.Point(780, 27)
        Me.btnPost.Name = "btnPost"
        Me.btnPost.Size = New System.Drawing.Size(140, 25)
        Me.btnPost.TabIndex = 6
        Me.btnPost.Tag = ""
        Me.btnPost.Text = "Post Batch"
        Me.btnPost.ToolTip = "Once all invoices have been printed and/or emailed you can post the batch." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Pos" &
    "ting the batch updates your Financial System or Accounts package if integration " &
    "has been setup." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnPost.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.btnPost.ToolTipTitle = "Post Batch"
        '
        'btnPrintEmail
        '
        Me.btnPrintEmail.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnPrintEmail.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnPrintEmail.Appearance.Options.UseFont = True
        Me.btnPrintEmail.CausesValidation = False
        Me.btnPrintEmail.Location = New System.Drawing.Point(634, 27)
        Me.btnPrintEmail.Name = "btnPrintEmail"
        Me.btnPrintEmail.Size = New System.Drawing.Size(140, 25)
        Me.btnPrintEmail.TabIndex = 5
        Me.btnPrintEmail.Tag = ""
        Me.btnPrintEmail.Text = "Send Invoices"
        '
        'txtStatus
        '
        Me.txtStatus.CharacterCasing = CharacterCasing.Normal
        Me.txtStatus.EnterMoveNextControl = True
        Me.txtStatus.Location = New System.Drawing.Point(249, 29)
        Me.txtStatus.MaxLength = 0
        Me.txtStatus.Name = "txtStatus"
        Me.txtStatus.NumericAllowNegatives = False
        Me.txtStatus.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtStatus.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtStatus.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtStatus.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.txtStatus.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtStatus.Properties.Appearance.Options.UseBackColor = True
        Me.txtStatus.Properties.Appearance.Options.UseFont = True
        Me.txtStatus.Properties.Appearance.Options.UseTextOptions = True
        Me.txtStatus.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtStatus.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtStatus.Properties.ReadOnly = True
        Me.txtStatus.Size = New System.Drawing.Size(198, 22)
        Me.txtStatus.TabIndex = 3
        Me.txtStatus.TabStop = False
        Me.txtStatus.TextAlign = HorizontalAlignment.Left
        Me.txtStatus.ToolTipText = ""
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(211, 32)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(32, 15)
        Me.CareLabel2.TabIndex = 2
        Me.CareLabel2.Text = "Status"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBatchNo
        '
        Me.txtBatchNo.CharacterCasing = CharacterCasing.Normal
        Me.txtBatchNo.EnterMoveNextControl = True
        Me.txtBatchNo.Location = New System.Drawing.Point(95, 29)
        Me.txtBatchNo.MaxLength = 0
        Me.txtBatchNo.Name = "txtBatchNo"
        Me.txtBatchNo.NumericAllowNegatives = False
        Me.txtBatchNo.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtBatchNo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBatchNo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtBatchNo.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.txtBatchNo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtBatchNo.Properties.Appearance.Options.UseBackColor = True
        Me.txtBatchNo.Properties.Appearance.Options.UseFont = True
        Me.txtBatchNo.Properties.Appearance.Options.UseTextOptions = True
        Me.txtBatchNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtBatchNo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtBatchNo.Properties.ReadOnly = True
        Me.txtBatchNo.Size = New System.Drawing.Size(100, 22)
        Me.txtBatchNo.TabIndex = 1
        Me.txtBatchNo.TabStop = False
        Me.txtBatchNo.TextAlign = HorizontalAlignment.Left
        Me.txtBatchNo.ToolTipText = ""
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(12, 32)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(77, 15)
        Me.CareLabel1.TabIndex = 0
        Me.CareLabel1.Text = "Batch Number"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkHideNL
        '
        Me.chkHideNL.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.chkHideNL.EditValue = True
        Me.chkHideNL.EnterMoveNextControl = True
        Me.chkHideNL.Location = New System.Drawing.Point(812, 5)
        Me.chkHideNL.Name = "chkHideNL"
        Me.chkHideNL.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkHideNL.Properties.Appearance.Options.UseFont = True
        Me.chkHideNL.Properties.Appearance.Options.UseTextOptions = True
        Me.chkHideNL.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.chkHideNL.Properties.Caption = "Hide NL Codes"
        Me.chkHideNL.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.chkHideNL.Size = New System.Drawing.Size(108, 19)
        Me.chkHideNL.TabIndex = 4
        Me.chkHideNL.TabStop = False
        '
        'cgbInvoices
        '
        Me.cgbInvoices.AllowBuildColumns = True
        Me.cgbInvoices.AllowEdit = False
        Me.cgbInvoices.AllowHorizontalScroll = False
        Me.cgbInvoices.AllowMultiSelect = False
        Me.cgbInvoices.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgbInvoices.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgbInvoices.Appearance.Options.UseFont = True
        Me.cgbInvoices.AutoSizeByData = True
        Me.cgbInvoices.DisableAutoSize = False
        Me.cgbInvoices.DisableDataFormatting = False
        Me.cgbInvoices.FocusedRowHandle = -2147483648
        Me.cgbInvoices.HideFirstColumn = False
        Me.cgbInvoices.Location = New System.Drawing.Point(12, 180)
        Me.cgbInvoices.Name = "cgbInvoices"
        Me.cgbInvoices.PreviewColumn = ""
        Me.cgbInvoices.QueryID = Nothing
        Me.cgbInvoices.RowAutoHeight = False
        Me.cgbInvoices.SearchAsYouType = True
        Me.cgbInvoices.ShowAutoFilterRow = True
        Me.cgbInvoices.ShowFindPanel = True
        Me.cgbInvoices.ShowGroupByBox = False
        Me.cgbInvoices.ShowLoadingPanel = False
        Me.cgbInvoices.ShowNavigator = False
        Me.cgbInvoices.Size = New System.Drawing.Size(930, 302)
        Me.cgbInvoices.TabIndex = 2
        '
        'btnAmendInvoice
        '
        Me.btnAmendInvoice.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnAmendInvoice.Appearance.Options.UseFont = True
        Me.btnAmendInvoice.CausesValidation = False
        Me.btnAmendInvoice.Location = New System.Drawing.Point(163, 3)
        Me.btnAmendInvoice.Name = "btnAmendInvoice"
        Me.btnAmendInvoice.Size = New System.Drawing.Size(145, 25)
        Me.btnAmendInvoice.TabIndex = 1
        Me.btnAmendInvoice.Tag = ""
        Me.btnAmendInvoice.Text = "Amend Invoice"
        '
        'btnDeleteInvoice
        '
        Me.btnDeleteInvoice.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnDeleteInvoice.Appearance.Options.UseFont = True
        Me.btnDeleteInvoice.CausesValidation = False
        Me.btnDeleteInvoice.Location = New System.Drawing.Point(314, 3)
        Me.btnDeleteInvoice.Name = "btnDeleteInvoice"
        Me.btnDeleteInvoice.Size = New System.Drawing.Size(145, 25)
        Me.btnDeleteInvoice.TabIndex = 2
        Me.btnDeleteInvoice.Tag = ""
        Me.btnDeleteInvoice.Text = "Delete Invoice"
        '
        'btnAddInvoice
        '
        Me.btnAddInvoice.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnAddInvoice.Appearance.Options.UseFont = True
        Me.btnAddInvoice.CausesValidation = False
        Me.btnAddInvoice.Location = New System.Drawing.Point(12, 3)
        Me.btnAddInvoice.Name = "btnAddInvoice"
        Me.btnAddInvoice.Size = New System.Drawing.Size(145, 25)
        Me.btnAddInvoice.TabIndex = 0
        Me.btnAddInvoice.Tag = ""
        Me.btnAddInvoice.Text = "Add Invoice"
        '
        'btnCrossCheck
        '
        Me.btnCrossCheck.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnCrossCheck.Appearance.Options.UseFont = True
        Me.btnCrossCheck.CausesValidation = False
        Me.btnCrossCheck.Location = New System.Drawing.Point(616, 3)
        Me.btnCrossCheck.Name = "btnCrossCheck"
        Me.btnCrossCheck.Size = New System.Drawing.Size(145, 25)
        Me.btnCrossCheck.TabIndex = 4
        Me.btnCrossCheck.Tag = ""
        Me.btnCrossCheck.Text = "Cross Checking"
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl2.Controls.Add(Me.chkHideDeposits)
        Me.GroupControl2.Controls.Add(Me.chkHideDDInfo)
        Me.GroupControl2.Controls.Add(Me.radManual)
        Me.GroupControl2.Controls.Add(Me.radXCheck)
        Me.GroupControl2.Controls.Add(Me.radAll)
        Me.GroupControl2.Controls.Add(Me.chkHideXCheck)
        Me.GroupControl2.Controls.Add(Me.chkHideNL)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 146)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.ShowCaption = False
        Me.GroupControl2.Size = New System.Drawing.Size(930, 28)
        Me.GroupControl2.TabIndex = 1
        '
        'chkHideDDInfo
        '
        Me.chkHideDDInfo.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.chkHideDDInfo.EditValue = True
        Me.chkHideDDInfo.EnterMoveNextControl = True
        Me.chkHideDDInfo.Location = New System.Drawing.Point(532, 5)
        Me.chkHideDDInfo.Name = "chkHideDDInfo"
        Me.chkHideDDInfo.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkHideDDInfo.Properties.Appearance.Options.UseFont = True
        Me.chkHideDDInfo.Properties.Appearance.Options.UseTextOptions = True
        Me.chkHideDDInfo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.chkHideDDInfo.Properties.Caption = "Hide Voucher Info"
        Me.chkHideDDInfo.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.chkHideDDInfo.Size = New System.Drawing.Size(128, 19)
        Me.chkHideDDInfo.TabIndex = 5
        Me.chkHideDDInfo.TabStop = False
        '
        'radManual
        '
        Me.radManual.Location = New System.Drawing.Point(228, 5)
        Me.radManual.Name = "radManual"
        Me.radManual.Properties.AutoWidth = True
        Me.radManual.Properties.Caption = "Amended Invoices"
        Me.radManual.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radManual.Properties.RadioGroupIndex = 0
        Me.radManual.Size = New System.Drawing.Size(110, 19)
        Me.radManual.TabIndex = 2
        Me.radManual.TabStop = False
        Me.radManual.ToolTip = "Show only invoices with Manual amendments"
        Me.radManual.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.radManual.ToolTipTitle = "Amended Invoices"
        '
        'radXCheck
        '
        Me.radXCheck.Location = New System.Drawing.Point(93, 5)
        Me.radXCheck.Name = "radXCheck"
        Me.radXCheck.Properties.AutoWidth = True
        Me.radXCheck.Properties.Caption = "Cross Check Warnings"
        Me.radXCheck.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radXCheck.Properties.RadioGroupIndex = 0
        Me.radXCheck.Size = New System.Drawing.Size(129, 19)
        Me.radXCheck.TabIndex = 1
        Me.radXCheck.TabStop = False
        Me.radXCheck.ToolTip = "Show only Invoices with Cross Check warnings"
        Me.radXCheck.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.radXCheck.ToolTipTitle = "Cross Check Warnings"
        '
        'radAll
        '
        Me.radAll.EditValue = True
        Me.radAll.Location = New System.Drawing.Point(10, 5)
        Me.radAll.Name = "radAll"
        Me.radAll.Properties.AutoWidth = True
        Me.radAll.Properties.Caption = "All Invoices"
        Me.radAll.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radAll.Properties.RadioGroupIndex = 0
        Me.radAll.Size = New System.Drawing.Size(76, 19)
        Me.radAll.TabIndex = 0
        Me.radAll.ToolTip = "Show all Invoices in the batch"
        Me.radAll.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.radAll.ToolTipTitle = "All Invoices"
        '
        'chkHideXCheck
        '
        Me.chkHideXCheck.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.chkHideXCheck.EditValue = True
        Me.chkHideXCheck.EnterMoveNextControl = True
        Me.chkHideXCheck.Location = New System.Drawing.Point(666, 5)
        Me.chkHideXCheck.Name = "chkHideXCheck"
        Me.chkHideXCheck.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkHideXCheck.Properties.Appearance.Options.UseFont = True
        Me.chkHideXCheck.Properties.Appearance.Options.UseTextOptions = True
        Me.chkHideXCheck.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.chkHideXCheck.Properties.Caption = "Hide Cross Checking"
        Me.chkHideXCheck.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.chkHideXCheck.Size = New System.Drawing.Size(140, 19)
        Me.chkHideXCheck.TabIndex = 3
        Me.chkHideXCheck.TabStop = False
        '
        'txtRed
        '
        Me.txtRed.CharacterCasing = CharacterCasing.Normal
        Me.txtRed.EnterMoveNextControl = True
        Me.txtRed.Location = New System.Drawing.Point(24, 451)
        Me.txtRed.MaxLength = 0
        Me.txtRed.Name = "txtRed"
        Me.txtRed.NumericAllowNegatives = False
        Me.txtRed.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtRed.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtRed.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtRed.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtRed.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtRed.Properties.Appearance.Options.UseBackColor = True
        Me.txtRed.Properties.Appearance.Options.UseFont = True
        Me.txtRed.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRed.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtRed.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRed.Size = New System.Drawing.Size(25, 22)
        Me.txtRed.TabIndex = 7
        Me.txtRed.TextAlign = HorizontalAlignment.Left
        Me.txtRed.ToolTipText = ""
        Me.txtRed.Visible = False
        '
        'txtYellow
        '
        Me.txtYellow.CharacterCasing = CharacterCasing.Normal
        Me.txtYellow.EnterMoveNextControl = True
        Me.txtYellow.Location = New System.Drawing.Point(115, 451)
        Me.txtYellow.MaxLength = 0
        Me.txtYellow.Name = "txtYellow"
        Me.txtYellow.NumericAllowNegatives = False
        Me.txtYellow.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtYellow.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtYellow.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtYellow.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtYellow.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtYellow.Properties.Appearance.Options.UseBackColor = True
        Me.txtYellow.Properties.Appearance.Options.UseFont = True
        Me.txtYellow.Properties.Appearance.Options.UseTextOptions = True
        Me.txtYellow.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtYellow.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtYellow.Size = New System.Drawing.Size(25, 22)
        Me.txtYellow.TabIndex = 10
        Me.txtYellow.TextAlign = HorizontalAlignment.Left
        Me.txtYellow.ToolTipText = ""
        Me.txtYellow.Visible = False
        '
        'txtBlue
        '
        Me.txtBlue.CharacterCasing = CharacterCasing.Normal
        Me.txtBlue.EnterMoveNextControl = True
        Me.txtBlue.Location = New System.Drawing.Point(84, 451)
        Me.txtBlue.MaxLength = 0
        Me.txtBlue.Name = "txtBlue"
        Me.txtBlue.NumericAllowNegatives = False
        Me.txtBlue.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtBlue.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBlue.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtBlue.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtBlue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtBlue.Properties.Appearance.Options.UseBackColor = True
        Me.txtBlue.Properties.Appearance.Options.UseFont = True
        Me.txtBlue.Properties.Appearance.Options.UseTextOptions = True
        Me.txtBlue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtBlue.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtBlue.Size = New System.Drawing.Size(25, 22)
        Me.txtBlue.TabIndex = 9
        Me.txtBlue.TextAlign = HorizontalAlignment.Left
        Me.txtBlue.ToolTipText = ""
        Me.txtBlue.Visible = False
        '
        'txtGreen
        '
        Me.txtGreen.CharacterCasing = CharacterCasing.Normal
        Me.txtGreen.EnterMoveNextControl = True
        Me.txtGreen.Location = New System.Drawing.Point(53, 451)
        Me.txtGreen.MaxLength = 0
        Me.txtGreen.Name = "txtGreen"
        Me.txtGreen.NumericAllowNegatives = False
        Me.txtGreen.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtGreen.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtGreen.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtGreen.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtGreen.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.txtGreen.Properties.Appearance.Options.UseBackColor = True
        Me.txtGreen.Properties.Appearance.Options.UseFont = True
        Me.txtGreen.Properties.Appearance.Options.UseTextOptions = True
        Me.txtGreen.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtGreen.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtGreen.Size = New System.Drawing.Size(25, 22)
        Me.txtGreen.TabIndex = 8
        Me.txtGreen.TextAlign = HorizontalAlignment.Left
        Me.txtGreen.ToolTipText = ""
        Me.txtGreen.Visible = False
        '
        'gbxGlobalChanges
        '
        Me.gbxGlobalChanges.Anchor = AnchorStyles.None
        Me.gbxGlobalChanges.Controls.Add(Me.CareLabel5)
        Me.gbxGlobalChanges.Controls.Add(Me.cdtGlobalInvoiceDate)
        Me.gbxGlobalChanges.Controls.Add(Me.cdtGlobalInvoiceDue)
        Me.gbxGlobalChanges.Controls.Add(Me.btnGlobalCancel)
        Me.gbxGlobalChanges.Controls.Add(Me.btnGlobalRun)
        Me.gbxGlobalChanges.Controls.Add(Me.CareLabel4)
        Me.gbxGlobalChanges.Location = New System.Drawing.Point(380, 202)
        Me.gbxGlobalChanges.Name = "gbxGlobalChanges"
        Me.gbxGlobalChanges.Size = New System.Drawing.Size(195, 120)
        Me.gbxGlobalChanges.TabIndex = 3
        Me.gbxGlobalChanges.Text = "Batch Details"
        '
        'CareLabel5
        '
        Me.CareLabel5.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel5.Location = New System.Drawing.Point(12, 60)
        Me.CareLabel5.Name = "CareLabel5"
        Me.CareLabel5.Size = New System.Drawing.Size(62, 15)
        Me.CareLabel5.TabIndex = 5
        Me.CareLabel5.Text = "Invoice Due"
        Me.CareLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cdtGlobalInvoiceDate
        '
        Me.cdtGlobalInvoiceDate.EditValue = New Date(2012, 6, 30, 0, 0, 0, 0)
        Me.cdtGlobalInvoiceDate.EnterMoveNextControl = True
        Me.cdtGlobalInvoiceDate.Location = New System.Drawing.Point(103, 29)
        Me.cdtGlobalInvoiceDate.Name = "cdtGlobalInvoiceDate"
        Me.cdtGlobalInvoiceDate.Properties.AccessibleName = "Date of Birth"
        Me.cdtGlobalInvoiceDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cdtGlobalInvoiceDate.Properties.Appearance.Options.UseFont = True
        Me.cdtGlobalInvoiceDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtGlobalInvoiceDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtGlobalInvoiceDate.Size = New System.Drawing.Size(83, 22)
        Me.cdtGlobalInvoiceDate.TabIndex = 0
        Me.cdtGlobalInvoiceDate.Tag = ""
        Me.cdtGlobalInvoiceDate.Value = New Date(2012, 6, 30, 0, 0, 0, 0)
        '
        'cdtGlobalInvoiceDue
        '
        Me.cdtGlobalInvoiceDue.EditValue = New Date(2012, 6, 30, 0, 0, 0, 0)
        Me.cdtGlobalInvoiceDue.EnterMoveNextControl = True
        Me.cdtGlobalInvoiceDue.Location = New System.Drawing.Point(103, 57)
        Me.cdtGlobalInvoiceDue.Name = "cdtGlobalInvoiceDue"
        Me.cdtGlobalInvoiceDue.Properties.AccessibleName = "Date of Birth"
        Me.cdtGlobalInvoiceDue.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cdtGlobalInvoiceDue.Properties.Appearance.Options.UseFont = True
        Me.cdtGlobalInvoiceDue.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtGlobalInvoiceDue.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtGlobalInvoiceDue.Size = New System.Drawing.Size(83, 22)
        Me.cdtGlobalInvoiceDue.TabIndex = 1
        Me.cdtGlobalInvoiceDue.Tag = ""
        Me.cdtGlobalInvoiceDue.Value = New Date(2012, 6, 30, 0, 0, 0, 0)
        '
        'btnGlobalCancel
        '
        Me.btnGlobalCancel.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnGlobalCancel.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnGlobalCancel.Appearance.Options.UseFont = True
        Me.btnGlobalCancel.CausesValidation = False
        Me.btnGlobalCancel.Location = New System.Drawing.Point(102, 87)
        Me.btnGlobalCancel.Name = "btnGlobalCancel"
        Me.btnGlobalCancel.Size = New System.Drawing.Size(85, 25)
        Me.btnGlobalCancel.TabIndex = 3
        Me.btnGlobalCancel.Tag = ""
        Me.btnGlobalCancel.Text = "Cancel"
        '
        'btnGlobalRun
        '
        Me.btnGlobalRun.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Right), AnchorStyles)
        Me.btnGlobalRun.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnGlobalRun.Appearance.Options.UseFont = True
        Me.btnGlobalRun.CausesValidation = False
        Me.btnGlobalRun.Location = New System.Drawing.Point(11, 87)
        Me.btnGlobalRun.Name = "btnGlobalRun"
        Me.btnGlobalRun.Size = New System.Drawing.Size(85, 25)
        Me.btnGlobalRun.TabIndex = 2
        Me.btnGlobalRun.Tag = ""
        Me.btnGlobalRun.Text = "Change"
        '
        'CareLabel4
        '
        Me.CareLabel4.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel4.Location = New System.Drawing.Point(12, 32)
        Me.CareLabel4.Name = "CareLabel4"
        Me.CareLabel4.Size = New System.Drawing.Size(65, 15)
        Me.CareLabel4.TabIndex = 4
        Me.CareLabel4.Text = "Invoice Date"
        Me.CareLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbxPrint
        '
        Me.gbxPrint.Anchor = AnchorStyles.Bottom
        Me.gbxPrint.Controls.Add(Me.btnPrintBatch)
        Me.gbxPrint.Controls.Add(Me.btnPrintClose)
        Me.gbxPrint.Controls.Add(Me.btnPrintInvoices)
        Me.gbxPrint.Location = New System.Drawing.Point(251, 409)
        Me.gbxPrint.Name = "gbxPrint"
        Me.gbxPrint.Size = New System.Drawing.Size(452, 60)
        Me.gbxPrint.TabIndex = 11
        Me.gbxPrint.Text = "Print Options"
        '
        'btnPrintBatch
        '
        Me.btnPrintBatch.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnPrintBatch.Appearance.Options.UseFont = True
        Me.btnPrintBatch.CausesValidation = False
        Me.btnPrintBatch.Location = New System.Drawing.Point(11, 27)
        Me.btnPrintBatch.Name = "btnPrintBatch"
        Me.btnPrintBatch.Size = New System.Drawing.Size(140, 25)
        Me.btnPrintBatch.TabIndex = 5
        Me.btnPrintBatch.Tag = ""
        Me.btnPrintBatch.Text = "Print Batch Summary"
        '
        'btnPrintClose
        '
        Me.btnPrintClose.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnPrintClose.Appearance.Options.UseFont = True
        Me.btnPrintClose.CausesValidation = False
        Me.btnPrintClose.Location = New System.Drawing.Point(303, 27)
        Me.btnPrintClose.Name = "btnPrintClose"
        Me.btnPrintClose.Size = New System.Drawing.Size(140, 25)
        Me.btnPrintClose.TabIndex = 4
        Me.btnPrintClose.Tag = ""
        Me.btnPrintClose.Text = "Close"
        '
        'btnPrintInvoices
        '
        Me.btnPrintInvoices.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.btnPrintInvoices.Appearance.Options.UseFont = True
        Me.btnPrintInvoices.CausesValidation = False
        Me.btnPrintInvoices.Location = New System.Drawing.Point(157, 27)
        Me.btnPrintInvoices.Name = "btnPrintInvoices"
        Me.btnPrintInvoices.Size = New System.Drawing.Size(140, 25)
        Me.btnPrintInvoices.TabIndex = 2
        Me.btnPrintInvoices.Tag = ""
        Me.btnPrintInvoices.Text = "Print All Invoices"
        '
        'chkHideDeposits
        '
        Me.chkHideDeposits.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.chkHideDeposits.EditValue = True
        Me.chkHideDeposits.EnterMoveNextControl = True
        Me.chkHideDeposits.Location = New System.Drawing.Point(408, 5)
        Me.chkHideDeposits.Name = "chkHideDeposits"
        Me.chkHideDeposits.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.chkHideDeposits.Properties.Appearance.Options.UseFont = True
        Me.chkHideDeposits.Properties.Appearance.Options.UseTextOptions = True
        Me.chkHideDeposits.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.chkHideDeposits.Properties.Caption = "Hide Deposits"
        Me.chkHideDeposits.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.chkHideDeposits.Size = New System.Drawing.Size(128, 19)
        Me.chkHideDeposits.TabIndex = 6
        Me.chkHideDeposits.TabStop = False
        '
        'frmInvoiceBatch
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(954, 524)
        Me.Controls.Add(Me.gbxPrint)
        Me.Controls.Add(Me.gbxGlobalChanges)
        Me.Controls.Add(Me.txtRed)
        Me.Controls.Add(Me.txtYellow)
        Me.Controls.Add(Me.txtBlue)
        Me.Controls.Add(Me.txtGreen)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.cgbInvoices)
        Me.Controls.Add(Me.GroupControl1)
        Me.MaximizeBox = True
        Me.Name = "frmInvoiceBatch"
        Me.WindowState = FormWindowState.Maximized
        Me.Controls.SetChildIndex(Me.Panel1, 0)
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        Me.Controls.SetChildIndex(Me.cgbInvoices, 0)
        Me.Controls.SetChildIndex(Me.GroupControl2, 0)
        Me.Controls.SetChildIndex(Me.txtGreen, 0)
        Me.Controls.SetChildIndex(Me.txtBlue, 0)
        Me.Controls.SetChildIndex(Me.txtYellow, 0)
        Me.Controls.SetChildIndex(Me.txtRed, 0)
        Me.Controls.SetChildIndex(Me.gbxGlobalChanges, 0)
        Me.Controls.SetChildIndex(Me.gbxPrint, 0)
        CType(Me.bs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtComments.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBatchNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkHideNL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.chkHideDDInfo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radManual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radXCheck.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radAll.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkHideXCheck.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtYellow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBlue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGreen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxGlobalChanges, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxGlobalChanges.ResumeLayout(False)
        Me.gbxGlobalChanges.PerformLayout()
        CType(Me.cdtGlobalInvoiceDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtGlobalInvoiceDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtGlobalInvoiceDue.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtGlobalInvoiceDue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gbxPrint, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxPrint.ResumeLayout(False)
        Me.gbxPrint.PerformLayout()
        CType(Me.chkHideDeposits.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnPrintOptions As Care.Controls.CareButton
    Friend WithEvents btnPost As Care.Controls.CareButton
    Friend WithEvents btnPrintEmail As Care.Controls.CareButton
    Friend WithEvents txtStatus As Care.Controls.CareTextBox
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents txtBatchNo As Care.Controls.CareTextBox
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents btnAmendInvoice As Care.Controls.CareButton
    Friend WithEvents cgbInvoices As Care.Controls.CareGrid
    Friend WithEvents btnDeleteInvoice As Care.Controls.CareButton
    Friend WithEvents btnAddInvoice As Care.Controls.CareButton
    Friend WithEvents btnCrossCheck As Care.Controls.CareButton
    Friend WithEvents chkHideNL As Care.Controls.CareCheckBox
    Friend WithEvents chkHideXCheck As Care.Controls.CareCheckBox
    Friend WithEvents radManual As Care.Controls.CareRadioButton
    Friend WithEvents radXCheck As Care.Controls.CareRadioButton
    Friend WithEvents radAll As Care.Controls.CareRadioButton
    Friend WithEvents txtRed As Care.Controls.CareTextBox
    Friend WithEvents txtYellow As Care.Controls.CareTextBox
    Friend WithEvents txtBlue As Care.Controls.CareTextBox
    Friend WithEvents txtGreen As Care.Controls.CareTextBox
    Friend WithEvents btnGlobalChanges As Care.Controls.CareButton
    Friend WithEvents btnGlobalCancel As Care.Controls.CareButton
    Friend WithEvents btnGlobalRun As Care.Controls.CareButton
    Friend WithEvents CareLabel4 As Care.Controls.CareLabel
    Friend WithEvents CareLabel5 As Care.Controls.CareLabel
    Friend WithEvents cdtGlobalInvoiceDate As Care.Controls.CareDateTime
    Friend WithEvents cdtGlobalInvoiceDue As Care.Controls.CareDateTime
    Friend WithEvents chkHideDDInfo As Care.Controls.CareCheckBox
    Friend WithEvents btnPrintBatch As Care.Controls.CareButton
    Friend WithEvents btnPrintClose As Care.Controls.CareButton
    Friend WithEvents btnPrintInvoices As Care.Controls.CareButton
    Friend WithEvents txtComments As Care.Controls.CareTextBox
    Friend WithEvents CareLabel3 As Care.Controls.CareLabel
    Friend WithEvents GroupControl1 As Care.Controls.CareFrame
    Friend WithEvents GroupControl2 As Care.Controls.CareFrame
    Friend WithEvents gbxGlobalChanges As Care.Controls.CareFrame
    Friend WithEvents gbxPrint As Care.Controls.CareFrame
    Friend WithEvents chkHideDeposits As Care.Controls.CareCheckBox
End Class
