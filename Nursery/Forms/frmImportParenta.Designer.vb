﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportParenta
    Inherits Care.Shared.frmBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.cgRecords = New Care.Controls.CareGrid()
        Me.gbxFamily = New Care.Controls.CareFrame()
        Me.CareLabel1 = New Care.Controls.CareLabel(Me.components)
        Me.cbxSite = New Care.Controls.CareComboBox(Me.components)
        Me.cbxKeyworker = New Care.Controls.CareComboBox(Me.components)
        Me.cbxGroup = New Care.Controls.CareComboBox(Me.components)
        Me.Label15 = New Care.Controls.CareLabel(Me.components)
        Me.cdtStartDate = New Care.Controls.CareDateTime(Me.components)
        Me.Label13 = New Care.Controls.CareLabel(Me.components)
        Me.Label14 = New Care.Controls.CareLabel(Me.components)
        Me.ofd = New OpenFileDialog()
        Me.GroupControl3 = New Care.Controls.CareFrame()
        Me.radFamily = New Care.Controls.CareRadioButton(Me.components)
        Me.radStaff = New Care.Controls.CareRadioButton(Me.components)
        Me.GroupControl2 = New Care.Controls.CareFrame()
        Me.btnImport = New Care.Controls.CareButton(Me.components)
        Me.txtFile = New Care.Controls.CareTextBox(Me.components)
        Me.CareLabel2 = New Care.Controls.CareLabel(Me.components)
        Me.btnBrowse = New Care.Controls.CareButton(Me.components)
        CType(Me.gbxFamily, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxFamily.SuspendLayout()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxKeyworker.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbxGroup.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtStartDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdtStartDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.radFamily.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radStaff.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.txtFile.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpellChecker1
        '
        Me.SpellChecker1.CheckAsYouTypeOptions.CheckControlsInParentContainer = True
        Me.SpellChecker1.CheckAsYouTypeOptions.UnderlineStyle = DevExpress.XtraSpellChecker.UnderlineStyle.Line
        Me.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.OptionsSpelling.IgnoreUri = DevExpress.Utils.DefaultBoolean.[True]
        Me.SpellChecker1.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.OnDemand
        '
        'cgRecords
        '
        Me.cgRecords.AllowBuildColumns = True
        Me.cgRecords.AllowEdit = False
        Me.cgRecords.AllowHorizontalScroll = False
        Me.cgRecords.AllowMultiSelect = False
        Me.cgRecords.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
            Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.cgRecords.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cgRecords.Appearance.Options.UseFont = True
        Me.cgRecords.AutoSizeByData = True
        Me.cgRecords.DisableAutoSize = False
        Me.cgRecords.DisableDataFormatting = False
        Me.cgRecords.FocusedRowHandle = -2147483648
        Me.cgRecords.HideFirstColumn = False
        Me.cgRecords.Location = New System.Drawing.Point(12, 162)
        Me.cgRecords.Name = "cgRecords"
        Me.cgRecords.PreviewColumn = ""
        Me.cgRecords.QueryID = Nothing
        Me.cgRecords.RowAutoHeight = False
        Me.cgRecords.SearchAsYouType = True
        Me.cgRecords.ShowAutoFilterRow = False
        Me.cgRecords.ShowFindPanel = True
        Me.cgRecords.ShowGroupByBox = True
        Me.cgRecords.ShowLoadingPanel = False
        Me.cgRecords.ShowNavigator = True
        Me.cgRecords.Size = New System.Drawing.Size(740, 391)
        Me.cgRecords.TabIndex = 3
        '
        'gbxFamily
        '
        Me.gbxFamily.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.gbxFamily.Controls.Add(Me.CareLabel1)
        Me.gbxFamily.Controls.Add(Me.cbxSite)
        Me.gbxFamily.Controls.Add(Me.cbxKeyworker)
        Me.gbxFamily.Controls.Add(Me.cbxGroup)
        Me.gbxFamily.Controls.Add(Me.Label15)
        Me.gbxFamily.Controls.Add(Me.cdtStartDate)
        Me.gbxFamily.Controls.Add(Me.Label13)
        Me.gbxFamily.Controls.Add(Me.Label14)
        Me.gbxFamily.Location = New System.Drawing.Point(12, 57)
        Me.gbxFamily.Name = "gbxFamily"
        Me.gbxFamily.ShowCaption = False
        Me.gbxFamily.Size = New System.Drawing.Size(740, 99)
        Me.gbxFamily.TabIndex = 2
        Me.gbxFamily.Text = "GroupControl1"
        '
        'CareLabel1
        '
        Me.CareLabel1.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel1.Location = New System.Drawing.Point(12, 70)
        Me.CareLabel1.Name = "CareLabel1"
        Me.CareLabel1.Size = New System.Drawing.Size(33, 15)
        Me.CareLabel1.TabIndex = 4
        Me.CareLabel1.Text = "Group"
        Me.CareLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxSite
        '
        Me.cbxSite.AllowBlank = False
        Me.cbxSite.DataSource = Nothing
        Me.cbxSite.DisplayMember = Nothing
        Me.cbxSite.EnterMoveNextControl = True
        Me.cbxSite.Location = New System.Drawing.Point(83, 39)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Properties.AccessibleName = "Group"
        Me.cbxSite.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxSite.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxSite.Properties.Appearance.Options.UseFont = True
        Me.cbxSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxSite.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxSite.SelectedValue = Nothing
        Me.cbxSite.Size = New System.Drawing.Size(250, 22)
        Me.cbxSite.TabIndex = 3
        Me.cbxSite.Tag = ""
        Me.cbxSite.ValueMember = Nothing
        '
        'cbxKeyworker
        '
        Me.cbxKeyworker.AllowBlank = False
        Me.cbxKeyworker.DataSource = Nothing
        Me.cbxKeyworker.DisplayMember = Nothing
        Me.cbxKeyworker.EnterMoveNextControl = True
        Me.cbxKeyworker.Location = New System.Drawing.Point(409, 67)
        Me.cbxKeyworker.Name = "cbxKeyworker"
        Me.cbxKeyworker.Properties.AccessibleName = "Keyworker"
        Me.cbxKeyworker.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxKeyworker.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxKeyworker.Properties.Appearance.Options.UseFont = True
        Me.cbxKeyworker.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxKeyworker.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxKeyworker.SelectedValue = Nothing
        Me.cbxKeyworker.Size = New System.Drawing.Size(238, 22)
        Me.cbxKeyworker.TabIndex = 7
        Me.cbxKeyworker.Tag = ""
        Me.cbxKeyworker.ValueMember = Nothing
        '
        'cbxGroup
        '
        Me.cbxGroup.AllowBlank = False
        Me.cbxGroup.DataSource = Nothing
        Me.cbxGroup.DisplayMember = Nothing
        Me.cbxGroup.EnterMoveNextControl = True
        Me.cbxGroup.Location = New System.Drawing.Point(83, 67)
        Me.cbxGroup.Name = "cbxGroup"
        Me.cbxGroup.Properties.AccessibleName = "Group"
        Me.cbxGroup.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbxGroup.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cbxGroup.Properties.Appearance.Options.UseFont = True
        Me.cbxGroup.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbxGroup.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbxGroup.SelectedValue = Nothing
        Me.cbxGroup.Size = New System.Drawing.Size(250, 22)
        Me.cbxGroup.TabIndex = 5
        Me.cbxGroup.Tag = ""
        Me.cbxGroup.ValueMember = Nothing
        '
        'Label15
        '
        Me.Label15.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label15.Location = New System.Drawing.Point(12, 14)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(51, 15)
        Me.Label15.TabIndex = 0
        Me.Label15.Text = "Start Date"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label15.ToolTip = "The group start date indicates when the child started in the particular group."
        Me.Label15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'cdtStartDate
        '
        Me.cdtStartDate.EditValue = New Date(2011, 6, 10, 0, 0, 0, 0)
        Me.cdtStartDate.EnterMoveNextControl = True
        Me.cdtStartDate.Location = New System.Drawing.Point(83, 11)
        Me.cdtStartDate.Name = "cdtStartDate"
        Me.cdtStartDate.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.cdtStartDate.Properties.Appearance.Options.UseFont = True
        Me.cdtStartDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cdtStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.cdtStartDate.Size = New System.Drawing.Size(104, 22)
        Me.cdtStartDate.TabIndex = 1
        Me.cdtStartDate.Tag = ""
        Me.cdtStartDate.Value = New Date(2011, 6, 10, 0, 0, 0, 0)
        '
        'Label13
        '
        Me.Label13.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label13.Location = New System.Drawing.Point(348, 70)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(55, 15)
        Me.Label13.TabIndex = 6
        Me.Label13.Text = "Keyworker"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label14
        '
        Me.Label14.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Label14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Label14.Location = New System.Drawing.Point(12, 42)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(19, 15)
        Me.Label14.TabIndex = 2
        Me.Label14.Text = "Site"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.radFamily)
        Me.GroupControl3.Controls.Add(Me.radStaff)
        Me.GroupControl3.Location = New System.Drawing.Point(12, 9)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.ShowCaption = False
        Me.GroupControl3.Size = New System.Drawing.Size(244, 42)
        Me.GroupControl3.TabIndex = 0
        '
        'radFamily
        '
        Me.radFamily.EditValue = True
        Me.radFamily.Location = New System.Drawing.Point(9, 12)
        Me.radFamily.Name = "radFamily"
        Me.radFamily.Properties.AutoWidth = True
        Me.radFamily.Properties.Caption = "Import Family Data"
        Me.radFamily.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radFamily.Properties.RadioGroupIndex = 1
        Me.radFamily.Size = New System.Drawing.Size(113, 19)
        Me.radFamily.TabIndex = 0
        '
        'radStaff
        '
        Me.radStaff.Location = New System.Drawing.Point(128, 12)
        Me.radStaff.Name = "radStaff"
        Me.radStaff.Properties.AutoWidth = True
        Me.radStaff.Properties.Caption = "Import Staff Data"
        Me.radStaff.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.radStaff.Properties.RadioGroupIndex = 1
        Me.radStaff.Size = New System.Drawing.Size(107, 19)
        Me.radStaff.TabIndex = 1
        Me.radStaff.TabStop = False
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.GroupControl2.Controls.Add(Me.btnImport)
        Me.GroupControl2.Controls.Add(Me.txtFile)
        Me.GroupControl2.Controls.Add(Me.CareLabel2)
        Me.GroupControl2.Controls.Add(Me.btnBrowse)
        Me.GroupControl2.Location = New System.Drawing.Point(262, 9)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.ShowCaption = False
        Me.GroupControl2.Size = New System.Drawing.Size(490, 42)
        Me.GroupControl2.TabIndex = 1
        '
        'btnImport
        '
        Me.btnImport.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnImport.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImport.Appearance.Options.UseFont = True
        Me.btnImport.Location = New System.Drawing.Point(416, 9)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(65, 24)
        Me.btnImport.TabIndex = 4
        Me.btnImport.Tag = ""
        Me.btnImport.Text = "Import"
        '
        'txtFile
        '
        Me.txtFile.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
            Or AnchorStyles.Right), AnchorStyles)
        Me.txtFile.CharacterCasing = CharacterCasing.Normal
        Me.txtFile.EnterMoveNextControl = True
        Me.txtFile.Location = New System.Drawing.Point(71, 11)
        Me.txtFile.MaxLength = 0
        Me.txtFile.Name = "txtFile"
        Me.txtFile.NumericAllowNegatives = False
        Me.txtFile.NumericMode = Care.Controls.CareTextBox.EnumNumericMode.UseTag
        Me.txtFile.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtFile.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.txtFile.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFile.Properties.Appearance.Options.UseFont = True
        Me.txtFile.Properties.Appearance.Options.UseTextOptions = True
        Me.txtFile.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.txtFile.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtFile.Size = New System.Drawing.Size(268, 22)
        Me.txtFile.TabIndex = 1
        Me.txtFile.Tag = "R"
        Me.txtFile.TextAlign = HorizontalAlignment.Left
        Me.txtFile.ToolTipText = ""
        '
        'CareLabel2
        '
        Me.CareLabel2.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CareLabel2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.CareLabel2.Location = New System.Drawing.Point(8, 14)
        Me.CareLabel2.Name = "CareLabel2"
        Me.CareLabel2.Size = New System.Drawing.Size(57, 15)
        Me.CareLabel2.TabIndex = 0
        Me.CareLabel2.Text = "Import File"
        Me.CareLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CareLabel2.ToolTip = "The group start date indicates when the child started in the particular group."
        Me.CareLabel2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'btnBrowse
        '
        Me.btnBrowse.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
        Me.btnBrowse.Appearance.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBrowse.Appearance.Options.UseFont = True
        Me.btnBrowse.Location = New System.Drawing.Point(345, 9)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(65, 24)
        Me.btnBrowse.TabIndex = 2
        Me.btnBrowse.Tag = ""
        Me.btnBrowse.Text = "Browse"
        '
        'frmImportParenta
        '
        Me.Appearance.Options.UseFont = True
        Me.ClientSize = New System.Drawing.Size(764, 561)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.gbxFamily)
        Me.Controls.Add(Me.cgRecords)
        Me.Name = "frmImportParenta"
        Me.WindowState = FormWindowState.Maximized
        CType(Me.gbxFamily, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxFamily.ResumeLayout(False)
        Me.gbxFamily.PerformLayout()
        CType(Me.cbxSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxKeyworker.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbxGroup.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtStartDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdtStartDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.radFamily.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radStaff.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.txtFile.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cgRecords As Care.Controls.CareGrid
    Friend WithEvents gbxFamily As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cbxKeyworker As Care.Controls.CareComboBox
    Friend WithEvents cbxGroup As Care.Controls.CareComboBox
    Friend WithEvents Label15 As Care.Controls.CareLabel
    Private WithEvents cdtStartDate As Care.Controls.CareDateTime
    Friend WithEvents Label13 As Care.Controls.CareLabel
    Friend WithEvents Label14 As Care.Controls.CareLabel
    Friend WithEvents ofd As OpenFileDialog
    Friend WithEvents CareLabel1 As Care.Controls.CareLabel
    Friend WithEvents cbxSite As Care.Controls.CareComboBox
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents radFamily As Care.Controls.CareRadioButton
    Friend WithEvents radStaff As Care.Controls.CareRadioButton
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CareLabel2 As Care.Controls.CareLabel
    Friend WithEvents btnBrowse As Care.Controls.CareButton
    Friend WithEvents txtFile As Care.Controls.CareTextBox
    Friend WithEvents btnImport As Care.Controls.CareButton

End Class
