﻿Imports Care.Global
Imports Care.Shared
Imports Care.Data

Public Class frmFood

    Private m_Food As Business.Food
    Private m_TAPI As New TescoAPI

#Region "Overrides"

    Protected Overrides Sub FindRecord()

        Dim _ReturnValue As String = Business.Food.FindFood(Me)
        If _ReturnValue <> "" Then

            MyBase.RecordID = New Guid(_ReturnValue)
            MyBase.RecordPopulated = True

            m_Food = Business.Food.RetreiveByID(New Guid(_ReturnValue))
            cbxManu.SelectedValue = m_Food._ManuId
            cbxGroup.SelectedValue = m_Food._GroupId

            bs.DataSource = m_Food

        End If

    End Sub

    Protected Overrides Sub CommitUpdate()

        m_Food = CType(bs.Item(bs.Position), Business.Food)

        m_Food._GroupId = New Guid(cbxGroup.SelectedValue.ToString)
        m_Food._GroupName = cbxGroup.Text

        If cbxManu.SelectedIndex > 0 Then
            m_Food._ManuId = New Guid(cbxManu.SelectedValue.ToString)
            m_Food._ManuName = cbxManu.Text
        Else
            m_Food._ManuId = Nothing
            m_Food._ManuName = ""
        End If

        Business.Food.SaveRecord(m_Food)

    End Sub

    Protected Overrides Sub SetBindings()

        m_Food = New Business.Food
        bs.DataSource = m_Food

        txtName.DataBindings.Add("Text", bs, "_Name")
        chkBabyFood.DataBindings.Add("Checked", bs, "_BabyFood")

        txtWeight.DataBindings.Add("Text", bs, "_Weight")
        txtPack.DataBindings.Add("Text", bs, "_PackSize")

        txtCost.DataBindings.Add("Text", bs, "_Cost")
        txtCostUnit.DataBindings.Add("Text", bs, "_CostItem")
        txtQty.DataBindings.Add("Text", bs, "_QtyPhys")
        txtQtyMin.DataBindings.Add("Text", bs, "_QtyMin")

        txtNutKJ.DataBindings.Add("Text", bs, "_NutKj")
        txtNutKcal.DataBindings.Add("Text", bs, "_NutKcal")
        txtProtein.DataBindings.Add("Text", bs, "_NutProt")
        txtCarbs.DataBindings.Add("Text", bs, "_NutCarb")
        txtFat.DataBindings.Add("Text", bs, "_NutFat")
        txtFibre.DataBindings.Add("Text", bs, "_NutFibre")
        txtSalt.DataBindings.Add("Text", bs, "_NutSalt")

        txtIngredients.DataBindings.Add("Text", bs, "_Ingredients")

        chkEggs.DataBindings.Add("Checked", bs, "_Eggs")
        chkMilk.DataBindings.Add("Checked", bs, "_Milk")
        chkFish.DataBindings.Add("Checked", bs, "_Fish")
        chkCrustaceans.DataBindings.Add("Checked", bs, "_Crustaceans")
        chkMolluscs.DataBindings.Add("Checked", bs, "_Molluscs")
        chkPeanuts.DataBindings.Add("Checked", bs, "_Peanuts")
        chkTreenuts.DataBindings.Add("Checked", bs, "_Treenuts")
        chkSeasameSeeds.DataBindings.Add("Checked", bs, "_Sesame")
        chkCereals.DataBindings.Add("Checked", bs, "_Cereals")
        chkSoya.DataBindings.Add("Checked", bs, "_Soya")
        chkCelery.DataBindings.Add("Checked", bs, "_Celery")
        chkMustard.DataBindings.Add("Checked", bs, "_Mustard")
        chkLupin.DataBindings.Add("Checked", bs, "_Lupin")
        chkSulphur.DataBindings.Add("Checked", bs, "_Sulphur")

    End Sub

    Protected Overrides Sub AfterAdd()
        btnLookup.Enabled = True
    End Sub

    Protected Overrides Sub AfterEdit()

    End Sub

    Protected Overrides Sub CommitDelete()
        Dim _SQL As String = "delete from Food where ID = '" + m_Food._ID.Value.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)
    End Sub

    Protected Overrides Function BeforeDelete() As Boolean

        Dim _Return As Boolean = True
        Dim _Meals As String = ""
        Dim _SQL As String = "select Meals.name from MealComponents" & _
                             " left join Meals on Meals.ID = MealComponents.meal_id" & _
                             " where MealComponents.food_id = '" + m_Food._ID.Value.ToString + "'"

        Dim _DT As DataTable = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)
        If _DT IsNot Nothing Then

            If _DT.Rows.Count > 0 Then

                For Each _DR As DataRow In _DT.Rows
                    _Meals += _DR.Item("name").ToString.TrimEnd + vbCrLf
                Next

                CareMessage("This food item cannot be deleted as its used in the following Meals:" + vbCrLf + vbCrLf + _Meals, MessageBoxIcon.Exclamation, "Delete Food")
                _Return = False

            End If

            _DT.Dispose()
            _DT = Nothing

        End If

        Return _Return

    End Function

#End Region

    Private Sub frmFood_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        With cbxGroup
            .DataSource = ListHandler.ReturnItems("Food Groups")
            .ValueMember = "_ID"
            .DisplayMember = "_Name"
            .SelectedIndex = -1
        End With

        With cbxManu
            .DataSource = ListHandler.ReturnItems("Food Manufacturers")
            .ValueMember = "_ID"
            .DisplayMember = "_Name"
            .SelectedIndex = -1
        End With

        btnLookup.Enabled = False

    End Sub

    Private Sub unePack_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)
        'uceCostUnit.Value = m_Food._CostItem
    End Sub

    Private Sub uceCost_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)
        'uceCostUnit.Value = m_Food._CostItem
    End Sub

    Private Sub btnLookup_Click(sender As Object, e As EventArgs) Handles btnLookup.Click
        DoSearch()
    End Sub

    Private Sub DoSearch()

        If m_TAPI.LoggedIn Then

            Dim _SearchText As String = InputBox("Barcode:")
            Dim _Products As List(Of TescoAPI.ProductExtended) = m_TAPI.SearchExtended(_SearchText)
            If _Products IsNot Nothing Then
                If _Products.Count = 1 Then

                    Dim _P As TescoAPI.ProductExtended = _Products.First

                    txtName.Text = _P.Name
                    txtCost.Text = _P.Price

                    For Each _n As TescoAPI.Nutrient In _P.Nutrients
                        If _n.NutrientName.ToUpper.Contains("ENERGY") Then txtNutKJ.Text = _n.SampleSize
                        If _n.NutrientName.ToUpper.Contains("KCAL") Then txtNutKcal.Text = _n.SampleSize
                        If _n.NutrientName.ToUpper.Contains("PROTEIN") Then txtProtein.Text = _n.SampleSize
                        If _n.NutrientName.ToUpper.Contains("CARBOHYDRATE") Then txtCarbs.Text = _n.SampleSize
                        If _n.NutrientName.ToUpper.Contains("FAT") Then txtFat.Text = _n.SampleSize
                        If _n.NutrientName.ToUpper.Contains("FIBRE") Then txtFibre.Text = _n.SampleSize
                        If _n.NutrientName.ToUpper.Contains("SALT") Then txtSalt.Text = _n.SampleSize
                        If _n.NutrientName.ToUpper.Contains("SODIUM") Then txtSalt.Text = _n.SampleSize
                    Next

                    txtIngredients.Text = ""
                    For Each _i As TescoAPI.Ingredient In _P.Ingredients
                        If txtIngredients.Text = "" Then
                            txtIngredients.Text = _i.Name
                        Else
                            txtIngredients.Text += ", " + _i.Name
                        End If
                    Next

                End If
            End If

        Else
            If CareMessage("Try Login?", MessageBoxIcon.Question, MessageBoxButtons.YesNo, "Tesco API") = DialogResult.Yes Then
                m_TAPI.Login()
                DoSearch()
            End If
        End If
    End Sub

    Private Sub TableLayoutPanel1_Paint(sender As Object, e As PaintEventArgs)

    End Sub
End Class

