﻿Imports Care.Global
Imports Care.Data
Imports DevExpress.XtraGrid.Columns

Public Class frmTariffMonthMatrix

    Private m_TariffID As Guid = Nothing
    Private m_TariffDesc As String = ""
    Private m_AgeBands As List(Of Business.TariffAgeBand) = Nothing
    Private m_MatrixData As List(Of Business.TariffDay) = Nothing
    Private m_SelectedMatrixRecord As Business.TariffDay = Nothing

    Public Sub New(ByVal TariffID As Guid, ByVal TariffDesc As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_TariffID = TariffID
        m_TariffDesc = TariffDesc

        Me.Text = "Age Matrix for " + m_TariffDesc

    End Sub

    Private Sub frmTariffMatrix_Load(sender As Object, e As EventArgs) Handles Me.Load

        txtValue.BackColor = Session.ChangeColour
        gbxMatrix.Hide()

        'fetch and cache all the data from DB
        m_AgeBands = Business.TariffAgeBand.RetreiveAll
        m_MatrixData = Business.TariffDay.RetreiveAll

        grdMatrix.HideFirstColumn = True
        grdMatrix.Populate(m_AgeBands)

        For _i As Integer = 1 To 7
            Dim _Desc As String = _i.ToString + " days"
            If _i = 1 Then _Desc = "1 day"
            Dim _col As GridColumn = grdMatrix.UnderlyingGridView.Columns.AddVisible(_i.ToString, _Desc)
            _col.UnboundType = DevExpress.Data.UnboundColumnType.Decimal
            _col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            _col.DisplayFormat.FormatString = "0.00"
        Next

        grdMatrix.Columns("_Description").Caption = "Age Band"
        grdMatrix.Columns("_MinAge").Caption = "Min Age"
        grdMatrix.Columns("_MaxAge").Caption = "Max Age"
        grdMatrix.Columns("IsNew").Visible = False
        grdMatrix.Columns("IsDeleted").Visible = False

        grdMatrix.Columns("_MinAge").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending

    End Sub

    Private Sub grdMatrix_CustomUnboundColumnData(sender As Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs) Handles grdMatrix.CustomUnboundColumnData
        If e.IsGetData Then
            If e.Row IsNot Nothing Then
                Dim _tb As Business.TariffAgeBand = CType(e.Row, Business.TariffAgeBand)
                Dim _DaysPerWeek As String = e.Column.FieldName
                e.Value = GetMatrixRate(_tb._ID, _DaysPerWeek)
            Else
                e.Value = 0
            End If
        End If
    End Sub

    Private Function GetMatrixRate(ByVal DaysPerWeek As String, ByVal MatrixRecord As Business.TariffDay) As Decimal
        If DaysPerWeek = "1" Then Return MatrixRecord._Days1
        If DaysPerWeek = "2" Then Return MatrixRecord._Days2
        If DaysPerWeek = "3" Then Return MatrixRecord._Days3
        If DaysPerWeek = "4" Then Return MatrixRecord._Days4
        If DaysPerWeek = "5" Then Return MatrixRecord._Days5
        If DaysPerWeek = "6" Then Return MatrixRecord._Days6
        If DaysPerWeek = "7" Then Return MatrixRecord._Days7
        Return 0
    End Function

    Private Function GetMatrixRate(ByVal AgeBand As Guid?, ByVal DaysPerWeek As String) As Decimal
        Dim _TD As Business.TariffDay = GetMatrixRecord(AgeBand)
        If _TD IsNot Nothing Then
            Return GetMatrixRate(DaysPerWeek, _TD)
        Else
            Return 0
        End If
    End Function

    Private Function GetMatrixRecord(ByVal AgeBandID As Guid?) As Business.TariffDay

        Dim _Q As IEnumerable(Of Business.TariffDay) = From _M As Business.TariffDay In m_MatrixData _
                                                          Where _M._TariffId = m_TariffID _
                                                          And _M._AgeId.Value = AgeBandID

        If _Q IsNot Nothing Then
            If _Q.Count = 1 Then
                Return _Q.First
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If

    End Function

    Private Sub grdMatrix_GridDoubleClick(sender As Object, e As EventArgs) Handles grdMatrix.GridDoubleClick
        AmendValue()
    End Sub

    Private Sub AmendValue()

        Dim _AgeID As String = grdMatrix.GetRowCellValue(grdMatrix.UnderlyingGridView.FocusedRowHandle, "_ID").ToString
        Dim _Age As String = grdMatrix.GetRowCellValue(grdMatrix.UnderlyingGridView.FocusedRowHandle, "_Description").ToString
        Dim _DaysPerWeek As String = grdMatrix.UnderlyingGridView.FocusedColumn.FieldName
        Dim _Desc As String = grdMatrix.UnderlyingGridView.FocusedColumn.Caption + ", " + _Age

        If IsNumeric(_DaysPerWeek) Then

            Dim _M As Business.TariffDay = GetMatrixRecord(New Guid(_AgeID))
            If _M IsNot Nothing Then
                m_SelectedMatrixRecord = _M
                txtValue.Text = Format(GetMatrixRate(_DaysPerWeek, _M), "0.00")
            Else
                m_SelectedMatrixRecord = Nothing
                txtValue.Text = ""
            End If

            gbxMatrix.Text = "Amend monthly fee for " + _Desc

            grdMatrix.Enabled = False
            gbxMatrix.Show()

            txtValue.Focus()

        Else
            'we didn't select 1 day, 2 days etc
        End If

    End Sub

    Private Sub btnAmendCancel_Click(sender As Object, e As EventArgs) Handles btnAmendCancel.Click
        grdMatrix.Enabled = True
        gbxMatrix.Hide()
    End Sub

    Private Sub btnAmendOK_Click(sender As Object, e As EventArgs) Handles btnAmendOK.Click

        UpdateMatrix()

        grdMatrix.Enabled = True
        grdMatrix.UnderlyingGridView.RefreshRow(grdMatrix.UnderlyingGridView.FocusedRowHandle)

        gbxMatrix.Hide()

    End Sub

    Private Sub UpdateMatrix()

        Dim _AgeID As String = grdMatrix.GetRowCellValue(grdMatrix.UnderlyingGridView.FocusedRowHandle, "_ID").ToString
        Dim _DaysPerWeek As String = grdMatrix.UnderlyingGridView.FocusedColumn.FieldName
        Dim _Value As Decimal = ValueHandler.ConvertDecimal(txtValue.Text)

        If m_SelectedMatrixRecord Is Nothing Then

            Dim _M As New Business.TariffDay
            With _M
                ._TariffId = m_TariffID
                ._AgeId = New Guid(_AgeID)
                ._Days1 = 0
                ._Days2 = 0
                ._Days3 = 0
                ._Days4 = 0
                ._Days5 = 0
                ._Days6 = 0
                ._Days7 = 0
            End With

            UpdateMatrixRow(_M, _DaysPerWeek, _Value)
            m_MatrixData.Add(_M)

        Else
            m_MatrixData.Remove(m_SelectedMatrixRecord)
            UpdateMatrixRow(m_SelectedMatrixRecord, _DaysPerWeek, _Value)
            m_MatrixData.Add(m_SelectedMatrixRecord)
        End If

    End Sub

    Private Sub UpdateMatrixRow(ByRef MatrixRecord As Business.TariffDay, ByVal DaysPerWeek As String, ByVal Rate As Decimal)
        If DaysPerWeek = "1" Then MatrixRecord._Days1 = Rate
        If DaysPerWeek = "2" Then MatrixRecord._Days2 = Rate
        If DaysPerWeek = "3" Then MatrixRecord._Days3 = Rate
        If DaysPerWeek = "4" Then MatrixRecord._Days4 = Rate
        If DaysPerWeek = "5" Then MatrixRecord._Days5 = Rate
        If DaysPerWeek = "6" Then MatrixRecord._Days6 = Rate
        If DaysPerWeek = "7" Then MatrixRecord._Days7 = Rate
    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Me.DialogResult = DialogResult.OK
        Business.TariffDay.SaveAll(m_MatrixData)
        Me.Close()
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Class DayPerWeek

        Public Days As String
        Public Description As String

        Public Sub New(ByVal Days As String, ByVal Desc As String)
            Days = Days
            Description = Desc
        End Sub
    End Class

End Class
