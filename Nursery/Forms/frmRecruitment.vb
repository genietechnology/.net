﻿

Imports Care.Global
Imports Care.Shared
Imports Care.Data

Public Class frmRecruitment

    Private m_Job As Business.RecruitJob
    Private m_FormTitle As String = ""

    Private Sub frmRecruitment_Load(sender As Object, e As EventArgs) Handles Me.Load

        m_FormTitle = Me.Text

        cbxDepartment.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Department"))
        cbxSite.PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)
        cbxLineManager.PopulateWithSQL(Session.ConnectionString, Business.Staff.LineManagerSQL)
        cbxStage.PopulateFromListMaintenance(ListHandler.ReturnListFromCache("Recruitment Stages"))

        cbxStatus.AddItem("Open")
        cbxStatus.AddItem("Closed")

        ClearLabels()

    End Sub

    Protected Overrides Function BeforeDelete() As Boolean
        If InputBox("Type 'Delete' to Continue", "Delete Vacancy").ToUpper = "DELETE" Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Overrides Sub CommitDelete()

        Dim _SQL As String = "delete from RecruitApp"
        _SQL += " where job_id = '" + m_Job._ID.Value.ToString + "'"

        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        Business.RecruitJob.DeleteRecord(m_Job._ID.Value)

    End Sub

    Protected Overrides Sub FindRecord()

        Dim _SQL As String = ""

        _SQL += "select ref as 'Ref', site_name as 'Site', name as 'Name', status as 'Status', date_to as 'Closing'"
        _SQL += " from RecruitJob"

        Dim _ReturnValue As String = ""
        Dim _Find As New GenericFind
        With _Find
            .Caption = "Find Vacancy"
            .ParentForm = Me
            .PopulateOnLoad = True
            .ConnectionString = Session.ConnectionString
            .GridSQL = _SQL
            .GridOrderBy = "order by name, date_to desc"
            .ReturnField = "ID"
            .FormWidth = 900
            .Show()
            _ReturnValue = .ReturnValue
        End With

        If _ReturnValue <> "" Then
            DisplayRecord(New Guid(_ReturnValue))
        End If

    End Sub

    Protected Overrides Sub AfterAcceptChanges()
        cbxStage.Enabled = True
        cbxStage.ReadOnly = False
        cgApps.ButtonsEnabled = False
    End Sub

    Protected Overrides Sub AfterCommitUpdate()

    End Sub

    Protected Overrides Sub AfterCancelChanges()
        cbxStage.Enabled = True
        cbxStage.ReadOnly = False
        cgApps.ButtonsEnabled = False
    End Sub

    Protected Overrides Sub CommitUpdate()
        m_Job = CType(bs.Item(bs.Position), Business.RecruitJob)
        m_Job.Store()
    End Sub

    Protected Overrides Sub SetBindings()

        m_Job = New Business.RecruitJob
        bs.DataSource = m_Job

        txtRef.DataBindings.Add("Text", bs, "_Ref")
        txtName.DataBindings.Add("Text", bs, "_Name")
        cbxStatus.DataBindings.Add("Text", bs, "_Status")

        txtJobTitle.DataBindings.Add("Text", bs, "_JobTitle")
        cbxSite.DataBindings.Add("Text", bs, "_SiteName")
        cbxDepartment.DataBindings.Add("Text", bs, "_Department")
        cbxLineManager.DataBindings.Add("Text", bs, "_LineMan")

        cdtDateFrom.DataBindings.Add("Value", bs, "_DateFrom", True)
        cdtDateTo.DataBindings.Add("Value", bs, "_DateTo", True)
        cdtDateInt.DataBindings.Add("Value", bs, "_DateInt", True)
        cdtDateStart.DataBindings.Add("Value", bs, "_DateStart", True)

        txtContHours.DataBindings.Add("EditValue", bs, "_ContHours")
        txtContSalary.DataBindings.Add("EditValue", bs, "_ContSalary")

    End Sub

    Protected Overrides Sub AfterAdd()

        MyBase.RecordID = Nothing
        MyBase.RecordPopulated = False

        m_Job = New Business.RecruitJob
        bs.DataSource = m_Job

        cgAds.ButtonsEnabled = False

        ClearLabels()

        Me.Text = m_FormTitle + " - New Vacancy"

        cbxStatus.Text = "Open"
        txtRef.Focus()

    End Sub

    Private Sub ClearLabels()
        lblOpening.Text = ""
        lblClosing.Text = ""
        lblInterviews.Text = ""
        lblStarting.Text = ""
    End Sub

    Protected Overrides Sub AfterEdit()

        Select Case ctMain.SelectedTabPage.Name

            Case "tabVacancy"
                cgAds.ButtonsEnabled = True
                txtRef.Focus()

            Case "tabApplicants"
                cgApps.ButtonsEnabled = True

        End Select

    End Sub

    Private Sub DisplayRecord(ByVal ID As Guid)

        MyBase.RecordID = ID
        MyBase.RecordPopulated = True

        m_Job = Business.RecruitJob.RetreiveByID(ID)
        bs.DataSource = m_Job

        DisplayApplicants()

    End Sub

    Private Sub DisplayApplicants()

        Dim _SQL As String = ""
        _SQL += "select ID, fullname as 'Name', qual_level as 'Level', stage as 'Stage',"
        _SQL += " chk_id_when as 'ID Check', chk_qual_when as 'Q Check',"
        _SQL += " chk_dbs_when as 'DBS Check', chk_ruk_when as 'RUK Check',"
        _SQL += " chk_ref1_when as 'Ref1 Check', chk_ref2_when as 'Ref2 Check',"
        _SQL += " tel_mobile as 'Mobile'"
        _SQL += " from RecruitApp"
        _SQL += " where job_id = '" + m_Job._ID.Value.ToString + "'"

        If cbxStage.Text <> "" Then
            _SQL += " and stage = '" + cbxStage.Text + "'"
        End If

        _SQL += " order by fullname"

        cgApps.HideFirstColumn = True
        cgApps.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub cgApps_AddClick(sender As Object, e As EventArgs) Handles cgApps.AddClick
        ShowRecruitForm(Nothing)
    End Sub

    Private Sub cgApps_EditClick(sender As Object, e As EventArgs) Handles cgApps.EditClick
        ShowRecruitForm(New Guid(cgApps.CurrentRow("ID").ToString))
    End Sub

    Private Sub cgApps_RemoveClick(sender As Object, e As EventArgs) Handles cgApps.RemoveClick
        If cgApps.CurrentRow IsNot Nothing Then
            If CareMessage("Are you sure you want to delete this Applicant?", MessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, "Confirm Delete") = DialogResult.Yes Then
                If InputBox("Please enter DELETE to continue.").ToUpper = "DELETE" Then
                    Dim _SQL As String = "delete from RecruitApp where ID = '" + cgApps.CurrentRow("ID").ToString + "'"
                    DAL.ExecuteSQL(Session.ConnectionString, _SQL)
                    cgApps.RePopulate()
                End If
            End If
        End If
    End Sub

    Private Sub ShowRecruitForm(ByVal RecordID As Guid?)

        If m_Job Is Nothing OrElse m_Job._ID.HasValue = False OrElse m_Job._ID.Value.ToString = "" Then Exit Sub

        Dim _frm As New frmRecruit(m_Job, RecordID)
        _frm.ShowDialog()

        If _frm.DialogResult = DialogResult.OK Then
            cgApps.RePopulate()
        End If

    End Sub

    Private Sub cbxStage_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxStage.SelectedIndexChanged
        DisplayApplicants()
    End Sub

    Private Sub ctMain_SelectedPageChanging(sender As Object, e As DevExpress.XtraTab.TabPageChangingEventArgs) Handles ctMain.SelectedPageChanging
        If e.Page.Name = "tabApplicants" Then
            Me.ToolbarMode = ToolbarEnum.EditOnly
            cbxStage.Enabled = True
            cbxStage.ReadOnly = False
            cgApps.ButtonsEnabled = False
        Else
            Me.ToolbarMode = ToolbarEnum.Standard
        End If
    End Sub
End Class