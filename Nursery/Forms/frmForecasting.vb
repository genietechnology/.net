﻿Imports Care.Global
Imports Care.Data
Imports DevExpress.XtraPivotGrid
Imports System.Windows.Forms

Public Class frmForecasting

    Private m_SiteExclusions As Boolean = False
    Private m_PivotData As DataTable = Nothing

    Private Sub frmForecasting_Load(sender As Object, e As EventArgs) Handles Me.Load

        m_SiteExclusions = Business.Site.UserHasExclusions

        With cbxSite
            .AllowBlank = Not m_SiteExclusions
            .PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)
        End With

    End Sub

    Private Sub frmForecasting_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        btnRefresh.Enabled = False
        Application.DoEvents()

        Session.CursorWaiting()

        ThisWeek()

        btnRefresh.Enabled = True
        Session.CursorDefault()

    End Sub

    Private Sub AddField(ByVal FieldName As String, ByVal Caption As String, ByVal FormatType As DevExpress.Utils.FormatType, ByVal Area As PivotArea)

        Dim _PGV As New PivotGridField(FieldName, Area)
        With _PGV

            .Caption = Caption

            If FormatType = DevExpress.Utils.FormatType.DateTime Then
                _PGV.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime
                _PGV.ValueFormat.FormatString = "dd/MM/yyyy"
            Else
                _PGV.ValueFormat.FormatType = FormatType
            End If

        End With

        PivotGridControl1.Fields.Add(_PGV)

    End Sub

    Private Sub BuildPivotGrid()

        If m_SiteExclusions AndAlso cbxSite.SelectedIndex < 0 Then Exit Sub
        If cdtFrom.Text = "" Then Exit Sub
        If cdtTo.Text = "" Then Exit Sub

        Session.CursorWaiting()

        Dim _SQL As String = "select date, r.room_name, rr.capacity, rr.ratio," &
                             " b.child_name, b.am, b.pm, b.tariff_rate from Calendar c" &
                             " left join Bookings b on b.booking_date = c.date" &
                             " left join SiteRoomRatios rr on rr.ID = b.ratio_id" &
                             " left join SiteRooms r on r.ID = rr.room_id" &
                             " where c.weekend = 0"

        If cbxSite.Text <> "" Then _SQL += " and b.site_name = '" & cbxSite.Text & "'"
        If chkExclHolidays.Checked Then _SQL += " and b.booking_status <> 'Holiday'"

        _SQL += " and c.date between '" & ValueHandler.SQLDate(cdtFrom.Value.Value) & "' and '" & ValueHandler.SQLDate(cdtTo.Value.Value) & "'"

        _SQL += " order by c.date"

        m_PivotData = DAL.GetDataTablefromSQL(Session.ConnectionString, _SQL)

        PivotGridControl1.DataSource = m_PivotData
        PivotGridControl1.OptionsData.DataFieldUnboundExpressionMode = DataFieldUnboundExpressionMode.UseSummaryValues

        PivotGridControl1.Fields.Clear()

        If radHeadCountDate.Checked Then
            AddField("date", "Date", DevExpress.Utils.FormatType.DateTime, PivotArea.ColumnArea)
            AddField("room_name", "Room", DevExpress.Utils.FormatType.None, PivotArea.RowArea)
            AddField("child_name", "Child", DevExpress.Utils.FormatType.None, PivotArea.RowArea)
            AddField("am", "AM", DevExpress.Utils.FormatType.Numeric, PivotArea.DataArea)
            AddField("pm", "PM", DevExpress.Utils.FormatType.Numeric, PivotArea.DataArea)
        End If

        If radHeadCountRoom.Checked Then
            AddField("room_name", "Room", DevExpress.Utils.FormatType.None, PivotArea.ColumnArea)
            AddField("date", "Date", DevExpress.Utils.FormatType.DateTime, PivotArea.RowArea)
            AddField("am", "AM", DevExpress.Utils.FormatType.Numeric, PivotArea.DataArea)
            AddField("pm", "PM", DevExpress.Utils.FormatType.Numeric, PivotArea.DataArea)
        End If

        If radTurnOverDate.Checked Then
            AddField("date", "Date", DevExpress.Utils.FormatType.DateTime, PivotArea.ColumnArea)
            AddField("room_name", "Room", DevExpress.Utils.FormatType.None, PivotArea.RowArea)
            AddField("child_name", "Child", DevExpress.Utils.FormatType.None, PivotArea.RowArea)
            AddField("tariff_rate", "TurnOver", DevExpress.Utils.FormatType.Numeric, PivotArea.DataArea)
        End If

        If radTurnOverRoom.Checked Then
            AddField("room_name", "Room", DevExpress.Utils.FormatType.None, PivotArea.ColumnArea)
            AddField("date", "Date", DevExpress.Utils.FormatType.DateTime, PivotArea.RowArea)
            AddField("tariff_rate", "TurnOver", DevExpress.Utils.FormatType.Numeric, PivotArea.DataArea)
        End If

        PivotGridControl1.BestFit()
        PivotGridControl1.OptionsView.RowTotalsLocation = PivotRowTotalsLocation.Tree
        PivotGridControl1.OptionsView.RowTreeWidth = 230

        Session.CursorDefault()

    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        BuildPivotGrid()
    End Sub

    Private Sub PivotGridControl1_CellDoubleClick(sender As Object, e As PivotCellEventArgs) Handles PivotGridControl1.CellDoubleClick

        If radHeadCountDate.Checked Then
            If e.ColumnField IsNot Nothing Then
                Dim _Date As Date = ValueHandler.ConvertDate(e.GetFieldValue(e.ColumnField)).Value
                Bookings.BookingsDrillDown(_Date, Nothing, Nothing, "Forecasting Drilldown")
            End If
        Else
            If e.RowField IsNot Nothing Then
                Dim _Date As Date = ValueHandler.ConvertDate(e.GetFieldValue(e.RowField)).Value
                Bookings.BookingsDrillDown(_Date, Nothing, Nothing, "Forecasting Drilldown")
            End If
        End If

    End Sub

    Private Sub PivotGridControl1_CustomCellValue(sender As Object, e As PivotCellValueEventArgs) Handles PivotGridControl1.CustomCellValue

        If TypeOf (e.Value) Is DevExpress.Data.PivotGrid.PivotErrorValue Then
            e.Value = 0
        Else
            If e.Value Is Nothing Then
                e.Value = 0
            Else
                If IsDBNull(e.Value) Then
                    e.Value = 0
                Else
                    'If e.DataField.Name = "am_staff" Then
                    '    e.Value = Math.Ceiling(ValueHandler.ConvertDecimal(e.Value))
                    'End If
                End If
            End If
        End If

    End Sub

    Private Sub PivotGridControl1_CustomSummary(sender As Object, e As PivotGridCustomSummaryEventArgs) Handles PivotGridControl1.CustomSummary
        'other formatting
    End Sub

    Private Sub ThisWeek()

        Dim _Day As Date = Today
        While _Day.DayOfWeek <> DayOfWeek.Monday
            _Day = _Day.AddDays(-1)
        End While
        cdtFrom.Value = _Day
        cdtTo.Value = _Day.AddDays(6)

        BuildPivotGrid()

    End Sub

    Private Sub radWeekThis_CheckedChanged(sender As Object, e As EventArgs) Handles radWeekThis.CheckedChanged
        ThisWeek()
    End Sub

    Private Sub radWeekNext_CheckedChanged(sender As Object, e As EventArgs) Handles radWeekNext.CheckedChanged

        Dim _Day As Date = Today
        While _Day.DayOfWeek <> DayOfWeek.Monday
            _Day = _Day.AddDays(1)
        End While
        cdtFrom.Value = _Day
        cdtTo.Value = _Day.AddDays(6)

        BuildPivotGrid()

    End Sub

    Private Sub radMonthThis_CheckedChanged(sender As Object, e As EventArgs) Handles radMonthThis.CheckedChanged

        cdtFrom.Value = DateSerial(Today.Year, Today.Month, 1)

        Dim _NextMonth As Date
        If Today.Month = 12 Then
            _NextMonth = DateSerial(Today.Year + 1, 1, 1)
        Else
            _NextMonth = DateSerial(Today.Year, Today.Month + 1, 1)
        End If

        cdtTo.Value = _NextMonth.AddDays(-1)

        BuildPivotGrid()

    End Sub

    Private Sub radMonthNext_CheckedChanged(sender As Object, e As EventArgs) Handles radMonthNext.CheckedChanged

        Dim _NextMonth As Date
        If Today.Month = 12 Then
            _NextMonth = DateSerial(Today.Year + 1, 1, 1)
        Else
            _NextMonth = DateSerial(Today.Year, Today.Month + 1, 1)
        End If

        cdtFrom.Value = _NextMonth

        Dim _NextNextMonth As Date
        If _NextMonth.Month = 12 Then
            _NextNextMonth = DateSerial(_NextMonth.Year + 1, 1, 1)
        Else
            _NextNextMonth = DateSerial(_NextMonth.Year, _NextMonth.Month + 1, 1)
        End If

        cdtTo.Value = _NextNextMonth.AddDays(-1)

        BuildPivotGrid()

    End Sub

    Private Sub radHeadCountDate_CheckedChanged(sender As Object, e As EventArgs) Handles radHeadCountDate.CheckedChanged
        BuildPivotGrid()
    End Sub

    Private Sub radHeadCountRoom_CheckedChanged(sender As Object, e As EventArgs) Handles radHeadCountRoom.CheckedChanged
        BuildPivotGrid()
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click

        With PivotGridControl1.OptionsPrint.PageSettings
            .PaperKind = Drawing.Printing.PaperKind.A4
            .Landscape = True
            .Margins.Top = 25
            .Margins.Bottom = 25
            .Margins.Left = 25
            .Margins.Right = 25
        End With

        PivotGridControl1.ShowPrintPreview()

    End Sub

    Private Sub cbxSite_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSite.SelectedIndexChanged
        BuildPivotGrid()
    End Sub

    Private Sub radTurnOverDate_CheckedChanged(sender As Object, e As EventArgs) Handles radTurnOverDate.CheckedChanged
        BuildPivotGrid()
    End Sub

    Private Sub radTurnOverRoom_CheckedChanged(sender As Object, e As EventArgs) Handles radTurnOverRoom.CheckedChanged
        BuildPivotGrid()
    End Sub

End Class
