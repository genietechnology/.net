﻿Imports Care.Global

Public Class frmTariffUsage

    Private m_TariffID As Guid = Nothing
    Private m_TariffName As String = ""

    Public Sub New(ByVal TariffID As Guid, ByVal TariffName As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_TariffID = TariffID
        TariffName = TariffName

    End Sub

    Private Sub frmTariffPopup_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.Text = "Tariff Usage for " + m_TariffName
        PopulateGrid()
    End Sub

    Private Sub PopulateGrid()

        Dim _SQL As String = ""

        _SQL += " select c.id,"
        _SQL += " (select fullname from Children sqc where sqc.ID = c.ID) as 'Child',"
        _SQL += " count(*) as 'Usage' from ChildAdvanceSessions s"
        _SQL += " left join Children c on c.ID = s.child_id"
        _SQL += " where '" + m_TariffID.ToString + "'"
        _SQL += " in (s.monday, s.tuesday, s.wednesday, s.thursday, s.friday, s.saturday, s.sunday)"
        _SQL += " and c.status <> 'Left'"
        _SQL += " group by c.id"
        _SQL += " order by c.id, count(*) desc"

        cgTariff.HideFirstColumn = True
        cgTariff.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Function GetID() As Guid?
        If cgTariff Is Nothing Then Return Nothing
        If cgTariff.RecordCount < 1 Then Return Nothing
        If cgTariff.CurrentRow(0).ToString = "" Then Return Nothing
        Return New Guid(cgTariff.CurrentRow(0).ToString)
    End Function

    Private Sub cgTariff_GridDoubleClick(sender As Object, e As EventArgs) Handles cgTariff.GridDoubleClick

        Session.CursorWaiting()

        Dim _ID As Guid? = GetID()
        If _ID.HasValue Then
            Business.Child.DrillDown(_ID.Value, True)
        End If

        Session.CursorDefault()

    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class
