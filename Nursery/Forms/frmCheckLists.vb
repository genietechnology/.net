﻿

Imports Care.Global
Imports Care.Data
Imports Care.Shared
Imports DevExpress.XtraTab
Imports System.Windows.Forms
Imports DevExpress.XtraGrid.Views.Base

Public Class frmCheckLists

    Private m_Checklist As Business.CheckList
    Private m_FormTitle As String

    Private Sub frmCheckLists_Load(sender As Object, e As EventArgs) Handles Me.Load

        m_FormTitle = Me.Text

        cbxSite.PopulateWithSQL(Session.ConnectionString, Business.Site.ListSQL)
        cbxSite.AddItem("All Sites", "0")

        cbxFreq.AddItem("Every day")
        cbxFreq.AddItem("Every week")
        cbxFreq.AddItem("Every fornight")
        cbxFreq.AddItem("Every month")
        cbxFreq.AddItem("Every 6 weeks")
        cbxFreq.AddItem("Every quarter")
        cbxFreq.AddItem("Every 6 months")
        cbxFreq.AddItem("Every year")

        cbxWeekday.AddItem("Monday")
        cbxWeekday.AddItem("Tuesday")
        cbxWeekday.AddItem("Wednesday")
        cbxWeekday.AddItem("Thursday")
        cbxWeekday.AddItem("Friday")
        cbxWeekday.AddItem("Saturday")
        cbxWeekday.AddItem("Sunday")

        cbxItemResponse.AddItem("Yes/No")
        cbxItemResponse.AddItem("Reverse Yes/No")
        cbxItemResponse.AddItem("Decimal")
        cbxItemResponse.AddItem("Number")
        cbxItemResponse.AddItem("Time")

        ToggleItem(False)
        tabMain.DisableTabs()

        btnBlankCheckList.Enabled = False

    End Sub

    Private Sub DisplayRecord(ByVal ID As Guid)

        MyBase.RecordID = ID
        MyBase.RecordPopulated = True

        m_Checklist = Business.CheckList.RetreiveByID(ID)
        bs.DataSource = m_Checklist

        If m_Checklist._ChkStatus = "Active" Then
            chkActive.Checked = True
        Else
            chkActive.Checked = False
        End If

        Me.Text = m_FormTitle + " - " + m_Checklist._ChkName
        tabMain.EnableTabs()
        tabMain.SelectedTabPageIndex = 0

        DisplayItems()
        btnBlankCheckList.Enabled = True

    End Sub

    Protected Overrides Sub SetBindings()

        m_Checklist = New Business.CheckList
        bs.DataSource = m_Checklist

        txtName.DataBindings.Add("Text", bs, "_ChkName")
        cbxSite.DataBindings.Add("Text", bs, "_ChkScopeSiteName")
        cbxFreq.DataBindings.Add("Text", bs, "_ChkFreq")

    End Sub

    Protected Overrides Sub AfterAdd()

        m_Checklist = New Business.CheckList
        bs.DataSource = m_Checklist

        cbxSite.Text = "All Sites"

        cgItems.Clear()
        txtName.Focus()

    End Sub

    Protected Overrides Sub AfterEdit()
        cgItems.ButtonsEnabled = True
        txtName.Focus()
    End Sub

    Protected Overrides Sub FindRecord()

        Dim _SQL As String = ""

        _SQL += "select chk_name as 'Name', chk_status as 'Status', chk_scope_site_name as 'Site', chk_freq as 'Frequency'"
        _SQL += " from Checklists"

        Dim _ReturnValue As String = ""
        Dim _Find As New GenericFind
        With _Find
            .Caption = "Find Checklist"
            .ParentForm = Me
            .PopulateOnLoad = True
            .ConnectionString = Session.ConnectionString
            .GridSQL = _SQL
            .GridOrderBy = "order by name"
            .ReturnField = "ID"
            .FormWidth = 800
            .GridWhereClause = ""
            .Show()
            _ReturnValue = .ReturnValue
        End With

        If _ReturnValue <> "" Then
            DisplayRecord(New Guid(_ReturnValue))
        End If

    End Sub

    Protected Overrides Sub CommitUpdate()

        m_Checklist = CType(bs.Item(bs.Position), Business.CheckList)

        If chkActive.Checked Then
            m_Checklist._ChkStatus = "Active"
        Else
            m_Checklist._ChkStatus = "In-Active"
        End If

        m_Checklist.Store()
        cgItems.ButtonsEnabled = False

        Me.RecordID = m_Checklist._ID

    End Sub

    Protected Overrides Sub AfterCancelChanges()
        cgItems.ButtonsEnabled = False
    End Sub

    Protected Overrides Sub CommitDelete()

        Dim _SQL As String = ""

        _SQL = "delete from CheckListItems where list_id = '" + m_Checklist._ID.ToString + "'"
        DAL.ExecuteSQL(Session.ConnectionString, _SQL)

        Business.CheckList.DeleteRecord(m_Checklist._ID.Value)

    End Sub

    Private Sub cgItems_EditClick(sender As Object, e As EventArgs) Handles cgItems.EditClick
        Dim _ID As Guid? = GetID(cgItems)
        If _ID.HasValue Then
            panItem.Tag = _ID.ToString
            txtItemText.Text = cgItems.CurrentRow.Item("Question").ToString
            cbxItemResponse.Text = cgItems.CurrentRow.Item("Type").ToString
            txtItemSequence.Text = cgItems.CurrentRow.Item("Sequence").ToString
            ToggleItem(True)
        End If
    End Sub

    Private Sub cgItems_AddClick(sender As Object, e As EventArgs) Handles cgItems.AddClick
        ClearItem()
        ToggleItem(True)
    End Sub

    Private Sub ClearItem()
        panItem.Tag = ""
        txtItemText.Text = ""
        cbxItemResponse.SelectedIndex = 0
        txtItemSequence.Text = "0"
    End Sub

    Private Sub cgItems_RemoveClick(sender As Object, e As EventArgs) Handles cgItems.RemoveClick
        Dim _ID As Guid? = GetID(cgItems)
        If _ID.HasValue Then
            DAL.ExecuteSQL(Session.ConnectionString, "delete from CheckListItems where ID = '" + _ID.ToString + "'")
            DisplayItems()
        End If
    End Sub

    Private Function GetID(ByRef GridControl As Care.Controls.CareGridWithButtons) As Guid?

        If GridControl Is Nothing Then Return Nothing
        If GridControl.RecordCount < 0 Then Return Nothing
        If GridControl.CurrentRow Is Nothing Then Return Nothing

        Dim _ID As String = GridControl.CurrentRow.Item(0).ToString

        Return New Guid(_ID)

    End Function

    Private Function GetID(ByRef GridControl As Care.Controls.CareGrid) As Guid?

        If GridControl Is Nothing Then Return Nothing
        If GridControl.RecordCount < 0 Then Return Nothing
        If GridControl.CurrentRow Is Nothing Then Return Nothing

        Dim _ID As String = GridControl.CurrentRow.Item(0).ToString

        Return New Guid(_ID)

    End Function

    Private Sub ToggleItem(ByVal Visible As Boolean)

        panItem.Visible = Visible

        If Visible Then

            txtItemText.BackColor = Session.ChangeColour
            txtItemText.ReadOnly = False
            txtItemText.TabStop = True

            cbxItemResponse.BackColor = Session.ChangeColour
            cbxItemResponse.ReadOnly = False
            cbxItemResponse.TabStop = True

            txtItemSequence.BackColor = Session.ChangeColour
            txtItemSequence.ReadOnly = False
            txtItemSequence.TabStop = True

            txtItemText.Focus()

        End If

    End Sub

    Private Sub btnItemCancel_Click(sender As Object, e As EventArgs) Handles btnItemCancel.Click
        ToggleItem(False)
    End Sub

    Private Sub btnItemOK_Click(sender As Object, e As EventArgs) Handles btnItemOK.Click
        If Not CheckItem() Then Exit Sub
        SaveItem()
        ToggleItem(False)
    End Sub

    Private Function CheckItem() As Boolean

        If txtItemText.Text = "" Then
            CareMessage("Please enter the Question/Item text.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        If cbxItemResponse.SelectedIndex < 0 Then
            CareMessage("Please select the response type.", MessageBoxIcon.Exclamation, "Mandatory Field")
            Return False
        End If

        Return True

    End Function

    Private Sub DisplayItems()

        Dim _SQL As String = ""
        _SQL += "select ID, description as 'Question', item_type as 'Type', sequence as 'Sequence'"
        _SQL += " from CheckListItems"
        _SQL += " where list_id = '" + Me.RecordID.ToString + "'"
        _SQL += " order by sequence, description"

        cgItems.HideFirstColumn = True
        cgItems.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub SaveItem()

        Dim _Item As New Business.CheckListItem
        If panItem.Tag.ToString <> "" Then
            _Item = Business.CheckListItem.RetreiveByID(New Guid(panItem.Tag.ToString))
        Else
            _Item._ListId = Me.RecordID
        End If

        _Item._Description = txtItemText.Text
        _Item._ItemType = cbxItemResponse.Text
        _Item._Mandatory = True
        _Item._Points = 0
        _Item._Sequence = ValueHandler.ConvertInteger(txtItemSequence.Text)

        _Item.Store()

        DisplayItems()

    End Sub

    Private Sub btnBlankCheckList_Click(sender As Object, e As EventArgs) Handles btnBlankCheckList.Click

        Dim _SQL As String = ""
        _SQL += "select list_id, chk_name, chk_status, chk_scope_site_name, chk_freq,"
        _SQL += " description, item_type, sequence"
        _SQL += " from CheckLists l"
        _SQL += " left join CheckListItems i on i.list_id = l.ID"
        _SQL += " where l.ID = '" + m_Checklist._ID.Value.ToString + "'"
        _SQL += " order by sequence, description"

        Session.CursorWaiting()

        If btnBlankCheckList.ShiftDown Then
            btnBlankCheckList.Enabled = False
            ReportHandler.DesignReport("CheckList.repx", Session.ConnectionString, _SQL)
            btnBlankCheckList.Enabled = True
        Else
            btnBlankCheckList.Enabled = False
            ReportHandler.RunReport("CheckList.repx", Session.ConnectionString, _SQL)
            btnBlankCheckList.Enabled = True
        End If

    End Sub

    Private Sub btnPrintSelected_Click(sender As Object, e As EventArgs) Handles btnPrintSelected.Click

        Dim _ResponseID As Guid? = GetID(cgHeader)
        If _ResponseID.HasValue = False Then Exit Sub

        Dim _SQL As String = ""

        _SQL += "select c.ID, c.chk_name, c.chk_status, c.chk_scope_site_name, c.chk_freq, s.person_name, s.signature, s.stamp,"
        _SQL += " q.description, q.item_type, q.sequence, a.response, a.response_ok, a.response_comments"
        _SQL += " from CheckListRespItems a"
        _SQL += " left join CheckListItems q On q.ID = a.item_id"
        _SQL += " left join CheckListResp r on r.ID = a.response_id"
        _SQL += " left join Signatures s on s.ID = r.staff_signature"
        _SQL += " left join CheckLists c on c.ID = r.list_id"
        _SQL += " where r.ID = '" + _ResponseID.ToString + "'"
        _SQL += " order by sequence"

        If btnPrintSelected.ShiftDown Then
            btnPrintSelected.Enabled = False
            ReportHandler.DesignReport("CheckListCompleted.repx", Session.ConnectionString, _SQL)
            btnPrintSelected.Enabled = True
        Else
            btnPrintSelected.Enabled = False
            ReportHandler.RunReport("CheckListCompleted.repx", Session.ConnectionString, _SQL)
            btnPrintSelected.Enabled = True
        End If

    End Sub

    Private Sub tabMain_SelectedPageChanged(sender As Object, e As TabPageChangedEventArgs) Handles tabMain.SelectedPageChanged

        Select Case e.Page.Name

            Case "tabResults"
                DisplayHeaderGrid()

        End Select

    End Sub

    Private Sub DisplayHeaderGrid()

        Dim _SQL As String = ""
        _SQL += "select r.id, r.stamp as 'Completed', s.person_name as 'Staff' from CheckListResp r"
        _SQL += " left join Signatures s on s.ID = r.staff_signature"
        _SQL += " where r.list_id = '" + m_Checklist._ID.ToString + "'"
        _SQL += " order by r.stamp desc"

        cgHeader.HideFirstColumn = True
        cgHeader.Populate(Session.ConnectionString, _SQL)

    End Sub

    Private Sub cgHeader_FocusedRowChanged(sender As Object, e As FocusedRowChangedEventArgs) Handles cgHeader.FocusedRowChanged

        If e Is Nothing Then Exit Sub
        If e.FocusedRowHandle < 0 Then Exit Sub

        Dim _HeaderID As String = cgHeader.GetRowCellValue(e.FocusedRowHandle, "id").ToString

        Dim _SQL As String = ""

        _SQL += "select a.id, description As 'Item', item_type as 'Type', response as 'Response', response_ok as 'OK', response_comments"
        _SQL += " from CheckListRespItems a"
        _SQL += " left join CheckListItems q On q.ID = a.item_id"
        _SQL += " where response_id = '" + _HeaderID + "'"
        _SQL += " order by sequence"

        cgAnswers.HideFirstColumn = True
        cgAnswers.Populate(Session.ConnectionString, _SQL)

        cgAnswers.Columns("response_comments").Visible = False
        cgAnswers.PreviewColumn = "response_comments"

    End Sub

    Private Sub btnItemSaveAdd_Click(sender As Object, e As EventArgs) Handles btnItemSaveAdd.Click
        If Not CheckItem() Then Exit Sub
        SaveItem()
        ClearItem()
        txtItemText.Focus()
    End Sub

End Class
